package GER;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import sys.HtmlPopupBean;

/**
* <b>Title:</b>       Gerencial - Cadastar Destinatários <br>
* <b>Description:</b> Tela de Cadastro dos e-mails dos Relatores de acordo com a junta<br>
* <b>Copyright:</b>   Copyright (c) 2006<br>
 * <b>Company: </b>   MIND Informatica <br>
* @author  Luciana Rocha	
* @version 1.0
*/

public class CadastraDestinatarioBean extends HtmlPopupBean { 
	
	private String codDestinatario;
	private String txtDestinatario;
	private String txtEmail;
	private String codOrgao;
	private String sigOrgao;
	private String txtTitulo;
	private ArrayList msg;


	public CadastraDestinatarioBean(){
	  	super();
	  	setTabela("TSMI_DESTINATARIO");	
	  	setPopupWidth(35);
	  	codDestinatario	= "";	
	  	txtDestinatario = "";
	  	txtEmail	    = "";
	  	codOrgao        = "";
	  	txtTitulo       = "";
		sigOrgao        = "";
	  	msg             = new ArrayList();
	  	
	}


	public void setCodDestinatario(String codDestinatario) {
		if(codDestinatario == null) codDestinatario = "";
		else this.codDestinatario = codDestinatario;
	}
	public String getCodDestinatario() {
		return codDestinatario;
	}


	public void setTxtDestinatario(String txtDestinatario) {
		if(txtDestinatario == null) txtDestinatario = "";
		else this.txtDestinatario = txtDestinatario;
	}
	public String getTxtDestinatario() {
		return txtDestinatario;
	}


	public void setTxtEmail(String txtEmail) {
		if(txtEmail == null) txtEmail = "";
		else this.txtEmail = txtEmail;
	}
	public String getTxtEmail() {
		return txtEmail;
	}


	public void setCodOrgao(String codOrgao) {
		if(codOrgao == null) codOrgao = "";
		else this.codOrgao = codOrgao;
	}
	public String getCodOrgao() {
		return codOrgao;
	}


	public void setTxtTitulo(String txtTitulo) {
		if (txtTitulo == null) txtTitulo = "";
		else this.txtTitulo = txtTitulo;
	}
	public String getTxtTitulo() {
		return txtTitulo;
	}
	
	
	public void setMsg(ArrayList msg) {
		if(msg == null) msg = new ArrayList();
		else this.msg = msg;
	}
	public ArrayList getMsg() {
		return msg;
	}


	public void setSigOrgao(String sigOrgao) {
		if(sigOrgao == null) sigOrgao = "";
		else this.sigOrgao = sigOrgao;
	}
	public String getSigOrgao() {
		return sigOrgao;
	}

	
//---------------------------------------------------------------------------------------
	public List getDestinatarios(CadastraDestinatarioBean DestinatarioBean)  {
		List myDestinatrios = null;
		String todosOrgaos = "S";
		try  {
			Dao dao = Dao.getInstance();	
			if( (myDestinatrios = dao.getDestinatarios(DestinatarioBean, todosOrgaos)) == null )
				setMsgErro("Erro de Leitura:");
		}
		catch (Exception e) {
			setMsgErro("Erro de Leitura: " + e.getMessage());
		}
		return myDestinatrios ;
	}
	
	public List getDestinatarios(CadastraDestinatarioBean DestinatarioBean, String todosOrgaos)  {
		List myDestinatrios = null;
		try  {
			Dao dao = Dao.getInstance();
			if( (myDestinatrios = dao.getDestinatarios(DestinatarioBean,todosOrgaos)) == null )
				setMsgErro("Erro de Leitura:");
		}
		catch (Exception e) {
			setMsgErro("Erro de Leitura: " + e.getMessage());
		}
		return myDestinatrios ;
	}


//	---------------------------------------------------------------------------------------	  
		public boolean isInsertDestinatario(List myDestinatario)   {
			boolean retorno = true;
			Vector vErro = new Vector() ;
			try  {
				Dao dao = Dao.getInstance();	
				retorno = dao.InsertDestinatario(myDestinatario,vErro);    	   
			}
			catch (Exception e) {
				vErro.addElement("Erro na alteração: "+ e.getMessage() +"\n");
				retorno = false;
			}
			setMsgErro(vErro);
			return retorno;
		}




}
