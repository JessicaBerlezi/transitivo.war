package GER;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import sys.BeanException;

public class EstatisticaArtigoBean extends sys.HtmlPopupBean {

	private String artigo;
	private String descricao;
	private String mesAnoInicio;
	private String mesAnoFim;
	private String mesAno;
	private String codEstado;
	private String sigorgao;
	private String orgao;
	private String deferidos;
	private String indeferidos;
	private String todosOrgaos;
	private String tpreq;	
	private String totalGer;
	private String totalDef;
	private String totalInDef;
	private String defPercent;
	private String indefPercent;
	private String totalDefPerc;
	private String totalInDefPerc;
	private String totalMedia;
	private String total;
	private String totalMediaDef;
	private String totMediaInDef;

	private ArrayList resultado;
		
	public EstatisticaArtigoBean() throws sys.BeanException {
		artigo = "";
		descricao = "";
		defPercent = "";
		indefPercent = "";
		totalDefPerc = "";
		totalInDefPerc = "";
		totalMedia = "";
		totalMediaDef = "";  
		total ="0";
		totMediaInDef = "";
		mesAnoInicio = sys.Util.formatedToday().substring(0,10);
		mesAnoFim = sys.Util.formatedToday().substring(0,10);
		mesAno = sys.Util.formatedToday().substring(3,10);
		resultado = new ArrayList();
	}
	
	
	public void setResultado(ArrayList resultado) {
		this.resultado = resultado;
		if( resultado == null ) this.resultado = new ArrayList();
	}
	public ArrayList getResultado() {
		return this.resultado;
	}
	
	public void setTxtArtigo(String artigo) {
		this.artigo = artigo;
		if( artigo == null ) this.artigo ="";
	}
	public String getTxtArtigo() {
		return this.artigo;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
		if( descricao == null )	this.descricao ="";
	}
	public String getDescricao() {
		return this.descricao; 
	}

	public void setMesAnoInicio(String mesAnoInicio) {
		this.mesAnoInicio = mesAnoInicio;
		if( mesAnoInicio == null ) this.mesAnoInicio =sys.Util.formatedToday().substring(0,10);
	}
	public String getMesAnoInicio() {
		return this.mesAnoInicio; 
	}
	public void setMesAnoFim(String mesAnoFim) {
		this.mesAnoFim = mesAnoFim;
		if( mesAnoFim == null ) this.mesAnoFim =sys.Util.formatedToday().substring(0,10);
	}
	public String getMesAnoFim() {
		return this.mesAnoFim; 
	}
	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
		if( mesAno == null ) this.mesAno =sys.Util.formatedToday().substring(3,10);
	}
	public String getMesAno() {
		return this.mesAno; 
	}
	
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
		if( codEstado == null )	this.codEstado = "";
	}
	public String getCodEstado() { return this.codEstado; }

	public void setOrgao(String orgao) {
		this.orgao = orgao;
		if( orgao == null )	this.orgao = "";
	}
	public String getOrgao() { return this.orgao; }

	public void setSigOrgao(String sigorgao) {
		this.sigorgao = sigorgao;
		if( sigorgao == null )	this.sigorgao = "";
	}
	public String getSigOrgao() { return this.sigorgao; }

	public void setTotalGer(String totalGer) {
		this.totalGer = totalGer;
	}
	public String getTotalGer() { return this.totalGer; }


	public void setTotalDef(String totalDef) {
		this.totalDef = totalDef;
	}
	public String getTotalDef() { return this.totalDef; }	
	
	public void setDeferidos(String deferidos) {
		this.deferidos = deferidos;
		if (deferidos == null) this.deferidos = "0";
	}  
	public String getDeferidos() { return this.deferidos; }
	
	public void setTodosOrgaos(String todosOrgaos) {
		this.todosOrgaos = todosOrgaos;
		if (todosOrgaos == null) this.todosOrgaos = "";
	}
	public String getTodosOrgaos() { return this.todosOrgaos; }

	public void setIndeferidos(String indeferidos) {
		this.indeferidos = indeferidos;
		if (indeferidos == null) this.indeferidos = "0";
	}
	public String getIndeferidos() { return this.indeferidos; }	
	
	public void setTpreq(String tpreq) {
		this.tpreq = tpreq;
		if (tpreq == null) this.tpreq = "";
	}
	public String getTpreq() { return this.tpreq; }	

	public void setTotalDefPerc(String totalDefPerc) {
		this.totalDefPerc = totalDefPerc;
		if( totalDefPerc == null )	this.totalDefPerc = "0";
	}
	public String getTotalDefPerc() { return this.totalDefPerc; }

	public void setTotalInDef(String totalInDef) {
		this.totalInDef = totalInDef;
		if( totalInDef == null )	this.totalInDef = "0";
	}
	public String getTotalInDef() { return this.totalInDef; }	
	
	public void setTotalInDefPerc(String totalInDefPerc) {
		this.totalInDefPerc = totalInDefPerc;
		if( totalInDefPerc == null )	this.totalInDefPerc = "0";
	}
	public String getTotalInDefPerc() { return this.totalInDefPerc; }	

	public void setDefPercent(String defPercent) {
		this.defPercent = defPercent;
		if( defPercent == null )	this.defPercent = "0";
	}
	public String getDefPercent() { return this.defPercent; }
	
	public void setIndefPercent(String indefPercent) {
		this.indefPercent = indefPercent;
		if( indefPercent == null )	this.indefPercent = "0";
	}
	public String getIndefPercent() { return this.indefPercent; }
	
	public void setTotalMediaDef(String totalMediaDef) {
		this.totalMediaDef = totalMediaDef;
		if( totalMediaDef == null )	this.totalMediaDef = "0";
	}
	public String getTotalMediaDef() { return this.totalMediaDef; }
	

	public void setTotMediaInDef(String totMediaInDef) {
		this.totMediaInDef = totMediaInDef;
		if( totMediaInDef == null )	this.totMediaInDef = "0";
	}
	public String getTotMediaInDef() { return this.totMediaInDef; }

	public void setTotalMedia(String totalMedia) {
		this.totalMedia = totalMedia;
		if( totalMedia == null )	this.totalMedia = "0";
	}
	public String getTotalMedia() { return this.totalMedia; }
	
	public void setTotal(String total) {
		this.total = total;
		if( total == null )	this.total = "0";
	}
	public String getTotal() { return this.total; }
	
//--------------------------------------------------------------------------	
	public boolean CarregaEstatisticas()   {
		boolean retorno = true;
		try  {
			Dao dao = Dao.getInstance();		   
			dao.CarregaEstatisticas(this);	        
		}
		catch (Exception e) {			
			retorno = false;
		}
		return retorno;
	}  
	
	public String getMesExtenso(String mes){
		String mesExtenso = "";
		String [] meses  = {"","JAN","FEV","MAR","ABR","MAI","JUN","JUL","AGO","SET","OUT","NOV","DEZ"};
		int i = Integer.parseInt(mes.substring(0,2));
		mesExtenso = meses[i];
		return mesExtenso;
	}

	public String formataInteiro (long valor) throws  DaoException, BeanException{	
		String valorFormatado ="";
		if (valor > 0){
			NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
			formato = new DecimalFormat(",#00") ;
		    valorFormatado = formato.format(valor); 
	    }
		else
			valorFormatado=String.valueOf(valor);
	    return valorFormatado;
	}

  
}