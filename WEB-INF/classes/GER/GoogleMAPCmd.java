package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ACSS.SistemaBean;

import java.util.Vector;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Sistema<br>
* <b>Description:</b>  Comando para Manter os Parametros por Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class GoogleMAPCmd extends sys.Command {

	private static final String jspPadrao = "/GER/GoogleMAP_Ini.jsp";
	private String next;

	public GoogleMAPCmd() {
		next = jspPadrao;
	}

	public GoogleMAPCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException{
		String nextRetorno = next;

		try {  
			HttpSession session   = req.getSession() ;								

			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
	
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;			
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
	
			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  	

			
			GoogleMAPBean GoogleMAPBeanId = (GoogleMAPBean) req.getAttribute("GoogleMAPBeanId");
			if (GoogleMAPBeanId == null) GoogleMAPBeanId = new GoogleMAPBean();
			

			
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";

			String codMunic = req.getParameter("codMunic");
			if (codMunic == null) codMunic = "0";
			
			String desMunic = req.getParameter("desMunic");
			if (desMunic == null) desMunic = "";

			String tipoJSP = req.getParameter("tipoJSP");
			if (tipoJSP == null) tipoJSP = "";
			
			
			GoogleMAPBeanId.setCodMunicipio(codMunic);
			GoogleMAPBeanId.setDesMunicipio(desMunic);

			GoogleMAPBeanId.getListMunicipios("RJ","");
			GoogleMAPBeanId.getListPardais();

			
			if (acao.compareTo("voltar") == 0) {
			  GoogleMAPBeanId.setCodMunicipio("0");
			  GoogleMAPBeanId.setDesMunicipio("");
		      nextRetorno = "/GER/GoogleMAP_Cad.jsp";
		    }
			
			
			if ("GER928".equals(UsuarioFuncBeanId.getJ_sigFuncao())) 
			    nextRetorno = "/GER/GoogleMAP_Ini.jsp";

			
			if ("GER929".equals(UsuarioFuncBeanId.getJ_sigFuncao())) 
				nextRetorno = "/GER/GoogleMAP_Cad.jsp";
			

			if (acao.compareTo("BuscaPardal") == 0) {
				if (tipoJSP.equals("GoogleMAP_Cad"))
				     nextRetorno = "/GER/GoogleMAP_Cad.jsp";
				else
				    nextRetorno = "/GER/GoogleMAP.jsp";
			}
			
			
		    if(acao.compareTo("A") == 0){
				
				String[] idPardal= req.getParameterValues("idPardal");
				if(idPardal == null) idPardal = new String[0];  

				String[] latitude = req.getParameterValues("latitude");
				if(latitude == null)  latitude = new String[0];         

				String[] longitude= req.getParameterValues("longitude");
				if(longitude == null) longitude = new String[0];  
				  
				String[] sentido= req.getParameterValues("sentido");
				if(sentido == null) sentido = new String[0];  

							 
					Vector fonte = new Vector(); 
					for (int i = 0; i < idPardal.length-1;i++) {
						EquipamentoEletrBean myBean = new EquipamentoEletrBean() ;
						
						myBean.setIdEquipamentoEletr(idPardal[i]);
						myBean.setLatitude(latitude[i]);
						myBean.setLongitude(longitude[i]);
						myBean.setSentido(sentido[i]);

						fonte.addElement(myBean) ; 	
					}
					GoogleMAPBeanId.setPardais(fonte) ;
					GoogleMAPBeanId.AtualizaPardais() ;
					GoogleMAPBeanId.getListPardais();
		    }

		    if (acao.compareTo("I") == 0) {
                 
		    	 String tituloConsulta = "CONSULTA PARDAIS POR MUNIC�PIO " ;
		    	 SistBeanId.setNomSistema("");
                 req.setAttribute("tituloConsulta", tituloConsulta);
				 nextRetorno = "/GER/GoogleMAP_CadImp.jsp";
			}
			
			
			 req.setAttribute("GoogleMAPBeanId", GoogleMAPBeanId);
			 req.setAttribute("SistId", SistBeanId);

		}
		catch (Exception ue) {
			throw new sys.CommandException(
				"ProcessoSistemaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
}