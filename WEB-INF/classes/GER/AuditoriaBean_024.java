package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE INFRA��ES,UNIDADES<br>
* <b>Description:</b>  Bean Auditoria_024 - Informa��es daS transa��es 081,082<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_024 extends AuditoriaBean {	
	
	private String codigo 				;
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String descricao	 		;	
	private String endereco 			;	
	private String numero 				;
	private String complemento 			;	
	private String bairro		 		;	
	private String cep			 		;
	private String municipio	 		;
	private String telefoneDDD 			;	
	private String numTelefone	 		;	
	private String numFax		 		;		
	private String sigla 				;
	private String contato 				;
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_024() throws sys.BeanException {		
	
		codigo 				= "";
		tipoAcao 			= "";
		indContinuidade     = "";
		descricao 			= "";
		endereco 			= "";
		numero	 			= "";
		complemento 		= "";
		bairro 				= "";
		cep 				= "";
		municipio 			= "";
		telefoneDDD 		= "";
		numTelefone 		= "";
		numFax 				= "";
		sigla 				= "";
		sigla 				= "";
		contato 			= "";
		codOrgao  			= "";	
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",39);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean (); 
	    
	     this.codigo 			= reg.substring(0,3);
	     this.tipoAcao 			= reg.substring(3,4);
	     this.indContinuidade 	= reg.substring(4,7);	     
	     this.codOrgao 			= reg.substring(7,13);
	     this.codOrgoaLotacao 	= reg.substring(13,19);
	     this.nomUserName 		= reg.substring(19,39);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	public String getCodigo() {
		return codigo;
	}	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDescricao() {
		return descricao;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getTelefoneDDD() {
		return telefoneDDD;
	}	
	public String getNumTelefone() {
		return numTelefone;
	}
	public String getNumFax() {
		return numFax;
	}
	public String getSigla() {
		return sigla;
	}
	public String getContato() {
		return contato;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	}