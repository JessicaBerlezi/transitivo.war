package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - ENVIO DE RECURSO/RESULTADO/ESTORNO 1 INST�NCIA (RECJARI)<br>
* <b>Description:</b>  Bean Auditoria_049 - Informa��es da transa��o 403<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_049 extends AuditoriaBean {
	
	private String nomUserName			;
	private String codOrgoaLotacao		;
	private String codOrgao 			;	
	private String responsavelOrgao		;
	private String tipoTransacao		;
	private String numProcesso   		;	
	private String numAutoInfracao  	;
	private String nomRequerente		;
	private String dataAbertura  		;
	private String numPlaca				;
	private String dataJulgamento  		;
	private String responsavelTransacao	;
	private String observacao   		;
	
	
	public AuditoriaBean_049() throws sys.BeanException {
		
		nomUserName     	 		 = "";
		codOrgoaLotacao      		 = "";
		codOrgao  			 		 = "";
		responsavelOrgao  			 = "";
		tipoTransacao  				 = "";
		numProcesso  				 = "";
		numAutoInfracao  			 = "";
		nomRequerente  				 = "";
		dataAbertura  				 = "";
		numPlaca      				 = "";
		dataJulgamento  			 = "";		
		responsavelTransacao 		 = "";		
		observacao			         = "";			
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",215);		 
	    
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 
		 this.nomUserName   		= reg.substring(0,20)   ;
	     this.codOrgoaLotacao 		= reg.substring(20,26)  ;
	     this.codOrgao 				= reg.substring(26,32)  ;	     
	     this.responsavelOrgao 		= reg.substring(32,40)  ;
	     this.tipoTransacao 		= reg.substring(40,42)  ;
	     this.numProcesso 			= reg.substring(42,62)  ;
	     this.numAutoInfracao 		= reg.substring(62,74)  ;     
	     this.nomRequerente 		= reg.substring(74,114) ;	    
	     this.dataAbertura 			= reg.substring(114,122);	 
	     this.numPlaca 				= reg.substring(122,129);	
	     this.dataJulgamento 		= reg.substring(129,137);	
	     this.responsavelTransacao 	= reg.substring(137,145)  ;	
	     this.observacao 			= reg.substring(145,215)  ;	
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}	
		public String getDatStatus() {
		String data = "";
		data = this.dataAbertura.substring(6,8)+"/"+this.dataAbertura.substring(4,6)+"/"+
		this.dataAbertura.substring(0,4);
		return data;
	}

		public String getCodOrgao() {
			return codOrgao;
		}

		public String getCodOrgoaLotacao() {
			return codOrgoaLotacao;
		}

		public String getDataAbertura() {
			String data = "";
			data = this.dataAbertura.substring(6,8)+"/"+this.dataAbertura.substring(4,6)+"/"+
			this.dataAbertura.substring(0,4);
			return data;
		}

		public String getDataJulgamento() {
			String data = "";
			data = this.dataJulgamento.substring(6,8)+"/"+this.dataJulgamento.substring(4,6)+"/"+
			this.dataJulgamento.substring(0,4);
			return data;
		}

		public String getNomRequerente() {
			return nomRequerente;
		}

		public String getNomUserName() {
			return nomUserName;
		}

		public String getNumAutoInfracao() {
			return numAutoInfracao;
		}

		public String getNumPlaca() {
			return numPlaca;
		}

		public String getNumProcesso() {
			return numProcesso;
		}

		public String getObservacao() {
			return observacao;
		}

		public String getResponsavelOrgao() {
			return responsavelOrgao;
		}

		public String getResponsavelTransacao() {
			return responsavelTransacao;
		}

		public String getTipoTransacao() {
			return tipoTransacao;
		}	
	
}