package GER;

import sys.BeanException;
import sys.Util;
import ACSS.OrgaoBean;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Usu�rios - Tabela de Hist�rico de Usu�rios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_001 extends AuditoriaBean {

	private String nomUserName;
	private String codOrgoaLotacao;
	private String transacao;
	private String codOrgao;
	private String numAutoInfracao;
	private String numPlaca;
	private String codMarcaModelo;
	private String localInfracao;
	
	private String motivoInvalid;
	private String processo;
	private String respInvalid;
	private String vago;
	
	private String datInfracao;
	private String horaInfracao;
	private String codMunInfracao;
	private String codInfracao;
	private String valMulta;
	private String datVencimento;
	private String codAgente;
	private String matAgente;
	private String velocidadePerm;
	private String velocidadeAfer;
	private String velocidadeCons;
	private String tipoDispositivo;
	private String identAparelho;
	private String numImetro;
	private String datUltAfer;
	private String numCertAfer;
	private String localAparelho;
	private String especieVeiculo;
	private String categVeiculo;
	private String tipoVeiculo;
	private String corVeiculo;
	private String nomCondutor;
	private String cpfCondutor;
	private String cnhCondutor;
	private String ufCNH;
	private String endCondutor;
	private String numEndCondutor;
	private String compEndCondutor;
	private String cepEndCondutor;
	private String muniEndCondutor;
	private String txtObs;
	private String identOrgao;
	
	
	

	public AuditoriaBean_001() throws sys.BeanException {
		
		nomUserName      = "";
		codOrgoaLotacao  = "";
		transacao        = "";
		codOrgao         = "";
		numAutoInfracao  = "";
		numPlaca         = "";
		codMarcaModelo   = "";
		localInfracao    = "";

		motivoInvalid    = "";
		processo         = "";
		respInvalid      = "";
		vago             = "";
		
		datInfracao      = "";
		horaInfracao     = "";
		codMunInfracao   = "";
		codInfracao      = "";
		valMulta         = "";
		datVencimento    = "";
		codAgente        = "";
		matAgente        = "";
		velocidadePerm   = "";
		velocidadeAfer   = "";
		velocidadeCons   = "";
		
		tipoDispositivo  = "";
		identAparelho    = "";
		numImetro        = "";
		datUltAfer       = "";
		numCertAfer      = "";
		localAparelho    = "";
		especieVeiculo   = "";
		categVeiculo     = "";
		tipoVeiculo      = "";
		corVeiculo       = "";
		nomCondutor      = "";
		cpfCondutor      = "";
		cnhCondutor      = "";
		ufCNH            = "";
		endCondutor      = "";
		numEndCondutor   = "";
		compEndCondutor  = "";
		cepEndCondutor   = "";
		muniEndCondutor  = "";
		txtObs           = "";
		identOrgao       = "";
		
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 reg = Util.rPad(reg," ",437);
	     
		 this.nomUserName = reg.substring(0,20);
	     this.codOrgoaLotacao = reg.substring(20,26);
	     this.transacao = reg.substring(26,27);
	     this.codOrgao = reg.substring(27,33);
	     this.numAutoInfracao = reg.substring(33,45);
	     this.numPlaca = reg.substring(45,52);
	     this.codMarcaModelo = reg.substring(45,59);
	     this.localInfracao = reg.substring(59,104);

	     this.motivoInvalid = reg.substring(59,62);
	     this.processo = reg.substring(62,82);
	     this.respInvalid = reg.substring(82,90);
	     this.vago = reg.substring(82,104);
	     
	     this.datInfracao = reg.substring(104,112);
	     this.horaInfracao = reg.substring(112,116);
	     this.codMunInfracao = reg.substring(116,120);
	     this.codInfracao = reg.substring(120,124);
	     this.valMulta = reg.substring(124,131);
	     this.datVencimento = reg.substring(131,139);
	     this.codAgente = reg.substring(139,141);
	     this.matAgente = reg.substring(141,151);
	     this.velocidadePerm = reg.substring(151,154);
	     this.velocidadeAfer = reg.substring(154,158);
	     this.velocidadeCons = reg.substring(158,162);
	     this.tipoDispositivo = reg.substring(162,163);
	     this.identAparelho = reg.substring(163,178);
	     this.numImetro = reg.substring(178,187);
	     this.datUltAfer = reg.substring(187,195);
	     this.numCertAfer = reg.substring(195,205);
	     this.localAparelho = reg.substring(205,245);
	     this.especieVeiculo = reg.substring(245,247);
	     this.categVeiculo = reg.substring(247,249);
	     this.tipoVeiculo = reg.substring(249,252);
	     this.corVeiculo = reg.substring(252,255);
	     this.nomCondutor = reg.substring(255,300);
	     this.cpfCondutor = reg.substring(300,311);
	     this.cnhCondutor = reg.substring(311,322);
	     this.ufCNH = reg.substring(322,324);
	     this.endCondutor = reg.substring(324,349);
	     this.numEndCondutor = reg.substring(349,354);
	     this.compEndCondutor = reg.substring(354,365);
	     this.cepEndCondutor = reg.substring(365,373);
	     this.muniEndCondutor = reg.substring(373,377);
	     this.txtObs = reg.substring(377,428);
	     this.identOrgao = reg.substring(428,437);
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgaoAtuacao(OrgaoLot.getSigOrgao());
	     }	     
	     
	}


	public String getNomUserName()  {
		return this.nomUserName;
	}

	public String getCodOrgoaLotacao()  {
		return this.codOrgoaLotacao;
	}

	public String getTransacao()  {
		return this.transacao;
	}

	public String getCodOrgao()  {
		return this.codOrgao;
	}
	
	
	public String getNumAutoInfracao()  {
		return this.numAutoInfracao;
	}

	public String getNumPlaca()  {
		return this.numPlaca;
	}
	
	public String getCodMarcaModelo()  {
		return this.codMarcaModelo;
	}

	public String getLocalInfracao()  {
		return this.localInfracao;
	}
	
	public String getMotivoInvalid()  {
		return this.motivoInvalid;
	}
	
	public String getProcesso()  {
		return this.processo;
	}

	public String getRespInvalid()  {
		return this.respInvalid;
	}

	public String getVago()  {
		return this.vago;
	}

	public String getDatInfracao()  {
		return this.datInfracao;
	}

	public String getHoraInfracao()  {
		return this.horaInfracao;
	}

	public String getCodMunInfracao()  {
		return this.codMunInfracao;
	}

	public String getCodInfracao()  {
		return this.codInfracao;
	}

	public String getValMulta()  {
		return this.valMulta;
	}

	public String getDatVencimento()  {
		return this.datVencimento;
	}

	public String getCodAgente()  {
		return this.codAgente;
	}

	public String getMatAgente()  {
		return this.matAgente;
	}

	public String getVelocidadePerm()  {
		return this.velocidadePerm;
	}

	public String getVelocidadeAfer()  {
		return this.velocidadeAfer;
	}

	public String getVelocidadeCons()  {
		return this.velocidadeCons;
	}

	public String getTipoDispositivo()  {
		return this.tipoDispositivo;
	}

	public String getIdentAparelho()  {
		return this.identAparelho;
	}

	public String getNumImetro()  {
		return this.numImetro;
	}

	public String getDatUltAfer()  {
		return this.datUltAfer;
	}

	public String getNumCertAfer()  {
		return this.numCertAfer;
	}

	public String getLocalAparelho()  {
		return this.localAparelho;
	}

	public String getespecieVeiculo()  {
		return this.especieVeiculo;
	}

	public String getCategVeiculo()  {
		return this.categVeiculo;
	}

	public String getTipoVeiculo()  {
		return this.tipoVeiculo;
	}

	public String getCorVeiculo()  {
		return this.corVeiculo;
	}

	public String getNomCondutor()  {
		return this.nomCondutor;
	}

	public String getCpfCondutor()  {
		return this.cpfCondutor;
	}

	public String getCnhCondutor()  {
		return this.cnhCondutor;
	}

	public String getUfCNH()  {
		return this.ufCNH;
	}

	public String getEndCondutor()  {
		return this.endCondutor;
	}

	public String getNumEndCondutor()  {
		return this.numEndCondutor;
	}
	
	public String getCompEndCondutor()  {
		return this.compEndCondutor;
	}

	public String getCepEndCondutor()  {
		return this.cepEndCondutor;
	}

	public String getMuniEndCondutor()  {
		return this.muniEndCondutor;
	}

	public String getTxtObs()  {
		return this.txtObs;
	}

	public String getIdentOrgao()  {
		return this.identOrgao;
	}
}