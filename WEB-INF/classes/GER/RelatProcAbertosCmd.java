package GER;

/**
* <b>Title:</b>        Gerencial - Relatorio de Processos Por Per�odo<br>
* <b>Description:</b>  Informa os autos deferidos/indeferidos por relator. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RelatProcAbertosCmd extends sys.Command {
	private static final String jspPadrao = "/GER/RelatProcAbertos.jsp";
	private String next;
	
	public RelatProcAbertosCmd() { 
		next = jspPadrao; 
	}
	public RelatProcAbertosCmd(String next) { 
		this.next = next; 
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException	{
		String nextRetorno = next;
		try {
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
			 RelatProcAbertosBean RelatProcAbertosBeanId = (RelatProcAbertosBean)req.getAttribute("RelatProcAbertosBeanId") ;
			 if (RelatProcAbertosBeanId == null)  RelatProcAbertosBeanId = new RelatProcAbertosBean() ;

         	 //Carrego os dados da Tela
			 String tipoJunta = req.getParameter("TPreq");
			 if (tipoJunta == null) tipoJunta = "";
			 
			 String procWEB = req.getParameter("procWEB");
			 if (procWEB == null) procWEB="N";
			 
			 String datInicio = req.getParameter("datInicio");
			 if (datInicio == null) datInicio = "";
			 
			 String datFim = req.getParameter("datFim");
			 if (datFim == null) datFim = "";
			 
			 String codOrgao = req.getParameter("codOrgao");
			 if (codOrgao == null) codOrgao = "";
			 
			 String acao = req.getParameter("acao");
			 if( acao == null ) acao = "";
            
             if ( acao.equals("") ) 
             {
                req.setAttribute("RelatProcAbertosBeanId", RelatProcAbertosBeanId);
                req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
                String sigFuncao = req.getParameter("j_sigFuncao");
                if(sigFuncao.equals("GER0184"))
                	nextRetorno = "/GER/RelatProcAbertosDP.jsp";
                else if(sigFuncao.equals("GER0185"))
                	nextRetorno = "/GER/RelatProcAbertos1A.jsp";
                else if(sigFuncao.equals("GER0186"))
                	nextRetorno = "/GER/RelatProcAbertos2A.jsp";
                else if(sigFuncao.equals("GER0187"))
                	nextRetorno = "/GER/RelatProcAbertos.jsp";
             }
             else if(acao.equals("ImprimeRelatorio")){    
	            	RelatProcAbertosBeanId.setDataIni(datInicio);
	            	RelatProcAbertosBeanId.setDataFim(datFim);
	            	RelatProcAbertosBeanId.setCodOrgao(codOrgao);
	            	RelatProcAbertosBeanId.setProcWEB(procWEB);
	    			
	    			String[] tipoReq  = req.getParameterValues("TPreq");
	    			if(tipoReq == null) tipoReq = new String[0];

	    			String tipoRequerimento = "";
	    			String LabelRec = "";
	    			for (int i = 0; i < tipoReq.length; i++)
	    			{
		    			if ("0".equals(tipoReq[i])){     
		    				tipoRequerimento +=  "'DP','DC','DI',";
		    			 	LabelRec += "DEFESA PR�VIA, ";
						}else if("1".equals(tipoReq[i])){ 
							tipoRequerimento +=  "'1P','1C','1I',";
						 	LabelRec += "1a. INST�NCIA, ";
						}else if("2".equals(tipoReq[i])){
							tipoRequerimento +=  "'2P','2C','2I',";
						 	LabelRec += "2a. INST�NCIA, ";
						}else if("3".equals(tipoReq[i])){
							tipoRequerimento += "'TR',";
				 			LabelRec += "TRI, ";
						}else if("4".equals(tipoReq[i])){
							tipoRequerimento += "'RD',";
							LabelRec += "REN�NCIA DE DEFESA PR�VIA, ";
						}
	    			}
	    			RelatProcAbertosBeanId.setTpreq(tipoRequerimento.substring(0,tipoRequerimento.length()-1));
	    			
			        //Carrega os relatores 
	    			RelatProcAbertosBeanId.getProcessosAbertos(RelatProcAbertosBeanId);
	    			String espaco = "<br>";
	    			String tituloConsulta = "RELAT�RIO DE PROCESSOS ABERTOS "+ espaco+ LabelRec.substring(0,LabelRec.length()-2) + espaco
					    + RelatProcAbertosBeanId.getDataIni()+ " A "+RelatProcAbertosBeanId.getDataFim();
	    			String msg = "";
	    			if(RelatProcAbertosBeanId.getDados().size()==0)
	    				msg ="N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE PER�ODO.";
	    			req.setAttribute("msg", msg);
	    			req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/GER/RelatProcAbertosImp.jsp";
			  }
             
  			  req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
			  req.setAttribute("RelatProcAbertosBeanId", RelatProcAbertosBeanId);

		} catch (Exception e) {	
			throw new sys.CommandException("EstatisticaArtigoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}