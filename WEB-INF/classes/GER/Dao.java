package GER;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;
import REC.ParamUFBean;
import REG.RelatArqNotificacaoBean;

/**
 * <b>Title:</b>        Gerencial - Acesso a Base de Dados<br>
 * <b>Description:</b>  Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b>     MIND Informatica <br>
 * @author jheitor
 * @version 1.0
 */

public class Dao {
	
	private static final String MYABREVSIST = "GER";
	private static Dao instance;
	private sys.ServiceLocator serviceloc ;  
	
	public static Dao getInstance()
	throws DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}
	//**************************************************************************************
	private Dao() throws DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	
	public void criaGrafico(GraficoBean myGrf) throws DaoException {
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}	
			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			// CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  = "SELECT   I.COD_MUNICIPIO,J.NOM_MUNICIPIO,SUM(I.QTD)as iqtd "+
			" FROM    TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE   I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND     I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND     I.COD_MUNICIPIO = J.COD_MUNICIPIO " ;
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			
			rs  = stmt.executeQuery(sCmd);
			double totInf = 0;
			double maiorValor = 0;
			double i = 0 ;
			int tamMun = 0;
			Double lNumInf[] = new Double[93];
			String lNomeMun[] = new String[93];
			String s;
			ParamUFBean paramUFBeanId = new ParamUFBean();
			paramUFBeanId.PreparaParam(myGrf.getCodEstado());
			String estado = paramUFBeanId.getParamUF("TXT_NOME").toLowerCase();
			
			while (rs.next()) {				
				s = rs.getString("nom_municipio");
				s = s.toLowerCase();
				lNomeMun[tamMun] = s.trim();
				i = Long.parseLong(rs.getString("iqtd")) ;
				totInf += i ;
				if ((i>maiorValor) && (estado.equals(s.trim())==false)) 
					maiorValor=i;
				
				lNumInf[tamMun] = new Double(i);	
				tamMun ++;				
			}	
			
			lNomeMun[tamMun] = "outros";
			lNumInf[tamMun] = new Double(0);
			double outrosValor;
			int posOutros = tamMun;
			
			// retirar os n�o significativos - < maiorValor/10
			double k;
			Double lNumInf2[] = new Double[93];
			String lNomeMun2[] = new String[93];
			int tamMun2 = 0;
			
			for(int l = 0;l <= 92;l++){
				lNumInf2[l] = new Double(0);
				lNomeMun2[l] = " ";
			}
			
			for(int l = 0;l <= tamMun;l++) {
				
				k = lNumInf[l].longValue();
				if ((k < maiorValor/10) && (l != posOutros)) {
					outrosValor = lNumInf[posOutros].doubleValue() + k;	
					lNumInf[posOutros] = new Double(outrosValor);
				}
				else {
					lNumInf2[tamMun2] = new Double(k);
					lNomeMun2[tamMun2] = lNomeMun[l];	
					tamMun2 ++;				
				}
				
			}
			
			// calcular percentual
			for(int l = 0;l <= tamMun2;l++) {
				double ni = lNumInf2[l].longValue();
				if (totInf !=0 ) 
					ni = (ni*100/totInf);
				lNumInf2[l] = new Double(ni);
				
			}	
			myGrf.setInfMunicipio(lNumInf2);
			myGrf.setNomeMunicipio(lNomeMun2);
			
			conn.close();		
		} 
		catch (SQLException e) {
			
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			
			throw new DaoException(ex.getMessage());
		}		
	}
	
	public void infPontuacao(GraficoBean myGrf) throws DaoException {	
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			ResultSet rs  = stmt.executeQuery(sCmd);	
			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}			
			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			// CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  = "SELECT   I.NUM_PONTOS,SUM(I.QTD)as iqtd "+
			" FROM     TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE     I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " ;
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.NUM_PONTOS  "+
			" ORDER BY I.NUM_PONTOS " ;	
			
			rs  = stmt.executeQuery(sCmd);
			
			int i = 0 ;
			Double lNumInf[]  = new Double[4];
			String lNomeMun[] = new String[4];
			String s;
			String nomPts[] = {"LEVE","MEDIA","GRAVE","GRAV�SSIMA"};
			
			for(int j = 0;j < 4;j++)
				lNumInf[j] = new Double(0);
			
			while (rs.next()) {				
				s = rs.getString("NUM_PONTOS");			  
				i = Integer.parseInt(s);
				
				if (i >= 7){
					lNumInf[3] = new Double(rs.getString("iqtd"));	
				}			 
				else { 
					if (i >= 5){ 
						lNumInf[2] = new Double(rs.getString("iqtd"));	
					}			 
					else { 
						if (i >= 4)	{
							lNumInf[1] = new Double(rs.getString("iqtd"));
						} 
						else{
							lNumInf[0] = new Double(rs.getString("iqtd"));	
						}				 
					}	
				}				
			}
			
			for(int m = 0;m < 4;m++)
				lNomeMun[m] = nomPts[m];
			
			myGrf.setInfMunicipio(lNumInf);
			myGrf.setNomeMunicipio(lNomeMun);	
			
			conn.close();
			
		} 
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}			
	}	
	
	public void criaGraficoTipoInfracao(GraficoBean myGrf) throws DaoException {		
		
		Connection conn = null;	
		try{			
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			ResultSet rs  = stmt.executeQuery(sCmd);		
			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}	
			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			// CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  = "SELECT   I.COD_INFRACAO,F.DSC_INFRACAO,SUM(I.QTD)as iqtd "+
			" FROM    TGER_INFR_DIARIO I,TSMI_MUNICIPIO J,TSMI_INFRACAO F "+						
			" WHERE   I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND     I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND     I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND     I.COD_INFRACAO = F.COD_INFRACAO ";
			if ("N".equals(myGrf.getTodosOrgaos())) 
			{
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_INFRACAO,F.DSC_INFRACAO "+
			" ORDER BY I.COD_INFRACAO " ;		
			rs  = stmt.executeQuery(sCmd);
			double totInf = 0;
			double maiorValor = 0;
			double i = 0 ;
			int tamMun = 0;
			Double lNumInf[] = new Double[215];
			String lNomeMun[] = new String[215];
			String dsc;
			
			while (rs.next()) {				
				dsc = rs.getString("dsc_infracao");
				lNomeMun[tamMun] = dsc.trim();
				i = Long.parseLong(rs.getString("iqtd")) ;
				totInf += i ;
				if (i > maiorValor) 
					maiorValor = i;
				
				lNumInf[tamMun] = new Double(i);	
				tamMun ++;				
			}	
			
			lNomeMun[tamMun] = "OUTROS";
			lNumInf[tamMun] = new Double(0);
			double outrosValor;
			int posOutros = tamMun;
			
			// retirar os n�o significativos - < maiorValor/10
			double k;
			Double lNumInf2[] = new Double[93];
			String lNomeMun2[] = new String[93];
			int tamMun2 = 0;
			
			for(int l = 0;l <= 92;l++){
				lNumInf2[l] = new Double(0);
				lNomeMun2[l] = " ";
			}
			for(int l = 0;l <= tamMun;l++) {
				
				k = lNumInf[l].longValue();
				if ((k < maiorValor/10) && (l != posOutros)) {
					outrosValor = lNumInf[posOutros].doubleValue() + k;	
					lNumInf[posOutros] = new Double(outrosValor);
				}
				else {
					lNumInf2[tamMun2] = new Double(k);
					lNomeMun2[tamMun2] = lNomeMun[l];	
					tamMun2 ++;				
				}
				
			}
			
			//calcular percentual
			String st = new String();
			for(int l = 0;l <= tamMun2;l++) {
				double ni = lNumInf2[l].doubleValue();
				if (totInf !=0 ) 
					ni = (ni*100/totInf);
				lNumInf2[l] = new Double(ni);	
				st = lNomeMun[l];
				st = st.toLowerCase();
				if(st.length() > 35)
					st = st.substring(0,35);
				
				lNomeMun2[l] = st;		
			}	
			
			myGrf.setInfMunicipio(lNumInf2);
			myGrf.setNomeMunicipio(lNomeMun2);
			conn.close();				
		} 
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}		
		
	}	
	
	public void criaRegDiario(GraficoBean myGrf) throws DaoException{
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}
			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			// CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  = "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_REG_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_REGISTRADO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_REGISTRADO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " ;
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			
			rs  = stmt.executeQuery(sCmd);
			double totInf = 0;
			double maiorValor = 0;
			double i = 0 ;
			int tamMun = 0;
			Double lNumInf[] = new Double[93];
			String lNomeMun[] = new String[93];
			String s;
			ParamUFBean paramUFBeanId = new ParamUFBean();
			paramUFBeanId.PreparaParam(myGrf.getCodEstado());
			String estado = paramUFBeanId.getParamUF("TXT_NOME").toLowerCase();
			
			while (rs.next()) {				
				s = rs.getString("nom_municipio");
				s = s.toLowerCase();
				lNomeMun[tamMun] = s.trim();
				i = Long.parseLong(rs.getString("iqtd")) ;
				totInf += i ;
				if ((i > maiorValor) && (estado.equals(s.trim())==false)) 
					maiorValor=i;
				
				lNumInf[tamMun] = new Double(i);	
				tamMun ++;				
			}	
			
			lNomeMun[tamMun] = "outros";
			lNumInf[tamMun] = new Double(0);
			double outrosValor;
			int posOutros = tamMun;
			
			// retirar os n�o significativos - < maiorValor/10
			double k;
			Double lNumInf2[] = new Double[93];
			String lNomeMun2[] = new String[93];
			int tamMun2 = 0;
			
			for(int l = 0;l <= 92;l++){
				lNumInf2[l] = new Double(0);
				lNomeMun2[l] = " ";
			}
			
			for(int l = 0;l <= tamMun;l++) {
				
				k = lNumInf[l].longValue();
				if ((k < maiorValor/10) && (l != posOutros)) {
					outrosValor = lNumInf[posOutros].doubleValue() + k;	
					lNumInf[posOutros] = new Double(outrosValor);
				}
				else {
					lNumInf2[tamMun2] = new Double(k);
					lNomeMun2[tamMun2] = lNomeMun[l];	
					tamMun2 ++;				
				}
			}
			
			// calcular percentual
			for(int l = 0;l <= tamMun2;l++) {
				double ni = lNumInf2[l].doubleValue();
				if (totInf !=0 ) 
					ni = (ni*100/totInf);
				lNumInf2[l] = new Double(ni);
				
			}	
			myGrf.setInfMunicipio(lNumInf2);
			myGrf.setNomeMunicipio(lNomeMun2);
			
			conn.close();		
		} 
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}	
		
	}
	
	
	public void demonstSaldosAuto(GraficoBean myGrf) throws DaoException{
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			//CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  =  "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND      COD_STATUS IN (000,001,002,004,005) ";
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			rs  = stmt.executeQuery(sCmd);	
			
			String 	meses[] = {"Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"};
			Double Status[][] = new Double [5][12];
			
			for(int i = 0;i < 5;i++)
				for(int j = 0;j < 12;j++)
					Status[i][j] = new Double(0);	
			
			while (rs.next()) {	
				
				int status = Integer.parseInt(rs.getString("COD_STATUS"));
				switch(status){
				
				case 0: atualizaQtd(Status,rs,0);
				break;
				case 1:	atualizaQtd(Status,rs,0);
				break;
				case 2:	atualizaQtd(Status,rs,0);
				break;
				case 4:	atualizaQtd(Status,rs,0);
				break;
				case 5:	atualizaQtd(Status,rs,1);
				break;			
				
				}										
			}	
			
			myGrf.setNomeMunicipio(meses);					
			myGrf.setStatus(Status);			
			conn.close();
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}
		
	}
	
	public void demonstSaldosPD(GraficoBean myGrf) throws DaoException{
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {		
				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));	
				
			}			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			//CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  =  "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND      COD_STATUS IN (010,011,012,014,015,016,030,035) ";
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			rs  = stmt.executeQuery(sCmd);	
			
			String 	meses[] = {"Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"};
			Double Status[][] = new Double [5][12];
			
			for(int i = 0;i < 5;i++)
				for(int j = 0;j < 12;j++)
					Status[i][j] = new Double(0);
			
			while (rs.next()) {	
				
				int status = Integer.parseInt(rs.getString("COD_STATUS"));
				switch(status){
				
				case 10:  	atualizaQtd(Status,rs,0);
				break;
				case 11:	atualizaQtd(Status,rs,0);
				break;
				case 12:	atualizaQtd(Status,rs,0);
				break;
				case 14:	atualizaQtd(Status,rs,0);
				break;
				case 15:	atualizaQtd(Status,rs,1);
				break;
				case 16:	atualizaQtd(Status,rs,1);
				break;
				case 30:	atualizaQtd(Status,rs,2);
				break;
				case 35:	atualizaQtd(Status,rs,3);
				break;						
				
				}										
			}	
			
			myGrf.setNomeMunicipio(meses);					
			myGrf.setStatus(Status);
			conn.close();
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}
	}
	
	public void demonstSaldosTJ(GraficoBean myGrf)throws DaoException{
		
		Connection conn = null;
		try{			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {	
				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));	
				
			}
			
			// PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			//CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  =  "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND      COD_STATUS IN (090,091,095,096,097,098,099) ";
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			rs  = stmt.executeQuery(sCmd);	
			
			String 	meses[] = {"Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"};
			Double Status[][] = new Double [5][12];
			
			for(int i = 0;i < 5;i++)
				for(int j = 0;j < 12;j++)
					Status[i][j] = new Double(0);
			
			while (rs.next()) {	
				
				int status = Integer.parseInt(rs.getString("COD_STATUS"));
				switch(status){
				
				case 90:  	atualizaQtd(Status,rs,0);
				break;
				case 91:	atualizaQtd(Status,rs,1);
				break;
				case 95:	atualizaQtd(Status,rs,2);
				break;
				case 96:	atualizaQtd(Status,rs,3);
				break;
				case 97:	atualizaQtd(Status,rs,3);
				break;
				case 98:	atualizaQtd(Status,rs,3);
				break;
				case 99:	atualizaQtd(Status,rs,4);
				break;	
				}										
			}	
			
			myGrf.setNomeMunicipio(meses);					
			myGrf.setStatus(Status);
			conn.close();
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}
		
	}
	
	public void atualizaQtd(Double[][] Status,ResultSet rs,int status)throws DaoException{
		
		double i = 0 ;
		String st;
		try{
			
			i = Double.parseDouble(rs.getString("iqtd"));
			st = rs.getString("dat_infracao");
			st = st.substring(5,7); // pega apenas o mes da data
			int mes = Integer.parseInt(st);
			double qtd = Status[status][mes - 1].doubleValue();
			qtd = qtd + i;
			Status[status][mes - 1] = new Double(qtd);
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		
	}
	
	public void demonstSaldosTotal(GraficoBean myGrf)throws DaoException{
		
		Connection conn = null;
		try{
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}			
			
			//PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			//CALCULAR A QTD POR MUNICIPIO A SER MOSTRADA								 
			sCmd  =  "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_INFR_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_INFRACAO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_INFRACAO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND      COD_STATUS IN (090,091,095,096,097,098,099,010,011,012,014,015,016,030,035,001,002,004,005) ";
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO,COD_STATUS,DAT_INFRACAO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			rs  = stmt.executeQuery(sCmd);	
			
			String 	status[] = {"Autua��o","Penalidade","Tr�nsito Julgado"};
			
			Double lNumInf[]  = new Double[3];
			String lNomeMun[] = new String[3];
			
			//lista de nomes recebe os nomes de cada tipo
			for(int h = 0;h < 3;h++){
				lNomeMun[h] = status[h];
				lNumInf[h] = new Double(0);
			}
			
			while (rs.next()) {	
				
				int stat = Integer.parseInt(rs.getString("COD_STATUS"));
				switch(stat){
				
				case 90:  	atualizaQtd2(lNumInf,rs,2);
				break;
				case 91:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 95:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 96:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 97:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 98:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 99:	atualizaQtd2(lNumInf,rs,2);
				break;
				case 10:  	atualizaQtd2(lNumInf,rs,1);
				break;
				case 11:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 12:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 14:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 15:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 16:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 30:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 35:	atualizaQtd2(lNumInf,rs,1);
				break;
				case 0: 	atualizaQtd2(lNumInf,rs,0);
				break;
				case 1:		atualizaQtd2(lNumInf,rs,0);
				break;
				case 2:		atualizaQtd2(lNumInf,rs,0);
				break;
				case 4:		atualizaQtd2(lNumInf,rs,0);
				break;
				case 5:		atualizaQtd2(lNumInf,rs,0);
				break;														
				
				}										
			}	
			
			double total = 0;
			for(int i = 0; i < 3;i++)
				total += lNumInf[i].doubleValue();
			
			double qtd = 0;
			for(int i = 0;i < 3;i++){
				qtd = lNumInf[i].doubleValue();
				if(total != 0) qtd = (qtd*100/total);
				lNumInf[i] = new Double(qtd);
			}
			
			myGrf.setInfMunicipio(lNumInf);
			myGrf.setNomeMunicipio(lNomeMun);
			conn.close();
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}
		
	}
	
	public void atualizaQtd2(Double[] status,ResultSet rs,int indice)throws DaoException{
		
		double i = 0 ;
		try{
			
			i = Double.parseDouble(rs.getString("iqtd"));
			double qtd = status[indice].doubleValue();
			qtd += i;
			status[indice] = new Double(qtd);
			
		}	
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	public void cancelAjuste(GraficoBean myGrf) throws DaoException {
		
		Connection conn = null;
		try{
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			// Setar Estado e Regiao
			String sCmd  = "SELECT M.NOM_MUNICIPIO,M.COD_REGIAO,R.TXT_REGIAO,M.COD_UF "+
			" FROM     TSMI_MUNICIPIO M, TSMI_REGIAO R"+
			" WHERE    M.COD_REGIAO = R.COD_REGIAO AND M.COD_UF=R.COD_UF AND M.COD_MUNICIPIO = '" + myGrf.getCodMunicipio() +"' ";
			
			ResultSet rs  = stmt.executeQuery(sCmd);			
			while (rs.next()) {				
				myGrf.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				myGrf.setSiglaMunicipio(myGrf.getNomMunicipio());					
				myGrf.setCodRegiao(rs.getString("COD_REGIAO"));
				myGrf.setNomRegiao(rs.getString("TXT_REGIAO"));					
				myGrf.setCodEstado(rs.getString("COD_UF"));				
			}	
			
			//PEGAR DADOS DO ORGAO
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}
			else {
				myGrf.setSiglaOrgao("TODOS");
			}
			
			// consulta para cancelamento e ajute status
			sCmd  =  "SELECT    I.COD_MUNICIPIO,J.NOM_MUNICIPIO,I.COD_EVENTO,DAT_EVENTO,SUM(I.QTD)as iqtd "+
			" FROM     TGER_CANC_AJUSTE_DIARIO I,TSMI_MUNICIPIO J "+						
			" WHERE    I.DAT_EVENTO >= to_date('"+ myGrf.getDatInicio() +"', 'dd/mm/yyyy')"+
			" AND      I.DAT_EVENTO <= to_date('"+ myGrf.getDatFim() + "', 'dd/mm/yyyy') "+
			" AND      I.COD_MUNICIPIO = J.COD_MUNICIPIO " +
			" AND      COD_EVENTO IN (405,411,412,413,414) ";
			
			if ("N".equals(myGrf.getTodosOrgaos())) {
				sCmd +=	" AND  I.COD_ORGAO = '" + myGrf.getCodOrgao() +"' ";				
			}
			if ("E".equals(myGrf.getMyDominio())) {
				sCmd +=	" AND J.COD_UF = '"+myGrf.getCodEstado()+"'";
			} else {
				if ("R".equals(myGrf.getMyDominio())) {
					sCmd +=	" AND J.COD_REGIAO = '"+myGrf.getCodRegiao()+"'";
				} else {
					sCmd +=	" AND I.COD_MUNICIPIO = '"+myGrf.getCodMunicipio()+"'";
				}
			}
			sCmd +=	" GROUP BY I.COD_MUNICIPIO,J.NOM_MUNICIPIO,I.COD_EVENTO,I.DAT_EVENTO  "+
			" ORDER BY J.NOM_MUNICIPIO " ;
			
			rs  = stmt.executeQuery(sCmd);	
			
			String 	meses[] = {"Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"};
			Double Status[][] = new Double[2][12];
			
			for(int i = 0;i < 2;i++)
				for(int j = 0;j < 12;j++)
					Status[i][j] = new Double(0);
			
			String st,ss;
			while(rs.next())
			{
				ss = rs.getString("cod_evento");
				st = rs.getString("dat_evento");				
				if (ss.equals("405"))
					atualizaQtd3(Status,rs,st,0);
				if (ss.equals("411") || ss.equals("412") || ss.equals("413") || ss.equals("414"))
					atualizaQtd3(Status,rs,st,1);
			}		
			
			myGrf.setNomeMunicipio(meses);					
			myGrf.setStatus(Status);
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}	
	}
	
	public void atualizaQtd3(Double[][] list,ResultSet rs,String st,int status) throws DaoException{
		
		double i = 0;
		try{				
			i = Double.parseDouble(rs.getString("iqtd"));
			st = st.substring(5,7); // pega apenas o mes da data
			int mes = Integer.parseInt(st);
			double qtd = list[status][mes - 1].doubleValue();
			qtd = qtd + i;
			list[status][mes - 1] = new Double(qtd);
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}	
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqNotificacaoBean
	 *-----------------------------------------------------------
	 */
	
	public boolean ConsultaRelDiario(GER.RelatArqNotificacaoBean ran,String datIni,String datFim) throws DaoException
	{
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
            ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
            ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
            ParamSistemaBeanId.PreparaParam();
            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
            if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			
			String sCmd = "SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
				+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
				+ " to_char(TAR.Dat_Recebimento,'dd/mm/yyyy') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
				+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
				+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
				+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
				+ " WHERE TAR.Cod_Ident_Arquivo = 'EMITEVEX' "
				+ " AND TAR.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('" + datFim +"','dd/mm/yyyy')"
				+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
				+ " AND TAR.Cod_Status <> 9 ";
			if (sHistorico.equals("S")) {
				sCmd += " UNION "
				+ " ("
				+ " SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
				+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
				+ " to_char(TAR.Dat_Recebimento,'dd/mm/yyyy') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
				+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
				+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
				+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
				+ " WHERE TAR.Cod_Ident_Arquivo = 'EMITEVEX' "
				+ " AND TAR.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('" + datFim +"','dd/mm/yyyy')"
				+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
				+ " AND TAR.Cod_Status <> 9 " 
				+ " )";
			}
			sCmd += "ORDER BY 1";		
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			Vector listaArq = new Vector();
			
			long totQtdReg =0;
			long totLinhPend =0;
			long totLinhCanc =0;
			long totQtdRec =0;			
			String qtdPendente, qtdRecebido, qtdCancelada;
			while( rs.next() ) {
				RelatArqNotificacaoBean myBean = new RelatArqNotificacaoBean();
				myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));
				myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
				myBean.setCodStatus(         rs.getString("Cod_status"));
				
				myBean.setQtdReg(            rs.getString("Qtd_Reg"));
				totQtdReg   += Long.parseLong(rs.getString("Qtd_Reg")); 
				
				if( rs.getString("Qtd_Linha_Pendente") == null )
					qtdPendente = "0";
				else
					qtdPendente = rs.getString("Qtd_Linha_Pendente");				
				myBean.setQtdLinhaPendente( qtdPendente );
				totLinhPend += Long.parseLong( qtdPendente );
				
				if( rs.getString("Qtd_Linha_Cancelada") == null )
					qtdCancelada = "0";
				else
					qtdCancelada = rs.getString("Qtd_Linha_Cancelada");				
				myBean.setQtdLinhaCancelada( qtdCancelada );
				totLinhCanc += Long.parseLong( qtdCancelada );
				
				if( rs.getString("Qtd_Recebido") == null )
					qtdRecebido = "0";
				else
					qtdRecebido = rs.getString("Qtd_Recebido");				
				myBean.setQtdRecebido( qtdRecebido );
				totQtdRec += Long.parseLong( qtdRecebido );
				
				myBean.setNumProtocolo(      rs.getString("Num_Protocolo"));
				myBean.setDatRecebimento(    rs.getString("Dat_Recebimento"));
				myBean.setTarNomUsername(    rs.getString("tarNomUserName"));
				myBean.setTarCodOrgaoLotacao(rs.getString("tarCodOrgaoLotacao"));
				myBean.setDatDownload(       rs.getString("Dat_Download"));
				myBean.setNomUsername(       rs.getString("tdNomUserName"));
				myBean.setCodOrgaoLotacao(   rs.getString("tdCodOrgaoLotacao"));
				myBean.setSitControleVEX(rs.getString("SIT_VEX"));
				listaArq.addElement( myBean );
				retorno = true;
			}
			
			sCmd ="SELECT DISTINCT a.NOM_ARQUIVO,DECODE(a.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"+
			"a.QTD_LINHA,a.QTD_LINHA_ERRO,a.QTD_LINHA_CANCELADA,a.NUM_PROTOCOLO, " +
			"(a.QTD_LINHA - a.QTD_LINHA_ERRO - a.QTD_LINHA_CANCELADA) AS Qtd_Recebido,"+
			" to_char(a.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO,"+
			" a.NOM_USERNAME,c.SIG_ORGAO,b.TOT_NOTIFICACOES,a.COD_ARQUIVO,a.SIT_VEX "+
			"  FROM TPNT_ARQUIVO a  , TSMI_ORGAO c, "+
			" (select count(*) as tot_notificacoes,cod_arquivo from tpnt_notificacao group by cod_arquivo) b "+
			" WHERE a.Cod_Ident_Arquivo = 'EMITEPONTO' AND a.COD_STATUS<> 9 " +
			" AND a.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') "+
			" AND a.Dat_Recebimento <=to_date('"+ datFim +"','dd/mm/yyyy') "+
			" AND a.cod_arquivo=b.cod_arquivo "+
			" AND a.Cod_orgao_lotacao=c.Cod_orgao "+
			" ORDER BY a.Nom_Arquivo";
			rs = stmt.executeQuery(sCmd);
			while( rs.next() ) {
				RelatArqNotificacaoBean myBean = new RelatArqNotificacaoBean();
				myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));
				myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
				myBean.setCodStatus(         rs.getString("Cod_status"));
				
				if( rs.getString("TOT_NOTIFICACOES") == null )
					qtdRecebido = "0";
				else	qtdRecebido = rs.getString("TOT_NOTIFICACOES");				
				myBean.setQtdReg(qtdRecebido);
				
				myBean.setQtdLinhaPendente( "0" );				
				
				totQtdReg   += Long.parseLong(qtdRecebido); 		
				myBean.setQtdRecebido( qtdRecebido );
				totQtdRec += Long.parseLong( qtdRecebido );
				
				myBean.setNumProtocolo(      rs.getString("NUM_PROTOCOLO"));
				myBean.setDatRecebimento(    rs.getString("DAT_RECEBIMENTO"));				
				myBean.setTarNomUsername(    rs.getString("NOM_USERNAME"));
				myBean.setTarCodOrgaoLotacao(rs.getString("SIG_ORGAO"));
				myBean.setSitControleVEX(rs.getString("SIT_VEX"));				
				listaArq.addElement( myBean );
				retorno = true;
			}			
			ran.setTotalQtdReg( String.valueOf(totQtdReg));
			ran.setTotalQtdLinhaPendente( String.valueOf(totLinhPend));
			ran.setTotalQtdLinhaCancelada( String.valueOf(totLinhCanc));
			ran.setTotalQtdRecebido( String.valueOf(totQtdRec));
			
			ran.setListaArqs( listaArq );
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return retorno;
	}
	
	public boolean ConsultaRelDiarioRet(RelatArqRetornoBean ran,String datIni,String datFim) throws DaoException
	{
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();			
			
			String sCmd = "SELECT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'Em P', 8,'E') Cod_status,"
				+ " TAR.Qtd_Reg,TAR.Num_Protocolo, (SELECT Count(*) FROM TSMI_LINHA_ARQUIVO_REC TLN WHERE TLN.Cod_Status=1 "
				+ " AND TLN.Cod_Retorno='000' AND TLN.Cod_Arquivo = TAR.Cod_Arquivo) AS Qtd_Recebido,"
				+ " to_char(TAR.Dat_Recebimento,'dd/mm/yyyy') Dat_Recebimento,TAR.Nom_Username as nomUserName,TTA.SIG_ORGAO AS codOrgaoLotacao"
				+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
				+ " WHERE TAR.Cod_Ident_Arquivo = 'CODRET' "
				+ " AND TAR.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('" + datFim +"','dd/mm/yyyy')"
				+ " AND TAR.Cod_Status <> 9 ORDER BY TAR.Nom_Arquivo";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			Vector listaArq = new Vector();
			
			long totQtdReg =0;
			long totQtdRec =0;
			String zero="";
			while( rs.next() ) {
				RelatArqRetornoBean myBean = new RelatArqRetornoBean();
				myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
				myBean.setCodStatus(         rs.getString("Cod_status"));
				
				myBean.setQtdReg(            rs.getString("Qtd_Reg"));
				totQtdReg   += Long.parseLong(rs.getString("Qtd_Reg")); 
				
				if( rs.getString("Qtd_Recebido") == null )
					zero = "0";
				else
					zero = rs.getString("Qtd_Recebido");
				
				myBean.setQtdRecebido( zero );
				totQtdRec += Long.parseLong( zero );
				
				myBean.setNumProtocolo(      rs.getString("Num_Protocolo"));
				myBean.setDatRecebimento(    rs.getString("Dat_Recebimento"));
				myBean.setNomUsername(       rs.getString("nomUserName"));
				myBean.setCodOrgaoLotacao(   rs.getString("codOrgaoLotacao"));
				listaArq.addElement( myBean );
				retorno = true;
			}
			ran.setTotalQtdReg( String.valueOf(totQtdReg));
			ran.setTotalQtdRecebido( String.valueOf(totQtdRec));
			
			ran.setListaArqs( listaArq );
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a DemonstrativoSaldoBean
	 *-----------------------------------------------------------
	 */	
	
	public void geraDemonstrativoAutuacao(DemonstrativoSaldoBean bean, Integer codOrgao, Date datInicial, Date datFinal) 
	throws DaoException
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST); 
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			String cmdSaldo = "SELECT QTD_SALDO FROM TSMI_SALDO_STATUS WHERE DAT_SALDO = ? AND COD_ORGAO = ? AND COD_STATUS = ?";
			PreparedStatement stmSaldo = conn.prepareStatement(cmdSaldo);			
			stmSaldo.setInt(2, codOrgao.intValue());
			
			//Carrega o saldo anterior
			stmSaldo.setDate(1, new java.sql.Date(datInicial.getTime()));
			Map saldoAnterior = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_AUTUACAO[i]);
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();				
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo anterior no dia " + df.format(datInicial) + " para o status " + bean.STATUS_AUTUACAO[i] + " !");
				saldoAnterior.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo));
			}
			bean.setSaldoAnterior(saldoAnterior);
			
			//Carrega o saldo atual
			stmSaldo.setDate(1, new java.sql.Date(datFinal.getTime()));
			Map saldoAtual = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_AUTUACAO[i]);										
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo atual no dia " + df.format(datFinal) + " para o status " + bean.STATUS_AUTUACAO[i] + " !");
				saldoAtual.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo));
			}
			bean.setSaldoAtual(saldoAtual);
			
			stmSaldo.close();			
			
			
			//Carrega o registrado
			String cmdRegistrado = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IS NULL AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtRegistrado = conn.prepareStatement(cmdRegistrado);
			smtRegistrado.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtRegistrado.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtRegistrado.setInt(3, codOrgao.intValue());
			smtRegistrado.setInt(4, bean.STATUS_REGISTRADO);
			
			Integer registrado = new Integer(0);
			ResultSet rsRegistrado = smtRegistrado.executeQuery();
			if (rsRegistrado.next()) registrado = new Integer(rsRegistrado.getInt("QTD_AUTOS"));
			bean.setRegistrado(registrado);						
			smtRegistrado.close();
			
			
			//Carrega a entrada da fase penalidade
			String cmdEntradaPen = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusPenalidade() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaPen = conn.prepareStatement(cmdEntradaPen);
			smtEntradaPen.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaPen.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaPen.setInt(3, codOrgao.intValue());
			
			Map entradaPenalidade = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtEntradaPen.setInt(4, bean.STATUS_AUTUACAO[i]);										
				ResultSet rs = smtEntradaPen.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaPenalidade.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo));
			}
			bean.setEntradaPenalidade(entradaPenalidade);			
			smtEntradaPen.close();
			
			
			//Carrega a saida da fase penalidade
			String cmdSaidaPen = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusPenalidade() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaPen = conn.prepareStatement(cmdSaidaPen);
			smtSaidaPen.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaPen.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaPen.setInt(3, codOrgao.intValue());	
			
			Map saidaPenalidade = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtSaidaPen.setInt(4, bean.STATUS_AUTUACAO[i]);										
				ResultSet rs = smtSaidaPen.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaPenalidade.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaPenalidade(saidaPenalidade);			
			smtSaidaPen.close();
			
			
			//Carrega a entrada da fase transitado em julgado
			String cmdEntradaTran = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusTransitado() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaTran = conn.prepareStatement(cmdEntradaTran);
			smtEntradaTran.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaTran.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaTran.setInt(3, codOrgao.intValue());
			
			Map entradaTransitado = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtEntradaTran.setInt(4, bean.STATUS_AUTUACAO[i]);
				ResultSet rs = smtEntradaTran.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaTransitado.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo));
			}
			bean.setEntradaTransitado(entradaTransitado);			
			smtEntradaTran.close();
			
			
			//Carrega a saida da fase transitado em julgado
			String cmdSaidaTran = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusTransitado() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaTran = conn.prepareStatement(cmdSaidaTran);
			smtSaidaTran.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaTran.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaTran.setInt(3, codOrgao.intValue());
			
			Map saidaTransitado = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtSaidaTran.setInt(4, bean.STATUS_AUTUACAO[i]);
				ResultSet rs = smtSaidaTran.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaTransitado.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaTransitado(saidaTransitado);			
			smtSaidaTran.close();
			
			
			//Carrega a entrada dentro dos status da fase de autua��o
			String cmdEntradaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusAutuacao() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaStat = conn.prepareStatement(cmdEntradaStat);
			smtEntradaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaStat.setInt(3, codOrgao.intValue());
			
			Map entradaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtEntradaStat.setInt(4, bean.STATUS_AUTUACAO[i]);
				ResultSet rs = smtEntradaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaStatus.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo));
			}
			bean.setEntradaStatus(entradaStatus);			
			smtEntradaStat.close();
			
			
			//Carrega a saida dentro dos status da fase de autua��o
			String cmdSaidaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusAutuacao() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaStat = conn.prepareStatement(cmdSaidaStat);
			smtSaidaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaStat.setInt(3, codOrgao.intValue());
			
			Map saidaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				int saldo = 0;				
				smtSaidaStat.setInt(4, bean.STATUS_AUTUACAO[i]);
				ResultSet rs = smtSaidaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaStatus.put(new Integer(bean.STATUS_AUTUACAO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaStatus(saidaStatus);			
			smtSaidaStat.close();
			
			
			//Gera o total de cada linha do demonstrativo
			Map total = new HashMap();
			
			int totSaldoAnterior = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAnterior().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totSaldoAnterior += saldo.intValue();
			}
			total.put("saldoAnterior", new Integer(totSaldoAnterior));
			
			total.put("registrado", bean.getRegistrado());
			
			int totEntradaPenalidade = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaPenalidade().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totEntradaPenalidade += saldo.intValue();
			}
			total.put("entradaPenalidade", new Integer(totEntradaPenalidade));
			
			int totSaidaPenalidade = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaPenalidade().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totSaidaPenalidade += saldo.intValue();
			}
			total.put("saidaPenalidade", new Integer(totSaidaPenalidade));
			
			int totEntradaTransitado = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaTransitado().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totEntradaTransitado += saldo.intValue();
			}
			total.put("entradaTransitado", new Integer(totEntradaTransitado));
			
			int totSaidaTransitado = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaTransitado().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totSaidaTransitado += saldo.intValue();
			}
			total.put("saidaTransitado", new Integer(totSaidaTransitado));	
			
			int totEntradaStatus = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaStatus().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totEntradaStatus += saldo.intValue();
			}
			total.put("entradaStatus", new Integer(totEntradaStatus));
			
			int totSaidaStatus = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaStatus().get(new Integer(bean.STATUS_AUTUACAO[i]));
				totSaidaStatus += saldo.intValue();
			}
			total.put("saidaStatus", new Integer(totSaidaStatus));
			
			bean.setTotal(total);
			
			int totTotais = 0;
			totTotais += totSaldoAnterior;
			totTotais += bean.getRegistrado().intValue();
			totTotais += totEntradaPenalidade;
			totTotais += totSaidaPenalidade;
			totTotais += totEntradaTransitado;
			totTotais += totSaidaTransitado;
			totTotais += totEntradaStatus;
			totTotais += totSaidaStatus;
			total.put("totais", new Integer(totTotais));
			
			//Verifica qual status tem a soma da movimenta��o diferente do saldo atual			
			Map verifTotal = new HashMap();
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {
				Integer status = new Integer(bean.STATUS_AUTUACAO[i]);
				
				int qtd = 0;
				qtd += ((Integer) saldoAnterior.get(status)).intValue();
				if (status.equals(new Integer(0))) qtd += registrado.intValue();
				qtd += ((Integer) entradaPenalidade.get(status)).intValue();
				qtd += ((Integer) saidaPenalidade.get(status)).intValue();
				qtd += ((Integer) entradaTransitado.get(status)).intValue();
				qtd += ((Integer) saidaTransitado.get(status)).intValue();
				qtd += ((Integer) entradaStatus.get(status)).intValue();
				qtd += ((Integer) saidaStatus.get(status)).intValue();				
				
				int saldo = ((Integer) saldoAtual.get(status)).intValue();
				if (qtd != saldo) verifTotal.put(status, new Integer(qtd));
			}
			bean.setVerifTotal(verifTotal);
			
			//Verifica se a soma do saldo atual � diferente da soma das linhas de totais			
			int verifSaldo = 0;
			for (int i = 0; i < bean.STATUS_AUTUACAO.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAtual().get(new Integer(bean.STATUS_AUTUACAO[i]));
				verifSaldo += saldo.intValue();
			}
			if (verifSaldo != totTotais)
				bean.setVerifSaldo(new Integer(verifSaldo));	
			
			//Verifica se a quantidade de entrada e sa�da dentro do status de autua��o s�o diferentes
			if (totEntradaStatus != (totSaidaStatus*-1)) bean.setIndVerifStatus(false);						
			
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage()); 
		} finally {
			if (conn != null) { try { serviceloc.setReleaseConnection(conn); } 
			catch (Exception e) {System.out.println(e);} }
		}		
	}
	
	public void geraDemonstrativoPenalidade(DemonstrativoSaldoBean bean, Integer codOrgao, Date datInicial, Date datFinal) 
	throws DaoException
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			String cmdSaldo = "SELECT QTD_SALDO FROM TSMI_SALDO_STATUS WHERE DAT_SALDO = ? AND COD_ORGAO = ? AND COD_STATUS = ?";
			PreparedStatement stmSaldo = conn.prepareStatement(cmdSaldo);			
			stmSaldo.setInt(2, codOrgao.intValue());
			
			//Carrega o saldo anterior
			stmSaldo.setDate(1, new java.sql.Date(datInicial.getTime()));
			Map saldoAnterior = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_PENALIDADE[i]);										
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo anterior no dia " + df.format(datInicial) + " para o status " + bean.STATUS_PENALIDADE[i] + " !");
				saldoAnterior.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo));
			}
			bean.setSaldoAnterior(saldoAnterior);
			
			//Carrega o saldo atual
			stmSaldo.setDate(1, new java.sql.Date(datFinal.getTime()));
			Map saldoAtual = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_PENALIDADE[i]);										
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo atual no dia " + df.format(datFinal) + " para o status " + bean.STATUS_PENALIDADE[i] + " !");
				saldoAtual.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo));
			}
			bean.setSaldoAtual(saldoAtual);
			
			stmSaldo.close();
			
			
			//Carrega a entrada da fase autua��o
			String cmdEntradaAut = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusAutuacao() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaAut = conn.prepareStatement(cmdEntradaAut);
			smtEntradaAut.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaAut.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaAut.setInt(3, codOrgao.intValue());
			
			Map entradaAutuacao = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtEntradaAut.setInt(4, bean.STATUS_PENALIDADE[i]);										
				ResultSet rs = smtEntradaAut.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaAutuacao.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo));
			}
			bean.setEntradaAutuacao(entradaAutuacao);			
			smtEntradaAut.close();
			
			
			//Carrega a saida da fase autua��o
			String cmdSaidaAut = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusAutuacao() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaAut = conn.prepareStatement(cmdSaidaAut);
			smtSaidaAut.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaAut.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaAut.setInt(3, codOrgao.intValue());	
			
			Map saidaAutuacao = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtSaidaAut.setInt(4, bean.STATUS_PENALIDADE[i]);										
				ResultSet rs = smtSaidaAut.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaAutuacao.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo*-1));
			}
			bean.setSaidaAutuacao(saidaAutuacao);
			smtSaidaAut.close();
			
			
			//Carrega a entrada da fase transitado em julgado
			String cmdEntradaTran = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusTransitado() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaTran = conn.prepareStatement(cmdEntradaTran);
			smtEntradaTran.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaTran.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaTran.setInt(3, codOrgao.intValue());
			
			Map entradaTransitado = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtEntradaTran.setInt(4, bean.STATUS_PENALIDADE[i]);
				ResultSet rs = smtEntradaTran.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaTransitado.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo));
			}
			bean.setEntradaTransitado(entradaTransitado);			
			smtEntradaTran.close();
			
			
			//Carrega a saida da fase transitado em julgado
			String cmdSaidaTran = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusTransitado() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaTran = conn.prepareStatement(cmdSaidaTran);
			smtSaidaTran.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaTran.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaTran.setInt(3, codOrgao.intValue());
			
			Map saidaTransitado = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtSaidaTran.setInt(4, bean.STATUS_PENALIDADE[i]);
				ResultSet rs = smtSaidaTran.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaTransitado.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo*-1));
			}
			bean.setSaidaTransitado(saidaTransitado);			
			smtSaidaTran.close();
			
			
			//Carrega a entrada dentro dos status da fase de penalidade
			String cmdEntradaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusPenalidade() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaStat = conn.prepareStatement(cmdEntradaStat);
			smtEntradaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaStat.setInt(3, codOrgao.intValue());
			
			Map entradaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtEntradaStat.setInt(4, bean.STATUS_PENALIDADE[i]);
				ResultSet rs = smtEntradaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaStatus.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo));
			}
			bean.setEntradaStatus(entradaStatus);			
			smtEntradaStat.close();
			
			
			//Carrega a saida dentro dos status da fase de penalidade
			String cmdSaidaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusPenalidade() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaStat = conn.prepareStatement(cmdSaidaStat);
			smtSaidaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaStat.setInt(3, codOrgao.intValue());
			
			Map saidaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				int saldo = 0;				
				smtSaidaStat.setInt(4, bean.STATUS_PENALIDADE[i]);
				ResultSet rs = smtSaidaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaStatus.put(new Integer(bean.STATUS_PENALIDADE[i]), new Integer(saldo*-1));
			}
			bean.setSaidaStatus(saidaStatus);			
			smtSaidaStat.close();
			
			
			//Gera o total de cada linha do demonstrativo
			Map total = new HashMap();
			
			int totSaldoAnterior = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAnterior().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totSaldoAnterior += saldo.intValue();
			}
			total.put("saldoAnterior", new Integer(totSaldoAnterior));		
			
			int totEntradaAutuacao = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaAutuacao().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totEntradaAutuacao += saldo.intValue();
			}
			total.put("entradaAutuacao", new Integer(totEntradaAutuacao));
			
			int totSaidaAutuacao = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaAutuacao().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totSaidaAutuacao += saldo.intValue();
			}
			total.put("saidaAutuacao", new Integer(totSaidaAutuacao));
			
			int totEntradaTransitado = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaTransitado().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totEntradaTransitado += saldo.intValue();
			}
			total.put("entradaTransitado", new Integer(totEntradaTransitado));
			
			int totSaidaTransitado = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaTransitado().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totSaidaTransitado += saldo.intValue();
			}
			total.put("saidaTransitado", new Integer(totSaidaTransitado));	
			
			int totEntradaStatus = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaStatus().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totEntradaStatus += saldo.intValue();
			}
			total.put("entradaStatus", new Integer(totEntradaStatus));
			
			int totSaidaStatus = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaStatus().get(new Integer(bean.STATUS_PENALIDADE[i]));
				totSaidaStatus += saldo.intValue();
			}
			total.put("saidaStatus", new Integer(totSaidaStatus));
			
			int totTotais = 0;
			totTotais += totSaldoAnterior;
			totTotais += totEntradaAutuacao;
			totTotais += totSaidaAutuacao;
			totTotais += totEntradaTransitado;
			totTotais += totSaidaTransitado;
			totTotais += totEntradaStatus;
			totTotais += totSaidaStatus;
			total.put("totais", new Integer(totTotais));
			
			bean.setTotal(total);
			
			
			//Verifica qual status tem a soma da movimenta��o diferente do saldo atual			
			Map verifTotal = new HashMap();
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {
				Integer status = new Integer(bean.STATUS_PENALIDADE[i]);
				
				int qtd = 0;
				qtd += ((Integer) saldoAnterior.get(status)).intValue();
				qtd += ((Integer) entradaAutuacao.get(status)).intValue();
				qtd += ((Integer) saidaAutuacao.get(status)).intValue();
				qtd += ((Integer) entradaTransitado.get(status)).intValue();
				qtd += ((Integer) saidaTransitado.get(status)).intValue();
				qtd += ((Integer) entradaStatus.get(status)).intValue();
				qtd += ((Integer) saidaStatus.get(status)).intValue();				
				
				int saldo = ((Integer) saldoAtual.get(status)).intValue();
				if (qtd != saldo) verifTotal.put(status, new Integer(qtd));
			}
			bean.setVerifTotal(verifTotal);
			
			//Verifica se a soma do saldo atual � diferente da soma das linhas de totais			
			int verifSaldo = 0;
			for (int i = 0; i < bean.STATUS_PENALIDADE.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAtual().get(new Integer(bean.STATUS_PENALIDADE[i]));
				verifSaldo += saldo.intValue();
			}
			if (verifSaldo != totTotais)
				bean.setVerifSaldo(new Integer(verifSaldo));
			
			//Verifica se a quantidade de entrada e sa�da dentro do status de penalidade s�o diferentes
			if (totEntradaStatus != (totSaidaStatus*-1)) bean.setIndVerifStatus(false);
			
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());    
		} finally {
			if (conn != null) { try { serviceloc.setReleaseConnection(conn); } 
			catch (Exception e) {System.out.println(e);} }
		}		
	}
	
	public void geraDemonstrativoTransitado(DemonstrativoSaldoBean bean, Integer codOrgao, Date datInicial, Date datFinal) 
	throws DaoException
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			String cmdSaldo = "SELECT QTD_SALDO FROM TSMI_SALDO_STATUS WHERE DAT_SALDO = ? AND COD_ORGAO = ? AND COD_STATUS = ?";
			PreparedStatement stmSaldo = conn.prepareStatement(cmdSaldo);			
			stmSaldo.setInt(2, codOrgao.intValue());
			
			//Carrega o saldo anterior
			stmSaldo.setDate(1, new java.sql.Date(datInicial.getTime()));
			Map saldoAnterior = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo anterior no dia " + df.format(datInicial) + " para o status " + bean.STATUS_TRANSITADO[i] + " !");
				saldoAnterior.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo));
			}
			bean.setSaldoAnterior(saldoAnterior);
			
			//Carrega o saldo atual
			stmSaldo.setDate(1, new java.sql.Date(datFinal.getTime()));
			Map saldoAtual = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				stmSaldo.setInt(3, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = stmSaldo.executeQuery();
				boolean existeSaldo = false;
				if (rs.next()) {
					existeSaldo = true;
					saldo = rs.getInt("QTD_SALDO");
				}
				rs.close();
				if (!existeSaldo)
					throw new DaoException("N�o existe saldo atual no dia " + df.format(datFinal) + " para o status " + bean.STATUS_TRANSITADO[i] + " !");
				saldoAtual.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo));
			}
			bean.setSaldoAtual(saldoAtual);
			
			stmSaldo.close();			
			
			
			//Carrega a entrada da fase autua��o
			String cmdEntradaAut = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusAutuacao() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaAut = conn.prepareStatement(cmdEntradaAut);
			smtEntradaAut.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaAut.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaAut.setInt(3, codOrgao.intValue());
			
			Map entradaAutuacao = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtEntradaAut.setInt(4, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = smtEntradaAut.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaAutuacao.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo));
			}
			bean.setEntradaAutuacao(entradaAutuacao);			
			smtEntradaAut.close();
			
			
			//Carrega a saida da fase autua��o
			String cmdSaidaAut = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusAutuacao() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaAut = conn.prepareStatement(cmdSaidaAut);
			smtSaidaAut.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaAut.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaAut.setInt(3, codOrgao.intValue());	
			
			Map saidaAutuacao = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtSaidaAut.setInt(4, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = smtSaidaAut.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaAutuacao.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaAutuacao(saidaAutuacao);
			smtSaidaAut.close();			
			
			
			//Carrega a entrada da fase penalidade
			String cmdEntradaPen = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusPenalidade() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaPen = conn.prepareStatement(cmdEntradaPen);
			smtEntradaPen.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaPen.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaPen.setInt(3, codOrgao.intValue());
			
			Map entradaPenalidade = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtEntradaPen.setInt(4, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = smtEntradaPen.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaPenalidade.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo));
			}
			bean.setEntradaPenalidade(entradaPenalidade);			
			smtEntradaPen.close();
			
			
			//Carrega a saida da fase penalidade
			String cmdSaidaPen = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusPenalidade() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaPen = conn.prepareStatement(cmdSaidaPen);
			smtSaidaPen.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaPen.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaPen.setInt(3, codOrgao.intValue());	
			
			Map saidaPenalidade = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtSaidaPen.setInt(4, bean.STATUS_TRANSITADO[i]);										
				ResultSet rs = smtSaidaPen.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaPenalidade.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaPenalidade(saidaPenalidade);			
			smtSaidaPen.close();
			
			
			//Carrega a entrada dentro dos status da fase de transitado em julgado
			String cmdEntradaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_ORIGEM IN " + bean.listStatusAutuacao() + " AND COD_STATUS_DESTINO = ?";
			PreparedStatement smtEntradaStat = conn.prepareStatement(cmdEntradaStat);
			smtEntradaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtEntradaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtEntradaStat.setInt(3, codOrgao.intValue());
			
			Map entradaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtEntradaStat.setInt(4, bean.STATUS_TRANSITADO[i]);
				ResultSet rs = smtEntradaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				entradaStatus.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo));
			}
			bean.setEntradaStatus(entradaStatus);			
			smtEntradaStat.close();
			
			
			//Carrega a saida dentro dos status da fase de transitado em julgado
			String cmdSaidaStat = "SELECT SUM(QTD_AUTOS) AS QTD_AUTOS FROM TSMI_MOV_STATUS WHERE DAT_MOVIMENTO BETWEEN ? AND ?"
				+ " AND COD_ORGAO = ? AND COD_STATUS_DESTINO IN " + bean.listStatusAutuacao() + " AND COD_STATUS_ORIGEM = ?";
			PreparedStatement smtSaidaStat = conn.prepareStatement(cmdSaidaStat);
			smtSaidaStat.setDate(1, new java.sql.Date(datInicial.getTime()));
			smtSaidaStat.setDate(2, new java.sql.Date(datFinal.getTime()));
			smtSaidaStat.setInt(3, codOrgao.intValue());
			
			Map saidaStatus = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				int saldo = 0;				
				smtSaidaStat.setInt(4, bean.STATUS_TRANSITADO[i]);
				ResultSet rs = smtSaidaStat.executeQuery();
				if (rs.next()) saldo = rs.getInt("QTD_AUTOS");
				rs.close();				
				saidaStatus.put(new Integer(bean.STATUS_TRANSITADO[i]), new Integer(saldo*-1));
			}
			bean.setSaidaStatus(saidaStatus);			
			smtSaidaStat.close();
			
			
			//Gera o total de cada linha do demonstrativo
			Map total = new HashMap();
			
			int totSaldoAnterior = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAnterior().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totSaldoAnterior += saldo.intValue();
			}
			total.put("saldoAnterior", new Integer(totSaldoAnterior));
			
			int totEntradaAutuacao = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaAutuacao().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totEntradaAutuacao += saldo.intValue();
			}
			total.put("entradaAutuacao", new Integer(totEntradaAutuacao));
			
			int totSaidaAutuacao = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaAutuacao().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totSaidaAutuacao += saldo.intValue();
			}
			total.put("saidaAutuacao", new Integer(totSaidaAutuacao));	
			
			int totEntradaPenalidade = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaPenalidade().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totEntradaPenalidade += saldo.intValue();
			}
			total.put("entradaPenalidade", new Integer(totEntradaPenalidade));
			
			int totSaidaPenalidade = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaPenalidade().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totSaidaPenalidade += saldo.intValue();
			}
			total.put("saidaPenalidade", new Integer(totSaidaPenalidade));
			
			int totEntradaStatus = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getEntradaStatus().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totEntradaStatus += saldo.intValue();
			}
			total.put("entradaStatus", new Integer(totEntradaStatus));
			
			int totSaidaStatus = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getSaidaStatus().get(new Integer(bean.STATUS_TRANSITADO[i]));
				totSaidaStatus += saldo.intValue();
			}
			total.put("saidaStatus", new Integer(totSaidaStatus));
			
			int totTotais = 0;
			totTotais += totSaldoAnterior;
			totTotais += totEntradaAutuacao;
			totTotais += totSaidaAutuacao;
			totTotais += totEntradaPenalidade;
			totTotais += totSaidaPenalidade;
			totTotais += totEntradaStatus;
			totTotais += totSaidaStatus;
			total.put("totais", new Integer(totTotais));
			
			bean.setTotal(total);
			
			
			//Verifica qual status tem a soma da movimenta��o diferente do saldo atual			
			Map verifTotal = new HashMap();
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {
				Integer status = new Integer(bean.STATUS_TRANSITADO[i]);
				
				int qtd = 0;
				qtd += ((Integer) saldoAnterior.get(status)).intValue();
				qtd += ((Integer) entradaAutuacao.get(status)).intValue();
				qtd += ((Integer) saidaAutuacao.get(status)).intValue();
				qtd += ((Integer) entradaPenalidade.get(status)).intValue();
				qtd += ((Integer) saidaPenalidade.get(status)).intValue();
				qtd += ((Integer) entradaStatus.get(status)).intValue();
				qtd += ((Integer) saidaStatus.get(status)).intValue();				
				
				int saldo = ((Integer) saldoAtual.get(status)).intValue();
				if (qtd != saldo) verifTotal.put(status, new Integer(qtd));
			}
			bean.setVerifTotal(verifTotal);
			
			//Verifica se a soma do saldo atual � diferente da soma das linhas de totais			
			int verifSaldo = 0;
			for (int i = 0; i < bean.STATUS_TRANSITADO.length; i++) {				
				Integer saldo = (Integer) bean.getSaldoAtual().get(new Integer(bean.STATUS_TRANSITADO[i]));
				verifSaldo += saldo.intValue();
			}
			if (verifSaldo != totTotais)
				bean.setVerifSaldo(new Integer(verifSaldo));
			
			//Verifica se a quantidade de entrada e sa�da dentro do status de transitado s�o diferentes
			if (totEntradaStatus != (totSaidaStatus*-1)) bean.setIndVerifStatus(false);
			
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());    
		} finally {
			if (conn != null) { try { serviceloc.setReleaseConnection(conn); } 
			catch (Exception e) {System.out.println(e);} }
		}		
	}
	
	/**-----------------------------------------------------------
	 * Inicio da Rotina de Cria��o autom�tica do 
	 * Cursor
	 * -----------------------------------------------------------
	 * @throws DaoException
	 * @throws ServiceLocatorException
	 * @throws SQLException
	 * @author Wellem
	 * */
	
	public boolean  CarregaEstatisticas(GER.ResultadoAnualBean myresult) throws DaoException, ServiceLocatorException, SQLException {
		
		boolean ok =true;
		Connection conn =null ;
		ResultSet rs = null;	
		conn = serviceloc.getConnection(MYABREVSIST) ;
		Statement stmt = conn.createStatement();
		conn.setAutoCommit(false);
		try{
			String sCmd  = "";
			BigDecimal percProcDef 				= new BigDecimal(0);
			BigDecimal quantProcInDef 			= new BigDecimal((double)0);
			BigDecimal quantProcDef 			= new BigDecimal((double)0);
			BigDecimal percProcInDef 			= new BigDecimal(0);
			BigDecimal defpercent 				= new BigDecimal((double)0);
			BigDecimal Indefpercent 			= new BigDecimal((double)0);
			BigDecimal totmediaDef 				= new BigDecimal((double)0);
			BigDecimal totmediaInDef 			= new BigDecimal((double)0);			
			BigDecimal totgeral 			    = new BigDecimal(0);			
			int quantosmesesDef=0;
			int quantosmesesIndef=0;
			
			String meses[]={"Janeiro","Fevereiro","Mar�o","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"};
			ArrayList Meses = new ArrayList();
			
			if (!"Todos".equals(myresult.getSigOrgao())){		
				sCmd = "SELECT sig_orgao FROM TSMI_ORGAO WHERE cod_orgao ='"+myresult.getOrgao()+"'";
				rs = stmt.executeQuery(sCmd);
				while (rs.next()) 
					myresult.setSigOrgao(rs.getString("sig_orgao"));
			}
			
			for (int i=0;i<12;i++){
				ResultadoAnualBean myresultado = new ResultadoAnualBean();
				myresultado.setMes(meses[i]);
				Meses.add(myresultado);					
			}				
			
			

			/*
			SELECT TO_CHAR(DAT_RESULTADO,'MM/YYYY'),IND_RESULTADO,COUNT(*)
			FROM TGER_JULGAMENTO
			WHERE TO_CHAR(DAT_RESULTADO,'YYYY')='2012'
			GROUP BY TO_CHAR(DAT_RESULTADO,'MM/YYYY'),IND_RESULTADO
			*/

			
			
			
			
			myresult.setListaMes(Meses);			
			sCmd="select to_char(DAT_RESULTADO,'MM') MesAno,sum(QTD_DEFERIDOS) Deferidos,"; 
			sCmd+="round((sum(QTD_DEFERIDOS))*100/(sum(QTD_DEFERIDOS) + sum(QTD_INDEFERIDOS)),2) DefPerc,"; 
			sCmd+="sum(QTD_INDEFERIDOS)Indeferidos,";
			sCmd+="round((sum(QTD_INDEFERIDOS))*100/(sum(QTD_DEFERIDOS) +sum(QTD_INDEFERIDOS)),2) IndefPerc,"; 
			sCmd+="sum(QTD_DEFERIDOS) + sum(QTD_INDEFERIDOS) Total";			
			sCmd+=" from TGER_RESUMO_JULGAMENTO WHERE ";
			/*
			sCmd+="cod_uf='"+myresult.getCodEstado()+"' ";
			*/				
			sCmd+="to_char(dat_resultado,'yyyy')='"+myresult.getAno()+"' ";
			/*
			sCmd+="and cod_status_requerimento>=3 ";
			*/			
			sCmd+="and cod_tipo_solic IN (" + myresult.getTpreq()+ ") ";
			if (!"Todos".equals(myresult.getSigOrgao()))
				sCmd+="and cod_orgao='"+myresult.getOrgao()+"' ";
			sCmd+="group by to_char(dat_resultado,'MM')";
			rs  = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				//	pegar as variaveis
				String sMes=meses[Integer.parseInt(rs.getString("MesAno").trim())-1];				
				Iterator it = myresult.getListaMes().iterator();										
				while (it.hasNext()) 
				{ 							
					ResultadoAnualBean mes = (GER.ResultadoAnualBean)it.next();
					if (sMes.equalsIgnoreCase(mes.getMes()))
					{
						String def=rs.getString("Deferidos");
						if (Long.parseLong(def)>0)
							quantosmesesDef=quantosmesesDef+1;						
						mes.setDeferidos(def);						
						mes.setDefPercent(rs.getString("DefPerc"));
						
						String indef=rs.getString("Indeferidos");
						if (Long.parseLong(indef)>0)
							quantosmesesIndef=quantosmesesIndef+1;						
						
						mes.setIndeferidos(indef);
						mes.setIndefPercent(rs.getString("indefPerc"));
						
						String tot=rs.getString("Total");
						mes.setTotal(tot);								
						
						quantProcDef=quantProcDef.add(new BigDecimal(def));				
						quantProcInDef=quantProcInDef.add(new BigDecimal (indef));
						
						percProcDef=percProcDef.add(new BigDecimal(mes.getDefPercent()));
						percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						percProcInDef=percProcInDef.add(new BigDecimal(mes.getIndefPercent()));
						percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
					}
				}					
			}
			
			//Seto os Totais
			String totdef = String.valueOf(quantProcDef);
			String totindef = String.valueOf(quantProcInDef);
			
			
			totgeral=quantProcDef.add(quantProcInDef);
			
			
			
			String totger= String.valueOf(totgeral);
			//String totdef=String.valueOf(quantProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN));
			
			//totdef=sys.Util.ComEdt(sys.Util.SemEdt(totdef,'.'),15,2).replace(',','.');
			long totdefer=Long.parseLong(totdef);
			myresult.setTotalDef(String.valueOf(totdefer));
			
			long totindefer=Long.parseLong(totindef);
			myresult.setTotalInDef(String.valueOf(totindefer));	
			
			long totgera=Long.parseLong(totger);
			myresult.setTotalGer(String.valueOf(totgera));
			
			//Porcentagem Deferidos
			defpercent=(quantProcDef.multiply(new BigDecimal((double)100)));
			defpercent=defpercent.setScale(2,1);			
			defpercent=defpercent.divide(totgeral,BigDecimal.ROUND_CEILING);		
			myresult.setTotalDefPerc(String.valueOf(defpercent).replace('.',','));
			
			//Porcentagem indeferidos		
			Indefpercent=(quantProcInDef.multiply(new BigDecimal((double)100)));
			Indefpercent=Indefpercent.setScale(2,1);
			Indefpercent=Indefpercent.divide(totgeral,BigDecimal.ROUND_CEILING);
			myresult.setTotalInDefPerc(String.valueOf(Indefpercent).replace('.',','));
			
			//M�dias
			totmediaDef=quantProcDef;
			totmediaDef=totmediaDef.setScale(0,1);			
			totmediaDef=totmediaDef.divide(new BigDecimal ((double)quantosmesesDef),BigDecimal.ROUND_CEILING);
			
			
			totmediaInDef=quantProcInDef;
			totmediaInDef=totmediaInDef.setScale(0,1);			
			totmediaInDef=totmediaInDef.divide(new BigDecimal ((double)quantosmesesIndef),BigDecimal.ROUND_CEILING);
			
//			Seto os Dados no bean		
			//Totais de M�dias
			String toMeddef=String.valueOf(totmediaDef);
			//toMeddef=sys.Util.ComEdt(sys.Util.SemEdt(toMeddef,'.'),15,2).replace(',','.');
			myresult.setTotalMediaDef(toMeddef);
			
			String toMedindef=String.valueOf(totmediaInDef);
			//toMedindef=sys.Util.ComEdt(sys.Util.SemEdt(toMedindef,'.'),15,2).replace(',','.');
			myresult.setTotMediaInDef(toMedindef);
			
			String toMed=String.valueOf(totmediaDef.add(totmediaInDef));
			Double.parseDouble(toMed);
			//toMed=sys.Util.ComEdt(sys.Util.SemEdt(toMed,'.'),15,2).replace(',','.');
			myresult.setTotalMedia(toMed);
			
			rs.close();	
			conn.commit();
			
			if (conn != null) stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ok;
	}	
	
	/**-----------------------------------------------------------
	 * Carrega as Estatisticas dos Artigos por m�s
	 * -----------------------------------------------------------
	 * @throws DaoException
	 * @throws ServiceLocatorException
	 * @throws SQLException
	 * @author Wellem
	 * */
	
	public boolean  CarregaEstatisticas(GER.EstatisticaArtigoBean myresult) throws DaoException, ServiceLocatorException, SQLException {
		
		boolean ok =true;
		Connection conn =null ;
		ResultSet rs = null;
		conn = serviceloc.getConnection(MYABREVSIST) ;
		Statement stmt = conn.createStatement();
		conn.setAutoCommit(false);
		try{
			String sCmd  = "";
			int quantDef=0;
			int quantInDef=0;
			BigDecimal percProcDef 	= new BigDecimal(0);
			BigDecimal quantProcInDef = new BigDecimal((double)0);
			BigDecimal quantProcDef = new BigDecimal((double)0);
			BigDecimal percProcInDef = new BigDecimal(0);
			BigDecimal defpercent = new BigDecimal((double)0);
			BigDecimal Indefpercent	= new BigDecimal((double)0);
			BigDecimal totmediaDef = new BigDecimal((double)0);
			BigDecimal totmediaInDef = new BigDecimal((double)0);			
			BigDecimal totgeral = new BigDecimal(0);			
			ArrayList resultados = new ArrayList();
			if (!"Todos".equals(myresult.getSigOrgao())){		
				sCmd = "SELECT sig_orgao FROM TSMI_ORGAO WHERE cod_orgao ='"+myresult.getOrgao()+"'";
				rs = stmt.executeQuery(sCmd);
				while (rs.next()) 
					myresult.setSigOrgao(rs.getString("sig_orgao"));
			}
			sCmd= "select txt_artigo , cod_infracao, txt_infracao,sum(QTD_DEFERIDOS) Deferidos,"; 
			sCmd+="sum(QTD_INDEFERIDOS)Indeferidos, round((sum(QTD_DEFERIDOS))*100/(sum(QTD_DEFERIDOS) + sum(QTD_INDEFERIDOS)),2) DefPerc,";
			sCmd+="round((sum(QTD_INDEFERIDOS))*100/(sum(QTD_DEFERIDOS) +sum(QTD_INDEFERIDOS)),2) IndefPerc,";
			sCmd+="sum(QTD_DEFERIDOS) + sum(QTD_INDEFERIDOS) Total  from TGER_REQ_RESULTADO WHERE ";			
			sCmd+="cod_uf='"+myresult.getCodEstado()+"' ";				
			if (!"Todos".equals(myresult.getSigOrgao()))
				sCmd+="and cod_orgao='"+myresult.getOrgao()+"' ";
			sCmd+="and dat_resultado BETWEEN to_date('"+myresult.getMesAnoInicio()+"','dd/mm/yyyy') AND " ;
			sCmd+="to_date('"+myresult.getMesAnoFim()+"','dd/mm/yyyy')";
			sCmd+="and cod_status_requerimento >= 3 ";			
			sCmd+="and cod_tipo_solic IN (" + myresult.getTpreq()+ ") ";
			sCmd+="group by cod_infracao,txt_artigo , txt_infracao";
			
			rs  = stmt.executeQuery(sCmd);
			while (rs.next()) {
				//	pegar as variaveis
				EstatisticaArtigoBean mesAno =  new EstatisticaArtigoBean() ;
				mesAno.setDeferidos(rs.getString("Deferidos"));
				mesAno.setDefPercent(rs.getString("DefPerc"));
				mesAno.setIndeferidos(rs.getString("Indeferidos"));
				mesAno.setIndefPercent(rs.getString("indefPerc"));
				mesAno.setTxtArtigo(rs.getString("txt_artigo"));
				mesAno.setDescricao(rs.getString("txt_infracao"));
				mesAno.setTotal(rs.getString("Total"));	
				resultados.add(mesAno);		
				if (Long.parseLong(mesAno.getDeferidos())>0)
					quantDef += 1;
				
				if (Long.parseLong(mesAno.getIndeferidos())>0)
					quantInDef += 1;
				
				quantProcInDef=quantProcInDef.add(new BigDecimal (mesAno.getIndeferidos()));
				quantProcDef=quantProcDef.add(new BigDecimal(mesAno.getDeferidos()));
				percProcDef=percProcDef.add(new BigDecimal(mesAno.getDefPercent()));
				percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				percProcInDef=percProcInDef.add(new BigDecimal(mesAno.getIndefPercent()));
				percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
			}					
			//Seto os Totais
			if (resultados.size()>0){
				String totdef = String.valueOf(quantProcDef);
				String totindef = String.valueOf(quantProcInDef);
				totgeral=quantProcDef.add(quantProcInDef);
				String totger= String.valueOf(totgeral);
				
				long totdefer=Long.parseLong(totdef);
				myresult.setTotalDef(String.valueOf(totdefer));
				
				long totindefer=Long.parseLong(totindef);
				myresult.setTotalInDef(String.valueOf(totindefer));	
				
				long totgera=Long.parseLong(totger);
				myresult.setTotalGer(String.valueOf(totgera));
				myresult.setResultado(resultados);
				
				//Porcentagem Deferidos
				defpercent=(quantProcDef.multiply(new BigDecimal((double)100)));
				defpercent=defpercent.setScale(2,1);			
				defpercent=defpercent.divide(totgeral,BigDecimal.ROUND_CEILING);		
				myresult.setTotalDefPerc(String.valueOf(defpercent));
				
				//Porcentagem indeferidos		
				Indefpercent=(quantProcInDef.multiply(new BigDecimal((double)100)));
				Indefpercent=Indefpercent.setScale(2,1);
				Indefpercent=Indefpercent.divide(totgeral,BigDecimal.ROUND_CEILING);
				myresult.setTotalInDefPerc(String.valueOf(Indefpercent));
				
				//M�dias
				totmediaDef=quantProcDef;
				totmediaDef=totmediaDef.setScale(2,1);			
				totmediaDef=totmediaDef.divide(new BigDecimal ((double)quantDef),BigDecimal.ROUND_CEILING);
				
				
				totmediaInDef=quantProcInDef;
				totmediaInDef=totmediaInDef.setScale(2,1);			
				totmediaInDef=totmediaInDef.divide(new BigDecimal ((double)quantInDef),BigDecimal.ROUND_CEILING);
				
				//Seto os Dados no bean		
				//Totais de M�dias
				String toMeddef=String.valueOf(totmediaDef);
				toMeddef=sys.Util.ComEdt(sys.Util.SemEdt(toMeddef,'.'),15,2).replace(',','.');
				myresult.setTotalMediaDef(toMeddef);
				
				String toMedindef=String.valueOf(totmediaInDef);
				toMedindef=sys.Util.ComEdt(sys.Util.SemEdt(toMedindef,'.'),15,2).replace(',','.');
				myresult.setTotMediaInDef(toMedindef);
				
				String toMed=String.valueOf(totmediaDef.add(totmediaInDef));
				Double.parseDouble(toMed);
				toMed=sys.Util.ComEdt(sys.Util.SemEdt(toMed,'.'),15,2).replace(',','.');
				myresult.setTotalMedia(toMed);
			}
			rs.close();
			conn.commit();
			if (conn != null) stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ok;
	}
	
	
	public void prodProcesso(GraficoBean myGrf) throws DaoException{
		
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
//			PEGAR DADOS DO ORGAO
			ResultSet rs = null; 
			
			sCmd =  "SELECT SIG_ORGAO ";
			sCmd += "FROM TSMI_ORGAO ";
			sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
			rs  = stmt.executeQuery(sCmd);		    
			while (rs.next()){
				myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
			}
			
			
			int dadosProcesso[][] = new int[5][5];
			String montaIn="";
			
			String [] relator=myGrf.getRelatores(); 
			for(int t=0;t<relator.length;t++){
				montaIn+="'"+relator[t]+"',";
			}
			montaIn=montaIn.substring(0,montaIn.length()-1);
			
			sCmd = "select TXT_RELATOR,SUM(NVL(QTD_DEFERIDOS,0)) DEFERIDOS,"+
			"SUM(NVL(QTD_INDEFERIDOS,0)) INDEFERIDOS,"+
			"SUM((NVL(QTD_DEFERIDOS,0))+(NVL(QTD_INDEFERIDOS,0))) TOTALANALISADOS,"+
			"SUM(NVL(QTD_DISTRIB_RESULT,0)) NAODISTRIBUIDOS,"+ 
			"SUM((NVL(QTD_DEFERIDOS,0))+(NVL(QTD_INDEFERIDOS,0))+(NVL(QTD_DISTRIB_RESULT,0))) TOTALGERAL "+
			" FROM tger_req_distr_result"+
			" WHERE DAT_DISTRIB >= to_date('01/"+myGrf.getDatInicio()+"','dd/mm/yyyy')";
			sCmd+="and cod_tipo_solic IN (" + myGrf.getTipoReq()+ ") ";			
			if("04,06,09,11".indexOf(myGrf.getDatInicio().substring(0,2))>=0)
				sCmd +=" AND DAT_DISTRIB <= to_date('30/"+myGrf.getDatInicio()+"','dd/mm/yyyy')";
			else if("02".indexOf(myGrf.getDatInicio().substring(0,2))>=0)
				sCmd +=" AND DAT_DISTRIB <= to_date('29/"+myGrf.getDatInicio()+"','dd/mm/yyyy')";
			else
				sCmd +=" AND DAT_DISTRIB <= to_date('31/"+myGrf.getDatInicio()+"','dd/mm/yyyy')";
			sCmd+=" AND cod_status_requerimento >=2 "+
			" AND COD_UF = '"+myGrf.getCodUF()+"'"; 
			sCmd +=" AND cod_RELATOR IN("+montaIn+")";
			sCmd += " GROUP BY TXT_RELATOR";
			
			
			rs = stmt.executeQuery(sCmd);
			int i = 0;
			String relatores[] = new String [5];
			while(rs.next()){
				String relatBd=rs.getString("TXT_RELATOR");
				int pos = relatBd.indexOf(" ");
				String relat = relatBd.substring(0,pos);
				relatores[i] = relat;
				dadosProcesso[i][0] = Integer.parseInt(rs.getString("DEFERIDOS"));
				dadosProcesso[i][1] = Integer.parseInt(rs.getString("INDEFERIDOS"));
				dadosProcesso[i][2] = Integer.parseInt(rs.getString("TOTALANALISADOS"));
				dadosProcesso[i][3] = Integer.parseInt(rs.getString("NAODISTRIBUIDOS"));
				dadosProcesso[i][4] = Integer.parseInt(rs.getString("TOTALGERAL"));
				i++;
			}
			
			myGrf.setDadosProcesso(dadosProcesso);
			myGrf.setRelatores(relatores);
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}finally{
			try{
				serviceloc.setReleaseConnection(conn);	
			}catch(Exception e){}
		}
	}
	public void geraConsulta(String orgao,ProdProcessoBean myprod){
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			ResultSet rs = null; 
			
			conn = serviceloc.getConnection(MYABREVSIST);
			stmt = conn.createStatement();
			sCmd ="select t1.num_cpf as cod_relator,t1.nom_relator from tsmi_relator t1,tsmi_junta t2 "+ 
			"where t1.cod_junta=t2.cod_junta and t2.cod_orgao  ='"+orgao+"' order by t1.nom_relator";
			
			rs = stmt.executeQuery(sCmd);
			ArrayList relat = new ArrayList(); 
			while(rs.next()){
				ProdProcessoBean myRelatores=new ProdProcessoBean();
				myRelatores.setNomrelator(rs.getString("nom_relator"));
				myRelatores.setCodrelator(rs.getString("cod_relator"));
				relat.add(myRelatores);
			}
			myprod.setRelatores(relat);
			stmt.close();
			
		}catch(Exception e){
		}finally{
			try{
				serviceloc.setReleaseConnection(conn);
			}catch(Exception e){	}
		}
	}	
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a SaldoOrgaoBean
	 * -----------------------------------------------------------
	 */
	
	public void GravaSaldoOrgao(SaldoOrgaoBean bean) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        			
			
			//Pega o valor da sequence para o campo CodAIDigitalizado
			Statement stmSeq = conn.createStatement();
			String sqlSeq = "SELECT SEQ_TSMI_SALDO_ORGAO.NEXTVAL AS CODIGO FROM DUAL";
			ResultSet rsSeq = stmSeq.executeQuery(sqlSeq);
			if (rsSeq.next())
				bean.setCodSaldoOrgao(new Integer(rsSeq.getInt("CODIGO")));
			stmSeq.close();
			
			String sqlIns = "INSERT INTO TSMI_SALDO_ORGAO (COD_SALDO_ORGAO, DAT_PROCESSAMENTO, COD_ORGAO,";
			sqlIns += " COD_STATUS, QTD_AUTOS, DAT_INFRACAO_INICIAL, DAT_INFRACAO_FINAL)";
			sqlIns += " VALUES (:COD_SALDO_ORGAO, :DAT_PROCESSAMENTO, :COD_ORGAO, :COD_STATUS, :QTD_AUTOS,";
			sqlIns += " :DAT_INFRACAO_INICIAL, :DAT_INFRACAO_FINAL)";
			PreparedStatement stmIns = conn.prepareStatement(sqlIns);
			stmIns.setInt(1, bean.getCodSaldoOrgao().intValue());
			stmIns.setTimestamp(2, new java.sql.Timestamp(bean.getDatProcessamento().getTime()));
			stmIns.setInt(3, Integer.parseInt(bean.getOrgao().getCodOrgao()));
			stmIns.setInt(4, Integer.parseInt(bean.getStatus().getCodStatus()));
			stmIns.setInt(5, bean.getQtdAutos().intValue());
			stmIns.setDate(6, new java.sql.Date(bean.getDatInfracaoInicial().getTime()));
			stmIns.setDate(7, new java.sql.Date(bean.getDatInfracaoFinal().getTime()));
			stmIns.executeUpdate();
			conn.commit();
			stmIns.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public List ConsultaSaldoOrgao(ACSS.OrgaoBean orgao) throws DaoException {		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT COD_SALDO_ORGAO, DAT_PROCESSAMENTO, COD_ORGAO, COD_STATUS, QTD_AUTOS,";
			sql += " DAT_INFRACAO_INICIAL, DAT_INFRACAO_FINAL FROM TSMI_SALDO_ORGAO";
			sql += " WHERE COD_ORGAO = :COD_ORGAO";
			PreparedStatement stm = conn.prepareStatement(sql);
			stm.setInt(1, Integer.parseInt(orgao.getCodOrgao()));
			ResultSet rs = stm.executeQuery();
			
			List listSaldos = new ArrayList();            
			while (rs.next()) {
				REC.StatusBean status = new REC.StatusBean();
				status.setCodStatus(rs.getString("COD_STATUS"));
				
				SaldoOrgaoBean saldo = new SaldoOrgaoBean();
				saldo.setCodSaldoOrgao(new Integer(rs.getInt("COD_SALDO_ORGAO")));
				saldo.setDatProcessamento(rs.getTimestamp("DAT_PROCESSAMENTO"));
				saldo.setOrgao(orgao);
				saldo.setStatus(status);
				saldo.setQtdAutos(new Integer(rs.getInt("QTD_AUTOS")));
				saldo.setDatInfracaoInicial(rs.getDate("DAT_INFRACAO_INICIAL"));
				saldo.setDatInfracaoFinal(rs.getDate("DAT_INFRACAO_FINAL"));
				listSaldos.add(saldo);
			}            
			stm.close();
			return listSaldos;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}  finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public void getRelatores(ResultadoBean resultadoBean) throws DaoException {
		Connection conn =null ;	
		Vector junta = new Vector();
		
		ResultSet rs1 = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt1 = conn.createStatement();
			
			String sCmd = "SELECT J.COD_JUNTA,J.NOM_JUNTA,J.SIG_JUNTA,J.IND_TIPO "+
			" FROM TSMI_JUNTA J "+
			" WHERE J.COD_ORGAO = ? AND "+
			" J.IND_TIPO = ? ORDER BY J.NOM_JUNTA";
			PreparedStatement stm = conn.prepareStatement(sCmd);
			stm.setInt(1, Integer.parseInt(resultadoBean.getOrgao()));
			stm.setInt(2, Integer.parseInt(resultadoBean.getTipoJunta()));
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				REC.JuntaBean myJunta = new REC.JuntaBean() ;
				myJunta.setCodJunta(rs.getString("cod_junta"));
				myJunta.setNomJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setIndTipo(rs.getString("ind_tipo"));
				String sCmd2 = "SELECT R.NOM_RELATOR, R.COD_RELATOR "+
				" FROM TSMI_RELATOR R WHERE R.COD_JUNTA = '"+rs.getString("cod_junta")+"' ORDER BY R.NUM_ORDEM,R.NOM_RELATOR";
				rs1 = stmt1.executeQuery(sCmd2);
				Vector relator = new Vector();
				int iNumMembro=1;
				while(rs1.next()) {
					REC.RelatorBean myRelator = new REC.RelatorBean() ;
					myRelator.setNomRelator(rs1.getString("nom_relator"));
					myRelator.setCodRelator(rs1.getString("cod_relator"));
					myRelator.setPkid(Integer.toString(iNumMembro));
					relator.add(myRelator);
					iNumMembro++;
				}
				myJunta.setRelator(relator);
				if(myJunta.getRelator().size()>0)
					junta.add(myJunta);
			}
			resultadoBean.setJuntaRelator(junta);
			rs.close();
			rs1.close();
		}//fim do try
		catch (Exception e) {
		}//fim do catch
		finally {
			if (conn != null) {
				try {serviceloc.setReleaseConnection(conn);}
				catch (Exception ey) { }
			}
		}
	}
	
	public void mostraEstatistica(ResultadoBean myresult) throws DaoException
	{
		
		Connection conn = null;
		ResultSet rs = null;
		String sCmd = "";
		String ultimoArtigo = "";
		
		ArrayList resultadoFinal = new ArrayList();
		BigDecimal quantProcInDef = new BigDecimal((double)0);
		BigDecimal quantProcDef = new BigDecimal((double)0);
		BigDecimal percProcInDef = new BigDecimal(0);
		BigDecimal percProcDef 	= new BigDecimal(0);
		BigDecimal Indefpercent = new BigDecimal((double)0);
		BigDecimal defpercent = new BigDecimal((double)0);
		BigDecimal totmedia = new BigDecimal((double)0);
		BigDecimal totgeral = new BigDecimal(0);
		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = "select R.TXT_ARTIGO,J.SIG_JUNTA,R.TXT_RELATOR,SUM(NVL(R.QTD_DEFERIDOS,0)) DEFERIDOS,"+
			" SUM(NVL(R.QTD_INDEFERIDOS,0)) INDEFERIDOS,"+
			" SUM((NVL(R.QTD_DEFERIDOS,0))+(NVL(R.QTD_INDEFERIDOS,0))) TOTALANALISADOS,"+
			" SUM(NVL(R.QTD_DISTRIB_RESULT,0)) NAODISTRIBUIDOS,"+
			" SUM((NVL(R.QTD_DEFERIDOS,0))+(NVL(R.QTD_INDEFERIDOS,0))) TOTALGERAL," +
			" round((sum(R.QTD_DEFERIDOS))*100/(sum(R.QTD_DEFERIDOS) + sum(R.QTD_INDEFERIDOS)),2) DefPerc," +
			" round((sum(R.QTD_INDEFERIDOS))*100/(sum(R.QTD_DEFERIDOS) +sum(R.QTD_INDEFERIDOS)),2) IndefPerc "+
			" FROM tger_req_distr_result R, TSMI_JUNTA J, TSMI_RELATOR RE "+
			" WHERE to_char(R.DAT_DISTRIB,'mm/yyyy')='"+myresult.getMesAno()+"'"+  
			" and R.COD_ORGAO = J.COD_ORGAO "+
			" and J.COD_JUNTA = RE.COD_JUNTA "+
			" AND R.TXT_RELATOR = RE.NOM_RELATOR "+
			" and R.cod_tipo_solic IN (" + myresult.getTpreq()+ ") "+			
			" AND R.cod_status_requerimento >=3 "+ 
			" AND R.COD_UF = '"+myresult.getCodEstado()+"'"+
			" GROUP BY J.SIG_JUNTA,R.TXT_ARTIGO,R.TXT_RELATOR"; 
			rs = stmt.executeQuery(sCmd);
			while(rs.next())
			{
				String sArtigo = rs.getString("TXT_ARTIGO");					
				String sJunta = rs.getString("SIG_JUNTA");
				String sRelator = rs.getString("TXT_RELATOR");
				String sDeferidos = rs.getString("DEFERIDOS");
				String sIndeferidos = rs.getString("INDEFERIDOS");					
				String sDefPerc = rs.getString("DEFPERC");
				String sIndefPerc = rs.getString("INDEFPERC");
				
				//Preenche o quadro da junta lida
				Iterator it = myresult.getResultado().iterator() ;
				GER.ResultadoBean dadosRelat = new GER.ResultadoBean();
				while (it.hasNext()) 
				{
					dadosRelat = (GER.ResultadoBean)it.next();
					if(dadosRelat.getSigJunta().equals(sJunta))
					{
						//Preenche cada linha da lista de artigos
						Iterator itx = dadosRelat.getArtigos().iterator() ;
						GER.ResultadoBean artigos = new	GER.ResultadoBean();	
						ArrayList listArtigos = new ArrayList();
						while (itx.hasNext()) 
						{
							artigos = (GER.ResultadoBean)itx.next();		
							if(artigos.getTxtArtigo().equals(sArtigo))
							{
								//Indentifica em que posicao o membro ocupa (se membro 1, 2, ...)
								artigos.setNomRelator(sRelator);
								artigos.setJuntaRelator(myresult.getJuntaRelator());
								artigos.identDadosMembros(artigos);
								if(artigos.getCodRelator().equals("1"))
								{
									artigos.setDefMembro1(sDeferidos);
									artigos.setInDefMembro1(sIndeferidos);
								}
								else if(artigos.getCodRelator().equals("2"))
								{
									artigos.setDefMembro2(sDeferidos);
									artigos.setInDefMembro2(sIndeferidos);
								}
								else if(artigos.getCodRelator().equals("3"))
								{
									artigos.setDefMembro3(sDeferidos);
									artigos.setInDefMembro3(sIndeferidos);
								}
								else if(artigos.getCodRelator().equals("4"))
								{
									artigos.setDefMembro4(sDeferidos);
									artigos.setInDefMembro4(sIndeferidos);
								}   
								artigos.setLinhaPreenchida("S");
							}
							listArtigos.add(artigos);
						}
						dadosRelat.setArtigos(listArtigos);
						resultadoFinal.add(dadosRelat);
					}
				}
				
			}
			myresult.setResultado(resultadoFinal);
			
			//Calcula total de deferido/ indeferido
			Iterator ity = myresult.getResultado().iterator() ;
			GER.ResultadoBean ResultTotalParcial = new GER.ResultadoBean();
			ArrayList resultadoTotal = new ArrayList();
			while (ity.hasNext()) 
			{
				ResultTotalParcial = (GER.ResultadoBean)ity.next();
				//Preenche cada linha da lista de artigos
				Iterator itx = ResultTotalParcial.getArtigos().iterator() ;
				GER.ResultadoBean artigos = new	GER.ResultadoBean();	
				ArrayList listArtigos = new ArrayList();
				while (itx.hasNext()) 
				{
					artigos = (GER.ResultadoBean)itx.next();		
					if ( (artigos.getLinhaPreenchida().equals("S")) && (!artigos.getTxtArtigo().equals("TOTAL")) ) 
					{
						if (!artigos.getDefMembro1().equals(""))
						{
							quantProcDef = quantProcDef.add(new BigDecimal (artigos.getDefMembro1().trim()));
							percProcDef=percProcDef.add(new BigDecimal(artigos.getDefMembro1().trim()));
							percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						if (!artigos.getInDefMembro1().equals(""))
						{
							quantProcInDef = quantProcInDef.add(new BigDecimal(artigos.getInDefMembro1().trim()));
							percProcInDef=percProcInDef.add(new BigDecimal(artigos.getInDefMembro1().trim()));
							percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
							
						}
						if (!artigos.getDefMembro2().equals(""))
						{
							quantProcDef = quantProcDef.add(new BigDecimal (artigos.getDefMembro2().trim()));
							percProcDef=percProcDef.add(new BigDecimal(artigos.getDefMembro2().trim()));
							percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						if (!artigos.getInDefMembro2().equals(""))
						{
							quantProcInDef = quantProcInDef.add(new BigDecimal(artigos.getInDefMembro2().trim()));
							percProcInDef=percProcInDef.add(new BigDecimal(artigos.getInDefMembro2().trim()));
							percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
							
						}
						if (!artigos.getDefMembro3().equals(""))
						{
							quantProcDef = quantProcDef.add(new BigDecimal (artigos.getDefMembro3().trim()));
							percProcDef=percProcDef.add(new BigDecimal(artigos.getDefMembro3().trim()));
							percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						if (!artigos.getInDefMembro3().equals(""))
						{
							quantProcInDef = quantProcInDef.add(new BigDecimal(artigos.getInDefMembro3().trim()));
							percProcInDef=percProcInDef.add(new BigDecimal(artigos.getInDefMembro3().trim()));
							percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
							
						}
						if (!artigos.getDefMembro4().equals(""))
						{
							quantProcDef = quantProcDef.add(new BigDecimal (artigos.getDefMembro4().trim()));
							percProcDef=percProcDef.add(new BigDecimal(artigos.getDefMembro4().trim()));
							percProcDef=percProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						if (!artigos.getInDefMembro4().equals(""))
						{
							quantProcInDef = quantProcInDef.add(new BigDecimal(artigos.getInDefMembro4().trim()));
							percProcInDef=percProcInDef.add(new BigDecimal(artigos.getInDefMembro4().trim()));
							percProcInDef=percProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
							
						}
						
						//Total de Deferido
						String totdef = String.valueOf(quantProcDef);
						long totdefer = Long.parseLong(totdef);
						if(totdefer > 0)
							artigos.setTotalDef(String.valueOf(totdefer));
						else artigos.setTotalDef("");
						
						//Total de Indeferido
						String totindef = String.valueOf(quantProcInDef);
						long totindefer = Long.parseLong(totindef);
						if (totindefer > 0)
							artigos.setTotalInDef(String.valueOf(totindefer));
						else artigos.setTotalInDef("");
						
						//Total Geral 
						totgeral=quantProcDef.add(quantProcInDef);
						String totger= String.valueOf(totgeral);
						long totgera=Long.parseLong(totger);
						if(totgera > 0)
							artigos.setTotalGer(String.valueOf(totgera));
						else 
							artigos.setTotalGer("");
						
						//Porcentagem Deferidos
						defpercent=(quantProcDef.multiply(new BigDecimal((double)100)));
						defpercent=defpercent.setScale(2,1);	
						if (!artigos.getTotalGer().equals(""))
						{
							defpercent=defpercent.divide(totgeral,BigDecimal.ROUND_CEILING);		
							artigos.setTotalDefPerc(String.valueOf(defpercent));
						}else artigos.setTotalDefPerc("");
						
						//Porcentagem indeferidos		
						Indefpercent=(quantProcInDef.multiply(new BigDecimal((double)100)));
						Indefpercent=Indefpercent.setScale(2,1);
						if (!artigos.getTotalGer().equals(""))
						{
							Indefpercent=Indefpercent.divide(totgeral,BigDecimal.ROUND_CEILING);
							artigos.setTotalInDefPerc(String.valueOf(Indefpercent));
						}else artigos.setTotalInDefPerc("");
						
						//Limpa Vari�veis
						if (!ultimoArtigo.equals(artigos.getTxtArtigo()))		
						{
							quantProcInDef = new BigDecimal((double)0);
							quantProcDef = new BigDecimal((double)0);
							percProcInDef = new BigDecimal(0);
							percProcDef = new BigDecimal(0);
						}
						ultimoArtigo = artigos.getTxtArtigo();	
					}
					listArtigos.add(artigos);
				}
				ResultTotalParcial.setArtigos(listArtigos);
				resultadoTotal.add(ResultTotalParcial);
			}
			myresult.setResultado(resultadoTotal);
			
			//Monta a linha Total de cada Junta 
			
			//total geral por membros, separados por deferido e indeferido
			BigDecimal totalquantProcDefMem1 = new BigDecimal((double)0);
			BigDecimal totalquantProcInDefMem1 = new BigDecimal((double)0);
			BigDecimal totalquantProcDefMem2 = new BigDecimal((double)0);
			BigDecimal totalquantProcInDefMem2 = new BigDecimal((double)0);
			BigDecimal totalquantProcDefMem3 = new BigDecimal((double)0);
			BigDecimal totalquantProcInDefMem3 = new BigDecimal((double)0);
			BigDecimal totalquantProcDefMem4 = new BigDecimal((double)0);
			BigDecimal totalquantProcInDefMem4 = new BigDecimal((double)0);
			
			//total geral de processos deferidos e indeferidos
			BigDecimal totalGeralProcDef = new BigDecimal((double)0);
			BigDecimal totalGeralProcInDef = new BigDecimal((double)0);
			
			//Total geral de percentuais de deferidos e indeferidos
			BigDecimal totalPercProcDef = new BigDecimal((double)0);
			BigDecimal totalPercProcInDef = new BigDecimal((double)0);
			
			//Total Geral de deferidos + indeferidos
			BigDecimal totalGeral = new BigDecimal((double)0);
			
			//Total geral de percentuais de deferidos + indeferidos
			BigDecimal totalPerc = new BigDecimal((double)0);
			
			//Percorre a lista ja montada para preencher a linha de Totais Gerais
			Iterator it = myresult.getResultado().iterator() ;
			GER.ResultadoBean junta = new GER.ResultadoBean();
			ArrayList resultadoTotalFinal = new ArrayList();
			while (it.hasNext()) 
			{
				junta = (GER.ResultadoBean)it.next();
				Iterator itx = junta.getArtigos().iterator() ;
				GER.ResultadoBean artigos = new GER.ResultadoBean();
				while (itx.hasNext()) 
				{
					artigos = (GER.ResultadoBean)itx.next();
					if ( (artigos.getLinhaPreenchida().equals("S")) && (!artigos.getTxtArtigo().equals("TOTAL")) ) 
					{
						if (!artigos.getDefMembro1().equals(""))
							totalquantProcDefMem1 = totalquantProcDefMem1.add(new BigDecimal (artigos.getDefMembro1().trim()));
						if (!artigos.getInDefMembro1().equals(""))
							totalquantProcInDefMem1 = totalquantProcInDefMem1.add(new BigDecimal(artigos.getInDefMembro1().trim()));
						if (!artigos.getDefMembro2().equals(""))
							totalquantProcDefMem2 = totalquantProcDefMem2.add(new BigDecimal (artigos.getDefMembro2().trim()));
						if (!artigos.getInDefMembro2().equals(""))
							totalquantProcInDefMem2 = totalquantProcInDefMem2.add(new BigDecimal(artigos.getInDefMembro2().trim()));
						if (!artigos.getDefMembro3().equals(""))
							totalquantProcDefMem3 = totalquantProcDefMem3.add(new BigDecimal (artigos.getDefMembro3().trim()));
						if (!artigos.getInDefMembro3().equals(""))
							totalquantProcInDefMem3 = totalquantProcInDefMem3.add(new BigDecimal(artigos.getInDefMembro3().trim()));
						if (!artigos.getDefMembro4().equals(""))
							totalquantProcDefMem4 = totalquantProcDefMem4.add(new BigDecimal (artigos.getDefMembro4().trim()));
						if (!artigos.getInDefMembro4().equals(""))
							totalquantProcInDefMem4 = totalquantProcInDefMem4.add(new BigDecimal(artigos.getInDefMembro4().trim()));
						
						if (!artigos.getTotalDef().equals(""))
							totalGeralProcDef = totalGeralProcDef.add(new BigDecimal(artigos.getTotalDef().trim()));
						if (!artigos.getTotalInDef().equals(""))
							totalGeralProcInDef = totalGeralProcInDef.add(new BigDecimal(artigos.getTotalInDef().trim()));
						
						if (!artigos.getTotalDefPerc().equals(""))
						{
							totalPercProcDef = totalPercProcDef.add(new BigDecimal(artigos.getTotalDefPerc().trim()));
							totalPercProcDef = totalPercProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						
						if (!artigos.getTotalInDefPerc().equals(""))
						{
							totalPercProcInDef = totalPercProcInDef.add(new BigDecimal(artigos.getTotalInDefPerc().trim()));
							totalPercProcInDef = totalPercProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
						
						if (!artigos.getTotalGer().equals(""))
							totalGeral = totalGeral.add(new BigDecimal (artigos.getTotalGer().trim()));
						
						if (!artigos.getTotalMedia().equals(""))
						{
							totalPerc = totalPerc.add(new BigDecimal(artigos.getTotalMedia().trim()));
							totalPerc = totalPerc.setScale(2,BigDecimal.ROUND_HALF_DOWN);
						}
					}
				}
				
				Iterator itz = junta.getArtigos().iterator() ;
				GER.ResultadoBean artigosTotal = new GER.ResultadoBean();
				ArrayList totalArtigo = new ArrayList();
				while (itz.hasNext()) 
				{
					artigosTotal = (GER.ResultadoBean)itz.next();
					if(artigosTotal.getTxtArtigo().equals("TOTAL"))
					{
						artigosTotal.setSigJunta(junta.getSigJunta());
						artigosTotal.setDefMembro1(String.valueOf(totalquantProcDefMem1));
						artigosTotal.setInDefMembro1(String.valueOf(totalquantProcInDefMem1));
						artigosTotal.setDefMembro2(String.valueOf(totalquantProcDefMem2));
						artigosTotal.setInDefMembro2(String.valueOf(totalquantProcInDefMem2));
						artigosTotal.setDefMembro3(String.valueOf(totalquantProcDefMem3));
						artigosTotal.setInDefMembro3(String.valueOf(totalquantProcInDefMem3));
						artigosTotal.setDefMembro4(String.valueOf(totalquantProcDefMem4));
						artigosTotal.setInDefMembro4(String.valueOf(totalquantProcInDefMem4));
						
						artigosTotal.setTotalDef(String.valueOf(totalGeralProcDef));
						artigosTotal.setTotalInDef(String.valueOf(totalGeralProcInDef));
						
						//Pecentual Deferidos e Indeferidos [Formula:totalDeferidoLinha/totalGeral*100]
						BigDecimal totalPercentDef = new BigDecimal(artigosTotal.getTotalDef());
						totalPercentDef = totalPercentDef.setScale(2,1);
						totalPercentDef = totalPercentDef.divide(new BigDecimal (Double.parseDouble(totalGeral.toString())),BigDecimal.ROUND_HALF_DOWN);
						totalPercentDef = totalPercentDef.multiply(new BigDecimal((double)100));
						String totPercentDef = String.valueOf(totalPercentDef);
						totPercentDef=sys.Util.ComEdt(sys.Util.SemEdt(totPercentDef,'.'),15,2).replace(',','.');
						artigosTotal.setTotalDefPerc(totPercentDef);
						
						BigDecimal totalPercentInDef = new BigDecimal(artigosTotal.getTotalInDef());
						totalPercentInDef = totalPercentInDef.setScale(2,1);
						totalPercentInDef = totalPercentInDef.divide(new BigDecimal (Double.parseDouble(totalGeral.toString())),BigDecimal.ROUND_HALF_DOWN);
						totalPercentInDef = totalPercentInDef.multiply(new BigDecimal((double)100));
						String totPercentInDef = String.valueOf(totalPercentInDef);
						totPercentInDef=sys.Util.ComEdt(sys.Util.SemEdt(totPercentInDef,'.'),15,2).replace(',','.');
						artigosTotal.setTotalInDefPerc(totPercentInDef);
						
						artigosTotal.setTotalGer(String.valueOf(totalGeral));
						artigosTotal.setTotalMedia("100.00");
					}
					else if (artigosTotal.getLinhaPreenchida().equals("S"))
					{
						//M�dias [Media = (totalGeralLinha/TotalGeral)*100]
						if(!artigosTotal.getTotalGer().equals(""))
						{
							totmedia = new BigDecimal(artigosTotal.getTotalGer());
							
							totmedia = totmedia.setScale(4);
							totmedia = totmedia.divide(new BigDecimal (Double.parseDouble(totalGeral.toString())),BigDecimal.ROUND_HALF_DOWN);
							totmedia = totmedia.multiply(new BigDecimal((double)100));
							totmedia = totmedia.setScale(2);
							String media=String.valueOf(totmedia);
							media=sys.Util.ComEdt(sys.Util.SemEdt(media,'.'),15,2).replace(',','.');
							artigosTotal.setTotalMedia(media);
						}
						else 
							artigosTotal.setTotalMedia("");
					}
					
					totalArtigo.add(artigosTotal);
				}
				totalquantProcDefMem1 = new BigDecimal((double)0);
				totalquantProcInDefMem1 = new BigDecimal((double)0);
				totalquantProcDefMem2 = new BigDecimal((double)0);
				totalquantProcInDefMem2 = new BigDecimal((double)0);
				totalquantProcDefMem3 = new BigDecimal((double)0);
				totalquantProcInDefMem3 = new BigDecimal((double)0);
				totalquantProcDefMem4 = new BigDecimal((double)0);
				totalquantProcInDefMem4 = new BigDecimal((double)0);
				
				//total geral de processos deferidos e indeferidos
				totalGeralProcDef = new BigDecimal((double)0);
				totalGeralProcInDef = new BigDecimal((double)0);
				
				//Total geral de percentuais de deferidos e indeferidos
				totalPercProcDef = new BigDecimal((double)0);
				totalPercProcInDef = new BigDecimal((double)0);
				
				//Total Geral de deferidos + indeferidos
				totalGeral = new BigDecimal((double)0);
				
				//Total geral de percentuais de deferidos + indeferidos
				totalPerc = new BigDecimal((double)0);
				
				junta.setArtigos(totalArtigo);
				resultadoTotalFinal.add(junta);
			}
			myresult.setResultado(resultadoTotalFinal);
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
			
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}finally{
			try{
				serviceloc.setReleaseConnection(conn);	
			}catch(Exception e){}
		}
	}	
	
	public void montaEstruturaRelatorio(ResultadoBean resultadoBean) throws DaoException {
		Connection conn =null ;	
		ArrayList junta = new ArrayList();
		
		boolean retorno = true;
		ResultSet rs1 = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt1 = conn.createStatement();
			
			String sCmd = "SELECT J.COD_JUNTA,J.NOM_JUNTA,J.SIG_JUNTA,J.IND_TIPO," +
			" O.SIG_ORGAO "+
			" FROM TSMI_JUNTA J, TSMI_RELATOR R, TSMI_ORGAO O "+
			" WHERE J.COD_ORGAO = ? AND "+
			" J.IND_TIPO = ? " +
			" AND J.COD_JUNTA = R.COD_JUNTA " +
			" AND J.COD_ORGAO = O.COD_ORGAO ORDER BY J.NOM_JUNTA";
			PreparedStatement stm = conn.prepareStatement(sCmd);
			stm.setInt(1, Integer.parseInt(resultadoBean.getOrgao()));
			stm.setInt(2, Integer.parseInt(resultadoBean.getTipoJunta()));
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				ResultadoBean myResult = new ResultadoBean() ;
				myResult.setSigJunta(rs.getString("SIG_JUNTA"));
				myResult.setSigOrgao(rs.getString("SIG_ORGAO"));
				
				String sCmd2 = "SELECT * FROM TSMI_INFRACAO ORDER BY DSC_ENQUADRAMENTO";
				rs1 = stmt1.executeQuery(sCmd2);
				ArrayList artigos = new ArrayList();
				while(rs1.next()) {
					ResultadoBean myArtigos = new ResultadoBean() ;
					myArtigos.setTxtArtigo(rs1.getString("dsc_enquadramento"));
					artigos.add(myArtigos);
				}
				ResultadoBean myArtigos = new ResultadoBean() ;
				myArtigos.setTxtArtigo("TOTAL");
				myArtigos.setLinhaPreenchida("S");
				artigos.add(myArtigos);	  				
				myResult.setArtigos(artigos);
				junta.add(myResult);
			}
			resultadoBean.setResultado(junta);
			rs.close();
			rs1.close();
		}//fim do try
		catch (Exception e) {
			retorno = false;
		}//fim do catch
		finally {
			if (conn != null) {
				try {serviceloc.setReleaseConnection(conn);}
				catch (Exception ey) { }
			}
		}
	}
	
	
	public void montaEstrutRelatConsolidacao(ResultadoBean resultadoBean) throws DaoException {
		Connection conn =null ;	
		ArrayList resultado = new ArrayList();
		boolean retorno = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT * FROM TSMI_INFRACAO ORDER BY DSC_ENQUADRAMENTO";
			ResultSet rs =  stmt.executeQuery(sCmd);
			ArrayList artigos = new ArrayList();
			while(rs.next()) {
				ResultadoBean myArtigos = new ResultadoBean() ;
				myArtigos.setTxtArtigo(rs.getString("dsc_enquadramento"));
				artigos.add(myArtigos);
			}
			ResultadoBean myArtigos = new ResultadoBean() ;
			myArtigos.setTxtArtigo("TOTAL");
			myArtigos.setLinhaPreenchida("S");
			artigos.add(myArtigos);	  				
			resultadoBean.setArtigos(artigos);
			rs.close();
		}//fim do try
		catch (Exception e) {
			retorno = false;
		}//fim do catch
		finally {
			if (conn != null) {
				try {serviceloc.setReleaseConnection(conn);}
				catch (Exception ey) { }
			}
		}
	}
	
	public void mostraConsolidacaoResultado(ResultadoBean myresult) throws DaoException
	{
		Connection conn = null;
		ResultSet rs = null;
		String sCmd = "";
		String ultimoArtigo = "";
		
		int quantDef = 0;
		int quantInDef = 0; 
		BigDecimal quantProcInDef = new BigDecimal((double)0);
		BigDecimal quantProcDef = new BigDecimal((double)0);
		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd =  "SELECT SIG_ORGAO FROM TSMI_ORGAO WHERE COD_ORGAO = ? ";		    
			PreparedStatement stm = conn.prepareStatement(sCmd);    
			stm.setInt(1, Integer.parseInt(myresult.getOrgao()));
			rs = stm.executeQuery();
			while (rs.next()){
				myresult.setSigOrgao(rs.getString("SIG_ORGAO"));
			}
			
			sCmd = "select R.TXT_ARTIGO,I.VAL_REAL,SUM(NVL(R.QTD_DEFERIDOS,0)) DEFERIDOS,"+ 
			" SUM(NVL(R.QTD_INDEFERIDOS,0)) INDEFERIDOS, "+
			" SUM((NVL(R.QTD_DEFERIDOS,0))+(NVL(R.QTD_INDEFERIDOS,0))) TOTALANALISADOS,"+ 
			" SUM((NVL(R.QTD_DEFERIDOS,0))+(NVL(R.QTD_INDEFERIDOS,0))) TOTALGERAL, "+
			" round((sum(R.QTD_DEFERIDOS))*100/(sum(R.QTD_DEFERIDOS) + sum(R.QTD_INDEFERIDOS)),2) DefPerc, "+
			" round((sum(R.QTD_INDEFERIDOS))*100/(sum(R.QTD_DEFERIDOS) +sum(R.QTD_INDEFERIDOS)),2) IndefPerc,"+
			" SUM(R.QTD_DISTRIB_RESULT) DISTRIBUIDOS FROM tger_req_distr_result R, tsmi_infracao I "+
			" WHERE to_char(R.DAT_DISTRIB,'mm/yyyy')='"+myresult.getMesAno()+"'"+  
			" AND trim(I.DSC_ENQUADRAMENTO) = trim(R.TXT_ARTIGO) "+
			" and R.cod_tipo_solic IN (" + myresult.getTpreq()+ ") "+			
			" AND R.cod_status_requerimento >=3 "+ 
			" AND R.COD_UF = '"+myresult.getCodEstado()+"'"+
			" GROUP BY R.TXT_ARTIGO,I.VAL_REAL"; 
			rs = stmt.executeQuery(sCmd);
			while(rs.next())
			{
				String sArtigo = rs.getString("TXT_ARTIGO");					
				String sValReal = rs.getString("VAL_REAL");
				String sDeferidos = rs.getString("DEFERIDOS");
				String sIndeferidos = rs.getString("INDEFERIDOS");					
				String sDefPerc = rs.getString("DEFPERC");
				String sIndefPerc = rs.getString("INDEFPERC");
				String sDistribuido = rs.getString("DISTRIBUIDOS");
				String sTotalGeral = rs.getString("TOTALGERAL");
				
				//Preenche cada linha da lista de artigos
				Iterator itx = myresult.getArtigos().iterator() ;
				GER.ResultadoBean artigos = new	GER.ResultadoBean();	
				ArrayList listArtigos = new ArrayList();
				while (itx.hasNext()) 
				{
					artigos = (GER.ResultadoBean)itx.next();		
					if(artigos.getTxtArtigo().equals(sArtigo))
					{
						artigos.setValUnitario(sValReal);
						artigos.setDeferidos(sDeferidos);
						artigos.setIndeferidos(sIndeferidos);
						//incrementa total de deferimento/ Indeferimento
						if  (Long.parseLong(sDeferidos)>0)
							quantDef += 1;
						if	(Long.parseLong(sIndeferidos)>0)
							quantInDef += 1;
						
						//Soma a quantidade de deferidos e indeferidos
						quantProcInDef=quantProcInDef.add(new BigDecimal (sIndeferidos));
						quantProcDef=quantProcDef.add(new BigDecimal(sDeferidos));
						
						//Total Geral 
						artigos.setTotalGer(sTotalGeral);
						
						//Porcentagem Deferidos/indeferidos
						artigos.setTotalDefPerc(sDefPerc);
						artigos.setTotalInDefPerc(sIndefPerc);
						
						//Total Distribuido
						artigos.setProcDistribuido(sDistribuido);
						
						//Total Processos
						BigDecimal totalGeral = new BigDecimal(sTotalGeral);
						BigDecimal totalProc = totalGeral.add(new BigDecimal(sDistribuido));
						artigos.setTotalProc(String.valueOf(totalProc));
						
						//Valor total [ValorUnitatio * TotalIndeferidos]
						BigDecimal valorUnitario = new BigDecimal(sValReal);
						BigDecimal valorTotal =(valorUnitario.multiply(new BigDecimal(Double.parseDouble(sIndeferidos))));
						artigos.setValTotal(String.valueOf(valorTotal));
						
						//Flag para linha preenchida
						artigos.setLinhaPreenchida("S");
						
					}
					listArtigos.add(artigos);
					//Limpa Vari�veis
					if (!ultimoArtigo.equals(sArtigo))
					{
						quantProcInDef = new BigDecimal((double)0);
						quantProcDef = new BigDecimal((double)0);
						quantDef = 0;
						quantInDef = 0;
					}
					if (artigos.getTxtArtigo().equals(sArtigo))
						ultimoArtigo = sArtigo;
				}
				myresult.setArtigos(listArtigos);
			}
			
			/* Monta a linha Total de cada Junta */
			
			//total geral de processos deferidos e indeferidos
			BigDecimal totalGeralProcDef = new BigDecimal((double)0);
			BigDecimal totalGeralProcInDef = new BigDecimal((double)0);
			
			//Total geral de percentuais de deferidos e indeferidos
			BigDecimal totalPercProcDef = new BigDecimal((double)0);
			BigDecimal totalPercProcInDef = new BigDecimal((double)0);
			
			//Total Geral de deferidos + indeferidos
			BigDecimal totalGeral = new BigDecimal((double)0);
			
			//Total geral de percentuais de deferidos + indeferidos
			BigDecimal totalPerc = new BigDecimal((double)0);
			
			//Total de Processos Distribuidos
			BigDecimal totalDistribuido = new BigDecimal((double)0);
			
			//Valor Total das infracoes
			BigDecimal totalValor = new BigDecimal((double)0);
			
			//Percorre a lista ja montada para preencher a linha de Totais Gerais
			Iterator itx = myresult.getArtigos().iterator() ;
			GER.ResultadoBean artigos = new GER.ResultadoBean();
			while (itx.hasNext()) 
			{  
				artigos = (GER.ResultadoBean)itx.next();
				if ( (artigos.getLinhaPreenchida().equals("S")) && (!artigos.getTxtArtigo().equals("TOTAL")) ) 
				{
					if (!artigos.getDeferidos().equals(""))
						totalGeralProcDef = totalGeralProcDef.add(new BigDecimal(artigos.getDeferidos().trim()));
					if (!artigos.getIndeferidos().equals(""))
						totalGeralProcInDef = totalGeralProcInDef.add(new BigDecimal(artigos.getIndeferidos().trim()));
					
					if (!artigos.getTotalDefPerc().equals(""))
					{
						totalPercProcDef = totalPercProcDef.add(new BigDecimal(artigos.getTotalDefPerc().trim()));
						totalPercProcDef = totalPercProcDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
					}
					
					if (!artigos.getTotalInDefPerc().equals(""))
					{
						totalPercProcInDef = totalPercProcInDef.add(new BigDecimal(artigos.getTotalInDefPerc().trim()));
						totalPercProcInDef = totalPercProcInDef.setScale(2,BigDecimal.ROUND_HALF_DOWN);
					}
					
					if (!artigos.getTotalGer().equals(""))
						totalGeral = totalGeral.add(new BigDecimal (Double.parseDouble(artigos.getTotalGer().trim())));
					
					if(!artigos.getProcDistribuido().equals(""))
						totalDistribuido = totalDistribuido.add(new BigDecimal (artigos.getProcDistribuido()));
					
					if(!artigos.getValTotal().equals(""))
						totalValor = totalValor.add(new BigDecimal (artigos.getValTotal()));
					
					if (!artigos.getTotalMedia().equals(""))
					{
						totalPerc = totalPerc.add(new BigDecimal(artigos.getTotalMedia().trim()));
						totalPerc = totalPerc.setScale(2,BigDecimal.ROUND_HALF_DOWN);
					}
				}
			}
			
			Iterator ity = myresult.getArtigos().iterator() ;
			GER.ResultadoBean artigosTotal = new GER.ResultadoBean();
			ArrayList totalArtigo = new ArrayList();
			while (ity.hasNext()) 
			{
				artigosTotal = (GER.ResultadoBean)ity.next();
				if(artigosTotal.getTxtArtigo().equals("TOTAL"))
				{
					artigosTotal.setDeferidos(String.valueOf(totalGeralProcDef));
					artigosTotal.setIndeferidos(String.valueOf(totalGeralProcInDef));
					
					//Pecentual Deferidos e Indeferidos [Formula:totalDeferidoLinha/totalGeral*100]
					BigDecimal totalPercentDef = new BigDecimal(artigosTotal.getDeferidos());
					totalPercentDef = totalPercentDef.setScale(2,1);
					totalPercentDef = totalPercentDef.divide(totalGeral,BigDecimal.ROUND_CEILING);
					totalPercentDef = totalPercentDef.multiply(new BigDecimal((double)100));
					String totPercentDef = String.valueOf(totalPercentDef);
					totPercentDef=sys.Util.ComEdt(sys.Util.SemEdt(totPercentDef,'.'),15,2).replace(',','.');
					artigosTotal.setTotalDefPerc(totPercentDef);
					
					BigDecimal totalPercentInDef = new BigDecimal(artigosTotal.getIndeferidos());
					totalPercentInDef = totalPercentInDef.setScale(2,1);
					totalPercentInDef = totalPercentInDef.divide(totalGeral,BigDecimal.ROUND_CEILING);
					totalPercentInDef = totalPercentInDef.multiply(new BigDecimal((double)100));
					String totPercentInDef = String.valueOf(totalPercentInDef);
					totPercentInDef=sys.Util.ComEdt(sys.Util.SemEdt(totPercentInDef,'.'),15,2).replace(',','.');
					artigosTotal.setTotalInDefPerc(totPercentInDef);
					
					//Total Processos
					BigDecimal totalProc = totalGeral.add(totalDistribuido);
					artigos.setTotalProc(String.valueOf(totalProc));
					
					//Total de Processos Distribuidos
					artigos.setProcDistribuido(String.valueOf(totalDistribuido));
					
					//Total Geral
					artigosTotal.setTotalGer(String.valueOf(totalGeral));
					
					//Media Geral 
					artigosTotal.setTotalMedia("100.00");
					artigosTotal.setTotalMediaDistribuidos("100.00");
					artigosTotal.setTotalMediaProc("100.00");
					
					//Valor total [ValorUnitatio * TotalIndeferidos]
					artigos.setValTotal(String.valueOf(totalValor));
					artigos.setValUnitario("-");
				}
				else if (artigosTotal.getLinhaPreenchida().equals("S"))
				{
					//M�dias [Media = (totalGeralLinha/TotalGeral)*100]
					BigDecimal totmedia = new BigDecimal(artigosTotal.getTotalGer());
					totmedia = totmedia.setScale(4);
					totmedia = totmedia.divide(totalGeral,BigDecimal.ROUND_HALF_DOWN);
					totmedia = totmedia.multiply(new BigDecimal((double)100));
					totmedia = totmedia.setScale(2);
					String media=String.valueOf(totmedia);
					media=sys.Util.ComEdt(sys.Util.SemEdt(media,'.'),15,2).replace(',','.');
					artigosTotal.setTotalMedia(media);
					
					//M�dias de Processos Distribuidos [Media = (Proc. Distribuidos/Total Proc. Distibuidos)*100]
					BigDecimal totmediaProcDistribuidos = new BigDecimal(artigosTotal.getProcDistribuido());
					totmediaProcDistribuidos = totmediaProcDistribuidos.setScale(4);
					totmediaProcDistribuidos = totmediaProcDistribuidos.divide(totalDistribuido,BigDecimal.ROUND_HALF_DOWN);
					totmediaProcDistribuidos = totmediaProcDistribuidos.multiply(new BigDecimal((double)100));
					totmediaProcDistribuidos = totmediaProcDistribuidos.setScale(2);
					String mediaProcDistribuido = String.valueOf(totmediaProcDistribuidos);
					mediaProcDistribuido = sys.Util.ComEdt(sys.Util.SemEdt(media,'.'),15,2).replace(',','.');
					artigosTotal.setTotalMediaDistribuidos(mediaProcDistribuido);
					
					//M�dias de Processos [Media = (Proc. /Total Proc.)*100]
					BigDecimal totalProc = totalGeral.add(totalDistribuido);
					BigDecimal totmediaProc = new BigDecimal(artigosTotal.getTotalProc());
					totmediaProc = totmediaProc.setScale(4);
					totmediaProc = totmediaProc.divide(totalProc,BigDecimal.ROUND_HALF_DOWN);
					totmediaProc = totmediaProc.multiply(new BigDecimal((double)100));
					totmediaProc = totmediaProc.setScale(2);
					String mediaProc = String.valueOf(totmediaProc);
					mediaProc = sys.Util.ComEdt(sys.Util.SemEdt(media,'.'),15,2).replace(',','.');
					artigosTotal.setTotalMediaProc(mediaProc);
				}
				
				totalArtigo.add(artigosTotal);
			}
			myresult.setArtigos(totalArtigo);
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			throw new DaoException(ex.getMessage());
		}finally{
			try{
				serviceloc.setReleaseConnection(conn);	
			}catch(Exception e){}
		}
	}
	
	
	public void DistribuicaoProcesso(GraficoBean myGrf)throws DaoException{
		
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
//			PEGAR DADOS DO ORGAO
			ResultSet rs = null; 
			
			if(myGrf.getTodosOrgaos().equals("N")){
				sCmd =  "SELECT SIG_ORGAO ";
				sCmd += "FROM TSMI_ORGAO ";
				sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";		    
				rs  = stmt.executeQuery(sCmd);		    
				while (rs.next()){
					myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
				}
			}else{
				myGrf.setSiglaOrgao("TODOS");
			}
			int dadosProcesso[][] = new int[8][12];
			
			String TPreq = null;
			if ("0".equals(myGrf.getTipoReq())){     
				TPreq = "'DP','DC'";
			}else if("1".equals(myGrf.getTipoReq())){ 
				TPreq = "'1P','1C'";  
			}else{
				TPreq = "'2P','2C'";
			}
			
			sCmd = "select DAT_DISTRIB, SUM(NVL(QTD_DEFERIDOS,0)) DEFERIDOS,"+
			"SUM(NVL(E.QTD_ENTRADA,0)) ENTRADAS,"+
			"SUM(NVL(QTD_INDEFERIDOS,0)) INDEFERIDOS,"+
			"SUM((NVL(QTD_DEFERIDOS,0))+(NVL(QTD_INDEFERIDOS,0))) TOTALANALISADOS,"+
			"SUM(NVL(QTD_DISTRIB_RESULT,0)) NAODISTRIBUIDOS,"+ 
			"SUM((NVL(QTD_DEFERIDOS,0))+(NVL(QTD_INDEFERIDOS,0))-(NVL(QTD_DISTRIB_RESULT,0))) TOTALGERAL, "+
			"SUM((NVL(QTD_DEFERIDOS,0))+(NVL(QTD_INDEFERIDOS,0)))-SUM(NVL(QTD_DISTRIB_RESULT,0)) DIFERENCA_ANALISE, "+
			"SUM(NVL(QTD_DEFERIDOS,0)) ESTOQUE "+			
			" FROM tger_req_distr_result R, tger_req_entrada E"+
			" WHERE DAT_DISTRIB >= to_date('01/"+myGrf.getDatInicio()+"','dd/mm/yyyy')"+
			" AND R.cod_tipo_solic IN (" + TPreq+ ") ";	
			if("04,06,09,11".indexOf(myGrf.getDatFim().substring(0,2))>=0)
				sCmd +=" AND DAT_DISTRIB <= to_date('30/"+myGrf.getDatFim()+"','dd/mm/yyyy')";
			else if("02".indexOf(myGrf.getDatFim().substring(0,2))>=0)
				sCmd +=" AND DAT_DISTRIB <= to_date('29/"+myGrf.getDatFim()+"','dd/mm/yyyy')";
			else
				sCmd +=" AND DAT_DISTRIB <= to_date('31/"+myGrf.getDatFim()+"','dd/mm/yyyy')";
			sCmd+=" AND cod_status_requerimento >=2 "+
			" AND R.COD_UF = '"+myGrf.getCodUF()+"'"+
			" AND R.COD_UF = E.COD_UF "+
			" AND R.COD_ORGAO = E.COD_ORGAO "+
			" AND R.COD_TIPO_SOLIC = E.COD_TIPO_SOLIC "+
			" AND trunc(R.DAT_DISTRIB) = trunc(E.DAT_ENTRADA) "+
			" GROUP BY DAT_DISTRIB";
			
			rs = stmt.executeQuery(sCmd);
			int i = 0;
			int iIndiceSaldo=0;
			boolean bSaldo=true;
			
			while(rs.next()){
				String sData = converteData(rs.getDate("DAT_DISTRIB"));
				
				i = Integer.parseInt(sData.substring(3,5))-1;
				dadosProcesso[0][i] += Integer.parseInt(rs.getString("DEFERIDOS"));
				dadosProcesso[1][i] += Integer.parseInt(rs.getString("INDEFERIDOS"));
				dadosProcesso[2][i] += Integer.parseInt(rs.getString("TOTALANALISADOS"));
				dadosProcesso[3][i] += Integer.parseInt(rs.getString("NAODISTRIBUIDOS"));
				dadosProcesso[4][i] += Integer.parseInt(rs.getString("TOTALGERAL"));
				dadosProcesso[5][i] += Integer.parseInt(rs.getString("ENTRADAS"));
				dadosProcesso[6][i] += Integer.parseInt(rs.getString("DIFERENCA_ANALISE"));
				dadosProcesso[7][i]  = Integer.parseInt(rs.getString("ESTOQUE"))+dadosProcesso[5][i];
				if (bSaldo)
				{
					if (dadosProcesso[7][i]>0)
					{
						iIndiceSaldo=i;
						bSaldo=false;
					}
				}
			}
			
			iIndiceSaldo++;
			for(int j = iIndiceSaldo; j <= 11;j++)
				dadosProcesso[7][j] = dadosProcesso[7][j-1] + dadosProcesso[5][j];
			
			myGrf.setDadosProcesso(dadosProcesso);
			
		}catch (SQLException e) {
			
			throw new DaoException(e.getMessage());
		}			
		catch (Exception ex) {	
			
			throw new DaoException(ex.getMessage());
		}finally{
			try{
				serviceloc.setReleaseConnection(conn);	
			}catch(Exception e){}
		}	
	}
	
	
	
	private java.sql.Date converteData(String data) {
		java.sql.Date Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = new java.sql.Date(df.parse(data).getTime());
		} catch (Exception e) {
			Retorno = null;
		}
		return Retorno;
	}
	private String converteData(java.sql.Date data) {
		String Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = df.format(data);
		} catch (Exception e) {
			Retorno = "";
		}
		return Retorno;
	}	 
	
	/**
	 * M�todo Principal responsavel pela inclus�o dos destinat�rios para um determindo
	 * n�vel.
	 * @param myNivelDestinatario
	 * @param vErro
	 * @throws DaoException
	 * @author loliveira
	 */
	public boolean gravaNivelDestinatario(CadastraNivelDestinatarioBean myNivelDestinatario, Vector vErro) throws DaoException{
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			PreparedStatement stmt  = null;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			retorno=retorno && deleteNivelDestinatario(myNivelDestinatario,stmt,conn) ;
			for (int i = 0 ; i<myNivelDestinatario.getNivelDestinatario().size(); i++) {
				CadastraNivelDestinatarioBean nivelDestinatarioBean = (CadastraNivelDestinatarioBean)myNivelDestinatario.getNivelDestinatario().elementAt(i) ;
				retorno=retorno && insertNivelDestinatario(nivelDestinatarioBean,stmt,conn) ;
			}
			
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else 		 conn.rollback();
			
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();		  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myNivelDestinatario.setMsgErro(vErro);
		return retorno;
	}
	
	public boolean deleteNivelDestinatario(CadastraNivelDestinatarioBean myNivelDestinatario,
			PreparedStatement stmt, Connection conn) throws DaoException {
		boolean retorno = false ;
		String sCmd = "";
		try {
			sCmd  = "DELETE TSMI_PROC_SIST_NIV_DEST WHERE COD_PROCESSO_SISTEMA_NIVEL = :1";
			stmt = conn.prepareStatement(sCmd);
			stmt.setInt(1,Integer.parseInt(myNivelDestinatario.getCodNivelProcesso()));
			stmt.execute();
			stmt.close();
			retorno = true ;
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
		return retorno ;
	}
	
	public boolean insertNivelDestinatario(CadastraNivelDestinatarioBean myNivelDestinatario,PreparedStatement stmt,
			Connection conn) throws DaoException {
		boolean retorno = false ;
		String sCmd = "";
		try {
			sCmd  = "INSERT INTO TSMI_PROC_SIST_NIV_DEST (COD_SIST_NIV_DESTINATARIO," +
			"COD_PROCESSO_SISTEMA_NIVEL,COD_DESTINATARIO) VALUES (SEQ_TSMI_PROC_SIST_NIV_DEST.NEXTVAL,:1,:2)";
			stmt = conn.prepareStatement(sCmd);
			stmt.setInt(1,Integer.parseInt(myNivelDestinatario.getCodNivelProcesso()));
			stmt.setInt(2,Integer.parseInt(myNivelDestinatario.getCodDestinatario()));
			stmt.execute();
			stmt.close();
			retorno =true ;
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
		return retorno ;
	}
	
	public void Le_NivelDestinatario(CadastraNivelDestinatarioBean myNivelDestinatario) throws DaoException 
	{
		String sCmd = "";
		Connection conn =null ;	
		ArrayList destinatarios = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			sCmd  = "SELECT N.COD_SIST_NIV_DESTINATARIO, N.COD_PROCESSO_SISTEMA_NIVEL, " +
			"N.COD_DESTINATARIO, D.TXT_DESTINATARIO,D.TXT_EMAIL,D.COD_ORGAO, O.SIG_ORGAO  " +
			"FROM TSMI_PROC_SIST_NIV_DEST N, TSMI_DESTINATARIO D,TSMI_ORGAO O  " +
			"WHERE N.COD_DESTINATARIO = D.COD_DESTINATARIO AND D.COD_ORGAO = O.COD_ORGAO " +
			"AND N.COD_PROCESSO_SISTEMA_NIVEL = ?" ;
			PreparedStatement stmt = conn.prepareStatement(sCmd);
			stmt.setInt(1,Integer.parseInt(myNivelDestinatario.getCodNivelProcesso()));
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				CadastraNivelDestinatarioBean NivelDestinatarioBean = new CadastraNivelDestinatarioBean();
				NivelDestinatarioBean.setCodSistNivDestinatario(rs.getString("COD_SIST_NIV_DESTINATARIO"));
				NivelDestinatarioBean.setCodNivelProcesso(rs.getString("COD_PROCESSO_SISTEMA_NIVEL"));
				NivelDestinatarioBean.setCodDestinatario(rs.getString("COD_DESTINATARIO"));
				NivelDestinatarioBean.setTxtDestinatario(rs.getString("TXT_DESTINATARIO"));
				NivelDestinatarioBean.setTxtEmail(rs.getString("TXT_EMAIL"));
				NivelDestinatarioBean.setSigOrgao(rs.getString("SIG_ORGAO"));
				destinatarios.add(NivelDestinatarioBean);
			}
			myNivelDestinatario.setListNivelDestino(destinatarios);
			rs.close();
		}
		catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();		  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public String getNivel(String codProcesso) throws DaoException {
		String myNiveis ="";
		Connection conn =null ;					
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String sCmd = "SELECT * from TSMI_PROCESSO_SISTEMA_NIVEL";
			sCmd += " WHERE COD_PROCESSO_SISTEMA ='"+codProcesso+"'";				
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myNiveis += ",\""+rs.getString("COD_PROCESSO_SISTEMA_NIVEL") +"\"";
				myNiveis += ",\""+rs.getString("DSC_NIVEL_PROCESSO") +"\"";
			}
			myNiveis = myNiveis.substring(1,myNiveis.length());
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myNiveis;
	}
	
	public boolean NivelLeBean(NivelProcessoSistemaBean myNivel,int tp) throws DaoException
	{
		boolean retorno = false;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd = "";
			sCmd = "SELECT * from TSMI_PROCESSO_SISTEMA_NIVEL ";
			if(tp==0) sCmd += "WHERE COD_PROCESSO_SISTEMA_NIVEL='"+ myNivel.getCodNivelProcesso()+"'";
			else sCmd += "WHERE DSC_NIVEL_PROCESSO ='"+ myNivel.getNomNivelProcesso()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while( rs.next() ) {
				retorno=true ;
				myNivel.setCodNivelProcesso( rs.getString("COD_PROCESSO_SISTEMA_NIVEL"));
				myNivel.setNomNivelProcesso( rs.getString("DSC_NIVEL_PROCESSO") );
			}
			
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) {
			myNivel.setCodSistema("0");
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			myNivel.setCodSistema("0");
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	/**---------------------------------------------------
	 * DAO Relativo ao CadastrarDestinatarioBean
	 -----------------------------------------------------*/
	public boolean InsertDestinatario(List destinatarios, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			CadastraDestinatarioBean myDestinatario;
			for (int i = 0 ; i< destinatarios.size(); i++) {
				myDestinatario = (CadastraDestinatarioBean)destinatarios.get(i);
				if ("".equals(myDestinatario.getTxtDestinatario().trim()) == false){
					if(myDestinatario.getCodDestinatario().equals("")){	
						if( DestinatarioInsert(myDestinatario, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro na Inclus�o do E-mail do Destinat�rio: " + myDestinatario.getTxtDestinatario() + "\n");
							retorno = false;
						}
					}
					else{
						if( DestinatarioUpdate(myDestinatario, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro na Atualiza��o do E-mail do Destinat�rio: " + myDestinatario.getTxtDestinatario()+ "\n");
							retorno = false;
						}
					}
				}
				else {
					if ("".equals(myDestinatario.getCodDestinatario()) == false) 
						if( DestinatarioDelete(myDestinatario, stmt, vErro) ){
							conn.commit();
							destinatarios.remove(i);
							i--;
						}
						else{
							conn.rollback();
							retorno = false;
						}
				}
			}
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if(!retorno)
						conn.rollback();  
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean DestinatarioInsert (CadastraDestinatarioBean myDestinatario, Statement stmt) {
		try {
			if (("".equals(myDestinatario.getCodDestinatario())) || ("0".equals(myDestinatario.getCodDestinatario())))  {
				String sCmd = "SELECT SEQ_TSMI_DESTINATARIO.nextval AS codDestinatario FROM dual";
				ResultSet rs = stmt.executeQuery(sCmd);
				rs.next();
				myDestinatario.setCodDestinatario(rs.getString("codDestinatario"));
				rs.close();
				
				sCmd  = "INSERT INTO TSMI_DESTINATARIO (COD_DESTINATARIO,";
				sCmd += "COD_ORGAO,TXT_DESTINATARIO,TXT_TITULO,TXT_EMAIL) ";
				sCmd += "VALUES (";
				sCmd += "'"+myDestinatario.getCodDestinatario()+"',";
				sCmd += "'"+myDestinatario.getCodOrgao()+"',";
				sCmd += "'"+myDestinatario.getTxtDestinatario()+"',";
				sCmd += "'"+myDestinatario.getTxtTitulo()+"',";
				sCmd += "'"+myDestinatario.getTxtEmail()+"')" ;
				
				stmt.execute(sCmd);
			}
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}	
	
	public boolean DestinatarioDelete(CadastraDestinatarioBean myDestinatario,
			Statement stmt, Vector vErro) throws DaoException {
		boolean bOk = false;
		boolean bExiste = false;
		
		try {
			String sCmd = "SELECT * FROM TSMI_PROC_SIST_NIV_DEST WHERE COD_DESTINATARIO = '"+
			myDestinatario.getCodDestinatario()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while(rs.next()){
				bExiste = true; 
			}
			if(!bExiste)
			{
				sCmd  = "DELETE TSMI_DESTINATARIO WHERE COD_DESTINATARIO = '"+
				myDestinatario.getCodDestinatario()+"'";
				stmt.execute(sCmd);
				bOk = true;
			}
			else vErro.add("N�o foi poss�vel atualizar os dados, pois o " +
			"destinat�rio est� vinculado a um n�vel de processo. ");
			
			rs.close();
		}
		catch (SQLException e) {
			vErro.add("Erro na Exclus�o do E-mail do Destinat�rio:"+e.getMessage()+"\n");
			return bOk;
		}
		catch (Exception ex){
			vErro.add("Erro na Exclus�o do E-mail do Destinat�rio:"+ex.getMessage()+"\n");
			return bOk;
		}
		return bOk;
	}
	
	public boolean DestinatarioUpdate(CadastraDestinatarioBean myDestinatario, Statement stmt) {    
		try{
			String sCmd  = "UPDATE TSMI_DESTINATARIO SET ";
			sCmd += " COD_ORGAO ='"+myDestinatario.getCodOrgao()+"',";
			sCmd += " TXT_DESTINATARIO ='"+myDestinatario.getTxtDestinatario()+"', ";
			sCmd += " TXT_TITULO ='"+myDestinatario.getTxtTitulo()+"', ";
			sCmd += " TXT_EMAIL = '"+myDestinatario.getTxtEmail()+"'";
			sCmd += " WHERE COD_DESTINATARIO = '"+myDestinatario.getCodDestinatario()+"' ";
			stmt.execute(sCmd);
		}
		
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	public List getDestinatarios(CadastraDestinatarioBean DestinatarioBean, String todosOrgaos) throws DaoException {
		Connection conn =null ;	
		List destinatario = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT D.COD_DESTINATARIO, D.COD_ORGAO, D.TXT_DESTINATARIO,";
			sCmd += " D.TXT_TITULO, D.TXT_EMAIL, O.SIG_ORGAO ";
			sCmd += " FROM TSMI_DESTINATARIO D, TSMI_ORGAO O WHERE D.COD_ORGAO = O.COD_ORGAO ";
			if (todosOrgaos.equals("S"))
				sCmd += " AND D.COD_ORGAO = '"+ DestinatarioBean.getCodOrgao() + "'";
			sCmd += " ORDER BY D.TXT_DESTINATARIO";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				CadastraDestinatarioBean myDestinatario = new CadastraDestinatarioBean() ;
				myDestinatario.setCodOrgao(DestinatarioBean.getCodOrgao());
				myDestinatario.setCodDestinatario(rs.getString("COD_DESTINATARIO"));
				myDestinatario.setTxtDestinatario(rs.getString("TXT_DESTINATARIO"));
				myDestinatario.setTxtTitulo(rs.getString("TXT_TITULO"));
				myDestinatario.setTxtEmail(rs.getString("TXT_EMAIL"));
				myDestinatario.setSigOrgao(rs.getString("SIG_ORGAO"));
				destinatario.add(myDestinatario) ;
			}    	
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return destinatario;
	}
	
	public void buscarProcessoPorPeriodo(RelatProcPeriodoBean relatProcPeriodoId) throws DaoException {
		Connection conn = null;
		ArrayList <RelatProcPeriodoBean>myListProcessos = new ArrayList <RelatProcPeriodoBean>();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * "+
			" FROM TSMI_REQUERIMENTO T1, TSMI_RELATOR T2," +
			" TSMI_AUTO_INFRACAO T3,TSMI_INFRACAO T4,TSMI_ORGAO T5 " +
			" WHERE T1.COD_RELATOR_JU = T2.NUM_CPF "+  
			" AND T1.COD_JUNTA_JU = T2.COD_JUNTA "+
			" AND T1.COD_AUTO_INFRACAO = T3.COD_AUTO_INFRACAO "+
			" AND T3.COD_INFRACAO = T4.COD_INFRACAO "+
			" AND T3.COD_ORGAO = '"+relatProcPeriodoId.getCodOrgao()+"' "+
			" AND T3.COD_ORGAO = T5.COD_ORGAO "+
			" AND T1.COD_TIPO_SOLIC IN ("+relatProcPeriodoId.getTpReq()+") " +
			" AND T1.DAT_RS BETWEEN to_date('"+ relatProcPeriodoId.getDataIni()+"','dd/mm/yyyy')"+
			" AND to_date('"+ relatProcPeriodoId.getDataFim()+"','dd/mm/yyyy') " ;
			
			if(relatProcPeriodoId.getIndResultado().equals("I"))
				sCmd +=  " AND T1.COD_RESULT_RS = 'I' ";
			else if (relatProcPeriodoId.getIndResultado().equals("D"))
				sCmd +=  " AND T1.COD_RESULT_RS = 'D' ";
			
			if (relatProcPeriodoId.getTipoOrdem().equals("R"))
			{
				if (relatProcPeriodoId.getTipoSubOrdem().equals("C"))
					sCmd += " ORDER BY T2.NOM_RELATOR,T3.NUM_DOC_PROPRIETARIO,T1.COD_RESULT_RS";
				else
					sCmd += " ORDER BY T2.NOM_RELATOR,T3.COD_INFRACAO,T1.COD_RESULT_RS";            		
			}
			else
			{
				if (relatProcPeriodoId.getTipoSubOrdem().equals("C"))
					sCmd += " ORDER BY T1.NOM_USERNAME_RS,T3.NUM_DOC_PROPRIETARIO,T1.COD_RESULT_RS";
				else
					sCmd += " ORDER BY T1.NOM_USERNAME_RS,T3.COD_INFRACAO,T1.COD_RESULT_RS";            		
			}
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatProcPeriodoBean myRelProcPeriodoId = new RelatProcPeriodoBean();
				relatProcPeriodoId.setNomOrgao(rs.getString("NOM_ORGAO"));
				relatProcPeriodoId.setSigOrgao(rs.getString("SIG_ORGAO"));
				myRelProcPeriodoId.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myRelProcPeriodoId.setDatProcDP(Util.converteData(rs.getDate("DAT_REQUERIMENTO")));
				myRelProcPeriodoId.setCodRelator(rs.getString("COD_RELATOR_JU"));
				myRelProcPeriodoId.setNomRelator(rs.getString("NOM_RELATOR"));
				myRelProcPeriodoId.setNomUsername(rs.getString("NOM_USERNAME_RS"));
				if (relatProcPeriodoId.getTipoOrdem().equals("R"))
					myRelProcPeriodoId.setNomRelatorUsername(myRelProcPeriodoId.getNomRelator());
				else
					myRelProcPeriodoId.setNomRelatorUsername(myRelProcPeriodoId.getNomUsername());					
				myRelProcPeriodoId.setDatResult(Util.converteData(rs.getDate("DAT_RS")));
				myRelProcPeriodoId.setCodResult(rs.getString("COD_RESULT_RS"));
				myRelProcPeriodoId.setCodInfracao(rs.getString("COD_INFRACAO"));
				myRelProcPeriodoId.setDscInfracao(rs.getString("DSC_INFRACAO"));
				myRelProcPeriodoId.setNomProprietario(rs.getString("NOM_PROPRIETARIO"));
				myRelProcPeriodoId.setCpfProprietario(rs.getString("NUM_DOC_PROPRIETARIO"));
				myRelProcPeriodoId.setNumPlaca(rs.getString("NUM_PLACA"));
				myListProcessos.add(myRelProcPeriodoId) ;
			}
			relatProcPeriodoId.setListProcessos(myListProcessos);
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}	
	
	public void getMontaRelatorio(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn =null ;	
		List dados = new ArrayList();
		String[] mes = {"01","02","03","04","05","06","07","08","09","10","11","12"};
		String[] dol = new String [12];
		String[] mr = new String [12];
		String[] vex = new String [12];
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT COD_ORGAO,SIG_ORGAO,NOM_ORGAO,IND_AUTUA,IND_SMITDOL" +
			"  FROM TSMI_ORGAO ORDER BY COD_ORGAO";
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
				myRelatorio.setCodOrgao(rs.getString("COD_ORGAO"));
				myRelatorio.setSigOrgao(rs.getString("SIG_ORGAO"));
				myRelatorio.setNomOrgao(rs.getString("NOM_ORGAO"));
				myRelatorio.setSitAutua(rs.getString("IND_AUTUA"));
				myRelatorio.setIndSmitDol(rs.getString("IND_SMITDOL"));
				myRelatorio.setOrigem("-, ");
				for (int i=0; i<12;i++){
					myRelatorio.getCodMes().add(mes[i]);
					myRelatorio.getQtdDOL().add(dol[i]);
					myRelatorio.getQtdMR().add(mr[i]);
					myRelatorio.getQtdVex().add(vex[i]);
				}
				dados.add(myRelatorio);
			}    	
			RelatAutosPorOrgaoBean.setDados(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}   
	
	public void getGerarRelatorio(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ResultSet rs = null;
		String dataRec = "";
		String mr = "";
		String dol = "";
		int somaDol = 0;
		int somaMr = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = " select to_char(a.dat_recebimento,'dd/mm/yyyy') as dat_recebimento,a.cod_orgao COD_ORGAO,"
				+ " to_char(sum(a.qtd_reg)) QTD_MR01,'' QTD_SMIT_DOL "
				+ " from smit.tsmi_arquivo_recebido a where a.cod_ident_arquivo = 'MR01' "
				+ " and a.dat_recebimento >= '01/"
				+ RelatAutosPorOrgaoBean.getMesInicial()
				+ "/"
				+ RelatAutosPorOrgaoBean.getAnoInicial()
				+ "' and"
				+ " a.dat_recebimento < '01/"
				+ RelatAutosPorOrgaoBean.getMesFinal()
				+ "/"
				+ RelatAutosPorOrgaoBean.getAnoFinal() + "' ";
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += " and  a.cod_orgao = "
					+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " group by a.cod_orgao,a.dat_recebimento union "
				+ " select to_char(b.dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " b.cod_orgao COD_ORGAO,'', to_char(sum(c.qtd_reg-c.qtd_linha_erro)) QTD_SMIT_DOL"
				+ " from tsmi_arquivo_dol b, tsmi_lote_dol c"
				+ " where b.cod_arquivo_dol = c.cod_arquivo_dol"
				+ " and b.dat_recebimento >= '01/"
				+ RelatAutosPorOrgaoBean.getMesInicial() + "/"
				+ RelatAutosPorOrgaoBean.getAnoInicial() + "' and"
				+ " b.dat_recebimento <'01/"
				+ RelatAutosPorOrgaoBean.getMesFinal() + "/"
				+ RelatAutosPorOrgaoBean.getAnoFinal() + "' ";
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += "and b.cod_orgao = "
					+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " group by b.cod_orgao,b.dat_recebimento "
				+ " union "
				+ " select to_char(b.dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " b.cod_orgao COD_ORGAO,'', to_char(sum(c.qtd_reg-c.qtd_linha_erro)) QTD_SMIT_DOL"
				+ " from tsmi_arquivo_dol_bkp b, tsmi_lote_dol_bkp c"
				+ " where b.cod_arquivo_dol = c.cod_arquivo_dol"
				+ " and b.dat_recebimento >= '01/"
				+ RelatAutosPorOrgaoBean.getMesInicial() + "/"
				+ RelatAutosPorOrgaoBean.getAnoInicial() + "' and"
				+ " b.dat_recebimento <'01/"
				+ RelatAutosPorOrgaoBean.getMesFinal() + "/"
				+ RelatAutosPorOrgaoBean.getAnoFinal() + "' ";
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += "and b.cod_orgao = "
					+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " group by b.cod_orgao,b.dat_recebimento";
			sCmd += " order by cod_orgao  ";
			rs = stmt.executeQuery(sCmd);
			String orgao = "";
			String datRecebimento = "";
			int QTD_MR01 = 0;
			int QTD_SMIT_DOL = 0;
			while (rs.next()) {
				orgao = rs.getString("COD_ORGAO");
				datRecebimento = rs.getString("dat_recebimento");
				QTD_MR01 = rs.getInt("QTD_MR01");
				QTD_SMIT_DOL = rs.getInt("QTD_SMIT_DOL");
				
				Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
				RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
				while (it.hasNext()) {
					myRelatorio = (RelatAutosPorOrgaoBean) it.next();
					if (myRelatorio.getCodOrgao().equals(orgao)) {
						// verifica o m�s lido
						dataRec = datRecebimento.substring(3, 5);
						for (int i = 0; i < 12; i++) {
							if (dataRec.equals(myRelatorio.getCodMes().get(i))) {
								// Soma quantidade de MR
								mr = (String) myRelatorio.getQtdMR().get(i);
								if (mr != null) {
									somaMr = Integer.parseInt(mr) + QTD_MR01;
									myRelatorio.getQtdMR().set(i,
											Integer.toString(somaMr));
								} else
									myRelatorio.getQtdMR().set(i,
											Integer.toString(QTD_MR01));
								
								// Soma quantidade de DOL
								dol = (String) myRelatorio.getQtdDOL().get(i);
								if (dol != null) {
									somaDol = Integer.parseInt(dol)
									+ QTD_SMIT_DOL;
									myRelatorio.getQtdDOL().set(i,
											Integer.toString(somaDol));
									
								} else
									myRelatorio.getQtdDOL().set(i,
											Integer.toString(QTD_SMIT_DOL));
								break;
							}
						}
						break;
					}
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}	
	public void getCarregaDadosVex(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn = null ;
		ResultSet rs  = null;
		
		String sCmd = "";
		String dataRec = "";
		String autos = "";
		String orgao = "";
		String datRecebimento = "";
		
		int somaVex = 0;
		int QTD_AUTOS = 0;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();        
			
			sCmd = " SELECT to_char(DAT_RECEBIMENTO,'mm/yyyy') AS DAT_RECEBIMENTO," +
			" COUNT(QTD_AUTOS) QTD_AUTOS, COD_ORGAO " +
			" FROM smit.mv_teste " +
			" WHERE DAT_RECEBIMENTO >='01/"+RelatAutosPorOrgaoBean.getMesInicial()+"/"+ RelatAutosPorOrgaoBean.getAnoInicial()+"' and" +
			" DAT_RECEBIMENTO <= '01/"+RelatAutosPorOrgaoBean.getMesFinal()+"/"+ RelatAutosPorOrgaoBean.getAnoFinal()+"' "  ;
			if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()<=0)
				sCmd += " AND COD_ORGAO='"+RelatAutosPorOrgaoBean.getCodOrgao()+"'  ";
			sCmd += " GROUP BY COD_ORGAO,DAT_RECEBIMENTO " +
			" ORDER BY COD_ORGAO,DAT_RECEBIMENTO ";
			rs   = stmt.executeQuery(sCmd) ;
			
			while (rs.next()) {
				orgao = rs.getString("COD_ORGAO");
				datRecebimento = rs.getString("dat_recebimento");
				QTD_AUTOS = rs.getInt("QTD_AUTOS");
				
				Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
				RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
				while(it.hasNext()){
					myRelatorio = (RelatAutosPorOrgaoBean) it.next();
					if (myRelatorio.getCodOrgao().equals(orgao))
					{
						// verifica o m�s lido
						dataRec = datRecebimento.substring(0,2);
						for (int i=0; i<12;i++)
						{
							if(dataRec.equals(myRelatorio.getCodMes().get(i)))
							{
								//Soma quantidade de VEX
								autos = (String)myRelatorio.getQtdVex().get(i);
								if(autos != null)
								{
									somaVex = Integer.parseInt(autos) + QTD_AUTOS;
									myRelatorio.getQtdVex().set(i,Integer.toString(somaVex));
								}
								else 
									myRelatorio.getQtdVex().set(i,Integer.toString(QTD_AUTOS));
								
								break;
							}
						}
						break;
					}
				}
			}
			rs.close();
			stmt.close();
			
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getAcesso(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn = null ;
		ArrayList dados = new ArrayList();
		String sCmd = "";
		ResultSet rs  = null;
		int acesso = 0;
		String codOrgao = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();        
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
			while(it.hasNext()){
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				sCmd = "  SELECT DISTINCT COUNT(*) ACESSO, COD_ORGAO_LOTACAO FROM TCAU_LOG_ACESSO WHERE  " +
				" DAT_OPERACAO between to_date('01/"+RelatAutosPorOrgaoBean.getMesInicial()+"/"+ RelatAutosPorOrgaoBean.getAnoInicial()+"','dd/mm/yyyy') and" +
				" to_date('01/"+RelatAutosPorOrgaoBean.getMesFinal()+"/"+ RelatAutosPorOrgaoBean.getAnoFinal()+"','dd/mm/yyyy') "  ;
				if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()<=0)
					sCmd += " AND COD_ORGAO_LOTACAO ="+RelatAutosPorOrgaoBean.getCodOrgao();
				else
					sCmd += " AND COD_ORGAO_LOTACAO ="+myRelatorio.getCodOrgao();
				sCmd += " GROUP BY COD_ORGAO_LOTACAO " +
				" ORDER BY COD_ORGAO_LOTACAO ";
				rs   = stmt.executeQuery(sCmd) ;
				
				while (rs.next()) {
					acesso = rs.getInt("ACESSO");
					codOrgao = rs.getString("COD_ORGAO_LOTACAO");
					if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()<=0){
						if(codOrgao.equals(RelatAutosPorOrgaoBean.getCodOrgao()))
							myRelatorio.setAcesso(acesso);
						break;
					}else{
						myRelatorio.setAcesso(acesso);
					}
				}
				dados.add(myRelatorio);
			}
			RelatAutosPorOrgaoBean.setDados(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getOrigem(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn = null ;
		ArrayList dados = new ArrayList();
		
		String sCmd = "";
		String sigOrgaoOrigem = "";
		String sCodStatus = "2";
		String sCodIdentArquivoOrigem = "'DE01','MR01'";
		
		
		ResultSet rs  = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();        
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
			while(it.hasNext()){
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				sCmd = " SELECT DISTINCT SIG_ORGAO_LOTACAO, COD_ORGAO " +
				" FROM TSMI_ARQUIVO_RECEBIDO WHERE " +
				" DAT_RECEBIMENTO between to_date('01/"+RelatAutosPorOrgaoBean.getMesInicial()+"/"+ RelatAutosPorOrgaoBean.getAnoInicial()+"','dd/mm/yyyy') and" +
				" to_date('01/"+RelatAutosPorOrgaoBean.getMesFinal()+"/"+ RelatAutosPorOrgaoBean.getAnoFinal()+"','dd/mm/yyyy') AND"  +
				" COD_STATUS = '"+sCodStatus+"' AND "+
				" COD_IDENT_ARQUIVO IN ("+sCodIdentArquivoOrigem+") ";
				if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()<=0)
					sCmd += " AND COD_ORGAO ="+RelatAutosPorOrgaoBean.getCodOrgao();
				else
					sCmd += " AND COD_ORGAO ="+myRelatorio.getCodOrgao();
				sCmd += " ORDER BY COD_ORGAO";
				rs = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					// Adiciona a origem do MR01,DE01 
					sigOrgaoOrigem +=rs.getString("SIG_ORGAO_LOTACAO")+", ";
				}
				if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()<=0)
				{
					if(myRelatorio.getCodOrgao().equals(RelatAutosPorOrgaoBean.getCodOrgao()))
					{
						if(sigOrgaoOrigem.length()>0)
							myRelatorio.setOrigem(sigOrgaoOrigem);
					}
				}
				else 
				{
					if(sigOrgaoOrigem.length()>0)
						myRelatorio.setOrigem(sigOrgaoOrigem);
				}
				sigOrgaoOrigem = "";
				dados.add(myRelatorio);
				
			}
			RelatAutosPorOrgaoBean.setDados(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void criaGrafProcAbertos(GraficoBean myGrf) throws DaoException{
		
		Connection conn = null;
		String sCmd = "";
		ResultSet rs = null;
		try{            
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt   = conn.createStatement();
			
			sCmd =  "SELECT SIG_ORGAO ";
			sCmd += "FROM TSMI_ORGAO ";
			sCmd += "WHERE COD_ORGAO = '"+ myGrf.getCodOrgao() +"' ";           
			rs  = stmt.executeQuery(sCmd);          
			while (rs.next()){
				myGrf.setSiglaOrgao(rs.getString("sig_orgao"));
			}
			
			
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("24"); //M�DULO GERENCIAL   
			param.PreparaParam(conn);
			
			String localTrabalho = param.getParamSist("LOCAL_DE_TRABALHO");
			
			sCmd = "SELECT count(R.COD_TIPO_SOLIC)AS QTD,substr(R.COD_TIPO_SOLIC,1,1) AS COD_TIPO_SOLIC " +
			" FROM TSMI_REQUERIMENTO R, TSMI_AUTO_INFRACAO A, TSMI_ORGAO O, TCAU_USUARIO U  " +
			" WHERE R.DAT_PROC_DP >= '"+myGrf.getDatInicio()+"' " +
			" AND R.DAT_PROC_DP <= '"+myGrf.getDatFim()+"'"+
			" AND R.COD_TIPO_SOLIC IN ("+myGrf.getTipoReq()+")"+
			" AND R.COD_ORGAO_LOTACAO_DP = "+myGrf.getCodOrgao()+
			" AND R.COD_AUTO_INFRACAO = A.COD_AUTO_INFRACAO "+
			" AND U.NOM_USERNAME = R.NOM_USERNAME_DP " +
			" AND U.TXT_LOCAL_TRABALHO = '"+ localTrabalho +"'" +
			" AND O.COD_ORGAO = R.COD_ORGAO_LOTACAO_DP" +
			" GROUP BY substr(R.COD_TIPO_SOLIC,1,1)" +
			" ORDER BY substr(R.COD_TIPO_SOLIC,1,1)";
			
			rs  = stmt.executeQuery(sCmd);
			double totInf = 0;
			double maiorValor = 0;
			double i = 0 ;
			int tamMun = 0;
			Double lNumInf[] = new Double[93];
			String lNomeTipoReq[] = new String[93];
			String legenda;
			
			while (rs.next()) {             
				legenda = rs.getString("COD_TIPO_SOLIC");
				if ("D".equals(legenda)){     
					legenda = "DEFESA PR�VIA";
				}else if("1".equals(legenda)){ 
					legenda = "1a. INST�NCIA";
				}else if("2".equals(legenda)){
					legenda = "2a. INST�NCIA";
				}else if("T".equals(legenda)){
					legenda = "TRI";
				}else if("R".equals(legenda)){
					legenda = "REN�NCIA DE DEFESA PR�VIA";
				}
				
				lNomeTipoReq[tamMun] = legenda.trim();
				i = Long.parseLong(rs.getString("QTD")) ;
				totInf += i ;
				if ((i > maiorValor)) 
					maiorValor=i;
				
				lNumInf[tamMun] = new Double(i);    
				tamMun ++;              
			}   
			
			//monta array com os valores 
			double k;
			Double lNumInf2[] = new Double[93];
			String lNomeTipoReq2[] = new String[93];
			int tamMun2 = 0;
			
			for(int l = 0;l <= 92;l++){
				lNumInf2[l] = new Double(0);
				lNomeTipoReq2[l] = " ";
			}
			
			for(int l = 0;l < tamMun;l++) {
				k = lNumInf[l].longValue();
				lNumInf2[tamMun2] = new Double(k);
				lNomeTipoReq2[tamMun2] = lNomeTipoReq[l];   
				tamMun2 ++;             
			}
			
			// calcular percentual
			for(int l = 0;l <= tamMun2;l++) {
				double ni = lNumInf2[l].doubleValue();
				if (totInf !=0 ) 
					ni = (ni*100/totInf);
				lNumInf2[l] = new Double(ni);
				
			}   
			myGrf.setInfMunicipio(lNumInf2);
			myGrf.setNomeMunicipio(lNomeTipoReq2);
			
			conn.close();       
		} 
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}           
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}   
		
	}
	
	public void getGerarQtdDol(
			RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ResultSet rs = null;
		int i = 0;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = " select  b.cod_orgao COD_ORGAO,'',b.cod_orgao_lotacao,o.sig_orgao, " +
			" to_char(sum(c.qtd_reg-c.qtd_linha_erro)) QTD_SMIT_DOL from tsmi_arquivo_dol b, " +
			" tsmi_lote_dol c , tsmi_orgao o where b.cod_arquivo_dol = c.cod_arquivo_dol " +
			" and b.dat_recebimento >= to_date('01/"+RelatAutosPorOrgaoBean.getMesInicial()+"/"+RelatAutosPorOrgaoBean.getAnoInicial()+"','dd/mm/yyyy') " +
			" and b.dat_recebimento < to_date('01/"+RelatAutosPorOrgaoBean.getMesFinal()+"/"+RelatAutosPorOrgaoBean.getAnoFinal()+"','dd/mm/yyyy') "  ;
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += " and b.cod_orgao = "+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " and b.cod_orgao_lotacao = o.cod_orgao  " +
			" group by b.cod_orgao,b.cod_orgao_lotacao,o.sig_orgao   " +
			" union  select  b.cod_orgao COD_ORGAO,'', b.cod_orgao_lotacao, o.sig_orgao," +
			" to_char(sum(c.qtd_reg-c.qtd_linha_erro)) QTD_SMIT_DOL from tsmi_arquivo_dol_bkp b, " +
			" tsmi_lote_dol_bkp c, tsmi_orgao o where b.cod_arquivo_dol = c.cod_arquivo_dol " +
			" and b.dat_recebimento >= to_date('01/"+RelatAutosPorOrgaoBean.getMesInicial()+"/"+RelatAutosPorOrgaoBean.getAnoInicial()+"','dd/mm/yyyy') " +
			" and b.dat_recebimento < to_date('01/"+RelatAutosPorOrgaoBean.getMesFinal()+"/"+RelatAutosPorOrgaoBean.getAnoFinal()+"','dd/mm/yyyy') "  ;
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += " and b.cod_orgao = "+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " and b.cod_orgao_lotacao = o.cod_orgao  " +
			" group by b.cod_orgao,b.cod_orgao_lotacao,o.sig_orgao  order by cod_orgao  ";
			
			rs = stmt.executeQuery(sCmd);
			String orgao = "";
			
			while (rs.next()) {
				orgao = rs.getString("COD_ORGAO");
				i = 0;
				Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
				RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
				while (it.hasNext()) 
				{
					myRelatorio = (RelatAutosPorOrgaoBean) it.next();
					
					if (myRelatorio.getCodOrgao().equals(orgao)) 
					{
						if(orgao.equals(rs.getString("cod_orgao_lotacao")))
						{
							myRelatorio.setOrigemDol(rs.getString("sig_orgao"));
							myRelatorio.setQtdDolOrg(rs.getInt("QTD_SMIT_DOL"));
						}
						else
						{
							myRelatorio.setOrigemDolOutros(rs.getString("sig_orgao"));
							myRelatorio.setQtdDolOutros(rs.getInt("QTD_SMIT_DOL"));
						}
						RelatAutosPorOrgaoBean.getDados().set(i,myRelatorio);
						break;
					}
					i++;
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getGerarQtdMr(
			RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ResultSet rs = null;
		int i = 0;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = "select sum(a.qtd_reg) as qtd_mr, o.sig_orgao, a.cod_orgao "
				+ " from tsmi_arquivo_recebido a, tsmi_orgao o where "
				+ " a.dat_recebimento >= '01/"
				+ RelatAutosPorOrgaoBean.getMesInicial() + "/"
				+ RelatAutosPorOrgaoBean.getAnoInicial() + "' and"
				+ " a.dat_recebimento <= '01/"
				+ RelatAutosPorOrgaoBean.getMesFinal() + "/"
				+ RelatAutosPorOrgaoBean.getAnoFinal() + "' "
				+ " and a.cod_ident_arquivo = 'MR01' "
				+ " and a.cod_orgao_lotacao = o.cod_orgao ";
			if (RelatAutosPorOrgaoBean.getTodosOrgaos().length() <= 0)
				sCmd += " and a.cod_orgao = "+ RelatAutosPorOrgaoBean.getCodOrgao();
			sCmd += " group by a.cod_orgao, o.sig_orgao ";
			rs = stmt.executeQuery(sCmd);
			String orgao = "";
			
			while (rs.next()) {
				orgao = rs.getString("COD_ORGAO");
				i = 0; 
				Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
				RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
				while (it.hasNext()) 
				{
					myRelatorio = (RelatAutosPorOrgaoBean) it.next();
					
					if (myRelatorio.getCodOrgao().equals(orgao)) 
					{
						myRelatorio.setOrigemMr(rs.getString("sig_orgao"));
						myRelatorio.setQtdMrOrg(rs.getInt("qtd_mr"));
						
						RelatAutosPorOrgaoBean.getDados().set(i,myRelatorio);
						break;
					}
					i++;
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void CarregaLista(AuditoriaBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		int prim = 1;
		int qtd = 0;
		int qtdParcial = 0;
		int tamVector = auxClasse.getPrimTrans().size();
		String oper = "";
		String trans = "";
		String datProc = "";
		
		if (acao.equals("Prox")) oper = ">";
		else oper = ">=";
		
		
		try {
			
			if (!auxClasse.getDatProcRef().equals("Ultima")){		
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
	            ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
	            ParamSistemaBeanId.PreparaParam();
	            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
	            if (MYABREVSIST.equals("GER")) sHistorico="S";
				
				ArrayList dados = new ArrayList();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd = 
					"SELECT NUM_TRANSACAO, DAT_PROC, TXT_PARTE_VARIAVEL "+ 
					"FROM TSMI_LOG_BROKER "+ 
					"WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";
					if (!auxClasse.getsIN().equals("0"))
						sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
				if (!auxClasse.getDatInicio().equals(""))
					sCmd +="AND DAT_PROC > '" + auxClasse.getDatInicio()+"' "  ;
				if (!auxClasse.getDatFim().equals(""))
					sCmd +="AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
				if (!auxClasse.getNomUserName().equals(""))
					sCmd +="AND trim(NOM_USERNAME) = '" + auxClasse.getNomUserName() + "' ";						
				if (!auxClasse.getCodOrgao().equals("0"))
					sCmd +="AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
				sCmd += "AND DAT_PROC > TO_DATE('" + auxClasse.getDatProcRef() + "', 'DD/MM/YYYY') ";										
				if (sHistorico.equals("S")) {
					sCmd += " UNION "
					+ " ("
					+ "SELECT NUM_TRANSACAO, DAT_PROC, TXT_PARTE_VARIAVEL "+ 
					" FROM TSMI_LOG_BROKER_BKP@SMITPBKP "+ 
					" WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";
					if (!auxClasse.getsIN().equals("0"))
						sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
					if (!auxClasse.getDatInicio().equals(""))
						sCmd +=" AND DAT_PROC > '" + auxClasse.getDatInicio()+"' "  ;
					if (!auxClasse.getDatFim().equals(""))
						sCmd +=" AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
					if (!auxClasse.getNomUserName().equals(""))
						sCmd +=" AND trim(NOM_USERNAME) = '" + auxClasse.getNomUserName() + "' ";						
					if (!auxClasse.getCodOrgao().equals("0"))
						sCmd +=" AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
					sCmd += " AND DAT_PROC > TO_DATE('" + auxClasse.getDatProcRef() + "', 'DD/MM/YYYY') ";										
					
					sCmd += " )";
				}
				
				sCmd += " ORDER BY DAT_PROC ";				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					trans = rs.getString("NUM_TRANSACAO");
					datProc = rs.getString("DAT_PROC");
					datProc = datProc.substring(8,10) + "/" + datProc.substring(5,7) + "/" + datProc.substring(0,4) + ":" + datProc.substring(11,19);
					qtd++;
					
					if ((prim == 1) && (!acao.equals("Ant"))){
						auxClasse.getPrimTrans().add(auxClasse.getTransRef());
						auxClasse.getPrimDatProc().add(auxClasse.getDatProcRef());
						prim++;
					}
					
					dados.add(rs.getString("TXT_PARTE_VARIAVEL"));
					if (qtd == 50) break;
					
				}
				
				qtdParcial = Integer.parseInt(auxClasse.getQtdParcialReg());
				
				if (qtd != 0){
					
					auxClasse.setQtdRegs(String.valueOf(qtd));
					if (acao.equals("Prox") || acao.equals("B")) 
						qtdParcial = qtdParcial + qtd;
				}
				
				if ((prim > 1) && (!acao.equals("Ant"))){
					auxClasse.getUltTrans().add(trans);
					auxClasse.getUltDatProc().add(datProc);
					auxClasse.getQtdReg().add(String.valueOf(qtd));
				}
				
				if ((qtd == 0) && (!auxClasse.getDatProcRef().equals("Ultima"))){
					
					auxClasse.getPrimTrans().add("Ultima");
					auxClasse.getPrimDatProc().add("Ultima");
					
					auxClasse.getUltTrans().add("Ultima");
					auxClasse.getUltDatProc().add("Ultima");
					auxClasse.getQtdReg().add("Ultima");
				}
				
				if (!auxClasse.getDatProcRef().equals("Ultima")){
					auxClasse.setQtdParcialReg(String.valueOf(qtdParcial));
				}
				
				auxClasse.setDados(dados);
				
				rs.close();
				stmt.close();
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}
	
	public String BuscaTotalReg(AuditoriaBean auxClasse)
	throws DaoException {
		
		Connection conn = null;
		String qtd = "0";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
            ParamSistemaBeanId.PreparaParam();
            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
            if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			ArrayList dados = new ArrayList();
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
//			"SELECT COUNT(*) AS QTD "+ 
//			"FROM ( SELECT * FROM TSMI_LOG_BROKER ORDER BY DAT_PROC, NUM_TRANSACAO) "+ 
				sCmd ="SELECT COUNT(*) AS QTD "; 
				sCmd +="FROM TSMI_LOG_BROKER ";
				sCmd +=	"WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";			
			if (!auxClasse.getsIN().equals("0"))
				sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd +="AND DAT_PROC > '" + auxClasse.getDatInicio() + "' " ;
			if (!auxClasse.getDatFim().equals(""))
				sCmd +="AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
			if (!auxClasse.getNomUserName().equals(""))
				sCmd +="AND NOM_USERNAME = '" + auxClasse.getNomUserName() + "' ";						
			if (!auxClasse.getCodOrgao().equals("0"))
				sCmd +="AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";						
			if (sHistorico.equals("S")) {
				sCmd += " UNION ";
				sCmd += " (";
				sCmd +="SELECT COUNT(*) AS QTD"; 
				sCmd +=" FROM TSMI_LOG_BROKER_BKP@SMITPBKP "; 
				sCmd +=" WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";				
				if (!auxClasse.getsIN().equals("0"))
					sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
				if (!auxClasse.getDatInicio().equals(""))
					sCmd +=" AND DAT_PROC > '" + auxClasse.getDatInicio() + "' " ;
				if (!auxClasse.getDatFim().equals(""))
					sCmd +=" AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
				if (!auxClasse.getNomUserName().equals(""))
					sCmd +=" AND NOM_USERNAME = '" + auxClasse.getNomUserName() + "' ";						
				if (!auxClasse.getCodOrgao().equals("0"))
					sCmd +=" AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
			
				sCmd += " )";
			}
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getString("QTD");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	public String BuscaCidade(AuditoriaBean auxClasse, String cod)
	throws DaoException {
		String cidade = "";		
		Connection conn = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();			
			// Busca o nome da cidade(munic�pio)
			String sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+cod+"'" ;	
			
			ResultSet rs = stmt.executeQuery(sCmd);				
			while (rs.next()) {
				cidade = (rs.getString("nom_municipio"));	  				  	  				  
			}			
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return cidade;
	}		
	
	public void getListPardais(GoogleMAPBean myBean) throws DaoException {
		Connection conn =null ;	
		Vector dados = new Vector();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT * " +
			"FROM TSMI_MUNICIPIO "+
			"WHERE COD_MUNICIPIO = " + myBean.getCodMunicipio();
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				
				myBean.setDesMunicipio(rs.getString("NOM_MUNICIPIO"));
				myBean.setLatitude(rs.getString("LATITUDE"));
				myBean.setLongitude(rs.getString("LONGITUDE"));
				
			}		
			
			rs.close();
			
			sCmd = "SELECT * " +
			"FROM TSMI_EQUIPAMENTO_ELETR "+
			"WHERE COD_MUNICIPIO = " + myBean.getCodMunicipio();
			rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				EquipamentoEletrBean RegBean = new EquipamentoEletrBean();
				RegBean.setIdEquipamentoEletr(rs.getString("ID_EQUIPAMENTO_ELETR"));
				RegBean.setLatitude(rs.getString("LATITUDE"));
				RegBean.setLongitude(rs.getString("LONGITUDE"));
				RegBean.setSentido(rs.getString("SENTIDO"));
				RegBean.setLocalizacao(rs.getString("LOCALIZACAO"));
				RegBean.setLocalizacao(RegBean.getLocalizacao().replaceAll("'", " "));				
				RegBean.setVelocidade(rs.getString("VELOCIDADE"));
				RegBean.setIdentEquipa(rs.getString("IDENT_APARELHO"));
				dados.addElement(RegBean);
			}    	
			myBean.setPardais(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}   
	
	public void getListMunicipios(GoogleMAPBean myBean, String uf, String codMun) throws DaoException {
		Connection conn =null ;	
		Vector dados = new Vector();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT * " +
			"FROM TSMI_MUNICIPIO ";
			
			if ((!uf.equals("")) && (!codMun.equals(""))){
				sCmd += "WHERE COD_UF = '" + uf + "' "+
				"AND COD_MUNICIPIO = " + codMun +" ";
			}
			
			if ((!uf.equals("")) && (codMun.equals(""))){
				sCmd += "WHERE COD_UF = '" + uf + "' ";
			}
			
			if ((uf.equals("")) && (!codMun.equals(""))){
				sCmd += "WHERE COD_MUNICIPIO = " + codMun +" ";
			}
			sCmd += "ORDER BY NOM_MUNICIPIO ";
			
			
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				
				TAB.MunicipioBean municBean = new TAB.MunicipioBean();
				municBean.setCodMunicipio(rs.getString("COD_MUNICIPIO"));
				municBean.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));
				dados.addElement(municBean);
				
			}		
			
			rs.close();
			
			myBean.setMunicipios(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}  
	
	
	public boolean AtualizaPardais(GoogleMAPBean myBean, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;
		String result = "";					
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			Vector FonteAtual = myBean.getPardais() ;
			Vector FonteCorrespondente = myBean.getPardaisGravado();
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			
			for (int i = 0 ; i< FonteAtual.size(); i++) {
				EquipamentoEletrBean statu = (EquipamentoEletrBean)FonteAtual.elementAt(i) ;
				EquipamentoEletrBean statuCorresp = (EquipamentoEletrBean)FonteCorrespondente.elementAt(i) ;
				if ((!statu.getLatitude().equals(statuCorresp.getLatitude())) ||
						(!statu.getLongitude().equals(statuCorresp.getLongitude())) ||
						(!statu.getSentido().equals(statuCorresp.getSentido())) 
						
				){
					
					retorno = alteraPardais(statu,vErro,conn);
					if (!retorno){
						break;
					}			
				}
				
			}
			
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else	  	 conn.rollback();
			
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException("Erro de SQL na inclus�o: "+sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException("Erro de Excep na inclus�o: "+e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myBean.setMsgErro(vErro);
		return retorno;
	}
	
	
	public boolean alteraPardais(EquipamentoEletrBean auxClasse, Vector vErro, Connection conn) throws sys.DaoException {
		
		boolean existe = false;
		
		try {
			Statement stmt = conn.createStatement();
			
			String sCmd = "UPDATE TSMI_EQUIPAMENTO_ELETR set ";
			sCmd += "LATITUDE = '" + auxClasse.getLatitude() + "', ";
			sCmd += "LONGITUDE = '" + auxClasse.getLongitude() + "', ";
			sCmd += "SENTIDO = '" + auxClasse.getSentido() + "' ";
			sCmd += "where ID_EQUIPAMENTO_ELETR = " + auxClasse.getIdEquipamentoEletr();
			stmt.execute(sCmd);
			
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			vErro.addElement("Erro de SQL1 na Altera��o: "+ e.getMessage() +"\n");
			existe = false;
		} catch (Exception ex) {
			vErro.addElement("Erro Execep1 na Altera��o: \n");
			existe = false;
		}
		return existe;
	}	
	
}
