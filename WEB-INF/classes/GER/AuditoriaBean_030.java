package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA TABELA DE MARCA MODELO <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 121<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_030 extends AuditoriaBean {

	private String codMarcaModelo  	;
	private String tipoAcao			;
	private String indContinuidade 	;
	private String codOrgaoAtuacao	;
	private String codOrgoaLotacao	;
	private String nomUserName		;


	public AuditoriaBean_030() throws sys.BeanException {

		codMarcaModelo       = "";
		tipoAcao  			 = "";
		indContinuidade  	 = "";
		codOrgaoAtuacao      = "";
		codOrgoaLotacao      = "";
		nomUserName      	 = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",45);
		 OrgaoBean Orgao = new OrgaoBean ();


		 this.codMarcaModelo    = reg.substring(0,6);
	     this.tipoAcao 			= reg.substring(6,7);
	     this.indContinuidade 	= reg.substring(7,13);
	     this.codOrgaoAtuacao 	= reg.substring(13,19);
	     this.codOrgoaLotacao 	= reg.substring(19,25);
	     this.nomUserName 		= reg.substring(25,45);
	     
	     if(!"".equals(this.codOrgoaLotacao.trim())){
	     Orgao.Le_Orgao(codOrgoaLotacao,0);
	     this.setSigOrgao(Orgao.getSigOrgao());
	     }

	}

	public String getcodMarcaModelo() {
		return codMarcaModelo;
	}

	public String getTipoAcao() {
		return tipoAcao;
	}

	public String getIndContinuidade() {
		return indContinuidade;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public String getNomUserName() {

		return nomUserName;
	}

	public String getDscAcao() {
		String dsc = "";
        if (this.tipoAcao.equals("4")) dsc = "CONSULTA SIMPLES";
        if (this.tipoAcao.equals("5")) dsc = "CONSULTA GERAL";
     return dsc;   
	}


}