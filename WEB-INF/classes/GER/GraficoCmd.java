package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GraficoCmd extends sys.Command{
	
	private static final String jspPadrao = "/GER/GrafInfDiarias.jsp";
	private String next,html;
	

	public GraficoCmd() {
			next = jspPadrao;
	}

	public GraficoCmd(String next) {
			this.next = next;
	}
	
	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws sys.CommandException {
		String nextRetorno = jspPadrao;		
		try {
						
			String verGrafico = req.getParameter("verGrafico");
			if (verGrafico == null) verGrafico = "N";
			
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			
			if (acao.equals("Grafico")) {
				verGrafico = "S";		
			}   
			
			if (acao.equals("Imprime")) {
				nextRetorno = "/GER/GrafInfDiariasImp.jsp"; 
				verGrafico = "S";				
			}  
			
			if(acao.equals("Retornar")){
				verGrafico = "N"; 
			}
			
			req.setAttribute("verGrafico", verGrafico);
			req.setAttribute("html",html);
			
		}
		catch(Exception ue)	{
			throw new sys.CommandException("GraficoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}	
	
	
}
