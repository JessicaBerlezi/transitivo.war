package GER;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ACSS.ParamSistemaBean;
import ACSS.SistemaBean;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Sistema<br>
* <b>Description:</b>  Comando para Manter os Parametros por Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Michel
* @version 1.0
* @Updates
*/

public class ProcessoSistemaCmd extends sys.Command {

	private static final String jspPadrao = "/GER/ProcessoSistema.jsp";
	private String next;

	public ProcessoSistemaCmd() {
		next = jspPadrao;
	}

	public ProcessoSistemaCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException{
		String nextRetorno = next;
		try {  
			// cria os Beans do Usuario, se n�o existir
			SistemaBean SistId = (SistemaBean) req.getAttribute("SistId");
			if (SistId == null)	SistId = new SistemaBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";

			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "";

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)
				atualizarDependente = "N";

			SistId.Le_Sistema(codSistema, 0);
			SistId.setAtualizarDependente(atualizarDependente);

			if ("buscaSistema".indexOf(acao) >= 0) {
				SistId.getProcessos(20, 6);
				SistId.setAtualizarDependente("S");
			}

			if (acao.compareTo("G") == 0) {
				String codSistemaCp = req.getParameter("codSistemaCp");
				ParamSistemaBean paramId = new ParamSistemaBean();
				paramId.setCodSistemaCp(codSistemaCp);
				paramId.CopiaParamSistema(SistId, paramId);
			}

			Vector vErro = new Vector();

			if (acao.compareTo("A") == 0) {
				String[] nomProcesso = req.getParameterValues("nomProcesso");
				if (nomProcesso == null) nomProcesso = new String[0];

				String[] nomDescricao = req.getParameterValues("nomDescricao");
				if (nomDescricao == null) nomDescricao = new String[0];

				String[] codProcesso = req.getParameterValues("codProcesso");
				if (codProcesso == null) codProcesso = new String[0];

				vErro =	isProcessos(nomProcesso,nomDescricao,codProcesso);
				if (vErro.size() == 0) {
					Vector processo = new Vector();
					for (int i = 0; i < nomProcesso.length; i++) {
						ProcessoSistemaBean myProcesso = new ProcessoSistemaBean();
						myProcesso.setNomProcesso(nomProcesso[i]);
						myProcesso.setNomDescricao(nomDescricao[i]);
						myProcesso.setCodSistema(codSistema);
						myProcesso.setCodProcesso(codProcesso[i]);
						processo.addElement(myProcesso);
					}
					SistId.setProcessos(processo);
					SistId.isInsertProcessos();
					SistId.getProcessos(20, 6);
				}
				else
					SistId.setMsgErro(vErro);
			}
			
		    if(acao.compareTo("I") == 0){
				req.setAttribute("vProcesso",SistId.getProcessos(0,0));
				nextRetorno = "/GER/ProcessoSistemaImp.jsp";
		    }
		
			req.setAttribute("SistId", SistId);
		}
		catch (Exception ue) {
			throw new sys.CommandException(
				"ProcessoSistemaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private Vector isProcessos(String[] nomProcesso, String[] nomDescricao,
		String[] codProcesso) {
		Vector vErro = new Vector();
				
		if ((nomProcesso.length != nomDescricao.length)
			|| (nomDescricao.length != codProcesso.length))
			vErro.addElement("Processos inconsistentes");
		return vErro;
	}
}