package GER;

public class GrafDistribuicaoBean extends GraficoBean {

	public GrafDistribuicaoBean(){
		super();
	}
	
	public void criaGrafico(){
		setLargura(670);
		setAltura(250);
		geraGrafico(BARRAQUINTUPLA,"distribuicao");		
	}
}
