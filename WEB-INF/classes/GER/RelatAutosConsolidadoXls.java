package GER;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelatAutosConsolidadoXls {
	
	public void write(RelatAutosPorOrgaoBean relatAutosPorOrgaoBean, String titulo, String arquivo) throws IOException, WriteException
	{
		int seq = 3;
		int celulaTitulo = 0;
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio de Autos Consolidado", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 7/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
            
            // T�tulo do Relat�rio
            label = new Label(0, 2, "SEQ", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(0, 5);
            
            label = new Label(1, 2, "�RG�O", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(1, 70);
            
            label = new Label(2, 2, "AUTUA", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(2, 20);
            
            label = new Label(3, 2, "SMIT DOL", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(3, 20);
            
            label = new Label(4, 2, "QTD MR", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(4, 20);
            
            label = new Label(5, 2, "QTD DOL", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(5, 20);
            
            label = new Label(6, 2, "QTD DOL - OUTROS", formatoTituloCampos); 
            sheet.addCell(label);
            sheet.setColumnView(6, 20);
            
			Iterator it = relatAutosPorOrgaoBean.getDados().iterator();
            RelatAutosPorOrgaoBean relatorioImp  = new RelatAutosPorOrgaoBean();
			if( relatAutosPorOrgaoBean.getDados().size()>0 ) { 
				while (it.hasNext()) {
					   relatorioImp = (RelatAutosPorOrgaoBean)it.next() ;
                       //Linhas	do Relat�rio				   
					   label = new Label(0, seq, String.valueOf(seq-2)); 
					   sheet.addCell(label);
					   
					   label = new Label(1, seq, relatorioImp.getCodOrgao()+" - "+relatorioImp.getSigOrgao()+" - "+relatorioImp.getNomOrgao()); 
					   sheet.addCell(label);
					   
					   label = new Label(2, seq, relatorioImp.getSitAutua()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, seq, relatorioImp.getIndSmitDol()); 
					   sheet.addCell(label);
					                          
				       label = new Label(4, seq,relatorioImp.formataIntero(relatorioImp.getQtdMrOrg()) ); 
				       sheet.addCell(label);

                       label = new Label(5, seq,relatorioImp.formataIntero(relatorioImp.getQtdDolOrg()) ); 
                       sheet.addCell(label);
                       
                       label = new Label(6, seq,relatorioImp.formataIntero(relatorioImp.getQtdDolOutros())+" - "+relatorioImp.getOrigemDolOutros()) ; 
                       sheet.addCell(label);
					   seq++;
				}
                label = new Label(seq,5,relatAutosPorOrgaoBean.formataIntero(relatAutosPorOrgaoBean.getTotalMrGeral())) ;
                sheet.addCell(label);
                label = new Label(seq,6,relatAutosPorOrgaoBean.formataIntero(relatAutosPorOrgaoBean.getTotalDolGeral())) ;
			}
			workbook.write(); 
			workbook.close();
			
			
		}catch (Exception e) {	
			throw new IOException("GeraXlsRelatAutosConsolidados: " + e.getMessage());	}
	
	}

}
