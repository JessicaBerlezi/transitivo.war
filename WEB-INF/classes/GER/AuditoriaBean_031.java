package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA TABELA DE TIPO/CORES/ESPECIE/CATEGORIA <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 122/124/125/126<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_031 extends AuditoriaBean {

	private String codigo  	    ;
	private String tipoAcao			;
	private String indContinuidade 	;
	private String codOrgaoAtuacao	;
	private String codOrgoaLotacao	;
	private String nomUserName		;


	public AuditoriaBean_031() throws sys.BeanException {

		codigo               = "";
		tipoAcao  			 = "";
		indContinuidade  	 = "";
		codOrgaoAtuacao      = "";
		codOrgoaLotacao      = "";
		nomUserName      	 = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",39);
		 OrgaoBean Orgao = new OrgaoBean ();


		 this.codigo            = reg.substring(0,3);
	     this.tipoAcao 			= reg.substring(3,4);
	     this.indContinuidade 	= reg.substring(4,7);
	     this.codOrgaoAtuacao 	= reg.substring(7,13);
	     this.codOrgoaLotacao 	= reg.substring(13,19);
	     this.nomUserName 		= reg.substring(19,39);
	     
	     if(!"".equals(this.codOrgoaLotacao.trim())){
	     Orgao.Le_Orgao(codOrgoaLotacao,0);
	     this.setSigOrgao(Orgao.getSigOrgao());
	     }

	}

	public String getCodigo() {
		return codigo;
	}

	public String getTipoAcao() {
		return tipoAcao;
	}

	public String getIndContinuidade() {
		return indContinuidade;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public String getNomUserName() {

		return nomUserName;
	}

	public String getDscAcao() {
		String dsc = "";
        if (this.tipoAcao.equals("4")) dsc = "CONSULTA SIMPLES";
        if (this.tipoAcao.equals("5")) dsc = "CONSULTA GERAL";
     return dsc;   
	}


}