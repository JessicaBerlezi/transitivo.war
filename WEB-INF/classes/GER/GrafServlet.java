/*
 * Created on 05/01/2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package GER;

/**
 * @author jheitor
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartUtilities;

public class GrafServlet extends HttpServlet {

	
	GraficoBean myGraf = null;

	public void init() throws ServletException {
	}

	public void   doGet(HttpServletRequest httpservletrequest, HttpServletResponse httpservletresponse)
	throws ServletException, IOException
	{
	 	
		httpservletresponse.setContentType("image/jpeg");
		javax.servlet.ServletOutputStream servletoutputstream = httpservletresponse.getOutputStream();
		
		
		try{
	   		carregaDados(httpservletrequest);
		}
		catch(Exception ue){	
		}
		
		myGraf.criaGrafico();
	    ChartUtilities.writeChartAsJPEG(servletoutputstream, myGraf.obtemGrafico(), myGraf.getLargura(), myGraf.getAltura());	
	    servletoutputstream.close();  	
	}
	
	public void carregaDados(HttpServletRequest httpservletrequest) throws sys.BeanException{
			
		try {
			
			Dao dao = Dao.getInstance();
			
		 	if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0280")){
				myGraf = new GrafDemonstSaldosTotBean();	
				atualizaBean(myGraf,httpservletrequest);
				dao.demonstSaldosTotal(myGraf);
			}
			
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0240")){
				myGraf = new GrafInfDiariasBean();	
				atualizaBean(myGraf,httpservletrequest);
				dao.criaGrafico(myGraf);
			}
			
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0230")){
				myGraf = new GrafRegDiarioBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.criaRegDiario(myGraf);
			}
			
			
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0271")){
				myGraf = new GrafInfPontuacaoBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.infPontuacao(myGraf); 
			}
			
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0245")){
				myGraf = new GrafTipoInfracaoBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.criaGraficoTipoInfracao(myGraf);
			}				
				
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0250")){
				myGraf = new GrafDemonstSaldosAutoBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.demonstSaldosAuto(myGraf);
			}	
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0260")){
				myGraf = new GrafDemonstSaldosPDBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.demonstSaldosPD(myGraf);
			}
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0265")){
				myGraf = new GrafDemonstSaldosTJBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.demonstSaldosTJ(myGraf);
			}
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0285")){
				myGraf = new GrafCancelAjusteBean();
				atualizaBean(myGraf,httpservletrequest);
				dao.cancelAjuste(myGraf);
			}
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER910")){
				myGraf = new GrafProdProcessoBean();
				atualizaBeanProdProcesso(myGraf,httpservletrequest);
				dao.prodProcesso(myGraf);																
			}
			else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0915")){
				myGraf = new GrafDistribuicaoBean();
				atualizaBeanDistribProcesso(myGraf,httpservletrequest);
				dao.DistribuicaoProcesso(myGraf);		
			}
            else if(httpservletrequest.getParameter("j_sigFuncao").equals("GER0286")){
                myGraf = new GrafProcAbertosBean();
                atualizaBeanProcAberto(myGraf,httpservletrequest);
                dao.criaGrafProcAbertos(myGraf);       
            }
		}
		catch (Exception ue) { 
			throw new sys.BeanException("GrafServlet: " + ue.getMessage());
		}		
		return;		
	}
	
	public void atualizaBean(GraficoBean myGraf,HttpServletRequest httpservletrequest){

        myGraf.setDatInicio(httpservletrequest.getParameter("De"));
		myGraf.setDatFim(httpservletrequest.getParameter("Ate"));				
		myGraf.setCodOrgao(httpservletrequest.getParameter("codOrgao"));
		myGraf.setTodosOrgaos(httpservletrequest.getParameter("TodosOrgaos"));	
	    myGraf.setMyDominio(httpservletrequest.getParameter("Est_Reg"));
		myGraf.setCodMunicipio(httpservletrequest.getParameter("Municipio"));	
		
	}
	
	public void atualizaBeanProdProcesso(GraficoBean myGraf,HttpServletRequest httpservletrequest){
		String anoMes=httpservletrequest.getParameter("anoMes");
		if (anoMes==null) anoMes="01/1900";
		
		myGraf.setDatInicio(anoMes);
		myGraf.setCodOrgao(httpservletrequest.getParameter("codOrgao"));		
		myGraf.setTipoReq(httpservletrequest.getParameter("TPreq"));
		myGraf.setCodUF(httpservletrequest.getParameter("cod_uf"));
		myGraf.setRelatores(((String [])httpservletrequest.getParameterValues("relator")));
			
	}
	
	public void atualizaBeanDistribProcesso(GraficoBean myGraf,HttpServletRequest httpservletrequest){
		String mesAnoIni=httpservletrequest.getParameter("mesAnoIni");
		if (mesAnoIni==null) mesAnoIni="01/1900";
		String mesAnoFim=httpservletrequest.getParameter("mesAnoFim");
		if (mesAnoFim==null) mesAnoFim="01/1900";
		
        myGraf.setDatInicio(mesAnoIni);
		myGraf.setDatFim(mesAnoFim);				
		myGraf.setCodOrgao(httpservletrequest.getParameter("codOrgao"));
		myGraf.setTodosOrgaos(httpservletrequest.getParameter("TodosOrgaos"));
		myGraf.setCodUF(httpservletrequest.getParameter("cod_uf"));
		myGraf.setTipoReq(httpservletrequest.getParameter("TPreq"));
        
	}
    
    public void atualizaBeanProcAberto(GraficoBean myGraf,HttpServletRequest httpservletrequest){
    	String datInicio=httpservletrequest.getParameter("datInicio");
    	if (datInicio==null) datInicio="01/01/1900";
    	String datFim   =httpservletrequest.getParameter("datFim");
    	if (datFim==null) datFim="01/01/1900";    	
      
        myGraf.setDatInicio(datInicio);
        myGraf.setDatFim(datFim);             
        myGraf.setCodOrgao(httpservletrequest.getParameter("codOrgao"));
        myGraf.setCodUF(httpservletrequest.getParameter("cod_uf"));
        myGraf.setTipoReq(httpservletrequest.getParameter("TPreq"));
        
    }
    
	public void destroy() {
	}
}
