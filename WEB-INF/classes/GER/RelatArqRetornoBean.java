package GER;

import java.util.Vector;

import sys.BeanException;
import sys.DaoException;

public class RelatArqRetornoBean extends sys.HtmlPopupBean {

	private String codArquivo;
	private String nomArquivo;
	private String codStatus;
	private String qtdReg;
	private String totalQtdReg;	
	private String qtdRecebido;
	private String totalQtdRecebido;
	private String numProtocolo;
	private String datRecebimento;
	private String nomUsername;
	private String codOrgaoLotacao;

	private String datIni;
	private String datFim;

	private Vector listaArq; 
	private String msgErro;

	public RelatArqRetornoBean() throws sys.BeanException {
		totalQtdReg		 ="";
		totalQtdRecebido ="";
		codArquivo       ="";
		nomArquivo       ="";
		codStatus        ="";
		qtdReg           ="";
		qtdRecebido      ="";
		numProtocolo     ="";
		datRecebimento   ="";
		nomUsername      ="";
		codOrgaoLotacao  ="";
		msgErro          ="";
		datIni           ="";
		datFim           ="";
		listaArq = new Vector(); 
	}
//	--------------------------------------------------------------------------
	public void setTotalQtdReg(String s) {
		this.totalQtdReg = s;
		if( s == null )	this.totalQtdReg = "";
	}
	public String getTotalQtdReg() { return this.totalQtdReg; }
//	--------------------------------------------------------------------------
	public void setTotalQtdRecebido(String s) {
		this.totalQtdRecebido = s;
		if( s == null )	this.totalQtdRecebido = "";
	}
	public String getTotalQtdRecebido() { return this.totalQtdRecebido; }
//	--------------------------------------------------------------------------
	public void setNumProtocolo(String s) {
		this.numProtocolo = s;
		if( s == null )	this.numProtocolo = "";			  
	}
	public String getNumProtocolo() { return sys.Util.lPad(this.numProtocolo, "0", 5); }
//	--------------------------------------------------------------------------
	public void setDatIni(String s) {
		this.datIni = s;
		if( s == null )	this.datIni = "";
	}
	public String getDatIni() { return this.datIni; }
//	--------------------------------------------------------------------------
	public void setDatFim(String s) {
		this.datFim = s;
		if( s == null )	this.datFim = "";
	}
	public String getDatFim() { return this.datFim; }
//	--------------------------------------------------------------------------
	public void setCodArquivo(String s) {
		this.codArquivo = s;
		if( s == null )	this.codArquivo = "";
	}
	public String getCodArquivo() { return this.codArquivo; }
//	--------------------------------------------------------------------------
	public void setNomArquivo(String s) {
		this.nomArquivo = s;
		if (s == null) this.nomArquivo = "";
	}
	public String getNomArquivo() { return this.nomArquivo; }
//	--------------------------------------------------------------------------	
	public void setCodStatus(String s) {
		this.codStatus = s;
		if (s == null) this.codStatus = "";
	}
	public String getCodStatus() { return this.codStatus; }
//	--------------------------------------------------------------------------
	public void setQtdReg(String s) {
		this.qtdReg = s;
		if (s == null) this.qtdReg = "";
	}
	public String getQtdReg() { return this.qtdReg; }
//	--------------------------------------------------------------------------
	public void setQtdRecebido(String s) {
		this.qtdRecebido = s;
		if (s == null) this.qtdRecebido = "";
	}
	public String getQtdRecebido() { return this.qtdRecebido; }
//	--------------------------------------------------------------------------
	public void setDatRecebimento(String s) {
		this.datRecebimento = s;
		if (s == null) this.datRecebimento = "";
	}
	public String getDatRecebimento() {	return this.datRecebimento;	}
//	--------------------------------------------------------------------------
	public void setNomUsername(String s) {
		this.nomUsername = s;
		if (s == null) this.nomUsername = "";
	}
	public String getNomUsername() { return this.nomUsername; }
//	--------------------------------------------------------------------------
	public void setCodOrgaoLotacao(String s) {
		this.codOrgaoLotacao = s;
		if(s == null) this.codOrgaoLotacao = "";
	}
	public String getCodOrgaoLotacao() { return this.codOrgaoLotacao; }
//	--------------------------------------------------------------------------
	public void setMsgErro( Vector vErro ) {
	   for (int j=0; j<vErro.size(); j++)  this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
	   this.msgErro = msgErro;
	}
	public String getMsgErro() {	return this.msgErro; }
//	--------------------------------------------------------------------------
	public void setListaArqs(Vector listaArqs) { 
		this.listaArq = listaArqs; 
	}
	public Vector getListaArqs(){ return this.listaArq; }
	public RelatArqRetornoBean getListaArqs(int i) { 
	  return ( RelatArqRetornoBean )this.listaArq.elementAt(i); 
	}   
//	--------------------------------------------------------------------------
	public boolean ConsultaRelDiario(String datIni,String datFim, Vector vErro ) throws  DaoException, BeanException
	{
		boolean existe = false;
		Dao dao = Dao.getInstance();
		try {
		 	 if(dao.ConsultaRelDiarioRet(this,datIni,datFim)== true) 
		 	    existe = true;
		 	 else
				 vErro.addElement("N�o existe(m) registro(s) entre as datas informadas para forma��o do relat�rio! \n");
			} 
			catch (Exception e) { throw new sys.BeanException( e.getMessage() ); } 
			
		setMsgErro(vErro);
		return existe;	
	}
}