package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - ENVIO PARA D.O<br>
* <b>Description:</b>  Bean Auditoria_019 - Informa��es das transa��es 261,381,388<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_019 extends AuditoriaBean {	
	
	private String txtComplemento1 		;
	private String txtComplemento2 		;
	private String nomUserName			;
	private String codOrgoaLotacao		;
	private String numAutoInfracao  	;
	private String numPlaca				;
	private String codOrgao 			;
	private String txtComplemento3  	;
	private String txtComplemento4  	;	
	
	public AuditoriaBean_019() throws sys.BeanException {
		
		txtComplemento1    	  = "";	
		txtComplemento2       = "";
		nomUserName      	  = "";
		codOrgoaLotacao       = "";
		numAutoInfracao       = "";
		numPlaca  			  = "";
		codOrgao  			  = "";
		txtComplemento3   	  = "";
		txtComplemento4       = "";		
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",110);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
		 this.txtComplemento1 	= reg.substring(0,8);	     
		 this.txtComplemento2 	= reg.substring(8,28);
		 this.nomUserName 		= reg.substring(28,48);
		 this.codOrgoaLotacao 	= reg.substring(48,54);
		 this.numAutoInfracao   = reg.substring(54,66) ;
	     this.numPlaca 			= reg.substring(66,73);	     
	     this.codOrgao 			= reg.substring(73,79);
	     this.txtComplemento3 	= reg.substring(79,80);	     
	     this.txtComplemento4 	= reg.substring(80,110);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }		     
	     
	}
	
	public String getTxtComplemento1() {
		String data = "";
		data = this.txtComplemento1.substring(6,8)+"/"+this.txtComplemento1.substring(4,6)+"/"+
		this.txtComplemento1.substring(0,4);
		return data;
	}	
	public String getTxtComplemento2() {
	
		return txtComplemento2;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}
	public String getNumPlaca() {
		return numPlaca;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getTxtComplemento3() {
		String resultado = this.txtComplemento3;
		if (resultado.equals("D"))
			resultado="Deferido";
		if (resultado.equals("I"))
			resultado="Indeferido";
		return resultado;
	}
	public String getTxtComplemento4() {
		return txtComplemento4;
	}	
}