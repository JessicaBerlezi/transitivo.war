package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        Módulo Gerencial - ATUALIZA DATA DEPUBLICAÇÃO NO D.O<br>
* <b>Description:</b>  Bean Auditoria_021 - Informações das transações 263,383,390<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_020 extends AuditoriaBean {	
	
	private String txtComplemento1 		;	
	private String nomUserName			;
	private String codOrgoaLotacao		;
	private String numAutoInfracao  	;
	private String numPlaca				;
	private String codOrgao 			;
	private String numRequerimento 		;	
	
	public AuditoriaBean_020() throws sys.BeanException {
		
		txtComplemento1    	  = "";			
		nomUserName      	  = "";
		codOrgoaLotacao       = "";
		numAutoInfracao       = "";
		numPlaca  			  = "";
		codOrgao  			  = "";
		numRequerimento       = "";
		
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",89);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	
	    
		 this.txtComplemento1 	= reg.substring(0,8);		 
		 this.nomUserName 		= reg.substring(8,28);
		 this.codOrgoaLotacao 	= reg.substring(28,34);
		 this.numAutoInfracao   = reg.substring(34,46) ;
	     this.numPlaca 			= reg.substring(46,53);	     
	     this.codOrgao 			= reg.substring(53,59);
	     this.numRequerimento 	= reg.substring(59,89);
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}
	
	public String getTxtComplemento1() {
		String data = "";
		data = this.txtComplemento1.substring(6,8)+"/"+this.txtComplemento1.substring(4,6)+"/"+
		this.txtComplemento1.substring(0,4);
		return data;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}
	public String getNumPlaca() {
		return numPlaca;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getNumRequerimento() {
		
		return numRequerimento;
	}	
}