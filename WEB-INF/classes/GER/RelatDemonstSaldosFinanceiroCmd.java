package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Gerencial - Demonstrativo Saldos Financeiro Cmd<br>
* <b>Description:</b>  Mostra o balan�o dos autos nas fases de autua��o, penalidade e Transito Julgado num dado per�odo de tempo. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class RelatDemonstSaldosFinanceiroCmd extends sys.Command {

	private static final String jspPadrao = "/GER/RelatDemonstSaldosFinanceiro.jsp";
	private String next;

	public RelatDemonstSaldosFinanceiroCmd() {
		next = jspPadrao;
	}

	public RelatDemonstSaldosFinanceiroCmd(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			
			RelatDemonstSaldosBean relatDemonstSaldosBean = (RelatDemonstSaldosBean) session.getAttribute("relatDemonstSaldosBean");
			if (relatDemonstSaldosBean == null)relatDemonstSaldosBean = new RelatDemonstSaldosBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			
			String dataIni = req.getParameter("dataIni");
			if (dataIni == null) dataIni = "";
			
			String dataFim = req.getParameter("dataFim");
			if (dataFim == null) dataFim = "";
			
			if (acao.equals("")){
				relatDemonstSaldosBean = new RelatDemonstSaldosBean();				
				String dataHoje = sys.Util.getDataHoje();
				String mes = dataHoje.substring(3,5);
				String ano = dataHoje.substring(6,10);
				dataIni	= "01" + "/" + mes + "/" + ano;
				dataHoje = dataHoje.replaceAll("-","/");  
				relatDemonstSaldosBean.setDataIni(dataIni);
				relatDemonstSaldosBean.setDataFim(dataHoje);				    
			}		
			
			String msg = "";
			if (acao.equals("ImprimeRelatorio")) {
				relatDemonstSaldosBean.setCodOrgao(codOrgao);
				relatDemonstSaldosBean.setDataIni(dataIni);
				relatDemonstSaldosBean.setDataFim(dataFim);			
				
				//Titulo do Relatorio 
				String tituloConsulta = "RELAT�RIO - DEMONSTRATIVO DE SALDOS FINANCEIRO: "+relatDemonstSaldosBean.getDataIni()+"  a  "+relatDemonstSaldosBean.getDataFim();
				req.setAttribute("tituloConsulta", tituloConsulta);
				
			  relatDemonstSaldosBean.consultaDemonstFinanceiro(relatDemonstSaldosBean);
			  if (relatDemonstSaldosBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE PER�ODO." ;
				nextRetorno="/GER/RelatDemonstSaldosFinanceiroImp.jsp";
			}
			session.setAttribute("UsrLogado", UsrLogado);
		  session.setAttribute("relatDemonstSaldosBean",relatDemonstSaldosBean);
		  req.setAttribute("msg", msg);
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatDemonstSaldosFinanceiroCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}