package GER;

import java.util.Date;
import java.util.List;

/**
 * <b>Title: </b> SMIT - GER - Saldo Org�o Bean <br>
 * <b>Description: </b> Saldo de Autos por Org�o <br>
 * <b>Copyright: </b> Copyright (c) 2005 <br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class SaldoOrgaoBean extends sys.HtmlPopupBean {
	
	private Integer codSaldoOrgao;
	private Date datProcessamento;
	private ACSS.OrgaoBean orgao;    
	private REC.StatusBean status;
	private Integer qtdAutos;
	private Date datInfracaoInicial;
	private Date datInfracaoFinal;
	
	public SaldoOrgaoBean() {
		
		super();
		codSaldoOrgao = null;
		datProcessamento = null;
		orgao = null;
		status = null;
		qtdAutos = new Integer(0);
		datInfracaoInicial = null;
		datInfracaoFinal = null;
	}
	
	public Integer getCodSaldoOrgao() {
		return codSaldoOrgao;
	}
	public void setCodSaldoOrgao(Integer codSaldoOrgao) {
		this.codSaldoOrgao = codSaldoOrgao;
	}
	
	public Date getDatInfracaoFinal() {
		return datInfracaoFinal;
	}
	public void setDatInfracaoFinal(Date datInfracaoFinal) {
		this.datInfracaoFinal = datInfracaoFinal;
	}
	
	public Date getDatInfracaoInicial() {
		return datInfracaoInicial;
	}
	public void setDatInfracaoInicial(Date datInfracaoInicial) {
		this.datInfracaoInicial = datInfracaoInicial;
	}
	
	public Date getDatProcessamento() {
		return datProcessamento;
	}
	public void setDatProcessamento(Date datProcessamento) {
		this.datProcessamento = datProcessamento;
	}
	
	public ACSS.OrgaoBean getOrgao() {
		return orgao;
	}
	public void setOrgao(ACSS.OrgaoBean orgao) {
		this.orgao = orgao;
	}
	
	public Integer getQtdAutos() {
		return qtdAutos;
	}
	public void setQtdAutos(Integer qtdAutos) {
		this.qtdAutos = qtdAutos;
	}
	
	public REC.StatusBean getStatus() {
		return status;
	}
	public void setStatus(REC.StatusBean status) {
		this.status = status;
	}
	
	public void grava() throws sys.BeanException {
		try {
			Dao.getInstance().GravaSaldoOrgao(this);
		} catch (sys.DaoException e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public static List consulta(ACSS.OrgaoBean orgao) throws sys.BeanException {        
		try {
			return Dao.getInstance().ConsultaSaldoOrgao(orgao);			     
		} catch (sys.DaoException e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
}