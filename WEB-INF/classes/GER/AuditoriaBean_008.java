package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - AJUSTE AUTOM�TICO DE CR DA VENDA<br>
* <b>Description:</b>  Bean Auditoria_008 - Informa��es da transa��o 412<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_008 extends AuditoriaBean {	
	
	private String nomUserName		;
	private String codOrgoaLotacao	;	
	private String numPlaca			;
	
	
	
	public AuditoriaBean_008() throws sys.BeanException {		
		
		nomUserName      	  = "";
		codOrgoaLotacao       = "";		
		numPlaca  			  = "";	
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",510);
		 
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
		 this.nomUserName 		= reg.substring(0,20);
		 this.codOrgoaLotacao 	= reg.substring(20,26);		 
	     this.numPlaca 			= reg.substring(26,33);	  
	    
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public void setCodOrgoaLotacao(String codOrgoaLotacao) {
		this.codOrgoaLotacao = codOrgoaLotacao;
	}

	public String getNomUserName() {
		return nomUserName;
	}

	public void setNomUserName(String nomUserName) {
		this.nomUserName = nomUserName;
	}

	public String getNumPlaca() {
		return numPlaca;
	}

	public void setNumPlaca(String numPlaca) {
		this.numPlaca = numPlaca;
	}

	
	
}