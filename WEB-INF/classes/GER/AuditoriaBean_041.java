package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA C�DIGOS DE RETORNO VEX<br>
* <b>Description:</b>  Bean Auditoria_041 - Informa��es da transa��o 091<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_041 extends AuditoriaBean {	
	
	
	private String codigoRetorno		;	
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String dscRetorno	 		;
	private String indRecebimento		;
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_041() throws sys.BeanException {		
	
		
		codigoRetorno 		= "";		
		tipoAcao 			= "";
		indContinuidade     = "";
		dscRetorno 			= "";
		indRecebimento		= "";
		codOrgao  			= "";
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",58);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	
	    
		 		
		 this.codigoRetorno		= reg.substring(0,2)     ;
	     this.tipoAcao 			= reg.substring(2,3)     ;
	     this.indContinuidade 	= reg.substring(3,5)     ;
	     this.dscRetorno 		= reg.substring(5,25)    ;
	     this.indRecebimento 	= reg.substring(25,26)   ;
	     this.codOrgao 			= reg.substring(26,32)   ;
	     this.codOrgoaLotacao 	= reg.substring(32,38)   ;
	     this.nomUserName 		= reg.substring(38,58)   ;	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }		     
	     
	}
	
	public String getCodigoRetorno() {		
		return codigoRetorno;
	}		
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	
	public String getIndContinuidade() {
		return indContinuidade;
	}
	
	public String getDscRetorno() {
		return dscRetorno;
	}
	public String getIndRecebimento() {
		return indRecebimento;
	}
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	}