package GER;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Vector;

import sys.BeanException;
import REC.GuiaDaoBroker;


public class ResultadoAnualBean extends sys.HtmlPopupBean {

	private String ano;
	private String mes;
	private String codEstado;
	private String sigorgao;
	private String orgao;
	private String deferidos;
	private String indeferidos;
	private String defPercent;
	private String indefPercent;
	private String todosOrgaos;
	private String tpreq;	
	private String totalGer;
	private String total;
	private String totalMedia;
	private String totalMediaDef;
	private String totMediaInDef;
	private String totalDef;
	private String totalDefPerc;
	private String totalInDefPerc;
	private String totalInDef;
	private List listaMes;	
	private String msgErro;
	
	public ResultadoAnualBean() throws sys.BeanException {
		
		ano             = sys.Util.formatedToday().substring(6,10);
		mes             ="";
		codEstado       ="";
		sigorgao        ="";
		orgao           ="";
		deferidos       ="0";		
		indeferidos     ="0";
		defPercent      ="0";
		indefPercent    ="0";
		todosOrgaos     ="";
		totalGer        ="0";
		total           ="0";
		totalMedia      ="0";
		totalMediaDef   ="0";
		totMediaInDef   ="0";
		totalDef        ="0";
		totalDefPerc    ="0";
		totalInDef      ="0";
		totalInDefPerc  ="0";
		tpreq           ="";		
		msgErro         ="";
		listaMes        = new ArrayList(); 
	}

//	--------------------------------------------------------------------------
	public void setListaMes(List listaMes) {
		this.listaMes = listaMes;
	}

	public List getListaMes() {
		return this.listaMes;}
//	--------------------------------------------------------------------------
	public void setAno(String ano) {
		this.ano = ano;
		if( ano == null )	this.ano = sys.Util.formatedToday().substring(6,10);
	}
	public String getAno() { return this.ano; }

//	--------------------------------------------------------------------------
	public void setMes(String mes) {
		this.mes = mes;
		if( mes == null )	this.mes = "";
	}
	public String getMes() { return this.mes; }

//	--------------------------------------------------------------------------
	public void setCodEstado(String codEstado) {
		this.codEstado = codEstado;
		if( codEstado == null )	this.codEstado = "";
	}
	public String getCodEstado() { return this.codEstado; }

//	--------------------------------------------------------------------------
	public void setOrgao(String orgao) {
		this.orgao = orgao;
		if( orgao == null )	this.orgao = "";
	}
	public String getOrgao() { return this.orgao; }

//	--------------------------------------------------------------------------
	public void setSigOrgao(String sigorgao) {
		this.sigorgao = sigorgao;
		if( sigorgao == null )	this.sigorgao = "";
	}
	public String getSigOrgao() { return this.sigorgao; }

	//	--------------------------------------------------------------------------
	public void setIndefPercent(String indefPercent) {
		this.indefPercent = indefPercent;
		if( indefPercent == null )	this.indefPercent = "0";
	}
	public String getIndefPercent() { return this.indefPercent; }

//	--------------------------------------------------------------------------
	public void setTotalGer(String totalGer) {
		this.totalGer = totalGer;
		if( totalGer == null )	this.totalGer = "0";
	}
	public String geTotalGer() { return this.totalGer; }

//	--------------------------------------------------------------------------
	public void setTotalMediaDef(String totalMediaDef) {
		this.totalMediaDef = totalMediaDef;
		if( totalMediaDef == null )	this.totalMediaDef = "0";
	}
	public String geTotalMediaDef() { return this.totalMediaDef; }
	
//	--------------------------------------------------------------------------
	public void setTotMediaInDef(String totMediaInDef) {
		this.totMediaInDef = totMediaInDef;
		if( totMediaInDef == null )	this.totMediaInDef = "0";
	}
	public String geTotMediaInDef() { return this.totMediaInDef; }

//	--------------------------------------------------------------------------
	public void setTotalMedia(String totalMedia) {
		this.totalMedia = totalMedia;
		if( totalMedia == null )	this.totalMedia = "0";
	}
	public String geTotalMedia() { return this.totalMedia; }
	
//	--------------------------------------------------------------------------
	public void setTotal(String total) {
		this.total = total;
		if( total == null )	this.total = "0";
	}
	public String geTotal() { return this.total; }
	
//	--------------------------------------------------------------------------
	public void setTotalDef(String totalDef) {
		this.totalDef = totalDef;
		if( totalDef == null )	this.totalDef = "0";
	}
	public String geTotalDef() { return this.totalDef; }

//	--------------------------------------------------------------------------
	public void setTotalDefPerc(String totalDefPerc) {
		this.totalDefPerc = totalDefPerc;
		if( totalDefPerc == null )	this.totalDefPerc = "0";
	}
	public String geTotalDefPerc() { return this.totalDefPerc; }

	
//	--------------------------------------------------------------------------
	public void setTotalInDef(String totalInDef) {
		this.totalInDef = totalInDef;
		if( totalInDef == null )	this.totalInDef = "0";
	}
	public String geTotalInDef() { return this.totalInDef; }	
	
//	--------------------------------------------------------------------------
	public void setTotalInDefPerc(String totalInDefPerc) {
		this.totalInDefPerc = totalInDefPerc;
		if( totalInDefPerc == null )	this.totalInDefPerc = "0";
	}
	public String geTotalInDefPerc() { return this.totalInDefPerc; }	
	//	--------------------------------------------------------------------------
	public void setDefPercent(String defPercent) {
		this.defPercent = defPercent;
		if( defPercent == null )	this.defPercent = "0";
	}
	public String getDefPercent() { return this.defPercent; }
	
//	--------------------------------------------------------------------------
	public void setDeferidos(String deferidos) {
		this.deferidos = deferidos;
		if (deferidos == null) this.deferidos = "0";
	}
	public String getDeferidos() { return this.deferidos; }
//	--------------------------------------------------------------------------	
	public void setTodosOrgaos(String todosOrgaos) {
		this.todosOrgaos = todosOrgaos;
		if (todosOrgaos == null) this.todosOrgaos = "";
	}
	public String getTodosOrgaos() { return this.todosOrgaos; }

//	--------------------------------------------------------------------------	
	public void setIndeferidos(String indeferidos) {
		this.indeferidos = indeferidos;
		if (indeferidos == null) this.indeferidos = "0";
	}
	public String getIndeferidos() { return this.indeferidos; }	
//	--------------------------------------------------------------------------	
	public void setTpreq(String tpreq) {
		this.tpreq = tpreq;
		if (tpreq == null) this.tpreq = "";
	}
	public String getTpreq() { return this.tpreq; }	
//	--------------------------------------------------------------------------
	public void setMsgErro( Vector vErro ) {
	   for (int j=0; j<vErro.size(); j++)  this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
	   this.msgErro = msgErro;
	}
	public String getMsgErro() {	return this.msgErro; }

//	--------------------------------------------------------------------------
	public boolean CarregaEstatisticas()   {
		boolean retorno = true;
		
		try  {
			Dao dao = Dao.getInstance();		   
			dao.CarregaEstatisticas(this);	        
		}
		catch (Exception e) {			
			retorno = false;
		}//fim do catch	   
		
		return retorno;
	}  
	
	public String formataInteiro (long valor) throws  DaoException, BeanException{	
		String valorFormatado ="";
		if (valor > 0){
		NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
		formato = new DecimalFormat(",#00") ;
	    valorFormatado = formato.format(valor); 
	    }
		else
			valorFormatado=String.valueOf(valor);
	    return valorFormatado;
	}
	
	/*Luciana*/
	public void getGuias(ResultadoAnualBean ResultadoAnualBeanId,String tipoResultado,
			String datInicio,String datFim,REC.GuiaDistribuicaoBean guiaBean) throws sys.BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.getGuias(ResultadoAnualBeanId,tipoResultado,datInicio,datFim,guiaBean);
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public String getMes(String mes){
		String [] mesesExtenso  = {"","JANEIRO","FEVEREIRO","MAR�O","ABRIL","MAIO","JUNHO","JULHO","AGOSTO","SETEMBRO","OUTUBRO","NOVEMBRO","DEZEMBRO"};
		for (int i=0;i< mesesExtenso.length;i++)
	    {
	       if(mesesExtenso[i] .equals(mes.toUpperCase()))
	       	{
	       	  mes = Integer.toString(i);
	       	  break;
	       	}
	    }
		return (mes.length()==1?"0"+mes:mes);
	}
	
	public String getUltimoDia(String mes,String ano){
		Calendar calendar = new GregorianCalendar();
		SimpleDateFormat df = new SimpleDateFormat("dd/mm/yyyy");
		int anoCorrente = Integer.parseInt(ano);
		int mesSelect = 0;
		if(mes.substring(0,1).equals("0"))
			mesSelect = Integer.parseInt(mes.substring(1))-1;
		else mesSelect = Integer.parseInt(mes)-1;
		
		calendar.set(Calendar.YEAR,anoCorrente);
		calendar.set(Calendar.MONTH,mesSelect);
		calendar.set(Calendar.DAY_OF_MONTH,calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		Date data = calendar.getTime();
		
		String ultimoDia = df.format(data);
		return ultimoDia.substring(0,2);
	}


}