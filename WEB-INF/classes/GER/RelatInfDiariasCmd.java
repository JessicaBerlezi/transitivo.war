package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Gerencial - Infra��es Diarias Cmd<br>
* <b>Description:</b>  Informa as infra��es di�rias de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class RelatInfDiariasCmd extends sys.Command {

	private static final String jspPadrao = "/GER/RelatInfDiarias.jsp";
	private String next;

	public RelatInfDiariasCmd() {
		next = jspPadrao;
	}

	public RelatInfDiariasCmd(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			
			RelatInfDiariasBean relatInfDiariasBean = (RelatInfDiariasBean) session.getAttribute("relatInfDiariasBean");
			if (relatInfDiariasBean == null)relatInfDiariasBean = new RelatInfDiariasBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			
			String todosOrgaos = req.getParameter("TodosOrgaos");
			if (todosOrgaos == null)todosOrgaos="N";
			
			String dataIni = req.getParameter("dataIni");
			if (dataIni == null) dataIni = "";

			String dataFim = req.getParameter("dataFim");
			if (dataFim == null) dataFim = "";
			
			String msg = "";
			relatInfDiariasBean.setCodOrgao(codOrgao);
			relatInfDiariasBean.setDataIni(dataIni);
			relatInfDiariasBean.setDataFim(dataFim);	
			relatInfDiariasBean.setTodosOrgaos(todosOrgaos);		
			
			if (acao.equals("ImprimeRelatorio")) {
				//Titulo do Relatorio 
				String tituloConsulta = "RELAT�RIO - INFRA��ES DI�RIAS DE : "+dataIni+ "AT� "+dataFim;
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				relatInfDiariasBean.consultaRegDiario(relatInfDiariasBean);
				
				if (relatInfDiariasBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE DIA." ;	

			 	nextRetorno="/GER/RelatInfDiariasImp.jsp";
			}

			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("msg", msg);
		  session.setAttribute("relatInfDiariasBean",relatInfDiariasBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatInfDiariasBeanCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}