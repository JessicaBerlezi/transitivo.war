package GER;

import java.util.Date;

public class RequerimentoResultado {
	
	private String 	codigoUf;
	private String 	codigoOrgao;
	private String 	textoOrgao;
	private String 	codigoTipoSolicitacao;
	private String 	codigoStatusRequerimento;
	private Date 	dataResultado;
	private Integer quantidadeDeferidos;
	private Integer quantidadeIndeferidos;
	private String 	codigoInfracao;
	private String 	textoInfracao;
	private String 	textoArtigo;

	
	
	public String getCodigoUf() {
		return codigoUf;
	}
	public void setCodigoUf(String codigoUf) {
		this.codigoUf = codigoUf;
	}
	public String getCodigoOrgao() {
		return codigoOrgao;
	}
	public void setCodigoOrgao(String codigoOrgao) {
		this.codigoOrgao = codigoOrgao;
	}
	public String getTextoOrgao() {
		return textoOrgao;
	}
	public void setTextoOrgao(String textoOrgao) {
		this.textoOrgao = textoOrgao;
	}
	public String getCodigoTipoSolicitacao() {
		return codigoTipoSolicitacao;
	}
	public void setCodigoTipoSolicitacao(String codigoTipoSolicitacao) {
		this.codigoTipoSolicitacao = codigoTipoSolicitacao;
	}
	public String getCodigoStatusRequerimento() {
		return codigoStatusRequerimento;
	}
	public void setCodigoStatusRequerimento(String codigoStatusRequerimento) {
		this.codigoStatusRequerimento = codigoStatusRequerimento;
	}
	public Date getDataResultado() {
		return dataResultado;
	}
	public void setDataResultado(Date dataResultado) {
		this.dataResultado = dataResultado;
	}
	public Integer getQuantidadeDeferidos() {
		return quantidadeDeferidos;
	}
	public void setQuantidadeDeferidos(Integer quantidadeDeferidos) {
		this.quantidadeDeferidos = quantidadeDeferidos;
	}
	public Integer getQuantidadeIndeferidos() {
		return quantidadeIndeferidos;
	}
	public void setQuantidadeIndeferidos(Integer quantidadeIndeferidos) {
		this.quantidadeIndeferidos = quantidadeIndeferidos;
	}
	public String getCodigoInfracao() {
		return codigoInfracao;
	}
	public void setCodigoInfracao(String codigoInfracao) {
		this.codigoInfracao = codigoInfracao;
	}
	public String getTextoInfracao() {
		return textoInfracao;
	}
	public void setTextoInfracao(String textoInfracao) {
		this.textoInfracao = textoInfracao;
	}
	public String getTextoArtigo() {
		return textoArtigo;
	}
	public void setTextoArtigo(String textoArtigo) {
		this.textoArtigo = textoArtigo;
	}

}