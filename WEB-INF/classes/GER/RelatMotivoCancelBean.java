package GER;

import java.util.ArrayList;

import sys.DaoException;

public class RelatMotivoCancelBean extends sys.HtmlPopupBean {
    
    public static final String COD_MOTIVO = "'1','2','3'"; 


    private ArrayList dados;
    
    private ArrayList total;

    private String codOrgao;

    private String sigOrgao;

    private String dataIni;

    private String dataFim;

    private String tpreq;

    private String codRelator;

    private String nomRelator;

    private String txtArtigo;

    private String codInfracao;

    private String datInfracao;

    private String numProcesso;

    private String numAutoInfracao;

    private String codMotivoDef;

    private String matAgente;

    private String codJunta;
    
    private String sigJunta;
    
    private String nomMotivoDef;
    
    private String totMotivoDef;
    

    public RelatMotivoCancelBean() throws sys.BeanException {
        this.dados = new ArrayList();
        this.total = new ArrayList();
        this.codOrgao = "";
        this.sigOrgao = "";
        this.dataIni = sys.Util.formatedToday().substring(0, 10);
        this.dataFim = sys.Util.formatedToday().substring(0, 10);
        this.tpreq = "";
        this.codRelator = "";
        this.nomRelator = "";
        this.txtArtigo = "";
        this.codInfracao = "";
        this.datInfracao = "";
        this.numProcesso = "";
        this.numAutoInfracao = "";
        this.nomMotivoDef = "";
        this.codMotivoDef = "";
        this.matAgente = "";
        this.codJunta = "";
        this.sigJunta = "";
        this.totMotivoDef = "";
        
    }

    public void setCodOrgao(String codOrgao) {
        this.codOrgao = codOrgao;
        if (codOrgao == null)
            this.codOrgao = "-";
    }

    public String getCodOrgao() {
        return this.codOrgao;
    }

    public void setSigOrgao(String sigOrgao) {
        this.sigOrgao = sigOrgao;
        if (sigOrgao == null)
            this.sigOrgao = "-";
    }

    public String getSigOrgao() {
        return this.sigOrgao;
    }

    public void setDataIni(String dataIni) {
        this.dataIni = dataIni;
        if (dataIni == null)
            this.dataIni = "-";
    }

    public String getDataIni() {
        return this.dataIni;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
        if (dataFim == null)
            this.dataFim = "-";
    }

    public String getDataFim() {
        return this.dataFim;
    }

    public void setTpreq(String tpreq) {
        if (tpreq == null)
            this.tpreq = "-";
        else
            this.tpreq = tpreq;
    }

    public String getTpreq() {
        return tpreq;
    }

    public void setCodRelator(String codRelator) {
        if (codRelator == null)
            this.codRelator = "-";
        else
            this.codRelator = codRelator;
    }

    public String getCodRelator() {
        return codRelator;
    }

    public void setCodInfracao(String codInfracao) {
        if (codInfracao == null)
            this.codInfracao = "-";
        else
            this.codInfracao = codInfracao;
    }

    public String getCodInfracao() {
        return codInfracao;
    }

    public void setNomRelator(String nomRelator) {
        if (nomRelator == null)
            this.nomRelator = "-";
        else
            this.nomRelator = nomRelator;
    }

    public String getNomRelator() {
        return nomRelator;
    }

    public void setNumProcesso(String numProcesso) {
        if (numProcesso == null)
            this.numProcesso = "-";
        else
            this.numProcesso = numProcesso;
    }

    public String getNumProcesso() {
        return numProcesso;
    }

    public void setDados(ArrayList dados) {
        this.dados = dados;
    }

    public ArrayList getDados() {
        return this.dados;
    }

    public void setCodMotivoDef(String codMotivoDef) {
        if (codMotivoDef == null)
            codMotivoDef = "-";
        else
            this.codMotivoDef = codMotivoDef;

    }

    public String getCodMotivoDef() {
        return codMotivoDef;
    }

    public void setNomMotivoDef(String nomMotivoDef){
        if (nomMotivoDef == null)
            nomMotivoDef = "-";
        else
            this.nomMotivoDef = nomMotivoDef;
    }
    
    public String getNomMotivoDef() {
        String nome = "";
        if (this.codMotivoDef.equals("1"))
            nome = "Inconsistência do AI";
        else if (this.codMotivoDef.equals("2"))
            nome = "Notificação";
        else if (this.codMotivoDef.equals("3"))
            nome = "Alegações de Defesa";
        else if (this.codMotivoDef.equals("4"))
            nome = "Outros";
        else nome = "Todos";
        return nome;
    }

    public void setCodJunta(String codJunta) {
        if(codJunta == null)
            codJunta = "-";
        else
            this.codJunta = codJunta;
    }

    public String getCodJunta() {
        return codJunta;
    }
    
    public void setSigJunta(String sigJunta) {
        if(sigJunta == null)
            sigJunta = "-";
        else
            this.sigJunta = sigJunta;
    }
    
    public String getSigJunta() {
        return sigJunta;
    }

    public void setDatInfracao(String datInfracao) {
        if(datInfracao == null)
            datInfracao = "-";
        else
          this.datInfracao = datInfracao;
    }

    public String getDatInfracao() {
        return datInfracao;
    }

    public void setMatAgente(String matAgente) {
        if(matAgente == null)
            matAgente = "-";
        else
            this.matAgente = matAgente;
    }

    public String getMatAgente() {
        return matAgente;
    }

    public void setNumAutoInfracao(String numAutoInfracao) {
        if(numAutoInfracao == null)
            numAutoInfracao = "-";
        else
            this.numAutoInfracao = numAutoInfracao;
    }

    public String getNumAutoInfracao() {
        return numAutoInfracao;
    }

    public void setTxtArtigo(String txtArtigo) {
        if(txtArtigo == null)
            txtArtigo = "-";
        else
            this.txtArtigo = txtArtigo;
    }

    public String getTxtArtigo() {
        return txtArtigo;
    }
    
    
    public void setTotMorivoDef(String totMotivoDef) {
        if(totMotivoDef == null) 
          this.totMotivoDef = "";
        else 
          this.totMotivoDef = totMotivoDef;
        
    }
    
    public String getTotMorivoDef() {
        return totMotivoDef;
    }


    public void setTotal(ArrayList total) {
        this.total = total;
    }
    
    public ArrayList getTotal() {
        return total;
    }



    public void getAutosCancel(RelatMotivoCancelBean RelatBeanId)
            throws  DaoException {
        DaoControle dao;
        try {
            dao = DaoControle.getInstance();
            dao.getAutosCancel(RelatBeanId);
        } catch (DaoException e) {
            throw new DaoException("Erro:" + e.getMessage());
        }
    }

    public void getTotalAutosCancel(RelatMotivoCancelBean RelatBeanId)
    throws  DaoException {
        DaoControle dao;
        try {
            dao = DaoControle.getInstance();
            dao.getTotalAutosCancel(RelatBeanId);
        } catch (DaoException e) {
            throw new DaoException("Erro:" + e.getMessage());
        }
}

}