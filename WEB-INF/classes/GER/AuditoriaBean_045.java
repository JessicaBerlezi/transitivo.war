package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE MOTIVO DE REJEITO<br>
* <b>Description:</b>  Bean Auditoria_045 - Informa��es da transa��o 095<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_045 extends AuditoriaBean {	
	
	
	private String codigoMotivo			;	
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String dscMotivo	 		;	
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_045() throws sys.BeanException {		
	
		
		codigoMotivo 		= "";		
		tipoAcao 			= "";
		indContinuidade     = "";
		dscMotivo 			= "";		
		codOrgao  			= "";
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",67);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	
	    
		 		
		 this.codigoMotivo		= reg.substring(0,2)     ;
	     this.tipoAcao 			= reg.substring(2,3)     ;
	     this.indContinuidade 	= reg.substring(3,5)     ;
	     this.dscMotivo 		= reg.substring(5,35)    ;	     
	     this.codOrgao 			= reg.substring(35,41)   ;
	     this.codOrgoaLotacao 	= reg.substring(41,47)   ;
	     this.nomUserName 		= reg.substring(47,67)   ;	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }		     
	     
	}
	
	public String getCodigoMotivo() {		
		return codigoMotivo;
	}		
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDscMotivo() {
		return dscMotivo;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	}