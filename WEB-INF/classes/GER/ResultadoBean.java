package GER;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Vector;

import sys.BeanException;
import sys.DaoException;

public class ResultadoBean extends GER.EstatisticaArtigoBean{

	private String tipoJunta;
	private String sigJunta;
	private String defmembro1;
	private String indefmembro1;
	private String defmembro2;
	private String indefmembro2;
	private String defmembro3;
	private String indefmembro3;
	private String defmembro4;
	private String indefmembro4;
	private String codRelator;
	private String nomRelator;
	private String linhaPreenchida;
	
	private String procDistribuido;
	private String valUnitario;
	private String totalMediaDistribuidos;
	private String totalProc;
	private String totalMediaProc;
	private String valTotal;
	
	private int qtdRegJunta;
	

	private ArrayList relatores;
	private ArrayList artigos;
	private ArrayList dadosRelatorio;
	private ArrayList totalGeral;
	private ArrayList totalArtigo;
	private Vector juntaRelator;	
	

    public ResultadoBean() throws sys.BeanException {
    	tipoJunta = "";
    	sigJunta = "";
    	defmembro1 = "";
    	indefmembro1 = "";
    	defmembro2 = "";
    	indefmembro2 = "";
    	defmembro3 = "";
    	indefmembro3 = "";
    	defmembro4 = "";
    	indefmembro4 = "";
    	codRelator = "";
    	nomRelator = "";
    	linhaPreenchida = "";
    	
    	procDistribuido = "";
    	valUnitario = "";
    	totalMediaDistribuidos = "";
    	totalMediaProc = "";
    	totalProc = "";
    	valTotal = "";
    	
    	qtdRegJunta = 0;
    	
		relatores = new ArrayList();
		artigos = new ArrayList();
		totalArtigo = new ArrayList();
		totalGeral = new ArrayList();
		juntaRelator = new Vector();
	}
	
    public void setTipoJunta(String tipoJunta) {
		if (tipoJunta == null)
			tipoJunta = "";
		else
			this.tipoJunta = tipoJunta;
	}
	public String getTipoJunta() {
		return tipoJunta;
	}

	public String getDefMembro1() {
		return defmembro1;
	}
	public void setDefMembro1(String defmembro1) {
		if(defmembro1 == null) defmembro1 = "0";
		else this.defmembro1 = defmembro1;
	}
	   
	public String getInDefMembro1() {
		return indefmembro1;
	}
	public void setInDefMembro1(String indefmembro1) {
		if(indefmembro1 == null) indefmembro1 = "0";
		else this.indefmembro1 = indefmembro1;
	}

	public String getDefMembro2() {
		return defmembro2;
	}
	public void setDefMembro2(String defmembro2) {
		if(defmembro2 == null) defmembro2 = "0";
		else this.defmembro2 = defmembro2;
	}
	
	public String getInDefMembro2() {
		return indefmembro2;
	}
	public void setInDefMembro2(String indefmembro2) {
		if(indefmembro2 == null) indefmembro2 = "0";
		else this.indefmembro2 = indefmembro2;
	}

	public String getDefMembro3() {
		return defmembro3;
	}
	public void setDefMembro3(String defmembro3) {
		if(defmembro3 == null) defmembro3 = "0";
		else this.defmembro3 = defmembro3;
	}
	
	public String getInDefMembro3() {
		return indefmembro3;
	}
	public void setInDefMembro3(String indefmembro3) {
		if(indefmembro3 == null) indefmembro3 = "0";
		else this.indefmembro3 = indefmembro3;
	}

	public String getDefMembro4() {
		return defmembro4;
	}
	public void setDefMembro4(String defmembro4) {
		if(defmembro4 == null) defmembro4 = "0";
		else this.defmembro4 = defmembro4;
	}
	
	public String getInDefMembro4() {
		return indefmembro4;
	}
	public void setInDefMembro4(String indefmembro4) {
		if(indefmembro4 == null) indefmembro4 = "0";
		else this.indefmembro4 = indefmembro4;
	}
	
	public void setRelatores(ArrayList relatores) {
		if (relatores == null)
			relatores = new ArrayList();
		else
			this.relatores = relatores;
	}
	public Vector getRelatores() {
		return juntaRelator;
	}
	
	public void setJuntaRelator(Vector juntaRelator) {
		if (juntaRelator == null)
			juntaRelator = new Vector();
		else
			this.juntaRelator = juntaRelator;
	}
	public Vector getJuntaRelator() {
		return juntaRelator;
	}

	public void setSigJunta(String sigJunta) {
		if (sigJunta == null) sigJunta = "";
		this.sigJunta = sigJunta;
	}
	public String getSigJunta() {
		return sigJunta;
	}
	
	public void setCodRelator(String codRelator) {
		if(codRelator == null) codRelator = "";
		else this.codRelator = codRelator;
	}
	public String getCodRelator() {
		return codRelator;
	}

	public String getNomRelator() {
		return nomRelator;
	}
	public void setNomRelator(String nomRelator) {
		if (nomRelator == null)nomRelator = "";
		else this.nomRelator = nomRelator;
	}
	
	public ArrayList getArtigos() {
		return artigos;
	}
	public void setArtigos(ArrayList artigos) {
		if(artigos == null) artigos = new ArrayList();
		else this.artigos = artigos;
	}
	
	public ArrayList getDadosRelatorio() {
		return dadosRelatorio;
	}
	public void setDadosRelatorio(ArrayList dadosRelatorio) {
		if(dadosRelatorio == null) dadosRelatorio = new ArrayList();
		else this.dadosRelatorio = dadosRelatorio;
	}
	

	public String getLinhaPreenchida() {
		return linhaPreenchida;
	}
	public void setLinhaPreenchida(String linhaPreenchida) {
		if(linhaPreenchida == null)linhaPreenchida = "";
		else this.linhaPreenchida = linhaPreenchida;
	}
	
	public ArrayList getTotalGeralJU() {
		return totalGeral;
	}
	public void setTotalGeralJU(ArrayList totalGeral) {
		this.totalGeral = totalGeral;
	}
	
	public ArrayList getTotalArtigo() {
		return totalArtigo;
	}
	public void setTotalArtigo(ArrayList totalArtigo) {
		this.totalArtigo = totalArtigo;
	}
	
	public String getProcDistribuido() {
		return procDistribuido;
	}
	public void setProcDistribuido(String procDistribuido) {
		if (procDistribuido == null)procDistribuido = "";
		else this.procDistribuido = procDistribuido;
	}
	
	public String getValUnitario() {
		return valUnitario;
	}
	public void setValUnitario(String valUnitario) {
		if(valUnitario == null) valUnitario = "";
		else this.valUnitario = valUnitario;
	}
	
	public String getValTotal() {
		return valTotal;
	}
	public void setValTotal(String valTotal) {
		if (valTotal == null) valTotal = "";
		else this.valTotal = valTotal;
	}

	public String getTotalProc() {
		return totalProc;
	}
	public void setTotalProc(String totalProc) {
		if (totalProc == null) totalProc = "";
		else this.totalProc = totalProc;
	}
	
	public String getTotalMediaDistribuidos() {
		return totalMediaDistribuidos;
	}
	public void setTotalMediaDistribuidos(String totalMediaDistribuidos) {
		if(totalMediaDistribuidos == null) totalMediaDistribuidos = "";
		else this.totalMediaDistribuidos = totalMediaDistribuidos;
	}
	
	public String getTotalMediaProc() {
		return totalMediaProc;
	}
	public void setTotalMediaProc(String totalMediaProc) {
		if(totalMediaProc == null) totalMediaProc = "";
		else this.totalMediaProc = totalMediaProc;
	}

	public String formataInteiro(String valor) throws DaoException, BeanException {
		String valorFormatado = "";
		if( (valor != null)||(valor.equals("")) )  {
			DecimalFormat formato = new DecimalFormat(",##0") ;
			valorFormatado = formato.format(Integer.parseInt(valor)); 
		}else valorFormatado = "0";
		return valorFormatado;
	}

	public void getRelatores(ResultadoBean resultadoBean) {
		try {
			Dao dao = Dao.getInstance();
			dao.getRelatores(resultadoBean);
		} catch (DaoException e) {
			setMsgErro("Erro no M�todo getRelatores : " + e.getMessage());
		}
	}
	
	public void buscaDadosEstatisticos(ResultadoBean resultadoBean) {
		try {
			Dao dao = Dao.getInstance();
			dao.mostraEstatistica(resultadoBean);
		} catch (DaoException e) {
			setMsgErro("Erro no M�todo mostraEstatistica : " + e.getMessage());
		}
	}
	
	public void buscaDadosConsolidacao(ResultadoBean resultadoBean) {
		try {
			Dao dao = Dao.getInstance();
			dao.mostraConsolidacaoResultado(resultadoBean);
		} catch (DaoException e) {
			setMsgErro("Erro no M�todo mostraEstatistica : " + e.getMessage());
		}
	}
	
	public void identDadosMembros(ResultadoBean resultadoBean) {
		//Identifico para qual membro pertence os dados
		boolean achei = false;
		for (int i = 0;i< resultadoBean.getJuntaRelator().size();i++)
		{
			for (int j = 0;j < ((REC.JuntaBean)resultadoBean.getJuntaRelator().get(i)).getRelator().size();j++)
			{ 
				if(((REC.RelatorBean)((REC.JuntaBean)resultadoBean.getJuntaRelator().get(i)).getRelator().get(j)).getNomRelator().equals(resultadoBean.getNomRelator()))
				{
					resultadoBean.setCodRelator(((REC.RelatorBean)((REC.JuntaBean)resultadoBean.getJuntaRelator().get(i)).getRelator().get(j)).getPkid());
					achei = true;
					break;
				} 
			}
			if(achei)break;
		}
	}
	
	public void montaEstruturaRelatorio(ResultadoBean resultadoBean) {
		try {
			Dao dao = Dao.getInstance();
			dao.montaEstruturaRelatorio(resultadoBean);
		} catch (DaoException e) {
			setMsgErro("Erro no M�todo getRelatores : " + e.getMessage());
		}
	}
	
	public void montaEstrutRelatConsolidacao(ResultadoBean resultadoBean) {
		try {
			Dao dao = Dao.getInstance();
			dao.montaEstrutRelatConsolidacao(resultadoBean);
		} catch (DaoException e) {
			setMsgErro("Erro no M�todo getRelatores : " + e.getMessage());
		}
	}
	
	public String formataNumero(String valor) throws DaoException, BeanException {
		String valorFormatado = "";
	    Locale locale = Locale.getDefault();
	    locale = Locale.GERMAN;  
		if( (valor != null)&&(!valor.equals("")) )  {
		    valor = valor.trim();
			valorFormatado = NumberFormat.getNumberInstance(locale).format(Integer.parseInt(valor));
		}else if (valor == null || valor.equals("") )
			valorFormatado = "";
		 else   
			valorFormatado = "0";
		
		return valorFormatado;
	}
	
	public String formataNumeroMoeda(String valor) throws DaoException, BeanException {
		String valorFormatado = "";
	    Locale BRAZIL = Locale.getDefault();
	    BRAZIL = new Locale("pt","BR");
	    valor = valor.trim();
		if( (valor != null)&&(!valor.equals(""))&&(!valor.equals("-")) )  {
			DecimalFormatSymbols REAL = new DecimalFormatSymbols(BRAZIL);
			DecimalFormat moedaReal = new DecimalFormat("###,###,##0.00",REAL);
			valorFormatado = moedaReal.format(Double.parseDouble(valor));
		}else if (valor.equals(""))
			valorFormatado = "";
		 else 
			valorFormatado = "0,00";
		
		return valorFormatado;
	}
	






}