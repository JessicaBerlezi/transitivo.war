package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;
import sys.Util;

/**
* <b>Title:</b>        Controle de Acesso - Consulta de Hist�ricos<br>
* <b>Description:</b>  Consulta de Hist�ricos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaCmd extends sys.Command {

	private static final String jspPadrao = "/GER/Auditoria.jsp";
	private String next;

	public AuditoriaCmd() {
		next = jspPadrao;
	}

	public AuditoriaCmd(String next) {
		this.next = next;
	}
      
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = Auditoria(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("AuditoriaCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
  
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = next;
		try {
			
			nextRetorno = Auditoria(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("AuditoriaCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

  public String Auditoria(HttpServletRequest req, String nextRetorno) throws CommandException{
	  try {
	       //	cria os Beans, se n�o existir
			 HttpSession session = req.getSession();
			 
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");

			 
			 ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			 if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) session.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
	    	 REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    	 if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			

			 
			 ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		    	if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
			 
			 ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			 if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			 REC.EventoBean EventoId = (REC.EventoBean) req.getAttribute("EventoId");
			 if (EventoId == null) EventoId = new REC.EventoBean();

			 AuditoriaBean AuditoriaBeanId =(AuditoriaBean) session.getAttribute("AuditoriaBeanId");
			 if (AuditoriaBeanId == null)AuditoriaBeanId = new AuditoriaBean();
		
			 // obtem e valida os parametros recebidos
			 String acao = req.getParameter("acao");
			 if (acao == null) acao = " ";
			 
			 // obtem e valida os parametros recebidos
			 String sIN = req.getParameter("sIN");
			 if (sIN == null) sIN = "0";
			 if((sIN.indexOf(","))>0)
				 sIN ="0";
			 
			 
			 String j_sigFuncao = req.getParameter("j_sigFuncao");
	    	 if (j_sigFuncao==null) j_sigFuncao="";
	    	 else
	    		 j_sigFuncao=j_sigFuncao.trim();
	    	 
	    	 
//	    	 AuditoriaBeanId.setTransRef(req.getParameter("transRef"));
//	    	 AuditoriaBeanId.setDatProcRef(req.getParameter("datProcRef"));
//	    	 AuditoriaBeanId.setUltimaTransList(req.getParameter("ultimaTransList"));
//	    	 AuditoriaBeanId.setUltimaDataProcList(req.getParameter("ultimaDataProcList"));
//	    	 AuditoriaBeanId.setUltimaTransListAnt(req.getParameter("ultimaTransListAnt"));
//	    	 AuditoriaBeanId.setUltimaDataProcListAnt(req.getParameter("ultimaDataProcListAnt"));
	    	 
	    	String codOrgaoAtuacao      = req.getParameter("codOrgaoAtu");	     
	    	String sigOrgaoAtuacao      = UsrLogado.getSigOrgaoAtuacao();	     


			 String datIni = req.getParameter("datIni"); 
			 if(datIni == null) datIni = "";  
			 String datFim = req.getParameter("datFim"); 
			 if(datFim == null) datFim = "";  
			 
			 
			 if (acao.equals("buscaUsuarios")) {
				 // Orgao de Lotacao do Usuario
				AuditoriaBeanId.buscaEventos();

				String codOrgao = req.getParameter("codOrgao");
				if (codOrgao == null) codOrgao = "0";

				String codUsuario = req.getParameter("codUsuario");
				if (codUsuario == null)	codUsuario = "0";

				String codEvento = req.getParameter("codEvento");
				if (codEvento == null)	codEvento = "0";
				 
				AuditoriaBeanId.setDatInicio(datIni);
				AuditoriaBeanId.setDatFim(datFim);

				OrgaoBeanId.setCodOrgao(codOrgao);
				EventoId.setCodEvento(codEvento);

				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			 }

			 if (acao.equals(" ")) {

				 session.removeAttribute("AuditoriaBeanId");
				 AuditoriaBeanId =(AuditoriaBean) session.getAttribute("AuditoriaBeanId");
				 if (AuditoriaBeanId == null)AuditoriaBeanId = new AuditoriaBean();
				 OrgaoBeanId.setCodOrgao("0");
				 AuditoriaBeanId.buscaEventos();
			 }			 
			 
			 if (acao.equals("B")) {

				 session.removeAttribute("AuditoriaBeanId");
				 AuditoriaBeanId =(AuditoriaBean) session.getAttribute("AuditoriaBeanId");
				 if (AuditoriaBeanId == null)AuditoriaBeanId = new AuditoriaBean();

				 AuditoriaBeanId.setsIN(sIN);
				 
				 // Usuario Selecionado (pertence ao Orgao de Lotacao)
				 String codUsuario = req.getParameter("codUsuario");
				 if (codUsuario == null)	codUsuario = "0";

				 String nomUserName = req.getParameter("nomUserName"); 
				 if(nomUserName == null) nomUserName = "";  
				 
				 
				 String codEvento = req.getParameter("codEvento");
				 if (codEvento == null)	codEvento = "0";
				 
				 String codEventoRef = EventoId.buscaCodEventoRef(codEvento);
				 if (codEventoRef == null)	codEventoRef = "0";

				 String dscEvento = req.getParameter("dscEvento");
				 if (dscEvento == null)	dscEvento = "";
				 
				 String qtdRegPag = ParamOrgaoId.getParamOrgao("QUANT_REG_PAGINA","50","7");
				
				 // Orgao de Lotacao do Usuario
				 String codOrgao = req.getParameter("codOrgao");
				 if (codOrgao == null) codOrgao = "0";

				 String desOrgao = req.getParameter("desOrgao"); 
				 if(desOrgao == null) desOrgao = "";  

				 String sigOrgao = req.getParameter("sigOrgao"); 
				 if(sigOrgao == null) sigOrgao = "";  
				 //Carrega Bean AuditoriaBean
				 AuditoriaBeanId.setDatInicio(datIni);
				 AuditoriaBeanId.setDatFim(datFim);
				 
				 AuditoriaBeanId.setCodTransacao(Util.lPad(codEvento,"0",3));
				 AuditoriaBeanId.setDscTransacao(dscEvento.trim());
				 AuditoriaBeanId.setCodTransacaoRef(Util.lPad(codEventoRef,"0",3));

				 AuditoriaBeanId.setCodOrgao(codOrgao);
				 AuditoriaBeanId.setSigOrgao(sigOrgao);
				 AuditoriaBeanId.setNomUserName(nomUserName.trim());
				 
				 AuditoriaBeanId.setCodOrgaoAtuacao(codOrgaoAtuacao);
				 AuditoriaBeanId.setSigOrgaoAtuacao(sigOrgaoAtuacao);
				 
				 //Pegar valor o Par�metro;
				 AuditoriaBeanId.setQtd_Registros_Rodada(qtdRegPag);
				 AuditoriaBeanId.setTransRef("0");  
				 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getDatInicio());  
				 
				 AuditoriaBeanId.setQtdParcialReg("0");
				 AuditoriaBeanId.setTotalRegistros(AuditoriaBeanId.BuscaTotalReg());
				 AuditoriaBeanId.CarregaLista(acao);
				 nextRetorno="/GER/AuditoriaLista_"+AuditoriaBeanId.getCodTransacaoRef()+".jsp";
			 }
			 
			 if (acao.equals("Prox")) {
				 
				 if (AuditoriaBeanId.getPrimTrans().size()>0) {
					 int tam = AuditoriaBeanId.getPrimTrans().size();
					 AuditoriaBeanId.setTransRef(AuditoriaBeanId.getUltTrans(tam-1));  
					 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getUltDatProc(tam-1));  
				 } else{
					 AuditoriaBeanId.setTransRef("0");  
					 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getDatInicio());  
				 }
					 
					 
				 AuditoriaBeanId.CarregaLista(acao);
				 nextRetorno="/GER/AuditoriaLista_"+AuditoriaBeanId.getCodTransacaoRef()+".jsp";
			 
			 }

			 if (acao.equals("Ant")) {
				 
				 int qtdParcial = 0;
				 int qtd = 0;

				 if (AuditoriaBeanId.getPrimTrans().size()>0) {
					 int tam = AuditoriaBeanId.getPrimTrans().size();
					 if (AuditoriaBeanId.getPrimTrans().size()==1) {
						 AuditoriaBeanId.setTransRef("0");  
						 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getDatInicio());  
					 }else {
						 if (!AuditoriaBeanId.getQtdReg(tam-1).equals("Ultima")){
							 qtd = Integer.parseInt(AuditoriaBeanId.getQtdReg(tam-1));
							 qtdParcial = Integer.parseInt(AuditoriaBeanId.getQtdParcialReg());
	
							 qtdParcial = qtdParcial - qtd;
							 AuditoriaBeanId.setQtdParcialReg(String.valueOf(qtdParcial));
						 }
						 
						 AuditoriaBeanId.delPrimTrans(tam-1);
						 AuditoriaBeanId.delPrimDatProc(tam-1);
						 AuditoriaBeanId.delUltTrans(tam-1);
						 AuditoriaBeanId.delUltDatProc(tam-1);
						 AuditoriaBeanId.delQtdReg(tam-1);
						 
						 tam = AuditoriaBeanId.getPrimTrans().size();
						 if (AuditoriaBeanId.getPrimTrans().size()==1) {
							 AuditoriaBeanId.setTransRef("0");  
							 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getDatInicio());  
						 }else {

						 AuditoriaBeanId.setTransRef(AuditoriaBeanId.getPrimTrans(tam-1));  
						 AuditoriaBeanId.setDatProcRef(AuditoriaBeanId.getPrimDatProc(tam-1));  

						 }
					 }
				 }
				 
				 AuditoriaBeanId.CarregaLista(acao);
				 nextRetorno="/GER/AuditoriaLista_"+AuditoriaBeanId.getCodTransacaoRef()+".jsp";
			 }
			 
			 
			 if (acao.equals("Imprimir")) {
				String NomTitulo =  UsuarioFuncBeanId.getJ_nomFuncao();
				req.setAttribute("tituloConsulta", NomTitulo);
			    nextRetorno="/GER/RelatAuditoriaImp_"+AuditoriaBeanId.getCodTransacaoRef()+".jsp";
			 }
			 
			 if (acao.equals("Informacoes")) {
					String NomTitulo =  UsuarioFuncBeanId.getJ_nomFuncao();
					String posicao = req.getParameter("posicao");
					 if (posicao == null) posicao = "0"; 
					req.setAttribute("tituloConsulta", NomTitulo);
					req.setAttribute("posicao", posicao);
				    nextRetorno="/GER/InfComplementVis_"+AuditoriaBeanId.getCodTransacaoRef()+".jsp";
			 }
			 
			 req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			 req.setAttribute("UsrBeanId", UsrBeanId);
			 req.setAttribute("EventoBeanId", EventoId);
			 session.setAttribute("AuditoriaBeanId",AuditoriaBeanId);
			 session.setAttribute("ParamSistId",param);
	  
	  }
		 catch (Exception ue) {
			  throw new sys.CommandException("AuditoriaCmd: "+ ue.getMessage());
		 }			 
		 return nextRetorno;
  }
  

}