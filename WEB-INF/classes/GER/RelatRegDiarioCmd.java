package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Gerencial - Registro Diario Cmd<br>
* <b>Description:</b>  Informa as infra��es di�rias de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class RelatRegDiarioCmd extends sys.Command {

	private static final String jspPadrao = "/GER/RelatRegDiario.jsp";
	private String next;

	public RelatRegDiarioCmd() {
		next = jspPadrao;
	}

	public RelatRegDiarioCmd(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			
			RelatRegDiarioBean relatRegDiarioBean = (RelatRegDiarioBean) session.getAttribute("relatRegDiarioBean");
			if (relatRegDiarioBean == null)relatRegDiarioBean = new RelatRegDiarioBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			
			String todosOrgaos = req.getParameter("TodosOrgaos");
			if (todosOrgaos == null)todosOrgaos="N";
			
			
			String dataIni = req.getParameter("dataIni");
			if (dataIni == null) dataIni = "";
			
			String dataFim = req.getParameter("dataFim");
			if (dataFim == null) dataFim = "";
			
			String msg = "";
			relatRegDiarioBean.setCodOrgao(codOrgao);
			relatRegDiarioBean.setDataIni(dataIni);
			relatRegDiarioBean.setDataFim(dataFim);
			relatRegDiarioBean.setTodosOrgaos(todosOrgaos);			
			
			if (acao.equals("ImprimeRelatorio")) {
				//Titulo do Relatorio 
				String tituloConsulta = "RELAT�RIO - REGISTRO DI�RIO DE : "+dataIni+ "AT� "+dataFim;
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				relatRegDiarioBean.consultaRegDiario(relatRegDiarioBean);
				
				if (relatRegDiarioBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE DIA." ;	

			 	nextRetorno="/GER/RelatRegDiarioImp.jsp";
			}

			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("msg", msg);
		  session.setAttribute("relatRegDiarioBean",relatRegDiarioBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatRegDiarioCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}