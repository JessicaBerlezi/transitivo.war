package GER;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelArqNotificacaoXls {
	
	public void write(RelatArqNotificacaoBean RelArqDownlBeanId, String titulo, String arquivo) throws IOException, WriteException
	{
	
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio de Notifica��es", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 9/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
			
			label = new Label(0, 2, "SEQ", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(0, 5);
			
			label = new Label(1, 2, "ARQUIVO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(1, 30);
			
			label = new Label(2, 2, "DATA ENVIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 20);
			
			label = new Label(3, 2, "QTD ENV.", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 10);
			
			label = new Label(4, 2, "USU�RIO ENVIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 20);
			
			label = new Label(5, 2, "DATA DOWNLOAD", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 20);
			
			label = new Label(6, 2, "USU�RIO DOWNLOAD", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(6, 20);
			
			label = new Label(7, 2, "QTD PEND", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(7, 10);
			
			label = new Label(8, 2, "QTD CANC", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(8, 10);
			
			label = new Label(9, 2, "QTD RECEB.", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(9, 12);
			
			
			Iterator it = RelArqDownlBeanId.getListaArqs().iterator();
			GER.RelatArqNotificacaoBean relatorioImp  = new GER.RelatArqNotificacaoBean();
			if( RelArqDownlBeanId.getListaArqs().size()>0 ) { 
				while (it.hasNext()) {
					   relatorioImp = (GER.RelatArqNotificacaoBean)it.next() ;
					   
					   label = new Label(0, cont, String.valueOf(seq)); 
					   sheet.addCell(label);
					   
					   label = new Label(1, cont, relatorioImp.getNomArquivo()); 
					   sheet.addCell(label);
					   
					   label = new Label(2, cont, relatorioImp.getDatRecebimento()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, cont, relatorioImp.getQtdReg()); 
					   sheet.addCell(label);
					   
					   label = new Label(4, cont, relatorioImp.getTarNomUsername()); 
					   sheet.addCell(label);
					   
					   label = new Label(5, cont, relatorioImp.getDatDownload()); 
					   sheet.addCell(label);
					   
					   label = new Label(6, cont, relatorioImp.getNomUsername()); 
					   sheet.addCell(label);
					   
					   label = new Label(7, cont, relatorioImp.getQtdLinhaPendente()); 
					   sheet.addCell(label);
					   
					   label = new Label(8, cont, relatorioImp.getQtdLinhaCancelada()); 
					   sheet.addCell(label);
					   
					   label = new Label(9, cont, relatorioImp.getQtdRecebido()); 
					   sheet.addCell(label);
					   
					   cont++;
					   seq++;
				}
			}
			
			workbook.write(); 
			workbook.close();
			
			//System.out.println("ARQUIVO GERADO COM SUCESSO");
			
		}catch (Exception e) {	
			throw new IOException("GeraXls: " + e.getMessage());	}
	
	}

}
