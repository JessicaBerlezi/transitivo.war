package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE �RG�OS<br>
* <b>Description:</b>  Bean Auditoria_025 - Informa��es daS transa��es 083<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_025 extends AuditoriaBean {	
	
	private String codigo 				;
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String descricao	 		;	
	private String endereco 			;	
	private String numero 				;
	private String complemento 			;	
	private String bairro		 		;	
	private String cep			 		;
	private String municipio	 		;
	private String telefoneDDD 			;	
	private String numTelefone	 		;	
	private String numFax		 		;		
	private String sigla 				;
	private String contato 				;
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_025() throws sys.BeanException {		
	
		codigo 				= "";
		tipoAcao 			= "";
		indContinuidade     = "";
		descricao 			= "";
		endereco 			= "";
		numero	 			= "";
		complemento 		= "";
		bairro 				= "";
		cep 				= "";
		municipio 			= "";
		telefoneDDD 		= "";
		numTelefone 		= "";
		numFax 				= "";
		sigla 				= "";		
		contato 			= "";
		codOrgao  			= "";	
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",238);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
	     this.codigo 			= reg.substring(0,6);
	     this.tipoAcao 			= reg.substring(6,7);
	     this.indContinuidade 	= reg.substring(7,13);
	     this.descricao 		= reg.substring(13,43);
	     this.endereco 			= reg.substring(43,70);
	     this.numero 			= reg.substring(70,75);
	     this.complemento 		= reg.substring(75,86);
	     this.bairro 			= reg.substring(86,106);
	     this.cep 				= reg.substring(106,114);
	     this.municipio 		= this.BuscaCidade(reg.substring(114,118));
	     this.telefoneDDD 		= reg.substring(118,121);
	     this.numTelefone 		= reg.substring(121,136);
	     this.numFax 			= reg.substring(136,151);
	     this.sigla 			= reg.substring(151,161);
	     this.contato 			= reg.substring(161,206);
	     this.codOrgao 			= reg.substring(206,212);
	     this.codOrgoaLotacao 	= reg.substring(212,218);
	     this.nomUserName 		= reg.substring(218,238);
	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	public String getCodigo() {
		return codigo;
	}	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDescricao() {
		return descricao;
	}
	public String getEndereco() {
		return endereco;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		String cep =this.cep;
		cep = cep.substring(0,5)+"-"+cep.substring(5,8);		
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getTelefoneDDD() {
		return telefoneDDD;
	}	
	public String getNumTelefone() {
		String tel =this.numTelefone;
		tel = tel.substring(0,4)+"-"+tel.substring(4,8);		
		return tel;
	}
	public String getNumFax() {
		String fax =this.numFax;
		fax = fax.substring(0,4)+"-"+fax.substring(4,8);		
		return fax;
	}
	public String getSigla() {
		return sigla;
	}
	public String getContato() {
		return contato;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	}