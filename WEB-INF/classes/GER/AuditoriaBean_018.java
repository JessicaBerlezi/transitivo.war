package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - REVIS�O DE REQUERIMENTO<br>
* <b>Description:</b>  Bean Auditoria_018 - Informa��es daS transa��es 264,384,391<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_018 extends AuditoriaBean {	
	
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String codOrgao 		;
	private String numProcesso 		;
	private String codStatus 		;	
	private String codEvento 		;
	private String codOrigemEvento  ;
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String txtComplemento1	;
	private String txtComplemento2	;
	private String txtComplemento3	;	
	
	public AuditoriaBean_018() throws sys.BeanException {
		
		numAutoInfracao       = "";
		numPlaca  			  = "";
		codOrgao  			  = "";
		numProcesso           = "";
		codStatus    		  = "";		
		codEvento      		  = "";
		codOrigemEvento   	  = "";
		nomUserName      	  = "";
		codOrgoaLotacao       = "";
		txtComplemento1       = "";
		txtComplemento2       = "";
		txtComplemento3       = "";			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",1112);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();		 
	    
		 this.numAutoInfracao   = reg.substring(0,12) ;
	     this.numPlaca 			= reg.substring(12,19);	     
	     this.codOrgao 			= reg.substring(19,25);
	     this.numProcesso 		= reg.substring(25,45);
	     this.codStatus 		= reg.substring(45,48);	     
	     this.codEvento 		= reg.substring(48,51);
	     this.codOrigemEvento 	= reg.substring(51,54);	     
	     this.nomUserName 		= reg.substring(54,74);
	     this.codOrgoaLotacao 	= reg.substring(74,80);
	     this.txtComplemento1 	= reg.substring(80,110);
	     this.txtComplemento2 	= reg.substring(110,112);
	     this.txtComplemento3 	= reg.substring(112,1112);	     
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }    
	     
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}

	public String getNumPlaca() {
		return numPlaca;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getNumProcesso() {
		return numProcesso;
	}	
	public String getCodStatus() {
		return codStatus;
	}	
	public String getCodEvento() {
	
		return codEvento;
	}
	public String getCodOrigemEvento() {
		return codOrigemEvento;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getTxtComplemento1() {
		return txtComplemento1;
	}
	public String getTxtComplemento2() {
		return txtComplemento2;
	}
	public String getTxtComplemento3() {		
		return txtComplemento3;
	}
}