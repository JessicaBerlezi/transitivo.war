package GER;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import sys.BeanException;
import sys.DaoException;

/**
* <b>Title:</b>        Gerencial - Autos Pagos Bean<br>
* <b>Description:</b>  Informa os autos pagos de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class RelatAutosPagosBean extends sys.HtmlPopupBean {

	
	private List dados;
	private List totalValor;
	private String codOrgao; 
	private String sigOrgao;
	private String dataIni;
	private String dataFim;
	private String datPagamento;
	
	private String numAuto;
	private String datInfracao;
	private String datVencimento;
	private String codAgente;
	private String valPago;
	private String valAcumulado;
	private String flagControle;
	private String todosOrgao;
	private int qtdLinhas;


	public RelatAutosPagosBean() throws sys.BeanException {
		dados = new ArrayList();
		totalValor = new ArrayList();
		codOrgao = "";
		sigOrgao = "";
		dataIni = "";
		dataFim = "";
		
		numAuto = "";
		datInfracao = "";
		datVencimento = "";
		datPagamento = "";
			
		valPago = "";
		valAcumulado = "";
		flagControle = "";
		todosOrgao = "";
		qtdLinhas=0;

	}
	
	public void setNumAuto(String numAuto) {
			this.numAuto = numAuto;
			if (numAuto == null)
				this.numAuto = "";
	}
	public String getNumAuto() {
		return this.numAuto;
	}
	
    public void setDatInfracao(String datInfracao) {
		this.datInfracao = datInfracao;
		if (datInfracao == null)
			this.datInfracao = "";
	}
	public String getDatInfracao() {
		return this.datInfracao;
	}

	public void setDatVencimento(String datVencimento) {
		this.datVencimento = datVencimento;
		if (datVencimento == null)
			this.datVencimento = "";
	}
	public String getDatVencimento() {
		return this.datVencimento;
	}
	
	public void setValPago(String valPago) {
		this.valPago = valPago;
		if (valPago == null)
			this.valPago = "";
	}
	public String getValPago() {
		return this.valPago;
	}		

	public void setValAcumulado(String valorAcumulado) {
		this.valAcumulado = valorAcumulado;
		if (valAcumulado == null)
			this.valAcumulado = "";
	}
	public String getValAcumulado() {
		return this.valAcumulado;
	}	


	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}
	
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}
	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setTotalValor(List totalValor) {
		this.totalValor = totalValor;
	}
	public List getTotalValor() {
		return this.totalValor;
	}
	
	public void setCodAgente(String codAgente) {
		if(codAgente == null)codAgente = "";
		else this.codAgente = codAgente;
	}
	public String getCodAgente() {
		return codAgente;
	}
	
	public void setDatPagamento(String datPagamento) {
		if(datPagamento == null) datPagamento = "";
		else this.datPagamento = datPagamento;
	}
	public String getDatPagamento() {
		return datPagamento;
	}

	public void setFlagControle(String flagControle) {
		if(flagControle == null) flagControle = "";
		else this.flagControle = flagControle;
	}
	public String getFlagControle() {
		return flagControle;
	}
	
	
	public void setTodosOrgao(String todosOrgao) {
		if(todosOrgao == null) todosOrgao = "";
		else this.todosOrgao = todosOrgao;
	}
	public String getTodosOrgao() {
		return todosOrgao;
	}
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaAutosPagos (RelatAutosPagosBean relatAutosPagosBean) throws  BeanException, DaoException{
			boolean existe = false;
			DaoControle dao = DaoControle.getInstance();
			try {
				if(dao.consultaAutosPagos(relatAutosPagosBean)== true)
				  existe = true;
		  
			} 
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			} 
			return existe;	
		}


	public String formataIntero (String valor) throws  DaoException, BeanException{	
	  if (valor == null) valor = "0";
	  DecimalFormat formato = new DecimalFormat(",#00") ;
	  String valorFormatado = formato.format(Integer.parseInt(valor)); 
	  return valorFormatado;
	}
	
	public String formataMoeda (String valor) throws  DaoException, BeanException{	
		if (valor == null) valor = "0";
		NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")); 
	  String valorFormatado = formato.format(Double.parseDouble(valor)); 
	  return valorFormatado;
	}
	
	
	public String formataDecimal (String valor) throws  DaoException, BeanException{	
		if (valor == null) valor = "0";
		NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
	  String valorFormatado = formato.format(Double.parseDouble(valor)); 
	  return valorFormatado.substring(3);
	}

	public int getQtdLinhas() {
		return qtdLinhas;
	}

	public void setQtdLinhas(int qtdLinhas) {
		this.qtdLinhas = qtdLinhas;
	}













}