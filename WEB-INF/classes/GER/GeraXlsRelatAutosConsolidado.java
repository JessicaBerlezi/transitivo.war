package GER;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GeraXlsRelatAutosConsolidado extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
	{
		try {
             RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean = (RelatAutosPorOrgaoBean)req.getAttribute("RelatAutosPorOrgaoBean") ;
             if(RelatAutosPorOrgaoBean == null) RelatAutosPorOrgaoBean = new RelatAutosPorOrgaoBean() ;
            
             ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
             if(OrgaoId == null) OrgaoId = new ACSS.OrgaoBean() ;            
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             String contexto = req.getParameter("contexto");
             if( contexto == null ) contexto = "";
             
             String nomArquivo = req.getParameter("nomArquivo");
             if( nomArquivo == null ) nomArquivo = "";
            
            if(acao.equals("GeraExcel")) 
			{
                String statusAuto = req.getParameter("statusAuto");
                if (statusAuto == null) statusAuto = "";
                RelatAutosPorOrgaoBean.setTipoSolicitacao(statusAuto);
                
                String todosOrgaos = req.getParameter("todosOrgaos");
                if (todosOrgaos == null) todosOrgaos = "";
                RelatAutosPorOrgaoBean.setTodosOrgaos(todosOrgaos);
                
                String codOrgao = req.getParameter("codOrgao");
                if (codOrgao == null) codOrgao = "";
                RelatAutosPorOrgaoBean.setCodOrgao(codOrgao);
     
                String mesAnoInicio = req.getParameter("mesAnoInicio");
                if (mesAnoInicio == null) mesAnoInicio = "";
                else{
                  RelatAutosPorOrgaoBean.setMesInicial(Integer.parseInt(mesAnoInicio.substring(0,2)));
                  RelatAutosPorOrgaoBean.setMesFinal(Integer.parseInt(mesAnoInicio.substring(0,2))+1);
                  RelatAutosPorOrgaoBean.setAnoInicial(Integer.parseInt(mesAnoInicio.substring(3,7)));
                  RelatAutosPorOrgaoBean.setAnoFinal(Integer.parseInt(mesAnoInicio.substring(3,7)));
                }
                 
                RelatAutosPorOrgaoBean.setCodSistema("40");
                //monta a estrutura do relatorio
				RelatAutosPorOrgaoBean.getMontaRelatorio(RelatAutosPorOrgaoBean);

                //Preenche o relatorio com os totais mensais 
				RelatAutosPorOrgaoBean.getGerarRelatConsolidado(RelatAutosPorOrgaoBean);
                
                //Preenche o total MR
                RelatAutosPorOrgaoBean.getTotalGeralMR(RelatAutosPorOrgaoBean);
                
                //Preenche o total DOL
                RelatAutosPorOrgaoBean.getTotalGeralDol(RelatAutosPorOrgaoBean);

                //Ordena 
                String ordem = req.getParameter("ordem");
                if(ordem == null) ordem = "";
                RelatAutosPorOrgaoBean.setClassificaPor(ordem);
                RelatAutosPorOrgaoBean.Classifica(RelatAutosPorOrgaoBean);

                
                OrgaoId.Le_Orgao(codOrgao,0);
                if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()>0)
                    OrgaoId.setSigOrgao("TODOS");
                
				RelatAutosConsolidadoXls geraxls = new RelatAutosConsolidadoXls();

                String tituloConsulta = "RELAT�RIO REGISTRO DE AUTOS POR �RG�O CONSOLIDADO <br>" +
                "M�S/ANO: "+mesAnoInicio+" - �RG�O: "+ OrgaoId.getSigOrgao();
	
            	ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
            	paramSys.setCodSistema("24"); // M�dulo GER
            	paramSys.PreparaParam();
            	String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
				geraxls.write(RelatAutosPorOrgaoBean, tituloConsulta, arquivo);
				
				req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
				rd.forward(req, resp);	
		
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());            
		} 
	}
  }
}
