package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Gerencial - Autos Pagos Cmd<br>
* <b>Description:</b>  Informa os autos pagos de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class RelatAutosPagosCmd extends sys.Command {

	private static final String jspPadrao = "/GER/RelatAutosPagos.jsp";
	private String next;

	public RelatAutosPagosCmd() {
		next = jspPadrao;
	}

	public RelatAutosPagosCmd(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		String nextRetorno = next;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			
			RelatAutosPagosBean relatAutosPagosBean = (RelatAutosPagosBean) session.getAttribute("relatInfDiariasBean");
			if (relatAutosPagosBean == null)relatAutosPagosBean = new RelatAutosPagosBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			
			String dataIni = req.getParameter("dataIni");
			if (dataIni == null) dataIni = "";
			
			String todosOrgaos = req.getParameter("todosOrgaos");
			if (todosOrgaos == null) todosOrgaos = "";
			relatAutosPagosBean.setTodosOrgao(todosOrgaos);
			
				
			String msg = "";
			relatAutosPagosBean.setCodOrgao(codOrgao);

			if (acao.equals("")){
				relatAutosPagosBean = new RelatAutosPagosBean();				
				String dataHoje = sys.Util.getDataHoje();
				String mes = dataHoje.substring(3,5);	
				String ano = dataHoje.substring(6,10);
				dataIni	=  mes + "/" + ano;
				dataHoje = dataHoje.replaceAll("-","/");  
				relatAutosPagosBean.setDataIni(dataIni);
			}		
			
			if (acao.equals("ImprimeRelatorio")) {
				//Titulo do Relatorio 
				relatAutosPagosBean.setDataIni(dataIni);
				String tituloConsulta = "RELAT�RIO DE AUTOS PAGOS - "+relatAutosPagosBean.getDataIni();
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				relatAutosPagosBean.consultaAutosPagos(relatAutosPagosBean);
				
				if (relatAutosPagosBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE M�S." ;	

			 	nextRetorno="/GER/RelatAutosPagosImp.jsp";
			}

			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("msg", msg);
		  session.setAttribute("relatAutosPagosBean",relatAutosPagosBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatAutosPagosCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}