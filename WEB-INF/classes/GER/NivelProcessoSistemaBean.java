package GER;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;




/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Michel
* @version 1.0
* @Updates
*/

public class NivelProcessoSistemaBean extends sys.HtmlPopupBean {

	private String codSistema;
	private String codProcesso;
	private String codNivelProcesso;	
	private String nomNivelProcesso;
	private String valNivelProcesso;
	private String valNivelProcessoMax;
	private String tipoMsg;
	private List nomNivelProcessoSist;
	private List valNivelProcessoSist;
	private ArrayList listNivelDestino;


	public NivelProcessoSistemaBean() {
		super();
		setTabela("TSMI_PROCESSO_SISTEMA_NIVEL");
		setPopupWidth(35);
		codSistema       = "";
		codProcesso      = "";
		codNivelProcesso = "";
		nomNivelProcesso = "";
		valNivelProcesso = "";
		valNivelProcessoMax = "";
		tipoMsg          = "";
		nomNivelProcessoSist = new ArrayList();
		valNivelProcessoSist = new ArrayList();
		listNivelDestino  = new ArrayList();
	}

	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if (codSistema == null)
			this.codSistema = "";
	}
	public String getCodSistema() {
		return this.codSistema;
	}

	public void setCodProcesso(String codProcesso) {
		this.codProcesso = codProcesso;
		if (codProcesso == null)
			this.codProcesso = "";
	}
	public String getCodProcesso() 
	{
		return this.codProcesso;
	}

	public void setCodNivelProcesso(String codNivelProcesso) {
		this.codNivelProcesso = codNivelProcesso;
		if (codNivelProcesso == null)
			this.codNivelProcesso = "";
	}
	public String getCodNivelProcesso() 
	{
		return this.codNivelProcesso;
	}
	
	public void setNomNivelProcesso(String nomNivelProcesso) {
		this.nomNivelProcesso = nomNivelProcesso;
		if (nomNivelProcesso == null)
			this.nomNivelProcesso = "";
	}
	public String getNomNivelProcesso() {
		return this.nomNivelProcesso;
	}

	public void setValNivelProcesso(String valNivelProcesso) {
		this.valNivelProcesso = valNivelProcesso;
		if (valNivelProcesso == null)
			this.valNivelProcesso = "";
	}
	public String getValNivelProcesso() {
		return this.valNivelProcesso;
	}

	public void setTipoMsg(String tipoMsg) {
		if(tipoMsg == null) tipoMsg = "";
		else this.tipoMsg = tipoMsg;
	}
	public String getTipoMsg() {
		return tipoMsg;
	}
	
	public void setValNivelProcessoMax(String valNivelProcessoMax) {
		if( valNivelProcessoMax == null)
			valNivelProcessoMax = "";
		else
		   this.valNivelProcessoMax = valNivelProcessoMax;
	}
	public String getValNivelProcessoMax() {
		return valNivelProcessoMax;
	}


	//--------------------------------------------------------------------------

	public void setNomNivelProcessoSist(List nomNivelProcessoSist) {
		this.nomNivelProcessoSist = nomNivelProcessoSist;
	}

	public List getNomNivelProcessoSist() {
		return this.nomNivelProcessoSist;
	}
	public String getNivelProcessoSist(String myNomNivelProcesso) throws sys.BeanException {
		try {
			String myVal = "";
			String re = "";
			Iterator itx = getNomNivelProcessoSist().iterator();
			int i = 0;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomNivelProcesso)) {
					myVal = getValNivelProcessoSist(i);
					break;
				}
				i++;
			}

			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
	//--------------------------------------------------------------------------
	public void setValNivelProcessoSist(List valNivelProcessoSist) {
		this.valNivelProcessoSist = valNivelProcessoSist;
	}

	public List getValNivelProcessoSist() {
		return this.valNivelProcessoSist;
	}

	public String getValNivelProcessoSist(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValNivelProcessoSist().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
//---------------------------------------------------------------------------------------
	public String getNiveis(String codProcesso) {
		String myNivel = "";
		try {
			Dao dao = Dao.getInstance();
			myNivel = dao.getNivel(codProcesso);
		}
		catch (Exception e) {
		}
		return myNivel;
	}
//---------------------------------------------------------------------------------------
	public void Le_Niveis(String tpNivel, int pos) {
		try {
			Dao dao = Dao.getInstance();
			if (pos == 0)
				this.codNivelProcesso = tpNivel;
			else
				this.nomNivelProcesso = tpNivel;
			if (dao.NivelLeBean(this, pos) == false) {
				this.codNivelProcesso = "0";
				this.nomNivelProcesso = "";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codNivelProcesso = "0";
			setPkid("0");
			System.err.println("Sistema001: " + e.getMessage());
		}
	}

	public void setListNivelDestino(ArrayList listNivelDestino) {
		if(listNivelDestino == null) listNivelDestino = new ArrayList();
		else this.listNivelDestino = listNivelDestino;
	}
	public ArrayList getListNivelDestino() {
		return this.listNivelDestino;
	}

	





}