package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GrafProcAbertosCmd extends sys.Command {

	private static final String jspPadrao = "/GER/GrafProcAbertos.jsp";
	private String next;
	public GrafProcAbertosCmd() { next = jspPadrao; }
	public GrafProcAbertosCmd(String next) { this.next = next; }
	
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{	
		
		String nextRetorno = next;
		
		try {
		
			ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
			
			String verGrafico = req.getParameter("verGrafico");
 			if (verGrafico == null) verGrafico = "N";
			
 			String acao = req.getParameter("acao");
 			if (acao == null) acao = " ";
 			
 			String orgao = req.getParameter("codOrgao");
 			if (orgao == null) orgao = " ";
            
            String datInicio = req.getParameter("datInicio");
            if (datInicio == null) datInicio = "";
            
            String datFim = req.getParameter("datFim");
            if (datFim == null) datFim = "";
            
            String[] tipoReq  = req.getParameterValues("TPreq");
            if(tipoReq == null) tipoReq = new String[0];
            
            String codTipoSolic = "";
            for (int i = 0; i < tipoReq.length; i++)
            {
                if ("0".equals(tipoReq[i])){     
                    codTipoSolic += "'DP','DC',";
                }else if("1".equals(tipoReq[i])){ 
                    codTipoSolic += "'1P','1C',";
                }else if("2".equals(tipoReq[i])){
                    codTipoSolic += "'2P','2C',";
                }else if("3".equals(tipoReq[i])){
                    codTipoSolic += "'TR',";
                }else if("4".equals(tipoReq[i])){
                    codTipoSolic += "'RD',";
                }
            }
            
            String TPreq = "";
            if (codTipoSolic.length() >0)
                TPreq = codTipoSolic.substring(0,codTipoSolic.length()-1);
            
            if (acao.equals("OK")) { 				
 				verGrafico = "S";		
 			}
 		
 			if(acao.equals("Retornar")){
 				verGrafico = "N"; 
 			}
 			
 			req.setAttribute("verGrafico", verGrafico);
 			req.setAttribute("cod_uf",SistemaBeanId.getCodUF());
            req.setAttribute("TPreq",TPreq);
			
		} catch (Exception e) {	throw new sys.CommandException("GrafProcAbertosCmd: " + e.getMessage());	}	
		return nextRetorno;
	}
	
	
}
