package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - AJUSTE MANUAL DO PROPRIET�RIO<br>
* <b>Description:</b>  Bean Auditoria_009 - Informa��es da transa��o 415<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_009 extends AuditoriaBean {
	
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String codOrgao 		;
	private String numProcesso 		;
	private String codStatus 		;
	private String datStatus 		;
	private String codEvento 		;
	private String codOrigemEvento  ;
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String txtComplemento1	;
	private String indCpf_Cnpj		;	
	private String txtComplemento2	;
	private String txtComplemento3	;
	private String txtComplemento4	;
	private String txtComplemento5	;
	private String txtComplemento6	;
	private String txtComplemento7	;
	
	public AuditoriaBean_009() throws sys.BeanException {
		
		numAutoInfracao      = "";
		numPlaca  			 = "";
		codOrgao  			 = "";
		numProcesso          = "";
		codStatus    		 = "";
		datStatus         	 = "";
		codEvento      		 = "";
		codOrigemEvento   	 = "";
		nomUserName      	 = "";
		codOrgoaLotacao      = "";
		txtComplemento1      = "";
		indCpf_Cnpj     	 = "";
		txtComplemento2      = "";
		txtComplemento3      = "";
		txtComplemento4      = "";
		txtComplemento5      = "";
		txtComplemento6      = "";
		txtComplemento7      = "";
		
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",220);		 
	    
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 
		 this.numAutoInfracao   = reg.substring(0,12) ;
	     this.numPlaca 			= reg.substring(12,19);
	     this.codOrgao 			= reg.substring(19,25);
	     this.numProcesso 		= reg.substring(25,45);
	     this.codStatus 		= reg.substring(45,48);
	     this.datStatus 		= reg.substring(48,56);
	     this.codEvento 		= reg.substring(56,59);
	     this.codOrigemEvento 	= reg.substring(59,62);	     
	     this.nomUserName 		= reg.substring(62,82);
	     this.codOrgoaLotacao 	= reg.substring(82,88);
	     this.txtComplemento1 	= reg.substring(88,96);
	     this.indCpf_Cnpj 		= reg.substring(96,97);
	     this.txtComplemento2 	= reg.substring(97,111);
	     this.txtComplemento3 	= reg.substring(111,151);
	     this.txtComplemento4 	= reg.substring(151,206);
	     this.txtComplemento5 	= reg.substring(206,207);
	     this.txtComplemento6 	= reg.substring(207,218);
	     this.txtComplemento7 	= reg.substring(218,220);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}

	public String getNumPlaca() {
		return numPlaca;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getNumProcesso() {
		return numProcesso;
	}	
	public String getCodStatus() {
		return codStatus;
	}	public String getDatStatus() {
		String data = "";
		data = this.datStatus.substring(6,8)+"/"+this.datStatus.substring(4,6)+"/"+
		this.datStatus.substring(0,4);
		return data;
	}	
	public String getCodEvento() {
	
		return codEvento;
	}
	public String getCodOrigemEvento() {
		return codOrigemEvento;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getTxtComplemento1() {
		String data = "";
		data = this.txtComplemento1.substring(6,8)+"/"+this.txtComplemento1.substring(4,6)+"/"+
		this.txtComplemento1.substring(0,4);
		return data;
	}	
	public String getIndCpf_Cnpj() {
		return indCpf_Cnpj;
	}	
	public String getCpf_CnpjEdt()  {
		if ("".equals(this.txtComplemento2)) return this.txtComplemento2 ;
	    if ("1".equals(this.indCpf_Cnpj)) {
	    	if (this.txtComplemento2.length()<14) this.txtComplemento2 += "               ";
		  	return this.txtComplemento2.substring(3,6)+"."+this.txtComplemento2.substring(6,9)+"."+this.txtComplemento2.substring(9,12)+"-"+this.txtComplemento2.substring(12,14) ;
	    }
		else { 
			if (this.txtComplemento2.length()<18) this.txtComplemento2 += "                    ";
			return this.txtComplemento2.substring(0,2)+"."+this.txtComplemento2.substring(2,5)+"."+this.txtComplemento2.substring(5,8)+"/"+this.txtComplemento2.substring(8,12)+"-"+this.txtComplemento2.substring(12,14);
		}
	  }
	public String getTxtComplemento2() {
		return txtComplemento2;
	}
	public String getTxtComplemento3() {		
		return txtComplemento3;
	}
	public String getTxtComplemento4() {
		return txtComplemento4;
	}
	public String getTxtComplemento5() {
		return txtComplemento5;
	}
	public String getTxtComplemento6() {
		return txtComplemento6;
	}
	public String getTxtComplemento7() {
		return txtComplemento7;
	}	
	public String getCepEdt()  {
		String cep= txtComplemento4.substring(43,51);
		cep=cep.substring(0,5)+"-"+cep.substring(5,8);
		return cep;
	  }
	public String getCidade() throws BeanException  {
		String cidade= txtComplemento4.substring(51,55);
		cidade=this.BuscaCidade(cidade);
		return cidade;
	  }	
}