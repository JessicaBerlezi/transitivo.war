package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;
/**
 * Relat�rio de Autos de Infra��o por �rg�o autuador
 * @author loliveira
 */
public class RelatAutosPorOrgaoCmd  extends sys.Command {
	private static final String jspPadrao="/GER/RelatAutosPorOrgao.jsp";    
	private String next;
	
	public RelatAutosPorOrgaoCmd() {
		next = jspPadrao;
	}
	
	public RelatAutosPorOrgaoCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
 		    if(OrgaoId == null)	OrgaoId = new ACSS.OrgaoBean() ;	 		
 		    
 		    RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean = (RelatAutosPorOrgaoBean)req.getAttribute("RelatAutosPorOrgaoBean") ;
		    if(RelatAutosPorOrgaoBean == null) RelatAutosPorOrgaoBean = new RelatAutosPorOrgaoBean() ;	 		
			
			String acao = req.getParameter("acao");  
			if(acao == null)acao = "";
			if(acao.equals(""))	acao =" "; 
			
			String statusAuto = req.getParameter("statusAuto");
			if (statusAuto == null) statusAuto = "";
			RelatAutosPorOrgaoBean.setTipoSolicitacao(statusAuto);
			
			String todosOrgaos = req.getParameter("todosOrgaos");
			if (todosOrgaos == null) todosOrgaos = "";
			RelatAutosPorOrgaoBean.setTodosOrgaos(todosOrgaos);
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			RelatAutosPorOrgaoBean.setCodOrgao(codOrgao);
 
			String AnoInicio = req.getParameter("AnoInicio");
			if (AnoInicio == null) AnoInicio = "";
            else{
                RelatAutosPorOrgaoBean.setMesInicial(1);
                RelatAutosPorOrgaoBean.setMesFinal(1);
                RelatAutosPorOrgaoBean.setAnoInicial(Integer.parseInt(AnoInicio));
                RelatAutosPorOrgaoBean.setAnoFinal(Integer.parseInt(AnoInicio)+1);
            }
            
			if (acao.equals("visualizacao"))
			{
				RelatAutosPorOrgaoBean.setCodSistema("40");
                //monta a estrutura do relatorio
				RelatAutosPorOrgaoBean.getMontaRelatorio(RelatAutosPorOrgaoBean);

                //Preenche o relatorio com os totais mensais 
				RelatAutosPorOrgaoBean.getGerarRelatorio(RelatAutosPorOrgaoBean);

                //Preenche os valores do EMITVEX
                RelatAutosPorOrgaoBean.getCarregaDadosVex(RelatAutosPorOrgaoBean);

                //Preenche o relatorio com o somatorio de MR, DOL e Vex
				RelatAutosPorOrgaoBean.getTotalMReDOL(RelatAutosPorOrgaoBean);

                //Preenche o somatorio total
				RelatAutosPorOrgaoBean.getTotalGeral(RelatAutosPorOrgaoBean);
                
                //Preenche a origem
                RelatAutosPorOrgaoBean.getOrigem(RelatAutosPorOrgaoBean);
                
                //Preenche o Acesso
                RelatAutosPorOrgaoBean.getAcesso(RelatAutosPorOrgaoBean);
                
                //Ordena pelo total do somatorio de MR e DOL
                RelatAutosPorOrgaoBean.setClassificaPor("totalGeral");
                RelatAutosPorOrgaoBean.Classifica(RelatAutosPorOrgaoBean);
				
     			OrgaoId.Le_Orgao(codOrgao,0);
				if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()>0)
					OrgaoId.setSigOrgao("TODOS");
				
				String tituloConsulta = "RELAT�RIO REGISTRO DE AUTOS POR �RG�O - " +
				                        "ANO: "+AnoInicio+" - �RG�O: "+ OrgaoId.getSigOrgao();
				req.setAttribute("tituloConsulta", tituloConsulta);
				next = "/GER/RelatAutosPorOrgaoImp.jsp";
				
				String msg = "";
				if(RelatAutosPorOrgaoBean.getDados().size()<=0)
					msg = "N�O EXISTE REGISTROS PARA ESTE ANO.";
				req.setAttribute("msg",msg);
			}
		   
			
			req.setAttribute("RelatAutosPorOrgaoBean",RelatAutosPorOrgaoBean);
			req.setAttribute("statusAuto",statusAuto);
			req.setAttribute("OrgaoId",OrgaoId);

		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
