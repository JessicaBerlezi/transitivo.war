package GER;

/**
* <b>Title:</b>        Gerencial - Relatorio de Motivos de Cancelamento<br>
* <b>Description:</b>  Informa motivo de Deferimento dos autos. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import REC.InfracaoBean;

public class RelatMotivoCancelCmd extends sys.Command {
	private static final String jspPadrao = "/GER/RelatMotivoCancel.jsp";
	private String next;
	
	public RelatMotivoCancelCmd() { 
		next = jspPadrao; 
	}
	public RelatMotivoCancelCmd(String next) { 
		this.next = next; 
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException	{
		String nextRetorno = next;
		try {
            //Carrega os Beans
        	 RelatMotivoCancelBean RelatMotivoCancelId = (RelatMotivoCancelBean) req.getSession().getAttribute("RelatMotivoCancelId");
			 if(RelatMotivoCancelId == null)
                 RelatMotivoCancelId = new RelatMotivoCancelBean();
			
             InfracaoBean InfracaoBeanId = (InfracaoBean) req.getSession().getAttribute("InfracaoBeanId");
             if(InfracaoBeanId == null)
                 InfracaoBeanId = new InfracaoBean();
             
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoId");
			 if (OrgaoBeanId == null) 
                 OrgaoBeanId = new ACSS.OrgaoBean();
			 
             
         	 //Carrega os dados da Tela
         	 String datInicio = req.getParameter("datInicio");
			 if (datInicio == null) 
                 datInicio = sys.Util.formatedToday().substring(0, 10);;
             RelatMotivoCancelId.setDataIni(datInicio);
			 
         	 String datFim = req.getParameter("datFim");
			 if (datFim == null) 
                 datFim = sys.Util.formatedToday().substring(0, 10);;
             RelatMotivoCancelId.setDataFim(datFim);

			 String codOrgao = req.getParameter("codOrgao");
 			 if (codOrgao == null) 
                 codOrgao = "";
             RelatMotivoCancelId.setCodOrgao(codOrgao);
             
             String codMotivo = req.getParameter("codMotivo");
             if (codMotivo == null) 
                 codMotivo = "";
             RelatMotivoCancelId.setCodMotivoDef(codMotivo);
             
             String matAgente = req.getParameter("matAgente");
             if (matAgente == null) 
                 matAgente = ""; 
             RelatMotivoCancelId.setMatAgente(matAgente);
             
             String codInfracao = req.getParameter("codInfracao");
             if(codInfracao == null)
                 codInfracao = "";
             RelatMotivoCancelId.setCodInfracao(codInfracao);
             
 			 String acao = req.getParameter("acao");
             if( acao == null ) 
                 acao = "";
            
             if ( acao.equals("") ) 
             {
                InfracaoBeanId.LeInfracao("enquadramento");
             }
             else if(acao.equals("ImprimeRelatorio"))
             {    
	   			    String tipoReq = req.getParameter("TPreq");
	            	String LabelRec = "";
	    			if ("0".equals(tipoReq))
                    {     
	    			 	tipoReq = "'DP','DC'";
	    			 	LabelRec = "DEFESA PR�VIA";
					}
                    else if("1".equals(tipoReq))
                    { 
					 	tipoReq = "'1P','1C'";
					 	LabelRec = "1a. INST�NCIA";
					}
                    else
                    {
					 	tipoReq = "'2P','2C'";
					 	LabelRec = "2a. INST�NCIA";
					}
	    			RelatMotivoCancelId.setTpreq(tipoReq);
                    
                    //Dados do Cabecalho do Relat�rio
	    			String tituloConsulta = "RELAT�RIO DE MOTIVO DE CANCELAMENTO : "+ RelatMotivoCancelId.getNomMotivoDef().toUpperCase() + " - "+
                                            LabelRec+" <br> " + RelatMotivoCancelId.getDataIni()+ " A "+RelatMotivoCancelId.getDataFim();
                    req.setAttribute("tituloConsulta", tituloConsulta);

                    //Carrega os dados
                    RelatMotivoCancelId.getAutosCancel(RelatMotivoCancelId);
                    RelatMotivoCancelId.getTotalAutosCancel(RelatMotivoCancelId);
                    String msg = "";
	    			if(RelatMotivoCancelId.getDados().size()==0)
	    				msg ="N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE PER�ODO.";
	    			req.setAttribute("msg", msg);
					nextRetorno = "/GER/RelatMotivoCancelImp.jsp";
			  }
             
  			  req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
			  req.setAttribute("RelatMotivoCancelId", RelatMotivoCancelId);
              req.setAttribute("InfracaoBeanId", InfracaoBeanId);
			  
		} catch (Exception e) {	
			throw new sys.CommandException("RelatMotivoCancelCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}