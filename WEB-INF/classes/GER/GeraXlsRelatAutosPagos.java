package GER;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sys.Util;

public class GeraXlsRelatAutosPagos extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
	{
		String dataIni ="";
		String codOrgao = "";
		String jspPadrao = "/GER/RelatAutosPagos.jsp";
		String JSPDIR   = "/WEB-INF/jsp/";
				        
		try {
			RelatAutosPagosBean relatAutosPagosBean = (RelatAutosPagosBean) req.getSession().getAttribute("relatAutosPagosBean");
			if( relatAutosPagosBean == null ) relatAutosPagosBean = new RelatAutosPagosBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             String contexto = req.getParameter("contexto");
             if( contexto == null ) contexto = "";
             
             String nomArquivo = req.getParameter("nomArquivo");
             if( nomArquivo == null ) nomArquivo = "";
            
             if ( acao.equals("") ) {
				relatAutosPagosBean.setDataIni(Util.getDataHoje().substring(3).replaceAll("-","/"));
                req.setAttribute("relatAutosPagosBean", relatAutosPagosBean);
            }
            else if(acao.equals("GeraExcel")) 
			{
				dataIni = req.getParameter("dataIni");
				relatAutosPagosBean.setDataIni(dataIni);
				
				codOrgao = req.getParameter("codOrgao");
				if (codOrgao == null)codOrgao = "";
				relatAutosPagosBean.setCodOrgao(codOrgao);
				
				String todosOrgaos = req.getParameter("todosOrgaos");
				if (todosOrgaos == null) todosOrgaos = "";
				relatAutosPagosBean.setTodosOrgao(todosOrgaos);

				if( dataIni==null ||dataIni.equals(""))
				{ 
					dataIni = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					dataIni = mes +"/"+ ano;     
				}  
				if( relatAutosPagosBean.consultaAutosPagos(relatAutosPagosBean) )				{ 

					RelatAutosPagosXls geraxls = new RelatAutosPagosXls();

					String tituloConsulta = "RELAT�RIO DE AUTOS PAGOS - "+relatAutosPagosBean.getDataIni();
		
	            	ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
	            	paramSys.setCodSistema("24"); // M�dulo GER
	            	paramSys.PreparaParam();
	            	String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
					geraxls.write(relatAutosPagosBean, tituloConsulta, arquivo);
					
					req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
					rd.forward(req, resp);	
				}
				else {
					  relatAutosPagosBean.setDataIni( dataIni );
					  String  msg = "N�O EXISTEM REGISTROS NESTE M�S." ;
					  relatAutosPagosBean.setMsgErro(msg);
					  req.setAttribute("relatAutosPagosBean", relatAutosPagosBean);
					  RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + jspPadrao);
					  rd.forward(req, resp);
					  
				}
		
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());            
		} 
	}
  }
}
