package GER;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import sys.BeanException;
import sys.DaoException;

public class RelatInfTipoVeiculoBean extends sys.HtmlPopupBean {
	
	public static final String COD_STATUS = "2";

	private String codOrgao;

	private String sigOrgao;

	private String datInicial;

	private String datFinal;
	
	private String datInfracao;

	private String msgErro;

	private String codTipo; // cod. tipo do ve�culo
	
	private String dscTipo; // dsc. tipo do ve�culo
	
	private HashMap qtdinf; 
	
	private HashMap totalInf; 
	
	private ArrayList dados;
	
	private int qtdInfracoes;
	
		
	

	public RelatInfTipoVeiculoBean() throws sys.BeanException {
		this.codOrgao = "";
		this.sigOrgao = "";
		this.codTipo = "";
		this.dscTipo = "";
		this.msgErro = "";
		this.datInicial = sys.Util.formatedToday().substring(0, 10);
		this.datFinal = sys.Util.formatedToday().substring(0, 10);
		this.datInfracao = sys.Util.formatedToday().substring(0, 10);
		this.qtdinf = new HashMap();
		this.totalInf = new HashMap();
		this.dados = new ArrayList();
		this.qtdInfracoes = 0;
	}
	
	

	public void setCodOrgao(String codOrgao) {
		if (codOrgao == null)
			this.codOrgao = "";
		else
			this.codOrgao = codOrgao;
	}

	public String getCodOrgao() {
		return codOrgao;
	}

	public void setDatFinal(String datFinal) {
		if (datFinal == null)
			this.datFinal = sys.Util.formatedToday().substring(0, 10);
		else
			this.datFinal = datFinal;
	}

	public String getDatFinal() {
		return datFinal;
	}

	public void setDatInicial(String datInicial) {
		if (datInicial == null)
			datInicial = sys.Util.formatedToday().substring(0, 10);
		else
			this.datInicial = datInicial;
	}

	public String getDatInicial() {
		return datInicial;
	}
	
	public void setDatInfracao(String datInfracao) {
		if (datInfracao == null)
			datInfracao = sys.Util.formatedToday().substring(0, 10);
		else
			this.datInfracao = datInfracao;
	}

	public String getDatInfracao() {
		return datInfracao;
	}

	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao == null)
			this.sigOrgao = "";
		else
			this.sigOrgao = sigOrgao;
	}

	public String getSigOrgao() {
		return sigOrgao;
	}

	public void setDados(ArrayList dados) {
		this.dados = dados;
	}

	public ArrayList getDados() {
		return dados;
	}
	
	public void setCodTipo(String codTipo) {
		if(codTipo == null)
			this.codTipo = "";
		else
		    this.codTipo = codTipo;
	}
	
	public String getCodTipo() {
		return codTipo;
	}

	public void setDscTipo(String dscTipo) {
		if(dscTipo == null)
			this.dscTipo = "";
		else
			this.dscTipo = dscTipo;
	}
	
	public String getDscTipo() {
		return dscTipo;
	}
		
	public void setQtdInfracoes(int qtdInfracoes) {
		this.qtdInfracoes = qtdInfracoes;
	}

	public int getQtdInfracoes() {
		return qtdInfracoes;
	}
	
	public void setMsgErro(String msgErro) {
		if(msgErro == null)
			this.msgErro = "";
		else
		    this.msgErro = msgErro;
	}

	public String getMsgErro() {
		return msgErro;
	}
	
	
	public void setQtdinf(HashMap qtdinf) {
		this.qtdinf = qtdinf;
	}
	
	public HashMap getQtdinf() {
		return qtdinf;
	}
	
	public void setTotalInf(HashMap totalInf) {
		this.totalInf = totalInf;
	}
   
	public HashMap getTotalInf() {
		
		return totalInf;
	}
	
	public String getQtdInfracoes(String key, RelatInfTipoVeiculoBean bean){
		String qtdInfracao = "0";
		if (bean.getQtdinf().get(key)!= null)
		{
			qtdInfracao = formataIntero(Integer.parseInt(bean.getQtdinf().get(key).toString()));
		}
		
		return qtdInfracao;
	}
	
	public String formataIntero (int valor){
		  String valorFormatado = "";
		  if (valor != 0){
			  DecimalFormat formato = new DecimalFormat(",#00") ;
			  valorFormatado = formato.format(valor); 
		  }
		  else valorFormatado = "0";
		  return valorFormatado;
	}

	public void getQtdInfracoes(RelatInfTipoVeiculoBean relatInfTipoVeiculoBean, ACSS.UsuarioBean UsrLogado) throws  BeanException
	{
		try {
			DaoControle dao = DaoControle.getInstance();
			dao.getQtdInfracoes(relatInfTipoVeiculoBean,UsrLogado);
		} catch (DaoException e) {
			throw new BeanException(e.getMessage());
		}
		
	}
	
	public void getTotalInfracoes(RelatInfTipoVeiculoBean relatInfTipoVeiculoBean)
	    throws  BeanException
	{ 
		try{
			int total1 = 0, total2 = 0, total3 = 0, total4 = 0, total5 = 0, total6 = 0;
			int total7 = 0, total8 = 0, total10 = 0, total11 = 0, total13 = 0;
			int total14 = 0, total17 = 0, total18 = 0,total19 = 0, total20 = 0, total21 = 0;
			int total23 = 0, total24 = 0, total25 = 0;
			Iterator it = relatInfTipoVeiculoBean.getDados().iterator();
			RelatInfTipoVeiculoBean bean = new RelatInfTipoVeiculoBean();
			while (it.hasNext())
			{
				bean = (RelatInfTipoVeiculoBean)it.next();
				if (bean.getQtdinf().get("1") != null) {
					total1 += Integer.parseInt(bean.getQtdinf().get("1")
							.toString());
				}
				if (bean.getQtdinf().get("2") != null) {
					total2 += Integer.parseInt(bean.getQtdinf().get("2")
							.toString());
				}
				if (bean.getQtdinf().get("3") != null) {
					total3 += Integer.parseInt(bean.getQtdinf().get("3")
							.toString());
				}
				if (bean.getQtdinf().get("4") != null) {
					total4 += Integer.parseInt(bean.getQtdinf().get("4")
							.toString());
				}
				if (bean.getQtdinf().get("5") != null) {
					total5 += Integer.parseInt(bean.getQtdinf().get("5")
							.toString());
				}
				if (bean.getQtdinf().get("6") != null) {
					total6 += Integer.parseInt(bean.getQtdinf().get("6")
							.toString());
				}
				if (bean.getQtdinf().get("7") != null) {
					total7 += Integer.parseInt(bean.getQtdinf().get("7")
							.toString());
				}
				if (bean.getQtdinf().get("8") != null) {
					total8 += Integer.parseInt(bean.getQtdinf().get("8")
							.toString());
				}
				if (bean.getQtdinf().get("10") != null) {
					total10 += Integer.parseInt(bean.getQtdinf().get("10")
							.toString());
				}
				if (bean.getQtdinf().get("11") != null) {
					total11 += Integer.parseInt(bean.getQtdinf().get("11")
							.toString());
				}
				if (bean.getQtdinf().get("13") != null) {
					total13 += Integer.parseInt(bean.getQtdinf().get("13")
							.toString());
				}
				if (bean.getQtdinf().get("14") != null) {
					total14 += Integer.parseInt(bean.getQtdinf().get("14")
							.toString());
				}
				if (bean.getQtdinf().get("17") != null) {
					total17 += Integer.parseInt(bean.getQtdinf().get("17")
							.toString());
				}
				if (bean.getQtdinf().get("18") != null) {
					total18 += Integer.parseInt(bean.getQtdinf().get("18")
							.toString());
				}
				if (bean.getQtdinf().get("19") != null) {
					total19 += Integer.parseInt(bean.getQtdinf().get("19")
							.toString());
				}
				if (bean.getQtdinf().get("20") != null) {
					total20 += Integer.parseInt(bean.getQtdinf().get("20")
							.toString());
				}
				if (bean.getQtdinf().get("21") != null) {
					total21 += Integer.parseInt(bean.getQtdinf().get("21")
							.toString());
				}
				if (bean.getQtdinf().get("23") != null) {
					total23 += Integer.parseInt(bean.getQtdinf().get("23")
							.toString());
				}
				if (bean.getQtdinf().get("24") != null) {
					total24 += Integer.parseInt(bean.getQtdinf().get("24")
							.toString());
				}
				if (bean.getQtdinf().get("25") != null) {
					total25 += Integer.parseInt(bean.getQtdinf().get("25")
							.toString());
				}
			}
			
			/* >> Carregando o total de cada tipo << */
			
			relatInfTipoVeiculoBean.getTotalInf().put("1",total1);
			relatInfTipoVeiculoBean.getTotalInf().put("2",total2);
			relatInfTipoVeiculoBean.getTotalInf().put("3",total3);
			relatInfTipoVeiculoBean.getTotalInf().put("4",total4);
			relatInfTipoVeiculoBean.getTotalInf().put("5",total5);
			relatInfTipoVeiculoBean.getTotalInf().put("6",total6);
			relatInfTipoVeiculoBean.getTotalInf().put("7",total7);
			relatInfTipoVeiculoBean.getTotalInf().put("8",total8);
			relatInfTipoVeiculoBean.getTotalInf().put("10",total10);
			relatInfTipoVeiculoBean.getTotalInf().put("11",total11);
			relatInfTipoVeiculoBean.getTotalInf().put("13",total13);
			relatInfTipoVeiculoBean.getTotalInf().put("14",total14);
			relatInfTipoVeiculoBean.getTotalInf().put("17",total17);
			relatInfTipoVeiculoBean.getTotalInf().put("18",total18);
			relatInfTipoVeiculoBean.getTotalInf().put("19",total19);
			relatInfTipoVeiculoBean.getTotalInf().put("20",total20);
			relatInfTipoVeiculoBean.getTotalInf().put("21",total21);
			relatInfTipoVeiculoBean.getTotalInf().put("23",total23);
			relatInfTipoVeiculoBean.getTotalInf().put("24",total24);
			relatInfTipoVeiculoBean.getTotalInf().put("25",total25);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
}