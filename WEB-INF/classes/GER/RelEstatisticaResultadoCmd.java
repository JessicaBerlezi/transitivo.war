package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RelEstatisticaResultadoCmd extends sys.Command {
	private static final String jspPadrao = "/GER/RelEstatisticaResultado.jsp";
	private String next;
	
	public RelEstatisticaResultadoCmd() { 
		next = jspPadrao; 
	}
	public RelEstatisticaResultadoCmd(String next) { 
		this.next = next; 
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException	{
		String nextRetorno = next;
		try {
             ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
			 
			 ResultadoBean ResultadoBeanId = (ResultadoBean) req.getSession().getAttribute("ResultadoBeanId");
			 if( ResultadoBeanId == null )
			 	ResultadoBeanId = new ResultadoBean();
			 
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) 
			 	OrgaoBeanId = new ACSS.OrgaoBean();
			 
             
			 //Verifica a Acao escolhida
			 String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
            
             if ( acao.equals("") ) 
             {
                req.setAttribute("ResultadoBeanId", ResultadoBeanId);
                req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
             }
             else if(acao.equals("ImprimeRelatorio")){    
	            	
             	   //Carrego os dados da Tela
	 			    String tipoJunta = req.getParameter("TPreq");
	 			    if (tipoJunta == null) tipoJunta = "";
	            	
	            	String mesAno = req.getParameter("mesAno");
	   			    if (mesAno == null) mesAno = "";
	
	   			    String codOrgao = req.getParameter("codOrgao");
	    			if (codOrgao == null) codOrgao = "";
	     			
	    			String todosOrgaos = req.getParameter("TodosOrgaos");
	   			    if (todosOrgaos == null) todosOrgaos = "";
	    			
	   			    String tipoReq = req.getParameter("TPreq");
	            	String LabelRec = "";
	    			if ("0".equals(tipoReq)){     
	    			 	tipoReq = "'DP','DC'";
	    			 	LabelRec = "DEFESA PR�VIA";
					}else if("1".equals(tipoReq)){ 
					 	tipoReq = "'1P','1C'";
					 	LabelRec = "1a. INST�NCIA";
					}else{
					 	tipoReq = "'2P','2C'";
					 	LabelRec = "2a. INST�NCIA";
					}
	            	    			
	    			//Carrego Bean de Resultado
	    			ResultadoBeanId.setMesAno(mesAno);
	    			ResultadoBeanId.setTpreq(tipoReq);
	    			ResultadoBeanId.setCodEstado(SistemaBeanId.getCodUF());    			 
					ResultadoBeanId.setOrgao(codOrgao);
	    			ResultadoBeanId.setTipoJunta(tipoJunta);
					if ("S".equals(todosOrgaos)) ResultadoBeanId.setSigOrgao("Todos");
					 
			        //Carrega os relatores por junta
					ResultadoBeanId.getRelatores(ResultadoBeanId);
					ResultadoBeanId.montaEstruturaRelatorio(ResultadoBeanId);
 				    ResultadoBeanId.buscaDadosEstatisticos(ResultadoBeanId);
	    			String tituloConsulta = "ESTAT�STICA DE RESULTADOS DA "+ LabelRec + " - M�S DE "
					    + ResultadoBeanId.getMesExtenso(ResultadoBeanId.getMesAno())
						+ "/"+ResultadoBeanId.getMesAno().substring(3,7);
	    			
	    			//Mensagem de Erro
	    			if(ResultadoBeanId.getResultado().size()==0)
	    				ResultadoBeanId.setMsgErro("N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE M�S");
	    			
	    			req.setAttribute("tituloConsulta", tituloConsulta);
	    			req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
					req.setAttribute("ResultadoBeanId", ResultadoBeanId);
					nextRetorno = "/GER/RelEstatisticaResultadoImp.jsp";
			  }
		} catch (Exception e) {	throw new sys.CommandException("RelEstatisticaResultadoCmd: " + e.getMessage());	}		
		return nextRetorno;
	}
}