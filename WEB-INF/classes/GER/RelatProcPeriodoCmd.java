package GER;

/**
* <b>Title:</b>        Gerencial - Relatorio de Processos Por Per�odo<br>
* <b>Description:</b>  Informa os autos deferidos/indeferidos por relator. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RelatProcPeriodoCmd extends sys.Command {
	private static final String jspPadrao = "/GER/RelatProcPeriodo.jsp";
	private String next;
	
	public RelatProcPeriodoCmd() { 
		next = jspPadrao; 
	}
	public RelatProcPeriodoCmd(String next) { 
		this.next = next; 
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException	{
		String nextRetorno = next;
		try {
        	 RelatProcPeriodoBean RelatProcPeriodoId = (RelatProcPeriodoBean) req.getSession().getAttribute("RelatProcPeriodoId");
			 if( RelatProcPeriodoId == null )RelatProcPeriodoId = new RelatProcPeriodoBean();
			
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
			 ProdProcessoBean ProdProcessoBeanId = (ProdProcessoBean)req.getAttribute("ProdProcessoBeanId") ;
			 if (ProdProcessoBeanId==null)  ProdProcessoBeanId = new ProdProcessoBean() ;

         	 //Carrego os dados da Tela
			 String tipoJunta = req.getParameter("TPreq");
			 if (tipoJunta == null) tipoJunta = "";
			 
			 String tipoOrdem = req.getParameter("tipoOrdem");
			 if (tipoOrdem == null) tipoOrdem="";

			 String tipoSubOrdem = req.getParameter("tipoSubOrdem");
			 if (tipoSubOrdem == null) tipoSubOrdem="";
			 
         	 String datInicio = req.getParameter("datInicio");
			 if (datInicio == null) datInicio = "";
			 
         	 String datFim = req.getParameter("datFim");
			 if (datFim == null) datFim = "";

			 String codOrgao = req.getParameter("codOrgao");
 			 if (codOrgao == null) codOrgao = "";
             
 			 String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
            
             if ( acao.equals("") ) 
             {
                req.setAttribute("RelatProcPeriodoId", RelatProcPeriodoId);
                req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
             }
             else if(acao.equals("ImprimeRelatorio")){    
	   			    String tipoReq  = req.getParameter("TPreq");
	            	String LabelRec = "";
	    			if ("0".equals(tipoReq)){     
	    			 	tipoReq = "'DP','DC'";
	    			 	LabelRec = "DEFESA PR�VIA";
					} else if("1".equals(tipoReq)){ 
					 	tipoReq = "'1P','1C'";
					 	LabelRec = "1a. INST�NCIA";
					} else {
					 	tipoReq = "'2P','2C'";
					 	LabelRec = "2a. INST�NCIA";
					}
                    
                    String tipoResult = req.getParameter("Tresult");
                    RelatProcPeriodoId.setIndResultado(tipoResult);
	    			RelatProcPeriodoId.setDataIni(datInicio);
	    			RelatProcPeriodoId.setDataFim(datFim);
	    			RelatProcPeriodoId.setCodOrgao(codOrgao);
	    			RelatProcPeriodoId.setTpReq(tipoReq);
	    			RelatProcPeriodoId.setTipoOrdem(tipoOrdem);
	    			RelatProcPeriodoId.setTipoSubOrdem(tipoSubOrdem);
	    			
					 
			        //Carrega os relatores 
	    			RelatProcPeriodoId.buscarProcessoPorPeriodo();
	    			
	    			String tituloConsulta = "<br />AUTOS JULGADOS POR PER�ODO - "+ LabelRec + " - "
					    + RelatProcPeriodoId.getDataIni()+ " a "+RelatProcPeriodoId.getDataFim();
	    			
	    			String tituloCab = "<br />AUTOS JULGADOS POR PER�ODO - "+ LabelRec + " - "
				    + RelatProcPeriodoId.getDataIni()+ " a "+RelatProcPeriodoId.getDataFim();
	    			
	    			if (tipoOrdem.equals("U")){
	    				tituloConsulta+="<br />ORDEM : USERNAME";
	    				tituloCab+="<br />ORDEM : USERNAME";
	    			}
	    			else{
	    				tituloConsulta+="<br />ORDEM : RELATOR";
	    				tituloCab+="<br />ORDEM : RELATOR";
	    			}
	    			if (tipoSubOrdem.equals("C")){
	    				tituloConsulta+=" e CPF/CNPJ<br /><br />";
	    				tituloCab+=" e CPF/CNPJ<br /><br />";
	    			}else{
	    				tituloConsulta+=" e INFRA��O<br /><br />";
	    				tituloCab+=" e INFRA��O<br /><br />";
	    			}
	    				
	    			String msg = "";
	    			if(RelatProcPeriodoId.getListProcessos().size()==0)
	    				msg ="N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE PER�ODO.";
	    			req.setAttribute("msg", msg);
	    			req.setAttribute("tituloConsulta", tituloConsulta);
	    			req.setAttribute("tituloCab", tituloCab);
					nextRetorno = "/GER/RelatProcPeriodoImp.jsp";
			  }
             
  			  req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
			  req.setAttribute("RelatProcPeriodoId", RelatProcPeriodoId);
			  req.setAttribute("ProdProcessoBeanId", ProdProcessoBeanId);
		} catch (Exception e) {	
			throw new sys.CommandException("EstatisticaArtigoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}