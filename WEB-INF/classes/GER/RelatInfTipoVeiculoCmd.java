package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;
import ACSS.OrgaoBean;

/**
 * Relat�rio de incid�ncia de infra��es de vans, �nibus, motos e caminh�es.
 * 
 * @author loliveira
 */
public class RelatInfTipoVeiculoCmd extends sys.Command {
	private static final String jspPadrao = "/GER/RelatInfTipoVeiculo.jsp";

	private String next;

	public RelatInfTipoVeiculoCmd() {
		next = jspPadrao;
	}

	public RelatInfTipoVeiculoCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(ServletContext contexto, HttpServletRequest req,
			HttpServletResponse res) throws CommandException {
		try {
			/* >> cria os Beans de sessao, se n�o existir << */

			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session
					.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)
				UsrLogado = new ACSS.UsuarioBean();

			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean) req
					.getAttribute("OrgaoId");
			if (OrgaoId == null)
				OrgaoId = new ACSS.OrgaoBean();

			RelatInfTipoVeiculoBean relatInfTipoVeiculoBean = (RelatInfTipoVeiculoBean) req
					.getAttribute("relatInfTipoVeiculoBean");
			if (relatInfTipoVeiculoBean == null)
				relatInfTipoVeiculoBean = new RelatInfTipoVeiculoBean();

			
			/* >> Armazena os valores digitados na tela << */
			
			String acao = req.getParameter("acao");
			if (acao == null)
				acao = "";
			
			String codOrgao = req.getParameter("codOrgao");
			relatInfTipoVeiculoBean.setCodOrgao(codOrgao);
			
			String datInicial = req.getParameter("datInicial");
			relatInfTipoVeiculoBean.setDatInicial(datInicial);
			
			String datFinal = req.getParameter("datFinal");
			relatInfTipoVeiculoBean.setDatFinal(datFinal);

			
			if (acao.equals("ImprimeRelatorio")) {
				/* >> Gera Estatistica de infracoes << */
				
				relatInfTipoVeiculoBean.getQtdInfracoes(relatInfTipoVeiculoBean, UsrLogado);
				
				
				/* >> Calcula total geral de infracoes por tipo veiculo << */
				
				relatInfTipoVeiculoBean.getTotalInfracoes(relatInfTipoVeiculoBean);
				
				/* >> Prepara msg caso nao tenha nenhum registro << */
				
				String msg = "";
				if (relatInfTipoVeiculoBean.getDados().size() <= 0)
				{
					msg = "N�O EXISTE REGISTROS PARA ESTE PER�ODO DE DIAS.";
					req.setAttribute("msg", msg);
				}
				/* >> Prepara titulo do Relatorio << */
					
				OrgaoBean myOrgao = new OrgaoBean();
				myOrgao.Le_Orgao(relatInfTipoVeiculoBean.getCodOrgao(),0);
				
				String tituloConsulta = "RELAT�RIO DE INFRA��ES POR TIPO DE VE�CULO <br> "
					+ " PER�ODO DE "+relatInfTipoVeiculoBean.getDatInicial()+" AT� "
					+ relatInfTipoVeiculoBean.getDatFinal()+" - �RG�O: "
					+ myOrgao.getSigOrgao();
				req.setAttribute("tituloConsulta", tituloConsulta);
				next = "/GER/RelatInfTipoVeiculoImp.jsp";

				
			}

			req.setAttribute("relatInfTipoVeiculoBean", relatInfTipoVeiculoBean);
			req.setAttribute("OrgaoId", OrgaoId);

		} catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
