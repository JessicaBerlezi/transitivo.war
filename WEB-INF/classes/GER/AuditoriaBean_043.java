package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE ORIGEM EVENTO<br>
* <b>Description:</b>  Bean Auditoria_043 - Informa��es da transa��o 093<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_043 extends AuditoriaBean {	
	
	
	private String codigoOrigemEvento	;	
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String dscOrigemEvento	 	;	
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_043() throws sys.BeanException {		
	
		
		codigoOrigemEvento 	= "";		
		tipoAcao 			= "";
		indContinuidade     = "";
		dscOrigemEvento 	= "";		
		codOrgao  			= "";
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",69);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	
	    
		 		
		 this.codigoOrigemEvento= reg.substring(0,3)     ;
	     this.tipoAcao 			= reg.substring(3,4)     ;
	     this.indContinuidade 	= reg.substring(4,7)     ;
	     this.dscOrigemEvento 	= reg.substring(7,37)    ;	     
	     this.codOrgao 			= reg.substring(37,43)   ;
	     this.codOrgoaLotacao 	= reg.substring(43,49)   ;
	     this.nomUserName 		= reg.substring(49,69)   ;	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }		     
	     
	}
	
	public String getCodigoOrigemEvento() {		
		return codigoOrigemEvento;
	}		
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDscOrigemEvento() {
		return dscOrigemEvento;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	}