package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.DaoException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - ENVIO DE RECURSO/RESULTADO/ESTORNO 1 INST�NCIA (RECJARI)<br>
* <b>Description:</b>  Bean Auditoria_049 - Informa��es da transa��o 403<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_050 extends AuditoriaBean {
	
	private String nomUserName			;
	private String codOrgoaLotacao		;
	private String codOrgao 			;	
	private String codBaixa 			;	
	private String numAutoInfracao  	;
	private String numNotificacao		;	
	private String numPlaca				;
	private String dataRecebimento  	;
	private String dscErro				;
	
	
	
	public AuditoriaBean_050() throws sys.BeanException {
		
		nomUserName     	 		 = "";
		codOrgoaLotacao      		 = "";
		codOrgao  			 		 = "";
		codBaixa					 = "";		
		numAutoInfracao  			 = "";
		numNotificacao  			 = "";		
		numPlaca      				 = "";
		dataRecebimento  			 = "";	
		dscErro						 = "";
			
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",253);		 
	    
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 
		 this.nomUserName   		= reg.substring(0,20)   ;
	     this.codOrgoaLotacao 		= reg.substring(20,26)  ;
	     this.codOrgao 				= reg.substring(26,32)  ;	
	     this.numNotificacao		= reg.substring(35,44)  ;	
	     this.dataRecebimento 		= reg.substring(63,71)  ;	
	     this.codBaixa				= reg.substring(71,73)  ;
	     this.dscErro				= reg.substring(133,reg.length());
	     
	     /*
	     try {
			Dao daoGer= Dao.getInstance();			
			daoGer.BuscaVexMaster(this);
			
		} catch (DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	     
	     
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}	
	

		public String getCodOrgao() {
			return codOrgao;
		}
		
		public String getCodBaixa() {
			return codBaixa;
		}

		public String getCodOrgoaLotacao() {
			return codOrgoaLotacao;
		}
		

		public String getDataRecebimento() {
			String data = "";
			data = this.dataRecebimento.substring(6,8)+"/"+this.dataRecebimento.substring(4,6)+"/"+
			this.dataRecebimento.substring(0,4);
			return data;
		}

		public String getNumNotificacao() {
			return numNotificacao;
		}

		public String getNomUserName() {
			return nomUserName;
		}
		
		public String getDscErro() {
			return dscErro;
		}

		public String getNumAutoInfracao() {
			return numAutoInfracao;
		}

		public String getNumPlaca() {
			return numPlaca;
		}

		public void setNumAutoInfracao(String numAutoInfracao) {
			if(numAutoInfracao==null) numAutoInfracao="";
			this.numAutoInfracao = numAutoInfracao;
		}

		public void setNumPlaca(String numPlaca) {
			if(numPlaca==null) numPlaca="";
			this.numPlaca = numPlaca;
		}
	
}