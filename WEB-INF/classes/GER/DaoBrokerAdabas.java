package GER;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import sys.DaoException;

import sys.Util;

public class DaoBrokerAdabas extends sys.DaoBase implements DaoBroker
{
	
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "GER";
	
	public DaoBrokerAdabas() throws DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	public void consultaTotalPagoDia(AutosPagosBean myAutosPagosId,ACSS.UsuarioBean UsrLogado) throws DaoException {
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			ResultSet rs       = null;
			String sCmd        = "SELECT DAT_PAGTO,COUNT(*) as QTD,SUM(VALOR_PAGO) as TOTAL_PAGO FROM TGER_AUTOS_PAGOS "+ 
			"WHERE "+ 
			"COD_ORGAO='"+myAutosPagosId.getCodOrgao()+"'"+
			"DAT_MES_REF='"+myAutosPagosId.getMesAno()+"'"+
			"GROUP BY DAT_PAGTO "+
			"ORDER BY DAT_PAGTO ";
			rs    = stmt.executeQuery(sCmd) ;
			ArrayList<AutosPagosDiaBean>ListTotalPago = new ArrayList<AutosPagosDiaBean>();			
			while (rs.next()) {
				AutosPagosDiaBean myTotalPago = new AutosPagosDiaBean();
				String sDia=Util.converteData(rs.getDate("DAT_PAGTO"));
				myTotalPago.setDia(sDia.substring(0,2));
				myTotalPago.setQtdPagos(rs.getString("QTD"));
				myTotalPago.setTotalPago(rs.getString("TOTAL_PAGO"));
				ListTotalPago.add(myTotalPago);				
			}	
			myAutosPagosId.setListAutos(ListTotalPago);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("DaoBrokerAdabas-consultaTotalPagoDia : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
		
		
}