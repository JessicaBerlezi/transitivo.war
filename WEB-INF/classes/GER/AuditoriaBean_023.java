package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE PARAMETROS DE ORG�O<br>
* <b>Description:</b>  Bean Auditoria_023 - Informa��es da transa��o 089<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_023 extends AuditoriaBean {	
	
	
	private String codOrgao 			;
	private String diasExpDir 			;
	private String diasFolgaExpDir 		;
	private String diasFolgaDefPrevia 	;
	private String diasJulg1Inst 		;	
	private String diasFolga1Inst 		;
	private String diasFolga2Inst 		;
	private String diasDefPrevia 		;
	private String dias1Inst 			;
	private String dias2Inst 			;
	private String codSecretaria 		;	
	private String codAssDefPrev 		;	
	private String codAssPenalidade 	;
	private String tipoAcao 			;		
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_023() throws sys.BeanException {		
		
		codOrgao  			= "";	
		diasExpDir 			= "";
		diasFolgaExpDir 	= "";
		diasFolgaDefPrevia 	= "";
		diasJulg1Inst 		= "";		
		diasFolga1Inst 		= "";
		diasFolga2Inst 		= "";
		diasDefPrevia 		= "";
		dias1Inst 			= "";
		dias2Inst 			= "";
		codSecretaria 		= "";	
		codAssDefPrev 		= "";	
		codAssPenalidade 	= "";
		tipoAcao 			= "";		
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",71);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();		 
	    
		
	     this.codOrgao 			= reg.substring(0,6);
	     this.diasExpDir 		= reg.substring(6,9);
	     this.diasFolgaExpDir 	= reg.substring(9,12);
	     this.diasFolgaDefPrevia= reg.substring(12,15);
	     this.diasJulg1Inst 	= reg.substring(15,18);	    
	     this.diasFolga1Inst 	= reg.substring(18,21);
	     this.diasFolga2Inst 	= reg.substring(21,24);
	     this.diasDefPrevia 	= reg.substring(24,27);
	     this.dias1Inst 		= reg.substring(27,30);
	     this.dias2Inst 		= reg.substring(30,33);
	     this.codSecretaria 	= reg.substring(33,36);
	     this.codAssDefPrev 	= reg.substring(36,40);
	     this.codAssPenalidade 	= reg.substring(40,44);
	     this.tipoAcao 			= reg.substring(44,45);
	     this.codOrgoaLotacao 	= reg.substring(45,51);
	     this.nomUserName 		= reg.substring(51,71);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	}

	public String getCodAssDefPrev() {
		return codAssDefPrev;
	}
	public String getCodAssPenalidade() {
		return codAssPenalidade;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getCodSecretaria() {
		return codSecretaria;
	}
	public String getDias1Inst() {
		return dias1Inst;
	}
	public String getDias2Inst() {
		return dias2Inst;
	}
	public String getDiasDefPrevia() {
		return diasDefPrevia;
	}
	public String getDiasExpDir() {
		return diasExpDir;
	}
	public String getDiasFolga1Inst() {
		return diasFolga1Inst;
	}
	public String getDiasFolga2Inst() {
		return diasFolga2Inst;
	}
	public String getDiasFolgaDefPrevia() {
		return diasFolgaDefPrevia;
	}
	public String getDiasFolgaExpDir() {
		return diasFolgaExpDir;
	}
	public String getDiasJulg1Inst() {
		return diasJulg1Inst;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	}