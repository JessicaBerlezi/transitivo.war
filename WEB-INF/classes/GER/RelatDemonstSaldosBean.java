package GER;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import sys.BeanException;
import sys.DaoException;

/**
* <b>Title:</b>        Gerencial - Demonstrativo Saldos Bean<br>
* <b>Description:</b>  Mostra o balan�o dos autos nas fases de autua��o, penalidade e Transito Julgado num dado per�odo de tempo. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class RelatDemonstSaldosBean extends sys.HtmlPopupBean {
  
  public  final String TIPO_FASE_AUTUACAO = "1";
  public  final String TIPO_FASE_PENALIDADE = "2";
  public  final String TIPO_FASE_TRANSJULGADO = "3";
	
	private List dados;
	private String codOrgao;
	private String sigOrgao;  
	private String dataIni;
	private String dataFim;
	private String codStatus;
	private String nomStatus;
	
	private String tipoFase; 
	private String qtdAnt;
	private String qtdEnt; 
	private String qtdSai;
	private String qtdAtual;

	private String totalSldAnt;
	private String totalEnt;
	private String totalSai;
	private String totalSldAtu;
	
	private String totalSldAntParcial;
	private String totalEntParcial; 
	private String totalSaiParcial;
	private String totalSldAtuParcial;
	
	private String numRegistro;
  
	public RelatDemonstSaldosBean() throws sys.BeanException {
		dados = new ArrayList();
		codOrgao = "";
		sigOrgao = "";
		dataIni = "";
		dataFim = "";
		codStatus = "";
		nomStatus = "";
				
		tipoFase = ""; 
		qtdAnt = "";
		qtdEnt = ""; 
		qtdSai = "";
		qtdAtual = "";
		
		totalSldAnt = "";
		totalEnt = "";
		totalSai = "";
		totalSldAtu = "";

		totalSldAntParcial = "";
		totalEntParcial = ""; 
		totalSaiParcial = "";
		totalSldAtuParcial = "";
		
		numRegistro = "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}
	public String getNomStatus() {
		return this.nomStatus;
	}
	
	public void setTipoFase(String tipoFase) {
		this.tipoFase = tipoFase;
		if (tipoFase == null)
			this.tipoFase = "";
	}
	public String getTipoFase() {
		return this.tipoFase;
	}
	
	public void setQtdAnt(String qtdAnt) {
		this.qtdAnt = qtdAnt;
		if (qtdAnt == null)
			this.qtdAnt = "";
	}
	public String getQtdAnt() {
		return this.qtdAnt;
	}
	
	public void setQtdEnt(String qtdEnt) {
		this.qtdEnt = qtdEnt;
		if (qtdEnt == null)
			this.qtdEnt = "";
	}
	public String getQtdEnt() {
		return this.qtdEnt;
	}
	
	public void setQtdSai(String qtdSai) {
		this.qtdSai = qtdSai;
		if (qtdSai == null)
			this.qtdSai = "";
	}
	public String getQtdSai() {
		return this.qtdSai;
	}
	
	public void setQtdAtual(String qtdAtual) {
		this.qtdAtual = qtdAtual;
		if (qtdAtual == null)
			this.qtdAtual = "";
	}
	public String getQtdAtual() {
		return this.qtdAtual;
	}
	
	public void setTotalSldAnt(String totalSldAnt) {
		this.totalSldAnt = totalSldAnt;
		if (totalSldAnt == null)
			this.totalSldAnt = "";
	}
	public String getTotalSldAnt() {
		return this.totalSldAnt;
	}
	
	public void setTotalEnt(String totalEnt) {
		this.totalEnt = totalEnt;
		if (totalEnt == null)
			this.totalEnt = "";
	}
	public String getTotalEnt() {
		return this.totalEnt;
	}
	
	public void setTotalSaida(String totalSai) {
		this.totalSai = totalSai;
		if (totalSai == null)
			this.totalSai = "";
	}
	public String getTotalSaida() {
		return this.totalSai;
	}
	
	public void setTotalSldAtu(String totalSldAtu) {
		this.totalSldAtu = totalSldAtu;
		if (totalSldAtu == null)
			this.totalSldAtu = "";
	}
	public String getTotalSldAtu() {
		return this.totalSldAtu;
	}
	
	public void setTotalSldAntParcial(String totalSldAntParcial) {
		this.totalSldAntParcial = totalSldAntParcial;
		if (totalSldAntParcial == null)
			this.totalSldAntParcial = "";
	}
	public String getTotalSldAntParcial() {
		return this.totalSldAntParcial;
	}
	
	public void setTotalEntParcial(String totalEntParcial) {
		this.totalEntParcial = totalEntParcial;
		if (totalEntParcial == null)
			this.totalEntParcial = "";
	}
	public String getTotalEntParcial() {
		return this.totalEntParcial;
	}
	
	public void setTotalSldAtuParcial(String totalSldAtuParcial) {
		this.totalSldAtuParcial = totalSldAtuParcial;
		if (totalSldAtuParcial == null)
			this.totalSldAtuParcial = "";
	}
	public String getTotalSldAtuParcial() {
		return this.totalSldAtuParcial;
	}
	
	public void setTotalSaiParcial(String totalSaiParcial) {
		this.totalSaiParcial = totalSaiParcial;
		if (totalSaiParcial == null)
			this.totalSaiParcial = "";
	}
	public String getTotalSaiParcial() {
		return this.totalSaiParcial;
	}
	
	public void setNumRegistro(String numRegistro) {
		this.numRegistro = numRegistro;
		if (numRegistro == null)
			this.numRegistro = "";
	}
	public String getNumRegistro() {
		return this.numRegistro;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaDemonstrativo (RelatDemonstSaldosBean relatDemonstSaldosBean) throws  BeanException, DaoException{
			boolean existe = false;
			DaoControle dao = DaoControle.getInstance();
			try {
				  if(dao.consultaDemonstrativo(relatDemonstSaldosBean)== true)
				  existe = true;
		  
			} 
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			} 
			return existe;	
	}
	
	public boolean consultaDemonstFinanceiro (RelatDemonstSaldosBean relatDemonstSaldosBean) throws  DaoException, BeanException{
				boolean existe = false;
				DaoControle dao = DaoControle.getInstance();
				try {
					  if(dao.consultaDemonstFinanceiro(relatDemonstSaldosBean)== true)
					  existe = true;
		  
				} 
				catch (Exception e) {
					throw new sys.BeanException(e.getMessage());
				} 
				return existe;	
		}
		
	public String getTipoFase  (String tipoFase) throws  DaoException, BeanException{	
      if(tipoFase.equals(TIPO_FASE_AUTUACAO)) tipoFase = "AUTUA��O";
      if(tipoFase.equals(TIPO_FASE_PENALIDADE))tipoFase = "PENALIDADE";
      if(tipoFase.equals(TIPO_FASE_TRANSJULGADO))tipoFase = "TR�NSITO JULGADO";
      return tipoFase;
	}

	public String formataMoeda (String valor) throws  DaoException, BeanException{	
		NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR")); 
    String valorFormatado = formato.format(Double.parseDouble(valor)); 
    return valorFormatado;
	}

	public String formataIntero (String valor) throws  DaoException, BeanException{	
    DecimalFormat formato = new DecimalFormat(",#00") ;
    String valorFormatado = formato.format(Integer.parseInt(valor)); 
    return valorFormatado;
	}

	public String formataDecimal (String valor) throws  DaoException, BeanException{	
		NumberFormat formato = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
	  String valorFormatado = formato.format(Double.parseDouble(valor)); 
	  return valorFormatado.substring(3);
	}

}