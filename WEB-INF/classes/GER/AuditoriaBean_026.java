package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE AGENTES,UF'S<br>
* <b>Description:</b>  Bean Auditoria_026 - Informa��es daS transa��es 084,085<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_026 extends AuditoriaBean {	
	
	private String codigo 				;
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String descricao			;	
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_026() throws sys.BeanException {		
	
		codigo 				= "";
		tipoAcao 			= "";
		indContinuidade     = "";
		descricao 			= "";		
		codOrgao  			= "";	
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",58);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
	     this.codigo 			= reg.substring(0,2);
	     this.tipoAcao 			= reg.substring(2,3);
	     this.indContinuidade 	= reg.substring(3,6);	
	     this.descricao		 	= reg.substring(6,26);
	     this.codOrgao 			= reg.substring(26,32);
	     this.codOrgoaLotacao 	= reg.substring(32,38);
	     this.nomUserName 		= reg.substring(38,58);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	public String getCodigo() {
		return codigo;
	}	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDescricao() {
		return descricao;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	}