package GER;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RelatArqNotificacaoCmd extends sys.Command 
{
	private static final String jspPadrao = "/GER/RelatArqNotificacao.jsp";
	private String next;
	public RelatArqNotificacaoCmd() { next = jspPadrao; }
	public RelatArqNotificacaoCmd(String next) { this.next = next; }

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{
		String nextRetorno = jspPadrao;
		String datIniRel ="";
		String datFimRel ="";
				        
		try {
             ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
			 RelatArqNotificacaoBean RelArqDownlBeanId = (RelatArqNotificacaoBean) req.getSession().getAttribute("RelArqDownlBeanId");
			if( RelArqDownlBeanId == null ) RelArqDownlBeanId = new RelatArqNotificacaoBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             if ( acao.equals("") ) {

				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
            	
				RelArqDownlBeanId.setDatIni( datIniRel );
            	RelArqDownlBeanId.setDatFim( datFimRel );
                req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
            }
            else if(acao.equals("ImprimeRelatorio")) 
			{
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");

				if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
				{ 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
				}  
				if( RelArqDownlBeanId.ConsultaRelDiario( datIniRel, datFimRel,vErro ) )				{ 
					
					String tituloConsulta = "RELATÓRIO -ARQUIVOS DE NOTIFICAÇÃO : "+ datIniRel +" a "+ datFimRel;
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/GER/RelatArqNotificacaoImp.jsp";
				}
				else {
					  RelArqDownlBeanId.setDatIni( datIniRel );
					  RelArqDownlBeanId.setDatFim( datFimRel );
					  nextRetorno = "/GER/RelatArqNotificacao.jsp";
				}
		
				req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);					
			}
		} catch (Exception e) {	throw new sys.CommandException("RelatArqNotificacaoCmd: " + e.getMessage());	}		
		return nextRetorno;
	}
}