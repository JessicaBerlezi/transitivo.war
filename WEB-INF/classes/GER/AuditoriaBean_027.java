package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE JUNTAS<br>
* <b>Description:</b>  Bean Auditoria_027 - Informa��es daS transa��es 086<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_027 extends AuditoriaBean {	
	
	
	private String codOrgao				;
	private String codigo 				;
	private String tipoAcao 			;	
	private String indContinuidade 		;
	private String descricao	 		;
	private String siglaJunta 			;
	private String tipoJunta 			;
	private String endereco 			;	
	private String numero 				;
	private String complemento 			;	
	private String bairro		 		;
	private String municipio	 		;
	private String cep			 		;	
	private String telefoneDDD 			;	
	private String numTelefone	 		;	
	private String numFax		 		;	
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_027() throws sys.BeanException {		
	
		codOrgao  			= "";
		codigo 				= "";
		tipoAcao 			= "";		
		indContinuidade     = "";
		descricao 			= "";
		siglaJunta 			= "";
		tipoJunta 			= "";
		endereco 			= "";
		numero	 			= "";
		complemento 		= "";
		bairro 				= "";
		municipio 			= "";
		cep 				= "";		
		telefoneDDD 		= "";
		numTelefone 		= "";
		numFax 				= "";		
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",184);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
		 this.codOrgao 			= reg.substring(0,6);
		 this.codigo 			= reg.substring(6,12);
	     this.tipoAcao 			= reg.substring(12,13);
	     this.indContinuidade 	= reg.substring(13,19);
	     this.descricao 		= reg.substring(19,39);
	     this.siglaJunta 		= reg.substring(39,49);	     
	     this.tipoJunta 		= reg.substring(49,50);	     
	     this.endereco 			= reg.substring(50,77);
	     this.numero 			= reg.substring(77,82);
	     this.complemento 		= reg.substring(82,93);
	     this.bairro 			= reg.substring(93,113);
	     if(!"".equals(reg.substring(113,117).trim()))
	    	 this.municipio 	= this.BuscaCidade(reg.substring(113,117));
	     this.cep 				= reg.substring(117,125);	     
	     this.telefoneDDD 		= reg.substring(125,128);
	     this.numTelefone 		= reg.substring(128,143);
	     this.numFax 			= reg.substring(143,158);	     
	     this.codOrgoaLotacao 	= reg.substring(158,164);
	     this.nomUserName 		= reg.substring(164,184);	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }     
	     
	}
	
	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodigo() {
		return codigo;
	}
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	public String getIndContinuidade() {
		return indContinuidade;
	}	
	public String getDescricao() {
		return descricao;
	}
	public String getSiglaJunta() {
		return siglaJunta;
	}	
	public String getTipoJunta() {
		return tipoJunta;
	}	
	
	public String getEndereco() {
		return endereco;
	}
	public String getNumero() {
		return numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public String getCep() {
		String cep =this.cep;
		cep = cep.substring(0,5)+"-"+cep.substring(5,8);		
		return cep;
	}
	public String getMunicipio() {
		return municipio;
	}
	public String getTelefoneDDD() {
		return telefoneDDD;
	}	
	public String getNumTelefone() {
		String tel =this.numTelefone;
		tel = tel.substring(0,4)+"-"+tel.substring(4,8);		
		return tel;
	}
	public String getNumFax() {
		String fax =this.numFax;
		fax = fax.substring(0,4)+"-"+fax.substring(4,8);		
		return fax;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	}