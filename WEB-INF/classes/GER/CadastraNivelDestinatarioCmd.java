package GER;

import java.util.List;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ACSS.SistemaBean;

/**
* <b>Title:</b>GER - Destinatarios x Processos - Niveis<br>
* <b>Description:</b>Seleciona os destinat�rios para cada n�vel<br>
* <b>Copyright:</b>Copyright (c) 2005<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class CadastraNivelDestinatarioCmd extends sys.Command {

	private static final String jspPadrao = "/GER/CadastraNivelDestinatario.jsp";
	private String next;

	public CadastraNivelDestinatarioCmd() {
		next = jspPadrao;
	}

	public CadastraNivelDestinatarioCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException{
		String nextRetorno = next;
		try {  
			// cria os Beans do Usuario, se n�o existir
			CadastraDestinatarioBean DestinatarioBean = new CadastraDestinatarioBean();
			SistemaBean SistId = (SistemaBean) req.getAttribute("SistId");
			if (SistId == null)	SistId = new SistemaBean();
			
			CadastraNivelDestinatarioBean CadastraNivelDestinatarioBean = (CadastraNivelDestinatarioBean)req.getAttribute("CadastraNivelDestinatarioBean");
			if (CadastraNivelDestinatarioBean == null) CadastraNivelDestinatarioBean = new CadastraNivelDestinatarioBean();
			
			NivelProcessoSistemaBean myNivel = (NivelProcessoSistemaBean)req.getAttribute("myNivel");
			if (myNivel == null) myNivel = new NivelProcessoSistemaBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";

			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "";

			String codProcesso = req.getParameter("codProcesso");
			if (codProcesso == null) codProcesso = "";
			
			String codNivelProcesso = req.getParameter("codNivelProcesso");
			if (codNivelProcesso == null) codNivelProcesso = "";

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)
				atualizarDependente = "N";

            // Carrega bean
			SistId.Le_Sistema(codSistema, 0);
			SistId.Le_Processo(codProcesso, 0);
			SistId.setCodProcesso(codProcesso);
			SistId.setAtualizarDependente(atualizarDependente);	
			myNivel.setCodNivelProcesso(codNivelProcesso);
			myNivel.Le_Niveis(codNivelProcesso,0);
			CadastraNivelDestinatarioBean.setCodNivelProcesso(codNivelProcesso);


			//Verifica que acao devera ser utilizada
            if(acao.compareTo("buscaDestinatario")==0) 
            {
		    	SistId.getNivelProcessos(20, 6);
				SistId.setAtualizarDependente("S");
				CadastraNivelDestinatarioBean.Le_NivelDestinatario();
				List destinatarios = DestinatarioBean.getDestinatarios(DestinatarioBean,"");
				req.setAttribute("destinatarios",destinatarios);
		    }
			else if (acao.compareTo("A") == 0) 
			{
				String[] destinatariosPorNivel = req.getParameterValues("destinatariosPorNivel");
				if (destinatariosPorNivel == null) destinatariosPorNivel = new String[0];
				
				Vector nivelDestinatario = new Vector();
				for (int i = 0; i < destinatariosPorNivel.length; i++) {
				    CadastraNivelDestinatarioBean myNivelDestinatario = CadastraNivelDestinatarioBean = new CadastraNivelDestinatarioBean();
				    myNivelDestinatario.setCodNivelProcesso(codNivelProcesso);
				    myNivelDestinatario.setCodDestinatario(destinatariosPorNivel[i]);
				    nivelDestinatario.addElement(myNivelDestinatario);
				}
				CadastraNivelDestinatarioBean.setNivelDestinatario(nivelDestinatario);
				CadastraNivelDestinatarioBean.isInsertNivelDestinatario();
				CadastraNivelDestinatarioBean.Le_NivelDestinatario();
				List destinatarios = DestinatarioBean.getDestinatarios(DestinatarioBean,"");
				req.setAttribute("destinatarios",destinatarios);
			}
			else if (acao.equals("I"))
			{
				CadastraNivelDestinatarioBean.Le_NivelDestinatario();
				nextRetorno = "/GER/CadastraNivelDestinatarioImp.jsp";
			}
			else if (acao.equals("voltar")) SistId.setAtualizarDependente("N");
		
			req.setAttribute("SistId", SistId);
			req.setAttribute("myNivel", myNivel);
			req.setAttribute("CadastraNivelDestinatarioBean", CadastraNivelDestinatarioBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("CadastraNivelDestinatarioCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	

}