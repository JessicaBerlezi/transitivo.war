package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA CONDUTOR <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 051<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_034 extends AuditoriaBean {

	private String tipoCNH			;
	private String numCNH  	        ;
	private String ufCNH 	;
	private String codOrgaoAtuacao	;
	private String codOrgoaLotacao	;
	private String nomUserName		;


	public AuditoriaBean_034() throws sys.BeanException {

		tipoCNH  			 = "";
		numCNH               = "";
		ufCNH  	             = "";
		codOrgaoAtuacao      = "";
		codOrgoaLotacao      = "";
		nomUserName      	 = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",46);
		 OrgaoBean Orgao = new OrgaoBean ();


		 this.tipoCNH           = reg.substring(0,1);
	     this.numCNH 			= reg.substring(1,12);
	     this.ufCNH 	        = reg.substring(12,14);
	     this.codOrgaoAtuacao 	= reg.substring(14,20);
	     this.codOrgoaLotacao 	= reg.substring(20,26);
	     this.nomUserName 		= reg.substring(26,46);
	     
	     if(!"".equals(this.codOrgoaLotacao.trim())){
	     Orgao.Le_Orgao(codOrgoaLotacao,0);
	     this.setSigOrgao(Orgao.getSigOrgao());
	     }

	}

	public String getTipoCNH() {
		return tipoCNH;
	}

	public String getNumCNH() {
		return numCNH;
	}

	public String getUfCNH() {
		return ufCNH;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public String getNomUserName() {

		return nomUserName;
	}

	public String getDscCNH() {
		String dsc = "";
        if (this.tipoCNH.equals("1")) dsc = "PJU";
        if (this.tipoCNH.equals("2")) dsc = "CNH";
     return dsc;   
	}


}