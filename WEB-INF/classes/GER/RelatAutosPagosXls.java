package GER;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelatAutosPagosXls {
	
	public void write(RelatAutosPagosBean relatAutosPagosBean, String titulo, String arquivo) throws IOException, WriteException
	{
	
		int iContador=1;
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			WritableSheet sheet = workbook.createSheet("Relat�rio de Autos Pagos", 0);
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 7/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
			
			boolean bExcesso=false;
			Iterator it = relatAutosPagosBean.getDados().iterator();
			RelatAutosPagosBean relatorioImp  = new RelatAutosPagosBean();
			if( relatAutosPagosBean.getDados().size()>0 ) { 
				while (it.hasNext()) {
					   relatorioImp = (RelatAutosPagosBean)it.next() ;
					   if(relatorioImp.getFlagControle().equals("S")){
						    //T�tulo do Relat�rio
						    seq = 1;
						    label = new Label(0,cont, "�RG�O: "+relatorioImp.getCodOrgao()+" - "+relatorioImp.getSigOrgao());
						    sheet.addCell(label);
							sheet.setColumnView(0, 5);
							cont++;
							
						    label = new Label(0,cont, "COD. AGENTE: "+relatorioImp.getCodAgente());
						    sheet.addCell(label);
							sheet.setColumnView(0, 5);
							cont++;
							
							label = new Label(0, cont, "SEQ", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(0, 5);
							
							label = new Label(1, cont, "N� AUTO", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(1, 30);
							
							label = new Label(2, cont, "DATA INFRA��O", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(2, 20);
							
							label = new Label(3, cont, "DATA VENCIMENTO", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(3, 20);
							
							label = new Label(4, cont, "DATA PAGAMENTO", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(4, 20);
							
							label = new Label(5, cont, "VALOR PAGO (R$)", formatoTituloCampos); 
							sheet.addCell(label);
							sheet.setColumnView(5, 20);
							
							label = new Label (6, cont, "VALOR ACUMULADO (R$)", formatoTituloCampos);
							sheet.addCell(label);
							sheet.setColumnView(6, 30);
							cont++;
					   }
			

                       //Linhas	do Relat�rio				   
					   label = new Label(0, cont, String.valueOf(seq)); 
					   sheet.addCell(label);
					   
					   label = new Label(1, cont, relatorioImp.getNumAuto()); 
					   sheet.addCell(label);
					   
					   label = new Label(2, cont, relatorioImp.getDatInfracao()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, cont, relatorioImp.getDatVencimento()); 
					   sheet.addCell(label);
					   
					   label = new Label(4, cont, relatorioImp.getDatPagamento()); 
					   sheet.addCell(label);
					   
					   label = new Label(5, cont, relatorioImp.getValPago()); 
					   sheet.addCell(label);
					   
					   label = new Label (6, cont, relatorioImp.getValAcumulado());
					   sheet.addCell(label);
					   cont++;
					   seq++;
					   iContador++;
					   if (iContador>4500)
					   {
						   bExcesso=true;
						   break;
					   }
				}
			}
			if (bExcesso)
			{
				label = new Label(0, cont, ""); 
				sheet.addCell(label);
				
				label = new Label(1, cont, "*** INCLU�DOS "+(iContador-1)+"/"+relatAutosPagosBean.getQtdLinhas()+" linhas. ***"); 
				sheet.addCell(label);
				
				label = new Label(2, cont, ""); 
				sheet.addCell(label);
				
				label = new Label(3, cont, ""); 
				sheet.addCell(label);
				
				label = new Label(4, cont, ""); 
				sheet.addCell(label);
				
				label = new Label(5, cont, ""); 
				sheet.addCell(label);
				
				label = new Label(6, cont, "");
				sheet.addCell(label);
			}
			
			workbook.write(); 
			workbook.close();
			
			
		}catch (Exception e) {	
			throw new IOException("GeraXlsRelatAutosPagos: " + e.getMessage());	}
	
	}

}
