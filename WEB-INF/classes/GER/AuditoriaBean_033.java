package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA REQUERIMENTO NUM DETERMINADO STATUS <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 049<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_033 extends AuditoriaBean {

	private String codOrgao  	;
	private String codStatus  	;
	private String tipoReq			;
	private String indContinuidade 	;
	private String codOrgaoAtuacao	;
	private String codOrgoaLotacao	;
	private String nomUserName		;


	public AuditoriaBean_033() throws sys.BeanException {

		codOrgao             = "";
		codStatus             = "";
		tipoReq  			 = "";
		indContinuidade  	 = "";
		codOrgaoAtuacao      = "";
		codOrgoaLotacao      = "";
		nomUserName      	 = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",69);
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 OrgaoBean OrgaoAtu = new OrgaoBean ();


		 this.codOrgao          = reg.substring(0,6);
		 this.codStatus         = reg.substring(6,9);
	     this.tipoReq 			= reg.substring(9,10);
	     this.indContinuidade 	= reg.substring(10,37);
	     this.codOrgaoAtuacao 	= reg.substring(37,43);
	     this.codOrgoaLotacao 	= reg.substring(43,49);
	     this.nomUserName 		= reg.substring(49,69);
	     
	     if(!"".equals(this.codOrgao.trim())){
	     OrgaoAtu.Le_Orgao(codOrgao,0);
	     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     
	     if(!"".equals(this.codOrgoaLotacao.trim())){
	     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
	     this.setSigOrgao(OrgaoLot.getSigOrgao());
	     }

	}

	public String getCodOrgao() {
		return codOrgao;
	}

	public String getCodStatus() {
		return codStatus;
	}
	
	public String getTipoReq() {
		return tipoReq;
	}

	public String getIndContinuidade() {
		return indContinuidade;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public String getNomUserName() {

		return nomUserName;
	}

	public String getDscReq() {
		String dsc = "";
        if (this.tipoReq.equals("1")) dsc = "DP";
        if (this.tipoReq.equals("2")) dsc = "1� INST";
        if (this.tipoReq.equals("3")) dsc = "2� INST";
        if (this.tipoReq.equals("4")) dsc = "DP - WEB";
        if (this.tipoReq.equals("9")) dsc = "TODOS";
     return dsc;   
	}


}