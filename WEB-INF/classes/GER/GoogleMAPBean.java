package GER;

import java.util.Vector;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class GoogleMAPBean extends sys.HtmlPopupBean {

	private String codMunicipio;
	private String desMunicipio;
	private String codBairro;
	private String latitude;
	private String longitude;

	private Vector pardais;
	private Vector pardaisGravado;

	private Vector municipios;

	public GoogleMAPBean() {
		codMunicipio = "";
		desMunicipio = "";
		codBairro = "";
		latitude = "";
		longitude = "";
	}

	public void setCodMunicipio(String codMunicipio) {
		this.codMunicipio = codMunicipio;
		if (codMunicipio == null)
			this.codMunicipio = "";
	}
	public String getCodMunicipio() {
		return this.codMunicipio;
	}

	public void setDesMunicipio(String desMunicipio) {
		this.desMunicipio = desMunicipio;
		if (desMunicipio == null)
			this.desMunicipio = "";
	}
	public String getDesMunicipio() {
		return this.desMunicipio;
	}

	public void setCodBairro(String codBairro) {
		this.codBairro = codBairro;
		if (codBairro == null)
			this.codBairro = "";
	}
	public String getCodBairro() {
		return this.codBairro;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
		if (latitude == null)
			this.latitude = "";
	}
	public String getLatitude() {
		return this.latitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
		if (longitude == null)
			this.longitude = "";
	}
	public String getLongitude() {
		return this.longitude;
	}
	
	public void setPardais(Vector pardais) {
		this.pardais = pardais;
	}
	
	public Vector getPardais() {
		return this.pardais;
	}
	
	public EquipamentoEletrBean getPardais(int i){
		   return (EquipamentoEletrBean)this.pardais.elementAt(i);
	}
	
	public void setMunicipios(Vector municipios) {
		this.municipios = municipios;
	}
	
	public Vector getMunicipios() {
		return this.municipios;
	}
	
	public TAB.MunicipioBean getMunicipios(int i){
		   return (TAB.MunicipioBean)this.municipios.elementAt(i);
	}
	
	
	public void setPardaisGravado(Vector pardaisGravado ){
		this.pardaisGravado = pardaisGravado;
	}
	
	
	public Vector getPardaisGravado(){
		return this.pardaisGravado;
	}
	
	public boolean getListPardais() throws sys.BeanException {
		try {
			
			Dao dao = Dao.getInstance();
			dao.getListPardais(this);
			setPardaisGravado(this.pardais) ;	
			
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
		return true;
	}


	public Vector getListMunicipios(String uf, String codMunic) throws sys.BeanException {
		   try  {
	 	
				Dao dao = Dao.getInstance();
				dao.getListMunicipios(this,uf,"");

		   }
		   catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
		   }
		  return this.municipios ;
		}
	
	
	public boolean AtualizaPardais()   {
		 boolean retorno = true;
		 Vector vErro = new Vector() ;
				   try  {
						Dao dao = Dao.getInstance();
						
				   if (dao.AtualizaPardais(this,vErro)){	
						   vErro.addElement("Pardais(l) atualizados(o)"+ " \n");
					   }
					   else{		
							vErro.addElement("Pardais(l) n�o atualizados(o)"+ " \n");     	   
					   }
				   }
		           catch (Exception e) {
				       vErro.addElement("Pardais(l) n�o atualizados(o)"+ " \n") ;
				       vErro.addElement("Erro na atualiza��o: "+ e.getMessage() +"\n");
				       retorno = false;
		           }//fim do catch
		   setMsgErro(vErro);
		   return retorno;
	  }
	
}