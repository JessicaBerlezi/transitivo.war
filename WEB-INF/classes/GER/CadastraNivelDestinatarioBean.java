package GER;

import java.util.ArrayList;
import java.util.Vector;

import sys.DaoException;




/**
* <b>Title:</b>GER - Destinatarios x Processos - Niveis<br>
* <b>Description:</b>Seleciona os destinat�rios para cada n�vel<br>
* <b>Copyright:</b>Copyright (c) 2005<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class CadastraNivelDestinatarioBean extends sys.HtmlPopupBean {

	private String codSistNivDestinatario;
	private String codNivelProcesso;	
	private String codDestinatario;
	private String txtDestinatario;
	private String nomNivelProcesso;
	private String txtEmail;	
	private String sigOrgao;
	private Vector nivelDestinatario;
	private ArrayList listNivelDestino;	

	public CadastraNivelDestinatarioBean() {
		super();
		setTabela("TSMI_PROC_SIST_NIV_DEST");
		setPopupWidth(35);
		codSistNivDestinatario = "";
		codNivelProcesso       = "";
		codDestinatario        = "";
		txtDestinatario        = "";
		nomNivelProcesso       = "";
		txtEmail               = "";
		sigOrgao               = "";
		nivelDestinatario = new Vector();		
	}

	
	public void setCodSistNivDestinatario(String codSistNivDestinatario) {
		this.codSistNivDestinatario = codSistNivDestinatario;
		if (codSistNivDestinatario == null)
			this.codSistNivDestinatario = "";
	}
	public String getCodSistNivDestinatario(){
		return this.codSistNivDestinatario;
	}

	
	public void setCodNivelProcesso(String codNivelProcesso) {
		this.codNivelProcesso = codNivelProcesso;
		if (codNivelProcesso == null)
			this.codNivelProcesso = "";
	}
	public String getCodNivelProcesso() {
		return this.codNivelProcesso;
	}
	

	public void setCodDestinatario(String codDestinatario) {
		if (codDestinatario == null) codDestinatario = "";
		else this.codDestinatario = codDestinatario;
	}
	public String getCodDestinatario() {
		return codDestinatario;
	}
	
	
	public void setTxtDestinatario(String txtDestinatario) {
		if (txtDestinatario == null) txtDestinatario = "";
		else this.txtDestinatario = txtDestinatario;
	}
	public String getTxtDestinatario() {
		return txtDestinatario;
	}
	
	
	public void setTxtEmail(String txtEmail) {
		if(txtEmail == null) txtEmail = "";
		else this.txtEmail = txtEmail;
	}
	public String getTxtEmail() {
		return txtEmail;
	}
	
	
	public void setNomNivelProcesso(String nomNivelProcesso) {
		if (nomNivelProcesso == null) nomNivelProcesso = "";
		else this.nomNivelProcesso = nomNivelProcesso;
	}
	public String getNomNivelProcesso() {
		return nomNivelProcesso;
	}
	
	
	public void setListNivelDestino(ArrayList listNivelDestino) {
		if(listNivelDestino == null) listNivelDestino = new ArrayList();
		else this.listNivelDestino = listNivelDestino;
	}
	public ArrayList getListNivelDestino() {
		return this.listNivelDestino;
	}


	public void setSigOrgao(String sigOrgao) {
		if(sigOrgao == null) sigOrgao = "";
		else this.sigOrgao = sigOrgao;
	}
	public String getSigOrgao() {
		return sigOrgao;
	}


//----------------------------------------------------------------------------------------
	public boolean isInsertNivelDestinatario() throws DaoException {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		Dao dao;
		try {
			dao = Dao.getInstance();
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
		
		if (dao.gravaNivelDestinatario(this, vErro)) {
			vErro.addElement("Destinatarios selecionados para este n�vel atualizados com sucesso. \n");
		}
		setMsgErro(vErro);
		return retorno;
	}


	public void setNivelDestinatario(Vector nivelDestinatario) {
		if(nivelDestinatario == null) nivelDestinatario = new Vector();
		else this.nivelDestinatario = nivelDestinatario;
	}
	public Vector getNivelDestinatario() {
		return this.nivelDestinatario;
	}
//----------------------------------------------------------------------------------------
	
	public void Le_NivelDestinatario() throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.Le_NivelDestinatario(this);
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}


	
	
//----------------------------------------------------------------------------------------	





	
	
	
}