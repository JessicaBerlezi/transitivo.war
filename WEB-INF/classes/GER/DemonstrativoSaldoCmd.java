package GER;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DemonstrativoSaldoCmd extends sys.Command 
{
	private static final String jspPadrao = "/GER/DemonstrativoSaldo.jsp";
	private String next;
	public DemonstrativoSaldoCmd() { next = jspPadrao; }
	public DemonstrativoSaldoCmd(String next) { this.next = next; }
	
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{		
		String nextRetorno = jspPadrao;		
		try {
			DemonstrativoSaldoBean RelDemonstrativo = new DemonstrativoSaldoBean();
			
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			if (acao.equals("")) {								
				RelDemonstrativo.geraDatasPadrao();
				req.setAttribute("RelDemonstrativo", RelDemonstrativo);
			}
			else if(acao.equals("ImprimeRelatorio")) 
			{				
				String fase = req.getParameter("fase");
				String codOrgao = req.getParameter("codOrgao");				
				String datIniRel = req.getParameter("dataIni");
				String datFimRel = req.getParameter("dataFim");
				List vErro = new ArrayList();
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				
				if (fase.equals("A")) { //Autua��o
					
					if (RelDemonstrativo.geraDemonstrativoAutuacao(Integer.valueOf(codOrgao), df.parse(datIniRel), df.parse(datFimRel), vErro)) {  
						String tituloConsulta = "DEMONSTRATIVO DE SALDO - AUTUA��O : " + datIniRel + " A " + datFimRel;
						req.setAttribute("tituloConsulta", tituloConsulta);
						nextRetorno = "/GER/DemonstrativoSaldoAut.jsp";					
					} else { 
						RelDemonstrativo.setDatInicial(df.parse(datIniRel));
						RelDemonstrativo.setDatFinal(df.parse(datFimRel));
					}
					
				} else if (fase.equals("P")) { //Penalidade
					
					if (RelDemonstrativo.geraDemonstrativoPenalidade(Integer.valueOf(codOrgao), df.parse(datIniRel), df.parse(datFimRel), vErro)) {  
						String tituloConsulta = "DEMONSTRATIVO DE SALDO - PENALIDADE : " + datIniRel + " A " + datFimRel;
						req.setAttribute("tituloConsulta", tituloConsulta);
						nextRetorno = "/GER/DemonstrativoSaldoPen.jsp";					
					} else { 
						RelDemonstrativo.setDatInicial(df.parse(datIniRel));
						RelDemonstrativo.setDatFinal(df.parse(datFimRel));
					}
					
				} else if (fase.equals("T")) { //Transitado em Julgado
					
					if (RelDemonstrativo.geraDemonstrativoTransitado(Integer.valueOf(codOrgao), df.parse(datIniRel), df.parse(datFimRel), vErro)) {  
						String tituloConsulta = "DEMONSTRATIVO DE SALDO - TRANSITADO EM JULGADO : " + datIniRel + " A " + datFimRel;
						req.setAttribute("tituloConsulta", tituloConsulta);
						nextRetorno = "/GER/DemonstrativoSaldoTran.jsp";					
					} else { 
						RelDemonstrativo.setDatInicial(df.parse(datIniRel));
						RelDemonstrativo.setDatFinal(df.parse(datFimRel));
					}					
				}								
				
				req.setAttribute("RelDemonstrativo", RelDemonstrativo);					
			}
		} catch (Exception e) {	
			throw new sys.CommandException("DemonstrativoSaldoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}