package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - RECEBER NOTIFICA��O(POSTO/SIRETRAN)- AUTUA��O<br>
* <b>Description:</b>  Bean Auditoria_046 - Informa��es das transa��es 204,324<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_047 extends AuditoriaBean {
	
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String codOrgao 		;	
	private String codStatus 		;
	private String datStatus 		;
	private String codEvento 		;
	private String codOrigemEvento  ;
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String txtComplemento1	;	
	private String txtComplemento2	;	
	
	public AuditoriaBean_047() throws sys.BeanException {
		
		numAutoInfracao      = "";
		numPlaca  			 = "";
		codOrgao  			 = "";		
		codStatus    		 = "";
		datStatus         	 = "";
		codEvento      		 = "";
		codOrigemEvento   	 = "";
		nomUserName      	 = "";
		codOrgoaLotacao      = "";
		txtComplemento1      = "";		
		txtComplemento2      = "";
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",78);		 
	    
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 
		 this.numAutoInfracao   = reg.substring(0,12) ;
	     this.numPlaca 			= reg.substring(12,19);
	     this.codOrgao 			= reg.substring(19,25);	     
	     this.codStatus 		= reg.substring(25,28);
	     this.datStatus 		= reg.substring(28,36);
	     this.codEvento 		= reg.substring(36,39);
	     this.codOrigemEvento 	= reg.substring(39,42);	     
	     this.nomUserName 		= reg.substring(42,62);
	     this.codOrgoaLotacao 	= reg.substring(62,68);
	     this.txtComplemento1 	= reg.substring(68,76);	    
	     this.txtComplemento2 	= reg.substring(76,78);	     
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}

	public String getNumPlaca() {
		return numPlaca;
	}
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodStatus() {
		return codStatus;
	}	public String getDatStatus() {
		String data = "";
		data = this.datStatus.substring(6,8)+"/"+this.datStatus.substring(4,6)+"/"+
		this.datStatus.substring(0,4);
		return data;
	}	
	public String getCodEvento() {
	
		return codEvento;
	}
	public String getCodOrigemEvento() {
		return codOrigemEvento;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getTxtComplemento1() {
		String data = "";
		data = this.txtComplemento1.substring(6,8)+"/"+this.txtComplemento1.substring(4,6)+"/"+
		this.txtComplemento1.substring(0,4);
		return data;
	}

	public String getTxtComplemento2() {
		return txtComplemento2;
	}	
}