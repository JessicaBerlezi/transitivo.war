package GER;

import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import sys.CommandException;

/**
* <b>Title:</b>        Gerencial - Autos Pagos Cmd<br>
* <b>Description:</b>  Informa os autos pagos de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class AutosPagosCmd extends sys.Command {

	private static final String jspPadrao = "/GER/AutosPagos.jsp";
	private String next;

	public AutosPagosCmd() {
		next = jspPadrao;
	}

	public AutosPagosCmd(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
		    // cria os Beans de sessao, se n�o existir
		    HttpSession session   = req.getSession() ;								
		    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
		  	// cria os Beans, se n�o existir
		    AutosPagosBean AutosPagosBeanID     = (AutosPagosBean)req.getAttribute("AutosPagosBeanID") ;
			if (AutosPagosBeanID==null)  AutosPagosBeanID = new AutosPagosBean() ;	  			

			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			if(acao.equals("EnviarMesAno")) next=processaMesAno(req,AutosPagosBeanID,UsrLogado);
			if(acao.equals("ListarMes"))    next=listarMes(req,AutosPagosBeanID,UsrLogado);
			if(acao.equals("ListarDia"))    next=listarDia(req,AutosPagosBeanID,UsrLogado);			
			req.setAttribute("AutosPagosBeanID", AutosPagosBeanID);			
		}
		catch (Exception ue) {
			throw new sys.CommandException("AutosPagosCmd "+ ue.getMessage());
		}
		return nextRetorno;
	}
	protected String processaMesAno(HttpServletRequest req,AutosPagosBean myAutosPagos,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		 	String nextRetorno=jspPadrao;		
		 	try {
	 			String mesAno   = req.getParameter("mesAno");
	 			if (mesAno==null) mesAno = "00/0000";
	 			myAutosPagos.setMesAno(mesAno);
	 			String codOrgao = req.getParameter("codOrgao");
		 		if (codOrgao==null) codOrgao = "";
	 			myAutosPagos.setCodOrgao(codOrgao);		 		
	 			Vector vErro = new Vector() ;		 			
	 			//validar mesAno
		 		if (sys.Util.ValidaData("01/"+mesAno).length()==0)
		 			 vErro.addElement("Mes/Ano invalido: "+mesAno+" \n") ;
				if (vErro.size()>0) {
					myAutosPagos.setMsgErro(vErro) ;
				}   
				else {		
					myAutosPagos.setListTotalPagoDia(UsrLogado) ;	
				    nextRetorno = "AutosPagosMensal.jsp" ;		 							
				}
	 		}
	 	  	catch (Exception e){
	 		    throw new sys.CommandException("AutosPagos - Processa MesAno: " + e.getMessage());
	 	  	}
	 	 	return nextRetorno  ;
	 	}
	protected String listarMes(HttpServletRequest req,AutosPagosBean myAutosPagos,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno=jspPadrao;		
	 	try {
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("AutosPagos - Processa Mes: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}
	protected String listarDia(HttpServletRequest req,AutosPagosBean myAutosPagos,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno=jspPadrao;		
	 	try {
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("AutosPagos - Processa Dia: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}	
}