package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;
/**
 * Relat�rio de Autos de Infra��o por �rg�o autuador - Consolidado
 * @author loliveira
 */
public class RelatAutosConsolidadoCmd  extends sys.Command {
	private static final String jspPadrao="/GER/RelatAutosConsolidado.jsp";    
	private String next;
	
	public RelatAutosConsolidadoCmd() {
		next = jspPadrao;
	}
	
	public RelatAutosConsolidadoCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
 		    if(OrgaoId == null)	OrgaoId = new ACSS.OrgaoBean() ;	 		
 		    
 		   RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean = (RelatAutosPorOrgaoBean)req.getAttribute("RelatAutosPorOrgaoBean") ;
		    if(RelatAutosPorOrgaoBean == null) RelatAutosPorOrgaoBean = new RelatAutosPorOrgaoBean() ;	 		
			
			String acao = req.getParameter("acao");  
			if(acao == null)acao = "";
			if(acao.equals(""))	acao =" "; 
			
			String statusAuto = req.getParameter("statusAuto");
			if (statusAuto == null) statusAuto = "";
			RelatAutosPorOrgaoBean.setTipoSolicitacao(statusAuto);
			
			String todosOrgaos = req.getParameter("todosOrgaos");
			if (todosOrgaos == null) todosOrgaos = "";
			RelatAutosPorOrgaoBean.setTodosOrgaos(todosOrgaos);
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			RelatAutosPorOrgaoBean.setCodOrgao(codOrgao);
 
			String mesAnoInicio = req.getParameter("mesAnoInicio");
			if (mesAnoInicio == null) mesAnoInicio = "";
            else{
                RelatAutosPorOrgaoBean.setMesInicial(Integer.parseInt(mesAnoInicio.substring(0,2)));
                if(mesAnoInicio.substring(0,2).equals("12"))
                {
                	RelatAutosPorOrgaoBean.setMesFinal(1);
                	RelatAutosPorOrgaoBean.setAnoFinal(Integer.parseInt(mesAnoInicio.substring(3,7))+1);
                }
                else 
                {
                	RelatAutosPorOrgaoBean.setMesFinal(Integer.parseInt(mesAnoInicio.substring(0,2))+1);
                	RelatAutosPorOrgaoBean.setAnoFinal(Integer.parseInt(mesAnoInicio.substring(3,7)));
                }
                RelatAutosPorOrgaoBean.setAnoInicial(Integer.parseInt(mesAnoInicio.substring(3,7)));
                
            }
		
			if (acao.equals("visualizacao"))
			{
				RelatAutosPorOrgaoBean.setCodSistema("40");
                //monta a estrutura do relatorio
				RelatAutosPorOrgaoBean.getMontaRelatorio(RelatAutosPorOrgaoBean);

                //Preenche o relatorio com os totais mensais 
				RelatAutosPorOrgaoBean.getGerarRelatConsolidado(RelatAutosPorOrgaoBean);
                
                //Preenche o total MR
                RelatAutosPorOrgaoBean.getTotalGeralMR(RelatAutosPorOrgaoBean);
                
                //Preenche o total DOL
                RelatAutosPorOrgaoBean.getTotalGeralDol(RelatAutosPorOrgaoBean);

                
                //Ordena 
                String ordem = req.getParameter("ordem");
                if(ordem == null) ordem = "";
                RelatAutosPorOrgaoBean.setClassificaPor(ordem);
                RelatAutosPorOrgaoBean.Classifica(RelatAutosPorOrgaoBean);
				
     			OrgaoId.Le_Orgao(codOrgao,0);
				if(RelatAutosPorOrgaoBean.getTodosOrgaos().length()>0)
					OrgaoId.setSigOrgao("TODOS");
				
				String tituloConsulta = "REGISTRO AUTOS CONSOLIDADO <br>" +
				                        "M�S/ANO: "+mesAnoInicio+" - �RG�O: "+ OrgaoId.getSigOrgao()+
                                        " - ORDENADO POR : "+RelatAutosPorOrgaoBean.getOrigem(ordem);
				req.setAttribute("tituloConsulta", tituloConsulta);
				next = "/GER/RelatAutosConsolidadoImp.jsp";
				
				String msg = "";
				if(RelatAutosPorOrgaoBean.getDados().size()<=0)
					msg = "N�O EXISTE REGISTROS PARA ESTE M�S/ANO.";
				req.setAttribute("msg",msg);
			}
		   
			
			req.setAttribute("RelatAutosPorOrgaoBean",RelatAutosPorOrgaoBean);
			req.setAttribute("statusAuto",statusAuto);
			req.setAttribute("OrgaoId",OrgaoId);

		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
