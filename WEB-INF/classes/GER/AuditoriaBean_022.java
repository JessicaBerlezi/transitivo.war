package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA AUTO DE INFRA��O <br>
* <b>Description:</b>  Bean Auditoria_022 - Informa��es da transa��O 041<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_022 extends AuditoriaBean {	
	
	private String numAutoInfracao  	;
	private String numPlaca				;
	private String codOrgao 			;
	private String numProcesso 		    ;	
	private String numNotificacao 	    ;
	private String numRenavam 	        ;
	private String codEletronico 	    ;
	private String codOrgoaLotacao		;
	private String nomUserName			;
	
	
	public AuditoriaBean_022() throws sys.BeanException {
		
		numAutoInfracao       = "";
		numPlaca  			  = "";
		codOrgao  			  = "";
		numProcesso			  = "";
		numNotificacao        = "";
		numRenavam    	      = "";	
		codEletronico         = "";
		codOrgoaLotacao       = "";
		nomUserName      	  = "";	
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",93);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	 
	    
		 		 
		
		 this.numAutoInfracao   = reg.substring(0,12) ;
	     this.numPlaca 			= reg.substring(12,19);	     
	     this.codOrgao 			= reg.substring(19,25);
	     this.numProcesso       = reg.substring(25,45);
	     this.numNotificacao 	= reg.substring(45,54);
	     this.numRenavam 	    = reg.substring(54,63);
	     this.codEletronico 	= reg.substring(63,67);
	     this.codOrgoaLotacao 	= reg.substring(67,73);
	     this.nomUserName 		= reg.substring(73,93);
		 
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}
	
	public String getNumNotificacao() {
		return numNotificacao;
	}		
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}
	public String getNumPlaca() {
		return numPlaca;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getNumProcesso() {
		
		return numProcesso;
	}
	public String getNumRenavam() {
		return numRenavam;
	}	
	public String getCodEletronico() {
		return codEletronico;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
}