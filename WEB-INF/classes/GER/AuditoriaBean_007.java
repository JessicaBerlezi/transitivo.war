package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - MOVIMENTO BANC�RIO<br>
* <b>Description:</b>  Bean Auditoria_006 - Informa��es da transa��o 407<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_007 extends AuditoriaBean {	
	
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String transacao 		;
	private String codOrgao 		;
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String seqRegistro  	;
	private String vago1 			;
	private String numProcesso 		;	
	private String responsavel 		;
	private String operacao 		;	
	private String vago2			;
	private String vago3			;
	
	
	public AuditoriaBean_007() throws sys.BeanException {		
		
		nomUserName      	  = "";
		codOrgoaLotacao       = "";	
		transacao      		  = "";
		codOrgao  			  = "";
		numAutoInfracao       = "";
		numPlaca  			  = "";	
		seqRegistro   	  	  = "";
		vago1    		  	  = "";
		numProcesso           = "";		
		responsavel           = "";	
		operacao           	  = "";
		vago2       		  = "";
		vago3       		  = "";		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",510);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();		 
	    
		 this.nomUserName 		= reg.substring(0,20);
		 this.codOrgoaLotacao 	= reg.substring(20,26);
		 this.transacao 		= reg.substring(26,27);
		 this.codOrgao 			= reg.substring(27,33);
		 this.numAutoInfracao   = reg.substring(33,45) ;
	     this.numPlaca 			= reg.substring(45,52);	
	     this.seqRegistro 		= reg.substring(52,58);
	     this.vago1 			= reg.substring(58,59);	     
	     this.numProcesso 		= reg.substring(59,79);	     
	     this.responsavel 		= reg.substring(79,87);	 
	     this.operacao 			= reg.substring(87,88);	
	     this.vago2 			= reg.substring(88,104);
	     this.vago3 			= reg.substring(104,510);	     
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }     
	     
	}

	public String getCodOrgao() {
		return codOrgao;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getNumPlaca() {
		return numPlaca;
	}
	public String getNumProcesso() {
		return numProcesso;
	}
	public String getResponsavel() {
		return responsavel;
	}	
	public String getOperacao() {
		return operacao;
	}
	public String getSeqRegistro() {
		return seqRegistro;
	}
	public String getTransacao() {
		return transacao;
	}
	public String getVago1() {
		return vago1;
	}
	public String getVago2() {
		return vago2;
	}
	public String getVago3() {
		return vago3;
	}	
}