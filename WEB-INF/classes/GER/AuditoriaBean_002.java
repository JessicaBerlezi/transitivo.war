package GER;

import sys.BeanException;
import sys.Util;
import ACSS.OrgaoBean;

/**
* <b>Title:</b>        M�dulo Gerencial - ATUALIZA DATA DE VENCIMENTO DO AUTO<br>
* <b>Description:</b>  Bean Auditoria_002 - Informa��es da transa��o 250<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_002 extends AuditoriaBean {

	
	
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String codOrgao 		;
	private String numProcesso 		;
	private String codStatus 		;
	private String datStatus 		;
	private String codEvento 		;
	private String codOrigemEvento  ;
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String txtComplemento1	;	

	public AuditoriaBean_002() throws sys.BeanException {
		
		numAutoInfracao      = "";
		numPlaca  			 = "";
		codOrgao  			 = "";
		numProcesso          = "";
		codStatus    		 = "";
		datStatus         	 = "";
		codEvento      		 = "";
		codOrigemEvento   	 = "";
		nomUserName      	 = "";
		codOrgoaLotacao      = "";
		txtComplemento1      = "";
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		
		OrgaoBean OrgaoAtu = new OrgaoBean ();	
		OrgaoBean OrgaoLot = new OrgaoBean ();	
	     
	     this.numAutoInfracao   = reg.substring(0,12);
	     this.numPlaca 			= reg.substring(12,19);
	     this.codOrgao 			= reg.substring(19,25);
	     this.numProcesso 		= reg.substring(25,45);
	     this.codStatus 		= reg.substring(45,48);
	     this.datStatus 		= reg.substring(48,56);
	     this.codEvento 		= reg.substring(56,59);
	     this.codOrigemEvento 	= reg.substring(59,62);	     
	     this.nomUserName 		= reg.substring(62,82);
	     this.codOrgoaLotacao 	= reg.substring(82,88);
	     this.txtComplemento1 	= reg.substring(88,96);
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}	
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}
	public String getNumPlaca() {
		return numPlaca;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getNumProcesso() {
		return numProcesso;
	}	
	public String getCodStatus() {
		return codStatus;
	}	
	public String getDatStatus() {
		String data = "";
		data = this.datStatus.substring(6,8)+"/"+this.datStatus.substring(4,6)+"/"+
		this.datStatus.substring(0,4);
		return data;
	}	
	public String getCodEvento() {
	
		return codEvento;
	}	
	public String getCodOrigemEvento() {
		return codOrigemEvento;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getTxtComplemento1() {
		String data = "";
		data = this.txtComplemento1.substring(6,8)+"/"+this.txtComplemento1.substring(4,6)+"/"+
		this.txtComplemento1.substring(0,4);
		return data;
	}	
}