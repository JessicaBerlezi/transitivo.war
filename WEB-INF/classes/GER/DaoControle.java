package GER;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import sys.BeanException;
import sys.DaoException;


/**
 * <b>Title:</b>        Gerencial - Acesso a Base de Dados<br>
 * <b>Description:</b>  Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b>     MIND Informatica <br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class DaoControle {
	
	private static final String MYABREVSIST = "GER";
	private static DaoControle instance;
	private sys.ServiceLocator serviceloc ;  
	
	public static DaoControle getInstance()
	throws DaoException {
		if (instance == null)
			instance = new DaoControle();
		return instance;
	}
//	**************************************************************************************
	private DaoControle() throws DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	/**------------------------------------------------
	 * DAO referente ao RelatRegDiarioBean
	 * @author loliviera
	 --------------------------------------------------*/
	public boolean consultaRegistroDiario(RelatRegDiarioBean regDiarioBean) throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		int qtd_mes = 0;
		int qtd_ano = 0;
		boolean ok = false;
		String condicao = "reg.dat_registrado >= to_date('"+regDiarioBean.getDataIni()+"','dd/mm/yyyy') and "+
		"reg.dat_registrado <= to_date('"+regDiarioBean.getDataFim()+"','dd/mm/yyyy') ";                
		if(!regDiarioBean.getTodosOrgaos().equals("S"))condicao += " and reg.cod_orgao = '"+regDiarioBean.getCodOrgao()+"' ";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtQTD = conn.createStatement();
			regDiarioBean.setDados(new ArrayList());
			
			sCmd = "SELECT reg.cod_orgao, reg.cod_municipio, to_char(reg.dat_registrado,'dd/mm/yyyy')as dat_reg,reg.qtd, "+
			" o.sig_orgao, muni.nom_municipio " +
			" from TGER_REG_DIARIO reg, TSMI_ORGAO o,  "+ 
			" TSMI_MUNICIPIO muni "+
			" WHERE " + condicao+" and reg.cod_orgao = o.cod_orgao and  "+
			" reg.cod_municipio = muni.cod_municipio  "+
			" order By cod_orgao ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				RelatRegDiarioBean dados = new RelatRegDiarioBean();	
				dados.setCodStatus("1");
				dados.setNomStatus("1");
				dados.setNomMunicipio(rs.getString("nom_municipio"));
				dados.setSigOrgao(rs.getString("sig_orgao")); 
				dados.setRegistroDia(dados.formataIntero(rs.getString("qtd")));
				
				sCmd =  "SELECT qtd, to_char(dat_registrado,'dd/mm/yyyy') as dat_reg from TGER_REG_DIARIO WHERE dat_registrado like '%"+regDiarioBean.getDataIni().substring(8)+"%' " +
				"and cod_orgao = '"+rs.getString("cod_orgao")+"' and cod_municipio = '"+rs.getString("cod_municipio")+"'";
				ResultSet rsQTD = stmtQTD.executeQuery(sCmd);
				while (rsQTD.next()) {
					if (regDiarioBean.getDataIni().substring(2,6).equals(rsQTD.getString("dat_reg").substring(2,6))){
						qtd_mes += Integer.parseInt(rsQTD.getString("qtd"));
						dados.setRegistroMes(dados.formataIntero(Integer.toString(qtd_mes)));
					}
					if (regDiarioBean.getDataIni().substring(8).equals(rsQTD.getString("dat_reg").substring(8))){
						qtd_ano += Integer.parseInt(rsQTD.getString("qtd"));
						dados.setRegistroAno(dados.formataIntero(Integer.toString(qtd_ano)));
					}
				}
				qtd_mes = 0;
				qtd_ano = 0;
				regDiarioBean.getDados().add(dados);
				ok = true;
			}
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	
	/**------------------------------------------------
	 * DAO referente ao RelatDemonstSaldosBean
	 * @author loliviera
	 --------------------------------------------------*/
	public boolean consultaDemonstrativo(RelatDemonstSaldosBean regDemosntSaldosBean) throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		int total_ant = 0;
		int total_ent = 0;
		int total_sai = 0;
		int total_atu = 0;
		String fase = "";
		boolean exibeTotalGeral = true;
		boolean ok = false;
		String condicao = "trunc(dat_proc) between " +
		"trunc(to_date('"+ regDemosntSaldosBean.getDataIni()+"','dd/mm/yyyy')) " +
		"and trunc(to_date('" + regDemosntSaldosBean.getDataFim()+"','dd/mm/yyyy'))"+
		" and desld.cod_orgao = '"+regDemosntSaldosBean.getCodOrgao()+"' ";		
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtTotalParcial = conn.createStatement();
			regDemosntSaldosBean.setDados(new ArrayList());
			
			sCmd = "SELECT desld.cod_orgao,o.sig_orgao, desld.tip_fase, desld.dat_proc, desld.qtd_ant_fisico," +
			" desld.qtd_ent_fisico, desld.qtd_sai_fisico, desld.qtd_atual_fisico," +
			" desld.qtd_ant_financeiro,desld.qtd_ent_financeiro, desld.qtd_sai_financeiro," +
			" desld.qtd_atual_financeiro,desld.status, sta.nom_status " +
			" from TGER_DEMONSTR_SALDO desld,TSMI_STATUS_AUTO sta, TSMI_ORGAO o" +
			" WHERE "+condicao+" and desld.status = sta.cod_status and desld.cod_orgao=o.cod_orgao " +
			" order by desld.tip_fase,desld.status ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				RelatDemonstSaldosBean dados = new RelatDemonstSaldosBean();	
				dados.setCodStatus(rs.getString("status"));
				dados.setTipoFase(rs.getString("tip_fase"));
				dados.setSigOrgao(rs.getString("sig_orgao"));
				dados.setQtdAnt(dados.formataIntero(rs.getString("qtd_ant_fisico")));
				dados.setQtdEnt(dados.formataIntero(rs.getString("qtd_ent_fisico")));
				dados.setQtdSai(dados.formataIntero(rs.getString("qtd_sai_fisico")));
				dados.setQtdAtual(dados.formataIntero(rs.getString("qtd_atual_fisico")));
				dados.setNomStatus(rs.getString("nom_status"));
				
				if (!fase.equals(rs.getString("tip_fase"))) {
					total_ant = 0;
					total_ent = 0;
					total_sai = 0;
					total_atu = 0;
				}
				
				// TotaL Parcial
				sCmd = "SELECT desld.tip_fase, count (desld.tip_fase) as nunlinha, SUM (desld.qtd_ant_fisico)as qtd_ant,SUM (desld.qtd_sai_fisico)as qtd_sai," +
				"SUM (desld.qtd_atual_fisico)as qtd_atual ,SUM (desld.qtd_ent_fisico)as qtd_ent from TGER_DEMONSTR_SALDO desld WHERE "+condicao+" and desld.tip_fase = "+ rs.getString("tip_fase")+" group by desld.tip_fase";
				ResultSet rsTotalParcial = stmtTotalParcial.executeQuery(sCmd);
				while (rsTotalParcial.next()) {
					dados.setTotalSldAntParcial(dados.formataIntero(rsTotalParcial.getString("qtd_ant")));
					dados.setTotalEntParcial(dados.formataIntero(rsTotalParcial.getString("qtd_ent")));
					dados.setTotalSaiParcial(dados.formataIntero(rsTotalParcial.getString("qtd_sai")));
					dados.setTotalSldAtuParcial(dados.formataIntero(rsTotalParcial.getString("qtd_atual")));
					dados.setNumRegistro(rsTotalParcial.getString("nunlinha"));
					fase = rsTotalParcial.getString("tip_fase");
					// Total Geral 
					total_ant += Integer.parseInt(rsTotalParcial.getString("qtd_ant"));
					total_ent += Integer.parseInt(rsTotalParcial.getString("qtd_ent"));
					total_sai += Integer.parseInt(rsTotalParcial.getString("qtd_sai"));
					total_atu += Integer.parseInt(rsTotalParcial.getString("qtd_atual"));
					dados.setTotalSldAnt(dados.formataIntero(Integer.toString(total_ant)));
					dados.setTotalEnt(dados.formataIntero(Integer.toString(total_ent)));
					dados.setTotalSaida(dados.formataIntero(Integer.toString(total_sai)));
					dados.setTotalSldAtu(dados.formataIntero(Integer.toString(total_atu)));
				}
				
				
				rsTotalParcial.close();
				regDemosntSaldosBean.getDados().add(dados);
				ok = true; 
				
			}
			rs.close();
			stmtTotalParcial.close();
			stmt.close(); 
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	public boolean consultaDemonstFinanceiro(RelatDemonstSaldosBean regDemosntSaldosBean) throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		double total_ant = 0;
		double total_ent = 0;
		double total_sai = 0;
		double total_atu = 0;
		String fase = "";
		boolean exibeTotalGeral = true;
		boolean ok = false;
		String condicao = "trunc(dat_proc) between " +
		"trunc(to_date('"+ regDemosntSaldosBean.getDataIni()+"','dd/mm/yyyy')) " +
		"and trunc(to_date('" + regDemosntSaldosBean.getDataFim()+"','dd/mm/yyyy'))"+
		" and desld.cod_orgao = '"+regDemosntSaldosBean.getCodOrgao()+"' ";		
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtTotalParcial = conn.createStatement();
			regDemosntSaldosBean.setDados(new ArrayList());
			
			sCmd = "SELECT desld.cod_orgao,o.sig_orgao, desld.tip_fase, desld.dat_proc, " +
			" desld.qtd_ant_financeiro,desld.qtd_ent_financeiro, desld.qtd_sai_financeiro," +
			" desld.qtd_atual_financeiro,desld.status, sta.nom_status " +
			" from TGER_DEMONSTR_SALDO desld,TSMI_STATUS_AUTO sta, TSMI_ORGAO o" +
			" WHERE "+condicao+" and desld.status = sta.cod_status and desld.cod_orgao=o.cod_orgao" +
			" order by desld.tip_fase,desld.status ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				RelatDemonstSaldosBean dados = new RelatDemonstSaldosBean();	
				dados.setCodStatus(rs.getString("status"));
				dados.setTipoFase(rs.getString("tip_fase"));
				dados.setSigOrgao(rs.getString("sig_orgao"));
				dados.setQtdAnt(dados.formataDecimal(rs.getString("qtd_ant_financeiro")));
				dados.setQtdEnt(dados.formataDecimal(rs.getString("qtd_ent_financeiro")));
				dados.setQtdSai(dados.formataDecimal(rs.getString("qtd_sai_financeiro")));
				dados.setQtdAtual(dados.formataDecimal(rs.getString("qtd_atual_financeiro")));
				dados.setNomStatus(rs.getString("nom_status"));
				
				if (!fase.equals(rs.getString("tip_fase"))) {
					total_ant = 0;
					total_ent = 0;
					total_sai = 0;   
					total_atu = 0;
				}
				
				// TotaL Parcial
				sCmd = "SELECT desld.tip_fase, count (desld.tip_fase) as nunlinha, SUM (desld.qtd_ant_financeiro)as qtd_ant,SUM (desld.qtd_sai_financeiro)as qtd_sai," +
				"SUM (desld.qtd_atual_financeiro)as qtd_atual ,SUM (desld.qtd_ent_financeiro)as qtd_ent from TGER_DEMONSTR_SALDO desld WHERE "+condicao+" and desld.tip_fase = "+ rs.getString("tip_fase")+" group by desld.tip_fase";
				ResultSet rsTotalParcial = stmtTotalParcial.executeQuery(sCmd);
				
				while (rsTotalParcial.next()) {
					
					dados.setTotalSldAntParcial(dados.formataMoeda(rsTotalParcial.getString("qtd_ant")));
					dados.setTotalEntParcial(dados.formataMoeda(rsTotalParcial.getString("qtd_ent")));
					dados.setTotalSaiParcial(dados.formataMoeda(rsTotalParcial.getString("qtd_sai")));
					dados.setTotalSldAtuParcial(dados.formataMoeda(rsTotalParcial.getString("qtd_atual")));
					dados.setNumRegistro(rsTotalParcial.getString("nunlinha"));
					fase = rsTotalParcial.getString("tip_fase");
					
					//Total Geral
					
					total_ant += rsTotalParcial.getDouble("qtd_ant");
					total_ent += rsTotalParcial.getDouble("qtd_ent");
					total_sai += rsTotalParcial.getDouble("qtd_sai");
					total_atu += rsTotalParcial.getDouble("qtd_atual");
					dados.setTotalSldAnt(dados.formataMoeda(Double.toString(total_ant)));
					dados.setTotalEnt(dados.formataMoeda(Double.toString(total_ent)));
					dados.setTotalSaida(dados.formataMoeda(Double.toString(total_sai)));
					dados.setTotalSldAtu(dados.formataMoeda(Double.toString(total_atu)));
					
				}
				
				rsTotalParcial.close();
				regDemosntSaldosBean.getDados().add(dados);
				ok = true; 
				
			}
			rs.close();
			stmtTotalParcial.close();
			stmt.close(); 
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	/**------------------------------------------------
	 * DAO referente ao RelatInfDiariasBean
	 * @author loliviera
	 --------------------------------------------------*/
	public boolean consultaInfracaoDiaria(RelatInfDiariasBean relatInfDiariasBean) throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		
		String condicao = "reg.dat_infracao >= to_date('"+relatInfDiariasBean.getDataIni()+"','dd/mm/yyyy') and "+
		"reg.dat_infracao <= to_date('"+relatInfDiariasBean.getDataFim()+"','dd/mm/yyyy') ";                
		if(!relatInfDiariasBean.getTodosOrgaos().equals("S"))condicao += " and reg.cod_orgao = '"+relatInfDiariasBean.getCodOrgao()+"' ";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			relatInfDiariasBean.setDados(new ArrayList());
			
			sCmd = "SELECT reg.cod_orgao, reg.cod_municipio,reg.status, to_char(reg.dat_infracao,'dd/mm/yyyy')as dat_inf,reg.qtd, "+
			" o.sig_orgao, muni.nom_municipio, st.nom_status " +
			" from TGER_REG_INFR_DIARIO reg, TSMI_ORGAO o, TSMI_STATUS_AUTO st, "+ 
			" TSMI_MUNICIPIO muni "+
			" WHERE " + condicao+" and reg.cod_orgao = o.cod_orgao and  "+
			" reg.cod_municipio = muni.cod_municipio and st.cod_status = reg.status "+
			" order By cod_orgao";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				RelatInfDiariasBean dados = new RelatInfDiariasBean();	
				dados.setCodStatus(rs.getString("status"));
				dados.setNomStatus(rs.getString("nom_status"));
				dados.setNomMunicipio(rs.getString("nom_municipio"));
				dados.setSigOrgao(rs.getString("sig_orgao")); 
				dados.setInfracaoDia(dados.formataIntero(rs.getString("qtd")));
				
				
				Statement stmtQTDAno = conn.createStatement();
				sCmd =  "SELECT sum (qtd) as qtd_ano from TGER_REG_INFR_DIARIO WHERE dat_infracao like '%"+relatInfDiariasBean.getDataIni().substring(8)+"%' " +
				"and cod_orgao = '"+rs.getString("cod_orgao")+"' and cod_municipio = '"+rs.getString("cod_municipio")+"'";
				ResultSet rsQTDAno = stmtQTDAno.executeQuery(sCmd);
				while (rsQTDAno.next()) {
					dados.setInfracaoAno(dados.formataIntero(rsQTDAno.getString("qtd_ano")));
				}
				stmtQTDAno.close();
				
				
				Statement stmtQTDMes = conn.createStatement();
				sCmd =  "SELECT sum (qtd) as qtd_mes from TGER_REG_INFR_DIARIO WHERE dat_infracao like '%"+relatInfDiariasBean.getDataIni().substring(2,6)+"%' " +
				"and cod_orgao = '"+rs.getString("cod_orgao")+"' and cod_municipio = '"+rs.getString("cod_municipio")+"'";
				ResultSet rsQTDMes = stmtQTDMes.executeQuery(sCmd);
				while (rsQTDMes.next()) {
					dados.setInfracaoMes(dados.formataIntero(rsQTDMes.getString("qtd_mes")));
				}
				stmtQTDMes.close();
				relatInfDiariasBean.getDados().add(dados);
				ok = true;
			}				   				   
			stmt.close();
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	/**------------------------------------------------
	 * DAO referente ao RelatAutosPagosBean
	 * @author loliviera
	 --------------------------------------------------*/		   
	
	public boolean consultaAutosPagos(RelatAutosPagosBean relatAutosPagosBean) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		BigDecimal valorAcumulado = new BigDecimal(0);
		String codAgente = "";		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs=null;
			
		//select para saber a quantidade de linhas retornadas
			
			sCmd = "SELECT count (*) as qtdLinhas FROM TGER_AUTOS_PAGOS P, TSMI_ORGAO O  WHERE " ;
			if(relatAutosPagosBean.getTodosOrgao().length()<=0)
				sCmd += "P.COD_ORGAO = "+ relatAutosPagosBean.getCodOrgao()+" AND ";
			sCmd += " P.COD_ORGAO = O.COD_ORGAO " +
			" AND P.DAT_PAGTO >= TO_DATE('01/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') AND P.DAT_PAGTO <= TO_DATE('";
			if("04,06,09,11".indexOf(relatAutosPagosBean.getDataIni().substring(0,2))>=0)
				sCmd += "30/"+relatAutosPagosBean.getDataIni()+"', 'DD/MM/YYYY') " ;
			else if("02".indexOf(relatAutosPagosBean.getDataIni().substring(0,2))>=0)
				sCmd +="28/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') ";
			else
				sCmd +="31/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') ";
			
			sCmd +=	" AND P.DAT_MES_REF = '" +relatAutosPagosBean.getDataIni()+"' "+
			" ORDER BY O.SIG_ORGAO, P.COD_AGENTE,P.DAT_PAGTO,P.NUM_AUTO_INFRACAO ";
			//Fim do Select para saber a quantidade de linhas retornadas
			rs = stmt.executeQuery(sCmd);
			if (rs.next()) relatAutosPagosBean.setQtdLinhas(rs.getInt("qtdLinhas"));
	
			sCmd = "SELECT P.NUM_AUTO_INFRACAO, TO_CHAR(P.DAT_PAGTO,'DD/MM/YYYY')AS DAT_PAGTO," +
			" P.COD_ORGAO, P.COD_AGENTE,TO_CHAR(P.DAT_VENCIMENTO,'DD/MM/YYYY')AS DAT_VENCIMENTO," +
			" TO_CHAR(P.DAT_INFRACAO,'DD/MM/YYYY')AS DAT_INFRACAO, P.VALOR_PAGO, " +
			" O.SIG_ORGAO FROM TGER_AUTOS_PAGOS P, TSMI_ORGAO O  WHERE " ;
			if(relatAutosPagosBean.getTodosOrgao().length()<=0)
				sCmd += "P.COD_ORGAO = "+ relatAutosPagosBean.getCodOrgao()+" AND ";
			
			sCmd += " P.COD_ORGAO = O.COD_ORGAO " +
			" AND P.DAT_PAGTO >= TO_DATE('01/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') AND P.DAT_PAGTO <= TO_DATE('";
			if("04,06,09,11".indexOf(relatAutosPagosBean.getDataIni().substring(0,2))>=0)
				sCmd += "30/"+relatAutosPagosBean.getDataIni()+"', 'DD/MM/YYYY') " ;
			else if("02".indexOf(relatAutosPagosBean.getDataIni().substring(0,2))>=0)
				sCmd +="28/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') ";
			else
				sCmd +="31/"+relatAutosPagosBean.getDataIni()+"','DD/MM/YYYY') ";
			
			sCmd +=	" AND P.DAT_MES_REF = '" +relatAutosPagosBean.getDataIni()+"' "+
			" ORDER BY O.SIG_ORGAO, P.COD_AGENTE,P.DAT_PAGTO,P.NUM_AUTO_INFRACAO ";
			
			rs = stmt.executeQuery(sCmd);
			int cont = 0;
			while (rs.next()&& cont < 4500) {
				RelatAutosPagosBean dados = new RelatAutosPagosBean();	
				if(!codAgente.equals(rs.getString("COD_AGENTE"))){
					valorAcumulado = new BigDecimal(0);
					codAgente = rs.getString("COD_AGENTE");
					dados.setFlagControle("S");
				}
				dados.setNumAuto(rs.getString("NUM_AUTO_INFRACAO"));
				dados.setDatInfracao(rs.getString("DAT_INFRACAO"));
				dados.setDatVencimento(rs.getString("DAT_VENCIMENTO"));
				dados.setSigOrgao(rs.getString("SIG_ORGAO"));
				dados.setCodOrgao(rs.getString("COD_ORGAO"));
				dados.setDatPagamento(rs.getString("DAT_PAGTO"));
				dados.setValPago(dados.formataDecimal(rs.getString("VALOR_PAGO")));
				dados.setCodAgente(rs.getString("COD_AGENTE"));
				valorAcumulado = valorAcumulado.add(rs.getBigDecimal("VALOR_PAGO")); 
				valorAcumulado = valorAcumulado.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				dados.setValAcumulado(dados.formataDecimal(String.valueOf(valorAcumulado)));
				relatAutosPagosBean.getDados().add(dados);
				ok = true; 
				cont++;
			}
			rs.close();
			stmt.close();
		}catch (SQLException e) {
			ok = false;
			if (e.getMessage().indexOf("ORA-00942")<0)
				throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}	
	public void getProcessosAbertos(RelatProcAbertosBean RelatProcAbertosId) throws DaoException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		ResultSet rs2 = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("24"); //M�DULO GERENCIAL	
			param.PreparaParam(conn);
			
			String localTrabalho = param.getParamSist("LOCAL_DE_TRABALHO");
			
			String sCmd = "SELECT R.COD_TIPO_SOLIC,R.NOM_USERNAME_DP," +
			" to_char(R.DAT_PROC_DP,'dd/mm/yyyy')AS DAT_PROC_DP,A.NUM_PROCESSO, O.SIG_ORGAO, R.COD_ORGAO_LOTACAO_DP " +
			" FROM TSMI_REQUERIMENTO R, TSMI_AUTO_INFRACAO A, TSMI_ORGAO O, TCAU_USUARIO U  " +
			" WHERE R.DAT_PROC_DP >= to_date('"+RelatProcAbertosId.getDataIni()+"','dd/mm/yyyy') " +
			" AND R.DAT_PROC_DP <= to_date('"+RelatProcAbertosId.getDataFim()+"','dd/mm/yyyy')"+
			" AND R.COD_TIPO_SOLIC IN ("+RelatProcAbertosId.getTpreq()+")"+
			" AND R.COD_ORGAO_LOTACAO_DP = "+RelatProcAbertosId.getCodOrgao()+
			" AND R.COD_AUTO_INFRACAO = A.COD_AUTO_INFRACAO "+
			" AND U.NOM_USERNAME = R.NOM_USERNAME_DP ";
			/*
			if(RelatProcAbertosId.getCodOrgao().equals("1"))
				sCmd +=" AND U.TXT_LOCAL_TRABALHO = '"+ localTrabalho +"'" ;
			*/
			if (RelatProcAbertosId.getProcWEB().equals("S"))
				sCmd +=" AND R.NOM_USERNAME_DP = 'SMITWEB'" ;
			
			sCmd +=" AND O.COD_ORGAO = R.COD_ORGAO_LOTACAO_DP" +
			" ORDER BY R.DAT_PROC_DP,R.COD_ORGAO_LOTACAO_DP,A.NUM_PROCESSO";
			
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatProcAbertosBean RelatProcAbertosBean = new RelatProcAbertosBean();
				RelatProcAbertosBean.setDatProcDP(rs.getString("DAT_PROC_DP"));
				if (RelatProcAbertosBean.getDatProcDP().equals(""))
					RelatProcAbertosBean.setDatProcDP("-");
				
				RelatProcAbertosBean.setCodOrgao(rs.getString("COD_ORGAO_LOTACAO_DP"));
				if (RelatProcAbertosBean.getCodOrgao().equals(""))
					RelatProcAbertosBean.setCodOrgao("-");
				
				RelatProcAbertosBean.setSigOrgao(rs.getString("SIG_ORGAO"));
				if (RelatProcAbertosBean.getSigOrgao().equals(""))
					RelatProcAbertosBean.setSigOrgao("-");
				
				RelatProcAbertosBean.setUserName(rs.getString("NOM_USERNAME_DP"));
				if (RelatProcAbertosBean.getUserName().equals(""))
					RelatProcAbertosBean.setUserName("-");
				
				RelatProcAbertosBean.setNumProcesso(rs.getString("NUM_PROCESSO"));
				if (RelatProcAbertosBean.getNumProcesso().equals(""))
					RelatProcAbertosBean.setNumProcesso("-");
				
				RelatProcAbertosBean.setTpreq(rs.getString("COD_TIPO_SOLIC"));
				if (RelatProcAbertosBean.getTpreq().equals(""))
					RelatProcAbertosBean.setTpreq("-");  
				else if(RelatProcAbertosBean.getTpreq().equals("TR"))
					RelatProcAbertosBean.setTpreq("TRI");
				
				dados.add(RelatProcAbertosBean) ;
			}  
			RelatProcAbertosId.setDados(dados);
			
			rs.close();
			stmt.close();
			if (rs2!=null) rs2.close();
			stmt2.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public void getAutosCancel(RelatMotivoCancelBean RelatBeanId) throws DaoException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT A.DSC_AGENTE, A.NUM_PROCESSO, A.NUM_AUTO_INFRACAO, O.SIG_ORGAO, "+
			" to_char(A.DAT_INFRACAO,'dd/mm/yyyy')DAT_INFRACAO, A.DSC_ENQUADRAMENTO, R.COD_JUNTA_JU, A.COD_ORGAO, "+
			" R.COD_RELATOR_JU,R.COD_MOTIVO_DEFERIDO, J.SIG_JUNTA, REL.NOM_RELATOR "+
			" FROM TSMI_AUTO_INFRACAO A, TSMI_REQUERIMENTO R, TSMI_RELATOR REL, "+
			" TSMI_JUNTA J, TSMI_ORGAO O "+
			" WHERE A.COD_AUTO_INFRACAO = R.COD_AUTO_INFRACAO "+
			" AND R.COD_JUNTA_JU = J.COD_JUNTA "+
			" AND A.COD_ORGAO = O.COD_ORGAO "+
			" AND R.COD_RELATOR_JU = REL.NUM_CPF "+
			" AND R.COD_JUNTA_JU = REL.COD_JUNTA " +
			" AND R.COD_RESULT_RS = 'D' "+
			" AND A.COD_ORGAO = '"+RelatBeanId.getCodOrgao()+"' ";
			if (!RelatBeanId.getCodMotivoDef().equals("TODOS")) 
				sCmd += " AND R.COD_MOTIVO_DEFERIDO = '"+RelatBeanId.getCodMotivoDef()+"'";
			else
				sCmd += " AND R.COD_MOTIVO_DEFERIDO IN ("+RelatMotivoCancelBean.COD_MOTIVO+")";
			sCmd += " AND R.COD_TIPO_SOLIC IN ("+RelatBeanId.getTpreq()+") " +
			" AND R.DAT_RS BETWEEN to_date('"+ RelatBeanId.getDataIni()+"','dd/mm/yyyy') "+
			" AND to_date('"+ RelatBeanId.getDataFim()+"','dd/mm/yyyy') " ;
			if (RelatBeanId.getMatAgente().length()>0)
				sCmd += " AND A.DSC_AGENTE = '"+ RelatBeanId.getMatAgente()+"'";
			if (RelatBeanId.getCodInfracao().length()>0)
				sCmd += " AND A.COD_INFRACAO = '"+ RelatBeanId.getCodInfracao()+"' ";
			
			sCmd += " ORDER BY A.DAT_INFRACAO,R.COD_MOTIVO_DEFERIDO" ;
			
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatMotivoCancelBean relatBeanResult = new RelatMotivoCancelBean(); 
				relatBeanResult.setNumProcesso(rs.getString("NUM_PROCESSO"));
				relatBeanResult.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				relatBeanResult.setDatInfracao(rs.getString("DAT_INFRACAO"));
				relatBeanResult.setTxtArtigo(rs.getString("DSC_ENQUADRAMENTO"));
				relatBeanResult.setMatAgente(rs.getString("DSC_AGENTE"));
				relatBeanResult.setSigJunta(rs.getString("SIG_JUNTA"));
				relatBeanResult.setNomRelator(rs.getString("NOM_RELATOR"));
				relatBeanResult.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));
				relatBeanResult.setCodOrgao(rs.getString("COD_ORGAO"));
				relatBeanResult.setSigOrgao(rs.getString("SIG_ORGAO"));
				dados.add(relatBeanResult) ;
			}  
			RelatBeanId.setDados(dados);
			
			rs.close();
			stmt.close();
			
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public void getTotalAutosCancel(RelatMotivoCancelBean RelatBeanId) throws DaoException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COUNT(*) TOTAL, R.COD_MOTIVO_DEFERIDO "+
			" FROM TSMI_AUTO_INFRACAO A, TSMI_REQUERIMENTO R, TSMI_RELATOR REL, "+
			" TSMI_JUNTA J, TSMI_ORGAO O "+
			" WHERE A.COD_AUTO_INFRACAO = R.COD_AUTO_INFRACAO "+
			" AND R.COD_JUNTA_JU = J.COD_JUNTA "+
			" AND A.COD_ORGAO = O.COD_ORGAO "+
			" AND R.COD_JUNTA_JU = REL.COD_JUNTA " +
			" AND R.COD_RELATOR_JU = REL.NUM_CPF "+
			" AND R.COD_RESULT_RS = 'D' "+
			" AND A.COD_ORGAO = '"+RelatBeanId.getCodOrgao()+"' ";
			if (!RelatBeanId.getCodMotivoDef().equals("TODOS")) 
				sCmd += " AND R.COD_MOTIVO_DEFERIDO = '"+RelatBeanId.getCodMotivoDef()+"'";
			else
				sCmd += " AND R.COD_MOTIVO_DEFERIDO IN ("+RelatMotivoCancelBean.COD_MOTIVO+")";
			sCmd += " AND R.COD_TIPO_SOLIC IN ("+RelatBeanId.getTpreq()+") " +
			" AND R.DAT_RS BETWEEN to_date('"+ RelatBeanId.getDataIni()+"','dd/mm/yyyy') "+
			" AND to_date('"+ RelatBeanId.getDataFim()+"','dd/mm/yyyy') " ;
			if (RelatBeanId.getMatAgente().length()>0)
				sCmd += " AND A.DSC_AGENTE = '"+ RelatBeanId.getMatAgente()+"'";
			if (RelatBeanId.getCodInfracao().length()>0)
				sCmd += " AND A.COD_INFRACAO = '"+ RelatBeanId.getCodInfracao()+"' ";
			
			sCmd += " GROUP BY R.COD_MOTIVO_DEFERIDO ORDER BY R.COD_MOTIVO_DEFERIDO" ;
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatMotivoCancelBean relatBeanResult = new RelatMotivoCancelBean(); 
				relatBeanResult.setTotMorivoDef(rs.getString("TOTAL"));
				relatBeanResult.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));
				dados.add(relatBeanResult) ;
			}  
			RelatBeanId.setTotal(dados);
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public void getQtdInfracoes(RelatInfTipoVeiculoBean relatInfTipoVeiculoBean,ACSS.UsuarioBean UsrLogado) throws  BeanException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		String datInfracao = "";
		RelatInfTipoVeiculoBean bean = new RelatInfTipoVeiculoBean();
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT SUM(QTD_INFRACAO) AS QTD_INFRACAO,TO_CHAR(DAT_INFRACAO,'DD/MM/YYYY')AS DAT_INFRACAO ,COD_TIPO "+
						" FROM MV_TIPO_VEICULO "+
						" WHERE DAT_INFRACAO BETWEEN to_date('"+relatInfTipoVeiculoBean.getDatInicial()+"','dd/mm/yyyy') " +
						" AND '"+relatInfTipoVeiculoBean.getDatFinal()+"'"+
						" AND COD_ORGAO = "+relatInfTipoVeiculoBean.getCodOrgao()+
						" AND COD_ORGAO_LOTACAO = "+UsrLogado.getOrgao().getCodOrgao()+
						" GROUP BY DAT_INFRACAO, COD_TIPO ";
			
   		    ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				if(!datInfracao.equals(rs.getString("DAT_INFRACAO"))) 
				{
					if(datInfracao.length()>0)
					{
						dados.add(bean);
						bean = new RelatInfTipoVeiculoBean();
					}
					datInfracao = rs.getString("DAT_INFRACAO");
					bean.setDatInfracao(datInfracao);
				}
				bean.getQtdinf().put(rs.getString("COD_TIPO"),rs.getInt("QTD_INFRACAO"));
			}
			
			if(dados.size()> 0)
				dados.add(bean);
			
			relatInfTipoVeiculoBean.setDados(dados);
			rs.close();
			stmt.close();

		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
}
