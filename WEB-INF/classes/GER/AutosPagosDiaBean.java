package GER;

/**
* <b>Title:</b>        Gerencial - Autos Pagos Bean<br>
* <b>Description:</b>  Informa os autos pagos de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
*/

public class AutosPagosDiaBean extends sys.HtmlPopupBean {

	private String dia;
	private String qtdPagos;
	private String totalPago;

	public AutosPagosDiaBean() throws sys.BeanException {
		dia       = "01";
		qtdPagos  = "0";
		totalPago = "0,00";
	}

	public String getDia() {
		return dia;
	}

	public void setDia(String dia) {
		if (dia==null) dia="01";
		this.dia = dia;
	}

	public String getQtdPagos() {
		return qtdPagos;
	}

	public void setQtdPagos(String qtdPagos) {
		if (qtdPagos==null) qtdPagos="0";
		this.qtdPagos = qtdPagos;
	}

	public String getTotalPago() {
		return totalPago;
	}

	public void setTotalPago(String totalPago) {
		if (totalPago==null) totalPago="0,00";
		this.totalPago = totalPago;
	}


}