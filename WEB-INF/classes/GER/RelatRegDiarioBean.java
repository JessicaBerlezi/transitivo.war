package GER;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import sys.BeanException;
import sys.DaoException;

/**
* <b>Title:</b>        Gerencial - Registro Diario Bean<br>
* <b>Description:</b>  Informa as infra��es di�rias de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class RelatRegDiarioBean extends sys.HtmlPopupBean {

	
	private List dados;
	private String codOrgao;
	private String todosOrgaos;	 
	private String sigOrgao;
	private String dataIni;
	private String dataFim;	
	private String nomMunicipio; 
	private String codStatus;
	private String nomStatus;
	private String registroDia;
	private String registroMes;    
	private String registroAno;  


	public RelatRegDiarioBean() throws sys.BeanException {
		dados = new ArrayList();
		codOrgao = "";
		todosOrgaos = "N";
		sigOrgao = "";
		dataIni = "";
		dataFim = "";
		codStatus = "";
		nomMunicipio = "";
		registroDia = "";
		registroMes = "";
		registroAno = "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setTodosOrgaos(String todosOrgaos){
		if (todosOrgaos.equals("null"))  todosOrgaos="N";	
		else	
		this.todosOrgaos = todosOrgaos;
	}
		
	public String getTodosOrgaos(){
		return this.todosOrgaos;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}


	
	public void setNomMunicipio(String nomMunicipio) {
		this.nomMunicipio = nomMunicipio;
		if (nomMunicipio == null)
			this.nomMunicipio = "";
	}
	public String getNomMunicipio() {
		return this.nomMunicipio;
	}
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}
	public String getNomStatus() {
		return this.nomStatus;
	}
	
	public void setRegistroDia(String registroDia) {
		this.registroDia = registroDia;
		if (registroDia == null)
			this.registroDia = "";
	}
	public String getRegistroDia() {
		return this.registroDia;
	}
	
	public void setRegistroMes(String registroMes) {
		this.registroMes = registroMes;
		if (registroMes == null)
			this.registroMes = "";
	}
	public String getRegistroMes() {
		return this.registroMes;
	}
	
	public void setRegistroAno(String registroAno) {
		this.registroAno = registroAno;
		if (registroAno == null)
			this.registroAno = "";
	}
	public String getRegistroAno() {
		return this.registroAno;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaRegDiario (RelatRegDiarioBean relatRegDiarioBean) throws  BeanException, DaoException{
			boolean existe = false;
			DaoControle dao = DaoControle.getInstance();
			try {
				if(dao.consultaRegistroDiario(relatRegDiarioBean)== true)
				  existe = true;
		  
			} 
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			} 
			return existe;	
		}


	public String formataIntero (String valor) throws  DaoException, BeanException{	
	  DecimalFormat formato = new DecimalFormat(",#00") ;
	  String valorFormatado = formato.format(Integer.parseInt(valor)); 
	  return valorFormatado;
	}



}