package GER;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemonstrativoSaldoBean extends sys.HtmlPopupBean {
	
	public final int[] STATUS_AUTUACAO   = {0, 1, 2, 4, 5};
	public final int[] STATUS_PENALIDADE = {10, 11, 12, 14, 15, 16, 30, 35};
	public final int[] STATUS_TRANSITADO = {90, 91, 95, 96, 97, 98, 99};	
	public final int STATUS_REGISTRADO = 0;
	
	private Date 	datInicial;
	private Date 	datFinal;	 
	private String	msgErro;
	
	private Map 	saldoAnterior;
	private Integer registrado;
	private Map 	saldoAtual;
	private Map 	total;
	
	private Map entradaAutuacao;
	private Map saidaAutuacao;
	
	private Map entradaPenalidade;
	private Map saidaPenalidade;
	
	private Map entradaTransitado;
	private Map saidaTransitado;
	
	private Map entradaStatus;
	private Map saidaStatus;		
	
	private Map 	verifTotal; //Identifica qual status tem a soma da movimenta��o diferente do saldo atual
	private Integer	verifSaldo; //Identifica se a soma do saldo atual � diferente da soma das linhas de totais
	private boolean indVerifStatus; //Identifica se a quantidade de entrada e sa�da dentro do status s�o diferentes
	
	public DemonstrativoSaldoBean() throws sys.BeanException {
		
		datInicial         	= null;
		datFinal           	= null;		
		msgErro          	= "";
		
		saldoAnterior 		= new HashMap();
		registrado 			= new Integer(0);
		saldoAtual 			= new HashMap();
		total 				= new HashMap();
		
		entradaAutuacao 	= new HashMap();
		saidaAutuacao	 	= new HashMap();
		
		entradaPenalidade 	= new HashMap();
		saidaPenalidade 	= new HashMap();
		
		entradaTransitado 	= new HashMap();
		saidaTransitado 	= new HashMap();
		
		entradaStatus 		= new HashMap();
		saidaStatus 		= new HashMap();				
		
		verifTotal			= new HashMap();
		verifSaldo			= null;		
		indVerifStatus		= true;
	}
//	--------------------------------------------------------------------------
	public void setDatInicial(Date d) {
		this.datInicial = d;
	}
	public Date getDatInicial() { return this.datInicial; }
	public String getDatInicialTxt() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df.format(this.datInicial); 
	}
//	--------------------------------------------------------------------------
	public void setDatFinal(Date d) {
		this.datFinal = d;
	}
	public Date getDatFinal() { return this.datFinal; }
	public String getDatFinalTxt() {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		return df.format(this.datFinal); 
	}
//	--------------------------------------------------------------------------
	//Gera a data incial e final padr�o
	public void geraDatasPadrao() {						
		try {
			Calendar calend = new GregorianCalendar();
			calend.setTime( new Date() );
			calend.add(Calendar.MONTH,0);
			Date date = calend.getTime();
			calend.setTime(date);
			String dia = "01";
			String mes = calend.get(Calendar.MONTH)+ 1 + "";
			if(mes.length() < 2) mes = "0" + mes;  
			String ano = calend.get(Calendar.YEAR) + "";
			String datIniRel = dia +"/"+ mes +"/"+ ano;
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 			
			this.datInicial = df.parse(datIniRel);
			this.datFinal = new Date();
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
//	--------------------------------------------------------------------------	
	
	public void setMsgErro( List vErro ) {
		for (int j=0; j<vErro.size(); j++)  this.msgErro += vErro.get(j) ;
	}
	public void setMsgErro( String msgErro ) {
		this.msgErro = msgErro;
	}
	public String getMsgErro() {	
		return this.msgErro; 
	}
//	--------------------------------------------------------------------------
	public Map getEntradaAutuacao() {
		return entradaAutuacao;
	}
	public void setEntradaAutuacao(Map entradaAutuacao) {
		this.entradaAutuacao = entradaAutuacao;
	}
//	--------------------------------------------------------------------------
	public Map getEntradaPenalidade() {
		return entradaPenalidade;
	}
	public void setEntradaPenalidade(Map entradaPenalidade) {
		this.entradaPenalidade = entradaPenalidade;
	}
//	--------------------------------------------------------------------------	
	public Map getEntradaStatus() {
		return entradaStatus;
	}
	public void setEntradaStatus(Map entradaStatus) {
		this.entradaStatus = entradaStatus;
	}
//	--------------------------------------------------------------------------	
	public Map getEntradaTransitado() {
		return entradaTransitado;
	}
	public void setEntradaTransitado(Map entradaTransitado) {
		this.entradaTransitado = entradaTransitado;
	}
//	--------------------------------------------------------------------------	
	public Integer getRegistrado() {
		return registrado;
	}
	public void setRegistrado(Integer registrado) {
		this.registrado = registrado;
	}
//	--------------------------------------------------------------------------	
	public Map getSaidaAutuacao() {
		return saidaAutuacao;
	}
	public void setSaidaAutuacao(Map saidaAutuacao) {
		this.saidaAutuacao = saidaAutuacao;
	}	
//	--------------------------------------------------------------------------	
	public Map getSaidaPenalidade() {
		return saidaPenalidade;
	}
	public void setSaidaPenalidade(Map saidaPenalidade) {
		this.saidaPenalidade = saidaPenalidade;
	}
//	--------------------------------------------------------------------------	
	public Map getSaidaStatus() {
		return saidaStatus;
	}
	public void setSaidaStatus(Map saidaStatus) {
		this.saidaStatus = saidaStatus;
	}
//	--------------------------------------------------------------------------	
	public Map getSaidaTransitado() {
		return saidaTransitado;
	}
	public void setSaidaTransitado(Map saidaTransitado) {
		this.saidaTransitado = saidaTransitado;
	}
//	--------------------------------------------------------------------------	
	public Map getSaldoAnterior() {
		return saldoAnterior;
	}
	public void setSaldoAnterior(Map saldoAnterior) {
		this.saldoAnterior = saldoAnterior;
	}
//	--------------------------------------------------------------------------	
	public Map getSaldoAtual() {
		return saldoAtual;
	}
	public void setSaldoAtual(Map saldoAtual) {
		this.saldoAtual = saldoAtual;
	}
//	--------------------------------------------------------------------------	
	public Map getTotal() {
		return total;
	}
	public void setTotal(Map total) {
		this.total = total;
	}
	//	--------------------------------------------------------------------------	
	public Integer getVerifSaldo() {
		return verifSaldo;
	}
	public void setVerifSaldo(Integer verifSaldo) {
		this.verifSaldo = verifSaldo;
	}
	//	--------------------------------------------------------------------------	
	public Map getVerifTotal() {
		return verifTotal;
	}
	public void setVerifTotal(Map verifTotal) {
		this.verifTotal = verifTotal;
	}
	//	--------------------------------------------------------------------------
	public boolean isIndVerifStatus() {
		return indVerifStatus;
	}
	public void setIndVerifStatus(boolean indVerifStatus) {
		this.indVerifStatus = indVerifStatus;
	}	
//	--------------------------------------------------------------------------	
	public String listStatusAutuacao() {
		String retorno = "(";
		for (int i = 0; i < STATUS_AUTUACAO.length; i ++) {
			if (i == (STATUS_AUTUACAO.length - 1))
				retorno += STATUS_AUTUACAO[i] + ")";
			else
				retorno += STATUS_AUTUACAO[i] + ",";							
		}
		return retorno;
	}
	
	public String listStatusPenalidade() {
		String retorno = "(";
		for (int i = 0; i < STATUS_PENALIDADE.length; i ++) {
			if (i == (STATUS_PENALIDADE.length - 1))
				retorno += STATUS_PENALIDADE[i] + ")";
			else
				retorno += STATUS_PENALIDADE[i] + ",";							
		}
		return retorno;
	}
	
	public String listStatusTransitado() {
		String retorno = "(";
		for (int i = 0; i < STATUS_TRANSITADO.length; i ++) {
			if (i == (STATUS_TRANSITADO.length - 1))
				retorno += STATUS_TRANSITADO[i] + ")";
			else
				retorno += STATUS_TRANSITADO[i] + ",";							
		}
		return retorno;
	}
//	--------------------------------------------------------------------------	
	public boolean geraDemonstrativoAutuacao(Integer codOrgao, Date datInicial, Date datFinal, List vErro ) throws sys.BeanException
	{
		boolean retorno = true;
		try {
			Dao.getInstance().geraDemonstrativoAutuacao(this, codOrgao, datInicial, datFinal); 
		} catch (Exception e) {
			retorno = false;
			setMsgErro(e.getMessage()); 
		}
		return retorno;
	}
//	--------------------------------------------------------------------------
	public boolean geraDemonstrativoPenalidade(Integer codOrgao, Date datInicial, Date datFinal, List vErro ) throws sys.BeanException
	{
		boolean retorno = true;
		try {
			Dao.getInstance().geraDemonstrativoPenalidade(this, codOrgao, datInicial, datFinal); 
		} catch (Exception e) {
			retorno = false;
			setMsgErro(e.getMessage()); 
		}
		return retorno;
	}
//	--------------------------------------------------------------------------
	public boolean geraDemonstrativoTransitado(Integer codOrgao, Date datInicial, Date datFinal, List vErro ) throws sys.BeanException
	{
		boolean retorno = true; 
		try {
			Dao.getInstance().geraDemonstrativoTransitado(this, codOrgao, datInicial, datFinal); 
		} catch (Exception e) {
			retorno = false;
			setMsgErro(e.getMessage()); 
		} 
		return retorno;
	}
//	--------------------------------------------------------------------------
}