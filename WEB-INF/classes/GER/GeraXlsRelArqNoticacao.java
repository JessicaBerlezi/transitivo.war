package GER;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GeraXlsRelArqNoticacao extends HttpServlet {
	
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
	{
		String datIniRel ="";
		String datFimRel ="";
		String jspPadrao = "/GER/RelatArqNotificacao.jsp";
		String JSPDIR   = "/WEB-INF/jsp/";
				        
		try {
             ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
             //ParamSistemaBean paramSys = (ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId");
			 RelatArqNotificacaoBean RelArqDownlBeanId = (RelatArqNotificacaoBean) req.getSession().getAttribute("RelArqDownlBeanId");
			if( RelArqDownlBeanId == null ) RelArqDownlBeanId = new RelatArqNotificacaoBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             String contexto = req.getParameter("contexto");
             if( contexto == null ) contexto = "";
             
             String nomArquivo = req.getParameter("nomArquivo");
             if( nomArquivo == null ) nomArquivo = "";
            
             if ( acao.equals("") ) {

				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
            	
				RelArqDownlBeanId.setDatIni( datIniRel );
            	RelArqDownlBeanId.setDatFim( datFimRel );
                req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
            }
            else if(acao.equals("GeraExcel")) 
			{
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");

				if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
				{ 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
				}  
				if( RelArqDownlBeanId.ConsultaRelDiario( datIniRel, datFimRel,vErro ) )				{ 
					RelArqDownlBeanId.setDatIni( datIniRel );
					RelArqDownlBeanId.setDatFim( datFimRel );
					RelArqNotificacaoXls geraxls = new RelArqNotificacaoXls();
	            	String tituloConsulta = "RELAT�RIO -ARQUIVOS DE NOTIFICA��O : "+ datIniRel +" a "+ datFimRel;
	            	ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
	            	paramSys.setCodSistema("24"); // M�dulo GER
	            	paramSys.PreparaParam();
	            	String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
					geraxls.write(RelArqDownlBeanId, tituloConsulta, arquivo);
					
					req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
					rd.forward(req, resp);	
				}
				else {
					  RelArqDownlBeanId.setDatIni( datIniRel );
					  RelArqDownlBeanId.setDatFim( datFimRel );
					  req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
					  RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + jspPadrao);
					  rd.forward(req, resp);
				}
		
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());            
		} 
	}
  }
}
