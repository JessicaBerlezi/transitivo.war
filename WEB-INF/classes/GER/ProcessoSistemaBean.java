package GER;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Michel
* @version 1.0
* @Updates
*/

public class ProcessoSistemaBean extends sys.HtmlPopupBean {

	private String codSistema;
	private String codProcesso;

	private String nomProcesso;
	private String nomDescricao;

	private List nomProcessoSist;
	private List valProcessoSist;


	public ProcessoSistemaBean() {
		super();
		setTabela("TSMI_PROCESSO_SISTEMA");
		setPopupWidth(35);
		codSistema = "";
		codProcesso = "";
		nomProcesso = "";
		nomDescricao = "";

		nomProcessoSist = new ArrayList();
		valProcessoSist = new ArrayList();
	}

	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if (codSistema == null)
			this.codSistema = "";
	}
	public String getCodSistema() {
		return this.codSistema;
	}

	public void setCodProcesso(String codProcesso) {
		this.codProcesso = codProcesso;
		if (codProcesso == null)
			this.codProcesso = "";
	}
	public String getCodProcesso() {
		return this.codProcesso;
	}

	public void setNomProcesso(String nomProcesso) {
		this.nomProcesso = nomProcesso;
		if (nomProcesso == null)
			this.nomProcesso = "";
	}
	public String getNomProcesso() {
		return this.nomProcesso;
	}

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	//--------------------------------------------------------------------------
	public void setNomProcessoSist(List nomProcessoSist) {
		this.nomProcessoSist = nomProcessoSist;
	}

	public List getNomProcessoSist() {
		return this.nomProcessoSist;
	}
	public String getProcessoSist(String myNomProcesso) throws sys.BeanException {
		try {
			String myVal = "";
			String re = "";
			Iterator itx = getNomProcessoSist().iterator();
			int i = 0;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomProcesso)) {
					myVal = getValProcessoSist(i);
					break;
				}
				i++;
			}

			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
	//--------------------------------------------------------------------------
	public void setValProcessoSist(List valProcessoSist) {
		this.valProcessoSist = valProcessoSist;
	}

	public List getValProcessoSist() {
		return this.valProcessoSist;
	}

	public String getValProcessoSist(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValProcessoSist().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
 
}