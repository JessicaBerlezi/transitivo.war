package GER;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import sys.BeanException;
import sys.DaoException;

/**
* <b>Title:</b>        Gerencial - Infra��es Diarias Bean<br>
* <b>Description:</b>  Informa as infra��es di�rias de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class RelatInfDiariasBean extends sys.HtmlPopupBean {

	
	private List dados;
	private String codOrgao; 
	private String todosOrgaos;
	private String sigOrgao;
	private String dataIni;
	private String dataFim;
	private String nomMunicipio; 
	private String codStatus;
	private String nomStatus;
	private String infracaoDia;
	private String infracaoMes;    
	private String infracaoAno;  


	public RelatInfDiariasBean() throws sys.BeanException {
		dados = new ArrayList();
		codOrgao = "";
		todosOrgaos = "N";
		sigOrgao = "";
		dataIni = "";
		dataFim = "";
		codStatus = "";
		nomMunicipio = "";
		infracaoDia = "";
		infracaoMes = "";
		infracaoAno = "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setTodosOrgaos(String todosOrgaos){
		if (todosOrgaos.equals("null"))  todosOrgaos="N";	
		else	
		this.todosOrgaos = todosOrgaos;
	}
		
	public String getTodosOrgaos(){
		return this.todosOrgaos;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}
	
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}
	
	public void setNomMunicipio(String nomMunicipio) {
		this.nomMunicipio = nomMunicipio;
		if (nomMunicipio == null)
			this.nomMunicipio = "";
	}
	public String getNomMunicipio() {
		return this.nomMunicipio;
	}
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}
	public String getNomStatus() {
		return this.nomStatus;
	}
	
	public void setInfracaoDia(String infracaoDia) {
		this.infracaoDia = infracaoDia;
		if (infracaoDia == null)
			this.infracaoDia = "";
	}
	public String getInfracaoDia() {
		return this.infracaoDia;
	}
	
	public void setInfracaoMes(String infracaoMes) {
		this.infracaoMes = infracaoMes;
		if (infracaoMes == null)
			this.infracaoMes = "";
	}
	public String getInfracaoMes() {
		return this.infracaoMes;
	}
	
	public void setInfracaoAno(String infracaoAno) {
		this.infracaoAno = infracaoAno;
		if (infracaoAno == null)
			this.infracaoAno = "";
	}
	public String getInfracaoAno() {
		return this.infracaoAno;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaRegDiario (RelatInfDiariasBean relatInfDiariasBean) throws BeanException, DaoException{
			boolean existe = false;
			DaoControle dao = DaoControle.getInstance();
			try {
				if(dao.consultaInfracaoDiaria(relatInfDiariasBean)== true)
				  existe = true;
		  
			} 
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			} 
			return existe;	
		}
		
	public String formataIntero (String valor) throws  DaoException, BeanException{	
	  DecimalFormat formato = new DecimalFormat(",#00") ;
	  String valorFormatado = formato.format(Integer.parseInt(valor)); 
	  return valorFormatado;
	}





}