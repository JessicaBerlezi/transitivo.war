package GER;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class EquipamentoEletrBean extends sys.HtmlPopupBean {

	private String idEquipamentoEletr;
	private String codMunicipio;
	private String codBairro;
	private String latitude;
	private String localizacao;
	private String longitude;
	private String sentido;
	private String velocidade;
	private String identEquipa;

	public EquipamentoEletrBean() {
		idEquipamentoEletr = "";
		codMunicipio = "";
		codBairro = "";
		localizacao = "";
		latitude = "";
		longitude = "";
		sentido = "";
		velocidade = "";
		identEquipa = "";
	}

	public void setIdEquipamentoEletr(String idEquipamentoEletr) {
		this.idEquipamentoEletr = idEquipamentoEletr;
		if (idEquipamentoEletr == null)
			this.idEquipamentoEletr = "";
	}
	public String getIdEquipamentoEletr() {
		return this.idEquipamentoEletr;
	}

	public void setCodBairro(String codBairro) {
		this.codBairro = codBairro;
		if (codBairro == null)
			this.codBairro = "";
	}
	public String getCodBairro() {
		return this.codBairro;
	}

	public void setCodMunicipio(String codMunicipio) {
		this.codMunicipio = codMunicipio;
		if (codMunicipio == null)
			this.codMunicipio = "";
	}
	public String getCodMunicipio() {
		return this.codMunicipio;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
		if (localizacao == null)
			this.localizacao = "";
	}
	public String getLocalizacao() {
		return this.localizacao;
	}
	
	
	public void setLatitude(String latitude) {
		this.latitude = latitude;
		if (latitude == null)
			this.latitude = "";
	}
	public String getLatitude() {
		return this.latitude;
	}
	
	public void setLongitude(String longitude) {
		this.longitude = longitude;
		if (longitude == null)
			this.longitude = "";
	}
	public String getLongitude() {
		return this.longitude;
	}
	
	public void setSentido(String sentido) {
		this.sentido = sentido;
		if (sentido == null)
			this.sentido = "";
	}
	public String getSentido() {
		return this.sentido;
	}

	public void setVelocidade(String velocidade) {
		this.velocidade = velocidade;
		if (velocidade == null)
			this.velocidade = "";
	}
	public String getVelocidade() {
		return this.velocidade;
	}

	public void setIdentEquipa(String identEquipa) {
		this.identEquipa = identEquipa;
		if (identEquipa == null)
			this.identEquipa = "";
	}
	public String getIdentEquipa() {
		return this.identEquipa;
	}

}