/*
 * Created on 10/12/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package GER;

/**
 * @author jheitor
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class GrafFactory {
	
	private String funcao = "";
	
	public void setFunction(String funcao){
		this.funcao = funcao;
	}
	
	public GraficoBean getInstance(){
		
		if(funcao.equals("GER0240"))
			return new GrafInfDiariasBean();	

		if(funcao.equals("GER0271"))
			return new GrafInfPontuacaoBean(); 
				
		if(funcao.equals("GER0245"))
			return new GrafTipoInfracaoBean();
		
		if(funcao.equals("GER0230"))
			return new GrafRegDiarioBean();	
			
		if(funcao.equals("GER0250"))
			return new GrafDemonstSaldosAutoBean();	
		
		if(funcao.equals("GER0260"))
			return new GrafDemonstSaldosPDBean();
		
		if(funcao.equals("GER0265"))
			return new GrafDemonstSaldosTJBean();
		
		if(funcao.equals("GER0266"))
			return new GrafDemonstSaldosTotBean();
		
		if(funcao.equals("GER0267"))
			return new GrafCancelAjusteBean();				
		return null; 
		
	}
	

}
