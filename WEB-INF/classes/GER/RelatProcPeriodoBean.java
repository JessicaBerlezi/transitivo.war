package GER;

import java.util.ArrayList;
import java.util.Iterator;

import sys.BeanException;
import sys.DaoException;
import sys.Util;


public class RelatProcPeriodoBean extends sys.HtmlPopupBean {

	private String codOrgao; 
	private String sigOrgao;
	private String nomOrgao;
	private String dataIni;
	private String dataFim;
	private String tpReq;
	private String tipoOrdem;
	private String tipoSubOrdem;	
	private String indResultado;
	private String numProcesso;
	private String datProcDP;
	private String nomRelatorUsername;
	private String codRelator;
	private String nomRelator;
	private String nomUsername;
	private String datResult;
	private String codResult;
    private String codInfracao;
	private String dscInfracao;
    private String nomProprietario;
    private String cpfProprietario;
    private String numPlaca;
	private ArrayList ListProcessos;
    
	public RelatProcPeriodoBean() throws sys.BeanException {
		codOrgao         = ""; 
		sigOrgao         = "";
		nomOrgao         = "";
		dataIni          = sys.Util.formatedToday().substring(0,10);
		dataFim          = sys.Util.formatedToday().substring(0,10);
		tpReq            = "";
		tipoOrdem        = "";
		tipoSubOrdem     = "";		
		indResultado     = "";
		numProcesso      = "";
		datProcDP        = "";
		nomRelatorUsername = "";
		codRelator       = "";
		nomRelator       = "";
		nomUsername      = "";
		datResult        = "";
		codResult        = "";
	    codInfracao      = "";
		dscInfracao      = "";
	    nomProprietario  = "";
	    cpfProprietario  = "";
	    numPlaca         = "";
		ListProcessos    = new ArrayList();
	}

	public void buscarProcessoPorPeriodo() throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.buscarProcessoPorPeriodo(this);
		} catch (DaoException e) {
			throw new DaoException("buscarProcessoPorPeriodo:"+e.getMessage());
		}
	}

	public String getCodInfracao() {
		return codInfracao;
	}

	public void setCodInfracao(String codInfracao) {
		if (codInfracao==null) codInfracao="";
		this.codInfracao = codInfracao;
	}

	public String getCodOrgao() {
		return codOrgao;
	}

	public void setCodOrgao(String codOrgao) {
		if (codOrgao==null) codOrgao="";
		this.codOrgao = codOrgao;
	}

	public String getCodRelator() {
		return codRelator;
	}

	public void setCodRelator(String codRelator) {
		if (codRelator==null) codRelator="";
		this.codRelator = codRelator;
	}

	public String getCodResult() {
		return codResult;
	}

	public void setCodResult(String codResult) {
		if (codResult==null) codResult="";
		this.codResult = codResult;
	}

	public String getDescResult() {
		String sDesc="";
		if (codResult.equals("D"))
		   sDesc="Deferido";
		else
		   sDesc="Indeferido";
		return sDesc;
	}

		
	public String getCpfProprietario() {
		return cpfProprietario;
	}

	public void setCpfProprietario(String cpfProprietario) {
		if (cpfProprietario==null) cpfProprietario="00000000000";
		
		if (cpfProprietario.length()<=11)
		{
			cpfProprietario=Util.lPad(cpfProprietario,"0",11);
			cpfProprietario=cpfProprietario.substring(0,3)+"."+cpfProprietario.substring(3,6)+"."+cpfProprietario.substring(6,9)+"-"+cpfProprietario.substring(9,11);
		}
		else
		{
			cpfProprietario=Util.lPad(cpfProprietario,"0",14);
			cpfProprietario=cpfProprietario.substring(0,2)+"."+cpfProprietario.substring(2,5)+"."+cpfProprietario.substring(5,8)+"/"+cpfProprietario.substring(8,12)+"-"+cpfProprietario.substring(12,14);
		}
		this.cpfProprietario = cpfProprietario;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		if (dataFim==null) dataFim="";
		this.dataFim = dataFim;
	}

	public String getDataIni() {
		return dataIni;
	}

	public void setDataIni(String dataIni) {
		if (dataIni==null) dataIni="";
		this.dataIni = dataIni;
	}

	public String getDatProcDP() {
		return datProcDP;
	}

	public void setDatProcDP(String datProcDP) {
		if (datProcDP==null) datProcDP="";
		this.datProcDP = datProcDP;
	}

	public String getDatResult() {
		return datResult;
	}

	public void setDatResult(String datResult) {
		if (datResult==null) datResult="";
		this.datResult = datResult;
	}

	public String getDscInfracao() {
		return dscInfracao;
	}

	public void setDscInfracao(String dscInfracao) {
		if (dscInfracao==null) dscInfracao="";
		this.dscInfracao = dscInfracao;
	}

	public ArrayList getListProcessos() {
		return ListProcessos;
	}

	public void setListProcessos(ArrayList listProcessos) {
		if (listProcessos==null) listProcessos=new ArrayList();
		ListProcessos = listProcessos;
	}

	public String getNomProprietario() {
		return nomProprietario;
	}

	public void setNomProprietario(String nomProprietario) {
		if (nomProprietario==null) nomProprietario="";
		this.nomProprietario = nomProprietario;
	}

	public String getNomRelator() {
		return nomRelator;
	}

	public void setNomRelator(String nomRelator) {
		if (nomRelator==null) nomRelator="";
		this.nomRelator = nomRelator;
	}

	public String getNomUsername() {
		return nomUsername;
	}

	public void setNomUsername(String nomUsername) {
		if (nomUsername==null) nomUsername="";
		this.nomUsername = nomUsername;
	}

	public String getNumProcesso() {
		return numProcesso;
	}

	public void setNumProcesso(String numProcesso) {
		if (numProcesso==null) numProcesso="";
		this.numProcesso = numProcesso;
	}

	public String getSigOrgao() {
		return sigOrgao;
	}

	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao==null) sigOrgao="";
		this.sigOrgao = sigOrgao;
	}

	public String getTipoOrdem() {
		return tipoOrdem;
	}

	public void setTipoOrdem(String tipoOrdem) {
		if (tipoOrdem==null) tipoOrdem="";
		this.tipoOrdem = tipoOrdem;
	}

	public String getTipoSubOrdem() {
		return tipoSubOrdem;
	}

	public void setTipoSubOrdem(String tipoSubOrdem) {
		if (tipoSubOrdem==null) tipoSubOrdem="";
		this.tipoSubOrdem = tipoSubOrdem;
	}
	
	public String getIndResultado() {
		return indResultado;
	}

	public void setIndResultado(String indResultado) {
		if (indResultado==null) indResultado="";
		this.indResultado = indResultado;
	}
	
	
	public String getTpReq() {
		return tpReq;
	}

	public void setTpReq(String tpReq) {
		if (tpReq==null) tpReq="";
		this.tpReq = tpReq;
	}

	public String getNumPlaca() {
		return numPlaca;
	}

	public void setNumPlaca(String numPlaca) {
		if (numPlaca==null) numPlaca="";
		this.numPlaca = numPlaca;
	}

	public String getNomRelatorUsername() {
		return nomRelatorUsername;
	}

	public void setNomRelatorUsername(String nomRelatorUsername) {
		if (nomRelatorUsername==null) nomRelatorUsername="";
		this.nomRelatorUsername = nomRelatorUsername;
	}

	public String getNomOrgao() {
		return nomOrgao;
	}

	public void setNomOrgao(String nomOrgao) {
		if (nomOrgao==null) nomOrgao="";
		this.nomOrgao = nomOrgao;
	}




}