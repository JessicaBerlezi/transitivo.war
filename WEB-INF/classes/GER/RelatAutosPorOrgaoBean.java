package GER;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.DaoException;

/**
 * Relat�rio de Autos de Infra��o por �rg�o autuador
 * 
 * @author loliveira
 */
public class RelatAutosPorOrgaoBean {

	private List dados;

	private List total;

	private Vector codMes;

	private Vector qtdDOL;

	private Vector qtdMR;

	private Vector qtdVex;

	private Vector totalDol;

	private Vector totalMr;

	private String codOrgao;

	private String sigOrgao;

	private String tipoSolicitacao;

	private String todosOrgaos;

	private String codSistema;

	private String datRecebimento;

	private String nomOrgao;

	private String ordem;

	private String origem;

	private String sitAutua;
	
	private String indSmitDol;

	private String classificaPor;

	private int totalMrGeral;

	private int totalDolGeral;

	private int totalVexGeral;

	private int totalGeral;

	private int acesso;

	private int mesInicial;

	private int mesFinal;

	private int anoInicial;

	private int anoFinal;

	private String origemDol;

	private String origemDolOutros;

	private String origemMr;

	private int qtdDolOrg;

	private int qtdDolOutros;

	private int qtdMrOrg;

	private int qtdMrOutros;

	private Comparator comparador;

	public RelatAutosPorOrgaoBean() throws sys.DaoException {
		this.dados = new ArrayList();
		this.total = new ArrayList();

		this.codMes = new Vector();
		this.qtdDOL = new Vector();
		this.qtdMR = new Vector();
		this.totalDol = new Vector();
		this.totalMr = new Vector();
		this.qtdVex = new Vector();

		this.codOrgao = "";
		this.sigOrgao = "";
		this.nomOrgao = "";
		this.ordem = "";
		this.origem = "SEM EMITVEX, ";
		this.datRecebimento = "";
		this.sitAutua = "";
		this.indSmitDol = "";
		this.classificaPor = "";

		this.totalMrGeral = 0;
		this.totalDolGeral = 0;
		this.totalVexGeral = 0;
		this.totalGeral = 0;
		this.acesso = 0;
		this.anoInicial = 0;
		this.anoFinal = 0;
		this.mesInicial = 0;
		this.mesFinal = 0;

		this.origemDol = "";
		this.origemDolOutros = "";
		this.origemMr = "";
		this.qtdDolOrg = 0;
		this.qtdDolOutros = 0;
		this.qtdMrOrg = 0;
		this.qtdMrOutros = 0;

		comparador = new Comparator() {
			public int compare(Object bean1, Object bean2) {
				Integer totalGeral1 = new Integer(
						((RelatAutosPorOrgaoBean) bean1).getTotalGeral());
				Integer totalGeral2 = new Integer(
						((RelatAutosPorOrgaoBean) bean2).getTotalGeral());
				return -totalGeral1.compareTo(totalGeral2);
			}
		};
	}

	public void setCodSistema(String codSistema) {
		if (codSistema == null)
			codSistema = "";
		else
			this.codSistema = codSistema;

	}

	public String getCodSistema() {
		return codSistema;
	}

	public void setAnoInicial(int anoInicial) {
		this.anoInicial = anoInicial;
	}

	public int getAnoInicial() {
		return anoInicial;
	}

	public void setAnoFinal(int anoFinal) {
		this.anoFinal = anoFinal;
	}

	public int getAnoFinal() {
		return anoFinal;
	}

	public void setTipoSolicitacao(String tipoSolicitacao) {
		if (tipoSolicitacao == null)
			tipoSolicitacao = "";
		else
			this.tipoSolicitacao = tipoSolicitacao;
	}

	public String getTipoSolicitacao() {
		return tipoSolicitacao;
	}

	public void setTodosOrgaos(String todosOrgaos) {
		if (todosOrgaos == null)
			todosOrgaos = "";
		else
			this.todosOrgaos = todosOrgaos;
	}

	public String getTodosOrgaos() {
		return todosOrgaos;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}

	public List getDados() {
		return dados;
	}

	public void setCodMes(Vector codMes) {
		this.codMes = codMes;
	}

	public Vector getCodMes() {
		return codMes;
	}

	public void setQtdDOL(Vector qtdDOL) {
		this.qtdDOL = qtdDOL;
	}

	public Vector getQtdDOL() {
		return qtdDOL;
	}

	public void setQtdMR(Vector qtdMR) {
		this.qtdMR = qtdMR;
	}

	public Vector getQtdMR() {
		return qtdMR;
	}

	public void setCodOrgao(String codOrgao) {
		if (codOrgao == null)
			codOrgao = "";
		else
			this.codOrgao = codOrgao;
	}

	public String getCodOrgao() {
		return codOrgao;
	}

	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao == null)
			sigOrgao = "";
		else
			this.sigOrgao = sigOrgao;
	}

	public String getSigOrgao() {
		return sigOrgao;
	}

	public void setDatRecebimento(String datRecebimento) {
		if (datRecebimento == null)
			datRecebimento = "";
		else
			this.datRecebimento = datRecebimento;
	}

	public String getDatRecebimento() {
		return datRecebimento;
	}

	public void setTotalDol(Vector totalDol) {
		this.totalDol = totalDol;
	}

	public Vector getTotalDol() {
		return totalDol;
	}

	public void setTotalMr(Vector totalMr) {
		this.totalMr = totalMr;
	}

	public Vector getTotalMr() {
		return totalMr;
	}

	public void setTotal(List total) {
		this.total = total;
	}

	public List getTotal() {
		return total;
	}

	public void setTotalDolGeral(int totalDolGeral) {
		this.totalDolGeral = totalDolGeral;
	}

	public int getTotalDolGeral() {
		return totalDolGeral;
	}

	public void setTotalMrGeral(int totalMrGeral) {
		this.totalMrGeral = totalMrGeral;
	}

	public int getTotalMrGeral() {
		return totalMrGeral;
	}

	public void setTotalVexGeral(int totalVexGeral) {
		this.totalVexGeral = totalVexGeral;
	}

	public int getTotalVexGeral() {
		return totalVexGeral;
	}

	public void setNomOrgao(String nomOrgao) {
		if (nomOrgao == null)
			nomOrgao = "";
		else
			this.nomOrgao = nomOrgao;
	}

	public String getNomOrgao() {
		return nomOrgao;
	}

	public void setTotalGeral(int totalGeral) {
		this.totalGeral = totalGeral;
	}

	public int getTotalGeral() {
		return totalGeral;
	}

	public void setOrdem(String ordem) {
		this.ordem = ordem;
		if (ordem == null)
			this.ordem = "totalGeral";
	}

	public String getOrdem() {
		return this.ordem;
	}

	public String getOrigem(String ord) {
		String ordenacao = "";
		if (ord.equals("totalGeral"))
			ordenacao = "TOTAL GERAL";
		else if (ord.equals("codOrgao"))
			ordenacao = "�RG�O";
		else if (ord.equals("sigOrgao"))
			ordenacao = "SIGLA �RG�O";
		else if (ord.equals("qtdDolASC"))
			ordenacao = "QTD DOL ASC";
		else if (ord.equals("qtdDolDSC"))
			ordenacao = "QTD DOL DESC";
		else if (ord.equals("qtdMrASC"))
			ordenacao = "QTD MR ASC";
		else if (ord.equals("qtdMrDSC"))
			ordenacao = "QTD MR DESC";
		return ordenacao;
	}

	public void setQtdVex(Vector qtdVex) {
		this.qtdVex = qtdVex;
	}

	public Vector getQtdVex() {
		return qtdVex;
	}

	public void setOrigem(String origem) {
		this.origem = origem;
		if (origem == null)
			this.origem = "";
	}

	public String getOrigem() {
		return origem;
	}

	public void setAcesso(int acesso) {
		this.acesso = acesso;
	}

	public int getAcesso() {
		return acesso;
	}

	public void setSitAutua(String sitAutua) {
		if (sitAutua == null)
			this.sitAutua = "-";
		else
			this.sitAutua = sitAutua;
	}

	public String getSitAutua() {
		if(this.sitAutua.equals("N"))
		{
			sitAutua = "N�O";
		}
		else if(this.sitAutua.equals("S"))
		{
			sitAutua = "SIM";
		}
		return sitAutua;
	}

	
	public void setIndSmitDol(String indSmitDol) {
		if (indSmitDol == null)
			this.indSmitDol = "-";
		else
			this.indSmitDol = indSmitDol;
	}
	
	public String getIndSmitDol() {
		if(this.indSmitDol.equals("N"))
		{
			indSmitDol = "N�O";
		}
		else if(this.indSmitDol.equals("S"))
		{
			indSmitDol = "SIM";
		}
		return indSmitDol;
	}

	public void setMesInicial(int mesInicial) {
		this.mesInicial = mesInicial;
	}

	public int getMesInicial() {
		return mesInicial;
	}

	public void setMesFinal(int mesFinal) {
		this.mesFinal = mesFinal;
	}

	public int getMesFinal() {
		return mesFinal;
	}

	public void setClassificaPor(String classificaPor) {
		if (classificaPor == null)
			this.classificaPor = "";
		else
			this.classificaPor = classificaPor;
	}

	public String getClassificaPor() {
		return classificaPor;
	}

	public void setOrigemDol(String origemDol) {
		this.origemDol = origemDol;
	}

	public String getOrigemDol() {
		return origemDol;
	}

	public void setOrigemDolOutros(String origemDolOutros) {
		this.origemDolOutros = origemDolOutros;
	}

	public String getOrigemDolOutros() {
		return origemDolOutros;
	}

	public void setOrigemMr(String origemMr) {
		this.origemMr = origemMr;
	}

	public String getOrigemMr() {
		return origemMr;
	}
	
	public void setQtdDolOrg(int qtdDolOrg) {
		this.qtdDolOrg = qtdDolOrg;
	}

	public int getQtdDolOrg() {
		return qtdDolOrg;
	}

	public void setQtdDolOutros(int qtdDolOutros) {
		this.qtdDolOutros = qtdDolOutros;
	}

	public int getQtdDolOutros() {
		return qtdDolOutros;
	}

	public void setQtdMrOrg(int qtdMrOrg) {
		this.qtdMrOrg = qtdMrOrg;
	}

	public int getQtdMrOrg() {
		return qtdMrOrg;
	}

	public void setQtdMrOutros(int qtdMrOutros) {
		this.qtdMrOutros = qtdMrOutros;
	}

	public int getQtdMrOutros() {
		return qtdMrOutros;
	}



	public void getMontaRelatorio(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getMontaRelatorio(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getGerarRelatorio(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getGerarRelatorio(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getGerarRelatConsolidado(
			RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean) throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getGerarQtdDol(RelatAutosPorOrgaoBean);
			dao.getGerarQtdMr(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public String formataIntero(int valor) throws DaoException, BeanException {
		String valorFormatado = "";
		if (valor != 0 ) {
			DecimalFormat formato = new DecimalFormat(",#00");
			valorFormatado = formato.format(valor);
			if (valorFormatado.substring(0,1).equals("0")){
				valorFormatado = valorFormatado.substring(1);
			}
		} 
		else
			valorFormatado = "0";
		
		return valorFormatado;
	}

	public void getTotalGeral(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		try {
			int somaGeral = 0;
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();
			List total = new ArrayList();

			while (it.hasNext()) {
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				somaGeral = myRelatorio.getTotalMrGeral()
						+ myRelatorio.getTotalDolGeral();
				myRelatorio.setTotalGeral(somaGeral);
				total.add(myRelatorio);
			}
			RelatAutosPorOrgaoBean.setDados(total);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getTotalGeralDol(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		try {
			int somaGeralOut = 0;
			int somaGeralOrg = 0;
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();

			while (it.hasNext()) {
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				somaGeralOrg += myRelatorio.getQtdDolOrg();
				somaGeralOut += myRelatorio.getQtdDolOutros();
			}
			RelatAutosPorOrgaoBean.setTotalDolGeral(somaGeralOrg+somaGeralOut);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getTotalGeralMR(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		try {
			int somaGeral = 0;
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();

			while (it.hasNext()) {
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				somaGeral += myRelatorio.getQtdMrOrg();
			}
			RelatAutosPorOrgaoBean.setTotalMrGeral(somaGeral);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getTotalMReDOL(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		try {
			int somaDol = 0;
			int somaMr = 0;
			int somaVex = 0;
			List total = new ArrayList();
			String mr = "";
			String dol = "";
			String vex = "";

			// Totalizar valores
			Iterator it = RelatAutosPorOrgaoBean.getDados().iterator();
			RelatAutosPorOrgaoBean myRelatorio = new RelatAutosPorOrgaoBean();

			while (it.hasNext()) {
				myRelatorio = (RelatAutosPorOrgaoBean) it.next();
				for (int i = 0; i < 12; i++) {
					// Totaliza quantidade de MR
					mr = (String) myRelatorio.getQtdMR().get(i);
					if (mr != null) {
						somaMr += Integer.parseInt(mr);
					}

					// Totaliza quantidade de DOL
					dol = (String) myRelatorio.getQtdDOL().get(i);
					if (dol != null) {
						somaDol += Integer.parseInt(dol);
					}

					// Totaliza quantidade de VEX
					vex = (String) myRelatorio.getQtdVex().get(i);
					if (vex != null) {
						somaVex += Integer.parseInt(vex);
					}

				}
				myRelatorio.setTotalMrGeral(somaMr);
				myRelatorio.setTotalDolGeral(somaDol);
				myRelatorio.setTotalVexGeral(somaVex);
				total.add(myRelatorio);
				somaMr = 0;
				somaDol = 0;
				somaVex = 0;
			}
			RelatAutosPorOrgaoBean.setTotal(total);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getCarregaDadosVex(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getCarregaDadosVex(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getAcesso(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getAcesso(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void getOrigem(RelatAutosPorOrgaoBean RelatAutosPorOrgaoBean)
			throws DaoException {
		Dao dao;
		try {
			dao = Dao.getInstance();
			dao.getOrigem(RelatAutosPorOrgaoBean);

		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}

	public void Classifica(RelatAutosPorOrgaoBean bean)
			throws sys.BeanException {

		this.comparador = new Comparator() {
			public int compare(Object bean1, Object bean2) {
				int retorno = 0;
				if (getClassificaPor().equals("totalGeral")) 
				{
					Integer totalGeral1 = new Integer(((RelatAutosPorOrgaoBean) bean1).getTotalGeral());
					Integer totalGeral2 = new Integer(((RelatAutosPorOrgaoBean) bean2).getTotalGeral());
					retorno = -totalGeral1.compareTo(totalGeral2);
				} 
				else if (getClassificaPor().equals("codOrgao")) 
				{
					Integer codOrgao1 = new Integer(((RelatAutosPorOrgaoBean) bean1).getCodOrgao());
					Integer codOrgao2 = new Integer(((RelatAutosPorOrgaoBean) bean2).getCodOrgao());
					retorno = codOrgao1.compareTo(codOrgao2);
				} 
				else if (getClassificaPor().equals("sigOrgao")) 
				{
					String sigOrgao1 = ((RelatAutosPorOrgaoBean) bean1).getSigOrgao();
					String sigOrgao2 = ((RelatAutosPorOrgaoBean) bean2).getSigOrgao();
					retorno = sigOrgao1.compareTo(sigOrgao2);
					
				} 
				else if (getClassificaPor().substring(0, 6).equals("qtdDol")) 
				{
					Integer qtdDol1 = new Integer(((RelatAutosPorOrgaoBean) bean1).getQtdDolOrg()+((RelatAutosPorOrgaoBean) bean1).getQtdDolOutros());
					Integer qtdDol2 = new Integer(((RelatAutosPorOrgaoBean) bean2).getQtdDolOrg()+((RelatAutosPorOrgaoBean) bean2).getQtdDolOutros());

					if (getClassificaPor().substring(6, 9).equals("ASC"))
						retorno = qtdDol1.compareTo(qtdDol2);
					else
						retorno = -qtdDol1.compareTo(qtdDol2);
				} 
				else if (getClassificaPor().substring(0, 5).equals("qtdMr")) 
				{
					Integer qtdMr1 = new Integer(((RelatAutosPorOrgaoBean) bean1).getQtdMrOrg());
					Integer qtdMr2 = new Integer(((RelatAutosPorOrgaoBean) bean2).getQtdMrOrg());

					if (getClassificaPor().substring(5, 8).equals("ASC"))
						retorno = qtdMr1.compareTo(qtdMr2);
					else
						retorno = -qtdMr1.compareTo(qtdMr2);
				}
				return retorno;
			}
		};
		Collections.sort(bean.getDados(), this.comparador);
	}

}