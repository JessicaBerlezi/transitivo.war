package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GrafDistribuicaoCmd extends sys.Command {

	private static final String jspPadrao = "/GER/GrafDistribuicao.jsp";
	private String next;
	public GrafDistribuicaoCmd() { next = jspPadrao; }
	public GrafDistribuicaoCmd(String next) { this.next = next; }
	
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{	
		
		String nextRetorno = jspPadrao;
		
		try {
		
			ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
			
			String verGrafico = req.getParameter("verGrafico");
 			if (verGrafico == null) verGrafico = "N";
			
 			String acao = req.getParameter("acao");
 			if (acao == null) acao = " ";
 			
 			String orgao = req.getParameter("codOrgao");
 			if (orgao == null) orgao = " ";
 			
 			if (acao.equals("OK")) { 				
 				verGrafico = "S";		
 			}
 		
 			if(acao.equals("Retornar")){
 				verGrafico = "N"; 
 			}
 			
 			req.setAttribute("verGrafico", verGrafico);
 			req.setAttribute("cod_uf",SistemaBeanId.getCodUF());
			
		} catch (Exception e) {	throw new sys.CommandException("DistribuicaoProcessoCmd: " + e.getMessage());	}	
		return nextRetorno;
	}
	
	
}
