package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - PARCELAR AUTOS <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 078/080<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_036 extends AuditoriaBean {

	private String numRenavam			;
	private String numPlaca  	        ;
	private String numParcelas 	        ;
	private String valPrimParcela	    ;
	private String totDifRateio	        ;
	private String tipoAuto	            ;
	private String valPrimParcela1	    ;
	private String demaisParcelas	    ;
	private String datPrimVancimento	;
	private String autosParcelados	    ;
	private String nomUserName		    ;
	private String codOrgaoAtuacao	    ;


	public AuditoriaBean_036() throws sys.BeanException {

		numRenavam  			 = "";
		numPlaca                 = "";
		numParcelas  	         = "";
		valPrimParcela           = "";
		totDifRateio             = "";
		tipoAuto                 = "";
		valPrimParcela1          = "";
		demaisParcelas           = "";
		datPrimVancimento        = "";
		autosParcelados          = "";
		nomUserName      	     = "";
		codOrgaoAtuacao          = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",101);
		 OrgaoBean Orgao = new OrgaoBean ();


		 this.numRenavam          = reg.substring(0,9);
		 this.numPlaca 			  = reg.substring(9,16);
	     this.numParcelas 	      = reg.substring(16,18);
	     this.valPrimParcela 	  = reg.substring(18,27);
	     this.totDifRateio 	      = reg.substring(27,36);
	     this.tipoAuto 	          = reg.substring(36,37);
	     this.valPrimParcela1 	  = reg.substring(37,46);
	     this.demaisParcelas 	  = reg.substring(46,55);
	     this.datPrimVancimento   = reg.substring(55,63);
	     this.autosParcelados     = reg.substring(63,75);
	     this.nomUserName   	  = reg.substring(75,95);
	     this.codOrgaoAtuacao 	  = reg.substring(95,101);
	     
	     if(!"".equals(this.codOrgaoAtuacao.trim())){
	     Orgao.Le_Orgao(codOrgaoAtuacao,0);
	     this.setSigOrgaoAtuacao(Orgao.getSigOrgao());
	     }

	}

	public String getNumRenavam() {
		return numRenavam;
	}

	public String getNumPlaca() {
		return numPlaca;
	}

	public String getNumParcelas() {
		return numParcelas;
	}

	public String getValPrimParcela() {
		return valPrimParcela;
	}

	public String getTotDifRateio() {
		return totDifRateio;
	}

	public String getTipoAuto() {
		return tipoAuto;
	}
	
	public String getValPrimParcela1() {
		return valPrimParcela1;
	}

	public String getDemaisParcelas() {
		return demaisParcelas;
	}
	
	public String getDatPrimVancimento() {
		return datPrimVancimento;
	}
	
	public String getAutosParcelados() {
		return autosParcelados;
	}

	public String getNomUserName() {
		return nomUserName;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}
	

}