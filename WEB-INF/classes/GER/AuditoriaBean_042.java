package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE EVENTOS<br>
* <b>Description:</b>  Bean Auditoria_042 - Informa��es da transa��o 092<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_042 extends AuditoriaBean {	
	
	
	private String codigoEvento			;	
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String dscEvento	 		;	
	private String codOrgao				;
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_042() throws sys.BeanException {		
	
		
		codigoEvento 		= "";		
		tipoAcao 			= "";
		indContinuidade     = "";
		dscEvento 			= "";		
		codOrgao  			= "";
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",58);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();	
	    
		 		
		 this.codigoEvento		= reg.substring(0,3)     ;
	     this.tipoAcao 			= reg.substring(3,4)     ;
	     this.indContinuidade 	= reg.substring(4,6)     ;
	     this.dscEvento 		= reg.substring(6,36)    ;	     
	     this.codOrgao 			= reg.substring(36,42)   ;
	     this.codOrgoaLotacao 	= reg.substring(42,38)   ;
	     this.nomUserName 		= reg.substring(38,58)   ;	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }		     
	     
	}
	
	public String getCodigoEvento() {		
		return codigoEvento;
	}		
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	
	public String getIndContinuidade() {
		return indContinuidade;
	}
	
	public String getDscEvento() {
		return dscEvento;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}	
	}