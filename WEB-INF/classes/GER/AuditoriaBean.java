package GER;


import java.util.ArrayList;
import java.util.List;
import REC.EventoBean;
import GER.Dao;


/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Usu�rios - Tabela de Hist�rico de Usu�rios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean  extends sys.HtmlPopupBean{

	private String codTransacao;
	private String dscTransacao;
	private String codTransacaoRef;

	
	private String datInicio;
	private String datFim;
	
	private String codOrgao ;
	private String sigOrgao ;
	private String nomUserName ;
	
	private String codOrgaoAtuacao ;
	private String sigOrgaoAtuacao ;
	
	private String totalRegistros ;
	private String qtdParcialReg ;
	private String qtd_Registros_Rodada ;
	private String qtdRegs ;
	
	private String transRef ;
	private String datProcRef ;
	private String sIN ;

	private List dados;
	
	//List sele��o consulta
	private List primTrans;
	private List primDatProc;
	
	private List ultTrans;
	private List ultDatProc;
	
	private List qtdReg;

	private List eventos;
	
	public AuditoriaBean() throws sys.BeanException {
		
		dados = new ArrayList();
		primTrans = new ArrayList();
		primDatProc = new ArrayList();
 
		ultTrans = new ArrayList();
		ultDatProc = new ArrayList();
		qtdReg = new ArrayList();
		
		eventos = new ArrayList();

		codTransacao = "";
		codTransacaoRef = "";
		datInicio = "";
		datFim = "";
		
		codOrgao = "";
		sigOrgao = "";
		nomUserName = "";

	
		codOrgaoAtuacao = "";
		sigOrgaoAtuacao = "";
	
		totalRegistros = "";
		qtdParcialReg = "";
		qtd_Registros_Rodada = "";
		qtdRegs = "";

	
		transRef = "0";
		datProcRef = "0";
		sIN = "0";
	
	}

	public void setCodTransacao(String codTransacao) {
		if(codTransacao == null) codTransacao = "";
		else this.codTransacao = codTransacao;
	}
	public String getCodTransacao() {
		return codTransacao;
	}

	public void setDscTransacao(String dscTransacao) {
		if(dscTransacao == null) dscTransacao = "";
		else this.dscTransacao = dscTransacao;
	}
	public String getDscTransacao() {
		return dscTransacao;
	}

	public void setCodTransacaoRef(String codTransacaoRef) {
		if(codTransacaoRef == null) codTransacaoRef = "";
		else this.codTransacaoRef = codTransacaoRef;
	}
	public String getCodTransacaoRef() {
		return codTransacaoRef;
	}
	
	public void setDatInicio(String datInicio) {
		if(datInicio == null) datInicio = "";
		else this.datInicio = datInicio;
	}
	public String getDatInicio() {
		return datInicio;
	}
	
	public void setDatFim(String datFim) {
		if(datFim == null) datFim = "";
		else this.datFim = datFim;
	}
	public String getDatFim() {
		return datFim;
	}

	public void setCodOrgao(String codOrgao) {
		if(codOrgao == null) codOrgao = "";
		else this.codOrgao = codOrgao;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	

	public void setSigOrgao(String sigOrgao) {
		if(sigOrgao == null) sigOrgao = "";
		else this.sigOrgao = sigOrgao;
	}
	public String getSigOrgao() {
		return sigOrgao;
	}
	
	public void setNomUserName(String nomUserName) {
		if(nomUserName == null) nomUserName = "";
		else this.nomUserName = nomUserName;
	}
	public String getNomUserName() {
		return nomUserName;
	}

	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		if(codOrgaoAtuacao == null) codOrgaoAtuacao = "";
		else this.codOrgaoAtuacao = codOrgaoAtuacao;
	}
	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}


	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
		if(sigOrgaoAtuacao == null) sigOrgaoAtuacao = "";
		else this.sigOrgaoAtuacao = sigOrgaoAtuacao;
	}
	
	public String getSigOrgaoAtuacao() {
		return sigOrgaoAtuacao;
	}
	
	public void setTotalRegistros(String totalRegistros) {
		if(totalRegistros == null) totalRegistros = "";
		else this.totalRegistros = totalRegistros;
	}
	
	public String getTotalRegistros() {
		return totalRegistros;
	}
	
	public void setQtdParcialReg(String qtdParcialReg) {
		if(qtdParcialReg == null) qtdParcialReg = "";
		else this.qtdParcialReg = qtdParcialReg;
	}
	
	public String getQtdParcialReg() {
		return qtdParcialReg;
	}

	public void setQtd_Registros_Rodada(String qtd_Registros_Rodada) {
		if(qtd_Registros_Rodada == null) qtd_Registros_Rodada = "";
		else this.qtd_Registros_Rodada = qtd_Registros_Rodada;
	}
	
	public String getQtd_Registros_Rodada() {
		return qtd_Registros_Rodada;
	}

	public void setQtdRegs(String qtdRegs) {
		if(qtdRegs == null) qtdRegs = "";
		else this.qtdRegs = qtdRegs;
	}
	
	public String getQtdRegs() {
		int regIni = 0;
		
		regIni = Integer.parseInt(this.qtdParcialReg) - (Integer.parseInt(this.qtdRegs)-1);
		
		return String.valueOf(regIni);
	}

	
	public void setTransRef(String transRef) {
		if(transRef == null) transRef = "";
		else this.transRef = transRef;
	}
	
	public String getTransRef() {
		return transRef;
	}

	
	public void setDatProcRef(String datProcRef) {
		if(datProcRef == null) datProcRef = "";
		else this.datProcRef = datProcRef;
	}
	
	public String getDatProcRef() {
		return datProcRef;
	}
	
	public void setsIN(String sIN) {
		if(sIN == null) sIN = "";
		else this.sIN = sIN;
	}
	
	public String getsIN() {
		return sIN;
	}

	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}

	public List getEventos() {
		return this.eventos;
	}

	public EventoBean getEventos(int i){
		   return (EventoBean)this.eventos.get(i);
	}
	
	public List getPrimTrans() {
		return this.primTrans;
	}

	public String getPrimTrans(int i){
		   return (String)this.primTrans.get(i);
	}

	public String delPrimTrans(int i){
		   return (String)this.primTrans.remove(i);
	}
	
	public List getUltTrans() {
		return this.ultTrans;
	}

	public String getUltTrans(int i){
		   return (String)this.ultTrans.get(i);
	}
	
	public String delUltTrans(int i){
		   return (String)this.ultTrans.remove(i);
	}


	public List getPrimDatProc() {
		return this.primDatProc;
	}

	public String getPrimDatProc(int i){
		   return (String)this.primDatProc.get(i);
	}
	
	public String delPrimDatProc(int i){
		   return (String)this.primDatProc.remove(i);
	}
	
	public List getUltDatProc() {
		return this.ultDatProc;
	}

	public String getUltDatProc(int i){
		   return (String)this.ultDatProc.get(i);
	}
	
	public String delUltDatProc(int i){
		   return (String)this.ultDatProc.remove(i);
	}

	public String getDadosArray(int i){
		   return (String)this.dados.get(i);
	}
	

	public List getQtdReg() {
		return this.qtdReg;
	}

	public String getQtdReg(int i){
		   return (String)this.qtdReg.get(i);
	}
	
	public String delQtdReg(int i){
		   return (String)this.qtdReg.remove(i);
	}
	
	
//	--------------------------  Metodos da Bean ----------------------------------


	public void CarregaLista(String acao)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.CarregaLista(this, acao);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	public String BuscaTotalReg()
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		return dao.BuscaTotalReg(this);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
}
	
	public String BuscaCidade(String cod)
	throws sys.BeanException {
		String cidade = "";
	try {		
		Dao dao = Dao.getInstance();
		cidade = dao.BuscaCidade(this, cod);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
	return cidade;
}

	public void buscaEventos()
	throws sys.BeanException {
	try {
		EventoBean event = new EventoBean();
		this.eventos = event.getEventos();
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
}
// -----------------------------------   m�TODOS REFERENTES AO REGISTRO  --------------------------------------	

	public void CarregaListaRegistro(String acao)
	throws sys.BeanException {
	try {
		REG.Dao daoReg= REG.Dao.getInstance();
		daoReg.CarregaListaRegistro(this, acao);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
}

public String BuscaTotalRegRegistro()
throws sys.BeanException {
try {
	REG.Dao daoReg= REG.Dao.getInstance();
	return daoReg.BuscaTotalRegRegistro(this);
} 
catch (Exception e) {
	throw new sys.BeanException(e.getMessage());
} 
}

public void buscaEventosRegistro()
throws sys.BeanException {
try {
	EventoBean event = new EventoBean();
	this.eventos = event.getEventosRegistro();
} 
catch (Exception e) {
	throw new sys.BeanException(e.getMessage());
} 


}

	
}