package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONSULTA AUTO PARCELAR <br>
* <b>Description:</b>  Bean Auditoria_030 - Informa��es da transa��O 077/079<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Alexandere Bahia
* @version 1.0
* @Updates
*/

public class AuditoriaBean_035 extends AuditoriaBean {

	private String numPlaca			;
	private String numRenavam  	    ;
	private String indContinuidade 	;
	private String codOrgaoAtuacao	;
	private String codOrgoaLotacao	;
	private String nomUserName		;


	public AuditoriaBean_035() throws sys.BeanException {

		numPlaca  			 = "";
		numRenavam           = "";
		indContinuidade  	 = "";
		codOrgaoAtuacao      = "";
		codOrgoaLotacao      = "";
		nomUserName      	 = "";

	}

	public void setPropriedades(String reg) throws DaoException, BeanException{
		 reg = Util.rPad(reg," ",60);
		 OrgaoBean Orgao = new OrgaoBean ();


		 this.numPlaca          = reg.substring(0,7);
	     this.numRenavam 		= reg.substring(7,16);
	     this.indContinuidade 	= reg.substring(16,28);
	     this.codOrgaoAtuacao 	= reg.substring(28,34);
	     this.codOrgoaLotacao 	= reg.substring(34,40);
	     this.nomUserName 		= reg.substring(40,60);
	     
	     if(!"".equals(this.codOrgoaLotacao.trim())){
	     Orgao.Le_Orgao(codOrgoaLotacao,0);
	     this.setSigOrgao(Orgao.getSigOrgao());
	     }

	}

	public String getNumPlaca() {
		return numPlaca;
	}

	public String getNumRenavam() {
		return numRenavam;
	}

	public String getIndContinuidade() {
		return indContinuidade;
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}

	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}

	public String getNomUserName() {

		return nomUserName;
	}



}