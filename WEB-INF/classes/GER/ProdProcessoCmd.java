package GER;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ProdProcessoCmd extends sys.Command 
{
	private static final String jspPadrao = "/GER/ProdProcesso.jsp";
	private String next;
	public ProdProcessoCmd() { next = jspPadrao; }
	public ProdProcessoCmd(String next) { this.next = next; }

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{
		String nextRetorno = jspPadrao;
		String datIniRel ="";
		String datFimRel ="";
		String[] relator = null;		        
		try {
             ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
			 ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
			ProdProcessoBean ProdProcessoBeanId     = (ProdProcessoBean)req.getAttribute("ProdProcessoBeanId") ;
					if (ProdProcessoBeanId==null)  ProdProcessoBeanId = new ProdProcessoBean() ;
            
             String verGrafico = req.getParameter("verGrafico");
 			if (verGrafico == null) verGrafico = "N";
 			
 			String acao = req.getParameter("acao");
 			if (acao == null) acao = " ";
 			
 			String orgao = req.getParameter("codOrgao");
 			if (orgao == null) orgao = " ";
 			
			String [] parRelatores = req.getParameterValues("Relatores");
			if (parRelatores == null) parRelatores = new String[0];
			
			String TPreq = req.getParameter("TPreq");
			
			if ("0".equals(TPreq)){     
				TPreq = "'DP','DC'";
			   /*LabelRec = "DEFESA PR�VIA";*/
			}else if("1".equals(TPreq)){ 
				TPreq = "'1P','1C'";
			   /*LabelRec = "1a. INST�NCIA";*/
			}else{
				TPreq = "'2P','2C'";
			   /*LabelRec = "2a. INST�NCIA";*/
			}
			
			
			
						
			String relatores = "";
			for (int i = 0; i < parRelatores.length; i++)
				relatores += "&relator=" + parRelatores[i];
 			
 			if (acao.equals("Grafico")) { 				
 				verGrafico = "S";	
				req.setAttribute("ProdProcessoBeanId",ProdProcessoBeanId);	
 			}
 		
 			if(acao.equals("Retornar")){
 				verGrafico = "N"; 
 			}
 			if(acao.equals("CarregaRelatores")){
 				ProdProcessoBeanId.geraConsulta(orgao,ProdProcessoBeanId);				
 			} 			
 			
 			req.setAttribute("verGrafico", verGrafico);
 			req.setAttribute("cod_uf",SistemaBeanId.getCodUF());
 			req.setAttribute("relatores",relatores);
 			req.setAttribute("TPreq",TPreq);
 			req.setAttribute("ProdProcessoBeanId",ProdProcessoBeanId);
 			
			
			
		} catch (Exception e) {	throw new sys.CommandException("ProdProcessoCmd: " + e.getMessage());	}		
		return nextRetorno;
	}
}



//================================================================