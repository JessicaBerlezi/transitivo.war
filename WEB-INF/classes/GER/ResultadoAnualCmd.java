package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResultadoAnualCmd extends sys.Command 
{
	private static final String jspPadrao = "/GER/ResultadoAnual.jsp";
	private String next;
	public ResultadoAnualCmd() { next = jspPadrao; }
	public ResultadoAnualCmd(String next) { this.next = next; }

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{
		String nextRetorno = next;
				        
		try {
             ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
			 ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
             ResultadoAnualBean ResultadoAnualBeanId = (ResultadoAnualBean) req.getSession().getAttribute("ResultadoAnualBeanId");
			 if( ResultadoAnualBeanId == null ) ResultadoAnualBeanId = new ResultadoAnualBean();
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
 			 
             if ( acao.equals("") ) {
                req.setAttribute("ResultadoAnualBeanId", ResultadoAnualBeanId);
                req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
            }
            else if(acao.equals("ImprimeRelatorio")) 
			{    
            	ResultadoAnualBeanId = new ResultadoAnualBean();
            	String LabelRec = "";
            	String Ano = req.getParameter("ano");
    			if (Ano == null) Ano = "";
    			
    			String codOrgao = req.getParameter("codOrgao");
     			if (codOrgao == null) codOrgao = "";
     			
     			String todosOrgaos = req.getParameter("TodosOrgaos");
    			if (todosOrgaos == null) todosOrgaos = "";
    			 
    			String tipoReq = req.getParameter("TPreq");
    			if ("0".equals(tipoReq)){     
    			 	tipoReq = "'DP','DC'";
    			 	LabelRec = "DEFESA PR�VIA";
				}else if("1".equals(tipoReq)){ 
				 	tipoReq = "'1P','1C'";
				 	LabelRec = "1a. INST�NCIA";
				}else{
				 	tipoReq = "'2P','2C'";
				 	LabelRec = "2a. INST�NCIA";
				}
            	 
    			 ResultadoAnualBeanId.setAno(Ano);
    			 ResultadoAnualBeanId.setTpreq(tipoReq);
				 if ("0".equals(codOrgao)) 
				 	ResultadoAnualBeanId.setSigOrgao("Todos");				 
				 	
    			 ResultadoAnualBeanId.setCodEstado(SistemaBeanId.getCodUF());    			 
    			 ResultadoAnualBeanId.setOrgao(codOrgao);
    			 ResultadoAnualBeanId.CarregaEstatisticas();
 
            	String tituloConsulta = "RELAT�RIO DE RESULTADOS ANUAIS - "+ LabelRec+
            	       " - ANO: "+Ano;
            	
				req.setAttribute("tituloConsulta", tituloConsulta);
				req.setAttribute("ResultadoAnualBeanId", ResultadoAnualBeanId);
				req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
				nextRetorno = "/GER/ResultadoAnualImp.jsp";

				
			/*
				if( ResultadoAnualBeanId.ConsultaRelDiario( datIniRel, datFimRel,vErro ) )				{  
					String tituloConsulta = "RELAT�RIO -ARQUIVOS DE NOTIFICA��O : "+ datIniRel +" a "+ datFimRel;
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/GER/ResultadoAnualImp.jsp";
				}
				else {
					  ResultadoAnualBeanId.setDatIni( datIniRel );
					  ResultadoAnualBeanId.setDatFim( datFimRel );
					  nextRetorno = "/GER/ResultadoAnual.jsp";
				}
		*/
			}else if(acao.equals("DETALHE")) 
			{ 
				ResultadoAnualBeanId.setCodEstado(req.getParameter("codUf"));
				ResultadoAnualBeanId.setOrgao(req.getParameter("codOrgao"));
                
				String mes = ResultadoAnualBeanId.getMes(req.getParameter("mes"));
				String datInicio = "01/"+mes+"/"+req.getParameter("ano");
                String datFim = ResultadoAnualBeanId.getUltimoDia(mes,req.getParameter("ano"))+"/"+mes+"/"+req.getParameter("ano");
                
				ResultadoAnualBeanId.setTpreq(req.getParameter("tipo1Solic")+","+req.getParameter("tipo2Solic"));
			    String tipoResultado = req.getParameter("tipoResultado");
			    REC.GuiaDistribuicaoBean guiaBean = new REC.GuiaDistribuicaoBean();
			    guiaBean.setCodOrgaoLotacao(UsuarioBeanId.getOrgao().getCodOrgao());
				
			    ResultadoAnualBeanId.getGuias(ResultadoAnualBeanId,tipoResultado,datInicio,datFim, guiaBean);
			    req.setAttribute("ResultadoAnualBeanId", ResultadoAnualBeanId);
	            req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
	            req.getSession().setAttribute("GuiaDistribuicaoId",guiaBean);
				nextRetorno = "/REC/GuiaPreparaImpAutomatica.jsp" ;   
			}
			
		} catch (Exception e) {	throw new sys.CommandException("ResultadoAnualCmd: " + e.getMessage());	}		
		return nextRetorno;
	}
}