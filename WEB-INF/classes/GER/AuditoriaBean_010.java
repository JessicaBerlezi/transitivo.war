package GER;
import ACSS.OrgaoBean;

import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - AJUSTE CPF<br>
* <b>Description:</b>  Bean Auditoria_010 - Informa��es da transa��o 414<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_010 extends AuditoriaBean {
	
	private String numAutoInfracao  ;
	private String numPlaca			;
	private String codOrgao 		;
	private String numProcesso 		;
	private String codStatus 		;
	private String datStatus 		;
	private String codEvento 		;
	private String codOrigemEvento  ;
	private String nomUserName		;
	private String codOrgoaLotacao	;
	private String indResp			;
	private String indCpf_Cnpj		;	
	private String cpf_Cnpj	;	
	
	public AuditoriaBean_010() throws sys.BeanException {
		
		numAutoInfracao      = "";
		numPlaca  			 = "";
		codOrgao  			 = "";
		numProcesso          = "";
		codStatus    		 = "";
		datStatus         	 = "";
		codEvento      		 = "";
		codOrigemEvento   	 = "";
		nomUserName      	 = "";
		codOrgoaLotacao      = "";
		indResp      		 = "";
		indCpf_Cnpj     	 = "";
		cpf_Cnpj      		 = "";		
		
		
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",104);		 
	    
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();
		 
		 this.numAutoInfracao   = reg.substring(0,12)  ;
	     this.numPlaca 			= reg.substring(12,19) ;
	     this.codOrgao 			= reg.substring(19,25) ;
	     this.numProcesso 		= reg.substring(25,45) ;
	     this.codStatus 		= reg.substring(45,48) ;
	     this.datStatus 		= reg.substring(48,56) ;
	     this.codEvento 		= reg.substring(56,59) ;
	     this.codOrigemEvento 	= reg.substring(59,62) ;	     
	     this.nomUserName 		= reg.substring(62,82) ;
	     this.codOrgoaLotacao 	= reg.substring(82,88) ;
	     this.indResp 			= reg.substring(88,89) ;
	     this.indCpf_Cnpj 		= reg.substring(89,90) ;
	     this.cpf_Cnpj 			= reg.substring(90,104);     
	     
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }
	     
	}
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}

	public String getNumPlaca() {
		return numPlaca;
	}	
	public String getCodOrgao() {
		return codOrgao;
	}
	public String getNumProcesso() {
		return numProcesso;
	}	
	public String getCodStatus() {
		return codStatus;
	}
	public String getDatStatus() {
		String data = "";
		data = this.datStatus.substring(6,8)+"/"+this.datStatus.substring(4,6)+"/"+
		this.datStatus.substring(0,4);
		return data;
	}	
	public String getCodEvento() {
	
		return codEvento;
	}
	public String getCodOrigemEvento() {
		return codOrigemEvento;
	}
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public String getIndResp() {
		return indResp;
	}
	public String getIndCpf_Cnpj() {
		return indCpf_Cnpj;
	}	
	public String getCpf_CnpjEdt()  {
		if ("".equals(this.cpf_Cnpj)) return this.cpf_Cnpj ;
	    if ("1".equals(this.indCpf_Cnpj)) {
	    	if (this.cpf_Cnpj.length()<14) this.cpf_Cnpj += "               ";
		  	return this.cpf_Cnpj.substring(3,6)+"."+this.cpf_Cnpj.substring(6,9)+"."+this.cpf_Cnpj.substring(9,12)+"-"+this.cpf_Cnpj.substring(12,14) ;
	    }
		else { 
			if (this.cpf_Cnpj.length()<18) this.cpf_Cnpj += "                    ";
			return this.cpf_Cnpj.substring(0,2)+"."+this.cpf_Cnpj.substring(2,5)+"."+this.cpf_Cnpj.substring(5,8)+"/"+this.cpf_Cnpj.substring(8,12)+"-"+this.cpf_Cnpj.substring(12,14);
		}
	  }

	public String getCpf_Cnpj() {
		return cpf_Cnpj;
	}		
}