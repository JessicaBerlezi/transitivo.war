	package GER;
	
	import java.util.ArrayList;
	
	
	public class ProdProcessoBean extends sys.HtmlPopupBean {
		
		private String codrelator;
		private String nomrelator;
		private ArrayList relatores; 
		private ArrayList relatoresescolhidos;
		
		public ProdProcessoBean() throws sys.BeanException {
			
			codrelator                 = "";
			nomrelator       		   = "";
			relatores        		   =new ArrayList(); 
			relatoresescolhidos        =new ArrayList();
		}
		//		--------------------------------------------------------------------------
		public void setRelatores(ArrayList relatores)  {
			this.relatores=relatores ;	
		}
		
		public ArrayList getRelatores()  {
			return this.relatores;
		}
		//		--------------------------------------------------------------------------
		public void setNomrelator(String nomrelator)  {
			this.nomrelator=nomrelator ;	
		}
		
		public String getNomrelator()  {
			return this.nomrelator;
		}
		
		//		--------------------------------------------------------------------------
		public void setCodrelator(String codrelator)  {
			this.codrelator=codrelator ;	
		}
		
		public String getCodrelator()  {
			return this.codrelator;
		}
		public void geraConsulta(String orgao,ProdProcessoBean myprod){
			try { 	 
				Dao dao = Dao.getInstance();
				dao.geraConsulta(orgao,this); {
				}	   		   
			}// fim do try
			catch (Exception ex) {            
			}
		}		
		
	}
	
	
	