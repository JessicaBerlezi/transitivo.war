package GER;

import java.util.ArrayList;

import sys.DaoException;


public class RelatProcAbertosBean extends sys.HtmlPopupBean {

	
	private ArrayList dados;
	private String codOrgao; 
	private String sigOrgao;
	private String dataIni;
	private String dataFim;

	private String tpreq;
	private String procWEB;
	private String userName;
	private String datProcDP;
	private String numProcesso;
	private int qtdProc;
	private String indUltimo;
	
	
	


	public RelatProcAbertosBean() throws sys.BeanException {
		dados       = new ArrayList();
		codOrgao    = "";
		sigOrgao    = "";
		dataIni     = sys.Util.formatedToday().substring(0,10);
		dataFim     = sys.Util.formatedToday().substring(0,10);
		userName    = "";
		tpreq       = "";
		procWEB     = "";
		datProcDP   = "";
		numProcesso = "";
		qtdProc     = 0;
		indUltimo   = "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	

	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}
	
	
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}
	
	
	public void setTpreq(String tpreq) {
		if(tpreq == null) tpreq = "";
		else {
			if ("0".equals(tpreq)){     
				tpreq = "DEFESA PR�VIA";
			}else if("1".equals(tpreq)){ 
				tpreq = "1a. INST�NCIA";
			}else if("2".equals(tpreq)){
				tpreq = "2a. INST�NCIA";
			}else if("3".equals(tpreq)){
			    tpreq = "TRI";
	        }else if("4".equals(tpreq)){
		        tpreq = "REN�NCIA DE DEFESA PR�VIA";
	        }
			else 			
			  this.tpreq = tpreq;
		}
	}
	public String getTpreq() {
		return tpreq;
	}
	
	
	public void setDatProcDP(String datProcDP) {
		if(datProcDP == null) datProcDP = "";
		else this.datProcDP = datProcDP;
	}
	public String getDatProcDP() {
		return datProcDP;
	}
	

	public void setNumProcesso(String numProcesso) {
		if (numProcesso == null) numProcesso = "";
		else this.numProcesso = numProcesso;
	}
	public String getNumProcesso() {
		return numProcesso;
	}
	
	
	public void setDados(ArrayList dados) {
		this.dados = dados;
	}
	public ArrayList getDados() {
		return this.dados;
	}
	
	
	public void setQtdProc(int qtdProc) {
		this.qtdProc = qtdProc;
	}
	public int getQtdProc() {
		return qtdProc;
	}
	
	
	public void setUserName(String userName) {
		if(userName == null) userName = "";
		else this.userName = userName;
	}
	public String getUserName() {
		return userName;
	}
	
	
	public void setIndUltimo(String indUltimo) {
		if(indUltimo == null) indUltimo = "";
		else this.indUltimo = indUltimo;
	}
	public String getIndUltimo() {
		return indUltimo;
	}

	
//	--------------------------  Metodos da Bean ----------------------------------
    
	public void getProcessosAbertos(RelatProcAbertosBean RelatProcAbertosId) throws DaoException  {
		DaoControle dao;
		dao = DaoControle.getInstance();
		dao.getProcessosAbertos(RelatProcAbertosId);
	}

	public String getProcWEB() {
		return procWEB;
	}

	public void setProcWEB(String procWEB) {
		if (procWEB==null) procWEB="";
		this.procWEB = procWEB;
	}



  







	










}