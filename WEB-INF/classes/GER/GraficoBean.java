/*
 * Created on 10/12/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package GER;

/**
 * @author jheitor
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Paint;
import java.text.DecimalFormat;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.Axis;
import org.jfree.chart.labels.CategoryLabelGenerator;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryLabelGenerator;
import org.jfree.chart.labels.StandardPieItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer3D;
import org.jfree.chart.renderer.category.StackedBarRenderer3D;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

public abstract class GraficoBean extends sys.HtmlPopupBean {

    protected String myGrafico;

    protected String myDominio;

    protected String todosOrgaos;

    protected String codOrgao;

    protected String siglaOrgao;

    protected String codMunicipio;

    protected String siglaMunicipio;

    protected String nomMunicipio;

    protected String codRegiao;

    protected String nomRegiao;

    protected String codEstado;

    protected String nomEstado;

    protected String datInicio;

    protected String datFim;

    protected String siglaFuncao;

    protected String tipoReq;

    protected String codUF;

    protected String nomeMunicipio[];

    protected Double InfMunicipio[];

    protected Double Status[][];

    protected String relatores[];

    protected int dadosProcesso[][];

    protected ArrayList relat;

    protected String TPreq[];

    protected static Color TitleColor = new java.awt.Color(165, 42, 42);

    protected static Color BRANCO = java.awt.Color.white;

    protected static Color PRETO = java.awt.Color.black;

    protected static Color AZUL = java.awt.Color.blue;

    protected static Color VERDE = java.awt.Color.green;

    protected static Color AMARELO = java.awt.Color.yellow;

    protected static Color VERMELHO = java.awt.Color.red;

    protected static Color VERMELHO_CLARO = new java.awt.Color(165, 42, 42);

    protected static Color LARANJA = java.awt.Color.orange;

    protected static Color AZUL_CLARO = java.awt.Color.cyan;

    protected static int BOLD = java.awt.Font.BOLD;

    protected static int LARGURA = 500;

    protected static int ALTURA = 275;

    protected static int PIZZA = 1;

    protected static int BARRA = 2;

    protected static int STACKED = 3;

    protected static int BARRADUPLA = 4;

    protected static int BARRAQUINTUPLA = 5;

    protected JFreeChart jfreechart;

    public GraficoBean() {

        myGrafico = "";
        myDominio = "E";
        todosOrgaos = "N";
        codOrgao = "";
        siglaOrgao = "";
        codMunicipio = "";
        siglaMunicipio = "";
        nomMunicipio = "";
        codRegiao = "";
        nomRegiao = "";
        codEstado = "";
        nomEstado = "RJ";
        datInicio = "";
        datFim = "";
        tipoReq = "";
        codUF = "";
        nomeMunicipio = new String[93];
        InfMunicipio = new Double[93];
        Status = new Double[5][12];
        relatores = new String[20];
        dadosProcesso = new int[20][10];
        TPreq = new String[20];
    }

    // Metodos para gera�ao e configura�ao do grafico
    public abstract void criaGrafico();

    public JFreeChart obtemGrafico() {
        return jfreechart;
    }

    public void geraGrafico(int tipo, String demonst) {

        if (tipo == PIZZA) {

            DefaultPieDataset defaultpiedataset = new DefaultPieDataset();
            jfreechart = ChartFactory.createPieChart3D("�rg�o: " + siglaOrgao,
                    defaultpiedataset, true, false, false);

            PiePlot3D plot = (PiePlot3D) jfreechart.getPlot();
            plot.setForegroundAlpha(0.4f);
            plot.setCircular(false);

            StandardPieItemLabelGenerator generator = new StandardPieItemLabelGenerator(
                    "{2}");
            plot.setLabelGenerator(generator);
            Paint paint[] = { Color.white };
            plot.setLabelBackgroundPaint(paint[0]);

            // popula o grafico
            int j = InfMunicipio.length;
            for (int i = 0; i < j; i++) {
                Double l = InfMunicipio[i];
                if (l.doubleValue() > 0)
                    defaultpiedataset.setValue(nomeMunicipio[i], l);
            }
        }

        else if (tipo == BARRA) {

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            jfreechart = ChartFactory.createBarChart3D("�rg�o: " + siglaOrgao,
                    "", "", dataset, PlotOrientation.VERTICAL, true, false,
                    false);

            CategoryPlot plot = (CategoryPlot) jfreechart.getPlot();
            plot.setForegroundAlpha(0.7f);
            CategoryLabelGenerator generator = new StandardCategoryLabelGenerator(
                    "{2}", new DecimalFormat("0000"));

            BarRenderer3D renderer = (BarRenderer3D) plot.getRenderer();
            renderer.setLabelGenerator(generator);
            renderer.setDrawBarOutline(true);
            renderer.setItemLabelsVisible(true);
            renderer.setSeriesPaint(0, AZUL_CLARO);
            renderer.setSeriesPaint(1, VERDE);
            renderer.setSeriesPaint(2, LARANJA);
            renderer.setSeriesPaint(3, VERMELHO);

            // popula o grafico
            int j = InfMunicipio.length;
            for (int i = 0; i < j; i++) {
                Double l = InfMunicipio[i];
                if (l.doubleValue() > 0)
                    dataset.setValue(l, nomeMunicipio[i], "");
            }
        }

        else if (tipo == STACKED) {

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            jfreechart = ChartFactory.createStackedBarChart3D("�rg�o: "
                    + siglaOrgao, "", "", dataset, PlotOrientation.VERTICAL,
                    true, false, false);

            CategoryPlot plot = (CategoryPlot) jfreechart.getPlot();
            plot.setForegroundAlpha(0.7f);
            CategoryLabelGenerator generator = new StandardCategoryLabelGenerator(
                    "{2}", new DecimalFormat("0000"));

            StackedBarRenderer3D renderer = (StackedBarRenderer3D) plot
                    .getRenderer();
            renderer.setLabelGenerator(generator);
            renderer.setDrawBarOutline(true);
            renderer.setPositiveItemLabelPosition(new ItemLabelPosition(
                    ItemLabelAnchor.OUTSIDE12, org.jfree.ui.TextAnchor.CENTER));
            renderer.setItemLabelsVisible(true);
            renderer.setSeriesPaint(0, AZUL_CLARO);
            renderer.setSeriesPaint(1, VERDE);
            renderer.setSeriesPaint(2, LARANJA);
            renderer.setSeriesPaint(3, VERMELHO);
            renderer.setSeriesPaint(4, AMARELO);

            String nomeStatus[] = new String[0];
            if (demonst.equals("TJ"))
                nomeStatus = new String[] { "Status 90", "Status 91",
                        "Status 95", "Status 96,97,98", "Status 99" };
            else if (demonst.equals("Auto"))
                nomeStatus = new String[] { "Status 0,1,2,4", "Status 5" };
            else if (demonst.equals("PD"))
                nomeStatus = new String[] { "Status 10,11,12,14",
                        "Status 15,16", "Status 30", "Status 35" };

            // popula o grafico
            for (int status = 0; status < nomeStatus.length; status++)
                for (int mes = 0; mes < 12; mes++)
                    dataset.addValue(Status[status][mes], nomeStatus[status],
                            nomeMunicipio[mes]);

        }

        else if (tipo == BARRADUPLA) {

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            jfreechart = ChartFactory.createBarChart3D("�rg�o: " + siglaOrgao,
                    "", "", dataset, PlotOrientation.VERTICAL, true, false,
                    false);

            CategoryPlot plot = (CategoryPlot) jfreechart.getPlot();
            plot.setForegroundAlpha(0.7f);
            CategoryLabelGenerator generator = new StandardCategoryLabelGenerator();

            BarRenderer3D renderer = (BarRenderer3D) plot.getRenderer();
            renderer.setLabelGenerator(generator);
            renderer.setDrawBarOutline(true);
            renderer.setPositiveItemLabelPosition(new ItemLabelPosition(
                    ItemLabelAnchor.OUTSIDE12, org.jfree.ui.TextAnchor.CENTER));
            renderer.setItemLabelsVisible(true);
            renderer.setSeriesPaint(0, AZUL_CLARO);
            renderer.setSeriesPaint(1, VERDE);

            String tipos[] = { "Cancelamentos", "Ajuste" };

            // popula o grafico
            for (int status = 0; status < 2; status++)
                for (int mes = 0; mes < 12; mes++) {
                    dataset.setValue(Status[status][mes], tipos[status],
                            nomeMunicipio[mes]);
                }

        } else if (tipo == BARRAQUINTUPLA) {

            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            jfreechart = ChartFactory.createBarChart3D(
                    "Distribui��o de Processos", "", "", dataset,
                    PlotOrientation.VERTICAL, true, false, false);

            CategoryPlot plot = (CategoryPlot) jfreechart.getPlot();
            plot.setForegroundAlpha(0.7f);
            plot.setBackgroundAlpha(0.1f);

            CategoryLabelGenerator generator = new StandardCategoryLabelGenerator();

            BarRenderer3D renderer = (BarRenderer3D) plot.getRenderer();
            renderer.setLabelGenerator(generator);
            renderer.setDrawBarOutline(true);
            renderer.setPositiveItemLabelPosition(new ItemLabelPosition(
                    ItemLabelAnchor.OUTSIDE12,
                    org.jfree.ui.TextAnchor.BASELINE_LEFT));
            renderer.setItemLabelsVisible(true);

            if (demonst.equals("distribuicao")) {
                // modifica o tamanho dos labels
                Axis axis = plot.getDomainAxis();
                axis.setTickLabelFont(new Font("BOLD", 12, 8));

                int numeroMeses = Integer.parseInt(datFim.substring(0, 2))
                        - Integer.parseInt(datInicio.substring(0, 2)) + 1;

                if (numeroMeses >= 1)
                    renderer.setSeriesPaint(0, AZUL_CLARO);
                if (numeroMeses >= 2)
                    renderer.setSeriesPaint(1, VERDE);
                if (numeroMeses >= 3)
                    renderer.setSeriesPaint(2, VERMELHO);
                if (numeroMeses >= 4)
                    renderer.setSeriesPaint(3, LARANJA);
                if (numeroMeses >= 5)
                    renderer.setSeriesPaint(4, AMARELO);
                if (numeroMeses >= 6)
                    renderer.setSeriesPaint(5, AZUL);
                if (numeroMeses >= 7)
                    renderer.setSeriesPaint(6, VERMELHO_CLARO);
                if (numeroMeses >= 8)
                    renderer.setSeriesPaint(7, LARANJA);
                if (numeroMeses >= 9)
                    renderer.setSeriesPaint(8, AZUL_CLARO);
                if (numeroMeses >= 10)
                    renderer.setSeriesPaint(9, AMARELO);
                if (numeroMeses >= 11)
                    renderer.setSeriesPaint(10, VERMELHO);
                if (numeroMeses >= 12)
                    renderer.setSeriesPaint(11, PRETO);

                /*
                 * String tipos[] =
                 * {"Deferidos","Indeferidos","Analisados","Distribu�dos","Diferen�a","Entrada","Dif
                 * Entrada ","Estoque"};
                 */
                String tipos[] = { "Deferidos", "Indeferidos", "Analisados",
                        "Distribu�dos", "Diferen�a" };
                String[] meses = { " ", "Jan", "Fev", "Mar", "Abr", "Mai",
                        "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez" };
                // popula o grafico
                for (int status = 0; status < tipos.length; status++)
                    for (int data = 1; data <= 12; data++) {
                        if (data >= Integer.parseInt(datInicio.substring(0, 2))
                                && data <= Integer.parseInt(datFim.substring(0,
                                        2)))
                            dataset.setValue(dadosProcesso[status][data - 1],
                                    meses[data], tipos[status]);
                    }

            } else {
                renderer.setSeriesPaint(0, AZUL_CLARO);
                renderer.setSeriesPaint(1, VERDE);
                renderer.setSeriesPaint(2, VERMELHO);
                renderer.setSeriesPaint(3, LARANJA);
                renderer.setSeriesPaint(4, AMARELO);

                String tipos[] = { "Deferidos", "Indeferidos",
                        "Total Analisados", "Distribuidos s/ res", "Total proc" };

                // popula o grafico
                for (int status = 0; status < tipos.length; status++)
                    for (int relat = 0; relat < relatores.length; relat++) {
                        if (relatores[relat] != null)
                            dataset.setValue(dadosProcesso[relat][status],
                                    tipos[status], relatores[relat]);
                    }
            }
        }

        jfreechart.setBackgroundPaint(Color.white);
        if (tipo != BARRAQUINTUPLA)
            jfreechart.addSubtitle(new TextTitle("De: " + datInicio + " At�: "
                    + datFim));
        else if (demonst.equals("distribuicao")) {
            String[] meses = { " ", "Janeiro", "Fevereiro", "Mar�o", "Abril",
                    "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro",
                    "Novembro", "Dezembro" };
            String mesIni = meses[Integer.parseInt(datInicio.substring(0, 2))]
                    + datInicio.substring(2, 7);
            String mesFim = meses[Integer.parseInt(datFim.substring(0, 2))]
                    + datFim.substring(2, 7);
            String label = "DEFESA PR�VIA";
            if ("1".equals(tipoReq))
                label = "1a. INST�NCIA";
            if ("2".equals(tipoReq))
                label = "2a. INST�NCIA";

            jfreechart.addSubtitle(new TextTitle("Per�odo: " + mesIni + " a "
                    + mesFim + "   �rg�o: " + siglaOrgao + " - " + label));

        } else {
            String[] meses = { " ", "Janeiro", "Fevereiro", "Mar�o", "Abril",
                    "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro",
                    "Novembro", "Dezembro" };
            String mes = meses[Integer.parseInt(datInicio.substring(0, 2))]
                    + datInicio.substring(2, 7);
            String label = "DEFESA PR�VIA";
            if ("'1P','1C'".equals(tipoReq))
                label = "1a. INST�NCIA";
            if ("'2P','2C'".equals(tipoReq))
                label = "2a. INST�NCIA";

            jfreechart.addSubtitle(new TextTitle("Per�odo: " + mes
                    + "   �rg�o: " + siglaOrgao + " - " + label));
        }
        ChartPanel chartpanel = new ChartPanel(jfreechart);

    }

    // Metodos para obten�ao dos atributos do bean
    public void setNomeMunicipio(String nome[]) {
        if (nome == null)
            nome = new String[92];
        this.nomeMunicipio = nome;
    }

    public String[] getNomeMunicipio() {
        return this.nomeMunicipio;
    }

    public void setInfMunicipio(Double intVal[]) {
        if (intVal == null)
            intVal = new Double[92];
        this.InfMunicipio = intVal;
    }

    public Double[] getInfMunicipio() {
        return this.InfMunicipio;
    }

    public void setMyGrafico(String myGrafico) {
        if (myGrafico == null)
            myGrafico = "";
        this.myGrafico = myGrafico;
    }

    public String getMyGrafico() {
        return this.myGrafico;
    }

    public void setMyDominio(String myDominio) {
        if (myDominio == null)
            myDominio = "E";
        this.myDominio = myDominio;
    }

    public String getMyDominio() {
        return this.myDominio;
    }

    public void setTodosOrgaos(String todosOrgaos) {
        if (todosOrgaos.equals("null"))
            todosOrgaos = "N";
        else
            this.todosOrgaos = todosOrgaos;
    }

    public String getTodosOrgaos() {
        return this.todosOrgaos;
    }

    public void setCodOrgao(String codOrgao) {
        if (codOrgao == null)
            codOrgao = "";
        this.codOrgao = codOrgao;
    }

    public String getCodOrgao() {
        return this.codOrgao;
    }

    public void setSiglaOrgao(String siglaOrgao) {
        if (siglaOrgao == null)
            siglaOrgao = "";
        this.siglaOrgao = siglaOrgao;
    }

    public String getSiglaOrgao() {
        return this.siglaOrgao;
    }

    public void setCodMunicipio(String codMunicipio) {
        if (codMunicipio == null)
            codMunicipio = "";
        this.codMunicipio = codMunicipio;
    }

    public String getCodMunicipio() {
        return this.codMunicipio;
    }

    public void setSiglaMunicipio(String siglaMunicipio) {
        if (siglaMunicipio == null)
            siglaMunicipio = "";
        this.siglaMunicipio = siglaMunicipio;
    }

    public String getSiglaMunicipio() {
        return this.siglaMunicipio;
    }

    public void setNomMunicipio(String nomMunicipio) {
        if (nomMunicipio == null)
            nomMunicipio = "";
        this.nomMunicipio = nomMunicipio;
    }

    public String getNomMunicipio() {
        return this.nomMunicipio;
    }

    public void setStatus(Double[][] status) {
        if (status == null)
            Status = new Double[5][12];
        Status = status;
    }

    public Double[][] getStatus() {
        return Status;
    }

    public void setCodRegiao(String codRegiao) {
        if (codRegiao == null)
            codRegiao = "";
        this.codRegiao = codRegiao;
    }

    public String getCodRegiao() {
        return this.codRegiao;
    }

    public void setNomRegiao(String nomRegiao) {
        if (nomRegiao == null)
            nomRegiao = "";
        this.nomRegiao = nomRegiao;
    }

    public String getNomRegiao() {
        return this.nomRegiao;
    }

    public void setCodEstado(String codEstado) {
        if (codEstado == null)
            codEstado = "";
        this.codEstado = codEstado;
    }

    public String getCodEstado() {
        return this.codEstado;
    }

    public void setNomEstado(String nomEstado) {
        if (nomEstado == null)
            nomEstado = "";
        this.nomEstado = nomEstado;
    }

    public String getNomEstado() {
        return this.nomEstado;
    }

    public void setDatInicio(String datInicio) {
        if (datInicio == null) datInicio = "01/01/2007";
        this.datInicio = datInicio;
    }

    public String getDatInicio() {
        return this.datInicio;
    }

    public void setDatFim(String datFim) {
        if (datFim == null) datFim = "31/12/2007";
        this.datFim = datFim;
    }

    public String getDatFim() {
        return this.datFim;
    }

    public void setFuncao(String funcao) {
        siglaFuncao = funcao;
    }

    public void setTipoReq(String tipo) {
        tipoReq = tipo;
    }

    public String getTipoReq() {
        return tipoReq;
    }

    public void setCodUF(String uf) {
        codUF = uf;
    }

    public String getCodUF() {
        return codUF;
    }

    public void setRelatores(String[] relat) {
        relatores = relat;
    }

    public void setRelat(ArrayList relat) {
        this.relat = relat;
    }

    public ArrayList getRelat() {
        return relat;
    }

    public String[] getRelatores() {
        return relatores;
    }

    public void setDadosProcesso(int[][] proc) {
        dadosProcesso = proc;
    }

    public void setAltura(int altura) {
        ALTURA = altura;
    }

    public void setLargura(int largura) {
        LARGURA = largura;
    }

    public int getAltura() {
        return ALTURA;
    }

    public int getLargura() {
        return LARGURA;
    }

    public void setTPreq(String[] preq) {
        TPreq = preq;
    }

    public String[] getTPreq() {
        return TPreq;
    }

}
