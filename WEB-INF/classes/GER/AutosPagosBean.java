package GER;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sys.BeanException;
import sys.DaoException;

/**
 * <b>Title:</b>        Gerencial - Autos Pagos Bean<br>
 * <b>Description:</b>  Informa os autos pagos de acordo com o �rg�o. <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author 
 * @version 1.0
 */

public class AutosPagosBean extends sys.HtmlPopupBean {
	
	private String codOrgao; 
	private String sigOrgao;
	private String mesAno;
	private String dia;
	ArrayList ListTotalPagoDia;
	ArrayList ListAutos;
	
	public AutosPagosBean() throws sys.BeanException {
		codOrgao = "";
		sigOrgao = "";
		mesAno   = PegarMesAno();
		dia      = "01";
		ListTotalPagoDia = new ArrayList();
		ListAutos        = new ArrayList();
	}
	
	public String getCodOrgao() {
		return codOrgao;
	}
	
	public void setCodOrgao(String codOrgao) {
		if (codOrgao==null) codOrgao="";
		this.codOrgao = codOrgao;
	}
	
	public String getSigOrgao() {
		return sigOrgao;
	}	
	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao==null) sigOrgao="";
		this.sigOrgao = sigOrgao;
	}
	public String getMesAno() {
		return mesAno;
	}
	
	public void setMesAno(String mesAno) {
		if (mesAno==null) mesAno=PegarMesAno();
		this.mesAno = mesAno;
	}
	
	public String getDia() {
		return dia;
	}
	
	public void setDia(String dia) {
		if (dia==null) dia="";
		this.dia = dia;
	}
	
	public ArrayList getListAutos() {
		return ListAutos;
	}
	
	public void setListAutos(ArrayList listAutos) {
		if (listAutos==null) listAutos = new ArrayList();
		this.ListAutos = listAutos;
	}
	
	public ArrayList getListTotalPagoDia() {
		return ListTotalPagoDia;
	}
	
	public void setListTotalPagoDia(ArrayList listTotalPagoDia) {
		if (ListTotalPagoDia==null) ListTotalPagoDia = new ArrayList();
		this.ListTotalPagoDia = listTotalPagoDia;
	}
	public void setListTotalPagoDia(ACSS.UsuarioBean UsrLogado) throws BeanException 
	{
		try{
			DaoBroker dao = DaoBrokerFactory.getInstance();			
			dao.consultaTotalPagoDia(this,UsrLogado);
		}
		catch (Exception ey) {
			throw new sys.BeanException(ey.getMessage());
		}
	}	
		
	public String PegarMesAno()
	{
		SimpleDateFormat df  = new SimpleDateFormat("dd/MM/yyyy");
		mesAno = df.format(new Date()).substring(3,10);
		String Mes = mesAno.substring(0,2);
		String Ano = mesAno.substring(3,7);
		if (Integer.parseInt(Mes)==1)
		{
			Mes="01";
			Ano=String.valueOf(Integer.parseInt(Mes)-1);
		}
		else
		{
			Mes=String.valueOf(Integer.parseInt(Mes)-1);
			if (Mes.length()==1) Mes="0"+Mes;
		}
		mesAno   = Mes+"/"+Ano;
		return mesAno;
	}
	
	 private String converteData(java.sql.Date data) {
		 String Retorno = null;
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 try {
			 Retorno = df.format(data);
		 } catch (Exception e) {
			 Retorno = "";
		 }
		 return Retorno;
	 }	 
	
	
}

