package GER;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ACSS.SistemaBean;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Sistema<br>
* <b>Description:</b>  Comando para Manter os Parametros por Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Michel
* @version 1.0
* @Updates
*/

public class NivelProcessoSistemaCmd extends sys.Command {

	private static final String jspPadrao = "/GER/NivelProcessoSistema.jsp";
	private String next;

	public NivelProcessoSistemaCmd() {
		next = jspPadrao;
	}

	public NivelProcessoSistemaCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException{
		String nextRetorno = next;
		try {  
			// cria os Beans do Usuario, se n�o existir
			SistemaBean SistId = (SistemaBean) req.getAttribute("SistId");
			if (SistId == null)	SistId = new SistemaBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";

			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "";

			String codProcesso = req.getParameter("codProcesso");
			if (codProcesso == null) codProcesso = "";

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)
				atualizarDependente = "N";

			SistId.Le_Sistema(codSistema, 0);
			SistId.setAtualizarDependente(atualizarDependente);				
			SistId.setCodProcesso(codProcesso);

			if ("buscaSistema".indexOf(acao) >= 0) {
				SistId.getNivelProcessos(20, 6);
				SistId.setAtualizarDependente("S");
			}


			Vector vErro = new Vector();

			if (acao.compareTo("A") == 0) {
				String[] nomNivelProcesso = req.getParameterValues("nomNivelProcesso");
				if (nomNivelProcesso == null) nomNivelProcesso = new String[0];

				String[] valNivelProcesso = req.getParameterValues("valNivelProcesso");
				if (valNivelProcesso == null) valNivelProcesso = new String[0];

				String[] codNivelProcesso = req.getParameterValues("codNivelProcesso");
				if (codNivelProcesso == null) codNivelProcesso = new String[0];
				
				String[] tipoMsg = req.getParameterValues("tipoMsg");
				if (tipoMsg == null) tipoMsg = new String[0];

				vErro =	isNivelProcessos(nomNivelProcesso,valNivelProcesso,codNivelProcesso, tipoMsg);
				if (vErro.size() == 0) {
					Vector nivelprocesso = new Vector();
					for (int i = 0; i < nomNivelProcesso.length; i++) {
						NivelProcessoSistemaBean myNivelProcesso = new NivelProcessoSistemaBean();
						myNivelProcesso.setNomNivelProcesso(nomNivelProcesso[i]);
						myNivelProcesso.setValNivelProcesso(valNivelProcesso[i]);
						myNivelProcesso.setCodSistema(codSistema);
						myNivelProcesso.setCodProcesso(codProcesso);
						myNivelProcesso.setCodNivelProcesso(codNivelProcesso[i]);
						myNivelProcesso.setTipoMsg(tipoMsg[i]);
						nivelprocesso.addElement(myNivelProcesso);
					}
					SistId.setNivelProcessos(nivelprocesso);
					SistId.isInsertNivelProcessos();
					SistId.getNivelProcessos(20, 6);
				}
				else
					SistId.setMsgErro(vErro);
			}
			
		    if(acao.compareTo("I") == 0){
				req.setAttribute("vNivelProcesso",SistId.getNivelProcessos(0,0));
				nextRetorno = "/GER/NivelProcessoSistemaImp.jsp";
		    }
		
			req.setAttribute("SistId", SistId);
		}
		catch (Exception ue) {
			throw new sys.CommandException(
				"NivelProcessoSistemaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	private Vector isNivelProcessos(String[] nomNivelProcesso, String[] valNivelProcesso,String[] codNivelProcesso,
			String[] tipoMsg)
	{
		Vector vErro = new Vector();
		if ((nomNivelProcesso.length != valNivelProcesso.length)
			|| (valNivelProcesso.length != codNivelProcesso.length)
			|| (tipoMsg.length != codNivelProcesso.length)
			|| (valNivelProcesso.length != tipoMsg.length)
			|| (nomNivelProcesso.length != tipoMsg.length))
			vErro.addElement("Nivel Processos inconsistentes");
		return vErro;
	}
}