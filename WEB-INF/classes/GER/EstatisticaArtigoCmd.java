package GER;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class EstatisticaArtigoCmd extends sys.Command {
	private static final String jspPadrao = "/GER/EstatisticaArtigo.jsp";
	private String next;
	
	public EstatisticaArtigoCmd() { 
		next = jspPadrao; 
	}
	public EstatisticaArtigoCmd(String next) { 
		this.next = next; 
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException	{
		String nextRetorno = next;
				        
		try {
             
			 ACSS.SistemaBean SistemaBeanId = (ACSS.SistemaBean) req.getSession().getAttribute("SistemaBeanId");
             EstatisticaArtigoBean EstatisticaArtigoBeanId = (EstatisticaArtigoBean) req.getSession().getAttribute("EstatisticaArtigoBeanId");
			 if( EstatisticaArtigoBeanId == null ) EstatisticaArtigoBeanId = new EstatisticaArtigoBean();
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
            
             if ( acao.equals("") ) {
                req.setAttribute("EstatisticaArtigoBeanId", EstatisticaArtigoBeanId);
                req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
            }
            else if(acao.equals("ImprimeRelatorio")){    
            	EstatisticaArtigoBeanId = new EstatisticaArtigoBean();
            	String LabelRec = "";
            	String mesAnoInicio = req.getParameter("mesAnoInicio");
   			    if (mesAnoInicio == null) mesAnoInicio = "";
            	String mesAnoFim = req.getParameter("mesAnoFim");
   			    if (mesAnoFim == null) mesAnoFim = "";
    			String codOrgao = req.getParameter("codOrgao");
     			if (codOrgao == null) codOrgao = "";
     			String todosOrgaos = req.getParameter("TodosOrgaos");
   			    if (todosOrgaos == null) todosOrgaos = "";
    			
   			    String tipoReq = req.getParameter("TPreq");
    			if ("0".equals(tipoReq)){     
    			 	tipoReq = "'DP','DC'";
    			 	LabelRec = "DEFESA PR�VIA";
				}else if("1".equals(tipoReq)){ 
				 	tipoReq = "'1P','1C'";
				 	LabelRec = "1a. INST�NCIA";
				}else{
				 	tipoReq = "'2P','2C'";
				 	LabelRec = "2a. INST�NCIA";
				}
            	 
    			EstatisticaArtigoBeanId.setMesAnoInicio(mesAnoInicio);
    			EstatisticaArtigoBeanId.setMesAnoFim(mesAnoFim);
    			EstatisticaArtigoBeanId.setTpreq(tipoReq);
				if ("S".equals(todosOrgaos)) 
					EstatisticaArtigoBeanId.setSigOrgao("Todos");
				 
				EstatisticaArtigoBeanId.setCodEstado(SistemaBeanId.getCodUF());    			 
				EstatisticaArtigoBeanId.setOrgao(codOrgao);
				EstatisticaArtigoBeanId.CarregaEstatisticas();
    			String tituloConsulta = "ESTATISTICA DA "+ LabelRec + " POR ARTIGO - PER�ODO DE "
				   	+ EstatisticaArtigoBeanId.getMesAnoInicio()+" A "
					+ EstatisticaArtigoBeanId.getMesAnoFim();
    			if(EstatisticaArtigoBeanId.getResultado().size()==0)
    				EstatisticaArtigoBeanId.setMsgErro("N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE PER�ODO");
    			req.setAttribute("tituloConsulta", tituloConsulta);
				req.setAttribute("EstatisticaArtigoBeanId", EstatisticaArtigoBeanId);
				nextRetorno = "/GER/EstatisticaArtigoImp.jsp";
				req.setAttribute("EstatisticaArtigoBeanId", EstatisticaArtigoBeanId);
				req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
			}
		} catch (Exception e) {	throw new sys.CommandException("EstatisticaArtigoCmd: " + e.getMessage());	}		
		return nextRetorno;
	}
}