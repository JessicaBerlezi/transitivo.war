package GER;

import ACSS.OrgaoBean;
import sys.BeanException;
import sys.Util;

/**
* <b>Title:</b>        M�dulo Gerencial - CONsulta ATUALIZA��O TABELA DE RELATORES<br>
* <b>Description:</b>  Bean Auditoria_028 - Informa��es daS transa��es 087<br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      Mind Informatica<br>
* @author Wellem Lyra
* @version 1.0
* @Updates
*/

public class AuditoriaBean_028 extends AuditoriaBean {	
	
	private String codOrgao				;
	private String codigo 				;
	private String cpf			 		;
	private String tipoAcao 			;
	private String indContinuidade 		;
	private String nome	 				;	
	private String codOrgoaLotacao 		;
	private String nomUserName 			;
	
	
	public AuditoriaBean_028() throws sys.BeanException {		
	
		codOrgao  			= "";	
		codigo 				= "";
		cpf 				= "";
		tipoAcao 			= "";
		indContinuidade     = "";
		nome 				= "";		
		codOrgoaLotacao     = "";
		nomUserName      	= "";		
			
	}

	public void setPropriedades(String reg) throws DaoException, BeanException{	
		 reg = Util.rPad(reg," ",96);
		 
		 OrgaoBean OrgaoAtu = new OrgaoBean ();	
		 OrgaoBean OrgaoLot = new OrgaoBean ();		 
	    
		 this.codOrgao 			= reg.substring(0,6);
		 this.codigo 			= reg.substring(6,12);
		 this.cpf		        = reg.substring(12,23);
	     this.tipoAcao 			= reg.substring(23,24);
	     this.indContinuidade 	= reg.substring(24,30);
	     this.nome 				= reg.substring(30,70);	        	     
	     this.codOrgoaLotacao 	= reg.substring(70,76);
	     this.nomUserName 		= reg.substring(76,96);	     
	    
	     if(!"".equals(this.codOrgao.trim())){
		     OrgaoAtu.Le_Orgao(codOrgao,0);
		     this.setSigOrgaoAtuacao(OrgaoAtu.getSigOrgao());
	     }
	     if(!"".equals(this.codOrgoaLotacao.trim())){
		     OrgaoLot.Le_Orgao(codOrgoaLotacao,0);
		     this.setSigOrgao(OrgaoLot.getSigOrgao());		     
	     }	     
	     
	}
	public String getCodOrgao() {
		return codOrgao;
	}	
	public String getCodigo() {
		return codigo;
	}	
	
	public String getTipoAcao() {
		String acao = this.tipoAcao;
		if (acao.equals("1"))
			acao="Inclus�o";
		if (acao.equals("2"))
			acao="Altera��o";
		if (acao.equals("3"))
			acao="Exclus�o";
		if (acao.equals("4"))
			acao="Consulta Simples";
		if (acao.equals("5"))
			acao="Consulta Geral";
		return acao;
	}	
	
	public String getIndContinuidade() {
		return indContinuidade;
	}
	public String getCpf() {
		String cpf =this.cpf;
		cpf = cpf.substring(0,3)+"."+this.cpf.substring(3,6)+"."+this.cpf.substring(6,9)+"-"+this.cpf.substring(9,11) ;	
		
		return cpf;
	}		
	public String getNome() {
		return nome;
	}	
	public String getCodOrgoaLotacao() {
		return codOrgoaLotacao;
	}	
	public String getNomUserName() {
		return nomUserName;
	}
	
	}