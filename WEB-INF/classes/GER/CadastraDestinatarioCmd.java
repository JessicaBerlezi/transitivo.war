package GER;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ACSS.OrgaoBean;

/**
* <b>Title:</b>       Gerencial - Cadastar Destinatários<br>
* <b>Description:</b> Tela de Cadastro dos e-mails dos Relatores de acordo com a junta<br>
* <b>Copyright:</b>   Copyright (c) 2006<br>
 * <b>Company: </b>   MIND Informatica <br>
* @author  Luciana Rocha	
* @version 1.0
*/

public class CadastraDestinatarioCmd extends sys.Command {


  private static final String jspPadrao="/GER/CadastraDestinatario.jsp";    
  private String next;

  public CadastraDestinatarioCmd() {
    next = jspPadrao;
  }

  public CadastraDestinatarioCmd(String next) {
    this.next = jspPadrao;
  }
  
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException{
	String nextRetorno  = next ;
    try {      
		// cria os Beans 
		OrgaoBean OrgaoId = (OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null) OrgaoId = new OrgaoBean() ;	 

		CadastraDestinatarioBean DestinatarioBean = (CadastraDestinatarioBean)req.getAttribute("DestinatarioBean") ;
		if(DestinatarioBean == null) DestinatarioBean = new CadastraDestinatarioBean() ;	
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)  acao =" ";   
		  
	    String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao == null) codOrgao =""; 
		
		String atualizarDependente = "N";
		
	    OrgaoId.Le_Orgao(codOrgao,0) ;  
		DestinatarioBean.setCodOrgao(codOrgao);
			 
	    if(acao.compareTo("buscaDestinatario")==0) {
		  List destinatarios = DestinatarioBean.getDestinatarios(DestinatarioBean);
		  if(destinatarios.size()<10){
		  	for(int i=destinatarios.size();i<10;i++)
		  		destinatarios.add(new CadastraDestinatarioBean());
		  }
		  else{
		  	for(int i=0;i<5;i++)
		  		destinatarios.add(new CadastraDestinatarioBean());
		  }
		  if( destinatarios != null ){
			  atualizarDependente ="S"; 	
			  req.setAttribute("destinatarios",destinatarios);
		  }
	    }
			 
		Vector vErro = new Vector(); 
					 
	    if(acao.compareTo("A") == 0){
			String[] codDestinatario= req.getParameterValues("codDestinatario");
			  
			String[] txtDestinatario = req.getParameterValues("txtDestinatario");  
		
			String[] txtEmail = req.getParameterValues("txtEmail");
					
			String[] txtTitulo = req.getParameterValues("txtTitulo");
					  
			vErro = isParametros(codDestinatario,txtDestinatario,txtEmail) ;
						 
            if (vErro.size()==0) {
				List destinatarios 	 = new ArrayList(); 
				CadastraDestinatarioBean myDestinatario; 
				for (int i = 0; i < txtDestinatario.length;i++) {
					myDestinatario = new CadastraDestinatarioBean();	
					myDestinatario.setCodDestinatario(codDestinatario[i]);
					myDestinatario.setTxtDestinatario(txtDestinatario[i]);	
					myDestinatario.setCodOrgao(codOrgao);
					myDestinatario.setTxtEmail(txtEmail[i]);
				    myDestinatario.setTxtTitulo(txtTitulo[i]);		
				    destinatarios.add(myDestinatario); 	
				}
				if( DestinatarioBean.isInsertDestinatario(destinatarios) )
					DestinatarioBean.setMsgErro("Dados Alterados com Sucesso.");
				atualizarDependente ="S"; 
				req.setAttribute("destinatarios",destinatarios);
			}
			else DestinatarioBean.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
			List destinatarios = DestinatarioBean.getDestinatarios(DestinatarioBean);
			req.setAttribute("destinatarios",destinatarios);
			nextRetorno = "/GER/CadastraDestinatarioImp.jsp";
	    }
	    
	    req.setAttribute("atualizarDependente",atualizarDependente);
		req.setAttribute("OrgaoId",OrgaoId) ;		 
		req.setAttribute("DestinatarioBean",DestinatarioBean) ;
    }
    catch (Exception ue) {
      throw new sys.CommandException("CadastraDestinatarioCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
 
  private Vector isParametros(String[] codDestinatario,String[] txtDestinatario,String[] txtEmail) {
		Vector vErro = new Vector() ;		 
		if ((codDestinatario.length!=txtDestinatario.length) || (txtDestinatario.length!=txtEmail.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 

}