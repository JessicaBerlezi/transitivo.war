package TAB;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.servlet.http.HttpServletRequest;

/*
 * 
 * Created on 14/06/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Administrador
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LogCargaCmd extends sys.Command {

	private static final String jspPadrao = "/TAB/logCarga.jsp";
	private String next;
	private String arquivos;

	public LogCargaCmd() {
		next = jspPadrao;
	}

	public LogCargaCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {

			LogCargaBean logCargaId = new LogCargaBean();
			// obtem e valida os parametros recebidos		
			String linha;
			String texto = "";
			String acao = req.getParameter("acao");
			if (acao == null)
				acao = " ";
			String arquivo = req.getParameter("arquivo");
			if (arquivo == null)
				acao = " ";

			if (acao == "L") {
				logCargaId.setNomArq(arquivo);
				BufferedReader arq =
					new BufferedReader(new FileReader(arquivo));

				while ((linha = arq.readLine()) != null) {
					texto += linha + "\n";
				}
				logCargaId.setLinhas(texto);			

			} else {
				File fil = new File("c:\\smit\\log\\");
				String [] arqs = fil.list();
				String arquivos = "";
				for (int i=0; i < arqs.length ; i++) {
					String nome = arqs[i].toUpperCase();
					if (arqs[i].toUpperCase().indexOf(".LOG") >= 0) {
						arquivos += ",\"" + arqs[i] +"\"";
					}
				}
				arquivos = arquivos.substring(1,arquivos.length());
				logCargaId.setArquivos(arquivos);
			}
			req.setAttribute("LogCargaId", logCargaId);
		} catch (Exception ue) {
			throw new sys.CommandException(
				"LogCargaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}

}
