/*
 * Created on 14/06/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package TAB;

/**
 * @author Administrador
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class LogCargaBean {

	private String arquivos;
	private String linhas;
	private String msgErro;
	private String nomArq;
	
	public LogCargaBean() {

	}

	public String getNomArq() {
		return this.nomArq;
	}

	public void setNomArq(String nomArq) {
		this.nomArq = nomArq;
	}

	public String getLinhas() {
		return this.linhas;
	}

	public void setLinhas(String linhas) {
		this.linhas = linhas;
	}

	public String getArquivos() {
		return this.arquivos;
	}

	public void setArquivos(String arquivos) {
		this.arquivos = arquivos;
	}
	public void setMsgErro(String msgErro)   {
		  this.msgErro = msgErro ;
	 }   
	public String getMsgErro()   {
	  return this.msgErro;
	 }     

}
