package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Munic�pios<br>
* <b>Description:</b>  	Bean dados dos Munic�pios - Cole��o de Munic�pios<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class MunicipioBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private MunicipioBean auxClasse;

	public MunicipioBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(MunicipioBean classe) {

		if (classe == null)
			classe = new MunicipioBean();
		colClasse.put(classe.getCodMunicipio(), classe);

	}

	public MunicipioBean getItem(String codInt) {
		return (MunicipioBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarMunicipios(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Munic�pios : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((MunicipioBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}

}
