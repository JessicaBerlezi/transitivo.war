package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Categoria <br>
* <b>Description:</b>  Registro - Carrega Tabela Categoria<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class CategoriaCmd extends sys.Command {

	public CategoriaCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Categorias  ----------");
		try {
			//Declaracao de variaveis
			String codCategoria = "";
			String dscCategoria = "";

			String codRetorno = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "   ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				CategoriaBean especieId = new CategoriaBean();

				//Chamar  a Transa��o
				resultTrans = daoBroker.Transacao126("999","5", indContinua, conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 89; j++) {

							if ((!linha.substring(j * 15, j * 15 + 3).trim().equals("")) 
								&& (!linha.substring(j * 15, j * 15 + 3).trim().equals("000"))) {
								codCategoria =
									linha.substring((15 * j), (15 * j) + 3);
								dscCategoria =
									linha.substring(
										(15 * j) + 3,
										(15 * j) + 15);
								especieId.setCodCategoria(codCategoria);
								especieId.setDscCategoria(dscCategoria);
								especieId.verificaOperacao(conn);
							}
							else continua = false;

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(1335, 1338);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000"))) {
							continua = false;
						}
					}
				}
				totalIns = especieId.getTotalIns();
				totalUpd = especieId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Categorias    ----------");
		return ok;
	}
}