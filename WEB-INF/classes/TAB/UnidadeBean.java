package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Unidade<br>
* <b>Description:</b>  	Bean dados das Unidades - Tabela de Unidades<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class UnidadeBean extends sys.HtmlPopupBean {

	private String codUnidade;
	private String dscUnidade;
	private String codOrgao;
	private String seqEndereco;
	
	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public UnidadeBean() {

		super();
		setTabela("TSMI_UNIDADE");
		setPopupWidth(35);
		codUnidade = "";
		dscUnidade = "";
		seqEndereco = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}


	public void setCodUnidade(String codUnidade) {
		this.codUnidade = codUnidade;
		if (codUnidade == null)
			this.codUnidade = "";
	}

	public String getCodUnidade() {
		return this.codUnidade;
	}

	public void setDscUnidade(String dscUnidade) {
		this.dscUnidade = dscUnidade;
		if (dscUnidade == null)
			this.dscUnidade = "";
	}

	public String getDscUnidade() {
		return this.dscUnidade;
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}

	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setSeqEndereco(String seqEndereco) {
		this.seqEndereco = seqEndereco;
		if (seqEndereco == null)	this.seqEndereco = "";
	}
	public String getSeqEndereco() {
		return this.seqEndereco.trim();
	}
	
	
	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
	
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}


	/**
	 * M�todo para inserir a unidade de auto na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertUnidade() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodUnidade() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UnidadeExiste(this) == true) {
					vErro.addElement(
						"Unidade j� est� cadastrada: "
							+ this.getCodUnidade()
							+ " - "
							+ this.getDscUnidade()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Unidade: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUnidade() == null) {
			vErro.addElement("C�digo da Unidade n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscUnidade() == null) {
			vErro.addElement("Descri��o da Unidade n�o preenchida" + " \n");
			bOk = false;
		}

		if (this.getCodOrgao() == null) {
			vErro.addElement("C�digo do �rg�o n�o preenchido" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.UnidadeInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Unidade n�o inclu�da: "
						+ this.getCodUnidade()
						+ " - "
						+ this.getDscUnidade()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Unidade na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a unidade existe.
	 */

	public boolean isUpdateUnidade() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodUnidade() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UnidadeExiste(this) == false) {
					vErro.addElement(
						"Unidade n�o existe: "
							+ this.getCodUnidade()
							+ " - "
							+ this.getDscUnidade()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Unidade: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUnidade() == null) {
			vErro.addElement("C�digo da Unidade n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.UnidadeUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Unidade n�o atualizada: " + this.getCodUnidade() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a Unidade da base de dados
	 * Valida se o Unidade existe.
	 */

	public boolean isDeleteUnidade() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodUnidade() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UnidadeExiste(this) == false) {
					vErro.addElement(
						"Unidade n�o existe: "
							+ this.getCodUnidade()
							+ " - "
							+ this.getDscUnidade()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Unidade: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUnidade() == null) {
			vErro.addElement("C�digo da Unidade n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.UnidadeDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Unidade n�o excluida: " + this.getCodUnidade() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	public void verificaUnidade(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.UnidadeExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertUnidade();
			  setTotalIns(totalIns);
			}
			else {
			  totalAtu = contador(totalAtu); 
			  isUpdateUnidade();
			  setTotalAtu(totalAtu);
			} 
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }
	
	public int contador (int cont){
		cont += 1; 
		return cont;
	}

}
