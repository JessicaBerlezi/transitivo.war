package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Tipos de Ve�culo <br>
* <b>Description:</b>  Registro - Carrega Tabela tipos de Ve�culo<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class TipoCmd extends sys.Command {
	public TipoCmd() {

	}
	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Tipos  ----------");
		System.out.println("Processando Tipos");

		try {
			//Declaracao de variaveis
			String codTipo = "";
			String dscTipo = "";

			String codRetorno = "";
			String indCont = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "   ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia do bean 
				TipoBean TipoId = new TipoBean();

				// Chamar a transa��o
				resultTrans = daoBroker.Transacao122("999","5","   ", conn);

				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						continua = false;
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 89; j++) {

							if ( (!linha.substring(21 * j, (21 * j) + 3).trim().equals("")) 
								&& (!linha.substring(21 * j, (21 * j) + 3).trim().equals("000"))){
							
								codTipo =
									linha.substring((21 * j), (21 * j) + 3);
								dscTipo =
									linha.substring(
										(21 * j) + 3,
										(21 * j) + 21);
								TipoId.setCodTipo(codTipo);
								TipoId.setDscTipo(dscTipo);
								TipoId.verificaTipo(conn);
							}

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(1871,1874);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000"))) {
							continua = false;
						}
					}
				}
				totalIns = TipoId.getTotalIns();
				totalUpd = TipoId.getTotalAtu();
			}
		} catch (Exception e) {
			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Tipos    ----------");
		return ok;
	}
}