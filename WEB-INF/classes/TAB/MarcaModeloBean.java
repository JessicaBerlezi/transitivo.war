package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de MarcaModelo de Carro<br>
* <b>Description:</b>  	Bean dados das Cores - Tabela de Cores de Carros<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class MarcaModeloBean extends sys.HtmlPopupBean {

	private String codMarcaModelo;
	private String dscMarcaModelo;

	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public MarcaModeloBean() {

		super();
		setTabela("TSMI_Marca_Modelo");
		setPopupWidth(35);
		codMarcaModelo = "";
		dscMarcaModelo = "";

		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	public void setConn(Connection conn) {
		this.conn = conn;
	}

	public Connection getConn() {
		return this.conn;
	}

	public void setCodMarcaModelo(String codMarcaModelo) {
		this.codMarcaModelo = codMarcaModelo;
		if (codMarcaModelo == null)
			this.codMarcaModelo = "";
	}

	public String getCodMarcaModelo() {
		return this.codMarcaModelo;
	}

	public void setDscMarcaModelo(String dscMarcaModelo) {
		this.dscMarcaModelo = dscMarcaModelo;
		if (dscMarcaModelo == null)
			this.dscMarcaModelo = "";
	}

	public String getDscMarcaModelo() {
		return this.dscMarcaModelo;
	}
	
	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}
	

	/**
	 * M�todo para inserir a cor do carro na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertMarcaModelo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodMarcaModelo() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MarcaModeloExiste(this) == true) {
					vErro.addElement(
						"Cor j� est� cadastrada: "
							+ this.getCodMarcaModelo()
							+ " - "
							+ this.getDscMarcaModelo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		if (this.getCodMarcaModelo() == null) {
			vErro.addElement("C�digo da Cor n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscMarcaModelo() == null) {
			vErro.addElement("Descri��o da Cor n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.MarcaModeloInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Cor n�o inclu�da: "
						+ this.getCodMarcaModelo()
						+ " - "
						+ this.getDscMarcaModelo()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Cor na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a cor existe.
	 */

	public boolean isUpdateMarcaModelo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodMarcaModelo() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MarcaModeloExiste(this) == false) {
					vErro.addElement(
						"Cor n�o existe: "
							+ this.getCodMarcaModelo()
							+ " - "
							+ this.getDscMarcaModelo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodMarcaModelo() == null) {
			vErro.addElement("C�digo da Cor n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.MarcaModeloUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Cor n�o atualizada: " + this.getCodMarcaModelo() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a Cor da base de dados
	 * Valida se a Cor existe.
	 */

	public boolean isDeleteMarcaModelo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodMarcaModelo() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MarcaModeloExiste(this) == false) {
					vErro.addElement(
						"Cor n�o existe: "
							+ this.getCodMarcaModelo()
							+ " - "
							+ this.getDscMarcaModelo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodMarcaModelo() == null) {
			vErro.addElement("C�digo da Cor n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.MarcaModeloDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Cor n�o excluida: " + this.getCodMarcaModelo() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	public void verificaMarcaModelo(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.MarcaModeloExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertMarcaModelo();
			  setTotalIns(totalIns);
			}
			else {
			  totalAtu = contador(totalAtu); 
			  isUpdateMarcaModelo();
			  setTotalAtu(totalAtu);
			} 
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }
	
	public int contador (int cont){
		cont += 1; 
		return cont;
	}

}
