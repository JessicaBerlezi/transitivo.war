package TAB;

/**
* <b>Title:</b>        Controle de Acesso - Acesso a Base de Dados - Exception<br>
* <b>Description:</b>  Dao - Acesso a Base de Dados<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class DaoException extends Exception {
  
  public DaoException() {
    super();
  }

  public DaoException(String msg) {  
    super(msg); 
  }
}
