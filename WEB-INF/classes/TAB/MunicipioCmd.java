package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Municipio <br>
* <b>Description:</b>  Registro - Carrega Tabela Municipio<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class MunicipioCmd extends sys.Command {
	public MunicipioCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {
		
		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Municipios  ----------");
		System.out.println("Processando Municipios");
		
		try {
			//Declaracao de variaveis
			String codMunicipio = "";
			String dscMunicipio = "";
			String codUF;

			String codRetorno = "";
			String indCont = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "     ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				MunicipioBean MunicipioId = new MunicipioBean();

				//Chamar a transa��o
				resultTrans = daoBroker.Transacao123("99999","99","5",indContinua, conn);

				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						continua = false;
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 55; j++) {

							if ((!linha.substring(j * 57, j * 57 + 5).trim().equals("")) 
								&& (!linha.substring(j * 57, j * 57 + 5).trim().equals("00000"))) {
								codMunicipio =
									linha.substring((57 * j), (57 * j) + 5);
								dscMunicipio =
									linha.substring(
										(57 * j) + 5,
										(57 * j) + 5 + 50);
								codUF = linha.substring((57 * j) + 5 + 50 , (57 * j) + 5 + 50 + 2);
								MunicipioId.setCodMunicipio(codMunicipio);
								MunicipioId.setNomMunicipio(dscMunicipio);
								MunicipioId.setCodUF(codUF);
								//Temporario para n�o atualizar o munic�pio 5819
								if (!codMunicipio.equals("5819"))
								MunicipioId.verificaOperacao(conn);
							}

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(55*57,55*57+5);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("00000"))) {
							continua = false;
						}
					}
				}
				totalIns = MunicipioId.getTotalIns();
				totalUpd = MunicipioId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Municipios    ----------");
		return ok;
	}
}