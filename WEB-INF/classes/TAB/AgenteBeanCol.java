package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Agentes<br>
* <b>Description:</b>  	Bean dados dos Agentes - Cole��o de Agentes<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class AgenteBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private AgenteBean auxClasse;

	public AgenteBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(AgenteBean classe) {

		if (classe == null)
			classe = new AgenteBean();
		colClasse.put(classe.getCodAgente(), classe);

	}

	public AgenteBean getItem(String codInt) {
		return (AgenteBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse =
				dao.BuscarAgentes(tipoCarga,vCod,vNom);

		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Agentes : "
					+ e.getMessage()+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {
			addItem((AgenteBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {
		return new Vector(colClasse.values());

	}

}
