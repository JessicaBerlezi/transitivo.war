package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Infra��es<br>
* <b>Description:</b>  	Bean dados dos Infra��es - Cole��o de Infra��es<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class InfracaoBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private InfracaoBean auxClasse;

	public InfracaoBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(InfracaoBean classe) {

		if (classe == null)
			classe = new InfracaoBean();
		colClasse.put(classe.getCodInfracao(), classe);

	}

	public InfracaoBean getItem(String codInt) {
		return (InfracaoBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom,
	  Vector vEqua,Vector vVal,Vector vNum,Vector vComp,Vector vIndCond,
	  Vector vIndSusp,Vector vDscGrav,Vector vValDesc){
	
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try { 
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarInfracoes(tipoCarga,vCod,vNom,vEqua,vVal,vNum,vComp,
			vIndCond,vIndSusp,vDscGrav,vValDesc);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Infra��es : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((InfracaoBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}

}
