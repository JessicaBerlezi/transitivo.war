package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Agentes<br>
* <b>Description:</b>  	Bean dados das Agentes - Tabela de Agentes<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class AgenteBean extends sys.HtmlPopupBean {

	private String codAgente;
	private String nomAgente;

	private Connection conn;
	private int totalReg;
	private int totalIns;
	private int totalAtu;

	public AgenteBean() {

		super();
		setTabela("TSMI_AGENTE");
		setPopupWidth(35);
		codAgente = "";
		nomAgente = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodAgente(String codAgente) {
		this.codAgente = codAgente;
		if (codAgente == null)
			this.codAgente = "";
	}

	public String getCodAgente() {
		return this.codAgente;
	}

	public void setNomAgente(String nomAgente) {
		this.nomAgente = nomAgente;
		if (nomAgente == null)
			this.nomAgente = "";
	}

	public String getNomAgente() {
		return this.nomAgente;
	}
	
	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}

	/**
	 * M�todo para inserir agente na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertCor() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodAgente() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.AgenteExiste(this) == true) {
					vErro.addElement(
						"Agente j� est� cadastrado: "
							+ this.getCodAgente()
							+ " - "
							+ this.getNomAgente()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Agente: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		if (this.getCodAgente() == null) {
			vErro.addElement("C�digo da Categoria n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getNomAgente() == null) {
			vErro.addElement("Descri��o da Categoria n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.AgenteInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Categoria n�o inclu�da: "
						+ this.getCodAgente()
						+ " - "
						+ this.getNomAgente()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para atualizar os dados do Agente na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o Agente existe.
	 */

	/**
	 * M�todo para atualizar os dados da Categoria na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a categoria existe.
	 */

	public boolean isUpdateAgente() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodAgente() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.AgenteExiste(this) == false) {
					vErro.addElement(
						"Agente n�o existe: "
							+ this.getCodAgente()
							+ " - "
							+ this.getNomAgente()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Agente: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodAgente() == null) {
			vErro.addElement("C�digo do Agente n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.AgenteUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Agente n�o atualizado: " + this.getCodAgente() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover o Agente da base de dados
	 * Valida se a Agente existe.
	 */

	public boolean isDeleteAgente() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodAgente() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.AgenteExiste(this) == false) {
					vErro.addElement(
						"Agente n�o existe: "
							+ this.getCodAgente()
							+ " - "
							+ this.getNomAgente()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Agente: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodAgente() == null) {
			vErro.addElement("C�digo do Agente n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.AgenteDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Agente n�o excluido: " + this.getCodAgente() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaAgente(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.AgenteExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertCor();
			  setTotalIns(totalIns);
			}
			else {
			  totalAtu = contador(totalAtu); 
			  isUpdateAgente();
			  setTotalAtu(totalAtu);
			}   
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }

	 
	public int contador (int cont){
		cont += 1; 
		return cont;
	}
}
