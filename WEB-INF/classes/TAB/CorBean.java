package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Cor de Carro<br>
* <b>Description:</b>  	Bean dados das Cores - Tabela de Cores de Carros<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class CorBean extends sys.HtmlPopupBean {

	private String codCor;
	private String dscCor;

	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public CorBean() {

		super();
		setTabela("TSMI_COR");
		setPopupWidth(35);
		codCor = "";
		dscCor = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodCor(String codCor) {
		this.codCor = codCor;
		if (codCor == null)
			this.codCor = "";
	}

	public String getCodCor() {
		return this.codCor;
	}

	public void setDscCor(String dscCor) {
		this.dscCor = dscCor;
		if (dscCor == null)
			this.dscCor = "";
	}

	public String getDscCor() {
		return this.dscCor;
	}

	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}
	

	/**
	 * M�todo para inserir a cor do carro na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertCor() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodCor() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CorExiste(this) == true) {
					vErro.addElement(
						"Cor j� est� cadastrada: "
							+ this.getCodCor()
							+ " - "
							+ this.getDscCor()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCor() == null) {
			vErro.addElement("C�digo da Cor n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscCor() == null) {
			vErro.addElement("Descri��o da Cor n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.CorInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Cor n�o inclu�da: "
						+ this.getCodCor()
						+ " - "
						+ this.getDscCor()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Cor na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a cor existe.
	 */

	public boolean isUpdateCor() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodCor() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CorExiste(this) == false) {
					vErro.addElement(
						"Cor n�o existe: "
							+ this.getCodCor()
							+ " - "
							+ this.getDscCor()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCor() == null) {
			vErro.addElement("C�digo da Cor n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.CorUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Cor n�o atualizada: " + this.getCodCor() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a Cor da base de dados
	 * Valida se a Cor existe.
	 */

	public boolean isDeleteCor() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodCor() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CorExiste(this) == false) {
					vErro.addElement(
						"Cor n�o existe: "
							+ this.getCodCor()
							+ " - "
							+ this.getDscCor()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Cor: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCor() == null) {
			vErro.addElement("C�digo da Cor n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.CorDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Cor n�o excluida: " + this.getCodCor() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaCor(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.CorExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertCor();     
			  setTotalIns(totalIns);
			}
			else {
			  totalAtu = contador(totalAtu); 
			  isUpdateCor();
			  setTotalAtu(totalAtu);
			} 
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }
	
	public int contador (int cont){
		cont += 1; 
		return cont;
	}

}
