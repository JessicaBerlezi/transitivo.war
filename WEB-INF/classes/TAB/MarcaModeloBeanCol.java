package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de _Marca_Modelo<br>
* <b>Description:</b>  	Bean dados das Marcas - Cole��o de Marcas<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class MarcaModeloBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private MarcaModeloBean auxClasse;

	public MarcaModeloBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(MarcaModeloBean classe) {

		if (classe == null)
			classe = new MarcaModeloBean();
		colClasse.put(classe.getCodMarcaModelo(), classe);

	}

	public MarcaModeloBean getItem(String codInt) {
		return (MarcaModeloBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarMarcaModelo(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Cores : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((MarcaModeloBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}

}
