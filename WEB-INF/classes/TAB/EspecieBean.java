package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Esp�cie de Ve�culo<br>
* <b>Description:</b>  	Bean dados das Esp�cies - Tabela de Esp�cie de Ve�culos<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class EspecieBean extends sys.HtmlPopupBean {

	private String codEspecie;
	private String dscEspecie;
	
	private Connection conn;
	private int totalReg;
	private int totalIns;
	private int totalAtu;


	public EspecieBean() {

		super();
		setTabela("TSMI_ESPECIE");
		setPopupWidth(35);
		codEspecie = "";
		dscEspecie = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodEspecie(String codEspecie) {
		this.codEspecie = codEspecie;
		if (codEspecie == null)
			this.codEspecie = "";
	}

	public String getCodEspecie() {
		return this.codEspecie;
	}

	public void setDscEspecie(String dscEspecie) {
		this.dscEspecie = dscEspecie;
		if (dscEspecie == null)
			this.dscEspecie = "";
	}

	public String getDscEspecie() {
		return this.dscEspecie;
	}
	
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}

	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}

	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}


	/**
	 * M�todo para inserir a esp�cie do ve�culo na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertEspecie() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodEspecie() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.EspecieExiste(this) == true) {
					vErro.addElement(
						"Especie j� est� cadastrada: "
							+ this.getCodEspecie()
							+ " - "
							+ this.getDscEspecie()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Especie: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodEspecie() == null) {
			vErro.addElement("C�digo da Especie n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscEspecie() == null) {
			vErro.addElement("Descri��o da Especie n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.EspecieInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Especie n�o inclu�da: "
						+ this.getCodEspecie()
						+ " - "
						+ this.getDscEspecie()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Especie na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a Esp�cie existe.
	 */

	public boolean isUpdateEspecie() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodEspecie() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.EspecieExiste(this) == false) {
					vErro.addElement(
						"Especie n�o existe: "
							+ this.getCodEspecie()
							+ " - "
							+ this.getDscEspecie()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Especie: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodEspecie() == null) {
			vErro.addElement("C�digo da Especie n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.EspecieUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Especie n�o atualizada: " + this.getCodEspecie() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a Especie da base de dados
	 * Valida se a Especie existe.
	 */

	public boolean isDeleteEspecie() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodEspecie() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.EspecieExiste(this) == false) {
					vErro.addElement(
						"Especie n�o existe: "
							+ this.getCodEspecie()
							+ " - "
							+ this.getDscEspecie()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Especie: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodEspecie() == null) {
			vErro.addElement("C�digo da Especie n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.EspecieDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Especie n�o excluida: " + this.getCodEspecie() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	public void verificaOperacao(Connection conn) throws sys.BeanException {
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.EspecieExiste(this) == false) {
				isInsertEspecie();
				totalIns = contador(totalIns);
				setTotalIns(totalIns);
			} 
			else {
				isUpdateEspecie();
				totalAtu = contador(totalAtu);
				setTotalAtu(totalAtu);
			}  
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
	 
	}
	public int contador(int cont) {
		cont += 1;
		return cont;
	}
 

}
