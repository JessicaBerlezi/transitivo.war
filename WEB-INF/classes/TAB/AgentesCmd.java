package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Agente <br>
* <b>Description:</b>  Registro - Carrega Tabela Agente<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class AgentesCmd extends sys.DaoBase {

	public AgentesCmd() throws sys.DaoException{
		super();
	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException { 

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Agentes  ----------");
//		System.out.println("Processando Agentes");

		try {
			//Declaracao de variaveis
			String codMunicipio = "";
			String dscMunicipio = "";

			String codRetorno = "";
			String indCont = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "  ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				AgenteBean agenteId = new AgenteBean();

				//Chamar  a Transa��o
				resultTrans = daoBroker.Transacao084("99","5",indContinua,"  ", conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						continua = false;
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 146; j++) {

							String codAgente = this.getPedaco(linha,2," "," ");

							agenteId.setCodAgente(codAgente);
							agenteId.setNomAgente(getPedaco(linha,20," "," "));

							if ( (!codAgente.trim().equals("")) 
								&& (!codAgente.trim().equals("00"))){

								agenteId.verificaAgente(conn);
							}
							else break;

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(3212,3214);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("00"))) {
							continua = false;
						}
					}
				}
				totalIns = agenteId.getTotalIns();
				totalUpd = agenteId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_INFO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Agentes    ----------");
		return ok;
	}
}