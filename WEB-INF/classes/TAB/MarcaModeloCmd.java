package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Marca Modelo <br>
* <b>Description:</b>  Registro - Carrega Tabela Marca Modelo<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class MarcaModeloCmd extends sys.Command {
	public MarcaModeloCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Marca/Modelo  ----------");
		System.out.println("Processando Marca/Modelo");

		try {
			//Declaracao de variaveis
			String codMarcaModelo = "";
			String dscMarcaModelo = "";

			String codRetorno = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "      ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				MarcaModeloBean MarcaModeloId = new MarcaModeloBean();

				// Chamar a transa��o
				resultTrans = daoBroker.Transacao121("999999","5", indContinua, conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						continua = false;
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 89; j++) {

							if ((!linha.substring(j * 31, j * 31 + 6).trim().equals("")) 
								&& (!linha.substring(j * 31, j * 31 + 6).trim().equals("000000"))){
								codMarcaModelo =
									linha.substring((31 * j), (31 * j) + 6);
								dscMarcaModelo =
									linha.substring(
										(31 * j) + 6,
										(31 * j) + 31);
								MarcaModeloId.setCodMarcaModelo(codMarcaModelo);
								MarcaModeloId.setDscMarcaModelo(dscMarcaModelo);
								MarcaModeloId.verificaMarcaModelo(conn);
							}

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(2759,2765);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000000"))) {
							continua = false;
						}
					}
				}
				totalIns = MarcaModeloId.getTotalIns();
				totalUpd = MarcaModeloId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Marca/Modelo    ----------");
		return ok;
	}
}