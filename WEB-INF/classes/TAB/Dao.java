package TAB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;
/**
* <b>Title:</b>        SMIT - Massa de Dados<br>
* <b>Description:</b>  Dao - Tabelas<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class Dao {

	private static final String MYABREVSIST = "TAB";
	private static Dao instance;
	private sys.ServiceLocator serviceloc;

	public static Dao getInstance() throws sys.DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}
	//	 **************************************************************************************
	private Dao() throws sys.DaoException {
		try {
			serviceloc = sys.ServiceLocator.getInstance();
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}

	
	
	/**
	*-----------------------------------------------------------
	* DAO relativo � AgentesBean
	*-----------------------------------------------------------
	*/
	public boolean AgenteExiste(AgenteBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_agente =" + auxClasse.getCodAgente();
			String sCmd = "SELECT COD_agente from TSMI_AGENTE " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}


	public void AgenteInsert(AgenteBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_AGENTE (cod_agente,nom_agente)";
			sCmd += "VALUES (" + auxClasse.getCodAgente() + ", ";
			sCmd += "'" + auxClasse.getNomAgente() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}


	public void AgenteUpdate(AgenteBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_AGENTE set ";
			sCmd += "nom_agente = '" + auxClasse.getNomAgente() + "'";
			sCmd += " where cod_agente = " + auxClasse.getCodAgente();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}
	
	public boolean AgenteDelete(AgenteBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_AGENTE ";
			sCmd += " where cod_agente = " + auxClasse.getCodAgente();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarAgentes(String tipoCarga,Vector vCod,Vector vNom)
		throws DaoException {

		boolean existe = false;
		String auxCond = " where ";
		String auxCod = " cod_agente in (";
		String auxNom = " nom_agente like '%";
		String auxCodInt = " cod_agente_int in (";
		String auxCodUnidade = " cod_unidade in (";

		AgenteBean relClasse;
		Vector vRetClasse = new Vector();

		if (vCod.size() != 0) {
			auxCond += auxCod;
			for (int i = 0; i < vCod.size(); i++) {
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}

		if (vNom.size() != 0) {
			if (vCod.size() != 0) auxCond += " and (";
			for (int i = 0; i < vNom.size(); i++) {
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}

		if (tipoCarga == "TODOS") auxCond = "";

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT cod_agente, nom_agente from TSMI_AGENTE "
					+ auxCond;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				relClasse = new AgenteBean();
				relClasse.setCodAgente(rs.getString("cod_agente"));
				relClasse.setNomAgente(rs.getString("nom_agente"));
				vRetClasse.addElement(relClasse);
				existe = true;

			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � CategoriaBean
	*-----------------------------------------------------------
	*/

	public boolean CategoriaExiste(CategoriaBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where =
				"WHERE cod_Categoria = " + auxClasse.getCodCategoria();
			String sCmd = "SELECT COD_CATEGORIA from TSMI_CATEGORIA " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void CategoriaInsert(CategoriaBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd =
				"INSERT INTO TSMI_CATEGORIA (cod_categoria,dsc_categoria)";
			sCmd += "VALUES (" + auxClasse.getCodCategoria() + ", ";
			sCmd += "'" + auxClasse.getDscCategoria() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

 
	public void CategoriaUpdate(CategoriaBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_CATEGORIA set ";
			sCmd += "dsc_categoria = '" + auxClasse.getDscCategoria() + "'";
			sCmd += " where cod_categoria = '"
				+ auxClasse.getCodCategoria()
				+ "'";
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}
	

	public boolean CategoriaDelete(CategoriaBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_CATEGORIA ";
			sCmd += " where cod_categoria = " + auxClasse.getCodCategoria();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarCategorias(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_categoria in (";
		String auxNom 		= " dsc_categoria like '%";
		String auxCodInt	= " cod_categoria_int in (";
		CategoriaBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0){
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++){
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0) auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_categoria, dsc_categoria from TSMI_CATEGORIA " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new CategoriaBean();
				relClasse.setCodCategoria(rs.getString("cod_categoria"));
				relClasse.setDscCategoria(rs.getString("dsc_categoria"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � CorBean
	*-----------------------------------------------------------
	*/

	public boolean CorExiste(CorBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}

			Statement stmt = conn.createStatement();
			String where = "WHERE cod_cor = " + auxClasse.getCodCor();
			String sCmd = "SELECT COD_COR from TSMI_COR " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void CorInsert(CorBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_COR (cod_cor,dsc_cor)";
			sCmd += "VALUES ('" + auxClasse.getCodCor() + "', ";
			sCmd += "'" + auxClasse.getDscCor() + "') ";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	
	public void CorUpdate(CorBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_COR set ";
			sCmd += "dsc_cor = '" + auxClasse.getDscCor() + "'";
			sCmd += " where cod_cor = '" + auxClasse.getCodCor() + "'";
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean CorDelete(CorBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_COR ";
			sCmd += " where cod_cor = " + auxClasse.getCodCor();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarCores(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_cor in (";
		String auxNom 		= " ds_cor like '%";
		String auxCodInt	= " cod_cor_int in (";
		CorBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0){
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++){
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1)  auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0) auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_cor, dsc_cor from TSMI_COR " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new CorBean();
				relClasse.setCodCor(rs.getString("cod_cor"));
				relClasse.setDscCor(rs.getString("dsc_cor"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � EspecieBean
	*-----------------------------------------------------------
	*/

	public boolean EspecieInsert(EspecieBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd;
			sCmd = "INSERT INTO TSMI_ESPECIE (cod_especie,dsc_especie) ";
			sCmd += "VALUES (" + auxClasse.getCodEspecie() + ", ";
			sCmd += "'" + auxClasse.getDscEspecie() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;

			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EspecieUpdate(EspecieBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_ESPECIE set ";
			sCmd += "dsc_especie = '" + auxClasse.getDscEspecie() + "'";
			sCmd += " where cod_especie =" + auxClasse.getCodEspecie();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EspecieDelete(EspecieBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_ESPECIE ";
			sCmd += " where cod_especie = " + auxClasse.getCodEspecie();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EspecieExiste(EspecieBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_especie = " + auxClasse.getCodEspecie();
			String sCmd = "SELECT COD_ESPECIE from TSMI_ESPECIE " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarEspecies(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_especie in (";
		String auxNom 		= " dsc_especie like '%";
		String auxCodInt	= " cod_especie_int in (";
		EspecieBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0) {
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++){
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1)	auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0)	auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1)  auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_especie, dsc_especie from TSMI_ESPECIE " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new EspecieBean();
				relClasse.setCodEspecie(rs.getString("cod_especie"));
				relClasse.setDscEspecie(rs.getString("dsc_especie"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � InfracaoBean
	*-----------------------------------------------------------
	*/

	public boolean InfracaoExiste(InfracaoBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where =
				"WHERE cod_infracao =" + auxClasse.getCodInfracao();
			String sCmd = "SELECT COD_infracao from TSMI_INFRACAO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void InfracaoInsert(InfracaoBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd =
				"INSERT INTO TSMI_INFRACAO (cod_infracao,dsc_infracao,dsc_enquadramento,"
					+ "val_real,num_ponto, dsc_competencia, ind_cond_prop, ind_suspensao)";
			sCmd += "VALUES ('" + auxClasse.getCodInfracao() + "', ";
			sCmd += "'" + auxClasse.getDscInfracao() + "',";
			sCmd += "'" + auxClasse.getDscEnquadramento().trim() + "',";
			sCmd += auxClasse.getValReal() + ",";
			sCmd += auxClasse.getNumPonto() + ",";
			sCmd += "'" + auxClasse.getDscCompetencia() + "',";
			sCmd += "'" + auxClasse.getIndCondProp() + "',";
			sCmd += "'" + auxClasse.getIndSuspensao() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}


	public void InfracaoUpdate(InfracaoBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_INFRACAO set ";
			sCmd += "dsc_infracao = '" + auxClasse.getDscInfracao() + "', ";
			sCmd += "dsc_enquadramento = '" + auxClasse.getDscEnquadramento().trim() + "', ";
			sCmd += "val_real = " + auxClasse.getValReal() + ", ";
			sCmd += "num_ponto = " + auxClasse.getNumPonto() + ", ";
			sCmd += "dsc_competencia = '" + auxClasse.getDscCompetencia() + "', ";
			sCmd += "ind_cond_prop = '" + auxClasse.getIndCondProp() + "', ";
			sCmd += "ind_suspensao = '" + auxClasse.getIndSuspensao() + "' ";
//			sCmd += "dsc_gravidade = '" + auxClasse.getDscGravidade + "', ";
			sCmd += " where cod_infracao = " + auxClasse.getCodInfracao();
				
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean InfracaoDelete(InfracaoBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_INFRACAO ";
			sCmd += " where cod_infracao = " + auxClasse.getCodInfracao();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarInfracoes(String tipoCarga,Vector vCod,Vector vNom,
	  Vector vEqua,Vector vVal,Vector vNum,Vector vComp,Vector vIndCond,
	  Vector vIndSusp,Vector vDscGrav,Vector vValDesc) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_infracao in (";
		String auxNom 		= " dsc_infracao like '%";
		String auxCodInt	= " cod_infracao_int in (";
		InfracaoBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0){
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++){
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0)  auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt  = conn.createStatement();
			String sCmd = "SELECT cod_infracao, dsc_infracao, dsc_enquadramento, " +
				  "val_real, num_ponto, dsc_competencia, ind_cond_prop, ind_suspensao," +
				  "dsc_gravidade, val_real_desconto from TSMI_INFRACAO " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new InfracaoBean();
				relClasse.setCodInfracao(rs.getString("cod_infracao"));
				relClasse.setDscInfracao(rs.getString("dsc_infracao"));
				relClasse.setDscEnquadramento(rs.getString("dsc_enquadramento"));
				relClasse.setValReal(rs.getString("val_real"));
				relClasse.setNumPonto(rs.getString("num_ponto"));
				relClasse.setDscCompetencia(rs.getString("dsc_competencia"));
				relClasse.setIndCondProp(rs.getString("ind_cond_prop"));
				relClasse.setIndSuspensao(rs.getString("ind_suspensao"));
				relClasse.setDscGravidade(rs.getString("dsc_gravidade"));
				relClasse.setValRealDesconto(rs.getString("val_real_desconto"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}


	/**
	*-----------------------------------------------------------
	* DAO relativo � MarcaModeloBean
	*-----------------------------------------------------------
	*/

	public boolean MarcaModeloExiste(MarcaModeloBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where =
				"WHERE cod_marca_modelo ="
					+ auxClasse.getCodMarcaModelo();
			String sCmd =
				"SELECT COD_marca_modelo from TSMI_MARCA_MODELO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void MarcaModeloInsert(MarcaModeloBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd =
				"INSERT INTO TSMI_MARCA_MODELO (cod_marca_modelo,dsc_marca_modelo)";
			sCmd += "VALUES (" + auxClasse.getCodMarcaModelo() + ", ";
			sCmd += "'" + auxClasse.getDscMarcaModelo() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}


	public void MarcaModeloUpdate(MarcaModeloBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_MARCA_MODELO set ";
			sCmd += "dsc_marca_modelo = '"
				+ auxClasse.getDscMarcaModelo()
				+ "'";
			sCmd += " where cod_marca_modelo = '"
				+ auxClasse.getCodMarcaModelo()
				+ "'";
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean MarcaModeloDelete(MarcaModeloBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_MARCA_MODELO ";
			sCmd += " where cod_marca_modelo = " + auxClasse.getCodMarcaModelo();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public Vector BuscarMarcaModelo(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_Marca_Modelo in (";
		String auxNom 		= " dsc_Marca_Modelo like '%";
		String auxCodInt	= " cod_Marca_Modelo_int in (";
		MarcaModeloBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0) {
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++)	{
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0) {
			if (vCod.size() != 0) auxCond += " and (";
			for (int i=0;i < vNom.size(); i++) {
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_Marca_Modelo, dsc_Marca_Modelo from TSMI_Marca_Modelo " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new MarcaModeloBean();
				relClasse.setCodMarcaModelo(rs.getString("cod_Marca_Modelo"));
				relClasse.setDscMarcaModelo(rs.getString("dsc_Marca_Modelo"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			stmt = null;
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � MunicipioBean
	*-----------------------------------------------------------
	*/

	public boolean MunicipioExiste(MunicipioBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where =
				"WHERE cod_municipio = " + auxClasse.getCodMunicipio();
			String sCmd = "SELECT COD_MUNICIPIO from TSMI_MUNICIPIO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}



	public void MunicipioInsert(MunicipioBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String auxCodUF = "";
			if (auxClasse.getCodUF().trim().equals("")) auxCodUF = "null";
			else auxCodUF = "'" + auxClasse.getCodUF() + "'";
			String sCmd =
				"INSERT INTO TSMI_MUNICIPIO (cod_municipio,cod_uf, nom_municipio)";
			sCmd += "VALUES (" + auxClasse.getCodMunicipio() + ", ";
			sCmd += auxCodUF + ",";
			sCmd += "'" + auxClasse.getNomMunicipio() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

 
	public void MunicipioUpdate(MunicipioBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String auxCodUF = "";
			if (auxClasse.getCodUF().trim().equals("")) auxCodUF = "null";
			else auxCodUF = "'" + auxClasse.getCodUF() + "'";
			String sCmd = "UPDATE TSMI_MUNICIPIO set ";
			sCmd += "cod_uf = " + auxCodUF + ", ";
			sCmd += "nom_municipio = '" + auxClasse.getNomMunicipio() + "'";
			sCmd += " where cod_municipio = "
				+ auxClasse.getCodMunicipio();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean MunicipioDelete(MunicipioBean auxClasse)
			throws sys.DaoException {

			boolean existe = false;
			boolean eConnLocal = false;
			Connection conn = null;

			try {
				conn = auxClasse.getConn();
				if (conn == null) {
					conn = serviceloc.getConnection(MYABREVSIST);
					eConnLocal = true;
				}
				Statement stmt = conn.createStatement();
				String sCmd = "DELETE TSMI_MUNICIPIO ";
				sCmd += " where cod_municipio = " + auxClasse.getCodMunicipio();
				stmt.execute(sCmd);
				conn.commit();
				stmt.close();
				stmt = null;
				existe = true;
			} catch (SQLException e) {
				existe = false;
				throw new sys.DaoException(e.getMessage());
			} catch (Exception ex) {
				existe = false;
				throw new sys.DaoException(ex.getMessage());
			} finally {
				if (conn != null) {
					try {
						if (eConnLocal) {
							conn.rollback();
							serviceloc.setReleaseConnection(conn);
						}
					} catch (Exception ey) {
					}
				}
			}
			return existe;
		}


	public Vector BuscarMunicipios(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_municipio in (";
		String auxNom 		= " nom_municipio like '%";
		String auxCodInt	= " cod_municipio_int in (";
		MunicipioBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0) {
			auxCond += auxCod;			
			for (int i=0;i < vCod.size(); i++){
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0) auxCond += " and (";
				for (int i=0;i < vNom.size(); i++){
				   auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				   if (i == vNom.size() - 1) auxCond += ")";
				   else auxCond += " OR ";
				}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
			
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_municipio, cod_uf, nom_municipio from TSMI_MUNICIPIO " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new MunicipioBean();
				relClasse.setCodMunicipio(rs.getString("cod_municipio"));
				relClasse.setCodUF(rs.getString("cod_uf"));
				relClasse.setNomMunicipio(rs.getString("nom_municipio"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}


	/**
	*-----------------------------------------------------------
	* DAO relativo � StatusBean
	*-----------------------------------------------------------
	*/

	public boolean StatusInsert(StatusBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd;
			sCmd = "INSERT INTO TSMI_STATUS_AUTO (cod_status,nom_status) ";
			sCmd += "VALUES (" + auxClasse.getCodStatus() + ", ";
			sCmd += "'" + auxClasse.getNomStatus() + "')";
			stmt.execute(sCmd);
			
			//Prepara para a inclus�o via Broker
			StatusBroker(auxClasse, "1");
			
			auxClasse.setMsgErro("");
			auxClasse.setPkid(auxClasse.getCodStatus());
			conn.commit();
			stmt.close();
			stmt = null;

			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean StatusUpdate(StatusBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_STATUS_AUTO set ";
			sCmd += "nom_status = '" + auxClasse.getNomStatus() + "'";
			sCmd += " where cod_status =" + auxClasse.getCodStatus();
			stmt.execute(sCmd);
			
			//Prepara para a altera��o via Broker
			 StatusBroker(auxClasse, "2");
			
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean StatusDelete(StatusBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_STATUS_AUTO ";
			sCmd += " where cod_status = " + auxClasse.getCodStatus();
			stmt.execute(sCmd);
			
			//Prepara para a exclus�o via Broker
			StatusBroker(auxClasse, "3");
			auxClasse.setPkid("0");
			
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean StatusExiste(StatusBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_status = " + auxClasse.getCodStatus();
			String sCmd = "SELECT COD_STATUS from TSMI_STATUS_AUTO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}
	
	//Metodos relativos a tela de StatusBean no REC
	public void getStatus(StatusBean myStatus) throws DaoException {
			  Connection conn =null ;					
			  try {
				 conn = serviceloc.getConnection(MYABREVSIST) ;
				 Statement stmt = conn.createStatement();
	
				 String sCmd = "SELECT cod_status,nom_status";
						sCmd += " from TSMI_STATUS_AUTO";
						sCmd += " order by nom_status";

				ResultSet rs = stmt.executeQuery(sCmd);
				rs = stmt.executeQuery(sCmd);
		
				Vector status = new Vector();
				while( rs.next() ) {
					StatusBean myStat = new StatusBean() ;
					myStat.setCodStatus(rs.getString("cod_status"));
					myStat.setNomStatus(rs.getString("nom_status"));
					status.addElement(myStat) ;
				}    
				myStatus.setStatus(status) ;	
				myStatus.setStatusGravado(status) ;			
				rs.close();
				stmt.close();
			 }//fim do try
			 catch (SQLException sqle) { 
				myStatus.setMsgErro("Status008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
				myStatus.setCodStatus("0") ;
			}
			catch (Exception e) {
				myStatus.setMsgErro("Status008 - Leitura: " + e.getMessage());
				myStatus.setCodStatus("0") ;
		  }//fim do catch
		  finally {
			if (conn != null) {
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
			}
		  }
		 }

	public boolean InsertStatus(StatusBean myStatus, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			String result = "";
						
			Vector StatusAtual = myStatus.getStatus() ;
			Vector StatusCorrespondente = myStatus.getStatusGravado();
		
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< StatusAtual.size(); i++) {
				StatusBean status = (StatusBean)StatusAtual.elementAt(i) ;
				StatusBean statusCorresp = (StatusBean)StatusCorrespondente.elementAt(i) ;
				
				if("".equals(status.getNomStatus().trim()) == false) 	{
					if(statusCorresp.getCodStatus().equals(status.getCodStatus())) {
						StatusUpdate(status);
					}
					
					else  {
						StatusInsert(status);
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						} 
				}
				else {
					if ("".equals(statusCorresp.getCodStatus())==false){
						if ("".equals(status.getCodStatus())){
							if("".equals(statusCorresp.getCodStatus()) == false) {
								StatusDelete(statusCorresp) ;
							}
	    				}
					}
		        }
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else	  	 conn.rollback();
			stmt.close();
	  }
	  catch (SQLException sqle) {
			   retorno = false;
		   throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			   retorno = false;
			throw new DaoException(e.getMessage());
	  }
	  finally {
		if (conn != null) {
		  try { 
			  conn.rollback();  
			serviceloc.setReleaseConnection(conn); }
		  catch (Exception ey) { }
		}
	  }
	    myStatus.setMsgErro(vErro);
		return retorno;
	  }
	
	
	  
	 /*****************************************
	  * M�todo respons�vel pelas opera��es de 
	  * inclus�o, altera��o e dele��o relativo 
	  * ao OrgaoBean na base via Broker
	  * @param myStatus
	  * @param codacao
	  * @throws DaoException
	  */
	 
	  public void StatusBroker(StatusBean myStatus, String codacao) throws DaoException{
	     //Declara��o de variaveis
	     boolean eConnLocal = false;
	     Connection conn =null ;
	  
	     String codEvento = "090";
	     String indContinua = "      ";
	    
	     String parteVar = sys.Util.lPad(myStatus.getCodStatus(),"0",3) + 
	         codacao + indContinua + sys.Util.rPad(myStatus.getNomStatus()," ",40) ;
	  
	     try {
	  	   conn = myStatus.getConn();
	  		if (conn == null) {
	  		   conn = ROB.Dao.getInstance().getConnection() ;
	  		   eConnLocal = true;
	  		}	
	  	   Statement stmt  = conn.createStatement();
	  
	  	   String sCmd  = "INSERT INTO TSMI_BRKR_TRANS_PENDENTE (cod_brkr_trans_pendente,";
	  	   sCmd += "txt_parte_variavel, dat_proc, cod_evento)";
	  	   sCmd += "VALUES(SEQ_TSMI_BRKR_TRANS_PENDENTE.NEXTVAL ,'"+ parteVar + "'," ;
	  	   sCmd += "sysdate, "+ codEvento + ")" ;
	  	
	  	   stmt.execute(sCmd);
	  	   stmt.close();
	  	   conn.commit();	
	     }
	     catch (SQLException e) {
	  	   throw new DaoException(e.getMessage());
	     }
	     catch (Exception ex) {
	  	   throw new DaoException(ex.getMessage());
	     }
	     finally {
	  	   if (conn != null) {
	  		 try { 
	  		   if(eConnLocal){
	  			   conn.rollback();		  
                   ROB.Dao.getInstance().setReleaseConnection(conn); }
	  		   }
	  		 catch (Exception ey) { }
	  	   }
	     }		
	  }
	 
	 
	 
	 
	 
	 /*****************************************
	  * M�todo respons�vel pelas opera��es de 
	  * inclus�o, altera��o e dele��o relativo 
	  * ao OrgaoBean na base via Broker
	  * @param myEvento
	  * @param codacao
	  * @throws DaoException
	  */
	 
	  public void EventoBroker(EventoBean myEvento, String codacao) throws DaoException{
	     //Declara��o de variaveis
	     boolean eConnLocal = false;
	     Connection conn =null ;
	  
	     String codEvento = "092";
	     String indContinua = "      ";
	    
	     String parteVar = sys.Util.lPad(myEvento.getCodEvento(), "0",3) + 
	           codacao + indContinua + sys.Util.rPad(myEvento.getNomEvento()," ",50) ;
	  
	     try {
	  	   conn = myEvento.getConn();
	  		if (conn == null) {
	  		   conn = ROB.Dao.getInstance().getConnection() ;
	  		   eConnLocal = true;
	  		}	
	  	   Statement stmt  = conn.createStatement();
	  
	  	   String sCmd  = "INSERT INTO TSMI_BRKR_TRANS_PENDENTE (cod_brkr_trans_pendente,";
	  	   sCmd += "txt_parte_variavel, dat_proc, cod_evento)";
	  	   sCmd += "VALUES(SEQ_TSMI_BRKR_TRANS_PENDENTE.NEXTVAL ,'"+ parteVar + "'," ;
	  	   sCmd += "sysdate, "+ codEvento + ")" ;
	  	
	  	   stmt.execute(sCmd);
	  	   stmt.close();
	  	   conn.commit();	
	     }
	     catch (SQLException e) {
	  	   throw new DaoException(e.getMessage());
	     }
	     catch (Exception ex) {
	  	   throw new DaoException(ex.getMessage());
	     }
	     finally {
	  	   if (conn != null) {
	  		 try { 
	  		   if(eConnLocal){
	  			   conn.rollback();		  
	  			   ROB.Dao.getInstance().setReleaseConnection(conn); }
	  		   }
	  		 catch (Exception ey) { }
	  	   }
	     }		
	  }



	/**
	*-----------------------------------------------------------
	* DAO relativo � TipoBean
	*-----------------------------------------------------------
	*/

	public boolean TipoExiste(TipoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_Tipo = " + auxClasse.getCodTipo();
			String sCmd = "SELECT COD_TIPO from TSMI_TIPO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {

			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void TipoInsert(TipoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_TIPO (cod_tipo,dsc_tipo)";
			sCmd += "VALUES (" + auxClasse.getCodTipo() + ", ";
			sCmd += "'" + auxClasse.getDscTipo() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {

			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}


	public void TipoUpdate(TipoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_TIPO set ";
			sCmd += "dsc_tipo = '" + auxClasse.getDscTipo() + "'";
			sCmd += " where cod_tipo = " + auxClasse.getCodTipo();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {

			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean TipoDelete(TipoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_TIPO ";
			sCmd += " where cod_tipo = " + auxClasse.getCodTipo();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}
	
	public Vector BuscarTipos(String tipoCarga, Vector vCod, 
								   Vector vNom) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_tipo in (";
		String auxNom 		= " ds_tipo like '%";
		String auxCodInt	= " cod_tipo_int in (";
		TipoBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0) {
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++) {
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0) auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_tipo, dsc_tipo from TSMI_TIPO " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new TipoBean();
				relClasse.setCodTipo(rs.getString("cod_tipo"));
				relClasse.setDscTipo(rs.getString("dsc_tipo"));
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}
	
	/**
	*-----------------------------------------------------------
	* DAO relativo � UFBean
	*-----------------------------------------------------------
	*/

	public boolean UFExiste(UFBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_UF ='" + auxClasse.getCodUF() + "'";
			String sCmd = "SELECT COD_UF from TSMI_UF " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void UFInsert(UFBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_UF (cod_uf,nom_uf)";
			sCmd += "VALUES ('" + auxClasse.getCodUF() + "', ";
			sCmd += "'" + auxClasse.getNomUF() + "')";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}


	public void UFUpdate(UFBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_UF set ";
			sCmd += "nom_Uf = '" + auxClasse.getNomUF() + "'";
			sCmd += " where cod_Uf = '" + auxClasse.getCodUF() + "'";
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public Vector BuscarUF(String tipoCarga, Vector vCodUF, Vector vNomUF ) throws DaoException {
	
		boolean existe = false ;
		String auxCond = " where ";
		String auxCodUF = " cod_UF in (";
		String auxNomUF = " nom_UF like '%";		
		UFBean relUF;
		Vector vRetUF = new Vector();
				
		if (vCodUF.size() != 0) {
			auxCond += auxCodUF;
			for (int i=0;i < vCodUF.size(); i++) {
				auxCond += (String) vCodUF.elementAt(i);
				if (i == vCodUF.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNomUF.size() != 0){
			if (vCodUF.size() != 0) auxCond += " and (";
			for (int i=0;i < vNomUF.size(); i++){
				auxCond += auxNomUF + (String) vNomUF.elementAt(i) + "'";
				if (i == vNomUF.size() - 1) auxCond += ")";
				else auxCond += " OR ";
			}
		}		
		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);				Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_UF, nom_UF from TSMI_UF " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				relUF = new UFBean();
				relUF.setCodUF(rs.getString("cod_UF"));
				relUF.setNomUF(rs.getString("nom_UF"));				
				vRetUF.addElement(relUF);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetUF;
	}

	public boolean UFDelete(UFBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_UF ";
			sCmd += " where cod_uf = " + auxClasse.getCodUF();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}


	/**
	*-----------------------------------------------------------
	* DAO relativo � UnidadesBean
	*-----------------------------------------------------------
	*/

	public boolean UnidadeExiste(UnidadeBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_unidade =" + auxClasse.getCodUnidade();
			String sCmd = "SELECT COD_unidade from TSMI_UNIDADE " + where;
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

 
	public void UnidadeInsert(UnidadeBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_UNIDADE (cod_unidade,dsc_unidade)";
			sCmd += "VALUES (" + auxClasse.getCodUnidade() + ", ";
			sCmd += "'" + auxClasse.getDscUnidade() + "')";
			//			sCmd += auxClasse.getCodOrgao() + ")";
			//			sCmd += auxClasse.getSeqEndereco() + ")";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("");
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	public void UnidadeUpdate(UnidadeBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_UNIDADE set ";
			sCmd += "dsc_unidade = '" + auxClasse.getDscUnidade();
			sCmd += "' where cod_unidade = " + auxClasse.getCodUnidade();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}
	
	public boolean UnidadeDelete(UnidadeBean auxClasse)
		throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_UNIDADE ";
			sCmd += " where cod_unidade = " + auxClasse.getCodUnidade();
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}


	public Vector BuscarUnidades(String tipoCarga, Vector vCod, 
								   Vector vNom,Vector vCodOrgao ) throws DaoException {
	
		boolean existe 		= false;
		String auxCond 		= " where ";
		String auxCod 		= " cod_unidade in (";
		String auxNom 		= " dsc_unidade like '%";
		String auxCodInt	= " cod_unidade_int in (";
		String auxCodOrgao	= " cod_orgao in (";
		UnidadeBean relClasse;
		Vector vRetClasse 	= new Vector();
				
		if (vCod.size() != 0){
			auxCond += auxCod;
			for (int i=0;i < vCod.size(); i++) {
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (vNom.size() != 0){
			if (vCod.size() != 0) auxCond += " and (";
			for (int i=0;i < vNom.size(); i++){
				auxCond += auxNom + (String) vNom.elementAt(i) + "'";
				if (i == vNom.size() - 1)  auxCond += ")";
				else auxCond += " OR ";
			}
		}		

		if (vCodOrgao.size() != 0) 	{
			if ((vCod.size() != 0) || (vNom.size() != 0)) auxCond += " and (";
			auxCond += auxCodOrgao;
			for (int i=0;i < vCodOrgao.size(); i++)	{
				auxCond += (String) vCod.elementAt(i);
				if (i == vCod.size() - 1) auxCond += ") ";
				else auxCond += ",";
			}
		}		
		
		if (tipoCarga == "TODOS") auxCond = "";
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT cod_unidade, dsc_unidade, cod_orgao from TSMI_UNIDADE " + auxCond;  
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) 
			{
				relClasse = new UnidadeBean();
				relClasse.setCodUnidade(rs.getString("cod_unidade"));
				relClasse.setDscUnidade(rs.getString("dsc_unidade"));
				relClasse.setCodOrgao(rs.getString("cod_orgao")); 
				vRetClasse.addElement(relClasse);
				existe =	true;  
				
			}
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return vRetClasse;
	}

	/**
	*-----------------------------------------------------------
	* DAO relativo � EventoBean
	*-----------------------------------------------------------
	*/

	public boolean EventoInsert(EventoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd;
			sCmd = "INSERT INTO TSMI_EVENTO (cod_evento,dsc_evento) ";
			sCmd += "VALUES (" + auxClasse.getCodEvento() + ", ";
			sCmd += "'" + auxClasse.getNomEvento() + "')";
			stmt.execute(sCmd);
			
			//Prepara para a inclus�o via Broker
			EventoBroker(auxClasse, "1");
			
			auxClasse.setMsgErro("");
			auxClasse.setPkid(auxClasse.getCodEvento());
			conn.commit();
			stmt.close();
			stmt = null;

			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EventoUpdate(EventoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_EVENTO set ";
			sCmd += "dsc_evento = '" + auxClasse.getNomEvento() + "'";
			sCmd += " where cod_evento =" + auxClasse.getCodEvento();
			stmt.execute(sCmd);
			
			//Prepara para a altera��o via Broker
			 EventoBroker(auxClasse, "2");
			
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EventoDelete(EventoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TSMI_EVENTO	 ";
			sCmd += " where cod_evento = " + auxClasse.getCodEvento();
			stmt.execute(sCmd);
			
			//Prepara para a exclus�o via Broker
			EventoBroker(auxClasse, "3");
			auxClasse.setPkid("0");
			
			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean EventoExiste(EventoBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_evento = " + auxClasse.getCodEvento();
			String sCmd = "SELECT COD_EVENTO from TSMI_EVENTO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}
	
	public void getEvento(EventoBean myEvento) throws DaoException {
			  Connection conn =null ;					
			  try {
				 conn = serviceloc.getConnection(MYABREVSIST) ;
				 Statement stmt = conn.createStatement();
	
				 String sCmd = "SELECT cod_evento,dsc_evento";
						sCmd += " from TSMI_EVENTO";
						sCmd += " order by dsc_evento";

				ResultSet rs = stmt.executeQuery(sCmd);
				rs = stmt.executeQuery(sCmd);
		
				Vector evento = new Vector();
				while( rs.next() ) {
					EventoBean myEven = new EventoBean() ;
					myEven.setCodEvento(rs.getString("cod_Evento"));
					myEven.setNomEvento(rs.getString("dsc_evento"));
					evento.addElement(myEven) ;
				}    
				myEvento.setEvento(evento) ;	
				myEvento.setEventoGravado(evento) ;	
				rs.close();
				stmt.close();
			 }//fim do try
			 catch (SQLException sqle) { 
				myEvento.setMsgErro("Evento008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
				myEvento.setCodEvento("0") ;
			}
			catch (Exception e) {
				myEvento.setMsgErro("Evento008 - Leitura: " + e.getMessage());
				myEvento.setCodEvento("0") ;
		  }//fim do catch
		  finally {
			if (conn != null) {
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
			}
		  }
		 }

	public boolean InsertEvento(EventoBean myEvento) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
	
			Vector EventoAtual = myEvento.getEvento() ;
		
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< EventoAtual.size(); i++) {
				EventoBean evento = (EventoBean)EventoAtual.elementAt(i) ;
				if("".equals(evento.getNomEvento().trim()) == false) 	{
					if(("".equals(evento.getCodEvento())))  EventoInsert(evento) ;
					else EventoUpdate(evento);
				}
				else {
					if ("".equals(evento.getCodEvento()) == false) EventoDelete(evento) ;
		 }
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else	  	 conn.rollback();
			stmt.close();
	  }
	  catch (SQLException sqle) {
			   retorno = false;
		   throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			   retorno = false;
			throw new DaoException(e.getMessage());
	  }
	  finally {
		if (conn != null) {
		  try { 
			  conn.rollback();  
			serviceloc.setReleaseConnection(conn); }
		  catch (Exception ey) { }
		}
	  }
		return retorno;
	  }
	  
	public boolean InsertEvento(EventoBean myEvento, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;
		String result = "";					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			Vector EventoAtual = myEvento.getEvento() ;
			Vector EventoCorrespondente = myEvento.getEventoGravado();
		
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< EventoAtual.size(); i++) {
				EventoBean evento = (EventoBean)EventoAtual.elementAt(i) ;
				EventoBean eventoCorresp = (EventoBean)EventoCorrespondente.elementAt(i) ;
				
				if("".equals(evento.getNomEvento().trim()) == false) 	{
					if(eventoCorresp.getCodEvento().equals(evento.getCodEvento())) {  
						EventoUpdate(evento);
					}
					else {
						EventoInsert(evento);
					} 
				}
				else {
					if ("".equals(eventoCorresp.getCodEvento())==false){
						if ("".equals(evento.getCodEvento())){
							if("".equals(eventoCorresp.getCodEvento()) == false) {
								EventoDelete(eventoCorresp) ;
							}
						}
					}
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else	  	 conn.rollback();
			stmt.close();
	  }
	  catch (SQLException sqle) {
			   retorno = false;
		   throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			   retorno = false;
			throw new DaoException(e.getMessage());
	  }
	  finally {
		if (conn != null) {
		  try { 
			  conn.rollback();  
			serviceloc.setReleaseConnection(conn); }
		  catch (Exception ey) { }
		}
	  }
		myEvento.setMsgErro(vErro);
		return retorno;
	  }
}
