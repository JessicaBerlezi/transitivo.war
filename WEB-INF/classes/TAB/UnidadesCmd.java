package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Unidade <br>
* <b>Description:</b>  Registro - Carrega Tabela Unidade<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class UnidadesCmd extends sys.Command {
	public UnidadesCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Unidades  ----------");
		System.out.println("Processando Unidades");

		try {
			//Declaracao de variaveis
			String codUnidade = "";
			String dscUnidade = "";
			String txtEnd = "";
			String txtNum = "";
			String txtCompl = "";
			String txtBairro = "";
			String txtCEP = "";
			String codMunic = "";

			String codRetorno = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "   ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				TAB.UnidadeBean unidadeId = new TAB.UnidadeBean();

				//Chamar  a Transa��o
				resultTrans = daoBroker.Transacao082("999","5", indContinua, conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						indContinua = linha.substring(25*124, 25*124+3);
						for (int j = 0; j < 25; j++) { 

							if ((!linha.substring(j * 124, j * 124 + 3).trim().equals("")) 
								&& (!linha.substring(j * 124, j * 124 + 3).trim().equals("000"))) {

								codUnidade = linha.substring((124 * j), (124 * j) + 3);
								dscUnidade = linha.substring((124 * j) + 3, (124 * j) + 48);
								txtEnd = linha.substring((124 * j) + 48, (124 * j) + 75);
								txtNum = linha.substring((124 * j) + 75, (124 * j) + 80);
								txtCompl = linha.substring((124 * j) + 80, (124 * j) + 91);
								txtBairro = linha.substring((124 * j) + 91, (124 * j) + 111);
								txtCEP = linha.substring((124 * j) + 111, (124 * j) + 119);
								codMunic = linha.substring((124 * j) + 119, (124 * j) + 124);
								
								unidadeId.setCodUnidade(codUnidade);
								unidadeId.setDscUnidade(dscUnidade);
								unidadeId.verificaUnidade(conn);
							}
							else continua = false;

						}
						// Verificar Indicador de Continuidade						
//						indContinua = linha.substring(3198, 3201);
						indContinua = linha.substring(25*124, 25*124+3);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000"))) {
							continua = false;
						}
					}
				}
				totalIns = unidadeId.getTotalIns();
				totalUpd = unidadeId.getTotalAtu();
			}
		} catch (Exception e) {
			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Unidades    ----------");
		return ok;
	}
}