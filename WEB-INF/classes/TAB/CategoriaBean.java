package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Categoria de Carro<br>
* <b>Description:</b>  	Bean dados das Categorias - Tabela de Categorias de Carros<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class CategoriaBean extends sys.HtmlPopupBean {

	private String codCategoria;
	private String dscCategoria;

	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public CategoriaBean() {

		super();
		setTabela("TSMI_CATEGORIA");
		setPopupWidth(35);
		codCategoria = "";
		dscCategoria = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodCategoria(String codCategoria) {
		this.codCategoria = codCategoria;
		if (codCategoria == null)
			this.codCategoria = "";
	}

	public String getCodCategoria() {
		return this.codCategoria;
	}

	public void setDscCategoria(String dscCategoria) {
		this.dscCategoria = dscCategoria;
		if (dscCategoria == null)
			this.dscCategoria = "";
	}

	public String getDscCategoria() {
		return this.dscCategoria;
	}
	
	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}
	

	/**
	 * M�todo para inserir a categoria na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertCategoria() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodCategoria() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CategoriaExiste(this) == true) {
					vErro.addElement(
						"Categoria j� est� cadastrada: "
							+ this.getCodCategoria()
							+ " - "
							+ this.getDscCategoria()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Categoria: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCategoria() == null) {
			vErro.addElement("C�digo da Categoria n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscCategoria() == null) {
			vErro.addElement("Descri��o da Categoria n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.CategoriaInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Categoria n�o inclu�da: "
						+ this.getCodCategoria()
						+ " - "
						+ this.getDscCategoria()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Categoria na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a categoria existe.
	 */

	public boolean isUpdateCategoria() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodCategoria() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CategoriaExiste(this) == false) {
					vErro.addElement(
						"Categoria n�o existe: "
							+ this.getCodCategoria()
							+ " - "
							+ this.getDscCategoria()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Categoria: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCategoria() == null) {
			vErro.addElement("C�digo da Categoria n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.CategoriaUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Categoria n�o atualizada: "
					+ this.getCodCategoria()
					+ " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a categoria na base de dados
	 * Valida se a categoria existe.
	 */

	public boolean isDeleteCategoria() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodCategoria() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.CategoriaExiste(this) == false) {
					vErro.addElement(
						"Categoria n�o existe: "
							+ this.getCodCategoria()
							+ " - "
							+ this.getDscCategoria()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Categoria: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodCategoria() == null) {
			vErro.addElement("C�digo da Categoria n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.CategoriaDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Categoria n�o excluido: " + this.getCodCategoria() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaOperacao(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.CategoriaExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertCategoria();
			  setTotalIns(totalIns);
			}
			else {
			  totalAtu = contador(totalAtu); 	
			  isUpdateCategoria();
			  setTotalAtu(totalAtu);
			} 
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }
	
	public int contador (int cont){
		cont += 1; 
		return cont;
	}


}
