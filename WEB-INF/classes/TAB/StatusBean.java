package TAB;

import java.sql.Connection;
import java.util.Vector;



/**
* <b>Title:</b>        	SMIT - Bean de Status do Auto<br>
* <b>Description:</b>  	Bean dados dos Status do Auto - Tabela de Status do Auto<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class StatusBean extends sys.HtmlPopupBean {

	private String atualizarDependente;
	private String codStatus;
	private String nomStatus;
	
	private Connection conn;
	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Vector status;
	private Vector statusGravado;
	
      
	public StatusBean(){

		super();
		setTabela("TSMI_STATUS_AUTO");	
		setPopupWidth(35);
		codStatus	= "";	
		nomStatus	= "";
		
		status = new Vector();
		statusGravado = new Vector();

	}

	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodStatus(String codStatus)  {
		this.codStatus=codStatus ;
		if (codStatus==null) this.codStatus="" ;
	}  

	public String getCodStatus()  {
		return this.codStatus;
	}

	public void setNomStatus(String nomStatus)  {
		this.nomStatus = nomStatus;
	if (nomStatus==null) this.nomStatus="" ;	
	}  

	public String getNomStatus()  {
		return this.nomStatus;
	}

	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}

	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}

	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}
	
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
	}
	public String getAtualizarDependente() {
		return this.atualizarDependente; 
	}


	/**
	 * M�todo para inserir o Status do Auto na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsert() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if (this.getCodStatus() != null) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.StatusExiste(this) == true) 
				{
					vErro.addElement("Status j� est� cadastrado: " +this.getCodStatus() + " - " + 
						this.getNomStatus()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Status do Auto: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodStatus() == null) 
		{
		vErro.addElement("C�digo do Status do Auto n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (this.getNomStatus() == null) 
		{
		vErro.addElement("Descri��o do Status do Auto n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (bOk == true) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				dao.StatusInsert(this) ;
				vErro.addElement("Status do Auto inclu�do: " +this.getCodStatus() + " - " + 
						this.getNomStatus()+" \n") ;
			}
			catch (Exception e) { 
				vErro.addElement("Status do Auto n�o inclu�do: " +this.getCodStatus() + " - " + 
						this.getNomStatus()+" \n") ;
				vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		setMsgErro(vErro) ;		
		return bOk;  
		
	}

	/**
	 * M�todo para atualizar os dados do Status do Auto na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o Status existe.
	 */
	
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodStatus() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.StatusExiste(this) == false) 
				{
					vErro.addElement("Status do Auto n�o existe: " +this.getCodStatus() + " - " + 
						this.getNomStatus()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Status do Auto: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodStatus() == null) 
		{
			vErro.addElement("C�digo do Status do Auto n�o preenchido" +" \n") ;
			bOk = false ;
		}
		
		if (this.getNomStatus() == null) 
		{
			vErro.addElement("Descri��o do Status do Auto n�o preenchida" +" \n") ;
			bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.StatusUpdate(this) ; 
		}
		catch (Exception e) { 
			vErro.addElement("Status do Auto n�o atualizado: " + this.getCodStatus()+" \n") ;
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}

	/**
	 * M�todo para remover o Status do Auto da base de dados
	 * Valida se o Status existe.
	 */
   
	public boolean isDelete() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodStatus() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.StatusExiste(this) == false) 
				{
					vErro.addElement("Status do Auto n�o existe: " +this.getCodStatus() + " - " + 
						this.getNomStatus()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Status do Auto: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodStatus() == null) 
		{
		vErro.addElement("C�digo do Status do Auto n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.StatusDelete(this) ; 
			vErro.addElement("Status do Auto excluido: " + this.getCodStatus()+"\n") ;
		}
		catch (Exception e) { 
			vErro.addElement("Status do Auto n�o excluido: " + this.getCodStatus()+" \n") ;
			vErro.addElement("Erro na exclus�o  : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}
	
	public void verificaOperacao(Connection conn) throws sys.BeanException {
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.StatusExiste(this) == false){
				isInsert();
				totalIns = contador(totalIns);
				setTotalIns(totalIns);
			}
			else{ 
				isUpdate();  
				totalAtu = contador(totalAtu);
				setTotalAtu(totalAtu);
			}
			
		} catch (Exception e) {
			throw new sys.BeanException("StatusBean - Erro ao efetuar verifica��o: "+e.getMessage());
		}
	 
	}

	public int contador(int cont) {
		cont += 1;
		return cont;
	}
	
	
	public Vector getStatus(int min,int livre)  {
		   try  {
 	
				Dao dao = Dao.getInstance();
				dao.getStatus(this);
				if (this.status.size()<min-livre) 
					livre = min-status.size() ;	  	
					for (int i=0;i<livre; i++) 	{
						this.status.addElement(new StatusBean()) ;
        		}
		   }
		   catch (Exception e) {
				setMsgErro("Status007 - Leitura: " + e.getMessage());
		   }
		  return this.status ;
		}


	public Vector getStatus()  {
	   return this.status;
	}   
  
	public StatusBean getStatus(int i)  { 
	   return (StatusBean)this.status.elementAt(i);
	}
 
	public void setStatus(Vector status)  {
			this.status = status ;
	}  
	
	public void setStatusGravado(Vector statusGravado ){
		this.statusGravado = statusGravado;
	}
	public Vector getStatusGravado(){
		return this.statusGravado;
	}
	
	public boolean isInsertStatus()   {
	   boolean retorno = true;
	   Vector vErro = new Vector() ;
		   // Verificada a valida��o dos dados nos campos
		   if( isValidStatus(vErro) )   {
				   try  {
					TAB.Dao dao = TAB.Dao.getInstance();	   
				   if (dao.InsertStatus(this,vErro))    {	
						   vErro.addElement("Status "
								+this.nomStatus+" atualizados:"+ " \n");
					   }
					   else{		
							vErro.addElement("Status "
								+this.nomStatus+" n�o atualizados:"+ " \n");     	   
					   }
				   }
		   catch (Exception e) {
				vErro.addElement("Status "+this.nomStatus+" n�o atualizados:"+ " \n") ;
				vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
				retorno = false;
		   }//fim do catch
		}
	 else retorno = false;   
		setMsgErro(vErro);
		return retorno;
	  }
   
	  public boolean isValidStatus(Vector vErro) {
 	
		String sDesc = "";
		for (int i = 0 ; i< this.status.size(); i++) {
		   TAB.StatusBean myStatus = (TAB.StatusBean)this.status.elementAt(i) ;
		sDesc = myStatus.getCodStatus();
		if((sDesc.length()==0) ||("".equals(sDesc)))
		  continue ;
  		  
		  // verificar duplicata 
	   for (int m = 0 ; m <this.status.size(); m++) {
		   TAB.StatusBean myStatusOutro = (TAB.StatusBean)this.status.elementAt(m) ;		
		if ((m!=i) && (myStatusOutro.getCodStatus().trim().length()!=0) && (myStatusOutro.getCodStatus().equals(sDesc))) {
			vErro.addElement("Status em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
		}
	   }
		}
		return (vErro.size() == 0) ;  
	  }

}
