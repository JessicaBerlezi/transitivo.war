package TAB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Especie <br>
* <b>Description:</b>  Registro - Carrega Tabela Especie<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luiz Medronho
* @version 1.0
*/

public class StatusCmd {


	public StatusCmd() {

	}

	public boolean carregaTab(Connection conn) {

		sys.Monitor monitor = new sys.Monitor("c:\\smit\\log\\","Carga");
		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		monitor.iniciaMonitor("----------  Inicio da Carga de Status do Auto  ----------");

		try {

			//Declaracao de variaveis
			String cod = "";
			String dsc = "";
			String codRetorno = "";
			String indCont = "";
			boolean continua = true;

			//Criando instancia o bean EspecieBean
			StatusBean classe = new StatusBean();

			BufferedReader arq =
				new BufferedReader(
					new FileReader("c:\\smit\\log\\status02.txt"));
			
			System.out.println("Processando StatusCmd ...");
			while (continua) {
				String linha = arq.readLine();
								
				if (linha != null) {
					codRetorno = linha.substring(37, 40);
					indCont = linha.substring(3253,3256);
					if (codRetorno.compareTo("000") == 0) {
						for (int i = 0; i < 51; i++) {
							totalLid++;
//							System.out.println("i - " + i);
							cod = linha.substring((63 * i) + 40, (63 * i) + 43);
							dsc = linha.substring((63 * i) + 43, (63 * i) + 103);
							if ((cod.trim().equals(null) == false) ||(cod.trim().equals("000") == false) ) {
								classe.setCodStatus(cod);
								classe.setNomStatus(dsc);
								classe.verificaOperacao(conn);
							}
							else {
								continua = false;
							}
						}
						totalReg++;
						if ((indCont.equals("000") == true) || (indCont.trim().equals(null) == true)) {
							classe.setTotalReg(totalReg);
							continua = false;
						}
					} else {
						classe.setMsgErro("Arquivo Inv�lido");
						monitor.gravaMonitor("     Erro - C�digo de Retorno do E-Broker: " + codRetorno);
						System.out.println("Arquivo Inv�lido");
					}
				} else {
					continua = false;
				}
			}
			arq.close();
			totalIns = classe.getTotalIns();
			totalUpd = classe.getTotalAtu();
			
		} catch (IOException e) {
			monitor.gravaMonitor("     Erro: " + e.getMessage());
			
			System.err.println(e.getMessage());

		} catch (Exception ue) {
			System.err.println("N�o pude ler o arquivo: ");
			monitor.gravaMonitor("     Erro: " + ue.getMessage());

		}
		monitor.gravaMonitor("     Registros lidos: " + totalReg + ". C�digos lidos: " + totalLid + ". Inseridos: " + totalIns + ". Atualizados: " + totalUpd);
		monitor.finalizaMonitor("----------    Fim da Carga de Status do Auto    ----------");

		return ok;
	}

}
