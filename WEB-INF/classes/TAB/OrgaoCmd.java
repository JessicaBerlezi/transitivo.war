package TAB;

/**
 * @author Administrador
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import sys.DaoBase;
import sys.DaoException;

public class OrgaoCmd extends DaoBase{

	public OrgaoCmd() throws DaoException {
		super();
	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor)throws sys.DaoException  {

		//sys.Monitor monitor = new sys.Monitor("c:\\smit\\log\\","Carga");
		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Orgaos  ----------");

		try {
			//Declaracao de variaveis
			String codTipo = "";
			String dscTipo = "";

			String codRetorno = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "   ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia do bean 
				ACSS.OrgaoBean OrgaoId = new ACSS.OrgaoBean();

				// Chamar a transa��o

				resultTrans = daoBroker.Transacao083("999999","5","      ","","","","","","","","","","","","");

				System.out.println("OrgaoCmd.carregaTab.resultTrans: " + resultTrans);
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						continua = false;
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						indContinua = linha.substring(15*200,15*200+6);
						for (int j = 0; j < 15; j++) {

//							this.setPosIni(j * 138);
//							System.out.println("OrgaoCmd.posIni: " + getPosIni());
							String codOrgao = this.getPedaco(linha,6," "," ");
							if ( (!codOrgao.trim().equals(null)) 
								&& (!codOrgao.trim().equals("000000"))){
							
								OrgaoId.setCodOrgao(codOrgao);
								OrgaoId.setNomOrgao(getPedaco(linha,30," "," "));
								sys.EnderecoBean endereco = new sys.EnderecoBean();
								endereco.setTxtEndereco(getPedaco(linha,27,"R"," "));
								endereco.setNumEndereco(getPedaco(linha,5,"R"," "));
								endereco.setTxtComplemento(getPedaco(linha,11,"R"," "));
								endereco.setNomBairro(getPedaco(linha,20,"R"," "));
								endereco.setNumCEP(getPedaco(linha,8," "," "));
//								Ler Municipio do Auto
								String sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+getPedacoNum(linha,5,0,".")+"'" ;
								Statement stmt = conn.createStatement();
//								System.out.println("OrgaoCmd: vou ler municipio: " + sCmd);
								ResultSet rs    = stmt.executeQuery(sCmd) ;
								while (rs.next()) {
								  endereco.setNomBairro(rs.getString("nom_municipio"));	  				  	  				  
								}

								getPedaco(linha,3," "," ");
								getPedaco(linha,15," "," ");
								getPedaco(linha,15," "," ");
								OrgaoId.setEndereco(endereco);

								//Por enquanto, vou colocar o c�digo na sigla
								OrgaoId.setSigOrgao(getPedaco(linha,10,"R"," "));
								OrgaoId.setSigOrgao(OrgaoId.getCodOrgao());
								OrgaoId.setNomContato(getPedaco(linha,45,"R"," "));
								
//								System.out.println("OrgaoCmd.carregaTab.codOrgao  : " + OrgaoId.getCodOrgao());
//								System.out.println("OrgaoCmd.carregaTab.nomOrgao  : " + OrgaoId.getNomOrgao());
//								System.out.println("OrgaoCmd.carregaTab.Endereco  : " + OrgaoId.getEndereco().getTxtEndereco());
//								System.out.println("OrgaoCmd.carregaTab.siglaOrgao: " + OrgaoId.getSigOrgao());
//								System.out.println("OrgaoCmd.carregaTab.Contado   : " + OrgaoId.getNomContato());
								OrgaoId.verificaOrgao(conn);
							}
							else 
								break;

						}
						// Verificar Indicador de Continuidade						
//						indContinua = linha.substring(2070,2076);
						indContinua = linha.substring(15*200,15*200+6);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000000")) ) {
							continua = false;
						}
					}
				}
			}
		} catch (Exception e) {
			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
				"     Totaliza��o===> "
					+ " C�digos Inseridos: "
					+ totalIns
					+ ". C�digos Atualizados: "
					+ totalUpd,monitor.MSG_AVISO);
			monitor.finalizaMonitor(
				"----------    Fim da Carga de Org�o    ----------");
		return ok;
	}
}
