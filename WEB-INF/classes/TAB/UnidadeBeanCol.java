package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Unidades<br>
* <b>Description:</b>  	Bean dados dos Unidades - Cole��o de Unidades<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class UnidadeBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private UnidadeBean auxClasse;

	public UnidadeBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(UnidadeBean classe) {

		if (classe == null)
			classe = new UnidadeBean();
		colClasse.put(classe.getCodUnidade(), classe);

	}

	public UnidadeBean getItem(String codInt) {
		return (UnidadeBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,
		Vector vNom,Vector vCodOrgao) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();
 
		try {
			Dao dao = Dao.getInstance();
			vetClasse =
				dao.BuscarUnidades(tipoCarga, vCod, vNom, vCodOrgao);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Unidades : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((UnidadeBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}
}
