package TAB;

import java.sql.Connection;
import java.util.List;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de UF's<br>
* <b>Description:</b>  	Bean dados das UF's - Tabela de UF<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Welem - Luiz Medronho
* @version 				1.0
*/

public class UFBean extends sys.HtmlPopupBean {

	private String codUF;
	private String NomUF;
	
	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public UFBean() {

		super();
		setTabela("TSMI_UF");
		setPopupWidth(35);
		codUF = "";
		NomUF = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}
	
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}

	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}

	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}


	public void setCodUF(String codUF) {
		this.codUF = codUF;
		if (codUF == null)
			this.codUF = "";
	}

	public String getCodUF() {
		return this.codUF;
	}

	public void setNomUF(String NomUF) {
		this.NomUF = NomUF;
		if (NomUF == null)
			this.NomUF = "";
	}

	public String getNomUF() {
		return this.NomUF;
	}

	/**
	 * M�todo para inserir a UF na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertUF() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodUF() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UFExiste(this) == true) {
					vErro.addElement(
						"UF j� est� cadastrado: "
							+ this.getCodUF()
							+ " - "
							+ this.getNomUF()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta da UF: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUF() == null) {
			vErro.addElement("C�digo da UF n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getNomUF() == null) {
			vErro.addElement("Nome da UF n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.UFInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"UF n�o inclu�da: "
						+ this.getCodUF()
						+ " - "
						+ this.getNomUF()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da UF na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a UF existe.
	 */

	public boolean isUpdateUF() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodUF() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UFExiste(this) == false) {
					vErro.addElement(
						"UF n�o existe: "
							+ this.getCodUF()
							+ " - "
							+ this.getNomUF()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta da UF: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUF() == null) {
			vErro.addElement("C�digo da UF n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.UFUpdate(this);
		} catch (Exception e) {
			vErro.addElement("UF n�o atualizada: " + this.getCodUF() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a UF da base de dados
	 * Valida se a UF existe.
	 */

	public boolean isDeleteUF() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodUF() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.UFExiste(this) == false) {
					vErro.addElement(
						"UF n�o existe: "
							+ this.getCodUF()
							+ " - "
							+ this.getNomUF()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta da UF: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodUF() == null) {
			vErro.addElement("C�digo da UF n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.UFDelete(this);
		} catch (Exception e) {
			vErro.addElement("UF n�o excluida: " + this.getCodUF() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaUF(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector();
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.UFExiste(this) == false) {
				totalIns = contador(totalIns);
				isInsertUF();
				setTotalIns(totalIns);
			}
			else {
				totalAtu = contador(totalAtu);
				isUpdateUF();
				setTotalAtu(totalAtu);
			}
		}
		catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: " + e.getMessage());
		}
		setMsgErro(vErro);
	}

	public int contador(int cont) {
		cont += 1;
		return cont;
	}
	
	public List getUF() throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			return dao.BuscarUF("TODOS",new Vector(),new Vector());			
		}
		catch(Exception e){
			throw new DaoException("Erro da carga das Estados: " + e.getMessage());
		}
	}

}
