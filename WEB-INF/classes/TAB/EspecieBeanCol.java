package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Esp�cie de Ve�culos<br>
* <b>Description:</b>  	Bean dados das Esp�cies - Cole��o de Esp�cies<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class EspecieBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private EspecieBean auxClasse;

	public EspecieBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(EspecieBean classe) {

		if (classe == null)
			classe = new EspecieBean();
		colClasse.put(classe.getCodEspecie(), classe);

	}

	public EspecieBean getItem(String codInt) {
		return (EspecieBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom){
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarEspecies(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Esp�cies : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((EspecieBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}
}
