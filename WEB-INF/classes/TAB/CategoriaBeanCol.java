package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Orgaos<br>
* <b>Description:</b>  	Bean dados das Categoria - Cole��o de Categorias<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class CategoriaBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private CategoriaBean auxClasse;

	public CategoriaBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(CategoriaBean classe) {

		if (classe == null)
			classe = new CategoriaBean();
		colClasse.put(classe.getCodCategoria(), classe);

	}

	public CategoriaBean getItem(String codInt) {
		return (CategoriaBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom){
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarCategorias(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Categorias : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((CategoriaBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}

}
