package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Tipos<br>
* <b>Description:</b>  	Bean dados dos Tipos - Cole��o de Tipos<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class TipoBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private TipoBean auxClasse;

	public TipoBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(TipoBean classe) {

		if (classe == null)
			classe = new TipoBean();
		colClasse.put(classe.getCodTipo(), classe);

	}

	public TipoBean getItem(String codInt) {
		return (TipoBean) colClasse.get(codInt);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codInt) {

		colClasse.remove(codInt);
	}

	public boolean carregarCol(String tipoCarga,Vector vCod,Vector vNom) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarTipos(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de Categorias : "
					+ e.getMessage()
					+ " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((TipoBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}
}
