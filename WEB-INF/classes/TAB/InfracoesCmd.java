package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Infracoes <br>
* <b>Description:</b>  Registro - Carrega Tabela Infracoes<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class InfracoesCmd extends sys.Command {
	public InfracoesCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Infracoes  ----------");
		System.out.println("Processando Infracoes");

		try {
			//Declaracao de variaveis
			String codInfracao = "";
			String dscInfracao = "";
			String dscEnquadramento = "";
			String valReal = "";
			String numPonto = "";
			String dscCompetencia = "";
			String indCondProp = "";
			String indSuspensao = "";

			String codRetorno = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "     ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				InfracaoBean infracaoId = new InfracaoBean();

				//Chamar  a Transa��o
				resultTrans = daoBroker.Transacao081("99999","5", indContinua, conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						indContinua = linha.substring(25*119,25*119+5);
						
						for (int j = 0; j < 25; j++) { 

							if ((!linha.substring(j * 119, j * 119 + 5).trim().equals("")) 
								&& (!linha.substring(j * 119, j * 119 + 5).trim().equals("00000"))) {

								codInfracao = linha.substring((119 * j), (119 * j) + 5);
								dscInfracao = linha.substring((119 * j) + 5, (119 * j) + 85);
								dscEnquadramento = linha.substring((119 * j) + 85, (119 * j) + 105);
								numPonto = linha.substring((119 * j) + 105, (119 * j) + 106);
								valReal = linha.substring((119 * j) + 106, (119 * j) + 113) + "." +
								linha.substring((119 * j) + 113, (119 * j) + 115);
								if (valReal.trim().equals(".")) 
									valReal = "0";
								dscCompetencia = linha.substring((119 * j) + 115, (119 * j) + 117);
								indCondProp = linha.substring((119 * j) + 117, (119 * j) + 118);
								indSuspensao = linha.substring((119 * j) + 118, (119 * j) + 119);
								
								infracaoId.setCodInfracao(codInfracao);
								infracaoId.setDscInfracao(dscInfracao);
								infracaoId.setDscEnquadramento(dscEnquadramento);
								infracaoId.setValReal(valReal);
								infracaoId.setNumPonto(numPonto);
								infracaoId.setDscCompetencia(dscCompetencia);		
								infracaoId.setIndCondProp(indCondProp);
								infracaoId.setIndSuspensao(indSuspensao);
								infracaoId.verificaInfracao(conn);
							}
							else continua = false;

						}
						// Verificar Indicador de Continuidade						
//						indContinua = linha.substring(3201, 3204);
						indContinua = linha.substring(25*119,25*119+5);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("00000"))) {
							continua = false;
						}
					}
				}
				totalIns = infracaoId.getTotalIns();
				totalUpd = infracaoId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Infracoes    ----------");
		return ok;
	}
}