package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Infra��o<br>
* <b>Description:</b>  	Bean dados dos Infra��o - Tabela de Infra��o<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class InfracaoBean extends sys.HtmlPopupBean {

	private String codInfracao;
	private String dscInfracao;
	private String dscEnquadramento;
	private String valReal;
	private String numPonto;
	private String dscCompetencia;
	private String indCondProp;
	private String indSuspensao;
	private String dscGravidade;
	private String valRealDesconto;
	
	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public InfracaoBean() {

		super();
		setTabela("TSMI_INFRACAO");
		setPopupWidth(35);
		codInfracao = "";
		dscInfracao = "";
		dscEnquadramento = "";
		valReal = "";
		numPonto = "";
		dscCompetencia = "";
		indCondProp = "";
		indSuspensao = "";
		dscGravidade = "";
		valRealDesconto = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;
		
	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}


	public void setCodInfracao(String codInfracao) {
		this.codInfracao = codInfracao;
		if (codInfracao == null)
			this.codInfracao = "";
	}
	public String getCodInfracao() {
		return this.codInfracao;
	}


	public void setDscEnquadramento(String dscEnquadramento) {
		this.dscEnquadramento = dscEnquadramento;
		if (dscEnquadramento == null)
			this.dscEnquadramento = "";
	}
	public String getDscEnquadramento() {
		return this.dscEnquadramento;
	}


	public void setValReal(String valReal) {
		this.valReal = valReal;
		if (valReal == null)
				this.valReal = "";
	}
	public String getValReal() {
		return this.valReal;
	}


	public void setNumPonto(String numPonto) {
		this.numPonto = numPonto;
		if (numPonto == null)
			this.numPonto = "";
	}
	public String getNumPonto() {
		return this.numPonto;
	}


	public void setDscCompetencia(String dscCompetencia) {
		this.dscCompetencia = dscCompetencia;
		if (dscCompetencia == null)
			this.dscCompetencia = "";
	}
	public String getDscCompetencia() {
		return this.dscCompetencia;
	}


	public void setIndCondProp(String indCondProp) {
		this.indCondProp = indCondProp;
		if (indCondProp == null)
			this.indCondProp = "";
	}
	public String getIndCondProp() {
		return this.indCondProp;
	}


	public void setIndSuspensao(String indSuspensao) {
		this.indSuspensao = indSuspensao;
		if (indSuspensao == null)
			this.indSuspensao = "";
	}
	public String getIndSuspensao() {
		return this.indSuspensao;
	}


	public void setDscGravidade(String dscGravidade) {
		this.dscGravidade = dscGravidade;
		if (dscGravidade == null)
			this.dscGravidade = "";
	}
	public String getDscGravidade() {
		return this.dscGravidade;
	}


	public void setValRealDesconto(String valRealDesconto) {
		this.valRealDesconto = valRealDesconto;
		if (valRealDesconto == null)
			this.valRealDesconto = "";
	}
	public String getValRealDesconto() {
		return this.valRealDesconto;
	}


	public void setDscInfracao(String dscInfracao) {
		this.dscInfracao = dscInfracao;
		if (dscInfracao == null)
			this.dscInfracao = "";
	}
	public String getDscInfracao() {
		return this.dscInfracao;
	}

	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}


	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}


	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}



	/**
	 * M�todo para inserir a Infra��o na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertInfracao() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodInfracao() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.InfracaoExiste(this) == true) {
					vErro.addElement(
						"Infracao j� est� cadastrada: "
							+ this.getCodInfracao()
							+ " - "
							+ this.getDscInfracao()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Infracao: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodInfracao() == null) {
			vErro.addElement("C�digo da Infra��o n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscInfracao() == null) {
			vErro.addElement("Descri��o da Infra��o n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.InfracaoInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Infra��o n�o inclu�da: "
						+ this.getCodInfracao()
						+ " - "
						+ this.getDscInfracao()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Infra��o na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se a Infra��o existe.
	 */

	public boolean isUpdateInfracao() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodInfracao() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.InfracaoExiste(this) == false) {
					vErro.addElement(
						"Infracao n�o existe: "
							+ this.getCodInfracao()
							+ " - "
							+ this.getDscInfracao()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Infracao: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodInfracao() == null) {
			vErro.addElement("C�digo da Infra��o n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.InfracaoUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Infra��o n�o atualizada: " + this.getCodInfracao() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover a Infra��o da base de dados
	 * Valida se a Infra��o existe.
	 */

	public boolean isDeleteInfracao() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodInfracao() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.InfracaoExiste(this) == false) {
					vErro.addElement(
						"Infra��o n�o existe: "
							+ this.getCodInfracao()
							+ " - "
							+ this.getDscInfracao()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta � Infracao: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodInfracao() == null) {
			vErro.addElement("C�digo da Infra��o n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.InfracaoDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Infra��o n�o excluida: " + this.getCodInfracao() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	public void verificaInfracao(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector();
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.InfracaoExiste(this) == false) {
				totalIns = contador(totalIns);
				isInsertInfracao();
				setTotalIns(totalIns);
			}
			else {
				totalAtu = contador(totalAtu);
				isUpdateInfracao();
				setTotalAtu(totalAtu);
			}
		}
		catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: " + e.getMessage());
		}
		setMsgErro(vErro);
	}

	public int contador(int cont) {
		cont += 1;
		return cont;
	}

}
