package TAB;

import java.sql.Connection;

public class DaoBroker {
    
    private Connection conn;
    
    public DaoBroker() {
    }
    
    /**************************************
     * 
     * @param indTipoCnh Indicador do Tipo de Cnh
     * @param numCnh N�mero da CNH
     * @param codUFCnh UF da CNH 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao051(String indTipoCnh, String numCnh, String codUFCnh, Connection conn) throws 
    	sys.DaoException {
        
        this.conn = conn;        
        return Transacao051(indTipoCnh, numCnh, codUFCnh);
    }
    
    /**************************************
     * 
     * @param indTipoCnh Indicador do Tipo de Cnh
     * @param numCnh N�mero da CNH
     * @param codUFCnh UF da CNH 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao051(String indTipoCnh, String numCnh, String codUFCnh) throws sys.DaoException {
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do Tipo de CNH 		
        if (indTipoCnh.trim().equals(null)){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao051: Tipo da CNH n�o preenchido";
        }
        if (indTipoCnh.length() != 1){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao051: Tipo da CNH inv�lido";
        }
        //Cr�tica da CNH 		
        if (numCnh.trim().equals(null)){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao051: N�mero da CNH n�o preenchido";
        }
        //Cr�tica da CNH 		
        if (codUFCnh.trim().equals(null)){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao051: UF da CNH n�o preenchido";
        }
        if (codUFCnh.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao051: UF da CNH inv�lido";
        }
        
        if (transOK) {
            String parteVar = indTipoCnh + sys.Util.rPad(numCnh," ",11) + codUFCnh;
            transBRK.setParteFixa("051");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codInfracao C�digo da Infracao. 99999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Infracao a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao081(String codInfracao, String codAcao, String indContinua, Connection conn) throws
    	sys.DaoException{
        
        this.conn = conn;        
        return Transacao081(codInfracao, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codInfracao C�digo da Infracao. 99999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Infracao a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao081(String codInfracao, String codAcao, String indContinua) throws sys.DaoException{
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if (codInfracao.trim().equals(null)){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao081: C�digo da Infra��o do Ve�culo n�o preenchido";
        }
        if (codInfracao.length() != 5){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao081: C�digo da Infra��o do Ve�culo inv�lido";
        }
        //Cr�tica da A��o 		
        if (codAcao.trim().equals(null)){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao081: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao081: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if (indContinua.trim().equals(null)){
            indContinua = "     ";
        }
        if (indContinua.length() != 5){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao081: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codInfracao + codAcao + indContinua;
            transBRK.setParteFixa("081");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codUnidade C�digo da Unidade. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Unidade a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao082(String codUnidade, String codAcao, String indContinua, Connection conn) throws
    sys.DaoException{
        
        this.conn = conn;
        return Transacao082(codUnidade, codAcao, indContinua);
    }
    /**************************************
     * 
     * @param codUnidade C�digo da Unidade. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Unidade a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao082(String codUnidade, String codAcao, String indContinua) throws sys.DaoException{
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codUnidade.equals(null)) ||(codUnidade.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao082: C�digo da Unidade do Ve�culo n�o preenchido";
        }
        if (codUnidade.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao082: C�digo da Unidade do Ve�culo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao082: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao082: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao082: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codUnidade + codAcao + indContinua;
            transBRK.setParteFixa("082");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo da Orgao. 999999 = Todos
     * @param indContinua C�digo do Orgao a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao083(String codOrgao, String codAcao, String indContinua,
            String nomOrgao, String txtEnd, 
            String numEnd, String txtCompl, String nomBairro, String numCep, 
            String codMun, String numDddTel, String numTel, String numFax, 
            String codSigla, String nomContato, Connection conn) throws sys.DaoException{
        
        this.conn = conn;
        return Transacao083(codOrgao, codAcao, indContinua, nomOrgao, txtEnd,
                numEnd, txtCompl, nomBairro, numCep, codMun, numDddTel,
                numTel, numFax, codSigla, nomContato);
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo da Orgao. 999999 = Todos
     * @param indContinua C�digo do Orgao a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao083(String codOrgao, String codAcao, String indContinua,
            String nomOrgao, String txtEnd, 
            String numEnd, String txtCompl, String nomBairro, String numCep, 
            String codMun, String numDddTel, String numTel, String numFax, 
            String codSigla, String nomContato) throws sys.DaoException{
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo do �rg�o n�o preenchido";
        }
        if (codOrgao.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo do �rgao inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "      ";
        }
        if (indContinua.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrgao + codAcao + indContinua 
            + sys.Util.rPad(nomOrgao," ",30) + sys.Util.rPad(txtEnd," ",27)
            + sys.Util.rPad(numEnd," ",5) + sys.Util.rPad(txtCompl," ",11) 
            + sys.Util.rPad(nomBairro," ",20) + sys.Util.lPad(numCep,"0",8)
            + sys.Util.lPad(codMun,"0",5) + sys.Util.rPad(numDddTel," ",3)
            + sys.Util.rPad(numTel," ",15) + sys.Util.rPad(numFax," ",15)
            + sys.Util.rPad(codSigla," ",10) + sys.Util.rPad(nomContato," ",45);  
            ;
            
          
            transBRK.setParteFixa("083");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codAgente C�digo da Agente. 99 = Todos
     * @param indContinua C�digo do Agente a partir do qual continuar� a consulta geral 
     * @param con Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao084(String codAgente, String codAcao, String indContinua,
            String nomAgente, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao084(codAgente, codAcao, indContinua, nomAgente);
    }
    
    /**************************************
     * 
     * @param codAgente C�digo da Agente. 99 = Todos
     * @param indContinua C�digo do Agente a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao084(String codAgente, String codAcao, String indContinua, String nomAgente)
    throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codAgente.equals(null)) ||(codAgente.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao084: C�digo do Agente n�o preenchido";
        }
        if (codAgente.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao084: C�digo do Agente inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao084: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao084: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "  ";
        }
        if (indContinua.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao084: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codAgente + codAcao + indContinua 
            + sys.Util.rPad(nomAgente," ",20);  
            ;
            transBRK.setParteFixa("084");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
            resultado = transBRK.getResultado();
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codUF C�digo da UF. 99 = Todos
     * @param indContinua C�digo da UF a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao085(String codUF, String codAcao, String indContinua,
            String nomUF, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao085(codUF, codAcao, indContinua, nomUF);
    }
    
    /**************************************
     * 
     * @param codUF C�digo da UF. 99 = Todos
     * @param indContinua C�digo da UF a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao085(String codUF, String codAcao, String indContinua,
            String nomUF) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codUF.equals(null)) ||(codUF.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao085: C�digo da UF n�o preenchido";
        }
        if (codUF.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao085: C�digo da UF inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao085: C�digo da a��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao085: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "  ";
        }
        if (indContinua.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao085: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codUF + codAcao + indContinua 
            + sys.Util.rPad(nomUF," ",20);  
            ;
            transBRK.setParteFixa("085");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
            resultado = transBRK.getResultado();
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao086(String codOrgao, String codJunta, String codAcao, 
            String indContinua,	String nomJunta, String sigJunta, String txtEnd, 
            String numEnd, String txtCompl, String nomBairro, String numCep, 
            String codMun, String codUF, String numDddTel, String numTel, String numFax, Connection conn) 
    throws sys.DaoException {
        
        this.conn = conn;
        return Transacao086(codOrgao, codJunta, codAcao, indContinua, nomJunta,
                sigJunta, txtEnd, numEnd, txtCompl, nomBairro, numCep,
                codMun, codUF, numDddTel, numTel, numFax);
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao086(String codOrgao, String codJunta, String codAcao, 
            String indContinua,	String nomJunta, String sigJunta, String txtEnd, 
            String numEnd, String txtCompl, String nomBairro, String numCep, 
            String codMun, String codUF, String numDddTel, String numTel, String numFax) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo do �rg�o n�o preenchido";
        }
        if (codOrgao.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo do �rgao inv�lido";
        }
        //Cr�tica do C�digo da Junta 		
        if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta n�o preenchido";
        }
        if (codJunta.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "            ";
        }
        if (indContinua.length() != 12){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao086: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrgao + codJunta + codAcao + indContinua 
            + sys.Util.rPad(nomJunta," ",20) + sys.Util.rPad(sigJunta," ",10) 
            + sys.Util.rPad(txtEnd," ",27)
            + sys.Util.rPad(numEnd," ",5) + sys.Util.rPad(txtCompl," ",11) 
            + sys.Util.rPad(nomBairro," ",20) + sys.Util.lPad(codMun,"0",4)
            + sys.Util.lPad(numCep,"0",8) + sys.Util.rPad(codUF," ",2) 
            + sys.Util.rPad(numDddTel," ",3)
            + sys.Util.rPad(numTel," ",15) + sys.Util.rPad(numFax," ",15);  
            
            transBRK.setParteFixa("086");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de Dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao087(String codOrgao, String codJunta, String numCPF, 
            String codAcao, String indContinua,	String nomRelator, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao087(codOrgao, codJunta, numCPF, codAcao, indContinua, nomRelator);
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao087(String codOrgao, String codJunta, String numCPF, 
            String codAcao, String indContinua,	String nomRelator) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo do �rg�o n�o preenchido";
        }
        if (codOrgao.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo do �rgao inv�lido";
        }
        //Cr�tica do C�digo da Junta 		
        if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo da Junta n�o preenchido";
        }
        if (codJunta.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo da Junta inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "                       ";
        }
        if (indContinua.length() != 23){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao087: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrgao + codJunta + numCPF + codAcao + indContinua 
            + sys.Util.rPad(nomRelator," ",43);  
            
            transBRK.setParteFixa("087");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Respons�vel pelo parecer Jur�dico a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao088(String codOrgao, String numCPF, 
            String codAcao, String indContinua,	String nomResponsavel, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao088(codOrgao, numCPF, codAcao, indContinua, nomResponsavel);
    }
    
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Respons�vel pelo parecer Jur�dico a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao088(String codOrgao, String numCPF, 
            String codAcao, String indContinua,	String nomResponsavel) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: C�digo do �rg�o n�o preenchido";
        }
        if (codOrgao.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: C�digo do �rg�o inv�lido";
        }
        //Cr�tica do CPF	
        if ((numCPF.equals(null)) ||(numCPF.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: CPF n�o preenchido";
        }
        if (numCPF.length() != 1){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: CPF inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "                 ";
        }
        if (indContinua.length() != 17){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao088: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrgao + numCPF + codAcao + indContinua 
            + sys.Util.rPad(nomResponsavel," ",43);  
            
            transBRK.setParteFixa("088");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao089(String codOrgao, String diasExpdir, 
            String diasFolgaExpdir, String diasFolgaDefPrevia,
            String diasJulg1ainstancia, String diasFolga1ainstancia,
            String diasFolga2ainstancia, String diasDefPrevia,
            String dias1ainstancia, String dias2ainstancia,
            String codSecretaria, String codAssuntoDefPrevia,
            String codAssuntoPenalidade, String codAcao, String indContinua,
            Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao089(codOrgao, diasExpdir, diasFolgaExpdir, diasFolgaDefPrevia,
                diasJulg1ainstancia, diasFolga1ainstancia, diasFolga2ainstancia,
                diasDefPrevia, dias1ainstancia, dias2ainstancia,
                codSecretaria, codAssuntoDefPrevia, codAssuntoPenalidade,
                codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao089(String codOrgao, String diasExpdir, 
            String diasFolgaExpdir, String diasFolgaDefPrevia,
            String diasJulg1ainstancia, String diasFolga1ainstancia,
            String diasFolga2ainstancia, String diasDefPrevia,
            String dias1ainstancia, String dias2ainstancia,
            String codSecretaria, String codAssuntoDefPrevia,
            String codAssuntoPenalidade, String codAcao, String indContinua
    ) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao089: C�digo do �rg�o n�o preenchido";
        }
        if (codOrgao.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao089: C�digo do �rg�o inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao089: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao089: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "      ";
        }
        if (indContinua.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao089: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrgao + sys.Util.lPad(diasExpdir,"0",3) 
            + sys.Util.lPad(diasFolgaExpdir,"0",3)
            + sys.Util.lPad(diasFolgaDefPrevia,"0",3)
            + sys.Util.lPad(diasJulg1ainstancia,"0",3)
            + sys.Util.lPad(diasFolga1ainstancia,"0",3)
            + sys.Util.lPad(diasFolga2ainstancia,"0",3)
            + sys.Util.lPad(diasDefPrevia,"0",3)
            + sys.Util.lPad(dias1ainstancia,"0",3)
            + sys.Util.lPad(dias2ainstancia,"0",3)
            + sys.Util.rPad(codSecretaria," ",3)
            + sys.Util.lPad(codAssuntoDefPrevia,"0",4)
            + sys.Util.lPad(codAssuntoPenalidade,"0",4)
            + indContinua;  
            
            transBRK.setParteFixa("089");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codStatus C�digo do Status do Auto de Infra��o. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Status a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao090(String codStatus, String codAcao, String indContinua,	
            String nomStatus, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao090(codStatus, codAcao, indContinua, nomStatus);
    }
    
    /**************************************
     * 
     * @param codStatus C�digo do Status do Auto de Infra��o. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Status a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao090(String codStatus, String codAcao, String indContinua,	
            String nomStatus) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codStatus.trim().equals(null)) ||(codStatus.trim().equals(""))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao090: C�digo do Status n�o preenchido";
        }
/*        
        if (codStatus.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao090: C�digo do Status inv�lido";
        }
*/        
        //Cr�tica da A��o 		
        if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao090: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao090: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
            indContinua = "   ";
        }
/*        
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao090: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
*/        
        if (transOK) {
            String parteVar = sys.Util.lPad(codStatus,"0",3) + codAcao + sys.Util.lPad(indContinua,"0",3) 
            + sys.Util.rPad(nomStatus," ",30);  
            
            transBRK.setParteFixa("090");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codRetorno C�digo do Retorno da VEX. 99 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Retorno a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao091(String codRetorno, String codAcao, String indContinua,	
            String nomRetorno, String indReceb, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao091(codRetorno, codAcao, indContinua, nomRetorno, indReceb);
    }
    
    /**************************************
     * 
     * @param codRetorno C�digo do Retorno da VEX. 99 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Retorno a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao091(String codRetorno, String codAcao, String indContinua,	
            String nomRetorno, String indReceb) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codRetorno.equals(null)) ||(codRetorno.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao091: C�digo do retorno n�o preenchido";
        }
        /*
        if (codRetorno.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao091: C�digo do Retorno inv�lido";
        }
        */
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao091: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao091: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "  ";
        }
        /*
        if (indContinua.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao091: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        */
        

        if (transOK) {
            String parteVar = sys.Util.lPad(codRetorno.trim(),"0",2) + codAcao + indContinua 
            + sys.Util.rPad(nomRetorno," ",20).substring(0,20) + indReceb;  
            
            transBRK.setParteFixa("091");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codEvento C�digo do Evento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Evento a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao092(String codEvento, String codAcao, String indContinua,	
            String nomEvento, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao092(codEvento, codAcao, indContinua, nomEvento);
    }
    
    /**************************************
     * 
     * @param codEvento C�digo do Evento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Evento a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao092(String codEvento, String codAcao, String indContinua,	
            String nomEvento) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codEvento.trim().equals(null)) ||(codEvento.trim().equals(""))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao092: C�digo do Evento n�o preenchido";
        }
/*        if (codEvento.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao092: C�digo do Evento inv�lido";
        }
*/
                //Cr�tica da A��o 		
        if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao092: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao092: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
            indContinua = "   ";
        }
/*        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao092: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
*/        
        if (transOK) {
            String parteVar = sys.Util.lPad(codEvento,"0",3) + sys.Util.lPad(codAcao,"0",1) + 
            				sys.Util.rPad(indContinua," ",3) 
            				+ sys.Util.rPad(nomEvento," ",30);  
            
            transBRK.setParteFixa("092");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codOrigem C�digo da Origem do Evento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo da Origem a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao093(String codOrigem, String codAcao, String indContinua,	
            String nomOrigem, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao093(codOrigem, codAcao, indContinua, nomOrigem);
    }
    
    /**************************************
     * 
     * @param codOrigem C�digo da Origem do Evento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo da Origem a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao093(String codOrigem, String codAcao, String indContinua,	
            String nomOrigem) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codOrigem.equals(null)) ||(codOrigem.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao093: C�digo da Origem do Evento n�o preenchido";
        }
        if (codOrigem.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao093: C�digo da Origem do Evento inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao093: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao093: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao093: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codOrigem + codAcao + indContinua 
            + sys.Util.rPad(nomOrigem," ",30);  
            
            transBRK.setParteFixa("093");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codStatus C�digo do Status do Requerimento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Status a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao094(String codStatus, String codAcao, String indContinua,	
            String nomStatus, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao094(codStatus, codAcao, indContinua, nomStatus);
    }
    
    /**************************************
     * 
     * @param codStatus C�digo do Status do Requerimento. 999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Status a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao094(String codStatus, String codAcao, String indContinua,	
            String nomStatus) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codStatus.equals(null)) ||(codStatus.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao094: C�digo do Status n�o preenchido";
        }
        if (codStatus.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao094: C�digo do Status inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao094: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao094: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao094: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codStatus + codAcao + indContinua 
            + sys.Util.rPad(nomStatus," ",30);  
            
            transBRK.setParteFixa("094");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codMotivo C�digo do Motivo do Rejeito. 99 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Motivo a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao095(String codMotivo, String codAcao, String indContinua,	
            String nomMotivo, Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao095(codMotivo, codAcao, indContinua, nomMotivo);
    }
    
    /**************************************
     * 
     * @param codMotivo C�digo do Motivo do Rejeito. 99 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Motivo a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao095(String codMotivo, String codAcao, String indContinua,	
            String nomMotivo) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do �rg�o		
        if ((codMotivo.equals(null)) ||(codMotivo.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao095: C�digo do Status n�o preenchido";
        }
        if (codMotivo.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao095: C�digo do Status inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao095: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao095: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "  ";
        }
        if (indContinua.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao095: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codMotivo + codAcao + indContinua 
            + sys.Util.rPad(nomMotivo," ",30);  
            
            transBRK.setParteFixa("095");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codMarca C�digo da Marca/Modelo do Ve�culo. 999999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Marca/Modelo a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao121(String codMarca, String codAcao, String indContinua, Connection conn) throws 
    sys.DaoException {
        
        this.conn = conn;
        return Transacao121(codMarca, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codMarca C�digo da Marca/Modelo do Ve�culo. 999999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Marca/Modelo a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao121(String codMarca, String codAcao, String indContinua) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codMarca.equals(null)) ||(codMarca.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao121: C�digo da Marca/Modelo n�o preenchido";
        }
        if (codMarca.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao121: C�digo da Marca/Modelo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao121: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao121: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao121: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codMarca + codAcao + indContinua;
            transBRK.setParteFixa("121");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codTipo C�digo do Tipo do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo do Tipo a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao122(String codTipo, String codAcao, String indContinua, Connection conn) throws
    sys.DaoException {
        
        this.conn = conn;
        return Transacao122(codTipo, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codTipo C�digo do Tipo do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo do Tipo a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao122(String codTipo, String codAcao, String indContinua) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codTipo.equals(null)) ||(codTipo.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao122: C�digo do Tipo n�o preenchido";
        }
        if (codTipo.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao122: C�digo do Tipo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao122: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao122: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao122: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codTipo + codAcao + indContinua;
            transBRK.setParteFixa("122");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }

    /**************************************
     * 
     * @param codMunicipio C�digo do Munic�pio. 99999 = Todos
     * @param codUF        C�digo da UF do Munic�pio. 99 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo do Munic�pio a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao123(String codMunicipio, String codUF, String codAcao, String indContinua, 
            Connection conn) throws sys.DaoException {
        
        this.conn = conn;
        return Transacao123(codMunicipio, codUF, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codMunicipio C�digo do Munic�pio. 99999 = Todos
     * @param codUF        C�digo da UF do Munic�pio. 99 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo do Munic�pio a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao123(String codMunicipio, String codUF, String codAcao, String indContinua) 
    throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codMunicipio.equals(null)) ||(codMunicipio.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: C�digo do Munic�pio n�o preenchido";
        }
        if (codMunicipio.length() != 5){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: C�digo do Munic�pio inv�lido";
        }
        if (codUF.length() != 2){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: C�digo da UF inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 5){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao123: Indicador de Continuidade inv�lido -> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codMunicipio + codAcao + indContinua;
            transBRK.setParteFixa("123");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codCor C�digo da Cor do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Cor a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de Dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao124(String codCor, String codAcao, String indContinua, Connection conn) throws
    sys.DaoException {
        
        this.conn = conn;        
        return Transacao124(codCor, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codCor C�digo da Cor do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Cor a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao124(String codCor, String codAcao, String indContinua) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codCor.equals(null)) ||(codCor.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao124: C�digo da Cor do Ve�culo n�o preenchido";
        }
        if (codCor.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao124: C�digo da Cor do Ve�culo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao124: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao124: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao124: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codCor + codAcao + indContinua;
            transBRK.setParteFixa("124");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codEspecie C�digo da Esp�cie do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Esp�cie a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao125(String codEspecie, String codAcao, String indContinua, Connection conn) 
    throws sys.DaoException {
        
        this.conn = conn;        
        return Transacao125(codEspecie, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codEspecie C�digo da Esp�cie do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Esp�cie a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao125(String codEspecie, String codAcao, String indContinua) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codEspecie.equals(null)) ||(codEspecie.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao125: C�digo da Esp�cie do Ve�culo n�o preenchido";
        }
        if (codEspecie.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao125: C�digo da Esp�cie do Ve�culo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao125: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao125: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao125: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codEspecie + codAcao + indContinua;
            transBRK.setParteFixa("125");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    /**************************************
     * 
     * @param codCategoria C�digo da Categoria do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Categoria a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com o banco de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao126(String codCategoria, String codAcao, String indContinua, Connection conn) 
    throws sys.DaoException {
        
        this.conn = conn;
        return Transacao126(codCategoria, codAcao, indContinua);
    }
    
    /**************************************
     * 
     * @param codCategoria C�digo da Categoria do Ve�culo. 999 = Todos
     * @param codAcao 4 : Consulta Comum. 5 : Consulta Geral
     * @param indContinua C�digo da Categoria a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao126(String codCategoria, String codAcao, String indContinua) throws sys.DaoException {
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((codCategoria.equals(null)) ||(codCategoria.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao126: C�digo da Categoria do Ve�culo n�o preenchido";
        }
        if (codCategoria.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao126: C�digo da Categoria do Ve�culo inv�lido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao126: C�digo do A��o n�o preenchido";
        }
        if ("45".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao126: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "   ";
        }
        if (indContinua.length() != 3){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao126: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = codCategoria + codAcao + indContinua;
            transBRK.setParteFixa("126");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado(conn);
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
}