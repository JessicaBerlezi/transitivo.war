package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Tipo de Carro<br>
* <b>Description:</b>  	Bean dados dos Tipos - Tabela de Tipos de Carros<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class TipoBean extends sys.HtmlPopupBean {

	private String codTipo;
	private String dscTipo;

	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Connection conn;

	public TipoBean() {

		super();
		setTabela("TSMI_TIPO");
		setPopupWidth(35);
		codTipo = "";
		dscTipo = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}
	
	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}


	public void setCodTipo(String codTipo) {
		this.codTipo = codTipo;
		if (codTipo == null)
			this.codTipo = "";
	}

	public String getCodTipo() {
		return this.codTipo;
	}

	public void setDscTipo(String dscTipo) {
		this.dscTipo = dscTipo;
		if (dscTipo == null)
			this.dscTipo = "";
	}

	public String getDscTipo() {
		return this.dscTipo;
	}
	
	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}

	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}

	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}


	/**
	 * M�todo para inserir o tipo de carro na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertTipo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodTipo() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.TipoExiste(this) == true) {
					vErro.addElement(
						"Tipo j� est� cadastrado: "
							+ this.getCodTipo()
							+ " - "
							+ this.getDscTipo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Tipo: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodTipo() == null) {
			vErro.addElement("C�digo do Tipo n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getDscTipo() == null) {
			vErro.addElement("Descri��o do Tipo n�o preenchida" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.TipoInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Tipo n�o inclu�do: "
						+ this.getCodTipo()
						+ " - "
						+ this.getDscTipo()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados da Tipo na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o tipo existe.
	 */

	public boolean isUpdateTipo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodTipo() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.TipoExiste(this) == false) {
					vErro.addElement(
						"Tipo n�o existe: "
							+ this.getCodTipo()
							+ " - "
							+ this.getDscTipo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Tipo: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodTipo() == null) {
			vErro.addElement("C�digo do Tipo n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.TipoUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Tipo n�o atualizado: " + this.getCodTipo() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover o tipo na base de dados
	 * Valida se o tipo existe.
	 */

	public boolean isDeleteTipo() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodTipo() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.TipoExiste(this) == false) {
					vErro.addElement(
						"Tipo n�o existe: "
							+ this.getCodTipo()
							+ " - "
							+ this.getDscTipo()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta ao Tipo: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodTipo() == null) {
			vErro.addElement("C�digo do Tipo n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.TipoDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Tipo n�o excluido: " + this.getCodTipo() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaTipo(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector();
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.TipoExiste(this) == false) {
				totalIns = contador(totalIns);
				isInsertTipo();
				setTotalIns(totalIns);

			}
			else {
				totalAtu = contador(totalAtu);
				isUpdateTipo();
				setTotalAtu(totalAtu);
			}
		}
		catch (Exception e) {
			throw new sys.BeanException(
				"Erro ao efetuar verifica��o: " + e.getMessage());
		}
		setMsgErro(vErro);
	}

	public int contador(int cont) {
		cont += 1;
		return cont;
	}

}
