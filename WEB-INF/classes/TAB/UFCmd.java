package TAB;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        SMIT - Carga da Tabela UFs <br>
* <b>Description:</b>  Registro - Carrega Tabela Ufs<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class UFCmd extends sys.Command {
	public UFCmd() {

	}

	public boolean carregaTab(Connection conn) {

		sys.Monitor monitor = new sys.Monitor("c:\\smit\\log\\","Carga");
		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		monitor.iniciaMonitor("----------  Inicio da Carga de UFs  ----------");

		try {
			//Declaracao de variaveis
			String codUF = "";
			String nomUF = "";

			String codRetorno = "";
			String indCont = "";
			boolean continua = true;

			Vector vErro = new Vector();

			//Criando instancia o bean 
			UFBean UFId = new UFBean();

			BufferedReader arq =
				new BufferedReader(new FileReader("c:\\smit\\log\\uf02.txt"));
			
			System.out.println("Processando UFCmd ...");
			while (continua) {
				String linha = arq.readLine();
												
				if (linha != null) {
					codRetorno = linha.substring(37, 40);
					indCont = linha.substring(3240, 3242);
					if (codRetorno.compareTo("000") == 0) {
						for (int i = 0; i < 100; i++) {
							totalLid++;
//							System.out.println("i - " + i);
							codUF = linha.substring((32 * i) + 40, (32 * i) + 42);
							nomUF =	linha.substring((32 * i) + 42, (32 * i) + 72);
							if ((codUF.trim().equals(null) == false)
								|| (codUF.equals("00"))) {
								UFId.setCodUF(codUF);
								UFId.setNomUF(nomUF);
								UFId.verificaUF(conn);
								totalReg = i + 1;
							}
							else {
								continua = false;
							}
						}
						if ((indCont.trim().equals(null) == true)
							|| (indCont.equals("00") == true)) {
							continua = false;
						}
						UFId.setTotalReg(totalReg);
					}
					else {
						//Menssagem no log
						monitor.gravaMonitor("     Erro - C�digo de Retorno do E-Broker: " + codRetorno);
						System.out.println("LOG: Arquivo Inv�lido");
					}
				}
				else {
					continua = false;
				}
			}
			arq.close();
			totalIns = UFId.getTotalIns();
			totalUpd = UFId.getTotalAtu();
		}
		catch (IOException e) {
			monitor.gravaMonitor("     Erro: " + e.getMessage());
		}
		catch (Exception ue) {
			monitor.gravaMonitor("     Erro: " + ue.getMessage());
		}
		monitor.gravaMonitor("     Registros lidos: " + totalReg + ". C�digos lidos: " + totalLid + ". Inseridos: " + totalIns + ". Atualizados: " + totalUpd);
		monitor.finalizaMonitor("----------    Fim da Carga de UFs    ----------");
		return ok;
	}
}