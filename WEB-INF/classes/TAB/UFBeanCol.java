package TAB;

import java.util.HashMap;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de UFs<br>
* <b>Description:</b>  	Bean dados dos UF'S - Cole��o de Tipos<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho/Wellem
* @version 				1.0
*/

public class UFBeanCol extends sys.HtmlPopupBean {

	private HashMap colClasse;
	private UFBean auxClasse;

	public UFBeanCol() {

		colClasse = new HashMap();
	}

	public void addItem(UFBean classe) {

		if (classe == null)
			classe = new UFBean();
		colClasse.put(classe.getCodUF(), classe);

	}

	public UFBean getItem(String codUF) {
		return (UFBean) colClasse.get(codUF);

	}

	public int numItem() {
		return colClasse.size();
	}

	public void removeItem(String codUF) {

		colClasse.remove(codUF);
	}

	public boolean carregarCol(String tipoCarga, Vector vCod, Vector vNom) {
		boolean bOk = true;
		Vector vetClasse = new Vector();
		Vector vErro = new Vector();

		try {
			Dao dao = Dao.getInstance();
			vetClasse = dao.BuscarUF(tipoCarga, vCod, vNom);
		} catch (Exception e) {
			vErro.addElement(
				"Erro na Carga da Cole��o de UFs : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	

		for (int i = 0; i < vetClasse.size(); i++) {

			addItem((UFBean) vetClasse.elementAt(i));
		}
		setMsgErro(vErro);
		return bOk;

	}

	public Vector ColToVector() {

		return new Vector(colClasse.values());

	}

}
