package TAB;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Municipios<br>
* <b>Description:</b>  	Bean dados dos Municipios - Tabela de Municipio<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Welem - Luiz Medronho
* @version 				1.0
*/

public class MunicipioBean extends sys.HtmlPopupBean {

	private String codMunicipio;
	private String nomMunicipio;
	private String codUF;
		
	private Connection conn;	
	private int totalReg;
	private int totalIns;
	private int totalAtu;


	public MunicipioBean() {

		super();
		setTabela("TSMI_Municipio");
		setPopupWidth(35);
		codMunicipio = "";
		nomMunicipio = "";
		
		totalReg = 0;
		totalIns = 0;
		totalAtu = 0;

	}

	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodMunicipio(String codMunicipio) {
		this.codMunicipio = codMunicipio;
		if (codMunicipio == null)
			this.codMunicipio = "";
	}

	public String getCodMunicipio() {
		return this.codMunicipio;
	}

	public void setCodUF(String codUF) {
		this.codUF = codUF;
		if (codUF == null)
			this.codUF = "";
	}

	public String getCodUF() {
		return this.codUF;
	}

	public void setNomMunicipio(String nomMunicipio) {
		this.nomMunicipio = nomMunicipio;
		if (nomMunicipio == null)
			this.nomMunicipio = "";
	}

	public String getNomMunicipio() {
		return this.nomMunicipio;
	}
	
	public void setTotalReg(int totalReg){
		this.totalReg = totalReg;
		if (totalReg == 0) this.totalReg = 0;
	}
	public int getTotalReg(){
		return this.totalReg;
	}
	
	public void setTotalIns(int totalIns){
		this.totalIns = totalIns;
		if (totalIns == 0) this.totalIns = 0;
	}
	public int getTotalIns(){
		return this.totalIns;
	}
    
	public void setTotalAtu(int totalAtu){
		this.totalAtu = totalAtu;
		if (totalAtu == 0) this.totalAtu = 0;
	}
	public int getTotalAtu(){
		return this.totalAtu;
	}

	/**
	 * M�todo para inserir o Municipio na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsertMunicipio() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if (this.getCodMunicipio() != null) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MunicipioExiste(this) == true) {
					vErro.addElement(
						"Municipio j� est� cadastrado: "
							+ this.getCodMunicipio()
							+ " - "
							+ this.getNomMunicipio()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta do Municipio: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodMunicipio() == null) {
			vErro.addElement("C�digo do Municipio n�o preenchido" + " \n");
			bOk = false;
		}

		if (this.getNomMunicipio() == null) {
			vErro.addElement("Nome do Municipio n�o preenchido" + " \n");
			bOk = false;
		}

		if (bOk == true) {
			try {
				Dao dao = Dao.getInstance();
				dao.MunicipioInsert(this);
			} catch (Exception e) {
				vErro.addElement(
					"Municipio n�o inclu�da: "
						+ this.getCodMunicipio()
						+ " - "
						+ this.getNomMunicipio()
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
				bOk = false;
			}
		}
		setMsgErro(vErro);
		return bOk;

	}

	/**
	 * M�todo para atualizar os dados do Municipio na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o Municipio existe.
	 */

	public boolean isUpdateMunicipio() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodMunicipio() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MunicipioExiste(this) == false) {
					vErro.addElement(
						"Municipio n�o existe: "
							+ this.getCodMunicipio()
							+ " - "
							+ this.getNomMunicipio()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta do Municipio: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodMunicipio() == null) {
			vErro.addElement("C�digo do Municipio n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.MunicipioUpdate(this);
		} catch (Exception e) {
			vErro.addElement(
				"Municipio n�o atualizado: " + this.getCodMunicipio() + " \n");
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}

	/**
	 * M�todo para remover o Municipio da base de dados
	 * Valida se o Municipio existe.
	 */

	public boolean isDeleteMunicipio() {
		boolean bOk = true;
		Vector vErro = new Vector();

		if ((this.getCodMunicipio() == null) == false) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MunicipioExiste(this) == false) {
					vErro.addElement(
						"Municipio n�o existe: "
							+ this.getCodMunicipio()
							+ " - "
							+ this.getNomMunicipio()
							+ " \n");
					bOk = false;

				}
			} catch (Exception e) {
				vErro.addElement(
					"Erro na consulta do: " + e.getMessage() + " \n");
				bOk = false;
			}
		}

		if (this.getCodMunicipio() == null) {
			vErro.addElement("C�digo do Municipio n�o prenchido" + " \n");
			bOk = false;
		}

		try {
			Dao dao = Dao.getInstance();
			dao.MunicipioDelete(this);
		} catch (Exception e) {
			vErro.addElement(
				"Municipio n�o excluido: " + this.getCodMunicipio() + " \n");
			vErro.addElement("Erro na exclus�o  : " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch 	
		setMsgErro(vErro);
		return bOk;
	}
	
	public void verificaOperacao(Connection conn) throws sys.BeanException {
		Vector vErro = new Vector() ;
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.MunicipioExiste(this) == false){
			  totalIns = contador(totalIns);
			  isInsertMunicipio();
			  setTotalIns(totalIns);
			  
			}
			else {
			  totalAtu = contador(totalAtu); 	
			  isUpdateMunicipio();
			  setTotalAtu(totalAtu);
			} 
		} catch (Exception e) {
			throw new sys.BeanException("Erro ao efetuar verifica��o: "+e.getMessage());
		}
		setMsgErro(vErro);	
	 }
	
	public int contador (int cont){
		cont += 1; 
		return cont;
	}


}
