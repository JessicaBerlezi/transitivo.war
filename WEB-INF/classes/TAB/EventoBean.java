package TAB;

import java.sql.Connection;
import java.util.Vector;




/**
* <b>Title:</b>        	SMIT - Bean de Evento <br>
* <b>Description:</b>  	Bean dados dos Evento  - Tabela de Evento <br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luciana Rocha
* @version 				1.0
*/

public class EventoBean extends sys.HtmlPopupBean {

	private String codEvento;
	private String nomEvento;
	
	private Connection conn;
	private String atualizarDependente;
	private Vector evento;
	private Vector eventoGravado;
	
      
	public EventoBean(){

		super(); 
		setTabela("TSMI_EVENTO");	
		setPopupWidth(35);
		codEvento	= "";	
		nomEvento	= "";
		
		evento = new Vector();
		eventoGravado = new Vector();

	}

	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodEvento(String codEvento)  {
		this.codEvento=codEvento ;
		if (codEvento==null) this.codEvento="" ;
	}  

	public String getCodEvento()  {
		return this.codEvento;
	}

	public void setNomEvento(String nomEvento)  {
		this.nomEvento = nomEvento;
	if (nomEvento==null) this.nomEvento="" ;	
	}  

	public String getNomEvento()  {
		return this.nomEvento;
	}
	
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
	}
	public String getAtualizarDependente() {
		return this.atualizarDependente; 
	}


	/**
	 * M�todo para inserir o Evento  na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsert() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if (this.getCodEvento() != null) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.EventoExiste(this) == true) 
				{
					vErro.addElement("Evento j� est� cadastrado: " +this.getCodEvento() + " - " + 
						this.getNomEvento()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Evento : "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodEvento() == null) 
		{
		vErro.addElement("C�digo do Evento  n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (this.getNomEvento() == null) 
		{
		vErro.addElement("Descri��o do Evento  n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (bOk == true) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				dao.EventoInsert(this) ;
				vErro.addElement("Evento  inclu�do: " +this.getCodEvento() + " - " + 
						this.getNomEvento()+" \n") ;
			}
			catch (Exception e) { 
				vErro.addElement("Evento  n�o inclu�do: " +this.getCodEvento() + " - " + 
						this.getNomEvento()+" \n") ;
				vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		setMsgErro(vErro) ;		
		return bOk;  
		
	}

	/**
	 * M�todo para atualizar os dados do Evento  na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o Evento existe.
	 */
	
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodEvento() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.EventoExiste(this) == false) 
				{
					vErro.addElement("Evento  n�o existe: " +this.getCodEvento() + " - " + 
						this.getNomEvento()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Evento : "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodEvento() == null) 
		{
			vErro.addElement("C�digo do Evento  n�o preenchido" +" \n") ;
			bOk = false ;
		}
		
		if (this.getNomEvento() == null) 
		{
			vErro.addElement("Descri��o do Evento  n�o preenchida" +" \n") ;
			bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.EventoUpdate(this) ; 
		}
		catch (Exception e) { 
			vErro.addElement("Evento  n�o atualizado: " + this.getCodEvento()+" \n") ;
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}

	/**
	 * M�todo para remover o Evento  da base de dados
	 * Valida se o Evento existe.
	 */
   
	public boolean isDelete() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodEvento() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.EventoExiste(this) == false) 
				{
					vErro.addElement("Evento  n�o existe: " +this.getCodEvento() + " - " + 
						this.getNomEvento()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Evento : "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodEvento() == null) 
		{
		vErro.addElement("C�digo do Evento  n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.EventoDelete(this) ; 
			vErro.addElement("Evento  excluido: " + this.getCodEvento()+"\n") ;
		}
		catch (Exception e) { 
			vErro.addElement("Evento  n�o excluido: " + this.getCodEvento()+" \n") ;
			vErro.addElement("Erro na exclus�o  : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}
	

	public Vector getEvento(int min,int livre)  {
	   try  {
 	
			Dao dao = Dao.getInstance();
			dao.getEvento(this);
			if (this.evento.size()<min-livre) 
				livre = min-evento.size() ;	  	
				for (int i=0;i<livre; i++) 	{
					this.evento.addElement(new EventoBean()) ;
			}
	   }
	   catch (Exception e) {
			setMsgErro("Evento007 - Leitura: " + e.getMessage());
	   }
	  return this.evento ;
	}
  
	public Vector getEvento()  {
	   return this.evento;
	}   
  
	public EventoBean getRelator(int i)  { 
	   return (EventoBean)this.evento.elementAt(i);
	}
 
	public void setEvento(Vector evento)  {
			this.evento = evento ;
	}  
 
	public EventoBean getEvento(int i)  { 
	   return (EventoBean)this.evento.elementAt(i);
	}
	
	public void setEventoGravado(Vector eventoGravado ){
		this.eventoGravado = eventoGravado;
	}
	public Vector getEventoGravado(){
		return this.eventoGravado;
	}
	
	public boolean isInsertEvento()   {
  	 boolean retorno = true;
  	 Vector vErro = new Vector() ;
	   // Verificada a valida��o dos dados nos campos
	   if( isValidEvento(vErro) )   {
			   try  {
//				TAB.Dao dao = TAB.Dao.getInstance();	
				   
				REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();	
			   if (daoTabela.InsertEvento(this,vErro))    {	
					   vErro.addElement("Evento "
							+this.nomEvento+" atualizados:"+ " \n");
				   }
				   else{		
						vErro.addElement("Evento "
							+this.nomEvento+" n�o atualizados:"+ " \n");     	   
				   }
			   }
	   catch (Exception e) {
			vErro.addElement("Evento "+this.nomEvento+" n�o atualizados:"+ " \n") ;
			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
			retorno = false;
	   }//fim do catch
	}
 else retorno = false;   
	setMsgErro(vErro);
	return retorno;
  }
   
  public boolean isValidEvento(Vector vErro) {
 	
	String sDesc = "";
	for (int i = 0 ; i< this.evento.size(); i++) {
	   TAB.EventoBean myEvento = (TAB.EventoBean)this.evento.elementAt(i) ;
	sDesc = myEvento.getCodEvento();
	if((sDesc.length()==0) ||("".equals(sDesc)))
	  continue ;
  		  
	  // verificar duplicata 
   for (int m = 0 ; m <this.evento.size(); m++) {
	   TAB.EventoBean myEventoOutro = (TAB.EventoBean)this.evento.elementAt(m) ;		
	if ((m!=i) && (myEventoOutro.getCodEvento().trim().length()!=0) && (myEventoOutro.getCodEvento().equals(sDesc))) {
		vErro.addElement("Evento em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
	}
   }
	}
	return (vErro.size() == 0) ;  
  }

 
}
