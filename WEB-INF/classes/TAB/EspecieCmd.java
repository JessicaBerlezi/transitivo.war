package TAB;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Carga da Tabela Especie <br>
* <b>Description:</b>  Registro - Carrega Tabela Especie<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luiz Medronho
* @version 1.0
*/
public class EspecieCmd {

	public EspecieCmd() {

	}

	public boolean carregaTab(Connection conn, sys.MonitorLog monitor) throws sys.DaoException {

		boolean ok;
		ok = true;
		int totalReg = 0;
		int totalLid = 0;
		int totalIns = 0;
		int totalUpd = 0;
		String parteVar = "";
		monitor.iniciaMonitor(
			"----------  Inicio da Carga de Especies  ----------");

		try {
			//Declaracao de variaveis
			String codEspecie = "";
			String dscEspecie = "";

			String codRetorno = "";
			String indCont = "";
			String resultTrans = "";
			boolean continua = true;
			String indContinua = "   ";
			DaoBroker daoBroker = new DaoBroker();
			
			while (continua) {
				//Criando instancia o bean 
				EspecieBean especieId = new EspecieBean();

				//Chamar  a Transa��o
				resultTrans = daoBroker.Transacao125("999","5", indContinua, conn);
				
				if (resultTrans.substring(0, 3).equals("ERR")) {
					continua = false;
					monitor.gravaMonitor(
						"     Erro: " + resultTrans.substring(4),monitor.MSG_ERRO);
				} else {
					if (resultTrans.substring(0, 3).equals("999")) {
						monitor.gravaMonitor(
							"     Erro: C�digo de Retorno da Transa��o: "
								+ resultTrans.substring(0, 3),monitor.MSG_ERRO);

					} else {
						String linha = resultTrans.substring(3);
						for (int j = 0; j < 89; j++) {

							if ((!linha.substring(j * 13, j * 13 + 3).trim().equals("")) 
								&& (!linha.substring(j * 13, j * 13 + 3).trim().equals("000"))) {
								codEspecie =
									linha.substring((13 * j), (13 * j) + 3);
								dscEspecie =
									linha.substring(
										(13 * j) + 3,
										(13 * j) + 13);
								especieId.setCodEspecie(codEspecie);
								especieId.setDscEspecie(dscEspecie);
								especieId.verificaOperacao(conn);
							}
							else continua = false;

						}
						// Verificar Indicador de Continuidade						
						indContinua = linha.substring(1157, 1160);
						if ((indContinua.trim().equals("")) || (indContinua.trim().equals("000"))) {
							continua = false;
						}
					}
				}
				totalIns = especieId.getTotalIns();
				totalUpd = especieId.getTotalAtu();
			}
		} catch (Exception e) {

			monitor.gravaMonitor("     Erro: " + e.getMessage(),monitor.MSG_ERRO);
		}
		monitor.gravaMonitor(
			"     Totaliza��o===> "
				+ " C�digos Inseridos: "
				+ totalIns
				+ ". C�digos Atualizados: "
				+ totalUpd,monitor.MSG_AVISO);
		monitor.finalizaMonitor(
			"----------    Fim da Carga de Especies    ----------");
		return ok;

	}
}