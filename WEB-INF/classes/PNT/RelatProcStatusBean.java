package PNT;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import sys.BeanException;
import sys.DaoException;

/**
* <b>Title:</b>        Pontua��o - Relatorio de Status <br>
* <b>Description:</b>  Informa o Status dos Processos no per�odo informado. <br>
* <b>Copyright:</b>    Copyright (c) 2007<br>
* <b>Company:</b>      MIND - Informatica <br>
* @author Wellem Lyra
* @version 1.0
*/

public class RelatProcStatusBean extends sys.HtmlPopupBean {

	
	private List<RelatProcStatusBean>   dados;
	private String codOrgao;
	private String todosStatus;	 
	private String sigOrgao;
	private String dataIni;
	private String dataFim;	
	private String codStatus;
	private String nomStatus;
	private String qtdProcStatus;

	public RelatProcStatusBean() throws sys.BeanException {
		dados        = new ArrayList<RelatProcStatusBean>();
		codOrgao     = "";
		todosStatus  = "N";
		sigOrgao     = "";
		dataIni      = formatedLast3Month().substring(0, 10);
		dataFim      = sys.Util.formatedToday().substring(0, 10);
		codStatus    = "";
		nomStatus    = "";
		qtdProcStatus= "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setTodosStatus(String todosStatus){
		if (todosStatus.equals("null"))  todosStatus="N";	
		else	
		this.todosStatus = todosStatus;
	}
		
	public String getTodosStatus(){
		return this.todosStatus;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}	
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}
	public String getNomStatus() {
		return this.nomStatus;
	}	
	
	public void setDados(List<RelatProcStatusBean> dados) {
		this.dados = dados;
	}
	public List<RelatProcStatusBean> getDados() {
		return this.dados;
	}
	
	public String getQtdProcStatus() {
		return qtdProcStatus;
	}

	public void setQtdProcStatus(String qtdProcStatus) {
		this.qtdProcStatus = qtdProcStatus;
		if (qtdProcStatus == null)
			this.qtdProcStatus = "";
	}
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaRegDiario (RelatProcStatusBean RelatProcStatusBean) throws  BeanException, DaoException{
			boolean existe = false;
			
			try {
				DaoBroker dao = DaoBrokerFactory.getInstance();
				dao.consultaStatusProc(this);
		  
			} 
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			} 
			return existe;	
		}


	public String formataIntero (String valor) throws  DaoException, BeanException{	
	  DecimalFormat formato = new DecimalFormat(",#00") ;
	  String valorFormatado = formato.format(Integer.parseInt(valor)); 
	  return valorFormatado;
	}

	public static String formatedLast3Month() {

		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -90);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;

	} // Fim formatedToday



}