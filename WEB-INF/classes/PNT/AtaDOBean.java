package PNT;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import REC.ParamOrgBean;
import sys.HtmlPopupBean;
import sys.Util;

/**
 * <b>Title:</b> SMIT - ATA de DO - AtaDO Bean<br>
 * <b>Description:</b> ATA de DO Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class AtaDOBean extends HtmlPopupBean {

    private String msgErro;
    private String msgOk;

    private String codAtaDO;
    private String codProcesso;
    private String numAtaDO;
    private String tipAtaDO;
    private String tipProcessosDO;
    private String codEnvioDO;
    
    
    private String codEnvioDOedital;
    
    
    private String datEDO;
    private String datEDOInicial;
    private String datEDOFinal;
    
    private String nomUserNameEDO;
    private String codOrgaoLotacaoEDO;
    private String datProcEDO;
    
    private String datEmissaoEDO;
    private String nomUserNameEmissaoEDO;
    private String codOrgaoLotacaoEmissaoEDO;
    private String datProcEmissaoEDO;

    private String datPublAta;
    private String nomUserNamePDO;
    private String codOrgaoLotacaoPDO;
    private String datProcPDO;

    
    private String datEstorno;
    private String nomUserNameEST;
    private String codOrgaoLotacaoEST;
    private String datProcEST;

    private String nomUserNameDownload;
    private String codOrgaoLotacaoDownload;
    private String codOrgaoAtuacaoDownload;
    private String codArquivoDownload;
    private String nomArquivoDownload;
    private String statusArquivoDownload;
    private String codEventoDownload;
    
    private String maxProcessos;
    private String statusElegiveis;
    private String statusDestino;
    private String datReferencia;
    private String datReferenciaFinal;
    private String indAplicarPenalidade;
    private String txtParagrago_01;
    private String txtParagrago_02;    
    private String qtdProc; 
    private String statusAta;
    private String statusPublic;
    
    private List <ProcessoBean>processos;  
	
    private List eventos;

    
    public AtaDOBean() throws sys.BeanException {

        msgErro            = "";
        msgOk              = "";

        codAtaDO           = "";
        codProcesso        = "";
        numAtaDO           = "";
        tipAtaDO           = "";
        tipProcessosDO     = "";
        datEDO             = "";
        datEDOInicial      = sys.Util.lPad(sys.Util.formataDataYYYYMMDD("01/01/2007"),"0",8) ;
        datEDOFinal        = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(Util.formatedToday().substring(0,10)),"0",8) ;
        codEnvioDO         = "";
        
        codEnvioDOedital   = "";
        nomUserNameEDO     = "";
        codOrgaoLotacaoEDO = "";
        datProcEDO         = "";
        datEmissaoEDO      = Util.formatedToday().substring(0,10);
        nomUserNameEmissaoEDO     = "";
        codOrgaoLotacaoEmissaoEDO = "";
        datProcEmissaoEDO  = Util.formatedToday().substring(0,10);
        datPublAta         = "";
        nomUserNamePDO     = "";
        codOrgaoLotacaoPDO = "";
        datProcPDO         = "";

        datEstorno         = Util.formatedToday().substring(0,10);
        nomUserNameEST     = "";
        codOrgaoLotacaoEST = "";
        datProcEST         = "";
        
        nomUserNameDownload     = "";
        codOrgaoLotacaoDownload = "";
        codOrgaoAtuacaoDownload = "";
        codArquivoDownload      = "";
        nomArquivoDownload      = "";
        statusArquivoDownload   = "";
        codEventoDownload       = "";

        maxProcessos       = "300";
        statusElegiveis    = "001,002";
        statusDestino      = "010";
        datReferencia      = "";
        datReferenciaFinal = "";
        indAplicarPenalidade      = "S";
        txtParagrago_01    = "";
        txtParagrago_02    = "";
        qtdProc            = "";
        statusAta		   ="9";
        statusPublic       ="0";
        
        processos       = new ArrayList <ProcessoBean>() ;
		eventos         = new ArrayList();

        
    }

    public void setJ_sigFuncao(String sFuncao) {
    	this.tipAtaDO="0";     /*Defesa*/
    	if ("PNT0461,PNT0462,PNT0463,PNT0464,PNT0466,PNT0467,PNT0468".indexOf(sFuncao)>=0)
    		this.tipAtaDO="2"; //Recurso
    	if ("PNT1101,PNT1102,PNT1103,PNT1104,PNT1106,PNT1107".indexOf(sFuncao)>=0)
			this.tipAtaDO="4"; //Entrega
    }
    
    
    public String getDatEstorno() {
		return datEstorno;
	}

	public void setDatEstorno(String datEstorno) {
		if (datEstorno==null) datEstorno="";
		this.datEstorno = datEstorno;
	}

	public void setMsgErro(Vector vErro) {
        for (int j = 0; j < vErro.size(); j++) {
            this.msgErro += vErro.elementAt(j);
        }
    }
    public void setMsgErro(String msgErro) {
        if (msgErro == null)  msgErro = "";
    	this.msgErro = msgErro;
    }
    public String getMsgErro() {
        return this.msgErro;
    }

    public String getMsgOk() {
		return msgOk;
	}

	public void setMsgOk(String msgOk) {
		if (msgOk==null) msgOk="";
		this.msgOk = msgOk;
	}

	public void setCodAtaDO(String codAtaDO) {
        if (codAtaDO == null)  codAtaDO = "";
    	this.codAtaDO = codAtaDO;
    }
    public String getCodAtaDO() {
        return this.codAtaDO;
    }

    public void setCodProcesso(String codProcesso) {
        if (codProcesso == null) codProcesso = "";
    	this.codProcesso = codProcesso;
    }
    public String getCodProcesso() {
        return this.codProcesso;
    }

    // --------------------------------------------------------------------------
    public void setNumAtaDO(String numAtaDO) {
        if (numAtaDO == null) numAtaDO = "";
    	this.numAtaDO = numAtaDO;
    }
    public String getNumAtaDO() {
        return this.numAtaDO;
    }
    public void setTipAtaDO(String tipAtaDO) {
        if (tipAtaDO == null) tipAtaDO = "";
    	this.tipAtaDO = tipAtaDO;
    }
    public String getTipAtaDO() {
        return this.tipAtaDO;
    }
    public String getTipAtaDODesc() {
    	String desc = "";
    	if ("0".equals(this.tipAtaDO)) desc = "Autuações";
    	if ("1".equals(this.tipAtaDO)) desc = "Defesas Indeferidas";
    	if ("2".equals(this.tipAtaDO)) desc = "Penalidades";
    	if ("3".equals(this.tipAtaDO)) desc = "Recursos Indeferidos";
    	if ("4".equals(this.tipAtaDO)) desc = "Solic. Entrega de CNH";
        return desc;
    }

    
    public String getTipAtaEditalDODesc() {
    	String desc = "";
    	if ("0".equals(this.tipAtaDO)) desc = "Notificação de Instauração de Processo de Suspensão do Exercício do Direito de Dirigir";
    	if ("2".equals(this.tipAtaDO)) desc = "Notificação de Penalidade de Suspensão do Exercício do Direito de Dirigir";
    	if ("4".equals(this.tipAtaDO)) desc = "Notificação de Entrega de CNH";
        return desc;
    }
            
    // --------------------------------------------------------------------------
    public void setDatEDO(String datEDO) {
        if (datEDO == null)  datEDO = "";
    	this.datEDO = datEDO;
    }
    public String getDatEDO() {
        return this.datEDO;
    }
    public void setDatEDOInicial(String datEDOInicial) {
        if (datEDOInicial == null)  datEDOInicial = sys.Util.lPad(sys.Util.formataDataYYYYMMDD("01/01/2007"),"0",8);
    	this.datEDOInicial = datEDOInicial;
    }
    public String getDatEDOInicial() {
        return this.datEDOInicial;
    }

    public void setDatEDOFinal(String datEDOFinal) {
        if (datEDOFinal == null)  datEDOFinal = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(Util.formatedToday().substring(0,10)),"0",8) ;
    	this.datEDOFinal = datEDOFinal;
    }
    public String getDatEDOFinal() {
        return this.datEDOFinal;
    }

    public void setCodEnvioDO(String codEnvioDO) {
        if (codEnvioDO == null)  codEnvioDO = "";
    	this.codEnvioDO = codEnvioDO;
    }
    public String getCodEnvioDO() {
        return this.codEnvioDO;
    }

    public void setCodEnvioDOedital(String codEnvioDOedital) {
        if (codEnvioDOedital == null)  codEnvioDOedital = "";
    	this.codEnvioDOedital = codEnvioDOedital;
    }
    public String getCodEnvioDOedital() {
        return this.codEnvioDOedital;
    }
    
    public void setNomUserNameEDO(String nomUserNameEDO) {
        if (nomUserNameEDO == null) nomUserNameEDO = "";
    	this.nomUserNameEDO = nomUserNameEDO;
    }
    public String getNomUserNameEDO() {
        return this.nomUserNameEDO;
    }
    public void setCodOrgaoLotacaoEDO(String codOrgaoLotacaoEDO) {
        if (codOrgaoLotacaoEDO == null) codOrgaoLotacaoEDO = "";
    	this.codOrgaoLotacaoEDO = codOrgaoLotacaoEDO;
    }
    public String getCodOrgaoLotacaoEDO() {
        return this.codOrgaoLotacaoEDO;
    }
    public void setDatProcEDO(String datProcEDO) {
        if (datProcEDO == null) datProcEDO = "";
    	this.datProcEDO = datProcEDO;
    }
    public String getDatProcEDO() {
        return this.datProcEDO;
    }    
    // --------------------------------------------------------------------------
    public void setDatPublAta(String datPublAta) {
        if (datPublAta == null)  datPublAta = "";
    	this.datPublAta = datPublAta;
    }
    public String getDatPublAta() {
        return this.datPublAta;
    }
    public void setNomUserNamePDO(String nomUserNamePDO) {
        if (nomUserNamePDO == null) nomUserNamePDO = "";
    	this.nomUserNamePDO = nomUserNamePDO;
    }
    public String getNomUserNamePDO() {
        return this.nomUserNamePDO;
    }
    public void setCodOrgaoLotacaoPDO(String codOrgaoLotacaoPDO) {
        if (codOrgaoLotacaoPDO == null) codOrgaoLotacaoPDO = "";
    	this.codOrgaoLotacaoPDO = codOrgaoLotacaoPDO;
    }
    public String getCodOrgaoLotacaoPDO() {
        return this.codOrgaoLotacaoPDO;
    }
    public void setDatProcPDO(String datProcPDO) {
        if (datProcPDO == null) datProcPDO = "";
    	this.datProcPDO = datProcPDO;
    }
    public String getDatProcPDO() {
        return this.datProcPDO;
    }        
    public void setMaxProcessos(String maxProcessos) {
        if (maxProcessos == null) maxProcessos = "300";
    	this.maxProcessos = maxProcessos;
    }
    public String getMaxProcessos() {
        return this.maxProcessos;
    }
    public void setStatusElegiveis(String statusElegiveis) {
        if (statusElegiveis == null) statusElegiveis = "001,002";
    	this.statusElegiveis = statusElegiveis;
    }
    public String getStatusElegiveis() {
        return this.statusElegiveis;
    }
    public void setStatusDestino(String statusDestino) {
        if (statusDestino == null) statusDestino = "001,002";
    	this.statusDestino = statusDestino;
    }
    public String getStatusDestino() {
        return this.statusDestino;
    }   
    public void setDatReferencia(String datReferencia) {
        if (datReferencia == null) datReferencia = "";
    	this.datReferencia = datReferencia;
    }
    public String getDatReferencia() {
        return this.datReferencia;
    }        
    public void setDatReferenciaFinal(String datReferenciaFinal) {
        if (datReferenciaFinal == null) datReferenciaFinal = "";
    	this.datReferenciaFinal = datReferenciaFinal;
    }
    public String getDatReferenciaFinal() {
        return this.datReferenciaFinal;
    }        

    public void setProcessos (List<ProcessoBean> processos) {
		if (processos==null) processos= new ArrayList<ProcessoBean>();
		this.processos = processos;
	}
	public List<ProcessoBean> getProcessos() 	{
		return this.processos;
	}
	public ProcessoBean getProcessos(Integer i) 	{
		return this.processos.get(i);
	} 
	
	
	public void setEventos(List eventos) {
		this.eventos = eventos;
	}	
	
	public List getEventos() {
		return this.eventos;
	}

	public AtaDOBean getEventos(int i){
		   return (AtaDOBean)this.eventos.get(i);
	}

    // --------------------------------------------------------------------------

	public void LeAtas()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeAtasDO(this);		   
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
	
	
	public void GravaAtaDO(Connection conn) throws sys.BeanException {
  	  try {
  		//  DaoBroker dao = DaoBrokerFactory.getInstance();
  		//  		  dao.GravaAtaDO(this,conn);		   
  	  }
  	  catch (Exception ex) {
  		  throw new sys.BeanException(ex.getMessage());
  	  }		
    }

	public void AtualizaDataPublicacaoDO() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.AtualizaDataPublicacaoDO(this);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}
	
	public void AtualizaEditalDO() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.AtualizaEditalDO(this);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}
	
	public String getAnoDatEdo() {
		return getDatEDO().substring(6,10);
	}

	public String getMesDatEdo() {
		return Util.mesPorExtenso(getDatEDO().substring(3,5));
	}

	public String getDiaDatEdo() {
		return getDatEDO().substring(0,2);
	}
	
	public String getCodOrgaoLotacaoEmissaoEDO() {
		return codOrgaoLotacaoEmissaoEDO;
	}
	public void setCodOrgaoLotacaoEmissaoEDO(String codOrgaoLotacaoEmissaoEDO) {
		if (codOrgaoLotacaoEmissaoEDO==null) codOrgaoLotacaoEmissaoEDO="";
		this.codOrgaoLotacaoEmissaoEDO = codOrgaoLotacaoEmissaoEDO;
	}

	public String getDatEmissaoEDO() {
		return datEmissaoEDO;
	}
	public void setDatEmissaoEDO(String datEmissaoEDO) {
		if (datEmissaoEDO==null) datEmissaoEDO="";
		this.datEmissaoEDO = datEmissaoEDO;
	}

	public String getDatProcEmissaoEDO() {
		return datProcEmissaoEDO;
	}
	public void setDatProcEmissaoEDO(String datProcEmissaoEDO) {
		if (datProcEmissaoEDO==null) datProcEmissaoEDO="";
		this.datProcEmissaoEDO = datProcEmissaoEDO;
	}

	public String getNomUserNameEmissaoEDO() {
		return nomUserNameEmissaoEDO;
	}
	public void setNomUserNameEmissaoEDO(String nomUserNameEmissaoEDO) {
		if (nomUserNameEmissaoEDO==null) nomUserNameEmissaoEDO="";
		this.nomUserNameEmissaoEDO = nomUserNameEmissaoEDO;
	}

	public String getIndAplicarPenalidade() {
		return indAplicarPenalidade;
	}

	public void setIndAplicarPenalidade(String indAplicarPenalidade) {
		if (indAplicarPenalidade==null) indAplicarPenalidade="";
		this.indAplicarPenalidade = indAplicarPenalidade;
	}

	public String getTipProcessosDO() {
		return tipProcessosDO;
	}

	public void setTipProcessosDO(String tipProcessosDO) {
		if (tipProcessosDO==null) tipProcessosDO="";
		this.tipProcessosDO = tipProcessosDO;
	}

	public String getTxtParagrago_01() {
		return txtParagrago_01;
	}

	public void setTxtParagrago_01(String txtParagrago_01) {
		if (txtParagrago_01==null) txtParagrago_01="";
		this.txtParagrago_01 = txtParagrago_01;
	}

	public String getTxtParagrago_02() {
		return txtParagrago_02;
	}

	public void setTxtParagrago_02(String txtParagrago_02) {
		if (txtParagrago_02==null) txtParagrago_02="";
		this.txtParagrago_02 = txtParagrago_02;
	}
	

	public String getCodOrgaoLotacaoEST() {
		return codOrgaoLotacaoEST;
	}

	public void setCodOrgaoLotacaoEST(String codOrgaoLotacaoEST) {
		if (codOrgaoLotacaoEST==null) codOrgaoLotacaoEST="";
		this.codOrgaoLotacaoEST = codOrgaoLotacaoEST;
	}

	public String getDatProcEST() {
		return datProcEST;
	}

	public void setDatProcEST(String datProcEST) {
		if (datProcEST==null) datProcEST="";
		this.datProcEST = datProcEST;
	}

	public String getNomUserNameEST() {
		return nomUserNameEST;
	}

	public void setNomUserNameEST(String nomUserNameEST) {
		if (nomUserNameEST==null) nomUserNameEST="";
		this.nomUserNameEST = nomUserNameEST;
	}
	
	public String getCodOrgaoAtuacaoDownload() {
		return codOrgaoAtuacaoDownload;
	}

	public void setCodOrgaoAtuacaoDownload(String codOrgaoAtuacaoDownload) {
		if (codOrgaoAtuacaoDownload==null) codOrgaoAtuacaoDownload="";
		this.codOrgaoAtuacaoDownload = codOrgaoAtuacaoDownload;
	}

	public String getCodOrgaoLotacaoDownload() {
		return codOrgaoLotacaoDownload;
	}

	public void setCodOrgaoLotacaoDownload(String codOrgaoLotacaoDownload) {
		if (codOrgaoLotacaoDownload==null) codOrgaoLotacaoDownload="";
		this.codOrgaoLotacaoDownload = codOrgaoLotacaoDownload;
	}

	public String getNomUserNameDownload() {
		return nomUserNameDownload;
	}

	public void setNomUserNameDownload(String nomUserNameDownload) {
		if (nomUserNameDownload==null) nomUserNameDownload="";
		this.nomUserNameDownload = nomUserNameDownload;
	}

	public String getCodArquivoDownload() {
		return codArquivoDownload;
	}

	public void setCodArquivoDownload(String codArquivoDownload) {
		if (codArquivoDownload==null) codArquivoDownload="";
		this.codArquivoDownload = codArquivoDownload;
	}

	public String getNomArquivoDownload() {
		return nomArquivoDownload;
	}

	public void setNomArquivoDownload(String nomArquivoDownload) {
		if (nomArquivoDownload==null) nomArquivoDownload="";
		this.nomArquivoDownload = nomArquivoDownload;
	}

	public String getStatusArquivoDownload() {
		return statusArquivoDownload;
	}

	public void setStatusArquivoDownload(String statusArquivoDownload) {
		if (statusArquivoDownload==null) statusArquivoDownload="";
		this.statusArquivoDownload = statusArquivoDownload;
	}

	public String getCodEventoDownload() {
		return codEventoDownload;
	}

	public void setCodEventoDownload(String codEventoDownload) {
		if (codEventoDownload==null) codEventoDownload="";
		this.codEventoDownload = codEventoDownload;
	}
	
	public Vector ConsultaAtaDownload(String dataIni, String dataFim) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			return dao.ConsultaAtaDownload(this,dataIni,dataFim);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}
	
	public void DownloadArquivoAta() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.DownloadArquivoAta(this);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}
	
	public boolean gravaArquivo(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId,UsuarioBean UsrLogado) throws Exception{
		boolean ok=false;
		try{ 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			ok=dao.gravaArquivo(this, paramSys,  ParamOrgaoId, UsrLogado);				
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		
		return ok;
	}
	public String getQtdProc() {
		return qtdProc;
	}

	public void setQtdProc(String qtdProc) {
		if (qtdProc==null) qtdProc="";
		this.qtdProc = qtdProc;
	}

	 public void setStatusAta(String statusAta) {
	        if (statusAta == null) statusAta = "0";
	    	this.statusAta = statusAta;
	    }
	    public String getStatusAta() {
	        return this.statusAta;
	    }
	    public List<AtaDOBean> consultaAtas(AtaDOBean myAta) throws Exception{
	    	List <AtaDOBean>atas = new ArrayList<AtaDOBean>();
			try{ 
				DaoBroker dao = DaoBrokerFactory.getInstance();
				atas=dao.ConsultaAtasDO(myAta);				
			}
			catch(Exception e){
				throw new Exception(e.getMessage());
			}
			
			return atas;
		}

		public String getStatusPublic() {
			return statusPublic;
		}

		public void setStatusPublic(String statusPublic) {
			if (statusPublic==null) statusPublic="0";
			this.statusPublic = statusPublic;
		}
	
}