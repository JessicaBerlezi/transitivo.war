package PNT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.NOT.ArquivoBean;

public class ProcessaProcessoCmd extends sys.Command {
	private String next;
	
	private static final String jspPadrao = "/PNT/ProcessoAbre.jsp" ;
	
	public ProcessaProcessoCmd() {
		next = jspPadrao;
	}
	
	public ProcessaProcessoCmd(String next) {
		this.next = next;
	}
	
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)	UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (UsuarioFuncBeanId == null)	UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();			
			
			ProcessoBean ProcessoBeanId = (ProcessoBean) session.getAttribute("ProcessoBeanId");
			if (ProcessoBeanId == null) ProcessoBeanId = new ProcessoBean();				
			
			// obtem e valida os parametros Recebidos
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
			} 
			if (acao.equals("processoMostra")) {   				
				  ProcessoBeanId = new ProcessoBean();
				  ProcessoBeanId.setNumProcesso(req.getParameter("processoMostra"));				
				  ProcessoBeanId.LeProcesso();
				  ProcessoBeanId.LeRequerimentos();		
				  ProcessoBeanId.LeARDigitalizado();
				  session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				  nextRetorno = "/PNT/ProcessoMostra.jsp" ;	  
			}		 										
			if (acao.equals("processoLe")) {
				ProcessoBeanId = new ProcessoBean();				
				nextRetorno = processoLe(req,ProcessoBeanId,UsrLogado) ;				
				ProcessoBeanId.LeRequerimentos();
				ProcessoBeanId.LeARDigitalizado();				
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			}	
			// mostra o Ato de Puni��o � Revelia - 11/12/2012
			if (acao.equals("atoDePunicao")){
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/NOT/AtoDePunicao.jsp";
			}
			// mostra Multas do Processo
			if (acao.equals("multasMostra")) {
				// montar lista de Multas 
				if (ProcessoBeanId.getMultas().size()==0) ProcessoBeanId.LeMultas();
				nextRetorno = "/PNT/MultasMostra.jsp";
			}
			// mostra Proc Ant do Processo
			if (acao.equals("ProcAntMostra")) {
				// montar lista de Proc Ant 
				if (ProcessoBeanId.getProcAnteriores().size()==0) ProcessoBeanId.LeProcessoAnts();
				nextRetorno = "/PNT/ProcAntMostra.jsp";
			}			
			// mostra requerimentos do Processo
			if (acao.equals("requerimentosMostra")) {
				// montar lista de requerimentos 
				if (ProcessoBeanId.getRequerimentos().size()==0) ProcessoBeanId.LeRequerimentos();
				nextRetorno = "/PNT/RequerimentosMostra.jsp";
			}
			// mostra historicos do Processo
			if (acao.equals("historicosMostra")) {
				// montar lista de Historicos 
				if (ProcessoBeanId.getHistoricos().size()==0) ProcessoBeanId.LeHistoricos();
				nextRetorno = "/PNT/HistoricosMostra.jsp";
			}
			
			// mostra ARs do Processo
			if (acao.equals("ARMostra")) {
				// montar lista de ARs
				ProcessoBeanId.LeARDigitalizado();
				nextRetorno = "/PNT/VisARDig.jsp";
			}
	
			if  (acao.equals("processoImprime"))  {							
				ProcessoBeanId.LeProcesso();		
				String tituloConsulta = "CONSULTAR PROCESSO  ";
				session.setAttribute("tituloConsulta", tituloConsulta);
				nextRetorno = "/PNT/ProcessoImprime.jsp" ;
			}						

			// imprime 2 via da notificacoes
			if (acao.equals("notifImprime")) {	
				if (ProcessoBeanId.getProcAnteriores().size()==0) ProcessoBeanId.LeProcessoAnts();
				if (ProcessoBeanId.getNotificacoes().size()==0)   ProcessoBeanId.LeNotificacoes();
				if (ProcessoBeanId.getMultas().size()==0)         ProcessoBeanId.LeMultas();
				ArquivoBean myArquivo = new ArquivoBean();
	 			myArquivo.setCodArquivo(ProcessoBeanId.getCodArquivo());	
	 			myArquivo.getListaProcessos().add(ProcessoBeanId);
	 			session.setAttribute("ArquivoBeanID", myArquivo);
				nextRetorno="/PNT/NOT/ImprimeNotifImp.jsp";	 							
			}			
			// Imprime Requerimento do Processo
			if  (acao.equals("requerimentoImprime"))  {								
				if (ProcessoBeanId.getRequerimentos().size()==0) ProcessoBeanId.LeRequerimentos();
				nextRetorno = "/PNT/RequerimentoImprime.jsp" ;
			}							
//			Imprime Historico do Processo
			if  (acao.equals("historicoImprime"))  {								
				if (ProcessoBeanId.getHistoricos().size()==0) ProcessoBeanId.LeHistoricos();
				nextRetorno = "/PNT/HistoricoImprime.jsp" ;
			}			  		  
			
			if  (acao.equals("multaImprime"))  {							
				if (ProcessoBeanId.getMultas().size()==0) 
					ProcessoBeanId.LeMultas();
				nextRetorno = "/PNT/MultasImprime.jsp";
			}	
			
			
			if  (acao.equals("VisARDigImagem"))  {
				    String codARDigitalizado = req.getParameter("codARDigitalizado");
					ProcessoBeanId.setCodARDigitalizado(codARDigitalizado);				    
					ProcessoBeanId.LeARDigitalizado();	
					nextRetorno = "/PNT/VisARDigImagem.jsp" ;
			}														
				
		} catch (Exception ue) {
			throw new sys.CommandException("ProcessaProcessoCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

	protected String processoLe(HttpServletRequest req,ProcessoBean myProcesso,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		String nextRetorno=jspPadrao;		
		try { 		
			myProcesso.setNumProcesso(req.getParameter("numProcesso"));		
			myProcesso.setNumCPFEdt(req.getParameter("numCpf"));
			if ( (myProcesso.getNumProcesso().length()==0) || (myProcesso.getNumCPF().length()==0)) {
				req.setAttribute("semProcesso","N�O FORNECIDO CPF OU NUM. PROCESSO.");
			}
			else { 
				myProcesso.LeProcesso("Processo/CPF");  // // CPF e NUM PROCESSO
				if (myProcesso.getNumProcesso().length()==0) {
					req.setAttribute("semProcesso","PROCESSO  N�O LOCALIZADO COM OS PARAMETROS FORNECIDOS");
				}
			}
			if  (myProcesso.getNumProcesso().length()>0) nextRetorno = "/PNT/ProcessoMostra.jsp" ;
		} catch (Exception e){
			throw new sys.CommandException("ConsultaProcessoCmd - processoLe: " + e.getMessage());
		}
		return nextRetorno  ;
	  }	
	
	
}