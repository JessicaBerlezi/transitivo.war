package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Tabelas - Manutencao de Agravo<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Agravos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	   MIND Informatica <br>
 * @author 			   Marlon Falzetta.	
 * <b>Revised by</b>   Wellem Lyra
 * @version 1.1
 */

public class AgravoCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Agravo.jsp";    
	public AgravoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {	 
		
		String nextRetorno  = jspPadrao ;	
		HttpSession session   = req.getSession() ;	
		try {    
		    ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;			
	    	
			// Declara��o de vari�veis
			List<String> vErro = new ArrayList<String>() ;
			AgravoBean AgravoId = new AgravoBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todas as Agravos
				AgravoId.carregar(20,5);
				
				req.setAttribute("AgravoId", AgravoId);
			}				
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codInfracao = req.getParameterValues("codInfracao");
				if(codInfracao == null) codInfracao = new String[0];  
				
				String[] Agravo = req.getParameterValues("Agravo");
				if(Agravo == null)  Agravo = new String[0];  				
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codInfracao,Agravo,vErro) ;
				
				// Caso nao tenha erro as opera��es necessarias s�o realizadas
				if (vErro.size()==0) {
					
					List <AgravoBean> lista = new ArrayList<AgravoBean>(); 
					for (int i = 0; i < codInfracao.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto AgravoBean.
						AgravoBean myAgravo = new AgravoBean() ;
						if(! codInfracao[i].equals(""))							
						myAgravo.setCodInfracao(codInfracao[i]);
						myAgravo.setAgravo(Agravo[i]);																	
						myAgravo.setCodOrgaoLotacao(codOrgaoLotacao);
						myAgravo.setNomUserName(nomUsername);					
						lista.add(myAgravo) ;				
					}					
					// Instancio um objeto com os novos registros.
					AgravoBean agravosAlterados = new AgravoBean();
					agravosAlterados.setAgravos(lista) ;					
					// Instancio um objeto com os registros gravados no banco.
					AgravoBean agravosCadastradas = new AgravoBean();
					agravosCadastradas.carregar(20,5);				
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */					
					if(agravosAlterados.atualizar(agravosCadastradas))
						AgravoId.setMsgErro("Dados atualizados com sucesso.");
					else
						AgravoId.setMsgErro("N�o houve atualiza��o dos dados.");
					
					AgravoId.carregar(20,5);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("AgravoId",AgravoId) ;
				}
				else AgravoId.setMsgErro(vErro.toString()) ;  	      
			}			
			if(acao.equals("I")){
				AgravoId.carregar(0,0);
				req.setAttribute("AgravoId", AgravoId);
				nextRetorno = "/PNT/TAB/AgravoImp.jsp";
			}			
		}
		catch (Exception e) {
			throw new sys.CommandException("AgravoCmd: " + e.getMessage());
		}
		return nextRetorno;
	}	
	private void isParametros(String[] codInfracao, String[] Agravo,List<String> vErro) {				 
		if ((codInfracao.length != Agravo.length))
			vErro.add("Parametros inconsistentes") ;		
	}	
}