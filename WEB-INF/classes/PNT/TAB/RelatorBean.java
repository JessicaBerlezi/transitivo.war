package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.Util;
import PNT.DEF.FuncionalidadesBean;
import PNT.TAB.Dao;
import PNT.TAB.DaoFactory;
import PNT.TAB.RelatorBean;
import PNT.TAB.JuntaBean;

/**
 * <b>Title:</b>        	SMIT-PNT - Bean de Relator <br>
 * <b>Description:</b>  	Bean dados dOs relatores - Tabela de Relator<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author  				Luciana Rocha
 * @version 				1.0
 */
public class RelatorBean extends TabelaBean { 	
	private String codRelator;
	private String nomRelator;
	private String numCpf;
	private String codJunta;	
	private String sigJunta;
	private String nomJunta;
	private String codOrgao;
	private String nomTitulo;
	private String indAtivo;	
	private List<RelatorBean> relatores;
	
	private int processoPorGuia = 0;
	private int qtdProcDistrib = 0;	
	
	
	public RelatorBean()  throws sys.BeanException {		
		super();
		setTabela("TPNT_RELATOR");	
		setPopupWidth(35);
		codRelator	= "";	  	
		nomRelator	= "";
		numCpf	    = "";		
		codJunta	= "";
		sigJunta    = "";
		nomJunta    = "";
		codOrgao    = "";
		nomTitulo	= "";
		indAtivo    = "";		
		relatores   = new ArrayList<RelatorBean>();		
	}	
	
	public int getProcessoPorGuia() {
		return processoPorGuia;
	}
	public void setProcessoPorGuia(int iProcessoPorGuia) {
		this.processoPorGuia = iProcessoPorGuia;
	}
	public int getQtdProcDistrib() {
		return qtdProcDistrib;
	}
	public void setQtdProcDistrib(int qtdProcDistrib) {
		this.qtdProcDistrib = qtdProcDistrib;
	}
	public void setCodRelator(String codRelator)  {
		this.codRelator = codRelator ;
		if (codRelator == null) 
			this.codRelator ="" ;
	}
	public String getCodRelator()  {
		return this.codRelator;
	}	
	public void setNomRelator(String nomRelator){
		this.nomRelator = nomRelator;
		if (nomRelator == null)
			this.nomRelator = ""; 
	}
	public String getNomRelator()  {
		return this.nomRelator;
	}
	public void setNumCpf(String numCpf)  {
		this.numCpf = numCpf ;
		if (numCpf == null)	 this.numCpf="" ;
	}  
	public String getNumCpf()  {
		return this.numCpf;
	}	
	public void setNumCpfEdt(String numCpf)  {
		if (numCpf==null)      numCpf = "";
		if (numCpf.length()<14) numCpf += "               ";
		this.numCpf=numCpf.substring(0,3)
		+ numCpf.substring(4,7) 
		+ numCpf.substring(8,11)
		+ numCpf.substring(12,14);
	}  
	public String getNumCpfEdt()  {
		if ("".equals(this.numCpf.trim())) return this.numCpf.trim() ;
		return this.numCpf=numCpf.substring(0,3)+ "." + numCpf.substring(3,6) 
		+ "." + numCpf.substring(6,9) + "-" + numCpf.substring(9,11) ;
	}	
	public void setCodJunta(String codJunta)  {
		this.codJunta = codJunta ;
		if (codJunta == null) 
			this.codJunta = "" ;
	}  
	public String getCodJunta()  {
		return this.codJunta;
	}	
	public void setSigJunta(String sigJunta){
		this.sigJunta = sigJunta;
		if (sigJunta == null)
			this.sigJunta = ""; 
	}
	public String getSigJunta()  {
		return this.sigJunta;
	}	
	public void setNomJunta(String nomJunta){
		this.nomJunta = nomJunta;
		if (nomJunta == null)
			this.nomJunta = ""; 
	}
	public String getNomJunta()  {
		return this.nomJunta;
	}	
	public void setCodOrgao(String codOrgao)  {
		this.codOrgao = codOrgao ;
		if (codOrgao == null) 
			codOrgao = "";
	}  
	public String getCodOrgao()  {
		return codOrgao;
	}	
	public String getNomTitulo()  {
		return this.nomTitulo;
	}	
	public void setNomTitulo(String nomTitulo)  {
		this.nomTitulo = nomTitulo ;
		if (nomTitulo == null) 
			this.nomTitulo = "";
	}	
	public String getIndAtivo() {
		return indAtivo;
	}
	public void setIndAtivo(String indAtivo) {
		this.indAtivo = indAtivo;
	}	
	public void setRelatores(List<RelatorBean> relatores){
		this.relatores = relatores;
		if (relatores == null) relatores = new ArrayList<RelatorBean>();
	}	
	public List<RelatorBean> getRelatores() {
		return this.relatores;
	}	
	public RelatorBean getRelatores(int i) {
		return (RelatorBean) this.relatores.get(i);
	}	
//	--------------------------------------------------------------------------	
	public void Le_Relator(String chave) throws sys.BeanException {
		try {
			Dao dao = DaoFactory.getInstance();	
			dao.RelatorLeBean(this,chave);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}		
	/**	
	 * M�todo utilizado para Carregar todos os Relatores cadastrados 
	 * <BR>na base de dados (SMIT_PNT.TPNT_RELATOR).	  
	 * @param min   - M�nimo de beans que ser�o instanciados.
	 * <BR>O padr�o � de 20 beans. Caso n�o tenha 20 registros no banco de dados
	 * estes beans ficaram vazios para a inser��o de novos dados e grava��o    na
	 * base de dados.  
	 * @param livre - Total de beans excedentes que ser�o inst�nciados. 
	 * <BR>           Estes beans ser�o utilizados para inserir novos Relatores. 
	 * <BR>O padr�o � que sejam inseridos 5 relatores por vez. 
	 * <br>
	 * <br>Estes par�metros est�o muito ligados a visualiza��o da tela(Relator.jsp).
	 * Quando a tela � carregada pela primeira vez, podemos inserir at� 20 registros
	 * de uma vez, ap�s 20 registros s� s�o apresentados 5 campos vazios (param 
	 * livre) para a inser��o de novos registros.
	 * 
	 * @throws sys.BeanException
	 */
	public void carregarRelatores(int min, int livre) throws sys.BeanException {		
		try {		
			/* Carregando todos os relatores cadastradas no banco */
			Dao daoRelatores = DaoFactory.getInstance();
			daoRelatores.carregarRelatores(this);					
			/* A tela vai ter campos vazios para a inclus�o de novos dados			
			 Com este trecho de c�digo, sao criadas instancias vazias
			 s� para exibir os campos vazios na interface gr�fica.        */
			if (this.relatores.size()<min-livre) 
				livre = min-relatores.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.relatores.add(new RelatorBean()) ;
			}
		}
		catch(Exception e){			
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	}	
	public boolean atualizar(RelatorBean relatoresCadastrados) throws sys.BeanException {	
		boolean atualizado=false;
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio	    												*/
			RelatorBean relatoresAlterados = this;				  	
			
			/*	Percorrendo as listas de relatores para identificar qdo um
			 relator deve ser inserido, alterado, exclu�do ou apenas mantido */
			for (int i=0 ; i< relatoresAlterados.getRelatores().size(); i++) {
				
				RelatorBean relCad = (RelatorBean)relatoresCadastrados.getRelatores().get(i);
				RelatorBean relAlt = (RelatorBean)relatoresAlterados.getRelatores().get(i);
				relAlt.setCodJunta(this.getCodJunta());
				
				// NOVOS 					
				if( relAlt.getCodRelator().equals("")
						&&(! relAlt.getNomRelator().equals(""))
						&&(! relAlt.getNumCpf().equals(""))
						&&(! relAlt.getNomTitulo().equals(""))
						&&(! relAlt.getIndAtivo().equals(""))){
					
					Dao daoRelator = DaoFactory.getInstance();			
					daoRelator.RelatorInsert(relAlt);
					atualizado = true;					
				}				
				// REMOVIDOS 
				if ( (! relAlt.getCodRelator().equals(""))
						&& relAlt.getNomRelator().equals("")
						&& relAlt.getNumCpf().equals("")
						&& relAlt.getNomTitulo().equals("")) { 
					
					Dao daoRelator = DaoFactory.getInstance();
					daoRelator.RelatorDelete(relCad);
					atualizado = true;
				} 				
				
				// RELATORES ALTERADOS (identificando e tratando)
				if(	(! relAlt.getCodRelator().equals("")) 
						&&(	
								( (! relAlt.getNomRelator().equals(relCad.getNomRelator())) 	&& (! relAlt.getNomRelator().equals("")) )
								||	( (! relAlt.getNumCpf().equals(relCad.getNumCpf())) 		&& (! relAlt.getNumCpf().equals("")) 	 )
								||	( (! relAlt.getNomTitulo().equals(relCad.getNomTitulo()))   && (! relAlt.getNomTitulo().equals(""))  )
								||	( (! relAlt.getIndAtivo().equals(relCad.getIndAtivo()))     && (! relAlt.getCodRelator().equals("")) )
						)
				){
					
					Dao daoRelator = DaoFactory.getInstance();								
					daoRelator.RelatorUpdate(relAlt);
					atualizado = true;
				}					
			}   		
		}catch(Exception e){								
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizado;
	}	
	
	/**-------------------------------------------------------------------
	 /*  C�digo Relativo a Junta (em REC)
	  /*--------------------------------------------------------------------
	   **/ 
	public List getJuntaRelator()  {
		List myJunta = null;
		try  {
			
			Dao daoTabela = DaoFactory.getInstance();	
			if( (myJunta = daoTabela.getRelator(codJunta)) == null )
				setMsgErro("Relator009 - Erro de Leitura Smit: ");
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public List getRelatorBroker()  {
		List myJunta = null;
		try  {
			Dao daoTabela = DaoFactory.getInstance();	
			if( (myJunta = daoTabela.getRelatorBroker(this)) == null )
				setMsgErro("Relator009 - Erro de Leitura Detran: ");
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public void getRelatores(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorId)  {
		try  {
			Dao daoTabela = DaoFactory.getInstance();	
			daoTabela.getRelatores(UsuarioBeanId,TipoJunta, RelatorId);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}

	public void getRelatoresAtivos(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta)  {
		try  {
			Dao daoTabela = DaoFactory.getInstance();
			daoTabela.getRelatoresAtivos(UsuarioBeanId,TipoJunta, this);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}
	
	public void getRelatoresAutomatica ()  {
		try  {
			Dao daoTabela = DaoFactory.getInstance();
			daoTabela.getRelatoresAutomatica(this);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}
	
	public void getRelatoresAutomatica (FuncionalidadesBean funcionalidadesBean)  {
		try  {
			Dao daoTabela = DaoFactory.getInstance();
			daoTabela.getRelatoresAutomatica(this,funcionalidadesBean);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}
	
	public boolean compareSmitBroker(List relatorSmit, List relatorBroker) throws BeanException{
		RelatorBean relator, relator_aux;
		boolean flag = true;
		if( (relatorSmit == null) || (relatorBroker == null) )
			return false;
		
		for(int pos=0;pos<relatorSmit.size();pos++,flag=true){
			relator = (RelatorBean)relatorSmit.get(pos);
			for(int i= 0;i<relatorBroker.size();i++){
				relator_aux = (RelatorBean)relatorBroker.get(i);
				if(Util.lPad(relator.getNumCpf(),"0",11).equals(relator_aux.getNumCpf())){
					if(pos != i){
						relator = (RelatorBean)relatorBroker.set(pos,relator_aux);
						relatorBroker.set(i,relator);
					}
					flag=false;
					break;
				}
			}
			if(flag){
				if(pos>=relatorBroker.size())
					continue;
				relator = (RelatorBean)relatorBroker.set(pos,new RelatorBean());
				relatorBroker.add(relator);
			}
		}
		if( relatorSmit.size() > relatorBroker.size() ){
			for(int pos = relatorBroker.size(); pos < relatorSmit.size(); pos++)
				relatorBroker.add(new RelatorBean());
		}
		if( relatorSmit.size() < relatorBroker.size() ){
			for(int pos = relatorSmit.size(); pos < relatorBroker.size(); pos++)
				relatorSmit.add(new RelatorBean());
		}
		if( relatorSmit.size() < 10 ){
			for(int pos=relatorSmit.size();pos < 10;pos++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		else{
			for(int i=0;i<5;i++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		return true;
	}
	
	public boolean isInsertRelatorBroker(List relatorSmit, List relatorSmit_aux, List relatorBroker, List relatorBroker_aux) throws BeanException   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		
		RelatorBean myRelatorSmit, myRelatorSmit_aux, myRelatorBroker, myRelatorBroker_aux;
		// Verificada a valida��o dos dados nos campos
		try  {
			Dao daoTabela = DaoFactory.getInstance();		   
			for(int pos=0; pos < relatorSmit.size();pos++){
				myRelatorSmit       = (RelatorBean)relatorSmit.get(pos);
				myRelatorSmit_aux   = (RelatorBean)relatorSmit_aux.get(pos);
				myRelatorBroker     = (RelatorBean)relatorBroker.get(pos);
				myRelatorBroker_aux = (RelatorBean)relatorBroker_aux.get(pos);	  		
				
				if( (myRelatorSmit.getNomRelator().equals(myRelatorSmit_aux.getNomRelator())) 
						&& (myRelatorSmit.getNumCpf().equals(myRelatorSmit_aux.getNumCpf())) 
						&& (myRelatorBroker.getNomRelator().equals(myRelatorBroker_aux.getNomRelator())) 
						&& (myRelatorBroker.getNumCpf().equals(myRelatorBroker_aux.getNumCpf())) )
					continue;
				
				if( (myRelatorSmit.getCodRelator().equals("")) 
						&& (myRelatorSmit.getNomRelator().equals("")) 
						&& ((myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ) )
					continue;
				
				if( (!myRelatorSmit.getCodRelator().equals("")) 
						&& (!myRelatorSmit.getNomRelator().equals("")) 
						&& (!myRelatorBroker.getCodRelator().equals("")) ){
					
					if( daoTabela.RelatorUpdateBroker(myRelatorBroker,myRelatorBroker_aux) ){
						
						if( !daoTabela.RelatorUpdate(myRelatorSmit) ){
							vErro.add("Erro: 001 | Altera��o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorUpdateBroker(myRelatorBroker_aux);
						}
					} else{
						vErro.add("Erro: 001 | Altera��o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				
				if( (myRelatorSmit.getCodRelator().equals("")) 
						&& (!myRelatorSmit.getNomRelator().equals("")) 
						&& (!myRelatorBroker.getCodRelator().equals("")) 
						&& (!myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ){
					
					if( daoTabela.RelatorUpdateBroker(myRelatorBroker,myRelatorBroker_aux) ){
						
						if( !daoTabela.RelatorInsert(myRelatorSmit) ){
							vErro.add("Erro: 002 | Inclus�o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorUpdateBroker(myRelatorBroker_aux);
						}
					}
					else{
						vErro.add("Erro: 002 | Altera��o no Detran Relator: "+myRelatorBroker.getNomRelator()+myRelatorBroker.getCodRelator() +"\n");
						retorno = false;
					}
				}
				
				if( (!myRelatorSmit.getCodRelator().equals("")) 
						&& (!myRelatorSmit.getNomRelator().equals("")) 
						&& ((myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11)))) ){
					
					myRelatorBroker.setCodRelator(myRelatorSmit.getCodRelator());
					if( daoTabela.RelatorInsertBroker(myRelatorBroker) ){
						
						if( !daoTabela.RelatorUpdate(myRelatorSmit) ){
							vErro.add("Erro: 003 | Altera��o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorDeleteBroker(myRelatorBroker);
						}
					} else{
						vErro.add("Erro: 003 | Inclus�o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				
				if( (myRelatorSmit.getCodRelator().equals("")) 
						&& (!myRelatorSmit.getNomRelator().equals("")) 
						&& ((myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11)))) ){
					
					if( daoTabela.RelatorInsert(myRelatorSmit) ){
						
						myRelatorBroker.setCodRelator(myRelatorSmit.getCodRelator());
						if( !daoTabela.RelatorInsertBroker(myRelatorBroker) ){
							vErro.add("Erro: 004 | Inclus�o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorDelete(myRelatorSmit);
						}
					}
					else{
						vErro.add("Erro: 004 | Inclus�o no Smit Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				
				if( (myRelatorSmit.getNomRelator().equals("")) 
						&& (myRelatorBroker.getNomRelator().equals(""))){
					
					boolean sucesso = true;
					if( (!myRelatorBroker.getCodRelator().equals("")) || (!myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ){
						
						if( !daoTabela.RelatorDeleteBroker(myRelatorBroker_aux) ){
							vErro.add("Erro: 005 | Exclus�o no Detran Relator: "+myRelatorBroker_aux.getNomRelator()+ "\n");
							sucesso = false;
							retorno = false;
						}
					}
					if(sucesso){
						if( !myRelatorSmit.getCodRelator().equals("") ){
							if( !daoTabela.RelatorDelete(myRelatorSmit) ){
								vErro.add("Erro: 005 | Exclus�o no Smit Relator: "+myRelatorSmit_aux.getNomRelator()+ "\n");
								if( !myRelatorBroker.getCodRelator().equals(""))
									daoTabela.RelatorInsertBroker(myRelatorBroker_aux);
								sucesso = false;
								retorno = false;
							}
						}
					}
					if(sucesso){
						relatorSmit.remove(pos);
						relatorSmit_aux.remove(pos);
						relatorBroker.remove(pos);
						relatorBroker_aux.remove(pos);
						pos--;
					}
				}
			}
		}
		catch (Exception e) {
			vErro.addElement("Erro na altera��o: "+ e.getMessage() +"\n");
			retorno = false;
		}//fim do catch
		setMsgErro(vErro);
		
		if(relatorSmit.size()<10){
			for(int i=relatorSmit.size(); i<10;i++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		
		return retorno;
	}
	
	public boolean isInsertRelator(List myRelator, List myRelator_aux)   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		try  {
			
			Dao daoTabela = DaoFactory.getInstance();	
			retorno = daoTabela.JuntaInsertRelator(vErro,myRelator,myRelator_aux);    	   
		}
		catch (Exception e) {
			vErro.addElement("Erro na altera��o: "+ e.getMessage() +"\n");
			retorno = false;
		}
		setMsgErro(vErro);
		return retorno;
	}
	
	
	public String getRelatores(JuntaBean myJunta){
		String myRelator = "" ; 
		try    {
			
			Dao dao = DaoFactory.getInstance();	
			myRelator = dao.getRelatores(myJunta) ;
		}
		catch(Exception e){	}	
		return myRelator ;
	}	
	
	
	
}