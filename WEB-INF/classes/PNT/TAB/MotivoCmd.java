package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
 * <b>Title:</b>        Tabelas - Motivo de Cancelamento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Motivos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 			    Wellem Lyra.	
 * @version 1.0
 */

public class MotivoCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Motivo.jsp";  
	public MotivoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = jspPadrao ;
		HttpSession session   = req.getSession() ;	
		try {
			
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;
			
	    	List<String> vErro = new ArrayList<String>() ;			
			
			// Declara��o de vari�veis
			MotivoBean MotivoId = new MotivoBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();			
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todos os eventos 
				MotivoId.carregar(20,5);
				
				req.setAttribute("MotivoId", MotivoId);
			}	
			
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codMotivo= req.getParameterValues("codMotivo");
				if(codMotivo == null) codMotivo = new String[0];  
				
				String[] dscMotivo = req.getParameterValues("dscMotivo");
				if(dscMotivo == null)  dscMotivo = new String[0]; 				
				/* ** FIM Recuperando os dados da tela ** */
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codMotivo,dscMotivo, vErro);
				
				if (vErro.size()==0) {
					
					List <MotivoBean> lista = new ArrayList<MotivoBean>(); 
					for (int i = 0; i < dscMotivo.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto MotivoelaBean.
						MotivoBean myMotivo = new MotivoBean() ;
						if(! codMotivo[i].equals(""))
							myMotivo.setCodMotivo(Integer.decode(codMotivo[i]));
							myMotivo.setDescricao(dscMotivo[i]);						
							myMotivo.setCodOrgaoLotacao(codOrgaoLotacao);
							myMotivo.setNomUserName(nomUsername);						
							lista.add(myMotivo) ;					
					}									
					
					// Instancio um objeto com os novos registros.
					MotivoBean motivosAlterados = new MotivoBean();
					motivosAlterados.setMotivos(lista) ;
					
					// Instancio um objeto com os registros gravados no banco.
					MotivoBean motivosCadastrados = new MotivoBean();
					motivosCadastrados.carregar(20,5);
					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					if(motivosAlterados.atualizar(motivosCadastrados))
						MotivoId.setMsgErro("Dados atualizados com sucesso.");
					else
						MotivoId.setMsgErro("N�o houve atualiza��o dos dados");
					
					MotivoId.carregar(20,5);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("MotivoId", MotivoId);	
				}
				else MotivoId.setMsgErro(vErro.toString());  	      
			}
			
			if(acao.equals("I")){
				
				MotivoId.carregar(0,0);
				req.setAttribute("MotivoId", MotivoId);
				nextRetorno = "/PNT/TAB/MotivoImp.jsp";
			}
			
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("MotivoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	private void isParametros(String[] codMotivo,String[] dscMotivo, List<String> vErro) {				 
		if (dscMotivo.length!=codMotivo.length) vErro.add("Parametros inconsistentes") ;		
	} 
	
}