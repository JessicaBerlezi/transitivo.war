package PNT.TAB;

import javax.servlet.http.HttpServletRequest;

import sys.BeanException;

/**
* <b>Title:</b>      Pontua��o - Manuten��o Tabela<br>
* <b>Description:</b>Permite alterar / inserir Contudo dos E-mail<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    Itc - Informatica Tecnologica e Cientifica<br>
* @author Sergio Junior
* @version 1.0
*/
public class ConteudoEmailCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/PNT/TAB/ConteudoEmail.jsp";

	public ConteudoEmailCmd() {
		next = jspPadrao;
	}

	public ConteudoEmailCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;

			// cria os Beans de Usuario e Perfil, se n�o existir
			ConteudoEmailBean conteudoEmail =(ConteudoEmailBean) req.getAttribute("conteudoEmail");
			if (conteudoEmail == null)	conteudoEmail = new ConteudoEmailBean();
		    
		 	//obtem e valida os parametros recebidos
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";

			String codEvento = req.getParameter("codEvento");
			if (codEvento == null) codEvento = "0";
			
		    String dscEvento = req.getParameter("dscEvento");
			if (dscEvento == null) dscEvento = "";
				
			String nomRemetente = req.getParameter("nomRemetente");
			if (nomRemetente == null)nomRemetente = "";

			String txtAssunto = req.getParameter("txtAssunto");
			if (txtAssunto == null) txtAssunto = "";

			String textConteudo = req.getParameter("textConteudo");
			if (textConteudo == null) textConteudo = "";	
			
			String login = req.getParameter("login");
			if (login == null) login ="";	
			
			String senha = req.getParameter("senha");
			if (senha == null) senha = "";		

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)atualizarDependente = "N";
				
		    conteudoEmail.setCodOrgao(codOrgao);
		    conteudoEmail.setCodEvento(codEvento);
		    conteudoEmail.setDscEvento(dscEvento);
		    
		    
		    //Ler orgao selecionado	
		    ACSS.OrgaoBean OrgaoBeanId = null;
		    try {
				OrgaoBeanId = new ACSS.OrgaoBean();
				OrgaoBeanId.Le_Orgao(codOrgao,0);
			} catch (BeanException e) {
				throw new sys.CommandException("ConteudoEmailCmd 001: " + e.getMessage());
			}
			
			if (acao.compareTo("busca") == 0) {
				
				if ((conteudoEmail.consultaConteudo()) == false)
					req.setAttribute("acao", "insert");
				else
					req.setAttribute("acao", "alter");
			}

			if (acao.compareTo("alter") == 0) {
				try {
					if (conteudoEmail
						.alteraConteudo(textConteudo, nomRemetente, txtAssunto,
						 login, senha)== true)
					    req.setAttribute("acao", "alter");
					conteudoEmail.setMsgErro("Dados Alterados com Sucesso!");
				} catch (Exception e1) {
					throw new sys.CommandException("Erro ao alterar dados: " + e1.getMessage());
				}
			}

			if (acao.compareTo("insert") == 0) {
				try {
					if (conteudoEmail
						.insereConteudo(textConteudo, nomRemetente, txtAssunto,
						 login, senha)== true)
					    req.setAttribute("acao", "alter");			    
					conteudoEmail.setMsgErro("Dados Inseridos com Sucesso!");
				} catch (Exception e1) {
					throw new sys.CommandException("Erro ao incluir dados: " + e1.getMessage());
				}
			}

            if (acao.compareTo("excluir")==0){
				try {
					if (conteudoEmail.excluiConteudo()== true)
					   req.setAttribute("acao", "excluir");
					   
					conteudoEmail.setMsgErro("Dados Excluidos com Sucesso!");
				} catch (Exception e1) {
					throw new sys.CommandException("Erro ao excluir dados: " + e1.getMessage());
				}
				
            }
			req.setAttribute("conteudoEmailId", conteudoEmail);
			req.setAttribute("atualizarDependente", atualizarDependente);
		    req.setAttribute("OrgaoBeanId", OrgaoBeanId);
		
		return nextRetorno;
	}
}
