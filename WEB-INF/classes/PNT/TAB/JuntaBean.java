package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import sys.BeanException;
import sys.Util;
/**
 * <b>Title:</b>        	SMIT - Pontuacao <br>
 * <b>Description:</b>  	Junta de Julgamento Bean <br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author Wellem Lyra.
 * @version 1.0
 */
public class JuntaBean   extends TabelaBean { 
	
	// Dados b�sicos
	private String 	codJunta;
	private String  nomeJunta;
	private String 	codOrgao;	
	private String  sigJunta;	
	private String 	indTipo; // 0 = Defesa de Pontua��o, 1 = Recurso de Pontua��o, 2 = 2� Inst�ncia de Pontua��o
	private String  qtdRelatores;	
	// Armazenamento das juntas
	private List <JuntaBean>juntas; 
    	
	public JuntaBean()  throws sys.BeanException {	  
		super();		
		codJunta   ="-1";
		nomeJunta  ="";
		codOrgao   ="";		
		sigJunta   ="";		
		indTipo    ="";		
		qtdRelatores  ="";			
		juntas     = new ArrayList<JuntaBean>();    
	}
	public String getCodJunta() {		
		return codJunta;
	}
	public void setCodJunta(String codJunta) {
		if (codJunta==null) codJunta= "-1";
		this.codJunta = codJunta;
	}
	public String getNomeJunta() {
		return nomeJunta;
	}
	public void setNomeJunta(String nomeJunta) {
		if (nomeJunta==null) nomeJunta= "";
		this.nomeJunta = nomeJunta;
	}
	public String getCodOrgao() {
		return codOrgao;
	}
	public void setCodOrgao(String codOrgao) {
		if (codOrgao==null) codOrgao= "";
		this.codOrgao = codOrgao;
	}
	public String getIndTipo() {		
		return indTipo;
	}
	public void setIndTipo(String indTipo) {
		if (indTipo==null) indTipo= "";
		this.indTipo = indTipo;
	}	
	public String getTipoJunta()  {
		String descJunta = "";
		if ("0".equals(indTipo)) descJunta="Defesa de Pontua��o";
		if ("1".equals(indTipo)) descJunta="Recurso de Pontua��o";
		if ("2".equals(indTipo)) descJunta="2� Inst�ncia de Pontua��o";	
		return descJunta;	 
	}
	public List<JuntaBean> getJuntas() {
		return juntas;
	}
	public void setJuntas(List<JuntaBean> juntas) {
		if (juntas==null) juntas= new ArrayList<JuntaBean>();
		this.juntas = juntas;
	}
	public String getSigJunta() {
		return sigJunta;
	}
	public void setSigJunta(String sigJunta) {
		if (sigJunta==null) sigJunta= "";
		this.sigJunta = sigJunta;
	} 	
	public String getQtdRelatores() {
		return qtdRelatores;
	}
	public void setQtdRelatores(String qtdRelatores) {
		if (qtdRelatores==null) qtdRelatores= "";
		this.qtdRelatores = qtdRelatores;
	}	
	public void carregar(int min, int livre) throws sys.BeanException {		
		try {			
			// Carregando todas as juntas cadastradas no banco
			Dao daoJunta = DaoFactory.getInstance();
			daoJunta.carregarJuntas(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.juntas.size()<min-livre) 
				livre = min-juntas.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.juntas.add(new JuntaBean()) ;
			}
		}
		catch(Exception e){			
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 	
	public boolean atualizar(JuntaBean juntasCadastrados) throws sys.BeanException {		
		boolean atualizacaoOk=false;
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			JuntaBean juntasAlteradas = this;
			
			/*	Percorrendo as listas de juntas para identificar qdo uma
			 junta deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< juntasAlteradas.getJuntas().size(); i++) {
				
				JuntaBean juntaCad = (JuntaBean)juntasCadastrados.getJuntas().get(i);
				JuntaBean juntaAlt = (JuntaBean)juntasAlteradas.getJuntas().get(i);
				
				// JUNTAS NOVAS E REMOVIDAS (identificando e tratando)
				if (juntaCad.getCodJunta()     .equals("-1")						
						&& (!juntaAlt.getNomeJunta().equals("")
						&& (!juntaAlt.getSigJunta() .equals("")
						&& (!juntaAlt.getTipoJunta().equals("")))))
				{					
					Dao daoJunta = DaoFactory.getInstance();					
					if(juntaAlt.getCodJunta().equals("-1")){
						daoJunta.JuntaInsert(juntaAlt); 
							atualizacaoOk=true;
					}					
				} 	
				if(!juntaCad.getCodJunta()     .equals("-1")						
					&& (juntaAlt.getNomeJunta().equals("")
					&& (juntaAlt.getSigJunta() .equals("")
				    && (juntaAlt.getTipoJunta().equals("")))))
				{
					Dao daoJunta = DaoFactory.getInstance();	
					daoJunta.JuntaDelete(juntaCad);
					atualizacaoOk=true;			
				}		
				// JUNTAS ALTERADAS (identificando e tratando)
				if(juntaAlt.getCodJunta().equals(juntaCad.getCodJunta())){
					Dao daoJunta = DaoFactory.getInstance();
					daoJunta.JuntaUpdate(juntaAlt);
						atualizacaoOk=true;
				}					
			}   		
		}catch(Exception e){			
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk;
	}		
	public void carregarJuntas() throws sys.BeanException  {		
		try {    	
			Dao daoJunta = DaoFactory.getInstance();
			daoJunta.carregarJuntas(this);        
		} catch (Exception e) {			
			throw new sys.BeanException(e.getMessage()) ;			        			
		}
	}	
	public void Le_Junta() throws BeanException  {
		try { 
			Dao daoJunta = DaoFactory.getInstance();
			daoJunta.JuntaLeBean(this);						
		} catch (Exception e) {		
			throw new sys.BeanException(e.getMessage()) ;
		}
	}	
	
	
	
	
	
	
	
	 /**-------------------------------------------------------------------
    /*  C�digo Relativo a Junta (em REC)
    /*--------------------------------------------------------------------
    **/
    
	public void Le_Junta(String tpsist, int pos)  {
	     try { 
		       Dao daoTabela = DaoFactory.getInstance();
	 			if (pos==0) 
				   this.codJunta=tpsist;		 	
				else 
				   this.nomeJunta=tpsist;	
	    	   if ( daoTabela.JuntaLeBean(this, pos) == false) {
				    this.codJunta	  = "0";
					setPkid("0") ;		
	           }
	     }// fim do try
	     catch (Exception e) {
	          	this.codJunta = "0" ;
				setPkid("0") ;		        			
	     }
	   }

		public String getJunta(){
	    	String myJunta = "" ; 
	     	try    {
	     		Dao daoTabela = DaoFactory.getInstance();
	        	myJunta = daoTabela.getJunta() ;
				
	  		}
	   		catch(Exception e){
	   		}	
	   		return myJunta ;
	 	}
		
		public String getJunta(ACSS.UsuarioBean UsrLogado ){
	    	String myJunta = "" ; 
	     	try    {
	     		Dao daoTabela = DaoFactory.getInstance();
	        	myJunta = daoTabela.getJunta(UsrLogado ) ;
				
	  		}
	   		catch(Exception e){
	   		}	
	   		return myJunta ;
	 	}
	
	
	public Vector getJunta(int min,int livre)  {
		Vector myJunta = null;
		try  {
			
			Dao daoTabela = DaoFactory.getInstance();

			if( (myJunta = daoTabela.OrgaoGetJunta(codOrgao)) == null ){
				setMsgErro("Junta009 - Erro de Leitura: ");
				myJunta = new Vector();
			}
			if (myJunta.size()<min-livre) 
				livre = min-myJunta.size();	  	
			for (int i=0;i<livre; i++){
				myJunta.addElement(new PNT.TAB.JuntaBean());
			}
		}
		catch (Exception e){
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta;
	}

	
    /**-------------------------------------------------------------------
    /*  C�digo Relativo a Junta (em REC)
    /*--------------------------------------------------------------------
    **/
    
	public Vector getJunta(String codOrgao)  {
		Vector myJunta = null;
		try  {
			
			Dao daoTabela = DaoFactory.getInstance();
			if( (myJunta = daoTabela.OrgaoGetJunta(codOrgao)) == null )
				setMsgErro("Junta009 - Erro de Leitura Smit: ");
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
 
	 /**-------------------------------------------------------------------
	  C�digo Relativo a Junta Broker
	  --------------------------------------------------------------------**/

	public List getJuntaBroker()  {
		List myJunta = null;
		try  { 
			Dao daoTabela = DaoFactory.getInstance();
			if( (myJunta = daoTabela.OrgaoGetJuntaBroker(codOrgao)) == null )
				setMsgErro("Junta009 - Erro de Leitura Broker: ");
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public boolean compareSmitBroker(List juntaSmit, List juntaBroker,Vector errosPosicoes) throws BeanException{
		JuntaBean junta, aux_junta;
		boolean flag = true;
		if( (juntaSmit == null) || (juntaBroker == null) )
			return false;
		
		for(int pos=0;pos<juntaSmit.size();pos++,flag=true){
			junta = (JuntaBean)juntaSmit.get(pos);
			for(int i= 0;i<juntaBroker.size();i++){
				aux_junta = (JuntaBean)juntaBroker.get(i);
				if(Util.lPad(junta.getCodJunta(),"0",6).equals(aux_junta.getCodJunta())){
					if( !(Util.rPad(junta.getNomeJunta()," ",20).equals(aux_junta.getNomeJunta())) || !(Util.rPad(junta.getSigJunta()," ",10).equals(aux_junta.getSigJunta())) || !(junta.getIndTipo().equals(aux_junta.getIndTipo())) )
						errosPosicoes.add(""+pos);
					if(pos != i){
						junta = (JuntaBean)juntaBroker.set(pos,aux_junta);
						juntaBroker.set(i,junta);
					}
					flag=false;
					break;
				}
			}
			if(flag){
				if(pos>=juntaBroker.size())
					continue;
				junta = (JuntaBean)juntaBroker.set(pos,new JuntaBean());
				juntaBroker.add(junta);
			}
		}
		if( juntaSmit.size() > juntaBroker.size() ){
			for(int pos = juntaBroker.size(); pos < juntaSmit.size(); pos++)
				juntaBroker.add(new JuntaBean());
		}
		if( juntaSmit.size() < juntaBroker.size() ){
			for(int pos = juntaSmit.size(); pos < juntaBroker.size(); pos++)
				juntaSmit.add(new JuntaBean());
		}
		return true;
	}
	
	  public boolean isInsertJuntaBroker(List juntaSmit, List juntaSmit_aux, List juntaBroker, List juntaBroker_aux)   {
	  	boolean retorno = true;
	  	Vector vErro = new Vector() ;
	  	
	  	JuntaBean myJuntaSmit, myJuntaSmit_aux, myJuntaBroker, myJuntaBroker_aux;
	  	// Verificada a valida��o dos dados nos campos
	  	try  {
	  		Dao daoTabela = DaoFactory.getInstance();		   
	  		for(int pos=0; pos < juntaSmit.size();pos++){
	  			myJuntaSmit       = (JuntaBean)juntaSmit.get(pos);
	  			myJuntaSmit_aux   = (JuntaBean)juntaSmit_aux.get(pos);
	  			myJuntaBroker     = (JuntaBean)juntaBroker.get(pos);
	  			myJuntaBroker_aux = (JuntaBean)juntaBroker_aux.get(pos);	  		
	  			if( (myJuntaSmit.getNomeJunta().equals(myJuntaSmit_aux.getNomeJunta())) && (myJuntaSmit.getIndTipo().equals(myJuntaSmit_aux.getIndTipo())) && (myJuntaSmit.getSigJunta().equals(myJuntaSmit_aux.getSigJunta())  ) && (myJuntaBroker.getNomeJunta().equals(myJuntaBroker_aux.getNomeJunta())) && (myJuntaBroker.getIndTipo().equals(myJuntaBroker_aux.getIndTipo()))&& (myJuntaBroker.getSigJunta().equals(myJuntaBroker.getSigJunta())  ) )
	  				continue;
	  			 
	  			if( (myJuntaSmit.getCodJunta().trim().equals("")) && (!myJuntaSmit.getNomeJunta().equals("")) )
	  				continue;
	  			if( (!myJuntaSmit.getCodJunta().trim().equals("")&& !myJuntaSmit.getCodJunta().equals("-1")) && (!myJuntaSmit.getNomeJunta().equals("")) && (!myJuntaBroker.getCodJunta().equals("-1")&&!myJuntaBroker.getCodJunta().trim().equals("")) ){
	  				if( daoTabela.JuntaUpdateBroker(myJuntaBroker) )
	  				{
	  					if( !daoTabela.JuntaUpdate(myJuntaSmit) ){
	  						vErro.add("Erro: 001 | Altera��o no Smit Junta: "+myJuntaSmit.getNomeJunta()+ "\n");
	  						daoTabela.JuntaUpdateBroker(myJuntaBroker_aux);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 001 | Altera��o no Detran Junta: "+myJuntaBroker.getNomeJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getCodJunta().trim().equals("")) && (!myJuntaSmit.getNomeJunta().equals("")) && (!myJuntaBroker.getCodJunta().equals("-1")) ){
	  				if( daoTabela.JuntaUpdateBroker(myJuntaBroker) )
	  				{
	  					myJuntaSmit.setCodJunta(myJuntaBroker.getCodJunta());
	  					if( !daoTabela.JuntaInsert(myJuntaSmit,false) ){
	  						vErro.add("Erro: 002 | Inclus�o no Smit Junta: "+myJuntaSmit.getNomeJunta()+ "\n");
	  						daoTabela.JuntaUpdateBroker(myJuntaBroker_aux);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 002 | Altera��o no Detran Junta: "+myJuntaBroker.getNomeJunta()+ "\n");
	  			}
	  			if( (!myJuntaSmit.getCodJunta().trim().equals("-1")&&!myJuntaSmit.getCodJunta().trim().equals("")) && (!myJuntaSmit.getNomeJunta().equals("")) && (myJuntaBroker.getCodJunta().trim().equals("")||myJuntaBroker.getCodJunta().trim().equals("-1")) ){
	  				myJuntaBroker.setCodJunta(myJuntaSmit.getCodJunta());
	  				if( daoTabela.JuntaInsertBroker(myJuntaBroker) )
	  				{
	  					if( !daoTabela.JuntaUpdate(myJuntaSmit) ){
	  						vErro.add("Erro: 003 | Altera��o no Smit Junta: "+myJuntaSmit.getNomeJunta()+ "\n");
	  						daoTabela.JuntaDeleteBroker(myJuntaBroker);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 003 | Inclus�o no Detran Junta: "+myJuntaBroker.getNomeJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getCodJunta().trim().equals("")||myJuntaSmit.getCodJunta().trim().equals("-1")) && (!myJuntaSmit.getNomeJunta().equals("")) && (myJuntaBroker.getCodJunta().trim().equals("")) ){
	  				if( daoTabela.JuntaInsert(myJuntaSmit,true) )
	  				{
	  					myJuntaBroker.setCodJunta(myJuntaSmit.getCodJunta());
	  					if( !daoTabela.JuntaInsertBroker(myJuntaBroker) ){
	  						vErro.add("Erro: 004 | Inclus�o no Detran Junta: "+myJuntaBroker.getNomeJunta()+ "\n");
	  						daoTabela.JuntaDelete(myJuntaSmit);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 004 | Inclus�o no Smit Junta: "+myJuntaBroker.getNomeJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getCodJunta().trim().equals("")||myJuntaSmit.getCodJunta().trim().equals("-1")) && (!myJuntaSmit.getNomeJunta().equals("")) && (!myJuntaBroker.getCodJunta().equals("-1")) ){
	  					myJuntaSmit.setCodJunta(myJuntaBroker.getCodJunta());
	  					if( !daoTabela.JuntaInsert(myJuntaSmit,false) )
	  						vErro.add("Erro: 002 | Inclus�o no Smit Junta: "+myJuntaSmit.getNomeJunta()+ "\n");
	  				
	  			}	  			
	  			
	  			if( (myJuntaSmit.getNomeJunta().equals("")) && (myJuntaBroker.getNomeJunta().equals(""))){
	  				boolean sucesso = true;
	  				if( !myJuntaBroker.getCodJunta().equals("-1")&& !myJuntaBroker.getCodJunta().equals("-1") ){
	  					if( !daoTabela.JuntaDeleteBroker(myJuntaBroker) ){
	  						vErro.add("Erro: 005 | Exclus�o no Detran Junta: "+myJuntaBroker_aux.getNomeJunta()+ "\n");
	  						sucesso = false;
	  					}
	  				}
	  			
	  				if(sucesso){
	  					if( !myJuntaSmit.getCodJunta().equals("")&& !myJuntaSmit.getCodJunta().equals("-1") ){
	  						if( !daoTabela.JuntaDelete(myJuntaSmit) ){
	  							vErro.add("Erro: 005 | Exclus�o no Smit Junta: "+myJuntaSmit_aux.getNomeJunta()+ "\n");
	  							if( !myJuntaBroker.getCodJunta().equals(""))
	  								daoTabela.JuntaInsertBroker(myJuntaBroker_aux);
	  							sucesso = false;
	  						}
	  					}
	  				}
	  				if(sucesso){
	  					juntaSmit.remove(pos);
	  					juntaSmit_aux.remove(pos);
	  					juntaBroker.remove(pos);
	  					juntaBroker_aux.remove(pos);
	  					pos--;
	  				}
	  			}
	  		}
	  	}
	  	catch (Exception e) {
	  		vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
	  		retorno = false;
	  	}//fim do catch
	  	setMsgErro(vErro);
	  	return retorno;
	   }
	  
	  public boolean isValidJunta(Vector vErro, List myJuntas) {
	  	if("0".equals(this.codOrgao)) 
	  		vErro.addElement("Orgao n�o selecionado. \n") ;
	  	
	  	String sDesc = "";
	  	for (int i = 0 ; i< myJuntas.size(); i++) {
	  		REC.JuntaBean myJunta = (REC.JuntaBean)myJuntas.get(i) ;
	  		sDesc = myJunta.getNomJunta();
	  		if((sDesc.length()==0) ||("".equals(sDesc)))
	  			continue ;
	  		
	  		// verificar duplicata 
	  		for (int m = 0 ; m <myJuntas.size(); m++) {
	  			REC.JuntaBean myJuntaOutra = (REC.JuntaBean)myJuntas.get(m) ;		
	  			if ((m!=i) && (myJuntaOutra.getNomJunta().trim().length()!=0) && (myJuntaOutra.getNomJunta().equals(sDesc))) {
	  				vErro.addElement("Junta em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
	  			}
	  		}
	  	}
	  	return (vErro.size() == 0) ;  
	  }
	  
	  public boolean isInsertJunta(List myJuntas, List myJuntas_aux)   {
	  	boolean retorno = true;
	  	Vector vErro = new Vector() ;
	  	// Verificada a valida��o dos dados nos campos
	  	if( isValidJunta(vErro,myJuntas) )   {
	  		try  {
	  			REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
	  			retorno = daoTabela.InsertJunta(vErro,myJuntas,myJuntas_aux);

	  		}
	  		catch (Exception e) {
	  			vErro.addElement("Junta do Org�o "+codOrgao+" n�o atualizados:"+ " \n") ;
	  			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
	  			retorno = false;
	  		}//fim do catch
	  	}
	  	else retorno = false;   
	  	setMsgErro(vErro);
	  	return retorno;
	  }

	
}