package PNT.TAB;

import java.util.StringTokenizer;
import java.util.Vector;



public class ConteudoEmailBean extends sys.EmailBean{

	private String codOrgao;
	private String codEvento;
	private String dscEvento;
	private String nomLogin;
	private String txtSenha;
	private String msgErro;
	
	public ConteudoEmailBean(){
		super();
		codOrgao = "0";	
		codEvento = "0";
		nomLogin = "";
		txtSenha = "";
		dscEvento = "";
		msgErro = "";

	}
	
	public void setCodEvento(String codEvento){
		this.codEvento = codEvento;
		if (codEvento==null) this.codEvento="0" ;
	}
	public String getCodEvento(){
		return this.codEvento;
	}
	
	public void setCodOrgao(String codOrgao){
		this.codOrgao = codOrgao;
	}
	public String getCodOrgao(){
		return codOrgao;
	}
	
	public void setLogin(String nomLogin){
		this.nomLogin = nomLogin ; 
	}
	public String getLogin (){
		return nomLogin;
	}
	
	public void setSenha(String txtSenha){
		this.txtSenha = txtSenha;
	}
	public String getSenha(){
		return txtSenha;
	}
	
	public void setDscEvento(String dscEvento){
		this.dscEvento = dscEvento;
		if (dscEvento==null) this.dscEvento="" ;
	}
	public String getDscEvento(){
		return this.dscEvento;
	}
	
	public void setMsgErro(Vector vErro)   {
		 for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j) ;
		 }
	 }
	public void setMsgErro(String msgErro)   {
		  this.msgErro = msgErro ;
	 }   
	public String getMsgErro()   {
	  return this.msgErro;
	 }  
	
	public boolean enviaEmailChangeParam(String destinatario, String[] parametros) throws Exception{
		setDestinatario(destinatario);
		if (( consultaConteudo() == false ) || ( verificaEmail() == false ) )  
		   	return false;
		String aux_String;
		StringTokenizer texto = new StringTokenizer(conteudoEmail,"#");
		if(texto.countTokens() > 1){
			if( texto.toString().charAt(0) != '#' )	conteudoEmail = texto.nextToken();
			else conteudoEmail = "";
			while(texto.hasMoreTokens()){
				aux_String = texto.nextToken();
				if(aux_String.compareToIgnoreCase("DTENT") == 0)
					conteudoEmail += parametros[0];	   	
				if(aux_String.compareToIgnoreCase("NUMREQ") == 0)
					conteudoEmail += parametros[1];				   			
		   		if(aux_String.compareToIgnoreCase("NUMPROC") == 0)
		   			conteudoEmail += parametros[2];	   			
		   		if(aux_String.compareToIgnoreCase("ATAS") == 0)
		   			conteudoEmail += parametros[3];	   			
		   		if(texto.hasMoreTokens())
		   			conteudoEmail += texto.nextToken();
		   		else
		   			break;
		   	}
		}
		
		sendEmail(nomLogin,txtSenha);
		return true;
	}
	
	public boolean enviaEmailChangeParam(String destinatario, REC.HistoricoBean myHis) throws Exception{		
		String[] parametros = new String[4];
		codOrgao = myHis.getCodOrgao();
		codEvento = myHis.getCodEvento();

   		if("631".indexOf(codEvento) >= 0) {
			parametros[0] = myHis.getTxtComplemento03();
   			parametros[1] = myHis.getTxtComplemento01();
   		}
   		parametros[2] = myHis.getNumProcesso();
   		
		return enviaEmailChangeParam(destinatario,parametros) ;

	}
	
	public boolean enviaEmail(String destinatario) throws Exception{
		setDestinatario(destinatario);
		if( consultaConteudo() == false )
			return false;
		if ( verificaEmail() == false ) 
		   	return false;
		
		sendEmail(nomLogin, txtSenha);
		return true;
	}
	
	public boolean enviaEmail(String destinatario, REC.HistoricoBean myHis) throws Exception{  
		codOrgao = myHis.getCodOrgao();
		codEvento = myHis.getCodEvento();
		if( enviaEmail(destinatario) == false)
	   		return false;
		return true;
	}
	
	public boolean consultaConteudo(){
		try { 
			Dao dao = DaoFactory.getInstance();
			return dao.consultaConteudoEmail(this);
		}
		catch(Exception e){
			System.err.println(e.getMessage());
		}		
		return false;
	}
	
	
	public boolean alteraConteudo(String conteudoEmail, String remetente,
	 String assunto, String login, String senha) throws Exception{
		boolean retorno = true;
		try { 
			Dao dao = DaoFactory.getInstance();
			dao.alteraConteudoEmail(
			    new String[] {"COD_ORGAO", "COD_EVENTO"},
			    new String[] {codOrgao,codEvento}, conteudoEmail,
			    remetente,assunto, login, senha);
		}
		catch(Exception e){
			throw new Exception("Erro ConteudoEmailBean.alteraConteudo() " + e.getMessage());
		}
		setConteudoEmail(conteudoEmail);
		setRemetente(remetente);
		setAssunto(assunto);
		this.nomLogin = login;
		this.txtSenha = senha;
		
		return retorno;
	}
	
	public boolean insereConteudo(String conteudoEmail,String remetente, 
	  String assunto, String login, String senha) throws Exception{
		boolean retorno = true;
		try { 
			Dao dao = DaoFactory.getInstance();
			dao.insereConteudoEmail(new String[] {codOrgao,codEvento,conteudoEmail, 
				 remetente, assunto, login, senha });
		}
		catch(Exception e){
			throw new Exception("Erro ConteudoEmailBean.insereConteudo() " + e.getMessage());
		}
		setConteudoEmail(conteudoEmail);
		setRemetente(remetente);
		setAssunto(assunto);
		this.nomLogin = login;
		this.txtSenha = senha;
		return retorno;
	}

	public boolean excluiConteudo() throws Exception{
			boolean retorno = true;
			try { 
				Dao dao = DaoFactory.getInstance();
				dao.excluiConteudoEmail( new String[] {"COD_ORGAO", "COD_EVENTO"},
				                         new String[] {codOrgao,codEvento});
			}
			catch(Exception e){
				throw new Exception("Erro ConteudoEmailBean.excluiConteudo() " + e.getMessage());
			}
			return retorno;
		}
}
