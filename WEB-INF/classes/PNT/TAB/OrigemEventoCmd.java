package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Tabelas - Manutencao de Evento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Eventos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 			    Marlon Falzetta.	
 * @version 1.0
 */

public class OrigemEventoCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/OrigemEvento.jsp";    
	public OrigemEventoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {	 
		
		String nextRetorno  = jspPadrao ;	
		HttpSession session   = req.getSession() ;
		try {    
			
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;
			
	    	// Declara��o de vari�veis
			List<String> vErro = new ArrayList<String>() ;
			OrigemEventoBean OrigemEventoId = new OrigemEventoBean();
			
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todas as Penalidades
				OrigemEventoId.carregar(20,5);
				
				req.setAttribute("OrigemEventoId", OrigemEventoId);
			}	
			
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codOrigemEv = req.getParameterValues("codOrigemEv");
				if(codOrigemEv == null) codOrigemEv = new String[0];  
				
				String[] dscOrigemEv = req.getParameterValues("dscOrigemEv");
				if(dscOrigemEv == null)  dscOrigemEv = new String[0];  				
				/* ** FIM Recuperando os dados da tela ** */
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codOrigemEv, dscOrigemEv, vErro) ;
				
				// Caso nao tenha erro as opera��es necessarias s�o realizadas
				if (vErro.size()==0) {
					
					List <OrigemEventoBean> lista = new ArrayList<OrigemEventoBean>(); 
					for (int i = 0; i < codOrigemEv.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto PenalidadeBean.
						OrigemEventoBean myOrigem = new OrigemEventoBean() ;
						if(! codOrigemEv[i].equals(""))							
							myOrigem.setCodOrigEvento(Integer.decode(codOrigemEv[i]));
						    myOrigem.setDescricao(dscOrigemEv[i]);
						    myOrigem.setCodOrgaoLotacao(codOrgaoLotacao);
						    myOrigem.setNomUserName(nomUsername);
						lista.add(myOrigem) ;				
					}
					
					// Instancio um objeto com os novos registros.
					OrigemEventoBean origensAlteradas = new OrigemEventoBean();
					origensAlteradas.setOrigensEventos(lista) ;
					
					// Instancio um objeto com os registros gravados no banco.
					OrigemEventoBean origensCadastradas = new OrigemEventoBean();
					origensCadastradas.carregar(20,5);
					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					if(origensAlteradas.atualizar(origensCadastradas))
						OrigemEventoId.setMsgErro("Dados atuzalizados com sucesso.");
					else
						OrigemEventoId.setMsgErro("N�o houve atualiza��o dos dados");
					
					OrigemEventoId.carregar(20,5);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("OrigemEventoId",OrigemEventoId) ;
				}
				else OrigemEventoId.setMsgErro(vErro.toString()) ;  	      
			}
			
			if(acao.equals("I")){
				
				OrigemEventoId.carregar(0,0);
				req.setAttribute("OrigemEventoId", OrigemEventoId);
				nextRetorno = "/PNT/TAB/OrigemEventoImp.jsp";
			}			
		}
		catch (Exception e) {
			throw new sys.CommandException("OrigemEventoCmd: " + e.getMessage());
		}
		return nextRetorno;
	}
	
	private void isParametros(String[] codOrigemEv, String[] dscOrigemEv, List<String> vErro) {				 
		if ((codOrigemEv.length != dscOrigemEv.length))
			vErro.add("Parametros inconsistentes") ;		
	}
	
}