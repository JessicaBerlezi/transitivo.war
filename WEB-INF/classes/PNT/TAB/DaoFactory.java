package PNT.TAB;

public class DaoFactory {

	private static Dao instance;

	public static Dao getInstance() throws sys.DaoException {
        if (instance == null) instance = new Dao();
        return instance;
    }
	
}