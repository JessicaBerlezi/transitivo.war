package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * <b>Title:</b>        	SMIT - Pontuacao <br>
 * <b>Description:</b>  	Origem Evento Bean <br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author Marlon Falzetta.
 * <b>Revised By:Wellem Lyra </b>
 * @version 1.1
 */

public class OrigemEventoBean   extends TabelaBean { 
	
	private Integer codOrigEvento;
	private String descricao;	
	
	private List <OrigemEventoBean>origensEventos; 
	
	public OrigemEventoBean()  throws sys.BeanException {	
		super();
		codOrigEvento = -1;
		descricao = "";   		
		origensEventos = new ArrayList<OrigemEventoBean>();    
	} 
	
	
//	---------------------------------------------------
	public Integer getCodOrigEvento() {
		return codOrigEvento;
	}
	
	public void setCodOrigEvento(Integer codOrigEvento) {
		this.codOrigEvento = codOrigEvento;
	}
//	---------------------------------------------------
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
//	---------------------------------------------------
	public List<OrigemEventoBean> getOrigensEventos() {
		return origensEventos;
	}
	
	public void setOrigensEventos(List<OrigemEventoBean> origensEventos) {
		this.origensEventos = origensEventos;
	}
//	---------------------------------------------------
	
	public void carregar(int min, int livre) throws sys.BeanException {
		Vector<String> vErro;
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoEventos = DaoFactory.getInstance();
			daoEventos.carregarOrigenEventos(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.origensEventos.size()<min-livre) 
				livre = min-origensEventos.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.origensEventos.add(new OrigemEventoBean()) ;
			}
		}
		catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao carregar Origens de Eventos : " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 
	
	public boolean atualizar(OrigemEventoBean origensCadastradas) throws sys.BeanException {
		Vector<String> vErro;
		boolean atualizacaoOk=false;
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			OrigemEventoBean origensAlteradas = this;
			
			/*	Percorrendo as listas de penalidades para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< origensAlteradas.getOrigensEventos().size(); i++) {
				
				OrigemEventoBean orCad = (OrigemEventoBean)origensCadastradas.getOrigensEventos().get(i);
				OrigemEventoBean orAlt = (OrigemEventoBean)origensAlteradas.getOrigensEventos().get(i);
				
				// PENALIDADES NOVAS E REMOVIDAS (identificando e tratando)
				if (! orAlt.getCodOrigEvento().equals(orCad.getCodOrigEvento()) ) { 
					
					Dao daoOrigens = DaoFactory.getInstance();
					
					/* Qdo (cod(OrigEveAlterado) == ""), nao � um evento novo e 
					 *  sim os beans instanciados para os campos vazios da da tela
					 *  que receberam novos dados mas que n�o foram preenchidos. 
					 */
					if(! orAlt.getCodOrigEvento().equals(-1))
						if(daoOrigens.OrigemEventoInsert(orAlt) && (atualizacaoOk == false) )
							atualizacaoOk=true;
					
					
					/* Qdo (cod(EventoCadastrado)=="") n�o � um evento a ser
					 * exclu�do e sim benas que foram intanciados a mais qdo
					 * os dados do banco foram carregados, apenas para ficar
					 * com o mesmo nr de elementos da tela.
					 */
					if(! orCad.getCodOrigEvento().equals(-1))
						if(daoOrigens.OrigemEventoDelete(orCad) && (atualizacaoOk==false) )
							atualizacaoOk=true;
				} 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		orAlt.getCodOrigEvento().equals(orCad.getCodOrigEvento()) 
						&&(! orAlt.getDescricao().equals(orCad.getDescricao())) ){
					
					Dao daoOrigens = DaoFactory.getInstance();
					if(daoOrigens.OrigemEventoUpdate(orAlt) && (atualizacaoOk==false))
						atualizacaoOk=true;
				}					
			}   		
		}catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao atualizar Origens de Eventos: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk;
	}
	
}