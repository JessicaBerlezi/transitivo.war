package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Tabelas - Manutencao de Evento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Eventos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	   MIND Informatica <br>
 * @author 			   Marlon Falzetta.	
 * @version 1.0
 */

public class EventoCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Evento.jsp";  
	public EventoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = jspPadrao ;
		HttpSession session   = req.getSession() ;	
		try {
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;
			
			List<String> vErro = new ArrayList<String>() ;
			
			// Declara��o de vari�veis
			EventoBean EventoId = new EventoBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();			
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todos os eventos 
				EventoId.carregar(20,5);
				
				req.setAttribute("EventoId", EventoId);
			}	
			
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codEvento= req.getParameterValues("codEvento");
				if(codEvento == null) codEvento = new String[0];  
				
				String[] dscEvento = req.getParameterValues("dscEvento");
				if(dscEvento == null)  dscEvento = new String[0];  
				/* ** FIM Recuperando os dados da tela ** */
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codEvento,dscEvento, vErro);
				
				if (vErro.size()==0) {
					
					List <EventoBean> lista = new ArrayList<EventoBean>(); 
					for (int i = 0; i < dscEvento.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto EventoBean.
						EventoBean myEvento = new EventoBean() ;	
						
						if(!codEvento[i].equals(""))
						myEvento.setCodEvento(Integer.decode(codEvento[i]));						
						myEvento.setDscEvento(dscEvento[i]);
						myEvento.setCodOrgaoLotacao(codOrgaoLotacao);
						myEvento.setNomUserName(nomUsername);					
						lista.add(myEvento) ;					
					}									
					
					// Instancio um objeto com os novos registros.
					EventoBean eventosAlterados = new EventoBean();
					eventosAlterados.setEventos(lista) ;
					
					// Instancio um objeto com os registros gravados no banco.
					EventoBean eventosCadastrados = new EventoBean();
					eventosCadastrados.carregar(20,5);
					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					if(eventosAlterados.atualizar(eventosCadastrados))
						EventoId.setMsgErro("Dados atualizados com sucesso.");
					else
						EventoId.setMsgErro("N�o houve atualiza��o dos dados");
					
					EventoId.carregar(20,5);
					
//					Mandando o objeto para a requsi��o
					req.setAttribute("EventoId", EventoId);	
				}
				else EventoId.setMsgErro(vErro.toString());  	      
			}
			
			if(acao.equals("I")){
				
				EventoId.carregar(0,0);
				req.setAttribute("EventoId", EventoId);
				nextRetorno = "/PNT/TAB/EventoImp.jsp";
			}
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("EventoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private void isParametros(String[] codEvento,String[] dscEvento, List <String>vErro) {				 
		if (dscEvento.length!=codEvento.length) vErro.add("Parametros inconsistentes") ;		
	} 
}