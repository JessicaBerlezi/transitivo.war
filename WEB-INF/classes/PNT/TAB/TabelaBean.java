package PNT.TAB;
import sys.Util;

/**
 * <b>Title:</b>        SMIT - Tabelas Pontua��o - TabelaBean<br>
 * <b>Description:</b>  TabelaBean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra.
 * @version 1.0
 */

public class TabelaBean extends sys.HtmlPopupBean { 
	
	private String nomUserName;
	private String codOrgaoLotacao;
	private String datProc;	
	private String msgErro ; 
	
	public TabelaBean() {
		super();			
		
		nomUserName      = "";
		codOrgaoLotacao  = "";
		datProc          = Util.formatedToday().substring(0,10);	
		msgErro          = "" ;
	}

	public String getNomUserName() {
		return nomUserName;
	}

	public void setNomUserName(String nomUserName) {
		if (nomUserName==null) nomUserName= "";
		this.nomUserName = nomUserName;
	}	
	
	public String getCodOrgaoLotacao() {
		return codOrgaoLotacao;
	}

	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		if (codOrgaoLotacao==null) codOrgaoLotacao= "";
		this.codOrgaoLotacao = codOrgaoLotacao;
	}

	public String getDatProc() {
		return datProc;
	}

	public void setDatProc(String datProc) {
		if (datProc==null) datProc= "";
		this.datProc = datProc;
	}

	
	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String msgErro) {
		if (msgErro==null) msgErro= "";
		this.msgErro = msgErro;
	}
	
}