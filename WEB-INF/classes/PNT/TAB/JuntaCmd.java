package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;


/**
 * <b>Title:</b>        Tabelas - Tabela de JUntas<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Juntas<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 			    Wellem Lyra.	
 * @version 1.0
 */

public class JuntaCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Junta.jsp";  
	public JuntaCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = jspPadrao ;
		HttpSession session   = req.getSession() ;	
		try {
			
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;
			
	    	ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
    		if (parSis == null)	parSis = new ParamSistemaBean();			
			
			// Declara��o de vari�veis
			JuntaBean JuntaId = new JuntaBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();			
			String codOrgao        = parSis.getParamSist("ORGAO_PONTUACAO");
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equals("")){
				
				// Carregar todas as juntas 
				JuntaId.carregar(20,5);				
				req.setAttribute("JuntaId", JuntaId);
			}	
			
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codJunta= req.getParameterValues("codJunta");
				if(codJunta == null) codJunta = new String[0];  
				
				String[] sigJunta = req.getParameterValues("dscJunta");
				if(sigJunta == null)  sigJunta = new String[0]; 
				
				String[] nomeJunta = req.getParameterValues("nomeJunta");
				if(nomeJunta == null)  nomeJunta = new String[0]; 
				
				String[] tipo = req.getParameterValues("tipo");
				if(tipo == null)  tipo = new String[0]; 		
				/* ** FIM Recuperando os dados da tela ** */
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
		    	List<String> vErro = new ArrayList<String>() ;
				isParametros(codJunta,sigJunta, vErro);				
				if (vErro.size()==0) {
					
					List <JuntaBean> lista = new ArrayList<JuntaBean>(); 
					for (int i = 0; i < sigJunta.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto JuntaBean.
						JuntaBean myJunta = new JuntaBean() ;
						if(! codJunta[i].equals(""))
							myJunta.setCodJunta(codJunta[i]);
							myJunta.setNomeJunta((nomeJunta[i]));
							myJunta.setSigJunta	(sigJunta[i]);
							myJunta.setCodOrgao((codOrgao));
							myJunta.setIndTipo((tipo[i]));
							myJunta.setCodOrgaoLotacao(codOrgaoLotacao);
							myJunta.setNomUserName(nomUsername);						
							lista.add(myJunta) ;					
					}									
					
					// Instancio um objeto com os novos registros.
					JuntaBean JuntasAlteradas = new JuntaBean();
					JuntasAlteradas.setJuntas(lista) ;
					
					// Instancio um objeto com os registros gravados no banco.
					JuntaBean JuntasCadastradas = new JuntaBean();
					JuntasCadastradas.carregar(20,5);
					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					if(JuntasAlteradas.atualizar(JuntasCadastradas))
						JuntaId.setMsgErro("Dados atualizados com sucesso.");
					else
						JuntaId.setMsgErro("N�o houve atualiza��o dos dados");
					
					JuntaId.carregar(20,5);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("JuntaId", JuntaId);	
				}
				else JuntaId.setMsgErro(vErro.toString());  	      
			}
			
			if(acao.equals("I")){
				
				JuntaId.carregar(0,0);
				req.setAttribute("JuntaId", JuntaId);
				nextRetorno = "/PNT/TAB/JuntaImp.jsp";
			}
			
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("JuntaCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	private void isParametros(String[] codJunta,String[] sigJunta, List<String> vErro) {				 
		if (sigJunta.length!=codJunta.length) vErro.add("Parametros inconsistentes") ;		
	} 
	
}