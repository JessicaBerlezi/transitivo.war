package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * <b>Title:</b>        Tabelas - Manutencao de Evento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Eventos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 			    Marlon Falzetta.	
 * @version 1.0
 */
public class RelatorCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Relator.jsp";	
	public RelatorCmd() {
		super();
	}	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		
		// Declara��o de vari�veis
		String nextRetorno  = jspPadrao ;	
		HttpSession session   = req.getSession() ;	
		try {			
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;			
			RelatorBean RelatorBeanId = new RelatorBean();
			JuntaBean	JuntaBeanId = new JuntaBean();
			List<String> vErro = new ArrayList<String>() ;			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao =""; 			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();			
			// carregar as juntas.
			if(	   acao.equalsIgnoreCase("")
					|| acao.equalsIgnoreCase("buscarRelatores")
					|| acao.equalsIgnoreCase("A")
					|| acao.equals("I")){				
				// Carregar todos os relatores 
				JuntaBeanId.carregarJuntas();				
				if(JuntaBeanId.getJuntas().size()<=0)
					RelatorBeanId.setMsgErro("N�o existem juntas cadastradas.\nCadastre uma junta e repita a opera��o.");				
			}			
			// Carregar os relatores
			if(	   acao.equalsIgnoreCase("buscarRelatores")&& !"".equals(req.getParameter("cmbJunta"))
					|| acao.equalsIgnoreCase("A")){				
				// Carregar todos os relatores 
				RelatorBeanId.setCodJunta(req.getParameter("cmbJunta"));
				RelatorBeanId.carregarRelatores(20,5);			
			}			
			if(acao.equalsIgnoreCase("A")){				
				/* ** Recuperando os dados da tela ** */
				String[] codRelator = req.getParameterValues("codRelator");
				if(codRelator == null) codRelator = new String[0];  
				
				String[] nomRelator = req.getParameterValues("nomRelator");
				if(nomRelator == null)  nomRelator = new String[0];
				
				String[] numCpf = req.getParameterValues("numCpf");
				if(numCpf == null)  numCpf = new String[0];
				
				String[] nomTitulo = req.getParameterValues("nomTitulo");
				if(nomTitulo == null)  nomTitulo = new String[0];
				
				String sIndAtivo = req.getParameter("sIndAtivo");
				
				String[] indAtivo = new String[codRelator.length];
				
				for(int i=0; i<codRelator.length; i++){
					if(sIndAtivo.substring(i,i+1).equals("1"))
						indAtivo[i] = "S";
					else
						indAtivo[i] = "N";
				}
				/* ** FIM Recuperando os dados da tela ** */				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codRelator,nomRelator,numCpf,nomTitulo,indAtivo,vErro);				
				if (vErro.size()==0) {					
					List <RelatorBean> lista = new ArrayList<RelatorBean>(); 
					for (int i = 0; i < codRelator.length;i++) {						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto EventoBean.
						RelatorBean myRelator = new RelatorBean() ;	
						myRelator.setCodRelator(codRelator[i]);
						myRelator.setNomRelator(nomRelator[i]);	
						myRelator.setNumCpf(numCpf[i]);
						myRelator.setNomTitulo(nomTitulo[i]);
						myRelator.setIndAtivo(indAtivo[i]);
						myRelator.setCodOrgaoLotacao(codOrgaoLotacao);
						myRelator.setNomUserName(nomUsername);			
						lista.add(myRelator) ;					
					}						
					// Instancio um objeto com os novos registros.
					RelatorBean relatoresAlterados = new RelatorBean();
					relatoresAlterados.setRelatores(lista) ;					
					// Instancio um objeto com os registros gravados no banco.
					RelatorBean relatoresCadastrados = new RelatorBean();
					relatoresCadastrados.setCodJunta(req.getParameter("cmbJunta"));
					relatoresCadastrados.carregarRelatores(20,5);					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					relatoresAlterados.setCodJunta(req.getParameter("cmbJunta"));
					try{
						if(relatoresAlterados.atualizar(relatoresCadastrados))
							RelatorBeanId.setMsgErro("Dados atualizados com sucesso.");					
						else
							RelatorBeanId.setMsgErro("N�o houve atualiza��o dos dados." );
						
					}catch (Exception ue) {
						RelatorBeanId.setMsgErro("N�o houve atualiza��o dos dados: " + ue.getMessage());
					}					
					RelatorBeanId.setCodJunta(req.getParameter("cmbJunta"));
					RelatorBeanId.carregarRelatores(20,5);					 
				}
				else RelatorBeanId.setMsgErro(vErro.toString());  	      
			}			
			if(acao.equals("I")){				
					RelatorBeanId.setCodJunta(req.getParameter("cmbJunta"));
					RelatorBeanId.carregarRelatores(0,0);					
					JuntaBeanId.setCodJunta(RelatorBeanId.getCodJunta());
					JuntaBeanId.Le_Junta();
					req.setAttribute("RelatorBeanId", RelatorBeanId);
					req.setAttribute("JuntaBeanId", JuntaBeanId);
					nextRetorno = "/PNT/TAB/RelatorImp.jsp";			
			}			
			req.setAttribute("JuntaBeanId", JuntaBeanId);
			req.setAttribute("RelatorBeanId", RelatorBeanId);			 
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatorCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	}	
	private void isParametros(String[] codRelator,String[] nomRelator,String[] numCpf,String[] nomTitulo,String[]indAtivo, List<String> vErro) {				 
		if ( (codRelator.length !=  nomRelator.length) || (codRelator.length  != numCpf.length) || (codRelator.length != nomTitulo.length) || (codRelator.length != indAtivo.length))  
			vErro.add("Parametros inconsistentes") ;		
	} 	
}