package PNT.TAB ;

import java.util.ArrayList;
import java.util.List;

import PNT.DaoBrokerFactory;
import TAB.DaoException;

/**
 * <b>Title:</b>        	SMIT - Pontuacao <br>
 * <b>Description:</b>  	Penalidade Bean <br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author Wellem Lyra.
 * @version 1.0
 */

public class PenalidadeBean   extends TabelaBean { 
	
	private String  codPenalidade;
	private String  dscPenalidade;
	private String  enquadramento;  
	private Integer numPrazo;	
	private String  reincidente;
	private Integer qtdInfracaoDe;
	private Integer qtdInfracaoAte;
	private String  codFatorAgravamento;
	private String  dscResumida;
	private List <PenalidadeBean>penalidades; 
	
	public PenalidadeBean()  throws sys.BeanException {	
		super();
		codPenalidade = "-1";
		dscPenalidade = "";
		enquadramento = "";
		numPrazo = -1;
		reincidente = "";	
		qtdInfracaoDe = -1;
		qtdInfracaoAte= -1;
		codFatorAgravamento="-1";
		dscResumida="";
			
		penalidades = new ArrayList<PenalidadeBean>();    
	}
//	---------------------------------------------------
	public String getCodPenalidade() {
		return this.codPenalidade;
	}	
	public void setCodPenalidade(String codPenalidade) {
		if (codPenalidade==null) codPenalidade= "-1";		
		this.codPenalidade = codPenalidade;
	}
//	---------------------------------------------------
	public String getDscPenalidade() {
		return this.dscPenalidade;
	}	
	public void setDscPenalidade(String dscPenalidade) {
		if (dscPenalidade==null) dscPenalidade= "";	
		this.dscPenalidade = dscPenalidade;
	}
//	---------------------------------------------------
	public String getEnquadramento() {
		return this.enquadramento;
	}	
	public void setEnquadramento(String enquadramento) {
		if (enquadramento==null) enquadramento= "";		
		this.enquadramento = enquadramento;
	}
	public String getReincidente() {
		return reincidente;
	}
	public void setReincidente(String reincidente) {
		if (reincidente==null) reincidente= "";		
		this.reincidente = reincidente;
	}	
	public Integer getNumPrazo() {
		return numPrazo;
	}
	public void setNumPrazo(Integer numPrazo) {
		if (numPrazo==null) numPrazo=-1;
		this.numPrazo = numPrazo;
	}	
	//	---------------------------------------------------
	public List<PenalidadeBean> getPenalidades() {
		return penalidades;
	}	
	public void setPenalidades(List<PenalidadeBean> penalidades) {
		this.penalidades = penalidades;
	}
	//	---------------------------------------------------	
    public void Le_Penalidade(String chave) throws sys.BeanException {
  	  try {
  		  Dao dao = DaoFactory.getInstance();	
  		  dao.PenalidadeLeBean(this,chave);		   
  	  }
  	  catch (Exception ex) {
  		  throw new sys.BeanException(ex.getMessage());
  	  }		
    }	
	//	---------------------------------------------------	
	public void carregar(int min, int livre,int ordem) throws sys.BeanException {		
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoEventos = DaoFactory.getInstance();
			daoEventos.carregarPenalidades(this,ordem);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.penalidades.size()<min-livre) 
				livre = min-penalidades.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.penalidades.add(new PenalidadeBean()) ;
			}
		}
		catch(Exception e){				
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 	
	public boolean atualizar(PenalidadeBean penalidadesCadastradas) throws sys.BeanException {		
		boolean atualizacaoOk=false;
		String result = "";		
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			PenalidadeBean penalidadesAlteradas = this;
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			
			/*	Percorrendo as listas de penalidades para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< penalidadesAlteradas.getPenalidades().size(); i++) {
				
				PenalidadeBean penCad = (PenalidadeBean)penalidadesCadastradas.getPenalidades().get(i);
				PenalidadeBean penAlt = (PenalidadeBean)penalidadesAlteradas.getPenalidades().get(i);
				
				// PENALIDADES NOVAS E REMOVIDAS (identificando e tratando)				
					if(penCad.getCodPenalidade()          .equals("-1")						
							&& (!penAlt.getDscPenalidade().equals(""  )
							&& (!penAlt.getEnquadramento() .equals("" )
							&& (!penAlt.getEnquadramento() .equals("" )		
							&& (!penAlt.getReincidente().equals(""))))))
					{
						Dao daoPenalidades = DaoFactory.getInstance();
						daoPenalidades.PenalidadeInsert(penAlt); 
						broker.Transacao624(penAlt.getCodPenalidade().toString(),"1","   ",penAlt.getDscPenalidade(),penAlt.getEnquadramento(),penAlt.getReincidente(),
								penAlt.getNumPrazo().toString(),penAlt.getQtdInfracaoDe().toString(),
								penAlt.getQtdInfracaoAte().toString(),
								penAlt.getCodFatorAgravamento(),penAlt.getDscResumida());
						if (result.substring(0,3).equals("016")) {
							result = broker.Transacao624(penAlt.getCodPenalidade().toString(),"2","   ",penAlt.getDscPenalidade(),penAlt.getEnquadramento(),penAlt.getReincidente(),
									penAlt.getNumPrazo().toString(),penAlt.getQtdInfracaoDe().toString(),
									penAlt.getQtdInfracaoAte().toString(),
									penAlt.getCodFatorAgravamento(),penAlt.getDscResumida());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						}
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;				
					}					
				
					if(!penCad.getCodPenalidade()          .equals("-1")						
							&& (penAlt.getDscPenalidade() .equals(""   )
							&& (penAlt.getEnquadramento() .equals(""   )
							&& (penAlt.getEnquadramento() .equals(""   )		
							&& (penAlt.getReincidente()   .equals(""))))))
					{
						Dao daoPenalidades = DaoFactory.getInstance();
						daoPenalidades.PenalidadeDelete(penCad);
						broker.Transacao624(penCad.getCodPenalidade().toString(),"3","   ",penCad.getDscPenalidade(),penCad.getEnquadramento(),penCad.getReincidente(),
								penCad.getNumPrazo().toString(),penCad.getQtdInfracaoDe().toString(),
								penCad.getQtdInfracaoAte().toString(),
								penCad.getCodFatorAgravamento(),penCad.getDscResumida());
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;
				    } 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		penAlt.getCodPenalidade().equals(penCad.getCodPenalidade()) 
						&& 	( 	  (! penAlt.getDscPenalidade().equals(penCad.getDscPenalidade())) 
								||(! penAlt.getEnquadramento().equals(penCad.getEnquadramento()))
								||(! penAlt.getNumPrazo().equals(penCad.getNumPrazo()))								
								||(! penAlt.getReincidente().equals(penCad.getReincidente())
								||(! penAlt.getQtdInfracaoDe().equals(penCad.getQtdInfracaoDe())
								||(! penAlt.getQtdInfracaoAte().equals(penCad.getQtdInfracaoAte())
								||(! penAlt.getDscResumida().equals(penCad.getDscResumida())		
								||(! penAlt.getCodFatorAgravamento().equals(penCad.getCodFatorAgravamento())))))))	){
					
					Dao daoPenalidades = DaoFactory.getInstance();
					daoPenalidades.PenalidadeUpdate(penAlt);
					result = broker.Transacao624(penAlt.getCodPenalidade().toString(),"2","   ",penAlt.getDscPenalidade(),penAlt.getEnquadramento(),penAlt.getReincidente(),
							penAlt.getNumPrazo().toString(),
							penAlt.getQtdInfracaoDe().toString(),
							penAlt.getQtdInfracaoAte().toString(),
							penAlt.getCodFatorAgravamento(),penAlt.getDscResumida());
					if (result.substring(0,3).equals("017")) {
						result = broker.Transacao624(penAlt.getCodPenalidade().toString(),"1","   ",penAlt.getDscPenalidade(),penAlt.getEnquadramento(),penAlt.getReincidente(),
								penAlt.getNumPrazo().toString(),penAlt.getQtdInfracaoDe().toString(),
								penAlt.getQtdInfracaoAte().toString(),
								penAlt.getCodFatorAgravamento(),penAlt.getDscResumida());
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
					}
					if (! result.substring(0,3).equals("000"))
						throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
					atualizacaoOk=true;
				}					
			}   		
		}catch(Exception e){			
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk; 
	}	
//	---------------------------------------------------
	public String getCodFatorAgravamento() {		
		return codFatorAgravamento;
	}
	public void setCodFatorAgravamento(String codFatorAgravamento) {
		if (codFatorAgravamento==null) codFatorAgravamento= "-1";
		this.codFatorAgravamento = codFatorAgravamento;
	}
	public Integer getQtdInfracaoAte() {		
		return qtdInfracaoAte;
	}
	public void setQtdInfracaoAte(Integer qtdInfracaoAte) {
		if (qtdInfracaoAte==null) qtdInfracaoAte= -1;
		this.qtdInfracaoAte = qtdInfracaoAte;
	}
	public Integer getQtdInfracaoDe() {			
		return qtdInfracaoDe;
	}
	public void setQtdInfracaoDe(Integer qtdInfracaoDe) {
		if (qtdInfracaoDe==null) qtdInfracaoDe= -1;
		this.qtdInfracaoDe = qtdInfracaoDe;
	}
	public String getDscResumida() {
		return dscResumida;
	}
	public void setDscResumida(String dscResumida) {
		if (dscResumida==null) dscResumida= "";		
		this.dscResumida = dscResumida;
	}
}