package PNT.TAB;

/**
* <b>Title:</b>        	SMIT-PNT - Bean de Codigo de Retorno <br>
* <b>Description:</b>  	Tabela de Retorno<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company: </b> MIND Informatica <br>
* @author  				Sergio Roberto Jr.
* @version 				1.0
*/

public class CodigoRetornoBean extends sys.HtmlPopupBean { 

	private int codCodigoRetorno;
	private String numRetorno;
	private String descRetorno;
	private String indEntregue;

	public CodigoRetornoBean() throws sys.BeanException{

	  	super();
	  	setTabela("TSMI_CODIGO_RETORNO");	
	  	setPopupWidth(35);
	  	numRetorno  = "";   	
		descRetorno	= "";
		indEntregue = "N";
		
	}
	
	public void setCodCodigoRetorno(int codCodigoRetorno)  {
		this.codCodigoRetorno = codCodigoRetorno ;
	}
	public int getCodCodigoRetorno()  {
  		return this.codCodigoRetorno;
  	}	
	public void setNumRetorno(String numRetorno)  {
			if (numRetorno == null)	 this.numRetorno ="" ;
			else { 
				if(!numRetorno.equals(""))
				  this.numRetorno = sys.Util.lPad(numRetorno,"0",2);
				else this.numRetorno = numRetorno;
			}
	}
	public String getNumRetorno()  {
			return this.numRetorno;
	}
	public String getNumRetornoEdit()  {
		return sys.Util.lPad(getNumRetorno(),"0",2);
  	}
	
	public void setDescRetorno(String descRetorno){
		this.descRetorno = descRetorno;
		if (descRetorno == null)
		 this.descRetorno = ""; 
	}
	public String getDescRetorno()  {
  		return this.descRetorno;
  	}
	public void setIndEntregue(String indEntregue)  {
		if (indEntregue == null) indEntregue = "N"; 
		this.indEntregue = indEntregue;
	}  	
	public String getIndEntregue()  {
  		return this.indEntregue;
  	}
	public boolean isEntregue()  {					
		return (indEntregue.equals("S"));
  	}  
  
  public void LeRetorno() {
	  try  	  {		  
		  Dao dao = DaoFactory.getInstance();	   
		  dao.LeRetorno(this);
	  }  catch (Exception e) {	  }
  }
      
}