package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import PNT.DaoBrokerFactory;
import TAB.DaoBroker;
import TAB.DaoException;

/**
 * <b>Title:</b>        SMIT - Entrada de Recurso - Informar Resultado Bean<br>
 * <b>Description:</b>  Status Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra
 * @version 1.0
 */

public class StatusBean extends TabelaBean { 
	
	private String nomStatus;
	private String codStatus;
	private List <StatusBean>status;	
	
	public StatusBean() {
		super();
		nomStatus        = "";
		codStatus        = "";
		status = new ArrayList<StatusBean>();	
		
	}  
	
	public void setNomStatus(String nomStatus)  {
		this.nomStatus=nomStatus ;
		if (nomStatus==null) this.nomStatus= "";
	}  
	public String getNomStatus()  {
		return nomStatus;
	}
	
	public void setCodStatus(String codStatus)  {
		this.codStatus=codStatus ;
		if (codStatus==null) this.codStatus= "";
	}  
	public String getCodStatus()  {
		return this.codStatus;
	}
	
	public List<StatusBean> getStatus() {
		return status;
	}
	public void setStatus(List<StatusBean> status) {
		this.status = status;
	}	
	
	public void carregar(int min, int livre) throws sys.BeanException {
		Vector<String> vErro;
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoStatus = DaoFactory.getInstance();
			daoStatus.carregarStatus(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.status.size()<min-livre) 
				livre = min-status.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.status.add(new StatusBean()) ;
			}
		}
		catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao carregar Status do Auto : " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 
	
	public boolean atualizar(StatusBean statusCadastrados) throws sys.BeanException {
		Vector<String> vErro;
		boolean atualizacaoOk=false;
		String result = "";
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			StatusBean statusAlterados = this;
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			
			/*	Percorrendo as listas de penalidades para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< statusAlterados.getStatus().size(); i++) {
				
				StatusBean statusCad = (StatusBean)statusCadastrados.getStatus().get(i);
				StatusBean statusAlt = (StatusBean)statusAlterados.getStatus().get(i);
				
				// PENALIDADES NOVAS E REMOVIDAS (identificando e tratando)
				if (! statusAlt.getCodStatus().equals(statusCad.getCodStatus()) ) { 
					Dao daoStatus = DaoFactory.getInstance();
					if(! statusAlt.getCodStatus().equals(""))
						if(daoStatus.StatusInsert(statusAlt) && (atualizacaoOk==false) ) {
							broker.Transacao626(statusAlt.getCodStatus(),"1","   ",statusAlt.getNomStatus());
							if (result.substring(0,3).equals("016")) {
								result = broker.Transacao626(statusAlt.getCodStatus(),"2","   ",statusAlt.getNomStatus());
								if (! result.substring(0,3).equals("000"))
									throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							}
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
						}
					
					if(! statusCad.getCodStatus().equals(""))
						if(daoStatus.StatusDelete(statusCad) && (atualizacaoOk==false)) {
							broker.Transacao626(statusCad.getCodStatus(),"3","   ",statusCad.getNomStatus());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
						}
				} 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		statusAlt.getCodStatus().equals(statusCad.getCodStatus()) 
						&&(! statusAlt.getNomStatus().equals(statusCad.getNomStatus())) ){
					
					Dao daoStatus = DaoFactory.getInstance();
					if(daoStatus.StatusUpdate(statusAlt) && (atualizacaoOk==false) ) {
						result = broker.Transacao626(statusAlt.getCodStatus(),"2","   ",statusAlt.getNomStatus());
						if (result.substring(0,3).equals("017")) {
							result = broker.Transacao626(statusAlt.getCodStatus(),"1","   ",statusAlt.getNomStatus());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						}
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;
					}
				}					
			}   		
		}catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao atualizar Status do Auto: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk;
	}
	
	public String DscStatus(String codstatus) throws sys.BeanException {
		Vector<String> vErro;
		String Status="";
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoStatus = DaoFactory.getInstance();
			Status=daoStatus.carregaDescStatus(codstatus);	 
			
		}
		catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao carregar Status do Auto : " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}
		return Status;		
	}	
	
}