package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import PNT.DaoBrokerFactory;
import TAB.DaoException;


/**
 * <b>Title:</b>        SMIT - Pontua��o<br>
 * <b>Description:</b>  Motivos Cancelamento Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra
 * @version 1.0
 */

public class MotivoBean extends TabelaBean { 
	
	private Integer codMotivo;
	private String descricao;
	private List <MotivoBean>motivos;
	
	public MotivoBean() {
		super();
		codMotivo        = -1;
		descricao        = "";		
		motivos = new ArrayList<MotivoBean>(); 
	    
	}
	
	public Integer getCodMotivo() {
		return codMotivo;
	}
	
	public void setCodMotivo(Integer codMotivo) {
		this.codMotivo = codMotivo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public List<MotivoBean> getMotivos() {
		return motivos;
	}
	
	public void setMotivos(List<MotivoBean> motivos) {
		this.motivos = motivos;
	}  
	
	public void carregar(int min, int livre) throws sys.BeanException {
		Vector<String> vErro;
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoMotivo = DaoFactory.getInstance();
			daoMotivo.carregarMotivos(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.motivos.size()<min-livre) 
				livre = min-motivos.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.motivos.add(new MotivoBean()) ;
			}
		}
		catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao carregar Motivos de Cancelamento: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 
	
	public boolean atualizar(MotivoBean motivosCadastrados) throws sys.BeanException {
		Vector <String>vErro;
		boolean atualizacaoOk=false;
		String result = "";		
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			MotivoBean motivosAlterados = this;
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			
			/*	Percorrendo as listas de penalidades para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< motivosAlterados.getMotivos().size(); i++) {
				
				MotivoBean motCad = (MotivoBean)motivosCadastrados.getMotivos().get(i);
				MotivoBean motAlt = (MotivoBean)motivosAlterados.getMotivos().get(i);
				
				// MOTIVOS NOVOS E REMOVIDOS (identificando e tratando)
				if (! motAlt.getCodMotivo().equals(motCad.getCodMotivo()) ) {
					Dao daoMotivo = DaoFactory.getInstance();
					if(! motAlt.getCodMotivo().equals(-1))
						if(daoMotivo.MotivoInsert(motAlt) && (atualizacaoOk==false)) {
							broker.Transacao629(motAlt.getCodMotivo().toString(),"1","   ",motAlt.getDescricao());
							if (result.substring(0,3).equals("016")) {
								result = broker.Transacao629(motAlt.getCodMotivo().toString(),"2","   ",motAlt.getDescricao());
								if (! result.substring(0,3).equals("000"))
									throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							}
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
						}
					
					if(! motCad.getCodMotivo().equals(""))
						if(daoMotivo.MotivoDelete(motCad) && (atualizacaoOk==false) ) {
							broker.Transacao629(motCad.getCodMotivo().toString(),"3","   ",motCad.getDescricao());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
						}
				} 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		motAlt.getCodMotivo().equals(motCad.getCodMotivo()) 
						&&(! motAlt.getDescricao().equals(motCad.getDescricao()))){
					Dao daoMotivo = DaoFactory.getInstance();
					if(daoMotivo.MotivoUpdate(motAlt) && (atualizacaoOk==false) ) {
						result = broker.Transacao629(motAlt.getCodMotivo().toString(),"2","   ",motAlt.getDescricao());
						if (result.substring(0,3).equals("017")) {
							result = broker.Transacao629(motAlt.getCodMotivo().toString(),"1","   ",motAlt.getDescricao());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						}
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;
					}
				}					
			}   		
		}catch(Exception e){		
			vErro = new Vector<String>() ;
			vErro.addElement("Erro ao atualizar Motivos de Cancelamento: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk;
	}	
}