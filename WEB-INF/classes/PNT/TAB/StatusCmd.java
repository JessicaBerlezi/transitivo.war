package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Tabelas - Manutencao de Evento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Eventos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 			    Marlon Falzetta.	
 * @version 1.0
 */

public class StatusCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Status.jsp";    
	public StatusCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {	 
		
		String nextRetorno  = jspPadrao ;	
		HttpSession session   = req.getSession() ;	
		try {    
			
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;
	    	
			// Declara��o de vari�veis
			List<String> vErro = new ArrayList<String>() ;
			StatusBean StatusId = new StatusBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();			
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todas as Penalidades
				StatusId.carregar(20,5);
				
				req.setAttribute("StatusId", StatusId);
			}	
			
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codStatus = req.getParameterValues("codStatus");
				if(codStatus == null) codStatus = new String[0];  
				
				String[] dscStatus = req.getParameterValues("dscStatus");
				if(dscStatus == null)  dscStatus = new String[0];  				
				/* ** FIM Recuperando os dados da tela ** */
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codStatus, dscStatus, vErro) ;
				
				// Caso nao tenha erro as opera��es necessarias s�o realizadas
				if (vErro.size()==0) {
					
					List <StatusBean> lista = new ArrayList<StatusBean>(); 
					for (int i = 0; i < codStatus.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto PenalidadeBean.
						StatusBean myStatus = new StatusBean() ;
						myStatus.setCodStatus(codStatus[i]);
						myStatus.setNomStatus(dscStatus[i]);
						myStatus.setCodOrgaoLotacao(codOrgaoLotacao);
						myStatus.setNomUserName(nomUsername);						
						lista.add(myStatus) ;				
					}
					
					// Instancio um objeto com os novos registros.
					StatusBean statusAlterados = new StatusBean();
					statusAlterados.setStatus(lista) ;
					
					// Instancio um objeto com os registros gravados no banco.
					StatusBean statusCadastrados = new StatusBean();
					statusCadastrados.carregar(20,5);
					
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */
					if(statusAlterados.atualizar(statusCadastrados))
						StatusId.setMsgErro("Dados atualizados com sucesso.");
					else
						StatusId.setMsgErro("N�o houve atualiza��o dos dados");
					
					StatusId.carregar(20,5);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("StatusId",StatusId) ;
				}
				else StatusId.setMsgErro(vErro.toString()) ;  	      
			}
			
			if(acao.equals("I")){
				
				StatusId.carregar(0,0);
				req.setAttribute("StatusId", StatusId);
				nextRetorno = "/PNT/TAB/StatusImp.jsp";
			}			
		}
		catch (Exception e) {
			throw new sys.CommandException("StatusCMD: " + e.getMessage());
		}
		return nextRetorno;
	}
	
	private void isParametros(String[] codStatus, String[] dscStatus, List<String> vErro) {				 
		if ((codStatus.length != dscStatus.length))
			vErro.add("Parametros inconsistentes") ;		
	} 
	
}