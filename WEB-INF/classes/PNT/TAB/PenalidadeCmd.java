package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Tabelas - Manutencao de Evento<br>
 * <b>Description:</b>  Comando para Incluir/Alterar/Excluir/Consultar e Listar Eventos<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> 	   MIND Informatica <br>
 * @author 			   Marlon Falzetta.	
 * <b>Revised by</b>   Wellem Lyra
 * @version 1.1
 */

public class PenalidadeCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/TAB/Penalidade.jsp";    
	public PenalidadeCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {	 
		
		String nextRetorno  = jspPadrao ;	
		HttpSession session   = req.getSession() ;	
		try {    
		    ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;			
	    	
			// Declara��o de vari�veis
			List<String> vErro = new ArrayList<String>() ;
			PenalidadeBean PenalidadeId = new PenalidadeBean();
			
			String codOrgaoLotacao = UsrLogado.getOrgao().getCodOrgao();
			String nomUsername     = UsrLogado.getNomUserName();
			
			// Recuperar a a��o 				
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
			
			if(acao.equalsIgnoreCase("")){
				
				// Carregar todas as Penalidades
				PenalidadeId.carregar(20,5,1);
				
				req.setAttribute("PenalidadeId", PenalidadeId);
			}				
			if(acao.compareTo("A") == 0){
				
				/* ** Recuperando os dados da tela ** */
				String[] codPenalidade = req.getParameterValues("codPenalidade");
				if(codPenalidade == null) codPenalidade = new String[0];  
				
				String[] dscPenalidade = req.getParameterValues("dscPenalidade");
				if(dscPenalidade == null)  dscPenalidade = new String[0];  
				
				String[] enquadramento = req.getParameterValues("Enquadramento");
				if(enquadramento == null)  enquadramento = new String[0];	
				
				String[] codFatorAgravamento = req.getParameterValues("codFatorAgravamento");
				if(codFatorAgravamento == null)  codFatorAgravamento = new String[0];
				
				String[] dscResumida = req.getParameterValues("dscResumida");
				if(dscResumida == null)  dscResumida = new String[0];
				
//				String[] qtdInfracaoDe = req.getParameterValues("qtdInfracaoDe");
//				if(qtdInfracaoDe == null)  qtdInfracaoDe = new String[0];		
				
				String[] qtdInfracaoAte = req.getParameterValues("qtdInfracaoAte");
				if(qtdInfracaoAte == null)  qtdInfracaoAte = new String[0];						
				
				String[] numPrazo = req.getParameterValues("prazo");
				if(numPrazo == null) numPrazo = new String[0];
				
				String[] reincidente = req.getParameterValues("reincidente");
				if(reincidente == null) reincidente = new String[0];
				
				// Valida��o do total de elementos dos vetores (devem ser iguais)
				isParametros(codPenalidade, dscPenalidade, enquadramento, vErro, numPrazo, reincidente,codFatorAgravamento,dscResumida,qtdInfracaoAte) ;
				
				// Caso nao tenha erro as opera��es necessarias s�o realizadas
				if (vErro.size()==0) {
					
					List <PenalidadeBean> lista = new ArrayList<PenalidadeBean>(); 
					for (int i = 0; i < codPenalidade.length;i++) {
						
						// Organizando as informa��es num tipo List para serem
						// inseridos no Objeto PenalidadeBean.
						PenalidadeBean myPenalidade = new PenalidadeBean() ;
						if(! codPenalidade[i].equals(""))							
						myPenalidade.setCodPenalidade(codPenalidade[i]);
						myPenalidade.setDscPenalidade(dscPenalidade[i]);
						myPenalidade.setEnquadramento(enquadramento[i]);						
//					if(! qtdInfracaoDe[i].equals(""))
//						myPenalidade.setQtdInfracaoDe (Integer.decode(qtdInfracaoDe[i]));
						if(! qtdInfracaoAte[i].equals(""))
							myPenalidade.setQtdInfracaoAte (Integer.decode(qtdInfracaoAte[i]));
						
						myPenalidade.setCodFatorAgravamento(codFatorAgravamento[i]);	
						myPenalidade.setDscResumida(dscResumida[i]);	
						if(! numPrazo[i].equals(""))
						myPenalidade.setNumPrazo(Integer.decode(numPrazo[i]));						
						myPenalidade.setReincidente( reincidente[i] );												
						myPenalidade.setCodOrgaoLotacao(codOrgaoLotacao);
						myPenalidade.setNomUserName(nomUsername);					
						lista.add(myPenalidade) ;				
					}					
					// Instancio um objeto com os novos registros.
					PenalidadeBean penalidadesAlteradas = new PenalidadeBean();
					penalidadesAlteradas.setPenalidades(lista) ;					
					// Instancio um objeto com os registros gravados no banco.
					PenalidadeBean penalidadesCadastradas = new PenalidadeBean();
					penalidadesCadastradas.carregar(20,5,1);				
					/* 	M�todo utilizado para comparar todos os registros alterados
					 com os cadastrados no banco e fazer a atualiza��o, inserindo,
					 alterando ou excluindo caso necess�rio. */					
					if(penalidadesAlteradas.atualizar(penalidadesCadastradas))
						PenalidadeId.setMsgErro("Dados atualizados com sucesso.");
					else
						PenalidadeId.setMsgErro("N�o houve atualiza��o dos dados.");
					
					PenalidadeId.carregar(20,5,1);
					
					// Mandando o objeto para a requsi��o
					req.setAttribute("PenalidadeId",PenalidadeId) ;
				}
				else PenalidadeId.setMsgErro(vErro.toString()) ;  	      
			}			
			if(acao.equals("I")){
				PenalidadeId.carregar(0,0,2);
				req.setAttribute("PenalidadeId", PenalidadeId);
				nextRetorno = "/PNT/TAB/PenalidadeImp.jsp";
			}			
		}
		catch (Exception e) {
			throw new sys.CommandException("PenalidadeCmd: " + e.getMessage());
		}
		return nextRetorno;
	}	
	private void isParametros(String[] codPenalidade, String[] dscPenalidade,String[] enquadramento, List<String> vErro, String[] numPrazo, String[] reincidente,String[] dscResumida,String[] codFatorAgravamento,String[] qtdInfracaoAte) {				 
		if ((codPenalidade.length != dscPenalidade.length) && (codPenalidade.length != enquadramento.length) && ( dscPenalidade.length != enquadramento.length)&& (numPrazo.length != codPenalidade.length) && (reincidente.length != codPenalidade.length ))
			vErro.add("Parametros inconsistentes") ;		
	}	
}