package PNT.TAB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import ACSS.UsuarioBean;
import PNT.DEF.FuncionalidadesBean;
import PNT.TAB.JuntaBean;
import PNT.DaoBroker;
import PNT.DaoBrokerFactory;
import PNT.ProcessoBean;
import PNT.TAB.RelatorBean;
import sys.DaoException;
import sys.Util;


/**
 * <b>Title:</b>        	SMIT - MODULO PONTUA��O (Tabelas)<br>
 * <b>Description:</b>  	Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b>    	Copyright (c) 2006<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author Marlon Falzetta
 * @version 1.0
 */

public class Dao extends sys.DaoBase {
	
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "PNT";
	
//	**************************************************************************************
	protected Dao() throws sys.DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	/**
	 *-----------------------------------------------------------
	 * Le Junta e carrega no Bean
	 *-----------------------------------------------------------
	 */
	public void JuntaLeBean(JuntaBean myJunta)	throws DaoException {
		Connection conn =null ;	
		String sCmd ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();			
			sCmd = "SELECT j.cod_junta,j.nom_junta,j.cod_orgao,j.sig_junta,j.Ind_Tipo,"+
			"j.nom_username,j.cod_Orgao_Lotacao,j.Dat_Proc,r.QtdRelatores"+
			" from tpnt_junta j"+
			" left Join (select count(nom_relator)as QtdRelatores,cod_junta from tpnt_Relator group by cod_junta) r on(j.cod_junta=r.cod_junta)";
			
			if(! "-1".equals(myJunta.getCodJunta()))
				sCmd+= " WHERE j.COD_JUNTA = '"+ myJunta.getCodJunta()+ "'";			
			sCmd+= " ORDER BY j.NOM_JUNTA";
			ResultSet rs = stmt.executeQuery(sCmd) ;			
			if (rs.next()) {
				// Dados basicos
				myJunta.setCodJunta(rs.getString("cod_junta"));
				myJunta.setNomeJunta(rs.getString("nom_junta"));
				myJunta.setCodOrgao(rs.getString("cod_orgao"));
				myJunta.setSigJunta(rs.getString("sig_junta"));			
				myJunta.setIndTipo(rs.getString("Ind_Tipo"));
				myJunta.setQtdRelatores(rs.getString("QtdRelatores"));							
				myJunta.setNomUserName(rs.getString("nom_username"));
				myJunta.setCodOrgaoLotacao(rs.getString("cod_Orgao_Lotacao"));
				myJunta.setDatProc(rs.getString("Dat_Proc"));
			}
			rs.close();
			stmt.close();			
		}
		catch (SQLException e) {
			throw new DaoException("JuntaLeBean :"+ e.getMessage()+ " - "+ sCmd  );
		}
		catch (Exception ex) {			
			throw new DaoException("JuntaLeBean :"+ ex.getMessage()+ " - "+ sCmd  );
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
	}	
	public void carregarJuntas(JuntaBean myJunta)	throws DaoException {		
		Connection conn =null ;	
		String sCmd ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			sCmd =  "SELECT COD_JUNTA FROM TPNT_JUNTA ORDER BY NOM_JUNTA";			  				
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			List<JuntaBean> lista = new  ArrayList <JuntaBean> ();
			while (rs.next()) {
				JuntaBean junta = new JuntaBean();				
				// Dados basicos
				junta.setCodJunta(rs.getString("COD_JUNTA"));
				JuntaLeBean(junta);
				lista.add(junta);
			}
			myJunta.setJuntas(lista);
			rs.close();
			stmt.close();  				  
		}
		catch (SQLException e) {			
			throw new DaoException("carregarJuntas :"+ e.getMessage()+ " - "+ sCmd  );
		}
		catch (Exception ex) {			
			throw new DaoException("carregarJuntas :"+ ex.getMessage()+ " - "+ sCmd  );
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
	}	
	public boolean juntaExiste(JuntaBean myJunta) throws DaoException {		
		boolean existe = false ;		
		Connection conn = null ;	
		String sCmd     ="";
		try {		
			sCmd     = "SELECT COD_JUNTA FROM   TPNT_JUNTA " +
			"where SIG_JUNTA ='" + myJunta.getSigJunta() + "'";		
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			if (rs.next()) { existe =	true;  }
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {			
			throw new DaoException("juntaExiste :"+ e.getMessage()+ " - "+ sCmd  );
		}
		catch (Exception ex) {		
			throw new DaoException("juntaExiste :"+ ex.getMessage()+ " - "+ sCmd  );
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}		
	public void JuntaInsert(JuntaBean myJunta)throws DaoException{
		Connection conn=null;
		String sCmd ="";		 
		try{
			sCmd = "INSERT INTO TPNT_JUNTA(COD_JUNTA,NOM_JUNTA,COD_ORGAO,SIG_JUNTA," +
			"IND_TIPO,COD_ORGAO_LOTACAO,NOM_USERNAME,DAT_PROC)"  +
			"VALUES(SEQ_PNT_JUNTA.NEXTVAL," +
			"'" +  myJunta.getNomeJunta()   	+ "'," +
			"'" +  myJunta.getCodOrgao() 	  	+ "'," +
			"'" +  myJunta.getSigJunta() 	  	+ "'," +
			"'" +  myJunta.getIndTipo()	        + "'," +
			"'" + myJunta.getCodOrgaoLotacao()	+ "'," +
			"'" + myJunta.getNomUserName() 	    + "'," +
			"'" + myJunta.getDatProc()	        + "')" ;
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			stmt.execute(sCmd) ;
			stmt.close();
		}catch (Exception ex) {			
			throw new DaoException("JuntaInsert :"+ ex.getMessage()+ " - "+ sCmd  );
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}				
	}	
	public boolean JuntaInsert(JuntaBean myJunta, boolean flag) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			if(flag){
				sCmd = "SELECT seq_PNT_junta.nextval as codJunta FROM dual";
				ResultSet rs = stmt.executeQuery(sCmd);
				rs.next();
				myJunta.setCodJunta(rs.getString("codJunta"));
				rs.close();
			}
			sCmd  = "INSERT INTO TPNT_JUNTA (cod_junta,"+
			"nom_junta,cod_orgao,sig_junta,ind_tipo) " +
			"VALUES (" +
			"'"+myJunta.getCodJunta()+"'," +
			"'"+myJunta.getNomeJunta()+"'," +
			"'"+myJunta.getCodOrgao()+"',"+
			"'"+myJunta.getSigJunta()+"',"+
			"'"+myJunta.getIndTipo()+"')";
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	public boolean JuntaUpdate(JuntaBean myJunta){
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TPNT_JUNTA " +
			"SET NOM_JUNTA ='" 	+  myJunta.getNomeJunta()		+ "'," +
			"COD_ORGAO ='" 	+  myJunta.getCodOrgao()		+ "'," +
			"SIG_JUNTA ='" 	+  myJunta.getSigJunta()		+ "'," +
			"IND_TIPO  ='" 	+  myJunta.getIndTipo()	  		+ "' " +
			",COD_ORGAO_LOTACAO ='" + myJunta.getCodOrgaoLotacao() + "' " +
			",NOM_USERNAME ='" + myJunta.getNomUserName() + "' " +
			",DAT_PROC ='" + myJunta.getDatProc() + "' " +
			"WHERE COD_JUNTA = "   +  myJunta.getCodJunta(); 
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	public boolean JuntaDelete(JuntaBean myJunta) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd  = "DELETE TPNT_JUNTA " +
			"WHERE COD_JUNTA = "   +  myJunta.getCodJunta(); 	
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex){
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	public boolean JuntaLeBean(JuntaBean myJunta,int tpJun)
	throws DaoException {
		//synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT nom_junta,sig_junta,cod_orgao,ind_tipo from TPNT_JUNTA " ;
			sCmd += "WHERE cod_junta = '"+myJunta.getCodJunta()+"'" ;
			ResultSet rs = stmt.executeQuery(sCmd) ;
			myJunta.setMsgErro("") ;
			while (rs.next()) {
				myJunta.setNomeJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setCodOrgao(rs.getString("cod_orgao"));
				myJunta.setIndTipo(rs.getString("Ind_Tipo"));
				myJunta.setPkid(myJunta.getCodJunta()) ;
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myJunta.setCodJunta("0") ;
			myJunta.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myJunta.setCodJunta("0") ;
			myJunta.setPkid("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}	
	
	public String getJunta() throws DaoException {
		String myJunta ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_junta,cod_orgao,nom_junta, sig_junta from TPNT_JUNTA";
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myJunta += ",\""+rs.getString("cod_junta") +"\"";
				myJunta += ",\""+rs.getString("cod_orgao") +"\"";
				myJunta += ",\""+rs.getString("nom_junta") +"\"";
				myJunta += ",\""+rs.getString("sig_junta") +"\"";
			}
			myJunta = myJunta.substring(1,myJunta.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myJunta;
	}		
	
	public String getJunta(ACSS.UsuarioBean UsrLogado) throws DaoException {
		String myJunta ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_junta,cod_orgao,nom_junta, sig_junta from TPNT_JUNTA" +
			" where cod_orgao = "+UsrLogado.getCodOrgaoAtuacao();
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myJunta += ",\""+rs.getString("cod_junta") +"\"";
				myJunta += ",\""+rs.getString("cod_orgao") +"\"";
				myJunta += ",\""+rs.getString("nom_junta") +"\"";
				myJunta += ",\""+rs.getString("sig_junta") +"\"";
			}
			myJunta = myJunta.substring(1,myJunta.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myJunta;
	}	    
	public Vector OrgaoGetJunta(String codOrgao) throws DaoException {
		Connection conn =null ;	
		Vector junta = new Vector();
		boolean retorno = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT cod_junta,nom_junta,sig_junta,ind_tipo"+
			" from TPNT_JUNTA "+
			" WHERE cod_orgao = '"+ codOrgao + "'"+
			" order by nom_junta";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while( rs.next() ) {
				JuntaBean myJunta = new JuntaBean() ;
				myJunta.setCodJunta(rs.getString("cod_junta"));
				myJunta.setNomeJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setIndTipo(rs.getString("ind_tipo"));
				junta.addElement(myJunta) ;
			}    
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			retorno = false;
		}//fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn); 
					if(!retorno)
						return null;
				}
				catch (Exception ey) { }
			}
		}
		return junta;
	}	
	
	public List OrgaoGetJuntaBroker(String codOrgao) throws DaoException {
		List junta = new ArrayList();
		try {
			String indContinuidade = Util.lPad(""," ",12);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			JuntaBean myJunta;
			while(true){ 
				String sCmd = broker.Transacao622(Util.lPad(codOrgao,"0",6),Util.lPad("","9",6),"5",indContinuidade,"","","","","","","","","","","","");
				if(!sCmd.substring(0,3).equals("000") && (!sCmd.substring(0,3).equals("017")))
					return null;
				if( sCmd.substring(0,3).equals("017") )
					return junta;	
				
				int ini, pos, fim;
				for(pos=0,ini=3,fim=9;(!sCmd.substring(ini,fim).equals(Util.lPad("","0",6))) && (pos<20);ini = fim + 111, fim = ini+6,pos++){
					myJunta = new JuntaBean();
					myJunta.setCodOrgao(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim +6;
					myJunta.setCodJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 20;
					myJunta.setNomeJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 10;
					myJunta.setSigJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 1;					
						myJunta.setIndTipo(sCmd.substring(ini,fim));
					
					junta.add(myJunta);
				}
				indContinuidade = sCmd.substring(ini,fim);
				if(pos==20)
					break;
			}	
		}
		catch (Exception e) {
			return null;
		}//fim do catch
		return junta;
	}
	public boolean JuntaUpdateBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {	
			String indTipo;
			
				indTipo = myJuntaBroker.getIndTipo();
			
			if(!myJuntaBroker.getNomeJunta().equals("")){
				DaoBroker broker = DaoBrokerFactory.getInstance();
				String sCmd = broker.Transacao622(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"2",Util.lPad(""," ",12),Util.rPad(myJuntaBroker.getNomeJunta()," ",20),Util.rPad(myJuntaBroker.getSigJunta()," ",10),indTipo,"","","","","","","","","");
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	public boolean JuntaInsertBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {	
			String indTipo;
			
				indTipo = myJuntaBroker.getIndTipo();
			
			if(!myJuntaBroker.getNomeJunta().equals("")){
				DaoBroker broker = DaoBrokerFactory.getInstance();
				String sCmd = broker.Transacao622(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"1",Util.lPad(""," ",12),Util.rPad(myJuntaBroker.getNomeJunta()," ",20),Util.rPad(myJuntaBroker.getSigJunta()," ",10),indTipo,"","","","","","","","","");
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean JuntaDeleteBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {
			if(!myJuntaBroker.getCodJunta().equals("-1")){
				DaoBroker broker = DaoBrokerFactory.getInstance();
				String sCmd = broker.Transacao622(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"3",Util.lPad(""," ",12),"","","","","","","","","","","","");
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}	
	
	/*
	 *-----------------------------------------------------------
	 * Le Relator e carrega no Bean
	 *-----------------------------------------------------------
	 */
	public void RelatorLeBean(RelatorBean myRelator,String chave) throws DaoException {
		Connection conn = null ;	
		String sCmd     = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			sCmd =  "SELECT r.nom_relator,r.cod_junta,j.sig_junta,r.num_cpf,"        +
			"r.cod_relator, r.ind_ativo,r.nom_titulo from TPNT_RELATOR R,"   +
			"TPNT_JUNTA J"                                                   ;
			if ("CPF".equals(chave)) 
				sCmd += " where r.cod_junta   = '"+myRelator.getCodJunta()      +"' and "  + 
				" num_cpf           = '"+myRelator.getNumCpf()        +"'"       ;
			else 
				sCmd += " where cod_relator = '"+myRelator.getCodRelator()    +"'"       ;
			sCmd += " AND r.cod_junta   = j.cod_junta "                              ;
			ResultSet rs = stmt.executeQuery(sCmd)                                   ;
			myRelator.setMsgErro("")                                                 ;
			while (rs.next()) {
				myRelator.setNomRelator(rs.getString       ("nom_Relator"))          ;
				myRelator.setCodJunta(rs.getString         ("cod_junta"))            ;
				myRelator.setSigJunta(rs.getString         ("sig_junta"))            ;
				myRelator.setNumCpf(Util.lPad(rs.getString ("num_cpf"),"0",11))      ;
				myRelator.setCodRelator(rs.getString       ("cod_Relator"))          ;
				myRelator.setNomTitulo(rs.getString        ("nom_titulo"))           ;
				myRelator.setIndAtivo(rs.getString         ("ind_ativo"))            ;				
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException("Dao.Tab - RelatorLeBean: "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new DaoException("Dao.Tab - RelatorLeBean: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}	
	/**
	 * M�todo utilizado para carregar todos os relatores cadastrados na base de dados
	 * (SMIT_PNT.TPNT_RELATOR) FILTRADOS POR JUNTA.
	 * @param myRelator - Este objeto deve estar com seu atributo
	 * cod_junta preenchido.
	 * @return true se recuperou algum registro ou false caso contr�rio.
	 * @throws DaoException
	 */
	public void carregarRelatores(RelatorBean myRelator) throws DaoException {		
		Connection conn = null;
		String sCmd ="";		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "SELECT COD_RELATOR,NOM_RELATOR,NUM_CPF,COD_JUNTA,NOM_TITULO,IND_ATIVO " +
			"FROM TPNT_RELATOR " +
			"WHERE COD_JUNTA =" + myRelator.getCodJunta() + " " +
			"ORDER BY NOM_RELATOR";			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sCmd);			
			List<RelatorBean> lista = new ArrayList<RelatorBean>();
			while (rs.next()) {				 				 
				RelatorBean relator = new RelatorBean();				
				relator.setCodRelator(rs.getString("COD_RELATOR"));				
				RelatorLeBean(relator,"");				
				lista.add(relator);					
			}
			myRelator.setRelatores(lista);			
			rs.close();
			stmt.close();			
		} catch (Exception e) {
			throw new DaoException("carregarRelatores: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) {	}
			}
		}			
	}	
	public boolean RelatorInsert(RelatorBean relator) throws DaoException {
		Connection conn = null;	
		String sCmd ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			relator.setIndAtivo("S");
			
			sCmd = "INSERT INTO TPNT_RELATOR (COD_RELATOR,NOM_RELATOR,"           +
			"NUM_CPF,COD_JUNTA,NOM_TITULO,IND_ATIVO,COD_ORGAO_LOTACAO,"    +
			"NOM_USERNAME,DAT_PROC)VALUES(SEQ_PNT_RELATOR.NEXTVAL," 	      +
			"'" + relator.getNomRelator()        + "',"	                  +
			"'" + relator.getNumCpf()	        + "',"	                  +
			"'" + relator.getCodJunta()          + "',"	                  +
			"'" + relator.getNomTitulo()         + "',"	                  +
			"'" + relator.getIndAtivo()          + "',"                    +
			"'" + relator.getCodOrgaoLotacao()	+ "',"                    +
			"'" + relator.getNomUserName() 	    + "',"                    +
			"to_date('" + relator.getDatProc()	        + "','dd/mm/yyyy'))"                    ;			
						
			stmt.executeQuery(sCmd);
			stmt.close();
			
			
		} catch (Exception e) {
			return false;
		} finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); 
					
				}catch (Exception e) { 
				
					throw new DaoException(e.getMessage());	
				
				}
			}
		}
		return true ;
	}	
	public boolean RelatorDelete(RelatorBean relator) throws DaoException {
		Connection conn = null;	
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "DELETE FROM TPNT_RELATOR " +		 				 
			"WHERE COD_RELATOR       =" + relator.getCodRelator();			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);		 	 
			stmt.close();			
		} catch (Exception e) {			
			return false;
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}
		return true;	
	}	
	public boolean RelatorUpdate(RelatorBean relator) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "UPDATE TPNT_RELATOR "  +
			"SET NOM_RELATOR   ='"  + relator.getNomRelator() + "' "  +
			",NUM_CPF          ='"  + relator.getNumCpf()     + "' "  +
			",COD_JUNTA        ='"  + relator.getCodJunta()   + "' "  +
			",NOM_TITULO       ='"  + relator.getNomTitulo()  + "' "  +
			",IND_ATIVO        ='"  + relator.getIndAtivo()   + "' "  +
			",NOM_USERNAME     ='"  + relator.getNomUserName()+ "' "  +
			",DAT_PROC         =to_date('"  + relator.getDatProc()    + "','dd/mm/yyyy') "  +
			"WHERE COD_RELATOR ='"  + relator.getCodRelator() + "' "  ;			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);		 	 
			stmt.close();			
		} catch (Exception e) {			
			return false;
			
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}		
		return true;
	
	}
	public List getRelator(String codJunta) throws DaoException {
      	Connection conn =null ;	
  		List relator = new ArrayList();
      	try {
      		conn = serviceloc.getConnection(MYABREVSIST) ;
      		Statement stmt = conn.createStatement();
      		
      		String sCmd = "SELECT cod_relator,nom_relator,lpad(num_cpf,11,'0') num_cpf, nom_titulo,ind_ativo ";
      		sCmd += " from TPNT_RELATOR ";
      		sCmd += " WHERE cod_junta = '"+ codJunta + "'";
      		sCmd += " order by nom_relator";
      		ResultSet rs = stmt.executeQuery(sCmd);     
      		
      		while( rs.next() ) {
      			RelatorBean myRelator = new RelatorBean() ;
      			myRelator.setCodJunta(codJunta);
      			myRelator.setCodRelator(rs.getString("cod_relator"));
      			myRelator.setNomRelator(rs.getString("nom_relator"));
      			myRelator.setNumCpf(rs.getString("num_cpf"));
      			myRelator.setNomTitulo(rs.getString("nom_titulo"));
                myRelator.setIndAtivo(rs.getString("ind_ativo"));
      			relator.add(myRelator) ;
      		}    	
      		rs.close();
      		stmt.close();
      	}//fim do try
      	catch (Exception e) {
      		throw new DaoException(e.getMessage());
      	}//fim do catch
      	finally {
      		if (conn != null) {
      			try { serviceloc.setReleaseConnection(conn); }
      			catch (Exception ey) { }
      		}
      	}
      	return relator;
      }
	public List getRelatorBroker(RelatorBean RelatorId) throws DaoException {
		List relator = new ArrayList();
		try {
			String indContinuidade = Util.rPad(""," ",23);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			RelatorBean myRelator;
			while(true){ 
				String sCmd = broker.Transacao623(Util.lPad(RelatorId.getCodOrgao(),"0",6),Util.lPad(RelatorId.getCodJunta(),"0",6),Util.lPad(RelatorId.getNumCpf(),"9",11),"5",indContinuidade,Util.rPad(RelatorId.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") && (!sCmd.substring(0,3).equals("017")) )
					return null;
				if( sCmd.substring(0,3).equals("017") )
					return relator;	
				
				int ini, pos, fim;
				for(pos=0,ini=3,fim=9;(!sCmd.substring(ini,fim).equals(Util.lPad("","0",6))) && (pos<46);ini = fim, fim = ini+6,pos++){
					myRelator = new RelatorBean();
					myRelator.setCodOrgao(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim +6;
					myRelator.setCodJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 11;
					myRelator.setNumCpf(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 43;
					myRelator.setNomRelator(sCmd.substring(ini,fim));
					relator.add(myRelator);
				}
				indContinuidade = sCmd.substring(ini,fim);
				if(pos==46)
					break;
			}	
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		return relator;
	}
	public List getRelatores(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorId) throws DaoException {
		Connection conn =null ;	
		List relator = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo "+
			"FROM TPNT_RELATOR r,TPNT_JUNTA j "+
			"where r.cod_junta=j.cod_junta and j.ind_tipo='"+TipoJunta+"' AND "+
			"j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' "+
			" order by nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				relator.add(myRelator) ;
			}    	
			RelatorId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	public List getRelatoresAtivos(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorBeanId) throws DaoException {
		Connection conn =null ;	
		List relator = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo "+
			"FROM TPNT_RELATOR r,TPNT_JUNTA j "+
			"where r.cod_junta=j.cod_junta and j.ind_tipo='"+TipoJunta+"' AND "+
			"j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' AND "+
			"r.ind_ativo='S' "+
			" order by nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				relator.add(myRelator);
			}    	
			RelatorBeanId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	
	public List<RelatorBean> getRelatoresAutomatica(RelatorBean RelatorId) throws DaoException {
		Connection conn =null ;	
		List<RelatorBean> relator = new ArrayList<RelatorBean>();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo,r.qtd_proc_distrib "+
			"FROM TPNT_RELATOR r,TPNT_JUNTA j "+
			"where r.ind_ativo='S' "+
			" order by qtd_proc_distrib,nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
			//	myRelator.setQtdProcDistrib(rs.getInt("qtd_proc_distrib"));
				relator.add(myRelator) ;
			}    	
			RelatorId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	
	
	public List<RelatorBean> getRelatoresAutomatica(RelatorBean RelatorId, FuncionalidadesBean funcionalidadesBean) throws DaoException {
		Connection conn =null ;	
		List<RelatorBean> relator = new ArrayList<RelatorBean>();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo,r.qtd_proc_distrib "+
			"FROM TPNT_RELATOR r,TPNT_JUNTA j "+
			"where r.ind_ativo='S' "+
			"and j.ind_tipo='"+funcionalidadesBean.getTipoJunta()+"'"+
			" order by qtd_proc_distrib,nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
			    myRelator.setQtdProcDistrib(rs.getInt("qtd_proc_distrib"));
				relator.add(myRelator) ;
			}    	
			RelatorId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	
	
	public boolean RelatorUpdateBroker(RelatorBean myRelatorBroker, RelatorBean myRelatorBroker_aux){
		boolean retorno = true;
		String sCmd = "";
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			if( !myRelatorBroker.getNumCpf().equals(myRelatorBroker_aux.getNumCpf()) ){
				sCmd = broker.Transacao623(Util.lPad(myRelatorBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker_aux.getCodJunta(),"0",6),Util.lPad(myRelatorBroker_aux.getNumCpf(),"0",11),"3",Util.rPad(""," ",23),Util.rPad(myRelatorBroker_aux.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )
					return false;
				
				sCmd = broker.Transacao623(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") ){	
					sCmd = broker.Transacao623(Util.lPad(myRelatorBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker_aux.getCodJunta(),"0",6),Util.lPad(myRelatorBroker_aux.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelatorBroker_aux.getNomRelator()," ",40));
					return false;
				}
			}
			else{
				sCmd = broker.Transacao623(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"2",Util.rPad(""," ",23),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )	
					return false;
			}
		}
		catch (Exception ex)   {
			return false;
		}
		return true;
	}
	public boolean RelatorUpdateBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao623(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"2",Util.rPad(""," ",23),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	public boolean RelatorInsertBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao623(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RelatorDeleteBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao623(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"3",Util.rPad(""," ",23),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;	
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	public boolean JuntaInsertRelator(Vector vErro, List relatores, List relatores_aux) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			RelatorBean myRelator,myRelator_aux;
			for (int i = 0 ; i< relatores.size(); i++) {
				myRelator = (RelatorBean)relatores.get(i);
				myRelator_aux = (RelatorBean)relatores_aux.get(i);
				String indAtivo = verificaSitRelator(myRelator);
				if( (myRelator.getNomRelator().equals(myRelator_aux.getNomRelator())) 
						&& (myRelator.getNumCpf().equals(myRelator_aux.getNumCpf())) 
						&& (myRelator.getNomTitulo().equals(myRelator_aux.getNomTitulo()))
						&& (indAtivo.equals(myRelator.getIndAtivo())))
					continue;
				
				if ("".equals(myRelator.getNomRelator().trim()) == false){
					if(myRelator.getCodRelator().equals("")){	
						if( RelatorInsert(myRelator, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro de atualiza��o do Relator: " + myRelator.getNomRelator() + "\n");
							retorno = false;
						}
					}
					else{
						DaoBroker broker = DaoBrokerFactory.getInstance();
						String sCmd = "";
						if( !myRelator.getNumCpf().equals(myRelator_aux.getNumCpf()) ){
							sCmd = broker.Transacao623(Util.lPad(myRelator_aux.getCodOrgao(),"0",6),Util.lPad(myRelator_aux.getCodJunta(),"0",6),Util.lPad(myRelator_aux.getNumCpf(),"0",11),"3",Util.lPad(""," ",23),Util.rPad(myRelator_aux.getNomRelator()," ",40));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de exclus�o do Relator Broker: " + myRelator.getNomRelator() + "\n");	
								retorno = false;
								continue;
							}          		
							sCmd = broker.Transacao623(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelator.getNomRelator()," ",40));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de inser��o do Relator Broker: " + myRelator.getNomRelator() + "\n");	
								sCmd = broker.Transacao623(Util.lPad(myRelator_aux.getCodOrgao(),"0",6),Util.lPad(myRelator_aux.getCodJunta(),"0",6),Util.lPad(myRelator_aux.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelator_aux.getNomRelator()," ",40));
								retorno = false;
								continue;
							}
						}
						else{
							sCmd = broker.Transacao623(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"2",Util.rPad(""," ",23),Util.rPad(myRelator.getNomRelator()," ",40));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de atualiza��o do Relator no Broker: " + myRelator.getNomRelator() + "\n");	
								retorno = false;
								continue;
							}
						}
						if( RelatorUpdate(myRelator, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro de atualiza��o do Relator: " + myRelator.getNomRelator() + "\n");
							retorno = false;
						}
					}
				}
				else {
					if ("".equals(myRelator.getCodRelator()) == false) 
						if( RelatorDelete(myRelator_aux.getCodOrgao(), myRelator_aux, stmt) ){
							conn.commit();
							relatores.remove(i);
							relatores_aux.remove(i);
							i--;
						}
						else{
							conn.rollback();
							vErro.add("Erro de exclus�o do Relator: " + myRelator_aux.getNomRelator()+ " - " + myRelator_aux.getMsgErro());
							retorno = false;
						}
				}
			}
			/*Michel*/
			stmt.close();
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if(!retorno)
						conn.rollback(); 
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	public boolean RelatorDelete(String codOrgao, RelatorBean myRelator,Statement stmt) throws DaoException {
		String sCmd = "";
		String numGuia = "";
		try {
			if(!myRelator.getNumCpf().equals(""))
			{
				sCmd = "SELECT COD_LOTE_RELATOR FROM TPNT_LOTE_RELATOR " +
				" WHERE NUM_CPF = '"+myRelator.getNumCpf()+"'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					numGuia = rs.getString("COD_LOTE_RELATOR");
				}
			}
			
			if(numGuia.equals("")&&!myRelator.getIndAtivo().equals("N"))
			{
				sCmd  = "DELETE TPNT_Relator where cod_relator = '"+myRelator.getCodRelator()+"'";
				stmt.execute(sCmd);
				DaoBroker broker = DaoBrokerFactory.getInstance();
				sCmd = broker.Transacao623(Util.lPad(codOrgao,"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"3",Util.rPad(""," ",23),Util.rPad(myRelator.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
			else {
				if(!numGuia.equals(""))
					myRelator.setMsgErro("Relator vinculado em uma Guia.");
				else if(!myRelator.getIndAtivo().equals("N"))
					myRelator.setMsgErro("Relator inativo.");
				return false;
			}
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	public String getRelatores(JuntaBean myJunta)  throws DaoException {
		String myRelator ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			
			String	sCmd     = "select cod_relator,nom_relator from TPNT_RELATOR" +								   	
			" WHERE cod_Junta="+myJunta.getCodJunta()+								  
			" order by NOM_RELATOR";							
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()){
				
				myRelator += ",\""+rs.getString("cod_relator") +"\"";
				myRelator += ",\""+rs.getString("nom_relator") +"\"";							
			}
			myRelator = myRelator.substring(1,myRelator.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myRelator;
	}  
	
	public boolean RelatorUpdate(RelatorBean myRelator, Statement stmt) {    
		try{
			String sCmd  = "UPDATE TPNT_RELATOR set ";
			sCmd += "nom_relator ='"+myRelator.getNomRelator()+"',";
			sCmd += "cod_junta   ='"+myRelator.getCodJunta()+"', ";
			sCmd += "num_cpf     ='"+myRelator.getNumCpf()+"', ";
			sCmd += "nom_titulo  ='"+myRelator.getNomTitulo()+"',";
			sCmd += "ind_ativo   ='"+myRelator.getIndAtivo()+"',";			
			sCmd += " where cod_relator = '"+myRelator.getCodRelator()+"' ";
			stmt.execute(sCmd);
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	public boolean RelatorInsert(RelatorBean myRelator, Statement stmt) {
		Connection conn = null ;		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt_Rel = conn.createStatement();
			
			
			if (("".equals(myRelator.getCodRelator())) || ("0".equals(myRelator.getCodRelator())))  {
				String sCmd = "SELECT seq_TPNT_relator.nextval as codRelator FROM dual";
				ResultSet rs = stmt_Rel.executeQuery(sCmd);
				rs.next();
				myRelator.setCodRelator(rs.getString("codRelator"));
				rs.close();
				
				sCmd  = "INSERT INTO TPNT_RELATOR (cod_relator,";
				sCmd += "nom_relator,cod_junta,num_cpf,nom_titulo, ind_ativo,qtd_proc_distrib, num_ordem) ";
				sCmd += "VALUES (";
				sCmd += "'"+myRelator.getCodRelator()+"',";
				sCmd += "'"+myRelator.getNomRelator()+"',";
				sCmd += "'"+myRelator.getCodJunta()+"',";
				sCmd += "'"+myRelator.getNumCpf()+"',";
				sCmd += "'"+myRelator.getNomTitulo()+"'," ;
				sCmd += "'"+myRelator.getIndAtivo()+"')" ;				
				stmt_Rel.execute(sCmd);
				DaoBroker broker = DaoBrokerFactory.getInstance();
				sCmd = broker.Transacao623(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"1",Util.rPad(""," ",23),Util.rPad(myRelator.getNomRelator()," ",40));
				stmt_Rel.close();				
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
		}
		catch (Exception ex){
			return false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return true;
	}
	
	public String verificaSitRelator(RelatorBean myRelator) throws DaoException{
		String indAtivo = "";
		Connection conn = null;
		try {   
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			if(!myRelator.getCodRelator().equals(""))
			{
				String sCmd = "SELECT IND_ATIVO FROM TPNT_RELATOR WHERE " +
				" COD_RELATOR = " + myRelator.getCodRelator();
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					indAtivo = rs.getString("IND_ATIVO");
					if(indAtivo == null) indAtivo = "";
				}
			}
		}
		catch (Exception ex)   {
			throw new DaoException(ex.getMessage());
		}
		return indAtivo;
	}
	
	/* ******************************************************************************* */	
	/* ************** TODAS AS MANIPULACOES REFERENTES A  EVENTOS ******************** */
	/* ******************************************************************************* */
	
	protected void carregarEventos(EventoBean myEvento) throws DaoException {
		Connection conn = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT COD_EVENTO, DSC_EVENTO " +
			"FROM   TPNT_EVENTO " +
			"ORDER BY COD_EVENTO";
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sql);
			
			List<EventoBean> lista = new ArrayList<EventoBean>();
			while (rs.next()) {				 				 
				EventoBean evento = new EventoBean();
				evento.setCodEvento(Integer.parseInt(rs.getString("COD_EVENTO")));
				evento.setDscEvento(rs.getString("DSC_EVENTO"));
				lista.add(evento);		 
			}
			myEvento.setEventos(lista);
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}
	
	protected boolean EventoUpdate(EventoBean evento) throws DaoException {
		Connection conn = null;
		boolean ok=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "UPDATE TPNT_EVENTO " +
			"SET DSC_EVENTO ='" + evento.getDscEvento() + "' " +
			",COD_ORGAO_LOTACAO ='" + evento.getCodOrgaoLotacao() + "' " +
			",NOM_USERNAME ='" + evento.getNomUserName() + "' " +
			",DAT_PROC ='" + evento.getDatProc() + "' " +
			"WHERE COD_EVENTO ='" + evento.getCodEvento() + "'";		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				ok=false;
			stmt.close();
			
		} catch (Exception e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return ok;
	}
	
	protected boolean EventoInsert(EventoBean evento) throws DaoException {
		Connection conn = null;
		boolean ok=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "INSERT INTO TPNT_EVENTO (	COD_EVENTO, " +
			"DSC_EVENTO,COD_ORGAO_LOTACAO," +
			"NOM_USERNAME,DAT_PROC)"  +	 				
			
			"VALUES('" + evento.getCodEvento() + "','" +
			evento.getDscEvento() + "'," +
			"'" + evento.getCodOrgaoLotacao()	+ "'," +
			"'" + evento.getNomUserName() 	+ "'," +
			"'" + evento.getDatProc()	        + "')" ;
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				ok=false;
			stmt.close();			
		} catch (Exception e) {
			ok = false;
			throw new DaoException(e.getMessage());			
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return ok;
	}
	
	protected boolean EventoDelete(EventoBean evento) throws DaoException {
		Connection conn = null;
		boolean ok=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "DELETE FROM TPNT_EVENTO " +		 				 
			"WHERE COD_EVENTO ='" + evento.getCodEvento() + "'";		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);		
			if(stmt.getUpdateCount() <= 0)
				ok=false;
			stmt.close();
			
		} catch (Exception e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return ok;
	}
	
	/* ******************************************************************************* */	
	/* ************** TODAS AS MANIPULACOES REFERENTES A PENALIDADES ***************** */
	/* ******************************************************************************* */
	public void PenalidadeLeBean(PenalidadeBean myPenalidade,String chave) throws DaoException {
		Connection conn = null ;	
		String sCmd     = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			sCmd = " SELECT COD_PENALIDADE,DSC_PENALIDADE,DSC_ENQUADRAMENTO," +
			" NUM_PRAZO,IND_REINCIDENCIA,QTD_INFRACAO_DE,QTD_INFRACAO_ATE,COD_FATOR_AGRAVAMENTO,DSC_PENALIDADE_RES" +
			" FROM   TPNT_PENALIDADE ";			
			if ("descricao".equals(chave))  sCmd +=" where DSC_PENALIDADE = '"+myPenalidade.getDscPenalidade()+"' "; 
			else 
				sCmd +=" where COD_PENALIDADE = '"+myPenalidade.getCodPenalidade()+"' ";			
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myPenalidade.setCodPenalidade         (rs.getString("COD_PENALIDADE"));
				myPenalidade.setDscPenalidade         (rs.getString("DSC_PENALIDADE"));
				myPenalidade.setEnquadramento         (rs.getString("DSC_ENQUADRAMENTO"));
				myPenalidade.setNumPrazo              (rs.getInt   ("NUM_PRAZO"));				
				myPenalidade.setReincidente           (rs.getString("IND_REINCIDENCIA"));
				myPenalidade.setQtdInfracaoDe         (rs.getInt   ("QTD_INFRACAO_DE"));
				myPenalidade.setQtdInfracaoAte        (rs.getInt   ("QTD_INFRACAO_ATE"));
				myPenalidade.setCodFatorAgravamento   (rs.getString("COD_FATOR_AGRAVAMENTO" ));
				myPenalidade.setDscResumida           (rs.getString("DSC_PENALIDADE_RES"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException("Dao.Tab - PenalidadeLeBean: "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new DaoException("Dao.Tab - PenalidadeLeBean: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}	
	
	protected void carregarPenalidades(PenalidadeBean myPenalidade,int ordem) throws DaoException {
		Connection conn = null;
		String sCmd     = "";		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "SELECT COD_PENALIDADE FROM TPNT_PENALIDADE ";
			if (ordem==1)
				sCmd+="ORDER BY DSC_PENALIDADE" ;
			else
				sCmd+="ORDER BY COD_PENALIDADE" ;				
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sCmd);			
			List<PenalidadeBean> lista = new ArrayList<PenalidadeBean>();
			while (rs.next()) {				
				PenalidadeBean   penalidade = new PenalidadeBean();	
				penalidade.setCodPenalidade(rs.getString("COD_PENALIDADE"));
				PenalidadeLeBean(penalidade,"codigo");
				lista.add       (penalidade);		 
			}
			myPenalidade.setPenalidades(lista);
			rs.close();
			stmt.close();			
		} catch (Exception e) {
			throw new DaoException("Dao.Tab - carregarPenalidades: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}	
	
	protected void PenalidadeUpdate(PenalidadeBean penalidade) throws DaoException {
		Connection conn = null;	
		String sCmd ="";
		String result ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "UPDATE TPNT_PENALIDADE "        +
			"SET    DSC_PENALIDADE    ='"    + penalidade.getDscPenalidade()      + "' " +
			",DSC_ENQUADRAMENTO       ='"    + penalidade.getEnquadramento()      + "' " +
			",NUM_PRAZO               ='"    + penalidade.getNumPrazo()           + "' " +			
			",IND_REINCIDENCIA        ='"    + penalidade.getReincidente()        + "' " +
			",QTD_INFRACAO_DE         ='"    + penalidade.getQtdInfracaoDe()      + "' " +
			",QTD_INFRACAO_ATE        ='"    + penalidade.getQtdInfracaoAte()     + "' " +
			",COD_FATOR_AGRAVAMENTO   ='"    + penalidade.getCodFatorAgravamento()+ "' " +
			",DSC_PENALIDADE_RES      ='"    + penalidade.getDscResumida()        + "' " +
			",COD_ORGAO_LOTACAO       ='"    + penalidade.getCodOrgaoLotacao()    + "' " +
			",NOM_USERNAME            ='"    + penalidade.getNomUserName()        + "' " +
			",DAT_PROC                ='"    + penalidade.getDatProc()            + "' " +
			"WHERE  COD_PENALIDADE    ='"    + penalidade.getCodPenalidade()      + "' ";		 	
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);			
			stmt.close();	
			
			
			if(!"-1".equals(penalidade.getCodPenalidade())){
				String parteVar= sys.Util.lPad(penalidade.getCodPenalidade(),"0",3) +"2"+"000"+Util.rPad(penalidade.getDscPenalidade()," ",100)+
				penalidade.getEnquadramento() +	penalidade.getReincidente() + penalidade.getNumPrazo() +			
				penalidade.getQtdInfracaoDe() + penalidade.getQtdInfracaoAte() +
				penalidade.getCodFatorAgravamento();			
				
				ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();			
				result = chamaTransBRK("620",parteVar,"000",myUsuario);
				
				if (! result.substring(0,3).equals("000"))
					throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
				
			}
			
			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - PenalidadeUpdate: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}	
	}
	
	
	protected void PenalidadeInsert(PenalidadeBean penalidade) throws DaoException {
		Connection conn = null;
		String sCmd ="";
		String result = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = " INSERT INTO TPNT_PENALIDADE(COD_PENALIDADE, "             +
			" DSC_PENALIDADE,DSC_ENQUADRAMENTO,NUM_PRAZO, "             +			
			" IND_REINCIDENCIA,QTD_INFRACAO_DE,QTD_INFRACAO_ATE,COD_FATOR_AGRAVAMENTO,DSC_PENALIDADE_RES,"+
			" COD_ORGAO_LOTACAO,NOM_USERNAME,DAT_PROC)"+			
			" VALUES('" + penalidade.getCodPenalidade() 	               + "'," +
			"'" + penalidade.getDscPenalidade() 		                   + "'," +
			"'" + penalidade.getEnquadramento() 						   + "'," +
			"'" + penalidade.getNumPrazo() 	                           + "'," +			
			"'" + penalidade.getReincidente() 	                       + "'," +				   
			"'" + penalidade.getQtdInfracaoDe() 	                       + "'," +
			"'" + penalidade.getQtdInfracaoAte() 	                   + "'," +
			"'" + penalidade.getCodFatorAgravamento()                   + "'," +	
			"'" + penalidade.getDscResumida()                           + "'," +	
			"'" + penalidade.getCodOrgaoLotacao()	                   + "'," +
			"'" + penalidade.getNomUserName() 	                       + "'," +
			"'" + penalidade.getDatProc()	                           + "')" ;				
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);				
			stmt.close();
			
			String parteVar= sys.Util.lPad(penalidade.getCodPenalidade(),"0",3) +"1"+"000"+Util.rPad(penalidade.getDscPenalidade()," ",100)+
			penalidade.getEnquadramento() +	penalidade.getReincidente() + penalidade.getNumPrazo() +			
			penalidade.getQtdInfracaoDe() + penalidade.getQtdInfracaoAte() +
			penalidade.getCodFatorAgravamento();			
			
			ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();			
			result = chamaTransBRK("620",parteVar,"000",myUsuario);
			
			if (! result.substring(0,3).equals("000"))
				throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - PenalidadeInsert: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) {	}
			}
		}			
	}	
	
	protected void PenalidadeDelete(PenalidadeBean penalidade) throws DaoException {
		Connection conn = null;
		String sCmd ="";
		String result ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "DELETE FROM TPNT_PENALIDADE " +		 				 
			"WHERE COD_PENALIDADE       =" + penalidade.getCodPenalidade();			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);			
			stmt.close();
			
			String parteVar= sys.Util.lPad(penalidade.getCodPenalidade(),"0",3) +"3"+"000"+Util.rPad(penalidade.getDscPenalidade()," ",100)+
			penalidade.getEnquadramento() +	penalidade.getReincidente() + penalidade.getNumPrazo() +			
			penalidade.getQtdInfracaoDe() + penalidade.getQtdInfracaoAte() +
			penalidade.getCodFatorAgravamento();			
			
			ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();			
			result = chamaTransBRK("620",parteVar,"000",myUsuario);
			
			if (! result.substring(0,3).equals("000"))
				throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
			
			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - PenalidadeDelete: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}		
	}	
	
	/* ******************************************************************************* */	
	/* ********** TODAS AS MANIPULACOES REFERENTES A ORIGENS DE EVENTOS ************** */
	/* ******************************************************************************* */
	
	protected void carregarOrigenEventos(OrigemEventoBean myOrigem) throws DaoException {
		Connection conn = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT COD_ORIGEM_EVENTO, DSC_ORIGEM_EVENTO " +
			"FROM   TPNT_ORIGEM_EVENTO " +
			"ORDER BY COD_ORIGEM_EVENTO";
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sql);
			
			List<OrigemEventoBean> lista = new ArrayList<OrigemEventoBean>();
			while (rs.next()) {				 				 
				OrigemEventoBean origem = new OrigemEventoBean();
				
				origem.setCodOrigEvento(rs.getInt("COD_ORIGEM_EVENTO"));
				origem.setDescricao(rs.getString("DSC_ORIGEM_EVENTO"));		 		
				
				lista.add(origem);		 
			}
			myOrigem.setOrigensEventos(lista);
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}
	
	protected boolean OrigemEventoUpdate(OrigemEventoBean origem) throws DaoException {
		Connection conn = null;
		boolean updateOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "UPDATE TPNT_ORIGEM_EVENTO "  +
			"SET    DSC_ORIGEM_EVENTO ='" + origem.getDescricao() + "' " +
			",COD_ORGAO_LOTACAO ='" + origem.getCodOrgaoLotacao() + "' " +
			",NOM_USERNAME ='" + origem.getNomUserName() + "' " +
			",DAT_PROC ='" + origem.getDatProc() + "' " +
			"WHERE  COD_ORIGEM_EVENTO ="  + origem.getCodOrigEvento();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				updateOk=false;
			stmt.close();
			
		} catch (Exception e) {
			updateOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
		return updateOk;
	}
	
	protected boolean OrigemEventoInsert(OrigemEventoBean origem) throws DaoException {
		Connection conn = null;
		boolean insertOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "INSERT INTO TPNT_ORIGEM_EVENTO(COD_ORIGEM_EVENTO, " 	+
			"DSC_ORIGEM_EVENTO,COD_ORGAO_LOTACAO," +
			"NOM_USERNAME,DAT_PROC)"  +	 				 										   
			"VALUES(" + origem.getCodOrigEvento() 	+ "," +
			"'" + origem.getDescricao() 		+ "'," +
			"'" + origem.getCodOrgaoLotacao()	+ "'," +
			"'" + origem.getNomUserName() 	+ "'," +
			"'" + origem.getDatProc()	        + "')" ;
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				insertOk=false;
			stmt.close();
			
		} catch (Exception e) {
			insertOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return insertOk;
	}
	
	protected boolean OrigemEventoDelete(OrigemEventoBean origem) throws DaoException {
		Connection conn = null;
		boolean deleteOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "DELETE FROM TPNT_ORIGEM_EVENTO " +		 				 
			"WHERE COD_ORIGEM_EVENTO =" 	   + origem.getCodOrigEvento();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount()<= 0)
				deleteOk=false;
			stmt.close();
			
		} catch (Exception e) {
			deleteOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}
		return deleteOk;
	}
	
	/* ******************************************************************************* */	
	/* ************** TODAS AS MANIPULACOES REFERENTES A STATUS ********************** */
	/* ******************************************************************************* */
	
	protected String carregaDescStatus(String codStatus) throws DaoException {
		Connection conn = null;
		String status="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT NOM_STATUS_AUTO " +
			"FROM   TPNT_STATUS_PROCESSO " +
			"WHERE COD_STATUS_AUTO = '"+codStatus+"'";
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sql);			
			
			while (rs.next()) {				 				 
				status = rs.getString("NOM_STATUS_AUTO")	;				
				
			}		
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}
		return status;	 
	}
	protected void carregarStatus(StatusBean myStatus) throws DaoException {
		Connection conn = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT COD_STATUS_AUTO, NOM_STATUS_AUTO " +
			"FROM   TPNT_STATUS_PROCESSO " +
			"ORDER BY COD_STATUS_AUTO";
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sql);
			
			List<StatusBean> lista = new ArrayList<StatusBean>();
			while (rs.next()) {				 				 
				StatusBean status = new StatusBean();
				
				status.setCodStatus(rs.getString("COD_STATUS_AUTO"));
				status.setNomStatus(rs.getString("NOM_STATUS_AUTO"));		 		
				
				lista.add(status);		 
			}
			myStatus.setStatus(lista);
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}
	
	protected boolean StatusUpdate(StatusBean status) throws DaoException {
		Connection conn = null;
		boolean updateOk=true;
		String result = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "UPDATE TPNT_STATUS_PROCESSO "  +
			"SET    NOM_STATUS_AUTO ='" + status.getNomStatus() + "' " +
			",COD_ORGAO_LOTACAO ='" + status.getCodOrgaoLotacao() + "' " +
			",NOM_USERNAME ='" + status.getNomUserName() + "' " +
			",DAT_PROC ='" + status.getDatProc() + "' " +
			"WHERE  COD_STATUS_AUTO ="  + status.getCodStatus();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);		
			if(stmt.getUpdateCount() <= 0)
				updateOk=false;
			stmt.close();
			
			
			ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();	
			String parteVar = sys.Util.lPad(status.getCodStatus(),"0",3) +"2"+"000"+Util.rPad(status.getNomStatus()," ",30);			
			result = chamaTransBRK("621",parteVar,"000",myUsuario);
			
			if (! result.substring(0,3).equals("000"))
				throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
			
			
			
		} catch (Exception e) {
			updateOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
		return updateOk;
	}
	
	protected boolean StatusInsert(StatusBean status) throws DaoException {
		Connection conn = null;
		boolean insertOk=true;		
		String result = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "INSERT INTO TPNT_STATUS_PROCESSO(COD_STATUS_AUTO, " 	+
			"NOM_STATUS_AUTO,COD_ORGAO_LOTACAO," +
			"NOM_USERNAME,DAT_PROC)"  +		 										   
			"VALUES(" + status.getCodStatus() 	+ "," +
			"'" + status.getNomStatus() 	+ "'," +
			"'" + status.getCodOrgaoLotacao()	+ "'," +
			"'" + status.getNomUserName() 	+ "'," +
			"'" + status.getDatProc()	        + "')" ;
			
			Statement stmt = conn.createStatement();			
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				insertOk=false;
			stmt.close();
			ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();	
			
			String parteVar = sys.Util.lPad(status.getCodStatus(),"0",3) +"1"+"000"+Util.rPad(status.getNomStatus()," ",30);	
			result = chamaTransBRK("621",parteVar,"000",myUsuario);
			
			if (! result.substring(0,3).equals("000"))
				throw new DaoException("Erro na Grava��o da Base de Dados: " + result) ;
			
			
			
		} catch (Exception e) {
			insertOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
		return insertOk;
	}
	
	protected boolean StatusDelete(StatusBean status) throws DaoException {
		Connection conn = null;
		boolean deleteOk=true;
		String result = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "DELETE FROM TPNT_STATUS_PROCESSO " +		 				 
			"WHERE COD_STATUS_AUTO =" 	     + status.getCodStatus();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);		
			if(stmt.getUpdateCount() <= 0)
				deleteOk=false;
			stmt.close();
			
			ACSS.UsuarioBean myUsuario= new ACSS.UsuarioBean();	
			String parteVar = sys.Util.lPad(status.getCodStatus(),"0",3) +"3"+"000"+Util.rPad(status.getNomStatus()," ",30);			
			result = chamaTransBRK("621",parteVar,"000",myUsuario);
			
			if (! result.substring(0,3).equals("000"))
				throw new DaoException("Erro na Grava��o da Base de Dados: " + result) ;
			
			
		} catch (Exception e) {
			deleteOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return deleteOk;
	}	
	/* ******************************************************************************* */	
	/* ********** TODAS AS MANIPULACOES REFERENTES A MOTIVOS DE CANCELAMENTO ********* */
	/* ******************************************************************************* */
	
	protected void carregarMotivos(MotivoBean myMotivo) throws DaoException {		
		Connection conn = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "SELECT COD_MOTIVO, DSC_MOTIVO " +
			"FROM   TPNT_MOTIVO " +
			"ORDER BY COD_MOTIVO";
			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sql);
			
			List<MotivoBean> lista = new ArrayList<MotivoBean>();
			while (rs.next()) {				 				 
				MotivoBean motivo = new MotivoBean();
				
				motivo.setCodMotivo(rs.getInt("COD_MOTIVO"));
				motivo.setDescricao(rs.getString("DSC_MOTIVO"));				
				
				lista.add(motivo);		 
			}
			myMotivo.setMotivos(lista);
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}
	
	protected boolean MotivoUpdate(MotivoBean motivo) throws DaoException {
		Connection conn = null;
		boolean updateOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "UPDATE TPNT_MOTIVO "  +
			"SET    DSC_MOTIVO ='" + motivo.getDescricao() + "',' " +		
			"COD_ORGAO_LOTACAO ='" + motivo.getCodOrgaoLotacao() + "' " +
			",NOM_USERNAME ='" + motivo.getNomUserName() + "' " +
			",DAT_PROC ='" + motivo.getDatProc() + "' " +
			"WHERE  COD_MOTIVO ="  + motivo.getCodMotivo();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				updateOk=false;
			stmt.close();
			
		} catch (Exception e) {
			updateOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}
		return updateOk;
	}
	
	protected boolean MotivoInsert(MotivoBean motivo) throws DaoException {
		Connection conn = null;
		boolean insertOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "INSERT INTO TPNT_MOTIVO(COD_MOTIVO, " 	+
			"DSC_MOTIVO,COD_ORGAO_LOTACAO," +
			"NOM_USERNAME,DAT_PROC)"  +	 										   
			"VALUES(" + motivo.getCodMotivo() 	+ "," +
			"'" + motivo.getDescricao ()+ "',"+			
			"'" + motivo.getCodOrgaoLotacao()	+ "'," +
			"'" + motivo.getNomUserName() 	+ "'," +
			"'" + motivo.getDatProc()	        + "')" ;
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				insertOk=false;
			stmt.close();
			
		} catch (Exception e) {
			insertOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}
		return insertOk;
	}
	
	protected boolean MotivoDelete(MotivoBean motivo) throws DaoException {
		Connection conn = null;
		boolean deleteOk=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			String sql = "DELETE FROM TPNT_MOTIVO " +		 				 
			"WHERE COD_MOTIVO =" 	     + motivo.getCodMotivo();		 	
			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sql);	
			if(stmt.getUpdateCount() <= 0)
				deleteOk=false;
			stmt.close();
			
		} catch (Exception e) {
			deleteOk=false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	
		return deleteOk;
	}
	
	
	
	/**--------------------------------------
	 * Relativo ao ConteudoEmailBean
	 ---------------------------------------*/
	/*Michel*/
	//Consulta conteudo de email na tabela TPNT_CONTEUDO_EMAIL
	public boolean consultaConteudoEmail(ConteudoEmailBean myEmail) throws sys.DaoException{
		Connection conn = null;
		boolean temEmail=false;
		try {			
			String sql = "SELECT A.COD_EVENTO, A.COD_ORGAO, A.TXT_EMAIL," +
			" A.NOM_REMETENTE, A.TXT_ASSUNTO, A.NOM_LOGIN, A.TXT_SENHA,B.DSC_EVENTO " +
			" FROM TPNT_EMAIL_EVENTO A" +
			" left JOIN (SELECT DSC_EVENTO,COD_EVENTO FROM TPNT_EVENTO) B on  (A.COD_EVENTO=B.COD_EVENTO) "+			
			" WHERE A.COD_ORGAO='"+myEmail.getCodOrgao()+"' "+
			" AND A.COD_EVENTO='"+myEmail.getCodEvento()+"'";
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				temEmail=true;
				myEmail.setCodOrgao(rs.getString("COD_ORGAO"));
				myEmail.setCodEvento(rs.getString("COD_EVENTO"));
				myEmail.setAssunto(rs.getString("TXT_ASSUNTO"));
				myEmail.setRemetente(rs.getString("NOM_REMETENTE"));
				myEmail.setConteudoEmail(rs.getString("TXT_EMAIL"));
				myEmail.setLogin(rs.getString("NOM_LOGIN"));
				myEmail.setSenha(rs.getString("TXT_SENHA"));
				myEmail.setDscEvento(rs.getString("DSC_EVENTO"));				
			}
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("PNT.DaoTabelas: consultaConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return temEmail;
	}
	
	// Altera conteudo de email na tabela TPNT_CONTEUDO_EMAIL
	public void alteraConteudoEmail(String[] campos, String[] valores, String conteudo,
			String nomRemetente, String txtAssunto, String login, String senha)
	throws sys.DaoException{
		
		Connection conn = null;
		try {
			String sql = "UPDATE TPNT_EMAIL_EVENTO SET " +
			"TXT_EMAIL = '" + conteudo + "', " +
			"NOM_REMETENTE = '" + nomRemetente + "', "+
			"TXT_ASSUNTO = '" + txtAssunto + "', " +
			"NOM_LOGIN = '" + login + "', " +
			"TXT_SENHA = '" + senha + "' " ;
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("PNT.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	// Insere conteudo de email na tabela TPNT_CONTEUDO_EMAIL
	public void insereConteudoEmail(String[] valores) throws sys.DaoException{
		Connection conn = null;
		try {
			String sql = "INSERT INTO TPNT_EMAIL_EVENTO (COD_ORGAO,COD_EVENTO," +
			"TXT_EMAIL,NOM_REMETENTE,TXT_ASSUNTO, NOM_LOGIN, TXT_SENHA) ";
			sql += "VALUES ('" + valores[0] + "', '" + valores[1] + "', '" + valores[2] + "',"+
			"'"+ valores[3] + "', '" +  valores[4] + "','" + valores[5]+ "','" + valores[6]+"')";
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("PNT.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	//	Excluir conteudo de email na tabela TPNT_CONTEUDO_EMAIL
	public void excluiConteudoEmail(String[] campos, String[] valores) throws sys.DaoException{
		Connection conn = null;
		try {
			String sql = "DELETE FROM TPNT_EMAIL_EVENTO ";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("PNT.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
//	================================
	public void LeRetorno(CodigoRetornoBean myRetorno) throws DaoException {
		Connection conn = null; 
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			ResultSet rs=null;
			String sCmd="SELECT * FROM TSMI_CODIGO_RETORNO " +
			"WHERE NUM_CODIGO_RETORNO='"+myRetorno.getNumRetorno()+"'";
			rs=stmt.executeQuery(sCmd);
			if (rs.next())	{
				myRetorno.setCodCodigoRetorno(rs.getInt("COD_CODIGO_RETORNO"));
				myRetorno.setNumRetorno(rs.getString("NUM_CODIGO_RETORNO"));
				myRetorno.setDescRetorno(rs.getString("DSC_CODIGO_RETORNO"));
				myRetorno.setIndEntregue(rs.getString("IND_ENTREGUE"));
			}
			// fechar a transa��o
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {   
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return;
	}	
	
	public void AgravoLeBean(AgravoBean myagravo,String chave) throws DaoException {
		Connection conn = null ;	
		String sCmd     = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			sCmd = " SELECT COD_INFRACAO,FATOR_AGRAVAMENTO FROM  TPNT_AGRAVO ";			
			sCmd +=" WHERE COD_INFRACAO = '"+myagravo.getCodInfracao()+"' ";			
			
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myagravo.setCodInfracao(rs.getString("COD_INFRACAO"));
				myagravo.setAgravo(rs.getString("FATOR_AGRAVAMENTO"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException("Dao.Tab - AgravoLeBean: "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new DaoException("Dao.Tab - AgravoLeBean: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	protected void carregarInfracoes(AgravoBean myAgravo) throws DaoException {
		Connection conn = null;
		String sCmd     = "";		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = " SELECT COD_INFRACAO                       " +
			" FROM TPNT_AGRAVO ORDER BY FATOR_AGRAVAMENTO" ;			
			Statement stmt = conn.createStatement();		 
			ResultSet rs = stmt.executeQuery(sCmd);			
			List<AgravoBean> lista = new ArrayList<AgravoBean>();
			while (rs.next()) {				
				AgravoBean   agravo = new AgravoBean();	
				agravo.setCodInfracao(rs.getString("COD_INFRACAO"));
				AgravoLeBean(agravo,"codigo");
				lista.add       (agravo);		 
			}
			myAgravo.setAgravos(lista);
			rs.close();
			stmt.close();			
		} catch (Exception e) {
			throw new DaoException("Dao.Tab - carregarAgravos: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					System.out.println(e);
				}
			}
		}	 
	}
	
	protected void AgravoUpdate(AgravoBean agravo) throws DaoException {
		Connection conn = null;	
		String sCmd ="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "UPDATE TPNT_AGRAVO "            +
			"SET    COD_INFRACAO    ='"      + agravo.getCodInfracao()      + "' " +
			",FATOR_AGRAVAMENTO     ='"      + agravo.getAgravo()           + "' " +	
			",NOM_USERNAME          ='"      + agravo.getNomUserName()      + "' " +
			",COD_ORGAO_LOTACAO     ='"      + agravo.getCodOrgaoLotacao()  + "' " +					
			",DAT_PROC              ='"      + agravo.getDatProc()          + "' " +			       
			"WHERE  COD_INFRACAO    ='"      + agravo.getCodInfracao()      + "' " ;		 	
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);			
			stmt.close();			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - AgravoUpdate: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}	
	}
	
	protected void AgravoInsert(AgravoBean agravo) throws DaoException {
		Connection conn = null;
		String sCmd ="";		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = " INSERT INTO TPNT_AGRAVO(COD_INFRACAO, "             +
			" FATOR_AGRAVAMENTO,NOM_USERNAME,COD_ORGAO_LOTACAO,DAT_PROC)"         +			
			" VALUES('" + agravo.getCodInfracao() 	             + "'," +
			"'"         + agravo.getAgravo() 		             + "'," +
			"'"         + agravo.getNomUserName() + "', " +
			"'"         + agravo.getCodOrgaoLotacao() + "', " +			       
			"'"         + agravo.getDatProc() + "')";
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);				
			stmt.close();			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - AgravoInsert: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) {	}
			}
		}			
	}
	
	protected void AgravoDelete(AgravoBean agravo) throws DaoException {
		Connection conn = null;
		String sCmd ="";	
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			sCmd = "DELETE FROM TPNT_AGRAVO " +		 				 
			"WHERE COD_INFRACAO       =" + agravo.getCodInfracao();			
			Statement stmt = conn.createStatement();		 
			stmt.executeQuery(sCmd);			
			stmt.close();			
		} catch (Exception e) {			
			throw new DaoException("Dao.Tab - AgravoDelete: "+e.getMessage()+" - "+sCmd);
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 	}
			}
		}		
	}			

	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	
}