package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import PNT.DaoBrokerFactory;
import TAB.DaoBroker;
import TAB.DaoException;

/**
 * <b>Title:</b>        SMIT - Entrada de Recurso - Evento Bean<br>
 * <b>Description:</b>  Evento Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra.
 * @version 1.0
 */

public class EventoBean extends TabelaBean { 
	
	private Integer codEvento;
	private String dscEvento;
	private List <EventoBean>eventos;
	
	public EventoBean() {
		super();
		codEvento        = -1;
		dscEvento        = "";		
		eventos = new ArrayList<EventoBean>(); 
	    
	}
	
	public Integer getCodEvento() {
		return codEvento;
	}
	
	public void setCodEvento(Integer codEvento) {
		this.codEvento = codEvento;
	}
	
	public String getDscEvento() {
		return dscEvento;
	}
	
	public void setDscEvento(String dscEvento) {
		this.dscEvento = dscEvento;
	}
	
	public List<EventoBean> getEventos() {
		return eventos;
	}
	
	public void setEventos(List<EventoBean> eventos) {
		this.eventos = eventos;
	}  
	
	public void carregar(int min, int livre) throws sys.BeanException {
		Vector <String>vErro;
		try {			
			// Carregando todas as penalidades cadastradas no banco
			Dao daoEvento = DaoFactory.getInstance();
			daoEvento.carregarEventos(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.eventos.size()<min-livre) 
				livre = min-eventos.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.eventos.add(new EventoBean()) ;
			}
		}
		catch(Exception e){		
			vErro = new  Vector <String>() ;
			vErro.addElement("Erro ao carregar Eventos de Cancelamento: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 
	
	public boolean atualizar(EventoBean eventosCadastrados) throws sys.BeanException {
		Vector <String> vErro;
		boolean atualizacaoOk=false;
		String result = "";		
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			EventoBean eventosAlterados = this;
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			/*	Percorrendo as listas de penalidades para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< eventosAlterados.getEventos().size(); i++) {
				
				EventoBean motCad = (EventoBean)eventosCadastrados.getEventos().get(i);
				EventoBean motAlt = (EventoBean)eventosAlterados.getEventos().get(i);
				
				// MOTIVOS NOVOS E REMOVIDOS (identificando e tratando)
				if (! motAlt.getCodEvento().equals(motCad.getCodEvento()) ) {
					
					Dao daoEvento = DaoFactory.getInstance();
					
					if(! motAlt.getCodEvento().equals(-1))
						if(daoEvento.EventoInsert(motAlt) && (atualizacaoOk==false)) {
							broker.Transacao627(motAlt.getCodEvento().toString(),"1","   ",motAlt.getDscEvento());
							if (result.substring(0,3).equals("016")) {
								result = broker.Transacao627(motAlt.getCodEvento().toString(),"2","   ",motAlt.getDscEvento());
								if (! result.substring(0,3).equals("000"))
									throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							}
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
						}
					
					if(! motCad.getCodEvento().equals(""))
						if(daoEvento.EventoDelete(motCad) && (atualizacaoOk==false) ) {
							broker.Transacao627(motCad.getCodEvento().toString(),"3","   ",motCad.getDscEvento());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;							
						}
					
				} 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		motAlt.getCodEvento().equals(motCad.getCodEvento()) 
						&&(! motAlt.getDscEvento().equals(motCad.getDscEvento()))){
					Dao daoEvento = DaoFactory.getInstance();
					if(daoEvento.EventoUpdate(motAlt) && (atualizacaoOk==false) ) {
						result = broker.Transacao627(motAlt.getCodEvento().toString(),"2","   ",motAlt.getDscEvento());
						if (result.substring(0,3).equals("017")) {
							result = broker.Transacao627(motAlt.getCodEvento().toString(),"1","   ",motAlt.getDscEvento());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						}
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;						
					}
				}
				
			}   		
		}catch(Exception e){		
			vErro = new Vector <String>() ;
			vErro.addElement("Erro ao atualizar Eventos de Cancelamento: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk;
	}	}