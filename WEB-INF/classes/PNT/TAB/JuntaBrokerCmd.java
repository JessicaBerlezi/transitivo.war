package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import ACSS.OrgaoBean;
import PNT.TAB.JuntaBean;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar PerfisSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company: </b> MIND Informatica <br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class JuntaBrokerCmd extends sys.Command {

  private static final String jspPadrao="/PNT/TAB/JuntaBroker.jsp";    
  private String next;

  public JuntaBrokerCmd() {
    next = jspPadrao;
  }

  public JuntaBrokerCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
	String atualizarDependente;
	OrgaoBean OrgaoId= null;
    try {      
    	// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
 		if(acao == null)
		  acao =" ";   
		if( (OrgaoId=(OrgaoBean)req.getAttribute("OrgaoId")) == null )
			OrgaoId = new OrgaoBean();
	    String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao == null) 
		  codOrgao =""; 
		
		JuntaBean JuntaId = (JuntaBean)req.getAttribute("JuntaId");
		if(JuntaId == null)
			JuntaId = new JuntaBean();
		
		atualizarDependente ="N";  
	    if("buscaOrgao".indexOf(acao)>=0) {
	      Vector errosPosicoes = new Vector();	    
	      JuntaId.setCodOrgao(codOrgao);
	      List juntaSmit = JuntaId.getJunta(codOrgao);
 	      List juntaBroker = JuntaId.getJuntaBroker();
 	      if( !JuntaId.compareSmitBroker(juntaSmit,juntaBroker,errosPosicoes) )
 	      	atualizarDependente = "N";
 	      else{
 	      	atualizarDependente = "S";
 	      	OrgaoId.Le_Orgao(codOrgao,0);
 	      	for(int i=0;i<4;i++){
 	      		juntaBroker.add(new JuntaBean());
 	      		juntaSmit.add(new JuntaBean());
 	      	}
 	      	req.setAttribute("JuntaBrokerId",juntaBroker) ;
 	      	req.setAttribute("JuntaSmitId",juntaSmit) ;
 	      }
	    }
		Vector vErro = new Vector(); 
		 
		if(acao.compareTo("A") == 0){
			String[] sigJunta_aux = req.getParameterValues("sigJunta_aux");
			String[] sigJunta 	  = new String[sigJunta_aux.length];
			
			String[] nomJunta_aux = req.getParameterValues("nomJunta_aux");
			String[] nomJunta     = new String[nomJunta_aux.length];
			
			String[] codJunta = req.getParameterValues("codJunta");

			String[] indTipo_aux  = req.getParameterValues("indTipo_aux");
			String[] indTipo      = new String[indTipo_aux.length]; 
			
			String[] sigJuntaBroker_aux = req.getParameterValues("sigJuntaBroker_aux");
			String[] sigJuntaBroker     = new String[sigJuntaBroker_aux.length];
			
			String[] nomJuntaBroker_aux = req.getParameterValues("nomJuntaBroker_aux");
			String[] nomJuntaBroker     = new String[nomJuntaBroker_aux.length];
			
			String[] codJuntaBroker = req.getParameterValues("codJuntaBroker");

			String[] indTipoBroker_aux  = req.getParameterValues("indTipoBroker_aux");
			String[] indTipoBroker      = new String[indTipoBroker_aux.length];
			
			for(int i=0; i< codJunta.length;i++){
				sigJunta[i] 	 	= req.getParameter("sigJunta"+i);
				nomJunta[i]     	= req.getParameter("nomJunta"+i);
				indTipo[i]      	= req.getParameter("indTipo"+i);
				sigJuntaBroker[i]   = req.getParameter("sigJuntaBroker"+i);
				nomJuntaBroker[i]   = req.getParameter("nomJuntaBroker"+i);
				indTipoBroker[i]    = req.getParameter("indTipoBroker"+i);
			}
			
			List juntaSmit       = new ArrayList(); 
			List juntaSmit_aux   = new ArrayList();
			List juntaBroker     = new ArrayList();
			List juntaBroker_aux = new ArrayList();
			JuntaBean myJunta, myJunta_aux, myJuntaBroker, myJuntaBroker_aux;
			for (int i = 0; i < sigJunta.length;i++) {
				myJunta           = new JuntaBean() ;
				myJunta_aux       = new JuntaBean() ;
				myJuntaBroker     = new JuntaBean() ;
				myJuntaBroker_aux = new JuntaBean() ;
				myJunta.setCodOrgao(codOrgao);
				myJunta.setSigJunta(sigJunta[i]);
				myJunta_aux.setSigJunta(sigJunta_aux[i]);
				myJuntaBroker.setCodOrgao(codOrgao);
				myJuntaBroker.setSigJunta(sigJuntaBroker[i]);
				myJuntaBroker_aux.setCodOrgao(codOrgao);
				myJuntaBroker_aux.setSigJunta(sigJuntaBroker_aux[i]);
				myJunta.setNomeJunta(nomJunta[i]);
				myJunta_aux.setNomeJunta(nomJunta_aux[i]);
				myJuntaBroker.setNomeJunta(nomJuntaBroker[i]);
				myJuntaBroker_aux.setNomeJunta(nomJuntaBroker_aux[i]);
				myJunta.setCodJunta(codJunta[i]);
				myJuntaBroker.setCodJunta(codJuntaBroker[i]);
				myJunta.setIndTipo(indTipo[i]);
				myJunta_aux.setIndTipo(indTipo_aux[i]);
				myJuntaBroker.setIndTipo(indTipoBroker[i]);
				myJuntaBroker_aux.setIndTipo(indTipoBroker_aux[i]);
				
				juntaSmit.add(myJunta);
				juntaSmit_aux.add(myJunta_aux);
				juntaBroker.add(myJuntaBroker);
				juntaBroker_aux.add(myJuntaBroker_aux);
			}
			JuntaId = new JuntaBean(); 
			if( JuntaId.isInsertJuntaBroker(juntaSmit, juntaSmit_aux, juntaBroker, juntaBroker_aux) )
				JuntaId.setMsgErro("Dados Alterados com Sucesso");
			atualizarDependente = "S";
			OrgaoId.Le_Orgao(codOrgao,0);
			req.setAttribute("JuntaBrokerId",juntaBroker) ;
			req.setAttribute("JuntaSmitId",juntaSmit) ;
		}
		req.setAttribute("atualizarDependente",atualizarDependente);	
		req.setAttribute("OrgaoId",OrgaoId);
		req.setAttribute("JuntaId",JuntaId);
    }
    catch (Exception ue) {
      throw new sys.CommandException("JuntaBrokerCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
}


