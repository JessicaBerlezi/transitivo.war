package PNT.TAB;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import PNT.DaoBrokerFactory;
import TAB.DaoException;

/**
 * <b>Title:</b>        SMIT - Entrada de Recurso - Agravo Bean<br>
 * <b>Description:</b>  Agravo Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra.
 * @version 1.0
 */

public class AgravoBean extends TabelaBean { 
	
	private String codInfracao;
	private String agravo;
	private List <AgravoBean> agravos;
	
	public AgravoBean() {
		super();
		codInfracao        = "-1";
		agravo             = "";		
		agravos = new ArrayList<AgravoBean>(); 
	    
	}
	
	public String getCodInfracao() {
		return codInfracao;
	}
	
	public void setCodInfracao(String codInfracao) {
		this.codInfracao = codInfracao;
	}
	
	public String getAgravo() {
		return agravo;
	}
	
	public void setAgravo(String agravo) {
		this.agravo = agravo;
	}
	
	public List<AgravoBean> getAgravos() {
		return agravos;
	}
	
	public void setAgravos(List<AgravoBean> agravos) {
		this.agravos = agravos;
	}  
	
	public void carregar(int min, int livre) throws sys.BeanException {
		Vector <String>vErro;
		try {			
			// Carregando todas os agraavos cadastrados no banco
			Dao daoInfracao = DaoFactory.getInstance();
			daoInfracao.carregarInfracoes(this);	 
			
			// A tela vai ter campos vazios para a inclus�o de novos dados			
			// Com este trecho de c�digo, sao criadas instancias vazias
			// s� para exibir os campos vazios na interface gr�fica.
			if (this.agravos.size()<min-livre) 
				livre = min-agravos.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.agravos.add(new AgravoBean()) ;
			}
		}
		catch(Exception e){		
			vErro = new  Vector <String>() ;
			vErro.addElement("Erro ao carregar Agravos de Cancelamento: " + e.getMessage()+ " \n");
			throw new sys.BeanException(e.getMessage()) ;				
		}		
	} 
	public boolean atualizar(AgravoBean agravosCadastradas) throws sys.BeanException {		
		boolean atualizacaoOk=false;
		String result = "";
		try {		
			/* 	Esta declaracao foi feita para explicitar que este objeto (this)
			 cont�m informa��es da tela e portanto com as altera��es feitas
			 pelo usu�rio													*/
			AgravoBean agravosAlteradas = this;
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			
			/*	Percorrendo as listas de agravos para identificar qdo uma
			 penalidade deve ser inserida, alterada, exclu�da ou apenas mantida */
			for (int i=0 ; i< agravosAlteradas.getAgravos().size(); i++) {
				
				AgravoBean agravoCad = (AgravoBean)agravosCadastradas.getAgravos().get(i);
				AgravoBean agravoAlt = (AgravoBean)agravosAlteradas.getAgravos().get(i);
				
				// PENALIDADES NOVAS E REMOVIDAS (identificando e tratando)				
					if(agravoCad.getCodInfracao()            .equals("-1")						
							&& (!agravoAlt.getAgravo()       .equals(""  )))
					{
						Dao daoAgravos = DaoFactory.getInstance();
						daoAgravos.AgravoInsert(agravoAlt); 
						broker.Transacao625(agravoAlt.getCodInfracao().toString(),"1","   ",agravoAlt.getAgravo());
						if (result.substring(0,3).equals("016")) {
							result = broker.Transacao625(agravoAlt.getCodInfracao().toString(),"2","   ",agravoAlt.getAgravo());
							if (! result.substring(0,3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						}
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;				
					}					
				
					if(!agravoCad.getCodInfracao()          .equals("-1")						
							&& (agravoAlt.getAgravo()       .equals(""   )))
					{
						Dao daoAgravos = DaoFactory.getInstance();
						daoAgravos.AgravoDelete(agravoCad);
						broker.Transacao625(agravoCad.getCodInfracao().toString(),"3","   ",agravoCad.getAgravo());
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
							atualizacaoOk=true;
				    } 				
				
				// PENALIDADES ALTERADAS (identificando e tratando)
				if(		agravoAlt.getCodInfracao().equals(agravoCad.getCodInfracao()) 
						&& 	( 	  (! agravoAlt.getAgravo().equals(agravoCad.getAgravo())))){
					
					Dao daoAgravos = DaoFactory.getInstance();
					daoAgravos.AgravoUpdate(agravoAlt);
					result = broker.Transacao625(agravoAlt.getCodInfracao().toString(),"2","   ",agravoCad.getAgravo());
					if (result.substring(0,3).equals("017")) {
						result = broker.Transacao625(agravoAlt.getCodInfracao().toString(),"1","   ",agravoCad.getAgravo());
						if (! result.substring(0,3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
					}
					if (! result.substring(0,3).equals("000"))
						throw new DaoException("Erro na atualiza��o da Base de Dados: " + result) ;
						atualizacaoOk=true;
				}					
			}   		
		}catch(Exception e){			
			throw new sys.BeanException(e.getMessage()) ;				
		}	
		return atualizacaoOk; 
	}	
		}