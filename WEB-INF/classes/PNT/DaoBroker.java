package PNT;

import sys.DaoException;
import java.sql.Connection;
import java.util.List;
import java.util.Vector;

import REC.ParamOrgBean;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;


/**
 * @author glaucio
 *
 *  To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface DaoBroker {
	public abstract void buscarListaProcessos(ProcessoBean myProcesso, String ordem) throws DaoException ;	
	public abstract void LeProcesso(ProcessoBean myProcesso) throws DaoException;
	public abstract void LeProcesso(ProcessoBean myProcesso,String tpChave) throws DaoException;
	public abstract void LeProcessoPenalidade(ProcessoBean myProcesso, String numProcessosAplicarPenalidade) throws DaoException;
	public abstract void GravaProcesso(ProcessoBean myProcesso,Connection conn) throws DaoException;	
	public abstract void LeProcessoAnts(ProcessoBean myProcesso) throws DaoException;
	public abstract void GravaProcessoAnt(ProcessoAntBean myProcesso,Connection conn) throws DaoException;	
	public abstract void LeNotificacoes(ProcessoBean myProcesso) throws DaoException;
	public abstract void GravaNotificacao(NotificacaoBean myNotificacao,Connection conn) throws DaoException;
	public abstract void ExcluirNotificacao(NotificacaoBean myNotificacao,Connection conn) throws DaoException;	
	public abstract void LeNotificacao(NotificacaoBean myNotif,String tpChave) throws DaoException;	
	public abstract void AtualizaNotificacao(NotificacaoBean myNotif) throws DaoException;	
	
	public abstract void LeMultas(ProcessoBean myProcesso) throws DaoException;
	public abstract void GravaMulta(MultasBean myMulta,Connection conn) throws DaoException;	
	public abstract void GravaHistorico(HistoricoBean myHist,Connection conn) throws DaoException;
	public abstract void LeHistoricos(ProcessoBean myProcesso) throws DaoException;
	public abstract void LeRequerimentos(ProcessoBean myProcesso) throws DaoException;
	public abstract void GravaRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException;
	public abstract void GravaRequerimentoGuia(RequerimentoBean myReq,Connection conn) throws DaoException;
	public abstract void AtualizarRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException;	
	public abstract void AtualizarRequerimentoAta(RequerimentoBean myReq,Connection conn) throws DaoException;
	public abstract void AtualizarRequerimentoAtaPeloNumero(RequerimentoBean myReq,Connection conn) throws DaoException;
	
	public abstract void LeAtasDO(ProcessoBean myProcesso) throws DaoException;
	public abstract void LeAtasDO(AtaDOBean myAta) throws DaoException;
	public abstract void AtualizaEditalDO(AtaDOBean myAta) throws sys.BeanException;
	public abstract void AtualizaDataPublicacaoDO(AtaDOBean myAta) throws sys.BeanException;
	public Vector ConsultaAtaDownload(AtaDOBean AtaDOBeanId, String dataIni, String dataFim) throws DaoException;
	public void DownloadArquivoAta(AtaDOBean AtaDOBeanId) throws DaoException;
	public abstract boolean gravaArquivo(AtaDOBean AtaDOId, String quantProc,UsuarioBean usrlogado) throws DaoException;
	public boolean gravaArquivo(AtaDOBean myAta,ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId,UsuarioBean UsrLogado) throws DaoException;	
	public abstract void atualizaDigitalizacao(NotificacaoBean myNotif) throws DaoException;
	public abstract String GravaARDigitalizado(ARDigitalizadoBean myAR) throws DaoException;
	public abstract ARDigitalizadoBean ConsultaARDigitalizado(String[] campos, String[] valores) throws DaoException;
	public abstract void GravaLogARDigitalizado(LogARDigitalizadoBean myLog) throws DaoException;
	public abstract void LeARDigitalizado(ProcessoBean myProcesso) throws DaoException;
	public abstract void consultaStatusProc(RelatProcStatusBean relProcStatus) throws DaoException;
	public abstract boolean GravaARDigitalizadoTmp(ARDigitalizadoBean myAR) throws DaoException;
	public abstract String ConsultaCodRet(String codRet) throws DaoException;
	public abstract void CarregaLista(PNT.PaginacaoBean auxClasse, String acao) throws DaoException;
	public abstract String BuscaTotalReg(PNT.PaginacaoBean auxClasse, String acao) throws DaoException;
	public abstract void buscarListaProcessos(ProcessoBean myProcesso, String ordem, ACSS.UsuarioBean myUsrLog) throws DaoException;
	public abstract String buscaNomRelator(String cpf,String orgaoLotacao)throws DaoException; 
	public abstract List<AtaDOBean> ConsultaAtasDO(AtaDOBean myAtaDO) throws DaoException ;
	public String Transacao622(String codOrgao, String codJunta, String codAcao,
			String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd,
			String numEnd, String txtCompl, String nomBairro, String numCep, String codMun,
			String numDddTel, String numTel, String numFax, Connection conn) throws DaoException ;
	
	public abstract String Transacao622(String codOrgao, String codJunta, String codAcao,
			String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd,
			String numEnd, String txtCompl, String nomBairro, String numCep, String codMun,
			String numDddTel, String numTel, String numFax) throws DaoException;
	
	public abstract String Transacao623(String codOrgao, String codJunta, String numCPF,
			String codAcao, String indContinua, String nomRelator) throws DaoException;
	
	public String Transacao624(String codPenalidade, String codAcao, String indContinua,
			String descPenalidade,String enquadramento,String indReincidencia,
			String numPrazo,String qtdInfracaoDe,String qtdInfracaoAte,String codigoFator,String descResumida) throws sys.DaoException;
    public String Transacao625(String codInfracao, String codAcao, String indContinua, String codAgravo) throws sys.DaoException;    
    public String Transacao626(String codStatus, String codAcao, String indContinua, String nomStatus) throws sys.DaoException;
    public String Transacao627(String codEvento, String codAcao, String indContinua, String nomEvento) throws sys.DaoException;
    public String Transacao629(String codMotivo, String codAcao, String indContinua, String nomMotivo) throws sys.DaoException;
    public String Transacao630(String codOrgao, String codAcao, String indContinua, 
    		String prazoDef, String prazoEmissao, String prazoEntrega, String prazoPublDO,
    		String prazoRecursoPenal, String prazoRecurso2Inst, String prazoProsperaResult,
    		String prazoEntregaCNH, String folgaDef, String folgaRecursoPenal, String folgaRecurso2Inst,
    		String nomAssDef, String nomAssRecurso, String nomAssEntrega, String secretariaProcesso) throws sys.DaoException;
	public abstract void GravaProcessoOracle(ProcessoBean myProcesso) throws DaoException;
	
	public void atualizaDigitalizacaoPorNotificacao(NotificacaoBean myNotif) throws DaoException;
}