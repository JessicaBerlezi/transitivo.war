package PNT;

import java.util.Vector;
import java.sql.Connection;

import PNT.TAB.CodigoRetornoBean;
/**
 * <b>Title:</b>        SMIT - Pontuação <br>
 * <b>Description:</b>  Notificacao Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class NotificacaoBean   { 
		
	private String codNotificacao;
	private String codProcesso;
	
	private String numNotificacao;
	private String seqNotificacao;
	private String tipoNotificacao;
	private String tipoReemissao;	
	private String datEmissao;
	private String codArquivo;		
	private boolean temARDigitalizado; 
	private REC.ARDigitalizadoBean[] ARDigitalizado;
	private String sitDigitaliza;
	private String datDigitalizacao;
	
	private String nomUserName;  
	private String codOrgaoLotacao;
	private String datProc;
	private String datGeracaoCB;
	private String datPublDO;
	
	private String datImpressao;
	private String seqImpressao;
	private String nomUserNameImpressao;  
	private String codOrgaoLotacaoImpressao;
	private String datProcImpressao;
	private String seqNotificacaoImpressao;
	
    private CodigoRetornoBean retorno;
    private String nomUserNameRetorno;  
	private String codOrgaoLotacaoRetorno;
	private String datProcRetorno;
	private String datRetorno;
	private String numLoteAR;
	
	
	private String msgErro ; 
	private String msgOk ;  
	private String eventoOK;

	
	public NotificacaoBean()  throws sys.BeanException {
				
		codNotificacao        = "";
		codProcesso           = "";
		
		numNotificacao        = "";
		seqNotificacao        = "000";
		tipoNotificacao       = "";
		tipoReemissao         = "";
		datEmissao            = "";				
		codArquivo            = "";
		temARDigitalizado = false; 
		sitDigitaliza         = "";
		datDigitalizacao      = "";

		nomUserName     = "";    
		codOrgaoLotacao = "";
		datProc         = "";
		datGeracaoCB    = "";		
		
		datPublDO    = "";
		
		datImpressao             = "";
		seqImpressao             = "";
		nomUserNameImpressao     = "";  
		codOrgaoLotacaoImpressao = "";
		datProcImpressao         = "";
		seqNotificacaoImpressao  = "";	
		
		retorno                  = new CodigoRetornoBean();
		nomUserNameRetorno       = "";  
		codOrgaoLotacaoRetorno   = "";
		datProcRetorno           = "";
		datRetorno               = "";
		numLoteAR				 = "";
		
		msgErro         = "" ;
		msgOk           = "" ;
		eventoOK        = "" ;

	}

	public void setCodNotificacao(String codNotificacao)  {
		if (codNotificacao==null) codNotificacao= "";
		this.codNotificacao=codNotificacao ;
	}  
	public String getCodNotificacao()  {
		return this.codNotificacao;
	}	

	public void setCodProcesso(String codProcesso)  {
		if (codProcesso==null) codProcesso= "";
		this.codProcesso=codProcesso ;
	}  
	public String getCodProcesso()  {
		return this.codProcesso;
	}	
	public void setNumNotificacao(String numNotificacao)  {
		if (numNotificacao==null) numNotificacao= "";
		this.numNotificacao=numNotificacao ;
	}  
	public String getNumNotificacao()  {
		return this.numNotificacao;
	}
	public void setSeqNotificacao(String seqNotificacao)  {
		if (seqNotificacao==null) seqNotificacao= "000";
		try {	 Integer.parseInt(seqNotificacao) ;		}
		catch (Exception e) {seqNotificacao= "000"; }
		this.seqNotificacao=seqNotificacao ;
	}  
	public String getSeqNotificacao()  {
		String sq = "000"+this.seqNotificacao;
		sq = sq.substring(sq.length()-3,sq.length());
		return sq;
	}
	public void setTipoNotificacao(String tipoNotificacao)  {
		if (tipoNotificacao==null) tipoNotificacao= "0";
		if ("0,1,2,3,4".indexOf(tipoNotificacao)<0) tipoNotificacao= "0";
		this.tipoNotificacao=tipoNotificacao ;
	}  
	public String getTipoNotificacao()  {
		return this.tipoNotificacao;
	}	
	public String getTipoNotificacaoDesc()  {
		String desc = "Notificação de Autuação";
		if (this.tipoNotificacao.equals("1")) desc = "Aviso de Defesa";
		if (this.tipoNotificacao.equals("2")) desc = "Notificação de Penalidade";
		if (this.tipoNotificacao.equals("3")) desc = "Aviso de Recurso";
		if (this.tipoNotificacao.equals("4")) desc = "Notificação de Apreensão";		
		return desc;
	}
	public String getTipoNotificacaoDescNotif()  {
		String desc = "Notificação de Instauração de Processo";
		if (this.tipoNotificacao.equals("1")) desc = "Aviso de Resultado de Defesa";
		if (this.tipoNotificacao.equals("2")) desc = "Notificação de Penalidade";
		if (this.tipoNotificacao.equals("3")) desc = "Aviso de Resultado de Recurso";
		if (this.tipoNotificacao.equals("4")) desc = "Notificação de Apreensão";		
		return desc;
	}

	public void setTipoReemissao(String tipoReemissao)  {
		if (tipoReemissao==null) tipoReemissao= "E";
		if (tipoReemissao.indexOf("ER")<0) tipoReemissao= "E";
		this.tipoReemissao=tipoReemissao ;
	}  
	public String getTipoReemissao()  {
		return this.tipoReemissao;
	}	
	public String getTipoReemissaoDesc()  {
		String desc = "Emissão";
		if (this.tipoReemissao.equals("E")) desc = "Emissão";
		if (this.tipoReemissao.equals("R")) desc = "Reemissão";
		return desc;
	}
	public void setDatEmissao(String datEmissao)  {
		if (datEmissao==null) datEmissao= "";
		if (datEmissao.equals("00000000")) datEmissao= "";
		this.datEmissao=datEmissao ;  
	}  
	public String getDatEmissao()  {
		return this.datEmissao;
	}		
	public void setCodArquivo(String codArquivo)  {
		if (codArquivo==null) codArquivo= "";
		this.codArquivo=codArquivo ;
	}  
	public String getCodArquivo()  {
		return this.codArquivo;
	}	
	public void setDatImpressao(String datImpressao)  {
		if (datImpressao==null) datImpressao= "";
		if (datImpressao.equals("00000000")) datImpressao= "";
		this.datImpressao=datImpressao ;  
	}  
	public String getDatImpressao()  {
		return this.datImpressao;
	}		

	public void setSeqImpressao(String seqImpressao)  {
		if (seqImpressao==null) seqImpressao= "000000";
		try {	 Integer.parseInt(seqImpressao) ;		}
		catch (Exception e) {seqImpressao= "000000"; }
		this.seqImpressao=seqImpressao ;
	}  
	public String getSeqImpressao()  {
		String sq = "000000"+this.seqImpressao;
		sq = sq.substring(sq.length()-6,sq.length());
		return sq;
	}	            
	public void setNomUserNameImpressao(String nomUserNameImpressao)  {
		if (nomUserNameImpressao==null) nomUserNameImpressao= "";
		this.nomUserNameImpressao=nomUserNameImpressao ;
	}  
	public String getNomUserNameImpressao()  {
		return this.nomUserNameImpressao;
	}
	public void setCodOrgaoLotacaoImpressao(String codOrgaoLotacaoImpressao)  {
		if (codOrgaoLotacaoImpressao==null) codOrgaoLotacaoImpressao= "";
		this.codOrgaoLotacaoImpressao=codOrgaoLotacaoImpressao ;
	}  
	public String getCodOrgaoLotacaoImpressao()  {
		return this.codOrgaoLotacaoImpressao;
	}
	public String getDatProcImpressao()  {
		return this.datProcImpressao;
	}
	public void setDatProcImpressao(String datProcImpressao)  {
		if (datProcImpressao==null) datProcImpressao= "";
		if (datProcImpressao.equals("00000000")) datProcImpressao= "";
		this.datProcImpressao=datProcImpressao ;  
	}  
	
	public void setSeqNotificacaoImpressao(String seqNotificacaoImpressao)  {
		if (seqNotificacaoImpressao==null) seqNotificacaoImpressao= "000000";
		try {	 Integer.parseInt(seqNotificacaoImpressao) ;		}
		catch (Exception e) {seqNotificacaoImpressao= "000000"; }
		this.seqNotificacaoImpressao=seqNotificacaoImpressao ;
	}  
	public String getSeqNotificacaoImpressao()  {
		String sq = "000000"+this.seqNotificacaoImpressao;
		sq = sq.substring(sq.length()-6,sq.length());
		return sq;
	}
	

	//--------------------------------------------------------------------------
	public void LerNotificacao(String tpChave) throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeNotificacao(this,tpChave);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	public void AtualizarNotificacao()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.AtualizaNotificacao(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
	public void GravarNotificacao(Connection conn) throws sys.BeanException {
		try	{
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.GravaNotificacao(this,conn);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public void atualizaDigitalizacao()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.atualizaDigitalizacao(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
	
	public void atualizaDigitalizacaoPorNotificacao()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.atualizaDigitalizacaoPorNotificacao(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
	
	public void ExcluirNotificacao(Connection conn) throws sys.BeanException {
		try	{
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.ExcluirNotificacao(this,conn);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}	
	//--------------------------------------------------------------------------
	public boolean getTemARDigitalizado()  throws sys.BeanException {
		return temARDigitalizado;
	}
	public void setTemARDigitalizado(boolean temARDigitalizado)  throws sys.BeanException {
		this.temARDigitalizado = temARDigitalizado ;
		return ;
	}	

	public REC.ARDigitalizadoBean[] getARDigitalizado() {
		return ARDigitalizado;
	}
	public REC.ARDigitalizadoBean getARDigitalizado(int i)  { 
		return (REC.ARDigitalizadoBean)this.ARDigitalizado[i];
	}
	public void setARDigitalizado(REC.ARDigitalizadoBean[] beans) {
		ARDigitalizado = beans;
	}  	
			
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j) ;
		}
	}
	public void setNomUserName(String nomUserName)  {
		if (nomUserName==null) nomUserName= "";
		this.nomUserName=nomUserName ;
	}  
	public String getNomUserName()  {
		return this.nomUserName;
	}
	public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
		if (codOrgaoLotacao==null) codOrgaoLotacao= "";
		this.codOrgaoLotacao=codOrgaoLotacao ;
	}  
	public String getCodOrgaoLotacao()  {
		return this.codOrgaoLotacao;
	}
	public String getDatProc()  {
		return this.datProc;
	}
	public void setDatProc(String datProc)  {
		if (datProc==null) datProc= "";
		if (datProc.equals("00000000")) datProc= "";
		this.datProc=datProc ;  
	}  
	public void setMsgErro(String msgErro)   {
		if (msgErro==null) msgErro="";
		this.msgErro = msgErro ;
	}   
	public String getMsgErro()   {
		return this.msgErro;
	}     
	public void setMsgOk(String msgOk)   {
		if (msgOk==null) msgOk="";   	
		this.msgOk = msgOk ;
	}   
	public String getMsgOk()   {
		return this.msgOk;
	}       
	public void setEventoOK(String eventoOK)  {
		if (eventoOK==null) eventoOK= "";
		this.eventoOK=eventoOK ;
	}  
	public String getEventoOK()  {
		return this.eventoOK;
	}  

	public String getDatGeracaoCB() {
		return datGeracaoCB;
	}

	public void setDatGeracaoCB(String datGeracaoCB) {
		if (datGeracaoCB==null) datGeracaoCB= "";
		if (datGeracaoCB.equals("00000000")) datGeracaoCB= "";
		this.datGeracaoCB = datGeracaoCB;
	}
	  public String getArquivoCB() {
		  String pathArquivo;
		  try {	
			  String numNot = "000000000"+getNumNotificacao();
			  numNot = numNot.substring(numNot.length()-9,numNot.length());
			  pathArquivo = getFormaData(getDatGeracaoCB())+"/CodigoBarra/"+
			    numNot+getSeqNotificacao()+".jpg";
		  } catch (Exception e) {
			  pathArquivo = "";
		  }
		  return pathArquivo;
	  }    
	  public String getFormaData(String sData) {
		  if (sData.length()<10) sData="01/01/2007";
		  String  dataImagem =sData.substring(6,10)+sData.substring(3,5)+"/"+
		  sData.substring(0,2); 
		  return dataImagem;	
	  }
		public String getDatPublDO() {
			return datPublDO;
		}
		public void setDatPublDO(String datPublDO) {
			if (datPublDO==null) datPublDO= "";
			if (datPublDO.equals("00000000")) datPublDO= "";
			this.datPublDO = datPublDO;
		}

		public String getCodOrgaoLotacaoRetorno() {
			return codOrgaoLotacaoRetorno;
		}
		public void setCodOrgaoLotacaoRetorno(String codOrgaoLotacaoRetorno) {
			if (codOrgaoLotacaoRetorno==null) codOrgaoLotacaoRetorno="";
			this.codOrgaoLotacaoRetorno = codOrgaoLotacaoRetorno;
		}

		public String getDatProcRetorno() {
			return datProcRetorno;
		}
		public void setDatProcRetorno(String datProcRetorno) {
			if (datProcRetorno==null) datProcRetorno="";
			this.datProcRetorno = datProcRetorno;
		}

		public String getNomUserNameRetorno() {
			return nomUserNameRetorno;
		}
		public void setNomUserNameRetorno(String nomUserNameRetorno) {
			if (nomUserNameRetorno==null) nomUserNameRetorno="";
			this.nomUserNameRetorno = nomUserNameRetorno;
		}

	    public void setRetorno(CodigoRetornoBean retorno)   throws sys.BeanException  {
	        this.retorno=retorno ;
	        if (retorno==null) this.retorno= new CodigoRetornoBean() ;
	    }  
	    public CodigoRetornoBean getRetorno()  {
	        return this.retorno;
	    }
	    
		public String getDatRetorno() {
			return datRetorno;
		}
		public void setDatRetorno(String datRetorno) {
			if (datRetorno==null) datRetorno="";
			this.datRetorno = datRetorno;
		}

		public String getDatDigitalizacao() {
			return datDigitalizacao;
		}

		public void setDatDigitalizacao(String datDigitalizacao) {
			if (datDigitalizacao==null) datDigitalizacao="";
			this.datDigitalizacao = datDigitalizacao;
		}

		public String getSitDigitaliza() {
			return sitDigitaliza;
		}

		public void setSitDigitaliza(String sitDigitaliza) {
			if (sitDigitaliza==null) sitDigitaliza="";
			this.sitDigitaliza = sitDigitaliza;
		}
		public void setNumLoteAR(String numLoteAR)  {
			if (numLoteAR==null) numLoteAR= "";
			this.numLoteAR=numLoteAR ;
		}  
		public String getNumLoteAR()  {
			return this.numLoteAR;
		}	
	  
}