package PNT;

import java.sql.Connection;
import java.sql.Statement;
import java.util.List;
import java.util.Vector;
import REC.ParamOrgBean;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import sys.DaoException;

public class DaoBrokerOra extends sys.DaoBase implements DaoBroker{

	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "GER";
	public DaoBrokerOra() throws DaoException {
		super();
		try {	
		  serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
		  throw new DaoException(e.getMessage());
		}
	}
	public void buscarListaProcessos(ProcessoBean myProcesso, String ordem) throws DaoException {
	}
	public void LeProcesso(ProcessoBean myProcesso) throws DaoException {
	}
	public void LeProcesso(ProcessoBean myProcesso,String tpChave) throws DaoException {
	}
	public void LeProcessoPenalidade(ProcessoBean myProcesso, String numProcessosAplicarPenalidade) throws DaoException {
	}
	public void GravaProcesso(ProcessoBean myProcesso,Connection conn) throws DaoException {
	}
	public void LeProcessoAnts(ProcessoBean myProcesso) throws DaoException {
	}	
	public void GravaProcessoAnt(ProcessoAntBean myProcesso,Connection conn) throws DaoException {
	}	
	public void LeNotificacoes(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeNotificacao : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void GravaNotificacao(NotificacaoBean myNotificacao,Connection conn) throws DaoException {
		try {
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-GravaNotificacao : "+ex.getMessage());
		}
	}
	public void ExcluirNotificacao(NotificacaoBean myNotificacao,Connection conn) throws DaoException {
		try {
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-GravaNotificacao : "+ex.getMessage());
		}
	}
	public void LeNotificacao(NotificacaoBean myNotif,String tpChave) throws DaoException {
		
	}
	public void AtualizaNotificacao(NotificacaoBean myNotif) throws DaoException {
		
	}
	public void LeMultas(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeMulta : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void GravaMulta(MultasBean myMulta,Connection conn) throws DaoException {
		try {
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-GravaNotificacao : "+ex.getMessage());
		}
	}
	public void LeRequerimentos(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;					
		try {
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeRequerimento : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void GravaRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException {
		
	}
	public void AtualizarRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException {
		
	}
	public void AtualizarRequerimentoAta(RequerimentoBean myReq,Connection conn) throws DaoException {
		
	}
	public void AtualizarRequerimentoAtaPeloNumero(RequerimentoBean myReq,Connection conn) throws DaoException {
		
	}
	public void GravaHistorico(HistoricoBean myHistorico,Connection conn) throws DaoException {
		
	}
	public void LeHistoricos(ProcessoBean myProcesso) throws DaoException {
	
	}
	public void GravaAtaDO(AtaDOBean myHist,Connection conn) throws DaoException {
	}
	public void LeAtasDO(ProcessoBean myProcesso) throws DaoException {
	}
	public void LeAtasDO(AtaDOBean myAta) throws DaoException {
	}
    public void AtualizaEditalDO(AtaDOBean myAta) throws sys.BeanException {
    }
    public void AtualizaDataPublicacaoDO(AtaDOBean myAta) throws sys.BeanException {
    }	
	public void atualizaDigitalizacao(NotificacaoBean myNotif) throws DaoException {	
	}
	public String GravaARDigitalizado(ARDigitalizadoBean myAR) throws DaoException {
		return "";
	}
	public ARDigitalizadoBean ConsultaARDigitalizado(String[] campos, String[] valores)
	throws DaoException {
		ARDigitalizadoBean myAR = new ARDigitalizadoBean();
       return myAR;		
	}
	public void GravaLogARDigitalizado(LogARDigitalizadoBean myLog) throws DaoException {
	}
	public void LeARDigitalizado(ProcessoBean myProcesso) throws DaoException {
	}
	public void consultaStatusProc(RelatProcStatusBean relProcStatus) throws DaoException {
	}
	public boolean GravaARDigitalizadoTmp(ARDigitalizadoBean myAR) throws DaoException {
		   return true;
	}	
	public String ConsultaCodRet(String codRet) throws DaoException {
		return "";
	}
	public void CarregaLista(PNT.PaginacaoBean auxClasse, String acao)
	throws DaoException {
	}
	public String BuscaTotalReg(PNT.PaginacaoBean auxClasse, String acao)
	throws DaoException {
		return "";
	}
    public Vector ConsultaAtaDownload(AtaDOBean AtaDOBeanId, String dataIni, String dataFim) throws DaoException {
		return null;
	}
	public void DownloadArquivoAta(AtaDOBean AtaDOBeanId) throws DaoException {
	}
	public boolean gravaArquivo(AtaDOBean myAta,ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId,UsuarioBean UsrLogado) throws DaoException{
		return true;
	}	
	public boolean gravaArquivo(AtaDOBean AtaDOId, String quantProc,UsuarioBean usrlogado) throws DaoException{
		return true;
	}
	public void buscarListaProcessos(ProcessoBean myProcesso, String ordem, UsuarioBean myUsrLog) throws DaoException {
		// TODO Auto-generated method stub
	}
	public String buscaNomRelator(String cpf, String orgaoLotacao) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	public List<AtaDOBean> ConsultaAtasDO(AtaDOBean myAtaDO) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	public String Transacao623(String codOrgao, String codJunta, String numCPF, String codAcao, String indContinua, String nomRelator) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	public String Transacao622(String codOrgao, String codJunta, String cpf, String tipAcao, String indContinua, String nome) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	public String Transacao622(String codOrgao, String codJunta, String codAcao, String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd, String numEnd, String txtCompl, String nomBairro, String numCep, String codMun, String numDddTel, String numTel, String numFax, Connection conn) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	public String Transacao622(String codOrgao, String codJunta, String codAcao, String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd, String numEnd, String txtCompl, String nomBairro, String numCep, String codMun, String numDddTel, String numTel, String numFax) throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public String Transacao624(String codPenalidade, String codAcao, String indContinua,
			String descPenalidade,String enquadramento,String indReincidencia,
			String numPrazo,String qtdInfracaoDe,String qtdInfracaoAte,String codigoFator,String descResumida) throws sys.DaoException {
		return null;
	}
	public String Transacao625(String codInfracao, String codAcao, String indContinua,
			String codAgravo) throws sys.DaoException {
		return null;
	}
    public String Transacao626(String codStatus, String codAcao, String indContinua,	
            String nomStatus) throws sys.DaoException {
    	return null;
    }
    public String Transacao627(String codEvento, String codAcao, String indContinua,	
            String nomEvento) throws sys.DaoException {
    	return null;
    }
    public String Transacao629(String codMotivo, String codAcao, String indContinua,	
    		String nomMotivo) throws sys.DaoException {
    	return null;
    }
    public String Transacao630(String codOrgao, String codAcao, String indContinua, 
    		String prazoDef, String prazoEmissao, String prazoEntrega, String prazoPublDO,
    		String prazoRecursoPenal, String prazoRecurso2Inst, String prazoProsperaResult,
    		String prazoEntregaCNH, String folgaDef, String folgaRecursoPenal, String folgaRecurso2Inst,
    		String nomAssDef, String nomAssRecurso, String nomAssEntrega, String secretariaProcesso) throws sys.DaoException {
    	return null;
    }
	public void GravaProcessoOracle(ProcessoBean myProcesso)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
	public void GravaRequerimentoGuia(RequerimentoBean myReq, Connection conn)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
	public void atualizaDigitalizacaoPorNotificacao(NotificacaoBean myNotif)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
}