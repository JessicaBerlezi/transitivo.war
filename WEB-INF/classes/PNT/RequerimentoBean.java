package PNT;
import java.sql.Connection;
import java.util.Vector;

/**
 * <b>Title:</b> SMIT - Entrada de Recurso - Requerimento Bean<br>
 * <b>Description:</b> Requerimento Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class RequerimentoBean {

    private String msgErro;

    private String codRequerimento;

    private String codProcesso;

    private String codTipoSolic;
    private String numRequerimento;
    private String codStatusRequerimento;
    private String datRequerimento;

    private String nomUserNameDP;
    private String codOrgaoLotacaoDP;
    private String datProcDP;

    private String datJU;
    private String codJuntaJU;
    private String sigJuntaJU;
    private String tipoJunta;
    private String codRelatorJU;
    private String nomRelatorJU;
    private String cpfRelatorJU;
    
    private String nomUserNameJU;
    private String codOrgaoLotacaoJU;
    private String datProcJU;

    private String datRS;
    private String codResultRS; // D-deferido ou I- indeferido
    private String txtMotivoRS;
  
    private String nomUserNameRS;
    private String codOrgaoLotacaoRS;
    private String datProcRS;

    private String codPenalidade;
    private String dscPenalidade;

    private String datEST;
    private String txtMotivoEST;
    private String nomUserNameEST;
    private String codOrgaoLotacaoEST;
    private String datProcEST;

    private String txtMotivo;
    
    
    private String codEnvioEdo;
    private String datEnvioEdo;
    
    private String datPubliPDO;

    
    public RequerimentoBean() throws sys.BeanException {

        msgErro = "";

        codRequerimento = "";
        codProcesso = "";

        codTipoSolic = "";
        numRequerimento = "";
        datRequerimento = sys.Util.formatedToday().substring(0,10);
        codStatusRequerimento = "";
        nomUserNameDP = "";
        codOrgaoLotacaoDP = "";
        datProcDP = "";

        datJU = "";
        codJuntaJU = "";
        tipoJunta    	= "";

        sigJuntaJU = "";
        codRelatorJU = "";
        nomRelatorJU = "";
        cpfRelatorJU = "";
        nomUserNameJU = "";
        codOrgaoLotacaoJU = "";
        datProcJU = "";

        datRS = "";
        codResultRS = ""; // D-deferido ou I- indeferido
        txtMotivoRS = "";
        nomUserNameRS  = "";
        codOrgaoLotacaoRS = "";
        datProcRS = "";

        codPenalidade	= "";
        dscPenalidade	= "";


        datEST = "";
        txtMotivoEST = "";
        nomUserNameEST = "";
        codOrgaoLotacaoEST = "";
        datProcEST = "";
        
        txtMotivo       	= "";
        
        codEnvioEdo = "";
        datEnvioEdo = "";
        
        datPubliPDO = "";
                
    }

    public void setMsgErro(Vector vErro) {
        for (int j = 0; j < vErro.size(); j++) {
            this.msgErro += vErro.elementAt(j);
        }
    }
    public void setMsgErro(String msgErro) {
        if (msgErro == null)  msgErro = "";
    	this.msgErro = msgErro;
    }
    public String getMsgErro() {
        return this.msgErro;
    }

    public void setCodRequerimento(String codRequerimento) {
        if (codRequerimento == null)  codRequerimento = "";
    	this.codRequerimento = codRequerimento;
    }
    public String getCodRequerimento() {
        return this.codRequerimento;
    }

    public void setCodProcesso(String codProcesso) {
        if (codProcesso == null) codProcesso = "";
    	this.codProcesso = codProcesso;
    }
    public String getCodProcesso() {
        return this.codProcesso;
    }

    // --------------------------------------------------------------------------
    public void setCodTipoSolic(String codTipoSolic) {
        if (codTipoSolic == null) codTipoSolic = "";
    	this.codTipoSolic = codTipoSolic;
    }
    public String getCodTipoSolic() {
        return this.codTipoSolic;
    }
    public String getNomTipoSolic() {
        String ns = "";
        if ("DP".equals(this.codTipoSolic))
            ns = "DEFESA PR�VIA";
        if ("RP".equals(this.codTipoSolic))
            ns = "RECURSO 1a INST�NCIA";
        if ("AP".equals(this.codTipoSolic))
            ns = "APLICA��O DA PENALIDADE";
        if ("2P".equals(this.codTipoSolic))
            ns = "RECURSO 2a INST�NCIA";
        if ("AE".equals(this.codTipoSolic))
            ns = "ANTECIPA��O DE ENTREGA DE CNH";
        if ("EC".equals(this.codTipoSolic))
            ns = "ENTREGA DE CNH";

        return ns;
    }
    public void setNumRequerimento(String numRequerimento) {
        if (numRequerimento == null) numRequerimento = "";
    	this.numRequerimento = numRequerimento;
    }
    public String getNumRequerimento() {
        return this.numRequerimento;
    }

    public void setDatRequerimento(String datRequerimento) {
        if (datRequerimento == null) datRequerimento = "";
    	this.datRequerimento = datRequerimento;
    }
    public String getDatRequerimento() {
        return this.datRequerimento;
    }

    public void setCodStatusRequerimento(String codStatusRequerimento) {
        if (codStatusRequerimento == null) codStatusRequerimento = "";
    	this.codStatusRequerimento = codStatusRequerimento;
    }
    public String getCodStatusRequerimento() {
        return this.codStatusRequerimento;
    }

    public String getNomStatusRequerimento() {
        return getNomStatusRequerimento(this.codStatusRequerimento);
    }
    public String getNomStatusRequerimento(String codSta) {
        String ns = "";
        if ("0".equals(codSta))
            ns = "ABERTO ";
        if ("2".equals(codSta))
            ns = "JUNTA/RELATOR DEFINIDOS";
        if ("3".equals(codSta))
            ns = "RESULTADO INFORMADO";
        if ("4".equals(codSta))
            ns = "ENVIADO P/PUBLICA��O";
        if ("7".equals(codSta))
            ns = "CANCELADO";
        if ("8".equals(codSta))
            ns = "ESTORNADO";
        if ("9".equals(codSta))
            ns = "CONCLUIDO";
        return ns;
    }

    // --------------------------------------------------------------------------
    public void setNomUserNameDP(String nomUserNameDP) {
        if (nomUserNameDP == null) nomUserNameDP = "";
    	this.nomUserNameDP = nomUserNameDP;
    }
    public String getNomUserNameDP() {
        return this.nomUserNameDP;
    }
    public void setCodOrgaoLotacaoDP(String codOrgaoLotacaoDP) {
    	if (codOrgaoLotacaoDP == null) codOrgaoLotacaoDP = "";
    	this.codOrgaoLotacaoDP = codOrgaoLotacaoDP;
    }
    public String getCodOrgaoLotacaoDP() {
        return this.codOrgaoLotacaoDP;
    }
    public void setDatProcDP(String datProcDP) {
    	if (datProcDP == null) datProcDP = "";
    	this.datProcDP = datProcDP;
    }
    public String getDatProcDP() {
        return datProcDP;
    }
    // --------------------------------------------------------------------------
    public void setDatJU(String datJU) {
    	if (datJU == null) datJU = "";
    	this.datJU = datJU;
    }
    public String getDatJU() {
        return this.datJU;
    }
    public void setCodJuntaJU(String codJuntaJU) {
   	   if (codJuntaJU == null) codJuntaJU = "";
    	this.codJuntaJU = codJuntaJU;
    }
    public String getCodJuntaJU() {
        return this.codJuntaJU;
    }
    public void setSigJuntaJU(String sigJuntaJU) {
        if (sigJuntaJU == null)  sigJuntaJU = "";
    	this.sigJuntaJU = sigJuntaJU;
    }
    public String getSigJuntaJU() {
        return this.sigJuntaJU;
    }
    public void setCodRelatorJU(String codRelatorJU) {
        if (codRelatorJU == null) codRelatorJU = "";
    	this.codRelatorJU = codRelatorJU;
    }
    public String getCodRelatorJU() {
        return this.codRelatorJU;
    }
    public void setNomRelatorJU(String nomRelatorJU) {
    	if (nomRelatorJU == null) nomRelatorJU = "";
    	this.nomRelatorJU = nomRelatorJU;
    }
    public String getNomRelatorJU() {
        return this.nomRelatorJU;
    }
    public void setCpfRelatorJU(String cpfRelatorJU) {
    	if (cpfRelatorJU == null) cpfRelatorJU = "";
    	this.cpfRelatorJU = cpfRelatorJU;
    }
    public String getCpfRelatorJU() {
        return this.cpfRelatorJU;
    }

    public void setNomUserNameJU(String nomUserNameJU) {
        if (nomUserNameJU == null) nomUserNameJU = "";
    	this.nomUserNameJU = nomUserNameJU;
    }
    public String getNomUserNameJU() {
        return this.nomUserNameJU;
    }
    public void setCodOrgaoLotacaoJU(String codOrgaoLotacaoJU) {
        if (codOrgaoLotacaoJU == null) codOrgaoLotacaoJU = "";
    	this.codOrgaoLotacaoJU = codOrgaoLotacaoJU;
    }
    public String getCodOrgaoLotacaoJU() {
        return this.codOrgaoLotacaoJU;
    }
    public void setDatProcJU(String datProcJU) {
    	if (datProcJU == null) datProcJU = "";
    	this.datProcJU = datProcJU;
    }
    public String getDatProcJU() {
        return this.datProcJU;
    }
    // --------------------------------------------------------------------------
    public void setDatRS(String datRS) {
        if (datRS == null) datRS = "";
    	this.datRS = datRS;
    }
    public String getDatRS() {
        return this.datRS;
    }
    public void setCodResultRS(String codResultRS) {
    	if (codResultRS == null) codResultRS = "";
    	this.codResultRS = codResultRS;
    }
    public String getCodResultRS() {
        return this.codResultRS;
    }
    public String getNomResultRS() {
        String n = "";
        if ("D".equals(this.codResultRS))
            n = "Deferido";
        if ("I".equals(this.codResultRS))
            n = "Indeferido";
        if ("E".equals(this.codResultRS))
            n = "Estornado";
        return n;
    }
    public void setTxtMotivoRS(String txtMotivoRS) {
        if (txtMotivoRS == null) txtMotivoRS = "";
        if (txtMotivoRS.length() == 0) txtMotivoRS = ".";
        this.txtMotivoRS = txtMotivoRS;
    }
    public String getTxtMotivoRS() {
        return this.txtMotivoRS;
    }
    public void setNomUserNameRS(String nomUserNameRS) {
        if (nomUserNameRS == null) nomUserNameRS = "";
    	this.nomUserNameRS = nomUserNameRS;
    }
    public String getNomUserNameRS() {
        return this.nomUserNameRS;
    }
    public void setCodOrgaoLotacaoRS(String codOrgaoLotacaoRS) {
        if (codOrgaoLotacaoRS == null) codOrgaoLotacaoRS = "";
    	this.codOrgaoLotacaoRS = codOrgaoLotacaoRS;
    }
    public String getCodOrgaoLotacaoRS() {
        return this.codOrgaoLotacaoRS;
    }
    public void setDatProcRS(String datProcRS) {
        if (datProcRS == null) datProcRS = "";
    	this.datProcRS = datProcRS;
    }
    public String getDatProcRS() {
        return this.datProcRS;
    }
    // --------------------------------------------------------------------------
    public void setDatEST(String datEST) {
        if (datEST == null)  datEST = "";
    	this.datEST = datEST;
    }
    public String getDatEST() {
        return this.datEST;
    }
    public void setTxtMotivoEST(String txtMotivoEST) {
        if (txtMotivoEST == null)  txtMotivoEST = "";
    	this.txtMotivoEST = txtMotivoEST;
    }
    public String getTxtMotivoEST() {
        return this.txtMotivoEST;
    }
    public void setNomUserNameEST(String nomUserNameEST) {
        if (nomUserNameEST == null) nomUserNameEST = "";
    	this.nomUserNameEST = nomUserNameEST;
    }
    public String getNomUserNameEST() {
        return this.nomUserNameEST;
    }
    public void setCodOrgaoLotacaoEST(String codOrgaoLotacaoEST) {
        if (codOrgaoLotacaoEST == null) codOrgaoLotacaoEST = "";
    	this.codOrgaoLotacaoEST = codOrgaoLotacaoEST;
    }
    public String getCodOrgaoLotacaoEST() {
        return this.codOrgaoLotacaoEST;
    }
    public void setDatProcEST(String datProcEST) {
        if (datProcEST == null) datProcEST = "";
    	this.datProcEST = datProcEST;
    }
    public String getDatProcEST() {
        return this.datProcEST;
    }    
    // --------------------------------------------------------------------------
    public void GravaRequerimento(Connection conn) throws sys.BeanException {
  	  try {
  		  DaoBroker dao = DaoBrokerFactory.getInstance();
  		  dao.GravaRequerimento(this,conn);		   
  	  }
  	  catch (Exception ex) {
  		  throw new sys.BeanException(ex.getMessage());
  	  }		
    }	

    public void AtualizarRequerimento(Connection conn) throws sys.BeanException {
    	  try {
    		  DaoBroker dao = DaoBrokerFactory.getInstance();
    		  dao.AtualizarRequerimento(this,conn);		   
    	  }
    	  catch (Exception ex) {
    		  throw new sys.BeanException(ex.getMessage());
    	  }		
      }

	public String getCodPenalidade() {
		return codPenalidade;
	}

	public void setCodPenalidade(String codPenalidade) {
		if (codPenalidade == null) codPenalidade = "";
		this.codPenalidade = codPenalidade;
	}

	public String getDscPenalidade() {
		return dscPenalidade;
	}

	public void setDscPenalidade(String dscPenalidade) {
		if (dscPenalidade == null) dscPenalidade = "";
		this.dscPenalidade = dscPenalidade;
	}

	public String getTxtMotivo() {
		return txtMotivo;
	}

	public void setTxtMotivo(String txtMotivo) {
		if (txtMotivo == null) txtMotivo = "";
		this.txtMotivo = txtMotivo;
	}

	public String getTipoJunta() {
		return tipoJunta;
	}

	public void setTipoJunta(String tipoJunta) {
		if (tipoJunta == null) tipoJunta = "";
		this.tipoJunta = tipoJunta;
	}
	
	public String getCodEnvioEdo() {
		return codEnvioEdo;
	}

	public void setCodEnvioEdo(String codEnvioEdo) {
		if (codEnvioEdo == null) codEnvioEdo = "";
		this.codEnvioEdo = codEnvioEdo;
	}

	public String getDatEnvioEdo() {
		return datEnvioEdo;
	}

	public void setDatEnvioEdo(String datEnvioEdo) {
		if (datEnvioEdo == null) datEnvioEdo = "";
		this.datEnvioEdo = datEnvioEdo;
	}

	public String getDatPubliPDO() {
		return datPubliPDO;
	}

	public void setDatPubliPDO(String datPubliPDO) {
		if (datPubliPDO == null) datPubliPDO = "";
		this.datPubliPDO = datPubliPDO;
	}	
	
	
   
}