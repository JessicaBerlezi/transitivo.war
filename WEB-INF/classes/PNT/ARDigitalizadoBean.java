package PNT;


/**
 * <b>Title: </b> SMIT - REC - AR Digitalizado Bean <br>
 * <b>Description: </b> Aviso de Recebimento Digitalizado Bean <br>
 * <b>Copyright: </b> Copyright (c) 2007 <br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra
 * @version 1.0
 */

public class ARDigitalizadoBean extends sys.HtmlPopupBean {
	
	private int    codARDigitalizado;
	private String numNotificacao;
	private String seqNotificacao;
	private String numProcesso;    
	private String numLote;
	private String numCaixa;
	private String datDigitalizacao;
	private String datEntrega;
	private String codRetorno;
	
	
	public ARDigitalizadoBean() {
		
		super();
		codARDigitalizado = 0      ;
		numNotificacao    = ""     ;
		seqNotificacao    = "000"  ;
		numProcesso       = ""     ;
		numLote           = ""     ;
		numCaixa          = ""     ;
		datDigitalizacao  = ""     ;
		datEntrega        = ""     ;
		codRetorno        = ""     ;
	}
	
	public ARDigitalizadoBean(String numNotificacao, String seqNotificacao, String numProcesso, String numLote, String numCaixa,
			String datDigitalizacao) {
		
		this();
		this.numNotificacao   = numNotificacao;
		this.seqNotificacao   = seqNotificacao;
		this.numProcesso      = numProcesso;
		this.numLote          = numLote;
		this.numCaixa         = numCaixa;
		this.datDigitalizacao = datDigitalizacao;
	}
	
	
	public ARDigitalizadoBean(String numNotificacao, String seqNotificacao, String numProcesso, String numLote, String numCaixa,
			String datDigitalizacao, String datEntrega, String codRetorno) {
		
		this();
		this.numNotificacao   = numNotificacao;
		this.seqNotificacao   = seqNotificacao;
		this.numProcesso      = numProcesso;
		this.numLote          = numLote;
		this.numCaixa         = numCaixa;
		this.datDigitalizacao = datDigitalizacao;
		this.datEntrega       = datEntrega;
		this.codRetorno       = codRetorno;
	}
	
	public String getNumNotificacao() {
		if (numNotificacao == null) return "";
		else return numNotificacao;
	}
	
	public void setNumNotificacao(String string) {
		numNotificacao = string;
	}
	
	public void setSeqNotificacao(String seqNotificacao)  {
		if (seqNotificacao==null) seqNotificacao= "000";
		try {	 Integer.parseInt(seqNotificacao) ;		}
		catch (Exception e) {seqNotificacao= "000"; }
		this.seqNotificacao=seqNotificacao ;
	}  
	public String getSeqNotificacao()  {
		String sq = "000"+this.seqNotificacao;
		sq = sq.substring(sq.length()-3,sq.length());
		return sq;
	}    
	public String getNumProcesso() {
		return numProcesso;
	}
	public void setNumProcesso(String string) {
		numProcesso = string;
	}
	
	public String getNumCaixa() {
		return numCaixa;
	}
	
	public void setNumCaixa(String string) {
		numCaixa = string;
	}
	
	public String getNumLote() {
		return numLote;
	}
	
	public void setNumLote(String string) {
		numLote = string;
	}
	
	public int getCodARDigitalizado() {
		return codARDigitalizado;
	}
	
	public void setCodARDigitalizado(int i) {
		codARDigitalizado = i;
	}
	
	public String getDatDigitalizacao() {
		return datDigitalizacao;
	}
	
	public void setDatDigitalizacao(String datDigitalizacao) {
		this.datDigitalizacao = datDigitalizacao;
	}   
	
	public String getDatEntrega() {
		return datEntrega;
	}
	
	public void setDatEntrega(String datEntrega) {
		this.datEntrega = datEntrega;
	}   
	
	
	public String getCodRetorno() {
		return codRetorno;
	}
	
	public void setCodRetorno(String codRetorno) {
		this.codRetorno = codRetorno;
	}   
	
	public String getNumCaixaFrt() {
		boolean achou = false;
		char ch ;
		String numCaixaFrt = "";
		
		for (int h=0; h<this.numCaixa.length(); h++) {
			ch = this.numCaixa.charAt(h);
			
			if (achou == true )
				numCaixaFrt = numCaixaFrt + ch;
			if (ch == '-'){
				achou = true ;
			}
		}
		return numCaixaFrt;
	}
	
	public String getParametro() {        
		String parametro = "DIR_AR_DIGITALIZADO";
		/*Novo Path*/
		parametro+="_"+getDatDigitalizacao().substring(6,10);
		return parametro;
	} 
	public String getImagem() {        
		String pathImagem = "";
		
		pathImagem =  getDatDigitalizacao().substring(6,10)+ 
		getDatDigitalizacao().substring(3,5) +
		"/" + getDatDigitalizacao().substring(0,2); 
		return pathImagem;
	}   
	
	public String getNomFoto() {
		return getImagem() + "/AR/"+this.numNotificacao + "-" + this.seqNotificacao + ".gif";
	}
	
	
	public String getArquivo(ACSS.ParamSistemaBean param) {
		String pathArquivo;
		try {
			pathArquivo = param.getParamSist(this.getParametro())+ "/" +this.getImagem();
			pathArquivo += "/AR/" + this.numNotificacao + "-" + this.seqNotificacao+".gif";
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}    
	
	
	public String GravaARDigitalizado()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			return dao.GravaARDigitalizado(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public static ARDigitalizadoBean consultaAR(String[] campos, String[] valores) 
	throws sys.BeanException {
		
		ARDigitalizadoBean AR = null;
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();        	
			AR = dao.ConsultaARDigitalizado(campos, valores);
			if (AR == null) {
				AR = new ARDigitalizadoBean();
				AR.setMsgErro("AVISO DE RECEBIMENTO DIGITALIZADO N�O ENCONTRADO !");
			}
		} 
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return AR;		
	}
	
	public boolean GravaARDigitalizadoTmp()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			return dao.GravaARDigitalizadoTmp(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	public String ConsultaCodRet(String codRet)  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			return dao.ConsultaCodRet(codRet);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
}