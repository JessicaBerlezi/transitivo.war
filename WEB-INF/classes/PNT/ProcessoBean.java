package PNT;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.DaoException;
import sys.ServiceLocatorException;
import PNT.NOT.Dao;
import PNT.NOT.DaoFactory;

/**
 * <b>Title:</b> SMIT - Pontuação <br>
 * <b>Description:</b> Processo Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class ProcessoBean {

	private String codProcesso;
	private String numProcesso;
	private String datProcesso;
	private String tipProcesso;
	private String sitProcesso;
	private String formatProcesso;
	private String codArquivo;
	private String codArtigo;

	private String codOrgao;
	private String sigOrgao;
	private String datGeracao;
	private String totPontos;
	private String qtdProcAnteriores;
	private String qtdMultas;
	private String totNotificacoes;
	private String numNotificacao;

	private String nomResponsavel;
	private String numCPF;
	private String tipCNH;
	private String numCNH;
	private String ufCNH;
	private String datEmissaoCNH;
	private String datValidadeCNH;
	private String categoriaCNH;
	private String endereco;
	private String cep;
	private String codMunicipio;
	private String municipio;
	private String uf;

	private String ultDatDefesa;
	private String ultDatRecurso;
	private String ultDatEntregaCNH;
	private String datApresentaDefesa;
	private String datApresentaRecurso;
	private String codPenalidade;
	private String dscPenalidade;
	private String dscPenalidadeRes;
	private String numProtocoloEntregaCNH;
	private String dtEntregaCNH;
	private String dtDevolucaoCNH;

	private String codStatus;
	private String datStatus;
	private String nomStatus;

	private String datProcessoFis;
	private String nomUserNameFis;
	private String prazoSupensao;
	private String codOrgaoLotacaoFis;
	private String datProcFis;
	private String txtEMail;

	private List<ProcessoAntBean> procAnteriores;
	private List<MultasBean> multas;
	private List<NotificacaoBean> notificacoes;
	private List<RequerimentoBean> requerimentos;
	private String reqs;
	private List<HistoricoBean> historicos;
	private List<AtaDOBean> atasDO;
	private List<ARDigitalizadoBean> arDigitalizado;
	private String codArDigitalizado;

	private String nomUserName;
	private String codOrgaoLotacao;
	private String datProc;

	private String msgErro;
	private String msgOk;
	private String eventoOK;

	private String numRequerimento;

	private List<ProcessoBean> processos;

	private String chaveSort;
	//Propriedade para a revisão de ato
	private String proximoStatus;

	public ProcessoBean() throws sys.BeanException {

		codProcesso = "";
		numProcesso = "";
		datProcesso = "";
		tipProcesso = "P";
		sitProcesso = "";
		formatProcesso = "";
		codArquivo = "";
		codArtigo = "";
		codOrgao = "";
		sigOrgao = "";
		datGeracao = "";
		totPontos = "0";
		qtdProcAnteriores = "0";
		qtdMultas = "0";
		totNotificacoes = "0";
		numNotificacao = "";

		nomResponsavel = "";
		numCPF = "";
		tipCNH = "";
		numCNH = "";
		ufCNH = "";
		datEmissaoCNH = "";
		datValidadeCNH = "";
		categoriaCNH = "";
		endereco = "";
		cep = "";
		codMunicipio = "";
		municipio = "";
		uf = "";

		ultDatDefesa = "";
		ultDatRecurso = "";
		ultDatEntregaCNH = "";
		datApresentaDefesa = "";
		datApresentaRecurso = "";
		codPenalidade = "";
		dscPenalidade = "";
		dscPenalidadeRes = "";
		numProtocoloEntregaCNH = "";
		dtEntregaCNH = "";
		dtDevolucaoCNH = "";

		codStatus = "";
		datStatus = "";
		nomStatus = "";

		datProcessoFis = "";
		nomUserNameFis = "";
		prazoSupensao = "";
		codOrgaoLotacaoFis = "";
		datProcFis = "";
		txtEMail = "";

		procAnteriores = new ArrayList<ProcessoAntBean>();
		multas = new ArrayList<MultasBean>();
		notificacoes = new ArrayList<NotificacaoBean>();
		requerimentos = new ArrayList<RequerimentoBean>();
		reqs = "";
		historicos = new ArrayList<HistoricoBean>();
		atasDO = new ArrayList<AtaDOBean>();
		arDigitalizado = new ArrayList<ARDigitalizadoBean>();
		codArDigitalizado = "";

		nomUserName = "";
		codOrgaoLotacao = "";
		datProc = "";
		msgErro = "";
		msgOk = "";
		eventoOK = "";

		processos = new ArrayList<ProcessoBean>();

		chaveSort = "";

		numRequerimento = "";

	}

	public void setCodProcesso(String codProcesso) {
		if (codProcesso == null)
			codProcesso = "";
		this.codProcesso = codProcesso;
	}

	public String getCodProcesso() {
		return this.codProcesso;
	}

	public void setNumProcesso(String numProcesso) {
		if (numProcesso == null)
			numProcesso = "";
		this.numProcesso = numProcesso;
	}

	public String getNumProcesso() {
		return this.numProcesso;
	}

	public void setDatProcesso(String datProcesso) {
		if (datProcesso == null)
			datProcesso = "";
		if (datProcesso.equals("00000000"))
			datProcesso = "";
		if (datProcesso.equals("        "))
			datProcesso = "";
		this.datProcesso = datProcesso;
	}

	public String getDatProcesso() {
		return this.datProcesso;
	}

	public void setTipProcesso(String tipProcesso) {
		if (tipProcesso == null)
			tipProcesso = "P";
		this.tipProcesso = tipProcesso;
	}

	public String getTipProcesso() {
		return this.tipProcesso;
	}

	public String getTipProcessoDesc() {
		String desc = "Pontuação";
		if (this.tipProcesso.equals("S"))
			desc = "Suspensão";
		return desc;
	}

	public void setSitProcesso(String sitProcesso) {
		if (sitProcesso == null)
			sitProcesso = "";
		this.sitProcesso = sitProcesso;
	}

	public String getSitProcesso() {
		return this.sitProcesso;
	}

	public void setFormatProcesso(String formatProcesso) {
		if (formatProcesso == null)
			formatProcesso = "";
		this.formatProcesso = formatProcesso;
	}

	public String getFormatProcesso() {
		return this.formatProcesso;
	}

	public void setCodOrgao(String codOrgao) {
		if (codOrgao == null)
			codOrgao = "";
		this.codOrgao = codOrgao;
	}

	public String getCodOrgao() {
		return this.codOrgao;
	}

	public String getSigOrgao() {
		return sigOrgao;
	}

	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao == null)
			sigOrgao = "";
		this.sigOrgao = sigOrgao;
	}

	public void setDatGeracao(String datGeracao) {
		if (datGeracao == null)
			datGeracao = "";
		if (datGeracao.equals("00000000"))
			datGeracao = "";
		if (datGeracao.equals("        "))
			datGeracao = "";
		this.datGeracao = datGeracao;
	}

	public String getDatGeracao() {
		return this.datGeracao;
	}

	public void setTotPontos(String totPontos) {
		int qtd = 0;
		if (totPontos == null)
			totPontos = "0";
		try {
			qtd = Integer.parseInt(totPontos);
		} catch (Exception e) {
			totPontos = "0";
		}
		this.totPontos = String.valueOf(qtd);
	}

	public String getTotPontos() {
		return this.totPontos;
	}

	public void setQtdProcAnteriores(String qtdProcAnteriores) {
		int qtd = 0;
		if (qtdProcAnteriores == null)
			qtdProcAnteriores = "0";
		try {
			qtd = Integer.parseInt(qtdProcAnteriores);
		} catch (Exception e) {
			qtdProcAnteriores = "0";
		}
		this.qtdProcAnteriores = String.valueOf(qtd);
	}

	public String getQtdProcAnteriores() {
		return this.qtdProcAnteriores;
	}

	public String getNumRequerimento() {
		return numRequerimento;
	}

	public void setNumRequerimento(String numRequerimento) {
		if (numRequerimento == null)
			numRequerimento = "";
		this.numRequerimento = numRequerimento;
	}

	public void setQtdMultas(String qtdMultas) {
		int qtd = 0;
		if (qtdMultas == null)
			qtdMultas = "0";
		try {
			qtd = Integer.parseInt(qtdMultas);
		} catch (Exception e) {
			qtdMultas = "0";
		}
		this.qtdMultas = String.valueOf(qtd);
	}

	public String getQtdMultas() {
		return this.qtdMultas;
	}

	public String getQtdMultasZer() {
		String qm = "000" + this.qtdMultas;
		qm = qm.substring(qm.length() - 3, qm.length());
		return qm;
	}

	public void setNomResponsavel(String nomResponsavel) {
		if (nomResponsavel == null)
			nomResponsavel = "";
		this.nomResponsavel = nomResponsavel;
	}

	public String getNomResponsavel() {
		return this.nomResponsavel;
	}

	public void setNumCPF(String numCPF) {
		if (numCPF == null)
			numCPF = "";
		this.numCPF = numCPF;
	}

	public void setNumCPFEdt(String numCpf) {
		if (numCpf == null)
			numCpf = "";
		if (numCpf.length() < 14)
			numCpf += "               ";
		this.numCPF = numCpf.substring(0, 3) + numCpf.substring(4, 7)
				+ numCpf.substring(8, 11) + numCpf.substring(12, 14);
		this.numCPF = this.numCPF.trim();
	}

	public String getNumCPF() {
		return this.numCPF;
	}

	public String getNumCPFEdt() {
		String cpf = "";
		if (this.numCPF.trim().length() > 0) {
			cpf = "00000000000" + this.numCPF;
			cpf = cpf.substring(cpf.length() - 11, cpf.length());
			cpf = cpf.substring(0, 3) + "." + cpf.substring(3, 6) + "."
					+ cpf.substring(6, 9) + "-" + cpf.substring(9, 11);
		}
		return cpf;
	}

	public void setTipCNH(String tipCNH) {
		if (tipCNH == null)
			tipCNH = "";
		this.tipCNH = tipCNH;
	}

	public String getTipCNH() {
		return this.tipCNH;
	}

	public String getTipCNHDesc() {
		String tp = "CNH";
		if (this.tipCNH.equals("1"))
			tp = "PGU";
		if (this.tipCNH.equals("3"))
			tp = "PRV";
		return tp;
	}

	public void setNumCNH(String numCNH) {
		if (numCNH == null)
			numCNH = "";
		this.numCNH = numCNH;
	}

	public String getNumCNH() {
		String cnh = this.numCNH;
		if ((this.getTipCNH().equals("3")) && (cnh.length() > 9)) {
			cnh = cnh.substring(cnh.length() - 9, cnh.length());
			cnh = this.getUfCNH() + cnh;
		}
		return cnh;
	}

	public void setUfCNH(String ufCNH) {
		if (ufCNH == null)
			ufCNH = "";
		this.ufCNH = ufCNH;
	}

	public String getUfCNH() {
		return this.ufCNH;
	}

	public void setDatEmissaoCNH(String datEmissaoCNH) {
		if (datEmissaoCNH == null)
			datEmissaoCNH = "";
		if (datEmissaoCNH.equals("00000000"))
			datEmissaoCNH = "";
		if (datEmissaoCNH.equals("        "))
			datEmissaoCNH = "";
		this.datEmissaoCNH = datEmissaoCNH;
	}

	public String getDatEmissaoCNH() {
		return this.datEmissaoCNH;
	}

	public void setDatValidadeCNH(String datValidadeCNH) {
		if (datValidadeCNH == null)
			datValidadeCNH = "";
		if (datValidadeCNH.equals("00000000"))
			datValidadeCNH = "";
		if (datValidadeCNH.equals("        "))
			datValidadeCNH = "";
		this.datValidadeCNH = datValidadeCNH;
	}

	public String getDatValidadeCNH() {
		return this.datValidadeCNH;
	}

	public void setCategoriaCNH(String categoriaCNH) {
		if (categoriaCNH == null)
			categoriaCNH = "";
		this.categoriaCNH = categoriaCNH;
	}

	public String getCategoriaCNH() {
		return this.categoriaCNH;
	}

	public void setEndereco(String endereco) {
		if (endereco == null)
			endereco = "";
		this.endereco = endereco;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setCep(String cep) {
		if (cep == null)
			cep = "";
		this.cep = cep;
	}

	public String getCep() {
		return this.cep;
	}

	public String getCepEdt() {
		String c = this.cep;
		if (this.cep.length() > 0) {
			String cepEdt = "00000000000" + this.cep;
			cepEdt = cepEdt.substring(cepEdt.length() - 8, cepEdt.length());
			c = cepEdt.substring(0, 5) + "-" + cepEdt.substring(5, 8);
		}
		return c;
	}

	public String getCodMunicipio() {
		return codMunicipio;
	}

	public void setCodMunicipio(String codMunicipio) {
		if (codMunicipio == null)
			codMunicipio = "";
		this.codMunicipio = codMunicipio;
	}

	public void setMunicipio(String municipio) {
		if (municipio == null)
			municipio = "";
		this.municipio = municipio;
	}

	public String getMunicipio() {
		return this.municipio;
	}

	public void setUf(String uf) {
		if (uf == null)
			uf = "";
		this.uf = uf;
	}

	public String getUf() {
		return this.uf;
	}

	public void setUltDatDefesa(String ultDatDefesa) {
		if (ultDatDefesa == null)
			ultDatDefesa = "";
		if (ultDatDefesa.equals("00000000"))
			ultDatDefesa = "";
		if (ultDatDefesa.equals("        "))
			ultDatDefesa = "";
		this.ultDatDefesa = ultDatDefesa;
	}

	public String getUltDatDefesa() {
		return this.ultDatDefesa;
	}

	public void setUltDatRecurso(String ultDatRecurso) {
		if (ultDatRecurso == null)
			ultDatRecurso = "";
		if (ultDatRecurso.equals("00000000"))
			ultDatRecurso = "";
		if (ultDatRecurso.equals("        "))
			ultDatRecurso = "";
		this.ultDatRecurso = ultDatRecurso;
	}

	public String getUltDatRecurso() {
		return this.ultDatRecurso;
	}

	public void setUltDatEntregaCNH(String ultDatEntregaCNH) {
		if (ultDatEntregaCNH == null)
			ultDatEntregaCNH = "";
		if (ultDatEntregaCNH.equals("00000000"))
			ultDatEntregaCNH = "";
		if (ultDatEntregaCNH.equals("        "))
			ultDatEntregaCNH = "";
		this.ultDatEntregaCNH = ultDatEntregaCNH;
	}

	public String getUltDatEntregaCNH() {
		return this.ultDatEntregaCNH;
	}

	public String getDatApresentaDefesa() {
		return this.datApresentaDefesa;
	}

	public void setDatApresentaDefesa(String datApresentaDefesa) {
		if (datApresentaDefesa == null)
			datApresentaDefesa = "";
		if (datApresentaDefesa.equals("00000000"))
			datApresentaDefesa = "";
		if (datApresentaDefesa.equals("        "))
			datApresentaDefesa = "";
		this.datApresentaDefesa = datApresentaDefesa;
	}

	public String getDatApresentaRecurso() {
		return this.datApresentaRecurso;
	}

	public void setDatApresentaRecurso(String datApresentaRecurso) {
		if (datApresentaRecurso == null)
			datApresentaRecurso = "";
		if (datApresentaRecurso.equals("00000000"))
			datApresentaRecurso = "";
		if (datApresentaRecurso.equals("        "))
			datApresentaRecurso = "";
		this.datApresentaRecurso = datApresentaRecurso;
	}

	public String getCodPenalidade() {
		return this.codPenalidade;
	}

	public void setCodPenalidade(String codPenalidade) {
		if (codPenalidade == null)
			codPenalidade = "";
		this.codPenalidade = codPenalidade;
	}

	public String getDscPenalidade() {
		return this.dscPenalidade;
	}

	public void setDscPenalidade(String dscPenalidade) {
		if (dscPenalidade == null)
			dscPenalidade = "";
		this.dscPenalidade = dscPenalidade;
	}

	public void setCodStatus(String codStatus) {
		if (codStatus == null)
			codStatus = "";
		this.codStatus = codStatus;
	}

	public String getCodStatus() {
		return this.codStatus;
	}

	public void setDatStatus(String datStatus) {
		if (datStatus == null)
			datStatus = "";
		if (datStatus.equals("00000000"))
			datStatus = "";
		if (datStatus.equals("        "))
			datStatus = "";
		this.datStatus = datStatus;
	}

	public String getDatStatus() {
		return this.datStatus;
	}

	public void setNomStatus(String nomStatus) {
		if (nomStatus == null)
			nomStatus = "";
		this.nomStatus = nomStatus;
	}

	public String getNomStatus() {
		return nomStatus;
	}

	public String getDatProcessoFis() {
		return this.datProcessoFis;
	}

	public void setDatProcessoFis(String datProcessoFis) {
		if (datProcessoFis == null)
			datProcessoFis = "";
		if (datProcessoFis.equals("00000000"))
			datProcessoFis = "";
		if (datProcessoFis.equals("        "))
			datProcessoFis = "";
		this.datProcessoFis = datProcessoFis;
	}

	public void setNomUserNameFis(String nomUserNameFis) {
		if (nomUserNameFis == null)
			nomUserNameFis = "";
		this.nomUserNameFis = nomUserNameFis;
	}

	public String getNomUserNameFis() {
		return this.nomUserNameFis;
	}

	public void setPrazoSupensao(String prazoSupensao) {
		if (prazoSupensao == null)
			prazoSupensao = "";
		this.prazoSupensao = prazoSupensao;
	}

	public String getPrazoSupensao() {
		if (this.prazoSupensao.equals(""))
			return this.prazoSupensao;
		else {
			Integer prazoEmMeses = Integer.parseInt(prazoSupensao) / 30;
			return prazoEmMeses.toString();
		}
		// return this.prazoSupensao;
	}

	public void setCodOrgaoLotacaoFis(String codOrgaoLotacaoFis) {
		if (codOrgaoLotacaoFis == null)
			codOrgaoLotacaoFis = "";
		this.codOrgaoLotacaoFis = codOrgaoLotacaoFis;
	}

	public String getCodOrgaoLotacaoFis() {
		return this.codOrgaoLotacaoFis;
	}

	public String getDatProcFis() {
		return this.datProcFis;
	}

	public void setDatProcFis(String datProcFis) {
		if (datProcFis == null)
			datProcFis = "";
		if (datProcFis.equals("00000000"))
			datProcFis = "";
		if (datProcFis.equals("        "))
			datProcFis = "";
		this.datProcFis = datProcFis;
	}

	public void setTxtEMail(String txtEMail) {
		if (txtEMail == null)
			txtEMail = "";
		this.txtEMail = txtEMail;
	}

	public String getTxtEMail() {
		return this.txtEMail;
	}

	// --------------------------------------------------------------------------
	public void LeProcesso() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeProcesso(this);
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void LeProcesso(String tpChave) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeProcesso(this, tpChave);
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public void buscarListaProcessos(String ordem, ACSS.UsuarioBean myUsrLog)
			throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.buscarListaProcessos(this, ordem, myUsrLog);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void GravaProcesso(Connection conn) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.GravaProcesso(this, conn);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public void setRequerimentos(List<RequerimentoBean> requerimentos) {
		if (requerimentos == null)
			requerimentos = new ArrayList<RequerimentoBean>();
		this.requerimentos = requerimentos;
	}

	public List<RequerimentoBean> getRequerimentos() {
		return this.requerimentos;
	}

	public RequerimentoBean getRequerimentos(int seq) throws sys.BeanException {
		RequerimentoBean re = new RequerimentoBean();
		try {
			if ((this.requerimentos != null)
					&& (this.requerimentos.size() > seq)) {
				re = (RequerimentoBean) this.requerimentos.get(seq);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return re;
	}

	public RequerimentoBean getRequerimentos(String myCodRequerimento)
			throws sys.BeanException {
		try {
			if (myCodRequerimento == null)
				myCodRequerimento = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;

			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getCodRequerimento().equals(myCodRequerimento)) {
					achou = true;
					break;
				}
			}

			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentos(String myNumRequerimento,
			String campo) throws sys.BeanException {
		try {
			if (myNumRequerimento == null)
				myNumRequerimento = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getNumRequerimento().equals(myNumRequerimento)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentoTipo(String myTipo)
			throws sys.BeanException {
		try {
			if (myTipo == null)
				myTipo = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;

			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getCodTipoSolic().equals(myTipo)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentoStatus(String myStatus)
			throws sys.BeanException {
		try {
			if (myStatus == null)
				myStatus = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;

			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getCodStatusRequerimento().equals(myStatus)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentoPorStatusETipoRequerimento(
			String myStatus, String tipoSolicitacao) throws sys.BeanException {
		try {
			if (myStatus == null)
				myStatus = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;

			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if ((re.getCodStatusRequerimento().equals(myStatus))
						&& (re.getCodTipoSolic().equals(tipoSolicitacao))) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void LeRequerimentos() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeRequerimentos(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void setTipoRequerimento(String tiposSolic) throws sys.BeanException {
		this.reqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer
				.append("<select style='border-style: outset;' name='codRequerimento' size='1'> \n");

		try {
			Iterator<RequerimentoBean> it = getRequerimentos().iterator();
			RequerimentoBean req = new RequerimentoBean();
			String nom = "";
			String sel = " selected ";
			while (it.hasNext()) {
				req = (RequerimentoBean) it.next();
				if (tiposSolic.indexOf(req.getCodTipoSolic()) >= 0) {
					nom = req.getCodTipoSolic() + " "
							+ req.getNumRequerimento() + " ("
							+ req.getDatRequerimento() + ")";
					popupBuffer.append("<OPTION VALUE='"
							+ req.getCodRequerimento() + "'" + sel + ">" + nom
							+ "</OPTION> \n");
					sel = "";
				}
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.reqs = popupBuffer.toString();
	}

	public String getReqs() {
		return this.reqs;
	}

	// --------------------------------------------------------------------------
	public void setHistoricos(List<HistoricoBean> historicos) {
		this.historicos = historicos;
	}

	public List<HistoricoBean> getHistoricos() {
		return this.historicos;
	}

	public HistoricoBean getHistoricos(String myCodHistorico)
			throws sys.BeanException {
		try {
			HistoricoBean his = new HistoricoBean();
			Iterator<HistoricoBean> itx = getHistoricos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				his = (HistoricoBean) itx.next();
				if (his.getCodHistoricoProcesso().equals(myCodHistorico)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				his = new HistoricoBean();
			return his;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void LeHistoricos() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeHistoricos(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void GravaHistoricos(Connection conn) throws sys.BeanException {
		try {
			HistoricoBean myHist = new HistoricoBean();
			for (int i = 0; i < getHistoricos().size(); i++) {
				myHist = getHistoricos().get(i);
				myHist.setCodProcesso(getCodProcesso());
				myHist.GravaHistorico(conn);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	// --------------------------------------------------------------------------
	public void setAtasDO(List<AtaDOBean> atasDO) {
		this.atasDO = atasDO;
	}

	public List<AtaDOBean> getAtasDO() {
		return this.atasDO;
	}

	public AtaDOBean getAtasDO(String myCodAta) throws sys.BeanException {
		try {
			AtaDOBean ata = new AtaDOBean();
			Iterator<AtaDOBean> itx = getAtasDO().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				ata = (AtaDOBean) itx.next();
				if (ata.getCodAtaDO().equals(myCodAta)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				ata = new AtaDOBean();
			return ata;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void LeAtas() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeAtasDO(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void setNomUserName(String nomUserName) {
		if (nomUserName == null)
			nomUserName = "";
		this.nomUserName = nomUserName;
	}

	public String getNomUserName() {
		return this.nomUserName;
	}

	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		if (codOrgaoLotacao == null)
			codOrgaoLotacao = "";
		this.codOrgaoLotacao = codOrgaoLotacao;
	}

	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}

	public String getDatProc() {
		return this.datProc;
	}

	public void setDatProc(String datProc) {
		if (datProc == null)
			datProc = "";
		if (datProc.equals("00000000"))
			datProc = "";
		if (datProc.equals("        "))
			datProc = "";
		this.datProc = datProc;
	}

	public void setMsgErro(String msgErro) {
		if (msgErro == null)
			msgErro = "";
		this.msgErro = msgErro;
	}

	public void setMsgErro(Vector vErro) {
		for (int j = 0; j < vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j);
		}
	}

	public String getMsgErro() {
		return this.msgErro;
	}

	public void setMsgOk(String msgOk) {
		if (msgOk == null)
			msgOk = "";
		this.msgOk = msgOk;
	}

	public String getMsgOk() {
		return this.msgOk;
	}

	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "";
		this.eventoOK = eventoOK;
	}

	public void setEventoOK(Vector vEventoOK) {
		for (int j = 0; j < vEventoOK.size(); j++) {
			this.eventoOK += vEventoOK.elementAt(j);
		}
	}

	public String getEventoOK() {
		return this.eventoOK;
	}

	public void setMultas(List<MultasBean> multas) {
		if (multas == null)
			multas = new ArrayList<MultasBean>();
		this.multas = multas;
	}

	public List<MultasBean> getMultas() {
		return this.multas;
	}

	public MultasBean getMultas(Integer i) {
		return this.multas.get(i);
	}

	public void LeMultas() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeMultas(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void GravaMultas(Connection conn) throws sys.BeanException {
		try {
			MultasBean myMulta = new MultasBean();
			for (int i = 0; i < getMultas().size(); i++) {
				myMulta = getMultas().get(i);
				myMulta.setCodProcesso(getCodProcesso());
				myMulta.GravaMulta(conn);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void setNotificacoes(List<NotificacaoBean> notificacoes) {
		if (notificacoes == null)
			notificacoes = new ArrayList<NotificacaoBean>();
		this.notificacoes = notificacoes;
	}

	public List<NotificacaoBean> getNotificacoes() {
		return this.notificacoes;
	}

	public NotificacaoBean getNotificacoes(Integer i) {
		return this.notificacoes.get(i);
	}

	public void LeNotificacoes() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeNotificacoes(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void GravaNotificacoes(Connection conn) throws sys.BeanException {
		try {
			NotificacaoBean myNotificacao = new NotificacaoBean();
			for (int i = 0; i < getNotificacoes().size(); i++) {
				myNotificacao = getNotificacoes().get(i);
				myNotificacao.setCodProcesso(getCodProcesso());
				myNotificacao.GravarNotificacao(conn);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void setProcAnteriores(List<ProcessoAntBean> procAnteriores) {
		if (procAnteriores == null)
			procAnteriores = new ArrayList<ProcessoAntBean>();
		this.procAnteriores = procAnteriores;
	}

	public List<ProcessoAntBean> getProcAnteriores() {
		return this.procAnteriores;
	}

	public ProcessoAntBean getProcAnteriores(Integer i) {
		return this.procAnteriores.get(i);
	}

	public void LeProcessoAnts() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeProcessoAnts(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void GravaProcessoAnteriores(Connection conn)
			throws sys.BeanException {
		try {
			ProcessoAntBean myProcessoAnt = new ProcessoAntBean();
			for (int i = 0; i < getProcAnteriores().size(); i++) {
				myProcessoAnt = getProcAnteriores().get(i);
				myProcessoAnt.setCodProcesso(getCodProcesso());
				myProcessoAnt.GravaProcessoAnt(conn);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public String getCodArquivo() {
		return codArquivo;
	}

	public void setCodArquivo(String codArquivo) {
		if (codArquivo == null)
			codArquivo = "";
		this.codArquivo = codArquivo;
	}

	public String getTotNotificacoes() {
		return totNotificacoes;
	}

	public void setTotNotificacoes(String totNotificacoes) {
		int qtd = 0;
		if (totNotificacoes == null)
			totNotificacoes = "0";
		try {
			qtd = Integer.parseInt(totNotificacoes);
		} catch (Exception e) {
			totNotificacoes = "0";
		}
		this.totNotificacoes = String.valueOf(qtd);
	}

	public void setProcessos(List<ProcessoBean> processos) {
		if (processos == null)
			processos = new ArrayList<ProcessoBean>();
		this.processos = processos;
	}

	public List<ProcessoBean> getProcessos() {
		return this.processos;
	}

	public ProcessoBean getProcessos(Integer i) {
		return this.processos.get(i);
	}

	public void LeProcessoPenalidade(String numProcessosAplicarPenalidade)
			throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeProcessoPenalidade(this, numProcessosAplicarPenalidade);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public void setARDigitalizado(List<ARDigitalizadoBean> arDigitalizado) {
		this.arDigitalizado = arDigitalizado;
	}

	public List<ARDigitalizadoBean> getARDigitalizado() {
		return this.arDigitalizado;
	}

	public void LeARDigitalizado() throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeARDigitalizado(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	public String getCodARDigitalizado() {
		return codArDigitalizado;
	}

	public void setCodARDigitalizado(String codArDigitalizado) {
		if (codArDigitalizado == null)
			codArDigitalizado = "";
		this.codArDigitalizado = codArDigitalizado;
	}

	public String getCodArtigo() {
		return codArtigo;
	}

	public void setCodArtigo(String codArtigo) {
		if (codArtigo == null)
			codArtigo = "";
		this.codArtigo = codArtigo;
	}

	public void setCodArtigoEdt(String codArtigo) {
		if (codArtigo == null)
			codArtigo = "";
		codArtigo = codArtigo.replaceAll("ART.", "");
		codArtigo = codArtigo.replaceAll("ART ", "");
		codArtigo = codArtigo.trim();

		/* Retirar INCISO */
		codArtigo = codArtigo.replaceAll(" INC.", ",");
		codArtigo = codArtigo.replaceAll(" INC", ",");
		codArtigo = codArtigo.trim();

		this.codArtigo = codArtigo;
	}

	public String getDscPenalidadeRes() {
		return dscPenalidadeRes;
	}

	public void setDscPenalidadeRes(String dscPenalidadeRes) {
		if (dscPenalidadeRes == null)
			dscPenalidadeRes = "";
		this.dscPenalidadeRes = dscPenalidadeRes;
	}

	public String getDtDevolucaoCNH() {
		return dtDevolucaoCNH;
	}

	public void setDtDevolucaoCNH(String dtDevolucaoCNH) {
		if (dtDevolucaoCNH == null)
			dtDevolucaoCNH = "";
		this.dtDevolucaoCNH = dtDevolucaoCNH;
	}

	public String getDtEntregaCNH() {
		return dtEntregaCNH;
	}

	public void setDtEntregaCNH(String dtEntregaCNH) {
		if (dtEntregaCNH == null)
			dtEntregaCNH = "";
		this.dtEntregaCNH = dtEntregaCNH;
	}

	public String getNumProtocoloEntregaCNH() {
		return numProtocoloEntregaCNH;
	}

	public void setNumProtocoloEntregaCNH(String numProtocoloEntregaCNH) {
		if (numProtocoloEntregaCNH == null)
			numProtocoloEntregaCNH = "";
		this.numProtocoloEntregaCNH = numProtocoloEntregaCNH;
	}

	public String getNumNotificacao() {
		return numNotificacao;
	}

	public void setNumNotificacao(String numNotificacao) {
		if (numNotificacao == null)
			numNotificacao = "";
		this.numNotificacao = numNotificacao;
	}

	public String buscaNomRelator(String cpf, String orgaoLotacao)
			throws DaoException, ServiceLocatorException {
		String nomRelator = "";
		DaoBroker dao = DaoBrokerFactory.getInstance();
		nomRelator = dao.buscaNomRelator(cpf, orgaoLotacao);
		return nomRelator;
	}

	// --------------------------------------------------------------------------
	public void setChaveSort(String chaveSort) {
		this.chaveSort = chaveSort;
	}

	public String getChaveSort() {
		return this.chaveSort;
	}

	// --------------------------------------------------------------------------

	/**
	 * Busca a lista de Notificações do BD da aplicação
	 * 
	 * @param codArquivo
	 * @throws BeanException
	 */
	public void buscarNotificacoes() throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarListaNotificaçõesPor(this);
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}

	public String getProximoStatus() {
		return proximoStatus;
	}

	public void setProximoStatus(String proximoStatus) {
		this.proximoStatus = proximoStatus;
	}
}