package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import REC.ParamOrgBean;

public class GuiaIncluirProcessoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/PNT/DEF/GuiaIncluirProcesso.jsp";

	public GuiaIncluirProcessoCmd() {
		next = jspPadrao;
	}

	public GuiaIncluirProcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {
			

			
			// cria os Beans de sessao, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();

			ParamOrgBean ParamOrgaoId = (ParamOrgBean) session.getAttribute("ParamOrgBeanId");
			if (ParamOrgaoId == null)ParamOrgaoId = new ParamOrgBean();

			ProcessoBean ProcessoBeanId =(ProcessoBean) req.getAttribute("ProcessoBeanId");
			RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
			if (RequerimentoId == null)	RequerimentoId = new RequerimentoBean();
			
			GuiaIncluirProcessoBean GuiaIncluirProcessoBeanId =	(GuiaIncluirProcessoBean) req.getAttribute("GuiaIncluirProcessoBeanId");
			if (GuiaIncluirProcessoBeanId == null)	GuiaIncluirProcessoBeanId = new GuiaIncluirProcessoBean();
			/*
			 * Comentado porque na Pontua��o utiliza FuncionalidadesBean.
			 *GuiaIncluirProcessoBeanId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			 */
	      	FuncionalidadesBean funcionalidades         = (FuncionalidadesBean)session.getAttribute("funcionalidadesBean") ;
	      	if (funcionalidades==null)  funcionalidades  = new FuncionalidadesBean() ;  
	      	funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			session.setAttribute("codReq", "");
			if ((acao==null) || ("".equals(acao))){
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = " ";
			}

			String sigFuncao = usrFunc.getJ_sigFuncao();

			sys.Command cmd = (sys.Command) Class.forName("PNT.ProcessaProcessoCmd").newInstance();
			cmd.setNext(this.next);
			nextRetorno = cmd.execute(req);
			
			
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoBean GuiaDistribuicaoId  = new GuiaDistribuicaoBean();
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					
					
					ProcessoBeanId.LeRequerimentos();							
					RequerimentoId = funcionalidades.setEventoOK(ProcessoBeanId);
					
					GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,ProcessoBeanId, sigFuncao,RequerimentoId.getNumRequerimento(),UsrLogado,funcionalidades);
					session.setAttribute("codReq", GuiaIncluirProcessoBeanId.getNumReq());
					
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;									
					req.setAttribute("RequerimentoId",RequerimentoId);
					nextRetorno = next;					
				}				
			}
			
			if (acao.equals("IncluirProcesso")) {
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				String codLoteRelator = req.getParameter("codLoteRelator");
				String codReq = req.getParameter("codReq");
				String existeGuia = "";
				ProcessoBeanId.LeRequerimentos();
				//Bahia
				
				existeGuia = GuiaIncluirProcessoBeanId.ConsultaReqGuia(ProcessoBeanId);
				session.setAttribute("codReq", GuiaIncluirProcessoBeanId.getNumReq());
				
				GuiaIncluirProcessoBeanId.setEventoOK(ProcessoBeanId);
				
				GuiaIncluirProcessoBeanId.incluirProcessoGuia(ProcessoBeanId,codLoteRelator,codReq,UsrLogado,existeGuia,funcionalidades);
				if (ProcessoBeanId.getMsgErro().length()==0){
					ProcessoBeanId.setMsgErro("Requerimento inclu�do na guia " + codLoteRelator + " com sucesso.");
					ProcessoBeanId.setMsgOk("S") ;
				}
			}	

			//Bahia
			if (acao.equals("LeReq")) {
				String codReq = req.getParameter("codReq");
				ProcessoBeanId.LeRequerimentos();
				//GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,ProcessoBeanId,sigFuncao,codReq,UsrLogado);
				GuiaIncluirProcessoBeanId.setEventoOK(ProcessoBeanId);
				session.setAttribute("codReq", codReq);
			}	

			req.setAttribute("ProcessoBeanId", ProcessoBeanId);
			req.setAttribute("GuiaIncluirProcessoBeanId", GuiaIncluirProcessoBeanId); ;
      		session.setAttribute("funcionalidadesBean",funcionalidades) ;
		} catch (Exception ue) {
			throw new sys.CommandException("GuiaIncluirProcessoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

}
