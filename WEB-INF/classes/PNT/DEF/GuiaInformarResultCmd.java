package PNT.DEF;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.ProcessoBean;
import PNT.TAB.PenalidadeBean;
import REC.ParamOrgBean;

public class GuiaInformarResultCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/PNT/DEF/GuiaReemite.jsp" ;  
   
   public GuiaInformarResultCmd() {
      next = jspPadrao;
   }

   public GuiaInformarResultCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
          
          ACSS.UsuarioFuncBean UsuarioFuncBeanId  = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
          
		  ParamOrgBean ParamOrgaoId  = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId  = new ParamOrgBean() ;	 
		  
      	  GuiaDistribuicaoBean GuiaDistribuicaoId  = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;
      	  
      	  //Carrego os Beans
			/*
			 * Comentado porque na Pontua��o utiliza FuncionalidadesBean.
			 *GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			 */
			FuncionalidadesBean funcionalidadesBeanId = new FuncionalidadesBean();
			funcionalidadesBeanId.setSigFuncao(req.getParameter("j_sigFuncao"));
			session.setAttribute("funcionalidadesBean", funcionalidadesBeanId);
			
		  GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
     	  
		  String dataIni = req.getParameter("De");
		  if (dataIni == null) dataIni = "";
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  if (dataFim == null) dataFim = "";
		  GuiaDistribuicaoId.setDataFim(dataFim);
		  
		  
		  String acao = req.getParameter("acao");   
		  if (acao == null) acao = " ";	
		  if ( (acao.equals(" ")) || (acao.equals("Novo")) )
		  {		  
			GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));

			/*
			 * Comentado porque na Pontua��o utiliza FuncionalidadesBean. 
			 */
			funcionalidadesBeanId.setSigFuncao(req.getParameter("j_sigFuncao"));
			
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");	
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia 
			 * COMENTADO EM 26/04/2012*/
			//GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	acao = " ";
		  }
		  else 
		  {
			  if ("Classifica,processoMostra,imprimirInf,ImprimirGuia".indexOf(acao)>0)	{
					sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
					cmd.setNext(this.next) ;
					nextRetorno = cmd.execute(req);
				}		
		  }
		  
		  if (acao.equals("LeGuia")) 
		  {
		    GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
			/*
			 * Comentado porque na Pontua��o utiliza FuncionalidadesBean.
			 * GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			 */
			funcionalidadesBeanId.setSigFuncao(req.getParameter("j_sigFuncao"));
		    GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		    GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());						
			GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
			GuiaDistribuicaoId.LeGuia();
		    if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO") ;
			if (GuiaDistribuicaoId.getDatRS().length()==0)GuiaDistribuicaoId.setDatRS(sys.Util.formatedToday().substring(0,10));		    
			nextRetorno = "/PNT/DEF/GuiaInformaResult.jsp";
		  }	        
		  
          if (acao.equals("Classifica")) 
          {
             GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;       
			 nextRetorno = "/PNT/DEF/GuiaInformaResult.jsp";
      	  }
          
		  if  (acao.equals("ImprimirGuia"))  {	      	  
		  	if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","N�O EXISTEM PROCESSOS NA GUIA");				  	    
		  	nextRetorno = "/PNT/DEF/GuiaPreparaImp.jsp" ;      	  
		  }
		       	  
		  if (acao.equals("InformaResult"))
		  {
		  	Vector vErro = new Vector(); 
			GuiaDistribuicaoId.setDatRS(req.getParameter("datRS"));
			if (GuiaDistribuicaoId.getDatRS().length()==0) vErro.addElement("Data do Resultado n�o preenchida. \n") ;
			else {
				if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),GuiaDistribuicaoId.getDatRS())>0) vErro.addElement("Data do Resultado n�o pode ser superior a hoje. \n") ;
				else {			
					if (sys.Util.DifereDias(GuiaDistribuicaoId.getDatRS(),GuiaDistribuicaoId.getDatProc())>0) vErro.addElement("Data n�o pode ser anterior a Guia ("+GuiaDistribuicaoId.getDatProc()+"). \n") ;
				}
			}
			
			if (vErro.size()==0) 
			{
		    	int err = VerificaInformaResult(req,GuiaDistribuicaoId,UsrLogado) ;
		    	if (err==0) { 
					GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
					String datSessao = req.getParameter("datSessao");
					if (datSessao == null) datSessao = "";
					GuiaDistribuicaoId.setDatSessao(datSessao);
					/**
					 * Implementa��o para ler a penalidade e inserir nos processos.    
					 */
					int inc = dao.GuiaGravaResult(GuiaDistribuicaoId,funcionalidadesBeanId) ; 
			  		GuiaDistribuicaoId.setMsgErro("Incluidos "+inc+" Resultados para Guia "+GuiaDistribuicaoId.getNumGuia());
					GuiaDistribuicaoId.LeGuia();
					GuiaDistribuicaoId.setMsgOk("S");
			  		nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp" ;
		    	}
		    	else {
			  		GuiaDistribuicaoId.setMsgErro(GuiaDistribuicaoId.getMsgErro()+" \n Nenhum Resultado incluido ");
					nextRetorno = "/PNT/DEF/GuiaInformaResult.jsp"; 
		    	}
			}		
			else { 		
				GuiaDistribuicaoId.setMsgErro(vErro);
				nextRetorno = "/PNT/DEF/GuiaInformaResult.jsp";
		    } 
		  }
		  
          // processamento de saida dos formularios
          if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
      	  else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaInformaResultCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }

   public int VerificaInformaResult(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
	   int err = 0 ;
	   try {	
			GuiaDistribuicaoId.setMsgErro("") ;				
			// buscar os resultados
			if (GuiaDistribuicaoId.getProcessos().size()>0) 
			{
				int seq = 0;
		 		Iterator it = GuiaDistribuicaoId.getProcessos().iterator() ;
		 		while (it.hasNext()) 
		 		{
					ProcessoBean myProc  = new ProcessoBean();		 			
			 		myProc = (ProcessoBean)it.next() ;	
			 		myProc.LeProcesso();
					myProc.getRequerimentos(myProc.getNumRequerimento(),"numRequerimento").setCodResultRS(req.getParameter("codResultRS"+seq))	;					
					myProc.getRequerimentos(myProc.getNumRequerimento(),"numRequerimento").setTxtMotivoRS(req.getParameter("txtMotivoRS"+seq))	;
                    //myProc.getRequerimentos(0).setCodMotivoDef(req.getParameter("codMotivo"+seq)) ;
					if (myProc.getRequerimentos(myProc.getNumRequerimento(),"numRequerimento").getCodResultRS().length()==0) {
						myProc.setMsgErro("Resultado n�o informado.");
						err++;
		 			}
					/**
					 * FIXME Inclus�o da penalidade vindo da view
					 */
						//Verifica se o resultado � indeferido, pois se for Deferido n�o precisa escolher a Penalidade.
						if (myProc.getRequerimentos(myProc.getNumRequerimento(),"numRequerimento").getCodResultRS().equals("I")){
							myProc.setCodPenalidade(req.getParameter("codPenalidade"+seq));
							if(myProc.getCodPenalidade().length()==0){
								myProc.setMsgErro("Penalidade n�o informada.");
								err++;
							}else{
								PenalidadeBean myPenalidade = new PenalidadeBean();
								myPenalidade.setCodPenalidade(myProc.getCodPenalidade());
								myPenalidade.Le_Penalidade("codigo");
								myProc.setDscPenalidade(myPenalidade.getDscPenalidade());
								myProc.setDscPenalidadeRes(myPenalidade.getDscResumida());
							}
						}else if (myProc.getRequerimentos(myProc.getNumRequerimento(),"numRequerimento").getCodResultRS().equals("D")){
							myProc.setCodPenalidade("0");
						}
					seq++;			
				}
				if (err>0) GuiaDistribuicaoId.setMsgErro(err+" Requerimento(s) sem resultado.") ;			
			}
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaInformaResultCmd: " + ue.getMessage());
	   }
	   return err;
   }  
}