package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import REC.ParamOrgBean;

public class GuiaEstornoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/GuiaReemite.jsp" ;  
	
	public GuiaEstornoCmd() {
		next = jspPadrao;
	}
	
	public GuiaEstornoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
			ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
			GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
			if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
			
			FuncionalidadesBean funcionalidades = (FuncionalidadesBean)session.getAttribute("funcionalidadesBeanId") ;
			if (funcionalidades==null)  funcionalidades= new FuncionalidadesBean() ;
			funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
			        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");
			String acao    = req.getParameter("acao");   	 
			String statusInteracao = req.getParameter("statusInteracao");

			if (acao==null)	{				  	
				acao = " ";
				statusInteracao = "1";
			}else{
				String dataIni = req.getParameter("De");
			    if (dataIni == null) dataIni = "";
			    GuiaDistribuicaoId.setDataIni(dataIni);
			  
			    String dataFim = req.getParameter("Ate");
			    if (dataFim == null) dataFim = "";
			    GuiaDistribuicaoId.setDataFim(dataFim);
			}
			
			if (acao.equals("LeGuia")) {				
				GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
				GuiaDistribuicaoId.LeGuia();
				if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO") ;
				nextRetorno = "/PNT/DEF/GuiaEstorno.jsp";
			}	        
			
			if  (acao.equals("ImprimirGuia"))  {	      	  
				if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO") ;				  	    
				nextRetorno = "/PNT/DEF/GuiaPreparaImp.jsp" ;      	  
			}
			
			if (acao.equals("Classifica")) {  
				GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;
				nextRetorno = "/PNT/DEF/GuiaEstorno.jsp";
			}
			
			if (acao.equals("estonarGuia")) {  
				GuiaDistribuicaoId.estornarGuia(funcionalidades);
				nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp" ; 				
			}
			
			// processamento de saida dos formularios
			if (acao.equals("R")){
				session.removeAttribute("GuiaDistribuicaoId") ;
				if( statusInteracao!= null){
					if(statusInteracao.equals("apresentacao"))
						session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
				}
				String dataIni = GuiaDistribuicaoBean.formatedLast2Month().substring(0, 10);
			    String dataFim = sys.Util.formatedToday().substring(0, 10);
 		        GuiaDistribuicaoId.setDataIni(dataIni);
				GuiaDistribuicaoId.setDataFim(dataFim);
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
				
			}
			else{
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
			}	
			session.setAttribute("funcionalidadesBean",funcionalidades) ;
			session.setAttribute("FuncionalidadesId",funcionalidades) ;
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEstornarJuntaRelatorCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}  
}
