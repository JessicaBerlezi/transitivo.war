package PNT.DEF;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BeanException;
import sys.Util;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.RelatorBean;
import REC.ParamOrgBean;

public class GuiaAutomaticaCmd extends sys.Command {
	private String next;   
	private static final String jspPadrao="/PNT/DEF/GuiaAutomatica.jsp" ;
	
	
	public GuiaAutomaticaCmd() {
		next = jspPadrao;
	}
	
	public GuiaAutomaticaCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;      	
			
			ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			
			RelatorBean RelatorBeanId = (RelatorBean)session.getAttribute("RelatorBeanId") ;
			if (RelatorBeanId==null)  RelatorBeanId = new RelatorBean() ;
			
			String acao    = req.getParameter("acao");
			if (acao==null)	{
				session.removeAttribute("GuiaDistribuicaoId") ;
				acao = " ";
			}		  
			
			GuiaDistribuicaoBean GuiaDistribuicaoId = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId");
			if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
			
			//Carrego os Beans
			String sSigFuncao=req.getParameter("j_sigFuncao");
			//if (sSigFuncao==null) sSigFuncao="REC0250";
			FuncionalidadesBean funcionalidadesId = new FuncionalidadesBean() ;
			funcionalidadesId.setSigFuncao(sSigFuncao);
			
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		  
			
			if ( (acao.equals(" ")) || (acao.equals("Retornar")) ){
				nextRetorno="/PNT/DEF/GuiaAutomatica.jsp";			  			 
			}	  
			
			if  (acao.equals("EnviarProcesso"))  
			{
				String numProcesso[] = req.getParameterValues("numProcesso");
				if (numProcesso==null) numProcesso = new String[0];
				nextRetorno = EnviarProcesso(numProcesso,GuiaDistribuicaoId,UsrLogado, funcionalidadesId);
				RelatorBeanId.getRelatoresAutomatica(funcionalidadesId);

			}	  			
			
			if (acao.equals("Distribuir"))  
			{
				try {	
					GerarGuias(req,GuiaDistribuicaoId,UsrLogado,ParamOrgaoId,sSigFuncao,funcionalidadesId);
					
				}
				catch (Exception ue) {
					throw new sys.CommandException("GuiaAutomaticaCmd: " + ue.getMessage());
				}				
				nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostraAutomatica.jsp"; 
			}

			if  (acao.equals("ImprimirGuia"))  {	   
				nextRetorno="/PNT/DEF/GuiaPreparaImpAutomatica.jsp" ;				
			}
			
			if (acao.equals("R")){
				session.removeAttribute("GuiaDistribuicaoId") ;
				session.removeAttribute("RelatorBeanId") ;
			}
			else {
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
				session.setAttribute("RelatorBeanId",RelatorBeanId) ;
				session.setAttribute("funcionalidadesBean", funcionalidadesId);
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaAutomaticaCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String EnviarProcesso(String[] numProcesso,GuiaDistribuicaoBean GuiaDistribuicaoId,UsuarioBean UsrLogado, FuncionalidadesBean funcionalidadesId)throws BeanException
	{
		UsuarioBean  UsuarioLogado = new ACSS.UsuarioBean();  
		UsuarioLogado.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		String nextRetorno="/PNT/DEF/GuiaAutomaticaProcesso.jsp";
		int iTotalRequerimentos=0,iToTalProcessosErro=0,iToTalProcessos=0;;
		
		List <ProcessoBean>Processos = new ArrayList<ProcessoBean>();
		List <ProcessoBean>ProcessosSelect = new ArrayList<ProcessoBean>();		
		
		for (int j=0; j < numProcesso.length; j++) 
		{
			String sNumProcesso  = numProcesso[j];
			if (sNumProcesso.trim().length()==0) continue;
			iToTalProcessos++; 
			
			ProcessoBean myProcesso    = new ProcessoBean();
			ProcessoBean myProcessoReq = new ProcessoBean();
			
			myProcesso.setNumProcesso(sNumProcesso);
			myProcesso.LeProcesso();
			
			ProcessosSelect.add(myProcesso);
			
			// Verifica se existe numero de processo
			if (myProcesso.getNumProcesso().trim().length()!=0) {
				myProcesso.setMsgErro("Processo sem Requerimento");
				myProcesso.LeRequerimentos();
				int iRequerimentoAuto = 0;
				
				try {
					Iterator<RequerimentoBean> it = myProcesso.getRequerimentos().iterator();
					RequerimentoBean reqProcesso = new RequerimentoBean();
					
					// Itera a lista de requerimentos vindo do adab�s
					while (it.hasNext()) {
						reqProcesso = it.next();
						//Verifica se o processo possui requerimento
						if ((!reqProcesso.getCodStatusRequerimento().equals("7")) && (!reqProcesso.getCodStatusRequerimento().equals("8")) &&
								funcionalidadesId.getTipoRequerimento().indexOf(reqProcesso.getCodTipoSolic()) >=0){
							iTotalRequerimentos++;
							iRequerimentoAuto++;
							myProcesso.setMsgErro("");
							myProcessoReq = new ProcessoBean();
							
							myProcessoReq.setCodOrgao(myProcesso.getCodOrgao());
							myProcessoReq.setNumProcesso(myProcesso.getNumProcesso());
							myProcessoReq.setDatProcesso(myProcesso.getDatProcesso());
							myProcessoReq.setCodOrgao(myProcesso.getCodOrgao());
							myProcessoReq.setCodOrgaoLotacao(myProcesso.getCodOrgaoLotacao());
							myProcessoReq.setNumRequerimento(reqProcesso.getNumRequerimento());
							myProcessoReq.setTipoRequerimento(reqProcesso.getNomTipoSolic());
							//myProcessoReq.setIndPontuacao("N"); //Indicativo de Processo utilizado
							
							String sExiste = GuiaDaoBroker.getInstance().ConsultaReqGuia(myProcessoReq);
							if (sExiste.length()>0) 
								myProcessoReq.setMsgErro("Processo/Requerimento j� Cadastrado em outra Guia.");
							
							if (iRequerimentoAuto>1) 
								myProcessoReq.setCodArquivo("999");
							
							Processos.add(myProcessoReq);						
						}
					}					  
				} catch (Exception ex) {
					throw new sys.BeanException("GuiaAutomaticaCmd-EnviarProcesso"+ex.getMessage());
				}
			}
			else {
				Processos.add(myProcesso);
			}
			if (myProcesso.getMsgErro().length()>0)  iToTalProcessosErro++; 
			
			//Adicionar processos sem requerimento
			if (myProcesso.getMsgErro().equals("Processo sem Requerimento")) 
				Processos.add(myProcesso);
		}
		
		GuiaDistribuicaoId.setProcessos(Processos);
		GuiaDistribuicaoId.setProcessosSelect(ProcessosSelect);
		
		GuiaDistribuicaoId.setTotalRequerimentos(iTotalRequerimentos);
		GuiaDistribuicaoId.setToTalProcessosErro(iToTalProcessosErro);
		GuiaDistribuicaoId.setTotalProcessos(iToTalProcessos);
		if (iToTalProcessos==0) {
			GuiaDistribuicaoId.setMsgErro("Nenhum processo selecionado."); 
			nextRetorno="/PNT/DEF/GuiaAutomatica.jsp";			  
		}
		return nextRetorno;
	}
	
	
	public void GerarGuias(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
			UsuarioBean UsrLogado,ParamOrgBean ParamOrgaoId,String sSigFuncao,FuncionalidadesBean funcionalidadesBean) throws Exception {
		
		//Lista dos Processos Atual
		List<ProcessoBean> ListProcAtual = GuiaDistribuicaoId.getProcessos();
				
	    //Lista dos Relatores Atuais		
		String relatSelec[] = req.getParameterValues("relatores");
        
		if (relatSelec==null) relatSelec= new String[0];
		
        ArrayList<RelatorBean>listRelatores = new ArrayList<RelatorBean>();
		
        for (int j=0; j < relatSelec.length; j++) 
		{
			RelatorBean myRelator = new RelatorBean();
			myRelator.setCodJunta(relatSelec[j].substring(0,6));
			myRelator.setNumCpf(relatSelec[j].substring(6,17));
			myRelator.Le_Relator("CPF");
			listRelatores.add(myRelator);
		}
		CalcularProcessosPorRelator(listRelatores,ListProcAtual.size());
	
		ArrayList<Integer> ListSequencialProcSort = Util.ListaSequencialSort(ListProcAtual.size());
	
		GerarGuiasPorRelator(GuiaDistribuicaoId,listRelatores,ListProcAtual,ListSequencialProcSort,req,ParamOrgaoId,UsrLogado,sSigFuncao,funcionalidadesBean);
	}

	
	public void CalcularProcessosPorRelator(ArrayList <RelatorBean>ListRelatores,int iTamanho)
	{
		int iNumProcRelator=iTamanho/ListRelatores.size();
		int iRestoProcRelator=iTamanho%ListRelatores.size();
		for (int i=0;i<ListRelatores.size();i++)
		{
			if (i<iRestoProcRelator)
				ListRelatores.get(i).setProcessoPorGuia(iNumProcRelator+1);
			else
				ListRelatores.get(i).setProcessoPorGuia(iNumProcRelator);
		}
	}

	public void GerarGuiasPorRelator(GuiaDistribuicaoBean myGuias,ArrayList <RelatorBean>ListRelatores,List<ProcessoBean> ListProcAtual,ArrayList<Integer> ListSequencialProcSort,HttpServletRequest req,ParamOrgBean paramOrgao,UsuarioBean myUsuario,String sSigFuncao,FuncionalidadesBean funcionalidadesBean) throws Exception
	{
		List <GuiaDistribuicaoBean> myListGuia = new ArrayList <GuiaDistribuicaoBean>();
	        int iSequenciaProcUsados=0;
		for (int i=0;i<ListRelatores.size();i++)
		{
			GuiaDistribuicaoBean myGuia = new GuiaDistribuicaoBean();
			//myGuia.setJ_sigFuncao(sSigFuncao,paramOrgao.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			
			myGuia.setNomUserName(myUsuario.getNomUserName());
			myGuia.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			myGuia.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			myGuia.setTipoGuia(myGuia.GUIA_AUTOMATICA);
			myGuia.setCodJunta(ListRelatores.get(i).getCodJunta());
			myGuia.setSigJunta(ListRelatores.get(i).getSigJunta());			
			myGuia.setNumCPFRelator(ListRelatores.get(i).getNumCpf());
			myGuia.setNomRelator(ListRelatores.get(i).getNomRelator());			
			myGuia.setCodRelator(ListRelatores.get(i).getCodRelator());
			ArrayList <ProcessoBean>ListProcessos = new ArrayList<ProcessoBean>();			
			for (int j=0;j<ListRelatores.get(i).getProcessoPorGuia();j++)
			{
				ProcessoBean myProcessoRel = new ProcessoBean();
				myProcessoRel = ListProcAtual.get(ListSequencialProcSort.get(j+iSequenciaProcUsados));
				//if (myAutoRel.getIndPontuacao().equals("S")) continue;
				for (int k=0;k<ListProcAtual.size();k++) {
					ProcessoBean myAutoRelProc = new ProcessoBean();
					myAutoRelProc=(ProcessoBean)ListProcAtual.get(k);
					if (myProcessoRel.getNumProcesso().equals(myAutoRelProc.getNumProcesso()))
					{
						ListProcessos.add(myAutoRelProc);
						//((ProcessoBean)ListProcAtual.get(k)).setIndPontuacao("S");
					}
				}
                
				//ListProcessos.add((ProcessoBean)ListProcAtual.get((Integer)ListSequencialProcSort.get(j+iSequenciaProcUsados)));
				
			}
			myGuia.setProcessos(ListProcessos);
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			int retGuia= dao.GuiaGrava(myGuia,myUsuario,funcionalidadesBean);
			
			if (retGuia>0){
				myListGuia.add(myGuia);
				iSequenciaProcUsados+=ListRelatores.get(i).getProcessoPorGuia();
			}
		}
		myGuias.setRelatGuias(myListGuia);
	}
}