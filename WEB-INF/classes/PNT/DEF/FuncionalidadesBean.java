package PNT.DEF;

import PNT.ProcessoBean;
import PNT.RequerimentoBean;

public class FuncionalidadesBean  {
	
	private String statusValidos;
	private String statusDestino;
	private String tipoRequerimento;
	private String tipoJunta;
	private String statusRequerimento;
	private String codEvento;
	private String codOrigemEvento;
	private String nomFuncao;
	private String abrirRequerimento;
	private String tipoAta; //atributo novo jefferson.trindade
	
	public FuncionalidadesBean() {
		statusValidos      = "";
		statusDestino      = "";
		tipoRequerimento   = "";
		tipoJunta          = "";
		statusRequerimento = "";
		codEvento          = "";
		codOrigemEvento    = "101";
		nomFuncao          = "";
		abrirRequerimento  = "N";
		tipoAta = "";
	}
	
	public RequerimentoBean setEventoOK(ProcessoBean myProcesso)  throws sys.BeanException  {
		RequerimentoBean RequerimentoId = new RequerimentoBean();
		try {
			if ("".equals(myProcesso.getNomStatus())) {
				myProcesso.setMsgOk("Status: "+myProcesso.getCodStatus()+" n�o cadastrado na Tabela.");				
				myProcesso.setEventoOK("N");
			}
			else {
				myProcesso.setMsgOk(myProcesso.getNomStatus()+" (Data do Status: "+myProcesso.getDatStatus()+") ");
				String complMsg = getNomFuncao();
				myProcesso.setEventoOK("S");
				// Status 
				if ( (getStatusValidos()).indexOf(myProcesso.getCodStatus())<0) 	{
					myProcesso.setEventoOK("N");
					complMsg = " - Status n�o permite "+getNomFuncao();
				}
				// Verificar se existe Requerimento 
				RequerimentoId = myProcesso.getRequerimentoTipo(getTipoRequerimento());						
				if ("S".equals(getAbrirRequerimento())) {
					if(getTipoRequerimento().equals(RequerimentoId.getCodTipoSolic()) && RequerimentoId.getCodStatusRequerimento().indexOf("7,8")>0){
						myProcesso.setEventoOK("N");
						myProcesso.setMsgOk("");
						complMsg = "REQUERIMENTO "+getTipoRequerimento()+" ABERTO EM "+RequerimentoId.getDatRequerimento()+ " - Status do Processo n�o permite ABRIR DEFESA";
					}
				}
				else {
					if ( (getTipoRequerimento().equals(RequerimentoId.getCodTipoSolic())==false) || (RequerimentoId.getCodStatusRequerimento().equals("7")) ){					
						myProcesso.setEventoOK("N");
						complMsg = " - Sem Requerimento ("+getTipoRequerimento()+").";
					}
				}
				myProcesso.setMsgOk(myProcesso.getMsgOk() + complMsg) ;
			}
			
		} catch (Exception ue) {
			throw new sys.BeanException("FuncionalidadesBean: EventoOK " + ue.getMessage());
		}	
		return RequerimentoId;
	}	
	
	public void setSigFuncao(String j_sigFuncao)  {
		
		// 	Definir/Trocar Junta e Relator - DefesaPrevia - PNT0432 e PNT0420
		this.tipoJunta = "0";
		this.statusValidos = "010,011"; 
		this.tipoRequerimento = "DP";
		this.statusDestino = "011";
		this.statusRequerimento	= "2";
		this.codEvento = "811";
		this.nomFuncao = "Definir/Trocar Junta e Relator";
		
		if (j_sigFuncao==null) j_sigFuncao="";
	  	// Abrir Recurso - Defesa 
	  	if ("PNT0210".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "000,001,002,003,004,005,010";
			this.tipoRequerimento = "DP";
			this.statusDestino = "010";
			this.statusRequerimento	= "0";			
			this.codEvento = "810";
			this.nomFuncao = "Abrir Defesa";
			abrirRequerimento  = "S";
	  	}
	  	// Abrir Recurso - Recurso 
	  	if ("PNT0220".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "030,031,032,033,034,035,040";
			this.tipoRequerimento = "RP";
			this.statusDestino = "040";
			this.statusRequerimento	= "0";			
			this.codEvento = "840";
			this.nomFuncao = "Abrir Recurso";
			abrirRequerimento  = "S";
	  	}	  	
	  	// Abrir Recurso - 2a Instancia 
	  	if ("PNT0230".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "060,061,062,063,064,065,070";
			this.tipoRequerimento = "2P";
			this.statusDestino = "070";
			this.statusRequerimento	= "0";			
			this.codEvento = "870";
			this.nomFuncao = "Abrir Recurso 2aInst�ncia";
			abrirRequerimento  = "S";
	  	}	  		  	
		// Definir/Trocar Junta eRelator - 1a Instancia 
	  	if ("PNT0510".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "040,041";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";			
			this.codEvento = "841";
			this.nomFuncao = "Definir/Trocar Junta e Relator de Recurso";			
	  	}
	  	// Definir/Trocar Junta eRelator - 2a Instancia 
	  	if ("PNT0610".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "070,071";
			this.tipoRequerimento = "2P";
			this.statusDestino = "071";
			this.statusRequerimento	= "2";			
			this.codEvento = "871";			
			this.nomFuncao = "Definir/Trocar Junta e Relator de 2a Instancia";			
	  	}
	  	// Informa Resultado - Defesa
	  	if ("PNT0420".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "011";
			this.tipoRequerimento = "DP";
			this.statusDestino = "012";
			this.statusRequerimento	= "3";
			this.codEvento = "812";
			this.nomFuncao = "Informar Resultado de Defesa";
	  	}
	  	// Informa Resultado - 1a Instancia 
	  	if ("PNT0520".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "041";
			this.tipoRequerimento = "RP";
			this.statusDestino = "042";
			this.statusRequerimento	= "3";			
			this.codEvento = "842";
			this.nomFuncao = "Informar Resultado de Recurso";			
	  	}
	  	// Informa Resultado - 2a Instancia 
	  	if ("PNT0620".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "071";
			this.tipoRequerimento = "2P";
			this.statusDestino = "072";
			this.statusRequerimento	= "3";			
			this.codEvento = "872";			
			this.nomFuncao = "Informar Resultado de 2a Instancia";			
	  	}	  	
	  	// Estornar Resultado - Defesa 
	  	if ("PNT0492".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "012,018,019";
			this.tipoRequerimento = "DP";
			this.statusDestino = "011";
			this.statusRequerimento	= "2";
			this.codEvento = "817";
			this.nomFuncao = "Estornar Resultado de Defesa";
	  	}
	  	// Estornar Resultado - 1a Instancia 
	  	if ("PNT0592".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "042,048,049";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";			
			this.codEvento = "847";
			this.nomFuncao = "Estornar Resultado de Recurso";			
	  	}
	  	// Estornar Resultado - 2a Instancia 
	  	if ("PNT0692".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "072,078,079";
			this.tipoRequerimento = "2P";
			this.statusDestino = "071";
			this.statusRequerimento	= "2";			
			this.codEvento = "877";			
			this.nomFuncao = "Estornar Resultado de 2a Instancia";			
	  	}	  	
	  	
	  	// Estornar Requerimento - Defesa 
	  	if ("PNT0494".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
//			this.statusValidos = "010,011,012";
	  		this.statusValidos = "010";
			this.tipoRequerimento = "DP";
			this.statusDestino = "004";
			this.statusRequerimento	= "8";
			this.codEvento = "819";
			this.nomFuncao = "Estornar Requerimento de Defesa";
	  	}
	  	// Estornar Requerimento - 1a Instancia 
	  	if ("PNT0594".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
//			this.statusValidos = "040,041,042";
	  		this.statusValidos = "040";
			this.tipoRequerimento = "RP";
			this.statusDestino = "034";
			this.statusRequerimento	= "8";			
			this.codEvento = "849";
			this.nomFuncao = "Estornar Requerimento de Recurso";			
	  	}
	  	// Estornar Requerimento - 2a Instancia 
	  	if ("PNT0694".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
//			this.statusValidos = "070,071,072";
			this.statusValidos = "070";
			this.tipoRequerimento = "2P";
			this.statusDestino = "064";
			this.statusRequerimento	= "8";			
			this.codEvento = "879";			
			this.nomFuncao = "Estornar Requerimento de 2a Instancia";			
	  	}
	  	if ("PNT0710".equals(j_sigFuncao)) {
	  		this.tipoJunta = "3";
			this.statusValidos = "000,001,002,003,004,005,010,019,030,031,032,033,034,035,040,049,060,061,062,063,064,065,070,090,091,092,093,094,095,100";
			this.tipoRequerimento = "EC";
			this.statusDestino = "100";
			this.statusRequerimento	= "0";			
			this.codEvento = "900";
			this.nomFuncao = "Entrega CNH";
			abrirRequerimento  = "S";
	  	}
	  	
	  	// Guia de distribui��o
	  	
	  	//Defesa Pr�via
	  	// Prepara Guia e Prepara Guia autom�tica
	  	if ("PNT0431".equals(j_sigFuncao) || "PNT0439".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "010";
			this.tipoRequerimento = "DP";
			this.statusDestino = "011";
			this.statusRequerimento	= "2";			
			this.codEvento = "811";
			this.nomFuncao = "PREPARA GUIA DE DISTRIBUI��O - DEFESA PR�VIA";
	  	}
	  	// Troca Relator Guia
	  	if ("PNT0432".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "011";
			this.tipoRequerimento = "DP";
			this.statusDestino = "011";
			this.statusRequerimento	= "2";			
			this.codEvento = "811";
			this.nomFuncao = "TROCAR RELATOR DP - GUIA DE DISTRIBUI��O";
	  	}
	  	// Informar Resultado Guia
	  	if ("PNT0435".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "011";
			this.tipoRequerimento = "DP";
			this.statusDestino = "012";
			this.statusRequerimento	= "3";
			this.codEvento = "812";
			this.nomFuncao = "RESULTADO DE DEFESA PR�VIA - GUIA DE DISTRIBUI��O";
	  	}
	  	// Enviar para Publica��o
	  	if ("PNT0436".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "019,030";
			this.tipoRequerimento = "DP";
			this.statusDestino = "019,030";
			this.statusRequerimento	= "3";
			this.codEvento = "832"; // Novo Evento
			this.nomFuncao = "ENVIAR PARA PUBLICA��O DP - GUIA DE DISTRIBUI��O";
	  	}
	  	// Informar Publica��o
	  	if ("PNT0438".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "019,030";
			this.tipoRequerimento = "DP";
			this.statusDestino = "030";
			this.statusRequerimento	= "9";
			this.codEvento = "837";
			this.nomFuncao = "INFORMAR PUBLICA��O DP - GUIA DE DISTRIBUI��O";
	  	}
	  	// Estorno GUIA DE DISTRIBUI��O DP
	  	if ("PNT0442".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "010,011";
			this.tipoRequerimento = "DP";
			this.statusDestino = "010";
			this.statusRequerimento	= "8";
			this.codEvento = "822";
			this.nomFuncao = "ESTORNAR GUIA DE DISTRIBUI��O DP";
	  	}
	  	// Estorno RESULTADO GUIA DP
	  	if ("PNT0443".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "011,012";
			this.tipoRequerimento = "DP";
			this.statusDestino = "011";
			this.statusRequerimento	= "2";
			this.codEvento = "817";
			this.nomFuncao = "ESTORNAR GUIA DE DISTRIBUI��O DP";
	  	}
	  	// Estorno ENVIO P/ PUBLICA��O GUIA DP
	  	if ("PNT0444".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "019,030";
			this.tipoRequerimento = "DP";
			this.statusDestino = "019,030";
			this.statusRequerimento	= "3";
			this.codEvento = "818";
			this.nomFuncao = "ESTORNAR ENVIO P/ PUBLICA��O GUIA DP";
			this.tipoAta = "0";
	  	}
	  	// Estorno DATA DE PUBLICA��O GUIA DP
	  	if ("PNT0445".equals(j_sigFuncao)) {
	  		this.tipoJunta = "0";
			this.statusValidos = "019,030";
			this.tipoRequerimento = "DP";
			this.statusDestino = "019,030";
			this.statusRequerimento	= "9";
			this.codEvento = "820";
			this.nomFuncao = "ESTORNAR DATA DE PUBLICA��O GUIA DP";
			this.tipoAta = "0";
	  	}

	  	
	  	
	  	//1� Instancia
	  	// Prepara Guia e Prepara Guia autom�tica
	  	if ("PNT0531".equals(j_sigFuncao) || "PNT0539".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "040";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";			
			this.codEvento = "841";
			this.nomFuncao = "PREPARA GUIA DE DISTRIBUI��O - 1 Inst�ncia";
	  	}
	  	// Troca Relator Guia
	  	if ("PNT0532".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "041";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";			
			this.codEvento = "841";
			this.nomFuncao = "TROCAR RELATOR 1� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	// Incluir Processo
	  	if ("PNT0533".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "040";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";			
			this.codEvento = "841";
	  	}
	  	// Reemitir Guia
	  	if ("PNT0534".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.tipoRequerimento = "RP";
	  	}
	  	
	  	// Informar Resultado Guia
	  	if ("PNT0535".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "041";
			this.tipoRequerimento = "RP";
			this.statusDestino = "042";
			this.statusRequerimento	= "3";
			this.codEvento = "842";
			this.nomFuncao = "RESULTADO DE 1� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	// Enviar para Publica��o
	  	if ("PNT0536".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "049,090";
			this.tipoRequerimento = "RP";
			this.statusDestino = "049,090";
			this.statusRequerimento	= "3";
			this.codEvento = "862"; // Novo Evento para CETRAN 892
			this.nomFuncao = "ENVIAR PARA PUBLICA��O 1� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	
	  	// Reemitir Ata
	  	if ("PNT0537".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.tipoRequerimento = "RP";
	  	}
	  	// Informar Publica��o
	  	if ("PNT0538".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "049,064";
			this.tipoRequerimento = "RP";
			this.statusDestino = "064";
			this.statusRequerimento	= "9";
			this.codEvento = "867";
			this.nomFuncao = "INFORMAR PUBLICA��O 1� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	
	  	// Download ATA
	  	if ("PNT0540".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.tipoRequerimento = "RP";
	  	}
	  	
	 // 	Estorno GUIA DE DISTRIBUI��O 1� INST
	  	if ("PNT0542".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "040,041";
			this.tipoRequerimento = "RP";
			this.statusDestino = "040";
			this.statusRequerimento	= "8";
			this.codEvento = "852";
			this.nomFuncao = "ESTORNAR GUIA DE DISTRIBUI��O 1� INST";
	  	}
	  	// Estorno RESULTADO GUIA 1� INST
	  	if ("PNT0543".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "042";
			this.tipoRequerimento = "RP";
			this.statusDestino = "041";
			this.statusRequerimento	= "2";
			this.codEvento = "847";
			this.nomFuncao = "ESTORNAR GUIA DE DISTRIBUI��O 1� INST";
	  	}
	  	// Estorno ENVIO P/ PUBLICA��O GUIA 1� INST
	  	if ("PNT0544".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "049,090";
			this.tipoRequerimento = "RP";
			this.statusDestino = "049,090";
			this.statusRequerimento	= "3";
			this.codEvento = "848";
			this.nomFuncao = "ESTORNAR ENVIO P/ PUBLICA��O GUIA 1� INST";
			this.tipoAta = "2";
	  	}	
	  	// Estorno DATA DE PUBLICA��O GUIA 1� INST
	  	if ("PNT0544".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.statusValidos = "049,090";
			this.tipoRequerimento = "RP";
			this.statusDestino = "049,090";
			this.statusRequerimento	= "9";
			this.codEvento = "850";
			this.nomFuncao = "ESTORNAR ENVIO P/ PUBLICA��O GUIA 1� INST";
			this.tipoAta = "2";
	  	}	
	  	//Retirar processo da Guia
	  	if ("PNT0546".equals(j_sigFuncao)) {
	  		this.tipoJunta = "1";
			this.tipoRequerimento = "RP";
	  	}
	  	
	  	
	  	//2� Instancia
	  	// Prepara Guia e Prepara Guia autom�tica                            **** OK ****
	  	if ("PNT0631".equals(j_sigFuncao) || "PNT0639".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "070";
			this.tipoRequerimento = "2P";
			this.statusDestino = "071";
			this.statusRequerimento	= "2";			
			this.codEvento = "871";
			this.nomFuncao = "PREPARA GUIA DE DISTRIBUI��O - 2 Inst�ncia";
	  	}
	  	// Troca Relator Guia
	  	if ("PNT0632".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "071";
			this.tipoRequerimento = "2P";
			this.statusDestino = "071";
			this.statusRequerimento	= "2";			
			this.codEvento = "871";
			this.nomFuncao = "TROCAR RELATOR 2� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	// Incluir Processo
	  	if ("PNT0633".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "070";
			this.tipoRequerimento = "2P";
			this.statusDestino = "071";
			this.statusRequerimento	= "2";			
			this.codEvento = "871";
	  	}
	  	// Reemitir Guia                             **** OK ****
	  	if ("PNT0634".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.tipoRequerimento = "2P";
	  	}
	  	
	  	// Informar Resultado Guia                              **** OK ****
	  	if ("PNT0635".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "071";
			this.tipoRequerimento = "2P";
			this.statusDestino = "072";
			this.statusRequerimento	= "3";
			this.codEvento = "872";
			this.nomFuncao = "RESULTADO DE 2� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	// Enviar para Publica��o
	  	if ("PNT0636".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "079,090";
			this.tipoRequerimento = "2P";
			this.statusDestino = "079";
			this.statusRequerimento	= "3";
			this.codEvento = "892"; // Novo Evento para CETRAN 892
			this.nomFuncao = "ENVIAR PARA PUBLICA��O 2� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	// Reemitir Ata
	  	if ("PNT0637".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.tipoRequerimento = "2P";
	  	}
	  	// Informar Publica��o
	  	if ("PNT0638".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.statusValidos = "079";
			this.tipoRequerimento = "2P";
			this.statusDestino = "090";
			this.statusRequerimento	= "9";
			this.codEvento = "897";
			this.nomFuncao = "INFORMAR PUBLICA��O 2� Inst�ncia - GUIA DE DISTRIBUI��O";
	  	}
	  	
	  	// Download ATA
	  	if ("PNT0640".equals(j_sigFuncao)) {
	  		this.tipoJunta = "2";
			this.tipoRequerimento = "2P";
	  	}
	  	
	  	return;
	}

	
	public String getTipoJunta(String j_sigFuncao)  {
		if (j_sigFuncao==null) j_sigFuncao="";
	  	// Abrir Recurso - Defesa 
	  	if ("PNT0210".equals(j_sigFuncao)) return "0";
	  	// Abrir Recurso - Recurso 
	  	if ("PNT0220".equals(j_sigFuncao)) return "1";
	  	// Abrir Recurso - 2a Instancia 
	  	if ("PNT0230".equals(j_sigFuncao)) return "2";
		// Definir/Trocar Junta eRelator - 1a Instancia 
	  	if ("PNT0510".equals(j_sigFuncao)) return "1";
	  	// Definir/Trocar Junta eRelator - 2a Instancia 
	  	if ("PNT0610".equals(j_sigFuncao)) return "2";
	  	// Informa Resultado - Defesa 
	  	if ("PNT0420".equals(j_sigFuncao)) return "0";
	  	// Informa Resultado - 1a Instancia 
	  	if ("PNT0520".equals(j_sigFuncao)) return "1";
	  	// Informa Resultado - 2a Instancia 
	  	if ("PNT0620".equals(j_sigFuncao)) return "2";
	  	// Estornar Resultado - Defesa 
	  	if ("PNT0492".equals(j_sigFuncao)) return "0";
	  	// Estornar Resultado - 1a Instancia 
	  	if ("PNT0592".equals(j_sigFuncao)) return "1";
     	// Estornar Resultado - 2a Instancia 
	  	if ("PNT0692".equals(j_sigFuncao)) return "2";
	  	// Estornar Requerimento - Defesa 
	  	if ("PNT0494".equals(j_sigFuncao)) return "0";
	  	// Estornar Requerimento - 1a Instancia 
	  	if ("PNT0594".equals(j_sigFuncao)) return "1";
	  	// Estornar Requerimento - 2a Instancia 
	  	if ("PNT0694".equals(j_sigFuncao)) return "2";
	  	if ("PNT0710".equals(j_sigFuncao)) return "3";
	  	return "0";
	}
	public String getTipoJunta()  {
		return this.tipoJunta;
	}
	
	public String getStatusValidos()  {
		return this.statusValidos;
	}
	public String getTipoRequerimento()  {
		return this.tipoRequerimento;
	}
	public String getStatusDestino()  {
		return this.statusDestino;
	}
	public String getStatusRequerimento()  {
		return this.statusRequerimento;
	}
	public String getCodEvento()  {
		return this.codEvento;
	}	
	public String getCodOrigemEvento()  {
		return this.codOrigemEvento;
	}	
	public String getNomFuncao()  {
		return this.nomFuncao;
	}	
	public String getAbrirRequerimento()  {
		return this.abrirRequerimento;
	}

	public String getTipoAta() {
		return tipoAta;
	}

	public void setTipoAta(String tipoAta) {
		this.tipoAta = tipoAta;
	}	

	
}