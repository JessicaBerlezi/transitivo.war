package PNT.DEF;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.AtaDOBean;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class AtaDOXls {
	
	public void write(AtaDOBean AtaDOBeanId, String titulo, String arquivo,HttpServletRequest req) throws IOException, WriteException
	{   HttpSession session   = req.getSession() ;
	
	ACSS.ParamSistemaBean ParamSistemaBeanId = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
	if (ParamSistemaBeanId == null)	ParamSistemaBeanId = new ACSS.ParamSistemaBean();
	
	
	int iContador=1;
	int cont = 6;	
	int celulaTitulo = 0;
	
	try
	{
		boolean bImprime=true;
		if (AtaDOBeanId.getTipAtaDO().equals("4")) bImprime=false;
		
		
		WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
		WritableSheet sheet = workbook.createSheet("Ata DO", 0);
		WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
				WritableFont.DEFAULT_POINT_SIZE,
				WritableFont.BOLD,
				false,
				UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		
		WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
		WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
		formatoTituloCampos.setAlignment(Alignment.CENTRE);
		formatoTituloCampos.setBackground(Colour.GRAY_25);
		formatoTitulo.setAlignment(Alignment.CENTRE);
		
		celulaTitulo = 7/2;
		
		Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
		sheet.addCell(label);
		
		label = new Label(celulaTitulo, 1, ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_01"), formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 2, ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_02"), formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 3, ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_03"), formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 4, "       ", formatoTitulo);
		sheet.addCell(label);
		
		if (bImprime) {
			label = new Label(celulaTitulo, 5, AtaDOBeanId.getTipAtaEditalDODesc(), formatoTitulo);
			sheet.addCell(label);
		}
		
		boolean bExcesso=false;
		
		//T�tulo do Relat�rio				
		cont++;		
		
		if (bImprime) {
			for (int i=0;i<AtaDOBeanId.getProcessos().size();i++) 
			{		
				//Linhas	do Relat�rio				   
				label = new Label(0, cont, "Proc n� " + AtaDOBeanId.getProcessos().get(i).getNumProcesso()+ " -  " + 
						AtaDOBeanId.getProcessos().get(i).getNomResponsavel()+", CPF n� "+
						AtaDOBeanId.getProcessos().get(i).getNumCPF()+", "+AtaDOBeanId.getProcessos().get(i).getTipCNHDesc()+
						" n� "+AtaDOBeanId.getProcessos().get(i).getNumCNH()); 
				sheet.addCell(label);	   
				
				cont++;
				iContador++;					 
				if (iContador>3000)
				{
					bExcesso=true;
					break;
				}
			}
		}
		else
		{
			
			for (int i=0;i<AtaDOBeanId.getProcessos().size();i++) 
			{		
				//Linhas	do Relat�rio				   
				label = new Label(0, cont, AtaDOBeanId.getProcessos().get(i).getNumCPF()); 
				sheet.addCell(label);
				
				cont++;
			}
		}
		if (bExcesso)
		{
			label = new Label(0, cont, ""); 
			sheet.addCell(label);
			
			label = new Label(1, cont, "*** INCLU�DOS "+(iContador-1)+" linhas. ***"); 
			sheet.addCell(label);		
			
		}
		
		workbook.write(); 
		workbook.close();
		
		
	}catch (Exception e) {	
		throw new IOException("GeraXlsAtaDO: " + e.getMessage());	}
	
	}
	
}
