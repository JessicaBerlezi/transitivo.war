package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import PNT.AtaDOBean;
import sys.DaoException;
import sys.Util;


public class AtaEmiteCmdold extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/AtaEmite.jsp" ;  
	
	public AtaEmiteCmdold() {
		next = jspPadrao;
	}
	
	public AtaEmiteCmdold(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
			
			
			PNT.AtaDOBean AtaDOBeanId = new PNT.AtaDOBean();
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			AtaDOBeanId.setJ_sigFuncao(sSigFuncao);        	  
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			String sDescricao="ATA";
			if (AtaDOBeanId.getTipAtaDO().equals("4")) sDescricao="Arquivo"; 
			
			
			AtaDOBeanId.setColunaValue("COD_ENVIO_DO");
			AtaDOBeanId.setPopupNome("codEnvioDO");
			String sCmd = "ATA, SELECT DISTINCT A.COD_ENVIO_DO ,'"+sDescricao+": ' || A.COD_ENVIO_DO || '    ENVIADA: '  || TO_DATE(DAT_EDO,'DD/MM/YYYY') || '    PROCESSOS: ' || B.QTD_PROCESSOS  AS ATA "+ 
			"FROM TPNT_ATA_DO A "+
			"LEFT JOIN (SELECT COD_ENVIO_DO,COUNT(*) AS QTD_PROCESSOS FROM TPNT_ATA_DO GROUP BY COD_ENVIO_DO)  B ON  (A.COD_ENVIO_DO=B.COD_ENVIO_DO) "+
			"WHERE A.TIP_ATA='"+AtaDOBeanId.getTipAtaDO()+"' ";
			if ("PNT0451,PNT0461,PNT1101".indexOf(sSigFuncao)>=0)
				sCmd+="AND A.DAT_EMISSAO_EDO IS NULL ";
			if ("PNT0452,PNT0462,PNT1102".indexOf(sSigFuncao)>=0)
				sCmd+="AND A.DAT_EMISSAO_EDO IS NOT NULL AND A.DAT_PUBL_ATA IS NULL ";
			if ("PNT0456,PNT0466,PNT1106".indexOf(sSigFuncao)>=0)
				sCmd+="AND A.DAT_PUBL_ATA IS NOT NULL ";
			sCmd+="ORDER BY ATA";
			AtaDOBeanId.setPopupExt("onChange='javascript: mostraNumEdital(this.form);'");
			AtaDOBeanId.setPopupString(sCmd);
			
			if  (acao.equals("ImprimirAta"))  {
				String sCodEdital=req.getParameter("codEdital");
				if (sCodEdital==null) sCodEdital="";
				String sCodEnvioDO=req.getParameter("codEnvioDO");
				if (sCodEnvioDO==null) sCodEnvioDO="";
				
				AtaDOBeanId.setCodEnvioDO(sCodEnvioDO);
				if ("PNT0451,PNT0461,PNT1101".indexOf(sSigFuncao)>=0)
				{
					AtaDOBeanId.setCodEnvioDOedital(sCodEdital);
					AtaDOBeanId.setDatEmissaoEDO(Util.formatedToday().substring(0,10));
					AtaDOBeanId.setNomUserNameEmissaoEDO(UsrLogado.getNomUserName());
					AtaDOBeanId.setCodOrgaoLotacaoEmissaoEDO(UsrLogado.getOrgao().getCodOrgao());
					AtaDOBeanId.setDatProcEmissaoEDO(Util.formatedToday().substring(0,10));
					AtaDOBeanId.AtualizaEditalDO();
					AtaDOBeanId.setCodEnvioDO(sCodEdital);
				}
				AtaDOBeanId.LeAtas();
				setaParagrafosPNT(AtaDOBeanId,paramPnt);
				
				
				if (AtaDOBeanId.getTipAtaDO().equals("0")) /*Defesa*/
				{
					if (AtaDOBeanId.getTipProcessosDO().equals("S"))
						nextRetorno = "/PNT/DEF/AtaPublicaDO_SDDAut.jsp";					  
					else
						nextRetorno = "/PNT/DEF/AtaPublicaDO_PNTAut.jsp";					  
				}
				if (AtaDOBeanId.getTipAtaDO().equals("2")) /*Recurso*/
				{
					if (AtaDOBeanId.getTipProcessosDO().equals("S"))
						nextRetorno = "/PNT/DEF/AtaPublicaDO_SDDPen.jsp";					  
					else
						nextRetorno = "/PNT/DEF/AtaPublicaDO_PNTPen.jsp";					  
				}
				if (AtaDOBeanId.getTipAtaDO().equals("4")) /*Entrega*/
				{
					if (AtaDOBeanId.getTipProcessosDO().equals("S"))
						nextRetorno = "/PNT/DEF/AtaPublicaApreensaoSDD.jsp";
					else
						nextRetorno = "/PNT/DEF/AtaPublicaApreensaoPNT.jsp";					  
				}
			}
			
			if  (acao.equals("GeraArquivo"))  {	 		  	
				AtaDOBeanId.setCodEnvioDO(req.getParameter("codEnvioDO"));				  	
				AtaDOBeanId.LeAtas();
				setaParagrafosPNT(AtaDOBeanId,paramPnt);				
				
				if(req.getParameter("confirmaExclusao").equals("S"))
					AtaDOBeanId.setStatusArquivoDownload("-1");	
				
				if(!AtaDOBeanId.gravaArquivo((ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId"),ParamOrgaoId,UsrLogado)){
					req.setAttribute("confirmaExclusao","S");
				}	    		  	    		  	
			}		  
			
			session.setAttribute("AtaDOBeanId",AtaDOBeanId) ;  
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AtaEmiteCmd: " + ue.getMessage());
		}
	}
	
	public void setaParagrafosPNT (AtaDOBean myAta,ACSS.ParamSistemaBean paramPnt)
	throws DaoException {
		
		try {
			if (myAta.getTipAtaDO().equals("0")) /*Defesa*/
			{
				if (myAta.getTipProcessosDO().equals("S"))
				{
					myAta.setTxtParagrago_01(paramPnt.getParamSist("TXT_EDITAL_AUTUACAO_P1_SDD"));
					myAta.setTxtParagrago_02(paramPnt.getParamSist("TXT_EDITAL_AUTUACAO_P2_SDD"));
					
				}
				else
				{
					myAta.setTxtParagrago_01(paramPnt.getParamSist("TXT_EDITAL_AUTUACAO_P1_PNT"));
					myAta.setTxtParagrago_02(paramPnt.getParamSist("TXT_EDITAL_AUTUACAO_P2_PNT"));
					
				}
			}
			if (myAta.getTipAtaDO().equals("2")) /*Recurso*/
			{
				if (myAta.getTipProcessosDO().equals("S"))
				{
					myAta.setTxtParagrago_01(paramPnt.getParamSist("TXT_EDITAL_PENALIDADE_P1_SDD"));
					myAta.setTxtParagrago_02(paramPnt.getParamSist("TXT_EDITAL_PENALIDADE_P2_SDD"));
					
				}
				else
				{
					myAta.setTxtParagrago_01(paramPnt.getParamSist("TXT_EDITAL_PENALIDADE_P1_PNT"));
					myAta.setTxtParagrago_02(paramPnt.getParamSist("TXT_EDITAL_PENALIDADE_P2_PNT"));
					
				}
			}			
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}	
	
	
	
}
