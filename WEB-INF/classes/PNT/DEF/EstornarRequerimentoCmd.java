package PNT.DEF;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;

public class EstornarRequerimentoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/EstornarRequerimento.jsp" ;  
	
	public EstornarRequerimentoCmd() {
		next  =  jspPadrao;
	}
	
	public EstornarRequerimentoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
			
			UsuarioBean UsrLogado            = (UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado  = new UsuarioBean() ;	  			
			ParamSistemaBean parSis          = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis       = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 
			RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)   RequerimentoId = new RequerimentoBean() ;	 
			FuncionalidadesBean   FuncionalidadesId = new FuncionalidadesBean() ;
			FuncionalidadesId.setSigFuncao(req.getParameter("j_sigFuncao"));

			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
			if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if ("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				RequerimentoId = new RequerimentoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					ProcessoBeanId.LeRequerimentos();							
					RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);	
					if (RequerimentoId.getDatEST().length()==0)
						RequerimentoId.setDatEST(sys.Util.formatedToday().substring(0,10));					
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;									
					req.setAttribute("RequerimentoId",RequerimentoId);
					nextRetorno = next;					
				}				
			}
			if  (acao.equals("EstornarReq"))  {	
				RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);					
				nextRetorno = estornarReq(req,ProcessoBeanId,RequerimentoId,FuncionalidadesId,UsrLogado);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				req.setAttribute("RequerimentoId",RequerimentoId);
				req.setAttribute("TipoJunta",FuncionalidadesId.getTipoJunta(req.getParameter("j_sigFuncao")));					
			}
			if (acao.equals("R"))	{
				nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("EstornarRequerimentoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	public String estornarReq(HttpServletRequest req,ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId,UsuarioBean UsrLogado)
	throws sys.CommandException { 
	String nextRetorno = next ;
	try {			
		RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
		RequerimentoId.setDatEST(req.getParameter("datEST"));
		RequerimentoId.setTxtMotivoRS(req.getParameter("txtMotivoEST"));		
		Vector<String> vErro = new Vector<String>() ;
		myProcesso.setMsgErro("") ;
		//validar data 			
		if (RequerimentoId.getDatEST().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),RequerimentoId.getDatEST())>0)
			vErro.addElement("Data n�o pode ser superior a hoje. \n") ;

		RequerimentoId.setNomUserNameEST(UsrLogado.getNomUserName());
		RequerimentoId.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
		RequerimentoId.setDatProcEST(sys.Util.formatedToday().substring(0,10));
		if (vErro.size()>0) {myProcesso.setMsgErro(vErro) ;	}   
		else {	
			atualizarEstornarReq(myProcesso,RequerimentoId,FuncionalidadesId) ;
			nextRetorno= "/PNT/ProcessoAbre.jsp";
		}			
	}
	catch (Exception e){
		throw new sys.CommandException("estornarReq: " + e.getMessage());
	}
	return nextRetorno  ;
	}
	private void atualizarEstornarReq(ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId) throws sys.CommandException { 
		try { 	
			/*Setar Status Requerimento 02 - Definir Relator*/
			RequerimentoId.setCodStatusRequerimento(FuncionalidadesId.getStatusRequerimento()); 			
			// Atualizar Bean de Processo
			if (FuncionalidadesId.getStatusDestino().equals(myProcesso.getCodStatus())==false) { 
				myProcesso.setDatStatus(RequerimentoId.getDatEST()) ;				
				myProcesso.setCodStatus(FuncionalidadesId.getStatusDestino());			
			}
			myProcesso.setMsgErro("Estornado o Requerimento:"+RequerimentoId.getNumRequerimento()) ;
			myProcesso.setMsgOk("S");
			
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento(FuncionalidadesId.getCodOrigemEvento());			
			myHis.setCodEvento(FuncionalidadesId.getCodEvento());	
			myHis.setNomUserName(RequerimentoId.getNomUserNameEST());
			myHis.setCodOrgaoLotacao(RequerimentoId.getCodOrgaoLotacaoEST());
			myHis.setDatProc(RequerimentoId.getDatProcEST());
			myHis.setTxtComplemento01(RequerimentoId.getNumRequerimento());	
			myHis.setTxtComplemento02(RequerimentoId.getNomResultRS());
			myHis.setTxtComplemento03(RequerimentoId.getDatEST());	
			myHis.setTxtComplemento04(RequerimentoId.getTxtMotivoEST());			
			myHis.setTxtComplemento05(myProcesso.getCodPenalidade()); 
			myHis.setTxtComplemento06(myProcesso.getDscPenalidade()); 
			myProcesso.setDscPenalidade("");
			myProcesso.setCodPenalidade("");
			Dao dao = DaoFactory.getInstance();
			dao.estornarReq(myProcesso,RequerimentoId,myHis);
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(" atualizarEstornarReq "+ex.getMessage());
		}
		return ;
	} 		
}