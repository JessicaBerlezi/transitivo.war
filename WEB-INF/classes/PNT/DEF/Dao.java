package PNT.DEF;

import sys.DaoException;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.HistoricoBean;
import PNT.AtaDOBean;
public interface Dao {
	
			
	public abstract void DefinirRelator(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException;
	public abstract void informaResult(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException;	
	public abstract void estornarResult(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException;
	public abstract void estornarReq(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException;	
	public abstract void AtualizaDataPublicacaoDO(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException;
	public abstract void EstornaAta(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException;
	public abstract void EstornaDataPubAta(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException;	
}