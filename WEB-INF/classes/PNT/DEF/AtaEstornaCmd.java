package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import PNT.HistoricoBean;
import sys.Util;


public class AtaEstornaCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/AtaEstorna.jsp";  
	
	public AtaEstornaCmd() {
		next = jspPadrao;
	}
	
	public AtaEstornaCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession();								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
			
			
			PNT.AtaDOBean AtaDOBeanId = (PNT.AtaDOBean)session.getAttribute("AtaDOBeanId");
			if (AtaDOBeanId==null)  AtaDOBeanId = new PNT.AtaDOBean();
			AtaDOBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			AtaDOBeanId.setJ_sigFuncao(sSigFuncao);        	  
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			//AtaDOBeanId.setColunaValue("COD_ENVIO_DO");
			//AtaDOBeanId.setPopupNome("codEnvioDO");
			//String sCmd = "ATA, SELECT DISTINCT A.COD_ENVIO_DO ,'ATA: ' || A.COD_ENVIO_DO || '    ENVIADA: '  || TO_DATE(DAT_EDO,'DD/MM/YYYY') || '    EMITIDA: '  || TO_DATE(DAT_EMISSAO_EDO,'DD/MM/YYYY') || '    PROCESSOS: ' || B.QTD_PROCESSOS  AS ATA "+ 
			//"FROM TPNT_ATA_DO A "+
			//"LEFT JOIN (SELECT COD_ENVIO_DO,COUNT(*) AS QTD_PROCESSOS FROM TPNT_ATA_DO GROUP BY COD_ENVIO_DO)  B ON  (A.COD_ENVIO_DO=B.COD_ENVIO_DO) "+
			//"WHERE A.TIP_ATA='"+AtaDOBeanId.getTipAtaDO()+"' "+
			//"AND A.DAT_ESTORNO IS NULL AND A.DAT_PUBL_ATA IS NULL "+
			//"ORDER BY ATA";
			//AtaDOBeanId.setPopupString(sCmd);
						
			PNT.DaoBrokerAdabas DaoBrokerAdabas = new PNT.DaoBrokerAdabas();
			AtaDOBeanId.setEventos(DaoBrokerAdabas.ConsultaAtasDO(AtaDOBeanId)); 
			
			if  (acao.equals("EstornaAta"))  {
				String sCodEnvioDO=req.getParameter("codEnvioDO");
				if (sCodEnvioDO==null) sCodEnvioDO="";
				
				AtaDOBeanId.setCodEnvioDO(sCodEnvioDO);
				AtaDOBeanId.LeAtas();
				AtaDOBeanId.setDatEstorno(sys.Util.formatedToday().substring(0,10));
				AtaDOBeanId.setNomUserNameEST(UsrLogado.getNomUserName());
				AtaDOBeanId.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
				AtaDOBeanId.setDatProcEST(Util.formatedToday().substring(0,10));
				PNT.ProcessoBean myProc = new PNT.ProcessoBean();
				for (int i=0;i<AtaDOBeanId.getProcessos().size();i++)
				{
					AtaDOBeanId.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setNumCPF(AtaDOBeanId.getProcessos().get(i).getNumCPF());
					myProc.LeProcesso();
					/*Autua��o*/
					if (sSigFuncao.equals("PNT0453"))
					{
						if ("003".indexOf(myProc.getCodStatus())>=0)
						  myProc.setCodStatus("001");
					}
					/*Penalidade*/
					if (sSigFuncao.equals("PNT0463"))
					{
						if ("033".indexOf(myProc.getCodStatus())>=0)						
						  myProc.setCodStatus("031");
					}
					myProc.setDatStatus(sys.Util.formatedToday().substring(0,10));
					
					HistoricoBean myHis = new HistoricoBean();
					myHis.setCodProcesso(myProc.getCodProcesso());   
					myHis.setNumProcesso(myProc.getNumProcesso()); 
					myHis.setCodStatus(myProc.getCodStatus());  
					myHis.setDatStatus(myProc.getDatStatus());
					myHis.setCodOrigemEvento("101");
					if (sSigFuncao.equals("PNT0453"))
						myHis.setCodEvento("805");
					else
						myHis.setCodEvento("835");						
					myHis.setNomUserName(UsrLogado.getNomUserName());
					myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
					myHis.setTxtComplemento03(AtaDOBeanId.getDatPublAta());
					Dao dao = DaoFactory.getInstance();
					dao.EstornaAta(myProc,myHis,AtaDOBeanId);
					AtaDOBeanId.setMsgErro("Estorno Efetuado com sucesso. Edital n� "+AtaDOBeanId.getCodEnvioDO());
				}
			}
			session.setAttribute("AtaDOBeanId",AtaDOBeanId);  
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AtaEstornaCmd: " + ue.getMessage());
		}
	}
}
