package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import PNT.HistoricoBean;
import sys.Util;


public class AtaEstornaDataPubCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/AtaEstorna.jsp";  
	
	public AtaEstornaDataPubCmd() {
		next = jspPadrao;
	}
	
	public AtaEstornaDataPubCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession();								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
			
			
			PNT.AtaDOBean AtaDOBeanId = (PNT.AtaDOBean)session.getAttribute("AtaDOBeanId");
			if (AtaDOBeanId==null)  AtaDOBeanId = new PNT.AtaDOBean();
			AtaDOBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			AtaDOBeanId.setJ_sigFuncao(sSigFuncao);        	  
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if (sSigFuncao.equals("PNT0458"))
			{
				
				AtaDOBeanId.setTipAtaDO("0");
			}
			/*Penalidade*/
			if ( (sSigFuncao.equals("PNT0558")) || (sSigFuncao.equals("PNT0568"))
			   )
			{				
				AtaDOBeanId.setTipAtaDO("2");
			}
			AtaDOBeanId.setStatusAta("0");
			AtaDOBeanId.setStatusPublic("1");
			
			PNT.DaoBrokerAdabas DaoBrokerAdabas = new PNT.DaoBrokerAdabas();
			AtaDOBeanId.setEventos(DaoBrokerAdabas.ConsultaAtasDO(AtaDOBeanId)); 

			
			if  (acao.equals("EstornaAta"))  {
				String sCodEnvioDO=req.getParameter("codEnvioDO");
				if (sCodEnvioDO==null) sCodEnvioDO="";
				
				AtaDOBeanId.setCodEnvioDO(sCodEnvioDO);
				AtaDOBeanId.LeAtas();
				AtaDOBeanId.setDatEstorno(sys.Util.formatedToday().substring(0,10));
				AtaDOBeanId.setNomUserNameEST(UsrLogado.getNomUserName());
				AtaDOBeanId.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
				AtaDOBeanId.setDatProcEST(Util.formatedToday().substring(0,10));
				PNT.ProcessoBean myProc = new PNT.ProcessoBean();
				for (int i=0;i<AtaDOBeanId.getProcessos().size();i++)
				{
					AtaDOBeanId.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setNumProcesso(AtaDOBeanId.getProcessos().get(i).getNumProcesso());
					myProc.setNumCPF(AtaDOBeanId.getProcessos().get(i).getNumCPF());
					myProc.LeProcesso();
					/*Autua��o*/
					if (sSigFuncao.equals("PNT0458"))
					{
						if ("004".indexOf(myProc.getCodStatus())>=0)
						  myProc.setCodStatus("003");
					}
					/*Penalidade*/
					if ( (sSigFuncao.equals("PNT0558")) || (sSigFuncao.equals("PNT0568"))
					   )
					{
						if ("034".indexOf(myProc.getCodStatus())>=0)						
						  myProc.setCodStatus("033");
					}
					myProc.setDatStatus(sys.Util.formatedToday().substring(0,10));
					
					HistoricoBean myHis = new HistoricoBean();
					myHis.setCodProcesso(myProc.getCodProcesso());   
					myHis.setNumProcesso(myProc.getNumProcesso()); 
					myHis.setCodStatus(myProc.getCodStatus());  
					myHis.setDatStatus(myProc.getDatStatus());
					myHis.setCodOrigemEvento("101");
					if (sSigFuncao.equals("PNT0458"))
						myHis.setCodEvento("806");
					else
						myHis.setCodEvento("836");						
					myHis.setNomUserName(UsrLogado.getNomUserName());
					myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
					myHis.setTxtComplemento03(AtaDOBeanId.getDatPublAta());
					Dao dao = DaoFactory.getInstance();
					dao.EstornaDataPubAta(myProc,myHis,AtaDOBeanId);
					AtaDOBeanId.setMsgErro("Estorno Efetuado com sucesso. Edital n� "+AtaDOBeanId.getCodEnvioDO());
				}
			}
			session.setAttribute("AtaDOBeanId",AtaDOBeanId);  
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AtaEstornaDataPubCmd: " + ue.getMessage());
		}
	}
}
