package PNT.DEF;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sys.BeanException;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.StatusBean;

/**
 * <b>Title:</b> SMIT - Entrada de Recurso - Parecer Juridico Bean<br>
 * <b>Description:</b> Parecer Juridico Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * 
 * @version 1.0
 */

public class GuiaIncluirProcessoBean extends EventoBean {

	private String eventoOK;
	private String msg;
	private String comboReqs;
	private String comboGuias;
	private List<StatusBean> guias;
	private String codEvento;
	private String numReq;
	private String tipoJunta;

	public GuiaIncluirProcessoBean() throws sys.BeanException {
		super();
		eventoOK = "N";
		msg = "";
		comboReqs = "";
		comboGuias = "";
		codEvento = "811"; // INFORMAR JUNTA/RELATOR - DEFESA
		guias = new ArrayList<StatusBean>();
	}

	// --------------------------------------------------------------------------
	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "N";
		this.eventoOK = eventoOK;
	}

	public void setCodReq(String CodReq) {
		this.numReq = CodReq;
	}

	// --------------------------------------------------------------------------
	public void setMsg(String msg) {
		if (msg == null)
			msg = "";
		else
			this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	// --------------------------------------------------------------------------
	public String getNumReq() {
		return numReq;
	}

	// --------------------------------------------------------------------------
	public void setComboGuias(ProcessoBean myProc) throws sys.BeanException {
		this.comboGuias = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer
				.append("<select style='border-style: outset;' name='codLoteRelator' size='1'> \n");
		try {
			Iterator<StatusBean> it = guias.iterator();

			String nom = "";
			String cod = "";
			while (it.hasNext()) {

				StatusBean status = it.next();
				cod = status.getCodStatus();
				nom = status.getNomStatus();

				popupBuffer.append("<OPTION VALUE='" + cod + "'" + ">" + nom
						+ "</OPTION> \n");
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboGuias = popupBuffer.toString();
	}

	public String getComboGuias() {
		return (("S".equals(this.eventoOK)) ? this.comboGuias : this.msg);
	}

	public void setComboReqs(ProcessoBean myProc, String numReq)
			throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer
				.append(

				"<select style='border-style: outset;' name='codReq' size='1' onChange = 'javascript: valida(\"LeReq\",this.form)' > \n");
		try {
			Iterator<RequerimentoBean> it = myProc.getRequerimentos().iterator();
			RequerimentoBean req = new RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {
				req = (RequerimentoBean) it.next();
				if (req.getCodStatusRequerimento().equals("0")
						|| req.getCodStatusRequerimento().equals("1")
						|| req.getCodStatusRequerimento().equals("2")
						|| req.getCodStatusRequerimento().equals("3")
						|| req.getCodStatusRequerimento().equals("4")) {
					nom = req.getCodTipoSolic() + " "
							+ req.getNumRequerimento() + " ("
							+ req.getDatRequerimento() + ")";
					if (req.getNumRequerimento().equals(numReq))
						popupBuffer.append("<OPTION selected VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");
					else
						popupBuffer.append("<OPTION VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");

				}
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}

	public void setComboReqsManual(ProcessoBean myProcesso, String codReq)throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer
				.append(

				"<select style='border-style: outset;' name='codReq' size='1' onChange = 'javascript: valida(\"LeReq\",this.form)' > \n");
		try {
			Iterator<RequerimentoBean> it = myProcesso.getRequerimentos().iterator();
			RequerimentoBean req = new RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {
				req = it.next();
				if (req.getCodStatusRequerimento().equals("1")) {
					nom = req.getCodTipoSolic() + " "
							+ req.getNumRequerimento() + " ("
							+ req.getDatRequerimento() + ")";
					if (req.getNumRequerimento().equals(codReq))
						popupBuffer.append("<OPTION selected VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");
					else
						popupBuffer.append("<OPTION VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");

				}
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}
	
	public void setComboReqsManual(ProcessoBean myProcesso, String codReq, String tipoRequerimento)throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer
				.append(

				"<select style='border-style: outset;' name='codReq' size='1' onChange = 'javascript: valida(\"LeReq\",this.form)' > \n");
		try {
			Iterator<RequerimentoBean> it = myProcesso.getRequerimentos().iterator();
			RequerimentoBean req = new RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {
				req = it.next();
				if ((req.getCodStatusRequerimento().equals("1")) && (req.getCodTipoSolic().equals(tipoRequerimento))) {
					nom = req.getCodTipoSolic() + " "
							+ req.getNumRequerimento() + " ("
							+ req.getDatRequerimento() + ")";
					if (req.getNumRequerimento().equals(codReq))
						popupBuffer.append("<OPTION selected VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");
					else
						popupBuffer.append("<OPTION VALUE='"
								+ req.getNumRequerimento() + "'" + ">" + nom
								+ "</OPTION> \n");

				}
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}

	public String getComboReqs() {
		return (("S".equals(this.eventoOK)) ? this.comboReqs : "&nbsp;");
	}

	public void consultaGuiaIncluirProcesso(
			GuiaDistribuicaoBean GuiaDistribuicaoId, String statusGuia,
			String tipoJunta, String numCPF, String existeGuia,
			ACSS.UsuarioBean UsrLogado) throws BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			this.guias = dao.consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
					statusGuia, tipoJunta, numCPF, existeGuia, UsrLogado);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void incluirProcessoGuia(ProcessoBean myProcesso,
			String codLoteRelator, String codReq, ACSS.UsuarioBean UsrLogado,
			String existeGuia,FuncionalidadesBean funcionalidadesBean) throws BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.incluirProcessoGuia(myProcesso, codLoteRelator, codReq,
					codEvento, UsrLogado, existeGuia,funcionalidadesBean);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public String ConsultaReqGuia(ProcessoBean myProcesso) throws BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.ConsultaReqGuia(myProcesso);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void setTipoJunta(String tipoJunta) {
		this.tipoJunta = tipoJunta;
		if (tipoJunta == null)
			this.tipoJunta = "";
	}

	public String getTipoJunta() {
		return this.tipoJunta;
	}

	public void setEventoOK(GuiaDistribuicaoBean GuiaDistribuicaoId,
			ProcessoBean myProcesso, String sigFuncao, String numReq,
			UsuarioBean UsrLogado, FuncionalidadesBean funcionalidades)
			throws sys.BeanException {
		this.msg = "";
		this.eventoOK = "N";

		if ("".equals(myProcesso.getNomStatus()))
			return;

		this.msg = myProcesso.getNomStatus() + " (" + myProcesso.getDatStatus()+ ")";

		boolean existe = false;
		boolean temGuia = false;
		String existeGuia = "";

		// Incluir Processo / Preparar Guia Manual / Prepara Guia automatica
		if ((sigFuncao.equals("PNT0433")) || (sigFuncao.equals("PNT0431"))
				|| (sigFuncao.equals("PNT0439"))) {
			// Definir Relator DP
			this.codEvento = "811";

			
			// Defesa de Pontua��o aberto
			if (!myProcesso.getCodStatus().equals("010")) {

				this.msg += " - N�o existe requerimento para este Processo em status v�lido para inclu�-lo na guia.";
				return;
			}

			try {
				Iterator<RequerimentoBean> it = myProcesso.getRequerimentos().iterator();
				RequerimentoBean req = new RequerimentoBean();

				while (it.hasNext()) {
					req = it.next();

					 if ("".equals(numReq)){
						 temGuia = true;
						 existe = true;
						 this.msg = "";
						 break;
					 }

					if (numReq.equals(req.getNumRequerimento())) {

						existeGuia = ConsultaReqGuia(myProcesso);

						this.numReq = req.getNumRequerimento();

						if (existeGuia.length() > 0) {
							this.msg = "Requerimento incluido na Guia " + existeGuia;
						}

						if (req.getCodStatusRequerimento().equals("0")
								|| req.getCodStatusRequerimento().equals("1")
								|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"2", "0", "", existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"3", "0", req.getCodRelatorJU(),existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;

						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"4", "0", req.getCodRelatorJU(),existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
					}
				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: "
						+ ue.getMessage());
			}
		}

		if ((sigFuncao.equals("PNT0533")) || (sigFuncao.equals("PNT0531"))
				|| (sigFuncao.equals("PNT0539"))) {
			this.codEvento = "841";

			// Defesa de Pontua��o aberto
			if (!myProcesso.getCodStatus().equals("040")) {

				this.msg += " - N�o existe requerimento para este Processo em status v�lido para inclu�-lo na guia.";
				return;
			}
			try {
				Iterator<RequerimentoBean> it = myProcesso.getRequerimentos()
						.iterator();
				RequerimentoBean req = new RequerimentoBean();

				while (it.hasNext()) {
					req = it.next();

					 if ("".equals(numReq)){
						 temGuia = true;
						 existe = true;
						 this.msg = "";
						 break;
					 }

					if (numReq.equals(req.getNumRequerimento())) {

						if (req.getCodStatusRequerimento().equals("0")
								|| req.getCodStatusRequerimento().equals("1")
								|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
										"2", "1", "", existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
									"3", "1", req.getCodRelatorJU(),
									existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;

						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
									"4", "1", req.getCodRelatorJU(),
									existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: "
						+ ue.getMessage());
			}
		}

		
		
		
		
		if ((sigFuncao.equals("PNT0633")) || (sigFuncao.equals("PNT0631"))
				|| (sigFuncao.equals("PNT0639"))) {
			this.codEvento = "871";

			// Defesa de Pontua��o aberto
			if (!myProcesso.getCodStatus().equals("070")) {

				this.msg += " - N�o existe requerimento para este Processo em status v�lido para inclu�-lo na guia.";
				return;
			}
			try {
				Iterator<RequerimentoBean> it = myProcesso.getRequerimentos()
						.iterator();
				RequerimentoBean req = new RequerimentoBean();

				while (it.hasNext()) {
					req = it.next();

					 if ("".equals(numReq)){
						 temGuia = true;
						 existe = true;
						 this.msg = "";
						 break;
					 }

					if (numReq.equals(req.getNumRequerimento())) {

						if (req.getCodStatusRequerimento().equals("0")
								|| req.getCodStatusRequerimento().equals("1")
								|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
										"2", "1", "", existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
									"3", "1", req.getCodRelatorJU(),
									existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;

						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,
									"4", "1", req.getCodRelatorJU(),
									existeGuia, UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: "
						+ ue.getMessage());
			}
		}
		
		
		
		
		// Aguardar valida��o do CETRAN
		/*
		 * if ( (sigFuncao.equals("REC0438")) || (sigFuncao.equals("REC0436"))
		 * || (sigFuncao.equals("REC0450"))) { this.codEvento="335" ; if
		 * (!myProcesso.getCodStatus().equals("35") &&
		 * !myProcesso.getCodStatus().equals("36")) {
		 * 
		 * this.msg +=
		 * " - N�o existe requerimento para este processo em status v�lido para inclu�-lo na guia."
		 * ; return; } try { Iterator<RequerimentoBean> it =
		 * myProcesso.getRequerimentos().iterator(); RequerimentoBean req = new
		 * RequerimentoBean();
		 * 
		 * while (it.hasNext()) { req = it.next(); if ("".equals(codReq)){
		 * temGuia = true; existe = true; this.msg = ""; break; }
		 * 
		 * if (codReq.equals(req.getCodRequerimento())){
		 * 
		 * if (req.getCodStatusRequerimento().equals("0") ||
		 * req.getCodStatusRequerimento().equals("1") ||
		 * req.getCodStatusRequerimento().equals("2")) { existe = true;
		 * consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"2", "2", "",
		 * existeGuia,UsrLogado); if (!guias.isEmpty()) { temGuia = true; }
		 * 
		 * break; } if (req.getCodStatusRequerimento().equals("3")) { existe =
		 * true; consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"3", "2",
		 * req.getCodRelatorJU(), existeGuia,UsrLogado); if (!guias.isEmpty()) {
		 * temGuia = true; } break;
		 * 
		 * } if (req.getCodStatusRequerimento().equals("4")) { existe = true;
		 * consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"4",
		 * "2",req.getCodRelatorJU(), existeGuia,UsrLogado); if
		 * (!guias.isEmpty()) { temGuia = true; } break; } }
		 * 
		 * }
		 * 
		 * } catch (Exception ue) { throw new
		 * sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage()); } }
		 */

		if (existe && temGuia)
			this.eventoOK = "S";
		else if (existe && !temGuia)
			this.msg += " - N�o existe guia dispon�vel para incluir novos processos.";
		else
			this.msg += " - N�o existe requerimento para este Processo em status v�lido para inclu�-lo na guia.";

		setMsg(this.msg);
	}
}
