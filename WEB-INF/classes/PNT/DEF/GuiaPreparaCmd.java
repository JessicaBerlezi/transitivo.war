package PNT.DEF;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;
import ACSS.UsuarioFuncBean;
import PNT.ProcessoBean;
import PNT.TAB.JuntaBean;
import PNT.TAB.RelatorBean;
import REC.ParamOrgBean;

public class GuiaPreparaCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/PNT/DEF/GuiaPrepara.jsp";

	public GuiaPreparaCmd() {
		next = jspPadrao;
	}

	public GuiaPreparaCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {

			HttpSession session = req.getSession();

			ParamOrgBean ParamOrgaoId = (ParamOrgBean) session.getAttribute("ParamOrgBeanId");
			if (ParamOrgaoId == null) ParamOrgaoId = new ParamOrgBean();

			UsuarioFuncBean UsuarioFuncBeanId = (UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (UsuarioFuncBeanId == null) UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();

			FuncionalidadesBean funcionalidades = new FuncionalidadesBean();
			funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
			
//			String sPreparaGuia = ParamOrgaoId.getParamOrgao("PREPARA_GUIA","O", "7");
			String sPreparaGuia = "M";
			String numMaxProc = ParamOrgaoId.getParamOrgao("NUM_PROCESSOS_GUIA_WEB", "99999", "7");

			if (sPreparaGuia.equals("R")) { //Guia de Distribui��o � partir de Guia de Remessa
				
				nextRetorno = "/PNT/DEF/GuiaPreparaRemessaManual.jsp";
				session.setAttribute("UsuarioFuncBeanId", UsuarioFuncBeanId);
				sys.Command cmd = (sys.Command) Class.forName("PNT.DEF.GuiaPreparaRemessaManualCmd").newInstance();
				cmd.setNext(this.next);
				nextRetorno = cmd.execute(req);
				return nextRetorno;
				
			} else if (sPreparaGuia.equals("M")) { /* Guia de Remessa Manual */
				nextRetorno = "/PNT/DEF/GuiaPreparaManual.jsp";
				session.setAttribute("UsuarioFuncBeanId", UsuarioFuncBeanId);
				sys.Command cmd = (sys.Command) Class.forName("PNT.DEF.GuiaPreparaManualCmd").newInstance();
				cmd.setNext(this.next);
				nextRetorno = cmd.execute(req);
				return nextRetorno;
				
			} else {
				UsuarioBean UsrLogado = (UsuarioBean) session.getAttribute("UsuarioBeanId");
				if (UsrLogado == null)
					UsrLogado = new ACSS.UsuarioBean();
				GuiaDistribuicaoBean GuiaDistribuicaoId = (GuiaDistribuicaoBean) session.getAttribute("GuiaDistribuicaoId");
				if (GuiaDistribuicaoId == null)
					GuiaDistribuicaoId = new GuiaDistribuicaoBean();
				
				funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
				
				GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
				GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());

				String acao = req.getParameter("acao");

				if (acao == null) {
					session.removeAttribute("GuiaDistribuicaoId");
					acao = " ";
				} else {
					if ("Classifica,Voltar,MostraProcesso,imprimirInf,AtualizaGuia,ImprimirGuia,MostraAI".indexOf(acao) < 0) {
						sys.Command cmd = (sys.Command) Class.forName("PNT.ProcessaProcessoCmd").newInstance();
						cmd.setNext(this.next);
						nextRetorno = cmd.execute(req);
					}
				}

				if ((acao.equals(" ")) || (acao.equals("Novo"))) {
					GuiaDistribuicaoId = new GuiaDistribuicaoBean();
					//GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"), ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO", "N", "7"));
					GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
					GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
					GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO", "N", "2"));

					String datInicioRemessa = ParamOrgaoId.getParamOrgao("DAT_INICIO_REMESSA", "", "10");

					GuiaDistribuicaoId.PreparaGuia(UsrLogado, datInicioRemessa,numMaxProc);
					if (GuiaDistribuicaoId.getProcessos().size() == 0)
						req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO PARA ENVIAR AOS RELATORES");
				}
				if (acao.equals("Classifica"))
					GuiaDistribuicaoId.Classifica(req.getParameter("ordem"));
				if (acao.equals("AtualizaGuia")) {
					String numSessao = req.getParameter("numSessao");
					if (numSessao == null)
						numSessao = "";
					GuiaDistribuicaoId.setNumSessao(numSessao);
					int inc = atualizaGuia(req, GuiaDistribuicaoId, UsrLogado, funcionalidades);

					if (inc > 0) {
						GuiaDistribuicaoId.LeGuia();
						GuiaDistribuicaoId.setMsgErro("Incluidos " + inc + " Processos/Requerimentos para Guia "+ GuiaDistribuicaoId.getNumGuia());
						nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp";
					} else {
						GuiaDistribuicaoId.setMsgErro(GuiaDistribuicaoId.getMsgErro()+ " \n Nenhum Processos/Requerimentos incluido ");
						nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp";
					}
				}

				if (acao.equals("ImprimirGuia")) {
					GuiaDistribuicaoId.LeGuia();
					if (GuiaDistribuicaoId.getProcessos().size() == 0)
						req.setAttribute("semProcesso","N�O EXISTEM PROCESSOS SELECIONADOS");
					
					nextRetorno = "/PNT/DEF/GuiaPreparaImp.jsp";
				}

				if (acao.equals("MostraProcesso")) {
					ProcessoBean processoBeanId = new ProcessoBean();

					//FIXME: Verificar se existe duas formas de consultar um processo se a op��o for "MostraProcesso"
					if (acao.equals("MostraProcesso")) {
						processoBeanId.setNumProcesso(req.getParameter("mostranumprocesso"));
						nextRetorno = "/PNT/DEF/AbrirConsultaProcesso.jsp";
					} else {
						processoBeanId.setNumProcesso(req.getParameter("numProcesso"));
						nextRetorno = "/PNT/DEF/ProcessoImp.jsp";
					}

					// Verifica se o Usuario logado ve Todos os Orgaos
					String temp = UsrLogado.getCodOrgaoAtuacao();
					if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg()) >= 0)
						UsrLogado.setCodOrgaoAtuacao("999999");
					processoBeanId.LeProcesso();
					processoBeanId.LeRequerimentos();
					UsrLogado.setCodOrgaoAtuacao(temp);
					req.setAttribute("ProcessoBeanId", processoBeanId);
				}
				
				//FIXME: N�o existe Processo digitalizado
				/*if (acao.equals("MostraAI")) {
					ProcessoBean processoBeanId = new ProcessoBean();
					processoBeanId.setNumProcesso(req.getParameter("numProcesso"));
					processoBeanId.setNumCPF(req.getParameter("numCPF"));

					//processoBeanId.LeAIDigitalizado();

					req.setAttribute("AutoInfracaoBeanId", processoBeanId);
					nextRetorno = "/PNT/DEF/VisAIDig.jsp";

				}*/
				
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaPreparaCmd: " + ue.getMessage());
		}
		// System.err.println("Fim Classifica Prepara Guia - inicio jsp "+nextRetorno+": "+sys.Util.formatedToday());

		return nextRetorno;

	}

	public int atualizaGuia(HttpServletRequest req, GuiaDistribuicaoBean GuiaDistribuicaoId, 
			ACSS.UsuarioBean UsrLogado, FuncionalidadesBean funcionalidades)throws sys.CommandException {
		
		int inc = 0;
		try {
			GuiaDistribuicaoId.setMsgErro("");
			Vector<String> vErro = new Vector<String>();
			GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
			JuntaBean myJunta = new JuntaBean();
			myJunta.Le_Junta(GuiaDistribuicaoId.getCodJunta(), 0);
			GuiaDistribuicaoId.setSigJunta(myJunta.getSigJunta());

			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
			RelatorBean myRelator = new RelatorBean();
			myRelator.setNumCpf(GuiaDistribuicaoId.getNumCPFRelator());
			myRelator.setCodJunta(GuiaDistribuicaoId.getCodJunta());
			myRelator.Le_Relator("CPF");
			GuiaDistribuicaoId.setNomRelator(myRelator.getNomRelator());
			GuiaDistribuicaoId.setCodRelator(myRelator.getCodRelator());

			if ((GuiaDistribuicaoId.getCodJunta().length() == 0)
					|| (GuiaDistribuicaoId.getNumCPFRelator().length() == 0))
				vErro.addElement("Junta/Relator n�o selecionado. \n");
			
			// buscar os processos selecionados
			String procSelec[] = req.getParameterValues("Selecionado");
			int n = 0;
			
			ArrayList<ProcessoBean> processosSelec = new ArrayList<ProcessoBean>();
			if (procSelec != null) {
				for (int i = 0; i < procSelec.length; i++) {
					n = Integer.parseInt(procSelec[i]);
					if ((n >= 0) && (n < GuiaDistribuicaoId.getProcessos().size())) {
						processosSelec.add( GuiaDistribuicaoId.getProcessos().get(n));
					}
				}
				
				GuiaDistribuicaoId.setProcessos(processosSelec);
				
			} else
				vErro.addElement("Nenhum processo selecionado.");

			if ((vErro.size() == 0) && (GuiaDistribuicaoId.getProcessos().size() == 0))
				vErro.addElement("Nenhum processo selecionado.");

			if (vErro.size() > 0)
				GuiaDistribuicaoId.setMsgErro(vErro);
			else {
				GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
				if (GuiaDistribuicaoId.getNumGuia().length() > 0) {
					vErro.addElement("Processos j� cadastrados em outra guia /ou Aguarde o processamento dos dados .");
					GuiaDistribuicaoId.setMsgErro(vErro);
				} else
					//FIXME: Verificar o DAO Guia Grava 
					inc = dao.GuiaGrava(GuiaDistribuicaoId, UsrLogado, funcionalidades);
			}
		} catch (Exception ue) {
			throw new sys.CommandException("GuiaPreparaCmd: " + ue.getMessage());
		}
		return inc;
	}
}
