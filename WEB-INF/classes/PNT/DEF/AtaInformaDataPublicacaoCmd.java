package PNT.DEF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import PNT.HistoricoBean;
import sys.Util;


public class AtaInformaDataPublicacaoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/AtaInformaDataPublicacao.jsp" ;  
	
	public AtaInformaDataPublicacaoCmd() {
		next = jspPadrao;
	}
	
	public AtaInformaDataPublicacaoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
			
			
			PNT.AtaDOBean AtaDOBeanId = (PNT.AtaDOBean)session.getAttribute("AtaDOBeanId");
			if (AtaDOBeanId==null)  AtaDOBeanId = new PNT.AtaDOBean();
			AtaDOBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			AtaDOBeanId.setJ_sigFuncao(sSigFuncao);        	  
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if (sSigFuncao.equals("PNT0454"))
			{
				
				AtaDOBeanId.setTipAtaDO("0");
			}
			/*Penalidade*/
			if (sSigFuncao.equals("PNT0464"))
			{				
				AtaDOBeanId.setTipAtaDO("2");
			}
			/*Apreens�o*/
			if (sSigFuncao.equals("PNT1104")) 
			{				
				AtaDOBeanId.setTipAtaDO("4");
			}
			
			AtaDOBeanId.setStatusAta("0");
						
//			Lista de Atas=====
			List <PNT.AtaDOBean> listaAtas = new ArrayList <PNT.AtaDOBean>();			
			listaAtas=AtaDOBeanId.consultaAtas(AtaDOBeanId);			
			
//			AtaDOBeanId.setColunaValue("COD_ENVIO_DO");
//			AtaDOBeanId.setPopupNome("codEnvioDO");
//			String sCmd = "ATA, SELECT DISTINCT A.COD_ENVIO_DO ,'ATA: ' || A.COD_ENVIO_DO || '    ENVIADA: '  || TO_DATE(DAT_EDO,'DD/MM/YYYY') || '    EMITIDA: '  || TO_DATE(DAT_EMISSAO_EDO,'DD/MM/YYYY') || '    PROCESSOS: ' || B.QTD_PROCESSOS  AS ATA "+ 
//			"FROM TPNT_ATA_DO A "+
//			"LEFT JOIN (SELECT COD_ENVIO_DO,COUNT(*) AS QTD_PROCESSOS FROM TPNT_ATA_DO GROUP BY COD_ENVIO_DO)  B ON  (A.COD_ENVIO_DO=B.COD_ENVIO_DO) "+
//			"WHERE A.TIP_ATA='"+AtaDOBeanId.getTipAtaDO()+"' "+
//			"AND A.DAT_EMISSAO_EDO IS NOT NULL AND A.DAT_PUBL_ATA IS NULL "+
//			"ORDER BY ATA";
//			AtaDOBeanId.setPopupString(sCmd);
			
			if  (acao.equals("InformaDataPublicacao"))  {
				String sCodEnvioDO=req.getParameter("codEnvioDO");
				if (sCodEnvioDO==null) sCodEnvioDO="";
				
				String dataPublicacao = req.getParameter("dataPublicacao");
				if (dataPublicacao==null) dataPublicacao=Util.addData(sys.Util.formatedToday().substring(0, 10),-60);
				
				AtaDOBeanId.setCodEnvioDO(sCodEnvioDO);
				AtaDOBeanId.LeAtas();
				AtaDOBeanId.setDatPublAta(dataPublicacao);
				AtaDOBeanId.setNomUserNamePDO(UsrLogado.getNomUserName());
				AtaDOBeanId.setCodOrgaoLotacaoPDO(UsrLogado.getOrgao().getCodOrgao());
				AtaDOBeanId.setDatProcPDO(Util.formatedToday().substring(0,10));
				PNT.ProcessoBean myProc = new PNT.ProcessoBean();
				for (int i=0;i<AtaDOBeanId.getProcessos().size();i++)
				{
					AtaDOBeanId.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setCodProcesso(AtaDOBeanId.getProcessos().get(i).getCodProcesso());
					myProc.setNumProcesso(AtaDOBeanId.getProcessos().get(i).getNumProcesso());					
					myProc.setNumCPF(AtaDOBeanId.getProcessos().get(i).getNumCPF());
					myProc.LeProcesso();
					/*Autua��o*/
					if (sSigFuncao.equals("PNT0454"))
					{
						if ("003".indexOf(myProc.getCodStatus())>=0)
						  myProc.setCodStatus("004");
					}
					/*Penalidade*/
					if (sSigFuncao.equals("PNT0464"))
					{
						if ("033".indexOf(myProc.getCodStatus())>=0)						
						  myProc.setCodStatus("034");
					}
					/*Apreens�o*/
					if (sSigFuncao.equals("PNT1104")) 
					{
						if ("093".indexOf(myProc.getCodStatus())>=0)						
						  myProc.setCodStatus("094");
					}
					
					myProc.setDatStatus(sys.Util.formatedToday().substring(0,10));
					
					HistoricoBean myHis = new HistoricoBean();
					myHis.setCodProcesso(myProc.getCodProcesso());   
					myHis.setNumProcesso(myProc.getNumProcesso()); 
					myHis.setCodStatus(myProc.getCodStatus());  
					myHis.setDatStatus(myProc.getDatStatus());
					myHis.setCodOrigemEvento("101");
					if (sSigFuncao.equals("PNT0454")) /*Autua��o*/
						myHis.setCodEvento("804");
					else if (sSigFuncao.equals("PNT0464")) /*Penalidade*/
						myHis.setCodEvento("834");
					else
						myHis.setCodEvento("894"); /*Apreens�o*/
						
					myHis.setNomUserName(UsrLogado.getNomUserName());
					myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
					myHis.setTxtComplemento03(AtaDOBeanId.getDatPublAta());
					
					Dao dao = DaoFactory.getInstance();
					dao.AtualizaDataPublicacaoDO(myProc,myHis,AtaDOBeanId);	
					AtaDOBeanId.setMsgErro("Opera��o realizada com Sucesso");					
				}
			}
			session.setAttribute("AtaDOBeanId",AtaDOBeanId) ; 
			req.setAttribute("listaAtas",listaAtas);
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AtaInformaDataPublicacaoCmd: " + ue.getMessage());
		}
	}
}
