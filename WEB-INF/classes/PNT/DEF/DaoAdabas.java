package PNT.DEF;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.AtaDOBean;

import sys.DaoException;
import sys.Util;

/**
 * <b>Title:</b>       	SMIT - PNT.PTC.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados nas funcionalidades do Protocolo<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoAdabas extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoAdabas() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public void DefinirRelator(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException {		
		String resultado ="";
		String parteVar = "";
			
		try 
		
		{	UsuarioBean myUsuario= new UsuarioBean();	
		
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.rPad(myReq.getNumRequerimento()," ",20) +
			sys.Util.lPad(myReq.getCodJuntaJU(),"0",6) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatJU()),"0",8)+			
			sys.Util.rPad(myReq.getSigJuntaJU()," ",10) +
			sys.Util.lPad(myReq.getTipoJunta(),"0",1) +
			sys.Util.lPad(myReq.getCpfRelatorJU(),"0",11) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - DefinirRelator: "+e.getMessage());
		}		
	}
	public void informaResult(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException {		
		String resultado ="";
		String parteVar = "";
			
		try 
		
		{	UsuarioBean myUsuario= new UsuarioBean();	
		
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.rPad(myReq.getNumRequerimento()," ",20) +
			sys.Util.lPad(myReq.getCodResultRS(),"0",1) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatRS()),"0",8)+			
			sys.Util.rPad(myReq.getTxtMotivoRS()," ",1000) +
			sys.Util.lPad(myProcesso.getCodPenalidade(),"0",3) +
			sys.Util.rPad(myProcesso.getDscPenalidade()," ",100) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - informaResult: "+e.getMessage());
		}
	}
	
	public void estornarResult(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException {		
		String resultado ="";
		String parteVar = "";
			
		try 
		
		{	UsuarioBean myUsuario= new UsuarioBean();	
		
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.rPad(myReq.getNumRequerimento()," ",20) +
			sys.Util.lPad("D","0",1) +
			//sys.Util.lPad(myReq.getCodResultRS(),"0",1) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatEST()),"0",8)+			
			sys.Util.rPad("ESTORNO DE RESULTADO"," ",1000) +
			//sys.Util.rPad(myReq.getTxtMotivo()," ",1000) +
			sys.Util.lPad("0","0",3) +
			//sys.Util.lPad(myProcesso.getCodPenalidade(),"0",3) +
			sys.Util.rPad(" "," ",100);
			//sys.Util.rPad(myProcesso.getDscPenalidade()," ",100) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - informaResult: "+e.getMessage());
		}
	}
	
	public void estornarReq(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis) throws DaoException {		
		String resultado ="";
		String parteVar = "";
		Connection conn = null ;
		try 
		{	
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			UsuarioBean myUsuario= new UsuarioBean();	
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.rPad(myReq.getNumRequerimento()," ",20) +
			sys.Util.lPad(myReq.getCodResultRS(),"0",1) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatEST()),"0",8)+			
			sys.Util.rPad("ESTORNO REQUERIMENTO"," ",1000) +
			sys.Util.lPad(myProcesso.getCodPenalidade(),"0",3) +
			sys.Util.rPad(myProcesso.getDscPenalidade()," ",100) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			conn.commit();
			return  ;
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - estornarReq: "+e.getMessage());
		}
		
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
	}

	public void AtualizaDataPublicacaoDO(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException {		
		String resultado ="";
		String parteVar = "";
		try 
		{	
			UsuarioBean myUsuario= new UsuarioBean();	
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  +  
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) + 
			sys.Util.rPad(myHis.getNomUserName()," ",20) + 
			sys.Util.lPad(myHis.getCodEvento(),"0",3) + 
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +  
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) + 
			sys.Util.lPad(myProcesso.getNumNotificacao(),"0",9) + 
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(AtaDOBeanId.getDatPublAta()),"0",8);
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}		
		catch (Exception e) {
			throw new DaoException("AtualizaDataPublicacaoDO "+e.getMessage());
		}		
	}
	
	public void EstornaAta(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException {		
		String resultado ="";
		String parteVar = "";
		Connection conn = null ;
			
		try 
		
		{	
			
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			
			UsuarioBean myUsuario= new UsuarioBean();	
		    //myHis.GravaHistorico(conn);	
		
			parteVar =  sys.Util.lPad(sys.Util.formataDataYYYYMMDD(AtaDOBeanId.getDatEstorno()),"0",8)+	
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumNotificacao(),"0",9) +
			sys.Util.rPad(AtaDOBeanId.getTipAtaDO()," ",1) +
			sys.Util.rPad(AtaDOBeanId.getCodEnvioDO()," ",20);
			
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			conn.commit();
			return  ;
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - EstornaAta: "+e.getMessage());
		}
		
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
	}
	
	public void EstornaDataPubAta(ProcessoBean myProcesso,HistoricoBean myHis,AtaDOBean AtaDOBeanId) throws DaoException {		
		String resultado ="";
		String parteVar = "";
		Connection conn = null ;
			
		try 
		
		{	
			
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			
			UsuarioBean myUsuario= new UsuarioBean();	
		    //myHis.GravaHistorico(conn);	
		
			parteVar =  sys.Util.lPad(sys.Util.formataDataYYYYMMDD(AtaDOBeanId.getDatEstorno()),"0",8)+	
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumNotificacao(),"0",9) +
			sys.Util.rPad(AtaDOBeanId.getTipAtaDO()," ",1) +
			sys.Util.rPad(AtaDOBeanId.getCodEnvioDO()," ",20);
			
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			conn.commit();
			return  ;
			
		}		
		catch (Exception e) {
			throw new DaoException("DaoAdabas - EstornaAta: "+e.getMessage());
		}
		
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
	}
	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myProc,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk    = false ;
		 String codErro = resultado.substring(0,3);
		 try {
			 if ((codErro.equals("000")==false) &&
					 (codErro.equals("037")==false)) {
				 if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					 myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 codErro=Util.lPad(codErro.trim(),"0",3);
					 myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					 ResultSet rs   = stmt.executeQuery(sCmd) ;
					 while (rs.next()) {
						 myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					 }
					 rs.close();
				 }
			 }
			 else   bOk = true ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
	
}