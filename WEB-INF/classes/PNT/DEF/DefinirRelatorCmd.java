package PNT.DEF;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.RelatorBean;
import PNT.DEF.Dao;
import PNT.DEF.DaoFactory;

public class DefinirRelatorCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/DefinirRelator.jsp" ;  
		
	public DefinirRelatorCmd() {
		next  =  jspPadrao;
	}
	
	public DefinirRelatorCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
    
			UsuarioBean UsrLogado            = (UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado  = new UsuarioBean() ;	  			
			ParamSistemaBean parSis          = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis       = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 
			RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)   RequerimentoId = new RequerimentoBean() ;	 
			FuncionalidadesBean   FuncionalidadesId = new FuncionalidadesBean() ;
			FuncionalidadesId.setSigFuncao(req.getParameter("j_sigFuncao"));
			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
		    if ((acao==null) || ("".equals(acao)))	{
			  ProcessoBeanId       = new ProcessoBean() ;	  		 
			  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			  acao = "";
			}				
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if ("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				RequerimentoId = new RequerimentoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					ProcessoBeanId.LeRequerimentos();							
					RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);				
					if (RequerimentoId.getDatJU().length()==0)
						RequerimentoId.setDatJU(sys.Util.formatedToday().substring(0,10));
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;									
					req.setAttribute("RequerimentoId",RequerimentoId);
					req.setAttribute("TipoJunta",FuncionalidadesId.getTipoJunta(req.getParameter("j_sigFuncao")));
					nextRetorno = next;					
				}				
			}
			if  (acao.equals("DefineRelator"))  {	
				RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);				
				nextRetorno = defineRelator(req,ProcessoBeanId,RequerimentoId,FuncionalidadesId,UsrLogado);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				req.setAttribute("RequerimentoId",RequerimentoId);
				req.setAttribute("TipoJunta",FuncionalidadesId.getTipoJunta(req.getParameter("j_sigFuncao")));					
			}
			if (acao.equals("R"))	{
			    nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("DefinirRelatorCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	public String defineRelator(HttpServletRequest req,ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId,UsuarioBean UsrLogado)
		throws sys.CommandException { 
		String nextRetorno = next ;
		try {			
			RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
			RequerimentoId.setDatJU(req.getParameter("datJU"));
			int codRelator =0;	
			if (req.getParameter("codRelatorJU")!=null && req.getParameter("codRelatorJU")!="")
				 codRelator = Integer.parseInt(req.getParameter("codRelatorJU"));					
			RequerimentoId.setCodRelatorJU(String.valueOf(codRelator));							
			Vector<String> vErro = new Vector<String>() ;
			myProcesso.setMsgErro("") ;
			//validar data 			
			if (RequerimentoId.getDatJU().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),RequerimentoId.getDatJU())>0)
				vErro.addElement("Data n�o pode ser superior a hoje. \n") ;
			if (sys.Util.DifereDias( RequerimentoId.getDatRequerimento(),RequerimentoId.getDatJU())<0)  
				vErro.addElement("Data n�o pode ser inferior a data de abertura do requerimento. \n") ;
			if (RequerimentoId.getCodRelatorJU().length()==0) 
				vErro.addElement("Junta/Relator n�o selecionado. \n") ;
			RequerimentoId.setNomUserNameJU(UsrLogado.getNomUserName());
			RequerimentoId.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
			RequerimentoId.setDatProcJU(sys.Util.formatedToday().substring(0,10));			
			if (vErro.size()>0) {myProcesso.setMsgErro(vErro) ;	}   
			else {	
				atualizarRelator(myProcesso,RequerimentoId,FuncionalidadesId,req) ;
				//nextRetorno= "/PNT/ProcessoAbre.jsp";
			}			
		}
		catch (Exception e){
			throw new sys.CommandException("DefinirRelatorCmd: " + e.getMessage());
		}
		return nextRetorno  ;
	}
	private void atualizarRelator(ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId,HttpServletRequest req) throws sys.CommandException { 
		try { 	
			myProcesso.setMsgErro("");
			
			/*Setar Status Requerimento 02 - Definir Relator*/
			RequerimentoId.setCodStatusRequerimento(FuncionalidadesId.getStatusRequerimento()); 			
			RelatorBean myRelator = new RelatorBean();
			myRelator.setCodRelator(RequerimentoId.getCodRelatorJU());
			myRelator.Le_Relator("codigo");
			RequerimentoId.setNomRelatorJU(myRelator.getNomRelator());
			RequerimentoId.setCpfRelatorJU(myRelator.getNumCpf());
			RequerimentoId.setSigJuntaJU(myRelator.getSigJunta());
			RequerimentoId.setCodJuntaJU(myRelator.getCodJunta());
			RequerimentoId.setTipoJunta(FuncionalidadesId.getTipoJunta(req.getParameter("j_sigFuncao")));

			
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento(FuncionalidadesId.getCodOrigemEvento());			
			myHis.setCodEvento(FuncionalidadesId.getCodEvento());	
			myHis.setNomUserName(RequerimentoId.getNomUserNameJU());
			myHis.setCodOrgaoLotacao(RequerimentoId.getCodOrgaoLotacaoJU());
			myHis.setDatProc(RequerimentoId.getDatProcJU());
			myHis.setTxtComplemento01(RequerimentoId.getNumRequerimento());	
			myHis.setTxtComplemento02(RequerimentoId.getCodJuntaJU());
			myHis.setTxtComplemento03(RequerimentoId.getDatJU());
			myHis.setTxtComplemento04(RequerimentoId.getSigJuntaJU());
			myHis.setTxtComplemento05(RequerimentoId.getCodRelatorJU());
			myHis.setTxtComplemento06(RequerimentoId.getCpfRelatorJU());
			Dao dao = DaoFactory.getInstance();
			dao.DefinirRelator(myProcesso,RequerimentoId,myHis) ;		 
			
			if (myProcesso.getMsgErro().trim().length() > 0){
				myProcesso.setMsgOk("N") ;
			}else{
				// Atualizar Bean de Processo
				if (FuncionalidadesId.getStatusDestino().equals(myProcesso.getCodStatus())==false) myProcesso.setDatStatus(RequerimentoId.getDatJU()) ;				
				myProcesso.setCodStatus(FuncionalidadesId.getStatusDestino());			
				myProcesso.setMsgErro("Junta/Relator informado para o Requerimento:"+RequerimentoId.getNumRequerimento()) ;
				myProcesso.setMsgOk("S") ;
				myProcesso.setEventoOK("N");
			}
			
			
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		return ;
	}
	
}