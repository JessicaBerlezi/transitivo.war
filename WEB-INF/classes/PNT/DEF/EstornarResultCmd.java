package PNT.DEF;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;

public class EstornarResultCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/EstornarResult.jsp" ;  
	
	public EstornarResultCmd() {
		next  =  jspPadrao;
	}
	
	public EstornarResultCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
			
			UsuarioBean UsrLogado            = (UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado  = new UsuarioBean() ;	  			
			ParamSistemaBean parSis          = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis       = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 
			RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)   RequerimentoId = new RequerimentoBean() ;	 
			FuncionalidadesBean   FuncionalidadesId = new FuncionalidadesBean() ;
			FuncionalidadesId.setSigFuncao(req.getParameter("j_sigFuncao"));

			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
			if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if ("".equals(acao)){
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			}
			
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				RequerimentoId = new RequerimentoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				
				if (ProcessoBeanId==null){
					ProcessoBeanId = new ProcessoBean() ;
				}
				
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					ProcessoBeanId.LeRequerimentos();							
					RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);	
					if (RequerimentoId.getDatEST().length()==0)
						RequerimentoId.setDatEST(sys.Util.formatedToday().substring(0,10));					
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;									
					req.setAttribute("RequerimentoId",RequerimentoId);
					nextRetorno = next;					
				}				
			}
			if  (acao.equals("EstornarResult"))  {	
				RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);					
				nextRetorno = estornarResult(req,ProcessoBeanId,RequerimentoId,FuncionalidadesId,UsrLogado);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				req.setAttribute("RequerimentoId",RequerimentoId);					
			}
			if (acao.equals("R"))	{
				nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("EstornarResultCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	public String estornarResult(HttpServletRequest req,ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId,UsuarioBean UsrLogado)
	throws sys.CommandException { 
	String nextRetorno = next ;
	try {			
		RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
		RequerimentoId.setDatEST(req.getParameter("datEST"));
		Vector<String> vErro = new Vector<String>() ;
		myProcesso.setMsgErro("") ;
		//validar data 			
		if (RequerimentoId.getDatEST().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),RequerimentoId.getDatEST())>0)
			vErro.addElement("Data n�o pode ser superior a hoje. \n") ;
//		if (sys.Util.DifereDias( RequerimentoId.getDatRS(),RequerimentoId.getDatEST())<0)  
//			vErro.addElement("Data n�o pode ser inferior a data de abertura do resultado. \n") ;

		RequerimentoId.setNomUserNameEST(UsrLogado.getNomUserName());
		RequerimentoId.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
		RequerimentoId.setDatProcEST(sys.Util.formatedToday().substring(0,10));
		if (vErro.size()>0) {myProcesso.setMsgErro(vErro) ;	}   
		else {	
			atualizarEstornarResult(myProcesso,RequerimentoId,FuncionalidadesId) ;
			nextRetorno= "/PNT/ProcessoAbre.jsp";
		}			
	}
	catch (Exception e){
		throw new sys.CommandException("estornarResult: " + e.getMessage());
	}
	return nextRetorno  ;
	}
	private void atualizarEstornarResult(ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId) throws sys.CommandException { 
		try { 	
			/*Setar Status Requerimento 02 - Definir Relator*/
			RequerimentoId.setCodStatusRequerimento(FuncionalidadesId.getStatusRequerimento()); 			
			// Atualizar Bean de Processo
			if (FuncionalidadesId.getStatusDestino().equals(myProcesso.getCodStatus())==false) { 
				myProcesso.setDatStatus(RequerimentoId.getDatEST()) ;				
				myProcesso.setCodStatus(FuncionalidadesId.getStatusDestino());			
			}
			myProcesso.setMsgErro("Resultado estornado para o Requerimento:"+RequerimentoId.getNumRequerimento()) ;
			myProcesso.setMsgOk("S") ;	
			
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento(FuncionalidadesId.getCodOrigemEvento());			
			myHis.setCodEvento(FuncionalidadesId.getCodEvento());	
			myHis.setNomUserName(RequerimentoId.getNomUserNameEST());
			myHis.setCodOrgaoLotacao(RequerimentoId.getCodOrgaoLotacaoEST());
			myHis.setDatProc(RequerimentoId.getDatProcEST());
			myHis.setTxtComplemento01(RequerimentoId.getNumRequerimento());	
			myHis.setTxtComplemento02(RequerimentoId.getNomResultRS());
			myHis.setTxtComplemento03(RequerimentoId.getDatEST());	
			myHis.setTxtComplemento04(RequerimentoId.getTxtMotivoRS());			
			myHis.setTxtComplemento05(myProcesso.getCodPenalidade()); 
			myHis.setTxtComplemento06(myProcesso.getDscPenalidade()); 
			myProcesso.setDscPenalidade("");
			myProcesso.setCodPenalidade("");
			Dao dao = DaoFactory.getInstance();
			dao.estornarResult(myProcesso,RequerimentoId,myHis) ;		    			
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(" atualizarEstornarResult "+ex.getMessage());
		}
		return ;
	} 		
}