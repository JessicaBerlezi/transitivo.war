package PNT.DEF;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.ProcessoBean;
import PNT.TAB.JuntaBean;
import PNT.TAB.RelatorBean;
import REC.ParamOrgBean;


public class GuiaTrocaRelatorCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="PNT/DEF/GuiaReemite.jsp" ;  
   
   public GuiaTrocaRelatorCmd() {
      next = jspPadrao;
   }

   public GuiaTrocaRelatorCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          
          if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
		  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  
		  if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
      	  GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
      		//Carrego os Beans
		  //GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		  FuncionalidadesBean funcionalidades = new FuncionalidadesBean();
		  funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
		  session.setAttribute("funcionalidadesBean", funcionalidades);
		  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
		  GuiaDistribuicaoId.setMsgErro("");
		  
		  String dataIni = req.getParameter("De");
		  if (dataIni == null) dataIni = "";
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  if (dataFim == null) dataFim = "";
		  GuiaDistribuicaoId.setDataFim(dataFim);
		  
		  String acao    = req.getParameter("acao");  
		  if (acao==null)	{
			GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			//GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");			
			
			// MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia
			//FIXME: Verificar se existe necessidade de tal verifica��o
			//GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	acao = " ";
		  } else {
			  if ("Classifica,processoMostra,imprimirInf,ImprimirGuia".indexOf(acao)<0)	{
				  sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
				  cmd.setNext(this.next) ;
				  nextRetorno = cmd.execute(req);
			  }		
		  }
		  if (acao.equals("LeGuia")) {
		    GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;		
		    //GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
		    GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		    GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());						
			GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
			GuiaDistribuicaoId.LeGuia();
		    if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO") ;
			nextRetorno = "/PNT/DEF/GuiaTrocaRelator.jsp";
		  }	        
          if (acao.equals("Classifica")) {  
          		GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;
          		nextRetorno = "/PNT/DEF/GuiaTrocaRelator.jsp";
          }
		  if  (acao.equals("ImprimirGuia"))  {	      	  
		  	if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","NENHUM PROCESSO LOCALIZADO");				  	    
		  	nextRetorno = "/PNT/DEF/GuiaPreparaImp.jsp" ;      	  
		  }
      	  if (acao.equals("MostraProcesso"))  {
        	ProcessoBean ProcessoBeanId = new ProcessoBean() ;	  			  			
			if (acao.equals("MostraProcesso")) { 
				ProcessoBeanId.setNumProcesso(req.getParameter("mostranumprocesso"));
				nextRetorno = "/PNT/DEF/AbrirConsultaProcesso.jsp" ;
			}
			else {
				ProcessoBeanId.setNumProcesso(req.getParameter("mostranumprocesso"));			
				nextRetorno = "/PNT/DEF/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			ProcessoBeanId.LeProcesso();
			UsrLogado.setCodOrgaoAtuacao(temp);	
			req.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
      	  }	
		  if (acao.equals("TrocaRelator")) {  
		    TrocaRelator(req,GuiaDistribuicaoId,UsrLogado,funcionalidades) ;
			nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp" ; 				
		  }
          // processamento de saida dos formularios
          if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
      	  else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaTrocaRelatorCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }

   public void TrocaRelator(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado,FuncionalidadesBean funcionabilidades) throws sys.CommandException {
		try {	
	 		GuiaDistribuicaoId.setMsgErro("") ;
	 		Vector vErro = new Vector() ;
	 		GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
	 		JuntaBean myJunta = new JuntaBean();
	 		myJunta.Le_Junta(GuiaDistribuicaoId.getCodJunta(),0);
	 		GuiaDistribuicaoId.setSigJunta(myJunta.getSigJunta());			

			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
	 		RelatorBean myRelator = new RelatorBean();
	 		myRelator.setNumCpf(GuiaDistribuicaoId.getNumCPFRelator());
	 		myRelator.setCodJunta(GuiaDistribuicaoId.getCodJunta());
	 		myRelator.Le_Relator("CPF");			
	 		GuiaDistribuicaoId.setNomRelator(myRelator.getNomRelator());			
	 		GuiaDistribuicaoId.setCodRelator(myRelator.getCodRelator());
		
			 if ( (GuiaDistribuicaoId.getCodJunta().length()==0) ||
				  (GuiaDistribuicaoId.getNumCPFRelator().length()==0)) vErro.addElement("Relator n�o selecionado. \n") ;
			 if (vErro.size()>0) GuiaDistribuicaoId.setMsgErro(vErro);
			 else{	 
				 GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
				 dao.GuiaGravaTrocaRelator(GuiaDistribuicaoId,funcionabilidades) ; 
	 		}		
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaTrocaRelatorCmd: " + ue.getMessage());
	   }
	   return ;
   } 	

}