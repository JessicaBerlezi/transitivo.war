package PNT.DEF;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

/**
 * <b>Title:</b>        Download de arquivos GRV e REJ<br>
 * <b>Description:</b>  Comando para Download de arquivos<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author 			   Elisson Dias	
 * @version 1.0
 */

public class AtaDownloadCmd extends sys.Command {
	
	private static final String jspPadrao="/PNT/DEF/AtaDownload.jsp";    
	private String next;
	
	public AtaDownloadCmd() {
		next = jspPadrao;
	}
	
	public AtaDownloadCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
			
			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
			
			String acao = req.getParameter("acao");
			if(acao == null) acao = "";

			List vetArquivos = null;
			
			String statusInteracao = "1";
			
			String dataIni = req.getParameter("De");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			if(dataIni == null) dataIni = "01/01/"+Util.formatedToday().substring(6,10);

			String dataFim = req.getParameter("Ate");	
			if(dataFim == null) dataFim = df.format(new Date());
			
			PNT.AtaDOBean AtaDOBeanId = new PNT.AtaDOBean();
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			AtaDOBeanId.setJ_sigFuncao(sSigFuncao);        	  
			AtaDOBeanId.setNomUserNameDownload(usrLogado.getNomUserName());
			AtaDOBeanId.setCodOrgaoAtuacaoDownload(usrLogado.getCodOrgaoAtuacao());
			AtaDOBeanId.setCodOrgaoLotacaoDownload(usrLogado.getOrgao().getCodOrgao());
			
			if(acao.equals("consultaArq")){											
				try {
					vetArquivos = AtaDOBeanId.ConsultaAtaDownload(dataIni,dataFim);
				}  
				catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}	
				statusInteracao = "2";
			}
			if(acao.equals("registra")){
				try {
					AtaDOBeanId.setCodArquivoDownload(req.getParameter("codArquivo"));
					AtaDOBeanId.DownloadArquivoAta();
					vetArquivos = AtaDOBeanId.ConsultaAtaDownload(dataIni,dataFim);
				}  
				catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}	
				statusInteracao = "2";
			}
			req.setAttribute("ArquivoAtaId",AtaDOBeanId);
			req.setAttribute("msgErro",msgErro);						
			req.setAttribute("vetArquivos",vetArquivos);
			req.setAttribute("dataIni",dataIni);
			req.setAttribute("dataFim",dataFim);
			req.setAttribute("statusInteracao",statusInteracao);
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}
