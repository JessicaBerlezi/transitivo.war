package PNT.DEF;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import PNT.AtaDOBean;

public class GeraXlsAtaDO extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
		{	
			String jspPadrao = "/PNT/DEF/AtaEmite.jsp" ; 
			String JSPDIR   = "/WEB-INF/jsp/";
			
			
			try {
				HttpSession session   = req.getSession() ;
				AtaDOBean AtaDOBeanId = (AtaDOBean) req.getSession().getAttribute("AtaDOBeanId");
				if( AtaDOBeanId == null ) AtaDOBeanId = new AtaDOBean();
				
				ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
				if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
				
				REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
				if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
				
				ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
				if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
				
				String acao = req.getParameter("acao");
				if( acao == null ) acao = "";             
				
				String contexto = req.getParameter("contexto");
				if( contexto == null ) contexto = "";
				
				String nomArquivo = req.getParameter("nomArquivo");
				if( nomArquivo == null ) nomArquivo = "";
				
				if ( acao.equals("") ) {				
					req.setAttribute("AtaDOBeanId", AtaDOBeanId);
				}
				else if(acao.equals("GeraExcel")) 
				{
					String sCodEdital=req.getParameter("codEdital");
					if (sCodEdital==null) sCodEdital="";
					String sCodEnvioDO=req.getParameter("codEnvioDO");
					if (sCodEnvioDO==null) sCodEnvioDO="";				
					
					AtaDOBeanId.setCodEnvioDO(sCodEnvioDO);
					AtaDOBeanId.LeAtas();			  
					
					AtaDOXls geraxls = new AtaDOXls();
					String tituloConsulta = "ATA DE PONTUA��O";
					ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
					paramSys.setCodSistema("21"); // M�dulo PNT
					paramSys.PreparaParam();
					String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
					geraxls.write(AtaDOBeanId, tituloConsulta, arquivo,req);	
					req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
					rd.forward(req, resp);	
				}
				else {					  
					session.setAttribute("AtaDOBeanId",AtaDOBeanId);  
					RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + jspPadrao);
					rd.forward(req, resp);
				}
				
			}   catch (Exception e) {
				throw new ServletException(e.getMessage());            
			} 
		}
	}
}
