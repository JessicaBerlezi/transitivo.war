package PNT.DEF;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.PenalidadeBean;

public class InformarResultCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/DEF/InformarResult.jsp" ;  
	
	public InformarResultCmd() {
		next  =  jspPadrao;
	}
	
	public InformarResultCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
			
			UsuarioBean UsrLogado            = (UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado  = new UsuarioBean() ;	  			
			ParamSistemaBean parSis          = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis       = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 
			RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)   RequerimentoId = new RequerimentoBean() ;	 
			FuncionalidadesBean   FuncionalidadesId = new FuncionalidadesBean() ;
			FuncionalidadesId.setSigFuncao(req.getParameter("j_sigFuncao"));

			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
			if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if ("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				RequerimentoId = new RequerimentoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					ProcessoBeanId.LeRequerimentos();							
					RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);				
					if (RequerimentoId.getDatRS().length()==0)
						RequerimentoId.setDatRS(sys.Util.formatedToday().substring(0,10));
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;									
					req.setAttribute("RequerimentoId",RequerimentoId);
					nextRetorno = next;					
				}				
			}
			if  (acao.equals("InformaResult"))  {	
				RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);					
				nextRetorno = informaResult(req,ProcessoBeanId,RequerimentoId,FuncionalidadesId,UsrLogado);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				req.setAttribute("RequerimentoId",RequerimentoId);					
			}
			if (acao.equals("R"))	{
				nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("InformarResultCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	public String informaResult(HttpServletRequest req,ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId,UsuarioBean UsrLogado)
	throws sys.CommandException { 
	String nextRetorno = next ;
	try {			
		RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
		RequerimentoId.setDatRS(req.getParameter("datRS"));
		RequerimentoId.setCodResultRS(req.getParameter("codResultRS"));
		RequerimentoId.setTxtMotivoRS(req.getParameter("txtMotivoRS"));
		myProcesso.setCodPenalidade(req.getParameter("COD_PENALIDADE"));		
		Vector<String> vErro = new Vector<String>() ;
		myProcesso.setMsgErro("") ;
		//validar data 			
/*
		if (RequerimentoId.getDatRS().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),RequerimentoId.getDatRS())>0)
			vErro.addElement("Data n�o pode ser superior a hoje. \n") ;
		if (sys.Util.DifereDias( RequerimentoId.getDatRequerimento(),RequerimentoId.getDatRS())<0)  
			vErro.addElement("Data n�o pode ser inferior a data de abertura do requerimento. \n") ;
		if (sys.Util.DifereDias( RequerimentoId.getDatJU(),RequerimentoId.getDatRS())<0)  
			vErro.addElement("Data n�o pode ser inferior a data de Defini��o do Relator. \n") ;
		if ("DI".indexOf(RequerimentoId.getCodResultRS())<0)
			vErro.addElement("Resultado: D-Deferido ou I-Indeferido. \n") ;		
		if ( ("D".equals(RequerimentoId.getCodResultRS())==false) && (myProcesso.getCodPenalidade().length()==0) ) 
			vErro.addElement("Penalidade n�o definida. \n") ;
*/
		RequerimentoId.setNomUserNameRS(UsrLogado.getNomUserName());
		RequerimentoId.setCodOrgaoLotacaoRS(UsrLogado.getOrgao().getCodOrgao());
		RequerimentoId.setDatProcRS(sys.Util.formatedToday().substring(0,10));
		if (vErro.size()>0) {myProcesso.setMsgErro(vErro) ;	}   
		else {	
			atualizarResult(myProcesso,RequerimentoId,FuncionalidadesId) ;
			nextRetorno= "/PNT/ProcessoAbre.jsp";
		}			
	}
	catch (Exception e){
		throw new sys.CommandException("informaResult: " + e.getMessage());
	}
	return nextRetorno  ;
	}
	private void atualizarResult(ProcessoBean myProcesso,RequerimentoBean RequerimentoId,FuncionalidadesBean FuncionalidadesId) throws sys.CommandException { 
		try { 	
			/*Setar Status Requerimento 02 - Definir Relator*/
			RequerimentoId.setCodStatusRequerimento(FuncionalidadesId.getStatusRequerimento()); 			
			PenalidadeBean myPenalidade = new PenalidadeBean();
			myPenalidade.setCodPenalidade(myProcesso.getCodPenalidade());
			myPenalidade.Le_Penalidade("codigo");
			myProcesso.setDscPenalidade(myPenalidade.getDscPenalidade());
			myProcesso.setDscPenalidadeRes(myPenalidade.getDscResumida());
			// Atualizar Bean de Processo
			if (FuncionalidadesId.getStatusDestino().equals(myProcesso.getCodStatus())==false) { 
				myProcesso.setDatStatus(RequerimentoId.getDatRS()) ;				
				myProcesso.setCodStatus(FuncionalidadesId.getStatusDestino());			
			}
			myProcesso.setMsgErro("Resultado informado para o Requerimento:"+RequerimentoId.getNumRequerimento()) ;
			myProcesso.setMsgOk("S") ;	
			
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento(FuncionalidadesId.getCodOrigemEvento());			
			myHis.setCodEvento(FuncionalidadesId.getCodEvento());	
			myHis.setNomUserName(RequerimentoId.getNomUserNameRS());
			myHis.setCodOrgaoLotacao(RequerimentoId.getCodOrgaoLotacaoRS());
			myHis.setDatProc(RequerimentoId.getDatProcRS());
			myHis.setTxtComplemento01(RequerimentoId.getNumRequerimento());	
			myHis.setTxtComplemento02(RequerimentoId.getNomResultRS());
			myHis.setTxtComplemento03(RequerimentoId.getDatRS());	
			myHis.setTxtComplemento04(RequerimentoId.getTxtMotivoRS());			
			myHis.setTxtComplemento05(myProcesso.getCodPenalidade()); 
			myHis.setTxtComplemento06(myProcesso.getDscPenalidade()); 
			Dao dao = DaoFactory.getInstance();
			dao.informaResult(myProcesso,RequerimentoId,myHis) ;		    			
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(" atualizarResult "+ex.getMessage());
		}
		return ;
	} 		
}