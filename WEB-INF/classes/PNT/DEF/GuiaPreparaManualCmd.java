package PNT.DEF;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.JuntaBean;
import PNT.TAB.RelatorBean;

public class GuiaPreparaManualCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/PNT/DEF/GuiaPreparaManual.jsp" ;  
   
   public GuiaPreparaManualCmd() {
      next = jspPadrao;
   }

   public GuiaPreparaManualCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	  String nextRetorno = next ;
      try {
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          
          if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
		  REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;

		  if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
		  
      	  GuiaDistribuicaoBean GuiaDistribuicaoId                   = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
      	  
		  FuncionalidadesBean funcionalidades                  = (FuncionalidadesBean)session.getAttribute("FuncionalidadeBeanId") ;
      	  if (funcionalidades==null)  funcionalidades         = new FuncionalidadesBean() ;
      	  
      	  funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
		  
		  //Carrego os Beans
		  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
		  GuiaDistribuicaoId.setMsgErro("");
		  
		  ProcessoBean ProcessoManualBeanId = (ProcessoBean) req.getAttribute("ProcessoManualBeanId");
		  if (ProcessoManualBeanId == null) ProcessoManualBeanId = new ProcessoBean();

		  RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
		  if (RequerimentoId == null) RequerimentoId = new RequerimentoBean();
		  
		  GuiaIncluirProcessoBean GuiaIncluirProcessoBeanId = (GuiaIncluirProcessoBean) req.getAttribute("GuiaIncluirProcessoBeanId");
		  if (GuiaIncluirProcessoBeanId == null) GuiaIncluirProcessoBeanId = new GuiaIncluirProcessoBean();
		  
		  
		  String acao    = req.getParameter("acao");
		  String sNumProcesso = req.getParameter("numProcesso");
		  if (sNumProcesso==null) sNumProcesso="";
		  String sNumSessao = req.getParameter("numSessao");
		  if (sNumSessao == null) sNumSessao = "";
		  session.setAttribute("codReq", "");
		  
		  String sigFuncao = UsuarioFuncBeanId.getJ_sigFuncao();
		  
		  if (acao==null)	{
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	acao = " ";
		  }else {
				if ("Classifica,processoMostra,imprimirInf,ImprimirGuia".indexOf(acao)>0)	{
					sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
					cmd.setNext(this.next) ;
					nextRetorno = cmd.execute(req);
				}		
		  }
		  if (acao.equals("processoLe")) {
 			ProcessoManualBeanId.setNumProcesso(sNumProcesso);
			GuiaDistribuicaoId.setNumSessao(sNumSessao);
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			ProcessoManualBeanId.LeProcesso();
			ProcessoManualBeanId.LeRequerimentos();
			UsrLogado.setCodOrgaoAtuacao(temp);	
			
			GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId, ProcessoManualBeanId, sigFuncao, "", UsrLogado, funcionalidades);
			
			GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));

			ProcessoManualBeanId.setNumRequerimento(GuiaIncluirProcessoBeanId.getNumReq());

			session.setAttribute("codReq", GuiaIncluirProcessoBeanId.getNumReq());
			req.setAttribute("ProcessoManualBeanId", ProcessoManualBeanId);			
	      }
		  
		  
		  if ( (acao.equals(" ")) || (acao.equals("Novo")) ){

			  GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
			  GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
		  }
		  
          if (acao.equals("Classifica"))    
        	  GuiaDistribuicaoId.Classifica(req.getParameter("ordem"));
          
      	  if (acao.equals("AtualizaGuia")) {
      		  String numSessao = req.getParameter("numSessao");
      	  	
      		  if (numSessao == null) 
      			  numSessao = "";
      	    
      		  GuiaDistribuicaoId.setNumSessao(numSessao);
      	    
			
      		  int inc = atualizaGuia(req,GuiaDistribuicaoId,UsrLogado, funcionalidades) ;
		  	if (inc>0) {
                GuiaDistribuicaoId.LeGuia();
		  		GuiaDistribuicaoId.setMsgErro("Incluidos "+inc+" Processos/Requerimentos para Guia "+GuiaDistribuicaoId.getNumGuia()); 			
				nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp" ;
      	  	}
			else {
				GuiaDistribuicaoId.setMsgErro(GuiaDistribuicaoId.getMsgErro()+" \n Nenhum Processos/Requerimentos incluido ");
				nextRetorno = "/PNT/DEF/GuiaDistribuicaoMostra.jsp" ; 
			}				
      	  }
		  if  (acao.equals("ImprimirGuia"))  {	
              GuiaDistribuicaoId.setOrdem("Processo");
		  	if (GuiaDistribuicaoId.getProcessos().size()==0) req.setAttribute("semProcesso","N�O EXISTEM PROCESSOS SELECIONADOS");				  	    
		  	nextRetorno = "/PNT/DEF/GuiaPreparaImp.jsp" ;      	  
		  }
		  
	
	  /**
	   * COMENTANDO TODA A PARTE DE AI
	   
	  if (acao.equals("MostraAI"))  {			
		  AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	  			  			
		  myAuto.setNumPlaca(req.getParameter("mostraplaca")); 
		  myAuto.setNumAutoInfracao(req.getParameter("mostranumauto"));
			
		  myAuto.LeAIDigitalizado();
			
		  req.setAttribute("AutoInfracaoBeanId",myAuto) ;
		  nextRetorno = "/REC/VisAIDig.jsp";
		
		  }
		*/
		if (acao.equals("LeReq")) {
			String codReq = req.getParameter("codReq");
			ProcessoManualBeanId.setNumProcesso(sNumProcesso);
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			ProcessoManualBeanId.LeProcesso();
			ProcessoManualBeanId.LeRequerimentos();
			UsrLogado.setCodOrgaoAtuacao(temp);	
			ProcessoManualBeanId.setNumRequerimento(ProcessoManualBeanId.getRequerimentos(codReq,null).getNumRequerimento());
			ProcessoManualBeanId.setTipoRequerimento(ProcessoManualBeanId.getRequerimentos(codReq,null).getCodTipoSolic());
			String sExiste=GuiaDaoBroker.getInstance().ConsultaReqGuia(ProcessoManualBeanId);
			if (sExiste.length()>0)
			{
				GuiaIncluirProcessoBeanId.setMsg(GuiaDistribuicaoId.getMsgErro()+" \n Processo j� Cadastrado em outra Guia.");
			    nextRetorno = "/PNT/DEF/GuiaPreparaManual.jsp" ;
			}
			else
			{ 
			  ArrayList<ProcessoBean> processos=new ArrayList<ProcessoBean>();			
			  Iterator<ProcessoBean> it = GuiaDistribuicaoId.getProcessos().iterator();
			  ProcessoBean myProcesso = new ProcessoBean();			
			  boolean bExiste=false;
			  while (it.hasNext())
			  {
				myProcesso = it.next();
				processos.add(myProcesso);
				if (myProcesso.getNumRequerimento().equals(ProcessoManualBeanId.getNumRequerimento()))
				  bExiste=true;
			  }
			  if (bExiste)
			  {
				  GuiaIncluirProcessoBeanId.setMsg(GuiaDistribuicaoId.getMsgErro()+" \n Processo j� Cadastrado para esta Guia.");
				nextRetorno = "/PNT/DEF/GuiaPreparaManual.jsp" ;
			  }
			  else
			    processos.add(ProcessoManualBeanId);

			  GuiaDistribuicaoId.setProcessos(processos);
			  GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
			  GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
			  ProcessoManualBeanId = new ProcessoBean();			
			  session.setAttribute("codReq", "");
			  req.setAttribute("ProcessoManualBeanId", ProcessoManualBeanId);
			}			
		}	

		
		req.setAttribute("GuiaIncluirProcessoBeanId", GuiaIncluirProcessoBeanId);
		
          if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
      	  else{ 				    
      		  session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      		  session.setAttribute("FuncionalidadeBeanId", funcionalidades);
      	  }
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaPreparaManualCmd: " + ue.getMessage());
      }
//System.err.println("Fim Classifica Prepara Guia - inicio jsp "+nextRetorno+": "+sys.Util.formatedToday());      
      return nextRetorno;
   }

   public int atualizaGuia(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado, FuncionalidadesBean funcionalidades) throws sys.CommandException {
	   int inc = 0 ;
	   try {	
	   	
			GuiaDistribuicaoId.setMsgErro("") ;
			Vector<String> vErro = new Vector<String>() ;
			GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
			JuntaBean myJunta = new JuntaBean();
			myJunta.Le_Junta(GuiaDistribuicaoId.getCodJunta(),0);
			GuiaDistribuicaoId.setSigJunta(myJunta.getSigJunta());			

			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
			RelatorBean myRelator = new RelatorBean();
			myRelator.setNumCpf(GuiaDistribuicaoId.getNumCPFRelator());
			myRelator.setCodJunta(GuiaDistribuicaoId.getCodJunta());
			myRelator.Le_Relator("CPF");			
			GuiaDistribuicaoId.setNomRelator(myRelator.getNomRelator());			
			GuiaDistribuicaoId.setCodRelator(myRelator.getCodRelator());
		
			if ( (GuiaDistribuicaoId.getCodJunta().length()==0) ||
				 (GuiaDistribuicaoId.getNumCPFRelator().length()==0)) vErro.addElement("Junta/Relator n�o selecionado. \n") ;
			// buscar os processos selecionados
			String procSelec[] = req.getParameterValues("Selecionado")	;
			int n =0 ;				
			ArrayList<ProcessoBean> processosSelec = new ArrayList<ProcessoBean>();			
			
			if (procSelec!=null)	{
				for (int i=0;  i<procSelec.length-1; i++) {					
					n = Integer.parseInt(procSelec[i]) ;	
					
					if ((n>=0) && (n<GuiaDistribuicaoId.getProcessos().size())) { 
						processosSelec.add(GuiaDistribuicaoId.getProcessos().get(n));
					}
				}
				GuiaDistribuicaoId.setProcessos(processosSelec);
			}
			else vErro.addElement("Nenhum processo selecionado.") ;
			
			if ((vErro.size()==0) && (GuiaDistribuicaoId.getProcessos().size()==0)) vErro.addElement("Nenhum processo selecionado.") ;
			
			if (vErro.size()>0) GuiaDistribuicaoId.setMsgErro(vErro);
			else{	 
				GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			    if (GuiaDistribuicaoId.getNumGuia().length()>0)				
                {
                    vErro.addElement("Processos j� cadastrados em outra guia /ou Aguarde o processamento dos dados .");
                    GuiaDistribuicaoId.setMsgErro(vErro);
                }
                else
				    inc = dao.GuiaGrava(GuiaDistribuicaoId,UsrLogado, funcionalidades) ; 
			}		
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaPreparaManualCmd: " + ue.getMessage());
	   }
	   return inc;
   } 	
}
