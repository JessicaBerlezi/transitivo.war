package PNT.DEF;

import java.util.ArrayList;
import java.util.Iterator;

import PNT.ProcessoBean;
import PNT.RequerimentoBean;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Evento Bean<br>
* <b>Description:</b>  Evento Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
* @author Sergio Monteiro
* @version 1.0
*/

public class EventoBean   extends sys.HtmlPopupBean { 

  private String eventoOK;  
  private String msg;
  private String reqs;

  private String statusValido   ;
  private String statusTPCVValido   ;  
  private String cFuncao         ;  
  private String nFuncao         ;
  private String tipoReqValidos  ;	

  private String statusReqValido  ;	
  private String origemEvento  ;	
  private String codEvento  ;	
  private String dscEvento  ;	
  private String codEventoRef  ;	
  private String proxStatus  ;
 
  
  public EventoBean()  throws sys.BeanException {

    eventoOK        = "N";
    msg             = "";
    reqs            = "";	
  
    statusValido   = "" ;
    statusTPCVValido   = "" ;	
    cFuncao         = "" ;
    nFuncao         = "" ;
    tipoReqValidos  = "" ;	
    statusReqValido = "" ;
    origemEvento    = "" ;
    codEvento       = "" ;
    dscEvento       = "" ;
    codEventoRef       = "" ;
	proxStatus      = "" ;    
	
  }
//--------------------------------------------------------------------------  
  public void setStatusValido(String statusValido)   {
	if (statusValido==null) statusValido="";  
    this.statusValido=statusValido ;	
  } 
  public String getStatusValido()   {
    	return this.statusValido;
    } 
//--------------------------------------------------------------------------  
  public void setStatusTPCVValido(String statusTPCVValido)   {
	if (statusTPCVValido==null) statusTPCVValido="";  
    this.statusTPCVValido=statusTPCVValido ;	
  } 
  public String getStatusTPCVValido()   {
      	return this.statusTPCVValido;
    } 
//--------------------------------------------------------------------------  
  public void setCFuncao(String j_sigFuncao)   {
	if (j_sigFuncao==null) j_sigFuncao="";      
	this.cFuncao=j_sigFuncao ;	
  } 
  public String getCFuncao()   {
    return this.cFuncao;	
  } 
//--------------------------------------------------------------------------  	
  public void setNFuncao(String nFuncao)   {
	  if (nFuncao==null) nFuncao="";      
	  this.nFuncao=nFuncao ;	
  } 
  public String getNFuncao()   {
       	return this.nFuncao;
   } 
//--------------------------------------------------------------------------     
  public void setTipoReqValidos(String tipoReqValidos)   {
    if (tipoReqValidos==null) tipoReqValidos="";      
    this.tipoReqValidos=tipoReqValidos ;	
  } 
  public String getTipoReqValidos()   {
      	return this.tipoReqValidos;
   } 
//--------------------------------------------------------------------------     
  public void setStatusReqValido(String statusReqValido)   {
    if (statusReqValido==null) statusReqValido="";      
    this.statusReqValido=statusReqValido ;	
  } 
  public String getStatusReqValido()   {
       	return this.statusReqValido;
   } 
//--------------------------------------------------------------------------     
  public void setOrigemEvento(String origemEvento)   {
    if (origemEvento==null) origemEvento="";      
    this.origemEvento=origemEvento ;	
  } 
  public String getOrigemEvento()   {
       	return this.origemEvento;
  } 
//--------------------------------------------------------------------------    
  public void setCodEvento(String codEvento)   {
    if (codEvento==null) codEvento="";      
    this.codEvento=codEvento ;	
  } 
  public String getCodEvento()   {
      	return this.codEvento;
  } 
  
//--------------------------------------------------------------------------    
  public void setDscEvento(String dscEvento)   {
    if (dscEvento==null) dscEvento="";      
    this.dscEvento=dscEvento ;	
  } 
  public String getDscEvento()   {
      	return this.dscEvento;
  } 

//--------------------------------------------------------------------------    
  public void setCodEventoRef(String codEventoRef)   {
    if (codEventoRef==null) codEventoRef="";      
    this.codEventoRef=codEventoRef ;	
  } 
  public String getCodEventoRef()   {
      	return this.codEventoRef;
  } 
  
//--------------------------------------------------------------------------  
  public void setEventoOK(String eventoOK)   {
  	if (eventoOK==null) eventoOK="N";
  	this.eventoOK = eventoOK ;
   }  
   public void setEventoOK(PNT.ProcessoBean myProcesso) throws sys.BeanException   {
		try {
			setEventoOK(myProcesso,sys.Util.formatedToday().substring(0,10));
		}
		catch (Exception ue) {
			  throw new sys.BeanException("EventoBean: " + ue.getMessage());
		}
	   return ;			
   }
   public void setEventoOK(PNT.ProcessoBean myProcesso,String dtRef) throws sys.BeanException   {
	  setMsg("");
	  setEventoOK("N");	
	  if ("".equals(myProcesso.getNomStatus())) return ;
	  setMsg(myProcesso.getNomStatus()+" ("+myProcesso.getDatStatus()+")") ;

	  // Estorno de TRI
	  if ("REC0840".equals(cFuncao)) { 
		  setEventoTRIOK(myProcesso);
	  	  return;
	  }
	  try {	  	
		  boolean  ok = (getStatusValido().equals(myProcesso.getCodStatus()));
//		  if (!ok) ok = ( ("".equals(getStatusTPCVValido())==false) && ("1".equals(myAuto.getIndTPCV())) && (getStatusTPCVValido().indexOf(myAuto.getCodStatus())>=0)) ;
		  if (!ok) ok = ( (getStatusValido().length()==myProcesso.getCodStatus().length()) && (getStatusTPCVValido().indexOf(myProcesso.getCodStatus())>=0)) ;
		  if ( (!ok) && ("15,35".indexOf(getStatusValido())>=0) ) ok = ("15,16,35,36".indexOf(myProcesso.getCodStatus())>=0);
	   	  if (ok) {
	      		Iterator it = myProcesso.getRequerimentos().iterator() ;
	      		REC.RequerimentoBean req  = new REC.RequerimentoBean();
	      		boolean existe = false ;
	      		while (it.hasNext()) {
	   				req =(REC.RequerimentoBean)it.next() ;		
	      			if ( (getTipoReqValidos().indexOf(req.getCodTipoSolic())>=0) && (getStatusReqValido().indexOf(req.getCodStatusRequerimento())>=0) ){
	      				existe = true ;
	      				break ;
	      			}
	      		}
	      		if (existe)  setEventoOK("S");	
	      		else 	setMsg(getMsg() + " - Sem requerimento para "+getNFuncao()+".");	      		   					   	
	      }
	      else {
	   		setMsg(getMsg() + " - Status do Auto n�o permite "+getNFuncao());				
	      }
	   }
	   catch (Exception ue) {
	     throw new sys.BeanException("EventoBean: " + ue.getMessage());
	   }

	  return ;	
  } 
   public void setEventoTRIOK(PNT.ProcessoBean myProcesso) throws sys.BeanException   {
	   // Estorno de TRI
	   try {
		  if (  ( (getStatusValido().indexOf(myProcesso.getCodStatus())>=0) && (myProcesso.getCodStatus().length()==1) ) ||
				( ("10,11,12,14,15,16,82".indexOf(myProcesso.getCodStatus())>=0) && (myProcesso.getCodStatus().length()==2) ) ) {
				Iterator it = myProcesso.getRequerimentos().iterator() ;
				REC.RequerimentoBean req  = new REC.RequerimentoBean();
				boolean existe = false ;
				while (it.hasNext()) {
					req =(REC.RequerimentoBean)it.next() ;		
					if ( (getTipoReqValidos().indexOf(req.getCodTipoSolic())>=0) && (getStatusReqValido().indexOf(req.getCodStatusRequerimento())>=0) ){
						existe = true ;
						break ;
					}
				}
				if (existe)  setEventoOK("S");	
				else 	setMsg(getMsg() + " - Sem requerimento para "+getNFuncao()+".");					   	
		  }
		  else {
			setMsg(getMsg() + " - Status do Auto n�o permite "+getNFuncao());				
		  }
	   }
	   catch (Exception ue) {
		 throw new sys.BeanException("EventoBean: " + ue.getMessage());
	   }
	  return ;	
  } 

  
  public String getEventoOK()   {
   	return this.eventoOK;
   } 
//--------------------------------------------------------------------------  
  public void setMsg(String msg)   {
  	if (msg==null) msg="";
  	this.msg = msg ;
   }   
  public String getMsg()   {
	return this.msg ;	
  }
//--------------------------------------------------------------------------  
  public void setReqs(REC.AutoInfracaoBean myAuto,REC.RequerimentoBean myReq) throws sys.BeanException {
	this.reqs="";
  	String aux="";
    for(int f=0;f<35;f++) 	aux=aux+"&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append("<select style='border-style: outset;' name='codRequerimento' size='0' onChange='javascript: valida(\"LeReq\",this.form)'> \n");	  
		popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");	    			  
		String seleciona="";	
		String nom = "";
		try {
		  Iterator it = myAuto.getRequerimentos().iterator() ;
		  REC.RequerimentoBean req  = new REC.RequerimentoBean();
		  while (it.hasNext()) {
		  	req =(REC.RequerimentoBean)it.next() ;
			// Requerimento de DP ou DC com status 2,3
			if ( (getTipoReqValidos().indexOf(req.getCodTipoSolic())>=0) && (getStatusReqValido().indexOf(req.getCodStatusRequerimento())>=0) ){
				  seleciona = (myReq.getCodRequerimento().equals(req.getCodRequerimento())) ? "selected" : "";
			  	nom = (req.getNomResponsavelDP().length()>15 ? req.getNomResponsavelDP().substring(0,15) : req.getNomResponsavelDP()) ;
			  	popupBuffer.append("<OPTION VALUE='"+req.getCodRequerimento()+"' "+seleciona+" >"+req.getCodTipoSolic()+" - "+nom+" - "+req.getNumRequerimento()+"</OPTION> \n");
		  	}
	  }
  }
    catch (Exception ex) {
	  	throw new sys.BeanException(ex.getMessage());
    }
    popupBuffer.append("</SELECT> \n");	  
	this.reqs = popupBuffer.toString() ;
  }
  public String getReqs()   {
    return  ("S".equals(this.eventoOK)) ? this.reqs : this.msg ;
  }
 
//--------------------------------------------------------------------------    
  public void setProxStatus(String proxStatus)   {
	if (proxStatus==null) proxStatus="";  
	this.proxStatus=proxStatus ;	
 } 
  public String getProxStatus()   {
		return this.proxStatus;
	} 
//---------------------------------------------------------------------------  
  public void removeAutoGuia(ProcessoBean myProcesso,RequerimentoBean myReq) throws Exception{
  	try{
  		GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
  		dao.removeAutoGuia(myProcesso,myReq);
  	}
  	catch(Exception e){
  		throw new Exception(e.getMessage());
  	}
  }

  public ArrayList getEventos() {
	   ArrayList eventos = new ArrayList();

	   try {
			Dao dao = DaoFactory.getInstance();
			//eventos = dao.getEventos();
		}
		catch (Exception e) {
		}
	return eventos;
  }
  

  public String buscaCodEventoRef(String codEvento) {
	   String eventos = "";

	   try {
			//Dao dao = Dao.getInstance();
			//eventos = dao.buscaCodEventoRef(codEvento);
		}
		catch (Exception e) {
		}
	return eventos;
 }
  
 
//--------------------------------------------------------------------------
  
  public ArrayList getEventosRegistro() {
	   ArrayList eventos = new ArrayList();

	   try {
			REG.Dao daoReg = REG.Dao.getInstance();
			eventos = daoReg.getEventosRegistro();
		}
		catch (Exception e) {
		}
	return eventos;
}  

}
