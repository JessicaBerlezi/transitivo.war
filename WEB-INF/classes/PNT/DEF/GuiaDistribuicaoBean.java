package PNT.DEF;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.CommandException;
import sys.Util;
import ACSS.ParamSistemaBean;
import PNT.DaoException;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import REC.ParamOrgBean;


/**
 * <b>Title:</b>        SMIT - Prepara Guia de Distribuicao<br>
 * <b>Description:</b>  Prepara Guia de Distribuicao <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Mello
 * @version 1.0
 */

public class GuiaDistribuicaoBean extends EventoBean  {
	
	private String msgErro ;
	private String msgOk ;
	
	private String numGuia;
	private String codStatus;
	private String nomStatus;
	private String numCPFRelator;
	private String codRelator;
	private String nomRelator;
	
	private String sigJunta;
	private String codJunta;
	private String tipoJunta;
	private String nomTitulo;
	
	private String nomUserName;
	private String codOrgaoLotacao;
	private String codOrgaoAtuacao;
	private String datProc;
	private String datRS;
	
	private String codEnvioDO;
	private String datEnvioDO;
	private String datPubliPDO;
	private String nomArquivoATA;
	private String tipoDO;
	
	private List Processos;
	private List ProcessosNaoProc;
	private String ordClass ;
	private String ordem ;
	
	private int proximoRel;
	private int iTotalProc;
	private List ProcessosSobra;
	private List processosSelect;
	private String geraNovaGuia;
	private List relatGuias;
	
	private String numSessao;
	private String datSessao;
	private String indSessao;
	private String tipoGuia;
	public final String GUIA_AUTOMATICA = "S";
	private String controleGuia ;
	private String dataIni ;
	private String dataFim;
	
	private int iTotalProcessos;
	private int iTotalRequerimentos;
	private int iToTalProcessosErro;
	
	private String tituloAta;
	private String subTituloAta;
	private String paragrafo1;
	private String paragrafo2;
	
	
	
	public GuiaDistribuicaoBean()  throws Exception {
		super() ;
		msgErro = "";
		msgOk = "" ;
		numGuia = "";
		codStatus = "";
		nomStatus = "";
		numCPFRelator = "";
		codRelator = "";
		nomRelator = "";
		nomTitulo  = "";
		
		sigJunta = "";
		codJunta = "";
		tipoJunta = "0";
		
		nomUserName = "";
		codOrgaoLotacao = "";
		codOrgaoAtuacao	= "";
		datProc = "";
		datRS = "";
		
		codEnvioDO = "";
		datEnvioDO = "";
		datPubliPDO = "";
		nomArquivoATA = "";
		tipoDO = "";// 0- Autua��o 2-Penalidade 3-Apreens�o
		
		ordClass  = "ascendente";
		ordem     = "Data";
		
		Processos = new ArrayList() ;
		processosSelect = new ArrayList() ;
		ProcessosNaoProc = new ArrayList() ;
		
		proximoRel   = 0;
		iTotalProc   = 0;
		ProcessosSobra   = new ArrayList() ;
		geraNovaGuia = "N";		
		relatGuias   = new ArrayList() ;
		
		numSessao = "";
		datSessao = "";
		indSessao = "";
		tipoGuia = "";
		controleGuia ="";
		dataIni = formatedLast2Month().substring(0, 10);
		dataFim = sys.Util.formatedToday().substring(0, 10);
		
		
		iTotalProcessos = 0;
		iTotalRequerimentos =0;
		iToTalProcessosErro = 0;
		
		tituloAta = "";
		subTituloAta = "";
		paragrafo1 = "";
		paragrafo2 = "";
		
	}

	public String getTipoDO() {
		return tipoDO;
	}

	public void setTipoDO(String tipoDO) {
		this.tipoDO = tipoDO;
	}

	public void setTotalProcessos(int iTotalProcessos) {
	  this.iTotalProcessos = iTotalProcessos;
	}
	public int getTotalProcessos() {
		return iTotalProcessos;
	}

	public void setTotalRequerimentos(int iTotalRequerimentos) {
		this.iTotalRequerimentos = iTotalRequerimentos;
	}
	public int getTotalRequerimentos() {
		return iTotalRequerimentos;
	}
	
	public void setToTalProcessosErro(int iToTalProcessosErro) {
		this.iToTalProcessosErro = iToTalProcessosErro;
	}
	public int getToTalProcessosErro() {
		return iToTalProcessosErro;
	}
	
	public void setTipoGuia(String tipoGuia) {
		if(tipoGuia == null) tipoGuia = "";
		else this.tipoGuia = tipoGuia;
	}
	
	public String getTipoGuia() {
		return tipoGuia;
	}
	
	public void setNumSessao(String numSessao)   {
		if(numSessao == null) numSessao = "";
		else this.numSessao = numSessao ;
	}
	public String getNumSessao()   {
		return this.numSessao;
	}
	
	public void setDatSessao(String datSessao)   {
		if(datSessao == null) datSessao = "";
		else this.datSessao = datSessao ;
	}
	public String getDatSessao()   {
		return this.datSessao;
	}
	
	public void setIndSessao(String indSessao)   {
		if(indSessao == null) indSessao = "N";
		this.indSessao = indSessao ;
	}
	public String getIndSessao()   {
		return this.indSessao;
	}
	
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j)+"\n" ;
		}
	}
	public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
	}
	
	public String getMsgErro()   {
		return this.msgErro;
	}
	public void setMsgOk(String msgOk)   {
		this.msgOk = msgOk ;
	}
	public String getMsgOk()   {
		return this.msgOk;
	}
	//--------------------------------------------------------------------------
	public void setProcessos(List<ProcessoBean> Processos)  {
		this.Processos=Processos ;
	}
	public List<ProcessoBean> getProcessos()  {
		return this.Processos;
	}
	/**
	 * Sobrecarga
	 * @param tipoRequerimento
	 * @return
	 * @author Rodrigo.fonseca
	 */
	public List<ProcessoBean> getProcessos(String tipoRequerimento)  {
		if ("DP".equals(tipoRequerimento)){
			return getProcessosComRecursoIndeferidos();
		}else{
			return this.Processos;
		}
		
	}
	
	public ProcessoBean getProcessos(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.Processos.size()) ) return (new ProcessoBean()) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return (ProcessoBean)this.Processos.get(i);
	}
	
	public ProcessoBean getProcessosSelect(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.processosSelect.size()) ) return (new ProcessoBean()) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return (ProcessoBean)this.processosSelect.get(i);
	}
	
	
	//--------------------------------------------------------------------------
	public void setProcessosNaoProc(List ProcessosNaoProc)  {
		this.ProcessosNaoProc=ProcessosNaoProc ;
	}
	public List getProcessosNaoProc()  {
		return this.ProcessosNaoProc;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao)  {
		this.codOrgaoAtuacao=codOrgaoAtuacao ;
		if (codOrgaoAtuacao==null) this.codOrgaoAtuacao= "";
	}
	public String getCodOrgaoAtuacao()  {
		return this.codOrgaoAtuacao;
	}
	//--------------------------------------------------------------------------
	public void setNomUserName(String nomUserName)  {
		this.nomUserName=nomUserName ;
		if (nomUserName==null) this.nomUserName= "";
	}
	public String getNomUserName()  {
		return this.nomUserName;
	}
	//--------------------------------------------------------------------------
	public void setNumGuia(String numGuia)  {
		this.numGuia=numGuia ;
		if (numGuia==null) this.numGuia= "";
	}
	public String getNumGuia()  {
		return this.numGuia;
	}
	//--------------------------------------------------------------------------
	public void setCodStatus(String codStatus)  {
		this.codStatus=codStatus ;
		if (codStatus==null) this.codStatus= "";
	}
	public String getCodStatus()  {
		return this.codStatus;
	}
	
	
	/*Luciana*/
	public void setProximoRelator(int proximoRel)   {
		this.proximoRel = proximoRel ;
	}
	
	public int getProximoRelator()   {
		return this.proximoRel;
	}
	
	public void setTotalProc(int iTotalProc)   {
		this.iTotalProc = iTotalProc ;
	}
	
	public int getTotalProc()   {
		return this.iTotalProc;
	}
	
	public void setGeraNovaGuia(String geraNovaGuia)   {
		this.geraNovaGuia = geraNovaGuia ;
		if (geraNovaGuia==null) this.geraNovaGuia= "N";		
	}
	
	public String getGeraNovaGuia()   {
		return this.geraNovaGuia;
	}
	
	public void setRelatGuias(List relatGuias)  {
		this.relatGuias=relatGuias ;
	}
	public List getRelatGuias()  {
		return this.relatGuias;
	}
	
	public void setProcessosSelect(List processosSelect)  {
		this.processosSelect=processosSelect ;
	}
	public List getProcessosSelect()  {
		return this.processosSelect;
	}
	
	public void setProcessosSobra(List ProcessosSobra)  {
		this.ProcessosSobra=ProcessosSobra ;
	}
	public List getProcessosSobra()  {
		return this.ProcessosSobra;
	}
	
	
	//--------------------------------------------------------------------------
	public void setNomStatus(String nomStatus)  {
		this.nomStatus=nomStatus ;
		if (nomStatus==null) this.nomStatus= "";
	}
	public String getNomStatus()  {
		if(codStatus.equals("2"))
			nomStatus = "Def. Junta/Relator";
		else if(codStatus.equals("3"))
			nomStatus = "Resultado";
		else if(codStatus.equals("4"))
			nomStatus = "Enviado p/ Publica��o";
		else if(codStatus.equals("8"))
			nomStatus = "Estornado";
		else if(codStatus.equals("9"))
			nomStatus = "Publicado";
		return nomStatus;
	}
	//--------------------------------------------------------------------------
	public void setNumCPFRelator(String numCPFRelator)  {
		this.numCPFRelator=numCPFRelator ;
		if (numCPFRelator==null) this.numCPFRelator= "";
	}
	public String getNumCPFRelator()  {
		return this.numCPFRelator;
	}
	public void setNumCPFRelatorEdt(String numCpf)  {
		if (numCpf==null)      numCpf = "";
		if (numCpf.length()<14) numCpf += "               ";
		this.numCPFRelator=numCpf.substring(0,3)
		+ numCpf.substring(4,7)
		+ numCpf.substring(8,11)
		+ numCpf.substring(12,14);
	}
	public String getNumCPFRelatorEdt()  {
		if ("".equals(this.numCPFRelator.trim())) return this.numCPFRelator.trim() ;
		if (this.numCPFRelator.length()<11) return this.numCPFRelator.trim() ;
		return numCPFRelator.substring(0,3)+ "." + numCPFRelator.substring(3,6)
		+ "." + numCPFRelator.substring(6,9) + "-" + numCPFRelator.substring(9,11) ;
	}
	
	//--------------------------------------------------------------------------
	public void setCodRelator(String codRelator)  {
		this.codRelator=codRelator ;
		if (codRelator==null) this.codRelator= "";
	}
	public String getCodRelator()  {
		return this.codRelator;
	}
	
	//--------------------------------------------------------------------------
	public void setNomRelator(String nomRelator)  {
		this.nomRelator=nomRelator ;
		if (nomRelator==null) this.nomRelator= "";
	}
	public String getNomRelator()  {
		return this.nomRelator;
	}
	//--------------------------------------------------------------------------
	public void setNomTituloRelator(String nomTitulo)  {
		this.nomTitulo=nomTitulo ;
		if (nomTitulo==null) this.nomTitulo= "";
	}
	public String getNomTituloRelator()  {
		return this.nomTitulo;
	}
	//--------------------------------------------------------------------------
	public void setSigJunta(String sigJunta)  {
		this.sigJunta=sigJunta ;
		if (sigJunta==null) this.sigJunta= "";
	}
	public String getSigJunta()  {
		return this.sigJunta;
	}
	//--------------------------------------------------------------------------
	public void setCodJunta(String codJunta)  {
		this.codJunta=codJunta ;
		if (codJunta==null) this.codJunta= "";
	}
	public String getCodJunta()  {
		return this.codJunta;
	}
	//--------------------------------------------------------------------------
	public void setTipoJunta(String tipoJunta)  {
		this.tipoJunta=tipoJunta ;
		if (tipoJunta==null) this.tipoJunta= "0";
	}
	public String getTipoJunta()  {
		return this.tipoJunta;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
		this.codOrgaoLotacao=codOrgaoLotacao ;
		if (codOrgaoLotacao==null) this.codOrgaoLotacao= "";
	}
	public String getCodOrgaoLotacao()  {
		return this.codOrgaoLotacao;
	}
	//--------------------------------------------------------------------------
	public void setDatProc(String datProc)  {
		this.datProc=datProc ;
		if (datProc==null) this.datProc= "";
	}
	public String getDatProc()  {
		return this.datProc;
	}
	//--------------------------------------------------------------------------
	public void setDatRS(String datRS)  {
		this.datRS=datRS ;
		if (datRS==null) this.datRS= "";
	}
	public String getDatRS()  {
		return this.datRS;
	}
	//--------------------------------------------------------------------------
	public void setCodEnvioDO(String codEnvioDO)  {
		this.codEnvioDO=codEnvioDO ;
		if (codEnvioDO==null) this.codEnvioDO= "";
	}
	public String getCodEnvioDO()  {
		return this.codEnvioDO;
	}
	//--------------------------------------------------------------------------
	public void setDatEnvioDO(String datEnvioDO)  {
		this.datEnvioDO=datEnvioDO ;
		if (datEnvioDO==null) this.datEnvioDO= "";
	}
	public String getDatEnvioDO()  {
		return this.datEnvioDO;
	}
	//--------------------------------------------------------------------------
	public void setDatPubliPDO(String datPubliPDO)  {
		this.datPubliPDO=datPubliPDO ;
		if (datPubliPDO==null) this.datPubliPDO= "";
	}
	public String getDatPubliPDO()  {
		return this.datPubliPDO;
	}
	//--------------------------------------------------------------------------
	public void setNomArquivoATA(String nomArquivoATA)  {
		this.nomArquivoATA=nomArquivoATA ;
		if (nomArquivoATA==null) this.nomArquivoATA= "";
	}
	public String getNomArquivoATA()  {
		return nomArquivoATA;
	}
	
	//-------------------------------------------------------------------------
	public void setControleGuia(String controleGuia) {
		if(controleGuia == null) controleGuia = "";
		else this.controleGuia = controleGuia;
	}
	public String getControleGuia() {
		return controleGuia;
	}
	
	//--------------------------------------------------------------------------
	public void setDataIni(String dataIni) {
		if (dataIni == null) dataIni = "";
		else this.dataIni = dataIni;
	}
	public String getDataIni() {
		return dataIni;
	}
	//--------------------------------------------------------------------------
	public void setDataFim(String dataFim) {
		if (dataFim == null) dataFim = "";
		else this.dataFim = dataFim;
	}
	public String getDataFim() {
		return dataFim;
	}
	//--------------------------------------------------------------------------
	
	public String getTituloAta() {
		return tituloAta;
	}

	public void setTituloAta(String tituloAta) {
		if (tituloAta == null) tituloAta = "";
		this.tituloAta = tituloAta;
	}
	//--------------------------------------------------------------------------

	public String getSubTituloAta() {
		return subTituloAta;
	}

	public void setSubTituloAta(String subTituloAta) {
		if (subTituloAta == null) subTituloAta = "";
		this.subTituloAta = subTituloAta;
	}
	//----------------------------------------------------------------------------
	
	public String getParagrafo1() {
		return paragrafo1;
	}

	public void setParagrafo1(String paragrafo1) {
		if (paragrafo1 == null) paragrafo1 = "";
		this.paragrafo1 = paragrafo1;
	}
	//--------------------------------------------------------------------------

	public String getParagrafo2() {
		return paragrafo2;
	}

	public void setParagrafo2(String paragrafo2) {
		if (paragrafo2 == null) paragrafo2 = "";
		this.paragrafo2 = paragrafo2;
	}
	
	//--------------------------------------------------------------------------
	public void PreparaGuia(ACSS.UsuarioBean myUsrLog,String datInicioRemessa,String numMaxProc) throws sys.BeanException {
		try {
			Processos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPrepara(this,myUsrLog,datInicioRemessa,numMaxProc)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				Classifica("Processo");
				
				
				
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	

	//--------------------------------------------------------------------------
	public void PreparaGuiaDetran(ACSS.UsuarioBean myUsrLog,String datInicioRemessa) throws sys.BeanException {
		try {
			Processos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPreparaDetran(this,myUsrLog,datInicioRemessa)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				Classifica("Processo");
				
				
				
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	public void PreparaGuiaAgravoInsert(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {
			Processos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPreparaAgravoInsert(this,myUsrLog)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				/*Classifica("DatProcesso");*/
				Classifica("Processo");
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	
	//--------------------------------------------------------------------------
	public void PreparaGuiaAgravo(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {
			Processos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPreparaAgravo(this,myUsrLog)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				/*Classifica("DatProcesso");*/
				//Classifica("Processo");
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	
	//--------------------------------------------------------------------------
	public void LeGuia() throws sys.BeanException {
		try {
			Processos  		     = new ArrayList() ;
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.GuiaLe(this);
			/*Classifica("Processo");*/
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public Vector LeGuiasAta() throws sys.BeanException {
		Vector guias = new Vector();
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			guias=dao.LeGuiasAta(this) ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return guias ;
	}
	//	--------------------------------------------------------------------------
	
	public String[] AtaGuias() throws sys.BeanException{
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.AtaGuias(codEnvioDO, "9",codOrgaoAtuacao) ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public List consultaGuia(ACSS.UsuarioBean UsrLogado) throws DaoException{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.consultaGuia(this,UsrLogado);
		}
		catch (Exception ey) {
			throw new DaoException(ey.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public List consultaAtasPublicar(ACSS.UsuarioBean UsrLogado ) throws DaoException{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.consultaAtasPublicar(this,UsrLogado);
		}
		catch (Exception ey) {
			throw new DaoException(ey.getMessage());
		}
	}
	public void consultaAta(int ordem) throws Exception{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.consultaAta(this,ordem);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	
	//	--------------------------------------------------------------------------
	public void setOrdClass(String ordClass)  {
		this.ordClass=ordClass ;
		if (ordClass==null) this.ordClass= "ascendente";
	}
	public String getOrdClass()  {
		return this.ordClass;
	}
	//	--------------------------------------------------------------------------
	public void setOrdem(String ordem)  {
		this.ordem=ordem ;
		if (ordem==null) this.ordem= "Data";
	}
	public String getOrdem()  {
		return this.ordem;
	}
	public String getNomOrdem()  {
		String nomOrdem = "Data Processo, Requerimento e Num Processo" ;
		if (this.ordem.equals("Orgao"))       nomOrdem = "Org�o e Num Auto" ;
		if (this.ordem.equals("Placa"))       nomOrdem = "Placa e Data Infra��o" ;
		if (this.ordem.equals("Processo"))    nomOrdem = "Processo" ;
		if (this.ordem.equals("Auto"))        nomOrdem = "Auto de Infra��o" ;
		if (this.ordem.equals("Responsavel")) nomOrdem = "Respons�vel e Placa" ;
		if (this.ordem.equals("Status"))      nomOrdem = "Status e Data Infra��o" ;
		if (this.ordem.equals("DatProcesso")) nomOrdem = "Data e Processo"  ;
		if (this.ordem.equals("Enquadramento")) nomOrdem = "Enquadramento, Data e Processo"  ;
		return nomOrdem+ " ("+getOrdClass()+")" ;
	}
	//	--------------------------------------------------------------------------
	public void Classifica(String ordemSol) throws sys.BeanException {
		int ord = 0;
		if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
		if (ordemSol.equals("Orgao"))               ord = 1 ;
		if (ordemSol.equals("Placa"))               ord = 2 ;
		if (ordemSol.equals("Processo"))            ord = 3 ;
		if (ordemSol.equals("Responsavel"))         ord = 4 ;
		if (ordemSol.equals("Auto"))                ord = 5 ;
		if (ordemSol.equals("Status"))              ord = 6 ;
		if (ordemSol.equals("DatProcesso"))         ord = 7 ;
		if (ordemSol.equals("Enquadramento"))       ord = 8 ;
		if (ordemSol.equals(getOrdem()))   {
			if ( getOrdClass().equals("ascendente")) setOrdClass("descendente");
			else 									 setOrdClass("ascendente");
		}
		else setOrdClass("ascendente");
		setOrdem(ordemSol) ;
		int tam = getProcessos().size() ;
		ProcessoBean tmp = new ProcessoBean();
		String chave ;
		for (int i=0; i<tam; i++)  {
			switch (ord) {
			// "Data Infra��o, Org�o e Num Auto" ;
			case 0:
				chave = sys.Util.formataDataYYYYMMDD(getProcessos(i).getDatProcesso()) +
				sys.Util.lPad(getProcessos(i).getCodOrgao(),"0",6) +
				sys.Util.rPad(getProcessos(i).getNumProcesso()," ",12);
				break;
				// "Org�o e Num Auto" ;
			case 1:
				chave = sys.Util.lPad(getProcessos(i).getCodOrgao(),"0",6) ;
				break;
				// "Placa e Data Infra��o" ;
			case 2:
				chave = sys.Util.formataDataYYYYMMDD(getProcessos(i).getDatProcesso());
				break;
				// "Processo" ;
			case 3:
				chave = sys.Util.rPad(getProcessos(i).getNumProcesso()," ",20) ;
				break;
				// "Respons�vel e Placa" ;
			case 4:
				chave = sys.Util.rPad(getProcessos(i).getNomResponsavel(),"",40); //+
				//FIXME sys.Util.rPad(getProcessos(i).getNumPlaca(),"",7) ;
				break;
				// "Num Auto" ;
			case 5:
				chave = sys.Util.rPad(getProcessos(i).getNumProcesso(),"",12) ;
				break;
				// Status e Data da Infracao
			case 6:
				chave = sys.Util.lPad(getProcessos(i).getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(getProcessos(i).getDatProcesso()) ;
				break;
				// "Data Processo, Processo" ;
			case 7:
				chave = sys.Util.formataDataYYYYMMDD(getProcessos(i).getDatProcesso()) +
				sys.Util.rPad(getProcessos(i).getNumProcesso()," ",20) ;
				break;
				// "Enquadramento, Processo" ;
			default:
				chave = sys.Util.rPad(getProcessos(i).getDscPenalidade()," ",50) +
				sys.Util.formataDataYYYYMMDD(getProcessos(i).getDatProcesso()) +
				sys.Util.rPad(getProcessos(i).getNumProcesso()," ",20) ;
			}
			tmp = getProcessos(i) ;
			tmp.setChaveSort(chave) ;
			getProcessos().set(i,tmp);
		}
		QSort(getProcessos(),getOrdClass()) ;
	}
	
	//	--------------------------------------------------------------------------
	public static void QSort(List lista,int lo0, int hi0,String ordem)
	throws sys.BeanException {
		int lo = lo0;
		int hi = hi0;
		ProcessoBean tmp ;
		if (lo >= hi) return;
		else if( lo == hi - 1 ) {
			/*
			 *  sort a two element list by swapping if necessary
			 */
			if (QSortCompara((ProcessoBean)lista.get(lo),(ProcessoBean)lista.get(hi),ordem))   {  // a[lo] > a[hi])
				tmp = (ProcessoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(ProcessoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}
			return;
		}
		/*
		 *  Pick a pivot and move it out of the way
		 */
		ProcessoBean pivot = (ProcessoBean)lista.get((lo + hi) / 2);
		lista.set( ((lo + hi)/2),lista.get(hi));
		lista.set(hi,pivot) ;
		
		while( lo < hi ) {
			/*
			 *  Search forward from a[lo] until an element is found that
			 *  is greater than the pivot or lo >= hi
			 */
			while ( !(QSortCompara((ProcessoBean)lista.get(lo),pivot,ordem)) && (lo < hi) )       {    //(a[lo] <= pivot) && (lo < hi))
				lo++;
			}
			/*
			 *  Search backward from a[hi] until element is found that
			 *  is less than the pivot, or lo &gt;= hi
			 */
			while ( !(QSortCompara(pivot,(ProcessoBean)lista.get(hi),ordem)) && (lo < hi) ) {    // (pivot <= a[hi])
				hi--;
			}
			/*
			 *  Swap elements a[lo] and a[hi]
			 */
			if( lo < hi ) {
				tmp = (ProcessoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(ProcessoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}
		}
		/*
		 *  Put the median in the "center" of the list
		 */
		lista.set(hi0,(ProcessoBean)lista.get(hi)) ;         // a[hi0] = a[hi];
		lista.set(hi,pivot) ;                     // a[hi] = pivot;
		/*
		 *  Recursive calls, elements a[lo0] to a[lo-1] are less than or
		 *  equal to pivot, elements a[hi+1] to a[hi0] are greater than
		 *  pivot.
		 */
		QSort(lista, lo0, lo-1,ordem);
		QSort(lista, hi+1, hi0,ordem);
	}
	
	public static void QSort(List lista,String ordem) throws sys.BeanException {
		QSort(lista, 0, lista.size()-1,ordem);
	}
	public static boolean QSortCompara(ProcessoBean a0,ProcessoBean a1,String ordem)
	throws sys.BeanException {
		boolean b = false ;
		if (ordem.equals("descendente")) b = (a1.getChaveSort()).compareTo(a0.getChaveSort())>0 ;
		else							 b = (a0.getChaveSort()).compareTo(a1.getChaveSort())>0 ;
		return b ;
	}
	
	//----------------------------------------------------------------------------------
	/**
	 * FIXME
	 */
	public boolean gravaArquivo(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId) throws Exception{
		try{
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = codEnvioDO;
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_" + nomAta+ ".rtf");
			
			if(!codStatus.equals("-1")){
				if(file.exists())
					return false;
			}
			file.createNewFile();
			if(!file.canWrite()){
				setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}
			nomArquivoATA = file.getName();
			int quantReq = 0;
			FileWriter fwrite = new FileWriter(file);
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_EMPRESA_ATA","SECRETARIA DE ESTADO DE SEGURAN�A","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_DEPARTAMENTO","DEPARTAMENTO DE TR�NSITO","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_JARI","JUNTA ADMINISTRATIVA DE RECURSOS DE INFRA��ES","3")+"\n");
			fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO);
			GuiaDistribuicaoBean myGuia = null;
			ProcessoBean myProc = null;
			String sigJunta = "", nomTitulo = "";
			for(int x=0; x<relatGuias.size() ;x++){
				myGuia = (GuiaDistribuicaoBean)relatGuias.get(x);
				if(!myGuia.getSigJunta().equals(sigJunta)){
					sigJunta = myGuia.getSigJunta();
					fwrite.write("\n\nAtas da: "+myGuia.getSigJunta()+"\n");
					fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO+" \n");					
				}
				//fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + " atrav�s da Guia " + myGuia.getNumGuia() +" \n");
				
				
				if(!myGuia.getNomTituloRelator().equals(nomTitulo)){
					nomTitulo = myGuia.getNomTituloRelator();
					fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + ".\n");					
				}
				
				//fwrite.write("Processos \t \t \t Requerimento \t \t \t Resultado \n");
				for(int y=0; y<myGuia.getProcessos().size(); y++){
					myProc = myGuia.getProcessos(y);
					String myProcInf=myProc.getNumProcesso().replaceAll("/    /","/");
					fwrite.write(myProcInf.trim()+", " + myProc.getRequerimentos(0).getNumRequerimento().trim()+ " ("+myProc.getRequerimentos(0).getNomResultRS() +")"+ (y%2==0?"\n":";"));
					quantReq++;
				}
			}
			fwrite.close();
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(!dao.gravaArquivo(this, String.valueOf(quantReq))){
				if(!file.delete()){
					setMsgErro("Erro F�sico no Arquivo");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		setMsgErro("Arquivo Gerado com Sucesso");
		return true;
	}
	
	/**
	 * FIXME
	 */
	public boolean gravaArquivo(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId, FuncionalidadesBean funcionalidadesBean) throws Exception{
		try{
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = codEnvioDO;
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_" + nomAta+ ".rtf");
			if(!codStatus.equals("-1")){
				if(file.exists())
					return false;
			}
			file.createNewFile();
			if(!file.canWrite()){
				setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}
			nomArquivoATA = file.getName();
			int quantReq = 0;
			FileWriter fwrite = new FileWriter(file);
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_EMPRESA_ATA","SECRETARIA DE ESTADO DE SEGURAN�A","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_DEPARTAMENTO","DEPARTAMENTO DE TR�NSITO","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_JARI","JUNTA ADMINISTRATIVA DE RECURSOS DE INFRA��ES","3")+"\n");
			fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO);
			GuiaDistribuicaoBean myGuia = null;
//			ProcessoBean myProc = null;
			String sigJunta = "", nomTitulo = "";
			for(int x=0; x<relatGuias.size() ;x++){
				myGuia = (GuiaDistribuicaoBean)relatGuias.get(x);
				if(!myGuia.getSigJunta().equals(sigJunta)){
					sigJunta = myGuia.getSigJunta();
					fwrite.write("\n\nAtas da: "+myGuia.getSigJunta()+"\n");
					fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO+" \n");					
				}
				//fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + " atrav�s da Guia " + myGuia.getNumGuia() +" \n");
				
				
				if(!myGuia.getNomTituloRelator().equals(nomTitulo)){
					nomTitulo = myGuia.getNomTituloRelator();
					fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + ".\n");					
				}
				
				//fwrite.write("Processos \t \t \t Requerimento \t \t \t Resultado \n");
//				for(int y=0; y<myGuia.getProcessos(funcionalidadesBean.getTipoRequerimento()).size(); y++){
//					myProc = myGuia.getProcessos(y);
//					String myProcInf=myProc.getNumProcesso().replaceAll("/    /","/");
//					fwrite.write(myProcInf.trim()+", " + myProc.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento()).getNumRequerimento().trim()+ " ("+myProc.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento()).getNomResultRS() +")"+ (y%2==0?"\n":";"));
//					quantReq++;
//				}
				for (ProcessoBean processoBean :  myGuia.getProcessos(funcionalidadesBean.getTipoRequerimento())){
					String myProcInf=processoBean.getNumProcesso().replaceAll("/    /","/");
					fwrite.write(myProcInf.trim()+", " + processoBean.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento()).getNumRequerimento().trim()+ " ("+processoBean.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento()).getNomResultRS() +")"+ (quantReq%2==0?"\n":";"));
					quantReq++;
				}
			}
			fwrite.close();
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(!dao.gravaArquivo(this, String.valueOf(quantReq))){
				if(!file.delete()){
					setMsgErro("Erro F�sico no Arquivo");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		setMsgErro("Arquivo Gerado com Sucesso");
		return true;
	}
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * FIXME
	 */
	public boolean gravaArquivoDP(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId, FuncionalidadesBean funcionalidadesBean) throws Exception{
		try{
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = codEnvioDO;
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_" + nomAta+ ".rtf");
			if(!codStatus.equals("-1")){
				if(file.exists())
					return false;
			}
			file.createNewFile();
			if(!file.canWrite()){
				setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}
			nomArquivoATA = file.getName();
			int quantReq = 0;
			FileWriter fwrite = new FileWriter(file);
			fwrite.write("GOVERNO DO RIO DE JANEIRO\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_EMPRESA_ATA","SECRETARIA DE ESTADO DE SEGURAN�A","3")+"\n");
			fwrite.write("DEPARTAMENTO DE TR�NSITO DO ESTADO DO RIO DE JANEIRO \n");
			fwrite.write(""+this.tituloAta+"\n");
			fwrite.write("EDITAL N�"+this.codEnvioDO+", DE "+this.datEnvioDO.substring(0,2)+" DE "+Util.mesPorExtenso(this.datEnvioDO.substring(3,5))+" DE "+this.datEnvioDO.substring(6,10)+"  \n");
			fwrite.write(""+this.subTituloAta+"\n");
			fwrite.write(""+this.paragrafo1+"\n");
			fwrite.write(""+this.paragrafo2+"\n");
			GuiaDistribuicaoBean myGuia = null;
//			ProcessoBean myProc = null;
			String sigJunta = "", nomTitulo = "";
			for(int x=0; x<relatGuias.size() ;x++){
				myGuia = (GuiaDistribuicaoBean)relatGuias.get(x);

				for (ProcessoBean processoBean :  myGuia.getProcessos(funcionalidadesBean.getTipoRequerimento())){
					String myProcInf=processoBean.getNumProcesso().replaceAll("/    /","/");
					fwrite.write("CPF n�:"+processoBean.getNumCPFEdt()+", Pts:  " + processoBean.getTotPontos() +", Penalidade: "+ processoBean.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento()).getDscPenalidade()+""+ (quantReq%2==0?"\n":";"));
					quantReq++;
				}
			}
			fwrite.close();
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(!dao.gravaArquivo(this, String.valueOf(quantReq))){
				if(!file.delete()){
					setMsgErro("Erro F�sico no Arquivo");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		setMsgErro("Arquivo Gerado com Sucesso");
		return true;
	}
	

	//----------------------------------------------------------------------------------
	public List buscaArquivosAta(String dataIni, String dataFim) throws DaoException{
		List vetArquivos = null;
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			vetArquivos = dao.buscaArquivosAta(this,dataIni,dataFim);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		if(vetArquivos.size() == 0){
			msgErro = "Nenhum arquivo localizado para a data especificada !!!";
		}
		return vetArquivos;
	}
	//-----------------------------------------------------------------------------------------
	public void downloadArquivoAta() throws DaoException{
		
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.downloadArquivoAta(this,codEnvioDO);
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	//-----------------------------------------------------------------------------------------
	
	public void estornarGuia(FuncionalidadesBean funcionalidadesBean) throws sys.CommandException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(codStatus.equals("2")){
				codStatus = "8";
				/*-------------------- C�digo para Carregar Tabela Oracle ----------------------*/
				setCodStatus(codStatus);
				dao.guiaEstornoJuntaRelator(this,funcionalidadesBean);
				
			}else if(codStatus.equals("3")){
				codStatus = "2";
				/*-------------------- C�digo para Carregar Tabela Oracle ----------------------*/
				setCodStatus(codStatus);
				dao.guiaEstornoResultado(this,funcionalidadesBean);
				
			}else if(codStatus.equals("4")){
				codStatus = "3";
				/*-------------------- C�digo para Carregar Tabela Oracle ----------------------*/
				setCodStatus(codStatus);
				dao.guiaEstornoDO(this,funcionalidadesBean);
				
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEstornoCmd: " + ue.getMessage());
		}
		if(this.getMsgErro().length() == 0)
			this.setMsgErro("Estorno da Guia: " + numGuia + " efetuado com sucesso");
		return ;
	}
	//---------------------------------------------------------------------------------------------
	public void consultaAtaImprime(int ordem) throws Exception{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.consultaAta(this,ordem);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	//---------------------------------------------------------------------------------------------
	public static String formatedLast2Month() {
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -60);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;
		
	} // Fim formatedToday
	
	
	public List<ProcessoBean> getProcessosComRecursoIndeferidos (){
		List<ProcessoBean> processosRecursoIndeferidos = new ArrayList<ProcessoBean>();
		
		for (ProcessoBean processoBean : this.getProcessos()){
			try {
				processoBean.LeProcesso();
				RequerimentoBean requerimentoBean = processoBean.getRequerimentoPorStatusETipoRequerimento("3", "DP");
				if ("I".equals(requerimentoBean.getCodResultRS())){
					processosRecursoIndeferidos.add(processoBean);
				}
			} catch (BeanException e) {
				e.printStackTrace();
			}
		}
		return processosRecursoIndeferidos;
	}

	public void removeProcessoGuia(List<ProcessoBean> processosARemover) throws CommandException {
		try {
			
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.removeProcessoGuia(processosARemover, this);
				
		}catch (Exception ue) {
			throw new sys.CommandException("GuiaEstornoCmd: " + ue.getMessage());
		}
		
		if(this.getMsgErro().length() == 0)
			this.setMsgErro("Processos removidos da Guia: " + numGuia + " com sucesso");
		return ;
	}
	
}