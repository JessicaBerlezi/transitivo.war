package PNT.DEF;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BeanException;

import REC.ParamOrgBean;

import ACSS.ParamSistemaBean;

public class GuiaAtaReemiteCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/PNT/DEF/GuiaAtaReemite.jsp" ;  
   
   public GuiaAtaReemiteCmd() {
      next = jspPadrao;
   }

   public GuiaAtaReemiteCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          
          if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
		  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  
		  if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
		  GuiaDistribuicaoBean GuiaDistribuicaoId = new GuiaDistribuicaoBean();

		  ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		  if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
		  
		  FuncionalidadesBean funcionalidades = (FuncionalidadesBean)session.getAttribute("funcionalidadesBeanId") ;
		  if (funcionalidades==null)  funcionalidades = new FuncionalidadesBean();
		  
		  funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
      	  
		  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
      	  
		  String dataIni = req.getParameter("De");
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  GuiaDistribuicaoId.setDataFim(dataFim);
      	  
		  String acao    = req.getParameter("acao");  
		  GuiaDistribuicaoId.setColunaValue("COD_ENVIO_PUBDO");
		  GuiaDistribuicaoId.setPopupNome("codEnvioDO");
		  String sql = "ATA, SELECT DISTINCT COD_ENVIO_PUBDO ,'ATA: ' || COD_ENVIO_PUBDO || '    ENVIADA: '  || TO_DATE(DAT_ENVIO_PUBDO,'DD/MM/YYYY') || '    PUBLICADA: ' || DAT_PUBDO || '    STATUS: ' || decode(COD_STATUS,4,'Enviado p/ Publica��o',9,'Publicado') AS ATA "+ 
		  " FROM SMIT_PNT.TPNT_LOTE_RELATOR L JOIN TPNT_JUNTA J"+
		  " ON L.COD_JUNTA = J.COD_JUNTA"+
		  " WHERE IND_TIPO = "+ funcionalidades.getTipoJunta() + 
		  " AND (COD_STATUS = 4 OR COD_STATUS = 9)" +
		  " AND L.DT_PROC BETWEEN TO_DATE('"+GuiaDistribuicaoId.getDataIni()+"','DD/MM/YYYY') AND TO_DATE('"+GuiaDistribuicaoId.getDataFim()+"','DD/MM/YYYY') "+
		  " ORDER BY ATA";
		  GuiaDistribuicaoId.setTipoJunta(funcionalidades.getTipoJunta());
		  

		  
		  if (acao==null)
		  	acao = "";

		  if  (acao.equals("ImprimirAta"))  {	 
		  	GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("codEnvioDO"));
			incluiTextosAta(GuiaDistribuicaoId, funcionalidades, paramPnt);
		  	int ordem = 2;
//		  	if(req.getParameter("layout").equals("Relatorio")){
//				/*
//		  		if(ParamOrgaoId.getParamOrgao("LAYOUT_ATA","S","2").indexOf("S")>=0){
//					ordem = 3;
//					GuiaDistribuicaoId.consultaAtaImprime(ordem);	
//					nextRetorno = "/PNT/DEF/GuiaPublicaAtaNitImp.jsp";
//				}else{
//				*/
//					GuiaDistribuicaoId.consultaAtaImprime(ordem);	
//		  		    nextRetorno = "/PNT/DEF/GuiaPublicaAtaImp.jsp";
//				//}
//		  	}
//		  	else{
		  		String layOutCabAta = "N";
		  		ACSS.OrgaoBean myOrg = new ACSS.OrgaoBean();
		  		GuiaDistribuicaoId.consultaAtaImprime(ordem);	
//		  		if(ParamOrgaoId.getParamOrgao("LAYOUT_CABECALHO_ATA","N","3").indexOf("S")>=0){
//		  			layOutCabAta = "S";
//		  			myOrg.Le_Orgao(UsrLogado.getCodOrgaoAtuacao(),0);
//		  			req.setAttribute("myOrg",myOrg);
//		  			
//		  		}
		  		req.setAttribute("layOutCabAta",layOutCabAta);
		  		nextRetorno = "/PNT/DEF/GuiaPublicaDO"+funcionalidades.getTipoRequerimento()+".jsp" ;   
//		  	}
		  }

		  
		  if  (acao.equals("GravarAta"))  {	 		  	
		  	GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("codEnvioDO"));
		  	GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
		  	GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  	GuiaDistribuicaoId.setChecked(GuiaDistribuicaoId.getCodEnvioDO());
		  	GuiaDistribuicaoId.setTipoJunta(funcionalidades.getTipoJunta());		  	
		  	GuiaDistribuicaoId.consultaAta(4);	
		  	incluiTextosAta(GuiaDistribuicaoId, funcionalidades, paramPnt);
		  	
		  	if(req.getParameter("confirmaExclusao").equals("S"))
		  		GuiaDistribuicaoId.setCodStatus("-1");	
 	
		  	if ("DP".equals(funcionalidades.getTipoRequerimento())){
			  	if(!GuiaDistribuicaoId.gravaArquivoDP((ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId"),ParamOrgaoId,funcionalidades))
			  		req.setAttribute("confirmaExclusao","S");
		  	}else{
		  		if(!GuiaDistribuicaoId.gravaArquivo((ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId"),ParamOrgaoId,funcionalidades))
		  			req.setAttribute("confirmaExclusao","S");
		  	}
		  }
		  
		  GuiaDistribuicaoId.setPopupString(sql);
		  session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
		  session.setAttribute("funcionalidadesBeanId",funcionalidades) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaAtaReemiteCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }
   
   public void incluiTextosAta (GuiaDistribuicaoBean guiaDistribuicaoBean, FuncionalidadesBean funcionalidadesBean, ParamSistemaBean paramSistemaBean) throws BeanException{
	   if (!"".equals(funcionalidadesBean.getTipoRequerimento())){
		   guiaDistribuicaoBean.setTituloAta(paramSistemaBean.getParamSist("TITULO_ATA_GUIA_PONTUACAO_"+funcionalidadesBean.getTipoRequerimento()));
		   guiaDistribuicaoBean.setSubTituloAta(paramSistemaBean.getParamSist("SUBTITULO_ATA_GUIA_PONTUACAO_"+funcionalidadesBean.getTipoRequerimento()));
		   guiaDistribuicaoBean.setParagrafo1(paramSistemaBean.getParamSist("PARAGRAFO1_ATA_GUIA_PONTUACAO_"+funcionalidadesBean.getTipoRequerimento()));
		   guiaDistribuicaoBean.setParagrafo2(paramSistemaBean.getParamSist("PARAGRAFO2_ATA_GUIA_PONTUACAO_"+funcionalidadesBean.getTipoRequerimento()));
	   }
   }
}