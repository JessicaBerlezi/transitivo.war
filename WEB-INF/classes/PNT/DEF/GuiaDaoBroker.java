package PNT.DEF;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;
import ACSS.UsuarioBean;
import PNT.DaoBroker;
import PNT.DaoBrokerFactory;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.TAB.StatusBean;
import REC.AutoInfracaoBean;
import REC.RemessaBean;

public class GuiaDaoBroker extends sys.DaoBase{
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "REC";
	private static GuiaDaoBroker instance;
	public static GuiaDaoBroker getInstance()
	throws sys.DaoException {
		if (instance == null)
			instance = new GuiaDaoBroker();
		return instance;
	}
	
	private GuiaDaoBroker() throws sys.DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	
	private String converteData(java.sql.Date data) {
		String Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = df.format(data);
		} catch (Exception e) {
			Retorno = "";
		}
		return Retorno;
	}	 
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPrepara (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog,String datInicioRemessa,String numMaxProc) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
//		Connection conn =null ;					
//		try {
//			DaoBroker dao = DaoBrokerFactory.getInstance();			
//			conn = serviceloc.getConnection(MYABREVSIST);	
//			Statement stmt = conn.createStatement();
//			String sCmd        = "";
//			ResultSet rs       = null;	  
//			boolean continua   = true;
//			String resultado   = "";
//			String parteVar    = "";
//			String tpJunta     = "1";	
//			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
//			else {
//				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
//				else tpJunta="3" ;				
//			}
//			if (myGuia.getCFuncao().equals("REC0251")) tpJunta="4";
//			
//			String indContinua = sys.Util.rPad(" "," ",27);
//			List autos = new ArrayList();
//			int reg=0;			
//			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
//				indContinua = sys.Util.rPad(" "," ",27);
//				continua   = true;				  
//				while (continua)
//				{
//					/*
//					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
//					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
//					tpJunta ;
//					*/
//					
//					/*DPWEB*/
//					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
//					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
//					tpJunta;
//
//					resultado = chamaTransBRK("049",parteVar,indContinua);
//					ProcessoBean myPro = new ProcessoBean();							
//					if (verErro(myPro,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
//						myGuia.setMsgErro(myPro.getMsgErro());
//						return false;					
//					}
//					String linha = "";
//					int i = 0;
//					for (i=0; i < 26; i++) {				
//						/*linha = resultado.substring((i*112)+3,(i+1)*112+3);*/
//						
//						/*DPWEB*/
//						linha = resultado.substring((i*115)+3,(i+1)*115+3);
//						
//						ProcessoBean myProcesso = new ProcessoBean();
//						RequerimentoBean myReq = new RequerimentoBean();
//						
//						//myProcesso.setInfracao(new InfracaoBean());	
//						//myProcesso.setProprietario(new ResponsavelBean());
//						myProcesso.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
//						myProcesso.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
//						setPosIni(0);											
//						myProcesso.setNumProcesso(getPedaco(linha,12," "," "));	
//						if (myProcesso.getNumProcesso().length()>0) {
//							myProcesso.setNumPlaca(getPedaco(linha,7," "," "));
//							/*Retirar*/
//							VerificaAutoOracle(myProcesso,conn);								
//							myProcesso.getInfracao().setCodAutoInfracao(myProcesso.getCodAutoInfracao());					
//							myProcesso.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
//							myProcesso.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
//							myProcesso.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
//							myProcesso.setNumProcesso(getPedaco(linha,20," "," "));
//							myProcesso.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
//							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
//							myProcesso.setNumRequerimento(getPedaco(linha,30," "," "));
//							myProcesso.setTpRequerimento(getPedaco(linha,2," "," "));								
//							// Buscar Resultado
//							myProcesso.setCodResultPubDO(getPedaco(linha,1," "," "));
//							myProcesso.setCodVinculada(getPedaco(linha,1," "," "));
//							
//							/*DPWEB*/
//							String sIndReqWEB=getPedaco(linha,1," "," ");
//							
//							
//							//								myAuto.setEventoOK("1");
//							
//							myReq.setNumRequerimento(myProcesso.getNumRequerimento());
//							myReq.setCodAutoInfracao(myProcesso.getCodAutoInfracao());
//
//							/*dao.LeRequerimentoLocal(myReq,conn); 02/04/2008*/
//							UsuarioBean myUsrLogado = new UsuarioBean();				
//							dao.LeRequerimento(myProcesso,myUsrLogado);				
//							int seq = 0;
//							Iterator it = myProcesso.getRequerimentos().iterator();
//							REC.RequerimentoBean reqProcesso = new REC.RequerimentoBean();
//							while (it.hasNext()) {				
//								reqProcesso =(REC.RequerimentoBean)it.next();
//								if (myReq.getNumRequerimento().equals(myProcesso.getRequerimentos(seq).getNumRequerimento()))	{						
//									myReq.setCodRequerimento(reqProcesso.getCodRequerimento());
//									myReq.setCodTipoSolic(reqProcesso.getCodTipoSolic());
//									myReq.setDatRequerimento(reqProcesso.getDatRequerimento());
//									myReq.setCodStatusRequerimento(reqProcesso.getCodStatusRequerimento());
//									myReq.setNomUserNameDP(reqProcesso.getNomUserNameDP());
//									myReq.setCodOrgaoLotacaoDP(reqProcesso.getCodOrgaoLotacaoDP());
//									myReq.setNomResponsavelDP(reqProcesso.getNomResponsavelDP());			
//									myReq.setDatProcDP(reqProcesso.getDatProcDP());
//									myReq.setDatPJ(reqProcesso.getDatPJ());                
//									myReq.setCodParecerPJ(reqProcesso.getCodParecerPJ());
//									myReq.setCodRespPJ(reqProcesso.getCodRespPJ());
//									myReq.setTxtMotivoPJ(reqProcesso.getTxtMotivoPJ());
//									myReq.setNomUserNamePJ(reqProcesso.getNomUserNamePJ());
//									myReq.setCodOrgaoLotacaoPJ(reqProcesso.getCodOrgaoLotacaoPJ());
//									myReq.setDatProcPJ(reqProcesso.getDatProcPJ());
//									myReq.setDatJU(reqProcesso.getDatJU());
//									myReq.setCodJuntaJU(reqProcesso.getCodJuntaJU());
//									myReq.setCodRelatorJU(reqProcesso.getCodRelatorJU());
//									myReq.setNomUserNameJU(reqProcesso.getNomUserNameJU());
//									myReq.setCodOrgaoLotacaoJU(reqProcesso.getCodOrgaoLotacaoJU());
//									myReq.setDatProcJU(reqProcesso.getDatProcJU());
//									myReq.setDatRS(reqProcesso.getDatRS());
//									myReq.setNomUserNameRS(reqProcesso.getNomUserNameRS());
//									myReq.setCodOrgaoLotacaoRS(reqProcesso.getCodOrgaoLotacaoRS());
//									myReq.setDatProcRS(reqProcesso.getDatProcRS());
//									myReq.setNomCondutorTR(reqProcesso.getNomCondutorTR());
//									myReq.setIndTipoCNHTR(reqProcesso.getIndTipoCNHTR());
//									myReq.setNumCNHTR(reqProcesso.getNumCNHTR());
//									myReq.setCodUfCNHTR(reqProcesso.getCodUfCNHTR());
//									myReq.setNumCPFTR(reqProcesso.getNumCPFTR());	
//									myReq.setDatAtuTR(reqProcesso.getDatAtuTR());
//									myReq.setNomUserNameTR(reqProcesso.getNomUserNameTR());
//									myReq.setCodOrgaoLotacaoTR(reqProcesso.getCodOrgaoLotacaoTR());
//									myReq.setSitProtocolo(reqProcesso.getSitProtocolo());
//									myReq.setDatRecebimentoGuia(reqProcesso.getDatRecebimentoGuia());
//									myReq.setDatEnvioGuia(reqProcesso.getDatEnvioGuia());
//									myReq.setNomUserNameRecebimento(reqProcesso.getNomUserNameRecebimento());
//									myReq.setNomUserNameEnvio(reqProcesso.getNomUserNameEnvio());
//									myReq.setCodRemessa(reqProcesso.getCodRemessa());	
//									myReq.setDatEnvioDO(reqProcesso.getDatEnvioDO());
//									myReq.setCodEnvioDO(reqProcesso.getCodEnvioDO());
//									myReq.setDatPublPDO(reqProcesso.getDatPublPDO());
//									myReq.setCodMotivoDef(reqProcesso.getCodMotivoDef());
//									sCmd =  "SELECT * FROM TSMI_REQUERIMENTO_WEB WHERE "+
//									"NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
//									rs = stmt.executeQuery(sCmd) ;			 
//									if (rs.next())
//									{
//										myReq.setNomRequerente(rs.getString("NOM_REQUERENTE"));
//										myReq.setTxtJustificativa(rs.getString("TXT_MOTIVO"));
//										myReq.setTxtEmail(rs.getString("TXT_EMAIL"));
//									}
//									break;
//								}
//								seq++;
//							}
//							
//							
//							/*DPWEB*/
//							myReq.setIndReqWEB(sIndReqWEB);		
//							
//							
//							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");							
//							Date dat_InicioRemessa = new Date();
//							Date dat_Requerimento  = new Date();
//							boolean bProtocolo=true;
//							
//							if(datInicioRemessa.length()>0)
//							{
//								/*dao.LeRequerimentoLocal(myReq,conn);*/
//								dat_InicioRemessa = df.parse(datInicioRemessa);								
//								if (myReq.getDatRequerimento().length()==0)
//									myReq.setDatRequerimento("26/07/2004");
//								
//								dat_Requerimento = df.parse(myReq.getDatRequerimento());
//								if (dat_Requerimento.after(dat_InicioRemessa))
//								{
//									if ( (myReq.getSitProtocolo().length()==0) || (myReq.getSitProtocolo().equals("0")) )
//										bProtocolo=false;
//								}
//							}
//							
////							Verifica se tem AI Digitalizado
//							sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
//							" WHERE NUM_AUTO_INFRACAO = '"+myProcesso.getNumAutoInfracao()+"'";
//							rs    = stmt.executeQuery(sCmd) ;
//							while (rs.next()) {
//								myProcesso.setTemAIDigitalizado(true);			
//							}				
//							myProcesso.setEventoOK("0");
//							if (myGuia.getTipoReqValidos().indexOf(myProcesso.getTpRequerimento())>=0) 
//							{
//								if (bProtocolo) {
//									myProcesso.getRequerimentos().add(myReq);
//									autos.add(myProcesso) ;
//								}
//							}
//						}										
//					}
//					/*DPWEB*/
//					//setPosIni(3+(28*115)); antigo
//					setPosIni(3+26*115); 
//					
//					if (reg>Integer.parseInt(numMaxProc)) break;					
//					
//					indContinua = getPedaco(resultado,27," "," ");
//					if (indContinua.trim().length()<=0) continua=false;
//					reg++;
//				}			    
//			}
//			myGuia.setAutos(autos);	
//			if (rs != null) rs.close();
//			stmt.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}
	
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaDetran (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog,String datInicioRemessa) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
//		Connection conn =null ;					
//		try {
//			DaoBroker dao = DaoBrokerFactory.getInstance();			
//			conn = serviceloc.getConnection(MYABREVSIST);	
//			Statement stmt = conn.createStatement();
//			String sCmd        = "";
//			ResultSet rs       = null;	  
//			boolean continua   = true;
//			String resultado   = "";
//			String parteVar    = "";
//			String tpJunta     = "1";	
//			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1" ;				
//			else {
//				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
//				else tpJunta="3" ;				
//			}
//			String indContinua = sys.Util.rPad(" "," ",27);
//			List autos = new ArrayList();
//			int reg=0;			
//			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
//				indContinua = sys.Util.rPad(" "," ",27);
//				continua   = true;				  
//				while (continua)
//				{
//					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
//					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
//					tpJunta ;
//					resultado = chamaTransBRK("049",parteVar,indContinua);
//					AutoInfracaoBean myAt = new AutoInfracaoBean();							
//					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
//						myGuia.setMsgErro(myAt.getMsgErro());
//						return false;					
//					}
//					String linha = "";
//					int i = 0;
//					for (i=0; i < 26; i++) {				
//						linha = resultado.substring((i*115)+3,(i+1)*115+3);					
//						AutoInfracaoBean myAuto = new AutoInfracaoBean();
//						RequerimentoBean myReq = new RequerimentoBean();
//						
//						myAuto.setInfracao(new InfracaoBean());	
//						myAuto.setProprietario(new ResponsavelBean());
//						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
//						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
//						setPosIni(0);											
//						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
//						if (myAuto.getNumAutoInfracao().trim().length()>0) {
//							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
//							/*Retirar*/
//							VerificaAutoOracle(myAuto,conn);								
//							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
//							myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
//							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
//							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
//							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
//							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
//							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
//							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
//							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
//							// Buscar Resultado
//							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
//							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
//							//								myAuto.setEventoOK("1");
//							
//							myReq.setNumRequerimento(myAuto.getNumRequerimento());
//							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
//							/*dao.LeRequerimentoLocal(myReq,conn); 08/02/2007*/
//							
//							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");							
//							Date dat_InicioRemessa = new Date();
//							Date dat_Requerimento  = new Date();
//							boolean bProtocolo=true;
//							
//							if(datInicioRemessa.length()>0)
//							{
//								dao.LeRequerimentoLocal(myReq,conn);
//								dat_InicioRemessa = df.parse(datInicioRemessa);								
//								if (myReq.getDatRequerimento().length()==0)
//									myReq.setDatRequerimento("26/07/2004");
//								
//								dat_Requerimento = df.parse(myReq.getDatRequerimento());
//								if (dat_Requerimento.after(dat_InicioRemessa))
//								{
//									if ( (myReq.getSitProtocolo().length()==0) || (myReq.getSitProtocolo().equals("0")) )
//										bProtocolo=false;
//								}
//							}
//							
//							/*Retirar*/
////							if (myAuto.getCodStatus().equals("99"))
////								bProtocolo=false;
//							int ianoProc = Integer.parseInt(myAuto.getDatProcesso().substring(6,10));
//							if (ianoProc < 2006)
//								bProtocolo=false;
//							
//							
//							
//							
////							Verifica se tem AI Digitalizado
//							sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
//							" WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
//							rs    = stmt.executeQuery(sCmd) ;
//							while (rs.next()) {
//								myAuto.setTemAIDigitalizado(true);			
//							}				
//							myAuto.setEventoOK("0");
//							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
//							{
//								if (bProtocolo)
//									autos.add(myAuto) ;	
//							}
//						}										
//					}
//					//setPosIni(3+(28*112));
//					setPosIni(3+26*115); 
//					
//					indContinua = getPedaco(resultado,27," "," ") ;				
//					if (indContinua.trim().length()<=0) continua=false;
//					reg++;
//				}			    
//			}
//			myGuia.setAutos(autos);	
//			if (rs != null) rs.close();
//			stmt.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}
	
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaAgravo (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
//		Connection conn =null ;					
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;	
//			Statement stmt  = conn.createStatement();
//			Statement stmt2 = conn.createStatement();
//			String sCmd        = "";
//			ResultSet rs       = null;
//			ResultSet rs2      = null;			
//			boolean continua   = true;
//			String resultado   = "";
//			String parteVar    = "";
//			String tpJunta     = "1";	
//			int iContador = 0;
//			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
//			else {
//				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
//				else tpJunta="3" ;				
//			}
//			
//			List autos = new ArrayList();			
//			sCmd="SELECT * FROM TSMI_GUIA ORDER BY NUM_PROCESSO,to_date(dat_processo,'dd/mm/yyyy')";
//			rs2=stmt2.executeQuery(sCmd);
//			while (rs2.next())
//			{
//				
//				AutoInfracaoBean myAuto = new AutoInfracaoBean();
//				myAuto.setInfracao(new InfracaoBean());
//				
//				myAuto.setProprietario(new ResponsavelBean());
//				myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
//				myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
//				myAuto.setNumAutoInfracao(rs2.getString("NUM_AUTO"));
//				myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
//				myAuto.getInfracao().setCodAutoInfracao(rs2.getString("COD_AUTO_INFRACAO"));
//				myAuto.getInfracao().setCodInfracao(rs2.getString("COD_INFRACAO"));
//				String sEnquadramento=rs2.getString("DSC_ENQUADRAMENTO");
//				myAuto.setDatInfracao(rs2.getString("DAT_INFRACAO"));
//				myAuto.setNumProcesso(rs2.getString("NUM_PROCESSO"));
//				myAuto.setDatProcesso(rs2.getString("DAT_PROCESSO"));
//				myAuto.setNumRequerimento(rs2.getString("NUM_REQUERIMENTO"));
//				myAuto.setTpRequerimento(rs2.getString("TIP_REQUERIMENTO"));
//				myAuto.setCodResultPubDO(rs2.getString("COD_RESULTADO"));
//				myAuto.setCodVinculada(rs2.getString("COD_VINCULADA"));
//				myAuto.getInfracao().setDscEnquadramento(sEnquadramento);
//				myAuto.setEventoOK("0");
//				if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
//					autos.add(myAuto);
//			}
//			myGuia.setAutos(autos);	
//			if (rs != null) rs.close();
//			if (rs2!= null) rs2.close();			
//			stmt.close();
//			stmt2.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaAgravoInsert (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
//		Connection conn =null ;					
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;	
//			Statement stmt = conn.createStatement();
//			String sCmd        = "";
//			ResultSet rs       = null;	  
//			boolean continua   = true;
//			String resultado   = "";
//			String parteVar    = "";
//			String tpJunta     = "1";	
//			int iContador = 0;
//			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
//			else {
//				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
//				else tpJunta="3" ;				
//			}
//			String indContinua = sys.Util.rPad(" "," ",27);
//			List autos = new ArrayList();
//			int reg=0;
//			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
//				indContinua = sys.Util.rPad(" "," ",27);
//				continua   = true;				  
//				while (continua) {
//					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
//					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
//					tpJunta ;
//					resultado = chamaTransBRK("049",parteVar,indContinua);
//					
//					String sResultadoSlv=resultado;
//					AutoInfracaoBean myAt = new AutoInfracaoBean();							
//					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
//						myGuia.setMsgErro(myAt.getMsgErro());
//						return false;					
//					}
//					String linha = "";
//					int i = 0;
//					for (i=0; i < 26; i++) {				
//						linha = resultado.substring((i*115)+3,(i+1)*115+3);					
//						AutoInfracaoBean myAuto = new AutoInfracaoBean();
//						myAuto.setInfracao(new InfracaoBean());	
//						myAuto.setProprietario(new ResponsavelBean());
//						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
//						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
//						setPosIni(0);											
//						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
//						if (myAuto.getNumAutoInfracao().trim().length()>0) {
//							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
//							VerificaAutoOracle(myAuto,conn);								
//							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
//							myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
//							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
//							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
//							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
//							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
//							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
//							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
//							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
//							// Buscar Resultado
//							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
//							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
//							//								myAuto.setEventoOK("1");
//							myAuto.setEventoOK("0");
//							
//							/*Chamar 041*/
//							resultado = chamaTransBRK("041",
//									sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
//									sys.Util.rPad(myAuto.getNumPlaca()," ",7)+
//									sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
//									sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
//									"         ","");
//							
//							setPosIni(581);
//							String sCodStatus=getPedaco(resultado,3,"0",".");
//							resultado=sResultadoSlv;
//							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) {
//								sCmd =  "insert into tsmi_guia (COD_AUTO_INFRACAO,NUM_AUTO,NUM_PLACA,COD_INFRACAO,DSC_ENQUADRAMENTO,"+
//								"NUM_PROCESSO,DAT_INFRACAO,DAT_PROCESSO,NUM_REQUERIMENTO,TIP_REQUERIMENTO,COD_RESULTADO,"+
//								"COD_VINCULADA,COD_STATUS) VALUES ("+
//								"'"+myAuto.getInfracao().getCodAutoInfracao()+"',"+
//								"'"+myAuto.getNumAutoInfracao()+"',"+
//								"'"+myAuto.getNumPlaca()+"',"+
//								"'"+myAuto.getInfracao().getCodInfracao()+"',"+			
//								"'"+myAuto.getInfracao().getDscEnquadramento()+"',"+
//								"'"+myAuto.getNumProcesso()+"',"+								
//								"'"+myAuto.getDatInfracao()+"',"+
//								"'"+myAuto.getDatProcesso()+"',"+							
//								"'"+myAuto.getNumRequerimento()+"',"+
//								"'"+myAuto.getTpRequerimento()+"',"+								
//								"'"+myAuto.getCodResultPubDO()+"',"+
//								"'"+myAuto.getCodVinculada()+"',"+
//								"'"+sCodStatus+"')";								
//								stmt.execute(sCmd);
//								autos.add(myAuto);
//								iContador++;									
//							}
//						}										
//					}
//					//setPosIni(3+(28*112));
//					setPosIni(3+26*115); 
//					indContinua = getPedaco(resultado,27," "," ") ;				
//					if (indContinua.trim().length()<=0) continua=false;
//					reg++;
//				}
//			}
//			myGuia.setAutos(autos);	
//			if (rs != null) rs.close();
//			stmt.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean PreparaRemessa (RemessaBean myRemessa,ACSS.UsuarioBean myUsrLog) throws sys.DaoException 
	{
		//		  synchronized
		boolean bOk = true ;
//		Connection conn =null ;	
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST);	
//			Statement stmt      = conn.createStatement();
//			String sCmd        = "";
//			ResultSet rs       = null;
//			DaoBroker dao = DaoBrokerFactory.getInstance();
//			List autos = new ArrayList();
//			
//			//Guia de Remessa / CEDOC
//			String sTipoSolic="";
//			if (myRemessa.getIndRecurso().equals("5") || myRemessa.getIndRecurso().equals("98"))
//				sTipoSolic="'DP','DC'";
//			else if (myRemessa.getIndRecurso().equals("15") || myRemessa.getIndRecurso().equals("96"))
//				sTipoSolic="'1P','1C'";
//			else if (myRemessa.getIndRecurso().equals("35") || myRemessa.getIndRecurso().equals("97"))
//				sTipoSolic="'2P','2C'";
//			
//			//Indica o sit_xx de acordo com o tipo de remessa
//			String sIndSit = "";
//			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
//					myRemessa.getIndRecurso().equals("97"))
//				sIndSit = "SIT_CEDOC";
//			else 
//				sIndSit = "SIT_PROTOCOLO";
//			
//			
//			String sStatus=myRemessa.getIndRecurso();
//			if (sStatus.equals("15"))
//				sStatus="'15','16'";					
//			
//			sCmd="SELECT /*+rule*/ NUM_AUTO_INFRACAO,NUM_PLACA,NUM_REQUERIMENTO FROM TSMI_AUTO_INFRACAO T1,TSMI_REQUERIMENTO T2 WHERE "+ 
//			"T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO AND "+sIndSit+" IS NULL AND T1.COD_STATUS in ("+sStatus+") "+
//			"AND T2.COD_TIPO_SOLIC IN ("+sTipoSolic+") "+
//			"AND T1.COD_ORGAO='"+myRemessa.getCodOrgao()+"' " +
//			"AND T2.COD_STATUS_REQUERIMENTO='"+myRemessa.getStatusReqValido()+"' ";
//			if (myRemessa.getDatInicio().length()>0)
//				sCmd+="AND T2.DAT_REQUERIMENTO>=TO_DATE('"+myRemessa.getDatInicio()+"','DD/MM/YYYY') ";
//			sCmd+="ORDER BY NUM_PROCESSO";
//			rs=stmt.executeQuery(sCmd);			
//			while (rs.next())
//			{
//				AutoInfracaoBean myAuto      = new AutoInfracaoBean();
//				if  (myAuto.getProprietario()==null) myAuto.setProprietario(new REC.ResponsavelBean());		
//				if  (myAuto.getCondutor()==null)     myAuto.setCondutor(new REC.ResponsavelBean());		
//				if  (myAuto.getVeiculo()==null)      myAuto.setVeiculo(new REC.VeiculoBean());		
//				if  (myAuto.getInfracao()==null)     myAuto.setInfracao(new REC.InfracaoBean());		
//				myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
//				myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
//				String sNumRequerimento=rs.getString("NUM_REQUERIMENTO");
//				myAuto.setCodOrgao(myRemessa.getCodOrgao());
//				dao.LeAutoInfracaoLocal(myAuto,"auto");
//				RequerimentoBean myReq=new RequerimentoBean();
//				myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
//				myReq.setNumRequerimento(sNumRequerimento);
//				dao.LeRequerimentoLocal(myReq,conn);
//				myAuto.setNumRequerimento(myReq.getNumRequerimento());							
//				myAuto.setCodRequerimento(myReq.getCodRequerimento());
//				List requerimentos = new ArrayList();
//				requerimentos.add(myReq);
//				myAuto.setRequerimentos(requerimentos);
//				autos.add(myAuto) ;	
//			}			
//			myRemessa.setAutos(autos);	
//			if (rs != null) rs.close();
//			stmt.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Remessa
	 *-----------------------------------------------------------
	 */
	public int RemessaGrava(RemessaBean myRemessa) throws sys.DaoException 
	{
//		Connection conn =null ;
//		List AutosProc = new ArrayList() ;		
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;		
//			Statement stmt = conn.createStatement();			
//			conn.setAutoCommit(false);
//			ResultSet rs   = null;
//			String sCmd    = "";
//			
//			//Indica o sit_xx de acordo com o tipo de remessa
//			String sIndSit = "";
//			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
//					myRemessa.getIndRecurso().equals("97"))
//				sIndSit = "SIT_CEDOC";
//			else 
//				sIndSit = "SIT_PROTOCOLO";
//			
//			if (myRemessa.getCodRemessa().length()>0)
//			{
//				// fazer iterator e pegar todos os processos
//				Iterator it = myRemessa.getAutos().iterator();
//				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
//				Vector vErro = new Vector();
//				while (it.hasNext()) {
//					myAuto = (REC.AutoInfracaoBean)it.next() ;
//					myAuto.setMsgErro("");				
//					sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
//					"DAT_RECEB_GUIA = '"+myRemessa.getDatProcRecebimento()+"',"+
//					"NOM_USERNAME_RECEB_GUIA='"+myRemessa.getUsernameRecebimento()+"'"+
//					" WHERE COD_REQUERIMENTO = '" +myAuto.getCodRequerimento()+"'";					       					
//					stmt.execute(sCmd);
//					AutosProc.add(myAuto);
//				}
//				if (myRemessa.getAutos().size()==AutosProc.size())
//				{
//					sCmd = "UPDATE TSMI_REMESSA SET "+
//					"IND_STATUS_REMESSA='"+myRemessa.getStatusRemessa()+"',"+
//					"USERNAME_RECEBIMENTO='"+myRemessa.getUsernameRecebimento()+"',"+
//					"DAT_PROC_RECEBIMENTO=to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),"+
//					"COD_ORGAO_LOT_RECEBIMENTO='"+myRemessa.getCodOrgaoLotacaoRecebimento()+"'"+				
//					" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
//					stmt.execute(sCmd);
//				}
//			}
//			else
//			{
//				String codRemessa = "";	
//				sCmd    = "SELECT SEQ_TSMI_REMESSA.NEXTVAL COD_REMESSA from dual" ;
//				rs      = stmt.executeQuery(sCmd);
//				while (rs.next())
//				{	
//					codRemessa = rs.getString("COD_REMESSA");
//				}
//				sCmd = "INSERT INTO TSMI_REMESSA (COD_REMESSA,IND_RECURSO, " +
//				"COD_ORGAO,IND_STATUS_REMESSA,USERNAME,COD_ORGAO_LOTACAO, "+
//				"DAT_PROC,USERNAME_RECEBIMENTO,DAT_PROC_RECEBIMENTO,COD_ORGAO_LOT_RECEBIMENTO) "+				
//				" VALUES ("+codRemessa+"," +
//				"'"+myRemessa.getIndRecurso()+"','" +myRemessa.getCodOrgao()+"','"+ myRemessa.getStatusRemessa()+ "'," +
//				"'"+myRemessa.getUsername()+"','" +myRemessa.getCodOrgaoLotacao()+"',"+ 
//				"to_date('"+sys.Util.formatedToday().substring(0,10)+"','DD/MM/YYYY'),'"+myRemessa.getUsernameRecebimento()+"',to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),'"+ myRemessa.getCodOrgaoLotacaoRecebimento()+"')";
//				stmt.execute(sCmd);
//				// fazer iterator e pegar todos os processos
//				Iterator it = myRemessa.getAutos().iterator();
//				myRemessa.setDatProc(sys.Util.formatedToday().substring(0,10));
//				myRemessa.setCodRemessa(codRemessa);					
//				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
//				
//				while (it.hasNext()) {
//					myAuto = (REC.AutoInfracaoBean)it.next() ;
//					myAuto.setMsgErro("");				
//					sCmd = "INSERT INTO TSMI_REMESSA_ITEM (COD_REMESSA,COD_REMESSA_ITEM," +
//					"COD_AUTO_INFRACAO,COD_REQUERIMENTO)" +					
//					" VALUES("+codRemessa+",SEQ_TSMI_REMESSA_ITEM.NEXTVAL," +
//					"'"+myAuto.getCodAutoInfracao()+"','"+myAuto.getCodRequerimento()+"')";					
//					stmt.execute(sCmd) ;					
//					sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
//					"DAT_ENVIO_GUIA=to_date('"+sys.Util.formatedToday().substring(0,10)+"','DD/MM/YYYY')," +
//					"NOM_USERNAME_ENVIO_GUIA = '"+myRemessa.getUsername()+"',COD_REMESSA='" +codRemessa+"'" +
//					" WHERE COD_REQUERIMENTO = '" +myAuto.getCodRequerimento()+"'";					       					
//					stmt.execute(sCmd);
//					AutosProc.add(myAuto);
//				}
//				rs.close();
//			}
//			conn.commit();	
//			myRemessa.setAutos(AutosProc);
//			stmt.close();					
//		}	
//		catch (SQLException sqle) {
//			throw new sys.DaoException(sqle.getMessage());
//		}
//		catch (Exception e) {	
//			throw new sys.DaoException(e.getMessage());
//		}//fim do catch
//		finally {
//			if (conn != null) {
//				try { 
//					conn.rollback();
//					serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}			
//		return AutosProc.size() ;
		return 0;
	}
	
	
	/**
	 *------------------------------------------------------------------------
	 * Insere novo processo na guia de Remessa
	 *------------------------------------------------------------------------
	 */
	public void incluirProcessoRemessa(AutoInfracaoBean myAuto,
			String codRemessa,String cRq,String codEvento,ACSS.UsuarioBean UsrLogado, String existeGuia)
	throws DaoException {	
//		Connection conn = null;
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST); 
//			Statement stmt = conn.createStatement();
//			// iniciar transa��o
//			conn.setAutoCommit(false);
//			DaoBroker dao = DaoBrokerFactory.getInstance();
//			dao.LeRequerimentoLocal(myAuto,conn);
//			RemessaBean myRemessa=new RemessaBean();
//			myRemessa.setCodRemessa(codRemessa);
//			RemessaLe(myRemessa,conn);
//			
//			String sCodreq="";
//			Iterator it = myAuto.getRequerimentos().iterator();		
//			RequerimentoBean myReq=new RequerimentoBean();		
//			while (it.hasNext()) {
//				myReq = (REC.RequerimentoBean)it.next() ;
//				if (myReq.getNumRequerimento().equals(cRq))
//				{
//					sCodreq=myReq.getCodRequerimento();
//					break;
//				}
//			}
//			
//			//Indica o sit_xx de acordo com o tipo de remessa
//			String sIndSit = "";
//			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
//					myRemessa.getIndRecurso().equals("97"))
//				sIndSit = "SIT_CEDOC";
//			else 
//				sIndSit = "SIT_PROTOCOLO";
//			
//			String sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
//			"DAT_ENVIO_GUIA = to_date('"+myRemessa.getDatProc()+"','DD/MM/YYYY'),"+
//			"NOM_USERNAME_ENVIO_GUIA='"+myRemessa.getUsername()+"',"+
//			"DAT_RECEB_GUIA = to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),"+
//			"NOM_USERNAME_RECEB_GUIA='"+myRemessa.getUsernameRecebimento()+"',"+
//			"COD_REMESSA='"+codRemessa+"'"+
//			" WHERE COD_REQUERIMENTO = '"+sCodreq+"'";
//			stmt.execute(sCmd);
//			
//			sCmd =	"INSERT INTO TSMI_REMESSA_ITEM "+
//			"(COD_REMESSA,COD_REMESSA_ITEM,COD_AUTO_INFRACAO,COD_REQUERIMENTO)" +
//			" VALUES (" + codRemessa + ",SEQ_TSMI_REMESSA_ITEM.NEXTVAL,'" + myAuto.getCodAutoInfracao() + "','"+
//			sCodreq+"')";
//			
//			stmt.execute(sCmd);
//			conn.commit();
//			stmt.close();		
//			
//		} catch (SQLException e) {
//			throw new DaoException(e.getMessage());
//		} catch (Exception ex) {
//			throw new DaoException(ex.getMessage());
//		} finally {
//			if (conn != null) {
//				try {
//					conn.rollback();	  
//					serviceloc.setReleaseConnection(conn);
//				} catch (Exception ey) {
//					throw new DaoException(ey.getMessage());
//				}
//			}
//		}
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Consulta Guia de Remessa
	 *-----------------------------------------------------------
	 */	
	public String ConsultaReqRemessa(REC.AutoInfracaoBean myAuto, String codReq) throws sys.DaoException{
		
		Connection conn = null;
		ResultSet rs = null;
		String existe = "";
		String sql = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sql = "Select cod_requerimento from tsmi_requerimento where " +
			" num_requerimento = '"+codReq+"' ";
			rs = stmt.executeQuery(sql);
			String codRequeriemento = "";
			while(rs.next()){
				codRequeriemento = rs.getString("cod_requerimento");
			}
			
			
			sql = "select r.cod_remessa "+
			" FROM tsmi_remessa r, tsmi_remessa_item i " +
			" WHERE i.cod_auto_infracao = '"+ myAuto.getCodAutoInfracao()+"'" ;
			if(!codRequeriemento.equals(""))
				sql += " and i.cod_requerimento = '" +codRequeriemento+"' ";
			sql += " and r.cod_remessa = i.cod_remessa " +
			" and rownum = 1" ;
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				existe = rs.getString("cod_remessa");
			}
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Consulta Guias de Remessa de acordo com Status da guia e tipoJunta
	 *------------------------------------------------------------------------
	 */
	public List consultaRemessaIncluirProcesso(String tipoSolic)throws DaoException {
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_REMESSA WHERE ";    
			if  ("DP,DC".indexOf(tipoSolic)>=0)
				sCmd+= " IND_RECURSO = 5";
			
			if  ("1P,1C".indexOf(tipoSolic)>=0) 
				sCmd+= " IND_RECURSO = 15 ";
			
			if  ("2P,2C".indexOf(tipoSolic)>=0)
				sCmd+= " IND_RECURSO = 35 ";
			
			sCmd += " AND IND_STATUS_REMESSA = 0 ORDER BY COD_REMESSA DESC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				StatusBean mystat = new StatusBean();
				mystat.setCodStatus(rs.getString("COD_REMESSA"));
				mystat.setNomStatus(rs.getString("IND_STATUS_REMESSA"));
				vList.add(mystat) ;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Consulta Guia de Remessa
	 *-----------------------------------------------------------
	 */	
	public void RemessaLe(RemessaBean myRemessa,Connection conn) throws sys.DaoException{
		
		ResultSet rs = null;
		String sCmd  = "";
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM TSMI_REMESSA WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next())
			{
				myRemessa.setCodOrgao(rs.getString("COD_ORGAO"));
				myRemessa.setIndRecurso(rs.getString("IND_RECURSO"));
				myRemessa.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myRemessa.setDatProc(converteData(rs.getDate("DAT_PROC")));
				myRemessa.setDatProcRecebimento(converteData(rs.getDate("DAT_PROC_RECEBIMENTO")));
				myRemessa.setUsername(rs.getString("USERNAME"));
				myRemessa.setUsernameRecebimento(rs.getString("USERNAME_RECEBIMENTO"));
				myRemessa.setCodOrgaoLotacaoRecebimento(rs.getString("COD_ORGAO_LOT_RECEBIMENTO"));
				myRemessa.setStatusRemessa(rs.getString("IND_STATUS_REMESSA"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Ler Guia de Remessa
	 *-----------------------------------------------------------
	 */
	public void RemessaLe(RemessaBean myRemessa,ACSS.UsuarioBean myUsrLog,ACSS.UsuarioFuncBean myUsrFunc) throws sys.DaoException {
//		Connection conn =null ;
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;		
//			Statement stmt = conn.createStatement();
//			
//			String sCmd = "Select IND_RECURSO,USERNAME,COD_ORGAO," +
//			"COD_ORGAO_LOTACAO,TO_CHAR(DAT_PROC,'DD/MM/YYYY') DAT_PROC,"+
//			"TO_CHAR(DAT_PROC_RECEBIMENTO,'DD/MM/YYYY') DAT_PROC_RECEBIMENTO,"+
//			"USERNAME_RECEBIMENTO,COD_ORGAO_LOT_RECEBIMENTO,IND_STATUS_REMESSA " +
//			"FROM TSMI_REMESSA "+
//			"WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"' and "+
//			"COD_ORGAO='"+myRemessa.getCodOrgao()+"'";
//			ResultSet rs   = stmt.executeQuery(sCmd) ;
//			while (rs.next()) {
//				myRemessa.setCodOrgao(rs.getString("COD_ORGAO"));
//				myRemessa.setIndRecurso(rs.getString("IND_RECURSO"));
//				myRemessa.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
//				myRemessa.setDatProc(rs.getString("DAT_PROC"));
//				myRemessa.setDatProcRecebimento(rs.getString("DAT_PROC_RECEBIMENTO"));
//				myRemessa.setUsername(rs.getString("USERNAME"));
//				myRemessa.setUsernameRecebimento(rs.getString("USERNAME_RECEBIMENTO"));
//				myRemessa.setCodOrgaoLotacaoRecebimento(rs.getString("COD_ORGAO_LOT_RECEBIMENTO"));
//				myRemessa.setStatusRemessa(rs.getString("IND_STATUS_REMESSA"));
//				
//			}	
//			String sigOrgao="";
//			String nomOrgao="";
//			sCmd = "Select O.SIG_ORGAO,O.NOM_ORGAO FROM TSMI_ORGAO O "+
//			"WHERE O.COD_ORGAO='"+myRemessa.getCodOrgao()+"' ";			  		              
//			rs   = stmt.executeQuery(sCmd) ;
//			while (rs.next()) {
//				sigOrgao = rs.getString("SIG_ORGAO");	
//				nomOrgao = rs.getString("NOM_ORGAO");				 
//			}
//			
//			ArrayList Autos = new ArrayList();
//			sCmd="SELECT T1.COD_REMESSA_ITEM, "+
//			"T1.COD_AUTO_INFRACAO,T1.COD_REQUERIMENTO,T2.NUM_REQUERIMENTO,T3.NUM_AUTO_INFRACAO,T3.NUM_PLACA "+
//			"FROM SMIT.TSMI_REMESSA_ITEM T1, TSMI_REQUERIMENTO T2,TSMI_AUTO_INFRACAO T3 "+
//			"WHERE T1.COD_REMESSA='"+myRemessa.getCodRemessa()+"' AND T1.COD_REQUERIMENTO=T2.COD_REQUERIMENTO AND "+
//			"T1.COD_AUTO_INFRACAO=T3.COD_AUTO_INFRACAO ";	   
//			if(myRemessa.getGeraGuia().equals("D"))
//				sCmd += " AND T1.SIT_GUIA_DISTRIBUICAO IS NULL";
//			rs   = stmt.executeQuery(sCmd) ;
//			while (rs.next()) {   
//				DaoBroker dao = DaoBrokerFactory.getInstance();
//				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
//				myAuto.setInfracao(new InfracaoBean());	
//				myAuto.setProprietario(new ResponsavelBean());
//				
//				String sCodAutoInfracao=rs.getString("COD_AUTO_INFRACAO");
//				String sCodRequerimento=rs.getString("COD_REQUERIMENTO");
//				String sNumRequerimento=rs.getString("NUM_REQUERIMENTO");
//				String sNumAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");
//				String sNumPlaca=rs.getString("NUM_PLACA");
//				
//				myAuto.setNumAutoInfracao(sNumAutoInfracao);
//				myAuto.setNumPlaca(sNumPlaca);
//				myAuto.setNumRequerimento(sNumRequerimento);
//				myAuto.setCodRequerimento(sCodRequerimento);				
//				myAuto.setCodOrgao(myRemessa.getCodOrgao());	
//				dao.LeAutoInfracaoLocal(myAuto,"auto");
//				
//				List reqs = new ArrayList();
//				REC.RequerimentoBean myReq = new REC.RequerimentoBean();
//				myReq.setCodAutoInfracao(sCodAutoInfracao);
//				myReq.setNumRequerimento(sNumRequerimento);
//				dao.LeRequerimentoLocal(myReq,conn);
//				reqs.add(myReq);
//				myAuto.setRequerimentos(reqs);
//				
//				Autos.add(myAuto);
//			}
//			myRemessa.setAutos(Autos);
//			rs.close();
//			stmt.close();					
//		}	
//		catch (SQLException sqle) {
//			throw new sys.DaoException(sqle.getMessage());
//		}
//		catch (Exception e) {	
//			throw new sys.DaoException(e.getMessage());
//		}//fim do catch
//		finally {
//			if (conn != null) {
//				try { 
//					conn.rollback();
//					serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}			
//		return;
	}
	
	/**
	 * Le a Guia e os Itens da Guia no Oracle pelo n�mero enviado como parametro e alimenta o pr�prio Bean
	 * @author jefferson.trindade
	 */
	public void GuiaLe(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt  = conn.createStatement();
			Statement stmt1 = conn.createStatement();
			ResultSet rs    = null;			
			ResultSet rs1   = null;			
			DaoBroker dao = DaoBrokerFactory.getInstance();
			String codItemRelator="0";
			
			String sCmd = "Select L.COD_STATUS,L.COD_LOTE_RELATOR,L.NOM_USERNAME, " +
			"L.COD_ORGAO_LOTACAO,to_char(L.DT_PROC,'dd/mm/yyyy') DT_PROC,"+
			"to_char(L.DAT_RS,'dd/mm/yyyy') DAT_RS,"+
			"L.COD_ORGAO,L.COD_JUNTA,J.SIG_JUNTA,J.IND_TIPO," +
			"L.NUM_CPF,R.COD_RELATOR,R.NOM_RELATOR,L.COD_ENVIO_PUBDO," +
			"to_char(L.DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO, L.TIPO_GUIA," +
			"to_char(L.DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO, L.NUM_SESSAO,to_char(L.DAT_SESSAO,'DD/MM/YYYY') DAT_SESSAO " +
			"FROM SMIT_PNT.TPNT_LOTE_RELATOR L, TPNT_RELATOR R,TPNT_JUNTA J "+
			"WHERE L.COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' and "+
			"L.COD_JUNTA=J.COD_JUNTA AND  "+
			"L.NUM_CPF=R.NUM_CPF AND L.COD_JUNTA=R.COD_JUNTA " ;		              
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myGuia.setCodStatus(rs.getString("COD_STATUS"));	
				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));	
				
				myGuia.setDatProc(rs.getString("DT_PROC"));	
				myGuia.setDatRS(rs.getString("DAT_RS"));	
				myGuia.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				
				myGuia.setCodJunta(rs.getString("COD_JUNTA"));
				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
				myGuia.setTipoJunta(rs.getString("IND_TIPO"));
				
				myGuia.setNumCPFRelator(rs.getString("NUM_CPF"));
				myGuia.setCodRelator(rs.getString("COD_RELATOR"));
				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				myGuia.setNumSessao(rs.getString("NUM_SESSAO"));
				myGuia.setDatSessao(rs.getString("DAT_SESSAO"));
				myGuia.setTipoGuia(rs.getString("TIPO_GUIA"));
			}
			
			/**
			if(myGuia.getNumSessao().length()>0)
			{
				sCmd = "SELECT to_char(DAT_SESSAO,'dd/mm/yyyy') DAT_SESSAO FROM SMIT_PNT.TPNT_LOTE_RELATOR WHERE "+
				" NUM_SESSAO ='"+myGuia.getNumSessao()+"' ";                               
				rs   = stmt.executeQuery(sCmd) ;
				while (rs.next()) 
					myGuia.setDatSessao(rs.getString("DAT_SESSAO"));              
			}
			**/
			
			String sigOrgao="";
			String nomOrgao="";
			sCmd = "Select O.SIG_ORGAO,O.NOM_ORGAO FROM TSMI_ORGAO O "+
			"WHERE O.COD_ORGAO='"+myGuia.getCodOrgaoAtuacao()+"' ";			  		              
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				sigOrgao = rs.getString("SIG_ORGAO");	
				nomOrgao = rs.getString("NOM_ORGAO");				 
			}
			
			ArrayList Processos = new ArrayList() ;
			sCmd = "Select COD_ITEM_LOTE_RELATOR," +
			"NUM_PROCESSO,to_char(DAT_PROCESSO,'dd/mm/yyyy') DAT_PROCESSO, " +
			"NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO," +
			"MSG_ERRO, " +
			"COD_RESULT_RS,TXT_MOTIVO_RS "+
			"FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR "+
			"WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'"+
			" order by NUM_PROCESSO ASC";
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				ProcessoBean myProcesso  = new ProcessoBean();
				RequerimentoBean req = new RequerimentoBean();	
				
				myProcesso.setCodOrgao(myGuia.getCodOrgaoAtuacao());
				myProcesso.setSigOrgao(sigOrgao);
				myProcesso.setNumProcesso(rs.getString("NUM_PROCESSO"));	
				myProcesso.setDatProcesso(rs.getString("DAT_PROCESSO"));	 
				myProcesso.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				myProcesso.setTipoRequerimento(rs.getString("IND_TIPO_REQUERIMENTO"));
				myProcesso.setMsgErro(rs.getString("MSG_ERRO"));

				req.setCodResultRS(rs.getString("COD_RESULT_RS"));
				req.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				req.setNumRequerimento(myProcesso.getNumRequerimento());
				
				dao.LeRequerimentos(myProcesso);				
				dao.LeProcesso(myProcesso);

				Processos.add(myProcesso);
			}
			myGuia.setProcessos(Processos);
			rs.close();
			if (rs1!=null) rs1.close();
			stmt.close();
			stmt1.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao
	 *-----------------------------------------------------------
	 */
	
	public int GuiaGrava(GuiaDistribuicaoBean myGuia, UsuarioBean UsrLogado, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		String sCmd = "";
		int inc = 0;
		try {
			if (myGuia.getProcessos().size()==0) return 0;
			
			conn = serviceloc.getConnection(MYABREVSIST);		
			Statement stmt = conn.createStatement();			
			
			conn.setAutoCommit(false);	
			
			sCmd    = "Select SMIT_PNT.SEQ_PNT_LOTE_RELATOR.NEXTVAL codLote from dual" ;
			String codLote = "0";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				codLote = rs.getString("codLote");	 
			}
			myGuia.setNumGuia(codLote);
			
			sCmd = "INSERT INTO SMIT_PNT.TPNT_LOTE_RELATOR (COD_LOTE_RELATOR,COD_STATUS, " +
			"NOM_USERNAME, COD_ORGAO_LOTACAO, DT_PROC,COD_ORGAO, "+
			"COD_JUNTA, NUM_CPF, DSC_TIPOS_REQUERIMENTO,NUM_SESSAO,DAT_SESSAO,TIPO_GUIA) "+				
			" VALUES ("+codLote+",'2'," +
			"'"+myGuia.getNomUserName()+"','" + myGuia.getCodOrgaoLotacao()+ "'," +
			"to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),'"+myGuia.getCodOrgaoAtuacao()+"',"+
			"'"+myGuia.getCodJunta()+"','" + myGuia.getNumCPFRelator()+ "','"+myGuia.getTipoReqValidos()+"','"+myGuia.getNumSessao()+"'" +
			",to_date('"+myGuia.getDatSessao()+"','dd/mm/yyyy'),'"+myGuia.getTipoGuia()+"')" ;
			
			if (stmt.execute(sCmd)){
				System.out.println("Lote "+codLote + "inserido!");
			}else{
				System.out.println("Lote n�o inserido. Lote atual: "+codLote);
			}
			
			// fazer iterator e pegar todos os processos
			Iterator<ProcessoBean> it = myGuia.getProcessos().iterator();
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProc  = new PNT.ProcessoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int s = 0;    
			List<ProcessoBean> processosProc = new ArrayList<ProcessoBean>() ;
			while (it.hasNext()) {
				myProc = it.next();
				/*Gravar dados do Auto de Infra��o*/
				//VerificaAutoOracle(myProc,conn);
				myProc.LeProcesso();
				myProc.setMsgErro("");
				// Gera transacao 811/841/871 para cada Processo							
				parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)  +
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3) +
						sys.Util.lPad("101","0",3) +
						sys.Util.rPad(myProc.getNumProcesso()," ",20) +
						sys.Util.lPad(myProc.getNumCPF(),"0",11) +	
						sys.Util.rPad(myProc.getNumRequerimento()," ",20) +
						sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
						sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)+	
						sys.Util.rPad(myGuia.getSigJunta()," ",10) +
						sys.Util.lPad(funcionalidadesBean.getTipoJunta(),"0",1) +
						sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);
						/*   
						sys.Util.rPad(myProc.getNumPlaca()," ",7) +
						sys.Util.lPad(myProc.getCodOrgao(),"0",6) +
						sys.Util.rPad(myProc.getNumProcesso()," ",20) +
						sys.Util.lPad(myProc.getCodStatus(),"0",3) +
						sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
						sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
						sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
						sys.Util.rPad(myProc.getNumRequerimento()," ",30) +							
						sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
						sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
						sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);			
						*/
				resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
				
				if("000".equals(resultado.substring(0,3))){
					int qtdAtualProc=0;					
					sCmd = "SELECT QTD_PROC_DISTRIB FROM SMIT_PNT.TPNT_RELATOR WHERE COD_RELATOR ='"+myGuia.getCodRelator()+"'";
					rs   = stmt.executeQuery(sCmd) ;
					if(rs.next())
						qtdAtualProc = rs.getInt("QTD_PROC_DISTRIB")+1;		
						
					sCmd = "UPDATE SMIT_PNT.TPNT_RELATOR  SET QTD_PROC_DISTRIB = "+qtdAtualProc+" WHERE COD_RELATOR ='"+myGuia.getCodRelator()+"'";				
					stmt.execute(sCmd);
				}
				
				if (verErro(myProc,resultado,"Processo: "+myProc.getNumProcesso()+" Erro na grava��o de Definir/Troca de Relator.")==false) {
					vErro.add(myProc.getMsgErro());
				} else {
					inc++;					
					sCmd = "INSERT INTO SMIT_PNT.TPNT_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR," +
					"NUM_PROCESSO,DAT_PROCESSO,"+					
					"NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO,"+
					"MSG_ERRO) " +					
					" VALUES("+codLote+",SMIT_PNT.SEQ_PNT_ITEM_LOTE_RELATOR.NEXTVAL," +
					"'"+myProc.getNumProcesso()+"',"+
					"to_date('"+myProc.getDatProcesso()+"','dd/mm/yyyy'),"+
					"'"+myProc.getNumRequerimento()+"','" +funcionalidadesBean.getTipoRequerimento()+"',"+
					"'"+myProc.getMsgErro()+"')" ;					
					
					stmt.execute(sCmd) ;

					DaoBroker dao = DaoBrokerFactory.getInstance();
					//dao.LeAutoInfracaoLocal(myProc,"auto");	

					HistoricoBean myHis = new HistoricoBean() ; 
					myHis.setNumProcesso(myProc.getNumProcesso());
					myHis.setCodStatus(myProc.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myProc.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento03(myGuia.getCodJunta());	
					myHis.setTxtComplemento04(myGuia.getNumCPFRelator());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setNumRequerimento(myProc.getNumRequerimento());
					//dao.LeRequerimentoLocal(myReq,conn);							
					myProc.LeRequerimentos();
					myReq.setNumRequerimento(myProc.getNumRequerimento());
					
					Iterator<RequerimentoBean> itReq = myProc.getRequerimentos().iterator();
					RequerimentoBean myRequerimento = new RequerimentoBean();
					while (itReq.hasNext())
					{
						myRequerimento = (RequerimentoBean)itReq.next();
						if (myRequerimento.getNumRequerimento().equals(myReq.getNumRequerimento()))
						{
							myReq.setCodTipoSolic(myRequerimento.getCodTipoSolic());
							myReq.setDatRequerimento(myRequerimento.getDatRequerimento());
							myReq.setNomUserNameDP(myRequerimento.getNomUserNameDP());
							myReq.setCodOrgaoLotacaoDP(myRequerimento.getCodOrgaoLotacaoDP());
							myReq.setDatProcDP(myRequerimento.getDatProcDP());
							break;
						}
					}
					myReq.setNomUserNameJU(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("2");
					myReq.setCodOrgaoLotacaoJU(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcJU(myGuia.getDatProc());
					myReq.setDatJU(myGuia.getDatProc());	
					myReq.setCodJuntaJU(myGuia.getCodJunta());	
					myReq.setCodRelatorJU(myGuia.getNumCPFRelator());

					/*Atualizar Dados de Remessa*/
					/*
					if(myGuia.getTipoGuia().equals("R"))
					{
						RemessaBean myRemessa = new RemessaBean();
						sCmd = "UPDATE TSMI_REMESSA_ITEM  SET SIT_GUIA_DISTRIBUICAO = '" +myRemessa.SIT_GUIA_DISTRIBUICAO+"'"+
						"WHERE COD_REMESSA ='"+myGuia.getNumGuia()+"' AND COD_AUTO_INFRACAO = "+myProc.getCodAutoInfracao()+
						" AND COD_REQUERIMENTO = "+myReq.getCodRequerimento();
						stmt.execute(sCmd);
					}
					*/
					
                    /*Gravar Auto de Infracao*/					
//					dao.GravaAutoInfracaoLocal(myProc, conn);
					if (myProc.getNumProcesso().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravaRequerimentoGuia(myReq,conn);
						/*Gravar Historico*/ 
						dao.GravaHistorico(myHis, conn);
					}
				}
				processosProc.add(myProc);
			}	
			if (inc==0) conn.rollback();	
			else 		conn.commit();
			myGuia.setMsgErro(vErro);				
			myGuia.setProcessos(processosProc);
			rs.close();
			stmt.close();					
		}
		catch (SQLException sqle) {
			System.out.println("* SQL Inv�lido :"+sCmd);			
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc ;
	}
	
	public int AtualizarGuia(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;		
//			Statement stmt = conn.createStatement();			
//			
//			conn.setAutoCommit(false);	
//			
//			// fazer iterator e pegar todos os processos
//			Iterator it = myGuia.getAutos().iterator() ;
//			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
//			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
//			String parteVar  = "" ;
//			String resultado = "";
//			Vector vErro = new Vector();
//			int s = 0;    
//			List AutosProc = new ArrayList() ;
//			while (it.hasNext()) {
//				myAuto = (REC.AutoInfracaoBean)it.next() ;
//				myAuto.setMsgErro("");
//				// Gera transacao 208/328/335 para cada Auto			
//				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
//				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
//				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
//				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
//				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
//				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
//				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
//				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
//				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
//				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
//				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
//				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
//				sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
//				sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);			
//				
//				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
//				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Definir/Troca de Relator.")==false) {
//					vErro.add(myAuto.getMsgErro());
//				}
//				else {
//					inc++;					
//					String sCmd = "INSERT INTO TSMI_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR," +
//					"NUM_AUTO_INFRACAO,NUM_PROCESSO,DAT_PROCESSO,NUM_PLACA,"+
//					"NOM_PROPRIETARIO,DSC_ENQUADRAMENTO,"+					
//					"DAT_INFRACAO,DSC_INFRACAO,DSC_LOCAL_INFRACAO,NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO,"+
//					"MSG_ERRO) " +					
//					" VALUES("+myGuia.getNumGuia()+",SEQ_TSMI_ITEM_LOTE_RELATOR.NEXTVAL," +
//					"'"+myAuto.getNumAutoInfracao()+"','"+myAuto.getNumProcesso()+"',"+
//					"to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),"+
//					"'"+myAuto.getNumPlaca()+"'," +
//					"'"+myAuto.getProprietario().getNomResponsavel()+"','"+myAuto.getInfracao().getDscEnquadramento()+"'," +					
//					"to_date('"+myAuto.getDatInfracao()+"','dd/mm/yyyy'),"+						
//					"'"+myAuto.getInfracao().getDscInfracao()+"'," +
//					"'"+myAuto.getDscLocalInfracao()+"'," +
//					"'"+myAuto.getNumRequerimento()+"','" +myAuto.getTpRequerimento()+"',"+
//					"'"+myAuto.getMsgErro()+"')" ;	
//					stmt.execute(sCmd) ;
//					
//					DaoBroker dao = DaoBrokerFactory.getInstance();
//					dao.LeAutoInfracaoLocal(myAuto,"auto");					
//					HistoricoBean myHis = new HistoricoBean() ;
//					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
//					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
//					myHis.setNumProcesso(myAuto.getNumProcesso());
//					myHis.setCodStatus(myAuto.getCodStatus());  
//					myHis.setDatStatus(myGuia.getDatProc());
//					myHis.setCodEvento(myGuia.getCodEvento());
//					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
//					myHis.setNomUserName(myGuia.getNomUserName());
//					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
//					myHis.setDatProc(myGuia.getDatProc());
//					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
//					myHis.setTxtComplemento02(myGuia.getDatProc());	
//					myHis.setTxtComplemento03(myGuia.getCodJunta());	
//					myHis.setTxtComplemento04(myGuia.getNumCPFRelator());
//					
//					//preparar Bean de Requerimento
//					RequerimentoBean myReq = new RequerimentoBean();
//					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
//					myReq.setNumRequerimento(myAuto.getNumRequerimento());	
//					dao.LeRequerimentoLocal(myReq,conn);
//					myReq.setNomUserNameJU(myGuia.getNomUserName());
//					myReq.setCodStatusRequerimento("2");
//					myReq.setCodOrgaoLotacaoJU(myGuia.getCodOrgaoLotacao());
//					myReq.setDatProcJU(myGuia.getDatProc());
//					myReq.setDatJU(myGuia.getDatProc());	
//					myReq.setCodJuntaJU(myGuia.getCodJunta());	
//					myReq.setCodRelatorJU(myGuia.getNumCPFRelator());
//					
//					dao.GravaAutoInfracaoLocal(myAuto, conn);
//					if (myAuto.getCodAutoInfracao().length()>0)
//					{
//						/*Gravar Requerimento*/
//						dao.GravarRequerimento(myReq,conn);
//						/*Gravar Historico*/
//						dao.GravarHistorico(myHis,conn); 
//					}
//				}
//				AutosProc.add(myAuto);
//			}	
//			if (inc==0) conn.rollback();	
//			else 		conn.commit();
//			myGuia.setMsgErro(vErro);				
//			myGuia.setAutos(AutosProc);
//			stmt.close();					
//		}	
//		catch (SQLException sqle) {
//			throw new sys.DaoException(sqle.getMessage());
//		}
//		catch (Exception e) {	
//			throw new sys.DaoException(e.getMessage());
//		}//fim do catch
//		finally {
//			if (conn != null) {
//				try { 
//					conn.rollback();
//					serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}			
		return inc ;
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Resultado
	 *-----------------------------------------------------------
	 */
	public int GuiaGravaResult(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			
			
			String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='3', " +
			" NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO='"+myGuia.getCodOrgaoLotacao()+"',"+
			" DAT_RS= to_date('"+myGuia.getDatRS()+"','dd/mm/yyyy'), "+
			" DAT_SESSAO = to_date('"+myGuia.getDatSessao()+"','dd/mm/yyyy') "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getProcessos().iterator() ;		
			PNT.ProcessoBean myProcesso        = new PNT.ProcessoBean();		
			PNT.ProcessoBean myProcessoBroker  = new PNT.ProcessoBean();			
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				
				// Gera transacao 812/842/872 para cada Processo	
				parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)  + 
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3) +
						sys.Util.lPad("101","0",3) +
						sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
						sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
						sys.Util.rPad(myProcesso.getNumRequerimento()," ",20) +
						sys.Util.lPad(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getCodResultRS(),"0",1) +
						sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatRS()),"0",8)+			
						sys.Util.rPad(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getTxtMotivoRS()," ",1000) +
						sys.Util.lPad(myProcesso.getCodPenalidade(),"0",3) +
						sys.Util.rPad(myProcesso.getDscPenalidade()," ",100) ;
						resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
				
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o do Resultado.")==false) {
					vErro.add(myProcesso.getMsgErro());
				}
				else {
					inc++;	
					
					sCmd = "UPDATE SMIT_PNT.TPNT_ITEM_LOTE_RELATOR set " +
					"COD_RESULT_RS='"+myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getCodResultRS()+"'," +
					"TXT_MOTIVO_RS='"+myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getTxtMotivoRS()+"' "+
					"WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' and " + 
					"COD_ITEM_LOTE_RELATOR='"+myProcesso.getCodProcesso()+"'" ;
					//System.out.println("GuiaDaoBroker: "+sCmd);
					stmt.execute(sCmd);
					
					
					DaoBroker dao = DaoBrokerFactory.getInstance();
					HistoricoBean myHis = new HistoricoBean() ; 
					myHis.setNumProcesso(myProcesso.getNumProcesso());
					myHis.setCodStatus(myProcesso.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myProcesso.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatRS());	
					myHis.setTxtComplemento03(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getCodResultRS());	
					myHis.setTxtComplemento04(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getTxtMotivoRS());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setNumRequerimento(myProcesso.getNumRequerimento());	
					myReq.setCodProcesso(myProcesso.getCodProcesso()); 
					myReq.setNomUserNameRS(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("3");
					myReq.setCodOrgaoLotacaoRS(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcRS(myGuia.getDatProc());
					myReq.setNumRequerimento(myProcesso.getNumRequerimento());	
					myReq.setDatRS(myGuia.getDatRS());
					myReq.setCodResultRS(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getCodResultRS());	
					myReq.setTxtMotivoRS(myProcesso.getRequerimentos(myProcesso.getNumRequerimento(),"numRequerimento").getTxtMotivoRS());


					if (myProcesso.getNumProcesso().length()>0)
					{
						/*Gravar Requerimento*/
						dao.AtualizarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravaHistorico(myHis,conn); 
					}
				}
			}	

			myGuia.setMsgErro(vErro);
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc ;
	}

	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Alterar Relator
	 * @author Rodrigo.fonseca
	 * @since 25/04/2012
	 *-----------------------------------------------------------
	 */
	public int GuiaGravaTrocaRelator(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBeanId) throws sys.DaoException {
		Connection conn =null ;
		int indicadorErro =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='"+myGuia.getCodStatus()+"', " +
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
			" COD_JUNTA         = '"+myGuia.getCodJunta()+"',"+
			" NUM_CPF           = '"+myGuia.getNumCPFRelator()+"' "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getProcessos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				
				/**
				 * FIXME: Implementa��o para executar a transa��o de pontua��o da Classe PNT.DaoAdabas.java metodo: DefinirRelator()
				 */
				parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)  +
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(funcionalidadesBeanId.getCodEvento(),"0",3) +
						sys.Util.lPad("101","0",3) +
						sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
						sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
						sys.Util.rPad(myProcesso.getNumRequerimento()," ",20) +
						sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
						sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)+	
						sys.Util.rPad(myGuia.getSigJunta()," ",10) +
						sys.Util.lPad(funcionalidadesBeanId.getTipoJunta(),"0",1) +
						sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);
						resultado = chamaTransBRK(funcionalidadesBeanId.getCodEvento(),parteVar,"");				
				
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Troca de Relator.")==false) {
					vErro.add(myProcesso.getMsgErro());
					indicadorErro++;
				}
			}
			if (indicadorErro==0) conn.commit();	
			else 		conn.rollback();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return indicadorErro;
	}
	
	
	
	public void RemessaEstorno(RemessaBean myRemessa) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			conn.setAutoCommit(false);
			
			String sCmd = "UPDATE TSMI_REQUERIMENTO set COD_REMESSA=null,SIT_PROTOCOLO=null "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			sCmd = "DELETE FROM TSMI_REMESSA_ITEM "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			sCmd = "DELETE FROM TSMI_REMESSA "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			conn.commit();		
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	
	
	
	public void RemessaEstornoRecebimento(RemessaBean myRemessa) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_REMESSA set IND_STATUS_REMESSA='0'," +
			"USERNAME_RECEBIMENTO=null,"+                  
			"DAT_PROC_RECEBIMENTO=null,"+                   
			"COD_ORGAO_LOT_RECEBIMENTO=null "+             
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			conn.commit();		
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	
	
	
	
	
	
	public int GuiaEstornoTrocaRelator(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;		
//			Statement stmt = conn.createStatement();		
//			
//			conn.setAutoCommit(false);	
//			
//			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='8', " +
//			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
//			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
//			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
//			" COD_JUNTA         = '"+myGuia.getCodJunta()+"',"+
//			" NUM_CPF           = '"+myGuia.getNumCPFRelator()+"' "+
//			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
//			stmt.execute(sCmd) ;
//			// fazer iterator e pegar todos os processos
//			Iterator it = myGuia.getAutos().iterator() ;
//			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
//			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
//			String parteVar  = "" ;
//			String resultado = "";
//			Vector vErro = new Vector();
//			while (it.hasNext()) {
//				myAuto = (REC.AutoInfracaoBean)it.next() ;
//				myAuto.setMsgErro("");
//				// Gera transacao 208/328/335 para cada Auto			
//				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
//				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
//				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
//				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
//				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
//				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
//				sys.Util.lPad("215","0",3) +																																			  
//				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
//				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
//				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
//				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
//				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
//				sys.Util.rPad(" "," ",50) +
//				sys.Util.rPad("ESTORNO DE JUNTA/RELATOR"," ",11) ;			 			
//				resultado = chamaTransBRK("215",parteVar,"");
//				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Troca de Junta/Relator.")==false) {
//					vErro.add(myAuto.getMsgErro());
//				}
//				else {	inc++;	}
//			}
//			if (inc==0) conn.rollback();	
//			else 		conn.commit();		
//			myGuia.setMsgErro(vErro);
//			stmt.close();	
//		}
//		catch (SQLException sqle) {
//			throw new sys.DaoException(sqle.getMessage());
//		}
//		catch (Exception e) {	
//			throw new sys.DaoException(e.getMessage());
//		}//fim do catch
//		finally {
//			if (conn != null) {
//				try { 
//					conn.rollback();
//					serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}			
		return inc;
	}
	/**
	 * Consulta Guias de acordo com Status da guia e tipoJunta
	 * @author jefferson.trindade
	 */
	public List<StatusBean> consultaGuiaIncluirProcesso(GuiaDistribuicaoBean GuiaDistribuicaoId, String statusGuia, String tipoJunta, String numCPF, String existeGuia,UsuarioBean UsrLogado)
	throws DaoException {
		List<StatusBean> vList = new ArrayList<StatusBean>();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String filtro = " AND L.COD_STATUS=" + statusGuia + " AND J.IND_TIPO=" + tipoJunta;
			
			String sCmd =
				"SELECT COD_LOTE_RELATOR,"
				+ "' Guia: '||L.cod_lote_relator||'  '||rpad(j.sig_junta,10,' ')||'-'||r.nom_relator||"
				+ "' Status: '||decode(L.cod_status,2,'Def. Junta/Relator',3,'Resultado',4,'Enviado p/ Publica��o',8,'Estornado','Publicacado')  NUM_GUIA "
				+ " FROM SMIT_PNT.TPNT_LOTE_RELATOR L, SMIT_PNT.TPNT_RELATOR R, TPNT_JUNTA J";
//			if (GuiaDistribuicaoId.getControleGuia().equals("S"))
//				sCmd +=  ",TSMI_CONTROLE_RELATOR C ";
			sCmd += " WHERE L.COD_JUNTA=J.COD_JUNTA " +
					//"AND l.COD_ORGAO='"+UsrLogado.getCodOrgaoAtuacao() +"' " +
							""
			+ " AND L.NUM_CPF=R.NUM_CPF"
			+ " AND L.COD_JUNTA=R.COD_JUNTA AND R.IND_ATIVO = 'S' ";
			
//			if (GuiaDistribuicaoId.getControleGuia().equals("S"))
//			{
//				sCmd += " AND C.COD_RELATOR = R.COD_RELATOR" +
//				" AND C.COD_USUARIO = "+UsrLogado.getCodUsuario();
//			}
			
			if (!numCPF.equals("")){ 
				sCmd = sCmd + " AND L.NUM_CPF='"+numCPF+"'";
			}
			if (!existeGuia.equals("")){ 
				sCmd = sCmd + " AND L.cod_lote_relator not in ("+existeGuia+")";
			}
			sCmd = sCmd + filtro;
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				StatusBean mystat = new StatusBean();
				mystat.setCodStatus(rs.getString("COD_LOTE_RELATOR"));
				mystat.setNomStatus(rs.getString("NUM_GUIA"));
				vList.add(mystat) ;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Insere novo processo na guia
	 * @author Elisson Dias
	 * Date: 13/09/2004 - 11:38
	 *------------------------------------------------------------------------
	 */
	
	public void incluirProcessoGuia(ProcessoBean myProcesso,
			String codLoteRelator,String cRq,String codEvento,ACSS.UsuarioBean UsrLogado, String existeGuia, FuncionalidadesBean funcionalidadesBean)
	throws DaoException {	
		Connection conn = null;
		boolean Ok = true;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST); 
			String sCmd ="SELECT COD_JUNTA,NUM_CPF FROM SMIT_PNT.TPNT_LOTE_RELATOR" +
			" WHERE COD_LOTE_RELATOR='" + codLoteRelator + "'" ;
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			String cjunta="";
			String ccpf = "";
			while (rs.next()) {
				cjunta = rs.getString("COD_JUNTA");
				if (cjunta==null) cjunta="";
				ccpf = rs.getString("NUM_CPF");
				if (ccpf==null) ccpf="";
			}
			
			sCmd = "SELECT SIG_JUNTA FROM SMIT_PNT.TPNT_JUNTA WHERE COD_JUNTA = '"+cjunta+"'";
			rs = stmt.executeQuery(sCmd);
			
			String cSiglaJunta = "";
			while (rs.next()) {
				cSiglaJunta = rs.getString("SIG_JUNTA");
				if (cSiglaJunta==null) cSiglaJunta="";
			}
			
			rs.close();		
			
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int s = 0;    
			
			if (myProcesso.getRequerimentos(cRq,"numRequerimento").getCodStatusRequerimento().equals("0") ||		
					myProcesso.getRequerimentos(cRq,"numRequerimento").getCodStatusRequerimento().equals("1") ||		
					myProcesso.getRequerimentos(cRq,"numRequerimento").getCodStatusRequerimento().equals("2")){		
			
				myProcesso.LeProcesso();
				myProcesso.setMsgErro("");
				// Gera transacao 811/841/871 para cada Processo							
				parteVar = sys.Util.lPad(sys.Util.formatedToday().substring(0,10),"0",8)  +
						sys.Util.lPad(UsrLogado.getCodOrgaoAtuacao(),"0",6) +
						sys.Util.rPad(UsrLogado.getNomUserName()," ",20) +
						sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3) +
						sys.Util.lPad("101","0",3) +
						sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
						sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
						sys.Util.rPad(myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento()," ",20) +
						sys.Util.lPad(cjunta,"0",6) +
						sys.Util.lPad(sys.Util.formatedToday().substring(0,10),"0",8)+	
						sys.Util.rPad(cSiglaJunta," ",10) +
						sys.Util.lPad(funcionalidadesBean.getTipoJunta(),"0",1) +
						sys.Util.lPad(ccpf,"0",11);

				resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
								
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Incluir Processo na Guia.")==false) {
					vErro.add(myProcesso.getMsgErro());
					Ok = false;
				}
			}
			if (existeGuia.length()>0) {
				//			System.out.println("Existe Guia e para Deletar");
				sCmd =	"DELETE FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR WHERE COD_LOTE_RELATOR = "+existeGuia+" AND NUM_PROCESSO = '"+myProcesso.getNumProcesso()+"'"+ 
				" AND NUM_REQUERIMENTO = '"+myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento()+"'";
				stmt.execute(sCmd) ;
			}
			
			sCmd = "INSERT INTO SMIT_PNT.TPNT_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR," +
					"NUM_PROCESSO,DAT_PROCESSO,"+					
					"NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO,"+
					"MSG_ERRO) " +					
					" VALUES("+codLoteRelator+",SMIT_PNT.SEQ_PNT_ITEM_LOTE_RELATOR.NEXTVAL," +
					"'"+myProcesso.getNumProcesso()+"',"+
					"to_date('"+myProcesso.getDatProcesso()+"','dd/mm/yyyy'),"+
					"'"+myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento()+"','" +funcionalidadesBean.getTipoRequerimento()+"',"+
					"'"+myProcesso.getMsgErro()+"')" ;
			
			stmt.execute(sCmd) ;
			
			HistoricoBean myHis = new HistoricoBean() ; 
			myHis.setNumProcesso(myProcesso.getNumProcesso());
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(sys.Util.formatedToday().substring(0,10));
			myHis.setCodEvento(funcionalidadesBean.getCodEvento());
			myHis.setCodOrigemEvento("101");		
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			myHis.setTxtComplemento01(myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento());	
			myHis.setTxtComplemento02(sys.Util.formatedToday().substring(0,10));	
			myHis.setTxtComplemento03(cjunta);	
			myHis.setTxtComplemento04(ccpf);
			
			DaoBroker dao = DaoBrokerFactory.getInstance();
			
			//preparar Bean de Requerimento
			RequerimentoBean myReq = new RequerimentoBean();
			myReq.setNumRequerimento(myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento());
			//dao.LeRequerimentoLocal(myReq,conn);							
			myProcesso.LeRequerimentos();
			myReq.setNumRequerimento(myProcesso.getRequerimentos(cRq,"numRequerimento").getNumRequerimento());
			
			Iterator<RequerimentoBean> itReq = myProcesso.getRequerimentos().iterator();
			RequerimentoBean myRequerimento = new RequerimentoBean();
			while (itReq.hasNext())
			{
				myRequerimento = (RequerimentoBean)itReq.next();
				if (myRequerimento.getNumRequerimento().equals(myReq.getNumRequerimento()))
				{
					myReq.setCodTipoSolic(myRequerimento.getCodTipoSolic());
					myReq.setDatRequerimento(myRequerimento.getDatRequerimento());
					myReq.setNomUserNameDP(myRequerimento.getNomUserNameDP());
					myReq.setCodOrgaoLotacaoDP(myRequerimento.getCodOrgaoLotacaoDP());
					myReq.setDatProcDP(myRequerimento.getDatProcDP());
					break;
				}
			}
			myReq.setNomUserNameJU(UsrLogado.getNomUserName());
			myReq.setCodStatusRequerimento("2");
			myReq.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcJU(sys.Util.formatedToday().substring(0,10));
			myReq.setDatJU(sys.Util.formatedToday().substring(0,10));	
			myReq.setCodJuntaJU(cjunta);	
			myReq.setCodRelatorJU(ccpf);

			if (myProcesso.getNumProcesso().length()>0)
			{
				/*Gravar Requerimento*/
				dao.GravaRequerimentoGuia(myReq,conn);
				/*Gravar Historico*/ 
				dao.GravaHistorico(myHis, conn);
			}
			
			// fechar a transa��o - commit ou rollback
			if (Ok) conn.commit();
			else 	conn.rollback();
			stmt.close();		
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Enviar DO
	 *-----------------------------------------------------------
	 */
	public int GuiaEnviaDO(GuiaDistribuicaoBean GuiaDistribuicaoId, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException 
	{
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			conn.setAutoCommit(false);	
			GuiaDistribuicaoBean myGuia = null;
			List listProcessos = null;
			for(int x=0;x<GuiaDistribuicaoId.getRelatGuias().size();x++){
				myGuia = (GuiaDistribuicaoBean)GuiaDistribuicaoId.getRelatGuias().get(x);
				if(myGuia.getProcessos().size()>0){
					
					String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR SET COD_STATUS='4'," +
					"NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
					"COD_ORGAO_LOTACAO='"+ myGuia.getCodOrgaoLotacao()+ "'," +
					" DT_PROC=to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),"+				
					"COD_ENVIO_PUBDO='"+ myGuia.getCodEnvioDO()+ "'," +
					"DAT_ENVIO_PUBDO=to_date('"+myGuia.getDatEnvioDO()+"','dd/mm/yyyy') " +
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'" ;
					stmt.execute(sCmd) ;
					
					// fazer iterator e pegar todos os processos
//					Iterator it = myGuia.getProcessosComRecursoIndeferidos().iterator() ;	
					Iterator it = myGuia.getProcessos(funcionalidadesBean.getTipoRequerimento()).iterator();
					myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
					PNT.ProcessoBean myProc  = new PNT.ProcessoBean();
					PNT.RequerimentoBean myReq   = new PNT.RequerimentoBean();
					String parteVar  = "" ;
					String resultado = "" ;
					
					String parteDo   =sys.Util.formataDataYYYYMMDD(myGuia.getDatEnvioDO()) + 
							sys.Util.rPad(myGuia.getCodEnvioDO()," ",20) +
							sys.Util.rPad(myGuia.getNomUserName()," ",20);
					
//					int nauto = 0;
					listProcessos = new ArrayList();
					
					while (it.hasNext()) {
						myProc = (PNT.ProcessoBean)it.next() ;
						myProc.LeProcesso();
						myProc.LeRequerimentos();
						listProcessos.add(myProc);
						myReq = myProc.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBean.getTipoRequerimento());					
						myProc.setMsgErro("");
						
						// Gera transacao 832/862/892 para cada Processo			
						parteVar += sys.Util.rPad(myProc.getNumProcesso()," ",20) +					
						sys.Util.lPad(myProc.getNumCPF(),"0",11) +
						sys.Util.rPad(myProc.getNumRequerimento()," ",20) ;
						
						/*				C�digo comentado para n�o enviar mais um grupo de autos de uma vez para o 
						 * 				Broker. Essa altera��o ir� permitir que uma prov�vel mensagem de erro
						 * 				fique contida somente no auto que tenha tido o problema no envio.
						 * 				
						 * C�DIGO------------------------------------------------------------------------------
						 * 						nauto++;
						 *							if (nauto<56) continue ;
						 *-------------------------------------------------------------------------------------
						 */
						
						resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteDo+parteVar,"");
						
						if (verErro(myProc,resultado,"Processo: "+myProc.getNumProcesso()+" Erro no Envio para Publica��o."))
						{
							inc ++/*=nauto*/;
							DaoBroker dao = DaoBrokerFactory.getInstance();
							
							myProc.LeRequerimentos();
							myReq = myProc.getRequerimentoPorStatusETipoRequerimento("4",funcionalidadesBean.getTipoRequerimento());
//							dao.VerificaAutoOracle(myProc,conn);
//							myReq.setCodAutoInfracao(myProc.getCodAutoInfracao());
//							myReq.setNumRequerimento(myProc.getNumRequerimento());
							
							if (myReq.getNumRequerimento().length()>0)
							{
								/*Atualiza Requerimento*/
								myReq.setCodEnvioEdo(myGuia.getCodEnvioDO());
								myReq.setDatEnvioEdo(myGuia.getDatEnvioDO());
								dao.AtualizarRequerimentoAtaPeloNumero(myReq,conn);
							}
							
						}
						else marcarProcessos(conn,myProc,myGuia,listProcessos) ; 
						listProcessos = new ArrayList();
						/*					
						 * 				  O coment�trio � realizado pelo mesmo motivo ao de cima.
						 * 
						 * C�DIGO------------------------------------------------------------------------------
						 *						nauto=0;
						 *-------------------------------------------------------------------------------------
						 */
						parteVar="";
					}	
					/*					
					 * 				  O coment�trio � realizado pelo mesmo motivo ao de cima.
					 * 
					 * C�DIGO------------------------------------------------------------------------------
					 * 					if (nauto>0) {				
					 *						resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
					 *						if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro no Envio para Publica��o.")) inc+=nauto;
					 *						else marcarAutos(conn,myAuto,myGuia,listAutos);
					 *					}	
					 *-------------------------------------------------------------------------------------
					 */
				}			
			}  // fim do for

			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if (inc==0) 
						conn.rollback();
					else{
						conn.commit();
					}
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		GuiaDistribuicaoId.setMsgErro(inc + " Requerimento(s) Enviado(s) para Publica��o");
		removeEmptyGuias(GuiaDistribuicaoId);
		return inc ;
	}
	
	private void marcarProcessos(Connection conn,ProcessoBean myProc,GuiaDistribuicaoBean myGuia, List listProcessos) throws sys.DaoException {
		try {	
			Statement stmt = conn.createStatement();				
			ProcessoBean auto = null;
			for(int x=0;x<listProcessos.size();x++){
				auto = (ProcessoBean)listProcessos.get(x);
				String sCmd = "UPDATE SMIT_PNT.TPNT_ITEM_LOTE_RELATOR SET MSG_ERRO='"+myProc.getMsgErro()+"' "+
				" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' AND " +
				" NUM_REQUERIMENTO='"+auto.getNumRequerimento()+"'";
				stmt.execute(sCmd) ;
				auto.setMsgErro(myProc.getMsgErro());
			}  // fim do for
			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	private void removeEmptyGuias(GuiaDistribuicaoBean GuiaDistribuicaoId){
		for(int x=0;x<GuiaDistribuicaoId.getRelatGuias().size();x++){
			if( ((GuiaDistribuicaoBean)GuiaDistribuicaoId.getRelatGuias().get(x)).getProcessos().size() == 0 ){
				GuiaDistribuicaoId.getRelatGuias().remove(x);
				x--;
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - InformaPublicacao DO
	 *-----------------------------------------------------------
	 */
	
	public int GuiaInformaPublicacaoDO(GuiaDistribuicaoBean GuiaDistribuicaoId, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			conn.setAutoCommit(false);	
			GuiaDistribuicaoBean myGuia = null;
			List listProcessos = null;
			for(int x=0;x<GuiaDistribuicaoId.getRelatGuias().size();x++){
				myGuia = (GuiaDistribuicaoBean)GuiaDistribuicaoId.getRelatGuias().get(x);
				if(myGuia.getProcessos().size()>0){
						String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR SET COD_STATUS='9'," +
						" NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
						" COD_ORGAO_LOTACAO='"+ myGuia.getCodOrgaoLotacao()+ "'," +
						" DT_PROC=to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),"+				
						" DAT_PUBDO=to_date('"+myGuia.getDatPubliPDO()+"','dd/mm/yyyy') " +			
						" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'" ;
						stmt.execute(sCmd) ;			
						
						// fazer iterator e pegar todos os processos
						Iterator it = myGuia.getProcessos(funcionalidadesBean.getTipoRequerimento()).iterator() ;
						myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
						PNT.ProcessoBean myProc  = new PNT.ProcessoBean();		
						String parteVar  = "" ;
						String resultado = "" ;
						String parteDo   =	sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) + 	
											sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6)+
											sys.Util.rPad(myGuia.getNomUserName()," ",20)+
											sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3)+
											sys.Util.lPad("101","0",3);  			
						
						int nProcesso = 0;
						listProcessos = new ArrayList();				
						while (it.hasNext()) {
							myProc = (PNT.ProcessoBean)it.next() ;
							listProcessos.add(myProc);
							myProc.setMsgErro("");
							// Gera transacao 837/867/897 para cada Processo			
							parteVar += sys.Util.rPad(myProc.getNumProcesso()," ",20) +
										sys.Util.formataDataYYYYMMDD(myGuia.getDatPubliPDO()) ;
							
							/*
							nProcesso++;
							if (nProcesso<58) continue ;
							resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
							if (verErro(myProc,resultado,"Processo: "+myProc.getNumProcesso()+" Erro na Publica��o.")) 
							{
								inc+=nProcesso;
							}
							else marcarProcessos(conn,myProc,myGuia,listProcessos);
							listProcessos = new ArrayList();
							nProcesso=0;
							parteVar="";
							*/
							
						//if (nProcesso>0) {				
							
							resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteDo+parteVar,"");
//							resultado = "000";
							if (verErro(myProc,resultado,"Processo: "+myProc.getNumProcesso()+" Erro no Envio para Publica��o."))
							{
								inc++;
								RequerimentoBean myReq = new RequerimentoBean();
								DaoBroker dao = DaoBrokerFactory.getInstance();  
								myProc.LeRequerimentos();
								myReq = myProc.getRequerimentoPorStatusETipoRequerimento("9", funcionalidadesBean.getTipoRequerimento());
								
								if (myReq.getNumRequerimento().length()>0)
								{
									/*Atualiza Requerimento*/
									myReq.setDatPubliPDO(myGuia.getDatPubliPDO());
									dao.AtualizarRequerimentoAtaPeloNumero(myReq, conn);
								}
							}
							else marcarProcessos(conn,myProc,myGuia,listProcessos);
							
							parteVar="";
						}
					}								 
				//}			
			}  // fim do for
			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if (inc==0) 
						conn.rollback();
					else
						conn.commit();	
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		GuiaDistribuicaoId.setMsgErro(inc + " Requerimentos Publicados");
		removeEmptyGuias(GuiaDistribuicaoId);
		return inc ;
	}
	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua) 
	throws sys.DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			//System.out.println("parte Var ("+codTransacao+"): " + opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws sys.DaoException {
		boolean bOk = false ;
		try {
			if ((resultado.substring(0,3).equals("000")==false) &&
					(resultado.substring(0,3).equals("037")==false)) {
				if ( (resultado.substring(0,3).equals("ERR")) || (sys.Util.isNumber(resultado.substring(0,3))==false) ) {
					myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				}
				else {
					myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+resultado.substring(0,3)+"'" ;
					ResultSet rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					}
					rs.close();
				}
			}
			else   bOk = true ;
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;  
	}
	public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws sys.DaoException {
		Connection conn = null ;
		boolean bOK = false ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			bOK = verErro(myProc,resultado,msg,stmt);
			stmt.close();		
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOK;
	}
	
	public void acertaComplemento (HistoricoBean myHis) throws sys.DaoException {
		if ("206,326,334, 211,331,338".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento03(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento03()));
		}
		if ("200, 207,352, 208,328,335, 209,329,336, 210,330,337, 226,350,351,352 227, 406,410,411".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento02(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento02()));
		}
		if ("204,324,214".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento01(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento01()));	
		}
	}
	/******************************************
	 Verifica se o Auto ja esta no Oracle
	 ******************************************/
	public void VerificaAutoOracle (ProcessoBean myAuto,Connection conn) throws sys.DaoException { 
//		try {	
//			boolean existeAuto = false;	  
//			String sCmd  = "SELECT cod_auto_infracao,cod_status from TSMI_AUTO_INFRACAO "+
//			"WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
//			Statement stmt = conn.createStatement();
//			ResultSet rs    = stmt.executeQuery(sCmd) ;
//			while (rs.next()) {
//				existeAuto = true;	  				  	  				  
//				myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	
//				myAuto.setCodStatus(rs.getString("cod_status"));
//			}
//			if (existeAuto == false) {
//				 sCmd =  "insert into tsmi_auto_infracao (cod_auto_infracao, num_auto_infracao, num_placa, dat_infracao) "+
//				 " values(seq_tsmi_auto_infracao.nextval,"+
//				 " '"+myAuto.getNumAutoInfracao()+ "','"+myAuto.getNumPlaca() + "','"+myAuto.getDatInfracao()+"')";
//				stmt.execute(sCmd) ;
//				sCmd  = "SELECT cod_auto_infracao from TSMI_AUTO_INFRACAO WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
//				rs    = stmt.executeQuery(sCmd) ;
//				while (rs.next()) {
//					myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	  			 
//				}
//			}
//			rs.close();
//			stmt.close();
//		}			
//		catch (Exception ex) {
//			throw new sys.DaoException("GuiaDaoBroker - VerificaAutoOracle: "+ex.getMessage());
//		}
//		return;
	}
	//------------------------------------------------------------------
	//------------------------------------------------------------------
	public String[] AtaGuias(String codEnvioDO, String codStatus, String codOrgao) throws sys.DaoException{
		Connection conn =null ;
		int numGuias=0,numReqs=0;
		String[] totalGuia = new String[] {"0","0"};
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();	
            //Verifica se o n� da Ata j� existe
		    boolean existeAta = false;
            String sCmd = "SELECT COD_LOTE_RELATOR FROM SMIT_PNT.TPNT_LOTE_RELATOR " +
            "WHERE COD_ENVIO_PUBDO = '"+ codEnvioDO + "' AND COD_STATUS = '" + codStatus + "'"+
            " AND COD_ORGAO = "+codOrgao;
            ResultSet rs   = stmt.executeQuery(sCmd) ;
            while(rs.next()){
            	existeAta = true;
            }
            if(existeAta) totalGuia = new String[] {"-2","-2"};
            else 
            {
                sCmd = "SELECT COD_LOTE_RELATOR FROM SMIT_PNT.TPNT_LOTE_RELATOR " +
    			"WHERE COD_ENVIO_PUBDO = '"+ codEnvioDO + "' AND COD_STATUS = '" + codStatus + "'"+
    			" AND COD_ORGAO = "+codOrgao;
    			rs   = stmt.executeQuery(sCmd) ;
    			boolean flag = true;
    			while(rs.next()){
    				flag = false;
    			}
    			if(!flag) totalGuia = new String[] {"-1","-1"};
    			else {		
    				sCmd = "SELECT I.COD_LOTE_RELATOR,COUNT(I.COD_LOTE_RELATOR) as numReq FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR I, SMIT_PNT.TPNT_LOTE_RELATOR R "+
    				"WHERE I.COD_LOTE_RELATOR=R.COD_LOTE_RELATOR AND COD_ENVIO_PUBDO='"+ codEnvioDO + "' " +
    				" AND COD_STATUS=4 GROUP BY I.COD_LOTE_RELATOR" ;		              
    				rs   = stmt.executeQuery(sCmd) ;
    				while (rs.next()) {
    					numGuias++;
    					numReqs += rs.getInt("numReq");
    				}
    				totalGuia[0]=String.valueOf(numGuias);
    				totalGuia[1]=String.valueOf(numReqs);
    			}
            }
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return totalGuia;
	}
	
	//------------------------------------------------------
	public List consultaGuia(GuiaDistribuicaoBean myG,ACSS.UsuarioBean UsrLogado) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT L.COD_LOTE_RELATOR,L.DT_PROC,L.COD_STATUS,L.DSC_TIPOS_REQUERIMENTO,R.NOM_USERNAME,"
				+ "L.COD_ORGAO_LOTACAO,L.COD_ORGAO,L.COD_JUNTA,L.NUM_CPF,DAT_RS,COD_ENVIO_PUBDO,DAT_ENVIO_PUBDO,DAT_PUBDO, "
				+ "SIG_JUNTA, IND_TIPO, COD_RELATOR, NOM_RELATOR "
				+ " FROM " ;
			
//			if (myG.getControleGuia().equals("S"))
//				sCmd +=  " TSMI_CONTROLE_RELATOR C JOIN ";
			
			sCmd += " TPNT_JUNTA J JOIN TPNT_RELATOR R JOIN SMIT_PNT.TPNT_LOTE_RELATOR L"
				+ " ON L.NUM_CPF = R.NUM_CPF ON L.COD_JUNTA = J.COD_JUNTA"  ;
			
			
			if (myG.getControleGuia().equals("S"))
				sCmd += " ON C.COD_RELATOR = R.COD_RELATOR ";
			
			sCmd += " where IND_TIPO = '"+myG.getTipoJunta() +"' and COD_STATUS='"+myG.getCodStatus()+"' " 
			+ " and L.COD_JUNTA = R.COD_JUNTA and R.IND_ATIVO = 'S' ";
			
			
			if (myG.getControleGuia().equals("S"))
			{
				sCmd += " AND C.COD_RELATOR = R.COD_RELATOR" +
				" AND C.COD_USUARIO = "+UsrLogado.getCodUsuario();
			}      
			
			
			sCmd += " ORDER BY COD_LOTE_RELATOR";
			ResultSet rs = stmt.executeQuery(sCmd);
			GuiaDistribuicaoBean myGuia = null;
			while (rs.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));
				myGuia.setDatProc(rs.getString("DT_PROC"));
				myGuia.setCodStatus(rs.getString("COD_STATUS"));
				myGuia.setTipoReqValidos(rs.getString("DSC_TIPOS_REQUERIMENTO"));
				myGuia.setNomUserName(rs.getString("NOM_USERNAME"));
				myGuia.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myGuia.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				myGuia.setCodJunta(rs.getString("COD_JUNTA"));
				myGuia.setNumCPFRelator(rs.getString("NUM_CPF"));
				myGuia.setDatRS(rs.getString("DAT_RS"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				myGuia.setTipoJunta(rs.getString("IND_TIPO"));
				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
				myGuia.setCodRelator(rs.getString("COD_RELATOR"));
				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
				vList.add(myGuia);
			}
			
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	/**
	 * Consulta Atas a publicar do Oracle
	 * @param myG
	 * @param UsrLogado
	 * @return
	 * @throws DaoException
	 * @author jefferson.trindade
	 */
	public List consultaAtasPublicar(GuiaDistribuicaoBean myG,ACSS.UsuarioBean UsrLogado) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			PreparedStatement stmt = null;
			StringBuffer sCmd = new StringBuffer();
			sCmd.append(" select DISTINCT L.COD_STATUS,L.COD_ENVIO_PUBDO,to_char(L.DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO,to_char(L.DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO");
			sCmd.append(" from SMIT_PNT.TPNT_LOTE_RELATOR l, SMIT_PNT.TPNT_JUNTA j");
			sCmd.append(" where j.COD_JUNTA = l.COD_JUNTA and j.IND_TIPO = ? and l.COD_STATUS = ?");
			sCmd.append(" ORDER BY DAT_ENVIO_PUBDO desc");
			stmt = conn.prepareStatement(sCmd.toString());
			stmt.setString(1, myG.getTipoJunta());
			stmt.setString(2, myG.getCodStatus());
			
			/**FIXME COMENTADO AT� RESOLVER O PROBLEMA DOS RELATORES DUPLICADOS
			String sCmd = "SELECT distinct COD_STATUS,COD_ENVIO_PUBDO,"+
			"to_char(DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO,to_char(DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO " +			
			" FROM" ;
			
			sCmd +=" SMIT_PNT.TPNT_LOTE_RELATOR L , TSMI_JUNTA J, TSMI_RELATOR R " +
			" WHERE J.IND_TIPO = '"+myG.getTipoJunta() +"' and COD_STATUS='"+myG.getCodStatus()+"' "+
			" AND L.COD_JUNTA = J.COD_JUNTA ";
			//" AND L.NUM_CPF = R.NUM_CPF " +
			//" AND R.IND_ATIVO = 'S' ";
			
			sCmd +=" ORDER BY DAT_ENVIO_PUBDO desc";
			**/
			ResultSet rs = stmt.executeQuery();
			
			GuiaDistribuicaoBean myGuia = null;
			while (rs.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setCodStatus(rs.getString("COD_STATUS"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				vList.add(myGuia);
			}
			
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}//------------------------------------------------------
	
	
	public Vector LeGuiasAta(GuiaDistribuicaoBean myG) throws DaoException{
		Vector guias = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_LOTE_RELATOR,COD_STATUS "+
			" FROM SMIT_PNT.TPNT_LOTE_RELATOR L, TPNT_JUNTA J" +			
			" where J.IND_TIPO = '"+myG.getTipoJunta() +"' " +
			" and L.COD_ENVIO_PUBDO='"+myG.getCodEnvioDO()+"' "+
			" and L.COD_JUNTA=J.COD_JUNTA " +
			" ORDER BY COD_LOTE_RELATOR";
			
			//System.out.println("LeGuiasAta Guia: "+sCmd);
			
			ResultSet rs = stmt.executeQuery(sCmd);
			String  mg = "";
			while (rs.next()) {
				mg = rs.getString("COD_LOTE_RELATOR");
				if (mg!=null) {
					guias.addElement(mg);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		//System.out.println("LeGuiasAta Guia FIM: "+guias.size());	
		return guias;
	}
	
	/**
	 * Verifica se o requerimento j� foi incluido na guia 
	 * @param myProcesso
	 * @return
	 * @throws sys.DaoException
	 * @author jefferson.trindade
	 */
	public String ConsultaReqGuia(ProcessoBean myProcesso) throws sys.DaoException{
		
		Connection conn = null;
		ResultSet rs = null;
		String existe = "";
		String sql = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sql = "select l.cod_lote_relator "+
			" FROM smit_pnt.tpnt_lote_relator l, smit_pnt.tpnt_item_lote_relator i " +
			" WHERE i.num_processo = '"+ myProcesso.getNumProcesso()+"' " +
			" and l.cod_status<>9 " + 
			" and l.cod_lote_relator = i.cod_lote_relator " +
			" and i.num_requerimento = '"+myProcesso.getNumRequerimento()+"' and rownum = 1" ;
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				existe = rs.getString("cod_lote_relator");
			}
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("PNT.DEF.DaoTabelas: " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	//------------------------------------------------------
	/**
	 * Metodo para consultar ATA
	 * @author Rodrigo.fonseca
	 * @since 26/04/2012
	 */
	public void consultaAta(GuiaDistribuicaoBean guia, int ordem) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtGuia = conn.createStatement();
			String sCmd = "SELECT COD_LOTE_RELATOR,DT_PROC,COD_STATUS,DSC_TIPOS_REQUERIMENTO,R.NOM_USERNAME,"
				+ "R.COD_ORGAO_LOTACAO,L.COD_ORGAO,L.COD_JUNTA,L.NUM_CPF,TO_CHAR(DAT_RS,'dd/mm/yyyy') AS DAT_RESULT,COD_ENVIO_PUBDO,DAT_ENVIO_PUBDO,DAT_PUBDO, "
				+ "SIG_JUNTA, IND_TIPO, COD_RELATOR, NOM_RELATOR, NOM_TITULO, L.NUM_SESSAO,TO_CHAR(L.DAT_SESSAO,'dd/mm/yyyy') DAT_SESSAO "
				+ " FROM TPNT_JUNTA J JOIN SMIT_PNT.TPNT_RELATOR R JOIN SMIT_PNT.TPNT_LOTE_RELATOR L"
				+ " ON L.NUM_CPF = R.NUM_CPF ON L.COD_JUNTA = J.COD_JUNTA"			
				+ " where IND_TIPO = '"+guia.getTipoJunta() + "' "  
				+ " AND COD_ENVIO_PUBDO='"+guia.getCodEnvioDO()+"' "
				+ " AND R.COD_JUNTA = J.COD_JUNTA";
			if(ordem == 1)
				sCmd += " ORDER BY COD_LOTE_RELATOR";
			else if(ordem == 2)
				sCmd += " ORDER BY COD_ENVIO_PUBDO ASC, COD_JUNTA ASC, DAT_RS ASC, NOM_TITULO ASC";
			else if(ordem == 3)
				sCmd += " ORDER BY NOM_RELATOR";
			else if(ordem == 4)
				sCmd += " ORDER BY COD_ENVIO_PUBDO ASC, COD_JUNTA ASC, NOM_TITULO ASC";
			
			
			ResultSet rsGuia = stmtGuia.executeQuery(sCmd);
			GuiaDistribuicaoBean myGuia = null;
			List guias = new ArrayList();
			List processos = null;
			ResultSet rs = null;
			SimpleDateFormat dat = new SimpleDateFormat("dd/MM/yyyy");
			while (rsGuia.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setNumGuia(rsGuia.getString("COD_LOTE_RELATOR"));
				myGuia.setDatProc(rsGuia.getString("DT_PROC"));
				myGuia.setCodStatus(rsGuia.getString("COD_STATUS"));
				myGuia.setNumSessao(rsGuia.getString("NUM_SESSAO"));
				myGuia.setDatSessao(rsGuia.getString("DAT_SESSAO"));
				myGuia.setTipoReqValidos(rsGuia.getString("DSC_TIPOS_REQUERIMENTO"));
				myGuia.setNomUserName(rsGuia.getString("NOM_USERNAME"));
				myGuia.setCodOrgaoLotacao(rsGuia.getString("COD_ORGAO_LOTACAO"));
				myGuia.setCodOrgaoAtuacao(rsGuia.getString("COD_ORGAO"));
				myGuia.setCodJunta(rsGuia.getString("COD_JUNTA"));
				myGuia.setNumCPFRelator(rsGuia.getString("NUM_CPF"));
				myGuia.setDatRS(rsGuia.getString("DAT_RESULT"));
				myGuia.setCodEnvioDO(rsGuia.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(dat.format(rsGuia.getDate("DAT_ENVIO_PUBDO")));
				myGuia.setDatPubliPDO(rsGuia.getString("DAT_PUBDO"));
				myGuia.setTipoJunta(rsGuia.getString("IND_TIPO"));
				myGuia.setSigJunta(rsGuia.getString("SIG_JUNTA"));
				myGuia.setCodRelator(rsGuia.getString("COD_RELATOR"));
				myGuia.setNomRelator(rsGuia.getString("NOM_RELATOR"));
				myGuia.setNomTituloRelator(rsGuia.getString("NOM_TITULO"));			
				
				sCmd = "Select I.COD_ITEM_LOTE_RELATOR," +
				"I.NUM_PROCESSO,to_char(I.DAT_PROCESSO,'dd/mm/yyyy') DAT_PROCESSO, " +
				"I.NUM_REQUERIMENTO,I.IND_TIPO_REQUERIMENTO," +
				"I.MSG_ERRO, " +
				"I.COD_RESULT_RS,I.TXT_MOTIVO_RS "+
				"FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR I "+
				"WHERE I.COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' " ;
				
				rs   = stmt.executeQuery(sCmd) ;
				processos = new ArrayList();
				while (rs.next()) {
					PNT.ProcessoBean myProc  = new PNT.ProcessoBean();
					List reqs = new ArrayList();
					PNT.RequerimentoBean req = new PNT.RequerimentoBean();
					myProc.setCodOrgao(myGuia.getCodOrgaoAtuacao());
					myProc.setCodProcesso(rs.getString("COD_ITEM_LOTE_RELATOR"));			
					myProc.setNumProcesso(rs.getString("NUM_PROCESSO"));	
					myProc.setDatProcesso(rs.getString("DAT_PROCESSO"));	
					myProc.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
					myProc.setTipoRequerimento(rs.getString("IND_TIPO_REQUERIMENTO"));			
					myProc.setMsgErro(rs.getString("MSG_ERRO"));
					req.setCodResultRS(rs.getString("COD_RESULT_RS"));
					req.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
					reqs.add(req);		
					myProc.LeProcesso();
					myProc.LeRequerimentos();
					processos.add(myProc);
				}
				myGuia.setProcessos(processos);
				guias.add(myGuia);	
				rs.close();
			}
			rsGuia.close();
			stmt.close();
			stmtGuia.close();
			guia.setRelatGuias(guias);
			guia.setDatEnvioDO(myGuia.getDatEnvioDO());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	//----------------------------------------------------------------------------------
	/*
	public void consultaAta(GuiaDistribuicaoBean guia) throws DaoException{
		consultaAta(guia,1);
	}
	*/
	
	//----------------------------------------------------------------------------------
	public boolean gravaArquivo(GuiaDistribuicaoBean GuiaDistribuicaoId, String quantReq) throws Exception{
		Connection conn = null;
		boolean existeArq = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = null;
			if(!GuiaDistribuicaoId.getCodStatus().equals("-1")){
				sCmd = "SELECT COD_ENVIO_DO FROM TPNT_ARQUIVO_ATA_DO WHERE COD_ENVIO_DO = '" + GuiaDistribuicaoId.getCodEnvioDO() + "'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					existeArq = true;
				}
				if(existeArq)
					return false;
			}
			else{
				sCmd = "UPDATE TPNT_ARQUIVO_ATA_DO SET COD_STATUS = 2 WHERE COD_ENVIO_DO = '" + GuiaDistribuicaoId.getCodEnvioDO() + "'";
				stmt.executeUpdate(sCmd);
			}
			sCmd = "INSERT INTO TPNT_ARQUIVO_ATA_DO (COD_ARQUIVO_ATA, COD_ENVIO_DO, DAT_CRIACAO, QTD_REG, NOM_USERNAME, COD_ORGAO_LOTACAO , NOM_ARQUIVO_ATA, COD_STATUS)"+
			" VALUES (SEQ_PNT_ARQUIVO_ATA_DO.NEXTVAL, '" + GuiaDistribuicaoId.getCodEnvioDO() + "',TO_DATE('" + sys.Util.formatedToday().substring(0,10) + "','DD/MM/YYYY'),'" +	
			quantReq + "','" + GuiaDistribuicaoId.getNomUserName() + "','" + GuiaDistribuicaoId.getCodOrgaoLotacao() + "','" + GuiaDistribuicaoId.getNomArquivoATA() + "', 1)";
			stmt.execute(sCmd);
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return true;
	}
	//----------------------------------------------------------------------------------
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */
	
	public Vector buscaArquivosAta(GuiaDistribuicaoBean myArq, String dataIni, String dataFim) throws DaoException {
		GuiaDistribuicaoBean relClasse;
		Vector vRetClasse   = new Vector();
		
		Connection conn = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			String sCmd ="SELECT DISTINCT COD_ARQUIVO_ATA, A.COD_ENVIO_DO, NOM_ARQUIVO_ATA, DAT_PUBDO, L.COD_STATUS AS STATUS_ATA" +
			" FROM SMIT_PNT.TPNT_ARQUIVO_ATA_DO A JOIN SMIT_PNT.TPNT_LOTE_RELATOR L JOIN SMIT_PNT.TPNT_JUNTA J" +
			" ON L.COD_JUNTA = J.COD_JUNTA" +
			" ON A.COD_ENVIO_DO = L.COD_ENVIO_PUBDO" +
			" WHERE IND_TIPO = '" + myArq.getTipoJunta() + "'" + 
			" AND DAT_ENVIO_PUBDO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" +
			" AND A.COD_STATUS = 1" +
			" ORDER BY COD_ENVIO_DO DESC";         
			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs2 = null;
			while (rs.next())   {
				relClasse = new GuiaDistribuicaoBean();
				relClasse.setNumGuia(rs.getString("COD_ARQUIVO_ATA"));
				relClasse.setCodEnvioDO(rs.getString("COD_ENVIO_DO")); 
				relClasse.setNomArquivoATA(rs.getString("NOM_ARQUIVO_ATA"));
				if(rs.getDate("DAT_PUBDO") != null)
					relClasse.setDatPubliPDO(df.format(rs.getDate("DAT_PUBDO")));   
				else
					relClasse.setDatPubliPDO("");
				relClasse.setCodStatus(rs.getString("STATUS_ATA"));
				sCmd = "SELECT COD_ARQUIVO_ATA FROM TPNT_DOWNLOAD_ATA_DO WHERE COD_ARQUIVO_ATA = '"+relClasse.getNumGuia()+"'";
				rs2 = stmt2.executeQuery(sCmd);
				if(rs2.next())
					relClasse.setCodEvento("1");
				else
					relClasse.setCodEvento("0");
				
				vRetClasse.addElement(relClasse);           
			}           
			rs.close();
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	//-------------------------------------------------------------------------------------------
	public void downloadArquivoAta(GuiaDistribuicaoBean myArq, String codArquivoAta) throws DaoException {
		GuiaDistribuicaoBean relClasse;
		Vector vRetClasse   = new Vector();
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat dat_download = new SimpleDateFormat("dd/MM/yyyy");
			String sCmd ="INSERT INTO TPNT_DOWNLOAD_ATA_DO (COD_DOWNLOAD_ATA, DAT_DOWNLOAD, NOM_USERNAME," +
			"COD_ARQUIVO_ATA,COD_ORGAO_LOTACAO) VALUES(SEQ_TSMI_DOWNLOAD_ATA.NEXTVAL, TO_DATE('"+
			dat_download.format(new Date()) + "','DD/MM/YYYY'),'" +myArq.getNomUserName() + "','" 
			+ codArquivoAta + "','" + myArq.getCodOrgaoLotacao() + "')";
			stmt.executeQuery(sCmd);
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
	}
	
	//-------------------------------------------------------------------------------------------
	public int guiaEstornoResultado(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;

		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List processos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
					" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
					" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
					" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy')," +
					" DAT_SESSAO = NULL," +
					" DAT_RS = NULL  "+
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
					stmt.execute(sCmd) ;
					sCmd = "UPDATE SMIT_PNT.TPNT_ITEM_LOTE_RELATOR SET COD_RESULT_RS = NULL "+
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
					stmt.execute(sCmd) ;
					
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getProcessos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				processos.add(myProcesso);
				
				// Gera transacao 817/847/877 para cada Processo	
				parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()),"0",8)  + 
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3) +
						sys.Util.lPad(funcionalidadesBean.getCodOrigemEvento(),"0",3) +
						sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
						sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
						sys.Util.rPad(myProcesso.getNumRequerimento()," ",20) +
						sys.Util.lPad("D","0",1) +
						sys.Util.lPad(sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8)+			
						sys.Util.rPad("ESTORNO RESULTADO GUIA DE DISTRIBUI��O"," ",1000) +
						sys.Util.lPad("0","0",3) +
						sys.Util.rPad(" "," ",100);
						resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
						
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Estornar RESULTADO Guia de Distribui��o.")==false) {
					vErro.add(myProcesso.getMsgErro());
					marcarProcessos(conn, myProcesso, myGuia, processos);
					//marcarAutos(conn,myProcesso,myGuia,processos);
				}
				else 
				{
					inc++;
					DaoBroker dao = DaoBrokerFactory.getInstance();
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myProcesso.LeRequerimentos();
					myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBean.getTipoRequerimento());
					if (myReq.getNumRequerimento().length()>0)
					{
						myReq.setCodStatusRequerimento("2");
						dao.AtualizarRequerimentoAtaPeloNumero(myReq,conn);
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	
	//----------------------------------------------------------------------------------------
	public int guiaEstornoJuntaRelator(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "DELETE FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' ";
			stmt.execute(sCmd) ;
			
			sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy') "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;

			Iterator<ProcessoBean> it = myGuia.getProcessos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
			String parteVar = "";
			String resultado = "";
			Vector<String> vErro = new Vector<String>();
			
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				processos.add(myProcesso);
				
				// Executa a transacao 822/852/882 para cada Processo pertencente a guia estornada
				parteVar = sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
						sys.Util.lPad(funcionalidadesBean.getCodEvento(),"0",3) +
						sys.Util.lPad(funcionalidadesBean.getCodOrigemEvento(),"0",3) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad("ESTORNO JUNTA/RELATOR DA GUIA DE DISTRIBUI��O"," ",100);			 			
				resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
				
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Estornar Guia de Distribui��o.")==false) {
					vErro.add(myProcesso.getMsgErro());
					marcarProcessos(conn, myProcesso, myGuia, processos);
					//marcarAutos(conn,myProcesso,myGuia,processos);
				}else{
					
					inc++;
					DaoBroker dao = DaoBrokerFactory.getInstance();
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myProcesso.LeRequerimentos();
					myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("2",funcionalidadesBean.getTipoRequerimento());
					if (myReq.getNumRequerimento().length()>0)
					{
						myReq.setCodStatusRequerimento("1");
						dao.AtualizarRequerimentoAtaPeloNumero(myReq,conn);
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	
	//----------------------------------------------------------------------------------------
	public int guiaEstornoDO(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;

		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List processos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
					" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
					" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
					" COD_ENVIO_PUBDO = NULL , DAT_ENVIO_PUBDO = NULL, NOM_RESP_ATA = NULL, TXT_TITULO_ATA = NULL, DAT_PUBDO =NULL"+
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
					stmt.execute(sCmd) ;
					
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getProcessos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				processos.add(myProcesso);
				
				parteVar = sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
						sys.Util.rPad(myGuia.getNomUserName()," ",20) +
						sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +
						sys.Util.rPad(myProcesso.getNumProcesso()," ",20) + 
						sys.Util.lPad(myProcesso.getNumNotificacao(),"0",9) +
						sys.Util.rPad(funcionalidadesBean.getTipoAta()," ",1) +
						sys.Util.rPad(myGuia.getCodEnvioDO()," ",20);
				resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
				
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Estornar ENVIO P/ PUBLICA��O Guia de Distribui��o.")==false) {
					vErro.add(myProcesso.getMsgErro());
					marcarProcessos(conn, myProcesso, myGuia, processos);
				}
				else 
				{
					inc++;
					DaoBroker dao = DaoBrokerFactory.getInstance();
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myProcesso.LeRequerimentos();
					myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("4",funcionalidadesBean.getTipoRequerimento());
					if (myReq.getNumRequerimento().length()>0)
					{
						myReq.setCodStatusRequerimento("8");
						dao.AtualizarRequerimentoAtaPeloNumero(myReq,conn);
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;

	}
	
	/**
	 * Verificar se � poss�vel
	 * @param myGuia
	 * @param funcionalidadesBean
	 * @return
	 * @throws sys.DaoException
	 */
	public int guiaEstornoDataPublicacaoDO(GuiaDistribuicaoBean myGuia, FuncionalidadesBean funcionalidadesBean) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;

		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List processos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE SMIT_PNT.TPNT_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
					" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
					" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
					" COD_ENVIO_PUBDO = NULL , DAT_ENVIO_PUBDO = NULL, NOM_RESP_ATA = NULL, TXT_TITULO_ATA = NULL, DAT_PUBDO =NULL"+
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
					stmt.execute(sCmd) ;
					
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getProcessos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myProcesso = (PNT.ProcessoBean)it.next() ;
				myProcesso.setMsgErro("");
				processos.add(myProcesso);
				
				// Gera transacao 820/850/880 para cada Processo
				/**
				 * TODO
				 * Aguardando PS desenvolver a transa��o
				 */
				parteVar = sys.Util.rPad(myProcesso.getNumProcesso()," ",20) + 
				sys.Util.lPad(myProcesso.getCodOrgao(),"0",6) +
				sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
				sys.Util.lPad(myProcesso.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myProcesso.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.rPad(" "," ",50) +
				sys.Util.rPad("ESTORNO ENVIO P/ PUBLICA��O GUIA DE DISTRIBUI��O"," ",1000) ;			 			
				resultado = chamaTransBRK(funcionalidadesBean.getCodEvento(),parteVar,"");
				
				if (verErro(myProcesso,resultado,"Processo: "+myProcesso.getNumProcesso()+" Erro na grava��o de Estornar ENVIO P/ PUBLICA��O Guia de Distribui��o.")==false) {
					vErro.add(myProcesso.getMsgErro());
					marcarProcessos(conn, myProcesso, myGuia, processos);
					//marcarAutos(conn,myProcesso,myGuia,processos);
				}
				else 
				{
					inc++;
					DaoBroker dao = DaoBrokerFactory.getInstance();
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myProcesso.LeRequerimentos();
					myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("4",funcionalidadesBean.getTipoRequerimento());
					if (myReq.getNumRequerimento().length()>0)
					{
						myReq.setCodStatusRequerimento("8");
						dao.AtualizarRequerimentoAtaPeloNumero(myReq,conn);
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;

	}
	//----------------------------------------------------------------------------------------
	public void removeAutoGuia(ProcessoBean myProcesso,RequerimentoBean myReq) throws DaoException{
		Connection conn =null ;
		boolean retorno = true;
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST) ;		
//			Statement stmt = conn.createStatement();	
//			conn.setAutoCommit(false);	
//			
//			String sCmd = "SELECT L.COD_LOTE_RELATOR as COD_LOTE_RELATOR from TSMI_ITEM_LOTE_RELATOR I JOIN TSMI_LOTE_RELATOR L"+
//			" ON I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR"+
//			" WHERE num_auto_infracao = '"+myProcesso.getNumAutoInfracao()+"'"+
//			" AND num_requerimento = '"+myReq.getNumRequerimento()+"'";
//			ResultSet rs = stmt.executeQuery(sCmd);
//			String codLoteRelator="";
//			while(rs.next()){
//				codLoteRelator = rs.getString("COD_LOTE_RELATOR");
//			}
//			myProcesso.setCodEnvioDO(codLoteRelator);
//			sCmd = "DELETE tsmi_item_lote_relator i" +
//			" WHERE num_auto_infracao = '"+myProcesso.getNumAutoInfracao() +"'"+
//			" AND num_requerimento = '"+myReq.getNumRequerimento()+"'"+
//			" AND COD_LOTE_RELATOR = '"+codLoteRelator+"'";
//			stmt.execute(sCmd);
//			
//			sCmd = "SELECT COUNT(COD_ITEM_LOTE_RELATOR) AS TOTAL from TSMI_ITEM_LOTE_RELATOR I JOIN TSMI_LOTE_RELATOR L"+
//			" ON I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR"+
//			" WHERE L.COD_LOTE_RELATOR = '"+codLoteRelator+"'";
//			ResultSet rs2 = stmt.executeQuery(sCmd);
//			rs2.next();
//			if(rs2.getInt("TOTAL") == 0){
//				sCmd = "UPDATE TSMI_LOTE_RELATOR SET COD_STATUS = 8,COD_ENVIO_PUBDO = '', DAT_ENVIO_PUBDO ='', NOM_RESP_ATA = '', TXT_TITULO_ATA = '', DAT_PUBDO ='' WHERE COD_LOTE_RELATOR = '"+codLoteRelator+"'";
//				stmt.executeUpdate(sCmd);
//			}
//			stmt.close();
//			rs.close();
//			rs2.close();
//			conn.commit();	
//		}
//		catch (Exception e) {	
//			retorno = false;
//			throw new DaoException(e.getMessage());
//		}//fim do catch
//		finally {
//			if (conn != null) {
//				try { 
//					if(retorno)
//						conn.commit();
//					else
//						conn.rollback();
//					serviceloc.setReleaseConnection(conn); 
//				}
//				catch (Exception ey) { }
//			}
//		}				
	}
	
	/*****************************************************************
	 Retorna guias de um determinado mes/ano 
	 *****************************************************************/
	public  boolean getGuias (GER.ResultadoAnualBean ResultadoAnualBeanId,String tipoResultado,
			String datInicio,String datFim,GuiaDistribuicaoBean guiaBean) throws sys.DaoException {
		boolean bOk = true ;
		Connection conn =null ;		
//		List guias = new ArrayList();
//		try {
//			conn = serviceloc.getConnection(MYABREVSIST);	
//			Statement stmt = conn.createStatement();
//			Statement stmt2 = conn.createStatement();
//			String sCmd = "";
//			ResultSet rs = null;
//			ResultSet rs2 = null;
//			
//			sCmd = " SELECT * FROM TSMI_LOTE_RELATOR LR left outer join tsmi_relator R on  LR.num_cpf = R.num_cpf and  LR.cod_junta=R.cod_junta " +
//			" JOIN tsmi_junta J on LR.cod_junta=J.cod_junta "+
//			" WHERE LR.DAT_RS >= to_date('"+datInicio+"','dd/mm/yyyy') AND " +
//			" LR.DAT_RS <= to_date('"+datFim+"','dd/mm/yyyy') AND LR.DSC_TIPOS_REQUERIMENTO = '"+ResultadoAnualBeanId.getTpreq()+"'";
//			if (!ResultadoAnualBeanId.getOrgao().equals("0"))
//				sCmd += " AND LR.COD_ORGAO = " + ResultadoAnualBeanId.getOrgao()+ " AND LR.COD_ORGAO_LOTACAO = "+ guiaBean.getCodOrgaoLotacao();
//			rs = stmt.executeQuery(sCmd);
//			while(rs.next())
//			{
//				GuiaDistribuicaoBean myGuia = new GuiaDistribuicaoBean();
//				List autos = new ArrayList(); 
//				sCmd = " SELECT * FROM TSMI_item_LOTE_RELATOR t1,tsmi_auto_infracao t2,tsmi_lote_relator t3, tsmi_orgao t4 "+
//				" WHERE t3.cod_lote_relator = t1.cod_lote_relator AND "+ 
//				" t4.cod_orgao = t2.cod_orgao AND "+
//				" t1.num_auto_infracao = t2.num_auto_infracao AND "+ 
//				" t3.DAT_RS >= to_date('"+datInicio+"','dd/mm/yyyy') AND t3.DAT_RS <= to_date('"+datFim+"','dd/mm/yyyy')"+
//				" AND t1.ind_tipo_requerimento in ('"+ResultadoAnualBeanId.getTpreq().substring(0,2)+"','"+ResultadoAnualBeanId.getTpreq().substring(3,5)+"') AND t1.cod_lote_relator = "+rs.getString("COD_LOTE_RELATOR");
//				if (!tipoResultado.substring(0,1).equals("T"))  
//					sCmd += " AND t1.COD_RESULT_RS = '"+tipoResultado.substring(0,1)+"'";
//				if (!ResultadoAnualBeanId.getOrgao().equals("0"))  
//					sCmd += " AND t2.COD_ORGAO = " + ResultadoAnualBeanId.getOrgao();
//				rs2 = stmt2.executeQuery(sCmd);
//				while(rs2.next())
//				{
//					AutoInfracaoBean myAuto = new AutoInfracaoBean(); 
//					RequerimentoBean myReq = new RequerimentoBean();
//					myAuto.setNumAutoInfracao(rs2.getString("NUM_AUTO_INFRACAO"));
//					myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
//					myAuto.setSigOrgao(rs2.getString("SIG_ORGAO"));
//					myAuto.setNumProcesso(rs2.getString("NUM_PROCESSO"));
//					myAuto.setDatProcesso(sys.Util.fmtData(rs2.getString("DAT_PROCESSO")).replaceAll("-","/"));							
//					myAuto.setNumRequerimento(rs2.getString("NUM_REQUERIMENTO"));
//					myAuto.setTpRequerimento(rs2.getString("IND_TIPO_REQUERIMENTO"));
//					myAuto.getInfracao().setDscEnquadramento(rs2.getString("DSC_ENQUADRAMENTO"));
//					myReq.setCodResultRS(rs2.getString("COD_RESULT_RS"));
//					myAuto.getRequerimentos().add(myReq);
//					myAuto.setDatInfracao(sys.Util.fmtData(rs2.getString("DAT_INFRACAO")).replaceAll("-","/"));
//					autos.add(myAuto);
//				}
//				myGuia.setAutos(autos);
//				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));
//				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
//				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
//				myGuia.setDatProc(sys.Util.fmtData(rs.getString("DT_PROC")).replaceAll("-","/"));
//				if(myGuia.getAutos().size()!=0) guias.add(myGuia);
//			}
//			guiaBean.setRelatGuias(guias);
//			if (rs2 != null) rs2.close();
//			if (rs != null) rs.close();
//			stmt.close();
//			stmt2.close();
//		}
//		catch (Exception ex) {
//			bOk = false ;
//			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
//		}
//		finally {
//			if (conn != null) {
//				try { serviceloc.setReleaseConnection(conn); }
//				catch (Exception ey) { }
//			}
//		}
		return bOk;
	}	
	
	/**---------------------------------------------------------------------------------------
	 * Informa que guia de remessa j� foi usada para gerar uma guia de distribui��o.
	 * Impossibilitando ser reutilizada.
	 * Campo: SIT_GUIA_DISTRIBUICAO 
	 * Tabela: TSMI_REMESSA
	 * @param myRemessa
	 * @throws sys.DaoException
	 */
	public void gravaSitGuiaDistribuicao(RemessaBean myRemessa) throws sys.DaoException{
//		String sCmd  = "";
//		Connection conn = null;
//		try 
//		{
//			conn = serviceloc.getConnection(MYABREVSIST);
//			Statement stmt = conn.createStatement();
//			sCmd = "UPDATE TSMI_REMESSA  SET SIT_GUIA_DISTRIBUICAO = '" +myRemessa.SIT_GUIA_DISTRIBUICAO+"'"+
//			"WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
//			stmt.execute(sCmd);
//			stmt.close();
//		}
//		catch (SQLException sqle) {
//			throw new sys.DaoException(sqle.getMessage());
//		}
//		catch (Exception e) {	
//			throw new sys.DaoException(e.getMessage());
//		}//fim do catch
//		return ;
	}

	public void removeProcessoGuia(List<ProcessoBean> processosARemover, GuiaDistribuicaoBean myGuia) throws DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();
			String sCmd;
			int registros = 0;
			
			conn.setAutoCommit(false);	
			
			for (ProcessoBean processo : processosARemover){
				sCmd = "DELETE FROM SMIT_PNT.TPNT_ITEM_LOTE_RELATOR "+
						" WHERE NUM_PROCESSO='"+processo.getNumProcesso()+"' ";
				stmt.execute(sCmd) ;
				inc ++;
			}
			
			//Se n�o sobraram processos vinculados � guia, exclui a guia (lote)
			sCmd = "SELECT COUNT(*) FROM SMIT_PNT.TPNT_LOTE_RELATOR WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()){
				registros = rs.getInt("count(*)");
			}
			rs.close();
			
			if (registros == 0){
				sCmd = "DELETE FROM SMIT_PNT.SMIT_PNT.TPNT_LOTE_RELATOR WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
				stmt.execute(sCmd) ;
				inc ++;
			}

			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			
			if (inc==0) 
				conn.rollback();	
			else 		
				conn.commit();
			
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
	}
}


