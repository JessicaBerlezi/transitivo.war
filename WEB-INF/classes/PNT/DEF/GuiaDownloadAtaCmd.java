package PNT.DEF;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

/**
* <b>Title:</b>        Download de ATAS<br>
* <b>Description:</b>  Comando para Download de arquivos<br>
* <b>Copyright:</b>    Copyright (c) 2012<br>
 * <b>Company: </b> DETRAN RJ <br>
* @author 	
* @version 1.0
*/

public class GuiaDownloadAtaCmd extends sys.Command {

  private static final String jspPadrao="/PNT/DEF/GuiaDownloadAta.jsp";    
  private String next;

  public GuiaDownloadAtaCmd() {
		next = jspPadrao;
  }

  public GuiaDownloadAtaCmd(String next) {
		this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
  	
		String nextRetorno = jspPadrao ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
			
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) req.getSession().getAttribute
				 ("ParamSistemaBeanId");
						
			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
			
			FuncionalidadesBean funcionalidades = (FuncionalidadesBean)session.getAttribute("funcionalidadesBeanId") ;
			if (funcionalidades==null)  funcionalidades= new FuncionalidadesBean() ;
			
			funcionalidades.setSigFuncao(req.getParameter("j_sigFuncao"));
			
			String acao = req.getParameter("acao");
			if(acao == null)
				acao = "";
			String statusInteracao = "1";	
			String dataIni = req.getParameter("De");
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			if(dataIni == null){
				dataIni = "01/01/"+Util.formatedToday().substring(6,10);
			}
			String dataFim = req.getParameter("Ate");	
			if(dataFim == null)
				dataFim = df.format(new Date());
			List vetArquivos = null;
			
			GuiaDistribuicaoBean guiaArquivoAtaId = new GuiaDistribuicaoBean();
//			guiaArquivoAtaId.setJ_sigFuncao(usrFunc.getJ_sigFuncao(),null);
			guiaArquivoAtaId.setCodOrgaoAtuacao(usrLogado.getCodOrgaoAtuacao());
			guiaArquivoAtaId.setNomUserName(usrLogado.getNomUserName());
			guiaArquivoAtaId.setCodOrgaoLotacao(usrLogado.getOrgao().getCodOrgao());
			guiaArquivoAtaId.setTipoJunta(funcionalidades.getTipoJunta());
			
			if(acao.equals("consultaArq")){											
				try { 
					vetArquivos = guiaArquivoAtaId.buscaArquivosAta(dataIni,dataFim);
				}catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}	
				statusInteracao = "2";
			}
			if(acao.equals("registra")){
				try { 
					guiaArquivoAtaId.setCodEnvioDO(req.getParameter("codArquivo"));
					guiaArquivoAtaId.downloadArquivoAta();
					vetArquivos = guiaArquivoAtaId.buscaArquivosAta(dataIni,dataFim);
				}catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}	
				statusInteracao = "2";
			}
			req.setAttribute("guiaArquivoAtaId",guiaArquivoAtaId);
			req.setAttribute("msgErro",msgErro);						
			req.setAttribute("vetArquivos",vetArquivos);
			req.setAttribute("dataIni",dataIni);
			req.setAttribute("dataFim",dataFim);
			req.setAttribute("statusInteracao",statusInteracao);
			session.setAttribute("funcionalidadesBeanId", funcionalidades);
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}
