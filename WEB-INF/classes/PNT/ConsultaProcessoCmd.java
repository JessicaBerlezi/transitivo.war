package PNT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ConsultaProcessoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/PNT/ConsultaProcesso.jsp" ;  
   
  public ConsultaProcessoCmd() {
    next             =  jspPadrao;
  }

  public ConsultaProcessoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {  			
		  // cria os Beans de sessao, se n�o existir
		  HttpSession session   = req.getSession() ;								
		  ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		  if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
		  ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		  if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  				
		  
		  ProcessoBean ProcessoBeanId                     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
		  if (ProcessoBeanId==null)  {
			  ProcessoBeanId    = new ProcessoBean() ;	  		    
			  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
		  }
		  
		  String acao = req.getParameter("acao");  
	  
		  if ((acao==null) || ("".equals(acao)))	{
			  ProcessoBeanId       = new ProcessoBean() ;	  		 
			  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			  acao = "";
		  }
		  sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
		  cmd.setNext(this.next) ;
		  nextRetorno = cmd.execute(req);		  
		  if ("Novo".equals(acao)) {
			  acao = "";				
			  ProcessoBeanId = new ProcessoBean();
			  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
		  } 
		  if ("".equals(acao)) nextRetorno  = next ;  
		  if (acao.equals("ListaProcessos")) {
			  nextRetorno = ListaProcessos(req,ProcessoBeanId,UsrLogado) ;
			  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
		  }  
		  if (acao.equals("R"))	{
			  nextRetorno ="" ;
			  ProcessoBeanId       = new ProcessoBean() ;
			  session.removeAttribute("ArquivoBeanId") ;
			  session.removeAttribute("ProcessoBeanId") ;
		  }
	
	  } catch (Exception ue) {
		  throw new sys.CommandException("ConsultaProcessoCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}


protected String ListaProcessos(HttpServletRequest req,ProcessoBean myProcesso,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	String nextRetorno=jspPadrao;		
	try { 		
		myProcesso.setNumProcesso(req.getParameter("numProcesso"));		
		myProcesso.setNumCPFEdt(req.getParameter("numCpf"));
		myProcesso.setNomResponsavel(req.getParameter("nomes"));	
		String ordem = "0";  // Nada escolhido
		//if (myProcesso.getNumProcesso().length()>0) ordem = "1";  // ordem de numero de processo	 
		//else if (myProcesso.getNumCPF().length()>0) ordem = "4";
		//else if (myProcesso.getNomResponsavel().length()>0) ordem = "5";
		//else  req.setAttribute("semProcesso","NENHUM PARAMETRO FORNECIDO"); 
		
		//if ("0".equals(ordem)==false){ 
			myProcesso.buscarListaProcessos(ordem,UsrLogado);  // ordem de numero de processo
			req.setAttribute("semProcesso","LOCALIZADO(s) "+myProcesso.getProcessos().size()+" PROCESSO(s)");			
		//}
	} catch (Exception e){
		throw new sys.CommandException("ConsultaProcessoCmd - ListaProcessos: " + e.getMessage());
	}
	return nextRetorno  ;
  }	
		  
}
