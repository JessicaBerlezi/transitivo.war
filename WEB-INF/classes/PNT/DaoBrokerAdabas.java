package PNT;


import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import ACSS.OrgaoBean;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.TAB.PenalidadeBean;
import PNT.TAB.StatusBean;
import REC.AutoInfracaoBean;
import REC.ParamOrgBean;


import sys.DaoException;
import sys.Util;

public class DaoBrokerAdabas extends sys.DaoBase implements DaoBroker
{
	
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "GER";
	
	public DaoBrokerAdabas() throws DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	public void buscarListaProcessos(ProcessoBean myProcesso, String ordem, ACSS.UsuarioBean myUsrLog) throws DaoException {
		boolean continua   = true;
		Connection conn 	= null;
		Statement stmt	    = null;
		ResultSet rs       = null;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			stmt = conn.createStatement();
			String resultado    = "";
			String opcEscolha   = "";
			String numProcesso  = "";
			String tipoCNH      = "";
			String numCNH       = "";
			String ufCNH        = "";
			String numCPF       = "";
			String numNotif     = "";
			String codStstus    = "";
			String datIni       = "";
			String datFim       = "";			
			String indContinua  = "            ";
			String orgaoAtuacao = "            ";
			String orgaoLotacao = "            ";
			String nomUserName  = "            ";
			
			numProcesso = sys.Util.rPad(myProcesso.getNumProcesso()," ",20);
			tipoCNH     = sys.Util.lPad(myProcesso.getTipCNH(),"0",1);
			numCNH      = sys.Util.lPad(myProcesso.getNumCNH(),"0",14);
			ufCNH       = sys.Util.rPad(myProcesso.getUfCNH()," ",2);
			numCPF      = sys.Util.lPad(myProcesso.getNumCPF(),"0",11);
			numNotif    = sys.Util.lPad("","0",9);
			codStstus   = sys.Util.lPad(myProcesso.getCodStatus(),"0",3);
			datIni      = sys.Util.lPad("","0",8);
			datFim      = sys.Util.lPad("","0",8);
			
			
			opcEscolha  = numProcesso +
			tipoCNH     +
			numCNH      +
			ufCNH       +
			numCPF      +
			numNotif    +
			codStstus   +
			datIni      +
			datFim      ;
			
			
			indContinua  = sys.Util.rPad(" "," ",20);
			
			orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
			
			List <ProcessoBean>processos = new ArrayList<ProcessoBean>();
			StatusBean myStatus = new StatusBean();
			int reg=0;
			while (continua) {
				resultado = chamaTransBRK("948",opcEscolha,indContinua+orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
				if (verErro(myProcesso,resultado,"Erro em Consulta Processos (Transa��o 948).")==false) { 
					return;
				}
				
				String linha = resultado.substring(3);
				int i = 0;
				for (i=0; i < 21; i++) {
					
					if ((!linha.substring(i * 152, i * 152 + 20).trim().equals(""))){ 
						
						ProcessoBean myProc = new ProcessoBean();
						setPosIni(0);											
						myProc.setNumProcesso(getPedaco(linha,20," "," "));
						myProc.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
						myProc.setTipCNH(getPedaco(linha,1," "," "));
						myProc.setNumCNH(getPedaco(linha,11," "," "));
						myProc.setUfCNH(getPedaco(linha,2," "," "));
						myProc.setDatValidadeCNH(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
						myProc.setCodPenalidade(getPedaco(linha,3," "," "));
						myProc.setPrazoSupensao(getPedaco(linha,3," "," "));
						myProc.setTotPontos(getPedaco(linha,5," "," "));
						myProc.setQtdMultas(getPedaco(linha,3," "," "));
						myProc.setTipProcesso(getPedaco(linha,1," "," "));
						myProc.setFormatProcesso(getPedaco(linha,1," "," "));
						myProc.setQtdProcAnteriores(getPedaco(linha,2," "," "));
						myProc.setNomResponsavel(getPedaco(linha,60," "," "));
						myProc.setNumCPF(getPedaco(linha,11," "," "));
						myProc.setCodStatus(getPedaco(linha,3," "," "));
						myStatus.setCodStatus(myProc.getCodStatus());
						myProc.setNomStatus(myStatus.DscStatus(myProc.getCodStatus()));
						myProc.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
						myProc.setTotNotificacoes(getPedaco(linha,2," "," "));
						
						/**
						 * FIXME
						 * Amarradinho de FIO para solucionar o problema de exibir a CNH do condutor.
						 * Lembrar de excluir, quando acertar no Ababas. 
						 */
						if (myProc.getNumCNH().equals("00000000000")){
							String sCmdCnh  = "SELECT NUM_CNH_CONDUTOR from TPNT_PROCESSO WHERE NUM_PROCESSO = '"+myProcesso.getNumProcesso()+"'" ;
							rs    = stmt.executeQuery(sCmdCnh) ;
							while (rs.next()) {
								myProc.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));	  				  	  				  
							}
						}
						
						processos.add(myProc) ;
					}
				}
				indContinua = linha.substring(21*150,21*150+20);			
				if (indContinua.trim().length()<=0) continua=false;
				reg++;
			}
			myProcesso.setProcessos(processos);
		}
		catch (Exception ex) {
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
		
	}	
	
	public void LeProcesso(ProcessoBean myProcesso) throws DaoException {					
		try {
			LeProcesso(myProcesso,"codigo");
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT)- LeProcesso(codigo): "+ex.getMessage());
		}
	}
	
	
	/*
	 public void LeProcesso(ProcessoBean myProcesso,String tpChave) throws DaoException {
	 Connection conn =null ;
	 String sCmd = "";
	 try {
	 String whr =" COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ";
	 if ("Processo/CPF".equals(tpChave)) { 
	 whr = " NUM_CPF_CONDUTOR='"+myProcesso.getNumCPF()+"' AND "+
	 " NUM_PROCESSO='"+myProcesso.getNumProcesso()+"' " ;
	 }
	 conn = serviceloc.getConnection(MYABREVSIST);	
	 Statement stmt    = conn.createStatement();
	 sCmd = "SELECT A.COD_PROCESSO,NUM_PROCESSO,to_char(DAT_PROCESSO,'dd/mm/yyyy') as DAT_PROCESSO,"+
	 "TIPO_PROCESSO,SIT_PROCESSO,TOT_PONTOS,"+
	 "NOM_CONDUTOR,NUM_CPF_CONDUTOR,TIP_CNH_CONDUTOR,"+
	 "NUM_CNH_CONDUTOR,UF_CNH_CONDUTOR,to_char(DAT_EMISSAO_CONDUTOR,'dd/mm/yyyy') as DAT_EMISSAO_CONDUTOR,"+
	 "to_char(DAT_VALID_CONDUTOR,'dd/mm/yyyy') as DAT_VALID_CONDUTOR," +
	 "CATEGORIA_CONDUTOR,DSC_END_CONDUTOR,NUM_CEP_CONDUTOR,NOM_MUNIC_CONDUTOR,UF_CONDUTOR,"+
	 "to_char(ULT_DAT_DEFESA,'dd/mm/yyyy') as ULT_DAT_DEFESA," +
	 "to_char(ULT_DAT_RECURSO,'dd/mm/yyyy') as ULT_DAT_RECURSO," +
	 "to_char(ULT_DAT_ENTREGACNH,'dd/mm/yyyy') as ULT_DAT_ENTREGACNH,A.COD_STATUS,E.NOM_STATUS," +
	 "to_char(DAT_ENTREGA_CNH,'dd/mm/yyyy') as DAT_ENTREGA_CNH,"+
	 "to_char(DAT_STATUS,'dd/mm/yyyy') as DAT_STATUS,NOM_USERNAME," +
	 "COD_ORGAO_LOTACAO,to_char(DAT_PROC,'dd/mm/yyyy') as DAT_PROC, "+
	 "COD_ORGAO,to_char(DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO,"+
	 "b.QTD_MULTAS,C.QTD_NOTIFICACAO,D.QTD_PROC_ANTERIORES,COD_ARQUIVO,TXT_EMAIL,  "+	
	 "to_char(DAT_PROCESSO_FIS,'dd/mm/yyyy') as DAT_PROCESSO_FIS,"+
	 "NOM_USERNAME_FIS,COD_ORGAO_LOTACAO_FIS," +
	 "to_char(DAT_PROC_FIS,'dd/mm/yyyy') as DAT_PROC_FIS, "+
	 "COD_PENALIDADE,DSC_PENALIDADE,DSC_PENALIDADE_RES  " +
	 " FROM (SELECT * FROM TPNT_PROCESSO WHERE "+whr+" ) a "+
	 " left JOIN (SELECT COUNT(*) AS QTD_MULTAS,COD_PROCESSO FROM TPNT_MULTA GROUP BY COD_PROCESSO)                  B on  (A.COD_PROCESSO=B.COD_PROCESSO) "+
	 " left JOIN (SELECT COUNT(*) AS QTD_NOTIFICACAO,COD_PROCESSO FROM TPNT_NOTIFICACAO GROUP BY COD_PROCESSO)       C  on  (A.COD_PROCESSO=C.COD_PROCESSO) "+
	 " left JOIN (SELECT COUNT(*) AS QTD_PROC_ANTERIORES,COD_PROCESSO FROM TPNT_PROC_ANTERIOR GROUP BY COD_PROCESSO) D  on  (A.COD_PROCESSO=D.COD_PROCESSO) "+
	 " left JOIN (SELECT COD_STATUS_AUTO AS COD_STATUS,NOM_STATUS_AUTO AS NOM_STATUS FROM TPNT_STATUS_PROCESSO) E  on  (A.COD_STATUS=E.COD_STATUS) ";
	 ResultSet rs    = stmt.executeQuery(sCmd) ;			
	 while (rs.next()) {
	 myProcesso.setCodProcesso(rs.getString("COD_PROCESSO"));
	 myProcesso.setNumProcesso(rs.getString("NUM_PROCESSO"));
	 myProcesso.setDatProcesso(rs.getString("DAT_PROCESSO"));
	 myProcesso.setTipProcesso(rs.getString("TIPO_PROCESSO"));				
	 myProcesso.setSitProcesso(rs.getString("SIT_PROCESSO"));
	 
	 myProcesso.setTotPontos(rs.getString("TOT_PONTOS"));
	 myProcesso.setQtdProcAnteriores(rs.getString("QTD_PROC_ANTERIORES"));
	 myProcesso.setQtdMultas(rs.getString("QTD_MULTAS"));	
	 myProcesso.setTotNotificacoes((rs.getString("QTD_NOTIFICACAO")));
	 
	 myProcesso.setNomResponsavel(rs.getString("NOM_CONDUTOR"));				
	 myProcesso.setNumCPF(rs.getString("NUM_CPF_CONDUTOR"));
	 myProcesso.setTipCNH(rs.getString("TIP_CNH_CONDUTOR"));
	 myProcesso.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));												
	 myProcesso.setUfCNH(rs.getString("UF_CNH_CONDUTOR"));
	 
	 myProcesso.setDatEmissaoCNH(rs.getString("DAT_EMISSAO_CONDUTOR"));
	 myProcesso.setDatValidadeCNH(rs.getString("DAT_VALID_CONDUTOR"));
	 myProcesso.setCategoriaCNH(rs.getString("CATEGORIA_CONDUTOR"));
	 myProcesso.setEndereco(rs.getString("DSC_END_CONDUTOR"));
	 myProcesso.setCep(rs.getString("NUM_CEP_CONDUTOR"));
	 myProcesso.setMunicipio(rs.getString("NOM_MUNIC_CONDUTOR"));
	 myProcesso.setUf(rs.getString("UF_CONDUTOR"));
	 
	 myProcesso.setUltDatDefesa(rs.getString("ULT_DAT_DEFESA"));
	 myProcesso.setUltDatRecurso(rs.getString("ULT_DAT_RECURSO"));
	 myProcesso.setUltDatEntregaCNH(rs.getString("ULT_DAT_ENTREGACNH"));
	 myProcesso.setDtEntregaCNH(rs.getString("DAT_ENTREGA_CNH"));
	 myProcesso.setCodStatus(rs.getString("COD_STATUS"));
	 myProcesso.setNomStatus(rs.getString("NOM_STATUS"));
	 myProcesso.setDatStatus(rs.getString("DAT_STATUS"));
	 
	 myProcesso.setNomUserName(rs.getString("NOM_USERNAME"));
	 myProcesso.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
	 myProcesso.setDatProc(rs.getString("DAT_PROC"));		
	 myProcesso.setCodOrgao(rs.getString("COD_ORGAO"));
	 myProcesso.setDatGeracao(rs.getString("DAT_GERACAO"));
	 myProcesso.setCodArquivo(rs.getString("COD_ARQUIVO"));				
	 myProcesso.setTxtEMail(rs.getString("TXT_EMAIL"));
	 myProcesso.setDatProcessoFis(rs.getString("DAT_PROCESSO_FIS"));					
	 myProcesso.setNomUserNameFis(rs.getString("NOM_USERNAME_FIS"));
	 myProcesso.setCodOrgaoLotacaoFis(rs.getString("COD_ORGAO_LOTACAO_FIS"));				
	 myProcesso.setDatProcFis(rs.getString("DAT_PROC_FIS"));					
	 
	 myProcesso.setCodPenalidade(rs.getString("COD_PENALIDADE"));
	 myProcesso.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
	 myProcesso.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
	 
	 }	
	 if (rs != null) rs.close();
	 stmt.close();
	 }
	 catch (Exception ex) {
	 throw new sys.DaoException("DaoBrokerAdabas(PNT)-LeProcesso : "+ex.getMessage()+" - "+sCmd);
	 }
	 finally {
	 if (conn != null) {
	 try { serviceloc.setReleaseConnection(conn); }
	 catch (Exception ey) { }
	 }
	 }
	 
	 }
	 */
	
	public void LeProcesso(ProcessoBean myProcesso,String tpChave) throws DaoException {
		boolean continua   = true;
		Connection conn 	= null;
		Statement stmt	    = null;
		ResultSet rs       = null;	  
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);	
			stmt = conn.createStatement();
			String resultado    = "";
			String opcEscolha   = "";
			String numProcesso  = "";			
			String numCPF       = "";
			String nome         = "";
			String numNotif     = "";					
			String orgaoAtuacao = "            ";
			String orgaoLotacao = "            ";
			String nomUserName  = "            ";
			numProcesso = sys.Util.rPad(myProcesso.getNumProcesso()," ",20);			
			numCPF      = sys.Util.lPad(myProcesso.getNumCPF(),"0",11);
			numNotif    = sys.Util.lPad("","0",9);
			opcEscolha  = numProcesso +			             
			numCPF      +
			numNotif    +	
			nome;
			
			StatusBean  myStatus = new StatusBean();
			UsuarioBean myUsrLog = new UsuarioBean();				
			orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
			
			resultado = chamaTransBRK("941",opcEscolha,orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
			if (verErro(myProcesso,resultado,"Erro em Consulta Processos (Transa��o 941).")==false) 
				return;
			
			String linha = resultado.substring(3);
			setPosIni(0);
			
			myProcesso.setNumProcesso(getPedaco(linha,20," "," "));				 
			myProcesso.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setDatProcessoFis(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setTipCNH(getPedaco(linha,1," "," "));
			myProcesso.setNumCNH(getPedaco(linha,11," "," "));				
			myProcesso.setUfCNH(getPedaco(linha,2," "," "));
			myProcesso.setDatEmissaoCNH(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setDatValidadeCNH(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setCodPenalidade(getPedaco(linha,3," "," "));
			if( myProcesso.getCodPenalidade().equals(" ")){
				System.out.println("Processo sem cod. Penalidade: "+ myProcesso.getNumProcesso());
			}
			String prazo=getPedaco(linha,5," "," ");
			prazo=String.valueOf(Integer.parseInt(prazo));
			myProcesso.setPrazoSupensao(prazo);
			myProcesso.setTotPontos(getPedaco(linha,5," "," "));
			myProcesso.setQtdMultas(getPedaco(linha,3," "," "));
			myProcesso.setTotNotificacoes(getPedaco(linha,3," "," "));
			myProcesso.setTipProcesso(getPedaco(linha,1," "," "));
			myProcesso.setSitProcesso(getPedaco(linha,1," "," "));
			myProcesso.setFormatProcesso(getPedaco(linha,1," "," "));
			myProcesso.setQtdProcAnteriores(getPedaco(linha,2," "," "));
			myProcesso.setNomResponsavel(getPedaco(linha,60," "," "));
			myProcesso.setNumCPF(getPedaco(linha,11," "," "));			 
			String sEndereco=getPedaco(linha,30," "," ")+" "+getPedaco(linha,5," "," ")+" "+
			getPedaco(linha,11," "," ");
			myProcesso.setEndereco(sEndereco);			
			myProcesso.setCodMunicipio(getPedaco(linha,5,"0"," "));
			
			//			 Ler Municipio 
			String sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+myProcesso.getMunicipio()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myProcesso.setMunicipio(rs.getString("nom_municipio"));	  				  	  				  
			}
			myProcesso.setCep(getPedaco(linha,8,"0"," "));
			myProcesso.setUf(getPedaco(linha,2," "," "));	
			myProcesso.setCodStatus(getPedaco(linha,3," "," "));			
			myProcesso.setNomStatus(myStatus.DscStatus(myProcesso.getCodStatus()));				
			myProcesso.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setUltDatDefesa(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setUltDatRecurso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setUltDatEntregaCNH(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setDtEntregaCNH(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
			myProcesso.setNumNotificacao(getPedaco(linha,9," "," "));
			myProcesso.setNomUserName(getPedaco(linha,20," "," "));
			myProcesso.setCodOrgaoLotacao(getPedaco(linha,6," "," "));
			
			/**
			 * Amarradinho de FIO para solucionar o problema de exibir a CNH do condutor.
			 * Lembrar de excluir, quando acertar no Ababas. 
			 */
			if (myProcesso.getNumCNH().equals("00000000000")){
				String sCmdCnh  = "SELECT NUM_CNH_CONDUTOR from TPNT_PROCESSO WHERE NUM_PROCESSO = '"+myProcesso.getNumProcesso()+"'" ;
				rs    = stmt.executeQuery(sCmdCnh) ;
				while (rs.next()) {
					myProcesso.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));	  				  	  				  
				}
			}
			/**
			 * Amarradinho de FIO para exibir a Data de defesa.
			 * Lembrar de excluir, quando acertar no Ababas.
			 */
			if (myProcesso.getUltDatDefesa().equals("")){
				String sCmdDatDefesa  = "SELECT ULT_DAT_DEFESA from TPNT_PROCESSO WHERE NUM_PROCESSO = '"+myProcesso.getNumProcesso()+"'" ;
				rs    = stmt.executeQuery(sCmdDatDefesa) ;
				while (rs.next()) {
					myProcesso.setUltDatDefesa(rs.getString("ULT_DAT_DEFESA"));	  				  	  				  
				}
			}
			
			//A Penalidade Bean � instanciada para obtermos a descri��o da penalidade
			PenalidadeBean myPenalidade = new PenalidadeBean();
			myPenalidade.setCodPenalidade(myProcesso.getCodPenalidade());
			myPenalidade.Le_Penalidade("codigo");
			myProcesso.setDscPenalidade(myPenalidade.getDscPenalidade());
			myProcesso.setDscPenalidadeRes(myPenalidade.getDscResumida());
			
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNTMudado)-LeProcesso : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
			if(stmt != null){
				try{stmt.close();}
				catch(Exception e){}
			}
			if(rs != null){
				try{ rs.close();}
				catch(Exception e){}
			}
		}
	}
	
	public void LeProcessoPenalidade(ProcessoBean myProcesso, String numProcessosAplicarPenalidade) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		try {
			String whr =" COD_STATUS in ("+myProcesso.getCodStatus()+") AND"+
			" COD_PENALIDADE IS NULL ";
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT A.COD_PROCESSO,NUM_PROCESSO,to_char(DAT_PROCESSO,'dd/mm/yyyy') as DAT_PROCESSO,"+
			"TIPO_PROCESSO,SIT_PROCESSO,TOT_PONTOS,"+
			"NOM_CONDUTOR,NUM_CPF_CONDUTOR,TIP_CNH_CONDUTOR,"+
			"NUM_CNH_CONDUTOR,UF_CNH_CONDUTOR,to_char(DAT_EMISSAO_CONDUTOR,'dd/mm/yyyy') as DAT_EMISSAO_CONDUTOR,"+
			"to_char(DAT_VALID_CONDUTOR,'dd/mm/yyyy') as DAT_VALID_CONDUTOR," +
			"CATEGORIA_CONDUTOR,DSC_END_CONDUTOR,NUM_CEP_CONDUTOR,NOM_MUNIC_CONDUTOR,UF_CONDUTOR,"+
			"to_char(ULT_DAT_DEFESA,'dd/mm/yyyy') as ULT_DAT_DEFESA," +
			"to_char(ULT_DAT_RECURSO,'dd/mm/yyyy') as ULT_DAT_RECURSO," +
			"to_char(ULT_DAT_ENTREGACNH,'dd/mm/yyyy') as ULT_DAT_ENTREGACNH,A.COD_STATUS,E.NOM_STATUS," +
			"to_char(DAT_ENTREGA_CNH,'dd/mm/yyyy') as DAT_ENTREGA_CNH,"+			
			"to_char(DAT_STATUS,'dd/mm/yyyy') as DAT_STATUS,NOM_USERNAME," +
			"COD_ORGAO_LOTACAO,to_char(DAT_PROC,'dd/mm/yyyy') as DAT_PROC, "+
			"COD_ORGAO,to_char(DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO,"+
			"b.QTD_MULTAS,C.QTD_NOTIFICACAO,D.QTD_PROC_ANTERIORES,COD_ARQUIVO,TXT_EMAIL,  "+	
			"to_char(DAT_PROCESSO_FIS,'dd/mm/yyyy') as DAT_PROCESSO_FIS,"+
			"NOM_USERNAME_FIS,COD_ORGAO_LOTACAO_FIS," +
			"to_char(DAT_PROC_FIS,'dd/mm/yyyy') as DAT_PROC_FIS, "+
			"COD_PENALIDADE,DSC_PENALIDADE  " +
			" FROM (SELECT * FROM TPNT_PROCESSO WHERE "+whr+" ) A "+
			" left JOIN (SELECT COUNT(*) AS QTD_MULTAS,COD_PROCESSO FROM TPNT_MULTA GROUP BY COD_PROCESSO)                  B on  (A.COD_PROCESSO=B.COD_PROCESSO) "+
			" left JOIN (SELECT COUNT(*) AS QTD_NOTIFICACAO,COD_PROCESSO FROM TPNT_NOTIFICACAO GROUP BY COD_PROCESSO)       C  on  (A.COD_PROCESSO=C.COD_PROCESSO) "+
			" left JOIN (SELECT COUNT(*) AS QTD_PROC_ANTERIORES,COD_PROCESSO FROM TPNT_PROC_ANTERIOR GROUP BY COD_PROCESSO) D  on  (A.COD_PROCESSO=D.COD_PROCESSO) "+
			" left JOIN (SELECT COD_STATUS_AUTO AS COD_STATUS,NOM_STATUS_AUTO AS NOM_STATUS FROM TPNT_STATUS_PROCESSO) E  on  (A.COD_STATUS=E.COD_STATUS) ORDER BY DAT_PROCESSO";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			ArrayList <ProcessoBean>myListProcAplicarPenalidade = new ArrayList<ProcessoBean>();
			int numProcessos=1;
			while (rs.next()) {
				numProcessos++;				
				ProcessoBean myProcAplicarPenalidade= new ProcessoBean();				
				myProcAplicarPenalidade.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProcAplicarPenalidade.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProcAplicarPenalidade.setDatProcesso(rs.getString("DAT_PROCESSO"));
				myProcAplicarPenalidade.setTipProcesso(rs.getString("TIPO_PROCESSO"));				
				myProcAplicarPenalidade.setSitProcesso(rs.getString("SIT_PROCESSO"));
				
				myProcAplicarPenalidade.setTotPontos(rs.getString("TOT_PONTOS"));
				myProcAplicarPenalidade.setQtdProcAnteriores(rs.getString("QTD_PROC_ANTERIORES"));
				myProcAplicarPenalidade.setQtdMultas(rs.getString("QTD_MULTAS"));	
				myProcAplicarPenalidade.setTotNotificacoes((rs.getString("QTD_NOTIFICACAO")));
				
				myProcAplicarPenalidade.setNomResponsavel(rs.getString("NOM_CONDUTOR"));				
				myProcAplicarPenalidade.setNumCPF(rs.getString("NUM_CPF_CONDUTOR"));
				myProcAplicarPenalidade.setTipCNH(rs.getString("TIP_CNH_CONDUTOR"));
				myProcAplicarPenalidade.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));												
				myProcAplicarPenalidade.setUfCNH(rs.getString("UF_CNH_CONDUTOR"));
				
				myProcAplicarPenalidade.setDatEmissaoCNH(rs.getString("DAT_EMISSAO_CONDUTOR"));
				myProcAplicarPenalidade.setDatValidadeCNH(rs.getString("DAT_VALID_CONDUTOR"));
				myProcAplicarPenalidade.setCategoriaCNH(rs.getString("CATEGORIA_CONDUTOR"));
				myProcAplicarPenalidade.setEndereco(rs.getString("DSC_END_CONDUTOR"));
				myProcAplicarPenalidade.setCep(rs.getString("NUM_CEP_CONDUTOR"));
				myProcAplicarPenalidade.setMunicipio(rs.getString("NOM_MUNIC_CONDUTOR"));
				myProcAplicarPenalidade.setUf(rs.getString("UF_CONDUTOR"));
				
				myProcAplicarPenalidade.setUltDatDefesa(rs.getString("ULT_DAT_DEFESA"));
				myProcAplicarPenalidade.setUltDatRecurso(rs.getString("ULT_DAT_RECURSO"));
				myProcAplicarPenalidade.setUltDatEntregaCNH(rs.getString("ULT_DAT_ENTREGACNH"));
				myProcAplicarPenalidade.setDtEntregaCNH(rs.getString("DAT_ENTREGA_CNH"));				
				myProcAplicarPenalidade.setCodStatus(rs.getString("COD_STATUS"));
				myProcAplicarPenalidade.setNomStatus(rs.getString("NOM_STATUS"));
				myProcAplicarPenalidade.setDatStatus(rs.getString("DAT_STATUS"));
				
				myProcAplicarPenalidade.setNomUserName(rs.getString("NOM_USERNAME"));
				myProcAplicarPenalidade.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myProcAplicarPenalidade.setDatProc(rs.getString("DAT_PROC"));		
				myProcAplicarPenalidade.setCodOrgao(rs.getString("COD_ORGAO"));
				myProcAplicarPenalidade.setDatGeracao(rs.getString("DAT_GERACAO"));
				myProcAplicarPenalidade.setCodArquivo(rs.getString("COD_ARQUIVO"));				
				myProcAplicarPenalidade.setTxtEMail(rs.getString("TXT_EMAIL"));
				myProcAplicarPenalidade.setDatProcessoFis(rs.getString("DAT_PROCESSO_FIS"));					
				myProcAplicarPenalidade.setNomUserNameFis(rs.getString("NOM_USERNAME_FIS"));
				myProcAplicarPenalidade.setCodOrgaoLotacaoFis(rs.getString("COD_ORGAO_LOTACAO_FIS"));				
				myProcAplicarPenalidade.setDatProcFis(rs.getString("DAT_PROC_FIS"));					
				
				myProcAplicarPenalidade.setCodPenalidade(rs.getString("COD_PENALIDADE"));
				myProcAplicarPenalidade.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
				myListProcAplicarPenalidade.add(myProcAplicarPenalidade);
				if (numProcessos>Integer.parseInt(numProcessosAplicarPenalidade))
					break;
			}	
			myProcesso.setProcessos(myListProcAplicarPenalidade);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT)-LeProcessoPenalidade : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	
	public void GravaProcesso(ProcessoBean myProcesso,Connection conn) throws DaoException {
		String sCmd = "";
		try {
			Statement stmt     = conn.createStatement();			
			ResultSet rs = null;
			
			sCmd = "SELECT SEQ_PNT_PROCESSO.NEXTVAL AS CODIGO FROM DUAL";
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
				myProcesso.setCodProcesso(rs.getString("CODIGO"));
			
			sCmd        = "INSERT into TPNT_PROCESSO ("+
			"COD_PROCESSO,NUM_PROCESSO,DAT_PROCESSO,TIPO_PROCESSO,"+
			"SIT_PROCESSO,TOT_PONTOS,QTD_PROC_ANTERIOR,QTD_MULTA,"+
			"NOM_CONDUTOR,NUM_CPF_CONDUTOR,TIP_CNH_CONDUTOR,NUM_CNH_CONDUTOR,"+
			"UF_CNH_CONDUTOR,DAT_EMISSAO_CONDUTOR,DAT_VALID_CONDUTOR,"+
			"CATEGORIA_CONDUTOR,DSC_END_CONDUTOR,NUM_CEP_CONDUTOR,NOM_MUNIC_CONDUTOR,"+
			"UF_CONDUTOR,ULT_DAT_DEFESA,ULT_DAT_RECURSO,ULT_DAT_ENTREGACNH,"+
			"COD_STATUS,DAT_STATUS,NOM_USERNAME,COD_ORGAO_LOTACAO,DAT_PROC,"+
			"COD_ORGAO,DAT_GERACAO,COD_ARQUIVO, COD_PENALIDADE, DSC_PENALIDADE, DSC_PENALIDADE_RES) VALUES ('"+myProcesso.getCodProcesso()+"',"+			
			"'"+myProcesso.getNumProcesso()+"',"+
			"to_date('"+myProcesso.getDatProcesso()+"','dd/mm/yyyy'),"+
			"'"+myProcesso.getTipProcesso()+"',"+
			"'"+myProcesso.getSitProcesso()+"',"+			
			"'"+myProcesso.getTotPontos()+"',"+
			"'"+myProcesso.getQtdProcAnteriores()+"',"+
			"'"+myProcesso.getQtdMultas()+"',"+			
			
			"'"+myProcesso.getNomResponsavel()+"',"+			
			"'"+myProcesso.getNumCPF()+"',"+
			"'"+myProcesso.getTipCNH()+"',"+
			"'"+myProcesso.getNumCNH()+"',"+	
			"'"+myProcesso.getUfCNH()+"',"+
			"to_date('"+myProcesso.getDatEmissaoCNH()+"','dd/mm/yyyy'),"+
			"to_date('"+myProcesso.getDatValidadeCNH()+"','dd/mm/yyyy'),"+
			"'"+myProcesso.getCategoriaCNH()+"',"+
			"'"+myProcesso.getEndereco()+"',"+
			"'"+myProcesso.getCep()+"',"+
			"'"+myProcesso.getMunicipio()+"',"+
			"'"+myProcesso.getUf()+"',"+
			
			"to_date('"+myProcesso.getUltDatDefesa()+"','dd/mm/yyyy'),"+
			"to_date('"+myProcesso.getUltDatRecurso()+"','dd/mm/yyyy'),"+
			"to_date('"+myProcesso.getUltDatEntregaCNH()+"','dd/mm/yyyy'),"+
			"'"+myProcesso.getCodStatus()+"',"+
			"to_date('"+myProcesso.getDatStatus()+"','dd/mm/yyyy'),"+
			"'"+myProcesso.getNomUserName()+"',"+
			"'"+myProcesso.getCodOrgaoLotacao()+"',"+
			"to_date('"+myProcesso.getDatProc()+"','dd/mm/yyyy'),"+
			"'"+myProcesso.getCodOrgao()+"',"+
			"to_date('"+myProcesso.getDatGeracao()+"','dd/mm/yyyy')," +
			"'"+myProcesso.getCodArquivo()+"',"+
			"'"+myProcesso.getCodPenalidade()+"',"+
			"'"+myProcesso.getDscPenalidade()+"',"+
			"'"+myProcesso.getDscPenalidadeRes()+
			"')";
			
			//System.out.println("gravarHistorico: "+sCmd );		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
	}	
	public void LeProcessoAnts(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_PROCESSO_ANT,COD_PROCESSO,NUM_PROCESSO_ANT," +
			"to_char(DAT_PROCESSO_ANT,'dd/mm/yyyy') as DAT_PROCESSO_ANT,"+
			"TIP_PROCESSO_ANT,SIT_PROCESSO_ANT,TOT_PONTOS_ANT "+
			" FROM TPNT_PROC_ANTERIOR "+
			" WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ORDER BY COD_PROCESSO_ANT ";
			ArrayList <ProcessoAntBean>myListProcAnt = new ArrayList<ProcessoAntBean>();				
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				ProcessoAntBean myProcAnt= new ProcessoAntBean();
				myProcAnt.setCodProcessoAnt(rs.getString("COD_PROCESSO_ANT"));				
				myProcAnt.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProcAnt.setNumProcessoAnt(rs.getString("NUM_PROCESSO_ANT"));
				myProcAnt.setDatProcessoAnt(rs.getString("DAT_PROCESSO_ANT"));
				myProcAnt.setTipProcessoAnt(rs.getString("TIP_PROCESSO_ANT"));				
				myProcAnt.setSitProcessoAnt(rs.getString("SIT_PROCESSO_ANT"));				
				myProcAnt.setTotPntProcessoAnt(rs.getString("TOT_PONTOS_ANT"));
				myListProcAnt.add(myProcAnt);
			}	
			myProcesso.setProcAnteriores(myListProcAnt);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeProcAnts : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void GravaProcessoAnt(ProcessoAntBean myProcesso,Connection conn) throws DaoException {
		String sCmd = "";
		try {
			sCmd        = "INSERT into TPNT_PROC_ANTERIOR ("+
			"COD_PROCESSO_ANT,COD_PROCESSO,NUM_PROCESSO_ANT,DAT_PROCESSO_ANT,"+
			"TIP_PROCESSO_ANT,SIT_PROCESSO_ANT,TOT_PONTOS_ANT) VALUES"+
			" (seq_pnt_proc_anterior.nextval,"+	
			"'"+myProcesso.getCodProcesso()+"',"+
			"'"+myProcesso.getNumProcessoAnt()+"',"+
			"to_date('"+myProcesso.getDatProcessoAnt()+"'dd/mm/yyyy'),"+
			"'"+myProcesso.getTipProcessoAnt()+"',"+
			"'"+myProcesso.getSitProcessoAnt()+"',"+			
			"'"+myProcesso.getTotPntProcessoAnt()+"')";
			Statement stmt     = conn.createStatement();
			//System.out.println("gravarHistorico: "+sCmd );		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
	}	
	
	
	
	public void LeNotificacoes(ProcessoBean myProcesso) throws DaoException {
		boolean continua   = true;
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			ResultSet rs = null;			
			String resultado    = "";
			String opcEscolha   = "";
			String numProcesso  = "";		
			String indContinua  = "            ";
			String orgaoAtuacao = "            ";
			String orgaoLotacao = "            ";
			String nomUserName  = "            ";
			UsuarioBean myUsrLog = new UsuarioBean();
			numProcesso = sys.Util.rPad(myProcesso.getNumProcesso()," ",20);			
			opcEscolha  = numProcesso ;
			indContinua  = sys.Util.rPad(" "," ",20);
			orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
			List <NotificacaoBean>notifs = new ArrayList<NotificacaoBean>();
			String linhaArquivo = null;
			while (continua) {
				resultado = chamaTransBRK("947",opcEscolha,indContinua+orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
				if (verErro(myProcesso,resultado,"Erro em Consulta Notifica��es (Transa��o 947).")==false) { 
					return;
				}
				String linha = resultado.substring(3);
				int i = 0;
				for (i=0; i < 64; i++) {
					linha = resultado.substring((i*50)+3,(i+1)*50+3);
					if (linha.trim().length()<=0) 
						break;
					NotificacaoBean myNotifs = new NotificacaoBean();
					setPosIni(0);			
					myProcesso.setNumProcesso(getPedaco(linha,20," "," "));
					myNotifs.setNumNotificacao(getPedaco(linha,9," "," "));
					myNotifs.setSeqNotificacao(getPedaco(linha,3," "," "));

					sCmd = "SELECT to_char(DAT_GERACAO_CB,'dd/mm/yyyy') as DAT_GERACAO_CB FROM TPNT_NOTIFICACAO "+
					" WHERE NUM_NOTIFICACAO='"+myNotifs.getNumNotificacao()+"' AND SEQ_NOTIFICACAO='"+myNotifs.getSeqNotificacao()+"'";
					rs    = stmt.executeQuery(sCmd) ;
					if (rs.next()) myNotifs.setDatGeracaoCB(rs.getString("DAT_GERACAO_CB"));
					
					// In�cio da Altera��o em 07/11/2011 por Jefferson Trindade para arquivo Pontos
					// Altera��o para pegar algumas informa��es do arquivo txt
					sCmd = "SELECT COD_LINHA_ARQUIVO, DSC_LINHA_ARQUIVO FROM TPNT_ARQUIVO_LINHA WHERE TIP_LINHA = 2 AND NUM_NOTIFICACAO = '"+myNotifs.getNumNotificacao()+"'";
					rs = stmt.executeQuery(sCmd);
					
					linhaArquivo = "";
					while (rs.next()){
						if (linhaArquivo.equals("")){
							linhaArquivo = rs.getString("DSC_LINHA_ARQUIVO");
							myNotifs.setTipoNotificacao(linhaArquivo.substring(252,253));
							myNotifs.setDatEmissao(linhaArquivo.substring(30,38));
							myNotifs.setTipoReemissao(linhaArquivo.substring(251,252));
							
							myNotifs.getRetorno().setCodCodigoRetorno(Integer.parseInt(getPedaco(linha,2," "," ")));
							myNotifs.setNumLoteAR(getPedaco(linha,8," "," "));
							notifs.add(myNotifs) ;
							break;
						}else{
							continue;
						}
					}
				}	
				setPosIni(3+64*50);			
				indContinua = getPedaco(resultado,20," "," ");
				if (indContinua.trim().length()<=0) continua=false;
			}
			
			myProcesso.setNotificacoes(notifs);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	} 	
	/*	
	 public void LeNotificacoes(ProcessoBean myProcesso) throws DaoException {
	 Connection conn =null ;
	 String sCmd = "";
	 try {
	 conn = serviceloc.getConnection(MYABREVSIST);	
	 Statement stmt = conn.createStatement();
	 sCmd = "SELECT COD_NOTIFICACAO FROM TPNT_NOTIFICACAO "+
	 " WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ORDER BY DAT_EMISSAO DESC,NUM_NOTIFICACAO,SEQ_NOTIFICACAO ";
	 ArrayList <NotificacaoBean>myListNotif = new ArrayList<NotificacaoBean>();			
	 ResultSet rs    = stmt.executeQuery(sCmd) ;			
	 while (rs.next()) {
	 NotificacaoBean myNotificacao  = new NotificacaoBean();
	 myNotificacao.setCodNotificacao(rs.getString("COD_NOTIFICACAO"));				
	 LeNotificacao(myNotificacao,"codigo");
	 myListNotif.add(myNotificacao);
	 }
	 myProcesso.setNotificacoes(myListNotif);
	 if (rs != null) rs.close();
	 stmt.close();
	 }
	 catch (Exception ex) {
	 throw new sys.DaoException("DaoBrokerAdabas-LeNotificacao : "+ex.getMessage()+" - "+sCmd);
	 }
	 finally {
	 if (conn != null) {
	 try { serviceloc.setReleaseConnection(conn); }
	 catch (Exception ey) { }
	 }
	 }
	 }
	 */
	
	public void LeNotificacao(NotificacaoBean myNotif,String tpChave) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		boolean bNaoAchou=true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_NOTIFICACAO,NUM_NOTIFICACAO,TIP_NOTIFICACAO,SEQ_NOTIFICACAO," +
			"COD_PROCESSO,TIP_REEMISSAO, "+
			"to_char(DAT_EMISSAO,'dd/mm/yyyy') as DAT_EMISSAO,"+
			"NOM_USERNAME,COD_ORGAO_LOTACAO,to_char(DAT_PROC,'dd/mm/yyyy') as DAT_PROC,"+
			"COD_ARQUIVO,"+
			"to_char(DAT_GERACAO_CB,'dd/mm/yyyy') as DAT_GERACAO_CB,"+
			"to_char(DAT_PUBL_DO,'dd/mm/yyyy') as DAT_PUBL_DO,"+
			"to_char(DAT_IMPRESSAO,'dd/mm/yyyy') as DAT_IMPRESSAO,"+
			"SEQ_IMPRESSAO,"+
			"NOM_USERNAME_IMPRESSAO,COD_ORGAO_LOTACAO_IMPRESSAO, "+
			"to_char(DAT_PROC_IMPRESSAO,'dd/mm/yyyy') as DAT_PROC_IMPRESSAO, "+
			"SEQ_NOTIFICACAO_IMPRESSAO,COD_RETORNO,IND_ENTREGUE,NOM_USERNAME_RETORNO, "+           
			"COD_ORGAO_LOTACAO_RETORNO, "+      
			"to_char(DAT_PROC_RETORNO,'dd/mm/yyyy') as DAT_PROC_RETORNO, "+			  
			"to_char(DAT_RETORNO,'dd/mm/yyyy') as DAT_RETORNO "+			  
			" FROM TPNT_NOTIFICACAO "+
			" WHERE ";
			if (tpChave.equals("codigo"))
				sCmd+="COD_NOTIFICACAO='"+myNotif.getCodNotificacao()+"'";
			else
				sCmd+="NUM_NOTIFICACAO='"+myNotif.getNumNotificacao()+
				"' AND SEQ_NOTIFICACAO='"+myNotif.getSeqNotificacao()+"'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				myNotif.setCodNotificacao(rs.getString("COD_NOTIFICACAO"));				
				myNotif.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				myNotif.setTipoNotificacao(rs.getString("TIP_NOTIFICACAO"));
				myNotif.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));
				myNotif.setCodProcesso(rs.getString("COD_PROCESSO"));
				myNotif.setTipoReemissao(rs.getString("TIP_REEMISSAO"));
				myNotif.setDatEmissao(rs.getString("DAT_EMISSAO"));				
				myNotif.setNomUserName(rs.getString("NOM_USERNAME"));				
				myNotif.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));				
				myNotif.setDatProc(rs.getString("DAT_PROC"));
				myNotif.setCodArquivo(rs.getString("COD_ARQUIVO"));
				myNotif.setDatGeracaoCB(rs.getString("DAT_GERACAO_CB"));
				myNotif.setDatPublDO(rs.getString("DAT_PUBL_DO"));
				myNotif.setDatImpressao(rs.getString("DAT_IMPRESSAO"));
				myNotif.setSeqImpressao(rs.getString("SEQ_IMPRESSAO"));
				myNotif.setNomUserNameImpressao(rs.getString("NOM_USERNAME_IMPRESSAO"));				
				myNotif.setCodOrgaoLotacaoImpressao(rs.getString("COD_ORGAO_LOTACAO_IMPRESSAO"));				
				myNotif.setDatProcImpressao(rs.getString("DAT_PROC_IMPRESSAO"));
				myNotif.setSeqNotificacaoImpressao(rs.getString("SEQ_NOTIFICACAO_IMPRESSAO"));
				myNotif.getRetorno().setNumRetorno(rs.getString("COD_RETORNO"));
				myNotif.getRetorno().setIndEntregue(rs.getString("IND_ENTREGUE"));
				myNotif.setDatRetorno(rs.getString("DAT_RETORNO"));		
				myNotif.setNomUserNameRetorno(rs.getString("NOM_USERNAME_RETORNO"));			
				myNotif.setCodOrgaoLotacaoRetorno(rs.getString("COD_ORGAO_LOTACAO_RETORNO"));
				myNotif.setDatProcRetorno(rs.getString("DAT_PROC_RETORNO"));
				bNaoAchou=false;
			}
			if (bNaoAchou)	{
				myNotif.setCodNotificacao("0");
				myNotif.setNumNotificacao("0");
				myNotif.setSeqNotificacao("0");
			}
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT)-LeProcesso : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void AtualizaNotificacao(NotificacaoBean myNotif) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TPNT_NOTIFICACAO SET "+
			"NUM_NOTIFICACAO='"+myNotif.getNumNotificacao()+"',"+                
			"TIP_NOTIFICACAO='"+myNotif.getTipoNotificacao()+"',"+                
			"SEQ_NOTIFICACAO='"+myNotif.getSeqNotificacao()+"',"+                
			"COD_PROCESSO='"+myNotif.getCodProcesso()+"',"+                   
			"TIP_REEMISSAO='"+myNotif.getTipoReemissao()+"',"+                  
			"DAT_EMISSAO = TO_DATE('"+myNotif.getDatEmissao()+"','DD/MM/YYYY'),"+
			"NOM_USERNAME='"+myNotif.getNomUserName()+"',"+                   
			"COD_ORGAO_LOTACAO='"+myNotif.getCodOrgaoLotacao()+"',"+              
			"DAT_PROC = TO_DATE('"+myNotif.getDatProc()+"','DD/MM/YYYY'),"+
			"COD_ARQUIVO='"+myNotif.getCodArquivo()+"',"+                    
			"DAT_GERACAO_CB = TO_DATE('"+myNotif.getDatGeracaoCB()+"','DD/MM/YYYY'),"+
			"DAT_IMPRESSAO = TO_DATE('"+myNotif.getDatImpressao()+"','DD/MM/YYYY'),"+
			"SEQ_IMPRESSAO='"+myNotif.getSeqImpressao()+"',"+                  
			"NOM_USERNAME_IMPRESSAO='"+myNotif.getNomUserNameImpressao()+"',"+         
			"COD_ORGAO_LOTACAO_IMPRESSAO='"+myNotif.getCodOrgaoLotacaoImpressao()+"',"+
			"DAT_PROC_IMPRESSAO = TO_DATE('"+myNotif.getDatProcImpressao()+"','DD/MM/YYYY'),"+
			"SEQ_NOTIFICACAO_IMPRESSAO='"+myNotif.getSeqNotificacaoImpressao()+"',"+      
			"DAT_PUBL_DO = TO_DATE('"+myNotif.getDatPublDO()+"','DD/MM/YYYY'),"+
			"COD_RETORNO='"+myNotif.getRetorno().getNumRetorno()+"',"+ 
			"IND_ENTREGUE='"+myNotif.getRetorno().getIndEntregue()+"',"+
			"NOM_USERNAME_RETORNO='"+myNotif.getNomUserNameRetorno()+"',"+           
			"COD_ORGAO_LOTACAO_RETORNO='"+myNotif.getCodOrgaoLotacaoRetorno()+"',"+      
			"DAT_PROC_RETORNO = TO_DATE('"+myNotif.getDatProcRetorno()+"','DD/MM/YYYY'), "+
			"DAT_RETORNO = TO_DATE('"+myNotif.getDatRetorno()+"','DD/MM/YYYY') "+			
			"WHERE "+
			"COD_NOTIFICACAO='"+myNotif.getCodNotificacao()+"'";
			stmt.execute(sCmd) ;
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT)-LeProcesso : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}	
	public void GravaNotificacao(NotificacaoBean myNotificacao,Connection conn) throws DaoException {
		String sCmd = "";
		try {
			sCmd    = "INSERT into TPNT_NOTIFICACAO ("+
			"COD_NOTIFICACAO,COD_PROCESSO,NUM_NOTIFICACAO,SEQ_NOTIFICACAO," +
			"DAT_EMISSAO,"+
			"TIP_NOTIFICACAO,TIP_REEMISSAO,COD_ARQUIVO, "+
			"NOM_USERNAME,COD_ORGAO_LOTACAO,DAT_PROC,DAT_GERACAO_CB,DAT_PUBL_DO) "+
			" VALUES "+
			" (seq_pnt_notificacao.nextval,"+	
			"'"+myNotificacao.getCodProcesso()+"',"+
			"'"+myNotificacao.getNumNotificacao()+"',"+
			"'"+myNotificacao.getSeqNotificacao()+"',"+
			"to_date('"+myNotificacao.getDatEmissao()+"','dd/mm/yyyy'),"+
			"'"+myNotificacao.getTipoNotificacao()+"',"+
			"'"+myNotificacao.getTipoReemissao()+"',"+
			"'"+myNotificacao.getCodArquivo()+"',"+
			"'"+myNotificacao.getNomUserName()+"',"+
			"'"+myNotificacao.getCodOrgaoLotacao()+"',"+
			"to_date('"+myNotificacao.getDatProc()+"','dd/mm/yyyy'), "+
			"to_date('"+myNotificacao.getDatGeracaoCB()+"','dd/mm/yyyy'), "+	
			"to_date('"+myNotificacao.getDatPublDO()+"','dd/mm/yyyy')) ";	
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException("DaoBrokerAdabas(PNT)-GravaNotificacao : "+e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException("DaoBrokerAdabas(PNT)-GravaNotificacao : "+e.getMessage()+" - "+sCmd);
		}//fim do catch
	}
	public void ExcluirNotificacao(NotificacaoBean myNotif,Connection conn) throws DaoException {
		String sCmd = "";
		try {				
			sCmd = "DELETE TPNT_NOTIFICACAO WHERE "+
			"COD_NOTIFICACAO='"+myNotif.getCodNotificacao()+"'";
			Statement stmt = conn.createStatement();
			stmt.execute(sCmd) ;
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT)-ExcluirNotificacao : "+ex.getMessage()+" - "+sCmd);
		}
	}
	
	public void LeMultas(ProcessoBean myProcesso) throws DaoException {
		boolean continua   = true;
		try {
			String resultado    = "";
			String opcEscolha   = "";
			String numProcesso  = "";
			String indContinua  = "            ";
			String orgaoAtuacao = "            ";
			String orgaoLotacao = "            ";
			String nomUserName  = "            ";
			numProcesso         = sys.Util.rPad(myProcesso.getNumProcesso()," ",20);
			opcEscolha          = numProcesso;
			indContinua  = sys.Util.rPad(" "," ",20);
			
			UsuarioBean myUsrLog = new UsuarioBean();
			orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
			
			ArrayList <MultasBean>myListMulta = new ArrayList<MultasBean>();
			OrgaoBean myOrgao = new OrgaoBean();
			int reg=0;
			while (continua) {
				resultado = chamaTransBRK("946",opcEscolha,indContinua,myUsrLog);
				if (verErro(myProcesso,resultado,"Erro em Consulta Processos (Transa��o 946).")==false) { 
					return;
				}
				
				String linha = resultado.substring(3);
				int i = 0;
				int cont=1;
				setPosIni(0);
				for (i=0; i < 12; i++) {
					if ((!linha.substring(i * 257, i * 257 + 20).trim().equals(""))){ 
						
						MultasBean myMulta = new MultasBean();
						
						
						myMulta.setNumProcesso(getPedaco(linha,20," "," ")); 					    
						myMulta.setNumAutoInfracao(getPedaco(linha,12," "," "));
						myMulta.setNumPlaca(getPedaco(linha,7," "," "));
						myMulta.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
						myMulta.setDatTransJulgado(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
						myMulta.setValHorInfracao(getPedaco(linha,4," "," "));
						myMulta.setCodOrgao(getPedacoNum(linha,6,0,"."));
						myOrgao.setCodOrgao(myMulta.getCodOrgao());
						myOrgao.Le_Orgao(myOrgao.getCodOrgao(),0);
						myMulta.setSigOrgao(myOrgao.getSigOrgao());           
						myMulta.setCodInfracao(getPedaco(linha,5," "," "));
						myMulta.setDscInfracao(getPedaco(linha,80," "," "));
						myMulta.setDscEnquadramento(getPedaco(linha,20," "," "));
						myMulta.setNumPonto(getPedacoNum(linha,1,0,"."));
						myMulta.setDscLocalInfracao(getPedaco(linha,80," "," "));
						myMulta.setSeqNotificacao(getPedacoNum(linha,3,0,"."));                
						myMulta.setSeqMultaNotificacao(getPedacoNum(linha,3,0,"."));                   
						myListMulta.add(myMulta);	
						setPosIni(cont*257);
						cont++;
					}                
					
				}
				indContinua = linha.substring(12*257,12*257+20);			
				if (indContinua.trim().length()<=0) continua=false;
				reg++;
			}
			myProcesso.setMultas(myListMulta);
			myProcesso.setQtdMultas(String.valueOf(myListMulta.size()));
		}
		catch (Exception ex) {
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
	}
	
	
	
	public void GravaMulta(MultasBean myMulta,Connection conn) throws DaoException {
		String sCmd = "";
		try{
			sCmd  =  "INSERT INTO TPNT_MULTA ( "+
			"COD_MULTA,COD_PROCESSO,NUM_PROCESSO,DAT_PROCESSO, "+			
			"NUM_NOTIFICACAO,SEQ_NOTIFICACAO,SEQ_MULTA,COD_ORGAO,SIG_ORGAO, "+           
			"NUM_AUTO_INFRACAO,NUM_PLACA,DAT_INFRACAO, "+   
			"VAL_HOR_INFRACAO,DSC_LOCAL_INFRACAO,COD_INFRACAO,DSC_ENQUADRAMENTO, "+              
			"NUM_PONTO,DAT_TRANS_JULGADO,NOM_USERNAME,COD_ORGAO_LOTACAO, "+              
			"DAT_PROC,DSC_INFRACAO) "+                   
			" VALUES (" +			 
			" SEQ_PNT_MULTA.NEXTVAL,"+                      
			" '"+myMulta.getCodProcesso()+"','"+myMulta.getNumProcesso()+"',"+                
			"to_date('"+myMulta.getDatProcesso()+"','dd/mm/yyyy'),"+
			" '"+myMulta.getNumNotificacao()+"','"+myMulta.getSeqNotificacao()+"',"+                
			" '"+myMulta.getSeqMultaNotificacao()+"',"+                   
			" '"+myMulta.getCodOrgao()+"','"+myMulta.getSigOrgao()+"',"+           
			" '"+myMulta.getNumAutoInfracao()+"','"+myMulta.getNumPlaca()+"',"+                
			"to_date('"+myMulta.getDatInfracao()+"','dd/mm/yyyy'),"+
			" '"+myMulta.getValHorInfracao()+"','"+myMulta.getDscLocalInfracao()+"',"+   
			" '"+myMulta.getCodInfracao()+"',"+
			" '"+myMulta.getDscEnquadramento()+"','"+myMulta.getNumPonto()+"',"+           
			"to_date('"+myMulta.getDatTransJulgado()+"','dd/mm/yyyy'),"+			 
			" '"+myMulta.getNomUserName()+"','"+myMulta.getCodOrgaoLotacao()+"',"+              
			"to_date('"+myMulta.getDatProc()+"','dd/mm/yyyy'),"+
			" '"+myMulta.getDscInfracao()+"')";         
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
		return ;
	}

	public void LeRequerimentos(ProcessoBean myProcesso) throws DaoException {
		
		Connection conn =null ;
		String sCmd = "";
		ResultSet rs       = null;	 
		boolean continua   = true;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			
			String resultado  		    = "";
			String opcEscolha   		= "";
			String numProcesso  		= sys.Util.rPad(myProcesso.getNumProcesso(),"",20);			
			String indContinua     		= "";
			String orgaoAtuacao 		= "            ";
			String orgaoLotacao 		= "            ";
			String nomUserName  		= "            ";
			int reg=0;
			indContinua   = sys.Util.rPad(""," ",15);
			opcEscolha  = numProcesso ;
			UsuarioBean myUsrLog= new UsuarioBean();
			orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
			
			List <RequerimentoBean> myReq = new ArrayList<PNT.RequerimentoBean>();
			int seq=1;
			while (continua)
			{   
				resultado = chamaTransBRK("945",opcEscolha,indContinua,myUsrLog);
				if (verErro(myProcesso,resultado,"Erro em Consulta Processos (Transa��o 945).")==false) 
				{ 
					return;
				}
				RequerimentoBean req=new RequerimentoBean();				
				
				String linha = resultado.substring(3);				
				
				if ((linha.trim().length()<=0) ){
					continua=false;						
				}
				else{ 
					req = new PNT.RequerimentoBean();
					setPosIni(0);
					myProcesso.setNumProcesso(getPedaco(linha,20," "," "));
					req.setCodTipoSolic(getPedaco(linha,2," "," "));
					req.setNumRequerimento(getPedaco(linha,20," "," "));
					myProcesso.setNomResponsavel(getPedaco(linha,60," "," "));
					req.setCodStatusRequerimento(getPedaco(linha,1," "," "));
					req.setNomUserNameDP(getPedaco(linha,20," "," "));
					req.setCodOrgaoLotacaoDP(getPedaco(linha,6," "," "));
					req.setDatProcDP(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					req.setDatRequerimento(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					req.setDatJU(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8))); 
					req.setCodJuntaJU(getPedaco(linha,6," "," "));
					req.setSigJuntaJU(getPedaco(linha,10," "," "));
					req.setTipoJunta(getPedaco(linha,1," "," "));
					req.setCpfRelatorJU(getPedaco(linha,11," "," "));
					req.setNomUserNameJU(getPedaco(linha,20," "," "));
					req.setCodOrgaoLotacaoJU(getPedaco(linha,6," "," "));
					req.setDatProcRS(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					req.setDatRS(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					req.setCodResultRS(getPedaco(linha,1," "," "));
					req.setTxtMotivo(getPedaco(linha,1000," "," "));
					req.setCodPenalidade(getPedaco(linha,3," "," "));
					req.setDscPenalidade(getPedaco(linha,100," "," "));
					req.setNomUserNameRS(getPedaco(linha,20," "," "));
					req.setCodOrgaoLotacaoRS(getPedaco(linha,6," "," "));
					req.setDatProcRS(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					
//					 Ler Penalidade
					sCmd  = "SELECT DSC_PENALIDADE from TPNT_PENALIDADE WHERE cod_penalidade = '"+req.getCodPenalidade()+"'" ;
					rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						req.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
					}					
					myReq.add(req);
					
					seq++;
					setPosIni(1361);
					indContinua = getPedaco(linha,15," "," ") ;
					if ((indContinua.trim().length()<=0) ) continua=false;						
				}
			}
			myProcesso.setRequerimentos(myReq);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeRequerimentos: "+ex.getMessage());
		}finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}

	public void GravaRequerimentoGuia(RequerimentoBean myReq,Connection conn) throws DaoException {
		String sCmd = "";
		try{
			
			sCmd = "SELECT num_requerimento FROM tpnt_requerimento WHERE num_requerimento = ?";
			PreparedStatement pStmt = conn.prepareStatement(sCmd);
			pStmt.setString(1, myReq.getNumRequerimento());
			ResultSet rs = pStmt.executeQuery();
			if (rs.next()){
				AtualizarRequerimento(myReq, conn);
			}else{
				GravaRequerimento(myReq, conn);
			}
			
			pStmt.close();
			rs.close();
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	
	public void GravaRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException {
		String sCmd = "";
		try{
			sCmd  =  "INSERT INTO TPNT_REQUERIMENTO " +
			"(COD_REQUERIMENTO,COD_PROCESSO,COD_TIPO_SOLIC,NUM_REQUERIMENTO, "+
			"DAT_REQUERIMENTO," +
			"COD_STATUS_REQUERIMENTO,"+
			"NOM_USERNAME_DEF,COD_ORGAO_LOTACAO_DEF,DAT_PROC_DEF) "+
			" VALUES (" +			 			             
			" seq_pnt_requerimento.nextval,"+				 
			" '"+myReq.getCodProcesso()+"','"+myReq.getCodTipoSolic()+"',"+
			" '"+myReq.getNumRequerimento()+"',"+
			" to_date('"+myReq.getDatRequerimento()+"','dd/mm/yyyy'), "+
			" '"+myReq.getCodStatusRequerimento()+"',"+
			" '"+myReq.getNomUserNameDP() +"','"+myReq.getCodOrgaoLotacaoDP()+"',"+
			" to_date('"+myReq.getDatProcDP()+"','dd/mm/yyyy'))";		
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	public void AtualizarRequerimento(RequerimentoBean myReq,Connection conn) throws DaoException {		
		String sCmd = "";
		try {
			sCmd  =  "UPDATE TPNT_REQUERIMENTO SET  " +
			" COD_STATUS_REQUERIMENTO = '"+myReq.getCodStatusRequerimento()+"', "+			
			" DAT_RES = to_date('"+myReq.getDatRS()+"','dd/mm/yyyy'),"+
			" COD_RESULT_RES = '"+myReq.getCodResultRS()+"',"+
			" TXT_MOTIVO_RES = '"+myReq.getTxtMotivoRS()+"',"+			 
			" NOM_USERNAME_RES = '"+myReq.getNomUserNameRS()+"', "+
			" COD_ORGAO_LOTACAO_RES = '"+myReq.getCodOrgaoLotacaoRS()+"', "+
			" DAT_PROC_RES = to_date('"+myReq.getDatProcRS()+"','dd/mm/yyyy') "+			 
			" WHERE COD_REQUERIMENTO='"+myReq.getCodRequerimento() +"' ";
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
			return  ;
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - informaResult: "+e.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void AtualizarRequerimentoAta(RequerimentoBean myReq,Connection conn) throws DaoException {		
		String sCmd = "";
		try {
			sCmd  =  "UPDATE TPNT_REQUERIMENTO SET  " +
			" COD_ENVIO_EDO = '"+myReq.getCodEnvioEdo()+"', "+	
			" COD_STATUS_REQUERIMENTO = '"+myReq.getCodStatusRequerimento()+"', "+	
			" DAT_RES = to_date('"+myReq.getDatEnvioEdo()+"','dd/mm/yyyy')"+			 
			" WHERE COD_REQUERIMENTO='"+myReq.getCodRequerimento() +"' ";
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
			return  ;
		}
		catch (Exception e) {
			throw new DaoException("DaoBrokerAdabas - AtualizarRequerimentoAta: "+e.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}	
	
	public void AtualizarRequerimentoAtaPeloNumero(RequerimentoBean myReq,Connection conn) throws DaoException {		
		String sCmd = "";
		try {
			sCmd  =  "UPDATE TPNT_REQUERIMENTO SET  " +
			" COD_ENVIO_EDO = '"+myReq.getCodEnvioEdo()+"', "+	
			" COD_STATUS_REQUERIMENTO = '"+myReq.getCodStatusRequerimento()+"' ";	
			
			if(myReq.getDatEnvioEdo().length() > 0)
				sCmd+=", DAT_EDO = to_date('"+myReq.getDatEnvioEdo()+"','dd/mm/yyyy')";
			if(myReq.getDatPubliPDO().length() > 0)
				sCmd+=", DAT_PDO = to_date('"+myReq.getDatPubliPDO()+"','dd/mm/yyyy')";
			
			sCmd+=" WHERE NUM_REQUERIMENTO='"+myReq.getNumRequerimento() +"' ";
			Statement stmt     = conn.createStatement();		
			stmt.execute(sCmd) ;
			stmt.close();
			conn.commit();
			return  ;
		}
		catch (Exception e) {
			try{
				conn.rollback();
			}catch (Exception ex) {
				throw new DaoException("DaoBrokerAdabas - AtualizarRequerimentoAta: "+e.getMessage()+" - "+sCmd);
			}
			throw new DaoException("DaoBrokerAdabas - AtualizarRequerimentoAta: "+e.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void GravaHistorico(PNT.HistoricoBean myHis,Connection conn) throws DaoException {
		String sCmd = "";
		try{
			sCmd  =  "INSERT INTO TPNT_HISTORICO_PROCESSO " +
			"(COD_HISTORICO_PROCESSO,"+
			"COD_PROCESSO,NUM_PROCESSO,COD_STATUS,"+
			"DAT_STATUS,COD_EVENTO,COD_ORIGEM_EVENTO,"+ 
			"NOM_USERNAME,COD_ORGAO_LOTACAO,DAT_PROC,"+
			"TXT_COMPLEMENTO_01,TXT_COMPLEMENTO_02,TXT_COMPLEMENTO_03,"+
			"TXT_COMPLEMENTO_04,TXT_COMPLEMENTO_05,TXT_COMPLEMENTO_06,"+
			"TXT_COMPLEMENTO_07,TXT_COMPLEMENTO_08,TXT_COMPLEMENTO_09,"+
			"TXT_COMPLEMENTO_10,TXT_COMPLEMENTO_11,TXT_COMPLEMENTO_12) "+
			" VALUES (" +			 			             
			" seq_pnt_historico_processo.nextval,"+
			" '"+myHis.getCodProcesso()+"','"+myHis.getNumProcesso()+"',"+
			" '"+myHis.getCodStatus()+"',"+
			" to_date('"+myHis.getDatStatus()+"','dd/mm/yyyy'), "+
			" '"+myHis.getCodEvento()+"','"+myHis.getCodOrigemEvento()+"',"+
			" '"+myHis.getNomUserName()+"','"+myHis.getCodOrgaoLotacao()+"',"+
			" to_date('"+myHis.getDatProc()+"','dd/mm/yyyy'), "+
			" '"+myHis.getTxtComplemento01()+"','"+myHis.getTxtComplemento02()+"',"+
			" '"+myHis.getTxtComplemento03()+"','"+myHis.getTxtComplemento04()+"',"+
			" '"+myHis.getTxtComplemento05()+"','"+myHis.getTxtComplemento06()+"',"+
			" '"+myHis.getTxtComplemento07()+"','"+myHis.getTxtComplemento08()+"',"+
			" '"+myHis.getTxtComplemento09()+"','"+myHis.getTxtComplemento10()+"',"+
			" '"+myHis.getTxtComplemento11()+"','"+myHis.getTxtComplemento12()+"')";		
			Statement stmt     = conn.createStatement();
			//System.out.println("gravarHistorico: "+sCmd );		
			stmt.execute(sCmd) ;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" - "+sCmd);
		} catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	public void LeHistoricos(ProcessoBean myProcesso) throws DaoException {
		boolean continua   = true;
		Connection conn =null ;
		Statement  stmt = null; 
		
		try 
		{   conn = serviceloc.getConnection(MYABREVSIST) ;	
		stmt = conn.createStatement();
		String sCmd        = "";
		ResultSet rs       = null;
		
		String resultado               = "";
		String opcEscolha              = "";
		String numProcesso             = "";			
		String codEvento     	  	   = "";
		String indContinuidade         = "";
		String indContinua             = "            ";
		String orgaoAtuacao            = "            ";
		String orgaoLotacao            = "            ";
		String nomUserName             = "            ";
		
		numProcesso 		= sys.Util.rPad(myProcesso.getNumProcesso()," ",20);	
		codEvento      		= sys.Util.lPad("","0",3);
		indContinuidade     = sys.Util.lPad(""," ",20);
		opcEscolha  = numProcesso +			             
		codEvento;
		UsuarioBean myUsrLog= new UsuarioBean();
		orgaoAtuacao = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
		orgaoLotacao = sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
		nomUserName  = sys.Util.rPad(myUsrLog.getNomUserName(),"",20);
		
		List <PNT.HistoricoBean>historicos = new ArrayList<PNT.HistoricoBean>();
		Vector <PNT.HistoricoBean>h           = new Vector<PNT.HistoricoBean>();
		
		
		int reg=0;
		OrgaoBean myOrgao = new OrgaoBean();
		StatusBean myStatus = new StatusBean();
		while (continua)
		{
			resultado = chamaTransBRK("944",opcEscolha,indContinua+orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
			if (verErro(myProcesso,resultado,"Erro em Consulta Processos (Transa��o 944).")==false) { 
				return;
			}
			String linha = "";
			int i = 0;
			for (i=0; i < 2; i++) {
				linha = resultado.substring((i*1470)+3,(i+1)*1470+3);				
				PNT.HistoricoBean myHist = new PNT.HistoricoBean();
				setPosIni(0);											
				myHist.setNumProcesso(getPedaco(linha,20," "," "));
				myHist.setCodStatus(getPedaco(linha,3," "," "));
				myHist.setNomStatus(myStatus.DscStatus(myHist.getCodStatus()));
				myHist.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
				myHist.setCodEvento(getPedaco(linha,3," "," "));
				myHist.setCodOrigemEvento(getPedaco(linha,3," "," "));
				
				sCmd  = "SELECT dsc_origem_evento from TPNT_ORIGEM_EVENTO WHERE cod_origem_evento = '"+myHist.getCodOrigemEvento()+"'" ;
				rs    = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					myHist.setNomOrigemEvento(rs.getString("dsc_origem_evento"));	  				  	  				  
				}
				sCmd  = "SELECT dsc_evento from TPNT_EVENTO WHERE cod_evento = '"+myHist.getCodEvento()+"'" ;
				rs    = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					myHist.setNomEvento(rs.getString("dsc_evento"));	  				  	  				  
				}			
				
				myHist.setNomUserName(getPedaco(linha,20," "," "));
				myHist.setCodOrgaoLotacao(getPedaco(linha,6," "," "));						
				myOrgao.setCodOrgao(myHist.getCodOrgaoLotacao());
				myOrgao.Le_Orgao(myOrgao.getCodOrgao(),0);
				myHist.setSigOrgaoLotacao(myOrgao.getSigOrgao());						
				myHist.setDatProc(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
				myHist.setTxtComplemento01(getPedaco(linha,30," "," "));
				myHist.setTxtComplemento02(getPedaco(linha,45," "," "));
				myHist.setTxtComplemento03(getPedaco(linha,50," "," "));
				myHist.setTxtComplemento04(getPedaco(linha,1000," "," "));
				myHist.setTxtComplemento05(getPedaco(linha,30," "," "));
				/* Altera��o realizada para descri��o de Penalidade na view Hist�rico de Processo de Pontua��o
				Resultado de Requerimento Evento 812,842 e 872 */ 
								
				// Garante que o TxtComplemento06 nunca fique null
				myHist.setTxtComplemento06(getPedaco(linha,50," "," "));
				
				// Pega a descri��o da penalidade no Oracle, caso for um evento de resultado de requerimento
				if (myHist.getCodEvento().equals("812") || myHist.getCodEvento().equals("842") || myHist.getCodEvento().equals("872")){
					sCmd  = "SELECT dsc_penalidade from TPNT_PENALIDADE WHERE cod_penalidade = '"+myHist.getTxtComplemento05()+"'" ;
					rs = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myHist.setTxtComplemento06(rs.getString("dsc_penalidade"));	  				  	  				  
					}
				}
				// Fim altera��o

				myHist.setTxtComplemento07(getPedaco(linha,50," "," "));
				myHist.setTxtComplemento08(getPedaco(linha,50," "," "));
				myHist.setTxtComplemento09(getPedaco(linha,12," "," "));
				myHist.setTxtComplemento10(getPedaco(linha,2," "," "));
				myHist.setTxtComplemento11(getPedaco(linha,30," "," "));
				myHist.setTxtComplemento12(getPedaco(linha,50," "," "));
				h.add(myHist);
			}
			setPosIni(2*1470+3);
			indContinua = getPedaco(resultado,20," "," ") ;
			if (indContinua.trim().length()<=0) continua=false;
			reg++;
		}
		int t = h.size();
		for (int i=t-1;i>=0;i--) {
			PNT.HistoricoBean hh = (PNT.HistoricoBean)h.elementAt(i);
			historicos.add(hh) ;			
		}
		
		
		myProcesso.setHistoricos(historicos);
		}catch (Exception ex) {
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
		finally {
			if (conn != null) {
				try 
				{
					stmt.close();					
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) { }
			}
		}
	}
	
	
	public List<AtaDOBean> ConsultaAtasDO(AtaDOBean myAtaDO) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		boolean continua = true;
		try 
		{	
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			Statement stmt = conn.createStatement();
			ResultSet rs       = null;
			
			UsuarioBean myUsrLog= new UsuarioBean();
			String resultado            = "";
			String orgaoAtuacao 		= "            ";
			String orgaoLotacao 		= "            ";
			String nomUserName  		= "            ";
			String opcEscolha   		= "";
			String tipoAta     = myAtaDO.getTipAtaDO();		
			String datAtaIni   = myAtaDO.getDatEDOInicial();
			
			datAtaIni="20110401";
			
			
			String datAtaFim   = myAtaDO.getDatEDOFinal();
			String statusAta   = sys.Util.lPad(myAtaDO.getStatusAta(),"0",1);
			String publicada   = sys.Util.lPad(myAtaDO.getStatusPublic(),"0",1);
			String indContinua = sys.Util.rPad(" "," ",20);	
			
			opcEscolha  = tipoAta + datAtaIni + datAtaFim + statusAta + publicada;
			ArrayList <AtaDOBean>myListAtas = new ArrayList<AtaDOBean>();		
			int reg=0;
			while (continua)
			{
				resultado = chamaTransBRK("950",opcEscolha,indContinua+orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
				if (verErro(myAtaDO,resultado,"Erro em Consulta Processos (Transa��o 950).")==false) { 
					return null;
				}	
				String linha = resultado.substring(3);
				int i = 0;
				int cont=1;
				setPosIni(0);
				for (i=0; i < 66; i++) {
					if ((!linha.substring(i * 48, i * 48 + 20).trim().equals(""))){ 
						AtaDOBean myAta = new AtaDOBean();				
						myAta.setCodEnvioDO(getPedaco(linha,20," "," "));
						myAta.setDatEDO(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));				
						myAta.setQtdProc(getPedaco(linha,3," "," "));
						myAta.setStatusAta(getPedaco(linha,1," "," "));           
						myAta.setDatEstorno(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));				
						myAta.setDatPublAta(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));				
						myListAtas.add(myAta);
						
						sCmd  =  "SELECT * FROM TPNT_ATA_DO WHERE COD_ENVIO_DO='"+myAta.getCodEnvioDO()+"'";
						rs    = stmt.executeQuery(sCmd) ;
						if (!rs.next())
						{
							sCmd  =  "INSERT INTO TPNT_ATA_DO " +
							"(COD_ATA_DO,TIP_ATA,COD_ENVIO_DO, "+
							"DAT_EDO,NOM_USERNAME_EDO,COD_ORGAO_LOTACAO_EDO,"+
							"DAT_PROC_EDO,TIPO_PROCESSO) VALUES (seq_pnt_ata_DO.nextval, "+
							" '"+myAtaDO.getTipAtaDO()+"',"+
							" '"+myAta.getCodEnvioDO()+"', "+
							" to_date('"+myAta.getDatEDO()+"','dd/mm/yyyy'), "+
							" '"+myAta.getNomUserNameEDO() +"','"+myAta.getCodOrgaoLotacaoEDO()+"', "+
							" to_date('"+myAta.getDatProcEDO()+"','dd/mm/yyyy'),'"+myAta.getTipProcessosDO()+"') ";
							
							
							stmt.execute(sCmd) ;
							
							
							
							setPosIni(cont*48);
							cont++;
						}                
					}
					indContinua = linha.substring(48*66,48*66+28);		
					if (indContinua.trim().length()<=0) continua=false;
					reg++;
				}
			}
			
			
		   if (rs != null) rs.close();
		   stmt.close();
		   return myListAtas;
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeAtasDO : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
	}
	
	
	public void LeAtasDO(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		try {			
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT " +
			"COD_ATA,COD_PROCESSO,COD_ENVIO_DO, "+
			"to_char(DAT_EDO,'dd/mm/yyyy') as DAT_EDO, "+
			"NOM_USERNAME_EDO,COD_ORGAO_LOTACAO_EDO,TIP_ATA, " +
			"to_char(DAT_PROC_EDO,'dd/mm/yyyy') as DAT_PROC_EDO, "+
			"to_char(DAT_PUBL_ATA,'dd/mm/yyyy') as DAT_PUBL_ATA, "+
			"NOM_USERNAME_PDO,COD_ORGAO_LOTACAO_PDO, " +
			"to_char(DAT_PROC_PDO,'dd/mm/yyyy') as DAT_PROC_PDO "+
			" FROM TPNT_ATA_DO "+
			" WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ORDER BY DAT_EDO DESC";
			ArrayList <AtaDOBean>myListAtas = new ArrayList<AtaDOBean>();			
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				AtaDOBean myAta = new AtaDOBean();
				myAta.setCodAtaDO(rs.getString("COD_ATA_DO"));
				myAta.setCodProcesso(rs.getString("COD_PROCESSO"));
				myAta.setCodEnvioDO(rs.getString("COD_ENVIO_DO"));
				myAta.setDatEDO(rs.getString("DAT_EDO"));
				myAta.setNomUserNameEDO(rs.getString("NOM_USERNAME_EDO"));           
				myAta.setCodOrgaoLotacaoEDO(rs.getString("COD_ORGAO_LOTACAO_EDO"));              
				myAta.setDatProcEDO(rs.getString("DAT_PROC_EDO"));
				myAta.setTipAtaDO(rs.getString("TIP_ATA"));
				myAta.setDatPublAta(rs.getString("DAT_PUBL_ATA"));
				myAta.setNomUserNamePDO(rs.getString("NOM_USERNAME_PDO"));           
				myAta.setCodOrgaoLotacaoPDO(rs.getString("COD_ORGAO_LOTACAO_PDO"));              
				myAta.setDatProcPDO(rs.getString("DAT_PROC_PDO"));
				myListAtas.add(myAta);
			}	
			myProcesso.setAtasDO(myListAtas);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeAtasDO : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
	}
	
	public void LeAtasDO(AtaDOBean myAta) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		boolean continua = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			UsuarioBean myUsrLog= new UsuarioBean();
			String resultado            ="";
			String orgaoAtuacao 		= "            ";
			String orgaoLotacao 		= "            ";
			String nomUserName  		= "            ";
			String opcEscolha   		= "";
			String tipoAta     = myAta.getTipAtaDO();		
			String codEnvioDO  = sys.Util.rPad(myAta.getCodEnvioDO()," ",20) ; 			
			String indContinua = sys.Util.rPad(" "," ",20);	
			
			opcEscolha  = tipoAta + codEnvioDO;		
			ArrayList <ProcessoBean>myListProcessos = new ArrayList<ProcessoBean>();			
			
			while (continua)
			{
				resultado = chamaTransBRK("951",opcEscolha,indContinua+orgaoAtuacao+orgaoLotacao+nomUserName,myUsrLog);
				if (verErro(myAta,resultado,"Erro em Consulta Processos (Transa��o 951).")==false) { 
					return ;
				}	
				String linha = resultado.substring(3);
				setPosIni(0);				
				int i = 0;
				int cont=1;
				for (i=0; i < 56; i++) { /*85*/
					if ((!linha.substring(i * 55, i * 55 + 20).trim().equals(""))){ //37 - 57
						ProcessoBean myProcesso = new ProcessoBean();					
						myProcesso.setNumProcesso(getPedaco(linha,20," "," "));				
						myProcesso.setTotPontos(getPedaco(linha,5," "," "));
						myProcesso.setTipProcesso(getPedaco(linha,1," "," "));
						myAta.setTipProcessosDO(myProcesso.getTipProcesso());
						myProcesso.setNumCPF(getPedaco(linha,11," "," "));
						myProcesso.setCodArtigo(getPedaco(linha,15," "," "));
						myProcesso.setCodPenalidade(getPedaco(linha,3," "," "));
						
						if (myProcesso.getCodArtigo().equals("")){
							myProcesso.setCodArtigo("-");
						}
						
						Statement stmt = conn.createStatement();
						sCmd = "SELECT DSC_PENALIDADE" +
						" FROM TPNT_PENALIDADE "+
						" WHERE COD_PENALIDADE='"+myProcesso.getCodPenalidade()+"'";
						ResultSet rs    = stmt.executeQuery(sCmd) ;		
						while (rs.next()) {
							myProcesso.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE"));
						}	
						if (rs != null) rs.close();
						stmt.close();
						
						myListProcessos.add(myProcesso);
						setPosIni(cont*55); //37 - 57 
						cont++;
					}
				}
				indContinua = linha.substring(55*56,55*56+20);    /*linha.substring(37*85,37*85+20);*/			
				if (indContinua.trim().length()<=0) continua=false;	
			}	
			myAta.setProcessos(myListProcessos);
			
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeAtasDO : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
	}	
	
	public void AtualizaEditalDO(AtaDOBean myAta) throws sys.BeanException {
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TPNT_ATA_DO SET "+
			"DAT_EMISSAO_EDO=TO_DATE('"+myAta.getDatEmissaoEDO()+"','DD/MM/YYYY'),"+                        
			"NOM_USERNAME_EMISSAO_EDO='"+myAta.getNomUserNameEmissaoEDO()+"',"+                           
			"COD_ORGAO_LOTACAO_EMISSAO_EDO='"+myAta.getCodOrgaoLotacaoEmissaoEDO()+"',"+                    
			"DAT_PROC_EMISSAO_EDO=TO_DATE('"+myAta.getDatProcEmissaoEDO()+"','DD/MM/YYYY'),"+
			"COD_ENVIO_DO='"+myAta.getCodEnvioDOedital()+"' "+
			"WHERE COD_ENVIO_DO='"+myAta.getCodEnvioDO()+"'";
			stmt.execute(sCmd) ;
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.BeanException("DaoBrokerAdabas(PNT)-AtualizaEditalDO : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	public void AtualizaDataPublicacaoDO(AtaDOBean myAta) throws sys.BeanException {
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TPNT_ATA_DO SET "+
			"DAT_PUBL_ATA=TO_DATE('"+myAta.getDatPublAta()+"','DD/MM/YYYY'),"+                        
			"NOM_USERNAME_PDO='"+myAta.getNomUserNamePDO()+"',"+                           
			"COD_ORGAO_LOTACAO_PDO='"+myAta.getCodOrgaoLotacaoPDO()+"',"+                    
			"DAT_PROC_PDO=TO_DATE('"+myAta.getDatProcPDO()+"','DD/MM/YYYY') "+
			"WHERE COD_ENVIO_DO='"+myAta.getCodEnvioDO()+"'";
			stmt.execute(sCmd) ;
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.BeanException("DaoBrokerAdabas(PNT)-AtualizaEditalDO : "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	
	public void atualizaDigitalizacao(NotificacaoBean myNotif) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TPNT_NOTIFICACAO SET " +
			"DAT_DIGITALIZACAO = TO_DATE('" + myNotif.getDatDigitalizacao() + "', 'DD/MM/YYYY'), " +
			"SIT_DIGITALIZACAO = '"+ myNotif.getSitDigitaliza()+"' "+
			"WHERE " +
			"NUM_NOTIFICACAO   = '" + myNotif.getNumNotificacao()+"' AND "+
			"SEQ_NOTIFICACAO   = '" + myNotif.getSeqNotificacao()+"' AND "+
			"SIT_DIGITALIZACAO = 'N'";
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public void atualizaDigitalizacaoPorNotificacao(NotificacaoBean myNotif) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TPNT_NOTIFICACAO SET " +
			"DAT_DIGITALIZACAO = TO_DATE('" + myNotif.getDatDigitalizacao() + "', 'DD/MM/YYYY'), " +
			"SIT_DIGITALIZACAO = '"+ myNotif.getSitDigitaliza()+"' "+
			"WHERE " +
			"NUM_NOTIFICACAO   = '" + myNotif.getNumNotificacao()+"' AND "+
			"SIT_DIGITALIZACAO = 'N'";
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public String GravaARDigitalizado(ARDigitalizadoBean myAR) throws DaoException {
		
		String retorno = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			//Verifica se o registro j� existe na base
			String[] campos = { "COD_AR_DIGITALIZADO" };
			String[] valores = { String.valueOf(myAR.getCodARDigitalizado()) };
			if (ConsultaARDigitalizados(campos, valores).length == 0) {
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_PNT_AR_DIGITALIZADO.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				myAR.setCodARDigitalizado(rs.getInt("CODIGO"));
				rs.close();
				
				sql = "INSERT INTO TPNT_AR_DIGITALIZADO (COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, "
					+ "SEQ_NOTIFICACAO,NUM_PROCESSO, NUM_LOTE, NUM_CAIXA, DAT_DIGITALIZACAO) VALUES ("
					+ myAR.getCodARDigitalizado() + ", "
					+ "'" + myAR.getNumNotificacao() + "', "
					+ "'" + myAR.getSeqNotificacao() + "', "					
					+ "'" + myAR.getNumProcesso() + "', "
					+ "'" + myAR.getNumLote() + "', " 
					+ "'" + myAR.getNumCaixa() + "', "
					+ "TO_DATE('" + myAR.getDatDigitalizacao() + "', 'DD/MM/YYYY'))";
				stmt.execute(sql);
				conn.commit();
				retorno = "I";
				
			} else {
				sql = "UPDATE TPNT_AR_DIGITALIZADO SET "
					+ "NUM_NOTIFICACAO   = '" + myAR.getNumNotificacao() + "', "
					+ "SEQ_NOTIFICACAO   = '" + myAR.getSeqNotificacao() + "', "					
					+ "NUM_PROCESSO      = '" + myAR.getNumProcesso()    + "', "
					+ "NUM_LOTE          = '" + myAR.getNumLote() + "', "
					+ "NUM_CAIXA         = '" + myAR.getNumCaixa() + "', "
					+ "DAT_DIGITALIZACAO = TO_DATE('" + myAR.getDatDigitalizacao()  + "', 'DD/MM/YYYY') "
					+ "WHERE COD_AR_DIGITALIZADO = '" + myAR.getCodARDigitalizado() + "'";
				stmt.execute(sql);
				conn.commit();
				retorno = "S";
			}
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	public ARDigitalizadoBean[] ConsultaARDigitalizados(String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "SELECT COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, SEQ_NOTIFICACAO, NUM_PROCESSO, NUM_LOTE, NUM_CAIXA, to_char(DAT_DIGITALIZACAO,'dd/mm/yyyy') as DAT_DIGITALIZACAO"
				+ " FROM TPNT_AR_DIGITALIZADO";
			if ((campos != null) && (valores != null))
				sql += sys.Util.montarWHERE(campos, valores);
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector <ARDigitalizadoBean>vetARs = new Vector<ARDigitalizadoBean>();            
			while (rs.next()) {
				//Cria o Bean de ARDigitalizado
				ARDigitalizadoBean myAR = new ARDigitalizadoBean();                                
				myAR.setCodARDigitalizado(rs.getInt("COD_AR_DIGITALIZADO"));
				myAR.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				myAR.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));				
				myAR.setNumProcesso(rs.getString("NUM_PROCESSO"));                
				myAR.setNumLote(rs.getString("NUM_LOTE"));
				myAR.setNumCaixa(rs.getString("NUM_CAIXA"));
				myAR.setDatDigitalizacao(rs.getString("DAT_DIGITALIZACAO"));
				vetARs.add(myAR);
			}            
			ARDigitalizadoBean[] colARs = new ARDigitalizadoBean[vetARs.size()];
			vetARs.toArray(colARs);
			
			rs.close();
			stmt.close();
			
			return colARs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}  finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public ARDigitalizadoBean ConsultaARDigitalizado(String[] campos, String[] valores) throws DaoException {
		
		try {
			ARDigitalizadoBean[] beans = ConsultaARDigitalizados(campos, valores);
			if (beans.length > 0)
				return beans[0];
			else
				return null;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void GravaLogARDigitalizado(LogARDigitalizadoBean myLog) throws DaoException {
		
		Connection conn = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt  = conn.createStatement();
			//Gravar Log de Processamento da Linha
			String sCmd = "INSERT INTO TPNT_LOG_DIGITALIZACAO_AR (COD_LOG_DIGITALIZACAO_AR, DAT_PROC, "
				+ "NOM_ARQUIVO, NUM_CAIXA, NUM_LOTE, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME) "
				+ "VALUES (SEQ_PNT_LOG_DIGITALIZACAO_AR.NEXTVAL, "
				+ "TO_DATE('" + myLog.getDatProcessamento() + "', 'DD/MM/YYYY'), "
				+ "'" + myLog.getNomArquivo() + "', "
				+ "'" + myLog.getNumCaixa() + "', "
				+ "'" + myLog.getNumLote() + "', "                
				+ "'" + (!myLog.getCodOperacao().equals("R") ? "" : myLog.getDscRetorno()) + "', "
				+ "'" + myLog.getCodOperacao() + "', "
				+ "'" + myLog.getNomUsuario() + "')";
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public void LeARDigitalizado(ProcessoBean myProcesso) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd =  " SELECT COD_AR_DIGITALIZADO,NUM_LOTE,NUM_CAIXA,NUM_NOTIFICACAO," +
			" SEQ_NOTIFICACAO,to_char(DAT_DIGITALIZACAO,'dd/mm/yyyy') as DAT_DIGIT,NUM_PROCESSO "+		
			" FROM TPNT_AR_DIGITALIZADO ";
			if(myProcesso.getCodARDigitalizado().length()==0){
				sCmd +=" WHERE NUM_PROCESSO='"+myProcesso.getNumProcesso()+"' " ;
			}else{
				sCmd +=" WHERE COD_AR_DIGITALIZADO='"+myProcesso.getCodARDigitalizado()+"' " ;
			}
			sCmd +=      " ORDER BY NUM_NOTIFICACAO,SEQ_NOTIFICACAO ";
			ArrayList <ARDigitalizadoBean>myListARDigitalizado = new ArrayList<ARDigitalizadoBean>();			
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				ARDigitalizadoBean myARDigit     = new ARDigitalizadoBean();
				myARDigit.setCodARDigitalizado   (rs.getInt("COD_AR_DIGITALIZADO"));
				myARDigit.setNumLote             (rs.getString("NUM_LOTE"));              
				myARDigit.setNumCaixa            (rs.getString("NUM_CAIXA")); 						         
				myARDigit.setSeqNotificacao      (Util.lPad(rs.getString("SEQ_NOTIFICACAO"),"0",3)); 
				myARDigit.setNumNotificacao      (Util.lPad(rs.getString("NUM_NOTIFICACAO"),"0",9));
				myARDigit.setDatDigitalizacao    (rs.getString("DAT_DIGIT"));                   
				myARDigit.setNumProcesso         (rs.getString("NUM_PROCESSO"));			    
				myListARDigitalizado.add         (myARDigit)                       ;
			}	
			myProcesso.setARDigitalizado(myListARDigitalizado);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas-LeARDigitalizado: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	/**------------------------------------------------
	 * DAO referente ao RelatProcStatusBean
	 * @author Wellem Lyra
	 --------------------------------------------------*/
	/*
	 public void consultaStatusProc(RelatProcStatusBean relProcStatus) throws DaoException {
	 
	 Connection conn = null;
	 String sCmd = "";		
	 String condicao = "PRO.COD_STATUS = STAT.COD_STATUS_AUTO ";                
	 if(!relProcStatus.getTodosStatus().equals("S"))
	 sCmd += "AND PRO.COD_STATUS = "+relProcStatus.getCodStatus()+" ";
	 
	 
	 try {
	 conn = serviceloc.getConnection(MYABREVSIST);
	 Statement stmt = conn.createStatement();		
	 ArrayList <RelatProcStatusBean>listDados= new ArrayList<RelatProcStatusBean>();
	 
	 sCmd = "SELECT PRO.COD_STATUS,STAT.NOM_STATUS_AUTO, COUNT(*) AS QTD"+
	 " FROM TPNT_PROCESSO PRO,TPNT_STATUS_PROCESSO STAT " +			
	 " WHERE " + condicao+" and   PRO.DAT_STATUS >= TO_DATE('"+relProcStatus.getDataIni()+"','DD/MM/YYYY') " +
	 "AND PRO.DAT_STATUS <= TO_DATE('"+relProcStatus.getDataFim()+"','DD/MM/YYYY') " ;
	 if (relProcStatus.getTodosStatus().equals("N"))
	 sCmd += "AND PRO.COD_STATUS =  " + relProcStatus.getCodStatus() + " ";			
	 sCmd += "GROUP BY PRO.COD_STATUS,STAT.NOM_STATUS_AUTO "+
	 "order By PRO.COD_STATUS ";
	 
	 ResultSet rs = stmt.executeQuery(sCmd);
	 
	 while (rs.next()) {
	 RelatProcStatusBean dados = new RelatProcStatusBean();	
	 dados.setCodStatus(Util.lPad(rs.getString("COD_STATUS"),"0",3));
	 dados.setNomStatus(rs.getString("NOM_STATUS_AUTO"));
	 dados.setQtdProcStatus(Util.formataIntero(rs.getString("QTD")));				
	 
	 listDados.add(dados);
	 }
	 relProcStatus.setDados(listDados);
	 rs.close();
	 stmt.close();			
	 
	 } catch (Exception ex) {
	 
	 throw new DaoException(ex.getMessage());
	 } finally {
	 if (conn != null) {
	 try {
	 serviceloc.setReleaseConnection(conn);
	 } catch (Exception ey) {
	 }
	 }
	 }	
	 }
	 */
	
	public void consultaStatusProc(RelatProcStatusBean relProcStatus) throws DaoException {
		boolean continua   = true;
		try {
			String resultado    = "";
			String opcEscolha   = "";
			String codStatus    = "";
			String datIni       = "";
			String datFim       = "";			
			String indContinua  = "            ";
			if (relProcStatus.getTodosStatus().equals("S"))
				codStatus   = "999";
			else codStatus   = sys.Util.lPad(relProcStatus.getCodStatus(),"0",3);
			datIni      = Util.formataDataYYYYMMDD(relProcStatus.getDataIni());
			datFim      = Util.formataDataYYYYMMDD(relProcStatus.getDataFim());
			opcEscolha  = codStatus+datIni+datFim;
			indContinua  = sys.Util.rPad(" "," ",20);
			ArrayList <RelatProcStatusBean>listDados= new ArrayList<RelatProcStatusBean>();
			StatusBean myStatus = new StatusBean();
			int reg=0;
			while (continua)
			{
				resultado = chamaTransBRK("952",opcEscolha,indContinua,new UsuarioBean());
				String linha = resultado.substring(3);
				int i = 0;
				int cont=1;
				setPosIni(0);
				for (i=0; i < 293; i++) {
					if ((!linha.substring(i * 11, i * 11 + 20).trim().equals(""))){ 
						RelatProcStatusBean dados = new RelatProcStatusBean();	
						dados.setCodStatus(getPedaco(linha,3," "," "));
						dados.setNomStatus(myStatus.DscStatus(dados.getCodStatus()));
						dados.setQtdProcStatus(Util.formataIntero(getPedaco(linha,8," "," ")));
						
						listDados.add(dados);
						setPosIni(cont*11);
						cont++;
					}                
				}
				indContinua = linha.substring(11*250,11*250+20);		
				if (indContinua.trim().length()<=0) continua=false;
				reg++;
			}
			relProcStatus.setDados(listDados);
		}
		catch (Exception ex) {
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
		
	}	
	
	
	public boolean GravaARDigitalizadoTmp(ARDigitalizadoBean myAR) throws DaoException {
		
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			
			//Pega o valor da sequence para o campo CodAIDigitalizado
			sql = "SELECT SEQ_PNT_AR_DIGITALIZADO_TMP.NEXTVAL AS CODIGO FROM DUAL";
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			myAR.setCodARDigitalizado(rs.getInt("CODIGO"));
			rs.close();
			
			sql = "INSERT INTO TPNT_AR_DIGITALIZADO_TMP (COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, "
				+ "SEQ_NOTIFICACAO,NUM_PROCESSO, " 
				+ "NUM_LOTE, NUM_CAIXA, DAT_DIGITALIZACAO, "
				+ "DAT_ENTREGA, "
				+ "NUM_CODIGO_RETORNO "
				+ ") VALUES ("
				+ myAR.getCodARDigitalizado() + ", "
				+ "'" + myAR.getNumNotificacao() + "', "
				+ "'" + myAR.getSeqNotificacao() + "', "					
				+ "'" + myAR.getNumProcesso() + "', "
				+ "'" + myAR.getNumLote() + "', " 
				+ "'" + myAR.getNumCaixaFrt() + "', "
				+ "TO_DATE('" + myAR.getDatDigitalizacao() + "', 'DD/MM/YYYY'),"
				+ "TO_DATE('" + myAR.getDatEntrega() + "', 'DD/MM/YYYY'),"
				+ "'" + myAR.getCodRetorno() + "') ";
			
			stmt.execute(sql);
			conn.commit();
			
			stmt.close();
			
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	
	public String ConsultaCodRet(String codRet) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		String codRetDETRAN = codRet;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd =  "SELECT NUM_CODIGO_RETORNO " +
			"FROM TSMI_CODIGO_RETORNO " +
			"WHERE NUM_CODIGO_RETORNO_VEX = '"+codRet+"' " ;
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				codRetDETRAN = rs.getString("NUM_CODIGO_RETORNO");
			}	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException("DaoBrokerAdabas-ConsultaCodRet: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return codRetDETRAN;
	}
	
	
	public void CarregaLista(PNT.PaginacaoBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		int prim = 1;
		int qtd = 0;
		int qtdParcial = 0;
		String trans = "";
		String datProc = "";
		
		try {
			
			if (!auxClasse.getDatProcRef().equals("Ultima")){		
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				
				ArrayList <ProcessoBean>dados = new ArrayList<ProcessoBean>();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd = "";
				
				sCmd =	"SELECT COD_PROCESSO, NUM_PROCESSO, to_char(DAT_PROCESSO,'dd/mm/yyyy') as DAT_PROCESSO, "+ 
				"NOM_CONDUTOR, TIP_CNH_CONDUTOR, NUM_CPF_CONDUTOR, NUM_CNH_CONDUTOR, UF_CNH_CONDUTOR "+ 
				"FROM TPNT_PROCESSO "+ 
				"WHERE COD_STATUS = " +auxClasse.getCodTransacao()+" "+
				"AND DAT_STATUS >= TO_DATE('" + auxClasse.getDatInicio() + "', 'DD/MM/YYYY') " +
				"AND DAT_STATUS <  TO_DATE('" + auxClasse.getDatFim() + "', 'DD/MM/YYYY') "+						
				"AND COD_PROCESSO > " + auxClasse.getDatProcRef() + " "+					
				"ORDER BY COD_PROCESSO ";
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					ProcessoBean contVex = new ProcessoBean();
					String codRetorno = rs.getString("COD_PROCESSO");
					String pkCodControle = rs.getString("COD_PROCESSO"); 
					contVex.setCodProcesso(pkCodControle);
					contVex.setNumProcesso(rs.getString("NUM_PROCESSO"));
					contVex.setDatProcesso(rs.getString("DAT_PROCESSO"));
					contVex.setNomResponsavel(rs.getString("NOM_CONDUTOR"));
					contVex.setTipCNH(rs.getString("TIP_CNH_CONDUTOR"));
					contVex.setNumCPF(rs.getString("NUM_CPF_CONDUTOR"));
					contVex.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));
					contVex.setUfCNH(rs.getString("UF_CNH_CONDUTOR"));
					
					trans = codRetorno;
					datProc = pkCodControle;
					
					//datProc = datProc.substring(8,10) + "/" + datProc.substring(5,7) + "/" + datProc.substring(0,4) + ":" + datProc.substring(11,19);
					qtd++;
					
					if ((prim == 1) && (!acao.equals("Ant"))){
						auxClasse.getPrimTrans().add(auxClasse.getTransRef());
						auxClasse.getPrimDatProc().add(auxClasse.getDatProcRef());
						prim++;
					}
					
					dados.add(contVex);
					if (qtd == 50) break;
					
				}
				
				auxClasse.setDados(dados);
				
				qtdParcial = Integer.parseInt(auxClasse.getQtdParcialReg());
				
				if (qtd != 0){
					
					auxClasse.setQtdRegs(String.valueOf(qtd));
					if (acao.equals("Prox") || acao.equals("detalhe") || acao.equals("detalheResumo")) 
						qtdParcial = qtdParcial + qtd;
				}
				
				if ((prim > 1) && (!acao.equals("Ant"))){
					auxClasse.getUltTrans().add(trans);
					auxClasse.getUltDatProc().add(datProc);
					auxClasse.getQtdReg().add(String.valueOf(qtd));
				}
				
				if ((qtd == 0) && (!auxClasse.getDatProcRef().equals("Ultima"))){
					
					auxClasse.getPrimTrans().add("Ultima");
					auxClasse.getPrimDatProc().add("Ultima");
					
					auxClasse.getUltTrans().add("Ultima");
					auxClasse.getUltDatProc().add("Ultima");
					auxClasse.getQtdReg().add("Ultima");
				}
				
				if (!auxClasse.getDatProcRef().equals("Ultima")){
					auxClasse.setQtdParcialReg(String.valueOf(qtdParcial));
				}
				
				auxClasse.setDados(dados);
				
				rs.close();
				stmt.close();
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}
	
	public String BuscaTotalReg(PNT.PaginacaoBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		String qtd = "0";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			// preparar lista os dados do Usuario por Sistema
			
			String sCmd = "";
			
			sCmd =	"SELECT COUNT(*) AS QTD "+ 
			"FROM TPNT_PROCESSO "+ 
			"WHERE COD_STATUS = " +auxClasse.getCodTransacao()+" "+
			"AND DAT_STATUS >= TO_DATE('" + auxClasse.getDatInicio() + "', 'DD/MM/YYYY') " +
			"AND DAT_STATUS < TO_DATE('" + auxClasse.getDatFim() + "', 'DD/MM/YYYY') ";						
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getString("QTD");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	public Vector ConsultaAtaDownload(AtaDOBean AtaDOBeanId, String dataIni, String dataFim) throws DaoException {
		AtaDOBean relClasse;
		Vector <AtaDOBean>vRetClasse   = new Vector<AtaDOBean>();
		
		Connection conn = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			
			String sCmd ="SELECT DISTINCT COD_ARQUIVO_ATA, A.COD_ENVIO_DO, NOM_ARQUIVO_ATA, to_char(DAT_EDO,'dd/mm/yyyy') as DAT_EDO, COD_STATUS" +
			" FROM TPNT_ARQUIVO_ATA_DO A JOIN TPNT_ATA_DO L " +
			" ON A.COD_ENVIO_DO = L.COD_ENVIO_DO" +
			" WHERE " +
			" TIP_ATA = '" + AtaDOBeanId.getTipAtaDO() + "'" + 
			" AND DAT_EDO BETWEEN to_date('" + dataIni + "','DD/MM/YYYY') AND to_date('" + dataFim + "','DD/MM/YYYY')" +
			" AND A.COD_STATUS = 1" +
			" ORDER BY COD_ENVIO_DO DESC";         
			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs2 = null;
			while (rs.next())   {
				relClasse = new AtaDOBean();
				relClasse.setCodArquivoDownload(rs.getString("COD_ARQUIVO_ATA"));
				relClasse.setCodEnvioDO(rs.getString("COD_ENVIO_DO")); 
				relClasse.setDatPublAta(rs.getString("DAT_EDO"));
				relClasse.setNomArquivoDownload(rs.getString("NOM_ARQUIVO_ATA"));
				relClasse.setStatusArquivoDownload(rs.getString("COD_STATUS"));
				sCmd = "SELECT COD_ARQUIVO_ATA FROM TPNT_DOWNLOAD_ATA_DO WHERE COD_ARQUIVO_ATA = '"+relClasse.getCodArquivoDownload()+"'";
				rs2 = stmt2.executeQuery(sCmd);
				if(rs2.next())
					relClasse.setCodEventoDownload("1");
				else
					relClasse.setCodEventoDownload("0");
				
				vRetClasse.addElement(relClasse);           
			}           
			rs.close();
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	
	
	public void DownloadArquivoAta(AtaDOBean AtaDOBeanId) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat dat_download = new SimpleDateFormat("dd/MM/yyyy");
			String sCmd ="INSERT INTO TPNT_DOWNLOAD_ATA_DO (COD_DOWNLOAD_ATA, DAT_DOWNLOAD, NOM_USERNAME," +
			"COD_ARQUIVO_ATA,COD_ORGAO_LOTACAO) VALUES(SEQ_PNT_DOWNLOAD_ATA_DO.NEXTVAL, TO_DATE('"+
			dat_download.format(new Date()) + "','DD/MM/YYYY'),'" +AtaDOBeanId.getNomUserNameDownload() + "','" 
			+ AtaDOBeanId.getCodArquivoDownload() + "','" + AtaDOBeanId.getCodOrgaoLotacaoDownload() + "')";
			stmt.executeQuery(sCmd);
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		
	}
	
	
	public boolean gravaArquivo(AtaDOBean AtaDOId, String quantProc,UsuarioBean usrlogado) throws DaoException{
		Connection conn = null;
		boolean existeArq = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = null;
			if(!AtaDOId.getStatusArquivoDownload().equals("-1")){
				sCmd = "SELECT COD_ENVIO_DO FROM TPNT_ARQUIVO_ATA_DO WHERE COD_ENVIO_DO = '" + AtaDOId.getCodEnvioDO() + "'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					existeArq = true;
				}
				if(existeArq)
					return false;
				
			}  else{
				sCmd = "UPDATE TPNT_ARQUIVO_ATA_DO SET COD_STATUS = 2 WHERE COD_ENVIO_DO = '" + AtaDOId.getCodEnvioDO() + "'";
				stmt.executeUpdate(sCmd);
			}
			sCmd = "INSERT INTO TPNT_ARQUIVO_ATA_DO (COD_ARQUIVO_ATA, COD_ENVIO_DO, DAT_CRIACAO, QTD_REG, NOM_USERNAME, COD_ORGAO_LOTACAO , NOM_ARQUIVO_ATA, COD_STATUS)"+
			" VALUES (SEQ_PNT_ARQUIVO_ATA_DO.NEXTVAL, '" + AtaDOId.getCodEnvioDO() + "',TO_DATE('" + sys.Util.formatedToday().substring(0,10) +	"','DD/MM/YYYY'),'" +	
			quantProc + "','" + usrlogado.getNomUserName() + "','" + usrlogado.getCodOrgaoAtuacao() + "','" + 
			AtaDOId.getNomArquivoDownload() + "', 1)";
			stmt.execute(sCmd);
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return true;
	}
	
	public boolean gravaArquivo(AtaDOBean myAta,ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId,UsuarioBean UsrLogado) throws DaoException{
		try{			
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = myAta.getCodEnvioDO();
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_PNT" + nomAta+ ".rtf");		
			if(!myAta.getStatusArquivoDownload().equals("-1")){
				if(file.exists())
					return false;
			}
			
			file.createNewFile();
			if(!file.canWrite()){
				myAta.setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}			
			myAta.setNomArquivoDownload(file.getName());
			
			FileWriter fwrite = new FileWriter(file);			
			fwrite.write(""+paramSys.getParamSist("NOM_ATA_TIT_02")+"\n");			
			fwrite.write(""+paramSys.getParamSist("NOM_ATA_TIT_03")+"\n");			
			fwrite.write("COORDENADORIA DE JULGAMENTO DE CONDUTORES"+"\n");
			fwrite.write("ATO DO ASSESSOR-CHEFE"+"\n");
			fwrite.write("DE 00.00.0000"+"\n");						
			fwrite.write("\n");
			fwrite.write("" + myAta.getTxtParagrago_01()  +"\n");
			fwrite.write("" + myAta.getTxtParagrago_02()  +"\n");
			fwrite.write("\n");
			
			if (myAta.getProcessos().size()>0)
			{
				int resto = myAta.getProcessos().size()%5;
				if (resto>0) 
				{
					for (int i=0; i<5-resto;i++) 
					{
						myAta.getProcessos().add(new PNT.ProcessoBean());
					}
				}
			}			
			for (int i=0;i<myAta.getProcessos().size();i=i+5) 
			{		
				if ( (myAta.getTipProcessosDO().equals("S")) || (myAta.getTipProcessosDO().equals("M")) )			
					
					if (i < myAta.getProcessos().size()-5)
						fwrite.write("CPF n� "+myAta.getProcessos().get(i).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i).getCodArtigo().trim()+"; CPF n� "+myAta.getProcessos().get(i+1).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+1).getCodArtigo().trim()
								+";"+(myAta.getProcessos().get(i+2).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+2).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+2).getCodArtigo().trim():"")
								+";"+(myAta.getProcessos().get(i+3).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+3).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+3).getCodArtigo().trim():"")
								+";"+(myAta.getProcessos().get(i+4).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+4).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+4).getCodArtigo().trim():"")+";\n");
					else
						fwrite.write("CPF n� "+myAta.getProcessos().get(i).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i).getCodArtigo().trim()+"; CPF n� "+myAta.getProcessos().get(i+1).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+1).getCodArtigo().trim()
								+";"+(myAta.getProcessos().get(i+2).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+2).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+2).getCodArtigo().trim():"")
								+";"+(myAta.getProcessos().get(i+3).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+3).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+3).getCodArtigo().trim():"")
								+";"+(myAta.getProcessos().get(i+4).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+4).getNumCPFEdt()+", art. "+myAta.getProcessos().get(i+4).getCodArtigo().trim():"")+"\n");
				else
					if (i < myAta.getProcessos().size()-5)
						fwrite.write("CPF n� "+myAta.getProcessos().get(i).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i).getTotPontos().trim()+"; CPF n� "+myAta.getProcessos().get(i+1).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+1).getTotPontos().trim()
								+";"+(myAta.getProcessos().get(i+2).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+2).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+2).getTotPontos().trim():"")
								+";"+(myAta.getProcessos().get(i+3).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+3).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+3).getTotPontos().trim():"")
								+";"+(myAta.getProcessos().get(i+4).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+4).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+4).getTotPontos().trim():"")+";\n");
					else
						fwrite.write("CPF n� "+myAta.getProcessos().get(i).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i).getTotPontos().trim()+"CPF n� "+myAta.getProcessos().get(i+1).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+1).getTotPontos().trim()
								+";"+(myAta.getProcessos().get(i+2).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+2).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+2).getTotPontos().trim():"")
								+";"+(myAta.getProcessos().get(i+3).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+3).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+3).getTotPontos().trim():"")
								+";"+(myAta.getProcessos().get(i+4).getNumCPFEdt().length()>0?"CPF n� "+myAta.getProcessos().get(i+4).getNumCPFEdt()+", pts. "+myAta.getProcessos().get(i+4).getTotPontos().trim():"")+"\n");
			}
			fwrite.close();
			myAta.setMsgErro("Arquivo Gerado com Sucesso");
			DaoBroker dao = DaoBrokerFactory.getInstance();
			if(!dao.gravaArquivo(myAta, String.valueOf(myAta.getProcessos().size()),UsrLogado)){
				if(!file.delete()){
					myAta.setMsgErro("Erro F�sico no Arquivo");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}	
		return true;
	}		
	public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		Connection conn = null ;
		boolean bOK = false ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			bOK = verErro(myProc,resultado,msg,stmt);
			stmt.close();		
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOK;
	}
	
	public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		boolean bOk    = false ;
		String codErro = resultado.substring(0,3);
		try {
			if ((codErro.equals("000")==false) &&
					(codErro.equals("037")==false)) {
				if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				}
				else {
					codErro=Util.lPad(codErro.trim(),"0",3);
					myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					ResultSet rs   = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					}
					rs.close();
				}
			}
			else   bOk = true ;
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return bOk;  
	}
	
	
	public String buscaNomRelator(String cpf,String orgaoLotacao) 
	throws DaoException {
		String nomRelator = "";
		Connection conn =null ;
		String sCmd = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT R.NOM_RELATOR " +
			" FROM TPNT_RELATOR R, TPNT_JUNTA J WHERE R.COD_JUNTA=J.COD_JUNTA "+	
			" AND R.NUM_CPF ='" +cpf+"'"+
			" AND R.COD_ORGAO_LOTACAO ='" +Integer.parseInt(orgaoLotacao)+"'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				nomRelator=rs.getString("NOM_RELATOR");
			}			
			if (rs != null) rs.close();
			stmt.close();
		}catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return nomRelator;
	}
	public boolean verErro(AtaDOBean myAta,String resultado,String msg) throws DaoException {
		Connection conn = null ;
		boolean bOK = false ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			bOK = verErro(myAta,resultado,msg,stmt);
			stmt.close();		
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOK;
	} 
	
	public boolean verErro(AtaDOBean myAta,String resultado,String msg,Statement stmt) throws DaoException {
		boolean bOk    = false ;
		String codErro = resultado.substring(0,3);
		try {
			if ((codErro.equals("000")==false) &&
					(codErro.equals("037")==false)) {
				if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					myAta.setMsgErro(msg+" Erro: "+resultado) ;		
				}
				else {
					codErro=Util.lPad(codErro.trim(),"0",3);
					myAta.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					ResultSet rs   = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myAta.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					}
					rs.close();
				}
			}
			else   bOk = true ;
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return bOk;  
	}
	
	public void buscarListaProcessos(ProcessoBean myProcesso, String ordem) throws DaoException {
		// TODO Auto-generated method stub
		
	}
	
	public String Transacao622(String codOrgao, String codJunta, String codAcao, 
			String indContinua,	String nomJunta, String sigJunta, String tpJunta, String txtEnd, 
			String numEnd, String txtCompl, String nomBairro, String numCep, 
			String codMun, String numDddTel, String numTel, String numFax, Connection conn) throws DaoException {
		
		return Transacao622(codOrgao,  codJunta, codAcao, 
				indContinua,	nomJunta, sigJunta, tpJunta, txtEnd, 
				numEnd, txtCompl, nomBairro, numCep, 
				codMun, numDddTel, numTel, numFax,conn);
	}
	
	/**************************************
	 * 
	 * @param codOrgao C�digo do Orgao. 999999 = Todos
	 * @param codJunta C�digo da Junta. 999999 = Todos
	 * @param codAcao C�digo da a��o desejada
	 * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	 */
	
	public String Transacao622(String codOrgao, String codJunta, String cpf, String tipAcao, 
			String indContinua,String nome) throws DaoException {
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		//Cr�tica do C�digo do �rg�o		
		if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo do �rg�o n�o preenchido";
		}
		if (codOrgao.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo do �rgao inv�lido";
		}
		//Cr�tica do C�digo da Junta 		
		if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo da Junta n�o preenchido";
		}
		if (codJunta.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo da Junta inv�lido";
		}
//		Cr�tica do  CPF
		if (cpf.length() != 11){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: CPF n�o informado";
		}
		//Cr�tica da A��o 		
		if ((tipAcao.equals(null)) ||(tipAcao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(tipAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			indContinua = "            ";
		}
		if (indContinua.length() != 12){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao622: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		}
		
		if (transOK) {
			String parteVar = codOrgao + codJunta + cpf+tipAcao + indContinua+nome;
			
			
			transBRK.setParteFixa("622");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new DaoException (e.getMessage());
			}
		}
		
		return resultado;
	}
	
	
	/**************************************
	 * 
	 * @param codOrgao C�digo do Orgao. 999999 = Todos
	 * @param codJunta C�digo da Junta. 999999 = Todos
	 * @param codAcao C�digo da a��o desejada
	 * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	 */
	
	public String Transacao623(String codOrgao, String codJunta, String numCPF, 
			String codAcao, String indContinua,	String nomRelator) throws DaoException {
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		
		//Cr�tica do C�digo do �rg�o		
		if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo do �rg�o n�o preenchido";
		}
		if (codOrgao.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo do �rgao inv�lido";
		}
		//Cr�tica do C�digo da Junta 		
		if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo da Junta n�o preenchido";
		}
		if (codJunta.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo da Junta inv�lido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			indContinua = "                       ";
		}
		if (indContinua.length() != 23){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao623: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		}
		
		if (transOK) {
			String parteVar = codOrgao + codJunta + numCPF + codAcao + indContinua 
			+ sys.Util.rPad(nomRelator," ",40);  
			
			transBRK.setParteFixa("623");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new DaoException (e.getMessage());
			}
		}
		
		return resultado;
	}
	
	public String Transacao622(String codOrgao, String codJunta, String codAcao, String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd, String numEnd, String txtCompl, String nomBairro, String numCep, String codMun, String numDddTel, String numTel, String numFax) throws DaoException {
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		//Cr�tica do C�digo do �rg�o		
		if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo do �rg�o n�o preenchido";
		}
		if (codOrgao.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo do �rgao inv�lido";
		}
		//Cr�tica do C�digo da Junta 		
		if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta n�o preenchido";
		}
		if (codJunta.length() != 6){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta inv�lido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			indContinua = "            ";
		}
		if (indContinua.length() != 12){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao086: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		}
		
		if (transOK) {
			String parteVar = codOrgao + codJunta + codAcao + indContinua 
			+ sys.Util.rPad(nomJunta," ",20) + sys.Util.rPad(sigJunta," ",10)
			+ sys.Util.rPad(tpJunta,"0",1) + sys.Util.rPad(txtEnd," ",27) 
			+ sys.Util.rPad(numEnd," ",5) + sys.Util.rPad(txtCompl," ",11)
			+ sys.Util.rPad(nomBairro," ",20) + sys.Util.lPad(codMun," ",5)
			+ sys.Util.lPad(numCep," ",8) + sys.Util.rPad(numDddTel," ",3)
			+ sys.Util.rPad(numTel," ",15) + sys.Util.rPad(numFax," ",15);  
			
			transBRK.setParteFixa("622");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new DaoException (e.getMessage());
			}
		}
		
		return resultado;
	}
	
	public String Transacao624(String codPenalidade, String codAcao, String indContinua,
			String descPenalidade,String enquadramento,String indReincidencia,
			String numPrazo,String qtdInfracaoDe,String qtdInfracaoAte,String codigoFator,String descResumida) throws sys.DaoException {

		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		
		//Cr�tica do C�digo 		
		if ((codPenalidade.trim().equals(null)) ||(codPenalidade.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao624: C�digo n�o preenchido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao624: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao624: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
			indContinua = "   ";
		}
		if (transOK) {
			String parteVar = sys.Util.lPad(codPenalidade,"0",3) + codAcao + sys.Util.lPad(indContinua,"0",3) 
			+ sys.Util.rPad(descPenalidade," ",100) + sys.Util.rPad(enquadramento," ",20) 
			+ sys.Util.rPad(indReincidencia," ",2) 
			+ sys.Util.lPad(numPrazo,"0",3) + sys.Util.lPad(qtdInfracaoDe,"0",3) 
			+ sys.Util.lPad(qtdInfracaoAte,"0",3)
			+ sys.Util.lPad(codigoFator,"0",3) + sys.Util.rPad(descResumida," ",30);  
			transBRK.setParteFixa("624");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
		return resultado;
	}
			
	public String Transacao625(String codInfracao, String codAcao, String indContinua,
			String codAgravo) throws sys.DaoException {
		
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		
		//Cr�tica do C�digo 		
		if ((codInfracao.trim().equals(null)) ||(codInfracao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao625: C�digo n�o preenchido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao625: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao625: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
			indContinua = "   ";
		}
		if (transOK) {
			String parteVar = sys.Util.lPad(codInfracao,"0",5) + codAcao + sys.Util.lPad(indContinua,"0",5) 
			+ sys.Util.lPad(codAgravo,"0",5);  
			
			transBRK.setParteFixa("625");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
		return resultado;
	}
	
	
	public String Transacao626(String codStatus, String codAcao, String indContinua,	
			String nomStatus) throws sys.DaoException {
		
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		
		//Cr�tica do C�digo		
		if ((codStatus.trim().equals(null)) ||(codStatus.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao626: C�digo n�o preenchido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao626: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao626: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
			indContinua = "   ";
		}
		if (transOK) {
			String parteVar = sys.Util.lPad(codStatus,"0",3) + codAcao + sys.Util.lPad(indContinua,"0",3) 
			+ sys.Util.rPad(nomStatus," ",30);  
			
			transBRK.setParteFixa("626");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
		return resultado;
	}
	
	
	public String Transacao627(String codEvento, String codAcao, String indContinua,	
			String nomEvento) throws sys.DaoException {
		
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		//Cr�tica do C�digo 		
		if ((codEvento.trim().equals(null)) ||(codEvento.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao627: C�digo n�o preenchido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao627: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao627: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
			indContinua = "   ";
		}
		if (transOK) {
			String parteVar = sys.Util.lPad(codEvento,"0",3) + sys.Util.lPad(codAcao,"0",1) + 
			sys.Util.rPad(indContinua," ",2) 
			+ sys.Util.rPad(nomEvento," ",30);  
			
			transBRK.setParteFixa("627");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
		
		return resultado;
	}
	
	public String Transacao629(String codMotivo, String codAcao, String indContinua,	
			String nomMotivo) throws sys.DaoException {
		
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		//Cr�tica do C�digo 		
		if ((codMotivo.trim().equals(null)) ||(codMotivo.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao629: C�digo n�o preenchido";
		}
		//Cr�tica da A��o 		
		if ((codAcao.trim().equals(null)) ||(codAcao.trim().equals(""))){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao629: C�digo do A��o n�o preenchido";
		}
		if ("12345".indexOf(codAcao) < 0){
			transOK=false;
			resultado = "ERR-DaoBroker.Transacao629: C�digo da A��o inv�lido";
		}
		//Cr�tica do Indicador de Continuidade 		
		if ((indContinua.trim().equals(null)) ||(indContinua.trim().equals(""))){
			indContinua = "   ";
		}
		if (transOK) {
			String parteVar = sys.Util.lPad(codMotivo,"0",2) + sys.Util.lPad(codAcao,"0",1) + 
			sys.Util.rPad(indContinua," ",2) 
			+ sys.Util.rPad(nomMotivo," ",30);  
			
			transBRK.setParteFixa("629");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
		return resultado;
	}

    public String Transacao630(String codOrgao, String codAcao, String indContinua, 
    		String prazoDef, String prazoEmissao, String prazoEntrega, String prazoPublDO,
    		String prazoRecursoPenal, String prazoRecurso2Inst, String prazoProsperaResult,
    		String prazoEntregaCNH, String folgaDef, String folgaRecursoPenal, String folgaRecurso2Inst,
    		String nomAssDef, String nomAssRecurso, String nomAssEntrega, String secretariaProcesso) throws sys.DaoException {
    	return null;
    }

	public void GravaProcessoOracle(ProcessoBean myProcesso)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
	
	
}