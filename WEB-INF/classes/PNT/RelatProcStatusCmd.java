package PNT;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import REC.StatusBean;
import REG.ResumoRetornoBean;

import sys.CommandException;
import sys.Util;

/**
* <b>Title:</b>        Gerencial - Registro Diario Cmd<br>
* <b>Description:</b>  Informa as infra��es di�rias de acordo com o �rg�o. <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      MIND - Informatica <br>
* @author Wellem Lyra
* @version 1.0
*/


public class RelatProcStatusCmd extends sys.Command {

	private static final String jspPadrao = "/PNT/RelatProcStatus.jsp";
	private String next;

	public RelatProcStatusCmd() {
		next = jspPadrao;
	}

	public RelatProcStatusCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			PNT.TAB.StatusBean StatusId   = (PNT.TAB.StatusBean)req.getAttribute("StatusId") ;
			if (StatusId==null)   	StatusId = new PNT.TAB.StatusBean() ;
			
			RelatProcStatusBean RelatProcStatusBean = (RelatProcStatusBean) session.getAttribute("RelatProcStatusBean");
			if (RelatProcStatusBean == null)RelatProcStatusBean = new RelatProcStatusBean();

			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			PaginacaoBean PaginacaoBeanId = (PaginacaoBean)session.getAttribute("PaginacaoBeanId") ;
			if (PaginacaoBeanId==null)  PaginacaoBeanId = new PaginacaoBean() ;
			
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			String msg = "";
			if (acao == null) acao = " ";			
					
			if  (acao.equals(" "))  {
				RelatProcStatusBean.setCodStatus("-1");
			}
			
			if  (acao.equals("detalhe"))  {
				
				session.removeAttribute("PaginacaoBeanId");
				PaginacaoBeanId =(PaginacaoBean) session.getAttribute("PaginacaoBeanId");
				if (PaginacaoBeanId == null)PaginacaoBeanId = new PaginacaoBean();
				 

				String codStatus = req.getParameter("codStatus");
				if (codStatus == null) codStatus = "";
				

				String nomStatus = req.getParameter("nomStatus");
				if (nomStatus == null) nomStatus = "";
				
				
				String todosStatus = req.getParameter("todosStatus");
				if (todosStatus == null)todosStatus="N";
				
				
				String dataIni = req.getParameter("dataIni");
				if (dataIni == null) dataIni = "";
				
				String dataFim = req.getParameter("dataFim");
				if (dataFim == null) dataFim = "";

				String qtdRegPag = ParamOrgaoId.getParamOrgao("QUANT_REG_PAGINA","50","7");
				
				PaginacaoBeanId.setDatInicio(dataIni);
				PaginacaoBeanId.setDatFim(dataFim );
				 
				PaginacaoBeanId.setCodTransacao(codStatus);
					
				PaginacaoBeanId.setAcao(acao);
				
				PaginacaoBeanId.setDscTransacao(codStatus.trim() + " - " + nomStatus.trim());
						

				PaginacaoBeanId.setQtd_Registros_Rodada(qtdRegPag);
				PaginacaoBeanId.setTransRef("0");  
				PaginacaoBeanId.setDatProcRef("0");  
				 
				PaginacaoBeanId.setQtdParcialReg("0");
				PaginacaoBeanId.setTotalRegistros(PaginacaoBeanId.BuscaTotalReg(acao));
				PaginacaoBeanId.CarregaLista(acao);
				nextRetorno="/PNT/Paginacao_Det.jsp";
			}	
			
			 if (acao.equals("Prox")) {
				 
				 if (PaginacaoBeanId.getPrimTrans().size()>0) {
					 int tam = PaginacaoBeanId.getPrimTrans().size();
					 PaginacaoBeanId.setTransRef(PaginacaoBeanId.getUltTrans(tam-1));  
					 PaginacaoBeanId.setDatProcRef(PaginacaoBeanId.getUltDatProc(tam-1));  
				 } else{
					 PaginacaoBeanId.setTransRef("0");  
					 PaginacaoBeanId.setDatProcRef("0");  
				 }
					 
					 
				 PaginacaoBeanId.CarregaLista(acao);
				 nextRetorno="/PNT/Paginacao_Det.jsp";
			 
			 }

			 if (acao.equals("Ant")) {
				 
				 int qtdParcial = 0;
				 int qtd = 0;

				 if (PaginacaoBeanId.getPrimTrans().size()>0) {
					 int tam = PaginacaoBeanId.getPrimTrans().size();
					 if (PaginacaoBeanId.getPrimTrans().size()==1) {
						 PaginacaoBeanId.setTransRef("0");  
						 PaginacaoBeanId.setDatProcRef("0");  
					 }else {
						 if (!PaginacaoBeanId.getQtdReg(tam-1).equals("Ultima")){
							 qtd = Integer.parseInt(PaginacaoBeanId.getQtdReg(tam-1));
							 qtdParcial = Integer.parseInt(PaginacaoBeanId.getQtdParcialReg());
	
							 qtdParcial = qtdParcial - qtd;
							 PaginacaoBeanId.setQtdParcialReg(String.valueOf(qtdParcial));
						 }
						 
						 PaginacaoBeanId.delPrimTrans(tam-1);
						 PaginacaoBeanId.delPrimDatProc(tam-1);
						 PaginacaoBeanId.delUltTrans(tam-1);
						 PaginacaoBeanId.delUltDatProc(tam-1);
						 PaginacaoBeanId.delQtdReg(tam-1);
						 
						 tam = PaginacaoBeanId.getPrimTrans().size();
						 if (PaginacaoBeanId.getPrimTrans().size()==1) {
							 PaginacaoBeanId.setTransRef("0");  
							 PaginacaoBeanId.setDatProcRef("0");  
						 }else {

						 PaginacaoBeanId.setTransRef(PaginacaoBeanId.getPrimTrans(tam-1));  
						 PaginacaoBeanId.setDatProcRef(PaginacaoBeanId.getPrimDatProc(tam-1));  

						 }
					 }
				 }
				 
				 PaginacaoBeanId.CarregaLista(acao);
				 nextRetorno="/PNT/Paginacao_Det.jsp";
			 }
			

			 if (acao.equals("consultarProc")) {
				
				String codStatus = Util.lPad(req.getParameter("codStatus"),"0",3);
				if (codStatus == null) codStatus = "";
				

				String nomStatus = req.getParameter("nomStatus");
				if (nomStatus == null) nomStatus = "";
				
				
				String todosStatus = req.getParameter("TodosStatus");
				if (todosStatus == null)todosStatus="N";
				
				
				String dataIni = req.getParameter("dataIni");
				if (dataIni == null) dataIni = "";
				
				String dataFim = req.getParameter("dataFim");
				if (dataFim == null) dataFim = "";
				
				
				RelatProcStatusBean.setCodStatus(codStatus);
				RelatProcStatusBean.setNomStatus(nomStatus);
				RelatProcStatusBean.setDataIni(dataIni);
				RelatProcStatusBean.setDataFim(dataFim);
				RelatProcStatusBean.setTodosStatus(todosStatus);
				
				
				//Titulo do Relatorio 
				String tituloConsulta = "QUANTITATIVO DE PROCESSOS POR STATUS  DE : "+"       "+dataIni+ " AT� "+dataFim;
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				RelatProcStatusBean.consultaRegDiario(RelatProcStatusBean);
				
				if (RelatProcStatusBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE DIA." ;	

			 	nextRetorno="/PNT/RelatProcStatusMostra.jsp";
			}
			
			if (acao.equals("ImprimeRelatorio")) {
				
				String codStatus = req.getParameter("codStatus");
				if (codStatus == null) codStatus = "";

				String nomStatus = req.getParameter("nomStatus");
				if (nomStatus == null) nomStatus = "";
				
				String todosStatus = req.getParameter("TodosStatus");
				if (todosStatus == null)todosStatus="N";
				
				
				String dataIni = req.getParameter("dataIni");
				if (dataIni == null) dataIni = "";
				
				String dataFim = req.getParameter("dataFim");
				if (dataFim == null) dataFim = "";
				
				
				RelatProcStatusBean.setCodStatus(codStatus);
				RelatProcStatusBean.setNomStatus(nomStatus);
				RelatProcStatusBean.setDataIni(dataIni);
				RelatProcStatusBean.setDataFim(dataFim);
				RelatProcStatusBean.setTodosStatus(todosStatus);
				
				
				//Titulo do Relatorio 
				String tituloConsulta = "QUANTITATIVO DE PROCESSOS POR STATUS  DE : "+"       "+dataIni+ " AT� "+dataFim;
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				RelatProcStatusBean.consultaRegDiario(RelatProcStatusBean);
				
				if (RelatProcStatusBean.getDados().size()==0) msg = "N�O EXISTEM REGISTROS NESTE DIA." ;	

			 	nextRetorno="/PNT/RelatProcStatusImp.jsp";
			}
			
			if  (acao.equals("Imprimir"))  {
				nextRetorno = "/PNT/Paginacao_Imp.jsp";
			}


			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("StatusId",StatusId) ;
			req.setAttribute("msg", msg);
		    session.setAttribute("RelatProcStatusBean",RelatProcStatusBean);
		    session.setAttribute("PaginacaoBeanId",PaginacaoBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatProcStatusCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}