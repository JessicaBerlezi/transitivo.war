package PNT;

import java.text.SimpleDateFormat;

import sys.Util;

/**
 * <b>Title:</b>        SMIT - MODULO RECURSO<br>
 * <b>Description:</b>  Bean de Aviso de Recebimento Digitalizado<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class LogARDigitalizadoBean extends sys.HtmlPopupBean {
    
    private String datProcessamento;
    private String nomArquivo;
    private String numCaixa;
    private String numLote;
    private String codOperacao;
    private String dscRetorno;
    private String nomUsuario;
    
    private LogARDigitalizadoBean[] $Logs;

    public LogARDigitalizadoBean() {
        
        super();
        datProcessamento = Util.formatedToday().substring(0,10);
        nomArquivo = "";
        numCaixa = "";
        numLote = "";
        codOperacao = "";
        dscRetorno = "";
        nomUsuario = "";
    }
        
    public LogARDigitalizadoBean(String nomArquivo, String numCaixa, String numLote, String codOperacao,
            String dscRetorno, String nomUsuario) {
        
        this();        
        this.nomArquivo = nomArquivo;
        this.numCaixa = numCaixa;
        this.numLote = numLote;
        this.codOperacao = codOperacao;
        this.dscRetorno = dscRetorno;
        this.nomUsuario = nomUsuario;
    }
    
    public String getDatProcessamento() {
        return datProcessamento;
    }
    public void setDatProcessamento(String datProcessamento) {
        this.datProcessamento = datProcessamento;
    }
    public String getDatProcessamentoExt() {
        String extenso = "";
        if (datProcessamento != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            extenso = df.format(datProcessamento);
        }
        return extenso;
    }
    
    public String getDscRetorno() {
        return dscRetorno;
    }
    public void setDscRetorno(String dscRetorno) {
        this.dscRetorno = dscRetorno;
    }
    
    public String getNomArquivo() {
        return nomArquivo;
    }
    public void setNomArquivo(String nomArquivo) {
        this.nomArquivo = nomArquivo;
    }
    
    public String getNumCaixa() {
        return numCaixa;
    }
    public void setNumCaixa(String numCaixa) {
        this.numCaixa = numCaixa;
    }
    
    public String getNumLote() {
        return numLote;
    }
    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }    
    
    public String getNomUsuario() {
        return nomUsuario;
    }
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }
    
    public String getCodOperacao() {
        return codOperacao;
    }
    public void setCodOperacao(String codOperacao) {
        this.codOperacao = codOperacao;
    }
    public String getCodOperacaoExt() {        
        String extenso = ""; 
        if (codOperacao.equals("I"))
            extenso = "Inserido";
        else if (codOperacao.equals("S"))
            extenso = "Substituido";
        else if (codOperacao.equals("R"))
            extenso = "Recusado";            
        return extenso;
    }

    
	public void GravaLogARDigitalizado()  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.GravaLogARDigitalizado(this);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
    
    
    public LogARDigitalizadoBean[] get$Logs() {
        return $Logs;
    }
    
}