package PNT.NOT;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.TAB.PenalidadeBean;

public class AplicarPenalidadeCmd extends sys.Command {
	private String next;   
	private static final String jspPadrao="/PNT/NOT/AplicarPenalidade.jsp";
	
	
	public AplicarPenalidadeCmd() {
		next = jspPadrao;
	}
	
	public AplicarPenalidadeCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;      	
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null) UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;

			ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ParamSistemaBean();

			String numProcessosAplicarPenalidade=parSis.getParamSist("NUM_PROCESSO_APLICAR_PENALIDADE");
			
			String acao    = req.getParameter("acao");
			if (acao==null)	{
				session.removeAttribute("ProcessoBeanID") ;
				acao = "";
			}
			ProcessoBean ProcessoBeanID = (ProcessoBean)session.getAttribute("ProcessoBeanID");
			if (ProcessoBeanID==null)  ProcessoBeanID = new ProcessoBean();
			
			ProcessoBean ProcessoImpBeanID = (ProcessoBean)session.getAttribute("ProcessoImpBeanID");
			if (ProcessoImpBeanID==null)  ProcessoImpBeanID = new ProcessoBean();

			if  (acao.equals("")) {
				ProcessoBeanID.setCodStatus("004,019"); 
				ProcessoBeanID.LeProcessoPenalidade(numProcessosAplicarPenalidade);
				nextRetorno="/PNT/NOT/AplicarPenalidade.jsp";			  			 
			}

			if (acao.equals("AplicarPenalidade"))  
			{   
				try {
					
					String datPenalidade = req.getParameter("datPenalidade");
			        if (datPenalidade==null) datPenalidade="";
					
					nextRetorno="/PNT/NOT/AplicarPenalidade.jsp";
					if (AplicarPenalidade(req,ProcessoBeanID,UsrLogado,ProcessoImpBeanID))
					{	String tituloConsulta = "APLICA��O DE PENALIDADE  "+" - "+datPenalidade;
						req.setAttribute("tituloConsulta", tituloConsulta);
						session.setAttribute("ProcessoImpBeanID",ProcessoImpBeanID);
						nextRetorno="/PNT/NOT/AplicarPenalidadeImp.jsp" ;
					}
				}
				catch (Exception ue) {
					throw new sys.CommandException("AplicarPenalidadeCmd: " + ue.getMessage());
				}
			}
			

			// processamento de saida dos formularios
			if (acao.equals("R")){
				session.removeAttribute("ProcessoBeanID") ;
				session.removeAttribute("ProcessoImpBeanID") ;
			}
			else {
				session.setAttribute("ProcessoBeanID",ProcessoBeanID) ;
			}

		}
		catch (Exception ue) {
			throw new sys.CommandException("AplicarPenalidadeCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public boolean AplicarPenalidade(HttpServletRequest req,ProcessoBean ProcessoBeanID,UsuarioBean UsrLogado,ProcessoBean ProcessoImpBeanID) throws Exception {
        /*Gerar Lista de Processos*/
		List <ProcessoBean>processosImp  = new ArrayList <ProcessoBean>();
		
		String datPenalidade = req.getParameter("datPenalidade");
        if (datPenalidade==null) datPenalidade="";
		
		String codPenalidade=req.getParameter("codPenalidade");
		if (codPenalidade==null) codPenalidade="";
		PenalidadeBean myPenalidade = new PenalidadeBean();
		myPenalidade.setCodPenalidade(codPenalidade);
		myPenalidade.Le_Penalidade("codigo");

		
		String processoPenalidade[] = req.getParameterValues("processoPenalidade");
		if (processoPenalidade==null) processoPenalidade = new String[0];
		for (int j=0; j < processoPenalidade.length; j++) 
		{
			ProcessoBeanID.getProcessos(j).setCodStatus("029");
			ProcessoBeanID.getProcessos(j).setDatStatus(sys.Util.formatedToday().substring(0,10));			
		    ProcessoBeanID.getProcessos(j).setCodPenalidade(myPenalidade.getCodPenalidade());
		    ProcessoBeanID.getProcessos(j).setDscPenalidade(myPenalidade.getDscPenalidade());		    
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(ProcessoBeanID.getProcessos(j).getCodProcesso());   
			myHis.setNumProcesso(ProcessoBeanID.getProcessos(j).getNumProcesso()); 
			myHis.setCodStatus(ProcessoBeanID.getProcessos(j).getCodStatus());  
			myHis.setDatStatus(ProcessoBeanID.getProcessos(j).getDatStatus());
			myHis.setCodOrigemEvento("101");
			myHis.setCodEvento("829");
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			// Codigo Penalidade
			myHis.setTxtComplemento02(myPenalidade.getCodPenalidade());
			// Data Penalidade
			myHis.setTxtComplemento03(datPenalidade);
			Dao dao = DaoFactory.getInstance();
			dao.AplicarPenalidade(ProcessoBeanID.getProcessos(j),myHis);
			
			processosImp.add(ProcessoBeanID.getProcessos(j));
		}
		ProcessoImpBeanID.setProcessos(processosImp);
		return true;		
	}
}

