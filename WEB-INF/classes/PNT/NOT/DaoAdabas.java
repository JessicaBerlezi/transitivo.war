package PNT.NOT;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import sys.BeanException;
import sys.DaoException;
import sys.Util;
import ACSS.UsuarioBean;
import PNT.MultasBean;
import PNT.NotificacaoBean;
import PNT.ProcessoBean;

/**
 * <b>Title:</b>       	SMIT - PNT.NOT.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados de Notifica��es<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoAdabas extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoAdabas() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public String buscaProtocolo(ArquivoBean auxClasse,ACSS.UsuarioBean UsrLogado) throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			conn.setAutoCommit(false);
			// buscar o numero do proximo arquivo e atualizar
			String sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" WHERE NOM_PARAMETRO = 'NUM_PROT_ENVIO' AND COD_ORGAO = '" +
			UsrLogado.getCodOrgaoAtuacao() + "' FOR UPDATE";
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			int numUltProt = 0;
			if (rs.next()) { 
				numUltProt = rs.getInt("VAL_PARAMETRO");                 
			}
			rs.close();
			boolean paramExiste = (numUltProt != 0);
			
			// atualizar numero de sequencia do arquivo
			String nSeq = sys.Util.lPad(String.valueOf(numUltProt), "0", 5);                
			if (!paramExiste)   {
				sCmd = "INSERT INTO TCAU_PARAM_ORGAO (COD_PARAMETRO,";
				sCmd += "NOM_PARAMETRO,NOM_DESCRICAO,VAL_PARAMETRO,COD_ORGAO) ";
				sCmd += "VALUES (SEQ_TCAU_PERFIL.NEXTVAL,'NUM_PROT_ENVIO',";
				sCmd += "'NUMERO DO PROTOCOLO DE ENVIO DE ARQUIVOS',";
				sCmd += "'" + sys.Util.lPad(String.valueOf(numUltProt++), "0", 5) + "'," ;
				sCmd += "'" + UsrLogado.getCodOrgaoAtuacao() + "') ";
			}
			else {
				sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO= '" + sys.Util.lPad(String.valueOf(numUltProt++), "0", 5) + "'" +
				" WHERE NOM_PARAMETRO = 'NUM_PROT_ENVIO' AND COD_ORGAO = '" + 
				UsrLogado.getCodOrgaoAtuacao() + "'";
			}
			stmt.executeUpdate(sCmd);
			conn.commit();
			stmt.close();
			return nSeq ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void arquivoInsertHeader(ArquivoBean myArq) throws DaoException {
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT SEQ_PNT_ARQUIVO.NEXTVAL AS CODIGO FROM DUAL";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			if(rs.next()){
				myArq.setCodArquivo(rs.getString("CODIGO"));
			}
			
			sCmd =  "INSERT INTO TPNT_ARQUIVO "+
			"(COD_ARQUIVO,NOM_ARQUIVO,COD_STATUS,DAT_RECEBIMENTO,DAT_PROCESSAMENTO,DAT_GERACAO,"+
			"NUM_CONTROLE_ARQUIVO,COD_ORGAO,NOM_USERNAME,COD_ORGAO_LOTACAO,"+
			"DAT_PROC,NUM_PROTOCOLO,QTD_LINHA,QTD_LINHA_ERRO,COD_IDENT_ARQUIVO,"+
			"QTD_LINHA_CANCELADA,DAT_INI_PNT,DAT_FIM_PNT) VALUES ("+
			"'"+myArq.getCodArquivo()+"',"+
			"'"+myArq.getNomArquivo()+"',"+
			"'"+myArq.getCodStatus()+"',"+
			"to_date('"+myArq.getDatRecebimento()+"','DD/MM/YYYY'),"+
			"to_date('"+myArq.getDatProcessamento()+"','DD/MM/YYYY'),"+			
			"to_date('"+myArq.getDatGeracao()+"','DD/MM/YYYY'),"+			
			"'"+myArq.getNumControleArq()+"',"+
			"'"+myArq.getCodOrgao()+"',"+
			"'"+myArq.getNomUsername()+"',"+
			"'"+myArq.getCodOrgaoLotacao()+"',"+
			"to_date('"+myArq.getDatProc()+"','DD/MM/YYYY'),"+			
			"'"+myArq.getNumProtocolo()+"',"+
			"'"+myArq.getQtdLinha()+"',"+
			"'"+myArq.getQtdLinhaErro()+"',"+
			"'"+myArq.getCodIdentArquivo()+"',"+		
			"'"+myArq.getQtdLinhaCancelada()+"',"+		
			"to_date('"+myArq.getDatIniPnt()+"','DD/MM/YYYY'),"+			
			"to_date('"+myArq.getDatFimPnt()+"','DD/MM/YYYY') )";
			stmt.executeQuery(sCmd) ;
			if (rs!=null) rs.close();
			stmt.close();
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}
		
	}
	public void arquivoInsertProcesso(ArquivoBean myArq) throws DaoException {
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();			
			String sCmd = "";
			String linha= "";
			for (int i = 0; i < myArq.getLinhasArquivo().size(); i++) {
				linha = myArq.getLinhasArquivo().get(i);
				sCmd=  "INSERT INTO TPNT_ARQUIVO_LINHA "+
				"(COD_LINHA_ARQUIVO,COD_ARQUIVO,TIP_LINHA,"+
				"NUM_SEQ_LINHA,DSC_LINHA_ARQUIVO,COD_STATUS,"+
				"NUM_NOTIFICACAO,NUM_PROCESSO,DAT_INFRACAO) VALUES ("+
				"SEQ_PNT_ARQUIVO_LINHA.NEXTVAL,"+
				"'"+myArq.getCodArquivo()+"',"+
				"'"+linha.substring(0,1)+"',"+
				"'"+(myArq.getSeqLinha()+i)+"',"+
				"'"+linha+"',"+"'0'," +
				"'"+linha.substring(1,10)+"','"+linha.substring(10,30)+"' " ;
				if ("4".equals(linha.substring(0,1))) 
					sCmd += ",to_date('"+Util.formataDataDDMMYYYY(linha.substring(59, 67))+"','dd/mm/yyyy') )" ;
				else	sCmd +=	",'' )";
				stmt.executeQuery(sCmd) ;
			}
			stmt.close();
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}
		
	}
	
	public void arquivoUpdateStatus(ArquivoBean myArq) throws DaoException {
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = "UPDATE TPNT_ARQUIVO SET "+
			"COD_STATUS='"+myArq.getCodStatus()+"' "+
			" WHERE COD_ARQUIVO = '"+myArq.getCodArquivo()+"'";
			stmt.executeQuery(sCmd) ;
			
			long iNumControle=0;
			ResultSet rs = null;
			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO WHERE COD_ORGAO='"+myArq.getCodOrgao()+"' AND "+
			"NOM_PARAMETRO='NUM_CONTROLE_EMITEPONTO'";
			rs=stmt.executeQuery(sCmd) ;
			if (rs.next())
				iNumControle=(rs.getLong("VAL_PARAMETRO"))+1;
			
			sCmd = "UPDATE TCAU_PARAM_ORGAO SET " +
			"VAL_PARAMETRO='"+Util.lPad(String.valueOf(iNumControle),"0",9)+"' "+
			"WHERE COD_ORGAO='"+myArq.getCodOrgao()+"' AND "+
			"NOM_PARAMETRO='NUM_CONTROLE_EMITEPONTO'";
			stmt.executeQuery(sCmd) ;
			
			if (rs!=null) rs.close();
			stmt.close();
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}
	}
	
	/**
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 */	
	public void buscarArquivoRecebido(ArquivoBean myArq) throws DaoException {
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			leArquivo(myArq,"Codigo","",conn);
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}		
	}
	
	public void buscarArquivosRecebidos(ArquivoBean myArq, String status, Connection conn)
	throws DaoException {						
		try {
			leArquivo(myArq, "CodIdent",status,conn);
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return;
	}
	public void leArquivo(ArquivoBean myArq, String codigo,String extra, Connection conn)
	throws DaoException {						
		try {
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT COD_ARQUIVO, NOM_ARQUIVO, COD_IDENT_ARQUIVO,"+
			"to_char(DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO,"
			+ " NUM_CONTROLE_ARQUIVO, COD_ORGAO, COD_STATUS, QTD_LINHA," +
			" NOM_USERNAME, COD_ORGAO_LOTACAO,QTD_LINHA_ERRO,QTD_LINHA_CANCELADA,"+
			" to_char(DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO,"+
			" to_char(DAT_PROCESSAMENTO,'dd/mm/yyyy') as DAT_PROCESSAMENTO,"+
			" to_char(DAT_INI_PNT,'dd/mm/yyyy') as DAT_INI_PNT, to_char(DAT_FIM_PNT,'dd/mm/yyyy') as DAT_FIM_PNT,"+
			" NUM_PROTOCOLO FROM TPNT_ARQUIVO ";
			if ("CodIdent".equals(codigo)) {
				sCmd +=" WHERE COD_IDENT_ARQUIVO ='"+myArq.getCodIdentArquivo()+ "' AND "+
				"COD_STATUS IN (" + extra + ") AND ROWNUM = 1 "+
				" ORDER BY COD_STATUS DESC, COD_ARQUIVO ";
			}
			if ("Codigo".equals(codigo)) {
				sCmd +=" WHERE COD_ARQUIVO ='"+myArq.getCodArquivo()+ "' "+
				" ORDER BY COD_STATUS DESC, COD_ARQUIVO ";
			}
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myArq.setCodArquivo(rs.getString("COD_ARQUIVO"));
				myArq.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myArq.setCodIdentArquivo(rs.getString("COD_IDENT_ARQUIVO"));
				myArq.setDatGeracao(rs.getString("DAT_GERACAO"));
				myArq.setNumControleArq(rs.getString("NUM_CONTROLE_ARQUIVO"));				
				myArq.setCodOrgao(rs.getString("COD_ORGAO"));
				myArq.setCodStatus(rs.getString("COD_STATUS"));
				myArq.setQtdLinha(rs.getInt("QTD_LINHA"));
				myArq.setNomUsername(rs.getString("NOM_USERNAME"));
				myArq.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myArq.setDatProcessamento(rs.getString("DAT_PROCESSAMENTO")); 
				myArq.setQtdLinhaErro(rs.getInt("QTD_LINHA_ERRO"));
				myArq.setQtdLinhaCancelada(rs.getInt("QTD_LINHA_CANCELADA"));
				myArq.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));
				myArq.setDatIniPnt(rs.getString("DAT_INI_PNT"));
				myArq.setDatFimPnt(rs.getString("DAT_FIM_PNT"));				
				myArq.setNumProtocolo(rs.getString("NUM_PROTOCOLO"));				  			
			}
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return;
	}
	
	/**
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 */	
	public void buscarListaArquivosRecebidos(ArquivoBean myArquivo)
	throws DaoException {						
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT a.COD_ARQUIVO, NOM_ARQUIVO, a.COD_IDENT_ARQUIVO,"+
			"to_char(DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO," +
			"NUM_CONTROLE_ARQUIVO, COD_ORGAO, COD_STATUS, QTD_LINHA," +
			"NOM_USERNAME, COD_ORGAO_LOTACAO,QTD_LINHA_ERRO,QTD_LINHA_CANCELADA,"+
			"to_char(DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO,"+
			"to_char(DAT_PROCESSAMENTO,'dd/mm/yyyy') as DAT_PROCESSAMENTO,"+
			"to_char(DAT_INI_PNT,'dd/mm/yyyy') as DAT_INI_PNT, to_char(DAT_FIM_PNT,'dd/mm/yyyy') as DAT_FIM_PNT,"+
			"NUM_PROTOCOLO,b.TOT_NOTIFICACOES,e.TOT_CEP,f.TOT_CEP_NIMP,g.TOT_CEP_IMP,c.tot_notif_nimp,d.tot_notif_imp,ULT_NOTIFICACAO_IMPRESSA  FROM TPNT_ARQUIVO a "+
			/*Total notifica��es n�o impressas*/
			"left join (select count(*) as tot_notif_nimp,cod_arquivo from tpnt_notificacao where dat_impressao is null group by cod_arquivo) c on (c.cod_arquivo=a.cod_arquivo) "+
			/*Total impressas*/
			"left join (select count(*) as tot_notif_imp ,cod_arquivo from tpnt_notificacao where dat_impressao is not null group by cod_arquivo) d on (d.cod_arquivo=a.cod_arquivo) "+
			/*Total cep n�o impressos*/			  
			"left join (select count(*) as tot_cep_nimp,cod_arquivo from ("+
			"select substr(pro.num_cep_condutor,1,5),notif.cod_arquivo "+
			"from tpnt_notificacao notif,tpnt_processo pro "+
			"where notif.cod_processo=pro.cod_processo and notif.dat_impressao is null "+
			"group by substr(pro.num_cep_condutor,1,5),notif.cod_arquivo ) group by cod_arquivo ) f on (f.cod_arquivo=a.cod_arquivo) "+
			/*Total cep impressos*/			  
			"left join (select count(*) as tot_cep_imp,cod_arquivo from ("+
			"select substr(pro.num_cep_condutor,1,5),notif.cod_arquivo "+
			"from tpnt_notificacao notif,tpnt_processo pro "+
			"where notif.cod_processo=pro.cod_processo and notif.dat_impressao is not null "+
			"group by substr(pro.num_cep_condutor,1,5),notif.cod_arquivo ) group by cod_arquivo ) g on (g.cod_arquivo=a.cod_arquivo) ,"+
			/*Total notifica��es*/
			"(select count(*) as tot_notificacoes,cod_arquivo from tpnt_notificacao group by cod_arquivo) b, "+
			/*Total cep*/
			"(select count(*) as tot_cep,cod_arquivo from ("+
			"select substr(pro.num_cep_condutor,1,5),notif.cod_arquivo "+
			"from tpnt_notificacao notif,tpnt_processo pro "+
			"where notif.cod_processo=pro.cod_processo "+
			"group by substr(pro.num_cep_condutor,1,5),notif.cod_arquivo ) group by cod_arquivo ) e "+
			"WHERE a.cod_arquivo=b.cod_arquivo and "+
			"e.cod_arquivo=a.cod_arquivo and "+
			"a.COD_IDENT_ARQUIVO ='" +myArquivo.getCodIdentArquivo()+ "' "+
			" ORDER BY DAT_GERACAO DESC";    
			
			ResultSet rs = stmt.executeQuery(sCmd);			
			List<ArquivoBean> myArquivos = new ArrayList<ArquivoBean>();
			while (rs.next()) {
				ArquivoBean myArq = new ArquivoBean();
				myArq.setCodArquivo(rs.getString("COD_ARQUIVO"));
				myArq.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myArq.setCodIdentArquivo(rs.getString("COD_IDENT_ARQUIVO"));
				myArq.setDatGeracao(rs.getString("DAT_GERACAO"));
				myArq.setNumControleArq(rs.getString("NUM_CONTROLE_ARQUIVO"));				
				myArq.setCodOrgao(rs.getString("COD_ORGAO"));
				myArq.setCodStatus(rs.getString("COD_STATUS"));
				myArq.setQtdLinha(rs.getInt("QTD_LINHA"));
				myArq.setNomUsername(rs.getString("NOM_USERNAME"));
				myArq.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myArq.setDatProcessamento(rs.getString("DAT_PROCESSAMENTO")); 
				myArq.setQtdLinhaErro(rs.getInt("QTD_LINHA_ERRO"));
				myArq.setQtdLinhaCancelada(rs.getInt("QTD_LINHA_CANCELADA"));
				myArq.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));
				myArq.setDatIniPnt(rs.getString("DAT_INI_PNT"));
				myArq.setDatFimPnt(rs.getString("DAT_FIM_PNT"));				
				myArq.setNumProtocolo(rs.getString("NUM_PROTOCOLO"));
				myArq.setTotCep(rs.getString("TOT_CEP"));
				myArq.setTotCepNaoImpressos(rs.getString("TOT_CEP_NIMP"));
				myArq.setTotCepImpressos(rs.getString("TOT_CEP_IMP"));				
				myArq.setTotNotificacoes(rs.getString("TOT_NOTIFICACOES"));
				myArq.setTotNotificacoesNaoImpressas(rs.getString("TOT_NOTIF_NIMP"));
				myArq.setTotNotificacoesImpressas(rs.getString("TOT_NOTIF_IMP"));
				myArq.setUltNotImpressa(rs.getString("ULT_NOTIFICACAO_IMPRESSA"));
				myArquivos.add(myArq);
			}			
			if (rs!=null) rs.close();
			stmt.close();
			myArquivo.setListaArquivos(myArquivos);				
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return;
	}
	
	public void arquivoUpdate(ArquivoBean auxClasse, Connection conn) throws DaoException {				
		try {			
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TPNT_ARQUIVO SET"
				+ " NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "',"
				+ " COD_STATUS = " + auxClasse.getCodStatus() + ","
				+ " DAT_PROCESSAMENTO = TO_DATE('"+auxClasse.getDatProcessamento()+"','DD/MM/YYYY'),"
				+ " QTD_LINHA = " + auxClasse.getQtdLinha()+","				
				+ " QTD_LINHA_ERRO = " + auxClasse.getQtdLinhaErro()+","
				+ " QTD_LINHA_CANCELADA = "+auxClasse.getQtdLinhaCancelada()
				+ " WHERE COD_ARQUIVO = " + auxClasse.getCodArquivo();
			stmt.executeUpdate(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	public List carregarNotifs(ArquivoBean myArq,Connection conn) throws DaoException {		
		List<String> lNotifs = new ArrayList<String>();
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT DISTINCT NUM_NOTIFICACAO "+
			" FROM TPNT_ARQUIVO_LINHA" +
			" WHERE COD_ARQUIVO = '" + myArq.getCodArquivo()+"' "+
			" AND COD_STATUS = '0' " +			
			" ORDER BY NUM_NOTIFICACAO ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			String nnot = "" ;
			while (rs.next()) {				
				nnot = rs.getString("NUM_NOTIFICACAO") ;
				if (nnot!=null)		lNotifs.add(nnot);				
			}
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return lNotifs;
	}
	
	public void carregarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn) throws DaoException {		
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_LINHA_ARQUIVO, TIP_LINHA,"
				+ " DSC_LINHA_ARQUIVO, NUM_SEQ_LINHA,"
				+ " COD_STATUS, COD_RETORNO, DSC_LINHA_RETORNO, NUM_PROCESSO"
				+ " FROM TPNT_ARQUIVO_LINHA"
				+ " WHERE COD_ARQUIVO = '" + myArq.getCodArquivo()+"' "
				+ " AND NUM_NOTIFICACAO='"+myNotif+"' "
				+ " AND COD_STATUS = '0' " ;			
			sCmd+=" ORDER BY TIP_LINHA,DAT_INFRACAO";
			ResultSet rs = stmt.executeQuery(sCmd);
			List<ArquivoLinhaEmitePontoBean> vLinhas = new ArrayList<ArquivoLinhaEmitePontoBean>();
			while (rs.next()) {				
				ArquivoLinhaEmitePontoBean linhaRec = new ArquivoLinhaEmitePontoBean();
				linhaRec.setCodLinhaArquivo(rs.getString("COD_LINHA_ARQUIVO"));
				linhaRec.setTipLinha(rs.getString("TIP_LINHA"));
				linhaRec.setDscLinhaArquivo(rs.getString("DSC_LINHA_ARQUIVO"));
				linhaRec.setNumSeqLinha(rs.getString("NUM_SEQ_LINHA"));
				linhaRec.setCodStatus(rs.getString("COD_STATUS"));
				linhaRec.setCodRetorno(rs.getString("COD_RETORNO"));
				linhaRec.setDscLinhaRetorno(rs.getString("DSC_LINHA_RETORNO"));
				linhaRec.setNumProcesso(rs.getString("NUM_PROCESSO"));
				linhaRec.setNumNotificacao(myNotif);
				linhaRec.setCodOrgaoLotacao(myArq.getCodOrgaoLotacao());
				linhaRec.setNomUsername(myArq.getNomUsername());
				linhaRec.setDatProc(myArq.getDatProc());
				linhaRec.setCodArquivo(myArq.getCodArquivo());
				linhaRec.setRegistro();
				vLinhas.add(linhaRec);				
			}
			myArq.setLinhaEmitePonto(vLinhas);
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return;
	}	
	public void atualizarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn,String myStatus) throws DaoException {		
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TPNT_ARQUIVO_LINHA SET COD_STATUS='"+myStatus+"' " 
			+ " WHERE COD_ARQUIVO = '" + myArq.getCodArquivo()+"' "
			+ " AND NUM_NOTIFICACAO='"+myNotif+"' ";
			
			stmt.executeQuery(sCmd);
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return;
	}	
	public boolean isNotificacaoExiste(String myNotif,Connection conn) throws DaoException {
		boolean Existe=false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT DISTINCT NUM_NOTIFICACAO "+
			" FROM TPNT_NOTIFICACAO" +
			" WHERE NUM_NOTIFICACAO = '" + myNotif+"' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {				
				Existe = true;				
			}
			if (rs!=null) rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return Existe;
	}
	
	/**
	 * Busca as Notifica��es e preenche o ProcessoBean do par�metro
	 * @param codArquivo
	 * @param myProcesso
	 * @throws DaoException
	 */
	public void buscarListaNotifica��esPor(ProcessoBean myProcesso) throws DaoException{
		Connection conn = null;
		
		try{conn = serviceloc.getConnection(MYABREVSIST);	
		Statement stmt = conn.createStatement();
		
		String sCmd = "SELECT N.COD_NOTIFICACAO AS COD_NOTIFICACAO, N.NUM_NOTIFICACAO AS NUM_NOTIFICACAO, N.TIP_NOTIFICACAO AS TIP_NOTIFICACAO, " +
					"N.SEQ_NOTIFICACAO AS SEQ_NOTIFICACAO, N.COD_PROCESSO AS COD_PROCESSO, N.TIP_REEMISSAO AS TIP_REEMISSAO, " +
					"to_char(N.DAT_EMISSAO,'dd/mm/yyyy') AS DAT_EMISSAO, N.COD_ARQUIVO AS COD_ARQUIVO FROM TPNT_PROCESSO P INNER JOIN TPNT_NOTIFICACAO N ON P.COD_PROCESSO = N.COD_PROCESSO "+
					"WHERE N.COD_ARQUIVO = '"+myProcesso.getCodArquivo()+"' AND P.NUM_PROCESSO ='"+myProcesso.getNumProcesso() +"'";
		
		sCmd += " ORDER BY P.NUM_PROCESSO" ;

		ResultSet rs    = stmt.executeQuery(sCmd) ;
		List<NotificacaoBean> myNotificacoes = new ArrayList<NotificacaoBean>();
		
		while (rs.next()) {
			NotificacaoBean notificacao = new NotificacaoBean();
			notificacao.setCodNotificacao(rs.getString("COD_NOTIFICACAO"));
			notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
			notificacao.setTipoNotificacao(rs.getString("TIP_NOTIFICACAO"));
			notificacao.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));
			notificacao.setCodProcesso(rs.getString("COD_PROCESSO"));
			notificacao.setTipoReemissao(rs.getString("TIP_REEMISSAO"));
			notificacao.setDatEmissao(rs.getString("DAT_EMISSAO"));
			notificacao.setCodArquivo(rs.getString("COD_ARQUIVO"));
			
			myNotificacoes.add(notificacao);
			
		
		}
		myProcesso.setNotificacoes(myNotificacoes);
		
		if (rs != null) rs.close();
		stmt.close();
		
	}catch (Exception ex) {
		throw new sys.DaoException("DaoAdabas(PNT.NOT)-buscarListaProcessos : "+ex.getMessage());
	}
	finally {
		if (conn != null) {
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
		}
	}
	}
	
	public void buscarListaProcessos(ArquivoBean myArquivo, String ordem) throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT DISTINCT(PRO.COD_PROCESSO),PRO.NUM_PROCESSO,to_char(PRO.DAT_PROCESSO,'dd/mm/yyyy') as DAT_PROCESSO,"+
			"PRO.TIPO_PROCESSO,PRO.SIT_PROCESSO,PRO.TOT_PONTOS,"+
			"PRO.NOM_CONDUTOR,PRO.NUM_CPF_CONDUTOR,PRO.TIP_CNH_CONDUTOR,"+
			"PRO.NUM_CNH_CONDUTOR,PRO.UF_CNH_CONDUTOR,to_char(PRO.DAT_EMISSAO_CONDUTOR,'dd/mm/yyyy') as DAT_EMISSAO_CONDUTOR,"+
			"to_char(PRO.DAT_VALID_CONDUTOR,'dd/mm/yyyy') as DAT_VALID_CONDUTOR," +
			"PRO.CATEGORIA_CONDUTOR,PRO.DSC_END_CONDUTOR,PRO.NUM_CEP_CONDUTOR,PRO.NOM_MUNIC_CONDUTOR,PRO.UF_CONDUTOR,"+
			"to_char(PRO.ULT_DAT_DEFESA,'dd/mm/yyyy') as ULT_DAT_DEFESA," +
			"to_char(PRO.ULT_DAT_RECURSO,'dd/mm/yyyy') as ULT_DAT_RECURSO," +
			"PRO.COD_PENALIDADE,PRO.DSC_PENALIDADE,PRO.DSC_PENALIDADE_RES,"+
			"to_char(PRO.ULT_DAT_ENTREGACNH,'dd/mm/yyyy') as ULT_DAT_ENTREGACNH,PRO.COD_STATUS," +
			"to_char(PRO.DAT_STATUS,'dd/mm/yyyy') as DAT_STATUS,PRO.NOM_USERNAME," +
			"PRO.COD_ORGAO_LOTACAO,to_char(PRO.DAT_PROC,'dd/mm/yyyy') as DAT_PROC, "+
			"PRO.COD_ORGAO,to_char(PRO.DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO,b.QTD_MULTAS,C.QTD_NOTIFICACAO,D.QTD_PROC_ANTERIORES  "+	
			"FROM TPNT_PROCESSO PRO "+
			"left JOIN (SELECT COUNT(*) AS QTD_MULTAS,COD_PROCESSO FROM TPNT_MULTA GROUP BY COD_PROCESSO)                  B  on  (PRO.COD_PROCESSO=B.COD_PROCESSO)  "+
			"left JOIN (SELECT COUNT(*) AS QTD_NOTIFICACAO,COD_PROCESSO FROM TPNT_NOTIFICACAO WHERE COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' GROUP BY COD_PROCESSO)       C  on  (PRO.COD_PROCESSO=C.COD_PROCESSO)  "+
			"left JOIN (SELECT COUNT(*) AS QTD_PROC_ANTERIORES,COD_PROCESSO FROM TPNT_PROC_ANTERIOR GROUP BY COD_PROCESSO) D  on  (PRO.COD_PROCESSO=D.COD_PROCESSO) ,"+
			"TPNT_NOTIFICACAO NOTIF "+
			"WHERE PRO.COD_PROCESSO=NOTIF.COD_PROCESSO AND "+
			"NOTIF.COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' ";
			
			if (myArquivo.getTipImpressao().equals("I"))
				sCmd+="AND NOTIF.DAT_IMPRESSAO IS NULL ";
			if (myArquivo.getTipImpressao().equals("R"))
				sCmd+="AND NOTIF.DAT_IMPRESSAO IS NOT NULL ";
			if (myArquivo.getListaCep().length()>0)
				sCmd+="AND SUBSTR(PRO.NUM_CEP_CONDUTOR,1,5) IN "+myArquivo.getListaCep()+" ";
			
			if ("1".equals(ordem)) sCmd += " ORDER BY PRO.NUM_PROCESSO" ;
			else if ("2".equals(ordem)) sCmd += " ORDER BY PRO.NUM_CNH_CONDUTOR" ;
			else if ("3".equals(ordem)) sCmd += " ORDER BY PRO.NUM_CEP_CONDUTOR" ;
			else if ("4".equals(ordem)) sCmd += " ORDER BY PRO.NUM_CPF_CONDUTOR" ;
			else 				   sCmd += " ORDER BY PRO.NOM_CONDUTOR" ;
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				ProcessoBean myProcesso = new ProcessoBean();
				myProcesso.setCodArquivo(myArquivo.getCodArquivo());
				myProcesso.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProcesso.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProcesso.setDatProcesso(rs.getString("DAT_PROCESSO"));
				myProcesso.setTipProcesso(rs.getString("TIPO_PROCESSO"));				
				myProcesso.setSitProcesso(rs.getString("SIT_PROCESSO"));
				
				myProcesso.setTotPontos(rs.getString("TOT_PONTOS"));
				myProcesso.setQtdProcAnteriores(rs.getString("QTD_PROC_ANTERIORES"));
				myProcesso.setQtdMultas(rs.getString("QTD_MULTAS"));	
				myProcesso.setTotNotificacoes((rs.getString("QTD_NOTIFICACAO")));
				
				myProcesso.setNomResponsavel(rs.getString("NOM_CONDUTOR"));				
				myProcesso.setNumCPF(rs.getString("NUM_CPF_CONDUTOR"));
				myProcesso.setTipCNH(rs.getString("TIP_CNH_CONDUTOR"));
				myProcesso.setNumCNH(rs.getString("NUM_CNH_CONDUTOR"));												
				myProcesso.setUfCNH(rs.getString("UF_CNH_CONDUTOR"));
				
				myProcesso.setDatEmissaoCNH(rs.getString("DAT_EMISSAO_CONDUTOR"));
				myProcesso.setDatValidadeCNH(rs.getString("DAT_VALID_CONDUTOR"));
				myProcesso.setCategoriaCNH(rs.getString("CATEGORIA_CONDUTOR"));
				myProcesso.setEndereco(rs.getString("DSC_END_CONDUTOR"));
				myProcesso.setCep(rs.getString("NUM_CEP_CONDUTOR"));
				myProcesso.setMunicipio(rs.getString("NOM_MUNIC_CONDUTOR"));
				myProcesso.setUf(rs.getString("UF_CONDUTOR"));
				myProcesso.setUltDatDefesa(rs.getString("ULT_DAT_DEFESA"));
				
				// Altera��o para adicionar 30 dias a partir da data de gera��o do arquivo
				
/*				Calendar dataAtual = Calendar.getInstance();
				dataAtual.add(Calendar.DAY_OF_YEAR,30);
				DateFormat df = new SimpleDateFormat ("dd/MM/yyyy");  
				myProcesso.setUltDatDefesa(df.format(dataAtual.getTime()));
*/
				// Fim altera��o
				
				myProcesso.setUltDatRecurso(rs.getString("ULT_DAT_RECURSO"));
				myProcesso.setUltDatEntregaCNH(rs.getString("ULT_DAT_ENTREGACNH"));
				myProcesso.setCodStatus(rs.getString("COD_STATUS"));	
				myProcesso.setDatStatus(rs.getString("DAT_STATUS"));
				
				myProcesso.setNomUserName(rs.getString("NOM_USERNAME"));
				myProcesso.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myProcesso.setDatProc(rs.getString("DAT_PROC"));		
				myProcesso.setCodOrgao(rs.getString("COD_ORGAO"));
				myProcesso.setDatGeracao(rs.getString("DAT_GERACAO"));
				
				myProcesso.setCodPenalidade(rs.getString("COD_PENALIDADE"));
				myProcesso.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
				myProcesso.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
				
				processos.add(myProcesso);
			}	
			myArquivo.setListaProcessos(processos);
			sCmd = "SELECT COUNT(*) AS TOTAL_NOTIFICACAO FROM TPNT_NOTIFICACAO"+	
			" WHERE COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' ";						
			rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				myArquivo.setTotNotificacoes(rs.getString("TOTAL_NOTIFICACAO"));				
			}
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoAdabas(PNT.NOT)-buscarListaProcessos : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		
	}
	
	public void buscarListaNotificacao(ArquivoBean myArquivo, String ordem) throws DaoException
	{
		Connection conn =null ;					
		try {
			buscarListaProcessos(myArquivo,ordem);			
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			String sCmd = "SELECT COD_NOTIFICACAO,COD_PROCESSO,NUM_NOTIFICACAO,SEQ_NOTIFICACAO,"+
			"TIP_NOTIFICACAO,TIP_REEMISSAO,"+
			"to_char(DAT_EMISSAO,'dd/mm/yyyy') as DAT_EMISSAO,"+
			"COD_ARQUIVO,NOM_USERNAME," +
			"COD_ORGAO_LOTACAO,to_char(DAT_PROC,'dd/mm/yyyy') as DAT_PROC, "+
			"to_char(DAT_GERACAO_CB,'dd/mm/yyyy') as DAT_GERACAO_CB, "+
			"to_char(DAT_PUBL_DO,'dd/mm/yyyy') as DAT_PUBL_DO, "+
			"to_char(DAT_IMPRESSAO,'dd/mm/yyyy') as DAT_IMPRESSAO, "+
			"SEQ_IMPRESSAO, "+
			"NOM_USERNAME_IMPRESSAO,COD_ORGAO_LOTACAO_IMPRESSAO, "+
			"to_char(DAT_PROC_IMPRESSAO,'dd/mm/yyyy') as DAT_PROC_IMPRESSAO, "+
			"SEQ_NOTIFICACAO_IMPRESSAO,COD_RETORNO,IND_ENTREGUE,NOM_USERNAME_RETORNO, "+           
			"COD_ORGAO_LOTACAO_RETORNO, "+      
			"to_char(DAT_PROC_RETORNO,'dd/mm/yyyy') as DAT_PROC_RETORNO, "+			  
			"to_char(DAT_RETORNO,'dd/mm/yyyy') as DAT_RETORNO "+						  
			" FROM TPNT_NOTIFICACAO ";					  						  			
			String sCmdMulta = "SELECT COD_MULTA,COD_PROCESSO,NUM_PROCESSO," +
			"to_char(DAT_PROCESSO,'dd/mm/yyyy') as DAT_PROCESSO,"+
			"NUM_NOTIFICACAO,SEQ_NOTIFICACAO,SEQ_MULTA,COD_ORGAO,SIG_ORGAO,"+
			"NUM_AUTO_INFRACAO,NUM_PLACA,VAL_HOR_INFRACAO,"+
			"to_char(DAT_INFRACAO,'dd/mm/yyyy') as DAT_INFRACAO,"+
			"DSC_LOCAL_INFRACAO,DSC_ENQUADRAMENTO,NUM_PONTO,"+
			"to_char(DAT_TRANS_JULGADO,'dd/mm/yyyy') as DAT_TRANS_JULGADO,"+
			"NOM_USERNAME,COD_ORGAO_LOTACAO,to_char(DAT_PROC,'dd/mm/yyyy') as DAT_PROC, "+
			"DSC_INFRACAO  FROM TPNT_MULTA ";					  						  			
			
			ProcessoBean myProcesso = new ProcessoBean();
			String sSql = "";
			for (int i=0;i<myArquivo.getListaProcessos().size();i++) {
				myProcesso = myArquivo.getListaProcessos().get(i);
				myProcesso.LeRequerimentos();
				sSql = sCmd + " WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' AND "+
				"COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' "+
				"ORDER BY SEQ_NOTIFICACAO ";
				List<NotificacaoBean> notifs = new ArrayList<NotificacaoBean>();
				rs    = stmt.executeQuery(sSql) ;				
				while (rs.next()) {
					NotificacaoBean myNot = new NotificacaoBean();
					myNot.setCodNotificacao(rs.getString("COD_NOTIFICACAO"));				
					myNot.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
					myNot.setTipoNotificacao(rs.getString("TIP_NOTIFICACAO"));
					myNot.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));
					myNot.setCodProcesso(rs.getString("COD_PROCESSO"));
					myNot.setTipoReemissao(rs.getString("TIP_REEMISSAO"));
					myNot.setDatEmissao(rs.getString("DAT_EMISSAO"));				
					myNot.setNomUserName(rs.getString("NOM_USERNAME"));				
					myNot.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));				
					myNot.setDatProc(rs.getString("DAT_PROC"));
					myNot.setCodArquivo(rs.getString("COD_ARQUIVO"));
					myNot.setDatGeracaoCB(rs.getString("DAT_GERACAO_CB"));
					myNot.setDatPublDO(rs.getString("DAT_PUBL_DO"));
					myNot.setDatImpressao(rs.getString("DAT_IMPRESSAO"));
					myNot.setSeqImpressao(rs.getString("SEQ_IMPRESSAO"));
					myNot.setNomUserNameImpressao(rs.getString("NOM_USERNAME_IMPRESSAO"));				
					myNot.setCodOrgaoLotacaoImpressao(rs.getString("COD_ORGAO_LOTACAO_IMPRESSAO"));				
					myNot.setDatProcImpressao(rs.getString("DAT_PROC_IMPRESSAO"));
					myNot.setSeqNotificacaoImpressao(rs.getString("SEQ_NOTIFICACAO_IMPRESSAO"));
					myNot.getRetorno().setNumRetorno(rs.getString("COD_RETORNO"));
					myNot.getRetorno().setIndEntregue(rs.getString("IND_ENTREGUE"));						
					myNot.setDatRetorno(rs.getString("DAT_RETORNO"));
					myNot.setNomUserNameRetorno(rs.getString("NOM_USERNAME_RETORNO"));			
					myNot.setCodOrgaoLotacaoRetorno(rs.getString("COD_ORGAO_LOTACAO_RETORNO"));
					myNot.setDatProcRetorno(rs.getString("DAT_PROC_RETORNO"));
					notifs.add(myNot);
				}	
				myProcesso.setNotificacoes(notifs);
				
				sSql = sCmdMulta + " WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ORDER BY SEQ_MULTA";
				
				List<MultasBean> multas = new ArrayList<MultasBean>();			
				rs    = stmt.executeQuery(sSql) ;				
				while (rs.next()) {
					MultasBean myMul = new MultasBean();
					myMul.setCodMultas(rs.getString("COD_MULTA"));
					myMul.setCodProcesso(rs.getString("COD_PROCESSO"));
					myMul.setNumProcesso(rs.getString("NUM_PROCESSO"));
					myMul.setDatProcesso(rs.getString("DAT_PROCESSO"));
					myMul.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
					myMul.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));
					myMul.setSeqMultaNotificacao(rs.getString("SEQ_MULTA"));
					myMul.setCodOrgao(rs.getString("COD_ORGAO"));
					myMul.setSigOrgao(rs.getString("SIG_ORGAO"));
					myMul.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myMul.setNumPlaca(rs.getString("NUM_PLACA"));
					myMul.setDatInfracao(rs.getString("DAT_INFRACAO"));
					myMul.setValHorInfracao(rs.getString("VAL_HOR_INFRACAO"));					
					myMul.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));
					myMul.setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));					
					myMul.setNumPonto(rs.getString("NUM_PONTO"));					
					myMul.setDatTransJulgado(rs.getString("DAT_TRANS_JULGADO"));
					myMul.setNomUserName(rs.getString("NOM_USERNAME"));
					myMul.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
					myMul.setDatProc(rs.getString("DAT_PROC"));
					myMul.setDscInfracao(rs.getString("DSC_INFRACAO"));					
					multas.add(myMul);
				}	
				myProcesso.setMultas(multas);
				myArquivo.getListaProcessos().set(i,myProcesso)   ;				
			}
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.NOT)-buscarListaNotificacao : "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public void buscarListaCEP(ArquivoBean myArquivo) 	throws DaoException {						
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt     = conn.createStatement();
			String sTotalNotif = "0";
			
			String sCmd ="SELECT SUBSTR(PRO.NUM_CEP_CONDUTOR,1,5) AS CEP,ARQ.NOM_ARQUIVO," +
			"COUNT(*) AS QTD FROM TPNT_NOTIFICACAO NOTIF,TPNT_PROCESSO PRO,TPNT_ARQUIVO ARQ "+
			"WHERE "+
			"PRO.COD_PROCESSO=NOTIF.COD_PROCESSO AND "+
			"NOTIF.COD_ARQUIVO=ARQ.COD_ARQUIVO AND "+
			"NOTIF.COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' ";			
			if (myArquivo.getTipImpressao().equals("I"))
				  sCmd+="AND NOTIF.DAT_IMPRESSAO IS NULL ";
				else
				  sCmd+="AND NOTIF.DAT_IMPRESSAO IS NOT NULL ";				

			sCmd+="GROUP BY SUBSTR(PRO.NUM_CEP_CONDUTOR,1,5),ARQ.NOM_ARQUIVO "+
			"ORDER BY SUBSTR(PRO.NUM_CEP_CONDUTOR,1,5)";			        
			ResultSet rs = stmt.executeQuery(sCmd);			
			List<ProcessoBean> myList = new ArrayList<ProcessoBean>();
			while (rs.next()) {
				ProcessoBean myProcesso = new ProcessoBean();
				myArquivo.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myProcesso.setCep(rs.getString("CEP"));
				myProcesso.setTotNotificacoes(rs.getString("QTD"));
				myList.add(myProcesso);
			}
			
			sCmd ="SELECT COUNT(*) AS TOTAL "+
			"FROM TPNT_NOTIFICACAO "+
			"WHERE "+
			"COD_ARQUIVO='"+myArquivo.getCodArquivo()+"' ";	
			if (myArquivo.getTipImpressao().equals("I"))
			  sCmd+=" AND DAT_IMPRESSAO IS NULL ";
			else
				  sCmd+=" AND DAT_IMPRESSAO IS NOT NULL ";				
			rs = stmt.executeQuery(sCmd);
			if (rs.next())
				sTotalNotif=rs.getString("TOTAL");				
			
			myArquivo.setListaProcessos(myList);
			myArquivo.setTotNotificacoes(sTotalNotif);
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return;
	}
	
	public String buscarSeqImpressao()	throws BeanException  {						
		Connection conn   = null ;
		String sSequencia = "0";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt     = conn.createStatement();						
			String sCmd ="SELECT MAX(SEQ_IMPRESSAO) AS SEQ FROM TPNT_NOTIFICACAO" ;
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				sSequencia=rs.getString("SEQ");
			if (sSequencia==null) sSequencia="0";
			
			sSequencia=String.valueOf( Integer.parseInt(sSequencia)+1 );
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new BeanException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new BeanException(ey.getMessage());
				}
			}
		}
		return sSequencia;
	}


	public void RetornoNotificacao(PNT.ProcessoBean myProc,PNT.NotificacaoBean myNotif,PNT.HistoricoBean myHis) throws DaoException {
		String resultado ="";
		String parteVar = "";
		try 
		
		{	UsuarioBean myUsuario= new UsuarioBean();	
		    parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNumProcesso()," ",20) +			
			sys.Util.lPad(myNotif.getNumNotificacao(),"0",9) +
			sys.Util.lPad(myHis.getTxtComplemento02(),"0",2) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myNotif.getDatRetorno()),"0",8)+
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1) +
			sys.Util.lPad("0000","0",4) +
			sys.Util.lPad("0000","0",4) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProc,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}		
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}		
	}

	/*
	public void RetornoNotificacao(PNT.ProcessoBean myProc,PNT.NotificacaoBean myNotif,PNT.HistoricoBean myHis) throws DaoException {
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			// Atualizar  Historico, Processo e Requerimento
			myNotif.AtualizarNotificacao();
			myHis.GravaHistorico(conn);	
			String sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
			" COD_STATUS = '"+myProc.getCodStatus()+"',"+
			" DAT_STATUS = to_date('"+myProc.getDatStatus()+"','dd/mm/yyyy') "+
			" WHERE COD_PROCESSO='"+myProc.getCodProcesso()+"' ";
			Statement stmt = conn.createStatement();
			stmt.execute(sCmd) ;
			conn.commit();
			stmt.close();
			return  ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
	}
	*/


	

	public void AplicarPenalidade(PNT.ProcessoBean myProc,PNT.HistoricoBean myHis) throws DaoException {
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			// Atualizar  Historico, Processo e Requerimento
			myHis.GravaHistorico(conn);	
			String sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
			" COD_STATUS     = '"+myProc.getCodStatus()+"',"+
			" DAT_STATUS     = to_date('"+myProc.getDatStatus()+"','dd/mm/yyyy'),"+			
			" COD_PENALIDADE = '"+myProc.getCodPenalidade()+"',"+
			" DSC_PENALIDADE = '"+myProc.getDscPenalidade()+"' "+
			" WHERE COD_PROCESSO='"+myProc.getCodProcesso()+"' ";
			Statement stmt = conn.createStatement();
			stmt.execute(sCmd) ;
			conn.commit();
			stmt.close();
			return  ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
	}
	
	public void LeRetornos(ResumoRetornoBean ResumoRetornoBeanId, String sTipoDt) throws DaoException {
		try {
            /*Carregar o Bean inical*/			
			ArrayList <ResumoRetornoBean>myListResumo = new ArrayList<ResumoRetornoBean>();
			for (int i=0;i<12;i++)
			{
				ResumoRetornoBean  myResumo = new ResumoRetornoBean();
				myResumo.setMes(Util.lPad(String.valueOf(i+1),"0",2));
				myListResumo.add(myResumo);
			}
			ResumoRetornoBeanId.setListRetornoMes(myListResumo);
			if (sTipoDt.equals("E")) /*Por data de envio*/
			{
			  calcularEnviados(ResumoRetornoBeanId);
			  calcularRetornados(ResumoRetornoBeanId,sTipoDt);
			  montarListaCodigoRetorno(ResumoRetornoBeanId,sTipoDt);
    		  calcularNaoRetornados(ResumoRetornoBeanId);
			}
			else /*Por data de retorno*/
			{
			  calcularRetornados(ResumoRetornoBeanId,sTipoDt);
			  montarListaCodigoRetorno(ResumoRetornoBeanId,sTipoDt);
			}
				

		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	public void calcularEnviados(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException{
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = null;
			String sCmd     = null;
			/*Calcular EMITEPONTO*/
			sCmd="SELECT COUNT(*) AS TOTAL_ENVIADO "+
			"FROM TPNT_NOTIFICACAO WHERE "+
			"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
			"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY') ";
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
				ResumoRetornoBeanId.setQtdTotalEnviada(rs.getString("TOTAL_ENVIADO"));
			
			sCmd="SELECT TO_CHAR(DAT_EMISSAO,'MM') AS MES,COUNT(*) AS QTD "+
			"FROM TPNT_NOTIFICACAO WHERE "+
			"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
			"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY') "+
			"GROUP BY TO_CHAR(DAT_EMISSAO,'MM') "+
			"ORDER BY TO_CHAR(DAT_EMISSAO,'MM') ";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes=rs.getInt("MES");
				String sQtd=rs.getString("QTD");
				ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalEnviada(sQtd);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void calcularRetornados(ResumoRetornoBean ResumoRetornoBeanId,String sTipoDt) throws DaoException{
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = null;
			String sCmd     = null;
			/*Calcular Retorno EMITEPONTO*/
			if (sTipoDt.equals("E")) /*Por data de envio*/
			{
				sCmd="SELECT COUNT(*) AS TOTAL_RETORNADO "+
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) ";
			}
			else
			{
				sCmd="SELECT COUNT(*) AS TOTAL_RETORNADO "+
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_RETORNO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) ";
			}
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
				ResumoRetornoBeanId.setQtdTotalRetornada(rs.getString("TOTAL_RETORNADO"));

			if (sTipoDt.equals("E")) /*Por data de envio*/
			{
				sCmd="SELECT TO_CHAR(DAT_EMISSAO,'MM') AS MES,COUNT(*) AS QTD "+
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) "+			
				"GROUP BY TO_CHAR(DAT_EMISSAO,'MM') "+
				"ORDER BY TO_CHAR(DAT_EMISSAO,'MM') ";
			}
			else /*Por data de retorno*/
			{
				sCmd="SELECT TO_CHAR(DAT_RETORNO,'MM') AS MES,COUNT(*) AS QTD "+
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_RETORNO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) "+			
				"GROUP BY TO_CHAR(DAT_RETORNO,'MM') "+
				"ORDER BY TO_CHAR(DAT_RETORNO,'MM') ";
			}
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes=rs.getInt("MES");
				String sQtd=rs.getString("QTD");
				ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalRetornada(sQtd);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void calcularCodigoRetorno(ResumoRetornoBean myCodigoResumo, String sTipoDt) throws DaoException{
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = null;
			String sCmd     = null; 
			/*Calcular Retorno EMITEPONTO*/

			if (sTipoDt.equals("E")) /*Por data de envio*/
			{
				sCmd="SELECT COUNT(*) AS QTD "+ 
				"FROM TPNT_NOTIFICACAO WHERE "+ 
				"DAT_EMISSAO>=TO_DATE('"+myCodigoResumo.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_EMISSAO<=TO_DATE('"+myCodigoResumo.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+ 
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatReferencia()+"','DD/MM/YYYY') ) AND "+
				"COD_RETORNO='"+myCodigoResumo.getRetorno().getNumRetorno()+"' AND "+
				"IND_ENTREGUE='"+myCodigoResumo.getRetorno().getIndEntregue()+"'";
			}
			else
			{
				sCmd="SELECT COUNT(*) AS QTD "+ 
				"FROM TPNT_NOTIFICACAO WHERE "+ 
				"DAT_RETORNO>=TO_DATE('"+myCodigoResumo.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+ 
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatReferencia()+"','DD/MM/YYYY') ) AND "+
				"COD_RETORNO='"+myCodigoResumo.getRetorno().getNumRetorno()+"' AND "+
				"IND_ENTREGUE='"+myCodigoResumo.getRetorno().getIndEntregue()+"'";
			}
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
				myCodigoResumo.setQtdTotalRetornada(rs.getString("QTD"));
				
			if (sTipoDt.equals("E")) /*Por data de envio*/
			{
				sCmd="SELECT TO_CHAR(DAT_EMISSAO,'MM') AS MES,COUNT(*) AS QTD "+ 
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_EMISSAO>=TO_DATE('"+myCodigoResumo.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_EMISSAO<=TO_DATE('"+myCodigoResumo.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatReferencia()+"','DD/MM/YYYY') ) AND "+
				"COD_RETORNO='"+myCodigoResumo.getRetorno().getNumRetorno()+"' AND "+
				"IND_ENTREGUE='"+myCodigoResumo.getRetorno().getIndEntregue()+"'"+
				"GROUP BY TO_CHAR(DAT_EMISSAO,'MM') "+
				"ORDER BY TO_CHAR(DAT_EMISSAO,'MM')";
			}
			else /*Por data de retorno*/
			{
				sCmd="SELECT TO_CHAR(DAT_RETORNO,'MM') AS MES,COUNT(*) AS QTD "+ 
				"FROM TPNT_NOTIFICACAO WHERE "+
				"DAT_RETORNO>=TO_DATE('"+myCodigoResumo.getDatInicio()+"','DD/MM/YYYY') AND "+
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatFim()+"','DD/MM/YYYY')   AND "+
				"(DAT_RETORNO IS NOT NULL AND "+
				"DAT_RETORNO<=TO_DATE('"+myCodigoResumo.getDatReferencia()+"','DD/MM/YYYY') ) AND "+
				"COD_RETORNO='"+myCodigoResumo.getRetorno().getNumRetorno()+"' AND "+
				"IND_ENTREGUE='"+myCodigoResumo.getRetorno().getIndEntregue()+"'"+
				"GROUP BY TO_CHAR(DAT_RETORNO,'MM') "+
				"ORDER BY TO_CHAR(DAT_RETORNO,'MM')";
			}
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes=rs.getInt("MES");
				String sQtd=rs.getString("QTD");
				myCodigoResumo.getRetornos(iMes-1).setQtdTotalRetornada(sQtd);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	

	public void montarListaCodigoRetorno(ResumoRetornoBean ResumoRetornoBeanId, String sTipoDt) throws DaoException{
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = null;
			String sCmd="SELECT DISTINCT(T1.COD_RETORNO),T1.IND_ENTREGUE, "+
			"T2.DSC_CODIGO_RETORNO " +
			"FROM TPNT_NOTIFICACAO T1,TSMI_CODIGO_RETORNO T2 "+
			"WHERE T1.COD_RETORNO=T2.NUM_CODIGO_RETORNO ORDER BY T1.IND_ENTREGUE DESC";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				ResumoRetornoBean  myCodigoResumo = new ResumoRetornoBean();
				myCodigoResumo.setDatReferencia(ResumoRetornoBeanId.getDatReferencia());
				myCodigoResumo.setDatInicio(ResumoRetornoBeanId.getDatInicio());
				myCodigoResumo.setDatFim(ResumoRetornoBeanId.getDatFim());
				myCodigoResumo.getRetorno().setNumRetorno(rs.getString("COD_RETORNO"));
				myCodigoResumo.getRetorno().setDescRetorno(rs.getString("DSC_CODIGO_RETORNO"));
				myCodigoResumo.getRetorno().setIndEntregue(rs.getString("IND_ENTREGUE"));
				ArrayList <ResumoRetornoBean>myListCodigoResumo = new ArrayList<ResumoRetornoBean>();
				for (int i=0;i<12;i++)
				{
					ResumoRetornoBean  myCodResumo = new ResumoRetornoBean();
					myCodResumo.setMes(Util.lPad(String.valueOf(i+1),"0",2));
					myListCodigoResumo.add(myCodResumo);
				}
				myCodigoResumo.setListRetornoMes(myListCodigoResumo);				
				calcularCodigoRetorno(myCodigoResumo,sTipoDt);
				ResumoRetornoBeanId.getListCodigoRetornoMes().add(myCodigoResumo);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void calcularNaoRetornados(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException{
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			ResultSet rs    = null;
			String sCmd     = null;
			/*Calcular Retorno EMITEPONTO*/
			sCmd="SELECT COUNT(*) AS TOTAL_NAO_RETORNADO "+
			"FROM TPNT_NOTIFICACAO WHERE "+
			"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
			"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
			"(DAT_RETORNO IS NULL OR "+
			"DAT_RETORNO>TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) "+			
			"GROUP BY TO_CHAR(DAT_EMISSAO,'MM') "+
			"ORDER BY TO_CHAR(DAT_EMISSAO,'MM') ";
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
				ResumoRetornoBeanId.setQtdTotalnaoRetornada(rs.getString("TOTAL_NAO_RETORNADO"));
			
			sCmd="SELECT TO_CHAR(DAT_EMISSAO,'MM') AS MES,COUNT(*) AS QTD "+
			"FROM TPNT_NOTIFICACAO WHERE "+
			"DAT_EMISSAO>=TO_DATE('"+ResumoRetornoBeanId.getDatInicio()+"','DD/MM/YYYY') AND "+
			"DAT_EMISSAO<=TO_DATE('"+ResumoRetornoBeanId.getDatFim()+"','DD/MM/YYYY')   AND "+
			"(DAT_RETORNO IS NULL OR "+
			"DAT_RETORNO>TO_DATE('"+ResumoRetornoBeanId.getDatReferencia()+"','DD/MM/YYYY') ) "+			
			"GROUP BY TO_CHAR(DAT_EMISSAO,'MM') "+
			"ORDER BY TO_CHAR(DAT_EMISSAO,'MM') ";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes=rs.getInt("MES");
				String sQtd=rs.getString("QTD");
				ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalnaoRetornada(sQtd);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}

	public String BuscaARDigitalizado(String numNotificacao, String seqNotificacao) throws DaoException {
		
		String codNotificacao = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT NUM_NOTIFICACAO FROM " +
			"TPNT_AR_DIGITALIZADO " +
			"WHERE "+
			"NUM_NOTIFICACAO = '" + numNotificacao + "' AND "+
			"SEQ_NOTIFICACAO = '" + seqNotificacao + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				codNotificacao = rs.getString("NUM_NOTIFICACAO");
			
			rs.close();
			stmt.close();                
			return codNotificacao;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public String BuscaNumProcesso(String numNotificacao, String seqNotificacao) throws DaoException {
		
		String numProcesso = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT NUM_PROCESSO FROM " +
			"TPNT_NOTIFICACAO NOTIF,TPNT_PROCESSO PRO " +
			"WHERE "+
			"NUM_NOTIFICACAO = '" + numNotificacao + "' AND "+
			"SEQ_NOTIFICACAO = '" + seqNotificacao + "' AND "+
			"NOTIF.COD_PROCESSO = PRO.COD_PROCESSO";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				numProcesso = rs.getString("NUM_PROCESSO");
			
			rs.close();
			stmt.close();                
			return numProcesso;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public String BuscaNumProcessoPorNotificacao(String numNotificacao) throws DaoException {
		
		String numProcesso = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT NUM_PROCESSO FROM " +
			"TPNT_NOTIFICACAO NOTIF,TPNT_PROCESSO PRO " +
			"WHERE "+
			"NUM_NOTIFICACAO = '" + numNotificacao + "' AND "+
			"NOTIF.COD_PROCESSO = PRO.COD_PROCESSO";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				numProcesso = rs.getString("NUM_PROCESSO");
			
			rs.close();
			stmt.close();                
			return numProcesso;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Busca todos os arquivos que possuem um indice de gera��o (IND_GERACAO_ARQ) = null
	 * 
	 */
	public void buscarArquivosaGerar(ArquivoBean myArq,Connection conn)
	throws DaoException {						
		try {
			leArquivoaGerar(myArq,conn);
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return;
	}
	
	public void leArquivoaGerar(ArquivoBean myArq,Connection conn)
	throws DaoException {						
		try {
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT COD_ARQUIVO, NOM_ARQUIVO, COD_IDENT_ARQUIVO,"+
			"to_char(DAT_GERACAO,'dd/mm/yyyy') as DAT_GERACAO,"
			+ " NUM_CONTROLE_ARQUIVO, COD_ORGAO, COD_STATUS, QTD_LINHA," +
			" NOM_USERNAME, COD_ORGAO_LOTACAO,QTD_LINHA_ERRO,QTD_LINHA_CANCELADA,"+
			" to_char(DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO,"+
			" to_char(DAT_PROCESSAMENTO,'dd/mm/yyyy') as DAT_PROCESSAMENTO,"+
			" to_char(DAT_INI_PNT,'dd/mm/yyyy') as DAT_INI_PNT, to_char(DAT_FIM_PNT,'dd/mm/yyyy') as DAT_FIM_PNT,"+
			" NUM_PROTOCOLO FROM TPNT_ARQUIVO WHERE IND_GERACAO_ARQ IS NULL AND ROWNUM = 1";				
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myArq.setCodArquivo(rs.getString("COD_ARQUIVO"));
				myArq.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myArq.setCodIdentArquivo(rs.getString("COD_IDENT_ARQUIVO"));
				myArq.setDatGeracao(rs.getString("DAT_GERACAO"));
				myArq.setNumControleArq(rs.getString("NUM_CONTROLE_ARQUIVO"));				
				myArq.setCodOrgao(rs.getString("COD_ORGAO"));
				myArq.setCodStatus(rs.getString("COD_STATUS"));
				myArq.setQtdLinha(rs.getInt("QTD_LINHA"));
				myArq.setNomUsername(rs.getString("NOM_USERNAME"));
				myArq.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myArq.setDatProcessamento(rs.getString("DAT_PROCESSAMENTO")); 
				myArq.setQtdLinhaErro(rs.getInt("QTD_LINHA_ERRO"));
				myArq.setQtdLinhaCancelada(rs.getInt("QTD_LINHA_CANCELADA"));
				myArq.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));
				myArq.setDatIniPnt(rs.getString("DAT_INI_PNT"));
				myArq.setDatFimPnt(rs.getString("DAT_FIM_PNT"));				
				myArq.setNumProtocolo(rs.getString("NUM_PROTOCOLO"));				  			
			}
			if (rs!=null) rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return;
	}	
	
	public void UpdateEmitePontoVex(ArquivoBean auxClasse, Connection conn) throws DaoException {				
		try {			
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TPNT_ARQUIVO SET"
				+ " IND_GERACAO_ARQ = 'S', "
				+ " COD_STATUS = 2 "
				+ " WHERE COD_ARQUIVO = " + auxClasse.getCodArquivo();
			stmt.executeUpdate(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myProc,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk    = false ;
		 String codErro = resultado.substring(0,3);
		 try {
			 if ((codErro.equals("000")==false) &&
					 (codErro.equals("037")==false)) {
				 if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					 myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 codErro=Util.lPad(codErro.trim(),"0",3);
					 myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					 ResultSet rs   = stmt.executeQuery(sCmd) ;
					 while (rs.next()) {
						 myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					 }
					 rs.close();
				 }
			 }
			 else   bOk = true ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
	
}