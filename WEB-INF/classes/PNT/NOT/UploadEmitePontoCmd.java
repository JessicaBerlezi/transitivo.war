package PNT.NOT;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import sys.CommandException;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import REC.ParamOrgBean;

/**
 * <b>Title:</b>       	SMIT - PNT.NOT.UploadCmd<br>
 * <b>Description:</b> 	Comando para Upload de Arquivos<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class UploadEmitePontoCmd extends sys.Command {
	
	private static final String jspPadrao = "/PNT/NOT/Upload.jsp";	
	public UploadEmitePontoCmd()  {
		super();
	}
	
	public String execute(HttpServletRequest req, sys.UploadFile upload) throws CommandException {		
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = jspPadrao;
			String acao = upload.getParameter("acao");
			if (acao == null) acao = "";
			
			HttpSession session = req.getSession();			
			UsuarioBean usrLogado = (UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new UsuarioBean();					
			ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ParamSistemaBean();
			ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId == null)  ParamOrgaoId = new ParamOrgBean() ;	  			
			
			ArquivoBean arqRec = new ArquivoBean();
			
			arqRec.setCodIdentArquivo(upload.getParameter("j_sigFuncao"));
			arqRec.setCodOrgao(usrLogado.getCodOrgaoAtuacao());
			arqRec.setSigOrgao(usrLogado.getSigOrgaoAtuacao());
			arqRec.setNomUsername(usrLogado.getNomUserName());
			arqRec.setCodOrgaoLotacao(usrLogado.getOrgao().getCodOrgao());
			arqRec.setSigOrgaoLotacao(usrLogado.getOrgao().getSigOrgao());
			arqRec.setNumProtocolo(usrLogado);
			arqRec.setCodStatus("9");
			
			if ("upload".equals(acao.toLowerCase())) {
				nextRetorno = "/PNT/NOT/UploadProtocolo.jsp";
				
				//Criticar os arquivos enviados
				try {
				
					//File file = upload.getFile("arqUpload"); Gl�ucio 09/04/2007 - M�todo
					File file = upload.getFile(req,"arqUpload");
					arqRec.setNomArquivo(file.getName().toUpperCase());												
					BufferedReader br = new BufferedReader(new FileReader(file));
					String auxLinha ;
					List<String> lstLinhas = new ArrayList<String>();
					int prim = 0;
					boolean primProc = true;
					while ((auxLinha = br.readLine()) != null) {
						auxLinha = auxLinha.replaceAll("'", " ");
						auxLinha = auxLinha.replaceAll("\"", " ");						
						if (prim == 0) {   // Header
							prim = 1;                            
							if (arqRec.isHeader(auxLinha,ParamOrgaoId)==false) 
								break;
							
						} else {
							if ("2".equals(auxLinha.substring(0,1))) {
								if (!primProc) {
									// salvo no bean as linhas do processo
									arqRec.setLinhasArquivo(lstLinhas);		
									// vou validar o processo
									if (arqRec.isProcessoOK(ParamOrgaoId)==false) break;
									else {
										//inserir se nao tiver erro e limpar lista
										arqRec.setCodStatus("0");
										arqRec.isInsertProcesso(ParamOrgaoId);					
										lstLinhas = new ArrayList<String>();				
									}
								}
								else {
									primProc=false;
								}
							}
							lstLinhas.add(auxLinha);
						}												
					}
					if ((arqRec.getErrosArquivo().size()==0) && (lstLinhas.size()>0)) {
						arqRec.setLinhasArquivo(lstLinhas);
						arqRec.isProcessoOK(ParamOrgaoId);
					}
					if (arqRec.getErrosArquivo().size()==0) { 
						arqRec.setCodStatus("0");
						arqRec.isInsertProcesso(ParamOrgaoId);							
						arqRec.updateStatus();
					}
					req.setAttribute("ArquivoBeanId", arqRec);

					String NomeArquivo=arqRec.getNomArquivoDir();
					NomeArquivo = NomeArquivo.substring(0,NomeArquivo.lastIndexOf(".")) + ".txt";
					String arqOrigem = file.getPath();
					String arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
					FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));					
					
				} catch (Exception e) {
					arqRec.setMsgErro("UpLoad ==> Erro ao ler arquivo: "+ e);
					req.setAttribute("ArquivoBeanId", arqRec);
				}				
			}
		} catch (Exception ue) {
			throw new CommandException("PNT - UploadCommand 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String execute(HttpServletRequest req) throws CommandException {		
		String nextRetorno = jspPadrao;
		try {
		} catch (Exception ue) {
			throw new CommandException("PNT UploadCommand 002: " + ue.getMessage());
		}		
		return nextRetorno;
	}
	
}