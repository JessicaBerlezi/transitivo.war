package PNT.NOT;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sys.BeanException;
import sys.TypeUtil;
import sys.Util;
import PNT.ProcessoBean;
import REC.ParamOrgBean;

/**
 * <b>Title:</b>       	SMIT - PNT.NOT.ArquivoBean<br>
 * <b>Description:</b> 	Bean de Arquivo Recebido via Upload<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class ArquivoBean extends sys.HtmlPopupBean  {
	
	private String codArquivo;
	private String nomArquivo;	
	private String codStatus; // 0 - Recebido, 1 - Processado, 2 - Retorno Gerado, 7 - Processando, 8 - Interrompido, 9 - Com Erro
	private String datRecebimento;
	private String datProcessamento;
	private String datGeracao;
	private String numControleArq;
	private String codOrgao;
	private String sigOrgao;
	private String nomUsername;
	private String codOrgaoLotacao;
	private String sigOrgaoLotacao;
	private String datProc;
	private String numProtocolo;	
	private Integer qtdLinha;
	private Integer qtdLinhaErro;
	private String codIdentArquivo;	
	private Integer qtdLinhaCancelada;
	private String datIniPnt;
	private String datFimPnt;
	private String totProcessos;
	private String listaCep;
	private String totCep;	
	private String totCepImpressos;
	private String totCepNaoImpressos;
	private String totNotificacoes;
	private String totNotificacoesImpressas;
	private String totNotificacoesNaoImpressas;
	private String tipImpressao;
	private String ultNotImpressa;
	private Integer seqLinha;	
	private List<String> errosArquivo;	
	private List<String> linhasArquivo;
	private List<ArquivoLinhaEmitePontoBean> linhaEmitePonto;
	private List<ArquivoBean> listaArquivos;
	private List<PNT.ProcessoBean> listaProcessos;
	
	public ArquivoBean() {
		super();
		setTabela("TPNT_ARQUIVO");
		setPopupWidth(35);
		codArquivo	           = "0";
		nomArquivo             = "";
		codStatus              = "9";
//			0 - Recebido, 1 - Processado, 2 - Retorno Gerado 
//			7 - Processando, 8 - Interrompido, 9 - Com Erro
		datRecebimento         = Util.formatedToday().substring(0,10);
		datProcessamento       = "";
		datGeracao             = "";
		numControleArq         = "";
		codOrgao               = "0";
		sigOrgao               = "";
		nomUsername            = "";
		codOrgaoLotacao        = "";
		sigOrgaoLotacao        = "";
		datProc                = Util.formatedToday().substring(0,10);
		numProtocolo           = "";
		qtdLinha               = 0;		
		qtdLinhaErro           = 0;
		codIdentArquivo        = "";			
		qtdLinhaCancelada      = 0;			
		seqLinha               = 1;
		datIniPnt              = "";
		datFimPnt              = "";
		totProcessos		   = "0";
		listaCep               = "";
		totCep                 = "0";	
		totCepImpressos        = "0";
		totCepNaoImpressos     = "0";
		totNotificacoes             = "0";
		totNotificacoesImpressas    = "0";
		totNotificacoesNaoImpressas ="0";
		tipImpressao           = "";
		ultNotImpressa         = "0";
		errosArquivo           = new ArrayList<String>();
		linhasArquivo          = new ArrayList<String>();
		linhaEmitePonto        = new ArrayList<ArquivoLinhaEmitePontoBean>();
		listaArquivos          = new ArrayList<ArquivoBean>();
		listaProcessos         = new ArrayList<ProcessoBean>();
	}
	public void setCodArquivo(String codArquivo)  {
		if (codArquivo==null) codArquivo="0";		
		this.codArquivo=codArquivo;
	}
	public String getCodArquivo()  {
		return this.codArquivo;
	}
	public void setNomArquivo (String nomArquivo)	{
		if (nomArquivo==null) nomArquivo="";
		this.nomArquivo = nomArquivo;
	}
	public String getNomArquivo() {
		return this.nomArquivo;
	}
	public void setCodStatus(String codStatus) 	{
		if (codStatus==null) codStatus="9";
		this.codStatus = codStatus;
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	public void setDatRecebimento(String datRecebimento)	{
		if (datRecebimento==null) datRecebimento = Util.formatedToday().substring(0,10);
		this.datRecebimento = datRecebimento;
	}
	public String getDatRecebimento()	{
		return this.datRecebimento;
	}	
	public void setDatProcessamento (String datProcessamento) {
		if (datProcessamento==null) datProcessamento = "";		
		this.datProcessamento = datProcessamento;
	}
	public String getDatProcessamento()	{
		return this.datProcessamento;
	}
	public String getDatGeracao() {
		return datGeracao;
	}
	public void setDatGeracao(String datGeracao) {
		if (datGeracao==null) datGeracao = "";
		this.datGeracao = datGeracao;
	}	
	public void setNumControleArq (String numControleArq) {
		if (numControleArq==null) numControleArq="00000";
		this.numControleArq = numControleArq;
		if (this.numControleArq.length()<5) {
			this.numControleArq=("00000"+this.numControleArq);
			this.numControleArq=this.numControleArq.substring(this.numControleArq.length()-5,this.numControleArq.length());
		}
	}
	public String getNumControleArq() {
		return this.numControleArq;
	}
	public void setCodOrgao (String codOrgao) {
		if (codOrgao==null) codOrgao="";
		this.codOrgao = codOrgao;
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	public void setNomUsername (String nomUsername) {
		if (nomUsername==null) nomUsername="";
		this.nomUsername = nomUsername;
	}
	public String getNomUsername() {
		return this.nomUsername;
	}	
	public void setCodOrgaoLotacao (String codOrgaoLotacao) {
		if (codOrgaoLotacao==null) codOrgaoLotacao="";
		this.codOrgaoLotacao = codOrgaoLotacao;
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	public void setDatProc (String datProc) {
		if (datProc==null) datProc= Util.formatedToday().substring(0,10);
		this.datProc = datProc;
	}
	public String getDatProc()	{
		return this.datProc;
	}
	public void setNumProtocolo (String numProtocolo)	{
		if (numProtocolo==null) numProtocolo="00000";
		this.numProtocolo = numProtocolo;
	}
	public void setNumProtocolo (ACSS.UsuarioBean UsrLogado)  throws BeanException	{
		try {
			Dao dao = DaoFactory.getInstance();
			this.numProtocolo=dao.buscaProtocolo(this,UsrLogado);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
		return;
	}
	public String getNumProtocolo() {
		return this.numProtocolo;
	}
	public void setQtdLinha (Integer qtdLinha){
		this.qtdLinha = qtdLinha;
		if (qtdLinha==null) this.qtdLinha=0;
	}
	public Integer getQtdLinha() 	{
		return this.qtdLinha;
	}
	public void setQtdLinhaErro (Integer qtdLinhaErro){
		if (qtdLinhaErro==null) qtdLinhaErro=0;
		this.qtdLinhaErro = qtdLinhaErro;
	}
	public Integer getQtdLinhaErro() 	{
		return this.qtdLinhaErro;
	}
	public void setQtdLinhaCancelada (Integer qtdLinhaCancelada){
		if (qtdLinhaCancelada==null) qtdLinhaCancelada=0;
		this.qtdLinhaCancelada = qtdLinhaCancelada;
	}
	public Integer getQtdLinhaCancelada() 	{
		return this.qtdLinhaCancelada;
	}	
	public void setSeqLinha (Integer seqLinha){
		if (seqLinha==null) seqLinha=0;
		this.seqLinha = seqLinha;
	}
	public Integer getSeqLinha() 	{
		return this.seqLinha;
	}	
	public String getDatIniPnt() {
		return datIniPnt;
	}
	public void setDatIniPnt(String datIniPnt) {
		if (datIniPnt==null) datIniPnt = "";
		this.datIniPnt = datIniPnt;
	}	
	public String getDatFimPnt() {
		return datFimPnt;
	}
	public void setDatFimPnt(String datFimPnt) {
		if (datFimPnt==null) datFimPnt = "";
		this.datFimPnt = datFimPnt;
	}	

	public void setCodIdentArquivo (String j_sigFuncao)	{
		if (j_sigFuncao==null) j_sigFuncao="";
		
		if ("PNT0110".equals(j_sigFuncao)) {
			this.codIdentArquivo    = "EMITEPONTO";

		// FIXME: Cria��o de um identificador para upload de arquivos para a Perrone
		}else if ("PNT0111".equals(j_sigFuncao)) {
			this.codIdentArquivo    = "EMITEPONTOPER";
			
		}else if ("PNT0116".equals(j_sigFuncao)) {
			this.codIdentArquivo    = "RETPONTO";
			
		}else
			this.codIdentArquivo = j_sigFuncao;
		
	}
	public String getCodIdentArquivo() {
		return this.codIdentArquivo;
	}

	public boolean isHeader(String linhaHeader,ParamOrgBean ParamOrgaoId) throws BeanException {
		boolean OK = true;
		Date dttemp = new Date();		
		try {
			if ("1".equals(linhaHeader.substring(0, 1))==false) {
				OK = false;
				getErrosArquivo().add("00000|999#Tipo de Registro de Header invalido (diferente de 1) \n");
			}
			
			// Descrimina o pr�ximo de tipo de arquivo pelo cod_ident_arquivo
			if (this.codIdentArquivo.equals("EMITEPONTO")){
				String proxCtr=ParamOrgaoId.getParamOrgao("NUM_CONTROLE_EMITEPONTO","000000001","10");
				if (proxCtr.equals(linhaHeader.substring(1, 10))==false) {
					OK = false;
					getErrosArquivo().add("00000|999#Proximo n�mero de controle: "+proxCtr+" \n");
				}
			} else if (this.codIdentArquivo.equals("EMITEPONTOPER")){
				String proxCtr=ParamOrgaoId.getParamOrgao("NUM_CONTROLE_EMITEPONTOPER","000000001","10");
				if (proxCtr.equals(linhaHeader.substring(1, 10))==false) {
					OK = false;
					getErrosArquivo().add("00000|999#Proximo n�mero de controle: "+proxCtr+" \n");
				}
			}
			
			
			this.setNumControleArq(linhaHeader.substring(1, 10));			
			try {
				dttemp=(TypeUtil.parseData(linhaHeader.substring(10, 18),"yyyyMMdd"));
				this.setDatGeracao(Util.fmtData(dttemp));
			} catch (Exception e) {
				OK = false;
				getErrosArquivo().add("00000|999#Data de Gera��o do arquivo invalida \n");
			}
			try {
				this.setQtdLinha(Integer.valueOf(linhaHeader.substring(19, 24)));	
			} catch (Exception e) {
				OK = false;
				getErrosArquivo().add("00000|999#Quantidade de Registros invalida \n");
			}
			if ("EMISSAO PONTUACAO".equals(linhaHeader.substring(24, 41))==false) {
				OK = false;
				getErrosArquivo().add("00000|999#Identifica��o do arquivo difere de:EMISSAO PONTUACAO \n");
			}
			/*
			try {
				dttemp=(TypeUtil.parseData(linhaHeader.substring(41, 49),"yyyyMMdd"));
				this.setDatIniPnt(Util.fmtData(dttemp));
			} catch (Exception e) {
				OK = false;
				getErrosArquivo().add("00000|999#Data de Inicio da Pontua��o invalida \n");
			}
			try {				
				dttemp=(TypeUtil.parseData(linhaHeader.substring(49, 57),"yyyyMMdd"));
				this.setDatFimPnt(Util.fmtData(dttemp));
			} catch (Exception e) {
				OK = false;
				getErrosArquivo().add("00000|999#Data de Termino da Pontua��o invalida \n");
			}
			try {
				if (Util.DifereDias(this.getDatIniPnt(),this.getDatFimPnt())<=0) {
					OK = false;
					getErrosArquivo().add("00000|999#Data de Termino da Pontua��o menor que Inicio \n");
				}
			} catch (Exception e) {			}
			*/

			if (OK==false) {
				getErrosArquivo().add("00000|999#Arquivo n�o carregado: " + this.getNomArquivo() + " \n");				
			}
			Dao dao = DaoFactory.getInstance();
			dao.arquivoInsertHeader(this);
			
			return OK;
		} catch (Exception e) {
			getErrosArquivo().add("00000|999#Arquivo n�o carregado: " + this.getNomArquivo() + " \n");
			getErrosArquivo().add("00000|999#Erro no carregamento: "+e.getMessage()+" \n");	
			throw new BeanException(e.getMessage());
		}
	}
	public boolean isProcessoOK(ParamOrgBean ParamOrgaoId) throws BeanException {
		boolean OK = true;
		int totMulta = 0;
		int totProcAnt = 0;
		int totPtn = 0;
		int totPtn4 = 0;
		int totReg2 = 0;
		int totReg3 = 0;
		int totReg4 = 0;		
		String myProc = "";
		String myNotif = "";
		int temp = 0 ;

		try {			
			for (int i = 0; i < this.linhasArquivo.size(); i++) {
				String linha = this.linhasArquivo.get(i);
				if ("234".indexOf(linha.substring(0, 1))<0) {
					OK = false;
					getErrosArquivo().add("00000|999#Tipo de Registro invalido (diferente de 2,3,4).Processo:"+myProc+" \n");
				}
				if ("2".equals(linha.substring(0, 1))) {
					totReg2++;
					myNotif = linha.substring(1,10);					
					myProc  = linha.substring(10,30);					
					try {						
						// Notificacao
						 Long.parseLong(linha.substring(1, 10));
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Numero da Notifica��o n�o numerico. Processo:"+myProc+" \n");
					}					
					// Notiticacao
					if (linha.substring(1, 10).trim().length()==0) {	
						OK = false;
						getErrosArquivo().add("00000|999#Numero da Notifica��o invalido. Processo:"+myProc+" \n");		
					}
					try {
						// Data Emissao
						TypeUtil.parseData(linha.substring(30, 38),"yyyyMMdd");
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Data de Emiss�o invalida. Processo:"+myProc+" \n");
					}
					try {						
						// Total de Pontos
						totPtn = Integer.valueOf(linha.substring(38, 43));					
					} catch (Exception e) {
						totPtn = 0;
						OK = false;
						getErrosArquivo().add("00000|999#Total de Pontos n�o numerico. Processo:"+myProc+" \n");
					}					
					try {						
						// Qtd Processos Anteriores
						totProcAnt = Integer.valueOf(linha.substring(43, 48));	
					} catch (Exception e) {
						totProcAnt = 0;						
						OK = false;
						getErrosArquivo().add("00000|999#Qtd Processos Anteriores n�o numerico. Processo:"+myProc+" \n");
					}					
					try {						
						// Qtd Multas
						totMulta = Integer.valueOf(linha.substring(48, 53));						
					} catch (Exception e) {
						totMulta = 0;
						OK = false;
						getErrosArquivo().add("00000|999#Qtd Multas n�o numerico. Processo:"+myProc+" \n");
					}							
					try {						
						// CPF
						Long.parseLong(linha.substring(98,109));	
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#CPF n�o numerico. Processo:"+myProc+" \n");
					}							
					try {						
						// CNH
						Long.parseLong(linha.substring(110,121));	
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#CNH n�o numerico. Processo:"+myProc+" \n");
					}		
					// Tipo Reemissao
					if ("ER".indexOf(linha.substring(251,252))<0) {						
						OK = false;
						getErrosArquivo().add("00000|999#Tipo Reemiss�o invalido. Processo:"+myProc+" \n");
					}
					// Tipo Notifica��o
					if ("01234".indexOf(linha.substring(252,253))<0) {						
						OK = false;
						getErrosArquivo().add("00000|999#Tipo Notifica��o invalido. Processo:"+myProc+" \n");
					}
					// Tipo Processo
					if ("SP".indexOf(linha.substring(253,254))<0) {						
						OK = false;
						getErrosArquivo().add("00000|999#Tipo Processo invalido. Processo:"+myProc+" \n");
					}
					try {
						// Data da Defesa
						if ("02".indexOf(linha.substring(252,253))>=0) {
							TypeUtil.parseData(linha.substring(254,262),"yyyyMMdd");
						}
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Data para Defesa/Recurso invalida. Processo:"+myProc+" \n");
					}
					try {
						// Data da Entrega da CNH
						if ("24".indexOf(linha.substring(252,253))>=0) {
							TypeUtil.parseData(linha.substring(262,270),"yyyyMMdd");
						}
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Data da Entrega da CNH invalida. Processo:"+myProc+" \n");
					}					
				}
				if ("3".equals(linha.substring(0, 1))) {
					totReg3++;
					// Notifica��o
					if (myNotif.equals(linha.substring(1,10))==false) {
						OK = false;
						getErrosArquivo().add("00000|999#Numero da Notifica��o diferente do Processo. Processo:"+myProc+" \n");
					}
					// Notifica��o
					if (myProc.equals(linha.substring(10,30))==false) {
						OK = false;
						getErrosArquivo().add("00000|999#Numero do Processo diferente. Processo:"+myProc+" \n");
					}
					// Processo Anterior
					if (linha.substring(30, 50).trim().length()==0) {	
						OK = false;
						getErrosArquivo().add("00000|999#Numero da Processo Anterior invalido. Processo:"+myProc+" \n");		
					}
					try {
						// Data Emissao
						TypeUtil.parseData(linha.substring(50, 58),"yyyyMMdd");
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Data de Emiss�o do Processo Anterior invalida. Processo:"+myProc+" \n");
					}
					// Tipo Processo
					if ("SP".indexOf(linha.substring(58,59))<0) {						
						OK = false;
						getErrosArquivo().add("00000|999#Tipo Processo Anterior invalido. Processo:"+myProc+" \n");
					}
					try {						
						// Total de Pontos
						temp = Integer.valueOf(linha.substring(59, 64));	
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Total de Pontos Proc Anterior n�o numerico. Processo:"+myProc+" \n");
					}		
				}
				if ("4".equals(linha.substring(0, 1))) {
					totReg4++;
					// Notifica��o
					if (myNotif.equals(linha.substring(1,10))==false) {
						OK = false;
						getErrosArquivo().add("00000|999#Numero da Notifica��o diferente do Processo. Processo:"+myProc+" \n");
					}
					// Processo
					if (myProc.equals(linha.substring(10,30))==false) {
						OK = false;
						getErrosArquivo().add("00000|999#Numero do Processo diferente. Processo:"+myProc+" \n");
					}
					try {						
						// Total de Pontos
						temp = Integer.valueOf(linha.substring(201,203));
						totPtn4+=temp;
					} catch (Exception e) {
						OK = false;
						getErrosArquivo().add("00000|999#Pontos da Multa n�o numerico. Processo:"+myProc+" \n");
					}					
				}
			}
			// Testar os totais, dos arquivos
			if (totPtn!=totPtn4) {
				OK = false;
				getErrosArquivo().add("00000|999#Total de Pontos difere da Soma do Regs 4. Processo:"+myProc+" \n");					
			}
			if (totProcAnt!=totReg3) {
				OK = false;
				getErrosArquivo().add("00000|999#Total de Regs 3 n�o confere. Processo:"+myProc+" \n");										
			}
			if (totMulta!=totReg4) {
				OK = false;
				getErrosArquivo().add("00000|999#Total de Regs 4 n�o confere. Processo:"+myProc+" \n");										
			}
			if (OK==false) {
				getErrosArquivo().add("00000|999#Arquivo n�o carregado: " + this.getNomArquivo() + " \n");				
			}
		} catch (Exception e) {
			getErrosArquivo().add("00000|999#Arquivo n�o carregado: " + this.getNomArquivo() + " \n");
			getErrosArquivo().add("00000|999#Erro no carregamento: "+e.getMessage()+" \n");
			OK=false;
		}
		return OK;		
	}

	public void setErrosArquivo(List<String> errosArquivo) {
		if (this.errosArquivo==null) errosArquivo = new ArrayList<String>();
		this.errosArquivo = errosArquivo;
	}
	public List<String> getErrosArquivo() 	{
		return errosArquivo;
	}
	public void setErroArquivo(String erroArquivo) {
		if (erroArquivo==null) erroArquivo = "";
		List<String> erroList = new ArrayList<String>();
		erroList.add(erroArquivo);
		this.errosArquivo = erroList;		
	}	

	public String getNomArquivoDir() {
		String nomArquivoDir = this.getCodIdentArquivo() + "/" + this.nomArquivo;
		return nomArquivoDir;
	}
	public void setLinhasArquivo (List<String> linhasArquivo) {
		if (linhasArquivo==null) linhasArquivo= new ArrayList<String>();
		this.linhasArquivo = linhasArquivo;
	}
	public List<String> getLinhasArquivo() 	{
		return this.linhasArquivo;
	}
	public String getLinhaArquivo(Integer i) 	{
		return this.linhasArquivo.get(i);
	}	
	public void setLinhaEmitePonto (List<ArquivoLinhaEmitePontoBean> linhaEmitePonto) {
		if (linhaEmitePonto==null) linhaEmitePonto= new ArrayList<ArquivoLinhaEmitePontoBean>();
		this.linhaEmitePonto = linhaEmitePonto;
	}
	public List<ArquivoLinhaEmitePontoBean> getLinhaEmitePonto() 	{
		return this.linhaEmitePonto;
	}
	public ArquivoLinhaEmitePontoBean getLinhaEmitePonto(Integer i) 	{
		return this.linhaEmitePonto.get(i);
	}	

	public List<ArquivoBean> getListaArquivos() {
		return listaArquivos;
	}
	public void setListaArquivos(List<ArquivoBean> listaArquivos) {
		if (listaArquivos == null) listaArquivos = new ArrayList<ArquivoBean>();
		this.listaArquivos = listaArquivos;
	}		
	public ArquivoBean getListaArquivos(Integer i) 	{
		return this.listaArquivos.get(i);
	}	
	
	public List<PNT.ProcessoBean> getListaProcessos() {
		return listaProcessos;
	}
	public void setListaProcessos(List<PNT.ProcessoBean> listaProcessos) {
		if (listaProcessos == null) listaProcessos = new ArrayList<PNT.ProcessoBean>();		
		this.listaProcessos = listaProcessos;
	}	
	public PNT.ProcessoBean getListaProcessos(Integer i) 	{
		return this.listaProcessos.get(i);
	}	
	
	
	public boolean isInsertProcesso(ParamOrgBean ParamOrgaoId) {
		boolean retorno = true;
		try {
			Dao dao = DaoFactory.getInstance();
			dao.arquivoInsertProcesso(this);
			setSeqLinha(getSeqLinha()+getLinhasArquivo().size());
		}
		catch (Exception e) {
			retorno = false;
			getErrosArquivo().add("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n");
			getErrosArquivo().add("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
		}
		return retorno;
	}
	public void updateStatus() {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.arquivoUpdateStatus(this);
		}
		catch (Exception e) {
		}
		return ;
	}
	public String getSigOrgao() {
		return sigOrgao;
	}
	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao==null) sigOrgao="";
		this.sigOrgao = sigOrgao;
	}
	public String getSigOrgaoLotacao() {
		return sigOrgaoLotacao;
	}
	public void setSigOrgaoLotacao(String sigOrgaoLotacao) {
		if (sigOrgaoLotacao==null) sigOrgaoLotacao="";
		this.sigOrgaoLotacao = sigOrgaoLotacao;
	}

	/**
	 * M�todo para buscar arquivos de acordo com o identArquivo e o staus
	 */	
	public void buscarArquivos(String status, Connection conn) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarArquivosRecebidos(this, status, conn);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	
	public void buscarArquivo() throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarArquivoRecebido(this);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
	/**
	 * M�todo para buscar Lista de arquivos de acordo com o identArquivo 
	 */	
	public void buscarListaArquivosRecebidos() throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarListaArquivosRecebidos(this);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	
	/**
	 * M�todo para buscar Lista de Processos do arquivo 
	 */	
	public void buscarListaProcessos(String ordem) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarListaProcessos(this,ordem);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	

	
	public void buscarListaNotificacoes(String ordem) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarListaNotificacao(this,ordem);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	
	
	
	/**
	 * M�todo para Alterar Arquivo na base de dados
	 * O �nico campo que pode ser alterado � o status.
	 */
	public boolean isUpdate(Connection conn) {
		boolean bOk = true;
		List <String>vErro = new ArrayList<String>();
		try {
			Dao dao = DaoFactory.getInstance();
			dao.arquivoUpdate(this, conn);
		}
		catch (Exception e) {
			vErro.add("Arquivo n�o atualizado: " + this.getNomArquivo() + " \n");
			vErro.add("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro((vErro.toString()));
		return bOk;
	}
	/**
	 * M�todo para carregar as linhas do arquivo
	 */	
	public List carregarNotifs(Connection conn) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			return dao.carregarNotifs(this, conn);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}		
	/**
	 * M�todo para carregar as linhas do arquivo no banco de dados Oracle, pelo numero da Notifica��o
	 */	

	public void carregarLinhas(String myNotif,Connection conn) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.carregarLinhasArquivo(this,myNotif, conn);
			return ;
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}		
	/**
	 * M�todo para cancelar as linhas do arquivo status da linha = 9 (cancelado)
	 */	

	public void atualizarLinhas(String myNotif,Connection conn,String myStatus) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.atualizarLinhasArquivo(this,myNotif, conn,myStatus);
			return ;
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}		
	/**
	 * M�todo para verificar se a Notificacao ja existe
	 */	

	public boolean isNotificacaoExiste(String myNotif,Connection conn) throws BeanException {
		boolean Existe=false;
		try {
			Dao dao = DaoFactory.getInstance();
			Existe=dao.isNotificacaoExiste(myNotif, conn);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
		return Existe;
	}
	public String getUltNotImpressa() {
		return ultNotImpressa;
	}
	public void setUltNotImpressa(String ultNotImpressa) {
		if (ultNotImpressa==null) ultNotImpressa="0";
		this.ultNotImpressa = ultNotImpressa;
	}
	public String getTotProcessos() {
		return totProcessos;
	}
	public void setToProcessos(String totProcessos) {
		if (totProcessos==null) totProcessos="0";
		this.totProcessos = totProcessos;
	}
	public String getTotNotificacoes() {
		return totNotificacoes;
	}
	public void setTotNotificacoes(String totNotificacoes) {
		if (totNotificacoes==null) totNotificacoes="0";
		this.totNotificacoes = totNotificacoes;
	}
	public String getTotCep() {
		return totCep;
	}
	public void setTotCep(String totCep) {
		if (totCep==null) totCep="0";
		this.totCep = totCep;
	}
	public String getTotCepImpressos() {
		return totCepImpressos;
	}
	public void setTotCepImpressos(String totCepImpressos) {
		if (totCepImpressos==null) totCepImpressos="0";
		this.totCepImpressos = totCepImpressos;
	}
	public String getTotCepNaoImpressos() {
		return totCepNaoImpressos;
	}
	public void setTotCepNaoImpressos(String totCepNaoImpressos) {
		if (totCepNaoImpressos==null) totCepNaoImpressos="0";
		this.totCepNaoImpressos = totCepNaoImpressos;
	}
	public void setTotProcessos(String totProcessos) {
		this.totProcessos = totProcessos;
	}
	public String getTotNotificacoesImpressas() {
		return totNotificacoesImpressas;
	}
	public void setTotNotificacoesImpressas(String totNotificacoesImpressas) {
		if (totNotificacoesImpressas==null) totNotificacoesImpressas="0";
		this.totNotificacoesImpressas = totNotificacoesImpressas;
	}
	public String getTotNotificacoesNaoImpressas() {
		return totNotificacoesNaoImpressas;
	}
	public void setTotNotificacoesNaoImpressas(String totNotificacoesNaoImpressas) {
		if (totNotificacoesNaoImpressas==null) totNotificacoesNaoImpressas="0";
		this.totNotificacoesNaoImpressas = totNotificacoesNaoImpressas;
	}
	public String getTipImpressao() {
		return tipImpressao;
	}
	public void setTipImpressao(String tipImpressao) {
		if (tipImpressao==null) tipImpressao="I";
		this.tipImpressao = tipImpressao;
	}
	public String getListaCep() {
		return listaCep;
	}
	public void setListaCep(String listaCep) {
		if (listaCep==null) listaCep="";
		this.listaCep = listaCep;
	}
	public void buscarListaCEP() throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarListaCEP(this);
			return;
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	public String buscarSeqImpressao() throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			return dao.buscarSeqImpressao();
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}

	/**
	 * M�todo para buscar arquivos para a gera��o do EmitePontoVex
	 */	
	public void buscarArquivosaGerar(Connection conn) throws BeanException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.buscarArquivosaGerar(this,conn);
		}
		catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	
	
	/**
	 * M�todo para Alterar O indicativo de gera��o dos Arquivos 
	 * que formaram o EmitePontoVex.
	 */
	public boolean UpdateEmitePontoVex(Connection conn) {
		boolean bOk = true;
		List <String>vErro = new ArrayList<String>();
		try {
			Dao dao = DaoFactory.getInstance();
			dao.UpdateEmitePontoVex(this, conn);
		}
		catch (Exception e) {
			vErro.add("Arquivo n�o atualizado: " + this.getNomArquivo() + " \n");
			vErro.add("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro((vErro.toString()));
		return bOk;
	}	
	
	public String getHeaderGrv() {

		String headerGrv = "";

		if (this.getCodIdentArquivo().equals("EMITEPONTO") || this.getCodIdentArquivo().equals("EMITEPONTOPER")) {
			String tipoReg  	 = "1";
			String numControle   = Util.lPad(this.getNumProtocolo(),"0",9);
			String dataGer 	     = Util.lPad(Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8);
			String qtdReg        = Util.lPad(String.valueOf(this.getQtdLinha()),"0",6);
			String identArq  	 = "EMISSAO PONTUACAO";
			String datIniMultas	 = Util.lPad(Util.formataDataYYYYMMDD(this.getDatIniPnt()),"0",8);
			String datFimMultas	 = Util.lPad(Util.formataDataYYYYMMDD(this.getDatFimPnt()),"0",8);
			headerGrv      		 = tipoReg+numControle+dataGer+qtdReg+identArq+datIniMultas+datFimMultas+Util.rPad(" "," ",393);;

		} 

		return headerGrv;
	}
	
	
	public String getNomArquivoGrv() {

		String nomArquivoGrv = "";

		if (this.getCodIdentArquivo().equals("EMITEPONTO")) {	
			nomArquivoGrv=this.getNomArquivo().replaceAll("EMITEPONTO","EMITEPONTOVEX_");
			
		} else if (this.getCodIdentArquivo().equals("EMITEPONTOPER")){
			nomArquivoGrv=this.getNomArquivo().replaceAll("EMITEPONTO","EMITEPONTOPER_");
			
		}
		nomArquivoGrv = this.getCodIdentArquivo() + "/" + nomArquivoGrv;

		return nomArquivoGrv;
	}
	
	
}