package PNT.NOT;

import java.util.ArrayList;
import java.util.List;
import PNT.TAB.CodigoRetornoBean;


/**
* <b>Title:</b>        	SMIT-REC - Bean de Relator <br>
* <b>Description:</b>  	Bean dados das Juntas - Tabela de Retorno<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
* @author  				Sergio Roberto Jr.
* @version 				1.0
*/

public class RetornoNotificacaoBean  { 


	private String datRetorno;
	private String totalNotificacoes;
	private List<String> listaNotificacoes;	
    private CodigoRetornoBean retorno;
	
	private String msgErro ; 
	private String msgOk ;  
	private String eventoOK;

	
	public RetornoNotificacaoBean() throws sys.BeanException{
		datRetorno        = sys.Util.formatedToday().substring(0,10);
		retorno           = new CodigoRetornoBean();
		msgErro           = ""; 
		msgOk             = "";  
		eventoOK          = "";
		listaNotificacoes = new ArrayList<String>();
		totalNotificacoes = "";
	}

	public List<String> getListaNotificacoes() {
		return listaNotificacoes;
	}
	public void setListaNotificacoes(List<String> listaNotificacoes) {
		if (listaNotificacoes==null) listaNotificacoes=new ArrayList<String>();
		this.listaNotificacoes = listaNotificacoes;
	}
	public String getNotificacao(Integer i) 	{
		return this.listaNotificacoes.get(i);
	}
	

	public String getEventoOK() {
		return eventoOK;
	}
	public void setEventoOK(String eventoOK) {
		if (eventoOK==null) eventoOK="";
		this.eventoOK = eventoOK;
	}

	public String getMsgErro() {
		return msgErro;
	}
	public void setMsgErro(String msgErro) {
		if (msgErro==null) msgErro="";
		this.msgErro = msgErro;
	}

	public String getMsgOk() {
		return msgOk;
	}
	public void setMsgOk(String msgOk) {
		if (msgOk==null) msgOk="";
		this.msgOk = msgOk;
	}

	public String getTotalNotificacoes() {
		return totalNotificacoes;
	}

	public void setTotalNotificacoes(String totalNotificacoes) {
		if (totalNotificacoes==null) totalNotificacoes="0";
		try	{ Integer.parseInt(totalNotificacoes);	 }
		catch (Exception e)	{ totalNotificacoes="0"; }
		this.totalNotificacoes = totalNotificacoes;
	}

	public String getDatRetorno() {
		return datRetorno;
	}

	public void setDatRetorno(String datRetorno) {
		if (datRetorno==null) datRetorno=sys.Util.formatedToday().substring(0,10);
		this.datRetorno = datRetorno;
	}

    public void setRetorno(CodigoRetornoBean retorno)   throws sys.BeanException  {
        this.retorno=retorno ;
        if (retorno==null) this.retorno= new CodigoRetornoBean() ;
    }  
    public CodigoRetornoBean getRetorno()  {
        return this.retorno;
    }
	

}