package PNT.NOT;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import PNT.PaginacaoBean;
import PNT.RelatProcStatusBean;

public class GeraXlsRelatProcStatus extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
		{	
			try {
				HttpSession session   = req.getSession() ;

				RelatProcStatusBean RelatProcStatusBean = (RelatProcStatusBean) session.getAttribute("RelatProcStatusBean");
				if (RelatProcStatusBean == null)RelatProcStatusBean = new RelatProcStatusBean();
				
				ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
				if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
				
				REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
				if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
				
				ACSS.ParamSistemaBean paramPnt = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
				if (paramPnt == null)	paramPnt = new ACSS.ParamSistemaBean();
				
				PaginacaoBean PaginacaoBeanId = (PaginacaoBean)session.getAttribute("PaginacaoBeanId") ;
				if (PaginacaoBeanId==null)  PaginacaoBeanId = new PaginacaoBean() ;
				
				
				String acao = req.getParameter("acao");
				if( acao == null ) acao = "";             
				
				String contexto = req.getParameter("contexto");
				if( contexto == null ) contexto = "";
				
				String nomArquivo = req.getParameter("nomArquivo");
				if( nomArquivo == null ) nomArquivo = "";
				
				if ( acao.equals("") ) {				
					req.setAttribute("RelatProcStatusBean", RelatProcStatusBean);
				}
				else if(acao.equals("GeraExcel")) 
				{
					
					String codStatus = req.getParameter("codStatus");
					if (codStatus == null) codStatus = "";
					

					String nomStatus = req.getParameter("nomStatus");
					if (nomStatus == null) nomStatus = "";
					
					String todosStatus = req.getParameter("todosStatus");
					if (todosStatus == null)todosStatus="N";
					
					String dataIni = req.getParameter("dataIni");
					if (dataIni == null) dataIni = "";
					
					String dataFim = req.getParameter("dataFim");
					if (dataFim == null) dataFim = "";

					PaginacaoBeanId.setDatInicio(dataIni);
					PaginacaoBeanId.setDatFim(dataFim );
					 
					PaginacaoBeanId.setCodTransacao(codStatus);
					PaginacaoBeanId.setQtd_Registros_Rodada("999999999");

						
					PaginacaoBeanId.setTransRef("0");  
					PaginacaoBeanId.setDatProcRef("0");  
					 
					PaginacaoBeanId.setQtdParcialReg("0");
					PaginacaoBeanId.CarregaLista(acao);
					
					RelatProcStatusXls geraxls = new RelatProcStatusXls();
					String tituloConsulta = codStatus + " - " + nomStatus;
					ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
					paramSys.setCodSistema("21"); // M�dulo PNT
					paramSys.PreparaParam();
					String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
					geraxls.write(PaginacaoBeanId, tituloConsulta, arquivo,req);	
					req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
					rd.forward(req, resp);
				}
				
			}   catch (Exception e) {
				throw new ServletException(e.getMessage());            
			} 
		}
	}
}
