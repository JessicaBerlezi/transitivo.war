package PNT.NOT;

import java.sql.Connection;
import java.util.List;

import sys.BeanException;
import sys.DaoException;
import PNT.ProcessoBean;

public interface Dao {
		public abstract String buscaProtocolo(ArquivoBean auxClasse,ACSS.UsuarioBean UsrLogado) throws DaoException ;	
		public abstract void arquivoInsertHeader(ArquivoBean myArq) throws DaoException;	
		public abstract void arquivoInsertProcesso(ArquivoBean myArq) throws DaoException ;
		public abstract void arquivoUpdateStatus(ArquivoBean myArq) throws DaoException ;		
		public abstract void buscarArquivosRecebidos(ArquivoBean myArq, String status, Connection conn) throws DaoException;		
		public abstract void buscarArquivoRecebido(ArquivoBean myArq) throws DaoException ;
		public abstract void buscarListaArquivosRecebidos(ArquivoBean myArquivo) throws DaoException ;
		public abstract void buscarListaProcessos(ArquivoBean myArquivo, String ordem) throws DaoException ;
		public abstract void buscarListaNotificacao(ArquivoBean myArquivo, String ordem) throws DaoException ;
		public abstract void buscarListaCEP(ArquivoBean myArquivo) throws DaoException ;
		public abstract void arquivoUpdate(ArquivoBean auxClasse, Connection conn) throws DaoException ;
		public abstract List carregarNotifs(ArquivoBean myArq,Connection conn) throws DaoException ;
		public abstract void carregarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn) throws DaoException ;
		public abstract void atualizarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn,String myStatus) throws DaoException  ;		
		public abstract boolean isNotificacaoExiste(String myNotif,Connection conn) throws DaoException ;
		public abstract String buscarSeqImpressao() throws BeanException;
		public abstract void RetornoNotificacao(ProcessoBean myProc,PNT.NotificacaoBean myNotif,PNT.HistoricoBean myHis) throws DaoException ;
		public abstract void AplicarPenalidade(ProcessoBean myProc,PNT.HistoricoBean myHis) throws DaoException ;		
		public abstract void LeRetornos(ResumoRetornoBean ResumoRetornoBeanId, String sTipoDt) throws DaoException;
		public abstract String BuscaNumProcesso(String numNotificacao, String seqNotificacao) throws DaoException;
		public abstract String BuscaARDigitalizado(String numNotificacao, String seqNotificacao) throws DaoException;
		public abstract void buscarArquivosaGerar(ArquivoBean myArq,Connection conn) throws DaoException;
		public abstract void UpdateEmitePontoVex(ArquivoBean auxClasse, Connection conn) throws DaoException ;
		public void buscarListaNotificaçõesPor(ProcessoBean myProcesso) throws DaoException;
		public String BuscaNumProcessoPorNotificacao(String numNotificacao) throws DaoException;
}