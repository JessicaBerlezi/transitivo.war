package PNT.NOT;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.OrgaoBean;

import sys.CommandException;

/**
* <b>Title:</b>        Pontuacao - Relatorio do Arquivo de Notificacoes<br>
* <b>Description:</b>  Informa os arquivos de Notificacoes enviados. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      MIND - Informatica <br>
* @author Luciana Rocha
* @version 1.0
*/


public class RelatArqNotifCmd extends sys.Command {

	private static final String jspPadrao  = "/PNT/NOT/RelatArqNotif.jsp";
	private String next;

	public RelatArqNotifCmd() {
		next = jspPadrao;
	}

	public RelatArqNotifCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req ) throws CommandException {
		String nextRetorno = next;
		try {
		    // cria os Beans de sessao, se n�o existir
		    HttpSession session   = req.getSession() ;								
		    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
		  	// cria os Beans, se n�o existir
		    ArquivoBean ArquivoBeanID     = (ArquivoBean)session.getAttribute("ArquivoBeanID") ;
			if (ArquivoBeanID==null)  ArquivoBeanID = new ArquivoBean() ;	  			

			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			if(acao.equals("")) {
				ArquivoBeanID.setCodIdentArquivo("PNT0110"); /*recebe j_funcao=PNT0110=EMITEPONTO*/
				ArquivoBeanID.buscarListaArquivosRecebidos();
			}	

			if(acao.equals("ImprimeArquivo")) nextRetorno=imprimeArquivo(req,ArquivoBeanID,UsrLogado);

			session.setAttribute("ArquivoBeanID", ArquivoBeanID);			
		}
		catch (Exception ue) {
			throw new sys.CommandException("RelatArqNotifCmd "+ ue.getMessage());
		}
		return nextRetorno;
	}

	protected String imprimeArquivo(HttpServletRequest req,ArquivoBean myArquivo,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno=jspPadrao;		
	 	try { 		
	 		// ler parametros com codigo do arquivo
 			String ordem   = req.getParameter("ordem");
 			if (ordem==null) ordem = "1";
			  
 			myArquivo.setCodArquivo(req.getParameter("codArquivo"));
 			myArquivo.buscarArquivo();
 			myArquivo.buscarListaProcessos(ordem);
 			String sOrdem = "ALFABETICA";
			if ("1".equals(ordem)) sOrdem = "PROCESSO" ;
			if ("2".equals(ordem)) sOrdem = "CNH" ;
			if ("3".equals(ordem)) sOrdem = "CEP" ;
			if ("4".equals(ordem)) sOrdem = "CPF" ;			
 			
 			OrgaoBean myOrg = new OrgaoBean(); 			
 			myOrg.Le_Orgao(myArquivo.getCodOrgao(),0);	 		
			String tituloConsulta = "PROCESSOS DE PONTUA��O - "+myArquivo.getDatGeracao() +
			  " - ORG�O: "+myOrg.getSigOrgao()+" - ORDEM: "+sOrdem;
			req.setAttribute("tituloConsulta", tituloConsulta);
			nextRetorno="/PNT/NOT/RelatArqNotifImp.jsp";	 		
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("RelatArqNotifCmd - imprimeArquivo: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}	
}