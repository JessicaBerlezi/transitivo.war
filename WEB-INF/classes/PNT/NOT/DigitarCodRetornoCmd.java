package PNT.NOT;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.NotificacaoBean;
import PNT.TAB.CodigoRetornoBean;
import sys.BeanException;
import sys.CommandException;
import java.util.ArrayList;
import java.util.List;

public class DigitarCodRetornoCmd extends sys.Command {
	private String next;   
	private static final String jspPadrao="/PNT/NOT/DigitarCodRetorno.jsp" ;
	
	
	public DigitarCodRetornoCmd() {
		next = jspPadrao;
	}
	
	public DigitarCodRetornoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;      	
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null) UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
	
			CodigoRetornoBean CodigoRetornoBeanID = (CodigoRetornoBean)session.getAttribute("CodigoRetornoBeanID") ;
			if (CodigoRetornoBeanID==null)  CodigoRetornoBeanID = new CodigoRetornoBean() ;
			
			String acao    = req.getParameter("acao");
			if (acao==null)	{
				session.removeAttribute("RetornoNotificacaoBeanID") ;
				acao = "";
			}
			RetornoNotificacaoBean RetornoNotificacaoBeanID = (RetornoNotificacaoBean)session.getAttribute("RetornoNotificacaoBeanID");
			if (RetornoNotificacaoBeanID==null)  RetornoNotificacaoBeanID = new RetornoNotificacaoBean();

			if  (acao.equals("")) {
				nextRetorno="/PNT/NOT/DigitarCodRetorno.jsp";			  			 
			}

			if (acao.equals("GravarRetorno"))  
			{
				try {
					nextRetorno="/PNT/NOT/DigitarCodRetorno.jsp";
					if (GravarRetornoNotificacoes(req,RetornoNotificacaoBeanID,UsrLogado))
					{
						RetornoNotificacaoBeanID = new RetornoNotificacaoBean();
						RetornoNotificacaoBeanID.setMsgErro("Atualiza��o do retorno das notifica��es feita com sucesso.");
					}
				}
				catch (Exception ue) {
					throw new sys.CommandException("DigitarCodRetornoCmd: " + ue.getMessage());
				}				
			}

			// processamento de saida dos formularios
			if (acao.equals("R")){
				session.removeAttribute("RetornoNotificacaoBeanID") ;
			}
			else {
				session.setAttribute("RetornoNotificacaoBeanID",RetornoNotificacaoBeanID) ;
			}

		}
		catch (Exception ue) {
			throw new sys.CommandException("DigitarCodRetornoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public boolean GravarRetornoNotificacoes(HttpServletRequest req,RetornoNotificacaoBean RetornoNotificacaoBeanID,UsuarioBean UsrLogado) throws Exception {
		RetornoNotificacaoBeanID.setDatRetorno(req.getParameter("datRetorno"));
		RetornoNotificacaoBeanID.setTotalNotificacoes(req.getParameter("qtdNotif"));
		RetornoNotificacaoBeanID.getRetorno().setNumRetorno(req.getParameter("codRetorno"));
		RetornoNotificacaoBeanID.getRetorno().LeRetorno();

		String numNotificacao[] = req.getParameterValues("numNotificacao");
		if (numNotificacao==null) numNotificacao = new String[0];
		
		VerificarNotificacao(numNotificacao,RetornoNotificacaoBeanID);
		if (RetornoNotificacaoBeanID.getMsgErro().length()>0)
		  return false;

		for (int i=0;i<RetornoNotificacaoBeanID.getListaNotificacoes().size();i++)
		{
			NotificacaoBean myNotif = new NotificacaoBean();
			LocalizarNotificacao(RetornoNotificacaoBeanID.getListaNotificacoes().get(i),myNotif);
			myNotif.getRetorno().setNumRetorno(RetornoNotificacaoBeanID.getRetorno().getNumRetorno());
			myNotif.getRetorno().setIndEntregue(RetornoNotificacaoBeanID.getRetorno().getIndEntregue());
			myNotif.setDatRetorno(RetornoNotificacaoBeanID.getDatRetorno());
			myNotif.setNomUserNameRetorno(UsrLogado.getNomUserName());			
			myNotif.setCodOrgaoLotacaoRetorno(UsrLogado.getOrgao().getCodOrgao());
			myNotif.setDatProcRetorno(sys.Util.formatedToday().substring(0,10));
			
			PNT.ProcessoBean myProc = new PNT.ProcessoBean();
			myProc.setCodProcesso(myNotif.getCodProcesso());
			myProc.LeProcesso();

			/*Autua��o*/
			if (myNotif.getTipoNotificacao().equals("0"))
			{
				if ("000,001,002".indexOf(myProc.getCodStatus())>=0)
				{
					if (RetornoNotificacaoBeanID.getRetorno().isEntregue())
						myProc.setCodStatus("004");
					else
						myProc.setCodStatus("002");
				}
			}
			/*Penalidade*/
			if (myNotif.getTipoNotificacao().equals("2"))
			{			
				if ("030,031,032".indexOf(myProc.getCodStatus())>=0)
				{
					if (RetornoNotificacaoBeanID.getRetorno().isEntregue())
						myProc.setCodStatus("034");
					else
						myProc.setCodStatus("032");
				}
			}
			/*Entrega CNH*/
			if (myNotif.getTipoNotificacao().equals("4"))
			{			
				if ("090,091,092".indexOf(myProc.getCodStatus())>=0)
				{
					if (RetornoNotificacaoBeanID.getRetorno().isEntregue())
						myProc.setCodStatus("094");
					else
						myProc.setCodStatus("092");
				}
			}
			myProc.setDatStatus(sys.Util.formatedToday().substring(0,10));
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodProcesso(myProc.getCodProcesso());   
			myHis.setNumProcesso(myProc.getNumProcesso()); 
			myHis.setCodStatus(myProc.getCodStatus());  
			myHis.setDatStatus(myProc.getDatStatus());
			myHis.setCodOrigemEvento("101");
			if (myNotif.getTipoNotificacao().equals("0")) myHis.setCodEvento("808");
			if (myNotif.getTipoNotificacao().equals("2")) myHis.setCodEvento("838");
			if (myNotif.getTipoNotificacao().equals("4")) myHis.setCodEvento("898");
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			//Num Notificacao
			myHis.setTxtComplemento01(myNotif.getNumNotificacao()+"-"+myNotif.getSeqNotificacao());
			// Codigo Retorno
			myHis.setTxtComplemento02(RetornoNotificacaoBeanID.getRetorno().getNumRetorno());
			// Data Retorno
			myHis.setTxtComplemento03(RetornoNotificacaoBeanID.getDatRetorno());
			// Ind Entrega
			myHis.setTxtComplemento04(RetornoNotificacaoBeanID.getRetorno().getIndEntregue());
			Dao dao = DaoFactory.getInstance();
			dao.RetornoNotificacao(myProc,myNotif,myHis);		    			
		}
		return true;		
	}

	public void VerificarNotificacao(String[] numNotificacao,RetornoNotificacaoBean RetornoNotificacaoBeanID) throws BeanException, CommandException
	{
		try
		{
			
			String sErro="";
			List <String>ListNotifs = new ArrayList<String>();
			for (int j=0; j < numNotificacao.length; j++) 
			{
				String sNumNotificacao = numNotificacao[j];
				if (sNumNotificacao.trim().length()==0) continue;
				
				PNT.NotificacaoBean myNotif = new PNT.NotificacaoBean();
				PNT.ProcessoBean myProc = new PNT.ProcessoBean();
				
				int iPos1=sNumNotificacao.indexOf('-');				
				if (iPos1<0)
					sErro+="Formato da notifica��o inv�lida. "+sNumNotificacao+" - Seq:"+(j+1)+"\n"; 
				else
				{
					LocalizarNotificacao(sNumNotificacao,myNotif);
					if (myNotif.getNumNotificacao().equals("0"))
						sErro+="N�mero da Notifica��o n�o cadastrada. "+sNumNotificacao+" - Seq:"+(j+1)+"\n";
					myProc.setCodProcesso(myNotif.getCodProcesso());
					myProc.LeProcesso();
					/*Autua��o*/
					if (myNotif.getTipoNotificacao().equals("0"))
					{
					  if ("001,002,003,004,010,011,012,013".indexOf(myProc.getCodStatus())<0)
						sErro+="Notifica��o com status imcompat�vel. "+sNumNotificacao+" - Seq:"+(j+1)+"\n";
					}
					/*Penalidade*/
					if (myNotif.getTipoNotificacao().equals("2"))
					{
					  if ("001,002,003,004,010,011,012,013,031,032,033,034,040,041,042,043,090,091,092,093,094,".indexOf(myProc.getCodStatus())<0)
						sErro+="Notifica��o com status imcompat�vel. "+sNumNotificacao+" - Seq:"+(j+1)+"\n";
					}
					/*Entrega CNH*/
					if (myNotif.getTipoNotificacao().equals("4"))
					{
					  if ("001,002,003,004,010,011,012,013,031,032,033,034,040,041,042,043,090,091,092,093,094,100".indexOf(myProc.getCodStatus())<0)
						sErro+="Notifica��o com status imcompat�vel. "+sNumNotificacao+" - Seq:"+(j+1)+"\n";
					}
				}
				ListNotifs.add(sNumNotificacao);
			}
			RetornoNotificacaoBeanID.setListaNotificacoes(ListNotifs);
			RetornoNotificacaoBeanID.setMsgErro(sErro);
		}
		catch (Exception e) {
			throw new sys.CommandException("DigitarCodRetornoCmd-VerificarNotificacao: " + e.getMessage());			
		}
	}

	public void LocalizarNotificacao(String numNotificacao,NotificacaoBean myNotif) throws BeanException
	{
		int iPos1=numNotificacao.indexOf('-'),iPos2=0;
		if (iPos1>0) iPos2=numNotificacao.length();

		String sPedaco1=numNotificacao.substring(0,iPos1);
		sPedaco1 = "000000000"+sPedaco1;
		sPedaco1 = sPedaco1.substring(sPedaco1.length()-9,sPedaco1.length());
		String sPedaco2=numNotificacao.substring(iPos1+1,iPos2);
		sPedaco2 = "000"+sPedaco2;
		sPedaco2 = sPedaco2.substring(sPedaco2.length()-3,sPedaco2.length());
		myNotif.setNumNotificacao(sPedaco1);
		myNotif.setSeqNotificacao(sPedaco2);
		myNotif.LerNotificacao("notificacao");
	}
	

}

