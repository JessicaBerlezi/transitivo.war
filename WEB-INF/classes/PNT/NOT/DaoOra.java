package PNT.NOT;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import PNT.ProcessoBean;
import sys.BeanException;
import sys.DaoException;


/**
 * <b>Title:</b>       	SMIT - PNT.NOT.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados de Notificações<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoOra extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoOra() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public String buscaProtocolo(ArquivoBean auxClasse,ACSS.UsuarioBean UsrLogado) throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			String nSeq = "";
			return nSeq;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	public void arquivoInsertHeader(ArquivoBean myArq) throws DaoException {
		
	}
	public void arquivoInsertProcesso(ArquivoBean myArq) throws DaoException {
	}
	public void arquivoUpdateStatus(ArquivoBean myArq) throws DaoException {
	}
	public void buscarArquivoRecebido(ArquivoBean myArq) throws DaoException {
		
	}
	public void buscarArquivosRecebidos(ArquivoBean myArq, String status, Connection conn){
		
	}
	public void buscarListaArquivosRecebidos(ArquivoBean myArquivo)	throws DaoException {
		
	}
	
	public void buscarListaProcessos(ArquivoBean myArquivo, String ordem) throws DaoException {	
 		
	}
	public void buscarListaNotificacao(ArquivoBean myArquivo, String ordem) throws DaoException {
		
	}
	public void buscarListaCEP(ArquivoBean myArquivo) throws DaoException {
		
	}
	
	public void arquivoUpdate(ArquivoBean auxClasse, Connection conn) throws DaoException {
		
	}
	public List carregarNotifs(ArquivoBean myArq,Connection conn) throws DaoException {
		List lNotifs = new ArrayList();
		return lNotifs;
	}
	public void carregarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn) throws DaoException {
		
	}
	public void atualizarLinhasArquivo(ArquivoBean myArq, String myNotif, Connection conn,String myStatus) throws DaoException {
		
	}
	public boolean isNotificacaoExiste(String myNotif,Connection conn) throws DaoException {
		boolean Existe=false;
		try {
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return Existe;	
	}

	public String buscarSeqImpressao() throws BeanException	{
		return "0";
	}
	
	public void RetornoNotificacao(PNT.ProcessoBean myProc,PNT.NotificacaoBean myNotif,PNT.HistoricoBean myHis) throws DaoException {
		
	}

	public void LeRetornos(ResumoRetornoBean ResumoRetornoBeanId, String sTipoDt) throws DaoException {
		
	}

	public void AplicarPenalidade(PNT.ProcessoBean myProc,PNT.HistoricoBean myHis) throws DaoException {
		
	}
	
	public String BuscaNumProcesso(String numNotificacao, String seqNotificacao) throws DaoException {	
		return "";
	}
	
	public String BuscaARDigitalizado(String numNotificacao, String seqNotificacao) throws DaoException {
		return "";		
	}
	
	public void buscarArquivosaGerar(ArquivoBean myArq, Connection conn) throws DaoException {
		// TODO Auto-generated method stub
		
	}
	public void UpdateEmitePontoVex(ArquivoBean auxClasse, Connection conn) throws DaoException {
		// TODO Auto-generated method stub
		
	}
	public void buscarListaNotificaçõesPor(ProcessoBean myProcesso)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
	public String BuscaNumProcessoPorNotificacao(String numNotificacao)
			throws DaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}