package PNT.NOT;

import sys.Util;
/**
 * <b>Title:</b>       	SMIT - PNT.NOT.LinhaArquivoBean<br>
 * <b>Description:</b> 	Bean de Linha de Arquivo Recebido via Upload<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class ArquivoLinhaEmitePontoBean extends sys.HtmlPopupBean { 
	
	private String codLinhaArquivo;
	private String codArquivo;
	private String tipLinha;
	private String numSeqLinha;
	private String dscLinhaArquivo;
	private String codStatus; // 0 - Recebida, 1 - Processada, 5 - Ignorada, 7 - Cancelada, 8 - Corrigida, 9 - Com Erro	
	private String dscLinhaRetorno;
	private String codRetorno;

	private String numProcesso;
	private String numNotificacao;
	private String seqNotificacao;
	
	// Registro tipo 2 - Processo
	private String datEmissao;
	private String totPontos;
	private String qtdProcAnteriores;
	private String qtdMultas;
	private String nomResponsavel;
	private String numCPF;
	private String tipCNH;
	private String numCNH;
	private String ufCNH;
	private String datEmissaoCNH;
	private String datValidadeCNH;
	private String categoriaCNH;
	private String endereco;
	private String cep;
	private String municipio;
	private String uf;
	private String tipReemissao;
	private String tipNotificacao;
	private String tipProcesso;	
	private String ultDatDefesa;
	private String ultDatEntregaCNH;	
	// Registro tipo 3 - Processos Anteriores
	private String numProcessoAnt;	
	private String datProcessoAnt;
	private String tipProcessoAnt;
	private String totPntProcessoAnt;
	private String sitProcessoAnt;	
	// Registro tipo 4 - Multas	
	private String codOrgao;	
	private String numAuto;
	private String numPlaca;
	private String codInfracao;	
	private String datInfracao;
	private String horInfracao;
	private String locInfracao;
	private String dscInfracao;
	private String enquadramento;
	private String pontuacao;
	private String datTrasJulg;
	
	private String datProc; 
	private String nomUsername;
	private String codOrgaoLotacao;
	
	
	public ArquivoLinhaEmitePontoBean() {		
		super();
		setTabela("TPNT_ARQUIVO_LINHA");	
		setPopupWidth(35);
		
		codLinhaArquivo  = "0";
		codArquivo       = "0";
		tipLinha         = "";
		numSeqLinha      = "0";
		dscLinhaArquivo  = "";
		codStatus        = "0";
		dscLinhaRetorno  = "";
		codRetorno       = "";
		numProcesso      = "";
		numNotificacao   = "";
		seqNotificacao   = "";
		
		// Reg 2
		datEmissao        = "";
		totPontos         = "0";
		qtdProcAnteriores = "0";
		qtdMultas         = "0";
		nomResponsavel    = "";
		numCPF            = "";
		tipCNH            = "";
		numCNH            = "";
		ufCNH             = "";
		datEmissaoCNH     = "";
		datValidadeCNH    = "";
		categoriaCNH      = "";
		endereco          = "";
		cep               = "";
		municipio         = "";
		uf                = "";
		tipReemissao      = "";
		tipNotificacao    = "";
		tipProcesso       = "";		
		ultDatDefesa      = "";
		ultDatEntregaCNH  = "";	

		// Reg 3
		numProcessoAnt    = "";	
		datProcessoAnt    = "";
		tipProcessoAnt    = "";
		totPntProcessoAnt = "0";
		sitProcessoAnt    = "";		
		// Reg 4
		codOrgao          = "";
		numAuto           = "";
		numPlaca          = "";
		codInfracao       = "";	
		datInfracao       = "";
		horInfracao       = "";
		locInfracao       = "";
		dscInfracao       = "";
		enquadramento     = "";
		pontuacao         = "0";
		datTrasJulg       = "";	
	}
	
	public void setCodLinhaArquivo(String codLinhaArquivo)  {
		if (codLinhaArquivo==null) codLinhaArquivo="0";
		this.codLinhaArquivo=codLinhaArquivo;
	}  	
	public String getCodLinhaArquivo()  {
		return this.codLinhaArquivo;
	}
	public void setCodArquivo(String codArquivo)  {
		if (codArquivo==null) codArquivo="0";
		this.codArquivo=codArquivo;
	}  	
	public String getCodArquivo()  {
		return this.codArquivo;
	}
	public void setTipLinha(String tipLinha)  {
		if (tipLinha==null) tipLinha="";		
		this.tipLinha=tipLinha;
	}  
	public String getTipLinha()  {
		return this.tipLinha;
	}	
	public void setNumSeqLinha(String numSeqLinha)  {
		if (numSeqLinha==null) this.numSeqLinha="0";
		this.numSeqLinha=numSeqLinha;
	}  
	public String getNumSeqLinha()  {
		return this.numSeqLinha;
	}
	public void setDscLinhaArquivo(String dscLinhaArquivo)  {
		if (dscLinhaArquivo==null) dscLinhaArquivo="";
		dscLinhaArquivo=dscLinhaArquivo.replaceAll("'", " ");
		dscLinhaArquivo=dscLinhaArquivo.replaceAll("\"", " ");
		this.dscLinhaArquivo = dscLinhaArquivo;	
	}  	
	public String getDscLinhaArquivo()  {
		return this.dscLinhaArquivo;
	}
	public void setCodStatus(String codStatus)  {
		if (codStatus==null) codStatus="9";
		//		 0 - Recebida, 1 - Processada, 5 - Ignorada, 7 - Cancelada, 8 - Corrigida, 9 - Com Erro		
		if ("015789".indexOf(codStatus)<0) codStatus="9";
		this.codStatus=codStatus;
	}	
	public String getCodStatus()  {
		return this.codStatus;
	}	
	public void setDscLinhaRetorno(String dscLinhaRetorno)  {
		if (dscLinhaRetorno==null) dscLinhaRetorno="";
		this.dscLinhaRetorno=dscLinhaRetorno;
	}  
	public String getDscLinhaRetorno()  {
		return this.dscLinhaRetorno;
	}
	public void setCodRetorno(String codRetorno)  {
		if (codRetorno==null) codRetorno="";
		this.codRetorno=codRetorno;
	}  
	public String getCodRetorno()  {
		return this.codRetorno;
	}
	public void setNumProcesso(String numProcesso)  {
		if (numProcesso==null) numProcesso="";
		this.numProcesso = numProcesso;
	}  
	public String getNumProcesso()  {
		return this.numProcesso;
	}	
	public void setNumNotificacao(String numNotificacao)  {
		if (numNotificacao==null) numNotificacao="";
		this.numNotificacao = numNotificacao;  
	}  
	public String getNumNotificacao()  {
		return this.numNotificacao;
	}
	public void setSeqNotificacao(String seqNotificacao)  {
		if (seqNotificacao==null) seqNotificacao="";
		this.seqNotificacao = seqNotificacao;  
	}  
	public String getSeqNotificacao()  {
		return this.seqNotificacao;
	}
	public void setRegistro()  {
		if ("2".equals(getTipLinha())) setReg2();
		if ("3".equals(getTipLinha())) setReg3();
		if ("4".equals(getTipLinha())) setReg4();
		return;
	}	

	//   Reg 2	
	public void setReg2()  {
		if ("2".equals(getTipLinha())==false) return ;
		if (getDscLinhaArquivo().length()<272)   return;
		try {				
			
			setDatEmissao(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(30, 38)));
			setTotPontos(getDscLinhaArquivo().substring(38, 43));
			setQtdProcAnteriores(getDscLinhaArquivo().substring(43, 48));
			setQtdMultas(getDscLinhaArquivo().substring(48, 53));
			setNomResponsavel(getDscLinhaArquivo().substring(53, 98));
			setNumCPF(getDscLinhaArquivo().substring(98,109));				
			setTipCNH(getDscLinhaArquivo().substring(109,110));
			setNumCNH(getDscLinhaArquivo().substring(110,121));
			setUfCNH(getDscLinhaArquivo().substring(121,123));
			setDatEmissaoCNH(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(123,131)));
			setDatValidadeCNH(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(131,139)));
			setCategoriaCNH(getDscLinhaArquivo().substring(139,141));
			setEndereco(getDscLinhaArquivo().substring(141,201));
			setCep(getDscLinhaArquivo().substring(201,209));
			setMunicipio(getDscLinhaArquivo().substring(209,249));
			setUf(getDscLinhaArquivo().substring(249,251));
			setTipReemissao(getDscLinhaArquivo().substring(251,252));
			setTipNotificacao(getDscLinhaArquivo().substring(252,253));
			setTipProcesso(getDscLinhaArquivo().substring(253,254));
			setUltDatDefesa(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(254,262)));
			setUltDatEntregaCNH(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(262,270)));
			
		} catch (Exception e) {
		}							
	}  
	public void setDatEmissao(String datEmissao)  {
		if (datEmissao==null) datEmissao="";
		this.datEmissao = datEmissao;  
	}  
	public String getDatEmissao()  {
		return this.datEmissao;
	}
	public void setTotPontos(String totPontos)  {
		if (totPontos==null) totPontos="0";
		this.totPontos = totPontos;  
	}  
	public String getTotPontos()  {
		return this.totPontos;
	}
	public void setQtdProcAnteriores(String qtdProcAnteriores)  {
		if (qtdProcAnteriores==null) qtdProcAnteriores="0";
		this.qtdProcAnteriores = qtdProcAnteriores;  
	}  
	public String getQtdProcAnteriores()  {
		return this.qtdProcAnteriores;
	}
	public void setQtdMultas(String qtdMultas)  {
		if (qtdMultas==null) qtdMultas="0";
		this.qtdMultas = qtdMultas;  
	}  
	public String getQtdMultas()  {
		return this.qtdMultas;
	}
	public void setNomResponsavel(String nomResponsavel)  {
		if (nomResponsavel==null) nomResponsavel="";
		this.nomResponsavel = nomResponsavel;  
	}  
	public String getNomResponsavel()  {
		return this.nomResponsavel;
	}
	public void setNumCPF(String numCPF)  {
		if (numCPF==null) numCPF="";
		this.numCPF = numCPF;  
	}  
	public String getNumCPF()  {
		return this.numCPF;
	}
	public void setTipCNH(String tipCNH)  {
		if (tipCNH==null) tipCNH="";
		this.tipCNH = tipCNH;  
	}  
	public String getTipCNH()  {
		return this.tipCNH;
	}
	public void setNumCNH(String numCNH)  {
		if (numCNH==null) numCNH="";
		this.numCNH = numCNH;  
	}  
	public String getNumCNH()  {
		return this.numCNH;
	}
	public void setUfCNH(String ufCNH)  {
		if (ufCNH==null) ufCNH="";
		this.ufCNH = ufCNH;  
	}  
	public String getUfCNH()  {
		return this.ufCNH;
	}
	public void setDatEmissaoCNH(String datEmissaoCNH)  {
		if (datEmissaoCNH==null) datEmissaoCNH="";
		this.datEmissaoCNH = datEmissaoCNH;  
	}  
	public String getDatEmissaoCNH()  {
		return this.datEmissaoCNH;
	}
	public void setDatValidadeCNH(String datValidadeCNH)  {
		if (datValidadeCNH==null) datValidadeCNH="";
		this.datValidadeCNH = datValidadeCNH;  
	}  
	public String getDatValidadeCNH()  {
		return this.datValidadeCNH;
	}
	public void setCategoriaCNH(String categoriaCNH)  {
		if (categoriaCNH==null) categoriaCNH="";
		this.categoriaCNH = categoriaCNH;  
	}  
	public String getCategoriaCNH()  {
		return this.categoriaCNH;
	}
	public void setEndereco(String endereco)  {
		if (endereco==null) endereco="";
		this.endereco = endereco;  
	}  
	public String getEndereco()  {
		return this.endereco;
	}
	public void setCep(String cep)  {
		if (cep==null) cep="";
		this.cep = cep;  
	}  
	public String getCep()  {
		return this.cep;
	}
	public void setMunicipio(String municipio)  {
		if (municipio==null) municipio="";
		this.municipio = municipio;  
	}  
	public String getMunicipio()  {
		return this.municipio;
	}
	public void setUf(String uf)  {
		if (uf==null) uf="";
		this.uf = uf;  
	}  
	public String getUf()  {
		return this.uf;
	}
	public void setTipReemissao(String tipReemissao)  {
		if (tipReemissao==null) tipReemissao="";
		this.tipReemissao = tipReemissao;  
	}  
	public String getTipReemissao()  {
		return this.tipReemissao;
	}
	public void setTipNotificacao(String tipNotificacao)  {
		if (tipNotificacao==null) tipNotificacao="";
		this.tipNotificacao = tipNotificacao;  
	}  
	public String getTipNotificacao()  {
		return this.tipNotificacao;
	}
	public void setTipProcesso(String tipProcesso)  {
		if (tipProcesso==null) tipProcesso="";
		this.tipProcesso = tipProcesso;  
	}  
	public String getTipProcesso()  {
		return this.tipProcesso;
	}
	public void setUltDatDefesa(String ultDatDefesa)  {
		if (ultDatDefesa==null) ultDatDefesa="";
		this.ultDatDefesa = ultDatDefesa;  
	}  
	public String getUltDatDefesa()  {
		return this.ultDatDefesa;
	}
	public void setUltDatEntregaCNH(String ultDatEntregaCNH)  {
		if (ultDatEntregaCNH==null) ultDatEntregaCNH="";
		this.ultDatEntregaCNH = ultDatEntregaCNH;  
	}  
	public String getUltDatEntregaCNH()  {
		return this.ultDatEntregaCNH;
	}
	//	 Reg 3
	public void setReg3()  {
		if ("3".equals(getTipLinha())==false) return ;
		if (getDscLinhaArquivo().length()<87)   return;
		try {						
			setNumProcessoAnt(getDscLinhaArquivo().substring(30, 50));
			setDatProcessoAnt(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(50, 58)));
			setTipProcessoAnt(getDscLinhaArquivo().substring(58, 59));
			setTotPntProcessoAnt(getDscLinhaArquivo().substring(59, 64));
			setSitProcessoAnt(getDscLinhaArquivo().substring(64, 84));			
		} catch (Exception e) {
		}							
	}  
	public void setNumProcessoAnt(String numProcessoAnt)  {
		if (numProcessoAnt==null) numProcessoAnt="";
		this.numProcessoAnt = numProcessoAnt;  
	}  
	public String getNumProcessoAnt()  {
		return this.numProcessoAnt;
	}
	public void setDatProcessoAnt(String datProcessoAnt)  {
		if (datProcessoAnt==null) datProcessoAnt="0";
		this.datProcessoAnt = datProcessoAnt;  
	}  
	public String getDatProcessoAnt()  {
		return this.datProcessoAnt;
	}
	public void setTipProcessoAnt(String tipProcessoAnt)  {
		if (tipProcessoAnt==null) tipProcessoAnt="0";
		this.tipProcessoAnt = tipProcessoAnt;  
	}  
	public String getTipProcessoAnt()  {
		return this.tipProcessoAnt;
	}
	public void setTotPntProcessoAnt(String totPntProcessoAnt)  {
		if (totPntProcessoAnt==null) totPntProcessoAnt="0";
		this.totPntProcessoAnt = totPntProcessoAnt;  
	}  
	public String getTotPntProcessoAnt()  {
		return this.totPntProcessoAnt;
	}
	public void setSitProcessoAnt(String sitProcessoAnt)  {
		if (sitProcessoAnt==null) sitProcessoAnt="0";
		this.sitProcessoAnt = sitProcessoAnt;  
	}  
	public String getSitProcessoAnt()  {
		return this.sitProcessoAnt;
	}
				
			
	//	 Reg 4	
	public void setReg4()  {
		if ("4".equals(getTipLinha())==false) return ;
		if (getDscLinhaArquivo().length()<214)   return;
		try {						
			setCodOrgao(getDscLinhaArquivo().substring(30, 36));
			setNumAuto(getDscLinhaArquivo().substring(36, 48));
			setNumPlaca(getDscLinhaArquivo().substring(48, 55));
			setCodInfracao(getDscLinhaArquivo().substring(55, 59));
			setDatInfracao(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(59, 67)));			
			setHorInfracao(Util.lPad(getDscLinhaArquivo().substring(67, 71),"0",4));
			setLocInfracao(getDscLinhaArquivo().substring(71, 116));
			setDscInfracao(getDscLinhaArquivo().substring(116,176));
			setEnquadramento(getDscLinhaArquivo().substring(176,201));
			setPontuacao(getDscLinhaArquivo().substring(201,203));			
			setDatTrasJulg(Util.formataDataDDMMYYYY(getDscLinhaArquivo().substring(203,211)));
		} catch (Exception e) {
		}							
	}  
	public void setCodOrgao(String codOrgao)  {
		if (codOrgao==null) codOrgao="";
		this.codOrgao = codOrgao;  
	}  
	public String getCodOrgao()  {
		return this.codOrgao;
	}
	public void setNumAuto(String numAuto)  {
		if (numAuto==null) numAuto="";
		this.numAuto = numAuto;  
	}  
	public String getNumAuto()  {
		return this.numAuto;
	}
	public void setNumPlaca(String numPlaca)  {
		if (numPlaca==null) numPlaca="";
		this.numPlaca = numPlaca;  
	}  
	public String getNumPlaca()  {
		return this.numPlaca;
	}
	public void setCodInfracao(String codInfracao)  {
		if (codInfracao==null) codInfracao="";
		this.codInfracao = codInfracao;  
	}  
	public String getCodInfracao()  {
		return this.codInfracao;
	}
	public void setDatInfracao(String datInfracao)  {
		if (datInfracao==null) datInfracao="";
		this.datInfracao = datInfracao;  
	}  
	public String getDatInfracao()  {
		return this.datInfracao;
	}
	public void setHorInfracao(String horInfracao)  {
		if (horInfracao==null) horInfracao="";
		this.horInfracao = horInfracao;  
	}  
	public String getHorInfracao()  {
		return this.horInfracao;
	}
	public void setLocInfracao(String locInfracao)  {
		if (locInfracao==null) locInfracao="";
		this.locInfracao = locInfracao;  
	}  
	public String getLocInfracao()  {
		return this.locInfracao;
	}
	public void setDscInfracao(String dscInfracao)  {
		if (dscInfracao==null) dscInfracao="";
		this.dscInfracao = dscInfracao;  
	}  
	public String getDscInfracao()  {
		return this.dscInfracao;
	}
	public void setEnquadramento(String enquadramento)  {
		if (enquadramento==null) enquadramento="";
		this.enquadramento = enquadramento;  
	}  
	public String getEnquadramento()  {
		return this.enquadramento;
	}
	public void setPontuacao(String pontuacao)  {
		if (pontuacao==null) pontuacao="0";
		this.pontuacao = pontuacao;  
	}  
	public String getPontuacao()  {
		return this.pontuacao;
	}
	public void setDatTrasJulg(String datTrasJulg)  {
		if (datTrasJulg==null) datTrasJulg="0";
		this.datTrasJulg = datTrasJulg;  
	}  
	public String getDatTrasJulg()  {
		return this.datTrasJulg;
	}

	public String getCodOrgaoLotacao() {
		return codOrgaoLotacao;
	}

	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		if (codOrgaoLotacao==null) codOrgaoLotacao="";
		this.codOrgaoLotacao = codOrgaoLotacao;
	}

	public String getDatProc() {
		return datProc;
	}

	public void setDatProc(String datProc) {
		if (datProc==null) datProc="";
		this.datProc = datProc;
	}

	public String getNomUsername() {
		return nomUsername;
	}

	public void setNomUsername(String nomUsername) {
		if (nomUsername==null) nomUsername="";
		this.nomUsername = nomUsername;
	}	
}