package PNT.NOT;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */
public class ResumoRetornoCmd  extends sys.Command {
	private static final String jspPadrao="/PNT/NOT/ResumoRetorno.jsp";    
	private String next;
	
	public ResumoRetornoCmd() {
		next = jspPadrao;
	}
	
	public ResumoRetornoCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ResumoRetornoBean ResumoRetornoBeanId = (ResumoRetornoBean)session.getAttribute("ResumoRetornoBeanId");
			if (ResumoRetornoBeanId==null)  ResumoRetornoBeanId = new ResumoRetornoBean();
			ResumoRetornoBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			ResumoRetornoBeanId.setColunaValue("ANO");
			ResumoRetornoBeanId.setPopupNome("numAno");
			String sCmd = null;
			String sTipoDt="E";
			if (sSigFuncao.equals("PNT0150"))
			  sCmd = "ANO, SELECT DISTINCT(TO_CHAR(DAT_EMISSAO,'YYYY')) AS ANO "+
			  "FROM TPNT_NOTIFICACAO ORDER BY ANO ";
			else
			{
				  sTipoDt="R";
				  sCmd = "ANO, SELECT DISTINCT(TO_CHAR(DAT_RETORNO,'YYYY')) AS ANO "+
				  "FROM TPNT_NOTIFICACAO WHERE DAT_RETORNO IS NOT NULL ORDER BY ANO ";
			}
			ResumoRetornoBeanId.setPopupString(sCmd);
			if  (acao.equals("consultarAno"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.LeRetornos(sTipoDt);
				if (sSigFuncao.equals("PNT0150"))
				  nextRetorno = "/PNT/NOT/ResumoRetornoAnoEnv.jsp";
				else
				  nextRetorno = "/PNT/NOT/ResumoRetornoAnoRet.jsp";
					
			}
			if  (acao.equals("imprimirAno"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.LeRetornos(sTipoDt);
				String tituloConsulta = null;
				if (sSigFuncao.equals("PNT0150"))
				{
				  tituloConsulta = "RESUMO RETORNO P/ DATA DE ENVIO - ANO : "+
				  ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+ResumoRetornoBeanId.getDatReferencia();
				  nextRetorno = "/PNT/NOT/ResumoRetornoAnoEnvImp.jsp";
				}
				else
				{
				  tituloConsulta = "RESUMO RETORNO P/ DATA DE RETORNO - ANO : "+
				  ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+ResumoRetornoBeanId.getDatReferencia();
				  nextRetorno = "/PNT/NOT/ResumoRetornoAnoRetImp.jsp";
				}
					
				req.setAttribute("tituloConsulta", tituloConsulta);
				
			}
			session.setAttribute("ResumoRetornoBeanId",ResumoRetornoBeanId) ;  
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("ResumoRetornoCmd: " + ue.getMessage());
		}
	}
}
