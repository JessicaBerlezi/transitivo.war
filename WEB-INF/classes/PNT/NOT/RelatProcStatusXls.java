package PNT.NOT;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelatProcStatusXls {
	
	public void write(PNT.PaginacaoBean PaginacaoBeanId, String titulo, String arquivo,HttpServletRequest req) throws IOException, WriteException
	{   HttpSession session   = req.getSession() ;
	
	ACSS.ParamSistemaBean ParamSistemaBeanId = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
	if (ParamSistemaBeanId == null)	ParamSistemaBeanId = new ACSS.ParamSistemaBean();
	
	
	int iContador=1;
	int cont = 6;	
	int celulaTitulo = 0;
	
	try
	{
		
		WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
		WritableSheet sheet = workbook.createSheet("Proc Status", 0);
		WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
				WritableFont.DEFAULT_POINT_SIZE,
				WritableFont.BOLD,
				false,
				UnderlineStyle.NO_UNDERLINE,
				Colour.BLACK);
		
		WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
		WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
		formatoTituloCampos.setAlignment(Alignment.CENTRE);
		formatoTituloCampos.setBackground(Colour.GRAY_25);
		formatoTitulo.setAlignment(Alignment.CENTRE);
		
		celulaTitulo = 7/2;
		
		Label label = new Label(celulaTitulo, 0, "PROCESSOS POR STATUS", formatoTitulo);
		sheet.addCell(label);
		
		label = new Label(celulaTitulo, 1, titulo, formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 2, "       ", formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 3, "       ", formatoTitulo);
		sheet.addCell(label);
		label = new Label(celulaTitulo, 4, "       ", formatoTitulo);
		sheet.addCell(label);

		//T�tulo do Relat�rio				
		label = new Label(0,cont, "SEQ     ",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(0, 8);							

		label = new Label(1,cont, "PROCESSO            ",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(1, 20);							
		
		label = new Label(2,cont, "DAT. PROC.",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(2, 13);							

		label = new Label(3,cont, "NOME CONDUTOR                                ",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(3, 45);							
		
		
		label = new Label(4,cont, "CPF            ",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(4, 15);							
		
		label = new Label(5,cont, "CNH                 ",formatoTituloCampos);
		sheet.addCell(label);
		sheet.setColumnView(5, 20);							
		
		
		
		cont++;

	    int max = PaginacaoBeanId.getDados().size() ;
		if (max>0) { 
		
			for(int i=0;i<max;i++){
				PNT.ProcessoBean myCont = (PNT.ProcessoBean) PaginacaoBeanId.getDados().get(i);

				//Linhas	do Relat�rio				   
				
				label = new Label(0, cont, String.valueOf(iContador)); 
				sheet.addCell(label);

				label = new Label(1, cont, myCont.getNumProcesso()); 
				sheet.addCell(label);
			
				label = new Label(2, cont, myCont.getDatProcesso()); 
				sheet.addCell(label);

				label = new Label(3, cont, myCont.getNomResponsavel()); 
				sheet.addCell(label);
				
				label = new Label(4, cont, myCont.getNumCPFEdt()); 
				sheet.addCell(label);

				label = new Label(5, cont, myCont.getTipCNHDesc() + " " + myCont.getNumCNH()+ " " + myCont.getUfCNH()); 
				sheet.addCell(label);
				
				cont++;
				iContador++;					 
				
			}		
		}		
		
		workbook.write(); 
		workbook.close();
		
		
	}catch (Exception e) {	
		throw new IOException("GeraXlsRelatProcStatus: " + e.getMessage());	}
	
	}
	
}
