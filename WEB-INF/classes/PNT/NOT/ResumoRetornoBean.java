package PNT.NOT;

import java.util.ArrayList;
import java.util.List;

import PNT.TAB.CodigoRetornoBean;


public class ResumoRetornoBean extends sys.HtmlPopupBean {
	
	private CodigoRetornoBean retorno;
	private String datInicio;
	private String datFim;
	private String mes;
	private String ano;
	private String datReferencia;
	private String qtdTotalEnviada;
	private String qtdTotalRetornada;
	private String qtdTotalnaoRetornada;	
	
	private List<ResumoRetornoBean> ListRetornoMes;
	private List<ResumoRetornoBean> ListCodigoRetornoMes;
	
	public ResumoRetornoBean() throws sys.BeanException {
		super();
		retorno               = new CodigoRetornoBean();
		datInicio             = "";
		datFim                = "";
		mes                   = "";
		ano                   = "";
		datReferencia         = sys.Util.formatedToday().substring(0,10);
		qtdTotalEnviada       = "0";
		qtdTotalRetornada     = "0";
		qtdTotalnaoRetornada  = "0";
		ListCodigoRetornoMes  = new ArrayList<ResumoRetornoBean>(); 
		ListRetornoMes        = new ArrayList<ResumoRetornoBean>(); 
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		if (ano==null) ano="";
		this.ano = ano;
	}

	public String getDatReferencia() {
		return datReferencia;
	}

	public void setDatReferencia(String datReferencia) {
		if (datReferencia==null) datReferencia="";
		this.datReferencia = datReferencia;
	}

	public List getListRetornoMes() {
		return ListRetornoMes;
	}

	public void setListRetornoMes(List<ResumoRetornoBean> listRetornoMes) {
		if (listRetornoMes==null) listRetornoMes=new ArrayList<ResumoRetornoBean>();
		ListRetornoMes = listRetornoMes;
	}

	public ResumoRetornoBean getRetornos(Integer i) 	{
		return this.ListRetornoMes.get(i);
	}

	public List<ResumoRetornoBean> getListCodigoRetornoMes() {
		return ListCodigoRetornoMes;
	}

	public void setListCodigoRetornoMes(List<ResumoRetornoBean> listCodigoRetornoMes) {
		if (listCodigoRetornoMes==null) listCodigoRetornoMes=new ArrayList<ResumoRetornoBean>();
		ListCodigoRetornoMes = listCodigoRetornoMes;
	}

	public ResumoRetornoBean getCodigoRetornos(Integer i) 	{
		return this.ListCodigoRetornoMes.get(i);
	}
	
	public String getQtdTotalEnviada() {
		return qtdTotalEnviada;
	}

	public void setQtdTotalEnviada(String qtdTotalEnviada) {
		if (qtdTotalEnviada==null) qtdTotalEnviada="0";
		this.qtdTotalEnviada = qtdTotalEnviada;
	}

	public String getQtdTotalnaoRetornada() {
		return qtdTotalnaoRetornada;
	}

	public void setQtdTotalnaoRetornada(String qtdTotalnaoRetornada) {
		if (qtdTotalnaoRetornada==null) qtdTotalnaoRetornada="0";
		this.qtdTotalnaoRetornada = qtdTotalnaoRetornada;
	}

	public String getQtdTotalRetornada() {
		return qtdTotalRetornada;
	}

	public void setQtdTotalRetornada(String qtdTotalRetornada) {
		if (qtdTotalRetornada==null) qtdTotalRetornada="0";
		this.qtdTotalRetornada = qtdTotalRetornada;
	}
	
	public void LeRetornos(String sTipoDt) throws sys.DaoException {
		try {
			Dao dao = DaoFactory.getInstance();
			dao.LeRetornos(this,sTipoDt);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}

	public String getDatFim() {
		return datFim;
	}

	public void setDatFim(String datFim) {
		if (datFim==null) datFim="";
		this.datFim = datFim;
	}

	public String getDatInicio() {
		return datInicio;
	}

	public void setDatInicio(String datInicio) {
		if (datInicio==null) datInicio="";
		this.datInicio = datInicio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		if (mes==null) mes="";
		this.mes = mes;
	}

    public void setRetorno(CodigoRetornoBean retorno)   throws sys.BeanException  {
        this.retorno=retorno ;
        if (retorno==null) this.retorno= new CodigoRetornoBean() ;
    }  
    public CodigoRetornoBean getRetorno()  {
        return this.retorno;
    }
	
}