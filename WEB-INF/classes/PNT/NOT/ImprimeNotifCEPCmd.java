package PNT.NOT;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;
import PNT.NotificacaoBean;
import PNT.ProcessoBean;
import sys.BeanException;
import sys.CommandException;
import sys.Util;

/**
* <b>Title:</b>        Pontuacao - Imprime Notificacoes<br>
* <b>Description:</b>  Informa os arquivos de Notificacoes enviados. <br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      MIND - Informatica <br>
* @author Luciana Rocha
* @version 1.0
*/


public class ImprimeNotifCEPCmd extends sys.Command {

	private static final String jspPadrao  = "/PNT/NOT/ImprimeNotif.jsp";
	private String next;

	public ImprimeNotifCEPCmd() {
		next = jspPadrao;
	}

	public ImprimeNotifCEPCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req ) throws CommandException {
		String nextRetorno = next;
		try {
		    // cria os Beans de sessao, se n�o existir
		    HttpSession session   = req.getSession() ;								
		    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
		  	// cria os Beans, se n�o existir
		    ArquivoBean ArquivoBeanID     = (ArquivoBean)session.getAttribute("ArquivoBeanID") ;
			if (ArquivoBeanID==null)  ArquivoBeanID = new ArquivoBean() ;	  			
		    
			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			if(acao.equals("")) {
				ArquivoBeanID.setCodIdentArquivo("PNT0110"); /*recebe j_funcao=PNT0110=EMITEPONTO*/
				ArquivoBeanID.buscarListaArquivosRecebidos();
			}	

			if(acao.equals("ImprimirNotificacao")) nextRetorno=imprimeNotificacao(req,ArquivoBeanID,UsrLogado);
			if(acao.equals("ConsultaCEP"))         nextRetorno=consultaCEP(req,ArquivoBeanID,UsrLogado);
			if(acao.equals("ImprimeCEP"))          nextRetorno=imprimeCEP(req,ArquivoBeanID,UsrLogado);

			session.setAttribute("ArquivoBeanID", ArquivoBeanID);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ImprimeNotifCmd "+ ue.getMessage());
		}
		return nextRetorno;
	}

	
	protected String imprimeNotificacao(HttpServletRequest req,ArquivoBean myArquivo,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno="/PNT/NOT/ImprimeNotifImp.jsp";		
	 	try { 		

 			String codArquivo   = req.getParameter("codArquivo");
 			if (codArquivo==null) codArquivo = "";
	 		
 			String ordemImpressao   = req.getParameter("ordem");
 			if (ordemImpressao==null) ordemImpressao = "1";

 			String tipoImpressao   = req.getParameter("tipImpressao");
 			if (tipoImpressao==null) tipoImpressao = "I";

	 		myArquivo.setCodArquivo(codArquivo);
	 		myArquivo.setTipImpressao(tipoImpressao);
 			myArquivo.buscarListaNotificacoes(ordemImpressao);
 			atualizaImpressaoNotificacao(myArquivo,UsrLogado);
			nextRetorno="/PNT/NOT/ImprimeNotifImp.jsp";
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("RelatArqNotifCmd - imprimeArquivo: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}	
	
	protected String consultaCEP(HttpServletRequest req,ArquivoBean myArquivo,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno="/PNT/NOT/ImprimeNotifCep.jsp";		
	 	try { 		

 			String codArquivo   = req.getParameter("codArquivo");
 			if (codArquivo==null) codArquivo = "";
	 		
 			String ordemImpressao   = req.getParameter("ordem");
 			if (ordemImpressao==null) ordemImpressao = "1";

 			String tipoImpressao   = req.getParameter("tipImpressao");
 			if (tipoImpressao==null) tipoImpressao = "I";

	 		myArquivo.setCodArquivo(codArquivo);
	 		myArquivo.setTipImpressao(tipoImpressao);
 			myArquivo.buscarListaCEP();
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("RelatArqNotifCmd - imprimeArquivo: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}	
	
	protected String imprimeCEP(HttpServletRequest req,ArquivoBean myArquivo,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	 	String nextRetorno="/PNT/NOT/ImprimeNotifImp.jsp";		
	 	try { 		

 			String ordemImpressao   = req.getParameter("ordem");
 			if (ordemImpressao==null) ordemImpressao = "1";
	 		
	 		String seqSelecionado[] = req.getParameterValues("selCep");
			if (seqSelecionado==null) seqSelecionado = new String[0];

 			String numCepSelecionado[] = req.getParameterValues("numCep");
			if (numCepSelecionado==null) numCepSelecionado = new String[0];
			
			String sListaCep    ="";
			for (int j=0; j <seqSelecionado.length; j++) 
			{
				int indice=Integer.parseInt(seqSelecionado[j]);
				if (j==0)
					sListaCep="('"+numCepSelecionado[indice]+"'";
				else
					sListaCep+=",'"+numCepSelecionado[indice]+"'";					
			}
			sListaCep+=")";
			myArquivo.setListaCep(sListaCep);
 			myArquivo.buscarListaNotificacoes(ordemImpressao);
 			atualizaImpressaoNotificacao(myArquivo,UsrLogado);
			nextRetorno="/PNT/NOT/ImprimeNotifImp.jsp";
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("RelatArqNotifCmd - imprimeArquivo: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}	
	
	public void atualizaImpressaoNotificacao(ArquivoBean myArquivo,UsuarioBean UsrLogado) throws BeanException {
		try{
			ProcessoBean ProcessoBeanId  = new ProcessoBean();					
			NotificacaoBean notificacao  = new NotificacaoBean();
			
			
			int iSeqNotificacao=0;
			for (int p=0;p<myArquivo.getListaProcessos().size();p++) { 
				ProcessoBeanId = myArquivo.getListaProcessos().get(p);
				for (int n=0;n<ProcessoBeanId.getNotificacoes().size();n++) { 
					notificacao  = ProcessoBeanId.getNotificacoes().get(n);
					if (myArquivo.getTipImpressao().equals("I"))
					{
					  notificacao.setDatImpressao(Util.formatedToday().substring(0,10));						
					  notificacao.setSeqImpressao(myArquivo.buscarSeqImpressao());
					  notificacao.setSeqNotificacaoImpressao(String.valueOf(iSeqNotificacao));
					}
					notificacao.setNomUserNameImpressao(UsrLogado.getNomUserName());
					notificacao.setCodOrgaoLotacaoImpressao(UsrLogado.getOrgao().getCodOrgao());
					notificacao.setDatProcImpressao(Util.formatedToday().substring(0,10));
					notificacao.AtualizarNotificacao();
					iSeqNotificacao++;
				}
			}
		}
		catch (Exception e) {	
			throw new BeanException(e.getMessage());
		}		
	}
	
}




