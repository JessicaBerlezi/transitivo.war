package PNT.AJT;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;


public class RevisaoDeAtoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/AJT/RevisaoDeAto.jsp" ;  
	
	public RevisaoDeAtoCmd() {
		next = jspPadrao;
	}
	
	public RevisaoDeAtoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
    
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			ParamSistemaBean parSis               = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis            = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 

			PNT.TAB.StatusBean StatusId   = (PNT.TAB.StatusBean)req.getAttribute("StatusId") ;
			if (StatusId==null)   	StatusId = new PNT.TAB.StatusBean() ;	  			  				   

			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  			
		    if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					setEventoOK(ProcessoBeanId);
					ProcessoBeanId.setProximoStatus(defineProximoStatus(ProcessoBeanId.getCodStatus()));
					req.setAttribute("StatusId",StatusId);
					session.setAttribute("ProcessoBeanId",ProcessoBeanId);
					nextRetorno = next;						
				}				
			}
							
			if  (acao.equals("RevisaoDeAto"))  {
				processaRevisaoDeAto(req,parSis,ProcessoBeanId,UsrLogado,StatusId);
				req.setAttribute("StatusId",StatusId);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId);
			}
			
			if (acao.equals("R"))	{
			    nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}			
		}
		catch (Exception ue) {
			throw new sys.CommandException("RevisaoDeAtoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}	
	
	
	private String defineProximoStatus(String codStatusProcesso) {
		
		String proximoStatus = "902"; //Cancelado por revis�o de ato
		
		if (codStatusProcesso.equals("995")){ //Deferimento de defesa
			proximoStatus = "019"; //Defesa de pontua��o indeferida
		} else if (codStatusProcesso.equals("996")){ //Deferimento de recurso
			proximoStatus = "049"; //Recurso de pontua��o indeferido
		} else if (codStatusProcesso.equals("994")){ //Deferimento em 2� inst�ncia
			proximoStatus = "079"; //Recurso de pontua��o indeferido
		} 
		
		return proximoStatus;
	}

	public void setEventoOK(ProcessoBean myProcesso)  throws sys.BeanException  {
		try {
			if ("".equals(myProcesso.getNomStatus())) {
				myProcesso.setMsgOk("Status: "+myProcesso.getCodStatus()+" n�o cadastrado na Tabela.");				
				myProcesso.setEventoOK("N");
			}
			else {
				myProcesso.setMsgOk(myProcesso.getNomStatus()+" (Data do Status: "+myProcesso.getDatStatus()+") ");
				myProcesso.setEventoOK("S");
			}
			
		} catch (Exception ue) {
			throw new sys.BeanException("FuncionalidadesBean: EventoOK " + ue.getMessage());
		}	
	}	
	
	protected void processaRevisaoDeAto(HttpServletRequest req,ParamSistemaBean parSis,ProcessoBean myProcesso,UsuarioBean UsrLogado,PNT.TAB.StatusBean StatusId) throws sys.CommandException 
	{
		Vector<String> vErro = new Vector<String>() ;		
		try {
			String dataStatus = req.getParameter("datStatus");
			if (dataStatus==null) dataStatus = "";
			if (dataStatus.length()==0) vErro.addElement("Data para o Novo Status n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),dataStatus)>0) vErro.addElement("Data do Status n�o pode ser superior a hoje. \n") ;
			
			String txtMotivo = req.getParameter("txtMotivo");
			
			if (vErro.size()>0) {
				myProcesso.setMsgErro(vErro) ;	
			}   
			else {		
				PNT.AJT.Dao dao = PNT.AJT.DaoFactory.getInstance();
				dao.revisaoDeAto(myProcesso,parSis,UsrLogado, txtMotivo);
				
				if (myProcesso.getMsgErro().length()==0){
					myProcesso.setMsgErro("Status alterado para o Processo: "+myProcesso.getNumProcesso()+" em "+myProcesso.getDatStatus() 
							+ "\nStatus: " + myProcesso.getProximoStatus() + " - "+ StatusId.DscStatus(myProcesso.getProximoStatus()));
				}
			}
		}
		catch (Exception e){
			throw new sys.CommandException("AjustaStatusCmd: " + e.getMessage());
		}
		return;
    }
}