package PNT.AJT;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

import ACSS.ParamSistemaBean;
import PNT.ProcessoBean;


public class CancSuspProcessoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/AJT/CancSuspProcesso.jsp" ;  
	
	public CancSuspProcessoCmd() {
		next = jspPadrao;
	}
	
	public CancSuspProcessoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
    
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			ParamSistemaBean parSis               = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis            = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 

			PNT.TAB.StatusBean StatusId   = (PNT.TAB.StatusBean)req.getAttribute("StatusId") ;
			if (StatusId==null)   	StatusId = new PNT.TAB.StatusBean() ;	  			  				   

			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  			
		    if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					setEventoOK(ProcessoBeanId);
					req.setAttribute("StatusId",StatusId) ;					
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;				
					nextRetorno = next;						
				}				
			}
							
			if  (acao.equals("CancSuspProcesso"))  {
				processaAjuste(req,parSis,ProcessoBeanId,UsrLogado,StatusId);
				req.setAttribute("StatusId",StatusId) ;
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			}
			if (acao.equals("R"))	{
			    nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}			
		}
		catch (Exception ue) {
			throw new sys.CommandException("CancSuspProcessoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}	
	
	
	public void setEventoOK(ProcessoBean myProcesso)  throws sys.BeanException  {
		try {
			if ("".equals(myProcesso.getNomStatus())) {
				myProcesso.setMsgOk("Status: "+myProcesso.getCodStatus()+" n�o cadastrado na Tabela.");				
				myProcesso.setEventoOK("N");
			}
			else {
				myProcesso.setMsgOk(myProcesso.getNomStatus()+" (Data do Status: "+myProcesso.getDatStatus()+") ");
				myProcesso.setEventoOK("S");
			}
			
		} catch (Exception ue) {
			throw new sys.BeanException("FuncionalidadesBean: EventoOK " + ue.getMessage());
		}	
	}	
	
	protected void processaAjuste(HttpServletRequest req,ParamSistemaBean parSis,ProcessoBean myProcesso,ACSS.UsuarioBean UsrLogado,PNT.TAB.StatusBean StatusId) throws sys.CommandException 
	{
		Vector<String> vErro = new Vector<String>() ;		
		try {

			
			String codMotivo = req.getParameter("codMotivo");
			if (codMotivo==null) codMotivo="";
			if( codMotivo.compareTo("") == 0)
				vErro.addElement("C�digo do Cancelamento n�o Informado. \n") ;
			
			String dscMotivo = req.getParameter("dscMotivo");
			if (dscMotivo==null) dscMotivo="";
			if( dscMotivo.compareTo("") == 0)
				vErro.addElement("Descricao do Cancelamento n�o Informado. \n") ;
			
			
			
			String data = req.getParameter("Data");
			if (data==null) data = "" ;
			
			String indOper = req.getParameter("IndicOper");
					if (indOper==null) indOper = "" ;
			
			String numProcesso = req.getParameter("ProcInv");
			if (numProcesso==null) numProcesso = "";
			
			//validar data de Notificacao		
			if (data.length()==0) vErro.addElement("Data do Cancelamento n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),data)>0) vErro.addElement("Data do Cancelamento n�o pode ser superior a hoje. \n") ;
			
			if (vErro.size()>0) {
				myProcesso.setMsgErro(vErro) ;	
			}   
			else {		
				
				PNT.HistoricoBean myHis = new PNT.HistoricoBean();
				myHis.setCodProcesso(myProcesso.getCodProcesso());   
				myHis.setNumProcesso(myProcesso.getNumProcesso()); 
				myHis.setCodStatus(myProcesso.getCodStatus());  
				myHis.setDatStatus(myProcesso.getDatStatus());
				myHis.setCodOrigemEvento("101");			
				myHis.setCodEvento("680");
				myHis.setNomUserName(UsrLogado.getNomUserName());
				myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
				myHis.setTxtComplemento01(codMotivo);
				myHis.setTxtComplemento02(numProcesso);				
				myHis.setTxtComplemento03(indOper);
				myHis.setTxtComplemento04(dscMotivo);
				PNT.AJT.Dao dao = PNT.AJT.DaoFactory.getInstance();
				dao.CancSuspProcesso(myProcesso,myHis,parSis) ;		    			
				if (myProcesso.getMsgErro().length()==0) {  			
					myProcesso.setMsgErro("Status alterado para o Processo: "+myProcesso.getNumProcesso()+" em "+myProcesso.getDatStatus() + "\nStatus: " + myProcesso.getCodStatus() + " - "+ myProcesso.getNomStatus()) ;
				}
			}
		}
		catch (Exception e){
			throw new sys.CommandException("AjustaStatusCmd: " + e.getMessage());
		}
		return;
    }
}