package PNT.AJT;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;

import sys.DaoException;
import sys.Util;

/**
 * <b>Title:</b>       	SMIT - PNT.PTC.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados nas funcionalidades do Protocolo<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoAdabas extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoAdabas() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public void AjusteStatus(ProcessoBean myProcesso,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {			
			UsuarioBean myUsuario= new UsuarioBean();
			parteVar = sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.lPad(myHis.getTxtComplemento01(),"0",3) + //myHis.getTxtComplemento01(),"0",3 
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()),"0",8) +  //myHis.getTxtComplemento02(),"0",8 
			sys.Util.rPad(myHis.getTxtComplemento03()," ",50)  +//myHis.getTxtComplemento03(),"0",50 
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000);//myHis.getTxtComplemento04(),"0",1000 		
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AtualizarProcesso: "+e.getMessage());
		}		
	}

	public void revisaoDeAto(ProcessoBean myProcesso,ParamSistemaBean parSis, UsuarioBean UsrLogado, String txtMotivo) throws DaoException {		
		String resultado ="";
		String parteVar = "";
		try {			
			UsuarioBean myUsuario= new UsuarioBean();
			
			PNT.HistoricoBean myHis = new PNT.HistoricoBean();
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento("101");			
			myHis.setCodEvento("961");
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			myHis.setTxtComplemento03("");
			myHis.setTxtComplemento04(txtMotivo);
			
			parteVar = sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000);//myHis.getTxtComplemento04(),"0",1000 		
			
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AtualizarProcesso: "+e.getMessage());
		}		
	}
	
	public void CancSuspProcesso(ProcessoBean myProcesso,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {			
			UsuarioBean myUsuario= new UsuarioBean();
			parteVar = sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.lPad(myHis.getTxtComplemento01(),"0",3) +  
			sys.Util.rPad(myHis.getTxtComplemento02()," ",20) +  
			sys.Util.lPad(myHis.getTxtComplemento03(),"0",1) +   
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000);   
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AtualizarProcesso: "+e.getMessage());
		}		
	}
	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myProc,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk    = false ;
		 String codErro = resultado.substring(0,3);
		 try {
			 if ((codErro.equals("000")==false) &&
					 (codErro.equals("037")==false)) {
				 if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					 myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 codErro=Util.lPad(codErro.trim(),"0",3);
					 myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					 ResultSet rs   = stmt.executeQuery(sCmd) ;
					 while (rs.next()) {
						 myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					 }
					 rs.close();
				 }
			 }
			 else   bOk = true ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
}