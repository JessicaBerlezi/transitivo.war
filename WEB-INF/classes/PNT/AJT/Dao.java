package PNT.AJT;

import sys.DaoException;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;
import PNT.HistoricoBean;
public interface Dao {

	public abstract void AjusteStatus(ProcessoBean myProcesso,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException;
	public abstract void CancSuspProcesso(ProcessoBean myProcesso,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException;
	public void revisaoDeAto(ProcessoBean myProcesso,ParamSistemaBean parSis, UsuarioBean UsrLogado, String txtMotivo) throws DaoException;
}