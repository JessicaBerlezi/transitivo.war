package PNT;

import sys.Util;
import java.util.Vector;
import java.sql.Connection;
/**
 * <b>Title:</b>        SMIT - Pontua��o <br>
 * <b>Description:</b>  Notificacao Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class MultasBean   { 
		
	private String codMultas;
	private String numNotificacao;
	private String seqNotificacao;
	private String seqMultaNotificacao;
	private String codProcesso;
	private String numProcesso;
	private String datProcesso;
	
	private String codOrgao;
	private String sigOrgao;
	private String numAutoInfracao;
	private String numPlaca;
	private String codInfracao;
	private String datInfracao  ;
	private String valHorInfracao;
	private String dscLocalInfracao;
    private String dscInfracao;
    private String dscEnquadramento;
    private String numPonto;
	private String datTransJulgado;

	private String nomUserName;  
	private String codOrgaoLotacao;
	private String datProc;
	
	private String msgErro ; 
	private String msgOk ;  
	private String eventoOK;

	
	public MultasBean()  throws sys.BeanException {
				
		codMultas             = "";
		numNotificacao        = "";
		seqNotificacao        = "001";

		seqMultaNotificacao   = "";
		codProcesso           = "";
		numProcesso           = "";  
		datProcesso           = "";
		
		codOrgao              = "";
		sigOrgao              = "";
		numAutoInfracao       = "";
		numPlaca              = "";
		codInfracao           = "";
		datInfracao           = "";
		valHorInfracao        = "";
		dscLocalInfracao      = "";
	    dscInfracao           = "";
	    dscEnquadramento      = "";
	    numPonto              = "";
		datTransJulgado       = "";
		

		nomUserName     = "";    
		codOrgaoLotacao = "";
		datProc         = "";
		msgErro         = "" ;
		msgOk           = "" ;
		eventoOK        = "" ;

	}
	public void setCodMultas(String codMultas)  {
		if (codMultas==null) codMultas= "";
		this.codMultas=codMultas ;
	}  
	public String getCodMultas()  {
		return this.codMultas;
	}	
	public void setNumNotificacao(String numNotificacao)  {
		if (numNotificacao==null) numNotificacao= "";
		this.numNotificacao=numNotificacao ;
	}  
	public String getNumNotificacao()  {
		return this.numNotificacao;
	}	
	public void setSeqNotificacao(String seqNotificacao)  {
		if (seqNotificacao==null) seqNotificacao= "001";
		try { Integer.parseInt(seqNotificacao) ;		}
		catch (Exception e) {seqNotificacao= "001"; }
		this.seqNotificacao=seqNotificacao ;
	}  
	public String getSeqNotificacao()  {
		String seq = "";
		if (this.seqNotificacao.length()>0) {
			seq = "000"+this.seqNotificacao;
			seq = seq.substring(seq.length()-3,seq.length());
		}
		return seq;
	}

	public void setSeqMultaNotificacao(String seqMultaNotificacao)  {
		if (seqMultaNotificacao==null) seqMultaNotificacao= "001";
		try {	 Integer.parseInt(seqMultaNotificacao) ;		}
		catch (Exception e) {seqMultaNotificacao= "001"; }
		this.seqMultaNotificacao=seqMultaNotificacao ;
	}  
	public String getSeqMultaNotificacao()  {
		String seq = "";
		if (this.seqMultaNotificacao.length()>0) {
			seq = "000"+this.seqMultaNotificacao;
			seq = seq.substring(seq.length()-3,seq.length());
		}
		return seq;

	}
	public void setCodProcesso(String codProcesso)  {
		if (codProcesso==null) codProcesso= "";
		this.codProcesso=codProcesso ;
	}  
	public String getCodProcesso()  {
		return this.codProcesso;
	}	
	public void setNumProcesso(String numProcesso)  {
		if (numProcesso==null) numProcesso= "";
		this.numProcesso=numProcesso ;
	}  
	public String getNumProcesso()  {
		return this.numProcesso;
	}
	public void setDatProcesso(String datProcesso)  {
		if (datProcesso==null) datProcesso= "";
		if (datProcesso.equals("00000000")) datProcesso= "";
		if (datProcesso.equals("        ")) datProcesso= "";		
		this.datProcesso=datProcesso ;
	}  
	public String getDatProcesso()  {
		return this.datProcesso;
	}

	public void setCodOrgao(String codOrgao)  {
		if (codOrgao==null) codOrgao= "";
		this.codOrgao=codOrgao ;
	}  
	public String getCodOrgao()  {
		return this.codOrgao;
	}
	public void setSigOrgao(String sigOrgao)  {
		if (sigOrgao==null) sigOrgao= "";
		this.sigOrgao=sigOrgao ;
	}  
	public String getSigOrgao()  {
		return this.sigOrgao;
	}
	public void setNumAutoInfracao(String numAutoInfracao)  {
		if (numAutoInfracao==null) numAutoInfracao= "";
		this.numAutoInfracao=numAutoInfracao ;
	}  
	public String getNumAutoInfracao()  {
		return this.numAutoInfracao;
	}
	public void setNumAutoInfracaoEdt(String numAutoInfracao)  {
		if (numAutoInfracao==null)      numAutoInfracao = "";
		if (numAutoInfracao.length()<15) numAutoInfracao += "               ";
		this.numAutoInfracao=numAutoInfracao.substring(0,1)+numAutoInfracao.substring(4,15) ;
	}  
	public String getNumAutoInfracaoEdt()  {
		if ("".equals(this.numAutoInfracao)) return this.numAutoInfracao ;
		this.numAutoInfracao=Util.rPad(this.numAutoInfracao," ",12);
		return this.numAutoInfracao.substring(0,1)+" - "+this.numAutoInfracao.substring(1,12);
	}
	public void setNumPlaca(String numPlaca)  {
		if (numPlaca==null) numPlaca= "";
		this.numPlaca=numPlaca ;
	}  
	public String getNumPlaca()  {
		return this.numPlaca;
	}
	public void setNumPlacaEdt(String numPlaca)  {
		if (numPlaca==null)      numPlaca = "";
		if (numPlaca.length()<8) numPlaca += "        ";
		this.numPlaca=numPlaca.substring(0,3)+numPlaca.substring(4,8) ;
	}  
	public String getNumPlacaEdt()  {
		if ("".equals(this.numPlaca)) return this.numPlaca ;
		return this.numPlaca.substring(0,3)+" "+this.numPlaca.substring(3,7);
	}
	public void setCodInfracao(String codInfracao)  {
		if (codInfracao==null) codInfracao="";
		this.codInfracao = codInfracao;  
	}  
	public String getCodInfracao()  {
		return this.codInfracao;
	}
	public void setDatInfracao(String datInfracao)  {
		if (datInfracao==null) datInfracao= "";
		if (datInfracao.equals("00000000")) datInfracao= "";
		if (datInfracao.equals("        ")) datInfracao= "";		
		this.datInfracao=datInfracao ;
	}  
	public String getDatInfracao()  {
		return this.datInfracao;
	}
	public void setValHorInfracao(String valHorInfracao)  {
		if (valHorInfracao==null) valHorInfracao= "";
		this.valHorInfracao=valHorInfracao.trim() ;
	}  
	public String getValHorInfracao()  {
		return this.valHorInfracao;
	}
	public String getValHorInfracaoEdt()  {
		if ("".equals(this.valHorInfracao)) return this.valHorInfracao;  
		String h = "0000"+this.valHorInfracao;
		return h.substring(h.length()-4,h.length()-2)+":"+h.substring(h.length()-2,h.length());
	}	
	public void setDscLocalInfracao(String dscLocalInfracao)  {
		if (dscLocalInfracao==null) dscLocalInfracao= "";
		this.dscLocalInfracao=dscLocalInfracao ;
	}  
	public String getDscLocalInfracao()  {
		return this.dscLocalInfracao;
	}
    public void setDscInfracao(String dscInfracao) {
        if (dscInfracao == null) dscInfracao = "";
    	this.dscInfracao = dscInfracao;
    }
    public String getDscInfracao() {
        return this.dscInfracao;
    }
    public void setDscEnquadramento(String dscEnquadramento) {
        if (dscEnquadramento == null) dscEnquadramento = "";
    	this.dscEnquadramento = dscEnquadramento;
    }
    public String getDscEnquadramento() {
        return this.dscEnquadramento;
    }
    public void setNumPonto(String numPonto) {
        if (numPonto == null) numPonto = "0";
    	this.numPonto = numPonto;
    }
    public String getNumPonto() {
        return this.numPonto;
    }
	public void setDatTransJulgado(String datTransJulgado)  {
		if (datTransJulgado==null) datTransJulgado= "";
		if (datTransJulgado.equals("00000000")) datTransJulgado= "";
		if (datTransJulgado.equals("        ")) datTransJulgado= "";		
		this.datTransJulgado=datTransJulgado ;  
	}  
	public String getDatTransJulgado()  {
		return this.datTransJulgado;
	}		
	//--------------------------------------------------------------------------
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j) ;
		}
	}
	public void setNomUserName(String nomUserName)  {
		if (nomUserName==null) nomUserName= "";
		this.nomUserName=nomUserName ;
	}  
	public String getNomUserName()  {
		return this.nomUserName;
	}
	public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
		if (codOrgaoLotacao==null) codOrgaoLotacao= "";
		this.codOrgaoLotacao=codOrgaoLotacao ;
	}  
	public String getCodOrgaoLotacao()  {
		return this.codOrgaoLotacao;
	}
	public String getDatProc()  {
		return this.datProc;
	}
	public void setDatProc(String datProc)  {
		if (datProc==null) datProc= "";
		if (datProc.equals("00000000")) datProc= "";
		if (datProc.equals("        ")) datProc= "";		
		this.datProc=datProc ;  
	}  
	public void setMsgErro(String msgErro)   {
		if (msgErro==null) msgErro="";
		this.msgErro = msgErro ;
	}   
	public String getMsgErro()   {
		return this.msgErro;
	}     
	public void setMsgOk(String msgOk)   {
		if (msgOk==null) msgOk="";   	
		this.msgOk = msgOk ;
	}   
	public String getMsgOk()   {
		return this.msgOk;
	}       
	public void setEventoOK(String eventoOK)  {
		if (eventoOK==null) eventoOK= "";
		this.eventoOK=eventoOK ;
	}  
	public String getEventoOK()  {
		return this.eventoOK;
	}  
	
	public void GravaMulta(Connection conn) throws sys.BeanException {
		try {
				DaoBroker dao = DaoBrokerFactory.getInstance();
				dao.GravaMulta(this,conn);		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}		
	}	
	
	
}