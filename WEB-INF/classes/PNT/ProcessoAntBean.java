package PNT;

import java.util.Vector;
import java.sql.Connection;

/**
 * <b>Title:</b>        SMIT - Pontua��o <br>
 * <b>Description:</b>  Processo Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class ProcessoAntBean   { 
		
	private String codProcesso;
	private String codProcessoAnt;	
	private String numProcessoAnt;
	private String datProcessoAnt;
	private String tipProcessoAnt;
	private String sitProcessoAnt;	
	private String totPntProcessoAnt;

	private String msgErro ; 
	private String msgOk ;  
	private String eventoOK;

	
	public ProcessoAntBean()  throws sys.BeanException {
				
		codProcesso           = "";
		codProcessoAnt        = "";
		numProcessoAnt        = "";  
		datProcessoAnt        = "";
		tipProcessoAnt        = "P";
		sitProcessoAnt        = "";		
		totPntProcessoAnt     = "0";

		msgErro         = "" ;
		msgOk           = "" ;
		eventoOK        = "" ;

	}

	public void setCodProcesso(String codProcesso)  {
		if (codProcesso==null) codProcesso= "";
		this.codProcesso=codProcesso ;
	}  
	public String getCodProcesso()  {
		return this.codProcesso;
	}	
	public void setCodProcessoAnt(String codProcessoAnt)  {
		if (codProcessoAnt==null) codProcessoAnt= "";
		this.codProcessoAnt=codProcessoAnt ;
	}  
	public String getCodProcessoAnt()  {
		return this.codProcessoAnt;
	}	

	public void setNumProcessoAnt(String numProcessoAnt)  {
		if (numProcessoAnt==null) numProcessoAnt= "";
		this.numProcessoAnt=numProcessoAnt ;
	}  
	public String getNumProcessoAnt()  {
		return this.numProcessoAnt;
	}
	public void setDatProcessoAnt(String datProcessoAnt)  {
		if (datProcessoAnt==null) datProcessoAnt= "";
		if (datProcessoAnt.equals("00000000")) datProcessoAnt= "";		
		if (datProcessoAnt.equals("        ")) datProcessoAnt= "";
		this.datProcessoAnt=datProcessoAnt ;
	}  
	public String getDatProcessoAnt()  {
		return this.datProcessoAnt;
	}
	public void setTipProcessoAnt(String tipProcessoAnt)  {
		if (tipProcessoAnt==null) tipProcessoAnt= "P";
		this.tipProcessoAnt=tipProcessoAnt ;
	}  
	public String getTipProcessoAnt()  {
		return this.tipProcessoAnt;
	}	
	public String getTipProcessoAntDesc()  {
		String desc = "Pontua��o";
		if (this.tipProcessoAnt.equals("S")) desc = "Suspens�o";
		return desc;
	}	
	public void setSitProcessoAnt(String sitProcessoAnt)  {
		if (sitProcessoAnt==null) sitProcessoAnt= "";
		this.sitProcessoAnt=sitProcessoAnt ;
	}  
	public String getSitProcessoAnt()  {
		return this.sitProcessoAnt;
	}	

	public void setTotPntProcessoAnt(String totPntProcessoAnt)  {
		if (totPntProcessoAnt==null) totPntProcessoAnt= "0";
		try { Integer.parseInt(totPntProcessoAnt) ;		}
		catch (Exception e) {totPntProcessoAnt= "0"; }
		this.totPntProcessoAnt=totPntProcessoAnt ;
	}  
	public String getTotPntProcessoAnt()  {
		return this.totPntProcessoAnt;
	}			
	public void GravaProcessoAnt(Connection conn)  throws sys.BeanException {
		try { 	 
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.GravaProcessoAnt(this,conn);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}
			
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j) ;
		}
	}  
	public void setMsgErro(String msgErro)   {
		if (msgErro==null) msgErro="";
		this.msgErro = msgErro ;
	}   
	public String getMsgErro()   {
		return this.msgErro;
	}     
	public void setMsgOk(String msgOk)   {
		if (msgOk==null) msgOk="";   	
		this.msgOk = msgOk ;
	}   
	public String getMsgOk()   {
		return this.msgOk;
	}       
	public void setEventoOK(String eventoOK)  {
		if (eventoOK==null) eventoOK= "";
		this.eventoOK=eventoOK ;
	}  
	public String getEventoOK()  {
		return this.eventoOK;
	}  

	  
}