package PNT.PTC;

import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import REC.ParamOrgBean;
public class CadastraProcCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/PNT/PTC/CadastraProc.jsp";  
   
  public CadastraProcCmd() {
    next = jspPadrao;
  }

  public CadastraProcCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    // cria os Beans, se n�o existir
	    String txtMotivo="";
	    
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	    
		ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
	    
	    ProcessoBean ProcessoBeanId   = (ProcessoBean)req.getAttribute("ProcessoBeanId") ;
	    if (ProcessoBeanId==null)  ProcessoBeanId = new ProcessoBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao"); 
	    
	    if ((acao==null) || ("".equals(acao)))	{
			ProcessoBeanId       = new ProcessoBean() ;	  		 
			session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			acao = "";
			req.setAttribute("statusIteracao",null);
		}				
	    
	    if("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
	   
	    if  (acao.equals("processoLe")) { 
			ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
			if (ProcessoBeanId.getCodProcesso().length()==0)  {
				// erro na leitura
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			}else{
				req.setAttribute("statusIteracao","1");
		    	nextRetorno = "/PNT/PTC/CadastraProc.jsp";
		    }
	    
	    }
	    if  (acao.equals("CadastraProc"))  {
			String dProc = req.getParameter("datprocesso");
			if (dProc==null) dProc="";
			String nProc = req.getParameter("numprocesso");
			if (nProc==null) nProc="";
			txtMotivo = req.getParameter("txtMotivo");
			if (txtMotivo==null) txtMotivo="";
	    	nextRetorno = TrocaProcesso(req,ProcessoBeanId,UsrLogado,txtMotivo,nProc,dProc);
	    }
	    if (acao.equals("imprimirInf")) {	
	    	txtMotivo = req.getParameter("txtMotivo")==null ? "" : req.getParameter("txtMotivo");
	    	nextRetorno = "/REC/CadProcImp.jsp" ;
	    }											
	    req.setAttribute("txtMotivo",txtMotivo) ;	    				
	    req.setAttribute("ProcessoBeanId",ProcessoBeanId) ;	    		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("CadastraProcCmd1: " + ue.getMessage());
    }
	return nextRetorno;
  }
  protected String TrocaProcesso(HttpServletRequest req,ProcessoBean myProcesso,
  ACSS.UsuarioBean UsrLogado,String txtMotivo,String nProc,String dProc) throws sys.CommandException { 
   	String nextRetorno = next ;
   	String procAntigo  = "" ;
   	String codProcAnt  = "" ;
   	try {
   		myProcesso.setMsgErro("") ;
  		Vector<String> vErro = new Vector<String>() ;
  		
  		//validar data de Notificacao
		if (dProc.length()==0) vErro.addElement("Data do Processo n�o preenchida. \n") ;
   		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),dProc)>0) vErro.addElement("Data do Processo n�o pode ser superior a hoje. \n") ;
   		if (sys.Util.DifereDias(dProc,myProcesso.getDatProcesso())>0) vErro.addElement("Data do Processo n�o pode ser anterior a data da Infra��o: "+myProcesso.getDatProcesso()+". \n") ;
   		if (txtMotivo.length()==0) vErro.addElement("Motivo n�o informado. \n") ;
  		if (vErro.size()==0) {
			// Atualizar status do Auto			

  			procAntigo=req.getParameter("numProcAnt").trim();
  			codProcAnt=req.getParameter("codProcAnt").trim();
  			myProcesso.setNumProcesso(procAntigo);
  			myProcesso.setCodProcesso(codProcAnt);
  			myProcesso.setDatStatus(sys.Util.formatedToday().substring(0,10));	
  			
  			
  			
	  		HistoricoBean myHis = new HistoricoBean();
	  		myHis.setCodProcesso(myProcesso.getCodProcesso());   
  			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
  			myHis.setNumProcesso(myProcesso.getNumProcesso());
	  		myHis.setCodStatus(myProcesso.getCodStatus());  
  			myHis.setDatStatus(myProcesso.getDatStatus());
  			myHis.setCodEvento("814");
	  		myHis.setCodOrigemEvento("101");		
  			myHis.setNomUserName(UsrLogado.getNomUserName());
  			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
	  		myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
	  		myHis.setTxtComplemento01(nProc);
	  		myHis.setTxtComplemento02(dProc);
	  		myHis.setTxtComplemento03(procAntigo);
	  		myHis.setTxtComplemento04(txtMotivo);									
	  		PNT.PTC.Dao dao = DaoFactory.getInstance();			
  			dao.CadastraNumProcesso(myProcesso,nProc,dProc,myHis);
  			if (myProcesso.getMsgErro().length() < 1){  
  				myProcesso.LeProcesso();
				myProcesso.setMsgOk("N") ;										
				myProcesso.setMsgErro("Numero do Processo alterado para : "+nProc) ;
				myProcesso.setDatProcesso(dProc);
				myProcesso.setNumProcesso(nProc);	
				req.setAttribute("statusIteracao",null);
				
  			}else{
  				myProcesso.LeProcesso();
  			}
  			
		}
		else {
			procAntigo=req.getParameter("numProcAnt").trim();
  			codProcAnt=req.getParameter("codProcAnt").trim();
  			myProcesso.setNumProcesso(procAntigo);
  			myProcesso.setCodProcesso(codProcAnt);
			myProcesso.LeProcesso();
			myProcesso.setMsgErro(vErro) ;
			req.setAttribute("ProcessoBeanId",myProcesso) ;
		}
   	}
    catch (Exception e){
  	    throw new sys.CommandException("CadastraProcCmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }  
 
}
