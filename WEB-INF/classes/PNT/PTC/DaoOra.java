package PNT.PTC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import sys.BeanException;
import sys.DaoException;


/**
 * <b>Title:</b>       	SMIT - PNT.NOT.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados de Notificações<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoOra extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoOra() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public void AbreDefesa(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException, BeanException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			return ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void EntregaCNH(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			return ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public boolean CadastraNumProcesso(ProcessoBean myProcesso, String ProcAtu,String dProc,HistoricoBean myHis) throws DaoException {
		return true;
	}
	
	public boolean ProcessoExiste(String myProcesso) throws DaoException {
		return true;
	}
	
	 public String consultaTotalProcessos(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {
		 return "0";
	 }
	
}