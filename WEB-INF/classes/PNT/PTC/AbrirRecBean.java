package PNT.PTC;

/**
 * <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
 * <b>Description:</b>  Abrir Recurso Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Wellem Lyra
 * @version 1.0
 */

public class AbrirRecBean         { 
	
	
	private String txtEMail;    
	private String txtMotivo;
	private String datEntrada;    
	
	
	
	public AbrirRecBean()  throws sys.BeanException {
		super() ;	
		datEntrada = sys.Util.formatedToday().substring(0,10) ;  
		txtEMail   = "";  
		txtMotivo  = "";		
	}	
	public String getNomTpSol(String tpSol)   {
		String nom="";
		if ("DP".equals(tpSol)) nom="Defesa Pr�via";		
		if ("RP".equals(tpSol)) nom="Recurso";						
		if ("2P".equals(tpSol)) nom="Rec. 2a Inst�ncia";						
		if ("AE".equals(tpSol)) nom="Antecipa��o de Entrega";	
		return nom;
	} 
//	--------------------------------------------------------------------------  
	public void setDatEntrada(String datEntrada)   {
		if (datEntrada==null) datEntrada = sys.Util.formatedToday().substring(0,10) ;
		this.datEntrada=datEntrada ;	
	}
	public String getDatEntrada()   {
		return datEntrada ;	
	}
//	--------------------------------------------------------------------------  
	public void setTxtEMail(String txtEMail)   {
		if (txtEMail==null) txtEMail = "";
		this.txtEMail=txtEMail ;	
	}
	public String getTxtEMail()   {
		return txtEMail ;	
	}
//	--------------------------------------------------------------------------  
	public void setTxtMotivo(String txtMotivo)   {
		if (txtMotivo==null) txtMotivo = "";
		this.txtMotivo = txtMotivo ;	
	}
	public String getTxtMotivo()   {
		return txtMotivo ;	
	}
	
}