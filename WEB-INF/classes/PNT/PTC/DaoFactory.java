package PNT.PTC;


public class DaoFactory {
	
	private static Dao instance;

	public static Dao getInstance() throws sys.DaoException, sys.ServiceLocatorException {
        if (instance == null) { 
            if (sys.ServiceLocator.getInstance().getQualBroker().equals("1"))
                instance = new DaoOra();
            else
                instance = new DaoAdabas();       
        }
        return instance;
    }

	
}


