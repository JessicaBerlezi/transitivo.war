package PNT.PTC;

import sys.BeanException;
import sys.DaoException;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.HistoricoBean;
public interface Dao {
	
			
	public abstract void AbreDefesa(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException, BeanException;	
	public abstract void EntregaCNH(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException;
	public abstract boolean CadastraNumProcesso(ProcessoBean myProcesso, String ProcAtu,String dProc,HistoricoBean myHis) throws DaoException;
	public abstract boolean ProcessoExiste(String myProcesso) throws DaoException;
	public abstract String consultaTotalProcessos(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException;	
}