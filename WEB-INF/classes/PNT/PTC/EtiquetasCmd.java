package PNT.PTC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;

public class EtiquetasCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/PTC/Etiquetas.jsp" ;  
	
	public EtiquetasCmd() {
		next             =  jspPadrao;
	}
	
	public EtiquetasCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     	
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			ParamSistemaBean parSis               = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis            = new ParamSistemaBean();

			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 
						
			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
			
			  if ((acao==null) || ("".equals(acao)))	{
				  ProcessoBeanId       = new ProcessoBean() ;	  		 
				  session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				  acao = "";
			  }				
			
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getCodProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					setEventoOK(ProcessoBeanId);
					ProcessoBeanId.LeRequerimentos() ;
					ProcessoBeanId.setTipoRequerimento("DP,RP,2P,EC")  ;
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
					nextRetorno = next;					
				}				
			}
			if  (acao.equals("Etiquetas"))  {	    			    						
				ProcessoBeanId.LeRequerimentos() ;				
				RequerimentoBean myReq = ProcessoBeanId.getRequerimentos(req.getParameter("codRequerimento"));				
				// montar historico
				PNT.HistoricoBean myHis = new PNT.HistoricoBean();
				myHis.setCodProcesso(ProcessoBeanId.getCodProcesso());   
				myHis.setNumProcesso(ProcessoBeanId.getNumProcesso()); 
				myHis.setCodStatus(ProcessoBeanId.getCodStatus());  
				myHis.setDatStatus(ProcessoBeanId.getDatStatus());
				myHis.setCodOrigemEvento("101");			
				myHis.setCodEvento("810");
				// complemento 01 - Num Requerimento no DAO
				myHis.setTxtComplemento01(myReq.getNumRequerimento());
				myHis.setTxtComplemento02(myReq.getCodTipoSolic());
				myHis.setTxtComplemento04(ProcessoBeanId.getNomResponsavel());		
				myHis.setTxtComplemento06(ProcessoBeanId.getNumCPF());
				myHis.setTxtComplemento03(myReq.getDatRequerimento());
				myHis.setTxtComplemento12(ProcessoBeanId.getTxtEMail());					
				req.setAttribute("HistoricoBeanId",myHis);

				if (sSigFuncao.equals("PNT0720"))
					nextRetorno = "/PNT/PTC/EntregaCNHMostra.jsp" ;
				else
					nextRetorno = "/PNT/PTC/AbrirDefesaMostra.jsp" ;
			}
			
			if  (acao.equals("Protocolo"))  {
				nextRetorno = "/PNT/PTC/ProtocoloEntregaCNH.jsp" ;  
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			}
			
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("EtiquetasCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

	public void setEventoOK(ProcessoBean myProcesso)  throws sys.BeanException  {
		try {
			if ("".equals(myProcesso.getNomStatus())) {
				myProcesso.setMsgOk("Status: "+myProcesso.getCodStatus()+" n�o cadastrado na Tabela.");				
				myProcesso.setEventoOK("N");
			}
			else {
				myProcesso.setMsgOk(myProcesso.getNomStatus()+" (Data do Status: "+myProcesso.getDatStatus()+")");
				myProcesso.setEventoOK("S");
				myProcesso.LeRequerimentos();
				// Verificar se ja existe Requerimento de Defesa
				RequerimentoBean req= myProcesso.getRequerimentoTipo("DP");
				boolean achouReq = false ;
				if("DP".equals(req.getCodTipoSolic()) && !req.getCodStatusRequerimento().equals("7")){
					achouReq = true;
				}				
				req= myProcesso.getRequerimentoTipo("RP");
				if("RP".equals(req.getCodTipoSolic()) && !req.getCodStatusRequerimento().equals("7")){
					achouReq = true;
				}
				req= myProcesso.getRequerimentoTipo("EC");
				if("EC".equals(req.getCodTipoSolic()) && !req.getCodStatusRequerimento().equals("7")){
					achouReq = true;
				}				
				if (achouReq==false) {
					myProcesso.setEventoOK("N");
					myProcesso.setMsgOk(myProcesso.getMsgOk()+". N�o encontrado requerimento (DP,RP,EC)");
				}
			}
		} catch (Exception ue) {
			throw new sys.BeanException("EtiquetasCmd: EventoOK " + ue.getMessage());
		}		 
	}
		
	
}
