package PNT.PTC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;


import sys.BeanException;
import sys.DaoException;
import sys.Util;

/**
 * <b>Title:</b>       	SMIT - PNT.PTC.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados nas funcionalidades do Protocolo<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoAdabas extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoAdabas() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	public void AbreDefesa(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis) throws DaoException, BeanException {		
		String resultado ="";
		String parteVar = "";
		UsuarioBean myUsuario= new UsuarioBean();		
		try 
		{
			
			String orgaoAtuacao 		= "            ";
			String orgaoLotacao 		= "            ";
			String nomUserName  		= "            ";
			myReq.setNumRequerimento(buscarReq(parSis));
			myHis.setTxtComplemento01(myReq.getNumRequerimento());
			
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +				
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.rPad(myReq.getNumRequerimento()," ",20) +
			sys.Util.rPad(myReq.getCodTipoSolic()," ",2) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatRequerimento()),"0",8) +		
			sys.Util.rPad(myProcesso.getNomResponsavel()," ",60) +	
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
			sys.Util.rPad(myHis.getTxtComplemento07()," ",50) +							
			sys.Util.rPad(myHis.getTxtComplemento12()," ",60) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
			if (myHis.getTxtComplemento08().length()>0) {
				// Cria��o de Processo Fisico
				myHis.setCodEvento("815"); 
				parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8) + 
				sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
				sys.Util.rPad(myHis.getNomUserName()," ",20) +
				sys.Util.lPad(myHis.getCodEvento(),"0",3) +
				sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +				
				sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
				sys.Util.rPad(myReq.getNumRequerimento()," ",20) +  //Numero do Requerimento
				sys.Util.rPad(myReq.getCodTipoSolic()," ",2) +
				sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myReq.getDatRequerimento()),"0",8) +		
				sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDatProcessoFis()),"0",8);
				resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
				if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
					return;
				}
			}		
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AbreDefesa: "+e.getMessage());
		}
	}	
	
	
	public void EntregaCNH(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {
			
			
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8) +
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +				
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
			sys.Util.rPad(myProcesso.getNumProtocoloEntregaCNH()," ",20) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDtEntregaCNH()),"0",8) +		
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDtDevolucaoCNH()),"0",8) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",UsrLogado);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - EntregaCNH: "+e.getMessage());
		}		
	}
	public void AntecipacaoCNH(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8) + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +				
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.rPad(myProcesso.getNumProtocoloEntregaCNH()," ",20) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDtEntregaCNH()),"0",8) +						
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDtDevolucaoCNH()),"0",8) +
			sys.Util.lPad(myProcesso.getCodPenalidade(),"0",3) +
			sys.Util.rPad(myProcesso.getDscPenalidade()," ",100);
			
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",UsrLogado);
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - EntregaCNH: "+e.getMessage());
		}		
	}
	
	public void EstornoEntregaCNH(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {
			
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8) + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +				
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +			
			sys.Util.rPad(myProcesso.getNumProtocoloEntregaCNH()," ",20) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)),"0",8);			
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",UsrLogado);
			
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - EntregaCNH: "+e.getMessage());
		}		
	}
	
	public String buscarReq(ParamSistemaBean parSis) throws DaoException {
		Connection conn =null ;
		String sCmd     = "";
		try {		
//			Pegar o Assunto - ASSUNTO_PROCESSO_PONTUACAO e  ORGAO
			String myOrgaoProcessoPnt = parSis.getParamSist("ORGAO_PROCESSO_PONTUACAO");
			String myCodReq           = parSis.getParamSist("CODIGO_REQUERIMENTO_PONTUACAO");			 			 
			// pegar numero de requerimento
			String myAnoPnt = "2007" ;
			sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" where nom_Parametro='PROCESSO_ANO' and cod_orgao='"+myOrgaoProcessoPnt+"' FOR UPDATE";
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();			
			ResultSet rs  = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myAnoPnt  = rs.getString("VAL_PARAMETRO");		  
			}
			sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myOrgaoProcessoPnt+"' FOR UPDATE";			
			rs  = stmt.executeQuery(sCmd) ;
			boolean naoExisteNumeracao  = true ;
			int numUltReq  = 0;
			while (rs.next()) {
				numUltReq  = rs.getInt("VAL_PARAMETRO");
				naoExisteNumeracao	= false;			  
			}
			if (naoExisteNumeracao) 	{
				//	System.err.println("naoExisteNumeracao: "+naoExisteNumeracao2+" "+naoExisteNumeracao);		
				throw new DaoException("Parametros para numera��o de processo n�o localizado para o Org�o: "+myOrgaoProcessoPnt);
			}			 
			// gravar no requerimentos
			numUltReq++		;
			String nReq = "000000"+String.valueOf(numUltReq) ;			    
			nReq = myCodReq+"/"+nReq.substring(nReq.length()-6,nReq.length())+"/"+myAnoPnt ;		
			// Update na Numeracao			
			sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltReq+"'," +
			" NOM_USERNAME_ALT ='PONTUACAO' "+			 
			" where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myOrgaoProcessoPnt+"'";		
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();
			rs.close();		
			return nReq;
		}	
		catch (SQLException sqle) {
			throw new DaoException("DaoAdabas - buscarReq: "+sqle.getMessage()+" - "+sCmd);
		}
		catch (Exception e) {	
			throw new DaoException("DaoAdabas - buscarReq: "+e.getMessage()+" - "+sCmd);
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			 
	}
	
	public String buscaProtocoloEntregaCNH(ACSS.UsuarioBean UsrLogado) throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			conn.setAutoCommit(false);
			// buscar o numero do proximo arquivo e atualizar
			String sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" WHERE NOM_PARAMETRO = 'NUM_PROT_ENTREGA_CNH' AND COD_ORGAO = '" +
			UsrLogado.getCodOrgaoAtuacao() + "' FOR UPDATE";
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			int numUltProt = 0;
			if (rs.next()) { 
				numUltProt = rs.getInt("VAL_PARAMETRO");                 
			}
			rs.close();
			boolean paramExiste = (numUltProt != 0);
			
			// atualizar numero de sequencia do arquivo
			String nSeq = sys.Util.lPad(String.valueOf(numUltProt), "0", 5);                
			if (!paramExiste)   {
				numUltProt++;
				sCmd = "INSERT INTO TCAU_PARAM_ORGAO (COD_PARAMETRO,";
				sCmd += "NOM_PARAMETRO,NOM_DESCRICAO,VAL_PARAMETRO,COD_ORGAO,COD_GRUPO) ";
				sCmd += "VALUES (SEQ_TCAU_PARAM_ORGAO.NEXTVAL,'NUM_PROT_ENTREGA_CNH',";
				sCmd += "'NUMERO DO PROTOCOLO DE ENTREGA DE CNH',";
				sCmd += "'" + sys.Util.lPad(String.valueOf(numUltProt), "0", 5) + "'," ;
				sCmd += "'" + UsrLogado.getCodOrgaoAtuacao() + "',10) ";
			}
			else {
				numUltProt++;
				sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO= '" + sys.Util.lPad(String.valueOf(numUltProt), "0", 5) + "'" +
				" WHERE NOM_PARAMETRO = 'NUM_PROT_ENTREGA_CNH' AND COD_ORGAO = '" + 
				UsrLogado.getCodOrgaoAtuacao() + "'";
			}
			stmt.executeUpdate(sCmd);
			conn.commit();
			stmt.close();
			return nSeq ;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public boolean CadastraNumProcesso(ProcessoBean myProcesso, String ProcAtu,String dtProc,HistoricoBean myHis) throws DaoException {
		String resultado ="";
		String parteVar = "";
		try 
		{
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20)    +
			sys.Util.lPad(myHis.getCodEvento(),"0",3)       +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11)    +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.rPad(myHis.getTxtComplemento01().trim()," ",20)  +
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			sys.Util.rPad(myHis.getTxtComplemento03()," ",50)         +
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000);
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",new UsuarioBean());
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return false;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AbreDefesa: "+e.getMessage());
		}
		return true;
	}
	
	public boolean ProcessoExiste(String myProcesso) throws DaoException {
		Connection conn = null ;
		String sCmd     = "";
		boolean existe=false;
		try {		
			conn = serviceloc.getConnection(MYABREVSIST) ;			
			conn.setAutoCommit(false);
			// Atualizar  Historico, Processo e Requerimento
			sCmd  =  "SELECT NUM_PROCESSO FROM  TPNT_PROCESSO WHERE NUM_PROCESSO = '"+myProcesso.trim()+"'";			
			Statement stmt = conn.createStatement();				
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				existe=true;
			}				
			return  existe;
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - Cadastra Numero de Processo: "+e.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myProc,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk    = false ;
		 String codErro = resultado.substring(0,3);
		 try {
			 if ((codErro.equals("000")==false) &&
					 (codErro.equals("037")==false)) {
				 if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					 myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 codErro=Util.lPad(codErro.trim(),"0",3);
					 myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					 ResultSet rs   = stmt.executeQuery(sCmd) ;
					 while (rs.next()) {
						 myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					 }
					 rs.close();
				 }
			 }
			 else   bOk = true ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
	 
	 public String consultaTotalProcessos(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {
		    String parteVar = "";
		    String resultado ="";				
			String sCmd     = "";
			String qtdProc  ="";
			String dtIni    ="01/05/2007" ;
			String dtFim    ="22/10/2008";
			try {
				
				parteVar = sys.Util.lPad(myProcesso.getTipCNH(),"0",1) + 
				sys.Util.lPad(myProcesso.getNumCNH(),"0",14) +
				sys.Util.rPad(myProcesso.getUfCNH()," ",2) +
				sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +
				sys.Util.lPad(myProcesso.getCodStatus(),"0",3) +
				sys.Util.lPad(sys.Util.formataDataYYYYMMDD(dtIni),"0",8)+
				sys.Util.lPad(sys.Util.formataDataYYYYMMDD(dtFim),"0",8);
							
				resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",UsrLogado);
				String linha = resultado.substring(3);
				setPosIni(0);
				qtdProc = linha;
				
				if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
					return qtdProc;
				}
			}				
			catch (Exception e) {	
				throw new DaoException("DaoAdabas - buscarReq: "+e.getMessage()+" - "+sCmd);
			}
			return qtdProc;
	 }
	 
	 
	
}