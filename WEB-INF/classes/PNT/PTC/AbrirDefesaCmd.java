package PNT.PTC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.HistoricoBean;
import PNT.PTC.Dao;
import PNT.DEF.FuncionalidadesBean;

public class AbrirDefesaCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/PNT/PTC/AbrirDefesa.jsp" ;  
	
	public AbrirDefesaCmd() {
		next = jspPadrao;
	}
	
	public AbrirDefesaCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;			
    
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			ParamSistemaBean parSis               = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis            = new ParamSistemaBean();
			
			ProcessoBean ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
			if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;	 

			AbrirRecBean AbrirRecBeanId     = (AbrirRecBean)req.getAttribute("AbrirRecBeanId") ;
			if (AbrirRecBeanId==null)   AbrirRecBeanId = new AbrirRecBean() ;
			RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			
			if (RequerimentoId==null)   RequerimentoId = new RequerimentoBean() ;	 
			FuncionalidadesBean   FuncionalidadesId = new FuncionalidadesBean() ;
			FuncionalidadesId.setSigFuncao(req.getParameter("j_sigFuncao"));		
			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  			
		    if ((acao==null) || ("".equals(acao)))	{
				ProcessoBeanId       = new ProcessoBean() ;	  		 
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
				acao = "";
			}				
			
			sys.Command cmd = (sys.Command)Class.forName("PNT.ProcessaProcessoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
			
			if("".equals(acao)) nextRetorno = "/PNT/ProcessoAbre.jsp";
			if ("Novo".equals(acao)) {
				acao = "";				
				ProcessoBeanId = new ProcessoBean();
				session.setAttribute("ProcessoBeanId", ProcessoBeanId);
				nextRetorno = "/PNT/ProcessoAbre.jsp";
			} 			
			if  (acao.equals("processoLe")) { 
				ProcessoBeanId     = (ProcessoBean)session.getAttribute("ProcessoBeanId") ;
				if (ProcessoBeanId==null)   ProcessoBeanId = new ProcessoBean() ;
				if (ProcessoBeanId.getNumProcesso().length()==0)  {
					// erro na leitura
					nextRetorno = "/PNT/ProcessoAbre.jsp";
				}
				else {
					ProcessoBeanId.LeRequerimentos();
					RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);	
					req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;
					req.setAttribute("RequerimentoId",RequerimentoId);
					session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;				
					nextRetorno = next;						
				}				
			}
							
			if  (acao.equals("AbreRecurso"))  {
				RequerimentoId = FuncionalidadesId.setEventoOK(ProcessoBeanId);				
				nextRetorno = processaDefesa(req,parSis,ProcessoBeanId,FuncionalidadesId,AbrirRecBeanId,UsrLogado);
				req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;
				req.setAttribute("RequerimentoId",RequerimentoId);
				session.setAttribute("ProcessoBeanId",ProcessoBeanId) ;
			}
			if (acao.equals("R"))	{
			    nextRetorno ="" ;
				ProcessoBeanId       = new ProcessoBean() ;	  				
				session.removeAttribute("ProcessoBeanId") ;
			}			
		}
		catch (Exception ue) {
			throw new sys.CommandException("AbrirDefesaDPCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}	
	protected String processaDefesa(HttpServletRequest req,ParamSistemaBean parSis,ProcessoBean myProcesso,FuncionalidadesBean FuncionalidadesId,AbrirRecBean AbrirRecBeanId,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		String nextRetorno=jspPadrao;		
		try {
								
			myProcesso.setMsgErro("") ;
			Vector<String> vErro = new Vector<String>() ;

			AbrirRecBeanId.setDatEntrada(req.getParameter("datEntrada"));
			AbrirRecBeanId.setTxtMotivo(req.getParameter("txtMotivo"));
			AbrirRecBeanId.setTxtEMail(req.getParameter("txtEMail"));
			String folga        = parSis.getParamSist("FOLGA_DEF_PREVIA");
			String folgaRecurso = parSis.getParamSist("FOLGA_RECURSO_PENALIDADE");
			
			
			int ifolga = 0,ifolgaRecurso = 0;
			try {
				ifolga        = Integer.parseInt(folga) ;
				ifolgaRecurso = Integer.parseInt(folgaRecurso) ;				
			} catch (Exception e) {		}
			
			
			//validar data de entrada
			if (AbrirRecBeanId.getDatEntrada().length()==0) {
				vErro.addElement("Data de Entrada n�o informada. \n") ;
			}
			else if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),AbrirRecBeanId.getDatEntrada())>0)
				vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;
			else 
			{	 
				/*Defesa*/
			    if (FuncionalidadesId.getTipoRequerimento().equals("DP"))
			    {
					if ( (sys.Util.DifereDias(myProcesso.getUltDatDefesa(),AbrirRecBeanId.getDatEntrada())>ifolga) )	{
						vErro.addElement("Prazo limite: "+myProcesso.getUltDatDefesa() + " mais toler�ncia ("+folga+"). \n") ;	
					}
			    }
			    /*Recurso*/
			    if (FuncionalidadesId.getTipoRequerimento().equals("RP"))
			    {
					if ( (sys.Util.DifereDias(myProcesso.getUltDatRecurso(),AbrirRecBeanId.getDatEntrada())>ifolgaRecurso) )	{
						vErro.addElement("Prazo limite: "+myProcesso.getUltDatRecurso() + " mais toler�ncia ("+folgaRecurso+"). \n") ;	
					}
			    }
			    /*Entrega CNH*/
			    if (FuncionalidadesId.getTipoRequerimento().equals("EC"))
			    {
					if ( (sys.Util.DifereDias(myProcesso.getUltDatEntregaCNH(),AbrirRecBeanId.getDatEntrada())>ifolgaRecurso) )	{
						vErro.addElement("Prazo limite: "+myProcesso.getUltDatEntregaCNH() + " mais toler�ncia ("+folgaRecurso+"). \n") ;	
					}
			    }
			}					 						
			if (vErro.size()>0) {
				myProcesso.setMsgErro(vErro) ;	
			}   
			else {							
				myProcesso.setMsgErro(atualizarProc(req,parSis,myProcesso,FuncionalidadesId,AbrirRecBeanId,UsrLogado)) ;						
				nextRetorno = "/PNT/PTC/AbrirDefesaMostra.jsp" ;   	
			}
		}
		catch (Exception e){
			throw new sys.CommandException("AbrirDefesaCmd: " + e.getMessage());
		}
		return nextRetorno  ;
	}
	private Vector atualizarProc(HttpServletRequest req,ParamSistemaBean parSis,ProcessoBean myProcesso,FuncionalidadesBean FuncionalidadesId,AbrirRecBean AbrirRecBeanId,UsuarioBean UsrLogado) throws sys.CommandException { 
		Vector<String> vErro = new Vector<String>() ;
		try { 	
			RequerimentoBean myReq = new 	RequerimentoBean();
			//Numero do Requerimento sera pego no Dao
			myReq.setCodProcesso(myProcesso.getCodProcesso());		
			myReq.setCodTipoSolic(FuncionalidadesId.getTipoRequerimento());
			myReq.setDatRequerimento(AbrirRecBeanId.getDatEntrada());
			myReq.setCodStatusRequerimento(FuncionalidadesId.getStatusRequerimento());
			myReq.setNomUserNameDP(UsrLogado.getNomUserName());
			myReq.setCodOrgaoLotacaoDP(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
			// Atualizar Bean de Processo
			if (FuncionalidadesId.getStatusDestino().equals(myProcesso.getCodStatus())==false) myProcesso.setDatStatus(AbrirRecBeanId.getDatEntrada()) ;				
			myProcesso.setCodStatus(FuncionalidadesId.getStatusDestino());
			if (AbrirRecBeanId.getTxtEMail().length()>0) myProcesso.setTxtEMail(AbrirRecBeanId.getTxtEMail());
			// preparar a data do Processo Fisico - se nao existir
			HistoricoBean myHis = new HistoricoBean() ;
			if (myProcesso.getDatProcessoFis().length()==0) {
				myHis.setTxtComplemento08(myReq.getDatRequerimento());
				myProcesso.setDatProcessoFis(myReq.getDatRequerimento());
				myProcesso.setNomUserNameFis(UsrLogado.getNomUserName());
				myProcesso.setCodOrgaoLotacaoFis(UsrLogado.getOrgao().getCodOrgao());
				myProcesso.setDatProcFis(sys.Util.formatedToday().substring(0,10));
			}
			// preparar Bean de Historico			
			myHis.setCodProcesso(myProcesso.getCodProcesso());   
			myHis.setNumProcesso(myProcesso.getNumProcesso()); 
			myHis.setCodStatus(myProcesso.getCodStatus());  
			myHis.setDatStatus(myProcesso.getDatStatus());
			myHis.setCodOrigemEvento(FuncionalidadesId.getCodOrigemEvento());			
			myHis.setCodEvento(FuncionalidadesId.getCodEvento() );
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			// complemento 01 - Num Requerimento no DAO
			// complemento 08 - Abrir Processo Fisico - gerar evento 015 no hist�tico
			myHis.setTxtComplemento02(myReq.getCodTipoSolic());
			myHis.setTxtComplemento04(myProcesso.getNomResponsavel());		
			myHis.setTxtComplemento06(myProcesso.getNumCPF());
			myHis.setTxtComplemento03(myReq.getDatRequerimento());
			myHis.setTxtComplemento07(AbrirRecBeanId.getTxtMotivo());
			myHis.setTxtComplemento12(AbrirRecBeanId.getTxtEMail());	
			req.setAttribute("HistoricoBeanId",myHis) ;			
			Dao dao = DaoFactory.getInstance();
			dao.AbreDefesa(myProcesso,myReq,myHis,parSis) ;		    			
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		return vErro ;
	}	
}