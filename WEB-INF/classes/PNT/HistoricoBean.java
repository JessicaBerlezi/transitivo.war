package PNT;

import java.util.Vector;
import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - Pontuacao - Historico Bean<br>
* <b>Description:</b>  Historico Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
* @author Sergio Monteiro
* @version 1.0
*/

public class HistoricoBean   { 

  private String msgErro ;  
         
  private String codHistoricoProcesso; 
  private String codProcesso;   
 
  private String numProcesso;
  
  private String codStatus;  
  private String nomStatus;    
  private String datStatus;
  private String codEvento;
  private String nomEvento;
  private String codOrigemEvento;
  private String nomOrigemEvento;
  
  private String txtComplemento01;
  private String txtComplemento02;
  private String txtComplemento03;
  private String txtComplemento04;
  private String txtComplemento05;
  private String txtComplemento06;
  private String txtComplemento07;
  private String txtComplemento08;
  private String txtComplemento09;
  private String txtComplemento10;
  private String txtComplemento11;
  private String txtComplemento12;
  private String nomUserName;
  private String codOrgaoLotacao;
  private String sigOrgaoLotacao;
  private String datProc;
  
  
  public HistoricoBean()  throws sys.BeanException {

    msgErro           = "" ;		
	
	codHistoricoProcesso  = ""; 	
	codProcesso   = ""; 			
	numProcesso       = ""; 		
			
	codStatus         = ""; 
	nomStatus         = ""; 					
	datStatus         = ""; 		
	codEvento         = ""; 
	nomEvento         = ""; 
	codOrigemEvento   = ""; 
	nomOrigemEvento   = ""; 
	
	codOrigemEvento   = ""; 		
	txtComplemento01  = ""; 		
	txtComplemento02  = ""; 		
	txtComplemento03  = ""; 		
	txtComplemento04  = ""; 		
	txtComplemento05  = ""; 		
	txtComplemento06  = ""; 		
	txtComplemento07  = ""; 		
	txtComplemento08  = ""; 
	txtComplemento09  = ""; 		
	txtComplemento10  = ""; 		
	txtComplemento11  = ""; 		
	txtComplemento12  = ""; 		
	nomUserName       = ""; 		
	codOrgaoLotacao   = "";
	sigOrgaoLotacao   = "";
	datProc           = ""; 		

  }

  public void setMsgErro(Vector vErro)   {
   	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
     }
   }
  public void setMsgErro(String msgErro)   {
  	this.msgErro = msgErro ;
   }   
  public String getMsgErro()   {
   	return this.msgErro;
   }     

  public void setCodHistoricoProcesso(String codHistoricoProcesso)  {
	  if (codHistoricoProcesso==null) codHistoricoProcesso= "";
	  this.codHistoricoProcesso=codHistoricoProcesso ;
  }  
  public String getCodHistoricoProcesso()  {
  	return this.codHistoricoProcesso;
  }
  
  public void setCodProcesso(String codProcesso)  {
	  if (codProcesso==null) codProcesso= "";
	  this.codProcesso=codProcesso ;
  }  
  public String getCodProcesso()  {
  	return this.codProcesso;
  }

  public void setNumProcesso(String numProcesso)  {
	  if (numProcesso==null) numProcesso= "";
	  this.numProcesso=numProcesso ;

  }  
  public String getNumProcesso()  {
  	return this.numProcesso;
  }
  public void setCodStatus(String codStatus)  {
	  if (codStatus==null) codStatus= "";
	  this.codStatus=codStatus ;
  }  
  public String getCodStatus()  {
  	return this.codStatus;
  }
  public void setNomStatus(String nomStatus)  {
	  if (nomStatus==null) nomStatus= "";
	  this.nomStatus=nomStatus ;
  }  
  public String getNomStatus()  {
  	return this.nomStatus;
  }
  public void setDatStatus(String datStatus)  {
	  if (datStatus==null) datStatus= "";
	  this.datStatus=datStatus ;
  }  
  public String getDatStatus()  {
   	return this.datStatus;
  }
  public void setCodEvento(String codEvento)  {
	  if (codEvento==null) codEvento= "";
	  this.codEvento=codEvento ;
  }  
  public String getCodEvento()  {
   	return this.codEvento;
  }
  public void setNomEvento(String nomEvento)  {
	  if (nomEvento==null) nomEvento= "";
	  this.nomEvento=nomEvento ;
  }  
  public String getNomEvento()  {
	  
	  
   	return this.nomEvento;
  }
  
  public void setCodOrigemEvento(String codOrigemEvento)  {
	  if (codOrigemEvento==null) codOrigemEvento= "";
	  this.codOrigemEvento=codOrigemEvento ;
  }  
  public String getCodOrigemEvento()  {
   	return this.codOrigemEvento;
  }
  public void setNomOrigemEvento(String nomOrigemEvento)  {
	  if (nomOrigemEvento==null) nomOrigemEvento= "";
	  this.nomOrigemEvento=nomOrigemEvento ;
  }  
  public String getNomOrigemEvento()  {
   	return this.nomOrigemEvento;
  }
  public void setTxtComplemento01(String txtComplemento01)  {
	  if (txtComplemento01==null) txtComplemento01= "";
	  this.txtComplemento01=txtComplemento01 ;
  }  
  public String getTxtComplemento01()  {
   	return this.txtComplemento01;
  }
  public void setTxtComplemento02(String txtComplemento02)  {
	  if (txtComplemento02==null) txtComplemento02= "";
	  this.txtComplemento02=txtComplemento02 ;
  }  
  public String getTxtComplemento02()  {
   	return this.txtComplemento02;
  }
  public void setTxtComplemento03(String txtComplemento03)  {
	  if (txtComplemento03==null) txtComplemento03= "";
	  this.txtComplemento03=txtComplemento03 ;
  }  
  public String getTxtComplemento03()  {
   	return this.txtComplemento03;
  }
  public void setTxtComplemento04(String txtComplemento04)  {
	  if (txtComplemento04==null) txtComplemento04= "";
	  this.txtComplemento04=txtComplemento04 ;
  }  
  public String getTxtComplemento04()  {
   	return this.txtComplemento04;
  }
  public void setTxtComplemento05(String txtComplemento05)  {
	  if (txtComplemento05==null) txtComplemento05= "";
	  this.txtComplemento05=txtComplemento05 ;
  }  
  public String getTxtComplemento05()  {
   	return this.txtComplemento05;
  }
  public void setTxtComplemento06(String txtComplemento06)  {
	  if (txtComplemento06==null) txtComplemento06= "";
	  this.txtComplemento06=txtComplemento06 ;
  }  
  public String getTxtComplemento06()  {
   	return this.txtComplemento06;
  }
  public void setTxtComplemento07(String txtComplemento07)  {
	  if (txtComplemento07==null) txtComplemento07= "";
	  this.txtComplemento07=txtComplemento07 ;
  }  
  public String getTxtComplemento07()  {
   	return this.txtComplemento07;
  }
  public void setTxtComplemento08(String txtComplemento08)  {
	  if (txtComplemento08==null) txtComplemento08= "";
	  this.txtComplemento08=txtComplemento08 ;
  }  
  public String getTxtComplemento08()  {
   	return this.txtComplemento08;
  }
  public void setTxtComplemento09(String txtComplemento09)  {
	  if (txtComplemento09==null) txtComplemento09= "";
	  this.txtComplemento09=txtComplemento09 ;
  }  
  public String getTxtComplemento09()  {
   	return this.txtComplemento09;
  }
  public void setTxtComplemento10(String txtComplemento10)  {
	  if (txtComplemento10==null) txtComplemento10= "";
	  this.txtComplemento10=txtComplemento10 ;
  }  
  public String getTxtComplemento10()  {
   	return this.txtComplemento10;
  }
  public void setTxtComplemento11(String txtComplemento11)  {
	  if (txtComplemento11==null) txtComplemento11= "";
	  this.txtComplemento11=txtComplemento11 ;
  }  
  public String getTxtComplemento11()  {
   	return this.txtComplemento11;
  }
  public void setTxtComplemento12(String txtComplemento12)  {
	  if (txtComplemento12==null) txtComplemento12= "";
	  this.txtComplemento12=txtComplemento12 ;
  }  
  public String getTxtComplemento12()  {
   	return this.txtComplemento12;
  }
  public void setNomUserName(String nomUserName)  {
	  if (nomUserName==null) nomUserName= "";
	  this.nomUserName=nomUserName ;
  }  
  public String getNomUserName()  {
   	return this.nomUserName;
  }
  public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
	  if (codOrgaoLotacao==null) codOrgaoLotacao= "";
	  this.codOrgaoLotacao=codOrgaoLotacao ;
  }  
  public String getCodOrgaoLotacao()  {
   	return this.codOrgaoLotacao;
  }
  public void setSigOrgaoLotacao(String sigOrgaoLotacao)  {
	  if (sigOrgaoLotacao==null) sigOrgaoLotacao= "";
	  this.sigOrgaoLotacao=sigOrgaoLotacao ;
  }  
  public String getSigOrgaoLotacao()  {
	return this.sigOrgaoLotacao;
  }
  public void setDatProc(String datProc)  {
	  if (datProc==null) datProc= "";
	  this.datProc=datProc ;
  }  
  public String getDatProc()  {
   	return this.datProc;
  }
  
  public void GravaHistorico(Connection conn) throws sys.BeanException {
	  try {
		  DaoBroker dao = DaoBrokerFactory.getInstance();
		  dao.GravaHistorico(this,conn);		   
	  }
	  catch (Exception ex) {
		  throw new sys.BeanException(ex.getMessage());
	  }		
  }	

}
