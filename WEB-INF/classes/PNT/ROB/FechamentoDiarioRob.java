package PNT.ROB;

import java.sql.Connection;
import java.sql.Statement;
import sys.ServiceLocatorException;
import sys.Util;
import sys.DaoException;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.AtaDOBean;
import PNT.NOT.ArquivoBean;
import PNT.TAB.ConteudoEmailBean;
import ROB.Dao;



/**
 * <b>Title:</b> SMIT - M�dulo Pontua��o<br>
 * <b>Description:</b> Rob� de Emiss�o de Notifica��es<br>
 * <b>Copyright:</b> Copyright (c) 2006<br>
 * <b>Company: </b> MIND Inform�tica <br>
 * @author Glaucio Jannotti
 */

public class FechamentoDiarioRob {
	
	private static final String MYABREVSIST = "PNT";
	private static final String NOM_ROBOT = "PNT.ROB.FechamentoDiarioRob";
	public  static final String QUEBRA_LINHA = "\n";
	private static final String NOM_USER = "FechDiarioPNTRobot";	
	private static final int NUM_DIAS_PUB_DO = 5;
	private static final int NUM_DIAS_DECURSO_PRAZO = 2;
	private static final String NUM_MAX_PROCESSOS = "999999";
	
	public FechamentoDiarioRob() throws DaoException, ServiceLocatorException {
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		PNT.ROB.Dao daoPnt = PNT.ROB.DaoFactory.getInstance();
		try {				
			conn = daoPnt.getServiceLocator().getConnection(MYABREVSIST);
			connLog = daoPnt.getServiceLocator().getConnection(MYABREVSIST);									
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	
			ParamSistemaBean paramSis = new ParamSistemaBean();
			paramSis.setCodSistema("21"); //M�dulo Pontua��o	
			paramSis.PreparaParam(conn);	
			try {               
				trava =  Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             						
			while (true) {					
				//Verifica se foi marcado para parar
				

				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				monitor.gravaMonitor("Fech Diario - Passo 1 - Atas DO para autua��o",monitor.MSG_INICIO);
				Passo1_AtasNaoEntregue(paramSis,daoPnt,conn);				
				monitor.gravaMonitor("Fech Diario - Fim do Passo 1",monitor.MSG_FIM);
				

				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;	
				 monitor.gravaMonitor("Fech Diario - Passo 2 - Fechamento Requerimento Defesa/Recurso",monitor.MSG_INICIO);
				 Passo2_AtasFechamentoRequerimento(paramSis,daoPnt,conn);				
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 2",monitor.MSG_FIM);

				 
				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				 monitor.gravaMonitor("Fech Diario - Passo 3 - Transitado Defesa/Recurso",monitor.MSG_INICIO);
				 Passo3_AtasDeferimento(paramSis,daoPnt,conn);				
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 3",monitor.MSG_FIM);

				 
				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				 monitor.gravaMonitor("Fech Diario - Passo 3 - Penalidade Registrada",monitor.MSG_INICIO);
				 Passo3_PenalidadeRegistrada(paramSis,daoPnt,conn);
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 3",monitor.MSG_FIM);

				 
				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				 monitor.gravaMonitor("Fech Diario - Passo 4 - Atas DO para penalidade",monitor.MSG_INICIO);
				 Passo4_AtasPenalidade(paramSis,daoPnt,conn);				
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 4",monitor.MSG_FIM);

				 
				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				 monitor.gravaMonitor("Fech Diario - Passo 6 - Apreens�o Registrada",monitor.MSG_INICIO);
				 Passo6_ApreensaoRegistrada(paramSis,daoPnt,conn);
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 6",monitor.MSG_FIM);

				 
				 if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				 monitor.gravaMonitor("Fech Diario - Passo 7 - Atas DO para apreens�o",monitor.MSG_INICIO);
				 Passo7_AtasNaoEntregueApreensao(paramSis,daoPnt,conn);				
				 monitor.gravaMonitor("Fech Diario - Fim do Passo 7",monitor.MSG_FIM);
				
				
				 break;
			}		
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Processar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new DaoException(ed.getMessage());
			}
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (Exception ed) {
					throw new DaoException(ed.getMessage());
				}
			}
		}
	}
	
	//	Passo 1
	public boolean Passo1_AtasNaoEntregue(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("0");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(paramSis.getParamSist("NUM_MAX_PROCESSOS_ATA"));
			myAta.setStatusElegiveis("001,002");
			myAta.setStatusDestino("003");			
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));
			int przPubl = 0;
			try {
				int przDef  = Integer.parseInt(paramSis.getParamSist("PRAZO_DEF_PREVIA"));
				przPubl = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO"));
				int iPrazo  = przDef+przPubl;
				myAta.setDatReferencia(Util.addData(myAta.getDatReferencia(),iPrazo)) ;				
			} catch (Exception e) {
			}
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("101");			
			// Evento 003 - Autua��o s/ sucesso,Evento 063 - Penal. s/ sucesso,Evento 093 - "Entg CNH" s/ sucesso
			myHist.setCodEvento("803");
			// Desc Tipo da Ata
			myHist.setTxtComplemento01(myAta.getTipAtaDODesc());
			// Cod Envio ao DO
			// complemento 02 - setado no Dao
			// Data de Envio p/ DO
			myHist.setTxtComplemento03(myAta.getDatEDO());			
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			
			/*Pontu�vel*/
			while (true) {
				myAta.setTipProcessosDO("'P'");
				daoPnt.buscaProcesso_AtasNaoEntregue(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}
			
			/*Mandat�rio*/
			while (true) {
				myAta.setTipProcessosDO("'S'");
				daoPnt.buscaProcesso_AtasNaoEntregue(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_AUTUACAO").split(",");
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				if (przPubl>3)  {
//					Data final de publica��o #DTENT#
					parametros[0]=Util.addData(Util.formatedToday().substring(0,10),przPubl-2);
				}
				else parametros[0]=Util.formatedToday().substring(0,10);
				
				int iDias = Util.isFimdeSemana(parametros[0]); // domingo =1; sabado=2 ou 0 
				if (iDias>0) parametros[0]=(Util.addData(parametros[0],iDias));

				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo1_AtasNaoEntregue "+e.getMessage());
		} 
	}	
	
	//	Passo 2
	public boolean Passo2_AtasFechamentoRequerimento(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("0");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(NUM_MAX_PROCESSOS);
			myAta.setStatusElegiveis("012,042");
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));			
			myAta.setDatReferenciaFinal(Util.DiminuiDias(NUM_DIAS_DECURSO_PRAZO).substring(0,10));            
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Desc Tipo da Ata
			myHist.setTxtComplemento01(myAta.getTipAtaDODesc());
			// Cod Envio ao DO
			// complemento 02 - setado no Dao
			// Data de Envio p/ DO
			myHist.setTxtComplemento03(myAta.getDatEDO());			
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			
			/*Pontu�vel*/
			myAta.setTipProcessosDO("'P'");
			daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
			// complemento 02 - Numero da ATA - setar no DAO				
			daoPnt.Inclui_AtasFechamentoRequerimento(myAta,myHist,conn);
			// setar variavel para eMail emMsgErro
			myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
					" - "+myAta.getDatEDO()+
					" Processos: "+myAta.getProcessos().size());
			
			/*Mandat�rio*/
			myAta.setTipProcessosDO("'S'");
			daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
			// complemento 02 - Numero da ATA - setar no DAO				
			daoPnt.Inclui_AtasFechamentoRequerimento(myAta,myHist,conn);
			// setar variavel para eMail emMsgErro
			myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
					" - "+myAta.getDatEDO()+
					" Processos: "+myAta.getProcessos().size());
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_AUTUACAO").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.formatedToday().substring(0,10);
				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo2_AtasFechamentoRequerimento "+e.getMessage());
		} 
	}	
	
	// Passo 3
	public boolean Passo3_AtasDeferimento(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("0");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(NUM_MAX_PROCESSOS);
			myAta.setStatusElegiveis("018,048");
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));
			myAta.setDatReferenciaFinal(Util.DiminuiDias(NUM_DIAS_DECURSO_PRAZO).substring(0,10));			
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Desc Tipo da Ata
			myHist.setTxtComplemento01(myAta.getTipAtaDODesc());
			// Cod Envio ao DO
			// complemento 02 - setado no Dao
			// Data de Envio p/ DO
			myHist.setTxtComplemento03(myAta.getDatEDO());			
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			
			/*Pontu�vel*/
			myAta.setTipProcessosDO("'P'");
			daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
			// complemento 02 - Numero da ATA - setar no DAO				
			daoPnt.Inclui_AtasDeferimento(myAta,myHist,conn);
			// setar variavel para eMail emMsgErro
			myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
					" - "+myAta.getDatEDO()+
					" Processos: "+myAta.getProcessos().size());
			
			/*Mandat�rio*/
			myAta.setTipProcessosDO("'S'");
			daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
			// complemento 02 - Numero da ATA - setar no DAO				
			daoPnt.Inclui_AtasDeferimento(myAta,myHist,conn);
			// setar variavel para eMail emMsgErro
			myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
					" - "+myAta.getDatEDO()+
					" Processos: "+myAta.getProcessos().size());
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_AUTUACAO").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.formatedToday().substring(0,10);
				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo3_AtasDeferimento "+e.getMessage());
		} 
	}	
	
	
	// Passo 3
	public boolean Passo3_PenalidadeRegistrada(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			//Utiliza o Bean de Ata para apoio
			UsuarioBean UsuarioBeanId = new UsuarioBean();
			UsuarioBeanId.setCodOrgaoAtuacao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			
			ArquivoBean arqRecebido = new ArquivoBean();
			arqRecebido.setNomArquivo("EMITEPONTO_"+
					Util.formatedToday().substring(0,10).substring(6,10)+
					Util.formatedToday().substring(0,10).substring(3,5)+
					Util.formatedToday().substring(0,10).substring(0,2)+".TXT");
			
			arqRecebido.setCodIdentArquivo("PNT0110"); /*recebe j_funcao=PNT0110=EMITEPONTO*/
			arqRecebido.setCodStatus("0");
			arqRecebido.setCodArquivo("0");
			arqRecebido.setNumProtocolo(UsuarioBeanId);
			arqRecebido.setCodOrgao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			arqRecebido.setCodOrgaoLotacao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			arqRecebido.setDatProcessamento(Util.formatedToday().substring(0,10));
			arqRecebido.setDatGeracao(Util.formatedToday().substring(0,10));
			arqRecebido.setDatProc(Util.formatedToday().substring(0,10));
			arqRecebido.setNomUsername(NOM_USER);
			
			PNT.NOT.Dao daoPntNot = PNT.NOT.DaoFactory.getInstance();
			/*daoPntNot.arquivoInsertHeader(arqRecebido);*/			
			/*Gera��o de Arquivo*/
			
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("0");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(paramSis.getParamSist("NUM_MAX_PROCESSOS_ATA"));
			myAta.setIndAplicarPenalidade(paramSis.getParamSist("APLICACAO_PENALIDADE_MANUAL"));
			if (myAta.getIndAplicarPenalidade().equals("S"))
				myAta.setStatusElegiveis("029");
			else
				myAta.setStatusElegiveis("004,019,029");
			
			myAta.setStatusDestino("031");
			
			
			// Calculo da Data de Referencia e ReferenciaFinal para definir processos para Penalidade 
			int folgaDef = 7;
			try {	folgaDef = Integer.parseInt(paramSis.getParamSist("FOLGA_DEF_PREVIA")); 
			} catch (Exception e) {	}
			
			myAta.setDatReferencia(Util.DiminuiDias(folgaDef).substring(0,10));
			
			// Calculo da Data de Ult Recurso	
			int przEmissao  = 5;
			try {	przEmissao  = Integer.parseInt(paramSis.getParamSist("PRAZO_EMISSAO")); 
			} catch (Exception e) {	}			
			int przEntrega  = 20;
			try {	przEntrega  = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTREGA")); 
			} catch (Exception e) {	}
			int przPublDO   = 7;
			try {	przPublDO  = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO")); 
			} catch (Exception e) {	}
			przPublDO       = 0;
			int przRecPenal = 30;
			try {	przRecPenal  = Integer.parseInt(paramSis.getParamSist("PRAZO_RECURSO_PENALIDADE")); 
			} catch (Exception e) {	}
			int iPrazo = przEmissao+przEntrega+przPublDO+przRecPenal ;
			
			// Ultiliza a DatReferenciaFinal para armazenar a ULTIMA DATA DE RECURSO
			myAta.setDatReferenciaFinal(Util.addData(Util.formatedToday().substring(0,10),iPrazo)) ;
			// Verificar sabado e  domingo - passar para segunda
			int iDias = Util.isFimdeSemana(myAta.getDatReferenciaFinal()); // domingo =1; sabado=2 ou 0 
			if (iDias>0) myAta.setDatReferenciaFinal(Util.addData(myAta.getDatReferenciaFinal(),iDias));
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Evento 030 - Penalidade Registrada
			myHist.setCodEvento("830");
			// Compl 01 - Numero da Notifica��o gerada
			// Compl 02 - Quantidade de Sequencias
			// Compl 03 - Ultima  Data de Recurso
			myHist.setTxtComplemento03(myAta.getDatReferenciaFinal());
			// Compl 04 - Codigo da Penalidade
			// Compl 05 - Descri��o da Penalidade	
			// Compl 06 - Ultima  Data de Entrega CNH
			myHist.setTxtComplemento06(myAta.getDatReferenciaFinal());
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			int  seq = 0;
			boolean bGeraArquivo=true;
			while (true) {
				seq++;
				myAta.setTipProcessosDO("'P','S'");
				daoPnt.buscaProcesso_AtasNaoEntregue(myAta,conn);		
				if (myAta.getProcessos().size()==0) break;		
				if (bGeraArquivo) {
					daoPntNot.arquivoInsertHeader(arqRecebido);
					bGeraArquivo=false;
				}
				daoPnt.Inclui_PenalidadeRegistrada(myAta,myHist,conn,paramSis,arqRecebido);
				
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n Penalidades Registradas: "+
						myAta.getDatProcEDO()+" - "+seq+ " "+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			/*Atualiza��o do Arquivo*/
			arqRecebido.setCodStatus("2");
			arqRecebido.setQtdLinha(arqRecebido.getLinhasArquivo().size());
			daoPntNot.arquivoInsertProcesso(arqRecebido);
			arqRecebido.isUpdate(conn);
			conn.commit();
			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) 
			{	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_PENALIDADE_REGISTRADA").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.formatedToday().substring(0,10);
				parametros[3]=myAta.getMsgErro();  // Penalidades Registradas #PENALIDADES#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo3_PenalidadeRegistrada "+e.getMessage());
		} 
	}
	
	//	 Passo 4
	public boolean Passo4_AtasPenalidade(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("2");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(paramSis.getParamSist("NUM_MAX_PROCESSOS_ATA"));
			myAta.setStatusElegiveis("031");
			myAta.setStatusDestino("033");
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Evento 003 - Autua��o s/ sucesso,Evento 063 - Penal. s/ sucesso,Evento 093 - "Entg CNH" s/ sucesso
			myHist.setCodEvento("833");
			// Desc Tipo da Ata
			myHist.setTxtComplemento01(myAta.getTipAtaDODesc());
			// Cod Envio ao DO
			// complemento 02 - setado no Dao
			// Data de Envio p/ DO
			myHist.setTxtComplemento03(myAta.getDatEDO());			
			
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			
			/*Pontu�vel*/
			while (true) {
				myAta.setTipProcessosDO("'P'");
				daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}
			
			/*Mandat�rio*/
			while (true) {
				myAta.setTipProcessosDO("'S'");
				daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_PENALIDADE").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.addData(Util.formatedToday().substring(0,10),NUM_DIAS_PUB_DO);

				int iDias = Util.isFimdeSemana(parametros[0]); // domingo =1; sabado=2 ou 0 
				if (iDias>0) parametros[0]=(Util.addData(parametros[0],iDias));
				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo4_AtasPenalidade "+e.getMessage());
		} 
	}	
	
	
	
	// Passo 6
	public boolean Passo6_ApreensaoRegistrada(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			//Utiliza o Bean de Ata para apoio
			UsuarioBean UsuarioBeanId = new UsuarioBean();
			UsuarioBeanId.setCodOrgaoAtuacao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			
			ArquivoBean arqRecebido = new ArquivoBean();
			arqRecebido.setNomArquivo("EMITEPONTO_"+
					Util.formatedToday().substring(0,10).substring(6,10)+
					Util.formatedToday().substring(0,10).substring(3,5)+
					Util.formatedToday().substring(0,10).substring(0,2)+".TXT");
			
			arqRecebido.setCodIdentArquivo("PNT0110"); /*recebe j_funcao=PNT0110=EMITEPONTO*/
			arqRecebido.setCodStatus("0");
			arqRecebido.setCodArquivo("0");
			arqRecebido.setNumProtocolo(UsuarioBeanId);
			arqRecebido.setCodOrgao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			arqRecebido.setCodOrgaoLotacao(paramSis.getParamSist("ORGAO_PONTUACAO"));
			arqRecebido.setDatProcessamento(Util.formatedToday().substring(0,10));
			arqRecebido.setDatGeracao(Util.formatedToday().substring(0,10));
			arqRecebido.setDatProc(Util.formatedToday().substring(0,10));
			arqRecebido.setNomUsername(NOM_USER);
			
			PNT.NOT.Dao daoPntNot = PNT.NOT.DaoFactory.getInstance();
			/*daoPntNot.arquivoInsertHeader(arqRecebido);*/			
			/*Gera��o de Arquivo*/
			
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("0");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(paramSis.getParamSist("NUM_MAX_PROCESSOS_ATA"));
			myAta.setStatusElegiveis("034,049");
			myAta.setStatusDestino("091");
			
//			Calculo da Data de Referencia e ReferenciaFinal para definir processos para Apreens�o 
			int folgaRec = 7;
			try {	folgaRec = Integer.parseInt(paramSis.getParamSist("FOLGA_RECURSO_PENALIDADE")); 
			} catch (Exception e) {	}
			myAta.setDatReferencia(Util.DiminuiDias(folgaRec).substring(0,10));
			
//			Calculo da Data de Apreens�o	
			int przEmissao  = 5;
			try {	przEmissao  = Integer.parseInt(paramSis.getParamSist("PRAZO_EMISSAO")); 
			} catch (Exception e) {	}			
			int przEntrega  = 20;
			try {	przEntrega  = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTREGA")); 
			} catch (Exception e) {	}
			
			int przEntregaCNH  = 2;
			try {	przEntregaCNH  = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTR_CNH")); 
			} catch (Exception e) {	}
			int iPrazo = przEmissao+przEntrega+przEntregaCNH;
			
//			Ultiliza a DatReferenciaFinal para armazenar a ULTIMA DATA DE APREENSAO
			myAta.setDatReferenciaFinal(Util.addData(Util.formatedToday().substring(0,10),iPrazo));
//			Verificar sabado e  domingo - passar para segunda
			int iDias = Util.isFimdeSemana(myAta.getDatReferenciaFinal()); // domingo =1; sabado=2 ou 0 
			if (iDias>0) myAta.setDatReferenciaFinal(Util.addData(myAta.getDatReferenciaFinal(),iDias));
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Evento 030 - Apreens�o Registrada
			myHist.setCodEvento("890");
			// Compl 01 - Numero da Notifica��o gerada
			// Compl 02 - Quantidade de Sequencias
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			int  seq = 0;
			boolean bGeraArquivo = true;
			while (true) {
				seq++;
				myAta.setTipProcessosDO("'P','S'");
				daoPnt.buscaProcesso_AtasApreensao(myAta,conn);		
				if (myAta.getProcessos().size()==0) break;
				if (bGeraArquivo) {
					daoPntNot.arquivoInsertHeader(arqRecebido);
					bGeraArquivo=false;
				}
				daoPnt.Inclui_ApreensaoRegistrada(myAta,myHist,conn,paramSis,arqRecebido);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n Apreens�es Registradas: "+
						myAta.getDatProcEDO()+" - "+seq+ " "+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			/*Atualiza��o do Arquivo*/
			arqRecebido.setCodStatus("2");
			arqRecebido.setQtdLinha(arqRecebido.getLinhasArquivo().size());
			daoPntNot.arquivoInsertProcesso(arqRecebido);
			arqRecebido.isUpdate(conn);
			conn.commit();
			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) 
			{	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_PENALIDADE_REGISTRADA").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.formatedToday().substring(0,10);
				parametros[3]=myAta.getMsgErro();  // Penalidades Registradas #PENALIDADES#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo6_ApreensaoRegistrada "+e.getMessage());
		}
	}
	
	
	public boolean Passo7_AtasNaoEntregueApreensao(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("4");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(paramSis.getParamSist("NUM_MAX_PROCESSOS_ATA"));
			myAta.setStatusElegiveis("091,092");
			myAta.setStatusDestino("093");			
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));
			int przPubl = 0;
			try {
				int przDef  = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTR_CNH"));
				przPubl = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO"));
				int iPrazo  = przDef+przPubl;
				myAta.setDatReferencia(Util.addData(myAta.getDatReferencia(),iPrazo)) ;				
			} catch (Exception e) {
			}
			
			HistoricoBean myHist = new HistoricoBean();
			myHist.setCodStatus(myAta.getStatusDestino());  
			myHist.setDatStatus(Util.formatedToday().substring(0,10));				
			myHist.setCodOrigemEvento("100");			
			// Evento 003 - Autua��o s/ sucesso,Evento 063 - Penal. s/ sucesso,Evento 093 - "Entg CNH" s/ sucesso
			myHist.setCodEvento("893");
			// Desc Tipo da Ata
			myHist.setTxtComplemento01(myAta.getTipAtaDODesc());
			// Cod Envio ao DO
			// complemento 02 - setado no Dao
			// Data de Envio p/ DO
			myHist.setTxtComplemento03(myAta.getDatEDO());			
			myHist.setNomUserName(myAta.getNomUserNameEDO());
			myHist.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
			myHist.setDatProc(myAta.getDatProcEDO());
			
			/*Pontu�vel*/
			while (true) {
				myAta.setTipProcessosDO("'P'");
				daoPnt.buscaProcesso_AtasNaoEntregueApreensao(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}
			
			/*Mandat�rio*/
			while (true) {
				myAta.setTipProcessosDO("'S'");
				daoPnt.buscaProcesso_AtasNaoEntregueApreensao(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_CNH").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				if (przPubl>3)  {
//					Data final de publica��o #DTENT#
					parametros[0]=Util.addData(Util.formatedToday().substring(0,10),przPubl-2);
				}
				else parametros[0]=Util.formatedToday().substring(0,10);
				
				int iDias = Util.isFimdeSemana(parametros[0]); // domingo =1; sabado=2 ou 0 
				if (iDias>0) parametros[0]=(Util.addData(parametros[0],iDias));

				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo7_AtasNaoEntregue "+e.getMessage());
		} 
	}	
	
	
	//Passo 8
	public boolean Passo8_AtasApreensao(ParamSistemaBean paramSis,PNT.ROB.Dao daoPnt,Connection conn) throws DaoException, ServiceLocatorException {
		try {				
			AtaDOBean myAta = new AtaDOBean();
			myAta.setTipAtaDO("4");
			myAta.setNomUserNameEDO(NOM_USER);
			myAta.setCodOrgaoLotacaoEDO(paramSis.getParamSist("ORGAO_PONTUACAO"));
			myAta.setDatEDO(Util.formatedToday().substring(0,10));			
			myAta.setDatProcEDO(Util.formatedToday().substring(0,10));
			myAta.setMaxProcessos(NUM_MAX_PROCESSOS);
			myAta.setStatusElegiveis("090,091,092,093,094,100");
			myAta.setDatReferencia(Util.formatedToday().substring(0,10));
			HistoricoBean myHist = new HistoricoBean();
			
			/*Pontu�vel*/
			while (true) {
				myAta.setTipProcessosDO("'P'");
				daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}
			
			/*Mandat�rio*/
			while (true) {
				myAta.setTipProcessosDO("'S'");
				daoPnt.buscaProcesso_GerarAtaDO(myAta,conn);
				if (myAta.getProcessos().size()==0) break;				
				// complemento 02 - Numero da ATA - setar no DAO				
				daoPnt.Inclui_Atas(myAta,myHist,conn);
				// setar variavel para eMail emMsgErro
				myAta.setMsgErro(myAta.getMsgErro()+"\n ATA:"+myAta.getCodEnvioDO()+
						" - "+myAta.getDatEDO()+
						" Processos: "+myAta.getProcessos().size());
			}			
			
			// Enviar eMail
			if (myAta.getMsgErro().length()>0) {	
				//pegar destinatarios nos parametros 
				String[] listaDestinatarios = paramSis.getParamSist("DESTINATARIO_NAO_ENTREGUE_PENALIDADE").split(",");				
				String[] parametros = new String[4];
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.setCodOrgao(myHist.getCodOrgaoLotacao());
				email.setCodEvento(myHist.getCodEvento());
				parametros[0]=Util.addData(Util.formatedToday().substring(0,10),NUM_DIAS_PUB_DO);				
				parametros[3]=myAta.getMsgErro();  // Atas Publicadas #ATAS#
				for(int t=0;t<listaDestinatarios.length;t++){
					email.enviaEmailChangeParam(listaDestinatarios[t], parametros);
				}
			}
			if (myAta.getProcessos().size()==0) return false;
			else
				return true;
		} catch (Exception e) {
			throw new DaoException("Erro Fech Diario - Passo8_AtasApreens�o "+e.getMessage());
		} 
	}	
	
	
	
	
	
	
	public static void main(String args[])  throws DaoException {		
		try {			
			new FechamentoDiarioRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}




