package PNT.ROB;

import java.sql.Connection;

import ACSS.ParamSistemaBean;
import PNT.AtaDOBean;
import PNT.HistoricoBean;
import PNT.NOT.ArquivoBean;
import sys.DaoException;


/**
 * <b>Title:</b>       	SMIT - PNT.NOT.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados de Notificações<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */

public class DaoOra extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoOra() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
			if ((serviceloc.getQualBanco() == null) || serviceloc.getQualBanco().equals(""))
				serviceloc.initParams("../web.xml");			
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	public sys.ServiceLocator getServiceLocator() throws DaoException {
		return this.serviceloc;
	}	
	public String pegarNumProcesso(Connection conn,ParamSistemaBean paramSis) throws DaoException {
		String nProcesso = "";
		Connection conn1 = null;
		try {
			conn1 = serviceloc.getConnection(MYABREVSIST) ;			 
		}	
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn1 != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		 
		return nProcesso;		
	}
	
	public void buscaProcesso_AtasNaoEntregue(AtaDOBean myAta,Connection conn) throws DaoException {
		
	}
	
	public void buscaProcesso_AtasNaoEntregueApreensao(AtaDOBean myAta,Connection conn) throws DaoException {
		
	}
	public void Inclui_Atas(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		
	}
	
	public void Inclui_AtasFechamentoRequerimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		
	}
	
	public void Inclui_AtasDeferimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		
	}
	
	public void Inclui_PenalidadeRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido) 
	throws DaoException {
		
	}
	
	public void buscaProcesso_GerarAtaDO(AtaDOBean myAta,Connection conn) throws DaoException {
	}
	
	public void buscaProcesso_AtasApreensao(AtaDOBean myAta,Connection conn) throws DaoException {
		
	}
	
	public void Inclui_ApreensaoRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido)
	throws DaoException {
		
	}
	
}