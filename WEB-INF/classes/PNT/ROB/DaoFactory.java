package PNT.ROB;


import sys.DaoException;


public class DaoFactory {
	
	private static Dao instance;
	public static sys.ServiceLocator serviceloc ;
	
	public static Dao getInstance() throws sys.DaoException, sys.ServiceLocatorException {
		if (instance == null) {
			try {			
				serviceloc =  sys.ServiceLocator.getInstance();
				if ((serviceloc.getQualBanco() == null) || serviceloc.getQualBanco().equals(""))
					serviceloc.initParams("../web.xml");
				if (serviceloc.getQualBroker().equals("1"))
					instance = new DaoOra();
				else
					instance = new DaoAdabas();       
			}
			catch (Exception e) {
				throw new DaoException(e.getMessage());
			}
		}
        return instance;
    }
}


