package PNT.ROB;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;

import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;
import ACSS.ParamSistemaBean;
import PNT.MultasBean;
import PNT.NotificacaoBean;
import PNT.ProcessoAntBean;
import PNT.ProcessoBean;
import PNT.RequerimentoBean;
import PNT.NOT.ArquivoBean;
import ROB.Dao;
import ROB.FileWriter;
import ROB.RobotException;



/**
 * <b>Title:</b> SMIT - M�dulo Pontua��o<br>
 * <b>Description:</b> Rob� de Gera��o do Arquivo EmitePontoVex<br>
 * <b>Copyright:</b> Copyright (c) 2008<br>
 * <b>Company: </b> MIND Inform�tica <br>
 * @author Wellem Lyra
 */

public class GeraEmitePontoVexRob {
	
	private static final String MYABREVSIST = "PNT";
	private static final String NOM_ROBOT = "PNT.ROB.GeraEmitePontoVexRob";
	private static final String quebraLinha = "\n";
	
	public GeraEmitePontoVexRob() throws DaoException, ServiceLocatorException {
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		FileWriter writer = null;		
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		PNT.ROB.Dao daoPnt = PNT.ROB.DaoFactory.getInstance();
		try {				
			conn = daoPnt.getServiceLocator().getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			connLog = daoPnt.getServiceLocator().getConnection(MYABREVSIST);									
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);			
			try {               
				trava =  Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             						
			ArquivoBean arqRecebido = new ArquivoBean();
			String orgaoPontuacao ="";	
			
			ParamSistemaBean paramSis = new ParamSistemaBean();
			paramSis.setCodSistema("21"); //M�dulo Pontua��o	
			paramSis.PreparaParam(conn);				
			
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean() ;			
			orgaoPontuacao=paramSis.getParamSist("ORGAO_PONTUACAO");			
			
			while (true) { 			
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;		

				arqRecebido.buscarArquivosaGerar(conn);		
				
				if (arqRecebido.getCodArquivo().equals("0")) break ;
				System.out.println("Gerando arquivo: "+ arqRecebido.getNomArquivo()+"Cod: "+arqRecebido.getCodArquivo());
				
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(orgaoPontuacao);
				monitor.gravaMonitor("Inicio do Processamento do Arquivo "+arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				
				String dirDownload = paramSis.getParamSist("DOWN_ARQUIVO_RECEBIDO")	;	
//				In�cio das informa��es para a gera��o do Header				
				UsuarioBeanId.setCodOrgaoAtuacao(orgaoPontuacao);
				arqRecebido.setNumProtocolo(UsuarioBeanId);
				
				/*
				Dao.getInstance().ControleEmiteVEXPNTV1(arqRecebido);
				*/				
				
				arqRecebido.buscarListaProcessos("2");				
				
//				In�cio da l�gica para saber o numero de registros do arquivo==============		
				ProcessoBean myProc  = new ProcessoBean();	
				MultasBean myMulta  = new MultasBean();
				ProcessoAntBean ProcessoAnt  = new ProcessoAntBean();
				int contProc=0;
				int contProcAnt=0;
				int contMultas=0;
				int qtdReg=0;	
				
				for (int i=0; i<arqRecebido.getListaProcessos().size();i++) {
					myProc = arqRecebido.getListaProcessos().get(i);
					contProc=arqRecebido.getListaProcessos().size();
					myProc.LeProcessoAnts();
					contProcAnt+=myProc.getProcAnteriores().size();
					myProc.LeMultas(); //Transacao 946
					contMultas+=myProc.getMultas().size();
				}
				qtdReg=contProc+contProcAnt+contMultas;
				arqRecebido.setQtdLinha(qtdReg);
//				Fim da l�gica para saber o numero de registros do arquivo==============
				
//				Chamada do m�todo em que o arquivo � criado e
//				onde o header � gravado  ==========================
				writer = criarArquivoGrv(arqRecebido, dirDownload); 
//				=================================================== 
				
//				Inicio da grava��o dos dados do layout 2	
				String numNotificacao="";
				String seqNotificacao="";
				int qtdLinhasProc=0;
				for (int i=0; i<arqRecebido.getListaProcessos().size();i++) {				
					qtdLinhasProc++;
					if ((qtdLinhasProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
								qtdLinhasProc+"/"+ qtdReg, monitor.MSG_INFO);
									
					myProc = arqRecebido.getListaProcessos().get(i);
					myProc.LeNotificacoes(); // Transacao 947
					myProc.LeRequerimentos(); // Transacao 945
					
					GeraEmitePontoVexRob.gravaLayout2(writer,myProc,numNotificacao,seqNotificacao,i);					
					
					//Inicio da grava��o dos dados do layout 3					
					ProcessoAnt  = new ProcessoAntBean();					
					myProc.LeProcessoAnts();
					for (int t=0; t<myProc.getProcAnteriores().size();t++) {
						qtdLinhasProc++;
						if ((qtdLinhasProc%200)==0)
							monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
									qtdLinhasProc+"/"+ qtdReg, monitor.MSG_INFO);
						
						ProcessoAnt = myProc.getProcAnteriores().get(t);
						GeraEmitePontoVexRob.gravaLayout3(writer,myProc,ProcessoAnt,numNotificacao,seqNotificacao);						
					}
					
					//Inicio da grava��o dos dados do layout 4				
					myMulta  = new MultasBean();					
					myProc.LeMultas();
					for (int p=0; p<myProc.getMultas().size();p++) {
						qtdLinhasProc++;						
						if ((qtdLinhasProc%200)==0)
							monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
									qtdLinhasProc+"/"+ qtdReg, monitor.MSG_INFO);
						myMulta = myProc.getMultas().get(p);						
						GeraEmitePontoVexRob.gravaLayout4(writer,myProc,myMulta,numNotificacao,seqNotificacao);							
					}
				}
				System.out.println("Fim da gera��o do arquivo!");
				
				if (writer != null)	writer.close();
				
				if (!arqRecebido.UpdateEmitePontoVex(conn))
					throw new DaoException("Erro ao Atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();		
				arqRecebido.setCodArquivo("0");				
				monitor.gravaMonitor("Fim da Gera��o do Arquivo: " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);
				
			}	
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Processar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				
				throw new DaoException(ed.getMessage());
			}
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (Exception ed) {
					throw new DaoException(ed.getMessage());
				}
			}
		}
	}
	
	public static void main(String args[])  throws DaoException {		
		try {			
			new GeraEmitePontoVexRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
	public static FileWriter criarArquivoGrv(ArquivoBean arqRecebido, String dirDestino)
	throws RobotException {
		
		String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();		
		
		//Cria o arquivo
		FileWriter writer = null;
		try {			
			File dir = new File(caminho.substring(0, caminho.lastIndexOf("/")));
			dir.mkdirs();
			
			File arquivo = new File(caminho);
			arquivo.createNewFile();
			writer = new FileWriter(arquivo);
			
			//Grava o header do arquivo	
			if (!arqRecebido.getHeaderGrv().equals("")) {
				writer.write(arqRecebido.getHeaderGrv());
				writer.write(quebraLinha);
			}
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
		return writer;
	}
	public static void gravaGrv(FileWriter writer,String linhaArq) throws RobotException {
		
		try {
			if (writer != null) {			
				writer.write(linhaArq);
				writer.write(quebraLinha);
			}
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public static void gravaLayout2(FileWriter writer,ProcessoBean myProc,String numNotificacao,
			String seqNotificacao,int i) throws RobotException {
		
		try {
			String layout2="2";
//			Inicio da gera��o da linha do layout 2=========================
			numNotificacao = myProc.getNotificacoes().get(0).getNumNotificacao();
			layout2		  += Util.lPad(numNotificacao,"0",9);				//N� da notifica��o
			seqNotificacao = myProc.getNotificacoes().get(0).getSeqNotificacao();
			layout2		  += Util.lPad(seqNotificacao,"0",3);				//Seq da notifica��o
			layout2		  += Util.rPad(myProc.getNumProcesso()," ",20);		//N� do Processo SDD
			//layout2		  += Util.lPad(Util.formataDataYYYYMMDD(myProc.getNotificacoes().get(0).getDatEmissao()),"0",8);//Data de Emiss�o da notifica��o
			layout2		  += Util.lPad(myProc.getNotificacoes().get(0).getDatEmissao(),"0",8);//Data de Emiss�o da notifica��o
			layout2		  += Util.lPad(myProc.getTotPontos(),"0",5);		//Total de Pontos
			layout2		  += Util.lPad(myProc.getQtdProcAnteriores(),"0",5);//Quantidade de Processos Anteriores
			layout2		  += Util.lPad(myProc.getQtdMultas(),"0",5);		//Quantidade de Multas
			layout2		  += Util.rPad(myProc.getNomResponsavel()," ",60);	//Nome do Respons�vel
			layout2		  += Util.lPad(myProc.getNumCPF(),"0",11);			//N�mero do CPF
			layout2		  += Util.lPad(myProc.getTipCNH(),"0",1);			//Tipo da CNH
			layout2		  += Util.lPad(myProc.getNumCNH(),"0",11);			//n�mero da CNH
			layout2		  += Util.rPad(myProc.getUfCNH()," ",2);		   	//UF da CNH
			layout2		  += Util.lPad(Util.formataDataYYYYMMDD(myProc.getDatEmissaoCNH()),"0",8);	//Data de Emiss�o da CNH
			layout2		  += Util.lPad(Util.formataDataYYYYMMDD(myProc.getDatValidadeCNH()),"0",8);	//Data de validade da CNH
			layout2       += Util.rPad(myProc.getCategoriaCNH()," ",2);		//Categoria da CNH
			layout2       += Util.rPad(myProc.getEndereco()," ",60);    	//Endere�o de Destino
			layout2       += Util.lPad(myProc.getCep(),"0",8);				//CEP de Destino
			layout2       += Util.rPad(myProc.getMunicipio()," ",50);		//Municipio
			layout2       += Util.rPad(myProc.getUf()," ",2);				//UF de Destino
			layout2       += Util.rPad(myProc.getNotificacoes().get(0).getTipoReemissao()," ",1);//Tipo de Reemiss�o
			String tipoNotificacao = obterTipoNotificacao(myProc);
			layout2       += Util.lPad(tipoNotificacao,"0",1);//Tipo de notifica��o
			layout2       += Util.rPad(myProc.getTipProcesso()," ",1);		//Tipo do Processo
			if("0".equals(tipoNotificacao))
				layout2   += Util.lPad(Util.formataDataYYYYMMDD(myProc.getUltDatDefesa()),"0",8);		//Prazo para Defesa
			if("2".equals(tipoNotificacao))
				layout2   += Util.lPad(Util.formataDataYYYYMMDD(myProc.getUltDatRecurso()),"0",8);		//Prazo para Recurso
			if("4".equals(tipoNotificacao))
				layout2   += "00000000";			 
			layout2       += Util.lPad(Util.formataDataYYYYMMDD(myProc.getUltDatEntregaCNH()),"0",8);	//Prazo para Entrega da CNH
			
			//Tipo de notifica��o diferente de 0 e com requerimentos
			if ((!tipoNotificacao.equals("0")) && myProc.getRequerimentos() != null){
				setarDataDefesaERecurso(myProc);
			}

			if("2".equals(tipoNotificacao) || "4".equals(tipoNotificacao))
			  layout2   += Util.lPad(Util.formataDataYYYYMMDD(myProc.getDatApresentaDefesa()),"0",8);		//Data Abertura Defesa
			else layout2   += "00000000";
				
			if("4".equals(tipoNotificacao))
			  layout2   += Util.lPad(Util.formataDataYYYYMMDD(myProc.getDatApresentaRecurso()),"0",8);		//Data Abertura Recurso
			else 	layout2   += "00000000";
			
			if("0".equals(tipoNotificacao) ) {
				layout2 += Util.lPad("0","0",5);	//Quantidade de meses Penalidade
				layout2 += Util.rPad(" "," ",100);	//Descri��o da penalidade
			}
			else {
				PNT.TAB.PenalidadeBean myPen = new PNT.TAB.PenalidadeBean();
				myPen.setCodPenalidade(myProc.getCodPenalidade());
				myPen.Le_Penalidade("codigo");
				layout2 += Util.lPad(myPen.getNumPrazo().toString(),"0",5);	//Quantidade de meses Penalidade
				layout2 += Util.rPad(myProc.getDscPenalidade()," ",100);	//Descri��o da penalidade
			}
			
			//Fim altera��o
			
			layout2	+= Util.rPad(" "," ",31);  //Filler
			
			writer.write(layout2);
			writer.write(quebraLinha);
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}

	private static void setarDataDefesaERecurso(ProcessoBean myProc) {
		for (RequerimentoBean req : myProc.getRequerimentos()){
			if (req.getCodTipoSolic().equals("DP")){
				myProc.setDatApresentaDefesa(req.getDatRequerimento());
			}
			if (req.getCodTipoSolic().equals("1P")){
				myProc.setDatApresentaRecurso(req.getDatRequerimento());
			}
			if (req.getCodTipoSolic().equals("2P")){
				myProc.setDatApresentaRecurso(req.getDatRequerimento());
			}
		}
	}
	
	// Obter o tipo de notifica��o maximo Tipos: 0, 2, 4
	private static String obterTipoNotificacao(ProcessoBean myProc) {
		String tipoNotificacao = "0";
		
		if (myProc.getNotificacoes().size() > 1){ 
			for (NotificacaoBean not : myProc.getNotificacoes()){
				if (tipoNotificacao.equals("4"))
					break;
				if (not.getTipoNotificacao().equals("4"))
					tipoNotificacao = "4";
				else if (not.getTipoNotificacao().equals("2"))
					tipoNotificacao = "2";
				else if (not.getTipoNotificacao().equals("0"))
					continue;
			}
		}
		return tipoNotificacao;
	}
	
	public static void gravaLayout3(FileWriter writer,ProcessoBean myProc,ProcessoAntBean ProcessoAnt,String numNotificacao,
			String seqNotificacao) throws RobotException {
		
		try {
			String layout3="3";
//			Inicio dos dados para gera��o do layout 3
			numNotificacao = myProc.getNotificacoes().get(0).getNumNotificacao();
			layout3		  += Util.lPad(numNotificacao,"0",9);				//N� da notifica��o
			seqNotificacao = myProc.getNotificacoes().get(0).getSeqNotificacao();
			layout3		  += Util.lPad(seqNotificacao,"0",3);				//Seq da notifica��o
			layout3	+= Util.rPad(myProc.getNumProcesso()," ",20);		//Numero do Processo SDD
			layout3	+= Util.rPad(ProcessoAnt.getNumProcessoAnt()," ",20);//Numero do Processo Anterior
			layout3	+= Util.lPad(Util.formataDataYYYYMMDD(ProcessoAnt.getDatProcessoAnt()),"0",8);	//Data do Processo Anterior
			layout3	+= Util.rPad(ProcessoAnt.getTipProcessoAnt()," ",1);	//Tipo de Processo Anterior
			layout3	+= Util.lPad(myProc.getTotPontos(),"0",5);			//Total de Pontos
			layout3	+= Util.rPad(ProcessoAnt.getSitProcessoAnt()," ",20);//Situa��o do Processo
			layout3	+= Util.rPad(" "," ",363);  //Filler
			
			writer.write(layout3);
			writer.write(quebraLinha);	
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public static void gravaLayout4(FileWriter writer,ProcessoBean myProc,MultasBean myMulta,String numNotificacao,String seqNotificacao) throws RobotException {
		
		try {
			String layout4="4";
//			//Inicio dos dados para gera��o do layout 4
			

			
			numNotificacao = myProc.getNotificacoes().get(0).getNumNotificacao();
			layout4		  += Util.lPad(numNotificacao,"0",9);				//N� da notifica��o
			seqNotificacao = myProc.getNotificacoes().get(0).getSeqNotificacao();
			layout4		  += Util.lPad(seqNotificacao,"0",3);				//Seq da notifica��o			
			layout4	+= Util.rPad(myMulta.getNumProcesso()," ",20);		//Numero do Processo SDD
			layout4	+= Util.lPad(myMulta.getCodOrgao(),"0",6);	//C�digo do �rgao
			layout4	+= Util.rPad(myMulta.getNumAutoInfracao()," ",12);	//N�mero do Auto
			layout4	+= Util.rPad(myMulta.getNumPlaca()," ",7);	        //N�mero do Auto
			layout4	+= Util.rPad(myMulta.getCodInfracao()," ",5);		//C�digo da Infra��o
			layout4	+= Util.lPad(Util.formataDataYYYYMMDD(myMulta.getDatInfracao()),"0",8);		//Data da Infra��o
			layout4	+= Util.rPad(myMulta.getValHorInfracao()," ",4);	    //Hora da infracao
			layout4	+= Util.rPad(myMulta.getDscLocalInfracao()," ",80);  //Local da Infra��o
			layout4	+= Util.rPad(myMulta.getDscInfracao()," ",80);		//Descri��o da infracao
			layout4	+= Util.rPad(myMulta.getDscEnquadramento()," ",25);  //Enquadramento
			layout4	+= Util.lPad(myMulta.getNumPonto(),"0",2);		    //Pontua��o					
			layout4	+= Util.lPad(Util.formataDataYYYYMMDD(myMulta.getDatTransJulgado()),"0",8); //Data de Transito em Julgado
			layout4	+= Util.rPad(" "," ",180);  //Filler
			
			writer.write(layout4);
			writer.write(quebraLinha);
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
}