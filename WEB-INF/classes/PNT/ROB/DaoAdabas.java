package PNT.ROB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import sys.DaoException;
import sys.Util;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import PNT.AtaDOBean;
import PNT.ProcessoBean;
import PNT.HistoricoBean;
import PNT.NotificacaoBean;
import PNT.RequerimentoBean;
import PNT.NOT.ArquivoBean;


/**
 * <b>Title:</b>       	SMIT - PNT.ROB.Dao<br>
 * <b>Description:</b> 	Objeto de Acesso a Dados - Robot<br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Glaucio Jannotti
 * @version 			1.0
 */
public class DaoAdabas extends sys.DaoBase implements Dao {
	
	private static final String MYABREVSIST = "PNT";
	private  sys.ServiceLocator serviceloc ;
	
	protected DaoAdabas() throws sys.DaoException {
		try {
			serviceloc =  sys.ServiceLocator.getInstance();
			if ((serviceloc.getQualBanco() == null) || serviceloc.getQualBanco().equals(""))
				serviceloc.initParams("../web.xml");
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	public sys.ServiceLocator getServiceLocator() throws DaoException {
		return this.serviceloc;
	}
	public String pegarNumProcesso(Connection conn,ParamSistemaBean paramSis) throws DaoException {
		String numProcesso = "";
		String sCmd        = "";
		try {
			String nAss             = paramSis.getParamSist("ASSUNTO_PROCESSO_PONTUACAO") ;
			String codSec           = paramSis.getParamSist("SECRETARIA_PROCESSO_PONTUACAO") ;			 
			String orgaoProcessoPnt = paramSis.getParamSist("ORGAO_PROCESSO_PONTUACAO") ; 
			String anoProc          = "2007" ;
			sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" where nom_Parametro='PROCESSO_ANO' "+
			" and cod_orgao='"+orgaoProcessoPnt+"' ";
			Statement stmt = conn.createStatement();			
			ResultSet rs  = stmt.executeQuery(sCmd) ;			 			 
			while (rs.next()) {
				anoProc = rs.getString("VAL_PARAMETRO");	  			 			  
			}		 
			sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" where nom_Parametro='NUM_ULT_PROCESSO_PNT' "+
			" and cod_orgao='"+orgaoProcessoPnt+"' FOR UPDATE";	
			rs  = stmt.executeQuery(sCmd) ;
			int numUltProc = 0;		
			while (rs.next()) {
				numUltProc = rs.getInt("VAL_PARAMETRO");	  			 			  
			}
			numUltProc++;
			numProcesso = "000000"+String.valueOf(numUltProc) ;
			numProcesso = numProcesso.substring(numProcesso.length()-6,numProcesso.length());
			if (nAss.length()>0) numProcesso = codSec+"/"+numProcesso+"/"+nAss+"/"+anoProc;
			else                 numProcesso = codSec+"/"+numProcesso+"/"+anoProc;			 
			if(numProcesso.length()>20) numProcesso=numProcesso.substring(0,20);
			// Update na Numeracao			
			sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltProc+"'," +
			" NOM_USERNAME_ALT ='NotificacaoPNTRobot' "+			 
			" where nom_Parametro='NUM_ULT_PROCESSO_PNT' "+
			" and cod_orgao='"+orgaoProcessoPnt+"'";		
			stmt.execute(sCmd);
			stmt.close();
			rs.close();		
			return numProcesso;
		}	
		catch (Exception e) {	
			throw new DaoException(" DaoBrokerAdabas(PNT.ROB)-pegarNumProcesso - "+sCmd);
		}//fim do catch		 
	}
	
	public void buscaProcesso_AtasNaoEntregue(AtaDOBean myAta,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM (SELECT COD_PROCESSO,NUM_PROCESSO,COD_STATUS," +
				   "COD_PENALIDADE,DSC_PENALIDADE,DSC_PENALIDADE_RES,TIPO_PROCESSO " +
				   "FROM TPNT_PROCESSO WHERE  "+
			" COD_STATUS in ("+myAta.getStatusElegiveis()+ ") "+
			" AND ULT_DAT_DEFESA < to_date('"+myAta.getDatReferencia() + "','dd/mm/yyyy') "+
			" AND TIPO_PROCESSO IN ("+myAta.getTipProcessosDO()+") "+
			" ORDER BY NUM_PROCESSO ) WHERE ROWNUM <= "+myAta.getMaxProcessos();
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();		
			ResultSet rs    = stmt.executeQuery(sCmd) ;	
			while (rs.next()) {
				ProcessoBean myProc = new ProcessoBean();				
				myProc.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProc.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProc.setCodStatus(rs.getString("COD_STATUS"));
				myProc.setCodPenalidade(rs.getString("COD_PENALIDADE"));
				myProc.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
				myProc.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
				myProc.setTipProcesso(rs.getString("TIPO_PROCESSO"));
				processos.add(myProc);
			}
			myAta.setProcessos(processos);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-buscaProcesso_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}
	}

	public void buscaProcesso_AtasNaoEntregueApreensao(AtaDOBean myAta,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM (SELECT COD_PROCESSO,NUM_PROCESSO,COD_STATUS," +
				   "COD_PENALIDADE,DSC_PENALIDADE,DSC_PENALIDADE_RES,TIPO_PROCESSO " +
				   "FROM TPNT_PROCESSO WHERE  "+
			" COD_STATUS in ("+myAta.getStatusElegiveis()+ ") "+
			" AND ULT_DAT_ENTREGACNH < to_date('"+myAta.getDatReferencia() + "','dd/mm/yyyy') "+
			" AND TIPO_PROCESSO IN ("+myAta.getTipProcessosDO()+") "+
			" ORDER BY NUM_PROCESSO ) WHERE ROWNUM <= "+myAta.getMaxProcessos();
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();		
			ResultSet rs    = stmt.executeQuery(sCmd) ;	
			while (rs.next()) {
				ProcessoBean myProc = new ProcessoBean();				
				myProc.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProc.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProc.setCodStatus(rs.getString("COD_STATUS"));
				myProc.setCodPenalidade(rs.getString("COD_PENALIDADE"));
				myProc.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
				myProc.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
				myProc.setTipProcesso(rs.getString("TIPO_PROCESSO"));
				processos.add(myProc);
			}
			myAta.setProcessos(processos);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-buscaProcesso_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}
	}
	
	public void buscaProcesso_AtasApreensao(AtaDOBean myAta,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM (SELECT COD_PROCESSO,NUM_PROCESSO,COD_STATUS FROM TPNT_PROCESSO WHERE  "+
			" COD_STATUS in ("+myAta.getStatusElegiveis()+ ") "+
			" AND ULT_DAT_RECURSO <= to_date('"+myAta.getDatReferencia() + "','dd/mm/yyyy') "+
			" AND TIPO_PROCESSO IN ("+myAta.getTipProcessosDO()+") "+
			" ORDER BY NUM_PROCESSO ) WHERE ROWNUM <= "+myAta.getMaxProcessos() ;
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();		
			ResultSet rs    = stmt.executeQuery(sCmd) ;	
			while (rs.next()) {
				ProcessoBean myProc = new ProcessoBean();				
				myProc.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProc.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProc.setCodStatus(rs.getString("COD_STATUS"));				
				processos.add(myProc);
			}
			myAta.setProcessos(processos);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-buscaProcesso_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}
	}

	public void buscaProcesso_GerarAtaDO(AtaDOBean myAta,Connection conn) throws DaoException {
		String sCmd        = "";
		try  {	
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM (SELECT COD_PROCESSO,NUM_PROCESSO,COD_STATUS,to_char(DAT_STATUS,'dd/mm/yyyy') as DAT_STATUS FROM TPNT_PROCESSO WHERE  "+
			" COD_STATUS in ("+myAta.getStatusElegiveis()+ ") "+
			" AND TIPO_PROCESSO IN ("+myAta.getTipProcessosDO()+") ";
			
			if (myAta.getTipAtaDO().equals("4")) /*Apreens�o*/
			 sCmd+=" AND ULT_DAT_ENTREGACNH <= to_date('"+myAta.getDatReferencia() + "','dd/mm/yyyy') "+
			       " AND DAT_ARQ_APREENSAO IS NULL ";
			
			sCmd+=" ORDER BY NUM_PROCESSO ) WHERE ROWNUM <= "+myAta.getMaxProcessos();
			List<ProcessoBean> processos = new ArrayList<ProcessoBean>();		
			ResultSet rs    = stmt.executeQuery(sCmd) ;	
			while (rs.next()) {
				ProcessoBean myProc = new ProcessoBean();				
				myProc.setCodProcesso(rs.getString("COD_PROCESSO"));
				myProc.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myProc.setCodStatus(rs.getString("COD_STATUS"));
				myProc.setDatStatus(rs.getString("DAT_STATUS"));
				processos.add(myProc);
			}
			myAta.setProcessos(processos);
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-buscaProcesso_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}
	}
	
	public void Inclui_Atas(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT SEQ_PNT_ATA_ENVIO_DO.NEXTVAL AS CODIGO FROM DUAL";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			if(rs.next()) {
				myAta.setCodEnvioDO(rs.getString("CODIGO"));
			}			
			myHist.setTxtComplemento02(myAta.getCodEnvioDO());
			
			for (int i=0; i<myAta.getProcessos().size();i++)   {
				sCmd  =  "INSERT INTO TPNT_ATA_DO " +
				"(COD_ATA_DO,TIP_ATA,COD_PROCESSO,COD_ENVIO_DO, "+
				"DAT_EDO,NOM_USERNAME_EDO,COD_ORGAO_LOTACAO_EDO,"+
				"DAT_PROC_EDO,TIPO_PROCESSO) VALUES (seq_pnt_ata_DO.nextval, "+
				" '"+myAta.getTipAtaDO()+"',"+
				" '"+myAta.getProcessos().get(i).getCodProcesso()+"','"+myAta.getCodEnvioDO()+"', "+
				" to_date('"+myAta.getDatEDO()+"','dd/mm/yyyy'), "+
				" '"+myAta.getNomUserNameEDO() +"','"+myAta.getCodOrgaoLotacaoEDO()+"', "+
				" to_date('"+myAta.getDatProcEDO()+"','dd/mm/yyyy'),"+myAta.getTipProcessosDO()+") ";								
				stmt.execute(sCmd) ;	
				// Atualizar Processo
				sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
				" COD_STATUS = '"+myAta.getStatusDestino() +"',"+
				" DAT_STATUS = to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')"+				 
				" WHERE COD_PROCESSO='"+myAta.getProcessos().get(i).getCodProcesso()+"' ";
				stmt.execute(sCmd) ;	
				// Atualizar Historico
				myHist.setCodProcesso(myAta.getProcessos().get(i).getCodProcesso());
				myHist.setNumProcesso(myAta.getProcessos().get(i).getNumProcesso());
				myHist.GravaHistorico(conn);
				// Evento 003 - Autua��o s/ sucesso,Evento 033 - Penal. s/ sucesso,Evento 063 - "Entg CNH" s/ sucesso
			}		
			conn.commit();
			stmt.close();			
		} catch (SQLException e) {
			throw new DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}	
	}

	
	public void Inclui_AtasFechamentoRequerimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			Statement stmt = conn.createStatement();
			RequerimentoBean myReq = new RequerimentoBean();
			for (int i=0; i<myAta.getProcessos().size();i++)   {
				// Atualizar Processo
				myAta.getProcessos().get(i).LeRequerimentos();
				String tipoResultado="";
				for (int iReq=0;iReq<myAta.getProcessos().get(i).getRequerimentos().size();iReq++) {
					/*Defesa*/
					myReq = new RequerimentoBean();
					myReq=myAta.getProcessos().get(i).getRequerimentos(iReq);
					int iDias=Util.DifereDias(myReq.getDatRS(),myAta.getDatReferenciaFinal());
					if  (myAta.getProcessos().get(i).getCodStatus().equals("012")) {
						if ( (myReq.getCodTipoSolic().equals("DP")) &&
								(myReq.getCodStatusRequerimento().equals("3")) && (iDias>=0)  
						)
						{
							tipoResultado=myAta.getProcessos().get(i).getRequerimentos(iReq).getCodResultRS();
							myReq.setCodStatusRequerimento("9");
							break;
						}
						
					}
					/*Recurso*/
					if  (myAta.getProcessos().get(i).getCodStatus().equals("042")) {
						if ( (myReq.getCodTipoSolic().equals("RP")) &&
								(myReq.getCodStatusRequerimento().equals("3")) && (iDias>=0)  
						)
						{
							tipoResultado=myAta.getProcessos().get(i).getRequerimentos(iReq).getCodResultRS();
							myReq.setCodStatusRequerimento("9");
							break;
						}
					}
				}

				
				if (tipoResultado.length()>0)
				{
					/*Defesa*/
					if (myAta.getProcessos().get(i).getCodStatus().equals("012"))
					{
						if (tipoResultado.equals("I")) {
							myAta.setStatusDestino("019");
							myHist.setCodStatus("019");							
							myHist.setCodEvento("813"); /*Fechamento de Requerimento*/
							myHist.setTxtComplemento02(myReq.getNumRequerimento());
						}
						else
						{
							myAta.setStatusDestino("018");
							myHist.setCodStatus("018");							
							myHist.setCodEvento("813"); /*Fechamento de Requerimento*/
							myHist.setTxtComplemento02(myReq.getNumRequerimento());
						}
					}
					
					/*Recurso*/
					if (myAta.getProcessos().get(i).getCodStatus().equals("042")) {
						if (tipoResultado.equals("I")) {
							myAta.setStatusDestino("049");
							myHist.setCodStatus("049");							
							myHist.setCodEvento("843"); /*Fechamento de Requerimento*/
							myHist.setTxtComplemento02(myReq.getNumRequerimento());
						}
						else
						{
							myAta.setStatusDestino("048");
							myHist.setCodStatus("048");							
							myHist.setCodEvento("843"); /*Fechamento de Requerimento*/
							myHist.setTxtComplemento02(myReq.getNumRequerimento());							
						}
					}
					
					sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
					" COD_STATUS = '"+myAta.getStatusDestino() +"',"+
					" DAT_STATUS = to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')"+				 
					" WHERE COD_PROCESSO='"+myAta.getProcessos().get(i).getCodProcesso()+"' ";
					stmt.execute(sCmd) ;	
					// Atualizar Historico
					myHist.setCodProcesso(myAta.getProcessos().get(i).getCodProcesso());
					myHist.setNumProcesso(myAta.getProcessos().get(i).getNumProcesso());
					myHist.GravaHistorico(conn);
					myReq.AtualizarRequerimento(conn);
					
				}
				
				// Evento 003 - Autua��o s/ sucesso,Evento 033 - Penal. s/ sucesso,Evento 063 - "Entg CNH" s/ sucesso				
			}		
			/*conn.commit();*/
			stmt.close();			
		} catch (SQLException e) {
			throw new DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}	
	}

	public void Inclui_AtasDeferimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException {
		String sCmd        = "";
		try {	
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			for (int i=0; i<myAta.getProcessos().size();i++)   {

				if (Util.DifereDias(myAta.getProcessos().get(i).getDatStatus(),myAta.getDatReferenciaFinal())<0) 
				  continue;

				// Atualizar Processo
				/*Defesa*/
				if (myAta.getProcessos().get(i).getCodStatus().equals("018"))
				{
						myAta.setStatusDestino("995");
						myHist.setCodStatus("995");						
						myHist.setCodEvento("821"); /*Transitado - Deferimento de Defesa*/
				}
				/*Recurso*/
				if (myAta.getProcessos().get(i).getCodStatus().equals("048")) {
						myAta.setStatusDestino("996");
						myHist.setCodStatus("996");						
						myHist.setCodEvento("851"); /*Transitado - Deferimento de Recurso*/						
				}
				
				sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
				" COD_STATUS = '"+myAta.getStatusDestino() +"',"+
				" DAT_STATUS = to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')"+				 
				" WHERE COD_PROCESSO='"+myAta.getProcessos().get(i).getCodProcesso()+"' ";
				stmt.execute(sCmd) ;	
				// Atualizar Historico
				myHist.setCodProcesso(myAta.getProcessos().get(i).getCodProcesso());
				myHist.setNumProcesso(myAta.getProcessos().get(i).getNumProcesso());
				myHist.GravaHistorico(conn);
				
				// Evento 003 - Autua��o s/ sucesso,Evento 033 - Penal. s/ sucesso,Evento 063 - "Entg CNH" s/ sucesso				
			}		
			conn.commit();
			stmt.close();			
		} catch (SQLException e) {
			throw new DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+e.getMessage()+" - "+sCmd);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Update_AtasNaoEntregue : "+ex.getMessage()+" - "+sCmd);
		}	
	}
	
	
	
	
	public void Inclui_PenalidadeRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido)
	throws DaoException {
		String sCmd    = "";
		String myNotif = "";
		String mySeqs  = "0";
		try {	
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs   = null;
			ProcessoBean  myProcesso = new ProcessoBean();
			NotificacaoBean  myNotificacao = new NotificacaoBean();
			NotificacaoBean  myNotifPenal  = new NotificacaoBean();
			boolean existePenal = false;
			String sNumNotif = "";
			GeraCodigoBarraBean geraCB = new GeraCodigoBarraBean();
			geraCB.setParam(paramSis);
			geraCB.setDataCB(myAta.getDatProcEDO());
			for (int i=0; i<myAta.getProcessos().size();i++)   {
				myProcesso = myAta.getProcessos().get(i);
				// Define Penalidade para o Processo padr�o
				
				if (myProcesso.getCodStatus().equals("004"))
				{
					myProcesso.setCodPenalidade(paramSis.getParamSist("PENALIDADE_PADRAO_CODIGO"));
					myProcesso.setDscPenalidade(paramSis.getParamSist("PENALIDADE_PADRAO_DESCRICAO"));
					myProcesso.setDscPenalidadeRes(paramSis.getParamSist("PENALIDADE_PADRAO_DESCRICAO_RES"));
					
					/*Processos Pontu�veis-Testar*/
					if (myProcesso.getTipProcesso().equals("P")) {
						myProcesso.LeMultas();
						int iFatorAgravamento=0;
						int iCodAgravo=0;
						for (int iMultas=0;iMultas<myProcesso.getMultas().size();iMultas++) {
							sCmd = "SELECT FATOR_AGRAVAMENTO FROM TPNT_AGRAVO "+
							"WHERE COD_INFRACAO='"+myProcesso.getMultas().get(iMultas).getCodInfracao()+"'";
							rs  = stmt.executeQuery(sCmd);
							if (rs.next()) {iCodAgravo=rs.getInt("FATOR_AGRAVAMENTO");}
							if (iCodAgravo>iFatorAgravamento) iFatorAgravamento=iCodAgravo;
						}
						
						sCmd = "SELECT * FROM TPNT_PENALIDADE "+
						"WHERE " +
						myProcesso.getMultas().size()+"<=QTD_INFRACAO_ATE AND "+
						"COD_FATOR_AGRAVAMENTO='"+iFatorAgravamento+"'";
						rs  = stmt.executeQuery(sCmd);
						if (rs.next()) {
							myProcesso.setCodPenalidade(rs.getString("COD_PENALIDADE"));
							myProcesso.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
							myProcesso.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
						}
					}
					else /*SDD*/
					{
						myProcesso.LeMultas();
						for (int iMultas=0;iMultas<myProcesso.getMultas().size();iMultas++) {
							sCmd = "SELECT COD_PENALIDADE FROM TPNT_PENAL_SDD "+
							"WHERE COD_INFRACAO='"+myProcesso.getMultas().get(iMultas).getCodInfracao()+"'";
							rs  = stmt.executeQuery(sCmd) ;
							if (rs.next()) {myProcesso.setCodPenalidade(rs.getString("COD_PENALIDADE"));}
						}
						
						sCmd = "SELECT * FROM TPNT_PENALIDADE "+
						"WHERE COD_PENALIDADE='"+myProcesso.getCodPenalidade()+"'";
						rs  = stmt.executeQuery(sCmd);
						if (rs.next()) {
							myProcesso.setDscPenalidade(rs.getString("DSC_PENALIDADE"));
							myProcesso.setDscPenalidadeRes(rs.getString("DSC_PENALIDADE_RES"));
						}
					}
				}
				
				// Le Notificacoes do Processo
				myProcesso.LeNotificacoes();				
				// Se j� existirem Notificacoes de Penalidade - apaga para gerar novamente
				existePenal = false;
				for (int n=0; n<myProcesso.getNotificacoes().size();n++)   {
					if ("2".equals(myProcesso.getNotificacoes().get(n).getTipoNotificacao())) {
						existePenal = true;
						/*N�o posso excluir - reemiss�o/ajuste de status 
						myProcesso.getNotificacoes().get(n).ExcluirNotificacao(conn);
						*/
					}
				}
				String sLinhaArquivo = "";
				// Gera Notificacoes de Penalidade para o Processo a partir da Autuacao
				/*
                ("000".equals(myProcesso.getNotificacoes().get(n).getSeqNotificacao())==false)
				*/
				for (int n=0; n<myProcesso.getNotificacoes().size();n++)   {
					if ("0".equals(myProcesso.getNotificacoes().get(n).getTipoNotificacao())) {
						myNotificacao = myProcesso.getNotificacoes().get(n);
						myNotifPenal  = new NotificacaoBean();
						myNotifPenal.setCodProcesso(myNotificacao.getCodProcesso());
						myNotifPenal.setTipoNotificacao("2");						
						sNumNotif = "000000000"+myNotificacao.getNumNotificacao();
						sNumNotif = "2"+sNumNotif.substring(sNumNotif.length()-8,sNumNotif.length());

						/*Verifica se Existe Notifica��o*/
						String numNotif=Existe_Pen_Apre_Registrada(myProcesso,myNotifPenal,conn);
						if (!numNotif.equals("0"))	sNumNotif=numNotif;
						
						myNotif=sNumNotif;
						mySeqs=String.valueOf(Integer.parseInt(mySeqs)+1);
						myNotifPenal.setNumNotificacao(sNumNotif);
						myNotifPenal.setSeqNotificacao(myNotificacao.getSeqNotificacao());
						myNotifPenal.setCodArquivo(arqRecebido.getCodArquivo());
						myNotifPenal.setTipoReemissao("E");
						if (existePenal) myNotifPenal.setTipoReemissao("R");	
						myNotifPenal.setTipoNotificacao("2");
						myNotifPenal.setDatEmissao(myAta.getDatProcEDO());
						myNotifPenal.setNomUserName(myAta.getNomUserNameEDO());
						myNotifPenal.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
						myNotifPenal.setDatProc(myAta.getDatProcEDO());
						try	{
							geraCB.setNumero(myNotifPenal.getNumNotificacao()+myNotifPenal.getSeqNotificacao());
							geraCB.setTexto(myNotifPenal.getNumNotificacao()+"-"+myNotifPenal.getSeqNotificacao());
							geraCB.drawingBarcodeDirectToGraphics();
						}
						catch (Exception e) {
							throw new DaoException(e.getMessage()+" DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada - Geracao Codigo Barra");						
						}					
						myNotifPenal.setDatGeracaoCB(myNotifPenal.getDatProc());
						// Grava Notificacoes de Penalidade
						
						myNotifPenal.GravarNotificacao(conn);
						
						sLinhaArquivo="2"+myNotifPenal.getNumNotificacao()+
						Util.rPad(""," ",20)+
						myNotifPenal.getSeqNotificacao()+myNotifPenal.getTipoNotificacao()+
						sys.Util.formataDataYYYYMMDD(myNotifPenal.getDatEmissao());
						arqRecebido.getLinhasArquivo().add(sLinhaArquivo);
					}
				}				
				// Atualizar Processo
				sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
				" COD_STATUS          = '"+myAta.getStatusDestino() +"',"+
				" DAT_STATUS          = to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'), ";
				if (myProcesso.getCodStatus().equals("004")) {
					sCmd+=" COD_PENALIDADE      = '"+myProcesso.getCodPenalidade() +"', "+
					" DSC_PENALIDADE      = '"+myProcesso.getDscPenalidade() +"', "+
					" DSC_PENALIDADE_RES  = '"+myProcesso.getDscPenalidadeRes() +"', ";
				}
				sCmd+=" ULT_DAT_RECURSO     = '"+myAta.getDatReferenciaFinal() +"', "+
				" ULT_DAT_ENTREGACNH  = '"+myAta.getDatReferenciaFinal() +"' "+
				" WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ";
				stmt.execute(sCmd) ;	
				// Atualizar Historico
				// Compl 01 - Numero da Notifica��o gerada
				// Compl 02 - Quantidade de Sequencias
				// Compl 03 - Ultima  Data de Recurso
				// Compl 04 - Codigo da Penalidade
				// Compl 05 - Descri��o da Penalidade	
				// Compl 06 - Ultima  Data de Entrega CNH
				myHist.setCodStatus("030");				
				myHist.setCodEvento("830");
				myHist.setCodProcesso(myProcesso.getCodProcesso());
				myHist.setNumProcesso(myProcesso.getNumProcesso());
				myHist.setTxtComplemento01(myNotif);
				myHist.setTxtComplemento02(mySeqs);
				// Compl 03 - Ultima  Data de Recurso - ja atualizado
				myHist.setTxtComplemento04(myProcesso.getCodPenalidade());
				myHist.setTxtComplemento05(myProcesso.getDscPenalidade());
				// Compl 06 - Ultima  Data de Entrega CNH - ja atualizado				
				myHist.GravaHistorico(conn);
				

				// Atualizar Historico
				// Compl 01 - Numero da Notifica��o gerada
				// Compl 02 - Quantidade de Sequencias
				// Compl 03 - Ultima  Data de Recurso
				// Compl 04 - Codigo da Penalidade
				// Compl 05 - Descri��o da Penalidade	
				// Compl 06 - Ultima  Data de Entrega CNH
				myHist.setCodStatus("031");				
				myHist.setCodEvento("831");
				myHist.setCodProcesso(myProcesso.getCodProcesso());
				myHist.setNumProcesso(myProcesso.getNumProcesso());
				myHist.setTxtComplemento01(myNotif);
				myHist.setTxtComplemento02(mySeqs);
				// Compl 03 - Ultima  Data de Recurso - ja atualizado
				myHist.setTxtComplemento04(myProcesso.getCodPenalidade());
				myHist.setTxtComplemento05(myProcesso.getDscPenalidade());
				// Compl 06 - Ultima  Data de Entrega CNH - ja atualizado				
				myHist.GravaHistorico(conn);
			}
			conn.commit();
			stmt.close();			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada - "+sCmd);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada : "+ex.getMessage());
		}	
	}
	
	public void Inclui_ApreensaoRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido)
	throws DaoException {
		String sCmd    = "";
		String myNotif = "";
		String mySeqs  = "0";
		try {	
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ProcessoBean  myProcesso = new ProcessoBean();
			NotificacaoBean  myNotificacao = new NotificacaoBean();
			NotificacaoBean  myNotifApreensao  = new NotificacaoBean();
			boolean existeApreensao = false;
			String sNumNotif = "";
			GeraCodigoBarraBean geraCB = new GeraCodigoBarraBean();
			geraCB.setParam(paramSis);
			geraCB.setDataCB(myAta.getDatProcEDO());
			for (int i=0; i<myAta.getProcessos().size();i++)   {
				myProcesso = myAta.getProcessos().get(i);
				
				// Le Notificacoes do Processo
				myProcesso.LeNotificacoes();				
				// Se j� existirem Notificacoes de Apreens�o - apaga para gerar novamente
				existeApreensao = false;
				for (int n=0; n<myProcesso.getNotificacoes().size();n++)   {
					if ("4".equals(myProcesso.getNotificacoes().get(n).getTipoNotificacao())) {
						existeApreensao = true;
						/*N�o posso excluir - reemiss�o/ajuste de status
						myProcesso.getNotificacoes().get(n).ExcluirNotificacao(conn);
						*/
					}
				}
				String sLinhaArquivo = "";
				// Gera Notificacoes de Penalidade para o Processo a partir da Penalidade
				/*
                ("000".equals(myProcesso.getNotificacoes().get(n).getSeqNotificacao())==false)
				*/
				for (int n=0; n<myProcesso.getNotificacoes().size();n++)   {
					if (
						("2".equals(myProcesso.getNotificacoes().get(n).getTipoNotificacao())) &&
						("001".equals(myProcesso.getNotificacoes().get(n).getSeqNotificacao()))
						) {
						myNotificacao = myProcesso.getNotificacoes().get(n);
						myNotifApreensao  = new NotificacaoBean();
						myNotifApreensao.setCodProcesso(myNotificacao.getCodProcesso());
						myNotifApreensao.setTipoNotificacao("4");						
						sNumNotif = "000000000"+myNotificacao.getNumNotificacao();
						sNumNotif = "4"+sNumNotif.substring(sNumNotif.length()-8,sNumNotif.length());
						
						/*Verifica se Existe Notifica��o*/
						String numNotif=Existe_Pen_Apre_Registrada(myProcesso,myNotifApreensao,conn);
						if (!numNotif.equals("0"))	sNumNotif=numNotif;
						
						myNotif=sNumNotif;
						mySeqs=String.valueOf(Integer.parseInt(mySeqs)+1);
						myNotifApreensao.setNumNotificacao(sNumNotif);
						myNotifApreensao.setSeqNotificacao(myNotificacao.getSeqNotificacao());
						myNotifApreensao.setCodArquivo(arqRecebido.getCodArquivo());
						myNotifApreensao.setTipoReemissao("E");
						if (existeApreensao) myNotifApreensao.setTipoReemissao("R");	
						myNotifApreensao.setTipoNotificacao("4");
						myNotifApreensao.setDatEmissao(myAta.getDatProcEDO());
						myNotifApreensao.setNomUserName(myAta.getNomUserNameEDO());
						myNotifApreensao.setCodOrgaoLotacao(myAta.getCodOrgaoLotacaoEDO());
						myNotifApreensao.setDatProc(myAta.getDatProcEDO());
						try	{
							geraCB.setNumero(myNotifApreensao.getNumNotificacao()+myNotifApreensao.getSeqNotificacao());
							geraCB.setTexto(myNotifApreensao.getNumNotificacao()+"-"+myNotifApreensao.getSeqNotificacao());
							geraCB.drawingBarcodeDirectToGraphics();
						}
						catch (Exception e) {
							throw new DaoException(e.getMessage()+" DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada - Geracao Codigo Barra");						
						}					
						myNotifApreensao.setDatGeracaoCB(myNotifApreensao.getDatProc());
						// Grava Notificacoes de Penalidade
						
						myNotifApreensao.GravarNotificacao(conn);
						
						sLinhaArquivo="2"+myNotifApreensao.getNumNotificacao()+
						Util.rPad(""," ",20)+
						myNotifApreensao.getSeqNotificacao()+myNotifApreensao.getTipoNotificacao()+
						sys.Util.formataDataYYYYMMDD(myNotifApreensao.getDatEmissao());
						arqRecebido.getLinhasArquivo().add(sLinhaArquivo);
					}
				}				
				// Atualizar Processo
				sCmd  =  "UPDATE TPNT_PROCESSO SET  " +
				" COD_STATUS          = '"+myAta.getStatusDestino() +"',"+
				" DAT_STATUS          = to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'), "+
				" ULT_DAT_ENTREGACNH  = '"+myAta.getDatReferenciaFinal() +"' "+				
				" WHERE COD_PROCESSO='"+myProcesso.getCodProcesso()+"' ";
				stmt.execute(sCmd) ;
				
				// Atualizar Historico
				// Compl 01 - Numero da Notifica��o gerada
				// Compl 02 - Quantidade de Sequencias
				myHist.setCodStatus("090");
				myHist.setCodEvento("890");
				myHist.setCodProcesso(myProcesso.getCodProcesso());
				myHist.setNumProcesso(myProcesso.getNumProcesso());
				myHist.setTxtComplemento01(myNotif);
				myHist.setTxtComplemento02(mySeqs);
				myHist.GravaHistorico(conn);				
				
				// Atualizar Historico
				// Compl 01 - Numero da Notifica��o gerada
				// Compl 02 - Quantidade de Sequencias
				myHist.setCodStatus("091");				
				myHist.setCodEvento("891");				
				myHist.setCodProcesso(myProcesso.getCodProcesso());
				myHist.setNumProcesso(myProcesso.getNumProcesso());
				myHist.setTxtComplemento01(myNotif);
				myHist.setTxtComplemento02(mySeqs);
				myHist.GravaHistorico(conn);				
			}
			conn.commit();
			stmt.close();			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage()+" DaoBrokerAdabas(PNT.ROB)-Inclui_ApreensaoRegistrada - "+sCmd);
		}
		catch (Exception ex) {
			throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Inclui_ApreensaoRegistrada : "+ex.getMessage());
		}	
	}
	
	public void AtualizarProcesso(ProcessoBean myProcesso,RequerimentoBean myReq,HistoricoBean myHis,ParamSistemaBean parSis,UsuarioBean UsrLogado) throws DaoException {		
		String resultado ="";
		String parteVar = "";		
		try {			
			
			parteVar = sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myHis.getDatProc()),"0",8)  + 
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +	
			sys.Util.lPad(myProcesso.getNumCPF(),"0",11) +	
			sys.Util.lPad(myProcesso.getNumNotificacao(),"0",9) +
			sys.Util.rPad(myProcesso.getNumProcesso()," ",20) +
			sys.Util.lPad(sys.Util.formataDataYYYYMMDD(myProcesso.getDatProcesso()),"0",8) +
			sys.Util.rPad(myHis.getTxtComplemento01()," ",1000) ;
			resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",UsrLogado);
			
			
			if (verErro(myProcesso,resultado,"Erro -  (Transa��o: "+myHis.getCodEvento()+" ).") ==false)  {
				return;
			}
		}
		catch (Exception e) {
			throw new DaoException("DaoAdabas - AtualizarProcesso: "+e.getMessage());
		}		
	}
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	throws DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			transBRK.setNomUserName(myUsuario.getNomUserName());
			
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myProc,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	
	 public boolean verErro(ProcessoBean myProc,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk    = false ;
		 String codErro = resultado.substring(0,3);
		 try {
			 if ((codErro.equals("000")==false) &&
					 (codErro.equals("037")==false)) {
				 if ( (codErro.equals("ERR")) || (sys.Util.isNumber(codErro)==false) ) {
					 myProc.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 codErro=Util.lPad(codErro.trim(),"0",3);
					 myProc.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'" ;
					 ResultSet rs   = stmt.executeQuery(sCmd) ;
					 while (rs.next()) {
						 myProc.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					 }
					 rs.close();
				 }
			 }
			 else   bOk = true ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
	 
	 
	 
	 
	 public String Existe_Pen_Apre_Registrada(ProcessoBean myProcesso,NotificacaoBean myNotif,Connection conn) throws DaoException {
		 int iNumNotif=0;
		 try {	
			 
			 Statement stmt = conn.createStatement();
			 ResultSet rs   = null;				   
			 
			 String sCmd = "SELECT MAX(NUM_NOTIFICACAO) AS NUM_NOTIFICACAO FROM TPNT_NOTIFICACAO T1,TPNT_PROCESSO T2 "+
			 "WHERE T1.COD_PROCESSO=T2.COD_PROCESSO " +
			 "AND T2.NUM_PROCESSO='"+ myProcesso.getNumProcesso()+"' "+			
			 "AND T1.TIP_NOTIFICACAO='"+ myNotif.getTipoNotificacao()+"'";
			 rs  = stmt.executeQuery(sCmd);
			 if (rs.next()) {
				 iNumNotif=rs.getInt("NUM_NOTIFICACAO");				
			 }	
			 
			 if (iNumNotif>0) iNumNotif+=10000000;
	
			 
			 rs.close();
			 stmt.close();			
		 } catch (SQLException e) {
			 throw new DaoException(e.getMessage()+" DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada - ");
		 }
		 catch (Exception ex) {
			 throw new sys.DaoException("DaoBrokerAdabas(PNT.ROB)-Inclui_PenalidadeRegistrada : "+ex.getMessage());
		 }
		 return Integer.toString(iNumNotif);
	 }	 
	 
	 
	 
	 
	
}