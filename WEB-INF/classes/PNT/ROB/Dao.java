package PNT.ROB;

import java.sql.Connection;

import ACSS.ParamSistemaBean;
import PNT.AtaDOBean;
import PNT.HistoricoBean;
import PNT.NOT.ArquivoBean;
import sys.DaoException;

public interface Dao {
	public abstract sys.ServiceLocator getServiceLocator() throws DaoException ;
	public abstract String pegarNumProcesso(Connection conn,ParamSistemaBean paramSis) throws DaoException ;

	public abstract void buscaProcesso_AtasNaoEntregue(AtaDOBean myAta,Connection conn) throws DaoException ;
	public abstract void buscaProcesso_AtasNaoEntregueApreensao(AtaDOBean myAta,Connection conn) throws DaoException ;	
	public abstract void buscaProcesso_GerarAtaDO(AtaDOBean myAta,Connection conn) throws DaoException ;
	public abstract void buscaProcesso_AtasApreensao(AtaDOBean myAta,Connection conn) throws DaoException;	
	public abstract void Inclui_Atas(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException ;
	public abstract void Inclui_AtasFechamentoRequerimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException;
	public abstract void Inclui_AtasDeferimento(AtaDOBean myAta,HistoricoBean myHist,Connection conn) throws DaoException;	
	public abstract void Inclui_PenalidadeRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido)
	throws DaoException ;
	
	public abstract void Inclui_ApreensaoRegistrada(AtaDOBean myAta,HistoricoBean myHist,Connection conn,ParamSistemaBean paramSis,ArquivoBean arqRecebido)
	throws DaoException;
	
}

