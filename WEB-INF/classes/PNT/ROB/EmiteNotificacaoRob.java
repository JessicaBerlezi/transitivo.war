package PNT.ROB;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import sys.BeanException;
import sys.DaoException;
import sys.MonitorLog;
import sys.ServiceLocatorException;
import sys.Util;
import ACSS.OrgaoBean;
import ACSS.ParamSistemaBean;
import PNT.HistoricoBean;
import PNT.MultasBean;
import PNT.NotificacaoBean;
import PNT.ProcessoAntBean;
import PNT.ProcessoBean;
import PNT.NOT.ArquivoBean;
import PNT.NOT.ArquivoLinhaEmitePontoBean;
import PNT.TAB.PenalidadeBean;
import ROB.Dao;



/**
 * <b>Title:</b> SMIT - M�dulo Pontua��o<br>
 * <b>Description:</b> Rob� de Emiss�o de Notifica��es<br>
 * <b>Copyright:</b> Copyright (c) 2006<br>
 * <b>Company: </b> MIND Inform�tica <br>
 * @author Glaucio Jannotti
 */

public class EmiteNotificacaoRob {
	
	private static final String MYABREVSIST = "PNT";
	private static final String NOM_ROBOT = "PNT.ROB.EmiteNotificacaoRob";
	public static final String QUEBRA_LINHA = "\n";
	private static final String NOM_USER = "NotificacaoPNTRobot";	
	
	
	public EmiteNotificacaoRob() throws DaoException, ServiceLocatorException {
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		PNT.ROB.Dao daoPnt = DaoFactory.getInstance();
		ArquivoBean arqRecebidoEmitePonto = new ArquivoBean();
		ArquivoBean arqRecebidoEmitePontoPer = new ArquivoBean();
		
		try {				
			conn = daoPnt.getServiceLocator().getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			connLog = daoPnt.getServiceLocator().getConnection(MYABREVSIST);									
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				//trava =  Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             						
			
			while (true) {					
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					break;
				}
				
				carregaEmitePonto(conn, arqRecebidoEmitePonto);
				carregaEmitePontoPerrone(conn, arqRecebidoEmitePontoPer);
				ParamSistemaBean paramSis = preparaParametrosDoSistema(conn);
				
				if(arqRecebidoEmitePonto == null && arqRecebidoEmitePontoPer == null){
					break;
				}
				if(arqRecebidoEmitePonto.getCodArquivo().equals("0") && arqRecebidoEmitePontoPer.getCodArquivo().equals("0")){
					break;
				}
				
				processaArquivo(conn, monitor, arqRecebidoEmitePonto, paramSis);
				processaArquivo(conn, monitor, arqRecebidoEmitePontoPer, paramSis);
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Processar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				
				throw new DaoException(ed.getMessage());
			}
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					daoPnt.getServiceLocator().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (Exception ed) {
					throw new DaoException(ed.getMessage());
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void processaArquivo(Connection conn, sys.MonitorLog monitor, ArquivoBean arqRecebido, ParamSistemaBean paramSis)
			throws DaoException, SQLException, BeanException {
		
		List<String> listNotifs;
		
		// Se n�o houver arquivo com c�digo 0, ou seja, se existir algum arquivo
		if (!arqRecebido.getCodArquivo().equals("0")){
			iniciarProcessamentoArquivo(conn, monitor, arqRecebido);
			listNotifs = arqRecebido.carregarNotifs(conn);
			
			String myNotif = "";
			int qtdLinhaErro = 0;
			int qtdLinhaCanc = 0;
			
			for (int i=0; i<listNotifs.size();i++) {
				myNotif = (String)listNotifs.get(i);
				ArquivoLinhaEmitePontoBean myLin = new ArquivoLinhaEmitePontoBean();
				arqRecebido.carregarLinhas(myNotif,conn);
				
				if (arqRecebido.getLinhaEmitePonto().size()>0) {
					// Pegar a primeira linha (Reg 2) para testar qual a notificacao 
					myLin = arqRecebido.getLinhaEmitePonto().get(0);						
				}
				
				qtdLinhaCanc = tratarPorTipoDeNotificacao(conn, monitor,arqRecebido, paramSis, myNotif, myLin);
			}
			
			finalizarProcessamentoArquivo(conn, arqRecebido,qtdLinhaErro, qtdLinhaCanc);
			
			monitor.gravaMonitor("Fim do Processamento do Arquivo: " + arqRecebido.getNomArquivo() +
					" Linhas: " + arqRecebido.getLinhasArquivo().size() +
					" Linhas com Erro: "+arqRecebido.getQtdLinhaErro(), monitor.MSG_FIM);
		}
	}

	private void carregaEmitePontoPerrone(Connection conn,ArquivoBean arqRecebidoEmitePontoPer) throws BeanException {
		arqRecebidoEmitePontoPer.setCodIdentArquivo("PNT0111"); //EMITEPER
		arqRecebidoEmitePontoPer.setCodArquivo("0");
		arqRecebidoEmitePontoPer.buscarArquivos("0,7,8", conn);
	}

	private void carregaEmitePonto(Connection conn, ArquivoBean arqRecebidoEmitePonto) throws BeanException {
		arqRecebidoEmitePonto.setCodIdentArquivo("PNT0110"); //EMITEPONTO 
		arqRecebidoEmitePonto.setCodArquivo("0");
		arqRecebidoEmitePonto.buscarArquivos("0,7,8", conn);
	}

	private ParamSistemaBean preparaParametrosDoSistema(Connection conn)
			throws BeanException {
		ParamSistemaBean paramSis = new ParamSistemaBean();
		paramSis.setCodSistema("21"); //M�dulo Pontua��o	
		paramSis.PreparaParam(conn);
		return paramSis;
	}

	/**
	 * Alimenta os atributos da inst�ncia do arqRecebido e atualiza a TPNT_ARQUIVO para status 7 (em processamento)
	 */
	private void iniciarProcessamentoArquivo(Connection conn, sys.MonitorLog monitor, ArquivoBean arqRecebido ) throws DaoException, SQLException,BeanException {
		
			String orgaoPontuacao = arqRecebido.getCodOrgao() ;
			monitor.setNomObjeto(arqRecebido.getNomArquivo());
			monitor.setCodOrgao(orgaoPontuacao);
			monitor.gravaMonitor("Inicio do Processamento do Arquivo "+arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
			
			arqRecebido.setDatProcessamento(Util.formatedToday().substring(0,10));
			arqRecebido.setDatProc(Util.formatedToday().substring(0,10));
			arqRecebido.setNomUsername(NOM_USER);
			arqRecebido.setCodStatus("7");
			if (!arqRecebido.isUpdate(conn)){
				throw new DaoException("Erro ao Atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
			}
			conn.commit();
	}
	
	/**
	 * Finaliza o processamento do arquivo atualizando a TPNT_ARQUIVO para status 2 (processado)
	 * Em caso de erro ele lan�a uma exce��o
	 */
	private void finalizarProcessamentoArquivo(Connection conn, ArquivoBean arqRecebidoEmitePonto, int qtdLinhaErro,int qtdLinhaCanc) throws DaoException, SQLException {
		
		arqRecebidoEmitePonto.setCodStatus("1");
		arqRecebidoEmitePonto.setQtdLinhaErro(qtdLinhaErro);
		arqRecebidoEmitePonto.setQtdLinhaCancelada(qtdLinhaCanc);
		
		if (!arqRecebidoEmitePonto.isUpdate(conn)){
			throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebidoEmitePonto.getMsgErro());
		}
		
		conn.commit();
	}
	
	/**
	 * Processa a emiss�o da notifica��o pelo tipo de notifica��o
	 * 
	 * Notificacoes 0 - Notifica��o de Atua��o
	 * Notificacoes 1 - Aviso Deferimento Defesa
	 * Notificacoes 2 - Notifica��o de Penalidade
	 * Notificacoes 3 - Aviso Deferimento Recurso
	 * Notificacoes 4 - Entrega de CNH
	 * 
	 * @return Quantidade de linhas canceladas
	 */
	private int tratarPorTipoDeNotificacao(Connection conn, MonitorLog monitor, ArquivoBean arqRecebido,ParamSistemaBean paramSis, String myNotif, ArquivoLinhaEmitePontoBean myLin) 
			throws BeanException, DaoException {
		
		int qtdLinhaCanc = 0;
		boolean ocorreErro = true ;
		
		if (myLin.getTipLinha().equals("2")) {
			if ( (myLin.getTipReemissao().equals("R")) && (arqRecebido.isNotificacaoExiste(myNotif,conn)) )  {		
				//reemissao e notificacao existe nao gera nova notificacao
				//ocorreErro = processaReemissaoNotifAutuacao(arqRecebido,myNotif,conn);
			}
			
			// Validar Reemissao para Notificacoes 0 - Atuacao
			if ((myLin.getTipReemissao().equals("E")) && (arqRecebido.isNotificacaoExiste(myNotif,conn))==false ){
				
				if (myLin.getTipNotificacao().equals("0")) {	
					ocorreErro = processaEmissaoNotifAutuacao(arqRecebido,myNotif,conn,paramSis,monitor);
				}
			    if (myLin.getTipNotificacao().equals("1")) {}
			    if (myLin.getTipNotificacao().equals("2")) {
			    	ocorreErro = processaEmissaoNotifPenalidade(arqRecebido, myNotif, conn, paramSis, monitor);
				}
				if (myLin.getTipNotificacao().equals("3")) {}
				if (myLin.getTipNotificacao().equals("4")) {
					//TODO: Testar
					ocorreErro = processaEmissaoNotifApreensao(arqRecebido, myNotif, conn, paramSis, monitor);
				}
			}
		}
		if (ocorreErro) {
			arqRecebido.atualizarLinhas(myNotif,conn,"9");
			qtdLinhaCanc += arqRecebido.getLinhaEmitePonto().size();
		}
		else{
			arqRecebido.atualizarLinhas(myNotif,conn,"1");
		}
			
		return qtdLinhaCanc;
	}
	
	/**
	 * Emissao e notificacao nao existe gera nova notificacao
	 * @return
	 */
	protected static boolean processaEmissaoNotifAutuacao(ArquivoBean myArq,String myNotif, Connection conn,ParamSistemaBean paramSis,sys.MonitorLog monitor) throws DaoException {	
			
		boolean ocorreErro = true;
		
		try {
            /*Calculo data �ltima defesa*/
			String dataUltDefesa=Util.formatedToday().substring(0,10);
			int przEmissao=5;
			int przEntrega=20;
			int przPublicacao=7;
			int przDefesa=45;
			try {
				przEmissao    = Integer.parseInt(paramSis.getParamSist("PRAZO_EMISSAO")); 
				przEntrega    = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTREGA"));
				przPublicacao = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO"));
				przDefesa     = Integer.parseInt(paramSis.getParamSist("PRAZO_DEF_PREVIA"));
			} catch (Exception e) {
			}
			
			int iPrazo  = przEmissao+przEntrega+przPublicacao+przDefesa;
			dataUltDefesa=(Util.addData(dataUltDefesa,iPrazo));
			int iDias = Util.isFimdeSemana(dataUltDefesa); // domingo =1; sabado=2 ou 0 
			if (iDias>0) dataUltDefesa=(Util.addData(dataUltDefesa,iDias));
			
			GeraCodigoBarraBean geraCB = new GeraCodigoBarraBean();
			geraCB.setParam(paramSis);			
			int contMulta  = 0;
			String numProc = "";
			String sDtEmissao="",sTipoReemissao="",sTipoNotificacao="";			
			int seqNotif   = 1 ;
			OrgaoBean OrgaoID = new ACSS.OrgaoBean();
			ProcessoBean myProc = new ProcessoBean();			
			myArq.carregarLinhas(myNotif,conn);
			ArquivoLinhaEmitePontoBean myLin = new ArquivoLinhaEmitePontoBean();
			for (int i=0;i<myArq.getLinhaEmitePonto().size();i++) {
				myLin = myArq.getLinhaEmitePonto().get(i);
				if (myLin.getTipLinha().equals("2")) {
					sDtEmissao      = myLin.getDatEmissao();
					sTipoReemissao  = myLin.getTipReemissao();
					sTipoNotificacao= myLin.getTipNotificacao();
					numProc = myLin.getNumProcesso();
					//numProc=pegarNumProcesso(conn,paramSis);
					myProc.setCodOrgao(myArq.getCodOrgao());
					myProc.setCodArquivo(myArq.getCodArquivo());
					myProc.setUltDatDefesa(dataUltDefesa);
					myLin.setNumProcesso(numProc); 
					cargaProcesso(myLin,myProc);
				}
				if (myLin.getTipLinha().equals("3")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					ProcessoAntBean myProcAnt = new ProcessoAntBean();
					cargaProcessoAnt(myLin,myProcAnt);
					myProc.getProcAnteriores().add(myProcAnt);
				}
				if (myLin.getTipLinha().equals("4")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					contMulta++;
					MultasBean myMulta = new MultasBean();
					cargaMultas(myLin,myMulta,OrgaoID);
					if (contMulta<8) {
						if (contMulta==1) {
							// criar Notificacao 000 e Notificacao 001
							myProc.getNotificacoes().add((criarNotificacao(myLin,"0")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"000");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-000");
								geraCB.drawingBarcodeDirectToGraphics();								
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-000", monitor.MSG_INICIO);						
							}
							
							myProc.getNotificacoes().add((criarNotificacao(myLin,"1")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"001");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-001");
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-001", monitor.MSG_INICIO);						
							}
							seqNotif = 1;
						}						
					}
					else {
						if((contMulta-8)%18==0) {
							seqNotif++ ;
							myProc.getNotificacoes().add((criarNotificacao(myLin,String.valueOf(seqNotif))));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3), monitor.MSG_INICIO);						
							}
						}
					}
					myMulta.setSeqNotificacao(String.valueOf(seqNotif));					
					myMulta.setSeqMultaNotificacao(String.valueOf(contMulta));					
					myProc.getMultas().add(myMulta);
				}				
			}
			// Historico de geracao - status 0
			myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"800",myArq.getNomArquivo(),dataUltDefesa));
			// Historico de emissao - status 1
			myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"801",myArq.getNomArquivo(),dataUltDefesa));			
			// Atualiza grava o Processo
			myProc.GravaProcesso(conn);
			myProc.GravaProcessoAnteriores(conn);			
			myProc.GravaMultas(conn);
			myProc.GravaNotificacoes(conn);
			myProc.GravaHistoricos(conn);	
			ocorreErro = false;
			conn.commit();
		} catch (Exception e) {
			monitor.gravaMonitor("Problemas na grava��o do Processo. Arquivo : "+myArq.getNomArquivo()+" Notifica��o : "+myNotif, monitor.MSG_INICIO);
			throw new DaoException(e.getMessage()+" "+myNotif);
		}
		return ocorreErro;
	}
	
	protected static boolean processaEmissaoNotifPenalidade(ArquivoBean myArq,String myNotif, Connection conn,ParamSistemaBean paramSis,sys.MonitorLog monitor) throws DaoException {	
		
		boolean ocorreErro = true;
		
		try {
            /*Calculo data �ltima defesa*/
			String dataUltRecurso=Util.formatedToday().substring(0,10);
			
			int przEmissao=5;
			int przEntrega=20;
			int przPublicacao=0;
			int przDefesa=30;
			try {
				przEmissao    = Integer.parseInt(paramSis.getParamSist("PRAZO_EMISSAO")); 
				przEntrega    = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTREGA"));
				przPublicacao = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO"));
				przDefesa     = Integer.parseInt(paramSis.getParamSist("PRAZO_RECURSO_2INSTANCIA"));
			} catch (Exception e) {
			}
			
			int iPrazo  = przEmissao+przEntrega+przPublicacao+przDefesa;
			dataUltRecurso=(Util.addData(dataUltRecurso,iPrazo));
			int iDias = Util.isFimdeSemana(dataUltRecurso); // domingo =1; sabado=2 ou 0 
			if (iDias>0) dataUltRecurso=(Util.addData(dataUltRecurso,iDias));
			
			GeraCodigoBarraBean geraCB = new GeraCodigoBarraBean();
			geraCB.setParam(paramSis);			
			int contMulta  = 0;
			String numProc = "";
			String sDtEmissao="",sTipoReemissao="",sTipoNotificacao="";			
			int seqNotif   = 1 ;
			OrgaoBean OrgaoID = new ACSS.OrgaoBean();
			ProcessoBean myProc = new ProcessoBean();			
			myArq.carregarLinhas(myNotif,conn);
			ArquivoLinhaEmitePontoBean myLin = new ArquivoLinhaEmitePontoBean();
			
			for (int i=0;i<myArq.getLinhaEmitePonto().size();i++) {
				myLin = myArq.getLinhaEmitePonto().get(i);
				if (myLin.getTipLinha().equals("2")) {
					sDtEmissao      = myLin.getDatEmissao();
					sTipoReemissao  = myLin.getTipReemissao();
					sTipoNotificacao= myLin.getTipNotificacao();
					numProc = myLin.getNumProcesso();
					myProc.setCodOrgao(myArq.getCodOrgao());
					myProc.setCodArquivo(myArq.getCodArquivo());
					myProc.setUltDatRecurso(dataUltRecurso);
					//myProc.setUltDatDefesa(dataUltDefesa);
					cargaProcesso(myLin,myProc);
				}
				if (myLin.getTipLinha().equals("3")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					ProcessoAntBean myProcAnt = new ProcessoAntBean();
					cargaProcessoAnt(myLin,myProcAnt);
					myProc.getProcAnteriores().add(myProcAnt);
				}
				if (myLin.getTipLinha().equals("4")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					contMulta++;
					MultasBean myMulta = new MultasBean();
					cargaMultas(myLin,myMulta,OrgaoID);
					if (contMulta<8) {
						if (contMulta==1) {
							// criar Notificacao 000 e Notificacao 001
							myProc.getNotificacoes().add((criarNotificacao(myLin,"0")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"000");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-000");
								geraCB.drawingBarcodeDirectToGraphics();								
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-000", monitor.MSG_INICIO);						
							}
							
							myProc.getNotificacoes().add((criarNotificacao(myLin,"1")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"001");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-001");
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-001", monitor.MSG_INICIO);						
							}
							seqNotif = 1;
						}						
					}
					else {
						if((contMulta-8)%18==0) {
							seqNotif++ ;
							myProc.getNotificacoes().add((criarNotificacao(myLin,String.valueOf(seqNotif))));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3), monitor.MSG_INICIO);						
							}
						}
					}
					myMulta.setSeqNotificacao(String.valueOf(seqNotif));					
					myMulta.setSeqMultaNotificacao(String.valueOf(contMulta));					
					myProc.getMultas().add(myMulta);
				}				
			}
			// Historico de geracao - status 0
			myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"800",myArq.getNomArquivo(),dataUltRecurso));
			// Historico de emissao - status 1
			myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"801",myArq.getNomArquivo(),dataUltRecurso));			
			myProc.GravaProcesso(conn);
			myProc.GravaProcessoAnteriores(conn);			
			//myProc.GravaMultas(conn);
			myProc.GravaNotificacoes(conn);
			myProc.GravaHistoricos(conn);	
			ocorreErro = false;
			conn.commit();
		} catch (Exception e) {
			monitor.gravaMonitor("Problemas na grava��o do Processo. Arquivo : "+myArq.getNomArquivo()+" Notifica��o : "+myNotif, monitor.MSG_INICIO);
			throw new DaoException(e.getMessage()+" "+myNotif);
		}
		return ocorreErro;
	}
	
	protected static boolean processaEmissaoNotifApreensao(ArquivoBean myArq,String myNotif, Connection conn,ParamSistemaBean paramSis,sys.MonitorLog monitor) throws DaoException {	
		
		boolean ocorreErro = true;
		
		try {
			/*Calculo data entrega CNH*/
			String dataUltDatEntregaCNH=Util.formatedToday().substring(0,10);
			
			int przEmissao=5;
			int przEntrega=20;
			int przPublicacao=7;
			int przEntregaCNH=2;
			try {
				przEmissao    = Integer.parseInt(paramSis.getParamSist("PRAZO_EMISSAO"));
				przEntrega    = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTREGA"));
				przPublicacao = Integer.parseInt(paramSis.getParamSist("PRAZO_PUBL_DO"));
				przEntregaCNH     = Integer.parseInt(paramSis.getParamSist("PRAZO_ENTR_CNH"));
			} catch (Exception e) {
			}
			
			int iPrazo  = przEmissao+przEntrega+przPublicacao+przEntregaCNH;
			dataUltDatEntregaCNH=(Util.addData(dataUltDatEntregaCNH,iPrazo));
			int iDias = Util.isFimdeSemana(dataUltDatEntregaCNH); // domingo =1; sabado=2 ou 0 
			if (iDias>0) dataUltDatEntregaCNH=(Util.addData(dataUltDatEntregaCNH,iDias));
			
			GeraCodigoBarraBean geraCB = new GeraCodigoBarraBean();
			geraCB.setParam(paramSis);			
			int contMulta  = 0;
			String numProc = "";
			String sDtEmissao="",sTipoReemissao="",sTipoNotificacao="";			
			int seqNotif   = 1 ;
			OrgaoBean OrgaoID = new ACSS.OrgaoBean();
			ProcessoBean myProc = new ProcessoBean();			
			myArq.carregarLinhas(myNotif,conn);
			ArquivoLinhaEmitePontoBean myLin = new ArquivoLinhaEmitePontoBean();
			
			for (int i=0;i<myArq.getLinhaEmitePonto().size();i++) {
				myLin = myArq.getLinhaEmitePonto().get(i);
				if (myLin.getTipLinha().equals("2")) {
					sDtEmissao      = myLin.getDatEmissao();
					sTipoReemissao  = myLin.getTipReemissao();
					sTipoNotificacao= myLin.getTipNotificacao();
					numProc = myLin.getNumProcesso();
					myProc.setCodOrgao(myArq.getCodOrgao());
					myProc.setCodArquivo(myArq.getCodArquivo());
					myProc.setUltDatEntregaCNH(dataUltDatEntregaCNH);
					cargaProcesso(myLin,myProc);
				}
				if (myLin.getTipLinha().equals("3")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					ProcessoAntBean myProcAnt = new ProcessoAntBean();
					cargaProcessoAnt(myLin,myProcAnt);
					myProc.getProcAnteriores().add(myProcAnt);
				}
				if (myLin.getTipLinha().equals("4")) {
					myLin.setNumProcesso(numProc);
					myLin.setDatEmissao(sDtEmissao);
					myLin.setTipReemissao(sTipoReemissao);
					myLin.setTipNotificacao(sTipoNotificacao);
					contMulta++;
					MultasBean myMulta = new MultasBean();
					cargaMultas(myLin,myMulta,OrgaoID);
					if (contMulta<8) {
						if (contMulta==1) {
							// criar Notificacao 000 e Notificacao 001
							myProc.getNotificacoes().add((criarNotificacao(myLin,"0")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"000");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-000");
								geraCB.drawingBarcodeDirectToGraphics();								
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-000", monitor.MSG_INICIO);						
							}
							
							myProc.getNotificacoes().add((criarNotificacao(myLin,"1")));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+"001");
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-001");
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-001", monitor.MSG_INICIO);						
							}
							seqNotif = 1;
						}						
					}
					else {
						if((contMulta-8)%18==0) {
							seqNotif++ ;
							myProc.getNotificacoes().add((criarNotificacao(myLin,String.valueOf(seqNotif))));
							/*Gera��o CB*/
							try	{
								geraCB.setDataCB(myLin.getDatProc());								
								geraCB.setNumero(Util.lPad(myNotif,"0",9)+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.setTexto(Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3));
								geraCB.drawingBarcodeDirectToGraphics();
							}
							catch (Exception e) {
								monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+Util.lPad(myNotif,"0",9)+"-"+Util.lPad(String.valueOf(seqNotif),"0",3), monitor.MSG_INICIO);						
							}
						}
					}
					myMulta.setSeqNotificacao(String.valueOf(seqNotif));					
					myMulta.setSeqMultaNotificacao(String.valueOf(contMulta));					
					myProc.getMultas().add(myMulta);
				}				
			}
			// Historico de geracao - status 0
			//FIXME: Debugar e verificar a necessidade dos m�todos comentados abaixo
			//myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"800",myArq.getNomArquivo(),dataUltRecurso));
			// Historico de emissao - status 1
			//myProc.getHistoricos().add(cargaHistorico(myNotif,myProc,"801",myArq.getNomArquivo(),dataUltRecurso));			
			myProc.GravaProcesso(conn);
			myProc.GravaProcessoAnteriores(conn);			
			//myProc.GravaMultas(conn);
			myProc.GravaNotificacoes(conn);
			myProc.GravaHistoricos(conn);	
			ocorreErro = false;
			conn.commit();
		} catch (Exception e) {
			monitor.gravaMonitor("Problemas na grava��o do Processo. Arquivo : "+myArq.getNomArquivo()+" Notifica��o : "+myNotif, monitor.MSG_INICIO);
			throw new DaoException(e.getMessage()+" "+myNotif);
		}
		return ocorreErro;
	}
	protected static void cargaProcesso(ArquivoLinhaEmitePontoBean myLin,ProcessoBean myProc) throws DaoException {		
		try {
			myProc.setNumProcesso(myLin.getNumProcesso());  
			myProc.setDatProcesso(Util.formatedToday().substring(0,10));
			myProc.setTipProcesso(myLin.getTipProcesso());
			myProc.setDatGeracao(myLin.getDatEmissao());
			myProc.setTotPontos(myLin.getTotPontos());			
			myProc.setQtdProcAnteriores(myLin.getQtdProcAnteriores());
			myProc.setQtdMultas(myLin.getQtdMultas());
			myProc.setNomResponsavel(myLin.getNomResponsavel());
			myProc.setNumCPF(myLin.getNumCPF());
			myProc.setTipCNH(myLin.getTipCNH());
			myProc.setNumCNH(myLin.getNumCNH());
			myProc.setUfCNH(myLin.getUfCNH());
			myProc.setDatEmissaoCNH(myLin.getDatEmissaoCNH());
			myProc.setDatValidadeCNH(myLin.getDatValidadeCNH());
			myProc.setCategoriaCNH(myLin.getCategoriaCNH());
			myProc.setEndereco(myLin.getEndereco());
			myProc.setCep(myLin.getCep());
			myProc.setMunicipio(myLin.getMunicipio());
			myProc.setUf(myLin.getUf());
			//As informa��es de data Defesa (Atua��o) e/ou data Recurso (Penalidade), s�o setadas anteriormente
			myProc.setCodStatus("001");
			myProc.setDatStatus(Util.formatedToday().substring(0,10));
			myProc.setNomUserName(myLin.getNomUsername());
			myProc.setCodOrgaoLotacao(myLin.getCodOrgaoLotacao());    
			myProc.setDatProc(myLin.getDatProc());
			
			// A transa��o 941 � invocada para obtermos o c�digo da penalidade
			ProcessoBean processoM97 = new ProcessoBean();
			processoM97.setNumProcesso(myProc.getNumProcesso());
			processoM97.setNumCPF(myProc.getNumCPF());
			processoM97.LeProcesso();
			
			myProc.setCodPenalidade(processoM97.getCodPenalidade());
			
			// A Penalidade Bean � instanciada para obtermos a descri��o da penalidade
			PenalidadeBean myPenalidade = new PenalidadeBean();
			myPenalidade.setCodPenalidade(myProc.getCodPenalidade());
			myPenalidade.Le_Penalidade("codigo");
			myProc.setDscPenalidade(myPenalidade.getDscPenalidade());
			myProc.setDscPenalidadeRes(myPenalidade.getDscResumida());
			
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}	
	
	protected static void cargaProcessoAnt(ArquivoLinhaEmitePontoBean myLin,ProcessoAntBean myProcAnt) throws DaoException {		
		try {
			myProcAnt.setNumProcessoAnt(myLin.getNumProcessoAnt());  
			myProcAnt.setDatProcessoAnt(myLin.getDatProcessoAnt());
			myProcAnt.setTipProcessoAnt(myLin.getTipProcessoAnt());
			myProcAnt.setTotPntProcessoAnt(myLin.getTotPntProcessoAnt());			
			myProcAnt.setSitProcessoAnt(myLin.getSitProcessoAnt());
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}	
	
	protected static void cargaMultas(ArquivoLinhaEmitePontoBean myLin,MultasBean myMulta,OrgaoBean OrgaoID) throws DaoException {		
		try {
			
//			codMultas             = "";
//			codNotificacao        = "";
//			codProcesso           = "";			
			myMulta.setNumNotificacao(myLin.getNumNotificacao());
			myMulta.setNumProcesso(myLin.getNumProcesso());
			myMulta.setDatProcesso(Util.formatedToday().substring(0,10)); 
			myMulta.setCodOrgao(myLin.getCodOrgao());
			OrgaoID.Le_Orgao(myMulta.getCodOrgao(),0);
			myMulta.setSigOrgao(OrgaoID.getSigOrgao());			
			myMulta.setNumAutoInfracao(myLin.getNumAuto());
			myMulta.setNumPlaca(myLin.getNumPlaca());
			myMulta.setCodInfracao(myLin.getCodInfracao());
			myMulta.setDatInfracao(myLin.getDatInfracao());
			myMulta.setValHorInfracao(myLin.getHorInfracao());
			myMulta.setDscLocalInfracao(myLin.getLocInfracao());
			myMulta.setDscInfracao(myLin.getDscInfracao());
		    myMulta.setDscEnquadramento(myLin.getEnquadramento());
		    myMulta.setNumPonto(myLin.getPontuacao());
		    myMulta.setDatTransJulgado(myLin.getDatTrasJulg());
			
			myMulta.setNomUserName(myLin.getNomUsername());
			myMulta.setCodOrgaoLotacao(myLin.getCodOrgaoLotacao());    
			myMulta.setDatProc(myLin.getDatProc());
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}		
	
	protected static NotificacaoBean criarNotificacao(ArquivoLinhaEmitePontoBean myLin,String seqNotif) throws DaoException {		
		try {
			NotificacaoBean myNotif = new NotificacaoBean();
//			codNotificacao        = "";
//			codProcesso           = "";	
			String sNotif = "000"+seqNotif;
			sNotif = sNotif.substring(sNotif.length()-3,sNotif.length());
			myNotif.setNumNotificacao(myLin.getNumNotificacao());
			myNotif.setSeqNotificacao(sNotif);
			myNotif.setTipoNotificacao(myLin.getTipNotificacao());
			myNotif.setTipoReemissao(myLin.getTipReemissao());
			myNotif.setDatEmissao(myLin.getDatEmissao());
			myNotif.setNomUserName(myLin.getNomUsername());
			myNotif.setCodOrgaoLotacao(myLin.getCodOrgaoLotacao());    
			myNotif.setDatProc(myLin.getDatProc());
			myNotif.setCodArquivo(myLin.getCodArquivo());
			myNotif.setDatGeracaoCB(myLin.getDatProc());
		    return myNotif;
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}	
	
	protected static HistoricoBean cargaHistorico(String myNotif,
			ProcessoBean myProc,String codEvento,String nomArquivo,String dataUltDefesa) throws DaoException {		
		try {
			HistoricoBean myHist = new HistoricoBean();
//			codHistoricoProcesso  = "";
//			codProcesso           = "";	
			
			myHist.setNumProcesso(myProc.getNumProcesso());
			String sCodEvento = "000"+codEvento;
			sCodEvento = sCodEvento.substring(sCodEvento.length()-3,sCodEvento.length());
			myHist.setCodEvento(sCodEvento);
			if (sCodEvento.equals("800")) {
				myHist.setCodStatus("00");
				myHist.setDatStatus(myProc.getDatGeracao());
				// Compl 01 - Nome do Arquivo
				myHist.setTxtComplemento01(nomArquivo);
				// Compl 03 - Data de Gera��o
				myHist.setTxtComplemento03(myProc.getDatGeracao());
			}
			else {
				myHist.setCodStatus(myProc.getCodStatus());				
				myHist.setDatStatus(myProc.getDatStatus());
				//Compl 01 - Notifica��o
				myHist.setTxtComplemento01(myNotif);
			    //Compl 02 - Qtd Sequencias						
				myHist.setTxtComplemento02(String.valueOf(myProc.getNotificacoes().size()));
				// Compl 03 - Data de Gera��o
				myHist.setTxtComplemento03(myProc.getDatGeracao());
				// Compl 04 - Nome do Processo
				myHist.setTxtComplemento04(myProc.getNumProcesso());
				// Compl 05 - Data �ltima defesa
				myHist.setTxtComplemento05(dataUltDefesa);
				
			}			
			myHist.setCodOrigemEvento("101");
			myHist.setNomUserName(myProc.getNomUserName());
			myHist.setCodOrgaoLotacao(myProc.getCodOrgaoLotacao());    
			myHist.setDatProc(myProc.getDatProc());
			
		    return myHist;
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}		
	
	protected static String pegarNumProcesso(Connection conn,ParamSistemaBean paramSis) throws DaoException {		
		try {
			PNT.ROB.Dao daoPnt = PNT.ROB.DaoFactory.getInstance();
			return daoPnt.pegarNumProcesso(conn,paramSis);
	    } catch (Exception e) {
		       throw new DaoException(e.getMessage());
	    }
	}		

	protected static boolean drawingBarcodeDirectToGraphics(String sPath,String sTexto,int iAltura,boolean bImprimeTexto)
	throws BarcodeException,ServletException,IOException {
		try	{
			Barcode barcode = BarcodeFactory.createCode128B(sTexto);
			barcode.setBarHeight((double)iAltura);
			barcode.setDrawingText(bImprimeTexto);
			BufferedImage image = getImage(barcode);
			File dir = new File(sPath);
			dir.mkdirs();    			
			ImageIO.write(image, "jpg", dir);			
		} catch (Exception e)		{	}    	
		return true;
	}    
	protected static BufferedImage getImage(Barcode barcode) {	
		BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bi.createGraphics();
		barcode.draw(g, 0, 0);
		bi.flush();
		return bi;
	}
	
	protected static String getParametro(ACSS.ParamSistemaBean param,String sData) {        
		String parametro = "DIR_CODIGO_BARRA";
		/*Novo Path*/
		parametro+="_"+sData.substring(6,10);
		return parametro;
	}
	
	protected static String getArquivo(ACSS.ParamSistemaBean param,String sData,String texto) {
		String pathArquivo;
		try {
			pathArquivo = param.getParamSist(getParametro(param,sData))+"/"+getImagem(sData)+
			"/CodigoBarra/"+texto+ ".jpg";				
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}    	
	
	protected static String getImagem(String sData) {
		String pathImagem = "";
		pathImagem =sData.substring(6,10)+ 
		sData.substring(3,5) +
		"/" + sData.substring(0,2); 
		return pathImagem;	
	}
	
	public static void main(String args[])  throws DaoException {		
		try {			
			new EmiteNotificacaoRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}