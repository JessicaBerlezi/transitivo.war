package PNT.ROB;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
import ACSS.ParamSistemaBean;
import sys.Util;


/**
 * <b>Title:</b>        SMIT - Pontua��o <br>
 * <b>Description:</b>  Processo Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company: </b> MIND Informatica <br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class GeraCodigoBarraBean   { 
		
	private String sPath;
	private int iAltura;
	private boolean bImprimeTexto;
	private String texto;
	private ParamSistemaBean param;
	private String dataCB;	
	private String numero;

	public GeraCodigoBarraBean()  throws sys.BeanException {
				
		sPath         = "" ;
		iAltura       = 70;
		bImprimeTexto = true;	
		texto         = "" ;
		param         = new ParamSistemaBean();
		dataCB        = Util.formatedToday().substring(0,10);
		numero        = "0" ;
	}

	
	protected boolean drawingBarcodeDirectToGraphics()
	throws BarcodeException,ServletException,IOException {
		try	{
			setSPath(getArquivo(param,dataCB));			
			Barcode barcode = BarcodeFactory.createCode128B(getTexto());
			barcode.setBarHeight((double)getIAltura());
			barcode.setDrawingText(isBImprimeTexto());
			BufferedImage image = getImage(barcode);
			File dir = new File(this.sPath);
			dir.mkdirs();    			
			ImageIO.write(image, "jpg", dir);			
		} catch (Exception e)		{	}    	
		return true;
	}    
	protected static BufferedImage getImage(Barcode barcode) {	
		BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bi.createGraphics();
		barcode.draw(g, 0, 0);
		bi.flush();
		return bi;
	}
	protected static String getParametro(String sData) {        
		String parametro = "DIR_CODIGO_BARRA";
		/*Novo Path*/
		parametro+="_"+sData.substring(6,10);
		return parametro;
	}
	protected String getArquivo(ParamSistemaBean param,String sData) {
		String pathArquivo;
		try {
			pathArquivo = param.getParamSist(getParametro(sData))+"/"+getImagem(sData)+
			"/CodigoBarra/"+getNumero()+ ".jpg";				
		} catch (Exception e) {	pathArquivo = "";	}
		return pathArquivo;
	}    	
	protected static String getImagem(String sData) {
		String pathImagem = "";
		pathImagem =sData.substring(6,10)+sData.substring(3,5) +"/" + sData.substring(0,2); 
		return pathImagem;	
	}
	public String getSPath() {
		return sPath;
	}
	public void setSPath(String path) {
		sPath = path;
	}
	public boolean isBImprimeTexto() {
		return bImprimeTexto;
	}
	public void setBImprimeTexto(boolean imprimeTexto) {
		bImprimeTexto = imprimeTexto;
	}
	public int getIAltura() {
		return iAltura;
	}
	public void setIAltura(int altura) {
		iAltura = altura;
	}
	public String getTexto() {
		return texto;
	}
	public void setTexto(String texto) {
		this.texto = texto;
	}
	public ParamSistemaBean getParam() {
		return param;
	}
	public void setParam(ParamSistemaBean param) {
		this.param = param;
	}	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}	
	public String getDataCB() {
		return dataCB;
	}
	public void setDataCB(String dataCB) {
		this.dataCB = dataCB;
	}	

}