/*
 * Created on 26/07/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package BRK;

import com.softwareag.entirex.aci.Broker;

/**
 * @author Administrador
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class VexFactory {

	//qualVex  2 - Desenvolvimento  3 - Produ��o 
	private String qualVex = "2";

	public void setQualVex(String qualVex){
		this.qualVex = qualVex;
	}
	
	public VexInter getInstance() {

		if ("3".equals(qualVex)) {
			Broker broker = new Broker(Vexp.DEFAULT_BROKERID, "ConsistRJ");
			return new Vexp(broker, Vexp.DEFAULT_SERVER);
		} else {
			Broker broker = new Broker(Vexd.DEFAULT_BROKERID, "ConsistRJ");
			return new Vexd(broker, Vexd.DEFAULT_SERVER);
		}
		
	}

}