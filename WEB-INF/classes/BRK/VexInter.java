/*
 * Created on 26/07/2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package BRK;

import com.softwareag.entirex.aci.BrokerException;

/**
 * @author Administrador
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface VexInter {
	
	public static final String DEFAULT_BROKERID = null;
	public static final String DEFAULT_SERVER = null;
	public abstract String getStubVersion();
	public abstract void setGtrn0003Parte_fixa(String p);
	public abstract String getGtrn0003Parte_fixa();
	public abstract void setGtrn0003Parte_variavel(String[] p);
	public abstract String[] getGtrn0003Parte_variavel();
	public abstract void gtrn0003() throws BrokerException;
    public void setNaturalLogon(boolean arg);
    public void setRPCUserId(String arg);
    public void setRPCPassword(String arg);
    public void setLibraryName(String arg);
}