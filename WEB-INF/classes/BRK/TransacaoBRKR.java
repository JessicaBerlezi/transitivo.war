package BRK;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.sql.SQLException;
import sys.ServiceLocatorException;

import ACSS.ParamSistemaBean;
import ROB.RobotException;

import com.softwareag.entirex.aci.BrokerException;


/**
* <b>Title:</b>        	SMIT - Transa��o para Acesso ao Entire Broker<br>
* <b>Description:</b>  	Acessa a Base do M97 usando as bibliotecas do E-Broker<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
* @Updates
*/

public class TransacaoBRKR {

	private sys.ServiceLocator serviceloc ;  
	private Connection conn ;  	
	private static final String MYABREVSIST = "REC";

	private String parteFixa;
	private String seqTransacao;
	private String parteVariavel;
	private String dtEnvioReq;
	private String dtRecebReq;
	private String dtEnvioBrk;
	private String dtRecebBrk;
	private String codUfBrk;
	private String codLogBroker;
	private String codOrgaoAtuacao;
	private String codOrgaoLotacao;
	private String nomUserName;
	
	public TransacaoBRKR() throws sys.DaoException {
		try {	
			dtEnvioReq      = "";
			dtRecebReq      = "";
			dtEnvioBrk      = "";
			dtRecebBrk      = "";
			codUfBrk        = "";
			seqTransacao    = "";
			codLogBroker    = "";
			codOrgaoAtuacao = "";
			codOrgaoLotacao = "";
			nomUserName     = "";
		    serviceloc =  sys.ServiceLocator.getInstance();
		    conn = null;
		}
		catch (Exception e) { 			
		  throw new sys.DaoException(e.getMessage());
		}
	
	}
	public TransacaoBRKR(sys.ServiceLocator sLoc) throws sys.DaoException {
		try {	
			dtEnvioReq      = "";
			dtRecebReq      = "";
			dtEnvioBrk      = "";
			dtRecebBrk      = "";
			codLogBroker    = "";
		    serviceloc      = sLoc;
			codOrgaoAtuacao = "";
			codOrgaoLotacao = "";
			nomUserName     = "";
		    conn            = null ;
		}
		catch (Exception e) { 			
		  throw new sys.DaoException(e.getMessage());
		}
	}

	public void setParteFixa(String codTrans) {

		int numTrans = 0;
		Calendar calendar = new GregorianCalendar();
		int numDia = Calendar.DAY_OF_YEAR;
		numDia = calendar.get(Calendar.DAY_OF_YEAR);
		//				N�mero da Transa��o
		parteFixa = "00000" + String.valueOf(numTrans);
		parteFixa =	parteFixa.substring(parteFixa.length()-6, parteFixa.length());
		//				Set do C�digo da Transa��o				
		parteFixa += codTrans;
		//				Valor Constante				
		parteFixa += "1";
		//				Valor Constante				
		parteFixa += "COVEX      ";
		//				Valor Constante				
		parteFixa += "BR";
		//				Valor Constante				
		parteFixa += "BR";
		//				Valor Constante				
		parteFixa += "RJ";
		//				Valor Constante				
		parteFixa += "1";
		//				TAMANHO DA TRANSA��O				
		parteFixa += "0037";
		//				Valor Constante				
		parteFixa += "00";
		//				DIA JULIANO						
		String auxJul = "000"+String.valueOf(numDia);
		auxJul = auxJul.substring(auxJul.length() - 3, auxJul.length());
		parteFixa += auxJul;
		
		
		
		/********ALTERA��O DO MODO DE ENVIAR A TRANSA��O
		
	    URL url = new URL("https://teste");  
        HttpsURLConnection.setDefaultSSLSocketFactory(factory);  
        HttpsURLConnection urlc = (HttpsURLConnection) url.openConnection();  
        urlc.setSSLSocketFactory(factory);    
        urlc.setDefaultSSLSocketFactory(factory);  
        urlc.setRequestMethod("GET");  
    //urlc.setDefaultSSLSocketFactory(sslContext.getSocketFactory());  
        urlc.setDoInput(true);  
        urlc.setUseCaches(false);  
        Object[] params = {strCabecalhoMsg, strDadosMsg};  
        Service service = new Service();  
        Call call = (Call) service.createCall();  
        call.setTargetEndpointAddress(url);  
        call.setOperationName("servico");  
          
        String ret = (String) call.invoke(params);  
		*/
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}

	public String getParteFixa() {
		return parteFixa;
	}

	public void setParteVariavel(String parteVariavel) {
		if (parteVariavel == null) parteVariavel = "";
		this.parteVariavel = parteVariavel;
	}

	public String getParteVariavel() {
		return this.parteVariavel;
	}
	public String getResultado(Connection conn) {
		this.conn = conn ;
		return getResultado();
	}
	
	
	
	public String getResultado() 
	{
		String sMensagemBRK = "";
		String resultado    = "";
		String erroMens     = "";
		boolean pegouConn   = false;				
		String codLogBroker = "0";
		boolean bErroBroker = false;
		boolean gravaLog    = true;
		gravaLog=gravaLog(getParteFixa().substring(6,9));
		try {
			// preparando Parte Variavel em Array[13]
			String strParteVar = this.getParteVariavel();
			if (strParteVar == null) strParteVar = "";			

			String[] vParteVar = new String[13];
			for (int i=0; i<13;i++) vParteVar[i] = "";		

			for (int i=0; i<13;i++) {
				if (strParteVar.length()<=0) break;
				if (strParteVar.length()>249) {
					vParteVar[i] = strParteVar.substring(0,249);
				}
				else {
					vParteVar[i] = strParteVar.substring(0);
					break;				
				}
				strParteVar=strParteVar.substring(249);
			}

			// Preparar instancia do Broker			
			VexFactory vf = new VexFactory();
			vf.setQualVex(serviceloc.getQualBanco());		
			VexInter pgmBroker = vf.getInstance();			
			pgmBroker.setNaturalLogon(true);
			pgmBroker.setRPCUserId("COVEX");
			pgmBroker.setRPCPassword("COVEX01");
			pgmBroker.setLibraryName("LIBBRK");
			pgmBroker.setGtrn0003Parte_variavel(vParteVar);			

			// Preparar a numeracao da transacao			
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST) ;
				pegouConn = true ;
			}			
			Statement stmt = conn.createStatement();
			ResultSet rs       = null;
			conn.setAutoCommit(false);	
			String sCmd    = "SELECT NVL(NUM_SEQ_TRANSACAO,0) NUM_SEQ_TRANSACAO, "+
					"to_char(DAT_TRANSACAO,'DD/MM/YYYY') as DAT_TRANSACAO "+
					" from TSMI_EVENTO "+
					" WHERE cod_evento = '"+getParteFixa().substring(6,9)+"' "+
					" FOR UPDATE" ;
			rs    = stmt.executeQuery(sCmd) ;
			int trans = 0;
			String dt = "";
			while (rs.next()) {
				trans = rs.getInt("NUM_SEQ_TRANSACAO");	 
				dt    = rs.getString("DAT_TRANSACAO");	
				if (dt==null) dt="";  			 
			}			
			if (dt.equals(sys.Util.formatedToday().substring(0,10))) {
				trans++;
			}
			else trans=1;
			sCmd    = "UPDATE  TSMI_EVENTO SET NUM_SEQ_TRANSACAO="+trans+", "+
					" DAT_TRANSACAO=to_date('"+sys.Util.formatedToday().substring(0,10)+"','DD/MM/YYYY')" +
					" WHERE cod_evento = '"+getParteFixa().substring(6,9)+"' " ;
			stmt.execute(sCmd) ;
			conn.commit();	
			rs.close();
			stmt.close();


			String strans="000000"+Integer.toString(trans);			
			strans = strans.substring(strans.length()-6, strans.length()) ;
			strans += getParteFixa().substring(6,getParteFixa().length());
			this.parteFixa=strans;
			this.seqTransacao=strans;

			pgmBroker.setGtrn0003Parte_fixa(this.parteFixa)	;

			// verifica e grava LOG antes do Broker
			if (gravaLog) {	 					
				conn.setAutoCommit(false);
				sCmd = "Select SEQ_TSMI_LOG_BROKER.NEXTVAL codLog from dual" ;
				Statement stmtLog = conn.createStatement();
				ResultSet rsLog   = stmtLog.executeQuery(sCmd) ;
				while (rsLog.next()) {
					codLogBroker = rsLog.getString("codLog");
					if (codLogBroker==null) codLogBroker="0";	 
				}
				sCmd = "INSERT INTO TSMI_LOG_BROKER (COD_TSMI_LOG_BROKER, DAT_PROC, COD_TRANSACAO, " +
						"TXT_PARTE_FIXA, TXT_PARTE_VARIAVEL, NUM_TRANSACAO, IND_INC,DAT_ENVIO_BROKER, " +
						"COD_ORGAO_LOTACAO,COD_ORGAO_ATUACAO,NOM_USERNAME) "+
						" VALUES("+codLogBroker+", SYSDATE, " +
						"'"+getParteFixa().substring(6,9) + "','" + getParteFixa() + "','" +
						getParteVariavel().trim() + "'," + trans + ",1,SYSDATE," +
						"'"+getCodOrgaoLotacao()+"',"+"'"+getCodOrgaoAtuacao()+"',"+ 
						"'"+getNomUserName()+"')";   
				stmtLog.execute(sCmd) ;
				conn.commit();	
				stmtLog.close();
				rsLog.close();			
			}
			//Executar o Broker						
			pgmBroker.gtrn0003();
			// pegar o resultado
			String [] resParteVar = pgmBroker.getGtrn0003Parte_variavel();
			String auxLin = "";			
			for (int i=0; i < 13; i++) {
				auxLin = (String) resParteVar[i];
				if (auxLin.trim() != null) {
					resultado += auxLin; 
				}				
			}
			resultado = resultado.replaceAll("'", " ");				
			// Preparar o Log de Retorno do Broker
			if (gravaLog) {						
				conn.setAutoCommit(false);				
				sCmd    = "UPDATE  TSMI_LOG_BROKER SET TXT_RESULTADO= ' " +resultado.trim()+ "'," +
				 		  " DAT_RETORNO_BROKER = SYSDATE "+	
						  " WHERE COD_TSMI_LOG_BROKER = '"+ codLogBroker+"'";
				Statement stmtLog = conn.createStatement();
				stmtLog.execute(sCmd);
				conn.commit();
				stmtLog.close();
			}




		} catch (BrokerException e) 
		{

			try 
			{
				ParamSistemaBean paramSis = new ParamSistemaBean();
				paramSis.setCodSistema("45");	
				paramSis.PreparaParam(conn);
				sMensagemBRK=paramSis.getParamSist("NOM_ERRO_BROKER");
			}
			catch (Exception eBrk) 
			{
				
				
				
				
			}


			/*Consultoria de Inform�tica do DETRAN: 2332-0159 ou SUPORTE SMIT : 0800-285-3434*/
			bErroBroker=true;
			resultado = "ERR-" + e.getMessage();
			if (resultado.toUpperCase().indexOf("ERR-SERVICE NOT REGISTERED")>= 0 || resultado.toUpperCase().indexOf("ERR-SOCKET CONNECT FAILED")>=0 || resultado.toUpperCase().indexOf("ERR-SEND/RECEIVE")>= 0 ) {
				resultado = "Problemas na comunica��o com o Banco de dados. Tente mais uma vez. Se n�o houver sucesso," +
						"entre em contato com o(a) "+sMensagemBRK+" e passe a seguinte "+
						"mensagem: " + "ERR-" + e.getMessage()+ "";
			}
			resultado = resultado.replaceAll("'", " ");
			try {	

				String codException = "0";
				conn.setAutoCommit(false);
				String sCmd = "Select SEQ_TSMI_ROBO_EXCEPTION.NEXTVAL codException from dual";
				Statement stmtLog = conn.createStatement();
				ResultSet rsLog   = stmtLog.executeQuery(sCmd) ;
				while (rsLog.next()) {
					codException = rsLog.getString("codException");
					if (codException==null) codException="0";	 
				}
				sCmd = "INSERT INTO TSMI_ROBO_EXCEPTION (ID_EXCEPTION, DAT_PROC, COD_TRANSACAO," +
						"COD_LOG_BROKER,TXT_MENSAGEM) "+
						" VALUES("+codException+", SYSDATE, " +
						"'"+getParteFixa().substring(6,9) + "',"+codLogBroker+",'"+e.getMessage()+"  "+e.getErrorCode()+"')";
				
				
				
				stmtLog.execute(sCmd) ;
				conn.commit();	
				stmtLog.close();
				rsLog.close();			
			} 
			catch (SQLException SQLe) {
				bErroBroker=true;			
				resultado = "ERR-" + SQLe.getMessage();			
				resultado = resultado.replaceAll("'", " ");			
			}
		}
		catch (Exception exp) {
			bErroBroker=true;			
			resultado = "ERR-" + exp.getMessage();			
			resultado = resultado.replaceAll("'", " ");			
		}
		finally {
			if ( (conn!=null) && ("0".equals(codLogBroker)==false) ) {
				// termina de gravar o LOG
				try {
					ResultSet rsLog = null;
					conn.setAutoCommit(false);
					String sCmd    = "UPDATE  TSMI_LOG_BROKER SET TXT_RESULTADO= ' " +resultado.trim()+ "'," +
							" DAT_RETORNO_BROKER = SYSDATE "+	
							" WHERE COD_TSMI_LOG_BROKER = "+ codLogBroker ;
					Statement stmtLog = conn.createStatement();
					stmtLog.execute(sCmd) ;

					/*Enviar email caso erro Broker*/
					String sCodigosErros="097,497,597,997,999";					
					String codRetorno  = resultado.substring(0,3);
					if (bErroBroker)
						codRetorno="999";
					if ( sCodigosErros.indexOf(codRetorno)>=0)
					{
						sys.EmailBean EmailBeanId = new sys.EmailBean();						
						String emailBroker =  "";
						String sEvento     =  "";
						sCmd="Select * from TSMI_EVENTO WHERE COD_EVENTO='"+getParteFixa().substring(6,9)+"'";
						rsLog = stmtLog.executeQuery(sCmd) ;
						if (rsLog.next())
							sEvento=rsLog.getString("DSC_EVENTO");

						String sConteudo   =  "Data : "+sys.Util.formatedToday()+" Transa��o : "+getParteFixa().substring(6,9)+" - "+sEvento.trim()+" codLogBroker : "+codLogBroker+"\n"+
								"Resultado : "+resultado;					    

						sCmd="Select VAL_PARAMETRO from TCAU_PARAM_SISTEMA T1 ,TCAU_SISTEMA T2 WHERE "+
								"T1.COD_SISTEMA=T2.COD_SISTEMA AND T2.NOM_ABREV='"+MYABREVSIST+"' AND NOM_PARAMETRO='EMAIL_LOG_BROKER'";
						rsLog = stmtLog.executeQuery(sCmd);
						while (rsLog.next())
						{
							emailBroker=rsLog.getString("VAL_PARAMETRO");
							EmailBeanId.setDestinatario( emailBroker.toLowerCase() );
							EmailBeanId.setRemetente( MYABREVSIST );
							EmailBeanId.setAssunto( "SMIT - TransacaoBRKR "+ MYABREVSIST+" "+sys.Util.formatedToday().substring(0,10));
							EmailBeanId.setConteudoEmail(sConteudo);
							//EmailBeanId.sendEmail();
						}
						rsLog.close();
					}
					conn.commit();
					stmtLog.close();
					rsLog.close(); 
				}
				catch (Exception ey) { }
			}
			if ( (pegouConn) && (conn!=null) ) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}		
		return resultado;
	}

	public void GravarBrokerROB(String sResultado,String sCodRetorno,String sCodOrgao) {
		String codLogBroker = "0";		
		try {

			// Preparar a numeracao da transacao			
			if (conn == null) 
				conn = serviceloc.getConnection(MYABREVSIST) ;

			// verifica e grava LOG antes do Broker
			conn.setAutoCommit(false);
			
			boolean bLogTransacao=false;			
			String sCmd = "SELECT * FROM TCAU_PARAM_ORGAO WHERE "+
			" NOM_PARAMETRO='PROCESSA_LOG_TRANSACAO' AND COD_ORGAO='"+sCodOrgao+"'" ;
			Statement stmtLog = conn.createStatement();
			ResultSet rsLog   = stmtLog.executeQuery(sCmd) ;
			if (rsLog.next())
			  bLogTransacao=(rsLog.getString("VAL_PARAMETRO").equals("S"));
			
			String sCodigosErros="097,497,597,997";
			if ( (sCodigosErros.indexOf(sCodRetorno)>=0) || (bLogTransacao) )
			{
				sCmd    = "Select SEQ_TSMI_LOG_BROKER.NEXTVAL codLog from dual" ;
				stmtLog = conn.createStatement();
				rsLog   = stmtLog.executeQuery(sCmd) ;
				while (rsLog.next()) {
					codLogBroker = rsLog.getString("codLog");
					if (codLogBroker==null) codLogBroker="0";	 
					setCodLogBroker(codLogBroker);
				}
				sCmd = "INSERT INTO TSMI_LOG_BROKER (COD_TSMI_LOG_BROKER, DAT_PROC, COD_TRANSACAO, " +
				"TXT_PARTE_FIXA, TXT_PARTE_VARIAVEL, NUM_TRANSACAO, IND_INC,DAT_ENVIO_BROKER,TXT_RESULTADO,"+
				"COD_ORGAO_LOTACAO,COD_ORGAO_ATUACAO,NOM_USERNAME) "+
				" VALUES("+codLogBroker+", SYSDATE, " +
				"'"+getParteFixa().substring(6,9) + "','" + getParteFixa() + "','" +
				getParteVariavel().trim() + "'," + getParteFixa().trim().substring(0,6) + ",1,SYSDATE,' "+sResultado.trim()+"',"+
				"'"+getCodOrgaoLotacao()+"',"+"'"+getCodOrgaoAtuacao()+"',"+ 
				"'"+getNomUserName()+"')";   
				stmtLog.execute(sCmd) ;
				conn.commit();	
			}
			stmtLog.close();
			rsLog.close();
		} 
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}  	
		finally 
		{
			
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
		}						
		return;
	}

	public void GravarBroker(String sResultado,String sCodRetorno) {
		String codLogBroker = "0";		
		try {

			// Preparar a numeracao da transacao			
			if (conn == null) 
				conn = serviceloc.getConnection(MYABREVSIST) ;

			// verifica e grava LOG antes do Broker
			conn.setAutoCommit(false);
			Statement stmtLog = conn.createStatement();
			
			String sCmd    = "Select SEQ_TSMI_LOG_BROKER.NEXTVAL codLog from dual" ;
			stmtLog = conn.createStatement();
			ResultSet rsLog   = stmtLog.executeQuery(sCmd) ;
			if (rsLog.next()) codLogBroker = rsLog.getString("codLog");
			sCmd = "INSERT INTO TSMI_LOG_BROKER (COD_TSMI_LOG_BROKER, DAT_PROC, COD_TRANSACAO, " +
			"TXT_PARTE_FIXA, TXT_PARTE_VARIAVEL, NUM_TRANSACAO, IND_INC,DAT_ENVIO_BROKER,TXT_RESULTADO,"+
			"COD_ORGAO_LOTACAO,COD_ORGAO_ATUACAO,NOM_USERNAME) "+
			" VALUES("+codLogBroker+", SYSDATE, " +
			"'"+getParteFixa().substring(6,9) + "','" + getParteFixa() + "','" +
			getParteVariavel().trim() + "'," + getParteFixa().trim().substring(0,6) + ",1,SYSDATE,' "+sResultado.trim()+"',"+
			"'"+getCodOrgaoLotacao()+"',"+"'"+getCodOrgaoAtuacao()+"',"+ 
			"'"+getNomUserName()+"')";   
			stmtLog.execute(sCmd);
			conn.commit();	
			stmtLog.close();
			rsLog.close();
		} 
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}  	
		finally 
		{
			
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
		}						
		return;
	}
	
	
	
	
	public String getDtEnvioReq() {
		return dtEnvioReq;
	}
	public void getDtEnvioReq(String dtEnvioReq) {
		if (dtEnvioReq == null) dtEnvioReq = "";
		this.dtEnvioReq = dtEnvioReq;
	}
	public String getDtRecebReq() {
		return dtRecebReq;
	}
	public void getDtRecebReq(String dtRecebReq) {
		if (dtRecebReq == null) dtRecebReq = "";
		this.dtRecebReq = dtRecebReq;
	}
	public String getDtEnvioBrk() {
		return dtEnvioBrk;
	}
	public void getDtEnvioBrk(String dtEnvioBrk) {
		if (dtEnvioBrk == null) dtEnvioBrk = "";
		this.dtEnvioBrk = dtEnvioBrk;
	}
	public String getDtRecebBrk() {
		return dtRecebBrk;
	}
	public void getDtRecebBrk(String dtRecebBrk) {
		if (dtRecebBrk == null) dtRecebBrk = "";
		this.dtRecebBrk = dtRecebBrk;
	}


	public String getCodLogBroker() {
		return codLogBroker;
	}
	public void setCodLogBroker(String codLogBroker) {
		if (codLogBroker == null) codLogBroker = "";
		else this.codLogBroker = codLogBroker;
	}

	public String getSeqTransacao() {
		return seqTransacao;
	}
	public void setSeqTransacao(String seqTransacao) {
		if(seqTransacao == null) seqTransacao = "";
		else this.seqTransacao = seqTransacao;
	}
	
	public boolean gravaLog(String codTransacao)
	{
		boolean indLogBroker = false;
		try {
	        // Preparar a numeracao da transacao			
			if (conn == null)
				conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT IND_LOG_BROKER FROM TSMI_EVENTO " +
					" WHERE COD_EVENTO = "+ codTransacao +
					" AND IND_LOG_BROKER = 'S'" ;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
			{
				indLogBroker = true;
			}
			rs.close();
			stmt.close();
		} catch (ServiceLocatorException e) {
			System.out.println("Erro - "+e.getMessage());
		} catch (SQLException e) {
			System.out.println("Erro - "+e.getMessage());
		}
		finally 
		{
			
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }
		}						
		return indLogBroker;
	}
	
	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		if (codOrgaoAtuacao==null) codOrgaoAtuacao="";
		this.codOrgaoAtuacao = codOrgaoAtuacao;
	}
	public String getCodOrgaoLotacao() {
		return codOrgaoLotacao;
	}
	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		if (codOrgaoLotacao==null) codOrgaoLotacao="";
		this.codOrgaoLotacao = codOrgaoLotacao;
	}
	public String getNomUserName() {
		return nomUserName;
	}
	public void setNomUserName(String nomUserName) {
		if (nomUserName==null) nomUserName="";
		this.nomUserName = nomUserName;
	}
	
	
}