package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ConsultaSessaoBean extends sys.HtmlPopupBean {

	private String numSessao;
	private String dataSessao;
	private String dataSessaoFim;
	private String nomUsrName;
	
	private List dados;
	private String codOrgaoLotacao; 
	private String codOrgaoAtuacao; 
	private String dataIni;
	private String dataFim;
	private String codSistema;
	private String nomSistema;
	private String ip;
	private String termAnormal;
	private String sigOrgaoLotacao;
	private String sigOrgaoAtuacao;
	
	//lista de ordenacao
	private String ordClass ;
	private String ordem ;
	

	public ConsultaSessaoBean() throws sys.BeanException {
		codOrgaoLotacao = "";
		codOrgaoAtuacao = "";
		numSessao = "";
		dataSessao = "";
		dataSessaoFim = "";
		
		nomUsrName = "";
		dataIni = "";
		dataFim = "";
		codSistema = "";
		nomSistema = "";
		ip = "";
		termAnormal = "";
		sigOrgaoAtuacao = "";
		sigOrgaoLotacao = "";
		
		dados = new ArrayList();
		ordClass = "ascendente";
		ordem = "datSessao";
	}

	public void setNumSessao(String numSessao) {
		this.numSessao = numSessao;
		if (numSessao == null)
			this.numSessao = "";
	}
	public String getNumSessao() {
		return this.numSessao;
	}
	
	public void setDataSessao(String dataSessao) {
		this.dataSessao = dataSessao;
		if (dataSessao == null)
			this.dataSessao = "";
	}
	public String getDataSessao() {
		return this.dataSessao;
	}
	
	public void setDataSessaoFim(String dataSessaoFim) {
		this.dataSessaoFim = dataSessaoFim;
		if (dataSessaoFim == null)
			this.dataSessaoFim = "";
	}
	public String getDataSessaoFim() {
		return this.dataSessaoFim;
	}

	public void setNomUsrName(String nomUsrName) {
		this.nomUsrName = nomUsrName;
		if (nomUsrName == null)
			this.nomUsrName = "";
	}
	public String getNomUsrName() {
		return this.nomUsrName;
	}
	
	public void setDataIni(String dataIni) {
			this.dataIni = dataIni;
			if (dataIni == null)
				this.dataIni = "";
	}
	public String getDataIni() {
			return this.dataIni;
	}
	
	public void setDataFim(String dataFim) {
			this.dataFim = dataFim;
			if (dataFim == null)
				this.dataFim = "";
	}
	public String getDataFim() {
			return this.dataFim;
	}
	
	public void setCodSistema(String codSistema) {
			this.codSistema = codSistema;
			if (codSistema == null)
				this.codSistema = "";
	}
	public String getCodSistema() {
			return this.codSistema;
	}
	
	public void setNomSistema(String nomSistema) {
			this.nomSistema = nomSistema;
			if (nomSistema == null)
				this.nomSistema = "";
	}
	public String getNomSistema() {
			return this.nomSistema ;
	}
	
	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
			this.codOrgaoLotacao = codOrgaoLotacao;
			if (codOrgaoLotacao == null)
				this.codOrgaoLotacao = "";
	}
	public String getCodOrgaoLotacao() {
			return this.codOrgaoLotacao;
	}
	
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
			this.codOrgaoAtuacao = codOrgaoAtuacao;
			if (codOrgaoAtuacao == null)
				this.codOrgaoAtuacao = "";
	}
	public String getCodOrgaoAtuacao() {
			return this.codOrgaoAtuacao;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
		if (ip == null)
			this.ip = "";
	}
	public String getIp() {
		return this.ip;
	}
	
	public void setTermAnormal(String termAnormal) {
		this.termAnormal = termAnormal;
		if (termAnormal == null)
			this.termAnormal = "0";
	}
	public String getTermAnormal() {
		return this.termAnormal ;
	}
	
	public void setSigOrgaoLotacao(String sigOrgaoLotacao) {
		this.sigOrgaoLotacao = sigOrgaoLotacao;
		if (sigOrgaoLotacao == null)
			this.sigOrgaoLotacao = "";
	}
	public String getSigOrgaoLotacao() {
		return this.sigOrgaoLotacao;
	}

	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;
		if (sigOrgaoAtuacao == null)
			this.sigOrgaoAtuacao = "";
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}
	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public ConsultaSessaoBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaSessaoBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaSessaoBean)this.dados.get(i);
	 }
// --------------------------  Metodos da Bean ----------------------------------


	public void consultaSessoesAtivas(ConsultaSessaoBean consultaSessaoBean)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.consultaHistoricoSessoes(consultaSessaoBean);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}
	

	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass==null) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
//	  --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "datSessao";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
		public String getNomOrdem()  {
			String nomOrdem = "Data Inicio Sess�o, N� Sess�o, Usu�rio, Sistema, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("numSessao"))  nomOrdem = "N� Sess�o, Data Inicio Sess�o, Usu�rio, Sistema, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("datSessao"))  nomOrdem = "Data Inicio Sess�o, N� Sess�o, Usu�rio, Sistema, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("datFimSessao")) nomOrdem = "Data Fim Sess�o, Sistema, N� Sess�o, Data Inicio Sess�o, Usu�rio, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("nomUsrName")) nomOrdem = "Usu�rio, Sistema, N� Sess�o, Data Inicio Sess�o, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;   
			if (this.ordem.equals("codSistema")) nomOrdem = "Sistema, N� Sess�o, Data Inicio Sess�o, Usu�rio, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("orgaoAtuacao")) nomOrdem = "Org�o Atua��o, Sistema, N� Sess�o, Data Inicio Sess�o, Usu�rio, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("orgaoLotacao")) nomOrdem = "Org�o Lota��o, Sistema, N� Sess�o, Data Inicio Sess�o, Usu�rio, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o, IP" ;
			if (this.ordem.equals("ip")) nomOrdem = "IP, Sistema, N� Sess�o, Data Inicio Sess�o, Usu�rio, Data Fim Sess�o, Org�o Atua��o, Org�o Lota��o" ;
			return nomOrdem+ " ("+getOrdClass()+")" ;
	  }
// --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
			if (ordemSol.equals("numSessao"))  ord = 1 ;
			if (ordemSol.equals("datSessao"))  ord = 2 ;   
			if (ordemSol.equals("nomUsrName")) ord = 3 ;  
			if (ordemSol.equals("codSistema")) ord = 4 ;
			if (ordemSol.equals("datFimSessao"))  ord = 5 ;   
			if (ordemSol.equals("orgaoAtuacao")) ord = 6 ;  
			if (ordemSol.equals("orgaoLotacao")) ord = 7 ; 
			if (ordemSol.equals("ip")) ord = 8 ;
			
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			if (ordemSol.equals(getOrdem()))   {
				if ( getOrdClass().equals("ascendente")) {
					trocaasc = false ;
					trocades = true ;
					setOrdClass("descendente"); 
				} 
				else {
					trocaasc = true ;
					trocades = false;
					setOrdClass("ascendente"); 
				}
			}
			else setOrdClass("ascendente");
			setOrdem(ordemSol) ;
			int tam = getDados().size() ;
			ConsultaSessaoBean tmp = new ConsultaSessaoBean();
			String numSess1,numSess2,datSess1,datSess2,usr1,usr2,sist1,sist2,
			datFimSess1,datFimSess2,ip1,ip2,orgLot1,orgLot2,orgAtu1,orgAtu2;
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					// N� Sess�o, Data Sess�o Ativa, Nome Usu�rio e Sistema
					case 0:
					  numSess1 = getDados(i).getNumSessao();
					  numSess2 = getDados(j).getNumSessao(); 
					  datSess1 = getDados(i).getDataSessao() ;
					  datSess2 = getDados(j).getDataSessao();
						usr1 = getDados(i).getNomUsrName();
						usr2 = getDados(j).getNomUsrName();
						sist1 =getDados(i).getCodSistema();
						sist2 =getDados(j).getCodSistema();
						datFimSess1 = getDados(i).getDataSessaoFim() ;
						datFimSess2 = getDados(j).getDataSessaoFim();
						ip1 = getDados(i).getIp() ;
						ip2 = getDados(j).getIp();
						orgLot1 = getDados(i).getCodOrgaoLotacao() ;
						orgLot2 = getDados(j).getCodOrgaoLotacao();
						orgAtu1 = getDados(i).getCodOrgaoAtuacao() ;
						orgAtu2 = getDados(j).getCodOrgaoAtuacao();
						
						if ((numSess1+datSess1+usr1+sist1).compareTo(numSess2+datSess2+usr2+sist2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "N� Sess�o"
					case 1:
						if(getDados(i).getNumSessao().compareTo(getDados(j).getNumSessao())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
					// "Data Sess�o" ;   
					case 2:
						if (getDados(i).getDataSessao().compareTo(getDados(j).getDataSessao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Nome Usu�rio" ;   
					case 3:
						if (getDados(i).getNomUsrName().compareTo(getDados(j).getNomUsrName())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Sistema" ; 
					case 4:
					  if (getDados(i).getNomSistema().compareTo(getDados(j).getNomSistema())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
                    //"Data Fim Sess�o" ; 
					case 5:
					  if (getDados(i).getDataSessaoFim().compareTo(getDados(j).getDataSessaoFim())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
                    //"Org�o Atua��o" ; 
					case 6:
					  if (getDados(i).getCodOrgaoAtuacao().compareTo(getDados(j).getCodOrgaoAtuacao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
                    //"Org�o Lota��o" ; 
					case 7:
					  if (getDados(i).getCodOrgaoLotacao().compareTo(getDados(j).getCodOrgaoLotacao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
                    //"IP" ; 
					case 8:
					  if (getDados(i).getIp().compareTo(getDados(j).getIp())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Data Sess�o" ;   	
					default:
					  if (getDados(i).getDataSessao().compareTo(getDados(j).getDataSessao())>0 ) troca=trocaasc;
					  else troca=trocades ;
				    break;
				}	
				if (troca) 	{		
				   tmp = (ConsultaSessaoBean)getDados(i);
				   getDados().set(i,getDados(j));
				   getDados().set(j,tmp);
				}
			}
		}
	  }	

}