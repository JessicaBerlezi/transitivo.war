package ACSS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios<br>
 * <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar OperacoesSistema<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Sergio Monteiro
 * @version 1.0
 * @Updates
 */

public class PerfilUsuarioLotacaoCmd extends sys.Command {
	
	private static final String jspPadrao="/ACSS/perfilUsuarioLotacao.jsp";      
	private String next;
	
	public PerfilUsuarioLotacaoCmd() {
		next             =  jspPadrao;
	}
	
	public PerfilUsuarioLotacaoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			// cria os Beans, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
			ACSS.UsuarioFuncBean UsrFuncLogado     = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsrFuncLogado==null)  UsrFuncLogado = new ACSS.UsuarioFuncBean() ;	  			
			
			// cria os Beans de Usuario e Perfil, se n�o existir
			ACSS.OrgaoBean OrgaoBeanId  = (ACSS.OrgaoBean)req.getAttribute("OrgaoBeanId") ;
			if (OrgaoBeanId==null)        OrgaoBeanId = new ACSS.OrgaoBean() ;	  			
			
			ACSS.UsuarioBean UsrBeanId  = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
			if (UsrBeanId==null)          UsrBeanId = new ACSS.UsuarioBean() ;	  			
			
			ACSS.PerfilLotacaoBean PerfilLotBeanId = (ACSS.PerfilLotacaoBean)req.getAttribute("PerfilLotBeanId") ;
			if (PerfilLotBeanId==null)         PerfilLotBeanId = new ACSS.PerfilLotacaoBean() ;	  			
			
			// obtem e valida os parametros recebidos					
			String acao          = req.getParameter("acao");  
			if(acao==null) acao  = " ";
			String codOrgao      = req.getParameter("codOrgao"); 		
			if (codOrgao==null) codOrgao="0";
			String codUsuario = req.getParameter("codUsuario"); 		
			if (codUsuario==null) codUsuario="0";
			
			String nomUserNameOper      = UsrLogado.getNomUserName();	     
			String codOrgaoLotacaoOper  = UsrLogado.getOrgao().getCodOrgao();
			
			
			if (acao.equals("buscaUsuarios"))
			{
				OrgaoBeanId.setCodOrgao(codOrgao);						
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);							
			}	
			
			if (acao.equals("buscaSistemas")) {
				OrgaoBeanId.Le_Orgao(codOrgao,0);	
				OrgaoBeanId.setAtualizarDependente("S");
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				PerfilLotBeanId.setCodOrgao(codOrgao);
				PerfilLotBeanId.setCodUsuario(codUsuario);
				PerfilLotBeanId.setCodUsrLogado(UsrLogado.getCodUsuario());
				PerfilLotBeanId.setCodNivelUsrLogado(UsrFuncLogado.getCodNivel());			
				PerfilLotBeanId.Le_PerfilLotacao();
			}		
			
			if (acao.equals("A")) {
				OrgaoBeanId.Le_Orgao(codOrgao,0);	
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				
				String codSistema[] = req.getParameterValues("codSistema"); 		
				if (codSistema==null) codSistema= new String[0];
				
				String codPerfilUsrnov[] = req.getParameterValues("codPerfilUsrnov"); 		
				if (codPerfilUsrnov==null) codPerfilUsrnov= new String[0];
				
				String codNivelUsrnov[] = req.getParameterValues("codNivelUsrnov"); 		
				if (codNivelUsrnov==null) codNivelUsrnov= new String[0];
				
				String txtJustificativa[] = req.getParameterValues("txtJustificativa"); 		
				if (txtJustificativa==null) txtJustificativa= new String[0];

				
				if ( (codSistema.length!=codPerfilUsrnov.length) 
					|| (codSistema.length!=codNivelUsrnov.length) 
					|| (codSistema.length != txtJustificativa.length)	)	{
					OrgaoBeanId.setMsgErro("Parametros invalidos.") ;
				}
				else {
					PerfilLotBeanId.setCodOrgao(codOrgao);
					PerfilLotBeanId.setCodUsuario(codUsuario);
					PerfilLotBeanId.gravaHistorico(codSistema,codPerfilUsrnov,codNivelUsrnov,txtJustificativa);
					
					PerfilLotBeanId.setNomUserNameOper(nomUserNameOper);
					PerfilLotBeanId.setOrgaoLotacaoOper(codOrgaoLotacaoOper);
					
					
					PerfilLotBeanId.atualPerfil(codSistema,codPerfilUsrnov,codNivelUsrnov) ;
					OrgaoBeanId.setMsgErro(PerfilLotBeanId.getMsgErro());
					OrgaoBeanId.setAtualizarDependente("S");
					PerfilLotBeanId.setCodOrgao(codOrgao);
					PerfilLotBeanId.setCodUsuario(codUsuario);
					PerfilLotBeanId.setCodUsrLogado(UsrLogado.getCodUsuario());
					PerfilLotBeanId.setCodNivelUsrLogado(UsrFuncLogado.getCodNivel());			
					PerfilLotBeanId.Le_PerfilLotacao();
				}
			}
			
			if (acao.equals("V")) {
				OrgaoBeanId.setAtualizarDependente("N");
				OrgaoBeanId.setCodOrgao(codOrgao);						
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);							
			}
			
			req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
			req.setAttribute("PerfilLotBeanId",PerfilLotBeanId) ;		  				  
			req.setAttribute("UsrBeanId",UsrBeanId) ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("PerfisUsuarioLotacaoCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
}