package ACSS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import REC.AutoInfracaoBean;
import REC.ControladorRecGeneric;
import REC.DaoBrokerFactory;
import REC.GerenciarAtaCmd;
import REC.HistoricoBean;
import REC.InfracaoBean;
import REC.RequerimentoBean;
import TAB.DaoException;
import UTIL.DataUtils;
import UTIL.Logs;

public class Ata extends sys.HtmlPopupBean {

	public static ArrayList<HashMap<String, String>> arListSemAtribuicao = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> arListComAtribuicao = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> arListBuscaInfResult = new ArrayList<HashMap<String, String>>();

	private String codAta;
	private String tpAta;
	private String descAta;
	private String dataSessao;
	private String dataDistribuicao;
	private String juntaAta;
	private String dataAviso;
	private String codOrgao;
	private String dataInclusao;
	private String statusAta;
	private int nrAta;
	private int anoAta;
	private int qtdeAtaProcesso;
	private ArrayList<Ata> listAtas;
	private String selectAta;
	private String selectAtaRelator;
	private String dataPublicacao;
	private int cdNatureza;

	public static String Ret;
	public static int flNrProcessoAtribuidoMaiorQueInformado = 0;
	public static String idAtaGen = "";

	public Ata() {
	}

	public Ata(String Nome, String Select) {
		Ret = montaSelectSql(Nome, Select, "");
	}

	public int getCdNatureza() {
		return cdNatureza;
	}

	public void setCdNatureza(int cdNatureza) {
		this.cdNatureza = cdNatureza;
	}

	public String getDataPublicacao() {
		return dataPublicacao;
	}

	public void setDataPublicacao(String dataPublicacao) {
		this.dataPublicacao = dataPublicacao;
	}

	public String getCodAta() {
		return codAta;
	}

	public void setCodAta(String codAta) {
		this.codAta = codAta;
	}

	public String getTpAta() {
		return tpAta;
	}

	public void setTpAta(String tpAta) {
		this.tpAta = tpAta;
	}

	public String getDescAta() {
		return descAta;
	}

	public void setDescAta(String descAta) {
		this.descAta = descAta;
	}

	public int getQtdeAtaProcesso() {
		return qtdeAtaProcesso;
	}

	public void setQtdeAtaProcesso(int qtdeAtaProcesso) {
		this.qtdeAtaProcesso = qtdeAtaProcesso;
	}

	public String getDataSessao() {
		return dataSessao;
	}

	public void setDataSessao(String dataSessao) {
		this.dataSessao = dataSessao;
	}

	public String getDataDistribuicao() {
		return dataDistribuicao;
	}

	public void setDataDistribuicao(String dataDistribuicao) {
		this.dataDistribuicao = dataDistribuicao;
	}

	public String getJuntaAta() {
		return juntaAta;
	}

	public void setJuntaAta(String juntaAta) {
		this.juntaAta = juntaAta;
	}

	public String getDataAviso() {
		return dataAviso;
	}

	public void setDataAviso(String dataAviso) {
		this.dataAviso = dataAviso;
	}

	public String getCodOrgao() {
		return codOrgao;
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
	}

	public String getDataInclusao() {
		return dataInclusao;
	}

	public void setDataInclusao(String dataInclusao) {
		this.dataInclusao = dataInclusao;
	}

	public String getStatusAta() {
		return statusAta;
	}

	public void setStatusAta(String statusAta) {
		this.statusAta = statusAta;
	}

	public String getSelectAta() {
		return selectAta;
	}

	public void setSelectAta(String selectAta) {
		this.selectAta = selectAta;
	}

	public String getSelectAtaRelator() {
		return selectAtaRelator;
	}

	public void setSelectAtaRelator(String selectAtaRelator) {
		this.selectAtaRelator = selectAtaRelator;
	}

	public int getflNrProcessoAtribuidoMaiorQueInformado() {
		return flNrProcessoAtribuidoMaiorQueInformado;
	}

	public void setflNrProcessoAtribuidoMaiorQueInformado(int flNrProcessoAtribuidoMaiorQueInformado) {
		this.flNrProcessoAtribuidoMaiorQueInformado = flNrProcessoAtribuidoMaiorQueInformado;
	}

	public int getNrAta() {
		return nrAta;
	}

	public void setNrAta(int nrAta) {
		this.nrAta = nrAta;
	}

	public int getAnoAta() {
		return anoAta;
	}

	public void setAnoAta(int anoAta) {
		this.anoAta = anoAta;
	}

	public boolean isInsert(Ata ata) {
		boolean bOk = true;
		Vector vErro = new Vector();
		try {
			Dao dao = Dao.getInstance();

			if (dao.AtaExiste(ata)) {
				vErro.addElement("Ata j� cadastrada com esta Descri��o : " + ata.getDescAta() + " \n" + "Tipo : "
						+ ata.getTpAta() + " \n" + "Status : " + ata.getStatusAta() + " \n");
				bOk = false;
			} else {
				if (dao.AtaInsert(ata)) {
					vErro.addElement("Ata incluida: " + ata.getDescAta() + " \n");
				} else {
					vErro.addElement("Usuario n�o incluido: " + ata.getDescAta() + " \n");
				}
			}
		} catch (Exception e) {
			Logs.gravarLog("exce��o no metodo: isInsert: " + e.getMessage());

			vErro.addElement("Ata n�o incluido: " + ata.getDescAta() + " \n");
			vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch
		setMsgErro(vErro);
		return bOk;
	}

	public boolean isAltera(Ata ata) {
		boolean bOk = true;
		Vector vErro = new Vector();
		try {
			Dao dao = Dao.getInstance();
			if (dao.AtaExisteI(ata) == false) {
				vErro.addElement("Ata n�o cadastrado com " + ata.getDescAta() + " \n");
				bOk = false;
			} else {
				// if (dao.UsuarioExisteOutro(this)) {
				// vErro.addElement("Existe outro Usuario cadastrado com Login:
				// "+this.nomUserName+" \n") ;
				// bOk = false ;
				// }
				// else
				{
					if (dao.AtaUpdate(ata))
						vErro.addElement("Ata alterada: " + ata.getDescAta() + " \n");
					else
						vErro.addElement("Ata n�o alterada: " + ata.getDescAta() + " \n");
				}
			}
		} catch (Exception e) {
			vErro.addElement("Ata n�o alterada: " + ata.getDescAta() + " \n");
			vErro.addElement("Erro na altera��o: " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch
		setMsgErro(vErro);
		return bOk;
	}

	public boolean isDelete(Ata ata) {
		boolean bOk = true;
		Vector vErro = new Vector();
		try {
			Dao dao = Dao.getInstance();
			if (dao.AtaExisteI(ata) == false) {
				vErro.addElement("Ata n�o cadastrada com " + ata.getDescAta() + " \n");
				bOk = false;
			} else {
				if (dao.AtaDelete(ata))
					vErro.addElement("Ata excluida " + ata.getDescAta() + " \n");

				else
					vErro.addElement("Ata n�o excluido: " + ata.getDescAta() + " \n");
			}
		} catch (Exception e) {
			vErro.addElement("Usuario n�o excluido: " + ata.getDescAta() + " \n");
			vErro.addElement("Erro na exclus�o: " + e.getMessage() + " \n");
			bOk = false;
		} // fim do catch
		setMsgErro(vErro);
		return bOk;
	}

	public ArrayList<Ata> getAtasDF() {
		try {
			Dao dao = Dao.getInstance();
			listAtas = dao.getAtasDF();
		} catch (Exception e) {
		}
		return listAtas;
	}

	public static ArrayList<Ata> consultarAtas() {
		ArrayList<Ata> atas = new ArrayList<Ata>();

		try {
			atas = Dao.getInstance().getAtasDF();
		} catch (Exception ex) {
		}

		return atas;
	}

	public static List<Ata> consultarAtasPorStatus(String status, String cdNatureza) throws Exception {
		List<Ata> atas = Dao.getInstance().consultarAtasPorStatus(status, cdNatureza);

		return atas;
	}

	public ArrayList<Ata> getAtasTp(String tpAta) {
		try {
			Dao dao = Dao.getInstance();
			listAtas = dao.getAtasDF();
		} catch (Exception e) {
		}
		return listAtas;
	}

	public ArrayList<Ata> getAtasJR() {
		try {
			Dao dao = Dao.getInstance();
			listAtas = dao.getAtasJR();
		} catch (Exception e) {
		}
		return listAtas;
	}

	public ArrayList<Ata> getAtasII() {
		try {
			Dao dao = Dao.getInstance();
			listAtas = dao.getAtasII();
		} catch (Exception e) {
		}
		return listAtas;
	}

	public Ata getAtasGen(String codAta) {
		Ata ata = new Ata();
		try {
			Dao dao = Dao.getInstance();
			ata = dao.getAtasGen(codAta);
		} catch (Exception e) {
		}
		return ata;
	}

	public Ata getAtasDaVez(String nrAuto) {
		Ata ata = new Ata();
		try {
			Dao dao = Dao.getInstance();
			ata = dao.getAtasDaVez(nrAuto);
		} catch (Exception e) {
		}
		return ata;
	}

	public Ata getAtasInfResult(String nrAuto) {
		Ata ata = new Ata();
		try {
			Dao dao = Dao.getInstance();
			ata = dao.getAtasInfResult(nrAuto);
		} catch (Exception e) {
		}
		return ata;
	}

	public ArrayList<Ata> buscaAtaDistribuicao(String descAta) {
		try {
			Ata ata = new Ata();
			Dao dao = Dao.getInstance();
			ata.setDescAta(descAta);
			listAtas = dao.getAtaDistribuicao(ata);
		} catch (Exception e) {
		}
		return listAtas;
	}

	public String getVerificaQtdeNrProcesso(String nameSelect) {
		String vlRetorno = "";
		try {
			Dao dao = Dao.getInstance();
			vlRetorno = dao.getVerificaQtdeNrProcesso(nameSelect);
			return vlRetorno;
		} catch (Exception e) {
		}
		return vlRetorno;
	}

	public boolean getNexAta(String nameSelect) {
		boolean vlRetorno = false;
		try {
			Dao dao = Dao.getInstance();
			vlRetorno = dao.getNexAta(nameSelect);
		} catch (Exception e) {
		}
		return vlRetorno;
	}

	public ArrayList<Ata> getAtasPublicacao(String tipoAta, String year, String month, String codOrgao,
			String cdNatureza) {
		ArrayList<Ata> listAta = new ArrayList<Ata>();

		try {
			Dao dao = Dao.getInstance();
			listAta = dao.getAtasPublicacao(tipoAta, year, month, codOrgao, cdNatureza);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE ATA: AO CONSULTAR AS ATA PARA PUBLICA��O!");
		}

		return listAta;
	}

	public String montaSelectSqlAbrirProcesso(String sqlOption, String nameSelect, String cdNatureza) {
		String sqlOpt = "";
		try {
			Dao dao = Dao.getInstance();
			sqlOpt = dao.montaSelectSqlAbrirProcesso(sqlOption, nameSelect, cdNatureza);
		} catch (Exception e) {
		}
		return sqlOpt;
	}

	public String montaSelectSql(String sqlOption, String nameSelect, String cdNatureza) {
		String sqlOpt = "";
		try {
			Dao dao = Dao.getInstance();
			sqlOpt = dao.montaSelectSql(sqlOption, nameSelect, cdNatureza);
		} catch (Exception e) {
		}
		return sqlOpt;
	}

	public String montaSelectSqlRelator(String nameSelect) {
		String sqlOpt = "";
		try {
			Dao dao = Dao.getInstance();
			sqlOpt = dao.montaSelectSqlRelator(nameSelect);
		} catch (Exception e) {
		}
		return sqlOpt;
	}

	// metodo do victor
	public String montarRelator(String nameSelect, String junta, String codOrgao, String cdNatureza) {
		String sqlOpt = "";
		try {
			Dao dao = Dao.getInstance();
			sqlOpt = dao.SelectRelatorProcesso(nameSelect, junta, codOrgao, cdNatureza);
		} catch (Exception e) {
		}
		return sqlOpt;
	}

	public ArrayList<HashMap<String, String>> getAtaDistribuicaoMap(String descAta, String sigFuncao) {
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();

		try {
			Ata ata = new Ata();
			Dao dao = Dao.getInstance();
			ata.setDescAta(descAta);
			arList = dao.getAtaDistribuicaoMap(ata, sigFuncao);
		} catch (Exception e) {
		}
		return arList;
	}

	public ArrayList<HashMap<String, String>> getAtaDeliberacaoMap(String descAta, String sigFuncao) {
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();

		try {
			Ata ata = new Ata();
			Dao dao = Dao.getInstance();
			ata.setDescAta(descAta);
			arList = dao.getAtaDeliberacaoMap(ata, sigFuncao);
		} catch (Exception e) {
		}
		return arList;
	}

	public ArrayList<HashMap<String, String>> getAtaPublicacaoMap(String descAta, String sigFuncao) {
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();

		try {
			Ata ata = new Ata();
			Dao dao = Dao.getInstance();
			ata.setDescAta(descAta);
			arList = dao.getAtaPublicacaoMap(ata, sigFuncao);
		} catch (Exception e) {
		}
		return arList;
	}

	/*
	 * Retorna true se a data for um sabado, domingo ou feriado.
	 */
	public boolean validaStatusAta(String data) {
		try {
			Dao dao = Dao.getInstance();
			return dao.validaStatusAtaDao(data);
		} catch (Exception e) {

		}
		return false;
	}

	/*
	 * Retorna true se a data for um sabado, domingo ou feriado.
	 */
	public boolean concluirAta(String data) {
		try {
			Dao dao = Dao.getInstance();
			return dao.concluirAta(data);
		} catch (Exception e) {

		}
		return false;
	}

	/*
	 * Retorna true se a data for um sabado, domingo ou feriado.
	 */
	public boolean validaData(String data) {
		try {
			Dao dao = Dao.getInstance();
			return dao.validaDataDao(data);
		} catch (Exception e) {

		}
		return false;
	}

	public Date convertStringInDate(String date) {// 10/03/2014
		Date data = null;
		DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
		try {
			data = new Date(fmt.parse(date).getTime());
		} catch (ParseException ex) {
			ex.printStackTrace();
		}
		return data;
	}

	public String convertDateInString(java.util.Date dtData) {
		try {
			DateFormat fmt = new SimpleDateFormat("dd/MM/yyyy");
			return fmt.format(dtData);
		} catch (Exception Ex) {
			return "Erro na convers�o da data";
		}
	}

	public ArrayList<HashMap<String, String>> getProcessoComAtribuicaoMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator, String codOrgao) {

		try {
			Dao dao = Dao.getInstance();
			arListComAtribuicao = dao.getProcessoComAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator,
					nomeRelator, codOrgao);
		} catch (Exception e) {
		}
		return arListComAtribuicao;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosPorRelator(String codAta, String codOrgao,
			String codRelator) {
		try {
			return Dao.getInstance().consultarProcessosPorRelator(codAta, codOrgao, codRelator);
		} catch (Exception ex) {
			return new ArrayList<HashMap<String, String>>();
		}
	}

	public void transfereDadosSemAtribuicaoPAtribuicao(String codRelator, String dsRelator, String codRequerimento,
			String nomeUsuario) {
		if (arListSemAtribuicao.size() > 0) {
			try {
				String codReq;

				for (HashMap<String, String> item : arListSemAtribuicao) {
					if (item.get("cod_requerimento").equals(codRequerimento)) {
						codReq = codRequerimento;

						REC.DaoBroker dao = DaoBrokerFactory.getInstance();

						try {
							// TABLE TSMI_AUTO_INFRACAO
							// TABLE TSMI_REQUERIMENTO
							// TABLE TSMI_HISTORICO_AUTO

							REC.RequerimentoBean myReq = RequerimentoBean.consultarRequerimentoPor(codReq);
							AutoInfracaoBean myAuto = AutoInfracaoBean
									.consultarAutoPor(Integer.parseInt(myReq.getCodAutoInfracao()));
							Ata ata = Ata.consultarAtaPor(myReq.getCodAta());
							HistoricoBean myHis = new HistoricoBean();

							if ((codRelator.length() > 0) && (dsRelator.length() > 0)) {
								myAuto.setDatInfracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInfracao()));
								myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(myAuto.getDatProcesso()));
								myAuto.setDatStatus(ControladorRecGeneric.dateToDateBr(myAuto.getDatStatus()));
								myAuto.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatPublPDO()));
								myAuto.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatEnvioDO()));
								myAuto.setDatInclusao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInclusao()));
								myAuto.setDatRegistro(ControladorRecGeneric.dateToDateBr(myAuto.getDatRegistro()));
								myAuto.setDatAlteracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatAlteracao()));

								myReq.setCodRelatorJU(codRelator);
								myReq.setNomRelatorJU(dsRelator);

								myReq.setDatAtuTR(ControladorRecGeneric.dateToDateBr(myReq.getDatAtuTR()));
								myReq.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioDO()));
								myReq.setDatEnvioGuia(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioGuia()));
								myReq.setDatEST(ControladorRecGeneric.dateToDateBr(myReq.getDatEST()));
								myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
								myReq.setDatJU(ControladorRecGeneric.dateToDateBr(myReq.getDatJU()));
								myReq.setDatPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatPJ()));
								myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
								myReq.setDatProcEST(ControladorRecGeneric.dateToDateBr(myReq.getDatProcEST()));
								myReq.setDatProcJU(ControladorRecGeneric.dateToDateBr(myReq.getDatProcJU()));
								myReq.setDatProcPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatProcPJ()));
								myReq.setDatProcRS(ControladorRecGeneric.dateToDateBr(myReq.getDatProcRS()));
								myReq.setDatProcTR(ControladorRecGeneric.dateToDateBr(myReq.getDatProcTR()));
								myReq.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myReq.getDatPublPDO()));
								myReq.setDatRecebimentoGuia(
										ControladorRecGeneric.dateToDateBr(myReq.getDatRecebimentoGuia()));
								myReq.setDatRequerimento(
										ControladorRecGeneric.dateToDateBr(myReq.getDatRequerimento()));
								myReq.setDatRS(ControladorRecGeneric.dateToDateBr(myReq.getDatRS()));
								myReq.setDatValidTR(ControladorRecGeneric.dateToDateBr(myReq.getDatValidTR()));

								ata.setDataAviso(ControladorRecGeneric.dateToDateBr(ata.getDataAviso()));
								ata.setDataDistribuicao(ControladorRecGeneric.dateToDateBr(ata.getDataDistribuicao()));
								ata.setDataInclusao(ControladorRecGeneric.dateToDateBr(ata.getDataInclusao()));
								ata.setDataSessao(ControladorRecGeneric.dateToDateBr(ata.getDataSessao()));
								ata.setDataPublicacao(ControladorRecGeneric.dateToDateBr(ata.getDataPublicacao()));

								myHis.setTxtComplemento01(myReq.getNumRequerimento());
								myHis.setTxtComplemento02(myReq.getCodTipoSolic());
								myHis.setTxtComplemento03(myReq.getCodResultRS());
								myHis.setNomUserName(nomeUsuario);
								myHis.setCodOrgaoLotacao(myAuto.getCodOrgao());
								myHis.setTxtComplemento12(codRelator);
								myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());
								myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao());
								myHis.setNumProcesso(myAuto.getNumProcesso());
								myHis.setCodOrgao(myAuto.getCodOrgao());
								myHis.setCodStatus(myAuto.getCodStatus());
								myHis.setDatStatus(myAuto.getDatStatus());
								myHis.setCodOrigemEvento("100");
								myHis.setCodEvento(HistoricoBean.EVENTO_ATRIBUIR_RELATOR);

								dao.atualizarAutoProcessos(myAuto, myReq, myHis, ata);
							} else {

							}
						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public void transfereDadosSemAtribuicaoPAtribuicao(String codRelator, String dsRelator, int remove) {
		if (arListSemAtribuicao.size() > 0) {
			try {
				String numAuto;
				String codAuto;
				String numRequerimento;
				String numProcesso;
				String codAta;
				String dsAta;
				String tpAta;
				String dataProcesso;

				for (int i = 0; i < arListSemAtribuicao.size(); i++) {
					if (i == remove) {
						numAuto = arListSemAtribuicao.get(i).get("NUM_AUTO_INFRACAO");
						codAuto = arListSemAtribuicao.get(i).get("COD_AUTO_INFRACAO");
						numProcesso = arListSemAtribuicao.get(i).get("NUM_PROCESSO");
						numRequerimento = arListSemAtribuicao.get(i).get("NUM_REQUERIMENTO");

						codAta = arListSemAtribuicao.get(i).get("COD_ATA");
						tpAta = arListSemAtribuicao.get(i).get("TIPO_ATA");
						dsAta = arListSemAtribuicao.get(i).get("DES_ATA");

						dataProcesso = arListSemAtribuicao.get(i).get("DAT_PROCESSO");

						REC.DaoBroker dao = DaoBrokerFactory.getInstance();

						try {
							// TABLE TSMI_AUTO_INFRACAO
							// TABLE TSMI_REQUERIMENTO
							// TABLE TSMI_HISTORICO_AUTO

							AutoInfracaoBean myAuto = new AutoInfracaoBean();
							REC.RequerimentoBean myReq = new REC.RequerimentoBean();
							HistoricoBean myHis = new HistoricoBean();
							Ata ata = new Ata();

							/*
							 * Processo sem relator: Atribiur processo a uma
							 * determinado relator
							 */

							if ((numAuto.length() > 0) && (codAuto.length() > 0)) {
								if ((codRelator.length() > 0) && (dsRelator.length() > 0)) {

									if (tpAta.equals("Def. Previa")) {
										// codTipoSolic = "DP";
										myAuto.setCodStatus("5");
									}
									if (tpAta.equals("1� INST�NCIA")) {
										// codTipoSolic = "1P";
										myAuto.setCodStatus("15");
									}
									if (tpAta.equals("2� INST�NCIA")) {
										myAuto.setCodStatus("35");
										// codTipoSolic = "2P";
									}

									ata.setCodAta(codAta);
									ata.setTpAta(tpAta);
									ata.setDescAta(dsAta);

									myAuto.setNumAutoInfracao(numAuto);
									myAuto.setCodAutoInfracao(codAuto);
									myAuto.setNumProcesso(numProcesso);
									myAuto.setTpRequerimento(tpAta);

									if (dataProcesso != "") {
										myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(dataProcesso));
									}

									myReq.setNumRequerimento(numRequerimento);
									myReq.setCodAutoInfracao(codAuto);
									myReq.setCodRelatorJU(codRelator);
									myReq.setNomRelatorJU(dsRelator);

									myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());
									myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao());
									myHis.setNumProcesso(numProcesso);
									myHis.setCodOrgao(myAuto.getCodOrgao());
									myHis.setCodStatus(myAuto.getCodStatus());
									myHis.setDatStatus(myAuto.getDatStatus());
									myHis.setCodOrigemEvento("100");
									myHis.setCodEvento("206");

									dao.atualizarAutoProcessos(myAuto, myReq, myHis, ata);
								} else {

								}
							}
						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public void transfereDadosComAtribuicaoPAtribuicao(String codRelator, String dsRelator, int remove) {
		if (arListComAtribuicao.size() > 0) {
			try {
				String numAuto;
				String codAuto;
				String numRequerimento;
				String numProcesso;
				String codAta;
				String dsAta;
				String tpAta;
				String dataProcesso;

				for (int i = 0; i < arListComAtribuicao.size(); i++) {
					if (i == remove) {
						numAuto = arListComAtribuicao.get(i).get("NUM_AUTO_INFRACAO");
						codAuto = arListComAtribuicao.get(i).get("COD_AUTO_INFRACAO");
						numProcesso = arListComAtribuicao.get(i).get("NUM_PROCESSO");
						numRequerimento = arListComAtribuicao.get(i).get("NUM_REQUERIMENTO");

						codAta = arListComAtribuicao.get(i).get("COD_ATA");
						tpAta = arListComAtribuicao.get(i).get("TIPO_ATA");
						dsAta = arListComAtribuicao.get(i).get("DES_ATA");

						dataProcesso = arListComAtribuicao.get(i).get("DAT_PROCESSO");

						REC.DaoBroker dao = DaoBrokerFactory.getInstance();

						try {
							// TABLE TSMI_AUTO_INFRACAO
							// TABLE TSMI_REQUERIMENTO
							// TABLE TSMI_HISTORICO_AUTO

							AutoInfracaoBean myAuto = new AutoInfracaoBean();
							REC.RequerimentoBean myReq = new REC.RequerimentoBean();
							HistoricoBean myHis = new HistoricoBean();
							Ata ata = new Ata();

							/*
							 * Processo sem relator: Remover processo de uma
							 * determinado relator
							 */

							if ((numAuto.length() > 0) && (codAuto.length() > 0)) {
								if ((codRelator.length() > 0) && (dsRelator.length() > 0)) {

									if (tpAta.equals("Def. Previa")) {
										// codTipoSolic = "DP";
										myAuto.setCodStatus("5");
									}
									if (tpAta.equals("1� INST�NCIA")) {
										// codTipoSolic = "1P";
										myAuto.setCodStatus("15");
									}
									if (tpAta.equals("2� INST�NCIA")) {
										myAuto.setCodStatus("35");
										// codTipoSolic = "2P";
									}

									ata.setCodAta(codAta);
									ata.setTpAta(tpAta);
									ata.setDescAta(dsAta);

									myAuto.setNumAutoInfracao(numAuto);
									myAuto.setCodAutoInfracao(codAuto);

									if (dataProcesso != "") {
										myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(dataProcesso));
									}

									myAuto.setTpRequerimento(tpAta);

									myReq.setNumRequerimento(numRequerimento);
									myReq.setCodAutoInfracao(codAuto);
									myReq.setCodRelatorJU("0");
									myReq.setNomUserNameJU("SELECIONE");

									myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());
									myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao());
									myHis.setNumProcesso(numProcesso);
									myHis.setCodOrgao(myAuto.getCodOrgao());
									myHis.setCodStatus(myAuto.getCodStatus());
									myHis.setDatStatus(myAuto.getDatStatus());
									myHis.setCodOrigemEvento("100");
									myHis.setCodEvento("206");

									dao.atualizarAutoProcessos(myAuto, myReq, myHis, ata);
								} else {

								}
							}
						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public void transfereDadosComAtribuicaoPAtribuicao(String codRelator, String dsRelator, String codRequerimento,
			String nomeUsuario) {
		if (arListComAtribuicao.size() > 0) {
			try {
				for (HashMap<String, String> item : arListComAtribuicao) {
					if (item.get("cod_requerimento").equals(codRequerimento)) {
						REC.DaoBroker dao = DaoBrokerFactory.getInstance();

						try {
							// TABLE TSMI_AUTO_INFRACAO
							// TABLE TSMI_REQUERIMENTO
							// TABLE TSMI_HISTORICO_AUTO

							REC.RequerimentoBean myReq = RequerimentoBean.consultarRequerimentoPor(codRequerimento);
							AutoInfracaoBean myAuto = AutoInfracaoBean
									.consultarAutoPor(Integer.parseInt(myReq.getCodAutoInfracao()));
							Ata ata = Ata.consultarAtaPor(myReq.getCodAta());
							HistoricoBean myHis = new HistoricoBean();

							if ((codRelator.length() > 0) && (dsRelator.length() > 0)) {
								myAuto.setDatInfracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInfracao()));
								myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(myAuto.getDatProcesso()));
								myAuto.setDatStatus(ControladorRecGeneric.dateToDateBr(myAuto.getDatStatus()));
								myAuto.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatPublPDO()));
								myAuto.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatEnvioDO()));
								myAuto.setDatInclusao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInclusao()));
								myAuto.setDatRegistro(ControladorRecGeneric.dateToDateBr(myAuto.getDatRegistro()));
								myAuto.setDatAlteracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatAlteracao()));

								myReq.setCodRelatorJU("0");
								myReq.setNomUserNameJU("SELECIONE");

								myReq.setDatAtuTR(ControladorRecGeneric.dateToDateBr(myReq.getDatAtuTR()));
								myReq.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioDO()));
								myReq.setDatEnvioGuia(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioGuia()));
								myReq.setDatEST(ControladorRecGeneric.dateToDateBr(myReq.getDatEST()));
								myReq.setDatJU(ControladorRecGeneric.dateToDateBr(myReq.getDatJU()));
								myReq.setDatPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatPJ()));
								myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
								myReq.setDatProcEST(ControladorRecGeneric.dateToDateBr(myReq.getDatProcEST()));
								myReq.setDatProcJU(ControladorRecGeneric.dateToDateBr(myReq.getDatProcJU()));
								myReq.setDatProcPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatProcPJ()));
								myReq.setDatProcRS(ControladorRecGeneric.dateToDateBr(myReq.getDatProcRS()));
								myReq.setDatProcTR(ControladorRecGeneric.dateToDateBr(myReq.getDatProcTR()));
								myReq.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myReq.getDatPublPDO()));
								myReq.setDatRecebimentoGuia(
										ControladorRecGeneric.dateToDateBr(myReq.getDatRecebimentoGuia()));
								myReq.setDatRequerimento(
										ControladorRecGeneric.dateToDateBr(myReq.getDatRequerimento()));
								myReq.setDatRS(ControladorRecGeneric.dateToDateBr(myReq.getDatRS()));
								myReq.setDatValidTR(ControladorRecGeneric.dateToDateBr(myReq.getDatValidTR()));

								ata.setDataAviso(ControladorRecGeneric.dateToDateBr(ata.getDataAviso()));
								ata.setDataDistribuicao(ControladorRecGeneric.dateToDateBr(ata.getDataDistribuicao()));
								ata.setDataInclusao(ControladorRecGeneric.dateToDateBr(ata.getDataInclusao()));
								ata.setDataSessao(ControladorRecGeneric.dateToDateBr(ata.getDataSessao()));
								ata.setDataPublicacao(ControladorRecGeneric.dateToDateBr(ata.getDataPublicacao()));

								myHis.setTxtComplemento01(myReq.getNumRequerimento());
								myHis.setTxtComplemento02(myReq.getCodTipoSolic());
								myHis.setTxtComplemento03(myReq.getCodResultRS());
								myHis.setNomUserName(nomeUsuario);
								myHis.setCodOrgaoLotacao(myAuto.getCodOrgao());
								myHis.setTxtComplemento12("0");
								myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());
								myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao());
								myHis.setNumProcesso(myAuto.getNumProcesso());
								myHis.setCodOrgao(myAuto.getCodOrgao());
								myHis.setCodStatus(myAuto.getCodStatus());
								myHis.setDatStatus(myAuto.getDatStatus());
								myHis.setCodOrigemEvento("100");
								myHis.setCodEvento(HistoricoBean.EVENTO_REMOVER_RELATOR);

								dao.atualizarAutoProcessos(myAuto, myReq, myHis, ata);
							} else {

							}
						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public boolean verificaExisteAtaAberta(String nameSelect) {
		boolean vlRetorno = false;
		try {
			Dao dao = Dao.getInstance();
			vlRetorno = dao.verificaExisteAtaAberta(nameSelect);
		} catch (Exception e) {
		}
		return vlRetorno;
	}

	public boolean validaQtdeProcesso(String nameSelect) {
		boolean vlRetorno = false;
		try {
			Dao dao = Dao.getInstance();
			vlRetorno = dao.validaQtdeProcesso(nameSelect);
		} catch (Exception e) {
		}
		return vlRetorno;
	}

	public String getValParam(String codOrgao, String CONSTANTE) {
		String result = null;
		try {
			Dao dao = Dao.getInstance();
			result = dao.getValParam(codOrgao, CONSTANTE);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return result;
	}

	public Ata populaAta(int codAta) {
		Ata ata = new Ata();
		try {
			Dao dao = Dao.getInstance();
			ata = dao.populaAtaDao(codAta);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return ata;
	}

	public int getValParamProcess(String codOrgao, String CONSTANTE, String tpAta) {
		int result = 0;
		try {
			Dao dao = Dao.getInstance();
			result = dao.getValParamQtdeProcess(codOrgao, CONSTANTE, tpAta);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return result;
	}

	public ArrayList<HashMap<String, String>> getProcessoSemAtribuicaoMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator, String codOrgao) {

		try {
			Dao dao = Dao.getInstance();
			arListSemAtribuicao = dao.getProcessoSemAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator,
					nomeRelator, codOrgao);
		} catch (Exception e) {
		}
		return arListSemAtribuicao;
	}

	public ArrayList<HashMap<String, String>> getProcessoSemAtribuicaoMapList() {

		return arListSemAtribuicao;
	}

	public ArrayList<HashMap<String, String>> getProcessoInfResultMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator) {

		try {
			Dao dao = Dao.getInstance();
			arListBuscaInfResult = dao.getProcessoInfResultMap(tipoRequerimento, codAta, descAta, codRelator,
					nomeRelator);
		} catch (Exception e) {
		}
		return arListBuscaInfResult;
	}

	public ArrayList<HashMap<String, String>> getProcessoComAtribuicaoMapList() {

		return arListComAtribuicao;
	}

	public void gerenciaOperacoes(String semOrComAtribuicao, int indice) {
		ArrayList<Integer> listIndiceSemAtribuicao = new ArrayList<Integer>();
		ArrayList<Integer> listIndiceComAtribuicao = new ArrayList<Integer>();
		try {
			if (semOrComAtribuicao.equals("S")) {
				listIndiceSemAtribuicao.add(indice);
			}
			if (semOrComAtribuicao.equals("C")) {
				listIndiceComAtribuicao.add(indice);
			}

		} catch (Exception e) {
			e.getMessage().toString();
		}
	}

	public void infResult(String result, String codRequerimento, String nomeUsuario) {
		if (arListBuscaInfResult.size() > 0) {
			try {
				String codTipoSolic;

				for (HashMap<String, String> item : arListBuscaInfResult) {
					if (item.get("cod_requerimento").equals(codRequerimento)) {
						codTipoSolic = item.get("COD_TIPO_SOLIC");

						REC.DaoBroker dao = DaoBrokerFactory.getInstance();

						try {
							// TABLE TSMI_AUTO_INFRACAO
							// TABLE TSMI_REQUERIMENTO
							// TABLE TSMI_HISTORICO_AUTO

							REC.RequerimentoBean myReq = RequerimentoBean.consultarRequerimentoPor(codRequerimento);
							AutoInfracaoBean myAuto = AutoInfracaoBean
									.consultarAutoPor(Integer.parseInt(myReq.getCodAutoInfracao()));
							Ata ata = Ata.consultarAtaPor(myReq.getCodAta());
							HistoricoBean myHis = new HistoricoBean();

							if (ata.getTpAta().equals("Def. Previa")) {
								codTipoSolic = "DP";
								myAuto.setCodStatus(AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA);
								myHis.setCodStatus(AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA);
								myReq.setCodTipoSolic(codTipoSolic);
								myReq.setCodStatusRequerimento("3");
								myHis.setCodEvento(HistoricoBean.RESULTADO_DP);

								if (result.equals("SEM RESULTADO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_DP);
								else if (result.equals("DEFERIDO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_DEFERIMENTO_DP);
							} else if (ata.getTpAta().equals("1� INST�NCIA")) {
								codTipoSolic = "1P";
								myAuto.setCodStatus(AutoInfracaoBean.STATUS_AGUARDANDO_2P);
								myHis.setCodStatus(AutoInfracaoBean.STATUS_AGUARDANDO_2P);
								myReq.setCodTipoSolic(codTipoSolic);
								myReq.setCodStatusRequerimento("3");
								myHis.setCodEvento(HistoricoBean.RESULTADO_1P);

								if (result.equals("SEM RESULTADO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_RECURSO_1P);
								else if (result.equals("DEFERIDO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_DEFERIMENTO_1P);
							} else if (ata.getTpAta().equals("2� INST�NCIA")) {
								codTipoSolic = "2P";
								myAuto.setCodStatus(AutoInfracaoBean.STATUS_TRANSITADO_JULGADO);
								myHis.setCodStatus(AutoInfracaoBean.STATUS_TRANSITADO_JULGADO);
								myHis.setCodEvento(HistoricoBean.RESULTADO_2P);

								if (result.equals("SEM RESULTADO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_RECURSO_2P);
								else if (result.equals("DEFERIDO"))
									myAuto.setCodStatus(AutoInfracaoBean.STATUS_DEFERIMENTO_2P);
							} else if (ata.getTpAta().equals("PAE")) {
								codTipoSolic = "PAE";
								
								myHis.setCodStatus(myAuto.getCodStatus());
								myHis.setCodEvento(HistoricoBean.EVENTO_RESULTADO_DO_RECURSO_PAE_ALTERADO);
							}

							myAuto.setDatInfracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInfracao()));
							myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(myAuto.getDatProcesso()));
							myAuto.setDatVencimento(ControladorRecGeneric.dateToDateBr(myAuto.getDatVencimento()));
							myAuto.setDatStatus(ControladorRecGeneric.dateToDateBr(DataUtils.getDateBr()));
							myAuto.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatPublPDO()));
							myAuto.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatEnvioDO()));
							myAuto.setDatNotifAut(ControladorRecGeneric.dateToDateBr(myAuto.getDatNotifAut()));
							myAuto.setDatNotifPen(ControladorRecGeneric.dateToDateBr(myAuto.getDatNotifPen()));
							myAuto.setDatPag(ControladorRecGeneric.dateToDateBr(myAuto.getDatPag()));
							myAuto.setDatInclusao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInclusao()));
							myAuto.setDatAlteracao(ControladorRecGeneric.dateToDateBr(DataUtils.getDateBr()));
							myAuto.setDatRegistro(ControladorRecGeneric.dateToDateBr(myAuto.getDatRegistro()));

							myReq.setCodStatusRequerimento("3");
							
							if (result.equals("INDEFERIDO")) {
								myHis.setTxtComplemento03("I");
								myReq.setCodResultRS("I");
								myAuto.setIndSituacao("I");
							} else if (result.equals("DEFERIDO")) {
								myHis.setTxtComplemento03("D");
								myReq.setCodResultRS("D");
								myAuto.setIndSituacao("D");
							} else if (result.equals("SUSPENSO")) {
								myHis.setTxtComplemento03("S");
								myReq.setCodResultRS("S");
								myAuto.setIndSituacao("S");
							} else if (result.equals("ARQUIVADO")) {
								myHis.setTxtComplemento03("A");
								myReq.setCodResultRS("A");
								myAuto.setIndSituacao("A");
							} else if (result.equals("SEM RESULTADO")) {
								myReq.setCodStatusRequerimento("0");
								myHis.setTxtComplemento03("");
								myReq.setCodResultRS("");
								myAuto.setIndSituacao("");
							}

							myReq.setDatAtuTR(ControladorRecGeneric.dateToDateBr(myReq.getDatAtuTR()));
							myReq.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioDO()));
							myReq.setDatEnvioGuia(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioGuia()));
							myReq.setDatEST(ControladorRecGeneric.dateToDateBr(myReq.getDatEST()));
							myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
							myReq.setDatJU(ControladorRecGeneric.dateToDateBr(myReq.getDatJU()));
							myReq.setDatPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatPJ()));
							myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
							myReq.setDatProcEST(ControladorRecGeneric.dateToDateBr(myReq.getDatProcEST()));
							myReq.setDatProcJU(ControladorRecGeneric.dateToDateBr(myReq.getDatProcJU()));
							myReq.setDatProcPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatProcPJ()));
							myReq.setDatProcRS(ControladorRecGeneric.dateToDateBr(myReq.getDatProcRS()));
							myReq.setDatProcTR(ControladorRecGeneric.dateToDateBr(myReq.getDatProcTR()));
							myReq.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myReq.getDatPublPDO()));
							myReq.setDatRecebimentoGuia(
									ControladorRecGeneric.dateToDateBr(myReq.getDatRecebimentoGuia()));
							myReq.setDatRequerimento(ControladorRecGeneric.dateToDateBr(myReq.getDatRequerimento()));
							myReq.setDatRS(ControladorRecGeneric.dateToDateBr(myReq.getDatRS()));
							myReq.setDatValidTR(ControladorRecGeneric.dateToDateBr(myReq.getDatValidTR()));

							ata.setDataAviso(ControladorRecGeneric.dateToDateBr(ata.getDataAviso()));
							ata.setDataDistribuicao(ControladorRecGeneric.dateToDateBr(ata.getDataDistribuicao()));
							ata.setDataInclusao(ControladorRecGeneric.dateToDateBr(ata.getDataInclusao()));
							ata.setDataSessao(ControladorRecGeneric.dateToDateBr(ata.getDataSessao()));
							ata.setDataPublicacao(ControladorRecGeneric.dateToDateBr(ata.getDataPublicacao()));

							myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());
							myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao());
							myHis.setNumProcesso(myAuto.getNumProcesso());
							myHis.setCodOrgao(myAuto.getCodOrgao());
							myHis.setCodStatus(myAuto.getCodStatus());
							myHis.setDatStatus(myAuto.getDatStatus());

							myHis.setTxtComplemento01(myReq.getNumRequerimento());
							myHis.setTxtComplemento02(myReq.getCodTipoSolic());
							myHis.setNomUserName(nomeUsuario);
							myHis.setCodOrgaoLotacao(myAuto.getCodOrgao());
							myHis.setTxtComplemento12(myReq.getCodRelatorJU());

							dao.atualizarAutoProcessos(myAuto, myReq, myHis, ata);
						} catch (Exception e) {

						}
					}
				}
			} catch (Exception e) {
			}
		}
	}

	public String getNumProcAuto(String codOrgao, String CONSTANTE) { // automatico
																		// 99999/AAAA,
																		// onde
																		// 99999
																		// ,
																		// sequencia
																		// num�rica
																		// e
																		// AAAA,
																		// ano
																		// corrente
		String result = null;
		try {
			Dao dao = Dao.getInstance();
			result = dao.getNumProcAuto(codOrgao, CONSTANTE);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return result;
	}

	public boolean validaStatusRequerimentoAuto(String tpRequerimento, String numAuto) {
		boolean result = false;
		try {
			Dao dao = Dao.getInstance();
			result = dao.validaStatusRequerimentoAuto(tpRequerimento, numAuto);
			return result;
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return result;
	}

	public Ata getNextNrAta(Ata ataNext, String cdNatureza) {
		try {
			Dao dao = Dao.getInstance();
			ataNext.setNrAta(dao.getNextNrAta(ataNext, cdNatureza));
		} catch (Exception e) {
		}
		return ataNext;
	}

	public int getCodAta(String dsAta) {
		int codAta = 0;
		try {
			Dao dao = Dao.getInstance();
			codAta = dao.getCodAta(dsAta);
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return codAta;
	}

	public ArrayList<HashMap<String, String>> consultarAtas(String tipoAta, String codOrgao, String cdNatureza) {
		ArrayList<HashMap<String, String>> atas = new ArrayList<HashMap<String, String>>();

		try {
			atas = Dao.getInstance().consultarAtas(tipoAta, codOrgao, cdNatureza);
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS ATA - M�TODO: consultarAtas(): " + ex.getMessage());
		}

		return atas;
	}

	public static Ata consultarAtaPor(String codAta) {
		Ata ata = null;

		try {
			HashMap<String, Object> hashedAta = Dao.getInstance().consultarAtaPor(codAta);

			if (hashedAta != null) {
				ata = new Ata();
				ata.setCodAta(hashedAta.get("codAta").toString());
				ata.setTpAta(hashedAta.get("tipoAta").toString());
				ata.setDescAta(hashedAta.get("desAta").toString());
				ata.setDataSessao(hashedAta.get("datSessao").toString());
				ata.setDataDistribuicao(hashedAta.get("datDistribuicao").toString());
				ata.setJuntaAta(hashedAta.get("juntaAta").toString());
				ata.setDataAviso(hashedAta.get("datAviso").toString());
				ata.setStatusAta(hashedAta.get("status").toString());
				ata.setQtdeAtaProcesso(Integer.parseInt(hashedAta.get("qntProcesso").toString()));
				ata.setNrAta(Integer.parseInt(hashedAta.get("numAta").toString()));
				ata.setAnoAta(Integer.parseInt(hashedAta.get("anoAta").toString()));
				ata.setDataInclusao(hashedAta.get("datInclusao").toString());
				ata.setCodOrgao(hashedAta.get("codOrgao").toString());
				ata.setDataPublicacao(
						hashedAta.get("datPublicacao") != null ? hashedAta.get("datPublicacao").toString() : "");
				ata.setCdNatureza(Integer.parseInt(hashedAta.get("codNatureza").toString()));
			}
		} catch (Exception ex) {
		}

		return ata;
	}

	public static Ata mapFrom(ResultSet resultSet) throws Exception {
		Ata ata = new Ata();

		ata.setCodAta(resultSet.getString("COD_ATA"));
		ata.setTpAta(resultSet.getString("TIPO_ATA"));
		ata.setDescAta(resultSet.getString("DES_ATA"));
		ata.setDataSessao(DataUtils.dateToDateBr(resultSet.getString("DT_SESSAO")));
		ata.setDataDistribuicao(DataUtils.dateToDateBr(resultSet.getString("DT_DISTRIBUICAO")));
		ata.setJuntaAta(resultSet.getString("JUNTA_ATA"));
		ata.setDataAviso(DataUtils.dateToDateBr(resultSet.getString("DT_AVISO")));
		ata.setStatusAta(resultSet.getString("STATUS"));
		ata.setQtdeAtaProcesso(resultSet.getInt("QTDE_PROCESSO"));
		ata.setNrAta(resultSet.getInt("NR_ATA"));
		ata.setAnoAta(resultSet.getInt("ANO_ATA"));
		ata.setDataInclusao(DataUtils.dateToDateBr(resultSet.getString("DAT_INCLUSAO")));
		ata.setCodOrgao(resultSet.getString("COD_ORGAO"));
		ata.setDataPublicacao(DataUtils.dateToDateBr(resultSet.getString("DATA_PUBLICACAO")));
		ata.setCdNatureza(resultSet.getInt("CD_NATUREZA"));

		return ata;
	}
}
