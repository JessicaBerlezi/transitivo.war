package ACSS;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios/Orgao Atuacao<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Perfil do Usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author dgeraldes
* @version 1.0
* @Updates
*/

public class ConsultaUsuarioLogadoCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ConsultaUsuarioLogado.jsp";
	private String next;

	public ConsultaUsuarioLogadoCmd() {
		next = jspPadrao;
	}

	public ConsultaUsuarioLogadoCmd(String next) {
		this.next = next;
	}
	

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaUsrLogado(req,nextRetorno);
			
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaUsuarioLogadoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public int BuscaArray(String[] array, String busca)
	{
		for(int i=0; i<array.length; i++)
		{
			if (busca.equals(array[i]) == true)
				return 1;
		}
		return 0;
	}
	
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{
			String nextRetorno = jspPadrao;
			try {
				nextRetorno = consultaUsrLogado(req,nextRetorno);
			}
			catch (Exception ue) {
				throw new sys.CommandException("ConsultaUsuarioLogadoCmd: "+ ue.getMessage());
			}
			return nextRetorno;
  }
  
  public String consultaUsrLogado(HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
	
			// cria os Beans de Usuario e Perfil, se n�o existir
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
	
			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  	
				
			ACSS.ConsultaUsuarioLogadoBean ConsultaUsuarioLogadoBeanId =(ACSS.ConsultaUsuarioLogadoBean) req.getAttribute("ConsultaUsuarioLogadoBeanId");
			if (ConsultaUsuarioLogadoBeanId == null)ConsultaUsuarioLogadoBeanId = new ACSS.ConsultaUsuarioLogadoBean();
	
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
				
			String num_sessao = req.getParameter("num_sessao");
			if (num_sessao == null) num_sessao = "";
				
			String nom_UserName = req.getParameter("nom_UserName");
			if (nom_UserName == null) nom_UserName = "";
				
			String orgaoatua = req.getParameter("orgaoatua");
			if (orgaoatua == null) orgaoatua = "";
				
			String sistema = req.getParameter("sistema");
			if (sistema == null) sistema = "";
				
			String dtentrada = req.getParameter("dtentrada");
			if (dtentrada == null) dtentrada = "";
				
			String ip = req.getParameter("ip");
			if (ip == null) ip = "";
				
			String datBloqueio = req.getParameter("DatBloqueio");
			if (datBloqueio == null) datBloqueio = "";
				
			String num_minutos = req.getParameter("NumMinutos");
			if (num_minutos == null) num_minutos = "";
				
			String ordClass = req.getParameter("ordClass");
			if (ordClass == null) ordClass = "descendente";
				
			String ordem = req.getParameter("ordem");
				
			ConsultaUsuarioLogadoBeanId.setdatBloqueio(datBloqueio);
			ConsultaUsuarioLogadoBeanId.setNumMinutos(num_minutos);
			ConsultaUsuarioLogadoBeanId.setOrdClass(ordClass);
			ConsultaUsuarioLogadoBeanId.setOrdem(ordem);
			ConsultaUsuarioLogadoBeanId.setNumSessao(num_sessao);
			ConsultaUsuarioLogadoBeanId.setNomUserName(nom_UserName);
			ConsultaUsuarioLogadoBeanId.setSigOrgaoAtuacao(orgaoatua);
			ConsultaUsuarioLogadoBeanId.setSistema(sistema);
			ConsultaUsuarioLogadoBeanId.setDataSessao(dtentrada);
			ConsultaUsuarioLogadoBeanId.setIp(ip);
				
			String codSistema = req.getParameter("codSistema"); 
			if(codSistema == null) codSistema = "0";  
				
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";
	
			//SistBeanId.setCodSistema(codSistema);
			ConsultaUsuarioLogadoBeanId.setCodSistema(codSistema);
			ConsultaUsuarioLogadoBeanId.setCodOrgaoAtuacao(codOrgao);
				
			if (datBloqueio.equals("")){		
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				String dataHoje = df.format(new Date());
				datBloqueio = dataHoje; 
					
				ConsultaUsuarioLogadoBeanId.setdatBloqueio(datBloqueio);
			}	
				
			if (num_minutos.equals("")){			 
				num_minutos = "10";
				ConsultaUsuarioLogadoBeanId.setNumMinutos(num_minutos);
			}	
	
			if (acao.equals("ok")) {
				OrgaoBeanId.setAtualizarDependente("S");
				OrgaoBeanId.Le_Orgao(codOrgao,0);
				SistBeanId.Le_Sistema(codSistema,0);
					
				//L� os dados de Usuarios Logados
				ConsultaUsuarioLogadoBeanId.Le_DadosUsuarioLogado();
				ConsultaUsuarioLogadoBeanId.Classifica(ordem);
			}
				
			if (acao.equals("retornarLista")) {
				OrgaoBeanId.setAtualizarDependente("S");
				OrgaoBeanId.Le_Orgao(codOrgao,0);
				SistBeanId.Le_Sistema(codSistema,0);
					
				//L� os dados de Usuarios Logados
				ConsultaUsuarioLogadoBeanId.Le_DadosUsuarioLogado();
				ConsultaUsuarioLogadoBeanId.Classifica(ordem);
			}
				
			if (acao.equals("Classifica")) {
					SistBeanId.Le_Sistema(codSistema,0) ;
					OrgaoBeanId.Le_Orgao(codOrgao,0);
					ConsultaUsuarioLogadoBeanId.Le_DadosUsuarioLogado();
					ConsultaUsuarioLogadoBeanId.Classifica(ordem);
					OrgaoBeanId.setAtualizarDependente("S");
			}
				
			if (acao.equals("confirmaBloqueio")) {
				String[] array_sujo = req.getParameterValues("bloq");
				String[] array_limpo = new String[array_sujo.length];
				String[] array_sujo2 = req.getParameterValues("codUsuario");
				String[] array_limpo2 = new String[array_sujo2.length];
				String[] array_CodOrgaoLot_sujo = req.getParameterValues("codOrgaoLotacao");
				String[] array_CodOrgaoLot_limpo = new String[array_CodOrgaoLot_sujo.length];
				int cont = 0;
				int i;
				for(i=0; i<array_sujo.length; i++)
				{
					if(BuscaArray(array_limpo, array_sujo[i]) == 0)
					{
						array_limpo[cont] = array_sujo[i];
						cont++;
					}
				}
				cont = 0;
				for(i=0; i<array_CodOrgaoLot_sujo.length; i++)
				{
					if(BuscaArray(array_CodOrgaoLot_limpo, array_CodOrgaoLot_sujo[i]) == 0)
					{
						array_CodOrgaoLot_limpo[cont] = array_CodOrgaoLot_sujo[i];
						cont++;
					}
				}
				ConsultaUsuarioLogadoBeanId.Le_CodUsuario(array_limpo2, array_limpo, array_CodOrgaoLot_limpo);
					
				for(i=0; i<array_limpo.length; i++)
				{
					ConsultaUsuarioLogadoBean dados = new ConsultaUsuarioLogadoBean();
					if(array_limpo[i] != null)
					{
						dados.setNomUserName(array_limpo[i]);
						dados.setCodUsuario(array_limpo2[i]);
						ConsultaUsuarioLogadoBeanId.getDados().add(dados);
					}
				}
				OrgaoBeanId.setAtualizarDependente("S");
				nextRetorno="/ACSS/ConfirmaBloqueio.jsp";
			}
				
			if (acao.equals("LogAcesso")) {
				ConsultaUsuarioLogadoBeanId.Le_LogAcesso();
					
				OrgaoBeanId.setAtualizarDependente("S");
				nextRetorno="/ACSS/UsuarioLogAcesso.jsp";
			}
				
			if (acao.equals("BloqUsuario")) {
				String[] usuarios_bloq = req.getParameterValues("codUsuario");
					
				OrgaoBeanId.setAtualizarDependente("S");
					
				//Insere os dados de Usuarios bloqueados
				ConsultaUsuarioLogadoBeanId.BloqueiaUsuario(UsrLogado, usuarios_bloq );
					
				//L� os dados de Usuarios Logados
				ConsultaUsuarioLogadoBeanId.Le_DadosUsuarioLogado();
					
				nextRetorno="/ACSS/ConsultaUsuarioLogado.jsp";
			}
				
			if (acao.equals("ImprimeResultConsulta")) {
				//Titulo do Relatorio de impressao
				String tituloConsulta = "CONSULTA USU�RIOS LOGADOS";
				req.setAttribute("tituloConsulta", tituloConsulta);
					
				//L� os dados de Usuarios Logados
				ConsultaUsuarioLogadoBeanId.Le_DadosUsuarioLogado();
					
				if(ConsultaUsuarioLogadoBeanId.getDados().size() == 0)
					req.setAttribute("msg", "N�o existem informa��es");
				else	
				{
					if(ConsultaUsuarioLogadoBeanId.getOrdClass().equals("ascendente"))
						ConsultaUsuarioLogadoBeanId.setOrdClass("descendente");
					else
						ConsultaUsuarioLogadoBeanId.setOrdClass("ascendente");
							
					ConsultaUsuarioLogadoBeanId.Classifica(ordem);
					ConsultaUsuarioLogadoBeanId.setDados(ConsultaUsuarioLogadoBeanId.getDados());
				}
							
				nextRetorno="/ACSS/ConsultaUsuarioLogadoImp.jsp";
			}
	
			if (acao.equals("Volta")) {
				OrgaoBeanId.setAtualizarDependente("N");
			}
				
			req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			req.setAttribute("SistBeanId", SistBeanId);
	
			req.setAttribute("ConsultaUsuarioLogadoBeanId",ConsultaUsuarioLogadoBeanId);
				
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaUsuarioLogadoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	  }

}