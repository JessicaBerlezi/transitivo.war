package ACSS;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import TAB.MunicipioBean;

/**
* <b>Title:</b>        Consulta - Consultas Orgao<br>
* <b>Description:</b>  Comando para Consultar Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br> 
* @author Wellem 
* @version 1.0
* @Updates
*/

public class ProtocoloUpoCmd extends sys.Command 
{

  private static final String jspPadrao="/ACSS/ProtocoloUpo.jsp";   
      
  
  private String next;
  
  public ProtocoloUpoCmd() {
    next             = jspPadrao ;
  }
  
  public ProtocoloUpoCmd(String next)  {
  	this.next = next;
  }
 
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = next ;
    try {      	
	    HttpSession session   = req.getSession() ;								
    	ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  				
	    // cria os Beans do Orgao, se n�o existir
	    ACSS.ProtocoloUpoBean ProtocoloUpoBeanId = (ACSS.ProtocoloUpoBean)req.getAttribute("ProtocoloUpoBeanId") ;
	    if (ProtocoloUpoBeanId==null)        ProtocoloUpoBeanId = new ACSS.ProtocoloUpoBean() ;	 
	
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)          acao =" ";  

		String pkid           = req.getParameter("pkid");   
		if(pkid==null)          pkid  ="";  		  	  	    	  
		
		String codProtUpo       = req.getParameter("codProtUpo");	     
		if(codProtUpo==null)      codProtUpo="";
		if(!acao.equals("Z"))
			pkid = codProtUpo; 
		
		String nomProtUpo       = req.getParameter("nomProtUpo");	     
		if(nomProtUpo==null)      nomProtUpo=""; 
		
		// Preencher o Bean
		ProtocoloUpoBeanId.setPkid(pkid) ;
		ProtocoloUpoBeanId.setCodProtocoloUpo(codProtUpo);							
		ProtocoloUpoBeanId.setNomProtocoloUpo(nomProtUpo);
		
		if ("ProtocoloUpoConsultaCmd".indexOf(acao) >= 0) {
			return protocoloUpoConsulta(req,ProtocoloUpoBeanId,UsrLogado); 	
		}
		if ("Top,Proximo,Anterior,Fim,Pkid".indexOf(acao) >= 0)  {
		   if ( "Pkid".indexOf(acao) >= 0 ) ProtocoloUpoBeanId.buscaCodProtUpo( pkid, acao,UsrLogado ) ;
		   else		                        ProtocoloUpoBeanId.buscaCodProtUpo( nomProtUpo, acao,UsrLogado );
		}				
	   	if ("123".indexOf(acao)>=0) {
		 	ProtocoloUpoBeanId.setMsgErro("") ;  	      
			if(acao.compareTo("1")==0){
				if( !ProtocoloUpoBeanId.isInsert() ) 
					ProtocoloUpoBeanId.setPkid("0");
			}
			if(acao.compareTo("2")==0){
				ProtocoloUpoBeanId.isAltera();	
			}
			if(acao.compareTo("3")==0){
				if( ProtocoloUpoBeanId.isDelete() ) 
					acao = "Novo";
			}
		}
		if ("Novo".indexOf(acao)>=0) {	  
			  ProtocoloUpoBeanId.setPkid("");
			  ProtocoloUpoBeanId.setCodProtocoloUpo("") ;
			  ProtocoloUpoBeanId.setNomProtocoloUpo("");
			  ProtocoloUpoBeanId.setNomAbrevProtocoloUpo("");
			}	  

		req.setAttribute("ProtocoloUpoBeanId",ProtocoloUpoBeanId) ;
		
    }
    catch (Exception ue) {
      throw new sys.CommandException("ProtocoloUpoCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
	private String protocoloUpoConsulta(HttpServletRequest req,ACSS.ProtocoloUpoBean ProtocoloUpoBeanId,ACSS.UsuarioBean UsrLogado) throws sys.CommandException	{
		HttpSession session   = req.getSession() ;
		String acaoConsulta = req.getParameter("acaoConsulta"); 
		if (acaoConsulta==null) acaoConsulta="" ;
		// inicio da consulta
        if (acaoConsulta.equals("")) {	
			// colocar os Beans a serem preservados na sessao
			session.setAttribute("ProtocoloUpoBeanId",ProtocoloUpoBeanId) ;	
			// Nome para guardar o Bean de Consultana sessao
			session.setAttribute("nomeBeanConsulta","ConsultaOrgaoBeanId") ;						
        }
        // fim da consuta - fica neste comando
        if ((acaoConsulta.equals("pkid")) || (acaoConsulta.equals("Retornar"))) {									        
        	//restaurar da  sessao os Bean preservados
        	String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");			
        	session.removeAttribute(nomeBeanConsulta); 						
        	session.removeAttribute("nomeBeanConsulta"); 
        	try 	{
	        	// recupera o(s) Bean(s) na sessao 
    	    	ProtocoloUpoBeanId = (ACSS.ProtocoloUpoBean)session.getAttribute("ProtocoloUpoBeanId") ;		
        		if (ProtocoloUpoBeanId==null) ProtocoloUpoBeanId = new ACSS.ProtocoloUpoBean() ;
        	}
        	catch (Exception e){
        	  throw new sys.CommandException("ProtocoloUpoCommand 003: " + e.getMessage());
        	}			
						
        	// remove o Bean da sessao
        	session.removeAttribute("ProtocoloUpoBeanId"); 				
        	//setar os Beans desejados				
        	if (acaoConsulta.equals("pkid")==true) 	{
        		String pkidConsulta = req.getParameter("pkidConsulta"); 		
        		if (pkidConsulta==null) pkidConsulta="" ;
        		ProtocoloUpoBeanId.buscaCodProtUpo(pkidConsulta,"Pkid",UsrLogado) ;
        		try{
        	    	req.setAttribute("ProtocoloUpoBeanId",ProtocoloUpoBeanId); 	

        		}
        		catch(Exception e){
        			throw new sys.CommandException("ProtocoloUpoCommand 002: " + e.getMessage());
        		}
        	}
        }
		else 		{
			try 	{
				// executar o comando de Consulta
				sys.Command cmd = (sys.Command)Class.forName("ACSS.ProtocoloUpoConsultaCmd").newInstance() ;							
				return cmd.execute(req);		
			}
			catch (Exception e){
		      throw new sys.CommandException("ProtocoloUpoCommand 002: " + e.getMessage());
			}			
		}
        return jspPadrao;
	}
} 
 
