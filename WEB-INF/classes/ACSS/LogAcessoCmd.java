package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Consulta Log de Acesso<br>
* <b>Description:</b>  Comando consultar e listar os acesso ao sistema de cada usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Marcelo de Souza Domingues
* @version 1.0
* @Updates
*/

public class LogAcessoCmd extends sys.Command {

	private static 	final String jspPadrao = "/ACSS/LogAcesso.jsp";
	private String 	next;
	
	public LogAcessoCmd() {
		next = jspPadrao;
	}

	public LogAcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = consultaSistema(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("LogAcessoCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}


	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	 {
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaSistema(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("LogAcessoCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String consultaSistema(HttpServletRequest req, String nextRetorno) throws CommandException{
		try {
					// cria os Beans, se n�o existir
					HttpSession session = req.getSession();
					ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
					if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

					// cria os Beans de Usuario e Perfil, se n�o existir
					ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
					if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

					ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
					if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

					ACSS.LogAcessoBean logAcessoBeanId =(ACSS.LogAcessoBean) session.getAttribute("LogAcessoBeanId");
					if (logAcessoBeanId == null)logAcessoBeanId = new ACSS.LogAcessoBean();
			
					SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
					if (SistBeanId==null) SistBeanId = new SistemaBean();			

					// obtem e valida os parametros recebidos					
					String acao = req.getParameter("acao");
					if (acao == null) acao = "";
			
					String codOrgao = req.getParameter("codOrgao");
					if ((codOrgao == null) || (codOrgao.equals(""))) codOrgao = "0";
			
					String codUsuario = req.getParameter("codUsuario");
					if ((codUsuario == null) || (codUsuario.equals(""))) codUsuario = "0";
			
					String codSistema = req.getParameter("codSistema"); 
					if ((codSistema == null) || (codSistema.equals(""))) codSistema = "0";

					String dataIni	= req.getParameter("edtDe");
					if ((dataIni == null) || (dataIni.equals("")))	dataIni = "0";
			
			
					String dataFim	= req.getParameter("edtAte");
					if ((dataFim == null) || (dataFim.equals(""))) dataFim = "0";			
			
					// Prepara beans
					logAcessoBeanId.setDatIni(dataIni);
					logAcessoBeanId.setDatFim(dataFim);
					logAcessoBeanId.setAcao(acao);						
					logAcessoBeanId.setAtualizarTela("N");
					SistBeanId.setCodSistema(codSistema);
					UsrBeanId.getOrgao().setCodOrgao(codOrgao);
					UsrBeanId.setCodUsuario(codUsuario);			
					OrgaoBeanId.setCodOrgao(codOrgao);		 			

					if (acao.equals("")){
						logAcessoBeanId = new ACSS.LogAcessoBean();				
						String dataHoje = sys.Util.getDataHoje();
						logAcessoBeanId.setAtualizarTela("N");
						String mes = dataHoje.substring(3,5);
						String ano = dataHoje.substring(6,10);
						dataIni	= "01" + "/" + mes + "/" + ano;
						dataHoje = dataHoje.replaceAll("-","/");  
						logAcessoBeanId.setDatIni(dataIni);
						logAcessoBeanId.setDatFim(dataHoje);				
					}		
       
					if (acao.equals("consultaRegistros")){	
						/* Verifica se a data de Inicio � inferior a data m�nima              *
						 * encontrada na tabel TCAU_LOG_ACESSO. Esse procedimento �           *
						 * necess�rio para saber se deve ser feita consulta na tabela atual e                                               */
						
						logAcessoBeanId.setIndLogBKP(logAcessoBeanId.verificaDataInicial(logAcessoBeanId));
    
						if (logAcessoBeanId.contaRegistros(SistBeanId,UsrBeanId,OrgaoBeanId,logAcessoBeanId))
						{
							logAcessoBeanId.setAtualizarTela("S");
						}
						else logAcessoBeanId.setAtualizarTela("confirma");
					}


					if (acao.equals("mostraRespConsulta")){			
						logAcessoBeanId.setAtualizarTela("S");
						logAcessoBeanId.Le_LogAcesso(SistBeanId, UsrBeanId, OrgaoBeanId);
						logAcessoBeanId.setSigOrgao(OrgaoBeanId.getSigOrgao());
						logAcessoBeanId.setNomAbrev(SistBeanId.getNomAbrev());
						logAcessoBeanId.setNomUserName(UsrBeanId.getNomUserName());
						logAcessoBeanId.setNomSistema(SistBeanId.getNomSistema());
					}

				  if (acao.equals("V")) {		
						logAcessoBeanId.setAcao(acao);		
						OrgaoBeanId.setCodOrgao(codOrgao);						
						SistBeanId.setCodSistema(codSistema);
						SistBeanId.Le_Sistema(codSistema,0);								
						UsrBeanId.getOrgao().setCodOrgao(codOrgao);
						UsrBeanId.setCodUsuario(codUsuario);
						UsrBeanId.Le_Usuario(Integer.parseInt(codUsuario));
						logAcessoBeanId.setIniCont(0);
						logAcessoBeanId.setFimCont(100);	
					  logAcessoBeanId = new ACSS.LogAcessoBean();				
						String dataHoje = sys.Util.getDataHoje();
						logAcessoBeanId.setAtualizarTela("N");
						String mes = dataHoje.substring(3,5);
						String ano = dataHoje.substring(6,10);
						dataIni	= "01" + "/" + mes + "/" + ano;
						dataHoje = dataHoje.replaceAll("-","/");  
						logAcessoBeanId.setDatIni(dataIni);
						logAcessoBeanId.setDatFim(dataHoje);
					} 	
	
					if (acao.equals("Classifica")){		
						logAcessoBeanId.Classifica(req.getParameter("ordem"));
						logAcessoBeanId.setAtualizarTela("S");				
						OrgaoBeanId.setSigOrgao(logAcessoBeanId.getSigOrgao());
						SistBeanId.setNomAbrev(logAcessoBeanId.getNomAbrev());
						SistBeanId.setNomSistema(logAcessoBeanId.getNomSistema());
						UsrBeanId.setNomUserName(logAcessoBeanId.getNomUserName());				
					}
						
					if  (acao.equals("ImprimeResultConsulta")){
				 //Titulo do Relatorio de impressao
						 String tituloConsulta = "CONSULTA LOG DE ACESSO";
						 req.setAttribute("tituloConsulta", tituloConsulta);
				 
						if ((logAcessoBeanId.getSigOrgao() == null) || 
							(logAcessoBeanId.getSigOrgao().equals(""))) 
							OrgaoBeanId.setSigOrgao("TODOS");
						else
							OrgaoBeanId.setSigOrgao(logAcessoBeanId.getSigOrgao());
					
						if ((logAcessoBeanId.getNomAbrev() == null) ||
							(logAcessoBeanId.getNomAbrev().equals("")))
							SistBeanId.setNomAbrev("TODOS");
						else				
							SistBeanId.setNomAbrev(logAcessoBeanId.getNomAbrev());
			
						if ((logAcessoBeanId.getNomUserName() == null) ||
							(logAcessoBeanId.getNomUserName().equals("")))						
							UsrBeanId.setNomUserName("TODOS");
						else								
							UsrBeanId.setNomUserName(logAcessoBeanId.getNomUserName());
			
						if ((logAcessoBeanId.getNomSistema() == null) ||
							(logAcessoBeanId.getNomSistema().equals("")))
							SistBeanId.setNomSistema("TODOS");
						else								
							SistBeanId.setNomSistema(logAcessoBeanId.getNomSistema());
					
						if (logAcessoBeanId.getListaTotal().size()==0) req.setAttribute("semLog","N�O EXISTEM LOGS COM OS DADOS FORNECIDOS");				  	    
						nextRetorno = "/ACSS/consultaLogAcessoImp.jsp" ;
					}	
						
					req.setAttribute("OrgaoBeanId", OrgaoBeanId);
					req.setAttribute("SistBeanId", SistBeanId);
					req.setAttribute("UsrBeanId", UsrBeanId);
					session.setAttribute("LogAcessoBeanId", logAcessoBeanId);		
				}
				catch (Exception ue) {
					throw new sys.CommandException("LogAcessoCmd: "+ ue.getMessage());
				}
				return nextRetorno;		
	}
					
}