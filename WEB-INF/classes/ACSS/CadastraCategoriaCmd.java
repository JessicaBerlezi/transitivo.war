package ACSS;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sys.CommandException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class CadastraCategoriaCmd extends sys.Command {

  private static final String jspPadrao="/ACSS/CadastraCategoria.jsp";    
  private String next;

  public CadastraCategoriaCmd() { next = jspPadrao; }
  public CadastraCategoriaCmd( String next ) { this.next = jspPadrao; }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
	try {      	
		  nextRetorno = cadastraCategoria(req,nextRetorno);
	}
	catch (Exception ue) {
	  throw new sys.CommandException("CadastraCategoriaCmd: " + ue.getMessage());
	}
	return nextRetorno;
  }

 
  public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	  {
	String nextRetorno  = jspPadrao;
    try {      
		   nextRetorno = cadastraCategoria(req,nextRetorno);
    }
    catch (Exception ue) { throw new sys.CommandException("CadastraCategoriaCmd: " + ue.getMessage()); }
	return nextRetorno;
  }
  
  private Vector isParametros(String[] desCat, String[] emailCat,Vector vErro,CadastraCategoriaBean CadCatId) {
		if ( emailCat.length != desCat.length ){	
		     vErro.addElement("Parametros inconsistentes!\n") ;
		     CadCatId.setMsgErro(vErro);
		}
		return vErro ;
  }
  
  public String cadastraCategoria(HttpServletRequest req, String nextRetorno) throws CommandException{
		try {      
			
			CadastraCategoriaBean CadCatId = (CadastraCategoriaBean) req.getAttribute("CadCatId");
			if( CadCatId == null ) CadCatId = new CadastraCategoriaBean();
			
			String acao = req.getParameter("acao");  
			if(acao == null) acao ="";   
		 					 
			if(acao.compareTo("I") == 0){
				String[] codCat   = req.getParameterValues("codCat");
					if( codCat == null)  codCat = new String[0];
				String[] desCat   = req.getParameterValues("desCat");
					if( desCat == null)  desCat = new String[0];
				String[] emailCat = req.getParameterValues("emailCat");
					if( emailCat == null)  emailCat = new String[0];
	     
				Vector vErro = new Vector(); 
				vErro = isParametros(desCat,emailCat,vErro,CadCatId);
	 
	 
				if (vErro.size()==0) {
					Vector CadCatList  	= new Vector(); 
					CadCatList.clear();
					for (int i = 0; i < codCat.length;i++)	{
						  CadastraCategoriaBean myCadCat = new CadastraCategoriaBean();	
						  myCadCat.setCodCategoria( codCat[i] );
						  myCadCat.setDesCat( desCat[i] );	
						  myCadCat.setEmailCat( emailCat[i] );
						  CadCatList.add( myCadCat );
					}
					CadCatId.isInsertCadCat(vErro, CadCatList );
				}
				else CadCatId.setMsgErro( vErro );  	      
			}
			CadCatId.ListaCadCat();
	
			req.setAttribute("CadCatId",CadCatId);
		}
		catch (Exception ue) { throw new sys.CommandException("JuntaCmd 001: " + ue.getMessage()); }
		return nextRetorno; 
  }
}