package ACSS;



/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Operacaos - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class OperacaoBean extends sys.HtmlPopupBean { 

  private String codSistema;
  private String codOperacao;
  private String codOperacaoFk;  
  private String numOrdem;  
  private String sigFuncao; 
  private String sigFuncaoPai; 
  private String nomFuncaoPai;         
  private String nomOperacao; 
  private String nomDescricao;  
  private String nomApresentacao;
  private String nomExecuta;       
  private String tipOperacao;
  private String nivel;
  private String sitBiometria;  
  private String habilita; 
  
        
  public OperacaoBean()  {

    super();
    setTabela("TCAU_OPERACAO");	
    setPopupWidth(35);
    codSistema        = "";	
    codOperacao       = "";
    codOperacaoFk     = "";	
    numOrdem          = "0";
	sigFuncao		  = "";
	sigFuncaoPai	  = "";	
	nomFuncaoPai	  = "";	
    nomOperacao    	  = "";
	nomDescricao	  = "";
    nomApresentacao	  = "";	
    nomExecuta   	  = "";	
    tipOperacao   	  = "";		
 	nivel             = "0" ;
 	sitBiometria      = "I";
 	habilita		  = "S";
  }

  public void setCodSistema(String codSistema)  {
  this.codSistema=codSistema ;
    if (codSistema==null) this.codSistema="" ;
  }  
  public String getCodSistema()  {
  	return this.codSistema;
  }
  
  public void setCodOperacao(String codOperacao)  {
	this.codOperacao=codOperacao ;
    if (codOperacao==null) this.codOperacao="" ;
  }  
  public String getCodOperacao()  {
  	return this.codOperacao;
  }
  
  public void setCodOperacaoFk(String codOperacaoFk)  {
  this.codOperacaoFk=codOperacaoFk ;
    if (codOperacaoFk==null) this.codOperacaoFk="" ;
  }  
  public String getCodOperacaoFk()  {
  	return this.codOperacaoFk;
  }
  
  public void setNumOrdem(String numOrdem)  {
  this.numOrdem=numOrdem ;
    if (numOrdem==null) this.numOrdem="0" ;
  }  
  public String getNumOrdem()  {
  	return this.numOrdem;
  }    
  public void setSigFuncao(String sigFuncao)  {
  	this.sigFuncao    = sigFuncao;
    if (sigFuncao==null) this.sigFuncao="" ;	
  }  
  public String getSigFuncao()  {
  	return this.sigFuncao;
  }

  public void setSigFuncaoPai(String sigFuncaoPai)  {
  	this.sigFuncaoPai    = sigFuncaoPai;
    if (sigFuncaoPai==null) this.sigFuncaoPai="" ;	
	if (sigFuncaoPai.equals(this.sigFuncao)) this.sigFuncaoPai="" ;	
  }  
  public String getSigFuncaoPai()  {
  	return this.sigFuncaoPai;
  }
  public void setNomFuncaoPai(String nomFuncaoPai)  {
  	this.nomFuncaoPai    = nomFuncaoPai;
    if (nomFuncaoPai==null) this.nomFuncaoPai="" ;
	if (this.nomFuncaoPai.equals(this.nomOperacao)) this.nomFuncaoPai="" ;	
  }  
  public String getNomFuncaoPai()  {
  	return this.nomFuncaoPai;
  }

  public void setNomOperacao(String nomOperacao)  {
  	this.nomOperacao    = nomOperacao;
    if (nomOperacao==null) this.nomOperacao="" ;	
  }  
  public String getNomOperacao()  {
  	return this.nomOperacao;
  }

  public void setNomDescricao(String nomDescricao)  {
  	this.nomDescricao = nomDescricao;
    if (nomDescricao==null) this.nomDescricao="" ;	
  }  
  public String getNomDescricao()  {
  	return this.nomDescricao;
  }
  public void setNomApresentacao(String nomApresentacao)  {
  	this.nomApresentacao= nomApresentacao;
    if (nomApresentacao==null) this.nomApresentacao="" ;		
  }  
  public String getNomApresentacao() {
  	return this.nomApresentacao;
  }
  public void setNomExecuta(String nomExecuta)  {
  	this.nomExecuta= nomExecuta;
    if (nomExecuta==null) this.nomExecuta="" ;		
  }  
  public String getNomExecuta() {
  	return this.nomExecuta;
  }

  public void setTipOperacao(String tipOperacao)  {
  	this.tipOperacao = tipOperacao;
    if (tipOperacao==null) this.tipOperacao="" ;		
  }  
  public String getTipOperacao()  {
  	return this.tipOperacao;
  }

  public void setNivel(String nivel)  {
  	this.nivel    = nivel;
    if (nivel==null) this.nivel="0" ;	
  }  
  public String getNivel()  {
  	return this.nivel;
  }
  
  public void setSitBiometria(String sitBiometria)  {
	this.sitBiometria    = sitBiometria;
	if (sitBiometria==null) this.sitBiometria="I" ;	
  }  
  public String getSitBiometria()  {
	return this.sitBiometria;
  }
  
  
  public void setHabilita(String habilita)  {
		this.habilita    = habilita;
		if (habilita==null) this.habilita="S" ;	
	  }  
	  public String getHabilita()  {
		return this.habilita;
	  }
  
  
//--------------------------------------------------------------------------
public void Le_Operacao(String sist, String cod,int pos)  {
     try { 
	       Dao dao = Dao.getInstance();
 			if (pos==0) this.codOperacao=cod;		 				
			else {
			  this.sigFuncao  = cod  ;		 
			  this.codSistema = sist ;
			}   		   
    	   if ( dao.OperacaoLeBean(this, pos) == false) {
			    this.codSistema	  = "0";
				this.codOperacao  = "0";
				setPkid("0") ;		
           }	   		   
     }// fim do try
     catch (Exception e) {
		    this.codSistema  = "0";
		    this.codOperacao = "0";
			setPkid("0") ;		
     }
  }
 //------------------------------------------------------------------------ 
  public String getFuncao(String codSistema, SistemaBean mySist ){
	  String funcao = "" ; 
	  try    {
		 Dao dao = Dao.getInstance();		
		 funcao = dao.getFuncao(this,mySist) ;
	}
	catch(Exception e){
	 }	
	return funcao ;
  }
  
  
  
}