package ACSS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Desbloquea senha do usuario<br>
* <b>Description:</b>  Comando para Desbloquear senha do Usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class BloqueaSenhaCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/BloqueaSenha.jsp";
	private String next;

	public BloqueaSenhaCmd() {
		next = jspPadrao;
	}

	public BloqueaSenhaCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			//UsuarioBeanId � o usuario logado
			UsuarioBean UsuarioBeanId = (UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsuarioBeanId == null)UsuarioBeanId = new UsuarioBean();

			OrgaoBean OrgaoBeanId = (OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new OrgaoBean();

			UsuarioBean UsrBeanId =(UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new UsuarioBean();
			
			BloqueioSenhaBean bloqBean = (BloqueioSenhaBean)req.getAttribute("desbloqBean");
			if(bloqBean == null) bloqBean = new BloqueioSenhaBean();
             

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			// Orgao de Lotacao do Usuario
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";
			// Usuario Selecionado (pertence ao Orgao de Lotacao)
			String codUsuario = req.getParameter("codUsuario");
			if (codUsuario == null)	codUsuario = "0";
			String desapareceBotao = "";
			if (desapareceBotao == null) desapareceBotao = "N";

			
			if (acao.equals("buscaUsuarios")) {
				OrgaoBeanId.setCodOrgao(codOrgao);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			}

			if (acao.equals("busca")) {
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				OrgaoBeanId.setAtualizarDependente("S");
				desapareceBotao = "S";
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				
				//Prepara para dados para bloqueio
				String dataHoje = sys.Util.getDataHoje();
				bloqBean.setDataIni(dataHoje);
				UsrBeanId.setBloqueadoPor(UsuarioBeanId.getNomUserName());
			}
		
		    if (acao.equals("bloquear"))	{	
				OrgaoBeanId.setAtualizarDependente("S");
				desapareceBotao = "N";
				UsrBeanId.setCodUsuario(codUsuario);
						
				//Pega parametros da tela
				String motivo = req.getParameter("motivo");
				if (motivo == null) motivo = "";
				//String nomUserName = req.getParameter("nomUserName");
				String nomUserName = UsuarioBeanId.getNomUserName();
				if (nomUserName == null) nomUserName = "";
				
				//bloquear senha
				bloqBean.bloqueaSenha(UsrBeanId, motivo, nomUserName);
				
				//restaura valores na tela
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				String dataHoje = sys.Util.getDataHoje();
				bloqBean.setDataIni(dataHoje);
				UsrBeanId.setBloqueadoPor(UsuarioBeanId.getNomUserName());
				bloqBean.setTxtMotivo(motivo);
			}
			
			if (acao.equals("V")) {
				OrgaoBeanId.setAtualizarDependente("N");
				OrgaoBeanId.setCodOrgao(codOrgao);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			}

			req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			req.setAttribute("UsrBeanId", UsrBeanId);
			req.setAttribute("bloqBean", bloqBean);
			req.setAttribute("desapareceBotao",desapareceBotao);
			req.setAttribute("UsuarioBeanId", UsuarioBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("BloqueaSenhaCmd 001: "+ ue.getMessage());
		}
		
		return nextRetorno;
	}

}