package ACSS;

import java.awt.Cursor;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import sys.BeanException;
import sys.ServiceLocatorException;
import sys.Util;
import GER.NivelProcessoSistemaBean;
import GER.ProcessoSistemaBean;
import PNT.DaoBrokerFactory;
import REC.AutoInfracaoBean;
import REC.ControladorRecGeneric;
import REC.DaoBroker;
import REC.GerenciarAtaCmd;
import REC.HistoricoBean;
import REC.InfracaoBean;
import REC.RelatorBean;
import REC.RequerimentoBean;
import TAB.DaoException;
import UTIL.DataUtils;
import UTIL.Logs;

/**
 * <b>Title:</b> Controle de Acesso - Acesso a Base de Dados<br>
 * <b>Description:</b> Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b> Copyright (c) 2003<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 * @Updates
 */

public class Dao {
	private static final String MYABREVSIST = "ACSS";
	private static Dao instance;
	private sys.ServiceLocator serviceloc;
	private ArrayList<Ata> listAtas;

	public static Dao getInstance() throws DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}

	// **************************************************************************************
	private Dao() throws DaoException {
		try {
			serviceloc = sys.ServiceLocator.getInstance();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * ao UsuarioBean
	 * -----------------------------------------------------------
	 */

	/**
	 * ----------------------------------------------------------- Consulta
	 * Usuarios por codigo ou Username
	 * -----------------------------------------------------------
	 */
	public boolean UsuarioExiste(UsuarioBean myUsr, int tpPesq) throws DaoException {

		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String where = "WHERE ";
			if (tpPesq == 0)
				where += " cod_Usuario='" + myUsr.getCodUsuario() + "' ";
			else {
				where += " cod_orgao = " + myUsr.getOrgao().getCodOrgao() + " and ";
				where += " UPPER(nom_UserName) = '" + myUsr.getNomUserName().toUpperCase() + "' ";
			}
			String sCmd = "SELECT COD_USUARIO from TCAU_USUARIO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Consulta se
	 * Existe outro Usuario com este UserName
	 * -----------------------------------------------------------
	 */
	public boolean UsuarioExisteOutro(UsuarioBean myUsr) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String where = " WHERE cod_Usuario<>'" + myUsr.getCodUsuario() + "' and ";
			where += " UPPER(nom_UserName) = '" + myUsr.getNomUserName().toUpperCase() + "' and ";
			where += " cod_Orgao = '" + myUsr.getOrgao().getCodOrgao() + "' ";
			String sCmd = "SELECT COD_USUARIO from TCAU_USUARIO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Consulta
	 * Usuarios por codigo ou Username
	 * -----------------------------------------------------------
	 */
	public boolean AtaExiste(Ata myAta) throws DaoException {

		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_ATA ATA WHERE " + "ATA.DES_ATA = '" + myAta.getDescAta() + "'" + " AND"
					+ " ATA.TIPO_ATA = '" + myAta.getTpAta() + "' AND " + " ATA.STATUS ='" + myAta.getStatusAta() + ""
					+ "' AND " + " ATA.DT_SESSAO ='" + myAta.getDataSessao() + "' AND COD_ORGAO = '"
					+ ControladorRecGeneric.codOrgao + "'" + " and ATA.CD_NATUREZA = " + myAta.getCdNatureza();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean validaStatusAtaDao(String data) throws DaoException {

		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT * FROM TSMI_ATA ATA WHERE  " + " ATA.DES_ATA = '" + data + "'"
					+ " AND ATA.STATUS = 'A'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				String status = rs.getString("STATUS");
				if (status.equals("A")) {
					existe = true; // retorna verdadeiro se a ata estiver
									// aberta.
				} else {
					existe = false; // retorna verdadeiro se a ata estiver
									// aberta.
				}
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	// Verifica se o status dos auto com ata informada � DP OU 1P no caso de
	// status 2P n�o � informado nenhum numero de Ata apensa e�exibido o campo
	// para o devido preenchimento.
	public boolean concluirAta(String data) {
		boolean existe = false;
		String sCmd = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_REQUERIMENTO REQ,   TSMI_ATA ATA,  TSMI_RELATOR RELA    WHERE\n"
					+ "AUT.COD_AUTO_INFRACAO = REQ.COD_AUTO_INFRACAO AND\n"
					+ "AUT.COD_ATA = ATA.COD_ATA                     AND\n"
					+ "REQ.COD_RELATOR_JU = RELA.NUM_CPF             AND\n" + "ATA.DES_ATA  =  '" + data + "'"; // 001JR/15
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs != null) {
				while (rs.next()) {
					if (rs.getString("COD_TIPO_SOLIC").equals("2P")) {
						return true;// se o status for 2P o usu�rio informara em
									// texto o numero da Ata.
					} else {
						sCmd = "  UPDATE TSMI_ATA SET STATUS = 'F' WHERE DES_ATA =  '" + data + "'";
						rs = stmt.executeQuery(sCmd);
						while (rs.next()) {
							existe = true; // retorna verdadeiro se OK
						}
					}
				}
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return existe;
	}

	public boolean validaDataDao(String data) throws DaoException {

		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			String ANO = data.substring(6, 10);

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_DIAS_NAO_UTEIS DIA WHERE " + "DIA.DT_CONTROLE = '" + data + "'" + " AND"
					+ " DIA.NR_ANO_REF = '" + ANO + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true; // retorna verdadeiro se for sabado, domingo ou
								// feriado.
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public ArrayList<HashMap<String, String>> getAtaDeliberacaoMap(Ata ataS, String sigFuncao) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> maps;
		String codAta = "";
		int upDate = 0;
		ResultSet rs;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String REQ_COD_TIPO_SOLIC = "";

			// if(sigFuncao.equals("REC0193")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			//
			// if(sigFuncao.equals("REC0200")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			// if(sigFuncao.equals("REC0300")){
			// REQ_COD_TIPO_SOLIC = "1P";
			// }
			// if(sigFuncao.equals("REC0400")){
			// REQ_COD_TIPO_SOLIC = "2P";
			// }

			try {
				// TODOS OS AUTOS COM ESSA DESCRICAO DE ATA E COM ESSE STATUS DE
				// ATA
				sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_ATA ATA                 "
						+ " WHERE  AUT.COD_ATA = ATA.COD_ATA                                     "
						+ " AND  ATA.DES_ATA = '" + ataS.getDescAta() + "' AND  ATA.STATUS  = 'A' ";

				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getString("COD_STATUS").equals("5")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}
						if (rs.getString("COD_STATUS").equals("10")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("98")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("96")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("15")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("30")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("35")) {
							REQ_COD_TIPO_SOLIC = "2P";
						}

						try {
							// PARA CADA REGISTRO SELECIONO DE ACORDO COM O SEU
							// REQUERIMENTO DP-1P OU 2P
							sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_REQUERIMENTO REQ,   TSMI_ATA ATA,  TSMI_RELATOR RELA    WHERE "
									+ " AUT.COD_AUTO_INFRACAO = REQ.COD_AUTO_INFRACAO AND "
									+ " AUT.COD_ATA = ATA.COD_ATA                     AND " +
									// " REQ.COD_RELATOR_JU = RELA.NUM_CPF AND "
									// +
							" ( REQ.COD_RELATOR_JU = RELA.NUM_CPF   OR REQ.COD_RELATOR_JU = RELA.Cod_Relator)          AND "
									+ " ATA.DES_ATA = '" + ataS.getDescAta() + "' AND "
									+ " ATA.STATUS  = 'A' AND REQ.COD_TIPO_SOLIC= '" + REQ_COD_TIPO_SOLIC + "' ";
							rs = stmt.executeQuery(sCmd);
							if (rs != null) {

								while (rs.next()) {
									upDate = 1;
									AutoInfracaoBean auto = new AutoInfracaoBean();
									HistoricoBean autoHist = new HistoricoBean();
									// RequerimentoBean requerimento = new
									// RequerimentoBean();
									Ata ata = new Ata();
									// JuntaBean junta = new JuntaBean();
									RelatorBean relator = new RelatorBean();

									// Auto
									auto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
									auto.setNumPlaca(rs.getString("NUM_PLACA"));
									auto.setNumProcesso(rs.getString("NUM_PROCESSO"));
									maps = new HashMap<String, String>();
									maps.put("NUM_AUTO_INFRACAO", auto.getNumAutoInfracao());
									maps.put("NUM_PLACA", auto.getNumPlaca());
									maps.put("NUM_PROCESSO", auto.getNumProcesso());

									// Historico Auto
									String resultado = "";
									if (rs.getString("COD_RESULT_RS") != null) {
										if (rs.getString("COD_RESULT_RS").equals("I")) {
											resultado = "INDEFERIDO";
										} else {
											resultado = "DEFERIDO";
										}

									} else {
										resultado = "?";
									}
									maps.put("COD_RESULT_RS", resultado);

									// Relator
									relator.setNomRelator(rs.getString("NOM_RELATOR"));
									maps.put("NOM_RELATOR", relator.getNomRelator());
									// Ata
									ata.setCodAta(rs.getString("COD_ATA"));
									ata.setTpAta(rs.getString("TIPO_ATA"));
									ata.setDescAta(rs.getString("DES_ATA"));
									ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
									ata.setDataDistribuicao(
											retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
									ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
									ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
									ata.setStatusAta(rs.getString("STATUS"));
									maps.put("COD_ATA", ata.getCodAta());
									maps.put("TIPO_ATA", ata.getTpAta());
									maps.put("DES_ATA", ata.getDescAta());
									maps.put("DT_SESSAO", ata.getDataSessao());
									maps.put("DT_DISTRIBUICAO", ata.getDataDistribuicao());
									maps.put("JUNTA_ATA", ata.getJuntaAta());
									maps.put("DT_AVISO", ata.getDataAviso());
									maps.put("STATUS", ata.getStatusAta());

									arList.add(maps);
									codAta = ata.getCodAta();
								}
								rs.close();
								stmt.close();
							}

						} catch (Exception e) {
						}
					}
				}
				conn.close();
			} catch (Exception e) {

			}
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arList;
	}

	public ArrayList<HashMap<String, String>> getAtaDistribuicaoMap(Ata ataS, String sigFuncao) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> maps;
		String codAta = "";
		int upDate = 0;
		ResultSet rs;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String REQ_COD_TIPO_SOLIC = "";

			// if(sigFuncao.equals("REC0193")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			//
			// if(sigFuncao.equals("REC0200")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			// if(sigFuncao.equals("REC0300")){
			// REQ_COD_TIPO_SOLIC = "1P";
			// }
			// if(sigFuncao.equals("REC0400")){
			// REQ_COD_TIPO_SOLIC = "2P";
			// }

			try {
				// TODOS OS AUTOS COM ESSA DESCRICAO DE ATA E COM ESSE STATUS DE
				// ATA
				sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_ATA ATA                 "
						+ " WHERE  AUT.COD_ATA = ATA.COD_ATA                                     "
						+ " AND  ATA.DES_ATA = '" + ataS.getDescAta() + "' AND  ATA.STATUS  = 'A' ";

				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getString("COD_STATUS").equals("5")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("10")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("15")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("35")) {
							REQ_COD_TIPO_SOLIC = "2P";
						}

						try {
							// PARA CADA REGISTRO SELECIONO DE ACORDO COM O SEU
							// REQUERIMENTO DP-1P OU 2P
							sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_REQUERIMENTO REQ,   TSMI_ATA ATA,  TSMI_RELATOR RELA    WHERE "
									+ " AUT.COD_AUTO_INFRACAO = REQ.COD_AUTO_INFRACAO AND "
									+ " AUT.COD_ATA = ATA.COD_ATA                     AND "
									+ " ( REQ.COD_RELATOR_JU = RELA.NUM_CPF   OR REQ.COD_RELATOR_JU = RELA.Cod_Relator)          AND "
									+ " ATA.DES_ATA = '" + ataS.getDescAta() + "' AND "
									+ " ATA.STATUS  = 'A' AND REQ.COD_TIPO_SOLIC= '" + REQ_COD_TIPO_SOLIC + "' ";// VOLTAR
																													// PARA
																													// A
							rs = stmt.executeQuery(sCmd);

							if (rs != null) {

								while (rs.next()) {
									upDate = 1;
									AutoInfracaoBean auto = new AutoInfracaoBean();
									// HistoricoBean autoHist = new
									// HistoricoBean();
									// RequerimentoBean requerimento = new
									// RequerimentoBean();
									Ata ata = new Ata();
									// JuntaBean junta = new JuntaBean();
									RelatorBean relator = new RelatorBean();

									// Auto
									auto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
									auto.setNumPlaca(rs.getString("NUM_PLACA"));
									auto.setNumProcesso(rs.getString("NUM_PROCESSO"));
									maps = new HashMap<String, String>();
									maps.put("NUM_AUTO_INFRACAO", auto.getNumAutoInfracao());
									maps.put("NUM_PLACA", auto.getNumPlaca());
									maps.put("NUM_PROCESSO", auto.getNumProcesso());
									maps.put("NOM_CONDUTOR", auto.getCondutor().getNomResponsavel());

									// Relator
									relator.setNomRelator(rs.getString("NOM_RELATOR"));
									maps.put("NOM_RELATOR", relator.getNomRelator());
									// Ata
									ata.setCodAta(rs.getString("COD_ATA"));
									ata.setTpAta(rs.getString("TIPO_ATA"));
									ata.setDescAta(rs.getString("DES_ATA"));
									ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
									ata.setDataDistribuicao(
											retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
									ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
									ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
									ata.setStatusAta(rs.getString("STATUS"));
									maps.put("COD_ATA", ata.getCodAta());
									maps.put("TIPO_ATA", ata.getTpAta());
									maps.put("DES_ATA", ata.getDescAta());
									maps.put("DT_SESSAO", ata.getDataSessao());
									maps.put("DT_DISTRIBUICAO", ata.getDataDistribuicao());
									maps.put("JUNTA_ATA", ata.getJuntaAta());
									maps.put("DT_AVISO", ata.getDataAviso());
									maps.put("STATUS", ata.getStatusAta());

									arList.add(maps);
									codAta = ata.getCodAta();
								}
								/*
								 * Quando fechar a Ata? R: S� na conclus�o da
								 * Ata
								 */
								// if(upDate>0){
								// /*
								// * Update Ata
								// */
								// sCmd = " update tsmi_ata " +
								// " set status = 'F'"+
								// " where cod_ata = " + codAta;
								//
								// rs = stmt.executeQuery(sCmd) ;
								// }

								rs.close();
								stmt.close();
							}
							conn.close();

						} catch (Exception e) {
						}
					}
				}
			} catch (Exception e) {

			}
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arList;
	}

	public ArrayList<HashMap<String, String>> getAtaPublicacaoMap(Ata ataS, String sigFuncao) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		ArrayList<HashMap<String, String>> arList = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> maps;
		String codAta = "";
		int upDate = 0;
		ResultSet rs;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String REQ_COD_TIPO_SOLIC = "";

			// if(sigFuncao.equals("REC0193")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			//
			// if(sigFuncao.equals("REC0200")){
			// REQ_COD_TIPO_SOLIC = "DP";
			// }
			// if(sigFuncao.equals("REC0300")){
			// REQ_COD_TIPO_SOLIC = "1P";
			// }
			// if(sigFuncao.equals("REC0400")){
			// REQ_COD_TIPO_SOLIC = "2P";
			// }

			try {

				sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_ATA ATA                 "
						+ " WHERE  AUT.COD_ATA = ATA.COD_ATA                                     "
						+ " AND  ATA.DES_ATA = '" + ataS.getDescAta() + "' AND  ATA.STATUS  = 'F' ";

				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getString("COD_STATUS").equals("5")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}
						if (rs.getString("COD_STATUS").equals("10")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("98")) {
							REQ_COD_TIPO_SOLIC = "DP";
						}

						if (rs.getString("COD_STATUS").equals("15")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("96")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("30")) {
							REQ_COD_TIPO_SOLIC = "1P";
						}

						if (rs.getString("COD_STATUS").equals("35")) {
							REQ_COD_TIPO_SOLIC = "2P";
						}

						try {
							// PARA CADA REGISTRO SELECIONO DE ACORDO COM O SEU
							// REQUERIMENTO DP-1P OU 2P

							sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT,  TSMI_REQUERIMENTO REQ,   TSMI_ATA ATA,  TSMI_RELATOR RELA    WHERE "
									+ " AUT.COD_AUTO_INFRACAO = REQ.COD_AUTO_INFRACAO AND "
									+ " AUT.COD_ATA = ATA.COD_ATA                     AND " +
									// " REQ.COD_RELATOR_JU = RELA.NUM_CPF AND "
									// +
							" ( REQ.COD_RELATOR_JU = RELA.NUM_CPF   OR REQ.COD_RELATOR_JU = RELA.Cod_Relator)          AND "
									+ " ATA.DES_ATA = '" + ataS.getDescAta() + "' AND "
									+ " ATA.STATUS  = 'F' AND REQ.COD_TIPO_SOLIC= '" + REQ_COD_TIPO_SOLIC + "' ";
							rs = stmt.executeQuery(sCmd);
							if (rs != null) {

								while (rs.next()) {
									upDate = 1;
									AutoInfracaoBean auto = new AutoInfracaoBean();
									HistoricoBean autoHist = new HistoricoBean();
									// RequerimentoBean requerimento = new
									// RequerimentoBean();
									Ata ata = new Ata();
									// JuntaBean junta = new JuntaBean();
									RelatorBean relator = new RelatorBean();

									// Auto
									auto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
									auto.setNumPlaca(rs.getString("NUM_PLACA"));
									auto.setNumProcesso(rs.getString("NUM_PROCESSO"));
									maps = new HashMap<String, String>();
									maps.put("NUM_AUTO_INFRACAO", auto.getNumAutoInfracao());
									maps.put("NUM_PLACA", auto.getNumPlaca());
									maps.put("NUM_PROCESSO", auto.getNumProcesso());

									// Historico Auto
									String resultado = "";
									if (rs.getString("COD_RESULT_RS") != null) {
										if (rs.getString("COD_RESULT_RS").equals("I")) {
											resultado = "INDEFERIDO";
										} else {
											resultado = "DEFERIDO";
										}

									} else {
										resultado = "?";
									}
									maps.put("COD_RESULT_RS", resultado);

									// Relator
									relator.setNomRelator(rs.getString("NOM_RELATOR"));
									maps.put("NOM_RELATOR", relator.getNomRelator());
									// Ata
									ata.setCodAta(rs.getString("COD_ATA"));
									ata.setTpAta(rs.getString("TIPO_ATA"));
									ata.setDescAta(rs.getString("DES_ATA"));
									ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
									ata.setDataDistribuicao(
											retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
									ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
									ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
									ata.setStatusAta(rs.getString("STATUS"));
									maps.put("COD_ATA", ata.getCodAta());
									maps.put("TIPO_ATA", ata.getTpAta());
									maps.put("DES_ATA", ata.getDescAta());
									maps.put("DT_SESSAO", ata.getDataSessao());
									maps.put("DT_DISTRIBUICAO", ata.getDataDistribuicao());
									maps.put("JUNTA_ATA", ata.getJuntaAta());
									maps.put("DT_AVISO", ata.getDataAviso());
									maps.put("STATUS", ata.getStatusAta());

									arList.add(maps);
									codAta = ata.getCodAta();
								}
							}
						} catch (Exception e) {
						}
					}
				}
				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception e) {

			}
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arList;
	}

	public ArrayList<Ata> getAtaDistribuicao(Ata ataS) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = " SELECT  * FROM TSMI_AUTO_INFRACAO AUT, " + " TSMI_HISTORICO_AUTO H, " + " TSMI_REQUERIMENTO REQ,  "
					+ " TSMI_ATA ATA, " + " TSMI_JUNTA JUNT, " + " TSMI_RELATOR RELA   " + " WHERE "
					+ " AUT.COD_AUTO_INFRACAO = H.COD_AUTO_INFRACAO   AND "
					+ " AUT.COD_AUTO_INFRACAO = REQ.COD_AUTO_INFRACAO AND "
					+ " AUT.COD_ATA           = ATA.COD_ATA AND " + " ATA.DES_ATA = '" + ataS.getDescAta() + "' AND "
					+ " ATA.STATUS  = 'A'        AND " + " AUT.COD_ORGAO = JUNT.COD_ORGAO            AND "
					+ " JUNT.COD_ORGAO = REQ.COD_ORGAO_LOTACAO_DP AND "
					+ " REQ.COD_ORGAO_LOTACAO_DP = JUNT.COD_ORGAO AND " + " REQ.COD_RELATOR_JU = RELA.NUM_CPF";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {

						AutoInfracaoBean auto = new AutoInfracaoBean();
						// HistoricoBean autoHist = new HistoricoBean();
						// RequerimentoBean requerimento = new
						// RequerimentoBean();
						Ata ata = new Ata();
						// JuntaBean junta = new JuntaBean();
						RelatorBean relator = new RelatorBean();

						// Auto
						auto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
						auto.setNumPlaca(rs.getString("NUM_PLACA"));
						auto.setNumProcesso(rs.getString("NUM_PROCESSO"));

						// Relator
						relator.setNomRelator(rs.getString("NOM_RELATOR"));

						// Ata
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						listAtas.add(ata);

					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	public boolean AtaExisteI(Ata myAta) throws DaoException {

		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_ATA ATA WHERE " + "ATA.COD_ATA = '" + myAta.getCodAta() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Le Usuario e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean UsuarioLeBean(UsuarioBean myUsr, int tpUsr) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			return UsuarioLeBean(conn, myUsr, tpUsr);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * ao UsuarioBean
	 * -----------------------------------------------------------
	 */

	/* NOVA FRIBURGO */

	public boolean UsuarioLeBean(Connection conn, UsuarioBean myUsr, int tpUsr) throws DaoException {
		// synchronized
		boolean bOk = true;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT nom_Usuario,num_cpf,cod_Usuario,nom_UserName,cod_Senha,";
			sCmd += "cod_Usr_Resp,to_char(dat_Cadastro,'dd/mm/yyyy') as dat_Cadastro, ";
			sCmd += "cod_Usr_Resp_Alt,to_char(dat_Cadastro_Alt,'dd/mm/yyyy') as dat_Cadastro_Alt,cod_Orgao,";
			sCmd += "to_char(dat_Validade,'dd/mm/yyyy') as dat_Validade, val_hora_validade, txt_email, txt_local_trabalho,COD_PROT_UPO from TCAU_USUARIO ";

			if (tpUsr == 3) {
				sCmd += " WHERE UPPER(nom_UserName) = '" + myUsr.getNomUserName().toUpperCase() + "' AND COD_ORGAO='"
						+ myUsr.getOrgao().getCodOrgao() + "'";
			} else {
				if (tpUsr == 1) {
					sCmd += " WHERE UPPER(nom_UserName) = '" + myUsr.getNomUserName().toUpperCase() + "' ";
				} else
					sCmd += "WHERE cod_Usuario = '" + myUsr.getCodUsuario() + "'";
			}
			ResultSet rs = stmt.executeQuery(sCmd);
			myUsr.setMsgErro("");
			while (rs.next()) {
				myUsr.setNomUsuario(rs.getString("nom_Usuario"));
				myUsr.setNumCpf(rs.getString("num_cpf"));
				myUsr.setCodUsuario(rs.getString("cod_Usuario"));
				myUsr.setPkid(myUsr.getCodUsuario());
				myUsr.setNomUserName(rs.getString("nom_UserName"));
				myUsr.setCodSenha(rs.getString("cod_Senha"));

				myUsr.setCodUsrResp(rs.getString("cod_Usr_Resp"));
				myUsr.setDatCadastro(rs.getString("dat_Cadastro"));
				myUsr.setCodUsrRespAlt(rs.getString("cod_Usr_Resp_Alt"));
				myUsr.setDatCadastroAlt(rs.getString("dat_Cadastro_Alt"));

				myUsr.getOrgao().setCodOrgao(rs.getString("cod_Orgao"));
				myUsr.setDatValidade(rs.getString("dat_Validade"));
				myUsr.setHoraValidade(rs.getString("val_hora_validade"));
				myUsr.setEmail(rs.getString("txt_email"));
				myUsr.setLocalTrabalho(rs.getString("txt_local_trabalho"));
				myUsr.getProtocoloUpoBean().setCodProtocoloUpo((rs.getString("COD_PROT_UPO")));
			}
			sCmd = "SELECT sig_Orgao,nom_Orgao from TSMI_ORGAO WHERE cod_Orgao = '" + myUsr.getOrgao().getCodOrgao()
					+ "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myUsr.getOrgao().setSigOrgao(rs.getString("sig_Orgao"));
				myUsr.getOrgao().setPkid(myUsr.getOrgao().getCodOrgao());
				myUsr.getOrgao().setNomOrgao(rs.getString("nom_Orgao"));
			}
			sCmd = "select COD_PROT_UPO,NOM_PROT_UPO from SMIT.TSMI_PROT_UPO WHERE COD_PROT_UPO = '"
					+ myUsr.getProtocoloUpoBean().getCodProtocoloUpo() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myUsr.getProtocoloUpoBean().setNomProtocoloUpo(rs.getString("NOM_PROT_UPO"));
				myUsr.getOrgao().setPkid(myUsr.getProtocoloUpoBean().getCodProtocoloUpo());
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			bOk = false;
			myUsr.setCodUsuario("0");
			myUsr.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myUsr.setCodUsuario("0");
			myUsr.setPkid("0");
			throw new DaoException(ex.getMessage());
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Le Pergunta e
	 * Resposta para o Usuario e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public void UsuarioLePergResp(UsuarioBean myUsr, String mySist) throws DaoException {
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT txt_Perg,txt_Resp from TFIS_ESQSENHA WHERE cod_Usuario=" + myUsr.getCodUsuario();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myUsr.setNomPerg(rs.getString("txt_Perg"));
				myUsr.setNomResp(rs.getString("txt_Resp"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
		} catch (Exception ex) {
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Posiciona
	 * Usuario e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean buscaUsuario(UsuarioBean myUsr, int pos, UsuarioBean myUsrLog) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_orgao,COD_USUARIO from TCAU_USUARIO where cod_orgao in ("
					+ getOrgaos(myUsrLog, "SQL") + ") order by nom_Usuario";
			if (pos == 4)
				sCmd = "SELECT cod_orgao,COD_USUARIO from TCAU_USUARIO where cod_orgao in ("
						+ getOrgaos(myUsrLog, "SQL")
						+ ") and rpad(NOM_USUARIO,50,' ')||lpad(cod_usuario,10,'0') > rpad('" + myUsr.getNomUsuario()
						+ "',50,' ')||lpad(" + myUsr.getCodUsuario() + ",10,'0') order by nom_Usuario,cod_usuario";
			if (pos == 12)
				sCmd = "SELECT cod_orgao,COD_USUARIO from TCAU_USUARIO where cod_orgao in ("
						+ getOrgaos(myUsrLog, "SQL")
						+ ") and rpad(NOM_USUARIO,50,' ')||lpad(cod_usuario,10,'0') < rpad('" + myUsr.getNomUsuario()
						+ "',50,' ')||lpad(" + myUsr.getCodUsuario()
						+ ",10,'0') order by nom_Usuario desc,cod_usuario desc";
			if (pos == 21)
				sCmd = "SELECT cod_orgao,COD_USUARIO from TCAU_USUARIO where cod_orgao in ("
						+ getOrgaos(myUsrLog, "SQL") + ") order by nom_Usuario desc";
			if (pos == 25)
				sCmd = "SELECT cod_orgao,COD_USUARIO from TCAU_USUARIO where cod_orgao in ("
						+ getOrgaos(myUsrLog, "SQL") + ") and COD_USUARIO = '" + myUsr.getCodUsuario() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myUsr.setMsgErro("");
			if (rs.next()) {
				myUsr.getOrgao().setCodOrgao(rs.getString("cod_Orgao"));
				myUsr.setCodUsuario(rs.getString("cod_Usuario"));
				myUsr.setPkid(myUsr.getCodUsuario());
			}
			rs.close();
			stmt.close();
			bOk = UsuarioLeBean(myUsr, 0);
		} catch (SQLException e) {
			bOk = false;
			myUsr.setCodUsuario("0");
			myUsr.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myUsr.setCodUsuario("0");
			myUsr.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Usuario -----------------------------------------------------------
	 */
	public boolean UsuarioInsert(UsuarioBean myUsr) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO TCAU_USUARIO (cod_Usuario,nom_Usuario,num_cpf,nom_UserName,";
			sCmd += "cod_Senha,cod_Usr_Resp,dat_Cadastro,cod_Usr_Resp_Alt,dat_Cadastro_Alt,cod_orgao"
					+ ",dat_Validade, val_hora_validade, txt_email, txt_local_trabalho, nom_username_oper, dat_proc_oper, orgao_lotacao_oper, cod_prot_upo) ";
			sCmd += "VALUES (seq_tcau_usuario.nextval,";
			sCmd += "'" + myUsr.getNomUsuario() + "',";
			sCmd += "'" + myUsr.getNumCpf() + "',";
			sCmd += "'" + myUsr.getNomUserName() + "',";
			sCmd += "'" + sys.Util.encriptar(myUsr.getCodSenha().toUpperCase().trim()) + "',";
			sCmd += "'" + myUsr.getCodUsrResp() + "',";
			sCmd += "to_date('" + myUsr.getDatCadastro().substring(0, 10) + "','dd/mm/yyyy'),";
			sCmd += "'" + myUsr.getCodUsrRespAlt() + "',";
			sCmd += "to_date('" + myUsr.getDatCadastroAlt().substring(0, 10) + "','dd/mm/yyyy'),";
			sCmd += "'" + myUsr.getOrgao().getCodOrgao() + "',";
			sCmd += "to_date('" + myUsr.getDatValidade().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "'" + myUsr.getHoraValidade() + "',";
			sCmd += "'" + myUsr.getEmail() + "',";
			sCmd += "'" + myUsr.getLocalTrabalho() + "',";
			sCmd += "'" + myUsr.getNomUserNameOper() + "',";
			sCmd += "to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "'" + myUsr.getOrgaoLotacaoOper() + "',";
			sCmd += "'" + myUsr.getProtocoloUpoBean().getCodProtocoloUpo() + "')";

			stmt.execute(sCmd);
			sCmd = "SELECT cod_Usuario from TCAU_USUARIO ";
			sCmd += " WHERE cod_Orgao = '" + myUsr.getOrgao().getCodOrgao() + "'";
			sCmd += " AND UPPER(nom_UserName) = '" + myUsr.getNomUserName().toUpperCase() + "' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			myUsr.setMsgErro("");
			while (rs.next()) {
				myUsr.setCodUsuario(rs.getString("cod_Usuario"));
				myUsr.setPkid(myUsr.getCodUsuario());
			}
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Update em
	 * Usuario -----------------------------------------------------------
	 */
	public boolean UsuarioUpdate(UsuarioBean myUsr) throws DaoException {
		boolean existe = false;
		boolean novaSenha = true;
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = " SELECT * FROM TCAU_USUARIO WHERE " + " cod_Usuario=" + myUsr.getCodUsuario()
					+ " and cod_Senha LIKE '%" + myUsr.getCodSenha() + "%'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				novaSenha = false;

			sCmd = "UPDATE TCAU_USUARIO set ";
			sCmd += "nom_Usuario= '" + myUsr.getNomUsuario() + "',";
			sCmd += "num_cpf= '" + myUsr.getNumCpf() + "',";
			sCmd += "nom_UserName= '" + myUsr.getNomUserName() + "',";
			if (novaSenha)
				sCmd += "cod_Senha= '" + sys.Util.encriptar(myUsr.getCodSenha().toUpperCase().trim()) + "',";
			// else
			// sCmd += "cod_Senha= '"+myUsr.getCodSenha().trim()+"',";
			sCmd += "cod_Usr_Resp= '" + myUsr.getCodUsrResp() + "',";
			sCmd += "dat_Cadastro= to_date('" + myUsr.getDatCadastro().substring(0, 10) + "','dd/mm/yyyy'),";
			sCmd += "cod_Usr_Resp_Alt= '" + myUsr.getCodUsrRespAlt() + "',";
			sCmd += "dat_Cadastro_Alt= to_date('" + myUsr.getDatCadastroAlt().substring(0, 10) + "','dd/mm/yyyy'),";
			sCmd += "cod_orgao   = '" + myUsr.getOrgao().getCodOrgao() + "',";
			sCmd += "dat_Validade= to_date('" + myUsr.getDatValidade().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "val_hora_validade= '" + myUsr.getHoraValidade() + "', ";
			sCmd += "txt_email = '" + myUsr.getEmail() + "', ";
			sCmd += "txt_local_Trabalho= '" + myUsr.getLocalTrabalho() + "', ";
			sCmd += "nom_username_oper= '" + myUsr.getNomUserNameOper() + "', ";
			sCmd += "dat_proc_oper= to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "orgao_lotacao_oper= '" + myUsr.getOrgaoLotacaoOper() + "', ";
			sCmd += "cod_prot_upo= '" + myUsr.getProtocoloUpoBean().getCodProtocoloUpo() + "' ";

			sCmd += " where cod_Usuario=" + myUsr.getCodUsuario();
			stmt.execute(sCmd);
			stmt.close();
			existe = true;
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Update Senha
	 * em Usuario -----------------------------------------------------------
	 */
	public boolean UsuarioUpdateSenha(UsuarioBean idUsuario, String codSenha) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TCAU_USUARIO set ";
			sCmd += "cod_Senha= '" + sys.Util.encriptar(codSenha) + "',";
			sCmd += "orgao_lotacao_oper= " + idUsuario.getOrgao().getCodOrgao() + ",";
			sCmd += "nom_username_oper= '" + idUsuario.getNomUserName() + "',";
			sCmd += "dat_proc_oper =to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy') ";
			sCmd += "where cod_Usuario=" + idUsuario.getCodUsuario();
			stmt.execute(sCmd);
			stmt.close();
			existe = true;

			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * Usuario -----------------------------------------------------------
	 */
	public boolean UsuarioDelete(UsuarioBean myUsr) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "UPDATE TCAU_USUARIO set ";
			sCmd += "nom_username_oper= '" + myUsr.getNomUserNameOper() + "', ";
			sCmd += "dat_proc_oper= to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "orgao_lotacao_oper= '" + myUsr.getOrgaoLotacaoOper() + "' ";

			sCmd += " where cod_Usuario=" + myUsr.getCodUsuario();
			stmt.execute(sCmd);

			sCmd = "DELETE TCAU_USUARIO ";
			sCmd += " where cod_Usuario=" + myUsr.getCodUsuario();

			stmt.execute(sCmd);
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Verifica
	 * Senha de Usuarios
	 * -----------------------------------------------------------
	 */
	public boolean UsuarioLeSenha(String codUsuario, String newSenha) throws DaoException {
		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			String chashSenha = "";
			String hashSenha = sys.Util.encriptar(newSenha).toUpperCase().trim();
			Statement stmt = conn.createStatement();
			String sCmd = "select cod_Senha from tcau_usuario where cod_Usuario='" + codUsuario + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
				chashSenha = rs.getString("cod_Senha");
			}
			if (existe) {
				if ((hashSenha.equals("") == true) || (hashSenha.equals(chashSenha.toUpperCase().trim()) == false)) {
					existe = false;
				}
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * M�todo respons�vel por bloquear a senha do usuario ap�s 3 tentativas
	 * consecutivas.
	 * 
	 * @param usrBean
	 * @return Ok
	 * @throws DaoException
	 */
	public boolean bloquearSenha(UsuarioBean usrBean, String motivo, String nomUsrName) throws DaoException {
		boolean ok = false;
		boolean existe = false;
		Connection conn = null;
		String sql = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sql = "INSERT INTO tcau_bloqueio (cod_tcau_bloqueio,"
					+ "cod_usuario,dat_ini,txt_motivo,nom_username, ind_motivo) VALUES "
					+ "(seq_tcau_bloqueio.NEXTVAL, '" + usrBean.getCodUsuario() + "', " + "sysdate,'" + motivo + "','"
					+ nomUsrName + "', '" + usrBean.IND_BLOQ_3TENTATIVAS + "')";
			stmt.execute(sql);

			sql = "UPDATE tcau_Usuario set ind_bloqueio = '" + usrBean.IND_BLOQ_3TENTATIVAS + "'"
					+ " WHERE cod_usuario = " + usrBean.getCodUsuario();
			stmt.execute(sql);
			stmt.close();
			ok = true;
		} catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}

	/*-------------------------------------
	 * DAO Relativo ao BloqueaSenhaBean
	 * @author Luciana Rocha
	 --------------------------------------*/

	/**
	 * M�todo respons�vel por desbloquear senha mediante o codigo do usus�rio
	 * informado.
	 * 
	 * @param codUsuario
	 * @return ok
	 * @throws DaoException
	 * @throws BeanException
	 */
	public boolean desbloquearSenha(String codUsuario, UsuarioBean UsrLogado) throws DaoException, sys.BeanException {
		boolean ok = false;
		Connection conn = null;
		Statement stmt = null;
		String sCmd = "";
		UsuarioBean usrBean = new UsuarioBean();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			stmt = conn.createStatement();

			sCmd = "UPDATE TCAU_USUARIO set ";
			sCmd += "ind_bloqueio= '" + usrBean.IND_BLOQ_OK + "' ";
			sCmd += " where cod_usuario='" + codUsuario + "'";
			stmt.execute(sCmd);

			sCmd = "UPDATE TCAU_BLOQUEIO set ";
			sCmd += "dat_fim = sysdate";
			sCmd += " where cod_usuario='" + codUsuario + "'";
			stmt.execute(sCmd);

			stmt.close();
			conn.commit();
			ok = true;
		} catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}

	public boolean consultarBloqueios(BloqueioSenhaBean auxClasse, UsuarioBean usr) throws DaoException {

		Connection conn = null;
		boolean ok = false;
		ArrayList ListTemp = new ArrayList();

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// prepara os dados com orgao selecionado
			String sCmd = "SELECT to_char(dat_ini,'dd/mm/yyyy  -  hh24:mi:ss') dat_ini, to_char(dat_fim,'dd/mm/yyyy  -  hh24:mi:ss') dat_fim,"
					+ "txt_motivo, nom_username, cod_orgao_lotacao FROM TCAU_BLOQUEIO WHERE cod_usuario = '"
					+ usr.getCodUsuario() + "' and dat_fim is null";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				BloqueioSenhaBean dados = new BloqueioSenhaBean();
				dados.setNomUserName(rs.getString("nom_username"));
				dados.setTxtMotivo(rs.getString("txt_motivo"));
				dados.setDataIni(rs.getString("dat_ini"));
				dados.setDataFim(rs.getString("dat_fim"));
				dados.setCodOrgao(rs.getString("cod_orgao_lotacao"));
				ListTemp.add(dados);
				ok = true;
			}
			auxClasse.setDados(ListTemp);
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}

	/**
	 * M�todo respons�vel por verificar se a senha do usu�rio est� bloqueada.
	 * 
	 * @return ok
	 */

	/* NOVA FRIBURGO */

	public boolean verificaIndBloqueio(UsuarioBean usrBean) throws DaoException {
		boolean ok = false;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT ind_bloqueio FROM tcau_usuario WHERE " + "cod_usuario = " + usrBean.getCodUsuario()
					+ " AND COD_ORGAO='" + usrBean.getCodOrgaoAtuacao() + "'";
			String iBloq = "";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				iBloq = rs.getString("ind_bloqueio");
				if (iBloq == null)
					iBloq = "";
				if (iBloq.equals("1"))
					ok = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}

	/**
	 * ----------------------------------------------------------- Monta Ordem e
	 * Select para Consulta de Usuario
	 * -----------------------------------------------------------
	 */
	public String[][] UsuarioConsultaMontaSelect(UsuarioBean myUsrLog, String ordemUsuario, String sigOrgao,
			String nomUserName, String nomUsuario) throws DaoException {
		// nome das colunas comando (AS no select)
		String[] nomCol = { "cod_Usuario", "sig_orgao", "nom_UserName", "nom_Usuario" };
		String[][] wk = new String[7][nomCol.length];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < nomCol.length; j++) {
				wk[i][j] = "";
			}
		}
		// expressao para colunas
		String[] exprCol = { "U.cod_Usuario", "O.sig_Orgao", "U.nom_UserName", "U.nom_Usuario" };
		// cabecalhos - nome externo das colunas
		String[] nomCabec = { "Codigo", "�rg�o", "Login", "Nome" };
		// percentual
		String[] percCol = { "10%", "10%", "25%", "55%" };
		try {
			// montar a ordem (opcional pode ser "")
			// express�o para a ordem de apresentacao
			Vector vParam = new Vector();
			String exprOrdem = "";
			String exprpkOrdem = "";

			String[] exOrdem = new String[3];
			String[] expkOrdem = new String[3];

			exOrdem[0] = " nom_Usuario ";
			exOrdem[1] = " nom_UserName ";
			exOrdem[2] = " sig_orgao,u.nom_usuario ";
			expkOrdem[0] = " nom_Usuario ";
			expkOrdem[1] = " nom_UserName ";
			expkOrdem[2] = " RPAD(SIG_ORGAO,10,' ')||U.NOM_USUARIO ";

			// nome externo da ordem
			String exprNomOrdem = "";
			String[] nomOrdem = new String[3];
			nomOrdem[0] = "Nome Usuario";
			nomOrdem[1] = "Login";
			nomOrdem[2] = "�rg�o";

			// expressao para chave primaria do arquivo
			String exprPk = "Cod_Usuario"; // opcional - pode ser ""
			exprOrdem = exOrdem[1];
			exprpkOrdem = exOrdem[1];
			exprNomOrdem = "Ordem: " + nomOrdem[1];
			for (int i = 0; i < 3; i++) {
				if (ordemUsuario.equals(nomOrdem[i])) {
					// Ordem by
					exprOrdem = exOrdem[i];
					exprpkOrdem = expkOrdem[i];

					// descricao dos parametros
					exprNomOrdem = "Ordem: " + nomOrdem[i];
					vParam.addElement(exprNomOrdem);
				}
			}

			// comando select - primeira e segunda posicao podem ser Ordem e PK
			// (opcional)
			String cmdSelect = "SELECT ";
			if (exprOrdem.length() > 0) { // existe ordem
				if (exprPk.length() > 0)
					cmdSelect += exprpkOrdem + " as CONTEUDOORDEM," + exprPk + " as CONTEUDOPK,";
				else
					cmdSelect += exprpkOrdem + " as CONTEUDOORDEM,";
			} else {
				if (exprPk.length() > 0)
					cmdSelect += exprPk + " as CONTEUDOPK,";
			}

			// coloca as colunas
			for (int i = 0; i < nomCol.length; i++) {
				cmdSelect += exprCol[i] + " as " + nomCol[i] + ",";
			}
			// retira virgula e coloca o FROM
			cmdSelect = cmdSelect.substring(0, cmdSelect.length() - 1) + " FROM TCAU_USUARIO U,TSMI_ORGAO O ";

			// montaWhereTexto(objeto do input,"nome do campo","Nome externo
			// docampo")
			// Nome do campo ==> nome no comando select
			// Nome externo do campo ==> ser� utilizado para montagem das
			// descricoes dos parametros

			String cmdWhere = montaWhereTexto(nomUserName, "NOM_USERNAME", "Login", vParam);
			cmdWhere += montaWhereTexto(nomUsuario, "NOM_USUARIO", "Nome", vParam);
			cmdWhere += montaWhereTexto(sigOrgao, "SIG_ORGAO", "�rg�o", vParam);
			// colocar where no select
			if (cmdWhere.length() > 0)
				cmdSelect += " WHERE U.COD_ORGAO=O.COD_ORGAO AND " + cmdWhere.substring(5, cmdWhere.length());
			else
				cmdSelect += " WHERE u.cod_orgao in (" + getOrgaos(myUsrLog, "SQL") + ") and U.COD_ORGAO=O.COD_ORGAO ";
			// colocar order by no select
			if (exprOrdem.length() > 0)
				cmdSelect += " ORDER BY " + exprOrdem;
			// setar os wk
			/*
			 * wk[0][0] ==> parametros 0 (primeiro string descrevendo os
			 * parametros de selecao wk[1][0] ==> parametros 1 (segundo string
			 * descrevendo os parametros de selecao wk[2][0] ==> string com a
			 * ordem desejada da consulta wk[3][0] ==> string com o select
			 * utilizado pela consulta primeiro campo (opcional) sera a ordem de
			 * apresentacao escolhida segundo campo (opcional) deve ser o PK que
			 * vai popular o Bean no retorno o terceiro campo em diante serao as
			 * coluna a serem mostradas wk[4][n] ==> colunas a serem mostradas
			 * (n=numero de colunas) wk[5][n] ==> cabecalho das colunas a serem
			 * mostradas (n=numero de colunas) wk[6][n] ==> percentual para
			 * calcular o tamanho das colunas a serem mostradas (n=numero de
			 * colunas)
			 */
			for (int j = 0; j < vParam.size(); j++) {
				wk[j % 2][0] += vParam.elementAt(j);
			}
			wk[2][0] = exprpkOrdem; // ==> string com a ordem desejada da
									// consulta
			wk[3][0] = cmdSelect; // ==> string com a ordem desejada da consulta

			// nome das colunas comando (AS no select)
			wk[4] = nomCol;
			// cabecalhos - nome externo das colunas
			wk[5] = nomCabec;
			// percentual
			wk[6] = percCol;
		} catch (Exception ex) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < nomCol.length; j++) {
					wk[i][j] = "";
				}
			}
		}
		return wk;
	}

	/**
	 * ----------------------------------------------------------- Busca os
	 * Perfis do Usuario nos Sistemas
	 * -----------------------------------------------------------
	 */
	public void PerfilLotacaoLeBean(PerfilLotacaoBean myPerLot) throws DaoException {
		// synchronized
		Connection conn = null;
		ResultSet rs2 = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			myPerLot.setPerfisLotacao(new ArrayList());

			// preparar lista com todos os sistemas eem que o Usuario Logado �
			// Administrador
			// usuario � administrador no ACSS
			String csis = "";
			String sCmd = "";
			if ("0".equals(myPerLot.getCodNivelUsrLogado()) == false) {
				sCmd = "select cod_sistema from tcau_sistema";
			}
			// usuario nao e administrador no ACSS
			else {
				sCmd = "select s.cod_sistema,n.cod_nivel from tcau_sistema s, tcau_nivel_sistema n "
						+ " where s.cod_sistema=n.cod_sistema and n.cod_nivel in (1,2) and cod_usuario='"
						+ myPerLot.getCodUsrLogado() + "'";
			}

			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				csis += rs.getString("cod_Sistema") + ",";
			}
			if (csis.length() > 1) {
				csis = csis.substring(0, csis.length() - 1);
				// preparar lista com todos os sistemas e o nivel do usuario, em
				// cada um deles
				sCmd = "select s.cod_sistema,s.nom_sistema,n.cod_nivel from tcau_sistema s, (select * from tcau_nivel_sistema where cod_usuario='"
						+ myPerLot.getCodUsuario() + "') n"
						+ " where s.cod_sistema=n.cod_sistema(+) and s.cod_sistema in (" + csis + ")";
				rs = stmt.executeQuery(sCmd);
				while (rs.next()) {
					PerfilLotacaoBean myPLot = new ACSS.PerfilLotacaoBean();
					myPLot.setCodUsuario(myPerLot.getCodUsuario());
					myPLot.setCodOrgao(myPerLot.getCodOrgao());
					myPLot.setCodSistema(rs.getString("cod_Sistema"));
					myPLot.setNomSistema(rs.getString("nom_Sistema"));
					myPLot.setCodNivelUsr(rs.getString("cod_Nivel"));
					myPerLot.getPerfisLotacao().add(myPLot);
				}
			}
			// varrer cada sistema com o iterator e montar uma lista de perfis
			// (myListPerf)
			Iterator it = myPerLot.getPerfisLotacao().iterator();
			// lista de sistemas em branco
			List ListSist = new ArrayList();
			while (it.hasNext()) {
				// pega o primeiro sistema
				PerfilLotacaoBean mySist = new PerfilLotacaoBean();
				mySist = (PerfilLotacaoBean) it.next();
				// preparar lista com todos os perfis dos sistemas cada um deles
				// com cod_perfil_orgao preenchido quando o perfil e selecionado
				sCmd = "select p.nom_descricao,p.cod_perfil,u.cod_perfil_orgao from tcau_perfil p, ";
				sCmd += " (select cod_perfil_orgao,cod_perfil from tcau_perfil_orgao where cod_usuario='"
						+ mySist.getCodUsuario() + "' and cod_orgao_atuacao='" + mySist.getCodOrgao() + "') u ";
				sCmd += " where p.cod_sistema='" + mySist.getCodSistema() + "' and p.cod_perfil=u.cod_perfil(+)";
				// limpa a lista de perfis para o sistema (mySist)
				List myListPerf = new ArrayList();
				rs = stmt.executeQuery(sCmd);
				while (rs.next()) {
					// cria um novo perfil
					PerfilBean myPerf = new PerfilBean();
					myPerf.setNomDescricao(rs.getString("nom_descricao"));
					myPerf.setCodPerfil(rs.getString("cod_perfil"));
					sCmd = " select txt_justificativa from tcau_historico_perfil " + " where cod_sistema = "
							+ mySist.getCodSistema() + " and cod_orgao =" + myPerLot.getCodOrgao()
							+ " and cod_usuario =" + myPerLot.getCodUsuario() + " order by cod_historico_perfil desc";
					rs2 = stmt2.executeQuery(sCmd);
					if (rs2.next())
						myPerf.setTxtJustificativa(rs2.getString("txt_justificativa"));

					if (rs.getString("cod_perfil_orgao") != null) {
						// perfil foi selecionado para o usuario/orgao/sistema
						mySist.setCodPerfilUsr(myPerf.getCodPerfil());
					}
					// coloca o novo perfil na lista de perfil do perfilLotacao
					myListPerf.add(myPerf);
				}
				// atualiza a lista de perfis do sistema
				mySist.setPerfis(myListPerf);
				ListSist.add(mySist);
			}
			// atualiza a lista de sistema
			myPerLot.setPerfisLotacao(ListSist);
			rs.close();
			stmt.close();
			if (rs2 != null)
				rs2.close();

			stmt2.close();
			conn.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	public void gravaHistorico(PerfilLotacaoBean myPerLot, String[] codSist, String[] codPerf, String[] codNiv,
			String[] txtJustificativa) throws DaoException {

		Connection conn = null;
		String sCmd = "";
		boolean bExiste = false;
		ResultSet rs = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);

			// Verifico se existe usuario
			for (int i = 0; i < codSist.length; i++) {
				sCmd = " SELECT * FROM TCAU_HISTORICO_PERFIL WHERE  " + " COD_USUARIO='" + myPerLot.getCodUsuario()
						+ "' AND " + " COD_ORGAO = " + myPerLot.getCodOrgao() + " AND COD_SISTEMA = " + codSist[i];
				rs = stmt.executeQuery(sCmd);
				if (rs.next())
					bExiste = true;

				// Se n�o existir crio
				if (!bExiste) {
					if (("0".equals(codPerf[i]) == false) && ("".equals(codNiv[i]) == false)) {
						sCmd = " INSERT INTO TCAU_HISTORICO_PERFIL (COD_HISTORICO_PERFIL,COD_ORGAO,"
								+ " COD_USUARIO,COD_SISTEMA,COD_PERFIL_ATUAL,TXT_JUSTIFICATIVA,DAT_HIST_PERFIL) "
								+ " VALUES (SEQ_TCAU_HISTORICO_PERFIL.nextval,'" + myPerLot.getCodOrgao() + "'," + " '"
								+ myPerLot.getCodUsuario() + "','" + codSist[i] + "','" + codPerf[i] + "','"
								+ txtJustificativa[i] + "'," + " to_date('" + Util.getDataHoje().replaceAll("-", "/")
								+ "','dd/mm/yyyy')) ";
						stmt.execute(sCmd);
					}
				} else {
					if (("0".equals(codPerf[i]) == false) && ("".equals(codNiv[i]) == false)) {
						sCmd = " SELECT * FROM TCAU_HISTORICO_PERFIL WHERE  " + " COD_USUARIO='"
								+ myPerLot.getCodUsuario() + "' AND " + " COD_ORGAO = " + myPerLot.getCodOrgao()
								+ " AND COD_SISTEMA = " + codSist[i] + " ORDER BY COD_HISTORICO_PERFIL DESC";
						rs = stmt.executeQuery(sCmd);
						String codPerfilAnterior = "";
						if (rs.next()) {
							codPerfilAnterior = rs.getString("COD_PERFIL_ATUAL");
						}
						if (!codPerfilAnterior.equals(codPerf[i])) {
							sCmd = " INSERT INTO TCAU_HISTORICO_PERFIL (COD_HISTORICO_PERFIL,COD_ORGAO,"
									+ " COD_USUARIO,COD_SISTEMA,COD_PERFIL_ATUAL,COD_PERFIL_ANTERIOR,TXT_JUSTIFICATIVA,DAT_HIST_PERFIL) "
									+ " VALUES (SEQ_TCAU_HISTORICO_PERFIL.nextval,'" + myPerLot.getCodOrgao() + "',"
									+ " '" + myPerLot.getCodUsuario() + "','" + codSist[i] + "','" + codPerf[i] + "','"
									+ codPerfilAnterior + "','" + txtJustificativa[i] + "'," + " to_date('"
									+ Util.getDataHoje().replaceAll("-", "/") + "','dd/mm/yyyy')) ";
							stmt.execute(sCmd);
						} else {
							sCmd = " UPDATE TCAU_HISTORICO_PERFIL SET " + " TXT_JUSTIFICATIVA = ' "
									+ txtJustificativa[i] + "' " + " WHERE COD_PERFIL_ATUAL = " + codPerf[i];
							stmt.execute(sCmd);
						}
					}
				}
			}

			// fechar a transa��o - commit ou rollback
			conn.commit();
			stmt.close();

			if (rs != null)
				rs.close();

			conn.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Atualiza os
	 * Perfis e Niveis dos Usuario nos Sistemas
	 * -----------------------------------------------------------
	 */
	public void atualPerfilLotacao(PerfilLotacaoBean myPerLot, String[] codSist, String[] codPerf, String[] codNiv)
			throws DaoException {
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			myPerLot.setMsgErro("Perfil/Nivel para o Usu�rio n�o atualizados");
			conn.setAutoCommit(false);
			// deletar Perfil_Orgao para o Usuario e o Orgao

			String sCmd = "UPDATE tcau_perfil_orgao set ";
			sCmd += "nom_username_oper= '" + myPerLot.getNomUserNameOper() + "', ";
			sCmd += "dat_proc_oper= to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "orgao_lotacao_oper= '" + myPerLot.getOrgaoLotacaoOper() + "' ";

			sCmd += "where cod_usuario='" + myPerLot.getCodUsuario() + "' and cod_orgao_atuacao='"
					+ myPerLot.getCodOrgao() + "'";
			stmt.execute(sCmd);

			sCmd = "delete tcau_perfil_orgao where cod_usuario='" + myPerLot.getCodUsuario()
					+ "' and cod_orgao_atuacao='" + myPerLot.getCodOrgao() + "'";

			stmt.execute(sCmd);
			// deletar Nivel o Usuario
			sCmd = "delete tcau_nivel_sistema where cod_usuario='" + myPerLot.getCodUsuario() + "'";
			stmt.execute(sCmd);
			for (int i = 0; i < codSist.length; i++) {
				if (("0".equals(codPerf[i]) == false) && ("".equals(codNiv[i]) == false)) {
					sCmd = "INSERT INTO TCAU_PERFIL_ORGAO (cod_perfil_orgao,cod_Perfil,cod_Usuario,cod_orgao_atuacao,nom_username_oper,dat_proc_oper,orgao_lotacao_oper) ";
					sCmd += " VALUES (seq_tcau_perfil_orgao.nextval,'" + codPerf[i] + "','" + myPerLot.getCodUsuario()
							+ "','" + myPerLot.getCodOrgao() + "','" + myPerLot.getNomUserNameOper() + "',to_date('"
							+ sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'),"
							+ myPerLot.getOrgaoLotacaoOper() + ") ";
					stmt.execute(sCmd);
					sCmd = "INSERT INTO TCAU_NIVEL_SISTEMA (cod_sistema,cod_usuario,cod_nivel) ";
					sCmd += " VALUES ('" + codSist[i] + "','" + myPerLot.getCodUsuario() + "','" + codNiv[i] + "') ";
					stmt.execute(sCmd);
				}
			}
			// fechar a transa��o - commit ou rollback
			conn.commit();

			myPerLot.setMsgErro("Perfil/Nivel para o Usu�rio  atualizados");
			stmt.close();

			conn.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Busca Perfil
	 * do Usuario no Sistema
	 * -----------------------------------------------------------
	 */
	public void UsuarioPerfil(UsuarioBean myUsr, PerfilBean myPerf) throws DaoException {
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT u.cod_perfil_orgao,p.cod_perfil,p.nom_descricao, "
					+ "p.Ind_Vis_Endereco,p.Ind_Vis_2via,p.ind_recebe_notif,p.Ind_Vis_Ar,p.Ind_Vis_Auto,p.Ind_Vis_Foto,"
					+ "p.Ind_Vis_Req,p.Ind_Vis_Hist,p.Ind_Vis_Rej,p.Ind_Vis_Impr,Ind_Vis_Todos_Org "
					+ "from TCAU_PERFIL p, TCAU_PERFIL_ORGAO u " + "WHERE u.cod_perfil=p.cod_perfil  and "
					+ "      u.cod_usuario=" + myUsr.getCodUsuario() + " and  u.cod_orgao_atuacao="
					+ myUsr.getCodOrgaoAtuacao() + " and  p.cod_sistema=" + myPerf.getCodSistema();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myPerf.setPkid(rs.getString("cod_perfil_orgao"));
				myPerf.setCodPerfil(rs.getString("cod_perfil"));
				myPerf.setNomDescricao(rs.getString("nom_descricao"));
				myPerf.setIndVisEndereco(rs.getString("Ind_Vis_Endereco"));
				myPerf.setIndVis2via(rs.getString("Ind_Vis_2via"));
				myPerf.setIndRecebNot(rs.getString("ind_recebe_notif"));
				myPerf.setIndVisAr(rs.getString("Ind_Vis_Ar"));
				myPerf.setIndVisAuto(rs.getString("Ind_Vis_Auto"));
				myPerf.setIndVisFoto(rs.getString("Ind_Vis_Foto"));
				myPerf.setIndVisReq(rs.getString("Ind_Vis_Req"));
				myPerf.setIndVisHist(rs.getString("Ind_Vis_Hist"));
				myPerf.setIndVisRej(rs.getString("Ind_Vis_Rej"));
				myPerf.setIndVisImpr(rs.getString("Ind_Vis_Impr"));
				myPerf.setIndVisTodosOrg(rs.getString("Ind_Vis_Todos_Org"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Le Perfis
	 * -----------------------------------------------------------
	 */
	public String getUsuarios(UsuarioBean myUsr, int indicador) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			/*
			 * Filtra qual metodo o est� invocando Indicador = 1 - getUsuarios()
			 * Indicador = 2 - getUsuariosBloqueados()
			 */
			if (indicador != 1) {
				sCmd = "Select cod_usuario,nom_username,cod_orgao from TCAU_USUARIO";
				sCmd += " where cod_orgao ='" + myUsr.getOrgao().getCodOrgao() + "'and ind_bloqueio = '"
						+ myUsr.IND_BLOQ_3TENTATIVAS + "' order by nom_username";
			} else {
				sCmd = "Select cod_usuario,nom_username,cod_orgao from TCAU_USUARIO";
				sCmd += " where cod_orgao ='" + myUsr.getOrgao().getCodOrgao() + "' and (ind_bloqueio = '"
						+ myUsr.IND_BLOQ_OK + "' or ind_bloqueio is null) order by nom_username";
			}
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myUsrs += ",\"" + rs.getString("cod_usuario") + "\"";
				myUsrs += ",\"" + rs.getString("nom_username") + "\"";
				myUsrs += ",\"" + rs.getString("cod_orgao") + "\"";
			}
			myUsrs = myUsrs.substring(1, myUsrs.length());
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myUsrs;
	}

	/**
	 * ----------------------------------------------------------- Insert Perfil
	 * e Usuario -----------------------------------------------------------
	 */
	public void PerfilUsuarioInsert(UsuarioBean myUsr, PerfilBean myPer) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String codUsrPerfil = "0";
			String sCmd = "SELECT cod_usuario_perfil ";
			sCmd += "from TCAU_PERFIL_ORGAO  ";
			sCmd += "WHERE cod_perfil ='" + myPer.getCodPerfil() + "'   ";
			sCmd += "  and cod_usuario='" + myUsr.getCodUsuario() + "' ";
			sCmd += "  and cod_orgao_atuacao='" + myUsr.getCodOrgaoAtuacao() + "' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				codUsrPerfil = rs.getString("cod_usuario_perfil");
			}
			if (codUsrPerfil.equals("0") == false) {
				myPer.setMsgErro("Perfil j� existente." + " \n");
			} else {
				sCmd = "INSERT INTO TCAU_PERFIL_ORGAO (cod_perfil_orgao,cod_perfil,cod_usuario,cod_orgao) VALUES ";
				sCmd += "(seq_tcau_perfil_orgao.nextval,'" + myPer.getCodPerfil() + "','" + myUsr.getCodUsuario()
						+ "','')";
				stmt.execute(sCmd);
				sCmd = "SELECT cod_usuario_perfil ";
				sCmd += "from TCAU_PERFIL_ORGAO  ";
				sCmd += "WHERE cod_perfil ='" + myPer.getCodPerfil() + "'  and ";
				sCmd += "      cod_usuario='" + myUsr.getCodUsuario() + "'";
				while (rs.next()) {
					myPer.setPkid(rs.getString("cod_usuario_perfil"));
				}
				myPer.setMsgErro("Perfil Incluido:" + " \n");
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			myPer.setMsgErro("Erro no Banco de Dados" + e.getMessage() + " \n");
		} catch (Exception ex) {
			myPer.setMsgErro("Erro " + ex.getMessage() + " \n");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Insert Perfil
	 * e Usuario -----------------------------------------------------------
	 */
	public void PerfilUsuarioDelete(UsuarioBean myUsr, PerfilBean myPer) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String codUsrPerfil = "0";
			String sCmd = "SELECT cod_usuario_perfil ";
			sCmd += "from TCAU_PERFIL_ORGAO  ";
			sCmd += "WHERE cod_perfil ='" + myPer.getCodPerfil() + "'  and ";
			sCmd += "      cod_usuario='" + myUsr.getCodUsuario() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				codUsrPerfil = rs.getString("cod_usuario_perfil");
			}
			if (codUsrPerfil.equals("0")) {
				myPer.setMsgErro("Perfil " + myPer.getNomDescricao() + " n�o existente." + " \n");
			} else {
				sCmd = "DELETE TCAU_PERFIL_ORGAO where ";
				sCmd += "cod_perfil='" + myPer.getCodPerfil() + "' and cod_usuario='" + myUsr.getCodUsuario() + "'";
				myPer.setMsgErro("Perfil " + myPer.getNomDescricao() + " excluido:" + " \n");
			}
			stmt.execute(sCmd);
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			myPer.setMsgErro("Erro no Banco de Dados" + e.getMessage() + " \n");
		} catch (Exception ex) {
			myPer.setMsgErro("Erro " + ex.getMessage() + " \n");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * ao SistemaBean
	 * -----------------------------------------------------------
	 */
	/**
	 * ----------------------------------------------------------- Monta Ordem e
	 * Select para Consulta de Sistemas
	 * -----------------------------------------------------------
	 */
	public String[][] SistemaConsultaMontaSelect(String ordemSistema, String abrev, String nome) throws DaoException {
		// nome das colunas comando (AS no select)
		String[] nomCol = { "cod_Sistema", "nom_Abrev", "nom_Sistema", "sig_Versao" };
		String[][] wk = new String[7][nomCol.length];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < nomCol.length; j++) {
				wk[i][j] = "";
			}
		}
		// expressao para colunas
		String[] exprCol = { "cod_Sistema", "nom_Abrev", "nom_Sistema", "sig_Versao" };
		// cabecalhos - nome externo das colunas
		String[] nomCabec = { "Codigo", "Abrev", "Nome", "Versao" };
		// percentual
		String[] percCol = { "15%", "20%", "50%", "15%" };

		try {
			// montar a ordem (opcional pode ser "")
			// express�o para a ordem de apresentacao
			Vector vParam = new Vector();
			String exprOrdem = "";
			String[] exOrdem = new String[2];
			exOrdem[0] = " nom_Abrev ";
			exOrdem[1] = " nom_Sistema ";

			// nome externo da ordem
			String exprNomOrdem = "";
			String[] nomOrdem = new String[2];
			nomOrdem[0] = "Abreviatura";
			nomOrdem[1] = "Nome";

			// expressao para chave primaria do arquivo
			String exprPk = "Cod_Sistema"; // opcional - pode ser ""

			for (int i = 0; i < 2; i++) {
				if (ordemSistema.equals(nomOrdem[i])) {
					// Ordem by
					exprOrdem = exOrdem[i];
					// descricao dos parametros
					exprNomOrdem = "Ordem: " + nomOrdem[i];
					vParam.addElement(exprNomOrdem);
				}
			}

			// comando select - primeira e segunda posicao podem ser Ordem e PK
			// (opcional)
			String cmdSelect = "SELECT ";
			if (exprOrdem.length() > 0) { // existe ordem
				if (exprPk.length() > 0)
					cmdSelect += exprOrdem + " as CONTEUDOORDEM," + exprPk + " as CONTEUDOPK,";
				else
					cmdSelect += exprOrdem + " as CONTEUDOORDEM,";
			} else {
				if (exprPk.length() > 0)
					cmdSelect += exprPk + " as CONTEUDOPK,";
			}

			// coloca as colunas
			for (int i = 0; i < nomCol.length; i++) {
				cmdSelect += exprCol[i] + " as " + nomCol[i] + ",";
			}
			// retira virgula e coloca o FROM
			cmdSelect = cmdSelect.substring(0, cmdSelect.length() - 1) + " FROM TCAU_SISTEMA";
			// montaWhereTexto(objeto do input,"nome do campo","Nome externo do
			// campo")
			// Nome do campo ==> nome no comando select
			// Nome externo do campo ==> ser� utilizado para montagem das
			// descricoes dos parametros
			String cmdWhere = montaWhereTexto(abrev, "NOM_ABREV", "Abreviatura", vParam);
			cmdWhere += montaWhereTexto(nome, "NOM_SISTEMA", "Nome", vParam);
			// colocar where no select
			if (cmdWhere.length() > 0)
				cmdSelect += " WHERE " + cmdWhere.substring(5, cmdWhere.length());
			// colocar order by no select
			if (exprOrdem.length() > 0)
				cmdSelect += " ORDER BY " + exprOrdem;
			// setar os wk
			/*
			 * wk[0][0] ==> parametros 0 (primeiro string descrevendo os
			 * parametros de selecao wk[1][0] ==> parametros 1 (segundo string
			 * descrevendo os parametros de selecao wk[2][0] ==> string com a
			 * ordem desejada da consulta wk[3][0] ==> string com o select
			 * utilizado pela consulta primeiro campo (opcional) sera a ordem de
			 * apresentacao escolhida segundo campo (opcional) deve ser o PK que
			 * vai popular o Bean no retorno o terceiro campo em diante serao as
			 * coluna a serem mostradas wk[4][n] ==> colunas a serem mostradas
			 * (n=numero de colunas) wk[5][n] ==> cabecalho das colunas a serem
			 * mostradas (n=numero de colunas) wk[6][n] ==> percentual para
			 * calcular o tamanho das colunas a serem mostradas (n=numero de
			 * colunas)
			 */
			for (int j = 0; j < vParam.size(); j++) {
				wk[j % 2][0] += vParam.elementAt(j);
			}
			wk[2][0] = exprOrdem; // ==> string com a ordem desejada da consulta
			wk[3][0] = cmdSelect; // ==> string com a ordem desejada da consulta

			// nome das colunas comando (AS no select)
			wk[4] = nomCol;
			// cabecalhos - nome externo das colunas
			wk[5] = nomCabec;
			// percentual
			wk[6] = percCol;
		} catch (Exception ex) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < nomCol.length; j++) {
					wk[i][j] = "";
				}
			}
		}
		return wk;
	}

	/**
	 * ----------------------------------------------------------- Consulta
	 * Sistema por codigo ou name
	 * -----------------------------------------------------------
	 */
	public boolean SistemaExiste(SistemaBean mySist, int tp) throws DaoException {
		boolean existe = false;
		// synchronized
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String where = "";
			if (tp == 0)
				where = " WHERE Cod_Sistema=" + mySist.getCodSistema();
			else {
				where = " UPPER(nom_Sistema) = '" + mySist.getNomSistema().toUpperCase() + "'";
				where += " or UPPER(nom_Abrev) = '" + mySist.getNomAbrev().toUpperCase() + "'";
			}
			String sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				existe = true;
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Le Sistema e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean SistemaLeBean(SistemaBean mySist, int tp) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			if (tp == 0)
				sCmd = "SELECT * from TCAU_SISTEMA WHERE cod_sistema='" + mySist.getCodSistema() + "'";
			else
				sCmd = "SELECT * from TCAU_SISTEMA WHERE (nom_abrev='" + mySist.getNomAbrev() + "') or (nom_sistema='"
						+ mySist.getNomSistema() + "')";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				retorno = true;
				mySist.setCodSistema(rs.getString("cod_sistema"));
				mySist.setPkid(mySist.getCodSistema());
				mySist.setSigVersao(rs.getString("sig_versao"));
				mySist.setNomSistema(rs.getString("nom_sistema"));
				mySist.setNomAbrev(rs.getString("nom_abrev"));
			}

			rs.close();
			stmt.close();
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			mySist.setCodSistema("0");
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			mySist.setCodSistema("0");
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Posiciona
	 * Sistema e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean BuscaSistema(SistemaBean mySist, int pos) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA order by nom_sistema";
			if (pos == 4)
				sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA where NOM_SISTEMA > '" + mySist.getNomSistema()
						+ "' order by nom_sistema";
			if (pos == 12)
				sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA where NOM_SISTEMA < '" + mySist.getNomSistema()
						+ "' order by nom_sistema desc";
			if (pos == 21)
				sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA order by nom_sistema desc";
			if (pos == 25)
				sCmd = "SELECT COD_SISTEMA from TCAU_SISTEMA where COD_SISTEMA = '" + mySist.getCodSistema() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			mySist.setMsgErro("");
			if (rs.next()) {
				mySist.setCodSistema(rs.getString("cod_sistema"));
				mySist.setPkid(mySist.getCodSistema());
			}
			rs.close();
			stmt.close();
			retorno = SistemaLeBean(mySist, 0);
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			retorno = false;
			mySist.setCodSistema("0");
			mySist.setPkid("0");
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			mySist.setCodSistema("0");
			mySist.setPkid("0");
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Sistema -----------------------------------------------------------
	 */
	public boolean SistemaInsert(SistemaBean mySist) throws DaoException {
		boolean retorno = false;
		Vector vErro = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			int existe = 0;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_sistema from TCAU_SISTEMA WHERE NOM_SISTEMA='" + mySist.getNomSistema()
					+ "' or nom_abrev= '" + mySist.getNomAbrev() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				existe = 1;
			if (existe == 1) {
				vErro.addElement(
						"Sistema j� cadastrado com " + mySist.getNomAbrev() + " ou " + mySist.getNomSistema() + " \n");
				retorno = false;
			} else {
				sCmd = "INSERT INTO TCAU_SISTEMA (cod_sistema, sig_versao, ";
				sCmd += "nom_sistema, nom_abrev)";
				sCmd += " VALUES (seq_tcau_sistema.nextval,";
				sCmd += "'" + mySist.getSigVersao() + "',";
				sCmd += "'" + mySist.getNomSistema() + "',";
				sCmd += "'" + mySist.getNomAbrev() + "')";
				stmt.execute(sCmd);
				sCmd = "SELECT cod_sistema from TCAU_SISTEMA WHERE nom_abrev='" + mySist.getNomAbrev() + "'";
				rs = stmt.executeQuery(sCmd);
				retorno = true;
				while (rs.next()) {
					mySist.setCodSistema(rs.getString("cod_sistema"));
					mySist.setPkid(mySist.getCodSistema());
				}
				vErro.addElement("Sistema incluido com o c�digo: " + mySist.getCodSistema() + " \n");
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			vErro.addElement("Sistema n�o incluido: " + mySist.getNomAbrev() + " \n");
			vErro.addElement("Erro na inclus�o: " + e.getMessage() + " \n");
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Update em
	 * Sistema -----------------------------------------------------------
	 */
	public boolean SistemaUpdate(SistemaBean mySist) throws DaoException {
		boolean retorno = true;
		Vector vErro = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			int existe = 0;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_sistema from TCAU_SISTEMA WHERE cod_sistema= '" + mySist.getCodSistema() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				existe = 1;
			if (existe == 0) {
				vErro.addElement("Sistema n�o cadastrado com c�digo " + mySist.getCodSistema() + " \n");
				retorno = false;
			} else {
				sCmd = "UPDATE TCAU_SISTEMA SET ";
				sCmd += "sig_versao = '" + mySist.getSigVersao() + "',";
				sCmd += "nom_sistema = '" + mySist.getNomSistema() + "',";
				sCmd += "nom_abrev   = '" + mySist.getNomAbrev() + "' ";
				sCmd += "where cod_sistema='" + mySist.getCodSistema() + "'";
				stmt.execute(sCmd);
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			vErro.addElement("Erro na altera��o do Banco de Dados: " + sqle.getMessage() + " \n");
			retorno = false;
		} catch (Exception e) {
			vErro.addElement("Erro na altera��o: " + e.getMessage() + " \n");
			retorno = false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;

	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * Sistema -----------------------------------------------------------
	 */
	public boolean SistemaDelete(SistemaBean mySist) throws DaoException {
		boolean retorno = false;
		Vector vErro = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			int existe = 0;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_sistema from TCAU_SISTEMA WHERE cod_sistema ='" + mySist.getCodSistema() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				existe = 1;

			if (existe == 0) {
				vErro.addElement("Sistema n�o cadastrado com c�dgio " + mySist.getCodSistema() + " \n");
				retorno = false;
			} else {
				sCmd = "DELETE TCAU_SISTEMA where cod_sistema=" + mySist.getCodSistema();
				stmt.execute(sCmd);
				vErro.addElement("Sistema excluido: " + mySist.getCodSistema() + " \n");
				retorno = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			vErro.addElement("Sistema n�o excluido: " + mySist.getCodSistema() + " \n");
			vErro.addElement("Erro na exclus�o do Banco de Dados: " + sqle.getMessage() + " \n");
			retorno = false;
		} // fim do catch
		catch (Exception ex) {
			retorno = false;
			throw new DaoException(ex.getMessage());
		}

		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Le Sistemas
	 * -----------------------------------------------------------
	 */
	public String getSistemas(String codUsr) throws DaoException {
		String mySists = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			int admACSS = 0;
			String sCmd = "Select ns.cod_nivel from TCAU_SISTEMA s, TCAU_NIVEL_SISTEMA ns where ns.cod_usuario='"
					+ codUsr + "' and ns.cod_nivel<>0 and ns.cod_sistema=s.cod_sistema and s.nom_abrev='ACSS' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				admACSS = rs.getInt("cod_nivel");
			}
			if (admACSS == 0)
				sCmd = "select s.cod_sistema,s.nom_sistema from TCAU_SISTEMA s, TCAU_NIVEL_SISTEMA ps where ps.cod_usuario='"
						+ codUsr + "' and ps.cod_nivel<>0 and ps.cod_sistema=s.cod_sistema order by s.nom_sistema";
			else
				sCmd = "Select s.cod_sistema,s.nom_sistema from TCAU_SISTEMA s order by s.nom_sistema";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				mySists += ",\"" + rs.getString("cod_sistema") + "\"";
				mySists += ",\"" + rs.getString("nom_sistema") + "\"";
			}
			mySists = mySists.substring(1, mySists.length());
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return mySists;
	}

	public String getProcessos(String codSistema) throws DaoException {
		String myProcs = "";
		Connection conn = null;
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "Select cod_processo_sistema,nom_processo from TSMI_PROCESSO_SISTEMA";
			sCmd += " where cod_sistema ='" + codSistema + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myProcs += ",\"" + rs.getString("cod_processo_sistema") + "\"";
				myProcs += ",\"" + rs.getString("nom_processo") + "\"";
			}
			myProcs = myProcs.substring(1, myProcs.length());
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myProcs;
	}

	public void SistemaGetPerfis(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT p.cod_perfil,p.nom_descricao,"
					+ "p.Ind_Vis_Endereco,p.Ind_Vis_2via,p.ind_recebe_notif,p.Ind_Vis_Ar,p.Ind_Vis_Auto,p.Ind_Vis_Foto,"
					+ "p.Ind_Vis_Req,p.Ind_Vis_Hist,p.Ind_Vis_Rej,p.Ind_Vis_Impr,p.Ind_Vis_Todos_Org "
					+ " from TCAU_PERFIL p" + " WHERE p.cod_sistema = '" + mySist.getCodSistema() + "'"
					+ " order by p.nom_descricao";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector perfis = new Vector();
			while (rs.next()) {
				PerfilBean myPerf = new PerfilBean();
				myPerf.setCodPerfil(rs.getString("cod_perfil"));
				myPerf.setNomDescricao(rs.getString("nom_descricao"));
				myPerf.setIndVisEndereco(rs.getString("Ind_Vis_Endereco"));
				myPerf.setIndVis2via(rs.getString("Ind_Vis_2via"));
				myPerf.setIndRecebNot(rs.getString("ind_recebe_notif"));
				myPerf.setIndVisAr(rs.getString("Ind_Vis_Ar"));
				myPerf.setIndVisAuto(rs.getString("Ind_Vis_Auto"));
				myPerf.setIndVisFoto(rs.getString("Ind_Vis_Foto"));
				myPerf.setIndVisReq(rs.getString("Ind_Vis_Req"));
				myPerf.setIndVisHist(rs.getString("Ind_Vis_Hist"));
				myPerf.setIndVisRej(rs.getString("Ind_Vis_Rej"));
				myPerf.setIndVisImpr(rs.getString("Ind_Vis_Impr"));
				myPerf.setIndVisTodosOrg(rs.getString("Ind_Vis_Todos_Org"));
				perfis.addElement(myPerf);
			}
			mySist.setPerfis(perfis);
			rs.close();
			stmt.close();
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			mySist.setMsgErro("Sistema008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("0");
		} catch (Exception e) {
			mySist.setMsgErro("Sistema008 - Leitura: " + e.getMessage());
			mySist.setCodSistema("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Funcoes em Sistema
	 * -----------------------------------------------------------
	 */
	public boolean SistemaInsertPerfis(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas os perfis antigas
			Vector funcAtual = mySist.getPerfis();
			String codPerf = "0";
			String sDesc = "";
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < funcAtual.size(); i++) {
				PerfilBean myPerf = (PerfilBean) funcAtual.elementAt(i);
				if ("".equals(myPerf.getNomDescricao()) == false) {
					PerfilInsert(myPerf, stmt);
				} else {
					if ("".equals(myPerf.getCodPerfil()) == false)
						PerfilDelete(myPerf, stmt);
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Perfil -----------------------------------------------------------
	 */
	public void PerfilInsert(PerfilBean myPerfil, Statement stmt) throws DaoException {
		try {
			String codPerf = myPerfil.getCodPerfil();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codPerf)) || ("0".equals(codPerf))) {
				sCmd = "INSERT INTO TCAU_PERFIL (cod_Perfil,nom_Descricao,"
						+ "Ind_Vis_Endereco,Ind_Vis_2via,Ind_Recebe_Notif,Ind_Vis_Ar,Ind_Vis_Auto,Ind_Vis_Foto,"
						+ "Ind_Vis_Req,Ind_Vis_Hist,Ind_Vis_Rej,Ind_Vis_Impr,Ind_Vis_Todos_Org," + "cod_Sistema) "
						+ "VALUES (seq_tcau_perfil.nextval,'" + myPerfil.getNomDescricao() + "'," + "'"
						+ (((myPerfil.getIndVisEndereco().length() > 0)
								&& ("Ss1".indexOf(myPerfil.getIndVisEndereco()) >= 0)) ? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVis2via().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVis2via()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndRecebNot().length() > 0) && ("Ss1".indexOf(myPerfil.getIndRecebNot()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisAr().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisAr()) >= 0)) ? "1"
								: "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisAuto().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisAuto()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisFoto().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisFoto()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisReq().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisReq()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisHist().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisHist()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisRej().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisRej()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisImpr().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisImpr()) >= 0))
								? "1" : "0")
						+ "'," + "'"
						+ (((myPerfil.getIndVisTodosOrg().length() > 0)
								&& ("Ss1".indexOf(myPerfil.getIndVisTodosOrg()) >= 0)) ? "1" : "0")
						+ "'," + "'" + myPerfil.getCodSistema() + "') ";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TCAU_PERFIL set " + "nom_Descricao     ='" + myPerfil.getNomDescricao() + "', "
						+ "Ind_Vis_Endereco  ='"
						+ (((myPerfil.getIndVisEndereco().length() > 0)
								&& ("Ss1".indexOf(myPerfil.getIndVisEndereco()) >= 0)) ? "1" : "0")
						+ "', " + "Ind_Vis_2via      ='"
						+ (((myPerfil.getIndVis2via().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVis2via()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Recebe_Notif  ='"
						+ (((myPerfil.getIndRecebNot().length() > 0) && ("Ss1".indexOf(myPerfil.getIndRecebNot()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Ar        ='"
						+ (((myPerfil.getIndVisAr().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisAr()) >= 0)) ? "1"
								: "0")
						+ "', " + "Ind_Vis_Auto      ='"
						+ (((myPerfil.getIndVisAuto().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisAuto()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Foto      ='"
						+ (((myPerfil.getIndVisFoto().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisFoto()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Req       ='"
						+ (((myPerfil.getIndVisReq().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisReq()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Hist      ='"
						+ (((myPerfil.getIndVisHist().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisHist()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Rej       ='"
						+ (((myPerfil.getIndVisRej().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisRej()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Impr      ='"
						+ (((myPerfil.getIndVisImpr().length() > 0) && ("Ss1".indexOf(myPerfil.getIndVisImpr()) >= 0))
								? "1" : "0")
						+ "', " + "Ind_Vis_Todos_org ='"
						+ (((myPerfil.getIndVisTodosOrg().length() > 0)
								&& ("Ss1".indexOf(myPerfil.getIndVisImpr()) >= 0)) ? "1" : "0")
						+ "' " + " where cod_perfil = '" + codPerf + "' ";
				stmt.execute(sCmd);
			}
			sCmd = "SELECT COD_PERFIL FROM TCAU_PERFIL ";
			sCmd += "WHERE nom_descricao='" + myPerfil.getNomDescricao() + "' and cod_sistema='"
					+ myPerfil.getCodSistema() + "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myPerfil.setCodPerfil(rs.getString("cod_Perfil"));
			}

			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * Perfil -----------------------------------------------------------
	 */
	public void PerfilDelete(PerfilBean myPerfil, Statement stmt) throws DaoException {
		try {
			String sCmd = "DELETE TCAU_PERFIL where cod_perfil = '" + myPerfil.getCodPerfil() + "'";
			stmt.execute(sCmd);
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Le
	 * Funcionalidades do Sistema
	 * -----------------------------------------------------------
	 */
	public void SistemaGetFuncoes(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT o.cod_operacao,o.num_ordem,o.nom_executa,o.sig_funcao,pai.sig_funcao sig_funcao_pai,"
					+ "o.nom_operacao, o.cod_operacao_fk, o.nom_apresentacao,o.nom_descricao,o.sit_biometria,o.ind_ativo "
					+ " from TCAU_OPERACAO o, TCAU_OPERACAO pai" + " WHERE o.cod_sistema = '" + mySist.getCodSistema()
					+ "'" + " and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao "
					+ " and o.cod_operacao_fk is null " + " order by o.sig_funcao, o.num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector funcoes = new Vector();
			while (rs.next()) {
				OperacaoBean myOper = new OperacaoBean();
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setSigFuncaoPai(rs.getString("sig_funcao_pai"));
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));
				myOper.setSitBiometria(rs.getString("sit_biometria"));
				myOper.setHabilita(rs.getString("ind_ativo"));
				funcoes.addElement(myOper);
				acharFilhos(conn, myOper, mySist, funcoes);
			}
			// prepara os niveis

			mySist.setFuncoes(FuncoesSetNivel(funcoes));
			rs.close();
			stmt.close();
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			mySist.setMsgErro("Sistema005 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("0");
		} catch (Exception e) {
			mySist.setMsgErro("Sistema006 - Leitura: " + e.getMessage());
			mySist.setCodSistema("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	private boolean acharFilhos(Connection conn, OperacaoBean bean, SistemaBean mySist, Vector funcoes)
			throws DaoException {

		boolean retorno = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT o.cod_operacao,o.num_ordem,o.nom_executa,o.sig_funcao,pai.sig_funcao sig_funcao_pai,"
					+ "o.nom_operacao, o.cod_operacao_fk, o.nom_apresentacao,o.nom_descricao,o.sit_biometria "
					+ " from TCAU_OPERACAO o, TCAU_OPERACAO pai" + " WHERE pai.sig_funcao = '" + bean.getSigFuncao()
					+ "'" + " and o.cod_sistema = '" + mySist.getCodSistema() + "' "
					+ " and o.cod_operacao_fk is not null "
					+ " and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao "
					+ " order by o.sig_funcao, o.num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				retorno = true;
				OperacaoBean myOper = new OperacaoBean();
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setSigFuncaoPai(rs.getString("sig_funcao_pai"));
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));
				myOper.setSitBiometria(rs.getString("sit_biometria"));
				funcoes.addElement(myOper);
				acharFilhos(conn, myOper, mySist, funcoes);
			}
			// prepara os niveis

			rs.close();
			stmt.close();
			conn.close();
			return retorno;
		} // fim do try
		catch (SQLException sqle) {
			throw new DaoException("Sistema005 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
		} catch (Exception e) {
			throw new DaoException("Sistema006 - Leitura: " + e.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Atualiza os
	 * niveis das Funcoes
	 * -----------------------------------------------------------
	 */
	public Vector FuncoesSetNivel(Vector funcoes) {
		try {
			int nivel = 0;
			for (int i = 0; i < funcoes.size(); i++) {
				nivel = 0;
				OperacaoBean myOper = (OperacaoBean) funcoes.elementAt(i);
				nivel = veNivel(funcoes, myOper.getSigFuncaoPai());
				myOper.setNivel(Integer.toString(nivel));
				funcoes.removeElementAt(i);
				funcoes.insertElementAt(myOper, i);
			}
		} catch (Exception e) {
		} // fim do catch
		return funcoes;
	}

	public int veNivel(Vector funcoes, String funcaoPai) {
		int niv = 0;
		if ("".equals(funcaoPai))
			return niv;
		for (int i = 0; i < funcoes.size(); i++) {
			OperacaoBean myO = (OperacaoBean) funcoes.elementAt(i);
			if (funcaoPai.equals(myO.getSigFuncao())) {
				niv = Integer.parseInt(myO.getNivel());
				niv++;
			}
		}
		return niv;
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Funcoes em Sistema
	 * -----------------------------------------------------------
	 */

	public boolean SistemaInsertFuncoes(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas as opera��es antigas
			Vector funcAtual = mySist.getFuncoes();
			String codOper = "0";
			String sFunc = "";
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int nivel = 0; nivel < 6; nivel++) {
				for (int i = 0; i < funcAtual.size(); i++) {
					OperacaoBean myOper = (OperacaoBean) funcAtual.elementAt(i);
					int myNivel = Integer.parseInt(myOper.getNivel());
					if (nivel == myNivel) {
						if ("".equals(myOper.getSigFuncao()) == false) {
							// Permite transformar um filho em pai
							String codOperacaoFk = myOper.getCodOperacaoFk();
							if (myOper.getSigFuncaoPai().equals("") && !codOperacaoFk.equals("")) {
								myOper.setCodOperacaoFk("");
								myOper.setSigFuncaoPai("");
							}

							OperacaoInsert(myOper, stmt);
							codOper = myOper.getCodOperacao();
							sFunc = myOper.getSigFuncao();
							// procurar os filhos e colocar o codigo de Operacao
							// do Pai
							for (int j = 0; j < funcAtual.size(); j++) {
								OperacaoBean myOp = (OperacaoBean) funcAtual.elementAt(j);
								if (sFunc.equals(myOp.getSigFuncaoPai()) == true) {
									myOp.setCodOperacaoFk(codOper);
									funcAtual.removeElementAt(j);
									funcAtual.insertElementAt(myOp, j);
								}
							}
						} else {
							if ("".equals(myOper.getCodOperacao()) == false)
								OperacaoDelete(myOper, stmt);
						}
					}
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Le
	 * Funcionalidades do Perfil no Sistema
	 * -----------------------------------------------------------
	 */
	public void PerfilGetFuncoes(PerfilBean myPerf) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT o.cod_operacao,o.num_ordem,o.nom_executa,o.sig_funcao,pai.sig_funcao sig_funcao_pai,";
			sCmd += "o.nom_operacao, o.cod_operacao_fk, o.nom_apresentacao,o.nom_descricao,o.sit_biometria ";
			sCmd += " from TCAU_OPERACAO o, TCAU_OPERACAO pai, TCAU_ACESSO acs";
			sCmd += " WHERE o.cod_sistema = '" + myPerf.getCodSistema() + "'";
			sCmd += " and acs.cod_operacao= o.cod_operacao  and acs.cod_perfil = '" + myPerf.getCodPerfil() + "' ";
			sCmd += " and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao  order by o.sig_funcao, o.num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector funcoes = new Vector();
			while (rs.next()) {
				OperacaoBean myOper = new OperacaoBean();
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setSigFuncaoPai(rs.getString("sig_funcao_pai"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));
				myOper.setSitBiometria(rs.getString("sit_biometria"));
				funcoes.addElement(myOper);
			}
			// prepara os niveis
			myPerf.setFuncoes(FuncoesSetNivel(funcoes));
			rs.close();
			stmt.close();
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			myPerf.setMsgErro("Perfil005 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myPerf.setCodPerfil("0");
		} catch (Exception e) {
			myPerf.setMsgErro("Perfil006 - Leitura: " + e.getMessage());
			myPerf.setCodPerfil("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Funcoes em Perfil
	 * -----------------------------------------------------------
	 */
	public boolean PerfilInsertFuncoes(PerfilBean myPerf, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas as opera��es antigas
			Vector funcAtual = myPerf.getFuncoes();
			// iniciar transa��o
			conn.setAutoCommit(false);
			retorno = retorno && PerfilOperacaoDelete(myPerf, stmt);
			for (int i = 0; i < funcAtual.size(); i++) {
				OperacaoBean myOper = (OperacaoBean) funcAtual.elementAt(i);
				retorno = retorno && PerfilOperacaoInsert(myOper, stmt, myPerf.getCodPerfil(), myPerf);
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		myPerf.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * ao UsuarioFuncBean
	 * -----------------------------------------------------------
	 */

	/**
	 * ----------------------------------------------------------- Le
	 * Funcionalidades de um Perfil
	 * -----------------------------------------------------------
	 */
	public boolean UsuarioFuncGetFuncoes(UsuarioFuncBean myUsrFunc) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		// myUsrFunc.setCodPerfil("63");
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT o.cod_operacao,o.num_ordem,o.nom_executa,o.sig_funcao,o.nom_operacao,pai.sig_funcao as sig_funcao_pai,pai.nom_operacao as nom_funcao_pai,";
			sCmd += "o.nom_descricao,o.cod_operacao_fk,o.nom_apresentacao,o.sit_biometria ";
			sCmd += " from TCAU_OPERACAO o, TCAU_ACESSO a, TCAU_OPERACAO pai ";
			sCmd += " WHERE a.cod_perfil = '" + myUsrFunc.getCodPerfil()
					+ "' and  o.cod_operacao = a.cod_operacao and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao and o.ind_ativo <> 'N' "
					+ " and pai.sig_funcao not in ('REG0400','REG0500','REG0600','REG0856','REG0870') order by sig_funcao,num_ordem";

			ResultSet rs = stmt.executeQuery(sCmd);
			myUsrFunc.setMsgErro("");
			Vector funcoes = new Vector();
			while (rs.next()) {
				OperacaoBean myOper = new OperacaoBean();
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setSigFuncaoPai(rs.getString("sig_funcao_pai"));
				myOper.setNomFuncaoPai(rs.getString("nom_funcao_pai"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));

				/*
				 * myOper.setSitBiometria(rs.getString("sit_biometria"));
				 */
				myOper.setSitBiometria(
						UsuarioFuncValidaBiometria(myUsrFunc.getCodOrgaoAtuacao(), myOper.getCodOperacao()));
				funcoes.addElement(myOper);
			}
			// prepara os niveis
			myUsrFunc.setFuncoes(FuncoesSetNivel(funcoes));
			rs.close();
			stmt.close();
			conn.close();
			if (funcoes.size() == 0) {
				myUsrFunc.setMsgErro("Nenhuma funcionalidade para o Usu�rio no sistema " + myUsrFunc.getAbrevSistema());
				myUsrFunc.setCodPerfil("0");
			} else
				existe = true;
		} // fim do try
		catch (SQLException sqle) {
			existe = false;
			myUsrFunc.setCodPerfil("0");
			myUsrFunc.setMsgErro("UsuarioFunc003 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			throw new DaoException("UsuarioFunc003 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
		} catch (Exception e) {
			existe = false;
			myUsrFunc.setCodPerfil("0");
			myUsrFunc.setMsgErro("UsuarioFunc004 - Leitura: " + e.getMessage());
			throw new DaoException("UsuarioFunc004 - Leitura: " + e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Le Perfil do
	 * Usuario -----------------------------------------------------------
	 */
	public boolean UsuarioFuncLePerfilUsuario(UsuarioFuncBean myUsrFunc, UsuarioBean myUsr, String abrevSistema)
			throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT u.cod_Usuario,s.cod_sistema,s.nom_sistema,s.nom_abrev,p.cod_perfil,p.nom_descricao, "
					+ "p.Ind_Vis_Endereco,p.Ind_Vis_2via,p.ind_recebe_notif,p.Ind_Vis_Ar,p.Ind_Vis_Auto,p.Ind_Vis_Foto,"
					+ "p.Ind_Vis_Req,p.Ind_Vis_Hist,p.Ind_Vis_Rej,p.Ind_Vis_Impr,p.Ind_Vis_Todos_Org "
					+ "from TCAU_PERFIL p, TCAU_SISTEMA s ,TCAU_PERFIL_ORGAO u "
					+ "WHERE s.cod_sistema = p.cod_sistema      and " + "      u.cod_perfil  = p.cod_perfil       and "
					+ "      u.cod_orgao_atuacao   = '" + myUsr.getCodOrgaoAtuacao() + "'       and "
					+ "      s.nom_abrev   = '" + abrevSistema + "' and u.cod_usuario=" + myUsr.getCodUsuario();
			ResultSet rs = stmt.executeQuery(sCmd);
			myUsrFunc.setMsgErro("");
			myUsrFunc.setCodPerfil("0");
			while (rs.next()) {
				myUsrFunc.setCodUsuario(rs.getString("cod_usuario"));
				myUsrFunc.setCodSistema(rs.getString("cod_sistema"));
				myUsrFunc.setNomSistema(rs.getString("nom_sistema"));
				myUsrFunc.setAbrevSistema(rs.getString("nom_abrev"));
				myUsrFunc.setCodPerfil(rs.getString("cod_Perfil"));
				myUsrFunc.setNomPerfil(rs.getString("nom_descricao"));
				myUsrFunc.setIndVisEndereco(rs.getString("Ind_Vis_Endereco"));
				myUsrFunc.setIndVis2via(rs.getString("Ind_Vis_2via"));
				myUsrFunc.setIndRecebNot(rs.getString("Ind_recebe_notif"));
				myUsrFunc.setIndVisAr(rs.getString("Ind_Vis_Ar"));
				myUsrFunc.setIndVisAuto(rs.getString("Ind_Vis_Auto"));
				myUsrFunc.setIndVisFoto(rs.getString("Ind_Vis_Foto"));
				myUsrFunc.setIndVisReq(rs.getString("Ind_Vis_Req"));
				myUsrFunc.setIndVisHist(rs.getString("Ind_Vis_Hist"));
				myUsrFunc.setIndVisRej(rs.getString("Ind_Vis_Rej"));
				myUsrFunc.setIndVisImpr(rs.getString("Ind_Vis_Impr"));
				myUsrFunc.setIndVisTodosOrg(rs.getString("Ind_Vis_Todos_Org"));
			}
			sCmd = "SELECT cod_nivel from TCAU_NIVEL_SISTEMA " + "WHERE cod_sistema = '" + myUsrFunc.getCodSistema()
					+ "' and " + " cod_usuario='" + myUsrFunc.getCodUsuario() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myUsrFunc.setCodNivel(rs.getString("cod_nivel"));
			}

			rs.close();
			stmt.close();
			if (myUsrFunc.getCodPerfil().equals("0"))
				myUsrFunc.setMsgErro(
						"Perfil n�o localizado no sistema " + abrevSistema + " Org�o " + myUsr.getSigOrgaoAtuacao());
			else
				existe = true;
			;
		} // fim do try
		catch (SQLException sqle) {
			myUsrFunc.setCodPerfil("0");
			myUsrFunc.setMsgErro("UsuarioFunc001 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			existe = false;
			throw new DaoException("UsuarioFunc001 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
		} catch (Exception e) {
			existe = false;
			myUsrFunc.setCodPerfil("0");
			myUsrFunc.setMsgErro("UsuarioFunc002 - Leitura: " + e.getMessage());
			throw new DaoException("UsuarioFunc002 - Leitura:" + e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Le Perfis
	 * -----------------------------------------------------------
	 */
	public String getPerfis() throws DaoException {
		String myPerfs = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "Select cod_sistema,cod_perfil,nom_descricao from TCAU_PERFIL order by nom_descricao";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myPerfs += ",\"" + rs.getString("cod_sistema") + "\"";
				myPerfs += ",\"" + rs.getString("cod_perfil") + "\"";
				myPerfs += ",\"" + rs.getString("nom_descricao") + "\"";
			}
			myPerfs = myPerfs.substring(1, myPerfs.length());
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myPerfs;
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * ao OperacaoBean
	 * -----------------------------------------------------------
	 */
	/**
	 * ----------------------------------------------------------- Le Sistema e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean OperacaoLeBean(OperacaoBean myOper, int tp) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			if (tp == 0)
				sCmd = "SELECT * from TCAU_OPERACAO WHERE cod_operacao='" + myOper.getCodOperacao() + "'";
			else
				sCmd = "SELECT * from TCAU_OPERACAO WHERE cod_sistema='" + myOper.getCodSistema() + "' and sig_funcao='"
						+ myOper.getSigFuncao() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				retorno = true;
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));
				myOper.setPkid(myOper.getCodOperacao());
				myOper.setSitBiometria(rs.getString("sit_biometria"));
			}
			rs.close();
			stmt.close();
		} // fim do try
		catch (SQLException sqle) {
			myOper.setCodSistema("0");
			myOper.setPkid("0");
			myOper.setCodOperacao("0");
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			myOper.setCodSistema("0");
			myOper.setPkid("0");
			myOper.setCodOperacao("0");
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Operacao -----------------------------------------------------------
	 */
	public void OperacaoInsert(OperacaoBean myOperacao, Statement stmt) throws DaoException {
		try {
			String codOper = myOperacao.getCodOperacao();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codOper)) || ("0".equals(codOper))) {
				sCmd = "INSERT INTO TCAU_OPERACAO (cod_Operacao,";
				sCmd += "num_Ordem,sig_Funcao,nom_Operacao,nom_Descricao,";
				if (("".equals(myOperacao.getCodOperacaoFk()) == false)
						&& ("0".equals(myOperacao.getCodOperacaoFk()) == false))
					sCmd += "cod_Operacao_fk,";
				sCmd += "nom_Apresentacao,nom_Executa,cod_sistema,sit_biometria) ";
				sCmd += "VALUES (seq_tcau_operacao.nextval,";
				sCmd += "'" + myOperacao.getNumOrdem() + "',";
				sCmd += "'" + myOperacao.getSigFuncao() + "',";
				sCmd += "'" + myOperacao.getNomOperacao() + "',";
				sCmd += "'" + myOperacao.getNomDescricao() + "',";
				if (("".equals(myOperacao.getCodOperacaoFk()) == false)
						&& ("0".equals(myOperacao.getCodOperacaoFk()) == false))
					sCmd += "'" + myOperacao.getCodOperacaoFk() + "',";
				sCmd += "'" + myOperacao.getNomApresentacao() + "',";
				sCmd += "'" + myOperacao.getNomExecuta() + "', ";
				sCmd += "'" + myOperacao.getCodSistema() + "', ";
				sCmd += "'" + myOperacao.getSitBiometria() + "')";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TCAU_OPERACAO set ";
				sCmd += "sig_Funcao      ='" + myOperacao.getSigFuncao() + "',";
				sCmd += "num_Ordem       ='" + myOperacao.getNumOrdem() + "',";
				sCmd += "nom_Operacao    ='" + myOperacao.getNomOperacao() + "',";
				sCmd += "nom_Descricao   ='" + myOperacao.getNomDescricao() + "',";
				sCmd += "cod_Operacao_fk ='" + myOperacao.getCodOperacaoFk() + "',";
				// if (("".equals(myOperacao.getCodOperacaoFk())==false) &&
				// ("0".equals(myOperacao.getCodOperacaoFk())==false)) sCmd +=
				// "cod_Operacao_fk ='"+myOperacao.getCodOperacaoFk()+"'," ;
				sCmd += "nom_Apresentacao='" + myOperacao.getNomApresentacao() + "',";
				sCmd += "nom_Executa     ='" + myOperacao.getNomExecuta() + "', ";
				sCmd += "sit_biometria   ='" + myOperacao.getSitBiometria() + "' where cod_operacao = '" + codOper
						+ "' ";
				stmt.execute(sCmd);
			}
			sCmd = "SELECT COD_OPERACAO FROM TCAU_OPERACAO ";
			sCmd += "WHERE sig_Funcao='" + myOperacao.getSigFuncao() + "' and cod_sistema='"
					+ myOperacao.getCodSistema() + "' ";
			rs = stmt.executeQuery(sCmd);
			stmt.close();
			while (rs.next()) {
				myOperacao.setCodOperacao(rs.getString("cod_Operacao"));
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * Operacao -----------------------------------------------------------
	 */
	public void OperacaoDelete(OperacaoBean myOperacao, Statement stmt) throws DaoException {
		try {
			String sCmd = "DELETE TCAU_OPERACAO where cod_operacao = '" + myOperacao.getCodOperacao() + "'";
			stmt.execute(sCmd);
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Operacao em Acesso
	 * -----------------------------------------------------------
	 */
	public boolean PerfilOperacaoInsert(OperacaoBean myOperacao, Statement stmt, String codPerfil, PerfilBean myPerf)
			throws DaoException {
		boolean retorno = false;
		try {
			String sCmd = "INSERT INTO TCAU_ACESSO (cod_Operacao,cod_Acesso,cod_perfil,nom_username_oper,dat_proc_oper,orgao_lotacao_oper) ";
			sCmd += "VALUES (";
			sCmd += "'" + myOperacao.getCodOperacao() + "',seq_tcau_acesso.nextval, ";
			sCmd += "'" + codPerfil + "','";
			sCmd += myPerf.getNomUserNameOper() + "',to_date('" + sys.Util.formatedToday().substring(0, 10)
					+ "','dd/mm/yyyy')," + myPerf.getOrgaoLotacaoOper() + ") ";

			stmt.execute(sCmd);
			retorno = true;
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Delete
	 * Operacaoem Acesso
	 * -----------------------------------------------------------
	 */
	public boolean PerfilOperacaoDelete(PerfilBean myPerf, Statement stmt) throws DaoException {
		boolean retorno = false;
		try {
			String sCmd = "UPDATE TCAU_ACESSO set NOM_USERNAME_OPER = '" + myPerf.getNomUserNameOper() + "',"
					+ "ORGAO_LOTACAO_OPER = " + myPerf.getOrgaoLotacaoOper() + "," + "DAT_PROC_OPER = to_date('"
					+ sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy')" + " " + "where cod_perfil = '"
					+ myPerf.getCodPerfil() + "'";
			stmt.execute(sCmd);

			sCmd = "DELETE TCAU_ACESSO where cod_perfil = '" + myPerf.getCodPerfil() + "'";
			stmt.execute(sCmd);
			retorno = true;
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Le Perfil e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean PerfilLeBean(PerfilBean myPer, int tpPer) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT nom_Descricao,cod_Sistema from TCAU_Perfil ";
			sCmd += "WHERE cod_Perfil = '" + myPer.getCodPerfil() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myPer.setMsgErro("");
			while (rs.next()) {
				myPer.setNomDescricao(rs.getString("nom_Descricao"));
				myPer.setCodSistema(rs.getString("cod_Sistema"));
				myPer.setPkid(myPer.getCodPerfil());
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			myPer.setCodPerfil("0");
			myPer.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myPer.setCodPerfil("0");
			myPer.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- DAO -
	 * Utilitarios -----------------------------------------------------------
	 */

	/**
	 * ----------------------------------------------------------- Monta Where
	 * para Consulta generica recebendo String
	 * -----------------------------------------------------------
	 */
	private static String montaWhereTexto(String objTexto, String nomVariavel, String nomExterno, Vector vParam) {
		String wWhere = "";
		if (objTexto.length() > 0) {
			wWhere = " and " + nomVariavel + " like '%" + objTexto + "%'";
			vParam.addElement(nomExterno + " igual(is) a '" + objTexto + "' <br> ");
		}
		return wWhere;
	}

	/**
	 * --------------------------------------------------------------- Monta
	 * Where para Consulta generica recebendo Datas (em String)
	 * ---------------------------------------------------------------
	 */
	private static String montaWhereDt(String objDt, String nomVariavel, String nomExterno, Vector vParam) {
		String wWhere = "";
		if (objDt.length() > 0) {
			wWhere = " and " + nomVariavel + " = " + "to_date(" + objDt + " ,'dd/mm/yyyy')";
			vParam.addElement(nomExterno + " igual(is) a '" + objDt + "' <br> ");
		}
		return wWhere;
	}

	/**
	 * ----------------------------------------------------------------- Monta
	 * Where para Consulta generica recebendo Numeros (em String)
	 * -----------------------------------------------------------------
	 */
	private static String montaWhereNum(String objNum, String nomVariavel, String nomExterno, Vector vParam) {
		String wWhere = "";
		if (objNum.length() > 0) {
			String numsed = "";
			String mcmd = "";
			char ch;
			boolean flagprimNum = false;
			for (int i = 0; i < objNum.length(); i++) {
				ch = objNum.charAt(i);
				if ((flagprimNum == false) && (ch == '=' || ch == '<' || ch == '>'))
					mcmd += ch;
				if (ch < '0' || ch > '9')
					continue;
				flagprimNum = true;
				numsed += ch;
			}
			if (mcmd == "")
				mcmd = "=";
			wWhere = " and " + nomVariavel + " " + mcmd + " " + numsed;
			vParam.addElement(nomExterno + " " + objNum + " <br> ");
		}
		return wWhere;
	}

	/**
	 * ----------------------------------------------------------- Consulta
	 * Orgao por codigo ou nomOrgao
	 * -----------------------------------------------------------
	 */
	public boolean OrgaoExiste(OrgaoBean myOrg, int tpPesq) throws DaoException {
		boolean existe = false;
		boolean eConnLocal = false;

		// synchronized
		Connection conn = null;
		try {
			conn = myOrg.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST);
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE ";
			if (tpPesq == 0)
				where += " cod_Orgao='" + myOrg.getCodOrgao() + "' ";
			else
				where += " sig_orgao = '" + myOrg.getSigOrgao() + "' ";

			String sCmd = "SELECT COD_ORGAO from TSMI_ORGAO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				if (eConnLocal)
					try {
						serviceloc.setReleaseConnection(conn);
					} catch (Exception ey) {
					}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Consulta
	 * Protocolo Upo por codigo ou nomProtocoloUpo
	 * -----------------------------------------------------------
	 */
	public boolean protocoloUpoExiste(ProtocoloUpoBean myProtUpo, int tpPesq) throws DaoException {
		boolean existe = false;
		boolean eConnLocal = false;

		// synchronized
		Connection conn = null;
		try {
			conn = myProtUpo.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST);
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE ";
			if (tpPesq == 0)
				where += " cod_prot_upo='" + myProtUpo.getCodProtocoloUpo() + "' ";
			else
				where += " NOM_PROT_UPO = '" + myProtUpo.getNomProtocoloUpo() + "' ";

			String sCmd = "SELECT cod_prot_upo from SMIT.TSMI_PROT_UPO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				if (eConnLocal)
					try {
						serviceloc.setReleaseConnection(conn);
					} catch (Exception ey) {
					}
			}
		}
		return existe;
	}

	public String getMensagensSistemas() throws DaoException {
		String myPerfs = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_SISTEMA,COD_MENSAGEM_SISTEMA,DSC_MENSAGEM,DSC_MOTIVO,DSC_ACAO,COD_TIP_MENSAGEM,COD_MENSAGEM_INTERNO";
			sCmd += "FROM TCAU_MENSAGEM_SISTEMA";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				myPerfs += ",\"" + rs.getString("COD_SISTEMA") + "\"";
				myPerfs += ",\"" + rs.getString("COD_MENSAGEM_SISTEMA") + "\"";
				myPerfs += ",\"" + rs.getString("DSC_MENSAGEM") + "\"";
				myPerfs += ",\"" + rs.getString("DSC_MOTIVO") + "\"";
				myPerfs += ",\"" + rs.getString("DSC_ACAO") + "\"";
				myPerfs += ",\"" + rs.getString("COD_TIP_MENSAGEM") + "\"";
				myPerfs += ",\"" + rs.getString("COD_MENSAGEM_INTERNO") + "\"";
			}
			myPerfs = myPerfs.substring(1, myPerfs.length());
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myPerfs;
	}

	public boolean MensagemInsertSistemas(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas as opera��es antigas
			Vector funcAtual = mySist.getMensagens();

			String codPerf = "0";
			String sDesc = "";
			// iniciar transa��o
			conn.setAutoCommit(false);

			for (int i = 0; i < funcAtual.size(); i++) {
				MensagemSistemaBean myMsgSis = (MensagemSistemaBean) funcAtual.elementAt(i);
				if ("".equals(myMsgSis.getDscMensagem()) == false)
					MensagemSistemaInsert(myMsgSis, stmt);
				else if ("".equals(myMsgSis.getCodMensagemSistema()) == false)
					MensagemSistemaDelete(myMsgSis, stmt);
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		}

		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	public void MensagemSistemaInsert(MensagemSistemaBean myMsgSist, Statement stmt) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			String sCmd = "SELECT COD_MENSAGEM_SISTEMA FROM TCAU_MENSAGEM_SISTEMA ";
			sCmd += "WHERE (DSC_MENSAGEM = '" + myMsgSist.getDscMensagem() + "' OR COD_MENSAGEM_SISTEMA = '"
					+ myMsgSist.getCodMensagemSistema() + "')";
			sCmd += "AND COD_SISTEMA = '" + myMsgSist.getCodSistema() + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			String codmsg = "";
			while (rs.next()) {
				codmsg = rs.getString("COD_MENSAGEM_SISTEMA");
			}

			if (("".equals(codmsg)) || ("0".equals(codmsg))) {
				sCmd = "INSERT INTO TCAU_MENSAGEM_SISTEMA (COD_MENSAGEM_SISTEMA,";
				sCmd += "DSC_MENSAGEM,DSC_MOTIVO,DSC_ACAO,COD_SISTEMA,COD_TIP_MENSAGEM,COD_MENSAGEM_INTERNO) ";
				sCmd += "VALUES (SEQ_TCAU_MENSAGEM_SISTEMA.nextval,";
				sCmd += "'" + myMsgSist.getDscMensagem() + "',";
				sCmd += "'" + myMsgSist.getDscMotivo() + "',";
				sCmd += "'" + myMsgSist.getDscAcao() + "',";
				sCmd += "'" + myMsgSist.getCodSistema() + "',";
				sCmd += "'" + myMsgSist.getCodTipMensagem() + "',";
				sCmd += "'" + myMsgSist.getCodMensagemInterno() + "') ";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TCAU_MENSAGEM_SISTEMA set ";
				sCmd += "DSC_MENSAGEM = '" + myMsgSist.getDscMensagem() + "',";
				sCmd += "DSC_MOTIVO = '" + myMsgSist.getDscMotivo() + "',";
				sCmd += "DSC_ACAO = '" + myMsgSist.getDscAcao() + "',";
				sCmd += "COD_SISTEMA = '" + myMsgSist.getCodSistema() + "',";
				sCmd += "COD_TIP_MENSAGEM = '" + myMsgSist.getCodTipMensagem() + "',";
				sCmd += "COD_MENSAGEM_INTERNO = '" + myMsgSist.getCodMensagemInterno() + "' ";
				sCmd += "WHERE COD_MENSAGEM_SISTEMA = '" + codmsg + "'";
				stmt.execute(sCmd);
			}
			sCmd = "SELECT COD_MENSAGEM_SISTEMA FROM TCAU_MENSAGEM_SISTEMA ";
			sCmd += "WHERE DSC_MENSAGEM ='" + myMsgSist.getDscMensagem() + "' and COD_SISTEMA='"
					+ myMsgSist.getCodSistema() + "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myMsgSist.setCodMensagemSistema(rs.getString("COD_MENSAGEM_SISTEMA"));
			}
			;

			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public void MensagemSistemaDelete(MensagemSistemaBean myMsgSist, Statement stmt) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			String sCmd = "DELETE TCAU_MENSAGEM_SISTEMA where COD_MENSAGEM_SISTEMA = '"
					+ myMsgSist.getCodMensagemSistema() + "'";
			stmt.execute(sCmd);

			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	// -----------
	public void MensagemGetSistemas(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT p.COD_MENSAGEM_SISTEMA, p.DSC_MENSAGEM, p.DSC_MOTIVO, p.DSC_ACAO, p.COD_TIP_MENSAGEM, p.COD_MENSAGEM_INTERNO";
			sCmd += " FROM TCAU_MENSAGEM_SISTEMA p";
			sCmd += " WHERE p.COD_SISTEMA = '" + mySist.getCodSistema() + "'";
			sCmd += " order by p.DSC_MENSAGEM";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			Vector mensagens = new Vector();
			while (rs.next()) {
				MensagemSistemaBean myPerf = new MensagemSistemaBean();

				myPerf.setCodMensagemSistema(rs.getString("cod_mensagem_sistema"));
				myPerf.setDscMensagem(rs.getString("dsc_mensagem"));
				myPerf.setDscMotivo(rs.getString("dsc_motivo"));
				myPerf.setDscAcao(rs.getString("dsc_acao"));
				myPerf.setCodTipMensagem(rs.getString("cod_tip_Mensagem"));
				myPerf.setCodMensagemInterno(rs.getString("cod_mensagem_interno"));

				mensagens.addElement(myPerf);
			}

			mySist.setMensagens(mensagens);

			rs.close();
			stmt.close();
		} // fim do try
		catch (SQLException sqle) {
			mySist.setMsgErro("Sistema008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("0");
		} catch (Exception e) {
			mySist.setMsgErro("Sistema008 - Leitura: " + e.getMessage());
			mySist.setCodSistema("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Orgao -----------------------------------------------------------
	 */
	public boolean OrgaoInsert(OrgaoBean myOrg, Vector vErro) throws DaoException {
		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;
		try {
			conn = myOrg.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			conn.setAutoCommit(false);
			if (("".equals(myOrg.getEndereco().getNumEndereco()) == false)
					|| ("".equals(myOrg.getEndereco().getTxtComplemento()) == false)
					|| ("".equals(myOrg.getEndereco().getNomBairro()) == false)
					|| ("".equals(myOrg.getEndereco().getCodCidade()) == false)
					|| ("".equals(myOrg.getEndereco().getNumCEP()) == false)
					|| ("".equals(myOrg.getEndereco().getCodUF()) == false)
					|| ("".equals(myOrg.getEndereco().getTxtEmail()) == false)) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.EnderecoInsert(myOrg.getEndereco(), conn);
			}
			if (("".equals(myOrg.getTelefone().getCodDDD()) == false)
					|| ("".equals(myOrg.getTelefone().getNumTelComercial()) == false)
					|| ("".equals(myOrg.getTelefone().getNumTelResidencial()) == false)
					|| ("".equals(myOrg.getTelefone().getNumTelFax()) == false)
					|| ("".equals(myOrg.getTelefone().getNumTelMovel()) == false)) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.TelefoneInsert(myOrg.getTelefone(), conn);
			}

			Statement stmt = conn.createStatement();
			String sCmd = "select seq_tsmi_Orgao.nextval Seq_Orgao from dual";
			myOrg.setPkid(myOrg.getCodOrgao());

			sCmd = "INSERT INTO TSMI_ORGAO (cod_orgao,nom_orgao,sig_orgao,";
			sCmd += "dat_Cadastro,nom_Contato,seq_endereco,seq_Telefone,ind_emite_vex,";
			sCmd += "ind_integrada,txt_email, ind_autua, ind_smitdol) ";
			sCmd += "VALUES (" + myOrg.getCodOrgao() + ",";
			sCmd += "'" + myOrg.getNomOrgao() + "',";
			sCmd += "'" + myOrg.getSigOrgao() + "',";
			sCmd += "to_date('" + myOrg.getDatCadastro().substring(0, 10) + "','dd/mm/yyyy'),";
			sCmd += "'" + myOrg.getNomContato() + "','" + myOrg.getEndereco().getSeqEndereco() + "','"
					+ myOrg.getTelefone().getSeqTelefone() + "',";
			sCmd += "'" + myOrg.getVex() + "',";
			sCmd += "'" + myOrg.getIntegrada() + "','" + myOrg.getTxtEmail() + "',";
			sCmd += "'" + myOrg.getIndAutua() + "',";
			sCmd += "'" + myOrg.getIndSmitDol() + "')";
			stmt.execute(sCmd);

			stmt.close();

			DaoBroker broker = new DaoBroker();
			sCmd = broker.Transacao083(myOrg, "1", "000000");
			if (!sCmd.substring(0, 3).equals("000")) {
				vErro.addElement(broker.vErroBroker(conn, sCmd));
				conn.rollback();
			}
			conn.commit();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Insert em
	 * Protocolo UPO -----------------------------------------------------------
	 */
	public boolean ProtocoloUpoInsert(ProtocoloUpoBean myProtUpo, Vector vErro) throws DaoException {
		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;
		try {
			conn = myProtUpo.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			conn.setAutoCommit(false);

			Statement stmt = conn.createStatement();
			myProtUpo.setPkid(myProtUpo.getCodProtocoloUpo());

			String sCmd = "INSERT INTO SMIT.TSMI_PROT_UPO (COD_PROT_UPO,NOM_PROT_UPO) ";
			sCmd += "VALUES (SMIT.SEQ_TSMI_PROT_UPO.nextval,";
			sCmd += "'" + myProtUpo.getNomProtocoloUpo() + "')";
			stmt.execute(sCmd);

			stmt.close();

			// DaoBroker broker = new DaoBroker();
			// sCmd = broker.Transacao083(myProtUpo,"1","000000");
			// if( !sCmd.substring(0,3).equals("000") ){
			// vErro.addElement(broker.vErroBroker(conn,sCmd));
			// conn.rollback();
			// }
			conn.commit();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Update em
	 * Orgao -----------------------------------------------------------
	 */
	public boolean OrgaoUpdate(OrgaoBean myOrg, Vector vErro) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		Statement stmt = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			if ("".equals(myOrg.getEndereco().getSeqEndereco()) == false) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.EnderecoUpdate(myOrg.getEndereco(), conn);
			} else {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.EnderecoInsert(myOrg.getEndereco(), conn);
			}
			if ("".equals(myOrg.getTelefone().getSeqTelefone()) == false) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.TelefoneUpdate(myOrg.getTelefone(), conn);
			} else {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.TelefoneInsert(myOrg.getTelefone(), conn);
			}
			stmt = conn.createStatement();
			sCmd = "UPDATE TSMI_ORGAO set " + "nom_Orgao= '" + myOrg.getNomOrgao() + "'," + "sig_Orgao= '"
					+ myOrg.getSigOrgao() + "'," + "seq_telefone= '" + myOrg.getTelefone().getSeqTelefone() + "',"
					+ "seq_endereco= '" + myOrg.getEndereco().getSeqEndereco() + "', " + "nom_contato= '"
					+ myOrg.getNomContato() + "', " + "ind_emite_vex= '" + myOrg.getVex() + "', " + "ind_integrada= '"
					+ myOrg.getIntegrada() + "', " + "txt_email = '" + myOrg.getTxtEmail() + "', " + "ind_autua = '"
					+ myOrg.getIndAutua() + "', " + "ind_smitdol = '" + myOrg.getIndSmitDol() + "' "
					+ " where cod_Orgao='" + myOrg.getCodOrgao() + "'";
			stmt.execute(sCmd);

			stmt.close();
			DaoBroker broker = new DaoBroker();
			sCmd = broker.Transacao083(myOrg, "2", "000000");
			if (!sCmd.substring(0, 3).equals("000")) {
				vErro.addElement(broker.vErroBroker(conn, sCmd));
				conn.rollback();
			}
			conn.commit();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Update em
	 * Protocolo UPO -----------------------------------------------------------
	 */
	public boolean ProtocoloUpoUpdate(ProtocoloUpoBean myProtUpo, Vector vErro) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		Statement stmt = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);

			stmt = conn.createStatement();
			sCmd = "UPDATE SMIT.TSMI_PROT_UPO set " + "nom_prot_upo= '" + myProtUpo.getNomProtocoloUpo() + "'"
					+ " where cod_prot_upo ='" + myProtUpo.getCodProtocoloUpo() + "'";
			stmt.execute(sCmd);

			stmt.close();
			DaoBroker broker = new DaoBroker();
			// sCmd = broker.Transacao083(myProtUpo,"2","000000");
			// if( !sCmd.substring(0,3).equals("000") ){
			// vErro.addElement(broker.vErroBroker(conn,sCmd));
			// conn.rollback();
			// }
			conn.commit();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * ORGAO -----------------------------------------------------------
	 */
	public boolean OrgaoDelete(OrgaoBean myOrg, Vector vErro) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		Statement stmt = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);

			stmt = conn.createStatement();
			sCmd = "DELETE TSMI_ORGAO where cod_Orgao=" + myOrg.getCodOrgao();
			stmt.execute(sCmd);
			stmt.close();
			if ("".equals(myOrg.getEndereco().getSeqEndereco()) == false) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.EnderecoDelete(myOrg.getEndereco(), conn);
			}
			if ("".equals(myOrg.getTelefone().getSeqTelefone()) == false) {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.TelefoneDelete(myOrg.getTelefone(), conn);
			}
			existe = true;
			DaoBroker broker = new DaoBroker();
			sCmd = broker.Transacao083(myOrg, "3", "000000");
			if (!sCmd.substring(0, 3).equals("000")) {
				vErro.addElement(broker.vErroBroker(conn, sCmd));
				conn.rollback();
			}
			conn.commit();
			myOrg.setPkid("0");
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * ORGAO -----------------------------------------------------------
	 */
	public boolean ProtocoloUpoDelete(ProtocoloUpoBean myProtUpo, Vector vErro) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		Statement stmt = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);

			stmt = conn.createStatement();
			sCmd = "DELETE SMIT.TSMI_PROT_UPO where cod_prot_upo=" + myProtUpo.getCodProtocoloUpo();
			stmt.execute(sCmd);
			stmt.close();

			existe = true;
			// DaoBroker broker = new DaoBroker();
			// sCmd = broker.Transacao083(myProtUpo,"3","000000");
			// if( !sCmd.substring(0,3).equals("000") ){
			// vErro.addElement(broker.vErroBroker(conn,sCmd));
			// conn.rollback();
			// }
			conn.commit();
			myProtUpo.setPkid("0");
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * M�todo respons�vel pelas opera��es de inclus�o, altera��o e exclus�o na
	 * base de dados via Broker
	 * 
	 * @param myOrg
	 * @param codacao
	 * @throws DaoException
	 */

	public void OrgaoBroker(OrgaoBean myOrg, String codacao) throws DaoException {
		// Declara��o de variaveis
		boolean eConnLocal = false;
		Connection conn = null;

		String codEvento = "083";
		String indContinua = "      ";

		String parteVar = myOrg.getCodOrgao() + codacao + indContinua + sys.Util.rPad(myOrg.getNomOrgao(), " ", 30)
				+ sys.Util.rPad(myOrg.getEndereco().getTxtEndereco(), " ", 27)
				+ sys.Util.rPad(myOrg.getEndereco().getNumEndereco(), " ", 5)
				+ sys.Util.rPad(myOrg.getEndereco().getTxtComplemento(), " ", 11)
				+ sys.Util.rPad(myOrg.getEndereco().getNomBairro(), " ", 20)
				+ sys.Util.lPad(myOrg.getEndereco().getNumCEP(), "0", 8)
				+ sys.Util.lPad(myOrg.getEndereco().getNomCidade(), "0", 4)
				+ sys.Util.rPad(myOrg.getTelefone().getCodDDD(), " ", 3)
				+ sys.Util.rPad(myOrg.getTelefone().getNumTel(), " ", 15)
				+ sys.Util.rPad(myOrg.getTelefone().getNumTelFax(), " ", 15)
				+ sys.Util.rPad(myOrg.getSigOrgao(), " ", 10) + sys.Util.rPad(myOrg.getNomContato(), " ", 45);

		try {
			conn = myOrg.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();

			String sCmd = "INSERT INTO TSMI_BRKR_TRANS_PENDENTE (cod_brkr_trans_pendente,";
			sCmd += "txt_parte_variavel, dat_proc, cod_evento)";
			sCmd += "VALUES(SEQ_TSMI_BRKR_TRANS_PENDENTE.NEXTVAL ,'" + parteVar + "',";
			sCmd += "sysdate, " + codEvento + ")";

			stmt.execute(sCmd);
			stmt.close();
			conn.commit();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- Le Orgaos
	 * -----------------------------------------------------------
	 */
	public String getOrgaos(UsuarioBean myUsr, String paraSQL) throws DaoException {
		String myOrgs = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			int admACSS = 0;
			String sCmd = "Select ps.cod_nivel from TCAU_SISTEMA s, TCAU_NIVEL_SISTEMA ps where ps.cod_usuario='"
					+ myUsr.getCodUsuario()
					+ "' and ps.cod_nivel<>0 and ps.cod_sistema=s.cod_sistema and s.nom_abrev='ACSS' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				admACSS = rs.getInt("cod_nivel");
			}
			if (admACSS == 0) {
				sCmd = "select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o where o.cod_orgao='"
						+ myUsr.getCodOrgaoAtuacao() + "' ";
			} else {
				sCmd = "Select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o";
			}
			sCmd += " order by o.sig_orgao";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				if ("SQL".equals(paraSQL)) {
					myOrgs += "," + rs.getString("cod_orgao") + "";
				} else {
					myOrgs += ",\"" + rs.getString("cod_orgao") + "\"";
					myOrgs += ",\"" + rs.getString("sig_orgao") + "\"";
				}
			}
			myOrgs = myOrgs.substring(1, myOrgs.length());
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myOrgs;
	}

	/**
	 * ----------------------------------------------------------- Le Protocolos
	 * UPO -----------------------------------------------------------
	 */
	public String getProtocolosUpo(UsuarioBean myUsr, String paraSQL) throws DaoException {
		String myProtUpo = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "select COD_PROT_UPO,NOM_PROT_UPO from SMIT.TSMI_PROT_UPO order by NOM_PROT_UPO";

			ResultSet rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				if ("SQL".equals(paraSQL)) {
					myProtUpo += "," + rs.getString("COD_PROT_UPO") + "";
				} else {
					myProtUpo += ",\"" + rs.getString("COD_PROT_UPO") + "\"";
					myProtUpo += ",\"" + rs.getString("NOM_PROT_UPO") + "\"";
				}
			}

			myProtUpo = myProtUpo.substring(1, myProtUpo.length());

			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myProtUpo;
	}

	public String getOrgaosAtuacao(UsuarioBean myUsr, ACSS.ParamSistemaBean myParamSistema) throws DaoException {
		String myOrgs = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT DISTINCT(T3.COD_ORGAO) AS COD_ORGAO,T3.SIG_ORGAO FROM "
					+ "TCAU_PERFIL_ORGAO T1,TCAU_PERFIL T2,TSMI_ORGAO T3 " + "WHERE "
					+ "T1.COD_PERFIL=T2.COD_PERFIL AND " + "T1.COD_ORGAO_ATUACAO=T3.COD_ORGAO AND " + "T2.COD_SISTEMA='"
					+ myParamSistema.getCodSistema() + "' AND " + "T1.COD_USUARIO='" + myUsr.getCodUsuario() + "' "
					+ "ORDER BY T3.SIG_ORGAO";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myOrgs += ",\"" + rs.getString("cod_orgao") + "\"";
				myOrgs += ",\"" + rs.getString("sig_orgao") + "\"";
			}
			myOrgs = myOrgs.substring(1, myOrgs.length());
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myOrgs;
	}

	public String getOrgaosACSS(UsuarioBean myUsr, String paraSQL) throws DaoException {
		String myOrgs = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			int admACSS = 0;
			String sCmd = "Select ps.cod_nivel from TCAU_SISTEMA s, TCAU_NIVEL_SISTEMA ps where ps.cod_usuario='"
					+ myUsr.getCodUsuario()
					+ "' and ps.cod_nivel<>0 and ps.cod_sistema=s.cod_sistema and s.nom_abrev='ACSS' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				admACSS = rs.getInt("cod_nivel");
			}
			if (admACSS == 0 || admACSS == 1) {
				sCmd = "select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o where o.cod_orgao='"
						+ myUsr.getCodOrgaoAtuacao() + "' ";
			} else {
				sCmd = "Select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o";
			}
			sCmd += " order by o.sig_orgao";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				if ("SQL".equals(paraSQL)) {
					myOrgs += "," + rs.getString("cod_orgao") + "";
				} else {
					myOrgs += ",\"" + rs.getString("cod_orgao") + "\"";
					myOrgs += ",\"" + rs.getString("sig_orgao") + "\"";
				}
			}
			myOrgs = myOrgs.substring(1, myOrgs.length());
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return myOrgs;
	}

	/**
	 * ----------------------------------------------------------- Consulta se
	 * Existe outro ORGAO com esta Sigla
	 * -----------------------------------------------------------
	 */
	public boolean OrgaoExisteOutro(OrgaoBean myOrg) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String where = "";
			if ((myOrg.getCodOrgao() != null) && (myOrg.getSigOrgao() != null)) {
				where = " WHERE cod_Orgao <> '" + myOrg.getCodOrgao() + "' and ";
				where += " sig_Orgao = '" + myOrg.getSigOrgao() + "'";
			}
			String sCmd = "SELECT COD_Orgao from TSMI_ORGAO " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Le Orgao e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean OrgaoLeBean(OrgaoBean myOrg, int tpUsr) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		String sCmd = null;
		try {

			/* Testar C�digo */
			if (tpUsr != 1) {
				try {
					Integer.parseInt(myOrg.getCodOrgao());
				} catch (Exception e) {
					myOrg.setCodOrgao("");
					myOrg.setPkid("0");
				}
			}

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT sig_Orgao,cod_Orgao,nom_Orgao,to_char(dat_Cadastro,'dd/mm/yyyy') dat_Cadastro,";
			sCmd += "seq_Endereco,seq_Telefone,nom_Contato,ind_emite_vex,ind_integrada,txt_email,ind_autua, ind_smitdol from TSMI_ORGAO";
			if (tpUsr == 1)
				sCmd += " WHERE sig_Orgao = '" + myOrg.getSigOrgao() + "'";
			else
				sCmd += " WHERE cod_Orgao = '" + myOrg.getCodOrgao() + "'";

			// Limpo o conte�do dos Beans de Endereco e Telefone
			ResultSet rs = stmt.executeQuery(sCmd);
			myOrg.setMsgErro("");
			while (rs.next()) {
				// Carrega os beans com os dados vindos da pesquisa de Orgao
				myOrg.setSigOrgao(rs.getString("sig_Orgao"));
				myOrg.setCodOrgao(rs.getString("cod_Orgao"));
				myOrg.setPkid(myOrg.getCodOrgao());
				myOrg.setNomOrgao(rs.getString("nom_Orgao"));
				myOrg.setDatCadastro(rs.getString("dat_Cadastro"));
				myOrg.setNomContato(rs.getString("nom_Contato"));
				myOrg.setVex(rs.getString("ind_emite_vex"));
				myOrg.setIntegrada(rs.getString("ind_integrada"));
				myOrg.getEndereco().setSeqEndereco(rs.getString("seq_Endereco"));
				myOrg.getTelefone().setSeqTelefone(rs.getString("seq_Telefone"));
				myOrg.setTxtEmail(rs.getString("txt_email"));
				myOrg.setIndAutua(rs.getString("ind_autua"));
				myOrg.setIndSmitDol(rs.getString("ind_smitdol"));
			}
			rs.close();
			stmt.close();
			if ("".equals(myOrg.getEndereco().getSeqEndereco()))
				myOrg.setEndereco(new sys.EnderecoBean());
			else {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.EnderecoLeBean(myOrg.getEndereco(), conn);
			}
			if ("".equals(myOrg.getTelefone().getSeqTelefone()))
				myOrg.setTelefone(new sys.TelefoneBean());
			else {
				sys.Dao sysDao = sys.Dao.getInstance();
				sysDao.TelefoneLeBean(myOrg.getTelefone(), conn);
			}
		} catch (SQLException e) {
			bOk = false;
			myOrg.setCodOrgao("");
			myOrg.setPkid("0");
			System.out.println("* SQL Exception : " + sCmd);
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myOrg.setCodOrgao("");
			myOrg.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Le Protocolo
	 * UPO e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean ProtocoloUpoLeBean(ProtocoloUpoBean myProtUpo, int tpUsr) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		String sCmd = null;
		try {

			/* Testar C�digo */
			if (tpUsr != 1) {
				try {
					Integer.parseInt(myProtUpo.getCodProtocoloUpo());
				} catch (Exception e) {
					myProtUpo.setCodProtocoloUpo("");
					myProtUpo.setPkid("0");
				}
			}

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "select COD_PROT_UPO,NOM_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO = '"
					+ myProtUpo.getCodProtocoloUpo() + "'";

			// Limpo o conte�do dos Beans de Endereco e Telefone
			ResultSet rs = stmt.executeQuery(sCmd);
			myProtUpo.setMsgErro("");

			while (rs.next()) {
				// Carrega os beans com os dados vindos da pesquisa de Orgao

				myProtUpo.setCodProtocoloUpo(rs.getString("COD_PROT_UPO"));
				myProtUpo.setPkid(myProtUpo.getCodProtocoloUpo());
				myProtUpo.setNomProtocoloUpo((rs.getString("NOM_PROT_UPO")));

			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			bOk = false;
			myProtUpo.setCodProtocoloUpo("");
			myProtUpo.setPkid("0");
			System.out.println("* SQL Exception : " + sCmd);
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myProtUpo.setCodProtocoloUpo("");
			myProtUpo.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Le uf do
	 * orgao -----------------------------------------------------------
	 */
	public String LeUfOrgao(String codOrgao) throws DaoException {
		// synchronized
		String codUf = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "select e.cod_uf from smit.tsmi_orgao o , smit.tsmi_endereco e " + "where cod_orgao ='"
					+ codOrgao + "' and o.seq_endereco = e.seq_endereco";
			ResultSet rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				codUf = rs.getString("cod_uf");

			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return codUf;
	}

	/**
	 * ----------------------------------------------------------- Posiciona
	 * Orgao e carrega no Bean
	 * -----------------------------------------------------------
	 */

	public boolean buscaOrgao(OrgaoBean myOrg, int pos, UsuarioBean myUsrLog) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
					+ ") order by nom_ORGAO";
			if (pos == 4) {
				if (myOrg.getCodOrgao().equals(""))
					sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
							+ ") order by nom_ORGAO desc";
				else
					sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
							+ ") and rpad(NOM_ORGAO,30,' ')||lpad(COD_ORGAO,6,'0') > rpad('" + myOrg.getNomOrgao()
							+ "',30,' ')||lpad('" + myOrg.getCodOrgao() + "',6,'0') order by nom_Orgao,cod_orgao";
			}
			if (pos == 12) {
				if (myOrg.getCodOrgao().equals(""))
					sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
							+ ") order by nom_Orgao asc";
				else
					sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
							+ ") and rpad(NOM_ORGAO,30,' ')||lpad(COD_ORGAO,6,'0') < rpad('" + myOrg.getNomOrgao()
							+ "',30,' ')||lpad('" + myOrg.getCodOrgao()
							+ "',6,'0') order by nom_Orgao desc,cod_orgao desc";
			}
			if (pos == 21)
				sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
						+ ") order by NOM_ORGAO desc";
			if (pos == 25)
				sCmd = "SELECT COD_ORGAO from TSMI_ORGAO where cod_orgao in (" + getOrgaos(myUsrLog, "SQL")
						+ ") and COD_ORGAO = '" + myOrg.getCodOrgao() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myOrg.setMsgErro("");
			if (rs.next()) {
				myOrg.setCodOrgao(rs.getString("cod_Orgao"));
				myOrg.setPkid(myOrg.getCodOrgao());
			}
			rs.close();
			stmt.close();
			bOk = OrgaoLeBean(myOrg, 0);
		} catch (SQLException e) {
			bOk = false;
			myOrg.setCodOrgao("");
			myOrg.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myOrg.setCodOrgao("");
			myOrg.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Posiciona
	 * Protocolo UPO e carrega no Bean
	 * -----------------------------------------------------------
	 */

	public boolean buscaProtocoloUpo(ProtocoloUpoBean myProtUpo, int pos, UsuarioBean myUsrLog) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
					+ getProtocolosUpo(myUsrLog, "SQL") + ") order by NOM_PROT_UPO";
			if (pos == 4) {
				if (myProtUpo.getCodProtocoloUpo().equals(""))
					sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
							+ getProtocolosUpo(myUsrLog, "SQL") + ") order by NOM_PROT_UPO desc";
				else
					sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
							+ getProtocolosUpo(myUsrLog, "SQL")
							+ ") and rpad(NOM_PROT_UPO,50,' ')||lpad(COD_PROT_UPO,6,'0') > rpad('"
							+ myProtUpo.getNomProtocoloUpo() + "',60,' ')||lpad('" + myProtUpo.getCodProtocoloUpo()
							+ "',6,'0') order by NOM_PROT_UPO,COD_PROT_UPO";
			}
			if (pos == 12) {
				if (myProtUpo.getCodProtocoloUpo().equals(""))
					sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
							+ getProtocolosUpo(myUsrLog, "SQL") + ") order by NOM_PROT_UPO asc";
				else
					sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
							+ getProtocolosUpo(myUsrLog, "SQL")
							+ ") and rpad(NOM_PROT_UPO,50,' ')||lpad(COD_PROT_UPO,6,'0') > rpad('"
							+ myProtUpo.getNomProtocoloUpo() + "',60,' ')||lpad('" + myProtUpo.getCodProtocoloUpo()
							+ "',6,'0') order by NOM_PROT_UPO,COD_PROT_UPO";
			}
			if (pos == 21)
				sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
						+ getProtocolosUpo(myUsrLog, "SQL") + ") order by NOM_PROT_UPO desc";
			if (pos == 25)
				sCmd = "SELECT COD_PROT_UPO from SMIT.TSMI_PROT_UPO where COD_PROT_UPO in ("
						+ getProtocolosUpo(myUsrLog, "SQL") + ") and COD_PROT_UPO = '" + myProtUpo.getCodProtocoloUpo()
						+ "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myProtUpo.setMsgErro("");
			if (rs.next()) {
				myProtUpo.setCodProtocoloUpo(rs.getString("cod_prot_upo"));
				myProtUpo.setPkid(myProtUpo.getCodProtocoloUpo());
			}
			rs.close();
			stmt.close();
			bOk = ProtocoloUpoLeBean(myProtUpo, 0);
		} catch (SQLException e) {
			bOk = false;
			myProtUpo.setCodProtocoloUpo("");
			myProtUpo.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myProtUpo.setCodProtocoloUpo("");
			myProtUpo.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/**
	 * ----------------------------------------------------------- Monta Ordem e
	 * Select para Consulta de Orgao
	 * -----------------------------------------------------------
	 */
	public String[][] OrgaoConsultaMontaSelect(String ordemOrgao, String sigOrgao, String nomOrgao)
			throws DaoException {
		// nome das colunas comando (AS no select)
		String[] nomCol = { "cod_Orgao", "sig_Orgao", "nom_Orgao", "dat_cadastro", "cod_uf" };
		String[][] wk = new String[7][nomCol.length];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < nomCol.length; j++) {
				wk[i][j] = "";
			}
		}
		// expressao para colunas
		String[] exprCol = { "o.cod_Orgao", "o.sig_Orgao", "o.nom_Orgao", "to_char(o.dat_Cadastro,'dd/mm/yyyy')",
				"uf.cod_uf" };
		// cabecalhos - nome externo das colunas
		String[] nomCabec = { "Codigo", "Sigla", "Nome", "Data Cadastro", "UF" };
		// percentual
		String[] percCol = { "10%", "10%", "40%", "20%", "10%" };
		try {
			// montar a ordem (opcional pode ser "")
			// express�o para a ordem de apresentacao
			Vector vParam = new Vector();
			String exprOrdem = "";

			String[] exOrdem = new String[2];
			exOrdem[0] = " nom_orgao ";
			exOrdem[1] = " sig_orgao ";

			// nome externo da ordem
			String exprNomOrdem = "";
			String[] nomOrdem = new String[2];
			nomOrdem[0] = "Nome";
			nomOrdem[1] = "Sigla";

			// expressao para chave primaria do arquivo
			String exprPk = "o.Cod_Orgao"; // opcional - pode ser ""
			exprOrdem = exOrdem[1];

			exprNomOrdem = "Ordem: " + nomOrdem[1];
			for (int i = 0; i < 2; i++) {
				if (ordemOrgao.equals(nomOrdem[i])) {
					// Ordem by
					exprOrdem = exOrdem[i];
					// descricao dos parametros
					exprNomOrdem = "Ordem: " + nomOrdem[i];
					vParam.addElement(exprNomOrdem);
				}
			}
			// comando select - primeira e segunda posicao podem ser Ordem e PK
			// (opcional)
			String cmdSelect = "SELECT ";
			if (exprOrdem.length() > 0) { // existe ordem
				if (exprPk.length() > 0)
					cmdSelect += exprOrdem + " as CONTEUDOORDEM," + exprPk + " as CONTEUDOPK,";
				else
					cmdSelect += exprOrdem + " as CONTEUDOORDEM,";
			} else {
				if (exprPk.length() > 0)
					cmdSelect += exprPk + " as CONTEUDOPK,";
			}

			// coloca as colunas
			for (int i = 0; i < nomCol.length; i++) {
				cmdSelect += exprCol[i] + " as " + nomCol[i] + ",";
			}
			// retira virgula e coloca o FROM
			cmdSelect = cmdSelect.substring(0, cmdSelect.length() - 1) + " FROM TSMI_ORGAO o, TSMI_ENDERECO uf";

			// montaWhereTexto(objeto do input,"nome do campo","Nome externo
			// docampo")
			// Nome do campo ==> nome no comando select
			// Nome externo do campo ==> ser� utilizado para montagem das
			// descricoes dos parametros
			String cmdWhere = montaWhereTexto(sigOrgao, "sig_orgao", "Sigla", vParam);
			cmdWhere += montaWhereTexto(nomOrgao, "NOM_orgao", "Nome", vParam);
			cmdWhere += " AND o.seq_endereco = uf.seq_endereco ";
			// colocar where no select
			if (cmdWhere.length() > 0) {
				cmdSelect += " WHERE " + cmdWhere.substring(5, cmdWhere.length());
				cmdSelect += " AND o.seq_endereco = uf.seq_endereco ";
			}
			// colocar order by no select
			if (exprOrdem.length() > 0)
				cmdSelect += " ORDER BY " + exprOrdem;
			// setar os wk
			/*
			 * wk[0][0] ==> parametros 0 (primeiro string descrevendo os
			 * parametros de selecao wk[1][0] ==> parametros 1 (segundo string
			 * descrevendo os parametros de selecao wk[2][0] ==> string com a
			 * ordem desejada da consulta wk[3][0] ==> string com o select
			 * utilizado pela consulta primeiro campo (opcional) sera a ordem de
			 * apresentacao escolhida segundo campo (opcional) deve ser o PK que
			 * vai popular o Bean no retorno o terceiro campo em diante serao as
			 * coluna a serem mostradas wk[4][n] ==> colunas a serem mostradas
			 * (n=numero de colunas) wk[5][n] ==> cabecalho das colunas a serem
			 * mostradas (n=numero de colunas) wk[6][n] ==> percentual para
			 * calcular o tamanho das colunas a serem mostradas (n=numero de
			 * colunas)
			 */
			for (int j = 0; j < vParam.size(); j++) {
				wk[j % 2][0] += vParam.elementAt(j);
			}
			wk[2][0] = exprOrdem; // ==> string com a ordem desejada da consulta
			wk[3][0] = cmdSelect; // ==> string com a ordem desejada da consulta

			// nome das colunas comando (AS no select)
			wk[4] = nomCol;
			// cabecalhos - nome externo das colunas
			wk[5] = nomCabec;
			// percentual
			wk[6] = percCol;
		} catch (Exception ex) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < nomCol.length; j++) {
					wk[i][j] = "";
				}
			}
		}
		return wk;
	}

	/**
	 * ----------------------------------------------------------- Monta Ordem e
	 * Select para Consulta de Protocolo UPO
	 * -----------------------------------------------------------
	 */
	public String[][] ProtocoloUpoConsultaMontaSelect(String ordemProtocoloUpo, String nomProtUpo) throws DaoException {
		// nome das colunas comando (AS no select)
		String[] nomCol = { "cod_prot_upo", "nom_prot_upo" };
		String[][] wk = new String[7][nomCol.length];
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < nomCol.length; j++) {
				wk[i][j] = "";
			}
		}
		// expressao para colunas
		String[] exprCol = { "o.cod_prot_upo", "o.nom_prot_upo" };
		// cabecalhos - nome externo das colunas
		String[] nomCabec = { "Codigo", "Nome Protocolo UPO" };
		// percentual
		String[] percCol = { "10%", "80%" };
		try {
			// montar a ordem (opcional pode ser "")
			// express�o para a ordem de apresentacao
			Vector vParam = new Vector();
			String exprOrdem = "";

			String[] exOrdem = new String[1];
			exOrdem[0] = " nom_prot_upo ";

			// nome externo da ordem
			String exprNomOrdem = "";
			String[] nomOrdem = new String[1];
			nomOrdem[0] = "Nome Protocolo UPO";

			// expressao para chave primaria do arquivo
			String exprPk = "o.cod_prot_upo"; // opcional - pode ser ""
			exprOrdem = exOrdem[0];

			exprNomOrdem = "Ordem: " + nomOrdem[0];
			for (int i = 0; i < 1; i++) {
				if (ordemProtocoloUpo.equals(nomOrdem[i])) {
					// Ordem by
					exprOrdem = exOrdem[i];
					// descricao dos parametros
					exprNomOrdem = "Ordem: " + nomOrdem[i];
					vParam.addElement(exprNomOrdem);
				}
			}
			// comando select - primeira e segunda posicao podem ser Ordem e PK
			// (opcional)
			String cmdSelect = "SELECT ";
			if (exprOrdem.length() > 0) { // existe ordem
				if (exprPk.length() > 0)
					cmdSelect += exprOrdem + " as CONTEUDOORDEM," + exprPk + " as CONTEUDOPK,";
				else
					cmdSelect += exprOrdem + " as CONTEUDOORDEM,";
			} else {
				if (exprPk.length() > 0)
					cmdSelect += exprPk + " as CONTEUDOPK,";
			}

			// coloca as colunas
			for (int i = 0; i < nomCol.length; i++) {
				cmdSelect += exprCol[i] + " as " + nomCol[i] + ",";
			}
			// retira virgula e coloca o FROM
			cmdSelect = cmdSelect.substring(0, cmdSelect.length() - 1) + " FROM SMIT.TSMI_PROT_UPO o";

			// montaWhereTexto(objeto do input,"nome do campo","Nome externo
			// docampo")
			// Nome do campo ==> nome no comando select
			// Nome externo do campo ==> ser� utilizado para montagem das
			// descricoes dos parametros
			String cmdWhere = montaWhereTexto(nomProtUpo, "nom_prot_upo", "Nome Protocolo UPO", vParam);

			// colocar where no select
			if (cmdWhere.length() > 0) {
				cmdSelect += " WHERE " + cmdWhere.substring(5, cmdWhere.length());
			}
			// colocar order by no select
			if (exprOrdem.length() > 0)
				cmdSelect += " ORDER BY " + exprOrdem;
			// setar os wk
			/*
			 * wk[0][0] ==> parametros 0 (primeiro string descrevendo os
			 * parametros de selecao wk[1][0] ==> parametros 1 (segundo string
			 * descrevendo os parametros de selecao wk[2][0] ==> string com a
			 * ordem desejada da consulta wk[3][0] ==> string com o select
			 * utilizado pela consulta primeiro campo (opcional) sera a ordem de
			 * apresentacao escolhida segundo campo (opcional) deve ser o PK que
			 * vai popular o Bean no retorno o terceiro campo em diante serao as
			 * coluna a serem mostradas wk[4][n] ==> colunas a serem mostradas
			 * (n=numero de colunas) wk[5][n] ==> cabecalho das colunas a serem
			 * mostradas (n=numero de colunas) wk[6][n] ==> percentual para
			 * calcular o tamanho das colunas a serem mostradas (n=numero de
			 * colunas)
			 */
			for (int j = 0; j < vParam.size(); j++) {
				wk[j % 2][0] += vParam.elementAt(j);
			}
			wk[2][0] = exprOrdem; // ==> string com a ordem desejada da consulta
			wk[3][0] = cmdSelect; // ==> string com a ordem desejada da consulta

			// nome das colunas comando (AS no select)
			wk[4] = nomCol;
			// cabecalhos - nome externo das colunas
			wk[5] = nomCabec;
			// percentual
			wk[6] = percCol;
		} catch (Exception ex) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < nomCol.length; j++) {
					wk[i][j] = "";
				}
			}
		}
		return wk;
	}

	/**
	 * ------------------------------------- Codigo Relativo ao ParamSitemaBean
	 * ----------------------------------------
	 */
	/**
	 * ----------------------------------------------------------- Prepara
	 * Parametros para o Login
	 * -----------------------------------------------------------
	 */
	public void PreparaParam(Connection conn, ParamSistemaBean myParam) throws DaoException {

		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TCAU_PARAM_SISTEMA where COD_SISTEMA = '" + myParam.getCodSistema() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			List vPar = new ArrayList();
			List nPar = new ArrayList();
			String v = "";
			String n = "";
			while (rs.next()) {
				n = rs.getString("nom_parametro");
				if (n == null)
					n = "";
				v = rs.getString("val_parametro");
				if (v == null)
					v = "";
				vPar.add(v);
				nPar.add(n);
			}
			myParam.setNomParamSist(nPar);
			myParam.setValParamSist(vPar);
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());

		} finally {
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception ey) {
			}
		}
	}

	public void PreparaParam(ParamSistemaBean myParam) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			PreparaParam(conn, myParam);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}

		}
	}

	public boolean ParamSistLeBean(ParamSistemaBean myParam) throws DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT nom_Descricao,cod_parametro,nom_Parametro,val_parametro,cod_sistema, num_ordem from TCAU_PARAM_SISTEMA ";
			if ((myParam.getCodParametro().equals("0")) || (myParam.getCodParametro().length() == 0))
				sCmd += "WHERE nom_Parametro = '" + myParam.getNomParam() + "'";
			else
				sCmd += "WHERE cod_Parametro = '" + myParam.getCodParametro() + "'";
			sCmd += " and  cod_Sistema   = '" + myParam.getCodSistema() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myParam.setMsgErro("");
			while (rs.next()) {
				myParam.setNomDescricao(rs.getString("nom_Descricao"));
				myParam.setCodParametro(rs.getString("cod_parametro"));
				myParam.setNomParam(rs.getString("nom_Parametro"));
				myParam.setValParametro(rs.getString("val_Parametro"));
				myParam.setCodSistema(rs.getString("cod_Sistema"));
				myParam.setNumOrdem(rs.getString("num_Ordem"));
				myParam.setPkid(myParam.getCodParametro());
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			myParam.setCodParametro("0");
			myParam.setPkid("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myParam.setCodParametro("0");
			myParam.setPkid("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public void CopiaParam(SistemaBean mySist, ParamSistemaBean myParam) throws DaoException {
		Connection conn = null;
		Vector vErro = new Vector();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT seq_tcau_param_sistema.nextval seq_param,nom_parametro,";
			sCmd += "nom_descricao,val_parametro,num_ordem ";
			sCmd += "FROM TCAU_PARAM_SISTEMA";
			sCmd += " WHERE cod_sistema =" + mySist.getCodSistema();

			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				String sCmdCp = "SELECT nom_parametro FROM TCAU_PARAM_SISTEMA WHERE cod_sistema = "
						+ myParam.getCodSistemaCp() + " and nom_parametro = '" + rs.getString("nom_parametro") + "'";
				Statement stmtCp = conn.createStatement();
				ResultSet rsCp = stmtCp.executeQuery(sCmdCp);

				String nomParam = " ";
				while (rsCp.next()) {
					nomParam = rsCp.getString("nom_parametro");
					vErro.addElement("Par�metro " + nomParam + " j� existente. \n");
				}

				if ((nomParam.trim().equals("")) == true) {
					Statement stmt1 = conn.createStatement();
					String sCmd1 = "INSERT INTO TCAU_PARAM_SISTEMA (cod_sistema,cod_parametro,nom_parametro,";
					sCmd1 += "nom_descricao, val_parametro, num_ordem)";
					sCmd1 += "VALUES (" + myParam.getCodSistemaCp() + "," + rs.getString("seq_param") + ",";
					sCmd1 += "'" + rs.getString("nom_parametro") + "', '" + rs.getString("nom_descricao") + "', '"
							+ rs.getString("val_parametro") + "', " + rs.getString("num_ordem") + ")";

					vErro.addElement("Opera��o efetuada com sucesso!");
					stmt1.execute(sCmd1);
					stmt1.close();
				}
				rsCp.close();
				stmtCp.close();
			}
			stmt.close();
			mySist.setMsgErro(vErro);
			conn.close();
		} catch (SQLException sqle) {
			mySist.setMsgErro(sqle.getMessage());
			mySist.setCodSistema("");
		} catch (Exception e) {
			mySist.setMsgErro(e.getMessage());
			mySist.setCodSistema("");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean SistemaInsertParam(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		String result = "";
		String prazoDef = "";
		String prazoEmissao = "";
		String prazoEntrega = "";
		String prazoPublDO = "";
		String prazoRecursoPenal = "";
		String prazoRecurso2Inst = "";
		String prazoProsperaResult = "";
		String prazoEntregaCNH = "";
		String folgaDef = "";
		String folgaRecursoPenal = "";
		String folgaRecurso2Inst = "";
		String nomAssDef = "";
		String nomAssRecurso = "";
		String nomAssEntrega = "";
		String secretariaProcesso = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Vector paramAtual = mySist.getParametros();
			PNT.DaoBroker broker = DaoBrokerFactory.getInstance();
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < paramAtual.size(); i++) {
				ParamSistemaBean myParam = (ParamSistemaBean) paramAtual.elementAt(i);
				if ("".equals(myParam.getNomDescricao()) == false) {
					ParamInsert(myParam, stmt);
					if (mySist.getNomAbrev().equals("PNT")) {
						String sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_DEF_PREVIA' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						ResultSet rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoDef = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_EMISSAO' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoEmissao = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_ENTREGA' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoEntrega = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_PUBL_DO' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoPublDO = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_RECURSO_PENALIDADE' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoRecursoPenal = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_RECURSO_2INSTANCIA' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoRecurso2Inst = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_PROSPERA_RESULT' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoProsperaResult = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='PRAZO_ENTR_CNH' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							prazoEntregaCNH = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='FOLGA_DEF_PREVIA' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							folgaDef = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='FOLGA_RECURSO_PENALIDADE' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							folgaRecursoPenal = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='FOLGA_RECURSO_2INSTANCIA' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							folgaRecurso2Inst = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='NOME_ASS_DEFESA_PNT' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							nomAssDef = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='NOME_ASS_RECURSO_PNT' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							nomAssRecurso = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='NOME_ASS_ENTREGA_PNT' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							nomAssEntrega = rs.getString("VAL_PARAMETRO");

						sComando = "SELECT * FROM TCAU_PARAM_SISTEMA WHERE NOM_PARAMETRO='SECRETARIA_PROCESSO_PONTUACAO' AND COD_SISTEMA='"
								+ mySist.getCodSistema() + "'";
						rs = stmt.executeQuery(sComando);
						if (rs.next())
							secretariaProcesso = rs.getString("VAL_PARAMETRO");

						broker.Transacao630(myParam.getCodParametro().toString(), "1", "   ", prazoDef, prazoEmissao,
								prazoEntrega, prazoPublDO, prazoRecursoPenal, prazoRecurso2Inst, prazoProsperaResult,
								prazoEntregaCNH, folgaDef, folgaRecursoPenal, folgaRecurso2Inst, nomAssDef,
								nomAssRecurso, nomAssEntrega, secretariaProcesso);
						if (result.substring(0, 3).equals("016")) {
							result = broker.Transacao630(myParam.getCodParametro().toString(), "2", "   ", prazoDef,
									prazoEmissao, prazoEntrega, prazoPublDO, prazoRecursoPenal, prazoRecurso2Inst,
									prazoProsperaResult, prazoEntregaCNH, folgaDef, folgaRecursoPenal,
									folgaRecurso2Inst, nomAssDef, nomAssRecurso, nomAssEntrega, secretariaProcesso);
							if (!result.substring(0, 3).equals("000"))
								throw new DaoException("Erro na atualiza��o da Base de Dados: " + result);
						}
						if (!result.substring(0, 3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result);
					}
				} else {
					if ("".equals(myParam.getCodParametro()) == false)
						ParamDelete(myParam, stmt);
					if (mySist.getNomAbrev().equals("PNT")) {
						result = broker.Transacao630(myParam.getCodParametro().toString(), "2", "   ", prazoDef,
								prazoEmissao, prazoEntrega, prazoPublDO, prazoRecursoPenal, prazoRecurso2Inst,
								prazoProsperaResult, prazoEntregaCNH, folgaDef, folgaRecursoPenal, folgaRecurso2Inst,
								nomAssDef, nomAssRecurso, nomAssEntrega, secretariaProcesso);
						if (!result.substring(0, 3).equals("000"))
							throw new DaoException("Erro na atualiza��o da Base de Dados: " + result);
					}
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	public boolean SistemaInsertProcesso(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Vector processoAtual = mySist.getProcessos();
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < processoAtual.size(); i++) {
				ProcessoSistemaBean myProcesso = (ProcessoSistemaBean) processoAtual.elementAt(i);
				if ("".equals(myProcesso.getNomDescricao()) == false) {
					ProcessoInsert(myProcesso, stmt);
				} else {
					if ("".equals(myProcesso.getCodProcesso()) == false)
						ProcessoDelete(myProcesso, stmt);
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	public boolean SistemaInsertNivelProcesso(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Vector nivelprocessoAtual = mySist.getNivelProcessos();
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < nivelprocessoAtual.size(); i++) {
				NivelProcessoSistemaBean myNivelProcesso = (NivelProcessoSistemaBean) nivelprocessoAtual.elementAt(i);
				if ("".equals(myNivelProcesso.getNomNivelProcesso()) == false) {
					NivelProcessoInsert(myNivelProcesso, stmt);
				} else {
					if ("".equals(myNivelProcesso.getCodProcesso()) == false)
						NivelProcessoDelete(myNivelProcesso, stmt);
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	public void ParamInsert(ParamSistemaBean myParam, Statement stmt) throws DaoException {
		try {
			String codParam = myParam.getCodParametro();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codParam)) || ("0".equals(codParam))) {
				sCmd = "INSERT INTO TCAU_PARAM_SISTEMA (cod_Parametro,";
				sCmd += "nom_Parametro,nom_Descricao,val_parametro,cod_sistema,num_ordem) ";
				sCmd += "VALUES (seq_tcau_param_sistema.nextval,";
				sCmd += "'" + myParam.getNomParam() + "',";
				sCmd += "'" + myParam.getNomDescricao() + "',";
				sCmd += "'" + myParam.getValParametro() + "',";
				sCmd += "'" + myParam.getCodSistema() + "',";
				sCmd += "'" + myParam.getNumOrdem() + "') ";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TCAU_PARAM_SISTEMA set ";
				sCmd += "nom_Parametro ='" + myParam.getNomParam() + "',";
				sCmd += "val_Parametro ='" + myParam.getValParametro() + "',";
				sCmd += "nom_Descricao ='" + myParam.getNomDescricao() + "', ";
				sCmd += "num_ordem ='" + myParam.getNumOrdem() + "' ";
				sCmd += " where cod_Parametro = '" + codParam + "' ";
				stmt.execute(sCmd);
			}
			sCmd = "SELECT COD_PARAMETRO FROM TCAU_PARAM_SISTEMA ";
			sCmd += "WHERE num_ordem ='" + myParam.getNumOrdem() + "' and cod_sistema='" + myParam.getCodSistema()
					+ "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myParam.setCodParametro(rs.getString("cod_Parametro"));
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void ProcessoInsert(ProcessoSistemaBean myProcesso, Statement stmt) throws DaoException {
		try {
			String codProcesso = myProcesso.getCodProcesso();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codProcesso)) || ("0".equals(codProcesso))) {
				sCmd = "INSERT INTO TSMI_PROCESSO_SISTEMA (cod_Processo_sistema,";
				sCmd += "nom_Processo,nom_Descricao,cod_Sistema) ";
				sCmd += "VALUES (seq_tsmi_processo_sistema.nextval,";
				sCmd += "'" + myProcesso.getNomProcesso() + "',";
				sCmd += "'" + myProcesso.getNomDescricao() + "',";
				sCmd += "'" + myProcesso.getCodSistema() + "') ";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TSMI_PROCESSO_SISTEMA set ";
				sCmd += "nom_Processo  ='" + myProcesso.getNomProcesso() + "',";
				sCmd += "nom_Descricao ='" + myProcesso.getNomDescricao() + "' ";
				sCmd += " where cod_Processo_sistema = '" + codProcesso + "' ";
				stmt.execute(sCmd);
			}

			sCmd = "SELECT COD_PROCESSO_SISTEMA FROM TSMI_PROCESSO_SISTEMA ";
			sCmd += "WHERE nom_processo ='" + myProcesso.getNomProcesso() + "' and cod_sistema='"
					+ myProcesso.getCodSistema() + "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myProcesso.setCodProcesso(rs.getString("cod_Processo_sistema"));
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void NivelProcessoInsert(NivelProcessoSistemaBean myNivelProcesso, Statement stmt) throws DaoException {
		try {
			String codNivelProcesso = myNivelProcesso.getCodNivelProcesso();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codNivelProcesso)) || ("0".equals(codNivelProcesso))) {
				sCmd = "INSERT INTO TSMI_PROCESSO_SISTEMA_NIVEL (cod_Processo_sistema_nivel,";
				sCmd += "cod_processo_sistema,dsc_nivel_processo,val_nivel, tip_msg) ";
				sCmd += "VALUES (seq_tsmi_processo_sistema_nvl.nextval,";
				sCmd += "'" + myNivelProcesso.getCodProcesso() + "',";
				sCmd += "'" + myNivelProcesso.getNomNivelProcesso() + "',";
				sCmd += "'" + myNivelProcesso.getValNivelProcesso() + "',";
				sCmd += "'" + myNivelProcesso.getTipoMsg() + "') ";
				stmt.execute(sCmd);
			} else {
				sCmd = "UPDATE TSMI_PROCESSO_SISTEMA_NIVEL set ";
				sCmd += "dsc_nivel_processo  ='" + myNivelProcesso.getNomNivelProcesso() + "',";
				sCmd += "val_nivel ='" + myNivelProcesso.getValNivelProcesso() + "',";
				sCmd += "tip_msg ='" + myNivelProcesso.getTipoMsg() + "' ";
				sCmd += " where cod_Processo_sistema_nivel = '" + codNivelProcesso + "' ";
				stmt.execute(sCmd);
			}

			sCmd = "SELECT COD_PROCESSO_SISTEMA_NIVEL FROM TSMI_PROCESSO_SISTEMA_NIVEL ";
			sCmd += "WHERE dsc_nivel_processo ='" + myNivelProcesso.getNomNivelProcesso()
					+ "' and cod_processo_sistema='" + myNivelProcesso.getCodProcesso() + "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myNivelProcesso.setCodNivelProcesso(rs.getString("cod_Processo_sistema_nivel"));
			}
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void ProcessoDelete(ProcessoSistemaBean myProcesso, Statement stmt) throws DaoException {
		try {
			String sCmd = "DELETE from TSMI_PROCESSO_SISTEMA where cod_Processo_sistema = '"
					+ myProcesso.getCodProcesso() + "'";
			stmt.execute(sCmd);
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void NivelProcessoDelete(NivelProcessoSistemaBean myNivelProcesso, Statement stmt) throws DaoException {
		try {
			String sCmd = "DELETE from TSMI_PROCESSO_SISTEMA_NIVEL where cod_Processo_sistema_nivel = '"
					+ myNivelProcesso.getCodNivelProcesso() + "'";
			stmt.execute(sCmd);
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void ParamUpdate(ParamSistemaBean myParam) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			sCmd = "UPDATE TCAU_PARAM_SISTEMA set ";
			sCmd += "nom_Parametro ='" + myParam.getNomParam() + "',";
			sCmd += "val_Parametro ='" + myParam.getValParametro() + "',";
			sCmd += "nom_Descricao ='" + myParam.getNomDescricao() + "', ";
			sCmd += "num_ordem ='" + myParam.getNumOrdem() + "' ";
			sCmd += " where cod_Parametro = '" + myParam.getCodParametro() + "' ";
			stmt.execute(sCmd);
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public void ParamDelete(ParamSistemaBean myParam, Statement stmt) throws DaoException {
		try {
			String sCmd = "DELETE TCAU_PARAM_SISTEMA where cod_parametro = '" + myParam.getCodParametro() + "'";
			stmt.execute(sCmd);
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	public void SistGetParam(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_parametro,nom_parametro,nom_descricao,val_parametro,num_ordem ";
			sCmd += " from TCAU_PARAM_SISTEMA";
			sCmd += " WHERE cod_sistema = '" + mySist.getCodSistema() + "'";
			sCmd += " order by num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector param = new Vector();
			while (rs.next()) {
				ParamSistemaBean myParam = new ParamSistemaBean();
				myParam.setCodParametro(rs.getString("cod_parametro"));
				myParam.setNomParam(rs.getString("nom_parametro"));
				myParam.setNomDescricao(rs.getString("nom_descricao"));
				myParam.setValParametro(rs.getString("val_parametro"));
				myParam.setNumOrdem(rs.getString("num_ordem"));
				param.addElement(myParam);
			}
			mySist.setParametros(param);
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			mySist.setMsgErro("Orgao008p - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("");
		} catch (Exception e) {
			mySist.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			mySist.setCodSistema("");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public void SistGetProcesso(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_processo_sistema,nom_processo,nom_descricao ";
			sCmd += " from TSMI_PROCESSO_SISTEMA";
			sCmd += " WHERE cod_sistema = '" + mySist.getCodSistema() + "'";
			sCmd += " order by cod_processo_sistema";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector processo = new Vector();
			while (rs.next()) {
				ProcessoSistemaBean myProcesso = new ProcessoSistemaBean();
				myProcesso.setCodProcesso(rs.getString("cod_processo_sistema"));
				myProcesso.setNomProcesso(rs.getString("nom_processo"));
				myProcesso.setNomDescricao(rs.getString("nom_descricao"));
				processo.addElement(myProcesso);
			}
			mySist.setProcessos(processo);
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			mySist.setMsgErro("Orgao008p - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("");
		} catch (Exception e) {
			mySist.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			mySist.setCodSistema("");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public void SistGetNivelProcesso(SistemaBean mySist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT NOM_PROCESSO FROM TSMI_PROCESSO_SISTEMA ";
			sCmd += "WHERE COD_SISTEMA='" + mySist.getCodSistema() + "' AND COD_PROCESSO_SISTEMA='"
					+ mySist.getCodProcesso() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				mySist.setNomProcesso(rs.getString("NOM_PROCESSO"));

			sCmd = "SELECT cod_processo_sistema_nivel,dsc_nivel_processo,val_nivel,nom_processo,tip_msg ";
			sCmd += " from TSMI_PROCESSO_SISTEMA T1,TSMI_PROCESSO_SISTEMA_NIVEL T2";
			sCmd += " WHERE T1.cod_sistema = '" + mySist.getCodSistema() + "' and ";
			sCmd += " T1.cod_processo_sistema= '" + mySist.getCodProcesso() + "' and ";
			sCmd += " T1.cod_processo_sistema=T2.cod_processo_sistema";
			sCmd += " order by cod_processo_sistema_nivel";
			rs = stmt.executeQuery(sCmd);
			Vector Nivelprocesso = new Vector();
			String sNomeProcesso = "";
			while (rs.next()) {
				NivelProcessoSistemaBean myNivelProcesso = new NivelProcessoSistemaBean();
				myNivelProcesso.setCodNivelProcesso(rs.getString("cod_processo_sistema_nivel"));
				myNivelProcesso.setNomNivelProcesso(rs.getString("dsc_nivel_processo"));
				myNivelProcesso.setValNivelProcesso(rs.getString("val_nivel"));
				myNivelProcesso.setTipoMsg(rs.getString("tip_msg"));
				Nivelprocesso.addElement(myNivelProcesso);
			}
			mySist.setNivelProcessos(Nivelprocesso);
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			mySist.setMsgErro("Orgao008p - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			mySist.setCodSistema("");
		} catch (Exception e) {
			mySist.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			mySist.setCodSistema("");
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * --------------------------------------------------------------- C�digo
	 * Realtivo ao Bean PerfilUsuarioAtuacao
	 * --------------------------------------------------------------
	 */

	public void atualPerfilAtuacao(PerfilUsuarioAtuacaoBean myPerAtu, String[] codPerf, String[] codOrgaoAtuacao,
			String orgLot, String cSist) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			String sCmd = "";

			sCmd = "UPDATE tcau_perfil_orgao set ";
			sCmd += "nom_username_oper= '" + myPerAtu.getNomUserNameOper() + "', ";
			sCmd += "dat_proc_oper= to_date('" + sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'), ";
			sCmd += "orgao_lotacao_oper= '" + myPerAtu.getOrgaoLotacaoOper() + "' ";

			sCmd += "where cod_usuario='" + myPerAtu.getCodUsuario() + "' and cod_orgao_atuacao<>'" + orgLot + "'"
					+ " and cod_perfil in (select cod_perfil from tcau_perfil where cod_sistema='" + cSist + "')";

			stmt.execute(sCmd);

			sCmd = "DELETE from TCAU_PERFIL_ORGAO where cod_usuario='" + myPerAtu.getCodUsuario()
					+ "' and cod_orgao_atuacao<>'" + orgLot + "'"
					+ " and cod_perfil in (select cod_perfil from tcau_perfil where cod_sistema='" + cSist + "')";
			stmt.execute(sCmd);
			for (int i = 0; i < codOrgaoAtuacao.length; i++) {
				if ("0".equals(codPerf[i]) == false) {
					sCmd = "INSERT INTO TCAU_PERFIL_ORGAO (cod_perfil_orgao,cod_Perfil,cod_Usuario,cod_orgao_atuacao,nom_username_oper,dat_proc_oper,orgao_lotacao_oper) ";
					sCmd += " VALUES (seq_tcau_perfil_orgao.nextval,'" + codPerf[i] + "','" + myPerAtu.getCodUsuario()
							+ "','" + codOrgaoAtuacao[i] + "','" + myPerAtu.getNomUserNameOper() + "',to_date('"
							+ sys.Util.formatedToday().substring(0, 10) + "','dd/mm/yyyy'),"
							+ myPerAtu.getOrgaoLotacaoOper() + ") ";
					stmt.execute(sCmd);
				}
			}
			// fechar a transa��o - commit ou rollback
			conn.commit();
			stmt.close();
			myPerAtu.setMsgErro("Opera��o efetuada com sucesso!");
		} catch (SQLException e) {
			myPerAtu.setMsgErro(e.getMessage());
		} catch (Exception ex) {
			myPerAtu.setMsgErro(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	public void PerfilAtuacaoLeBean(PerfilUsuarioAtuacaoBean myPerAtu, String codSistema, UsuarioBean myUsr,
			String orgLot) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			myPerAtu.setPerfisOrgao(new ArrayList());
			// prepara uma lista com todos os perfis do sistema
			String sCmdPerf = "select cod_perfil, nom_descricao from TCAU_PERFIL " + "where cod_sistema = "
					+ codSistema;
			ResultSet rsPerf = stmt.executeQuery(sCmdPerf);
			List ListPerf = new ArrayList();
			while (rsPerf.next()) {
				PerfilBean myPerf = new PerfilBean();
				myPerf.setNomDescricao(rsPerf.getString("nom_descricao"));
				myPerf.setCodPerfil(rsPerf.getString("cod_perfil"));
				ListPerf.add(myPerf);
			}
			// atualiza a lista de perfis
			myPerAtu.setPerfis(ListPerf);
			// preparar lista com os orgao
			int admACSS = 0;
			String sCmd = "Select ps.cod_nivel from TCAU_SISTEMA s, " + "TCAU_NIVEL_SISTEMA ps where ps.cod_usuario='"
					+ myUsr.getCodUsuario() + "' and ps.cod_nivel<>0 and "
					+ "ps.cod_sistema=s.cod_sistema and s.nom_abrev='ACSS' ";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			// verifica se e administrador do ACSS - se sim ver todos os Orgaos
			// se nao somente orgao em que esta atuando
			while (rs.next()) {
				admACSS = rs.getInt("cod_nivel");
			}
			// = 0 nao � administrador - somente orgao em que esta atuando
			if (admACSS == 0)
				sCmd = "select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o " + "where o.cod_orgao='"
						+ myUsr.getCodOrgaoAtuacao() + "' order by o.sig_orgao";
			else
				sCmd = "Select o.cod_orgao,o.sig_orgao from TSMI_ORGAO o" + " order by o.sig_orgao";
			rs = stmt.executeQuery(sCmd);
			String sCmdPerfUsu = "";
			Statement stmtPerfUsu = conn.createStatement();
			ResultSet rsPerfUsu = null;
			while (rs.next()) {
				PerfilUsuarioAtuacaoBean myOrg = new PerfilUsuarioAtuacaoBean();
				myOrg.setCodOrgaoAtuacao(rs.getString("cod_orgao"));
				myOrg.setSigOrgaoAtuacao(rs.getString("sig_orgao"));
				// nao inclui o Orgao de Lotacao do Usuario escolhido
				if (orgLot.equals(myOrg.getCodOrgaoAtuacao()) == false) {
					// buscar o perfil no Orgao de Atuacao - se existir
					sCmdPerfUsu = "select cod_perfil from TCAU_PERFIL_ORGAO " + "where cod_usuario      = "
							+ myPerAtu.getCodUsuario() + " and cod_orgao_atuacao =" + myOrg.getCodOrgaoAtuacao()
							+ " and cod_perfil in (" + " select cod_perfil from tcau_perfil where cod_sistema="
							+ codSistema + ")";
					rsPerfUsu = stmtPerfUsu.executeQuery(sCmdPerfUsu);
					while (rsPerfUsu.next()) {
						myOrg.setCodPerfil(rsPerfUsu.getString("cod_perfil"));
					}
					myPerAtu.getPerfisOrgao().add(myOrg);
				}
			}
			rsPerfUsu.close();
			stmtPerfUsu.close();
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Relativo ao ConsultaUsuarioBean
	 * ------------------------------------------
	 */
	public void ConsultaDadosUsr(ConsultaUsuarioBean auxClasse, String codSistema, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select per.cod_usuario, per.cod_orgao_atuacao, atu.sig_orgao, per.cod_perfil,"
					+ "perf.cod_perfil,perf.nom_descricao, usr.cod_usuario, usr.nom_usuario,"
					+ "usr.cod_orgao, orgaoLot.sig_orgao as OrgaoLotacao,usr.ind_bloqueio FROM "
					+ "TSMI_ORGAO atu,TCAU_USUARIO usr,TCAU_PERFIL_ORGAO per,"
					+ "TCAU_SISTEMA sis,TCAU_PERFIL perf ,(Select * From tsmi_orgao o) orgaoLot"
					+ " WHERE sis.cod_sistema = " + codSistema + " and sis.cod_sistema = perf.cod_sistema and "
					+ "per.cod_perfil  = perf.cod_perfil and " + "per.cod_orgao_atuacao = atu.cod_orgao and "
					+ "per.cod_usuario = usr.cod_usuario and ";
			if (auxClasse.getTipoUsuario().equals("I"))
				sCmd += "usr.ind_bloqueio = 1 and ";
			else if (auxClasse.getTipoUsuario().equals("A"))
				sCmd += "usr.ind_bloqueio is null and ";

			sCmd += "usr.cod_orgao = orgaoLot.cod_orgao order by OrgaoLotacao";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			String indBloqueio = "";
			while (rs.next()) {
				ConsultaUsuarioBean dados = new ConsultaUsuarioBean();
				dados.setNomUsuario(rs.getString("nom_usuario"));
				dados.setCodOrgaoLotacao(rs.getString("cod_orgao"));
				dados.setSigOrgaoLotacao(rs.getString("OrgaoLotacao"));
				dados.setCodOrgaoAtuacao(rs.getString("cod_orgao_atuacao"));
				dados.setSigOrgaoAtuacao(rs.getString("sig_orgao"));
				dados.setCodPerfil(rs.getString("cod_perfil"));
				dados.setNomDescricao(rs.getString("nom_descricao"));
				indBloqueio = rs.getString("ind_bloqueio");
				if (indBloqueio == null)
					indBloqueio = "";
				if (indBloqueio.equals("1"))
					dados.setTipoUsuario("INATIVO");
				else
					dados.setTipoUsuario("ATIVO");

				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Relativo ao
	 * ConsultaHistUsuarioBean ------------------------------------------
	 */
	public void ConsultaDadosHistUsr(ConsultaHistoricosBean auxClasse, String codSistema, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select HU.COD_HISTORICO_USUARIO,  HU.COD_ORGAO, ORG.SIG_ORGAO as SIGORG, HU.ACAO, "
					+ "HU.NOM_USUARIO, HU.COD_USUARIO,"
					+ "to_char(HU.DAT_PROC_OPER,'dd/mm/yyyy') as DAT_PROC_OPER,HU.NOM_USERNAME, HU.ORGAO_LOTACAO_OPER,ORGOP.SIG_ORGAO as SIGORGOPER, HU.NOM_USERNAME_OPER, "
					+ "to_char(HU.DAT_VALIDADE,'dd/mm/yyyy') as DAT_VALIDADE,HU.NUM_CPF, HU.TXT_EMAIL, "
					+ "HU.VAL_HORA_VALIDADE, HU.TXT_LOCAL_TRABALHO, "
					+ "to_char(HU.DAT_SENHA_ALTERADA,'dd/mm/yyyy') as DAT_SENHA_ALTERADA, "
					+ "to_char(HU.DAT_BIOMETRIA_ALTERADA,'dd/mm/yyyy') as DAT_BIOMETRIA_ALTERADA "
					+ "FROM SMIT_ACSS.TCAU_HISTORICO_USUARIO HU, TCAU_NIVEL_SISTEMA NS, TSMI_ORGAO ORG, "
					+ "TSMI_ORGAO ORGOP " + "WHERE NS.COD_SISTEMA = " + codSistema + " "
					+ "AND HU.COD_USUARIO = NS.COD_USUARIO " + "AND HU.COD_ORGAO = ORG.COD_ORGAO "
					+ "AND HU.ORGAO_LOTACAO_OPER = ORGOP.COD_ORGAO ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd += "AND HU.DAT_PROC_OPER >= TO_DATE('" + auxClasse.getDatInicio()
						+ ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			if (!auxClasse.getDatFim().equals(""))
				sCmd += "AND HU.DAT_PROC_OPER <= TO_DATE('" + auxClasse.getDatFim()
						+ ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			sCmd += "ORDER BY HU.NOM_USERNAME, HU.COD_HISTORICO_USUARIO";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				UsuarioBean dados = new UsuarioBean();
				dados.setCodUsuario(rs.getString("COD_USUARIO"));
				dados.getOrgao().setCodOrgao(rs.getString("COD_ORGAO"));
				dados.getOrgao().setSigOrgao(rs.getString("SIGORG"));
				dados.setAcaoAuditoria(rs.getString("ACAO"));
				dados.setDatProcOper(rs.getString("DAT_PROC_OPER"));
				dados.setNomUserName(rs.getString("NOM_USERNAME"));
				dados.setOrgaoLotacaoOper(rs.getString("ORGAO_LOTACAO_OPER"));
				dados.setSigOrgaoLotacaoOper(rs.getString("SIGORGOPER"));
				dados.setNomUserNameOper(rs.getString("NOM_USERNAME_OPER"));
				dados.setEmail(rs.getString("TXT_EMAIL"));
				dados.setNumCpf(rs.getString("NUM_CPF"));
				dados.setDatValidade(rs.getString("DAT_VALIDADE"));
				dados.setHoraValidade(rs.getString("VAL_HORA_VALIDADE"));
				dados.setLocalTrabalho(rs.getString("TXT_LOCAL_TRABALHO"));
				dados.setDatSenhaAlterada(rs.getString("DAT_SENHA_ALTERADA"));
				dados.setDatBioAlterada(rs.getString("DAT_BIOMETRIA_ALTERADA"));

				dados.setNomUsuario(rs.getString("NOM_USUARIO"));

				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Relativo ao ConsultaHistPerOrg
	 * ------------------------------------------
	 */
	public void ConsultaDadosHistPerOrg(ConsultaHistoricosBean auxClasse, String codSistema, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select HP.COD_HIST_PERFIL_ORGAO,  HP.ACAO, U.NOM_USERNAME AS COD_USUARIO, P.NOM_DESCRICAO AS COD_PERFIL, "
					+ "ORGATU.SIG_ORGAO AS COD_ORGAO_ATUACAO, "
					+ "to_char(HP.DAT_PROC_OPER,'dd/mm/yyyy') as DAT_PROC_OPER, "
					+ "HP.NOM_USERNAME_OPER, ORGOPER.SIG_ORGAO AS ORGAO_LOTACAO_OPER "
					+ "FROM SMIT_ACSS.TCAU_HIST_PERFIL_ORGAO HP, TCAU_PERFIL P, TCAU_USUARIO U, "
					+ "TSMI_ORGAO ORGATU,TSMI_ORGAO ORGOPER " + "WHERE P.COD_SISTEMA = " + codSistema + " "
					+ "AND HP.COD_PERFIL = P.COD_PERFIL " + "AND HP.COD_USUARIO = U.COD_USUARIO "
					+ "AND HP.COD_ORGAO_ATUACAO = ORGATU.COD_ORGAO " + "AND HP.ORGAO_LOTACAO_OPER = ORGOPER.COD_ORGAO ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd += "AND HP.DAT_PROC_OPER >= TO_DATE('" + auxClasse.getDatInicio()
						+ ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			if (!auxClasse.getDatFim().equals(""))
				sCmd += "AND HP.DAT_PROC_OPER <= TO_DATE('" + auxClasse.getDatFim()
						+ ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			sCmd += "ORDER BY HP.COD_USUARIO, HP.COD_HIST_PERFIL_ORGAO ";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				PerfilUsuarioAtuacaoBean dados = new PerfilUsuarioAtuacaoBean();
				dados.setAcaoAuditoria(rs.getString("ACAO"));
				dados.setCodUsuario(rs.getString("COD_USUARIO"));
				dados.setCodPerfil(rs.getString("COD_PERFIL"));
				dados.setCodOrgaoAtuacao(rs.getString("COD_ORGAO_ATUACAO"));
				dados.setDatProcOper(rs.getString("DAT_PROC_OPER"));
				dados.setNomUserNameOper(rs.getString("NOM_USERNAME_OPER"));
				dados.setOrgaoLotacaoOper(rs.getString("ORGAO_LOTACAO_OPER"));

				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Relativo ao ConsultaHistPerFunc
	 * ------------------------------------------
	 */
	public void ConsultaDadosHistPerFunc(ConsultaHistoricosBean auxClasse, String codSistema, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select HP.COD_HIST_PERFIL_FUNCAO, HP.ACAO , P.NOM_DESCRICAO AS COD_PERFIL, "
					+ "O.SIG_FUNCAO AS COD_OPERACAO, to_char(HP.DAT_PROC_OPER,'dd/mm/yyyy') as DAT_PROC_OPER, "
					+ "HP.NOM_USERNAME_OPER, ORGOPER.SIG_ORGAO AS ORGAO_LOTACAO_OPER "
					+ "FROM SMIT_ACSS.TCAU_HIST_PERFIL_FUNCAO HP, TCAU_PERFIL P, TCAU_OPERACAO O,SMIT.TSMI_ORGAO ORGOPER "
					+ "WHERE P.COD_SISTEMA = " + codSistema + " " + "AND HP.COD_PERFIL = P.COD_PERFIL "
					+ "AND HP.COD_OPERACAO = O.COD_OPERACAO " + "AND HP.ORGAO_LOTACAO_OPER = ORGOPER.COD_ORGAO ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd += "AND HP.DAT_PROC_OPER >= TO_DATE('" + auxClasse.getDatInicio()
						+ ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			if (!auxClasse.getDatFim().equals(""))
				sCmd += "AND HP.DAT_PROC_OPER <= TO_DATE('" + auxClasse.getDatFim()
						+ ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			sCmd += "ORDER BY HP.COD_PERFIL, HP.COD_OPERACAO";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				PerfilBean dados = new PerfilBean();
				dados.setAcaoAuditoria(rs.getString("ACAO"));
				dados.setCodSistema(rs.getString("COD_OPERACAO"));
				dados.setCodPerfil(rs.getString("COD_PERFIL"));
				dados.setDatProcOper(rs.getString("DAT_PROC_OPER"));
				dados.setNomUserNameOper(rs.getString("NOM_USERNAME_OPER"));
				dados.setOrgaoLotacaoOper(rs.getString("ORGAO_LOTACAO_OPER"));

				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Relativo ao ConsultaSistemaBean
	 * ------------------------------------------
	 */
	public void ConsultaDadosSis(ConsultaSistemaBean auxClasse, String codOrgao, String codUsuario, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = " select perf.cod_perfil,"
					+ " per.nom_descricao,perf.cod_usuario, sis.cod_sistema, sis.nom_abrev,"
					+ " perf.cod_orgao_atuacao,o.nom_orgao,o.sig_orgao, usr.ind_bloqueio "
					+ " from tcau_perfil_orgao perf, tcau_perfil per, tsmi_orgao o, "
					+ " TCAU_SISTEMA sis, tcau_usuario usr " + " where perf.cod_usuario =" + codUsuario
					+ " and per.cod_perfil = perf.cod_perfil " + " and perf.cod_orgao_atuacao = o.cod_orgao"
					+ " and sis.cod_sistema = per.cod_sistema" + " and perf.cod_usuario = usr.cod_usuario ";
			if (auxClasse.getTipoUsuario().equals("I"))
				sCmd += " and usr.ind_bloqueio = 1  ";
			else if (auxClasse.getTipoUsuario().equals("A"))
				sCmd += " and  usr.ind_bloqueio is null ";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			String indBloqueio = "";
			while (rs.next()) {
				ConsultaSistemaBean dados = new ConsultaSistemaBean();
				dados.setCodOrgaoAtuacao(rs.getString("cod_orgao_atuacao"));
				dados.setSigOrgaoAtuacao(rs.getString("sig_orgao"));
				dados.setCodPerfil(rs.getString("cod_perfil"));
				dados.setNomDescricao(rs.getString("nom_descricao"));
				dados.setCodSistema(rs.getString("cod_sistema"));
				dados.setNomAbrev(rs.getString("nom_abrev"));
				indBloqueio = rs.getString("ind_bloqueio");
				if (indBloqueio == null)
					indBloqueio = "";
				auxClasse.getDados().add(dados);
			}
			if (indBloqueio.equals("1"))
				auxClasse.setTipoUsuario("INATIVO");
			else
				auxClasse.setTipoUsuario("ATIVO");

			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	public boolean Le_LogAcesso(LogAcessoBean logAcessoBean, UsuarioBean usuarioBean, SistemaBean sistemaBean,
			OrgaoBean orgaoBean) throws DaoException {
		Connection conn = null;
		List lstUsrSistema = new ArrayList();
		boolean ok = false;
		try {
			String sNomAbrev = "";
			String sNomUsrName = "";

			// Cria a conex�o
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			logAcessoBean.setUsuariosDoSistema(new ArrayList());

			String sCmd2 = "";
			String sCmd = "	SELECT	LA.COD_ORGAO_ATUACAO, 	" + "	LA.COD_ORGAO_LOTACAO,	"
					+ "	OL.SIG_ORGAO ORGAO_LOTACAO, " + "	OA.SIG_ORGAO ORGAO_ATUACAO,	" + "	LA.COD_ACESSO, "
					+ "	LA.NOM_USERNAME, " + "	LA.NOM_ABREV, " + "	LA.NOM_OPERACAO	, 	" + "	LA.SIG_FUNCAO	,	"
					+ "	TO_CHAR(LA.DAT_OPERACAO, 'DD/MM/YYYY:HH24:MI:SS') DATA_OPERACAO "
					+ "	FROM   	TCAU_LOG_ACESSO LA LEFT JOIN TSMI_ORGAO OA "
					+ "	ON 		(LA.COD_ORGAO_ATUACAO = OA.COD_ORGAO) " + "	LEFT 	JOIN TSMI_ORGAO OL "
					+ "	ON 		(LA.COD_ORGAO_LOTACAO = OL.COD_ORGAO) " + "	WHERE  	DAT_OPERACAO >= TO_DATE('"
					+ logAcessoBean.getDatIni() + ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') "
					+ " 	AND 	DAT_OPERACAO <= TO_DATE('" + logAcessoBean.getDatFim()
					+ ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS')";

			if (!orgaoBean.getCodOrgao().equals("0")) {
				sCmd2 = " 	SELECT * " + "	FROM   TSMI_ORGAO " + "	WHERE  COD_ORGAO = " + orgaoBean.getCodOrgao();

				ResultSet rsUsr = stmt.executeQuery(sCmd2);
				if (rsUsr.next())
					orgaoBean.setSigOrgao(rsUsr.getString("SIG_ORGAO"));

			}

			if (!usuarioBean.getCodUsuario().equals("0")) {
				sCmd2 = " 	SELECT * " + "	FROM   TCAU_USUARIO " + "	WHERE  COD_USUARIO = "
						+ usuarioBean.getCodUsuario();

				ResultSet rsUsr = stmt.executeQuery(sCmd2);
				if (rsUsr.next()) {
					sNomUsrName = rsUsr.getString("NOM_USERNAME");
					usuarioBean.setNomUserName(sNomUsrName);
				}

			}

			if (!sistemaBean.getCodSistema().equals("0")) {
				sCmd2 = " 	SELECT  * " + "	FROM    TCAU_SISTEMA " + "	WHERE   COD_SISTEMA = '"
						+ sistemaBean.getCodSistema() + "'";

				ResultSet rsCodSys = stmt.executeQuery(sCmd2);
				if (rsCodSys.next()) {
					sNomAbrev = rsCodSys.getString("NOM_ABREV");
					sistemaBean.setNomSistema(rsCodSys.getString("NOM_SISTEMA"));
				}
			}

			if (!sistemaBean.getCodSistema().equals("0"))
				sCmd += " 	AND LA.NOM_ABREV = '" + sNomAbrev + "'";
			if (!orgaoBean.getCodOrgao().equals("0"))
				sCmd += "	AND	LA.COD_ORGAO_LOTACAO = " + orgaoBean.getCodOrgao();
			if (!usuarioBean.getCodUsuario().equals("0"))
				sCmd += "	AND	NOM_USERNAME = '" + sNomUsrName + "'";

			if (logAcessoBean.getIndLogBKP()) {
				ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
				param.setCodSistema("1"); // M�DULO ACESSO
				param.PreparaParam(conn);
				String instBancoBKP = param.getParamSist("INSTANCIA_BANCO_BKP");

				sCmd += " UNION SELECT	LABKP.COD_ORGAO_ATUACAO, 	" + "	LABKP.COD_ORGAO_LOTACAO,	"
						+ "	OL.SIG_ORGAO ORGAO_LOTACAO, " + "	OA.SIG_ORGAO ORGAO_ATUACAO,	" + "	LABKP.COD_ACESSO, "
						+ "	LABKP.NOM_USERNAME, " + "	LABKP.NOM_ABREV, " + "	LABKP.NOM_OPERACAO	, 	"
						+ "	LABKP.SIG_FUNCAO	,	"
						+ "	TO_CHAR(LABKP.DAT_OPERACAO, 'DD/MM/YYYY:HH24:MI:SS') DATA_OPERACAO "
						+ "	FROM   	TCAU_LOG_ACESSO_BKP" + instBancoBKP + " LABKP LEFT JOIN TSMI_ORGAO OA "
						+ "	ON 		(LABKP.COD_ORGAO_ATUACAO = OA.COD_ORGAO) " + "	LEFT 	JOIN TSMI_ORGAO OL "
						+ "	ON 		(LABKP.COD_ORGAO_LOTACAO = OL.COD_ORGAO) "
						+ "	WHERE  	LABKP.DAT_OPERACAO >= TO_DATE('" + logAcessoBean.getDatIni()
						+ ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') " + " 	AND 	LABKP.DAT_OPERACAO <= TO_DATE('"
						+ logAcessoBean.getDatFim() + ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS')";
				if (!sistemaBean.getCodSistema().equals("0"))
					sCmd += " 	AND LABKP.NOM_ABREV = '" + sNomAbrev + "'";
				if (!orgaoBean.getCodOrgao().equals("0"))
					sCmd += "	AND	LABKP.COD_ORGAO_LOTACAO = " + orgaoBean.getCodOrgao();
				if (!usuarioBean.getCodUsuario().equals("0"))
					sCmd += "	AND	NOM_USERNAME = '" + sNomUsrName + "'";

			}

			ResultSet rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				LogAcessoBean logAcesso = new LogAcessoBean();
				logAcesso.setCodAcesso(rs.getString("COD_ACESSO"));
				logAcesso.setNomUserName(rs.getString("NOM_USERNAME"));
				logAcesso.setNomAbrev(rs.getString("NOM_ABREV"));
				logAcesso.setNomOperacao(rs.getString("NOM_OPERACAO"));
				logAcesso.setSigFuncao(rs.getString("SIG_FUNCAO"));
				logAcesso.setDatOperacao(rs.getString("DATA_OPERACAO"));
				logAcesso.setSigOrgao(rs.getString("ORGAO_LOTACAO"));
				logAcesso.setSigOrgaoAtuacao(rs.getString("ORGAO_ATUACAO"));
				logAcesso.setCodOrgaoAtuacao(rs.getString("COD_ORGAO_ATUACAO"));
				logAcesso.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				lstUsrSistema.add(logAcesso);
				ok = true;
			}

			logAcessoBean.setListaTotal(lstUsrSistema);
			rs.close();
			stmt.close();
			return ok;

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	/**
	 * ----------------------------------------------------------- DAO relativo
	 * � GrupoParamBean
	 * -----------------------------------------------------------
	 */

	public boolean GrupoParamInsert(GrupoParamBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd;
			sCmd = "INSERT INTO TCAU_GRUPO_PARAM (cod_grupo, dsc_grupo) ";
			sCmd += "VALUES (" + auxClasse.getCodGrupo() + ", ";
			sCmd += "'" + auxClasse.getDscGrupo() + "')";
			stmt.execute(sCmd);

			auxClasse.setMsgErro("");
			auxClasse.setPkid(auxClasse.getCodGrupo());
			conn.commit();
			stmt.close();
			stmt = null;

			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean GrupoParamUpdate(GrupoParamBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TCAU_GRUPO_PARAM set ";
			sCmd += "dsc_grupo = '" + auxClasse.getDscGrupo() + "'";
			sCmd += " where cod_grupo =" + auxClasse.getCodGrupo();
			stmt.execute(sCmd);

			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean GrupoParamDelete(GrupoParamBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE TCAU_GRUPO_PARAM ";
			sCmd += " where cod_grupo = " + auxClasse.getCodGrupo();
			stmt.execute(sCmd);

			// Prepara para a exclus�o via Broker
			// StatusBroker(auxClasse, "3");
			auxClasse.setPkid("0");

			conn.commit();
			stmt.close();
			stmt = null;
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	public boolean GrupoParamExiste(GrupoParamBean auxClasse) throws sys.DaoException {

		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;

		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				conn = serviceloc.getConnection(MYABREVSIST);
				eConnLocal = true;
			}
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_grupo = " + auxClasse.getCodGrupo();
			String sCmd = "SELECT dsc_grupo from TCAU_GRUPO_PARAM " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
				auxClasse.setDscGrupo(rs.getString("dsc_grupo"));
			}
			rs.close();
			stmt.close();
			stmt = null;

		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	// Metodos relativos a tela de GrupoParamBean no REC
	public void getGrupoParam(GrupoParamBean myGrupo) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT cod_grupo,dsc_grupo";
			sCmd += " from TCAU_GRUPO_PARAM";
			sCmd += " order by dsc_grupo";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			Vector grupo = new Vector();
			while (rs.next()) {
				GrupoParamBean myGr = new GrupoParamBean();
				myGr.setCodGrupo(rs.getString("cod_grupo"));
				myGr.setDscGrupo(rs.getString("dsc_grupo"));
				grupo.addElement(myGr);
			}
			myGrupo.setGrupo(grupo);
			myGrupo.setGrupoGravado(grupo);
			rs.close();
			stmt.close();
		} // fim do try
		catch (SQLException sqle) {
			myGrupo.setMsgErro("GrupoParam008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myGrupo.setCodGrupo("0");
		} catch (Exception e) {
			myGrupo.setMsgErro("GrupoParam008 - Leitura: " + e.getMessage());
			myGrupo.setCodGrupo("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean InsertGrupoParam(GrupoParamBean myGrupo, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			Vector GrupoAtual = myGrupo.getGrupo();
			Vector GrupoCorrespondente = myGrupo.getGrupoGravado();

			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < GrupoAtual.size(); i++) {
				GrupoParamBean grupo = (GrupoParamBean) GrupoAtual.elementAt(i);
				GrupoParamBean grupoCorresp = (GrupoParamBean) GrupoCorrespondente.elementAt(i);

				if ("".equals(grupo.getDscGrupo().trim()) == false) {
					if (grupoCorresp.getCodGrupo().equals(grupo.getCodGrupo()))
						GrupoParamUpdate(grupo);
					else
						GrupoParamInsert(grupo);
				} else {
					if ("".equals(grupoCorresp.getCodGrupo()) == false) {
						if ("".equals(grupo.getCodGrupo())) {
							if ("".equals(grupoCorresp.getCodGrupo()) == false)
								GrupoParamDelete(grupoCorresp);
						}
					}
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		myGrupo.setMsgErro(vErro);
		return retorno;
	}

	// ----------------- Operacao Bean -----------------------------------
	/**
	 * M�todo que verifica as fun��es dado um sistema
	 * 
	 * @param funcao
	 * @return operacao
	 * @throws DaoException
	 */
	public String getFuncao(OperacaoBean funcao, SistemaBean mySist) throws DaoException {
		String operacao = "";
		String subPai = "";
		int nivel = 1;
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			// Carrega vetor com as operacoes ja ordenadas
			SistemaGetFuncoes(mySist);

			Iterator itCombo = mySist.getFuncoes().iterator();
			ACSS.OperacaoBean funcoesCombo = new ACSS.OperacaoBean();
			while (itCombo.hasNext()) {
				// lista os dados da combo
				funcoesCombo = (ACSS.OperacaoBean) itCombo.next();
				operacao += ",\"" + funcoesCombo.getCodOperacao() + "\"";

				// Carrega campo nomOperacao de acordo com os niveis
				if (!funcoesCombo.getCodOperacaoFk().equals("")) {
					// verifica os filhos
					if (!funcoesCombo.getNomExecuta().equals("")) {
						// Verifica filhos de subPai
						if (funcoesCombo.getCodOperacaoFk().equals(subPai))
							operacao += ",\"" + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
									+ funcoesCombo.getNomOperacao() + "\"";
						else
							operacao += ",\"" + "&nbsp;&nbsp;&nbsp;&nbsp;" + funcoesCombo.getNomOperacao() + "\"";
					}
					if (funcoesCombo.getNomExecuta().equals("")) {
						operacao += ",\"" + "&nbsp;&nbsp;&nbsp;&nbsp;" + funcoesCombo.getNomOperacao() + "\"";
						subPai = funcoesCombo.getCodOperacao();
						nivel++;
					}
				}
				// verifica os pai
				else
					operacao += ",\"" + funcoesCombo.getNomOperacao() + "\"";

			}

			operacao = operacao.substring(1, operacao.length());

		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return operacao;
	}

	/*----------------------------------------
	 * DAO Relativo ao ConsultaAcessoFuncao
	 * @author Luciana Rocha
	 ----------------------------------------- */
	public void ConsultaAcessoFuncao(ConsultaAcessoFuncaoBean auxClasse, String codOperacao, String codSistema)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "select  perf.cod_perfil,perf.nom_descricao, "
					+ "usr.cod_usuario, usr.nom_usuario,o.cod_orgao, "
					+ "orgaoLot.sig_orgao as OrgaoLotacao,per.cod_orgao_atuacao,o.sig_orgao "
					+ "FROM TSMI_ORGAO o,TCAU_USUARIO usr,TCAU_PERFIL_ORGAO per, "
					+ "TCAU_SISTEMA sis,TCAU_PERFIL perf , " + "tcau_acesso a ,(Select * From tsmi_orgao o) orgaoLot "
					+ "WHERE  sis.cod_sistema = '" + codSistema + "' and " + "a.cod_operacao = '" + codOperacao + "' "
					+ "and per.cod_usuario = usr.cod_usuario " + "and per.cod_orgao_atuacao = o.cod_orgao "
					+ "and per.cod_perfil  = perf.cod_perfil " + "and a.cod_perfil = per.cod_perfil "
					+ "and usr.cod_orgao = orgaoLot.cod_orgao " + "order by usr.nom_usuario";

			ResultSet rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				ConsultaAcessoFuncaoBean dados = new ConsultaAcessoFuncaoBean();
				dados.setNomUsuario(rs.getString("nom_usuario"));
				dados.setSigOrgaoAtuacao(rs.getString("sig_orgao"));
				dados.setSigOrgaoLotacao(rs.getString("OrgaoLotacao"));
				dados.setCodPerfil(rs.getString("cod_perfil"));
				dados.setNomDescricao(rs.getString("nom_descricao"));
				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/*---------------------------------------------
	 * Dao Relativo ao ConsultaFuncoesperfilBean
	 * @author loliveira
	 * ----------------------------------------------*/

	public void consultaFuncoesDoPerfil(ConsultaFuncoesPerfilBean auxClasse, String codSistema, String codPerfil,
			SistemaBean SistBeanId) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "SELECT o.nom_executa,o.sig_funcao, o.nom_operacao, perf.cod_perfil,"
					+ "o.cod_Operacao_Fk, o.cod_operacao, o.sig_funcao, o.sig_funcao, pai.sig_funcao sig_funcao_pai,o.sit_biometria "
					+ "   from TCAU_OPERACAO o, TCAU_ACESSO a, TCAU_PERFIL perf,TCAU_OPERACAO pai "
					+ "   WHERE o.cod_sistema = '" + codSistema + "'" + "   and perf.cod_perfil = '" + codPerfil + "' "
					+ "   and perf.cod_perfil = a.cod_perfil " + "   and o.cod_operacao = a.cod_operacao "
					+ "   and perf.cod_perfil = a.cod_perfil " + "   and o.cod_sistema = perf.cod_sistema "
					+ " and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao "
					+ " and o.cod_operacao_fk is null " + "   order by o.sig_funcao, o.num_ordem";

			ResultSet rs = stmt.executeQuery(sCmd);

			Vector funcoesOrdenadas = new Vector();
			while (rs.next()) {
				OperacaoBean myOper = new OperacaoBean();
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setCodOperacaoFk(rs.getString("cod_Operacao_Fk"));
				myOper.setCodOperacao(rs.getString("cod_Operacao"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setSitBiometria(rs.getString("sit_biometria"));
				funcoesOrdenadas.addElement(myOper);
				acharFilhos(conn, myOper, SistBeanId, funcoesOrdenadas);
			}
			auxClasse.setDados(funcoesOrdenadas);
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/*----------------------------------------------
	 * Dao Relativo ao ConsultaUsuarioPerfilBean
	 * @author loliveira
	 -----------------------------------------------*/

	public void consultaUsuarioDoPerfil(ConsultaUsuarioPerfilBean auxClasse, String codSistema, String codPerfil)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select per.cod_usuario, per.cod_orgao_atuacao, atu.sig_orgao, per.cod_perfil, "
					+ " perf.cod_perfil,perf.nom_descricao, usr.cod_usuario, usr.nom_usuario,usr.ind_bloqueio, "
					+ " usr.cod_orgao, orgaoLot.sig_orgao as OrgaoLotacao FROM "
					+ " TSMI_ORGAO atu,TCAU_USUARIO usr,TCAU_PERFIL_ORGAO per, "
					+ " TCAU_SISTEMA sis,TCAU_PERFIL perf ,(Select * From tsmi_orgao o) orgaoLot "
					+ " WHERE sis.cod_sistema = '" + codSistema + "' and per.cod_perfil = '" + codPerfil + "' "
					+ " and sis.cod_sistema = perf.cod_sistema and " + " per.cod_perfil  = perf.cod_perfil and "
					+ " per.cod_orgao_atuacao = atu.cod_orgao and " + " per.cod_usuario = usr.cod_usuario and ";
			if (auxClasse.getTipoUsuario().equals("I"))
				sCmd += "usr.ind_bloqueio = 1 and";
			else if (auxClasse.getTipoUsuario().equals("A"))
				sCmd += "(usr.ind_bloqueio is null or usr.ind_bloqueio = 0) and";
			sCmd += " usr.cod_orgao = orgaoLot.cod_orgao order by OrgaoLotacao";

			ResultSet rs = stmt.executeQuery(sCmd);
			String indBloqueio = "";
			while (rs.next()) {
				ConsultaUsuarioPerfilBean dados = new ConsultaUsuarioPerfilBean();
				dados.setNomUsuario(rs.getString("nom_usuario"));
				dados.setSigOrgaoAtuacao(rs.getString("sig_orgao"));
				dados.setSigOrgaoLotacao(rs.getString("OrgaoLotacao"));
				indBloqueio = rs.getString("ind_bloqueio");
				if (indBloqueio == null)
					indBloqueio = "";
				if (indBloqueio.equals("1"))
					dados.setTipoUsuario("INATIVO");
				else
					dados.setTipoUsuario("ATIVO");
				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	public int verificaNumRegistro(SistemaBean sistBeanId, UsuarioBean usuarioBeanId, OrgaoBean orgaoBeanId,
			LogAcessoBean logAcessoBean) throws DaoException {
		Connection conn = null;
		String sCmd = "";
		int totalRegistro = 0;
		String nomUsrName = "";
		String nomAbrev = "";
		String condicao = "";
		ResultSet rs;
		try {
			// Cria a conex�o
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			// Concatena clausula where
			if (!usuarioBeanId.getCodUsuario().equals("0")) {
				sCmd = " SELECT nom_username FROM tcau_usuario WHERE cod_usuario = '" + usuarioBeanId.getCodUsuario()
						+ "'";
				rs = stmt.executeQuery(sCmd);
				if (rs.next()) {
					nomUsrName = rs.getString("nom_username");
					condicao += " and nom_username = '" + nomUsrName + "'";
				}
			}

			if (!orgaoBeanId.getCodOrgao().equals("")) {
				condicao += " and cod_orgao_lotacao = '" + orgaoBeanId.getCodOrgao() + "'";
			}

			// verifica sigla do sistema
			sCmd = "SELECT nom_abrev FROM tcau_Sistema WHERE cod_sistema = '" + sistBeanId.getCodSistema() + "'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				nomAbrev = rs.getString("nom_abrev");
			}

			sCmd = "SELECT COUNT(*) as total FROM TCAU_LOG_ACESSO WHERE  	DAT_OPERACAO BETWEEN TO_DATE('"
					+ logAcessoBean.getDatIni() + "', 'DD/MM/YYYY') " + " and TO_DATE('" + logAcessoBean.getDatFim()
					+ "', 'DD/MM/YYYY') " + " and nom_abrev = '" + nomAbrev + "' " + condicao;
			rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				totalRegistro = rs.getInt("total");
				logAcessoBean.setTamTotal(totalRegistro);
			}
			rs.close();
			stmt.close();
			return totalRegistro;

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	/*----------------------------------------------
	 * Dao Relativo ao NoticiaBean
	 * @author Glaucio Jannotti
	 -----------------------------------------------*/

	public NoticiaBean[] carregaNoticias(UsuarioBean usuario) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_NOTICIA, DSC_NOTICIA, TXT_NOTICIA, IND_PRIORIDADE, DAT_INICIO, DAT_FIM, COD_USUARIO, DAT_ENVIO "
					+ "FROM TCAU_NOTICIA WHERE (SYSDATE BETWEEN DAT_INICIO AND DAT_FIM) ";

			ResultSet rs = stmt.executeQuery(sql);
			Vector vetNoticias = new Vector();
			while (rs.next()) {
				// Cria o Bean de Noticia
				NoticiaBean noticia = new NoticiaBean();
				noticia.setCodNoticia(rs.getInt("COD_NOTICIA"));
				noticia.setDscNoticia(rs.getString("DSC_NOTICIA"));
				noticia.setTxtNoticia(rs.getString("TXT_NOTICIA"));
				noticia.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				noticia.setDatInicio(rs.getDate("DAT_INICIO"));
				noticia.setDatFim(rs.getDate("DAT_FIM"));
				noticia.setUsuario(usuario);
				noticia.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				noticia.setUsuario(usuarioBean);

				vetNoticias.add(noticia);
			}
			NoticiaBean[] colNoticias = new NoticiaBean[vetNoticias.size()];
			vetNoticias.toArray(colNoticias);
			rs.close();
			stmt.close();
			conn.close();

			return colNoticias;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public NoticiaBean consultaNoticia(int codNoticia) throws DaoException {

		NoticiaBean noticia = null;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";

			sql = "SELECT COD_NOTICIA, DSC_NOTICIA, TXT_NOTICIA, IND_PRIORIDADE, DAT_INICIO, DAT_FIM, COD_USUARIO, DAT_ENVIO"
					+ " FROM TCAU_NOTICIA WHERE COD_NOTICIA = " + codNoticia;

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				// Cria o Bean de Noticia
				noticia = new NoticiaBean();
				noticia.setCodNoticia(rs.getInt("COD_NOTICIA"));
				noticia.setDscNoticia(rs.getString("DSC_NOTICIA"));
				noticia.setTxtNoticia(rs.getString("TXT_NOTICIA"));
				noticia.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				noticia.setDatInicio(rs.getDate("DAT_INICIO"));
				noticia.setDatFim(rs.getDate("DAT_FIM"));
				noticia.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				noticia.setUsuario(usuarioBean);
			}
			rs.close();
			stmt.close();
			conn.close();

			return noticia;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public void leNoticia(NoticiaBean noticia, UsuarioBean usuario) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");

			String sql = "INSERT INTO TCAU_NOTICIA_LEITURA (COD_NOTICIA_LEITURA,COD_NOTICIA,COD_USUARIO,DAT_LEITURA)"
					+ " VALUES (SEQ_TCAU_NOTICIA_LEITURA.NEXTVAL, " + noticia.getCodNoticia() + ", "
					+ usuario.getCodUsuario() + ", " + "TO_DATE('" + df.format(new Date())
					+ "','DD/MM/YYYY HH24:MI:SS'))";
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public boolean noticiaLida(NoticiaBean aviso, UsuarioBean usuario) throws DaoException {

		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COUNT(*) AS QTD FROM TCAU_NOTICIA_LEITURA WHERE COD_NOTICIA = " + aviso.getCodNoticia()
					+ " AND COD_USUARIO = " + usuario.getCodUsuario();
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next() && (rs.getInt("QTD") > 0))
				retorno = true;

			rs.close();
			stmt.close();

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}

	public NoticiaBean[] pegaprioridadeNoticias(UsuarioBean usuario, boolean naoLidas) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_NOTICIA, DSC_NOTICIA, TXT_NOTICIA, IND_PRIORIDADE, DAT_INICIO, DAT_FIM, COD_USUARIO, DAT_ENVIO"
					+ " FROM TCAU_NOTICIA WHERE (SYSDATE BETWEEN DAT_INICIO AND DAT_FIM) AND IND_PRIORIDADE ='A' ";

			if (naoLidas)
				sql += " AND (COD_NOTICIA NOT IN (SELECT COD_NOTICIA FROM TCAU_NOTICIA_LEITURA WHERE COD_USUARIO = "
						+ usuario.getCodUsuario() + "))";
			ResultSet rs = stmt.executeQuery(sql);
			Vector vetNoticias = new Vector();
			while (rs.next()) {
				// Cria o Bean de Noticia
				NoticiaBean noticia = new NoticiaBean();
				noticia.setCodNoticia(rs.getInt("COD_NOTICIA"));
				noticia.setDscNoticia(rs.getString("DSC_NOTICIA"));
				noticia.setTxtNoticia(rs.getString("TXT_NOTICIA"));
				noticia.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				noticia.setDatInicio(rs.getDate("DAT_INICIO"));
				noticia.setDatFim(rs.getDate("DAT_FIM"));
				noticia.setUsuario(usuario);
				noticia.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				noticia.setUsuario(usuarioBean);

				vetNoticias.add(noticia);
			}
			NoticiaBean[] colNoticias = new NoticiaBean[vetNoticias.size()];
			vetNoticias.toArray(colNoticias);
			rs.close();
			stmt.close();
			conn.close();

			return colNoticias;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public NoticiaBean[] MostraNoticiasEntreDatas(UsuarioBean usuario, String dat1, String dat2) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_NOTICIA, DSC_NOTICIA, TXT_NOTICIA, IND_PRIORIDADE, DAT_INICIO, DAT_FIM, COD_USUARIO, DAT_ENVIO"
					+ " FROM TCAU_NOTICIA WHERE (TRUNC(DAT_INICIO) BETWEEN '" + dat1 + "' AND '" + dat2
					+ "') AND (TRUNC(DAT_FIM) < TRUNC(SYSDATE) )";

			ResultSet rs = stmt.executeQuery(sql);

			Vector vetNoticias = new Vector();
			while (rs.next()) {
				// Cria o Bean de Noticia
				NoticiaBean noticia = new NoticiaBean();
				noticia.setCodNoticia(rs.getInt("COD_NOTICIA"));
				noticia.setDscNoticia(rs.getString("DSC_NOTICIA"));
				noticia.setTxtNoticia(rs.getString("TXT_NOTICIA"));
				noticia.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				noticia.setDatInicio(rs.getDate("DAT_INICIO"));
				noticia.setDatFim(rs.getDate("DAT_FIM"));
				noticia.setUsuario(usuario);
				noticia.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				noticia.setUsuario(usuarioBean);

				vetNoticias.add(noticia);
			}
			NoticiaBean[] colNoticias = new NoticiaBean[vetNoticias.size()];
			vetNoticias.toArray(colNoticias);
			rs.close();
			stmt.close();
			conn.close();

			return colNoticias;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	/*----------------------------------------------
	 * Dao Relativo ao AvisoBean
	 * @author Glaucio Jannotti
	 -----------------------------------------------*/

	public AvisoBean[] carregaAvisos(UsuarioBean usuario, UsuarioFuncBean usuarioFunc, boolean naoLidos)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_AVISO, DSC_AVISO, TXT_AVISO, IND_PRIORIDADE, COD_USUARIO, DAT_ENVIO FROM TCAU_AVISO"
					+ " WHERE (COD_AVISO IN (SELECT COD_AVISO FROM TCAU_AVISO_DESTINO WHERE COD_USUARIO = "
					+ usuario.getCodUsuario() + " OR COD_PERFIL = " + usuarioFunc.getCodPerfil() + " OR COD_SISTEMA = "
					+ usuarioFunc.getCodSistema() + " OR COD_ORGAO = " + usuario.getCodOrgaoAtuacao() + "))";

			if (naoLidos)
				sql += " AND (COD_AVISO NOT IN (SELECT COD_AVISO FROM TCAU_AVISO_LEITURA WHERE COD_USUARIO = "
						+ usuario.getCodUsuario() + "))";

			ResultSet rs = stmt.executeQuery(sql);
			Vector vetAvisos = new Vector();
			while (rs.next()) {
				// Cria o Bean de Aviso
				AvisoBean aviso = new AvisoBean();
				aviso.setCodAviso(rs.getInt("COD_AVISO"));
				aviso.setDscAviso(rs.getString("DSC_AVISO"));
				aviso.setTxtAviso(rs.getString("TXT_AVISO"));
				aviso.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				aviso.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				aviso.setUsuario(usuarioBean);

				vetAvisos.add(aviso);
			}
			AvisoBean[] colAvisos = new AvisoBean[vetAvisos.size()];
			vetAvisos.toArray(colAvisos);
			rs.close();
			stmt.close();

			return colAvisos;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public AvisoBean consultaAviso(int codAviso) throws DaoException {

		AvisoBean aviso = null;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";

			sql = "SELECT COD_AVISO, DSC_AVISO, TXT_AVISO, IND_PRIORIDADE, COD_USUARIO, DAT_ENVIO"
					+ " FROM TCAU_AVISO WHERE COD_AVISO = " + codAviso;

			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				// Cria o Bean de Aviso
				aviso = new AvisoBean();
				aviso.setCodAviso(rs.getInt("COD_AVISO"));
				aviso.setDscAviso(rs.getString("DSC_AVISO"));
				aviso.setTxtAviso(rs.getString("TXT_AVISO"));
				aviso.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				aviso.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				aviso.setUsuario(usuarioBean);
			}
			rs.close();
			stmt.close();

			return aviso;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public void leAviso(AvisoBean aviso, UsuarioBean usuario) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");

			String sql = "INSERT INTO TCAU_AVISO_LEITURA (COD_AVISO_LEITURA,COD_AVISO,COD_USUARIO,DAT_LEITURA)"
					+ " VALUES (SEQ_TCAU_AVISO_LEITURA.NEXTVAL, " + aviso.getCodAviso() + ", " + usuario.getCodUsuario()
					+ ", " + "TO_DATE('" + df.format(new Date()) + "','DD/MM/YYYY HH24:MI:SS'))";
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public boolean avisoLido(AvisoBean aviso, UsuarioBean usuario) throws DaoException {

		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COUNT(*) AS QTD FROM TCAU_AVISO_LEITURA WHERE COD_AVISO = " + aviso.getCodAviso()
					+ " AND COD_USUARIO = " + usuario.getCodUsuario();
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next() && (rs.getInt("QTD") > 0))
				retorno = true;

			rs.close();
			stmt.close();

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}

	public AvisoBean[] pegaprioridadeAviso(UsuarioBean usuario, UsuarioFuncBean usuarioFunc, boolean naoLidos)
			throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_AVISO, DSC_AVISO, TXT_AVISO, IND_PRIORIDADE, COD_USUARIO, DAT_ENVIO FROM TCAU_AVISO"
					+ " WHERE (COD_AVISO IN (SELECT COD_AVISO FROM TCAU_AVISO_DESTINO WHERE COD_USUARIO = "
					+ usuario.getCodUsuario() + " OR COD_PERFIL = " + usuarioFunc.getCodPerfil() + " OR COD_SISTEMA = "
					+ usuarioFunc.getCodSistema() + " OR COD_ORGAO = " + usuario.getCodOrgaoAtuacao()
					+ ")) AND IND_PRIORIDADE='A' ";
			if (naoLidos)
				sql += " AND (COD_AVISO NOT IN (SELECT COD_AVISO FROM TCAU_AVISO_LEITURA WHERE COD_USUARIO = "
						+ usuario.getCodUsuario() + "))";

			ResultSet rs = stmt.executeQuery(sql);
			Vector vetAvisos = new Vector();
			while (rs.next()) {
				// Cria o Bean de Aviso
				AvisoBean aviso = new AvisoBean();
				aviso.setCodAviso(rs.getInt("COD_AVISO"));
				aviso.setDscAviso(rs.getString("DSC_AVISO"));
				aviso.setTxtAviso(rs.getString("TXT_AVISO"));
				aviso.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				aviso.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				aviso.setUsuario(usuarioBean);

				vetAvisos.add(aviso);
			}
			AvisoBean[] colAvisos = new AvisoBean[vetAvisos.size()];
			vetAvisos.toArray(colAvisos);
			rs.close();
			stmt.close();

			return colAvisos;

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public AvisoBean[] MostraAvisosEntreDatas(UsuarioBean usuario, String dat1, String dat2) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sql = "SELECT COD_AVISO,DSC_AVISO,TXT_AVISO,IND_PRIORIDADE,COD_USUARIO,DAT_ENVIO "
					+ " FROM TCAU_AVISO WHERE (TRUNC(DAT_ENVIO) >='" + dat1 + "' AND TRUNC(DAT_ENVIO) <='" + dat2
					+ "')";

			ResultSet rs = stmt.executeQuery(sql);

			Vector vetAvisos = new Vector();
			while (rs.next()) {
				AvisoBean aviso = new AvisoBean();
				aviso.setCodAviso(rs.getInt("COD_AVISO"));
				aviso.setDscAviso(rs.getString("DSC_AVISO"));
				aviso.setTxtAviso(rs.getString("TXT_AVISO"));
				aviso.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				aviso.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				aviso.setUsuario(usuarioBean);

				vetAvisos.add(aviso);
			}
			AvisoBean[] colAviso = new AvisoBean[vetAvisos.size()];
			vetAvisos.toArray(colAviso);
			rs.close();
			stmt.close();
			return colAviso;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	/*-----------------------------------------------------------
	  * DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean TcauAbreSessaoInsert(String num_sessao) throws DaoException { // dgeraldes
		boolean existe = false;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "INSERT INTO TCAU_SESSAO (NUM_SESSAO, DAT_SESSAO)";
			sCmd += " VALUES (";
			sCmd += "'" + num_sessao + "',";
			sCmd += " sysdate" + ")";

			stmt.execute(sCmd);
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/*-----------------------------------------------------------
	  * DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean TcauFechaSessaoUpdate(String num_sessao) throws DaoException { // dgeraldes
		boolean existe = false;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "UPDATE TCAU_SESSAO SET ";
			sCmd += " DAT_FIM_SESSAO = ";
			sCmd += "sysdate";
			sCmd += ", SIT_SESSAO = " + "'0'";
			sCmd += " WHERE NUM_SESSAO = " + "'" + num_sessao + "'";

			stmt.execute(sCmd);
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public void LimpaTcauSessao() throws DaoException {
		Connection conn = null;
		// verifica sess�es com termino anormal
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TCAU_SESSAO SET " + " DAT_FIM_SESSAO = sysdate," + " SIT_SESSAO = '1'"
					+ " WHERE DAT_FIM_SESSAO IS NULL" + " AND DAT_SESSAO IS NOT NULL ";
			stmt.execute(sCmd);
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/*-----------------------------------------------------------
	* DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaTcauSessao(sys.ControleSessaoBean sessao, String num_sessao) throws DaoException {
		boolean bOk = true;
		String cod_orgao_lotacao = "";
		String nom_username = "";
		Connection conn = null;

		// Verifica se "QUEM" esta preenchido
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT NOM_USERNAME, COD_ORGAO_LOTACAO from TCAU_SESSAO ";
			sCmd += " WHERE NUM_SESSAO = " + "'" + num_sessao + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				sessao.setcodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				sessao.setnomUserName(rs.getString("NOM_USERNAME"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean AtualizaSessao(sys.ControleSessaoBean sessao) throws DaoException { // dgeraldes
		boolean existe = false;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "UPDATE TCAU_SESSAO SET ";
			sCmd += " NOM_USERNAME = ";
			sCmd += "'" + sessao.getnomUserName() + "'";
			sCmd += " , COD_SISTEMA = ";
			sCmd += sessao.getcodSistema();
			sCmd += " , COD_ORGAO_LOTACAO = ";
			sCmd += "'" + sessao.getcodOrgaoLotacao() + "'";
			sCmd += " , COD_ORGAO_ATUACAO = ";
			sCmd += "'" + sessao.getcodOrgaoAtuacao() + "'";
			sCmd += ", IP = " + "'" + sessao.getIp() + "'";
			sCmd += " WHERE NUM_SESSAO = " + "'" + sessao.getNumSessao() + "'";

			stmt.execute(sCmd);
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Sessao
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaUsuarioLogado(ConsultaUsuarioLogadoBean auxClasse) throws DaoException {
		boolean bOk = true;
		Connection conn = null;
		String sCmd2 = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			String sCmd = "SELECT DISTINCT SESS.NOM_USERNAME, SESS.NUM_SESSAO, SESS.COD_ORGAO_LOTACAO, to_char(SESS.DAT_SESSAO, 'DD/MM/YYYY HH24:MI:SS') as DAT_SESSAO, SESS.IP, SIST.NOM_ABREV AS SISTEMA, USU.COD_USUARIO, OA.sig_orgao as ORGAO_ATU "
					+ "FROM TCAU_SESSAO SESS, TCAU_SISTEMA SIST, TCAU_USUARIO USU, TCAU_SISTEMA_BLOQUEIO BLOQ, TSMI_ORGAO OA";
			sCmd += " WHERE SESS.NOM_USERNAME IS NOT NULL AND SESS.DAT_FIM_SESSAO IS NULL "
					+ "AND OA.COD_ORGAO = SESS.COD_ORGAO_ATUACAO AND SESS.COD_SISTEMA = SIST.COD_SISTEMA "
					+ "AND SESS.NOM_USERNAME = USU.NOM_USERNAME AND SESS.COD_ORGAO_LOTACAO = USU.COD_ORGAO "
					+ "AND USU.COD_USUARIO = BLOQ.COD_USUARIO_BL(+)";

			if (auxClasse.getCodSistema().equals("0") == false) {
				sCmd += " AND SESS.COD_SISTEMA = " + "'" + auxClasse.getCodSistema() + "'";
			}
			if (auxClasse.getCodOrgaoAtuacao().equals("0") == false) {
				sCmd += " AND SESS.COD_ORGAO_ATUACAO = " + "'" + auxClasse.getCodOrgaoAtuacao() + "'";
			}

			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs2 = stmt2.executeQuery(sCmd);
			while (rs.next()) {
				ConsultaUsuarioLogadoBean dados = new ConsultaUsuarioLogadoBean();
				dados.setNomUserName(rs.getString("NOM_USERNAME"));
				dados.setDataSessao(rs.getString("DAT_SESSAO"));
				dados.setIp(rs.getString("IP"));
				dados.setSistema(rs.getString("SISTEMA"));
				dados.setCodUsuario(rs.getString("COD_USUARIO"));
				dados.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				dados.setSigOrgaoAtuacao(rs.getString("ORGAO_ATU"));
				dados.setNumSessao(rs.getString("NUM_SESSAO"));
				sCmd2 = "SELECT DISTINCT SIG_FUNCAO, max(DAT_OPERACAO) " + " FROM TCAU_LOG_ACESSO "
						+ "WHERE NUM_SESSAO = " + "'" + dados.getNumSessao() + "'" + " GROUP BY SIG_FUNCAO";
				rs2 = stmt2.executeQuery(sCmd2);
				if (rs2.next())
					dados.setSigFuncao(rs2.getString("SIG_FUNCAO"));
				if (dados.VerificaBloqueio(rs.getString("COD_USUARIO")))
					dados.setflBloqueado("1");
				else
					dados.setflBloqueado("0");
				auxClasse.getDados().add(dados);
			}
			rs.close();
			rs2.close();
			stmt.close();
			stmt2.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*----------------------------------------------
	 * Dao Relativo ao ConsultaSessaoBean
	 * @author loliveira
	 * -----------------------------------------------
	 */
	public void consultaHistoricoSessoes(ConsultaSessaoBean consultaSessaoBean) throws DaoException {

		Connection conn = null;
		String condicao = " WHERE sess.dat_fim_sessao is not null AND sess.cod_sistema = sist.cod_sistema and OL.cod_orgao = sess.cod_orgao_lotacao and OA.cod_orgao = sess.cod_orgao_atuacao and trunc(dat_sessao) BETWEEN trunc(TO_DATE('"
				+ consultaSessaoBean.getDataIni() + "', 'dd/mm/yyyy')) and trunc(TO_DATE('"
				+ consultaSessaoBean.getDataFim() + "', 'dd/mm/yyyy'))";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			consultaSessaoBean.setDados(new ArrayList());

			if (!consultaSessaoBean.getCodOrgaoAtuacao().equals("0"))
				condicao += " AND sess.cod_orgao_atuacao = '" + consultaSessaoBean.getCodOrgaoAtuacao() + "'";
			if (!consultaSessaoBean.getCodSistema().equals("0"))
				condicao += " AND sess.cod_sistema = '" + consultaSessaoBean.getCodSistema() + "'";
			if (!consultaSessaoBean.getTermAnormal().equals("0"))
				condicao += " AND sess.sit_sessao = '" + consultaSessaoBean.getTermAnormal() + "'";

			String sCmd = "SELECT sess.num_sessao, sess.cod_sistema, TO_CHAR(sess.dat_sessao, 'DD/MM/YYYY - HH24:MI:SS')AS dataSessao, TO_CHAR(sess.dat_fim_sessao, 'DD/MM/YYYY - HH24:MI:SS')AS dataFimSessao, "
					+ "sess.cod_orgao_lotacao, sess.nom_username, sess.cod_orgao_atuacao, sess.ip, sist.nom_abrev, OA.sig_orgao as ORGAO_ATU, OL.sig_orgao as ORGAO_LOT  "
					+ "FROM TCAU_SESSAO sess, TCAU_SISTEMA sist, TSMI_ORGAO OA, TSMI_ORGAO OL" + condicao;

			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ConsultaSessaoBean dados = new ConsultaSessaoBean();
				dados.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				dados.setCodOrgaoAtuacao(rs.getString("cod_orgao_atuacao"));
				dados.setNumSessao(rs.getString("num_sessao"));
				dados.setDataSessao(rs.getString("dataSessao"));
				dados.setCodSistema(rs.getString("cod_sistema"));
				dados.setNomSistema(rs.getString("nom_abrev"));
				dados.setNomUsrName(rs.getString("nom_username"));
				dados.setDataSessaoFim(rs.getString("dataFimSessao"));
				dados.setIp(rs.getString("ip"));
				dados.setSigOrgaoLotacao(rs.getString("ORGAO_LOT"));
				dados.setSigOrgaoAtuacao(rs.getString("ORGAO_ATU"));
				consultaSessaoBean.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Sistema_Bloqueio
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean InsereUsuarioBloqueado(ConsultaUsuarioLogadoBean auxClasse, UsuarioBean myUsr,
			String[] usuarios_bloq) throws DaoException {
		boolean bOk = true;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			for (int i = 0; i < usuarios_bloq.length; i++) {

				String sCmd = "INSERT INTO TCAU_SISTEMA_BLOQUEIO (ID_SISTEMA_BLOQUEIO, " + "DAT_BLOQUEIO, NUM_MINUTOS, "
						+ "COD_USUARIO_PROC, COD_USUARIO_BL, DAT_PROCESSAMENTO)";
				sCmd += " VALUES (";
				sCmd += "SEQ_TCAU_SISTEMA_BLOQUEIO.nextval, ";
				sCmd += "TO_DATE('" + auxClasse.getdatBloqueio() + "', 'DD/MM/YYYY - HH24:MI'), ";
				sCmd += auxClasse.getNumMinutos() + ", ";
				sCmd += myUsr.getCodUsuario() + ", ";
				sCmd += usuarios_bloq[i] + ", ";
				sCmd += "sysdate" + ")";

				stmt.execute(sCmd);

			}
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Sistema_Bloqueio
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean VerificaBloqueio(ConsultaUsuarioLogadoBean auxClasse, String codUsuario) throws DaoException {
		boolean bOk = true;
		String cod_orgao_lotacao = "";
		String nom_username = "";
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT TO_CHAR(DAT_BLOQUEIO, 'DD/MM/YYYY - HH24:MI:SS')AS DAT_BLOQUEIO, NUM_MINUTOS FROM TCAU_SISTEMA_BLOQUEIO";
			sCmd += " WHERE COD_USUARIO_BL = " + codUsuario;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				auxClasse.setdatBloqueio(rs.getString("DAT_BLOQUEIO"));
				auxClasse.setNumMinutos(rs.getString("NUM_MINUTOS"));
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Usuario
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean CarregaCodUsuario(String[] codUsuarios, String[] nom_UserName, String[] codOrgaoLotacao)
			throws DaoException {
		boolean bOk = true;
		Connection conn = null;
		int cont = 0;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			for (int i = 0; i < nom_UserName.length; i++) {

				String sCmd = "SELECT COD_USUARIO " + "FROM TCAU_USUARIO";
				sCmd += " WHERE NOM_USERNAME = " + "'" + nom_UserName[i] + "'";
				sCmd += " AND COD_ORGAO = " + "'" + codOrgaoLotacao[i] + "'";

				ResultSet rs = stmt.executeQuery(sCmd);
				while (rs.next()) {
					codUsuarios[cont] = rs.getString("COD_USUARIO");
					cont++;
				}
				rs.close();
			}
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � Tcau_Log_Acesso
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaLogAcesso(ConsultaUsuarioLogadoBean auxClasse) throws DaoException {
		boolean bOk = true;
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			String sCmd = "SELECT NOM_ABREV, to_char(DAT_OPERACAO, 'DD/MM/YYYY HH24:MI:SS') as DAT_OPERACAO, SIG_FUNCAO, NOM_OPERACAO "
					+ "FROM TCAU_LOG_ACESSO ";
			sCmd += " WHERE NOM_USERNAME = " + "'" + auxClasse.getNomUserName() + "'";
			sCmd += " AND NUM_SESSAO = " + "'" + auxClasse.getNumSessao() + "'";
			sCmd += " ORDER BY DAT_OPERACAO";

			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ConsultaUsuarioLogadoBean dados = new ConsultaUsuarioLogadoBean();
				dados.setNomAbrev(rs.getString("NOM_ABREV"));
				dados.setDatOperacao(rs.getString("DAT_OPERACAO"));
				dados.setNomOperacao(rs.getString("NOM_OPERACAO"));
				dados.setSigFuncao(rs.getString("SIG_FUNCAO"));
				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � CadastraCategoriaBean
	 *-----------------------------------------------------------
	 */
	public boolean CadCatInsert(CadastraCategoriaBean myCadCat, Statement stmt) {
		try {
			String sCmd = "";
			boolean tem = false;

			sCmd = "SELECT * FROM TCAU_CATEGORIA_DUVIDA where COD_CATEGORIA_DUVIDA ='" + myCadCat.getCodCategoria()
					+ "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				tem = true;

			rs.close();

			if (!tem) {
				sCmd = "INSERT INTO TCAU_CATEGORIA_DUVIDA (COD_CATEGORIA_DUVIDA,DSC_CATEGORIA_DUVIDA,EMAIL_ENVIO)"
						+ " VALUES( SEQ_TCAU_CATEGORIA_DUVIDA.nextval,'" + myCadCat.getDesCat() + "','"
						+ myCadCat.getEmailCat() + "')";
				stmt.execute(sCmd);
				return true;
			} else {
				sCmd = "UPDATE TCAU_CATEGORIA_DUVIDA set DSC_CATEGORIA_DUVIDA ='" + myCadCat.getDesCat() + "',"
						+ " EMAIL_ENVIO ='" + myCadCat.getEmailCat() + "' where COD_CATEGORIA_DUVIDA ='"
						+ myCadCat.getCodCategoria() + "'";
				stmt.execute(sCmd);
				return true;
			}
		} catch (Exception ex) {
			System.out.println("Erro no metodo insert categoria " + ex.getMessage());
			return false;
		}
	}

	// ----------------------------------------------------------------------------------------
	public boolean CadCatDelete(CadastraCategoriaBean myCadCat, Statement stmt) {
		try {
			String sCmd = "DELETE TCAU_CATEGORIA_DUVIDA where COD_CATEGORIA_DUVIDA = '" + myCadCat.getCodCategoria()
					+ "'";
			stmt.execute(sCmd);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	// -------------------------------------------------------------------------------------------
	public boolean InsertCat(Vector vErro, Vector myCadCats) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);

			for (int i = 0; i < myCadCats.size(); i++) {
				CadastraCategoriaBean myCadCat = (CadastraCategoriaBean) myCadCats.get(i);
				if ("".equals(myCadCat.getDesCat().trim()) == false) {
					if (CadCatInsert(myCadCat, stmt))
						conn.commit();
					else {
						conn.rollback();
						vErro.add("Erro na atualiza��o da Categoria:" + myCadCat.getDesCat() + "\n");
						retorno = false;
					}
				} else {
					if ("".equals(myCadCat.getCodCategoria()) == false) {
						if (CadCatDelete(myCadCat, stmt)) {
							conn.commit();
							myCadCats.remove(i);
							i--;
						} else {
							conn.rollback();
							vErro.add("Erro na exclus�o da Catalogo:" + myCadCat.getDesCat() + "\n");
							retorno = false;
						}
					}
				}
			}
			stmt.close();
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (!retorno)
						conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return retorno;
	}

	// --------------------------------------------------------------------------------------------
	public boolean ListaCadCat(CadastraCategoriaBean myCadCat) throws DaoException {
		boolean bOk = true;

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "SELECT COD_CATEGORIA_DUVIDA,DSC_CATEGORIA_DUVIDA,EMAIL_ENVIO from TCAU_CATEGORIA_DUVIDA";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetcadcats = new Vector();
			while (rs.next()) {
				CadastraCategoriaBean mycad = new CadastraCategoriaBean();
				mycad.setCodCategoria(rs.getString("COD_CATEGORIA_DUVIDA"));
				mycad.setDesCat(rs.getString("DSC_CATEGORIA_DUVIDA"));
				mycad.setEmailCat(rs.getString("EMAIL_ENVIO"));
				vetcadcats.add(mycad);
			}
			myCadCat.setVetCadCats(vetcadcats);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			myCadCat.setCodCategoria("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myCadCat.setCodCategoria("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/*-----------------------------------------------------------
	 * DAO relativo � NoticiaBean
	 *-----------------------------------------------------------
	 */
	public void CadastraNoticia(NoticiaBean noticia, UsuarioBean usuario, Vector vErro) throws DaoException {
		Connection conn = null;
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

			String sql = "INSERT INTO TCAU_NOTICIA( COD_NOTICIA,DSC_NOTICIA,TXT_NOTICIA,IND_PRIORIDADE,"
					+ "DAT_INICIO,DAT_FIM,COD_USUARIO,DAT_ENVIO) VALUES(SEQ_TCAU_NOTICIA.NEXTVAL,'"
					+ noticia.getDscNoticia() + "','" + noticia.getTxtNoticia() + "','" + noticia.getIndPrioridade()
					+ "'," + "TO_DATE('" + df.format(noticia.getDatInicio()) + "','DD/MM/YYYY')," + "TO_DATE('"
					+ df.format(noticia.getDatFim()) + "','DD/MM/YYYY')," + usuario.getCodUsuario() + "," + "TO_DATE('"
					+ df.format(new Date()) + "','DD/MM/YYYY') )";

			stmt.executeUpdate(sql);
			vErro.addElement("Not�cia foi gravada com sucesso!\n");
			conn.commit();
			stmt.close();

		} catch (Exception e) {
			vErro.addElement("Erro algrava a not�cia!\n");
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
					noticia.setMsgErro(vErro);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public void Publicada(NoticiaBean MyNotBean, UsuarioBean usuario, int bol) throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";

			if (bol == 1) {
				sql = "SELECT COD_NOTICIA, DSC_NOTICIA, TXT_NOTICIA, IND_PRIORIDADE, DAT_INICIO, DAT_FIM, COD_USUARIO, DAT_ENVIO "
						+ "FROM TCAU_NOTICIA WHERE (TRUNC(SYSDATE) BETWEEN DAT_INICIO AND DAT_FIM)";
			} else if (bol == 2) {
				sql = "SELECT COD_NOTICIA,DSC_NOTICIA,TXT_NOTICIA,IND_PRIORIDADE,DAT_INICIO,DAT_FIM,COD_USUARIO,DAT_ENVIO"
						+ " FROM TCAU_NOTICIA WHERE (TRUNC(DAT_INICIO) BETWEEN '" + MyNotBean.getDatInicioNoticia()
						+ "' AND '" + MyNotBean.getDatFimNoticia() + "') AND (TRUNC(DAT_FIM) < TRUNC(SYSDATE) )";
			}
			ResultSet rs = stmt.executeQuery(sql);

			Vector vetNoticias = new Vector();
			while (rs.next()) {
				NoticiaBean noticia = new NoticiaBean();

				noticia.setCodNoticia(rs.getInt("COD_NOTICIA"));
				noticia.setDscNoticia(rs.getString("DSC_NOTICIA"));
				noticia.setTxtNoticia(rs.getString("TXT_NOTICIA"));
				noticia.setIndPrioridade(rs.getString("IND_PRIORIDADE"));
				noticia.setDatInicio(rs.getDate("DAT_INICIO"));
				noticia.setDatFim(rs.getDate("DAT_FIM"));
				noticia.setUsuario(usuario);
				noticia.setDatEnvio(rs.getTimestamp("DAT_ENVIO"));

				UsuarioBean usuarioBean = new UsuarioBean();
				usuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				usuarioBean.Le_Usuario(2);
				noticia.setUsuario(usuarioBean);

				vetNoticias.add(noticia);
			}
			MyNotBean.setVetNoticias(vetNoticias);

			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public void AlteraNoticia(NoticiaBean noticia, UsuarioBean usuario, Vector vErro) throws DaoException {
		Connection conn = null;
		Connection conn1 = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn1 = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt1 = conn.createStatement();

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

			String sql = "UPDATE TCAU_NOTICIA SET ";
			sql += "DSC_NOTICIA      = '" + noticia.getDscNoticia() + "',";
			sql += "TXT_NOTICIA      = '" + noticia.getTxtNoticia() + "',";
			sql += "IND_PRIORIDADE   = '" + noticia.getIndPrioridade() + "',";
			sql += "DAT_INICIO       = TO_DATE('" + df.format(noticia.getDatInicio()) + "','DD/MM/YYYY'),";
			sql += "DAT_FIM          = TO_DATE('" + df.format(noticia.getDatFim()) + "','DD/MM/YYYY'),";
			sql += "COD_USUARIO      = " + usuario.getCodUsuario() + ",";
			sql += "DAT_ENVIO        = TO_DATE('" + df.format(new Date()) + "','DD/MM/YYYY') WHERE COD_NOTICIA="
					+ noticia.getCodNoticia();
			stmt.executeUpdate(sql);

			sql = "DELETE TCAU_NOTICIA_LEITURA WHERE COD_NOTICIA =" + noticia.getCodNoticia();
			stmt1.execute(sql);

			vErro.addElement("Not�cia foi alterada com sucesso!\n");
			conn1.commit();
			stmt1.close();

			conn.commit();
			stmt.close();

			conn.close();
			conn1.close();
		} catch (Exception e) {
			vErro.addElement("Erro alterar a not�cia!\n");
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
					noticia.setMsgErro(vErro);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	/*-----------------------------------------------------------
	 * DAO relativo � AvisoBean
	 *-----------------------------------------------------------
	 */
	public void PegaOrgaoUsuario(AvisoBean MyAviso, UsuarioBean usuario, Vector vErro, int tipo) throws DaoException {
		Connection conn = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet rs;

			int aa = 0;
			String sql = "";
			switch (tipo) {
			case 1:
				sql = "SELECT COD_USUARIO,NOM_USUARIO FROM TCAU_USUARIO WHERE COD_ORGAO ='" + MyAviso.getOrgao()
						+ "' ORDER BY NOM_USUARIO";
				if (MyAviso.getOrgao() != "")
					aa = 1;
				break;
			case 2:
				sql = "SELECT COD_PERFIL,NOM_DESCRICAO FROM TCAU_PERFIL WHERE COD_SISTEMA='" + MyAviso.getSistemPerfil()
						+ "' ORDER BY NOM_DESCRICAO";
				if (MyAviso.getSistemPerfil() != "")
					aa = 1;
				break;
			case 3:
				sql = "SELECT COD_ORGAO,SIG_ORGAO FROM TSMI_ORGAO ORDER BY SIG_ORGAO";
				break;
			case 4:
				sql = "SELECT COD_SISTEMA,NOM_SISTEMA FROM TCAU_SISTEMA ORDER BY NOM_SISTEMA";
			}
			rs = stmt.executeQuery(sql);

			if (aa == 1)
				if (!rs.next())
					vErro.addElement("N�o h� item(ns) para selecionar!\n");

			rs.beforeFirst();

			Vector vetAvisos = new Vector();

			while (rs.next()) {
				AvisoBean aviso = new AvisoBean();
				switch (tipo) {
				case 1:
					aviso.setCodGenerico(rs.getString("COD_USUARIO"));
					aviso.setNomGenerico(rs.getString("NOM_USUARIO"));
					break;
				case 2:
					aviso.setCodGenerico(rs.getString("COD_PERFIL"));
					aviso.setNomGenerico(rs.getString("NOM_DESCRICAO"));
					break;
				case 3:
					aviso.setCodGenerico(rs.getString("COD_ORGAO"));
					aviso.setNomGenerico(rs.getString("SIG_ORGAO"));
					break;
				case 4:
					aviso.setCodGenerico(rs.getString("COD_SISTEMA"));
					aviso.setNomGenerico(rs.getString("NOM_SISTEMA"));
				}
				vetAvisos.add(aviso);
			}
			MyAviso.setVetItens(vetAvisos);

			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		MyAviso.setMsgErro(vErro);
	}

	public boolean InsertAviso(AvisoBean MyAviso, UsuarioBean usuario, Vector vErro, int tipo) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String sql = "";

			sql = "INSERT INTO TCAU_AVISO(COD_AVISO,DSC_AVISO,TXT_AVISO,IND_PRIORIDADE,COD_USUARIO,DAT_ENVIO)"
					+ " VALUES(SEQ_TCAU_AVISO.NEXTVAL,'" + MyAviso.getDscAviso() + "','" + MyAviso.getTxtAviso() + "','"
					+ MyAviso.getIndPrioridade() + "'," + usuario.getCodUsuario() + "," + "TO_DATE('"
					+ df.format(MyAviso.getDatEnvio()) + "','DD/MM/YYYY') )";
			stmt.executeQuery(sql);
			conn.commit();

			sql = "SELECT MAX(COD_AVISO) AS CODAVISO FROM TCAU_AVISO";
			ResultSet rs = stmt.executeQuery(sql);
			int codAviso = 0;
			while (rs.next())
				codAviso = rs.getInt("CODAVISO");

			switch (tipo) {
			case 1:
				for (int i = 0; i < MyAviso.getVetItens().size(); i++) {
					AvisoBean myAv = (AvisoBean) MyAviso.getVetItens(i);
					sql = "INSERT INTO TCAU_AVISO_DESTINO(COD_AVISO_DESTINO,COD_AVISO,COD_USUARIO)"
							+ " VALUES(SEQ_TCAU_AVISO_DESTINO.nextval," + codAviso + "," + myAv.getCodGenerico() + ")";
					stmt.executeQuery(sql);
					conn.commit();
				}
				break;
			case 2:
				for (int i = 0; i < MyAviso.getVetItens().size(); i++) {
					AvisoBean myAv = (AvisoBean) MyAviso.getVetItens(i);
					sql = "INSERT INTO TCAU_AVISO_DESTINO(COD_AVISO_DESTINO,COD_AVISO,COD_PERFIL)"
							+ " VALUES(SEQ_TCAU_AVISO_DESTINO.nextval," + codAviso + "," + myAv.getCodGenerico() + ")";
					stmt.executeQuery(sql);
					conn.commit();
				}
				break;
			case 3:
				for (int i = 0; i < MyAviso.getVetItens().size(); i++) {
					AvisoBean myAv = (AvisoBean) MyAviso.getVetItens(i);
					sql = "INSERT INTO TCAU_AVISO_DESTINO(COD_AVISO_DESTINO,COD_AVISO,COD_ORGAO)"
							+ " VALUES(SEQ_TCAU_AVISO_DESTINO.nextval," + codAviso + "," + myAv.getCodGenerico() + ")";
					stmt.executeQuery(sql);
					conn.commit();
				}
				break;
			case 4:
				for (int i = 0; i < MyAviso.getVetItens().size(); i++) {
					AvisoBean myAv = (AvisoBean) MyAviso.getVetItens(i);
					sql = "INSERT INTO TCAU_AVISO_DESTINO(COD_AVISO_DESTINO,COD_AVISO,COD_SISTEMA)"
							+ " VALUES(SEQ_TCAU_AVISO_DESTINO.nextval," + codAviso + "," + myAv.getCodGenerico() + ")";
					stmt.executeQuery(sql);
					conn.commit();
				}
			}

			retorno = true;
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}

	public boolean ProcessoLeBean(SistemaBean mySist, int tp) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			if (tp == 0)
				sCmd = "SELECT * from TSMI_PROCESSO_SISTEMA WHERE cod_processo_sistema='" + mySist.getCodProcesso()
						+ "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				retorno = true;
				mySist.setCodProcesso(rs.getString("cod_processo_sistema"));
				mySist.setNomProcesso(rs.getString("nom_processo"));
			}

			rs.close();
			stmt.close();
		} // fim do try
		catch (SQLException sqle) {
			mySist.setCodSistema("0");
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			mySist.setCodSistema("0");
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return retorno;
	}

	public ArrayList<NivelProcessoSistemaBean> getDestinatariosPorNivel(ParamSistemaBean mySist, String parametro)
			throws DaoException {
		Connection conn = null;
		ResultSet rs2 = null;
		ArrayList destPorNivel = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			String sCmd = "SELECT  N.COD_PROCESSO_SISTEMA_NIVEL,N.DSC_NIVEL_PROCESSO,N.VAL_NIVEL,N.TIP_MSG "
					+ " FROM TSMI_PROCESSO_SISTEMA P, " + " TSMI_PROCESSO_SISTEMA_NIVEL N "
					+ " WHERE P.COD_PROCESSO_SISTEMA = N.COD_PROCESSO_SISTEMA" + " AND P.COD_SISTEMA = "
					+ mySist.getCodSistema() + " AND P.NOM_PROCESSO = '" + parametro + "' "
					+ " ORDER BY N.COD_PROCESSO_SISTEMA_NIVEL ";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				GER.NivelProcessoSistemaBean myNivelDestinatario = new GER.NivelProcessoSistemaBean();
				myNivelDestinatario.setCodNivelProcesso(rs.getString("COD_PROCESSO_SISTEMA_NIVEL"));
				myNivelDestinatario.setNomNivelProcesso(rs.getString("DSC_NIVEL_PROCESSO"));
				myNivelDestinatario.setValNivelProcesso(rs.getString("VAL_NIVEL"));
				myNivelDestinatario.setTipoMsg(rs.getString("TIP_MSG"));
				sCmd = "SELECT  D.TXT_DESTINATARIO,D.COD_ORGAO,D.TXT_EMAIL,O.SIG_ORGAO FROM "
						+ " TSMI_PROC_SIST_NIV_DEST ND, TSMI_DESTINATARIO D, TSMI_ORGAO O "
						+ " WHERE ND.COD_PROCESSO_SISTEMA_NIVEL = " + myNivelDestinatario.getCodNivelProcesso()
						+ " AND ND.COD_DESTINATARIO = D.COD_DESTINATARIO" + " AND O.COD_ORGAO = D.COD_ORGAO";
				Statement stmt2 = conn.createStatement();
				rs2 = stmt2.executeQuery(sCmd);
				ArrayList listdestPorNivel = new ArrayList();
				while (rs2.next()) {
					GER.CadastraDestinatarioBean myDestinatario = new GER.CadastraDestinatarioBean();
					myDestinatario.setTxtDestinatario(rs2.getString("TXT_DESTINATARIO"));
					myDestinatario.setCodOrgao(rs2.getString("COD_ORGAO"));
					myDestinatario.setSigOrgao(rs2.getString("SIG_ORGAO"));
					myDestinatario.setTxtEmail(rs2.getString("TXT_EMAIL"));
					listdestPorNivel.add(myDestinatario);
				}
				stmt2.close();
				myNivelDestinatario.setListNivelDestino(listdestPorNivel);
				destPorNivel.add(myNivelDestinatario);
			}
			rs.close();
			stmt.close();
			if (rs2 != null)
				rs2.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return destPorNivel;
	}

	public void consultaParamOrg(ConsultaHistoricoParamOrgBean myConsultParamOrg) throws DaoException {
		List dados = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " SELECT P.NOM_PARAMETRO, PLOG.NOM_PARAMETRO_OLD,PLOG.NOM_PARAMETRO_NEW,"
					+ " PLOG.VAL_PARAMETRO_OLD, PLOG.VAL_PARAMETRO_NEW, PLOG.NOM_USERNAME, "
					+ " TO_CHAR(PLOG.DAT_LOG,'DD/MM/YYYY:hh24:mi:ss')DAT_LOG, "
					+ " PLOG.NOM_DESCRICAO_OLD, PLOG.NOM_DESCRICAO_NEW "
					+ " FROM TCAU_PARAM_ORGAO P, TCAU_PARAM_ORGAO_LOG PLOG, TCAU_GRUPO_PARAM GP "
					+ " WHERE P.COD_PARAMETRO = PLOG.COD_PARAMETRO_OLD AND"
					+ " GP.COD_GRUPO = P.COD_GRUPO AND GP.COD_GRUPO =" + myConsultParamOrg.getGrupo() + " AND "
					+ " PLOG.COD_ORGAO_OLD= " + myConsultParamOrg.getCodOrgao() + " AND " + " PLOG.DAT_LOG >= TO_DATE('"
					+ myConsultParamOrg.getDatIni() + ":00:00:00','DD/MM/YYYY:hh24:mi:ss') AND "
					+ " PLOG.DAT_LOG <= TO_DATE('" + myConsultParamOrg.getDatFim()
					+ ":23:59:59','DD/MM/YYYY:hh24:mi:ss') ORDER BY PLOG.DAT_LOG DESC ";
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				ConsultaHistoricoParamOrgBean myConsult = new ConsultaHistoricoParamOrgBean();
				myConsult.setNomParam(rs.getString("NOM_PARAMETRO"));
				myConsult.setDatLog(rs.getString("DAT_LOG"));
				myConsult.setNomDescOld(rs.getString("NOM_DESCRICAO_OLD"));
				myConsult.setNomDescNew(rs.getString("NOM_DESCRICAO_NEW"));
				myConsult.setNomParamOld(rs.getString("NOM_PARAMETRO_OLD"));
				myConsult.setValParamOld(rs.getString("VAL_PARAMETRO_OLD"));
				myConsult.setNomParamNew(rs.getString("NOM_PARAMETRO_NEW"));
				myConsult.setValParamNew(rs.getString("VAL_PARAMETRO_NEW"));
				myConsult.setNomUsrName(rs.getString("NOM_USERNAME"));
				dados.add(myConsult);
			}
			myConsultParamOrg.setDados(dados);
			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public boolean verificaDataInicial(LogAcessoBean logAcessoBean) throws DaoException {
		String datMinima = "";
		Connection conn = null;
		boolean bOk = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " SELECT TO_CHAR(MIN(DAT_OPERACAO), 'DD/MM/YYYY') AS DAT_MINIMA FROM TCAU_LOG_ACESSO ";
			ResultSet rs = stmt.executeQuery(sql);
			if (rs.next()) {
				datMinima = rs.getString("DAT_MINIMA");
			}

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			if (df.parse(logAcessoBean.getDatIni()).before(df.parse(datMinima))) {
				bOk = true;
			}
			stmt.close();
			rs.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}

		return bOk;
	}

	/**
	 * --------------------------------------- Relativo ao ConsultaHistCancAuto
	 * ------------------------------------------
	 */
	public void ConsultaDadosHistCancAuto(ConsultaHistoricosBean auxClasse, String codOrgao, UsuarioBean myUsr)
			throws DaoException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());

			// preparar lista os dados do Usuario por Sistema
			String sCmd = "Select ORG.SIG_ORGAO AS COD_ORGAO, A.COD_ORGAO, A.NUM_AUTO_INFRACAO, H.TXT_COMPLEMENTO_02 AS DAT_CANC, "
					+ "'CANCELAMENTO' AS ACAO, H.TXT_COMPLEMENTO_03 AS MOTIVO, H.TXT_COMPLEMENTO_06 AS COD_STATUS, "
					+ "H.NOM_USERNAME AS NOM_USERNAME_OPER, ORGOP.SIG_ORGAO AS ORGAO_LOTACAO_OPER, "
					+ "to_char(H.DAT_PROC,'dd/mm/yyyy') AS DAT_PROC_OPER "
					+ "FROM TSMI_HISTORICO_AUTO H, TSMI_AUTO_INFRACAO A, TSMI_ORGAO ORG, TSMI_ORGAO ORGOP "
					+ "WHERE H.COD_EVENTO = 405 ";
			if (!codOrgao.equals("0"))
				sCmd += "AND A.COD_ORGAO = " + codOrgao + " ";
			sCmd += "AND H.COD_AUTO_INFRACAO = A.COD_AUTO_INFRACAO " + "AND A.COD_ORGAO = ORG.COD_ORGAO "
					+ "AND H.COD_ORGAO_LOTACAO = ORGOP.COD_ORGAO ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd += "AND H.DAT_PROC >= TO_DATE('" + auxClasse.getDatInicio()
						+ ":00:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			if (!auxClasse.getDatFim().equals(""))
				sCmd += "AND H.DAT_PROC <= TO_DATE('" + auxClasse.getDatFim() + ":23:00:00', 'DD/MM/YYYY:HH24:MI:SS') ";
			sCmd += "ORDER BY A.COD_ORGAO, A.NUM_AUTO_INFRACAO";

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);

			while (rs.next()) {
				REC.HistoricoBean dados = new REC.HistoricoBean();
				dados.setCodOrgao(rs.getString("COD_ORGAO"));
				dados.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				dados.setTxtComplemento02(rs.getString("DAT_CANC"));
				dados.setTxtComplemento05(rs.getString("ACAO"));
				dados.setTxtComplemento03(rs.getString("MOTIVO"));
				dados.setTxtComplemento06(rs.getString("COD_STATUS"));
				dados.setNomUserName(rs.getString("NOM_USERNAME_OPER"));
				dados.setCodOrgaoLotacao(rs.getString("ORGAO_LOTACAO_OPER"));
				dados.setDatProc(rs.getString("DAT_PROC_OPER"));

				auxClasse.getDados().add(dados);
			}
			rs.close();
			stmt.close();

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	public String UsuarioFuncValidaBiometria(String codOrgao, String codOperacao) throws DaoException {
		String valBio = "I";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA " + "WHERE COD_OPERACAO = " + codOperacao + " "
					+ "AND COD_ORGAO = " + codOrgao;
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				valBio = "A";
			}

			rs.close();
			stmt.close();

		} // fim do try
		catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return valBio;
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Funcoes em Biometria
	 * -----------------------------------------------------------
	 */
	public boolean BiometriaInsertFuncoes(PerfilBean myPerf, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas as opera��es antigas
			Vector funcAtual = myPerf.getFuncoes();
			// iniciar transa��o
			conn.setAutoCommit(false);
			retorno = retorno && BiometriaOperacaoDelete(myPerf, stmt);
			for (int i = 0; i < funcAtual.size(); i++) {
				OperacaoBean myOper = (OperacaoBean) funcAtual.elementAt(i);
				retorno = retorno && BiometriaOperacaoInsert(myOper, stmt, myPerf.getCodPerfil(), myPerf);
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		myPerf.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Delete
	 * Operacaoem Biometria
	 * -----------------------------------------------------------
	 */
	public boolean BiometriaOperacaoDelete(PerfilBean myPerf, Statement stmt) throws DaoException {
		boolean retorno = false;
		try {
			// String sCmd = "UPDATE TCAU_ACESSO set NOM_USERNAME_OPER =
			// '"+myPerf.getNomUserNameOper()+"'," +
			// "ORGAO_LOTACAO_OPER = "+myPerf.getOrgaoLotacaoOper()+","+
			// "DAT_PROC_OPER =
			// to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')"+"
			// "+
			// "where cod_perfil = '"+myPerf.getCodPerfil()+"'";
			// stmt.execute(sCmd);

			String sCmd = "DELETE SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA where cod_orgao = '" + myPerf.getCodPerfil() + "'";
			stmt.execute(sCmd);
			retorno = true;
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Operacao em Biometria
	 * -----------------------------------------------------------
	 */
	public boolean BiometriaOperacaoInsert(OperacaoBean myOperacao, Statement stmt, String codPerfil, PerfilBean myPerf)
			throws DaoException {
		boolean retorno = false;
		try {
			String sCmd = "INSERT INTO SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA (cod_Operacao,cod_funcao_biometria,cod_orgao,nom_username_oper,dat_proc_oper,orgao_lotacao_oper) ";
			sCmd += "VALUES (";
			sCmd += "'" + myOperacao.getCodOperacao() + "',seq_tcau_acesso.nextval, ";
			sCmd += "'" + codPerfil + "','";
			sCmd += myPerf.getNomUserNameOper() + "',to_date('" + sys.Util.formatedToday().substring(0, 10)
					+ "','dd/mm/yyyy')," + myPerf.getOrgaoLotacaoOper() + ") ";

			stmt.execute(sCmd);
			retorno = true;
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Insert
	 * Funcoes em Biometria Todos
	 * -----------------------------------------------------------
	 */
	public boolean BiometriaInsertFuncoesTodos(PerfilBean myPerf, Vector vErro, String[] ArraycodOrgao,
			String tipoOperacao) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String codOrgaoAux = "";
			int res = 0;
			for (int i = 0; i < ArraycodOrgao.length; i++) {
				res = (i % 2);
				if (res == 0) {
					codOrgaoAux = ArraycodOrgao[i];
					codOrgaoAux = codOrgaoAux.replaceAll("\"", "");

					Vector funcAtual = myPerf.getFuncoes();
					// iniciar transa��o
					conn.setAutoCommit(false);

					for (int j = 0; j < funcAtual.size(); j++) {
						OperacaoBean myOper = (OperacaoBean) funcAtual.elementAt(j);
						retorno = retorno
								&& BiometriaOperacaoInsertDelete(myOper, stmt, codOrgaoAux, myPerf, tipoOperacao);

						if (!retorno)
							break;
					}
				}
			}

			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		myPerf.setMsgErro(vErro);
		return retorno;
	}

	/**
	 * ----------------------------------------------------------- Delete
	 * Operacaoem Biometria
	 * -----------------------------------------------------------
	 */
	public boolean BiometriaOperacaoInsertDelete(OperacaoBean myOperacao, Statement stmt, String codOrgaoAux,
			PerfilBean myPerf, String tipoOperacao) throws DaoException {
		boolean retorno = true;
		try {

			String sql = "";
			ResultSet rs = null;
			String sCmd = "";
			if (tipoOperacao.equals("I")) {

				sql = "SELECT COD_FUNCAO_BIOMETRIA " + "FROM SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA WHERE COD_OPERACAO = "
						+ myOperacao.getCodOperacao() + " " + "AND COD_ORGAO = " + codOrgaoAux;

				rs = stmt.executeQuery(sql);
				if (!rs.next()) {

					sCmd = "INSERT INTO SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA (cod_Operacao,cod_funcao_biometria,cod_orgao,nom_username_oper,dat_proc_oper,orgao_lotacao_oper) ";
					sCmd += "VALUES (";
					sCmd += "'" + myOperacao.getCodOperacao() + "',seq_tcau_acesso.nextval, ";
					sCmd += "" + codOrgaoAux + ",'";
					sCmd += myPerf.getNomUserNameOper() + "',to_date('" + sys.Util.formatedToday().substring(0, 10)
							+ "','dd/mm/yyyy')," + myPerf.getOrgaoLotacaoOper() + ") ";
					stmt.execute(sCmd);
				}

			} else {

				sql = "SELECT COD_FUNCAO_BIOMETRIA " + "FROM SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA WHERE COD_OPERACAO = "
						+ myOperacao.getCodOperacao() + " " + "AND COD_ORGAO = " + codOrgaoAux;

				rs = stmt.executeQuery(sql);
				if (rs.next()) {

					sCmd = "DELETE SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA where cod_orgao = " + codOrgaoAux + " "
							+ "AND COD_OPERACAO = " + myOperacao.getCodOperacao() + " ";
					stmt.execute(sCmd);
				}
			}

			rs.close();

		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		return retorno;
	}

	public void BiometriaGetFuncoes(PerfilBean myPerf) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT o.cod_operacao,o.num_ordem,o.nom_executa,o.sig_funcao,pai.sig_funcao sig_funcao_pai,";
			sCmd += "o.nom_operacao, o.cod_operacao_fk, o.nom_apresentacao,o.nom_descricao,o.sit_biometria ";
			sCmd += " from TCAU_OPERACAO o, TCAU_OPERACAO pai, SMIT_ACSS.TCAU_FUNCAO_BIOMETRIA acs";
			sCmd += " WHERE o.cod_sistema = '" + myPerf.getCodSistema() + "'";
			sCmd += " and acs.cod_operacao= o.cod_operacao  and acs.cod_orgao = '" + myPerf.getCodPerfil() + "' ";
			sCmd += " and nvl(o.cod_operacao_fk,o.cod_operacao) = pai.cod_operacao  order by o.sig_funcao, o.num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector funcoes = new Vector();
			while (rs.next()) {
				OperacaoBean myOper = new OperacaoBean();
				myOper.setCodOperacao(rs.getString("cod_operacao"));
				myOper.setNumOrdem(rs.getString("num_ordem"));
				myOper.setNomExecuta(rs.getString("nom_executa"));
				myOper.setSigFuncao(rs.getString("sig_funcao"));
				myOper.setSigFuncaoPai(rs.getString("sig_funcao_pai"));
				myOper.setNomOperacao(rs.getString("nom_operacao"));
				myOper.setNomDescricao(rs.getString("nom_descricao"));
				myOper.setCodOperacaoFk(rs.getString("cod_operacao_fk"));
				myOper.setNomApresentacao(rs.getString("nom_apresentacao"));
				myOper.setSitBiometria(rs.getString("sit_biometria"));
				funcoes.addElement(myOper);
			}
			// prepara os niveis
			myPerf.setFuncoes(FuncoesSetNivel(funcoes));
			rs.close();
			stmt.close();
			conn.close();
		} // fim do try
		catch (SQLException sqle) {
			myPerf.setMsgErro("Perfil005 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myPerf.setCodPerfil("0");
		} catch (Exception e) {
			myPerf.setMsgErro("Perfil006 - Leitura: " + e.getMessage());
			myPerf.setCodPerfil("0");
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean SistemaHabilitaFuncoes(SistemaBean mySist, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// deletar todas as opera��es antigas
			Vector funcHabilita = mySist.getFuncoes();
			// iniciar transa��o
			conn.setAutoCommit(false);
			String codOper = "0";
			for (int i = 0; i < funcHabilita.size(); i++) {
				OperacaoBean myOper = (OperacaoBean) funcHabilita.elementAt(i);
				codOper = myOper.getCodOperacao();
				OperacaoHabilita(myOper, stmt);

			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		} catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		mySist.setMsgErro(vErro);
		return retorno;
	}

	public void OperacaoHabilita(OperacaoBean myOperacao, Statement stmt) throws DaoException {
		try {
			String codOper = myOperacao.getCodOperacao();
			String sCmd = "";

			sCmd = "UPDATE TCAU_OPERACAO set ";
			sCmd += "ind_ativo      ='" + myOperacao.getHabilita() + "'";
			sCmd += " where cod_operacao = '" + codOper + "' ";
			stmt.execute(sCmd);

		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}

	/*
	 * Ata - Gerenciamento
	 * 
	 */

	/**
	 * ----------------------------------------------------------- Insert em Ata
	 * -----------------------------------------------------------
	 */
	public boolean AtaInsert(Ata myAta) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		String sCmd = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			// myAta.setStatusAta("A");
			// myAta.setTpAta("Def. Previa");
			// myAta.setJuntaAta("1");

			sCmd = "INSERT INTO TSMI_ATA (cod_ata, tipo_ata, des_ata, dt_sessao, dt_distribuicao, junta_ata, dt_aviso, status, qtde_processo, nr_ata, ano_ata, DAT_INCLUSAO, COD_ORGAO, cd_natureza) ";
			sCmd += "VALUES (seq_tcau_ata.nextval,";
			sCmd += "'" + myAta.getTpAta() + "',";
			sCmd += "'" + myAta.getDescAta() + "',";
			sCmd += " to_date('" + myAta.getDataSessao() + "'),";
			sCmd += " to_date('" + myAta.getDataDistribuicao().replaceAll("-", "/") + "'),"; // dataDistribuicao
			// sCmd += " to_date('"+Util.getDataHoje().replaceAll("-","/")+"'),"
			// ; //dataDistribuicao
			sCmd += "'" + myAta.getJuntaAta() + "',";
			sCmd += " to_date('" + myAta.getDataAviso().replaceAll("-", "/") + "'),"; // dataAviso
			// sCmd += " to_date('"+Util.getDataHoje().replaceAll("-","/")+"'),"
			// ; //dataAviso
			sCmd += "'" + myAta.getStatusAta() + "',";
			sCmd += "'" + myAta.getQtdeAtaProcesso() + "',";
			sCmd += "'" + myAta.getNrAta() + "',";
			sCmd += "'" + myAta.getAnoAta() + "',";
			// sCmd += resultdate ; //dataInclusao
			sCmd += " to_date('" + myAta.getDataInclusao().replaceAll("-", "/") + "'),"; // dataInclusao
			sCmd += " '" + myAta.getCodOrgao() + "'";
			sCmd += ", " + myAta.getCdNatureza() + ")";
			// sCmd += "
			// to_date('"+myAta.getDataInclusao().replaceAll("-","/")+"')" ;
			// //dataInclusao

			ResultSet rs = stmt.executeQuery(sCmd);
			stmt.close();
			existe = true;
		} catch (Exception ex) {
			Logs.gravarLog(ex.getMessage());
			Logs.gravarLog("QUERY: " + sCmd);

			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Update em
	 * Usuario -----------------------------------------------------------
	 */
	public boolean AtaUpdate(Ata ata) throws DaoException {
		boolean existe = false;
		boolean novaSenha = true;
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TSMI_ATA set ";
			sCmd += "TIPO_ATA= '" + ata.getTpAta() + "',";
			sCmd += "DES_ATA= '" + ata.getDescAta() + "',";
			sCmd += "DT_SESSAO =  to_date('" + ata.getDataSessao() + "'),";
			sCmd += "DT_DISTRIBUICAO =  to_date('" + ata.getDataDistribuicao() + "'),";
			sCmd += "JUNTA_ATA= '" + ata.getJuntaAta() + "',";
			sCmd += "DT_AVISO=  to_date('" + ata.getDataAviso() + "'),";
			sCmd += "QTDE_PROCESSO= '" + ata.getQtdeAtaProcesso() + "',";
			sCmd += "STATUS= '" + ata.getStatusAta() + "',";
			sCmd += "DATA_PUBLICACAO = '" + ata.getDataPublicacao() + "'";
			sCmd += " where COD_ATA='" + ata.getCodAta() + "'";
			sCmd += " and cd_natureza = " + ata.getCdNatureza();

			stmt.execute(sCmd);
			stmt.close();

			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}

	/**
	 * ----------------------------------------------------------- Delete em
	 * Usuario -----------------------------------------------------------
	 */
	public boolean AtaDelete(Ata myAta) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "DELETE FROM TSMI_ATA ";
			sCmd += " where COD_ATA='" + myAta.getCodAta() + "'";

			stmt.execute(sCmd);
			stmt.close();
			existe = true;
		} catch (SQLException e) {
			existe = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return existe;
	}

	public ArrayList<Ata> getAtasDF() throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO,
			// DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS, qtde_processo FROM
			// TSMI_ATA WHERE TIPO_ATA = 'Def. Previa' " +
			// " AND COD_ORGAO = '"+ ControladorRecGeneric.codOrgao +"'ORDER BY
			// COD_ATA";

			sCmd = "select ata.cod_ata, ata.des_ata, ata.qtde_processo, ata.dt_distribuicao, ata.dt_aviso, ata.dt_sessao, ata.status, ata.tipo_ata, ata.data_publicacao, cd_natureza"
					+ " from tsmi_ata ata" + " where ata.cod_orgao = " + ControladorRecGeneric.codOrgao
					+ " order by to_char(ata.dat_inclusao, 'yyyy') desc, ata.des_ata";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {
						Ata ata = new Ata();

						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setQtdeAtaProcesso(Integer.parseInt(rs.getString("QTDE_PROCESSO")));
						ata.setDataDistribuicao(ControladorRecGeneric.dateToDateBr(rs.getString("DT_DISTRIBUICAO")));
						ata.setDataAviso(ControladorRecGeneric.dateToDateBr(rs.getString("DT_AVISO")));
						ata.setDataSessao(ControladorRecGeneric.dateToDateBr(rs.getString("DT_SESSAO")));
						ata.setStatusAta(rs.getString("STATUS"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDataPublicacao(rs.getString("data_publicacao"));
						ata.setCdNatureza(rs.getInt("cd_natureza"));

						listAtas.add(ata);
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	public ArrayList<Ata> getAtasTp() throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS  FROM TSMI_ATA  ORDER BY COD_ATA";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {
						Ata ata = new Ata();

						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						listAtas.add(ata);
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	public boolean getNexAta(String cdAta) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String count_proc_atribuido = "";
		String qtde_processo_informado = "";
		int qtde_informada = 0;
		int qtde_atribuida = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.commit();
			Statement stmt = conn.createStatement();
			sCmd = " SELECT COUNT(A.COD_ATA) AS QTDE_PROCESSO_ATRIBUIDO FROM TSMI_AUTO_INFRACAO A WHERE A.COD_ATA = '"
					+ cdAta + "'"; // QTDE PROCESSO ATRIBUIDO

			// if((cdAta.length()>0) && (cdAta != null)){
			// System.out.println("O valor de cdAta foi esse:" +
			// cdAta.toString());
			// }

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_ATRIBUIDO") > 0) {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");// qtde
																							// de
																							// autos
																							// atribuidos
																							// com
																							// a
																							// ata
																							// indicada
						} else {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");
						}
					}
				}

				sCmd = " SELECT ATA.QTDE_PROCESSO AS QTDE_PROCESSO_INFORMADO FROM TSMI_ATA ATA WHERE ATA.cod_ata = '"
						+ cdAta + "'"; // QTDE PROCESSO INFORMADO
				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_INFORMADO") > 0) {
							qtde_processo_informado = rs.getString("QTDE_PROCESSO_INFORMADO");// qtde
																								// de
																								// processos
																								// indicado
																								// a
																								// esta
																								// ata
							qtde_informada = Integer.valueOf(qtde_processo_informado);
							qtde_atribuida = Integer.valueOf(count_proc_atribuido);

							if (qtde_atribuida == qtde_informada) {
								/*
								 * Verifica se j� existe uma ata posterior a ata
								 * verificada
								 */
								Ata ata = new Ata();
								ata = ata.getAtasGen(cdAta);
								sCmd = "SELECT COUNT(1) FROM TSMI_ATA S WHERE S.TIPO_ATA = '" + ata.getTpAta()
										+ "' AND S.STATUS= '" + ata.getStatusAta() + "'";
								rs = stmt.executeQuery(sCmd);
								if (rs == null) { //// s� inclui se n�o houver
													//// ata aberta para esse
													//// tipo
									return true;
								} else {
									return false;
								}
							}
						}
					}
				}
			} catch (Exception e) {

			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return false;
	}

	public boolean verificaExisteAtaAberta(String cdAta) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String count_proc_atribuido = "";
		String qtde_processo_informado = "";
		int qtde_informada = 0;
		int qtde_atribuida = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.commit();
			Statement stmt = conn.createStatement();

			Ata ata = new Ata();
			ata = ata.getAtasGen(cdAta);
			sCmd = "SELECT COUNT(1) QTDE FROM TSMI_ATA S WHERE S.TIPO_ATA = '" + ata.getTpAta() + "' AND S.STATUS= '"
					+ ata.getStatusAta() + "'  AND S.COD_ATA <>  " + cdAta + "";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE") == 0) {
							return true;
						} else {
							return false;
						}
					}
				}

			} catch (Exception e) {

			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return false;
	}

	public boolean validaQtdeProcesso(String cdAta) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String count_proc_atribuido = "";
		String qtde_processo_informado = "";
		int qtde_informada = 0;
		int qtde_atribuida = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.commit();
			Statement stmt = conn.createStatement();
			sCmd = " SELECT COUNT(A.COD_ATA) AS QTDE_PROCESSO_ATRIBUIDO FROM TSMI_AUTO_INFRACAO A WHERE A.COD_ATA = '"
					+ cdAta + "'"; // QTDE PROCESSO ATRIBUIDO

			// if((cdAta.length()>0) && (cdAta != null)){
			// System.out.println("O valor de cdAta foi esse:" +
			// cdAta.toString());
			// }

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_ATRIBUIDO") > 0) {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");// qtde
																							// de
																							// autos
																							// atribuidos
																							// com
																							// a
																							// ata
																							// indicada
						} else {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");
						}
					}
				}

				sCmd = " SELECT ATA.QTDE_PROCESSO AS QTDE_PROCESSO_INFORMADO FROM TSMI_ATA ATA WHERE ATA.cod_ata = '"
						+ cdAta + "'"; // QTDE PROCESSO INFORMADO
				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_INFORMADO") > 0) {
							qtde_processo_informado = rs.getString("QTDE_PROCESSO_INFORMADO");// qtde
																								// de
																								// processos
																								// indicado
																								// a
																								// esta
																								// ata
							qtde_informada = Integer.valueOf(qtde_processo_informado);
							qtde_atribuida = Integer.valueOf(count_proc_atribuido);

							if (qtde_atribuida == qtde_informada) {
								return true;
							} else {
								return false;
							}
						}
					}
				}
			} catch (Exception e) {

			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return false;
	}

	public ArrayList<Ata> getAtasPublicacao(String tipoAta, String year, String month, String codOrgao,
			String cdNatureza) {
		Connection conn = null;
		ArrayList<Ata> listAta = new ArrayList<Ata>();
		String query = "select distinct ata.*" + " from tsmi_ata ata, tsmi_requerimento req"
				+ " where ata.cod_ata = req.cod_ata" + " and ata.cod_orgao = " + codOrgao
				+ " and req.cod_result_rs is not null" + " and ata.tipo_ata = '" + tipoAta + "'"
				+ " and to_char(ata.dt_sessao, 'yyyy') = '" + year + "'" + " and to_char(ata.dt_sessao, 'mm') = '"
				+ month + "'" + " and ata.cd_natureza = " + cdNatureza + " order by ata.des_ata asc";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();

			ResultSet result = state.executeQuery(query);

			while (result.next()) {
				Ata a = new Ata();
				a.setCodAta(result.getString("cod_ata"));
				a.setTpAta(result.getString("tipo_ata"));
				a.setDescAta(result.getString("des_ata"));
				a.setDataSessao(result.getString("dt_sessao"));
				a.setDataDistribuicao(result.getString("dt_distribuicao"));
				a.setJuntaAta(result.getString("junta_ata"));
				a.setDataAviso(result.getString("dt_aviso"));
				a.setStatusAta(result.getString("status"));
				a.setQtdeAtaProcesso(result.getInt("qtde_processo"));
				a.setNrAta(result.getInt("nr_ata"));
				a.setAnoAta(result.getInt("ano_ata"));
				a.setCdNatureza(result.getInt("cd_natureza"));

				listAta.add(a);
			}

			result.close();
			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE DAO: AO CONSULTAR AS ATA PARA PUBLICA��O!");
		}

		return listAta;
	}

	public ArrayList<HashMap<String, String>> getProcessos(ArrayList<Ata> atas, String codOrgao) {
		Connection conn = null;
		ArrayList<HashMap<String, String>> listProcessos = new ArrayList<HashMap<String, String>>();

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();

			for (Ata ata : atas) {
				String query = "select aut.num_processo, req.cod_result_rs, ata.des_ata, ata.dat_inclusao, ata.dt_sessao "
						+ "from tsmi_auto_infracao aut, tsmi_requerimento req, tsmi_ata ata "
						+ "where req.cod_auto_infracao = aut.cod_auto_infracao " + "and ata.cod_ata = aut.cod_ata "
						+ "and req.cod_result_rs is not null " + "and req.cod_ata = " + ata.getCodAta()
						+ " and aut.cod_orgao = " + codOrgao + " order by aut.num_processo asc";

				ResultSet result = state.executeQuery(query);

				while (result.next()) {
					HashMap<String, String> entity = new HashMap<String, String>();
					entity.put("numProcesso", result.getString("num_processo"));
					entity.put("result", result.getString("cod_result_rs"));
					entity.put("desAta", result.getString("des_ata"));
					entity.put("data", result.getString("dat_inclusao"));
					entity.put("dataSessao", result.getString("dt_sessao"));

					listProcessos.add(entity);
				}

				result.close();
			}
			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE DAO: AO CONSULTAR OS PROCESSOS PARA O RELAT�RIO!");
		}

		return listProcessos;
	}

	public String getVerificaQtdeNrProcesso(String cdAta) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String count_proc_atribuido = "";
		String qtde_processo_informado = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.commit();
			Statement stmt = conn.createStatement();
			sCmd = " SELECT COUNT(A.COD_ATA) AS QTDE_PROCESSO_ATRIBUIDO FROM TSMI_AUTO_INFRACAO A WHERE A.COD_ATA = '"
					+ cdAta + "'"; // QTDE PROCESSO ATRIBUIDO

			// if((cdAta.length()>0) && (cdAta != null)){
			// System.out.println("O valor de cdAta foi esse:" +
			// cdAta.toString());
			// }

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_ATRIBUIDO") > 0) {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");// qtde
																							// de
																							// autos
																							// atribuidos
																							// com
																							// a
																							// ata
																							// indicada
						} else {
							count_proc_atribuido = rs.getString("QTDE_PROCESSO_ATRIBUIDO");
						}
					}
				}

				sCmd = " SELECT ATA.QTDE_PROCESSO AS QTDE_PROCESSO_INFORMADO FROM TSMI_ATA ATA WHERE ATA.cod_ata = '"
						+ cdAta + "'"; // QTDE PROCESSO INFORMADO
				rs = stmt.executeQuery(sCmd);
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO_INFORMADO") > 0) {
							qtde_processo_informado = rs.getString("QTDE_PROCESSO_INFORMADO");// qtde
																								// de
																								// processos
																								// indicado
																								// a
																								// esta
																								// ata
							int qtde_informada = Integer.valueOf(qtde_processo_informado);
							int qtde_atribuida = Integer.valueOf(count_proc_atribuido);

							if (qtde_atribuida < qtde_informada) {
								// Ata.flNrProcessoAtribuidoMaiorQueInformado =
								// 0; // nr de atribui��es n�o atinguiu o
								// informado
								System.out.println("bla de _OK " + cdAta);
								return "_OK";
							} else

							if (qtde_atribuida == qtde_informada) {
								// Ata.flNrProcessoAtribuidoMaiorQueInformado =
								// 0; // nr de atribui��es n�o atinguiu o
								// informado
								System.out.println("bla de N_OK " + cdAta);
								return "N_OK_ATEN��O";
							} else if (qtde_atribuida > qtde_informada) {
								// Ata.flNrProcessoAtribuidoMaiorQueInformado =
								// 0; // nr de atribui��es n�o atinguiu o
								// informado
								System.out.println("bla de N_OK " + cdAta);
								return "N_OK_ATEN��O";
							}

						}
					}
				}

			} catch (Exception e) {

			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return "";
	}

	public int getQtdeProcessoAtribuido(String cdAta) throws DaoException {

		int count_proc_atribuido = 0;
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = " SELECT A.QTDE_PROCESSO FROM TSMI_ATA A WHERE A.COD_ATA = '" + cdAta + "'"; // QTDE
																								// PROCESSO
																								// ATRIBUIDO
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						if (rs.getInt("QTDE_PROCESSO") > 0) {
							count_proc_atribuido = rs.getInt("QTDE_PROCESSO");
						}
					}
				}

			} catch (Exception e) {

			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}

		return count_proc_atribuido;
	}

	public String montaSelectSqlAbrirProcesso(String sqlOption, String nameSelect, String cdNatureza)
			throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, "
					+ "STATUS FROM TSMI_ATA  WHERE TIPO_ATA= '" + sqlOption + "'  AND STATUS != 'F' and cd_natureza = "
					+ cdNatureza;

			ResultSet rs = stmt.executeQuery(sCmd);

			try {
				if (rs != null) {
					// optRetorno +="<select name="+ nameSelect + " id= "+
					// nameSelect + " onChange="+"javascript:alert(" + "'ola'"
					// +")"+">";
					// <select name="tipoAta" id="tipoAta"
					// onchange="ChamPopula()">
					// optRetorno +="<select name="+ nameSelect + " id= "+
					// nameSelect + "
					// onchange="+"verificaQtdeProcAtribuidos();"+">";
					optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + ">";
					optRetorno += "<option value=" + 0 + "> " + "SELECIONE" + "</option>";
					while (rs.next()) {
						// optRetorno +="<option value="+
						// rs.getString("DES_ATA")+"> " +
						// rs.getString("DES_ATA") + "</option>";
						optRetorno += "<option value=" + rs.getInt("cod_ata") + "> " + rs.getString("des_ata")
								+ "</option>";
					}
					optRetorno += "</select>";
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return optRetorno;
	}

	public String montaSelectSql(String sqlOption, String nameSelect, String cdNatureza) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO,
			// DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS FROM TSMI_ATA WHERE
			// TIPO_ATA= '" + sqlOption +"' AND STATUS != 'F'";
			String codTipoSolic = "";

			if (sqlOption == "Def. Previa")
				codTipoSolic = "DP";
			else if (sqlOption == "1� INST�NCIA")
				codTipoSolic = "1P";
			else if (sqlOption == "2� INST�NCIA")
				codTipoSolic = "2P";
			else if (sqlOption == "PAE")
				codTipoSolic = "PAE";

			// sCmd = "select distinct ata.cod_ata, ata.des_ata, ata.tipo_ata"
			// + " from tsmi_ata ata, tsmi_requerimento req, tsmi_auto_infracao
			// aut"
			// + " where ata.cod_ata = req.cod_ata"
			// + " and req.cod_auto_infracao = aut.cod_auto_infracao"
			// + " and req.cod_result_rs is null"
			// + " and ata.tipo_ata = '" + sqlOption + "'"
			// + " and req.cod_tipo_solic = '" + codTipoSolic + "'"
			// + " order by ata.des_ata";

			sCmd = "select ata.cod_ata, ata.des_ata, ata.tipo_ata" + " from tsmi_ata ata" + " where ata.tipo_ata = '"
					+ sqlOption + "'" + " and ata.status = 'A'" + " and cd_natureza = " + cdNatureza;

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					// optRetorno +="<select name="+ nameSelect + " id= "+
					// nameSelect + " onChange="+"javascript:alert(" + "'ola'"
					// +")"+">";
					// <select name="tipoAta" id="tipoAta"
					// onchange="ChamPopula()">
					optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " onchange="
							+ "verificaQtdeProcAtribuidos(); limparProcessosSemRelator();" + ">";
					optRetorno += "<option value=" + 0 + "> " + "SELECIONE" + "</option>";
					while (rs.next()) {
						// optRetorno +="<option value="+
						// rs.getString("DES_ATA")+"> " +
						// rs.getString("DES_ATA") + "</option>";
						optRetorno += "<option value=" + rs.getInt("cod_ata") + "> " + rs.getString("des_ata")
								+ "</option>";
					}
					optRetorno += "</select>";
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return optRetorno;
	}

	public String montaSelectSqlFiltro(String sqlOption, String nameSelect) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS  FROM TSMI_ATA  WHERE TIPO_ATA= '"
					+ sqlOption + "'  AND STATUS != 'F'";

			ResultSet rs = stmt.executeQuery(sCmd);
			// rs.next(); //vai para primeira posicao
			// rs.beforeFirst(); //antes da primeira posicao do resultset

			try {
				if (rs != null) {
					int contador = 0;
					while (rs.next()) {
						contador++;
					}
					if (contador == 1) {
						rs = stmt.executeQuery(sCmd);
						while (rs.next()) {
							optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " onchange="
									+ "verificaQtdeProcAtribuidos()" + ">";
							optRetorno += "<option value=" + rs.getInt("COD_ATA") + "> " + rs.getString("DES_ATA")
									+ "</option>";
							optRetorno += "</select>";
						}
					} else {
						rs = stmt.executeQuery(sCmd);
						optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " onchange="
								+ "verificaQtdeProcAtribuidos()" + ">";
						optRetorno += "<option value=" + 0 + "> " + "SELECIONE" + "</option>";
						while (rs.next()) {
							optRetorno += "<option value=" + rs.getInt("COD_ATA") + "> " + rs.getString("DES_ATA")
									+ "</option>";
						}
						optRetorno += "</select>";
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return optRetorno;
	}

	public String montaSelectSqlRelator(String nameSelect) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_RELATOR, NOM_RELATOR FROM TSMI_RELATOR ";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " >";
					optRetorno += "<option value=" + "SELECIONE" + "> " + "SELECIONE" + "</option>";
					while (rs.next()) {
						// optRetorno +="<option value="+
						// rs.getString("NOM_RELATOR")+"> " +
						// rs.getString("NOM_RELATOR") + "</option>";
						optRetorno += "<option value=" + rs.getInt("COD_RELATOR") + "> " + rs.getString("NOM_RELATOR")
								+ "</option>";
					}
					optRetorno += "</select>";
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return optRetorno;
	}

	// m�todo victor para preencher o relator
	public String SelectRelatorProcesso(String nameSelect, String junta, String codOrgao, String cdNatureza)
			throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		int codUltJunta = Integer.parseInt(junta);

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT R.COD_RELATOR, R.NOM_RELATOR, R.CD_NATUREZA" + " FROM TSMI_RELATOR R, TSMI_JUNTA J"
					+ " WHERE J.COD_JUNTA = R.COD_JUNTA" + " AND J.COD_ULT_JUNTA = '" + codUltJunta + "'"
					+ " AND J.COD_ORGAO = " + codOrgao + " AND R.IND_ATIVO = 'S'" + " ORDER BY R.NOM_RELATOR ASC";

			ResultSet rs = stmt.executeQuery(sCmd);

			try {
				if (rs != null) {
					optRetorno += "<select name=" + nameSelect + " id= " + nameSelect
							+ " onchange='EscolherRelator(this.form);'>";
					optRetorno += "<option value=" + "SELECIONE" + "> " + "SELECIONE" + "</option>";
					while (rs.next()) {
						// optRetorno +="<option value="+
						// rs.getString("NOM_RELATOR")+"> " +
						// rs.getString("NOM_RELATOR") + "</option>";
						if (cdNatureza.equals(rs.getString("CD_NATUREZA")) || rs.getString("CD_NATUREZA") == null)
							optRetorno += "<option value=" + rs.getInt("COD_RELATOR") + "> "
									+ rs.getString("NOM_RELATOR") + "</option>";
					}
					optRetorno += "</select>";
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {

				}
			}
		}
		return optRetorno;
	}

	public static long converteData2Long(String data, String formato) throws ParseException {
		SimpleDateFormat formatter = new SimpleDateFormat(formato);
		Date date = formatter.parse(data);

		return date.getTime();
	}

	public Ata getAtasGen(String codAta) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		Ata ata = new Ata();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS, QTDE_PROCESSO, DATA_PUBLICACAO  FROM TSMI_ATA WHERE COD_ATA = ' "
					+ codAta + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						ata.setCodAta(String.valueOf(rs.getInt("COD_ATA")));
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						ata.setQtdeAtaProcesso(rs.getInt("QTDE_PROCESSO"));
						ata.setDataPublicacao(rs.getString("DATA_PUBLICACAO"));
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ata;
	}

	public Ata getAtasDaVez(String nrAuto) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String statusMulta = "";
		Ata ata = new Ata();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_STATUS  FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = '" + nrAuto + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						statusMulta = String.valueOf(rs.getInt("COD_STATUS"));
						if ((statusMulta.length() > 0) && (statusMulta != null)) {
							if (statusMulta.equals("0")) { // Defesa Previa -
															// Oferecer ata da
															// semana do tipo
															// Defesa Previa que
															// estaja aberta e
															// no limite
								ata = getAtaDaSemana("Def. Previa");
							} else {
								if (statusMulta.equals("10") || statusMulta.equals("30")) {// JARI
																							// -
																							// Oferecer
																							// ata
																							// da
																							// semana
																							// do
																							// tipo
																							// JARI
																							// que
																							// estaja
																							// aberta
																							// e
																							// no
																							// limite
									ata = getAtaDaSemana("1� INST�NCIA");
								}
							}
						} else {
							ata = null;
						}
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ata;
	}

	public ArrayList<Ata> getAtasDistribuicao() throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS  FROM TSMI_ATA WHERE TIPO_ATA = '1� INST�NCIA' ORDER BY COD_ATA";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {
						Ata ata = new Ata();
						// ata.setCodAta(String.valueOf(rs.getInt("COD_ATA")));
						System.out.println("Valor de tipo ata:" + rs.getString("TIPO_ATA"));
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						listAtas.add(ata);
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	/**
	 * ----------------------------------------------------------- Le Perfis
	 * -----------------------------------------------------------
	 */
	public ArrayList<Ata> getAtasJR() throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS, qtde_processo  FROM TSMI_ATA WHERE TIPO_ATA = '1� INST�NCIA' "
					+ " AND COD_ORGAO = '" + ControladorRecGeneric.codOrgao + "'ORDER BY COD_ATA";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {
						Ata ata = new Ata();
						// ata.setCodAta(String.valueOf(rs.getInt("COD_ATA")));
						System.out.println("Valor de tipo ata:" + rs.getString("TIPO_ATA"));
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						ata.setQtdeAtaProcesso(rs.getInt("QTDE_PROCESSO"));
						listAtas.add(ata);
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	public ArrayList<Ata> getAtasII() throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO,
			// DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS, QTDE_PROCESSO FROM
			// TSMI_ATA WHERE TIPO_ATA = '2� INST�NCIA' ORDER BY COD_ATA";
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS, qtde_processo  FROM TSMI_ATA WHERE TIPO_ATA = '2� INST�NCIA' "
					+ " AND COD_ORGAO = '" + ControladorRecGeneric.codOrgao + "'ORDER BY COD_ATA";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					listAtas = new ArrayList<Ata>();

					while (rs.next()) {
						Ata ata = new Ata();
						// ata.setCodAta(String.valueOf(rs.getInt("COD_ATA")));
						System.out.println("Valor de tipo ata:" + rs.getString("TIPO_ATA"));
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						ata.setQtdeAtaProcesso(rs.getInt("QTDE_PROCESSO"));
						listAtas.add(ata);
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listAtas;
	}

	/*
	 * Verifica a ata da semana de acordo com o tipo DF ou JARI e o seu referido
	 * status. O sistema tem que exibir as atas do tipo DP ou JARI que estao
	 * abertas para a semana Data da atual nao pode ser superior a data da
	 * sessao e nem a data da distribuicao(data de fechamento da ata)
	 */

	public Ata getAtaDaSemana(String tpAta) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String dataAtual = "";
		Ata ata = new Ata();
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			dataAtual = dateFormat.format(date);
			System.out.print("A data �: " + dataAtual);

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT COD_ATA, TIPO_ATA, DES_ATA, DT_SESSAO, DT_DISTRIBUICAO, JUNTA_ATA, DT_AVISO, STATUS  FROM TSMI_ATA"
					+ " WHERE TIPO_ATA = '" + tpAta + "' AND " + "  STATUS = 'A' AND "
					+ " TRUNC(DT_SESSAO) >= TRUNC(DT_DISTRIBUICAO) ORDER BY COD_ATA";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					// while (rs.next()) {
					if (rs.next()) {
						System.out.println("Valor de tipo ata:" + rs.getString("TIPO_ATA"));
						ata.setCodAta(rs.getString("COD_ATA"));
						ata.setTpAta(rs.getString("TIPO_ATA"));
						ata.setDescAta(rs.getString("DES_ATA"));
						ata.setDataSessao(retornaDataFormat(String.valueOf(rs.getDate("DT_SESSAO"))));
						ata.setDataDistribuicao(retornaDataFormat(String.valueOf(rs.getDate("DT_DISTRIBUICAO"))));
						ata.setJuntaAta(String.valueOf(rs.getInt("JUNTA_ATA")));
						ata.setDataAviso(retornaDataFormat(String.valueOf(rs.getDate("DT_AVISO"))));
						ata.setStatusAta(rs.getString("STATUS"));
						// }
					} else {
						ata = null;
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ata;
	}

	public Ata getAtasInfResult(String nrAuto) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		Ata ata = new Ata();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = " SELECT DS_ATA, COD_ATA FROM TSMI_AUTO_INFRACAO AU WHERE AU.NUM_AUTO_INFRACAO = '" + nrAuto + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {

					while (rs.next()) {
						ata.setDescAta(rs.getString("DS_ATA"));
						ata.setCodAta(rs.getString("COD_ATA"));
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ata;
	}

	public String retornaDataFormat(String data) { // vem assim 2014-03-17
		data.replace("-", "/"); // 2014/03/17
		String ano = data.substring(0, 4);
		String mes = data.substring(5, 7);
		String dia = data.substring(8, 10);
		return dia + "/" + mes + "/" + ano;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosSemAta(String codOrgao, String tipoAta,
			int cdNatureza) {
		ArrayList<HashMap<String, String>> processos = new ArrayList<HashMap<String, String>>();

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "select req.cod_requerimento, aut.num_auto_infracao, aut.num_processo"
					+ " from tsmi_auto_infracao aut, tsmi_requerimento req" + " where aut.cod_orgao = " + codOrgao
					+ " and req.cod_auto_infracao = aut.cod_auto_infracao"
					+ " and (req.cod_ata is null or req.cod_ata = 0)" +
					// " and req.cod_result_rs is null" +
					" and req.cod_tipo_solic = '" + tipoAta + "'" + " and aut.cd_natureza = " + cdNatureza;

			query += " order by req.dat_requerimento, req.num_requerimento";

			ResultSet result = state.executeQuery(query);

			if (result != null) {
				while (result.next()) {
					HashMap<String, String> processo = new HashMap<String, String>();

					processo.put("codRequerimento", result.getString("cod_requerimento"));
					processo.put("numAuto", result.getString("num_auto_infracao"));
					processo.put("numProcesso", result.getString("num_processo"));

					processos.add(processo);
				}
			}

			result.close();
			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: consultarProcessosSemAta(): " + ex.getMessage());
		}

		return processos;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosComAta(String codOrgao, String codAta) {
		ArrayList<HashMap<String, String>> processos = new ArrayList<HashMap<String, String>>();

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "select distinct req.cod_requerimento, ai.num_auto_infracao, ai.num_processo"
					+ " from tsmi_auto_infracao ai, tsmi_requerimento req"
					+ " where req.cod_auto_infracao = ai.cod_auto_infracao" + " and ai.num_processo is not null"
					+ " and ai.cod_orgao = " + codOrgao + " and req.cod_ata = " + codAta + " order by ai.num_processo";

			ResultSet result = state.executeQuery(query);

			if (result != null) {
				while (result.next()) {
					HashMap<String, String> processo = new HashMap<String, String>();

					processo.put("codRequerimento", result.getString("cod_requerimento"));
					processo.put("numAuto", result.getString("num_auto_infracao"));
					processo.put("numProcesso", result.getString("num_processo"));

					processos.add(processo);
				}
			}

			result.close();
			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: consultarProcessosComAta(): " + ex.getMessage());
		}

		return processos;
	}

	public ArrayList<HashMap<String, String>> consultarAtas(String tipoAta, String codOrgao, String cdNatureza) {
		ArrayList<HashMap<String, String>> atas = new ArrayList<HashMap<String, String>>();

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "select ata.cod_ata, ata.des_ata, to_char(ata.dat_inclusao, 'yyyy') ano"
					+ " from tsmi_ata ata" + " where ata.cod_orgao = " + codOrgao + " and ata.tipo_ata = '" + tipoAta
					+ "'" + " and ata.cd_natureza = " + cdNatureza + " and ata.status = 'A'" + " order by ata.des_ata";

			ResultSet result = state.executeQuery(query);

			if (result != null) {
				while (result.next()) {
					HashMap<String, String> ata = new HashMap<String, String>();

					ata.put("codAta", result.getString("cod_ata"));
					ata.put("desAta", result.getString("des_ata"));
					ata.put("dataInclusao", result.getString("ano"));

					atas.add(ata);
				}
			}

			result.close();
			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: consultarAtas(): " + ex.getMessage());
		}

		return atas;
	}

	public HashMap<String, Object> consultarAtaPor(String codAta) {
		HashMap<String, Object> ata = null;

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "select *" + " from tsmi_ata ata" + " where ata.cod_ata = " + codAta;

			ResultSet result = state.executeQuery(query);

			if (result.next()) {
				ata = new HashMap<String, Object>();

				ata.put("codAta", result.getString("COD_ATA"));
				ata.put("tipoAta", result.getString("TIPO_ATA"));
				ata.put("desAta", result.getString("DES_ATA"));
				ata.put("datSessao", DataUtils.dateToDateBr(result.getString("DT_SESSAO")));
				ata.put("datDistribuicao", DataUtils.dateToDateBr(result.getString("DT_DISTRIBUICAO")));
				ata.put("juntaAta", result.getString("JUNTA_ATA"));
				ata.put("datAviso", DataUtils.dateToDateBr(result.getString("DT_AVISO")));
				ata.put("status", result.getString("STATUS"));
				ata.put("qntProcesso", result.getString("QTDE_PROCESSO"));
				ata.put("numAta", result.getString("NR_ATA"));
				ata.put("anoAta", result.getString("ANO_ATA"));
				ata.put("datInclusao", DataUtils.dateToDateBr(result.getString("DAT_INCLUSAO")));
				ata.put("codOrgao", result.getString("COD_ORGAO"));
				ata.put("datPublicacao", DataUtils.dateToDateBr(result.getString("DATA_PUBLICACAO")));
				ata.put("codNatureza", result.getString("CD_NATUREZA"));
			}

			result.close();
			state.close();
			conn.close();
		} catch (Exception ex) {
		}

		return ata;
	}

	public List<Ata> consultarAtasPorStatus(String status, String cdNatureza) throws Exception {
		List<Ata> lista = new ArrayList<Ata>();
		Connection conn = null;
		Statement stmt = null;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			stmt = conn.createStatement();
			String query = "select * from tsmi_ata ata where ata.status = '" + status.toUpperCase()
					+ "' and ata.cd_natureza = " + cdNatureza + " order by ata.ANO_ATA desc, ata.DES_ATA";
			ResultSet result = stmt.executeQuery(query);

			while (result.next()) {
				Ata ata = Ata.mapFrom(result);

				lista.add(ata);
			}

			stmt.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());

			throw ex;
		} finally {
			if (stmt != null)
				stmt.close();

			if (conn != null)
				conn.close();
		}

		return lista;
	}

	public ArrayList<HashMap<String, String>> getLoteAuto(String nomeDbLink, String dsLote, String donoTabela) {
		ArrayList<HashMap<String, String>> loteAutos = new ArrayList<HashMap<String, String>>();

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "";
			Lote lote = new Lote();
			if (donoTabela.equals("")) {
				query = "SELECT * FROM CTL_NOTIFICACAO_AUTUACAO@" + nomeDbLink + " " + " WHERE CD_LOTE= '" + dsLote
						+ "'";
				ResultSet result = state.executeQuery(query);

				if (result != null) {
					while (result.next()) {
						HashMap<String, String> auto = new HashMap<String, String>();

						String numAuto = (result.getString("NR_SERIE") + result.getString("CD_MULTA")).replace(" ", "");

						auto.put("nrSerie", result.getString("NR_SERIE"));
						auto.put("cdMulta", result.getString("CD_MULTA"));
						auto.put("status", lote.callStatus(numAuto)); // retorna
																		// statusAuto
						loteAutos.add(auto);
					}
				}
			} else {
				query = "SELECT * FROM " + donoTabela + ".CTL_NOTIFICACAO_AUTUACAO " + " WHERE CD_LOTE= '" + dsLote
						+ "'";
				ResultSet result = state.executeQuery(query);

				if (result != null) {
					while (result.next()) {
						HashMap<String, String> auto = new HashMap<String, String>();

						auto.put("nrSerie", result.getString("NR_SERIE"));
						auto.put("cdMulta", result.getString("CD_MULTA"));

						loteAutos.add(auto);
					}
				}
			}

			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: getLoteAuto(): " + ex.getMessage());
		}

		return loteAutos;
	}

	public String getSelectLoteAuto(String nomeDbLink, String donoTabela, String nameSelect) {
		String optRetorno = "";
		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();
			String query = "";
			if (donoTabela.equals("")) {
				query = "SELECT DISTINCT CD_LOTE FROM CTL_NOTIFICACAO_AUTUACAO@" + nomeDbLink + "";
				ResultSet rs = state.executeQuery(query);

				if (rs != null) {
					while (rs.next()) {
						optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " onchange="
								+ "callLoteAutos(this.form);" + ">";
						optRetorno += "<option value=" + 0 + "> " + "SELECIONE" + "</option>";
						while (rs.next()) {
							optRetorno += "<option value=" + rs.getInt("CD_LOTE") + "> " + rs.getString("CD_LOTE")
									+ "</option>";
						}
						optRetorno += "</select>";
					}
				}
			} else {
				query = "SELECT DISTINCT CD_LOTE  FROM " + donoTabela + ".CTL_NOTIFICACAO_AUTUACAO " + "'";
				ResultSet rs = state.executeQuery(query);

				if (rs != null) {
					while (rs.next()) {
						optRetorno += "<select name=" + nameSelect + " id= " + nameSelect + " onchange="
								+ "callLoteAutos(this.form);" + ">";
						optRetorno += "<option value=" + 0 + "> " + "SELECIONE" + "</option>";
						while (rs.next()) {
							optRetorno += "<option value=" + rs.getInt("CD_LOTE") + "> " + rs.getString("CD_LOTE")
									+ "</option>";
						}
						optRetorno += "</select>";
					}
				}
			}

			state.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: getLoteAuto(): " + ex.getMessage());
		}

		return optRetorno;
	}

	public String consultarCodAutoPor(String codRequerimento) {
		String codAuto = "";

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();

			String query = "select req.cod_auto_infracao" + "	from tsmi_requerimento req"
					+ " where req.cod_requerimento = " + codRequerimento;

			ResultSet result = state.executeQuery(query);

			result.next();

			codAuto = result.getString("cod_auto_infracao");

			state.close();
			conn.commit();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: consultarAutoPor(): " + ex.getMessage());
		}

		return codAuto;
	}

	public void atribuirAta(ArrayList<HashMap<String, String>> processos, String codAta, String desAta, String codOrgao,
			String nomeUsuario) {
		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();

			for (HashMap<String, String> processo : processos) {
				RequerimentoBean requerimento = RequerimentoBean
						.consultarRequerimentoPor(processo.get("codRequerimento"));
				String codAuto = requerimento.getCodAutoInfracao();
				AutoInfracaoBean auto = AutoInfracaoBean.consultarAutoPor(Integer.parseInt(codAuto));

				String query = "update tsmi_auto_infracao aut" + " set aut.cod_ata = '" + codAta + "',"
						+ " aut.ds_ata = '" + desAta + "'" + " where aut.cod_orgao = " + codOrgao
						+ " and aut.cod_auto_infracao = " + codAuto;

				String updateRequerimento = "update tsmi_requerimento req" + " set req.cod_ata = " + codAta
						+ " where req.cod_requerimento = " + processo.get("codRequerimento");

				boolean autoAtualizado = state.execute(query);

				if (!autoAtualizado) {
					boolean requerimentoAtualizado = state.execute(updateRequerimento);

					if (!requerimentoAtualizado) {
						new HistoricoBean().cadastrarHistorico(requerimento, auto.getNumProcesso(), codOrgao, desAta,
								nomeUsuario, codAta, HistoricoBean.ATRIBUIR_ATA);
					} else
						throw new Exception("requerimento n�o atualizado!");
				} else
					throw new Exception("auto n�o atualizado!");
			}

			state.close();
			conn.commit();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: atribuirAta(): " + ex.getMessage());
		}
	}

	public void removerAta(ArrayList<HashMap<String, String>> processos, String codOrgao, String desAta,
			String nomeUsuario, String codAta) {
		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement state = conn.createStatement();

			for (HashMap<String, String> processo : processos) {
				RequerimentoBean requerimento = RequerimentoBean
						.consultarRequerimentoPor(processo.get("codRequerimento"));
				String codAuto = requerimento.getCodAutoInfracao();
				AutoInfracaoBean auto = AutoInfracaoBean.consultarAutoPor(Integer.parseInt(codAuto));

				String queryAuto = "update tsmi_auto_infracao aut" + " set aut.cod_ata = 0,"
						+ " aut.ds_ata = 'SELECIONE'" + " where aut.cod_orgao = " + codOrgao
						+ " and aut.cod_auto_infracao = '" + codAuto + "'";

				String queryRequerimento = "update tsmi_requerimento req" + " set req.cod_ata = null"
						+ " where req.cod_requerimento = " + processo.get("codRequerimento");

				boolean autoAtualizado = state.execute(queryAuto);

				if (!autoAtualizado) {
					boolean requerimentoAtualizado = state.execute(queryRequerimento);

					if (!requerimentoAtualizado)
						new HistoricoBean().cadastrarHistorico(requerimento, auto.getNumProcesso(), codOrgao, desAta,
								nomeUsuario, codAta, HistoricoBean.REMOVER_ATA);
					else
						throw new Exception("requerimento n�o atualizado!");
				} else
					throw new Exception("auto n�o atualizado!");
			}

			state.close();
			conn.commit();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO NA CLASS DAO - M�TODO: removerAta(): " + ex.getMessage());
		}
	}

	// Sem atribui��o porem com requerimento de selecione
	public ArrayList<HashMap<String, String>> getProcessoSemAtribuicaoMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator, String codOrgao) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		ArrayList<HashMap<String, String>> listaProcessos = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> maps;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT DISTINCT" + " REQ.COD_REQUERIMENTO," + " AI.NUM_PROCESSO," + " AI.DAT_PROCESSO,"
					+ " TO_CHAR(AI.DAT_PROCESSO, 'YYYY') ANO" + " FROM" + " TSMI_REQUERIMENTO REQ,"
					+ " TSMI_AUTO_INFRACAO AI" + " WHERE REQ.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO"
					+ " AND AI.COD_ORGAO = " + codOrgao + " AND REQ.COD_ATA = " + codAta
					+ " AND REQ.COD_RESULT_RS IS NULL" + " AND (REQ.COD_RELATOR_JU IS NULL OR REQ.COD_RELATOR_JU = 0)"
					+ " ORDER BY AI.NUM_PROCESSO ASC";

			ResultSet rs = stmt.executeQuery(sCmd);

			try {
				if (rs != null) {
					while (rs.next()) {
						HashMap<String, String> hashProcesso = new HashMap<String, String>();

						hashProcesso.put("cod_requerimento", rs.getString("cod_requerimento"));
						hashProcesso.put("num_processo", rs.getString("num_processo"));
						hashProcesso.put("dat_processo", rs.getString("dat_processo"));
						hashProcesso.put("ano", rs.getString("ano"));

						listaProcessos.add(hashProcesso);
					}
				}
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listaProcessos;
	}

	public ArrayList<HashMap<String, String>> getProcessoComAtribuicaoMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator, String codOrgao) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		ArrayList<HashMap<String, String>> listaProcessos = new ArrayList<HashMap<String, String>>();
		HashMap<String, String> maps;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			if ((codRelator.equals("SELECIONE")) && (codRelator != null)) {
				sCmd = " SELECT DISTINCT r.cod_requerimento, NUM_PROCESSO, R.NUM_REQUERIMENTO, AI.NUM_AUTO_INFRACAO, AI.COD_AUTO_INFRACAO, AI.COD_ATA, AI.COD_ORGAO, AI.COD_MUNICIPIO,  A.TIPO_ATA, A.DES_ATA, AI.DAT_PROCESSO , TO_CHAR(AI.DAT_PROCESSO,'YYYY') ANO  , R.COD_RELATOR_JU , T.NOM_RELATOR, AI.NUM_PLACA, R.NOM_USERNAME_DP, ai.cod_infracao, ai.nom_proprietario, a.dat_inclusao "
						+ " FROM TSMI_AUTO_INFRACAO AI , TSMI_ATA A, TSMI_REQUERIMENTO R , TSMI_RELATOR T "
						+ " WHERE R.COD_ATA = '" + codAta + "' " + " AND AI.COD_ATA = A.COD_ATA "
						+ " AND R.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO " + " AND R.COD_RELATOR_JU = T.COD_RELATOR "
						+ " AND R.COD_RELATOR_JU = 0 " + " and ai.cod_orgao = " + codOrgao
						+ " ORDER BY AI.NUM_PROCESSO";
			} else {
				sCmd = "SELECT DISTINCT" + " REQ.COD_REQUERIMENTO," + " AI.NUM_PROCESSO," + " AI.DAT_PROCESSO,"
						+ " REL.NOM_RELATOR," + " TO_CHAR(AI.DAT_PROCESSO, 'YYYY') ANO" + " FROM"
						+ " TSMI_REQUERIMENTO REQ," + " TSMI_AUTO_INFRACAO AI," + " TSMI_RELATOR REL"
						+ " WHERE REQ.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO"
						+ " AND REQ.COD_RELATOR_JU = REL.COD_RELATOR" + " AND AI.COD_ORGAO = " + codOrgao
						+ " AND REQ.COD_ATA = " + codAta + " AND REQ.COD_RELATOR_JU = " + codRelator
						+ " AND REQ.COD_RESULT_RS IS NULL" + " ORDER BY AI.NUM_PROCESSO ASC";

			}

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						HashMap<String, String> hashProcesso = new HashMap<String, String>();

						hashProcesso.put("cod_requerimento", rs.getString("cod_requerimento"));
						hashProcesso.put("num_processo", rs.getString("num_processo"));
						hashProcesso.put("dat_processo", rs.getString("dat_processo"));
						hashProcesso.put("nom_relator", rs.getString("nom_relator"));
						hashProcesso.put("ano", rs.getString("ano"));

						listaProcessos.add(hashProcesso);
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return listaProcessos;
	}

	public ArrayList<HashMap<String, String>> getProcessoInfResultMap(String tipoRequerimento, String codAta,
			String descAta, String codRelator, String nomeRelator) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		ArrayList<HashMap<String, String>> arList = null;
		HashMap<String, String> maps;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			if ((codRelator.equals("SELECIONE")) && (codRelator != null)) {
				/*
				 * sCmd =
				 * " SELECT DISTINCT r.cod_requerimento, NUM_PROCESSO, AI.COD_ORGAO, AI.COD_MUNICIPIO,  R.COD_TIPO_SOLIC, R.COD_STATUS_REQUERIMENTO, AI.NUM_PLACA, R.NOM_USERNAME_JU, R.NUM_REQUERIMENTO, R.COD_RESULT_RS, AI.NUM_AUTO_INFRACAO, AI.COD_AUTO_INFRACAO, AI.COD_ATA, A.TIPO_ATA, A.DES_ATA, AI.DAT_PROCESSO , TO_CHAR(AI.DAT_PROCESSO,'YYYY') ANO  , R.COD_RELATOR_JU , T.NOM_RELATOR "
				 * +
				 * " FROM TSMI_AUTO_INFRACAO AI , TSMI_ATA A, TSMI_REQUERIMENTO R , TSMI_RELATOR T "
				 * + " WHERE AI.COD_ATA = '" + codAta + "' " +
				 * " AND AI.COD_ATA = A.COD_ATA " +
				 * " AND R.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO " +
				 * " AND R.COD_RELATOR_JU = T.COD_RELATOR " +
				 * " AND R.COD_RELATOR_JU = 0" + " order by ai.num_processo";
				 */

				sCmd = "SELECT DISTINCT r.cod_requerimento, NUM_PROCESSO, AI.COD_ORGAO, AI.COD_MUNICIPIO,  R.COD_TIPO_SOLIC, R.COD_STATUS_REQUERIMENTO,  R.NUM_REQUERIMENTO, AI.NUM_AUTO_INFRACAO, AI.COD_AUTO_INFRACAO, AI.NUM_PLACA, AI.COD_ATA, A.TIPO_ATA, A.DES_ATA, AI.DAT_PROCESSO , TO_CHAR(AI.DAT_PROCESSO,'YYYY') ANO  , R.COD_RELATOR_JU , R.NOM_USERNAME_DP, R.NOM_USERNAME_JU, R.COD_RESULT_RS, T.NOM_RELATOR "
						+ " FROM TSMI_AUTO_INFRACAO AI , TSMI_ATA A, TSMI_REQUERIMENTO R , TSMI_RELATOR T "
						+ " WHERE R.COD_ATA = '" + codAta + "' AND R.COD_ATA = A.COD_ATA "
						+ " AND R.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO " + " AND R.NUM_REQUERIMENTO IS NOT NULL"
						+ " and r.cod_relator_ju = t.cod_relator" + " order by ai.num_processo";
			} else {
				sCmd =
				// " SELECT DISTINCT NUM_PROCESSO, AI.NUM_PLACA,
				// R.NOM_USERNAME_JU, R.NUM_REQUERIMENTO, R.COD_RESULT_RS,
				// AI.NUM_AUTO_INFRACAO, AI.COD_AUTO_INFRACAO, AI.COD_ATA,
				// A.TIPO_ATA, A.DES_ATA, AI.DAT_PROCESSO ,
				// TO_CHAR(AI.DAT_PROCESSO,'YYYY') ANO , R.COD_RELATOR_JU ,
				// T.NOM_RELATOR " +
				// " FROM TSMI_AUTO_INFRACAO AI , TSMI_ATA A, TSMI_REQUERIMENTO
				// R , TSMI_RELATOR T " +
				// " WHERE AI.COD_ATA = " + codAta +" " +
				// " AND AI.COD_ATA = A.COD_ATA " +
				// " AND R.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO " +
				// " AND R.COD_RELATOR_JU = T.COD_RELATOR " +
				// " AND R.COD_RELATOR_JU = " + codRelator +
				// " AND R.NUM_REQUERIMENTO IS NOT NULL";

				sCmd = " SELECT DISTINCT r.cod_requerimento, NUM_PROCESSO, AI.COD_ORGAO, AI.COD_MUNICIPIO,  R.COD_TIPO_SOLIC, R.COD_STATUS_REQUERIMENTO,  R.NUM_REQUERIMENTO, AI.NUM_AUTO_INFRACAO, AI.COD_AUTO_INFRACAO, AI.NUM_PLACA, AI.COD_ATA, A.TIPO_ATA, A.DES_ATA, AI.DAT_PROCESSO , TO_CHAR(AI.DAT_PROCESSO,'YYYY') ANO  , R.COD_RELATOR_JU , R.NOM_USERNAME_DP, R.NOM_USERNAME_JU, R.COD_RESULT_RS, T.NOM_RELATOR "
						+ " FROM TSMI_AUTO_INFRACAO AI , TSMI_ATA A, TSMI_REQUERIMENTO R , TSMI_RELATOR T "
						+ " WHERE R.COD_ATA = '" + codAta + "' AND R.COD_ATA = A.COD_ATA "
						+ " AND R.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO " + " AND R.COD_RELATOR_JU = T.COD_RELATOR  "
						+ " AND R.COD_RELATOR_JU = '" + codRelator + "' AND R.NUM_REQUERIMENTO IS NOT NULL"
						+ " order by ai.num_processo";
			}

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					arList = new ArrayList<HashMap<String, String>>();

					while (rs.next()) {
						AutoInfracaoBean auto = new AutoInfracaoBean();
						Ata ata = new Ata();
						// Auto
						auto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
						auto.setCodAutoInfracao(String.valueOf(rs.getInt("COD_AUTO_INFRACAO")));
						if (rs.getString("NUM_PLACA") == null) {
							auto.setNumPlaca("NAO ENCONTRADA");
						} else {
							auto.setNumPlaca(String.valueOf(rs.getString("NUM_PLACA")));
						}
						auto.setNumProcesso(rs.getString("NUM_PROCESSO"));
						auto.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
						auto.setDatProcesso(rs.getString("DAT_PROCESSO"));

						maps = new HashMap<String, String>();
						maps.put("cod_requerimento", rs.getString("cod_requerimento"));
						maps.put("NUM_AUTO_INFRACAO", auto.getNumAutoInfracao());
						maps.put("COD_AUTO_INFRACAO", auto.getCodAutoInfracao());
						maps.put("NUM_PROCESSO", auto.getNumProcesso());
						maps.put("NUM_REQUERIMENTO", rs.getString("NUM_REQUERIMENTO"));
						maps.put("DAT_PROCESSO", auto.getDatProcesso());
						maps.put("ANO", rs.getString("ANO"));
						maps.put("COD_TIPO_SOLIC", rs.getString("COD_TIPO_SOLIC"));
						maps.put("COD_STATUS_REQUERIMENTO", rs.getString("COD_STATUS_REQUERIMENTO"));
						maps.put("COD_ORGAO", rs.getString("COD_ORGAO"));
						maps.put("COD_MUNICIPIO", rs.getString("COD_MUNICIPIO"));

						if (rs.getString("NUM_PLACA") == null) {
							maps.put("NUM_PLACA", "NAO ENCONTRADA");
						} else {
							maps.put("NUM_PLACA", rs.getString("NUM_PLACA"));
						}

						if (rs.getString("NOM_USERNAME_DP") == null) {
							maps.put("NOM_USERNAME_DP", "NAO ENCONTRADA");
						} else {
							maps.put("NOM_USERNAME_DP", rs.getString("NOM_USERNAME_DP"));
						}

						if (rs.getString("COD_RESULT_RS") == null) {
							maps.put("COD_RESULT_RS", "S/R");
						} else {
							maps.put("COD_RESULT_RS", rs.getString("COD_RESULT_RS"));
						}

						if ((codRelator.equals("0")) && (codRelator != null)) {
							maps.put("COD_RELATOR_JU", String.valueOf(rs.getInt("COD_RELATOR_JU")));
							maps.put("NOME_RELATOR", rs.getString("NOM_RELATOR"));
						} else {
							maps.put("COD_RELATOR_JU", rs.getString("COD_RELATOR_JU"));
							maps.put("NOME_RELATOR", rs.getString("NOM_RELATOR"));
						}

						// Ata
						ata.setCodAta(String.valueOf(rs.getInt("Cod_Ata")));
						maps.put("COD_ATA", ata.getCodAta());
						ata.setTpAta(rs.getString("TIPO_ATA"));
						maps.put("TIPO_ATA", ata.getTpAta());
						ata.setDescAta(rs.getString("DES_ATA"));
						maps.put("DES_ATA", ata.getDescAta());

						arList.add(maps);
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arList;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosPorRelator(String codAta, String codOrgao,
			String codRelator) {
		ArrayList<HashMap<String, String>> listaProcessos = new ArrayList<HashMap<String, String>>();

		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String query = "SELECT DISTINCT" + " REQ.COD_REQUERIMENTO," + " AI.NUM_PROCESSO," + " AI.DAT_PROCESSO,"
					+ " REL.NOM_RELATOR," + " TO_CHAR(AI.DAT_PROCESSO, 'YYYY') ANO," + " AI.DS_ATA,"
					+ " AI.DAT_INCLUSAO," + " AI.NUM_AUTO_INFRACAO," + " AI.COD_INFRACAO," + " AI.NOM_PROPRIETARIO,"
					+ " AI.NUM_PLACA" + " FROM TSMI_REQUERIMENTO REQ, TSMI_AUTO_INFRACAO AI," + " TSMI_RELATOR REL"
					+ " WHERE REQ.COD_AUTO_INFRACAO = AI.COD_AUTO_INFRACAO"
					+ " AND REQ.COD_RELATOR_JU = REL.COD_RELATOR" + " AND AI.COD_ORGAO = " + codOrgao
					+ " AND REQ.COD_ATA = " + codAta + " AND REQ.COD_RESULT_RS IS NULL";

			if (!codRelator.isEmpty() && !codRelator.equals("SELECIONE"))
				query += " AND REQ.COD_RELATOR_JU = " + codRelator;

			query += " ORDER BY REL.NOM_RELATOR, AI.NUM_PROCESSO";

			ResultSet result = stmt.executeQuery(query);

			while (result.next()) {
				HashMap<String, String> hashProcesso = new HashMap<String, String>();

				hashProcesso.put("cod_requerimento", result.getString("cod_requerimento"));
				hashProcesso.put("num_processo", result.getString("num_processo"));
				hashProcesso.put("dat_processo", result.getString("dat_processo"));
				hashProcesso.put("nom_relator", result.getString("nom_relator"));
				hashProcesso.put("ano", result.getString("ano"));
				hashProcesso.put("DS_ATA", result.getString("DS_ATA"));
				hashProcesso.put("DAT_INCLUSAO", result.getString("DAT_INCLUSAO"));
				hashProcesso.put("NUM_AUTO_INFRACAO", result.getString("NUM_AUTO_INFRACAO"));
				hashProcesso.put("COD_INFRACAO", result.getString("COD_INFRACAO"));
				hashProcesso.put("NOM_PROPRIETARIO", result.getString("NOM_PROPRIETARIO"));
				hashProcesso.put("NUM_PLACA", result.getString("NUM_PLACA"));
				hashProcesso.put("DAT_PROCESSO", result.getString("DAT_PROCESSO"));

				listaProcessos.add(hashProcesso);
			}

			result.close();
			stmt.close();
			conn.close();
		} catch (Exception ex) {
		}

		return listaProcessos;
	}

	public String getValParam(String codOrgao, String CONSTANTE) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getString("VAL_PARAMETRO");
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public boolean validaStatusRequerimentoAuto(String tpRequerimento, String numAuto) {
		try {
			String myUsrs = "";
			Connection conn = null;
			String sCmd = "";
			Cursor c;
			String status = "";
			try {
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();

				sCmd = "SELECT COD_STATUS FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = '" + numAuto + "'";

				/*
				 * RESULTADO = 0 DefesaPrevia ABRIU DEFESA = 5 INFORMOU
				 * RESULTADO = 10 ABRIU 1� INSTANCIA = 15 INFORMOU RESULTADO =
				 * 30 ABRIU 2� INSTANCIA = 35 INFORMOU RESULTADO = 35
				 */
				ResultSet rs = stmt.executeQuery(sCmd);
				try {
					if (rs != null) {
						while (rs.next()) {
							status = rs.getString("COD_STATUS");

							switch (tpRequerimento) {

							case "1":// Def. Previa
								if (status.equals("0") || status.equals("4")) {// Def.
																				// Previa
									return true;
								} else {
									return false;
								}

							case "2":// 1� INST�NCIA
								if (status.equals("10") || status.equals("0") || status.equals("1")) {// 1�
																										// INSTANCIA
									return true;
								} else {
									return false;
								}

							case "3":// 2� INST�NCIA
								if (status.equals("30")) {// 2� INSTANCIA
									return true;
								} else {
									return false;
								}
							default:
								break;
							}
						}
					}
				} catch (Exception e) {
				}
				rs.close();
				stmt.close();
			} catch (SQLException sqle) {
				throw new DaoException(sqle.getMessage());
			} catch (Exception ex) {
				throw new DaoException(ex.getMessage());
			} finally {
				if (conn != null) {
					try {
						serviceloc.setReleaseConnection(conn);
					} catch (Exception ey) {
					}
				}
			}
		} catch (Exception e) {
		}
		return false;
	}

	public String getNumProcAuto(String codOrgao, String CONSTANTE) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		// forma��o: 99999/AAAA, onde 99999 , sequencia num�rica e AAAA, ano
		// corrente
		String anoVigente = "";
		Calendar data = Calendar.getInstance();

		anoVigente = "/" + String.valueOf(data.get(Calendar.YEAR)); // Thu Mar
																	// 19
																	// 15:56:58
																	// BRT 2015
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getString("VAL_PARAMETRO"); // 00001

						// result = "20510"; teste com 5 digitos
						int proximoNR = Integer.valueOf(result); // 1
						String proximoNrSql = ""; // 1
						result = String.valueOf(proximoNR);

						if (result.length() == 5) {
							proximoNR++;
							proximoNrSql = String.valueOf(proximoNR);
							proximoNrSql = proximoNrSql;
						} else {
							if (result.length() == 1) {
								result = "0000" + result;
								proximoNR++;
								proximoNrSql = String.valueOf(proximoNR);
								proximoNrSql = "0000" + proximoNrSql;
							}

							if (result.length() == 2) {
								result = "000" + result;
								proximoNR++;
								proximoNrSql = String.valueOf(proximoNR);
								proximoNrSql = "000" + proximoNrSql;
							}

							if (result.length() == 3) {
								result = "00" + result;
								proximoNR++;
								proximoNrSql = String.valueOf(proximoNR);
								proximoNrSql = "00" + proximoNrSql;
							}

							if (result.length() == 4) {
								result = "0" + result;
								proximoNR++;
								proximoNrSql = String.valueOf(proximoNR);
								proximoNrSql = "0" + proximoNrSql;
							}
						}

						// Update proximo numero - inicio
						try {
							sCmd = "UPDATE TCAU_PARAM_ORGAO T SET T.VAL_PARAMETRO = '" + String.valueOf(proximoNrSql)
									+ "'\n" + " WHERE T.nom_parametro = 'ULT_NR_PROCESSO_JARI'\n"
									+ " AND   T.COD_ORGAO = '" + codOrgao + "'\n" + " AND   T.NOM_PARAMETRO = '"
									+ CONSTANTE + "'";
							rs = stmt.executeQuery(sCmd);

						} catch (Exception e) {
							e.getMessage().toString();
						}
						// Update proximo numero - fim

						result = result + anoVigente;

					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public boolean getDbLink(String codOrgao, String CONSTANTE) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getString("VAL_PARAMETRO");
						if (result.equals("S")) {
							return true;
						}
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return false;
	}

	public boolean processaLoteCmd(ArrayList<HashMap<String, String>> processos, String dsAcao, String codOrgao)
			throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;

			if (dsAcao.equals("APLICAR")) {
				for (HashMap<String, String> processo : processos) {
					String query = "update tsmi_auto_infracao aut" + " set aut.COD_STATUS = 16 "
							+ " where aut.NUM_AUTO_INFRACAO = '" + processo.get("nrSerie").replace(" ", "") + "'";

					rs = stmt.executeQuery(query);

				}
			}
			if (dsAcao.equals("REMOVER")) {
				for (HashMap<String, String> processo : processos) {
					String query = "update tsmi_auto_infracao aut" + " set aut.COD_STATUS = 0 "
							+ " where aut.NUM_AUTO_INFRACAO = '" + processo.get("nrSerie").replace(" ", "") + "'";

					rs = stmt.executeQuery(query);

				}
			}

			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return false;
	}

	public String getNomeDbLink(String codOrgao, String CONSTANTE) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getString("VAL_PARAMETRO");
						if (result.length() > 0) {
							return result;
						}
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public String getDonoTabela(String codOrgao, String CONSTANTE) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		String result = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getString("VAL_PARAMETRO");
						if (result.length() > 0) {
							return result;
						}
					}

				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public String montaSelectSqlLote(String codOrgao, String nameSelect) throws DaoException {
		String optRetorno = "";
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		try {
			Lote lote = new Lote();
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String varParanGeneric = "USA_DATA_BASE_LINK";
			if (lote.getDbLink(codOrgao, varParanGeneric)) {
				varParanGeneric = "NOME_DATA_BASE_LINK";
				String nomeDbLink = lote.getNomeDbLink(codOrgao, varParanGeneric);

				ArrayList<HashMap<String, String>> list = new ArrayList<>();
				optRetorno = lote.getSelectLoteAuto(nomeDbLink, "", nameSelect);

			} else {
				varParanGeneric = "DONO_DA_TABELA";
				String donoTabela = lote.getDonoTabela(codOrgao, varParanGeneric);
				optRetorno = lote.getSelectLoteAuto("", donoTabela, nameSelect);

			}
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return optRetorno;
	}

	public String callStatus(String numAuto) {
		String retorno = "";
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT C.COD_STATUS FROM TSMI_AUTO_INFRACAO C WHERE NUM_AUTO_INFRACAO = '" + numAuto + "'";
			ResultSet rs = stmt.executeQuery(sCmd);

			if (rs != null) {
				while (rs.next()) {
					if (rs.getInt("COD_STATUS") == 16) { // 16 � efeito
															// suspensivo
						retorno = "SIM";
						return retorno;
					} else {
						retorno = "NAO";
						return retorno;
					}

				}
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return retorno;
	}

	public int getNextNrAta(Ata ata, String cdNatureza) {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		int proximoNR = 0;
		String result = "";
		String dsResult = "";

		// forma��o: 000/TP/YY, onde 000 � a sequencia num�rica, TP � o tipo da
		// ata(Def.Previa, 1P OU 2P) e YY � o ano corrente
		String anoVigente = "";
		Calendar data = Calendar.getInstance();
		anoVigente = "/" + String.valueOf(data.get(Calendar.YEAR)); // Thu Mar
																	// 19
																	// 15:56:58
																	// BRT 2015

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// sCmd = " SELECT MAX(ATA.NR_ATA) AS ULT_NR_ATA FROM TSMI_ATA ATA
			// WHERE ATA.TIPO_ATA = '"+ ata.getTpAta() +"'";

			// sCmd = "SELECT NVL(MAX(ATA.NR_ATA),1) AS ULT_NR_ATA FROM TSMI_ATA
			// ATA WHERE ATA.TIPO_ATA = '"+ ata.getTpAta() +"' " +
			// "AND TO_CHAR(ATA.DAT_INCLUSAO,'YYYY') = TO_CHAR(SYSDATE,'YYYY')";

			sCmd = "SELECT MAX(ATA.NR_ATA) AS ULT_NR_ATA FROM TSMI_ATA ATA WHERE ATA.TIPO_ATA = '" + ata.getTpAta()
					+ "' " + "AND TO_CHAR(ATA.DAT_INCLUSAO,'YYYY') = TO_CHAR(SYSDATE,'YYYY') AND COD_ORGAO = '"
					+ ControladorRecGeneric.codOrgao + "'" + " and ata.cd_natureza = " + cdNatureza;

			ResultSet rs = stmt.executeQuery(sCmd);

			try {
				if (rs != null) {
					while (rs.next()) {
						String proximoNrSql = "";
						result = rs.getString("ULT_NR_ATA");

						if (result == null) {
							proximoNR = 1;
							return proximoNR;
						}

						if (result != null) {
							proximoNR = Integer.valueOf(result);
							if (proximoNR > 0) {
								proximoNR++;
							}
						}

						// if(result.length()==3){
						// proximoNR ++;
						// proximoNrSql = String.valueOf(proximoNR);
						// proximoNrSql = proximoNrSql;
						// System.out.print("Incrementa: " + result.toString());
						// }else{
						// if(result.length()==1){
						// result = "00" + result;
						// proximoNR ++;
						// proximoNrSql = String.valueOf(proximoNR);
						// proximoNrSql = "00" +proximoNrSql;
						// System.out.print("Incrementa: " + result.toString());
						// }
						//
						// if(result.length()==2){
						// result = "0" + result;
						// proximoNR ++;
						// proximoNrSql = String.valueOf(proximoNR);
						// proximoNrSql = "0" +proximoNrSql;
						// System.out.print("Incrementa: " + result.toString());
						// }
						// }
						// result = result;
						// proximoNR = Integer.valueOf(proximoNrSql);
						return proximoNR;
					}
				}

				rs.close();
				stmt.close();
				conn.close();
			} catch (Exception e) {
				e.getMessage().toString();
			}
		} catch (Exception e) {
			e.getMessage().toString();
		}
		return proximoNR;
	}

	public Ata populaAtaDao(int codAta) {
		Ata bla = new Ata();
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM TSMI_ATA A WHERE A.COD_ATA = ' " + codAta + "'";
			ResultSet rs = stmt.executeQuery(sCmd);

			if (rs != null) {
				while (rs.next()) {

					// exemplo dataSessao: 2015-03-19 00:00:00.0
					// 19-03-2015
					String dataSessao = rs.getString("DT_SESSAO");
					String anoSessao = dataSessao.substring(0, 4);
					String mesSessao = dataSessao.substring(5, 7);
					String diaSessao = dataSessao.substring(8, 10);

					bla.setJuntaAta(rs.getString("JUNTA_ATA"));
					bla.setDataSessao(diaSessao + "/" + mesSessao + "/" + anoSessao);
					bla.setTpAta(rs.getString("TIPO_ATA"));
					bla.setDescAta(rs.getString("DES_ATA"));
					bla.setQtdeAtaProcesso(rs.getInt("QTDE_PROCESSO"));
					bla.setStatusAta(rs.getString("STATUS"));
					bla.setNrAta(rs.getInt("NR_ATA"));
					bla.setAnoAta(rs.getInt("ANO_ATA"));

					// Verifica se a data da sess�o informada �
					// sabado|domingo|feriado, retorna true se a data for uma
					// das op��es
					if (!bla.validaData(bla.getDataSessao())) {
						Date d = new Date();
						/// Converte data sess�o(string) informada em Date
						d = bla.convertStringInDate(bla.getDataSessao());
						do {
							d.setDate(d.getDate() - 1); // Dimimui 1 dia
							// Data do aviso � data da Sess�o menos 1
							bla.setDataAviso(bla.convertDateInString(d)); // converte
																			// a
																			// data
																			// em
																			// string
																			// e
																			// atribui
																			// a
																			// aviso
						} while (bla.validaData(bla.getDataAviso())); // enquanto
																		// for
																		// feriado
																		// incrementa
																		// -1
																		// dia

						if (!bla.validaData(bla.getDataSessao()) && !bla.validaData(bla.getDataAviso())) { // se
																											// os
																											// dois
																											// forem
																											// falsos
																											// �
																											// porque
																											// eles
																											// nao
																											// s�o
																											// sabado,
																											// domingo
																											// e
																											// nem
																											// feriado.
							Date dd = new Date();
							// Converte data aviso informada em Date
							dd = bla.convertStringInDate(bla.getDataAviso());
							do {
								dd.setDate(dd.getDate() - 1); // Dimimui 1 dia

								// Data do distribui��o � data da Aviso menos 1
								bla.setDataDistribuicao(bla.convertDateInString(dd)); // 1
																						// dia
							} while (bla.validaData(bla.getDataDistribuicao())); // enquanto
																					// for
																					// feriado
																					// incrementa
																					// -1
																					// dia
						}

						// bla.isInsert(bla);
					}
				}
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.getMessage().toString();
		}

		return bla;
	}

	/*
	 * Retorna o valor padrao para abertura de ata
	 */
	public int getValParamQtdeProcess(String codOrgao, String CONSTANTE, String tpAta) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		int result = 0;
		String retornoValParametro = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT * FROM TCAU_PARAM_ORGAO T where t.nom_parametro = '" + CONSTANTE + "'\n"
					+ "and T.COD_ORGAO = '" + codOrgao + "'\n" + "AND T.NOM_PARAMETRO = '" + CONSTANTE + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						retornoValParametro = rs.getString("VAL_PARAMETRO");
					}

				}

				// ex valParametro = 25 ,10 , 5
				if (retornoValParametro.length() > 0) {
					String retorno = "";
					if (tpAta.equals("Def. Previa")) {
						retorno = retornoValParametro.substring(0, 2);
						result = Integer.valueOf(retorno);
					}
					if (tpAta.equals("1� INST�NCIA")) {
						retorno = retornoValParametro.substring(4, 6);
						result = Integer.valueOf(retorno);
					}
					if (tpAta.equals("2� INST�NCIA")) {
						retorno = retornoValParametro.substring(9, 10);
						result = Integer.valueOf(retorno);
					}
				}

			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public int getCodAta(String dsAta) throws DaoException {
		String myUsrs = "";
		Connection conn = null;
		String sCmd = "";
		Cursor c;
		int result = 0;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			sCmd = "SELECT COD_ATA FROM TSMI_ATA WHERE DES_ATA = '" + dsAta + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			try {
				if (rs != null) {
					while (rs.next()) {
						result = rs.getInt("COD_ATA");
					}
				}
			} catch (Exception e) {
			}
			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return result;
	}

	public HashMap<String, Object> consultarAutoPor(int cod) throws Exception {
		HashMap<String, Object> auto = new HashMap<String, Object>();

		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select * from tsmi_auto_infracao ai where ai.cod_auto_infracao = " + cod;

		ResultSet result = stm.executeQuery(query);

		if (result.next())
			auto = AutoInfracaoBean.mapResultSetToHashMap(result);
		else
			return null;

		Set<String> sets = auto.keySet();
		for (String key : sets) {
			if (key == "NUM_PONTO")
				auto.put(key, "0");
			else if (auto.get(key) == null)
				auto.put(key, "");
		}

		stm.close();
		conn.close();

		return auto;
	}

	public HashMap<String, Object> consultarAutoPor(String numAuto, int cdNatureza) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select * from tsmi_auto_infracao ai where ai.num_auto_infracao = '" + numAuto + "'"
				+ " and ai.cd_natureza = " + cdNatureza;

		ResultSet result = stm.executeQuery(query);
		HashMap<String, Object> hashAuto = null;

		if (result.next())
			hashAuto = AutoInfracaoBean.mapResultSetToHashMap(result);

		stm.close();
		conn.close();

		return hashAuto;
	}

	public HashMap<String, Object> consultarAutoPorNumProcesso(String numProcesso, int cdNatureza) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select * from tsmi_auto_infracao ai where ai.num_processo = '" + numProcesso + "'"
				+ " and ai.cd_natureza = " + cdNatureza;

		ResultSet result = stm.executeQuery(query);
		HashMap<String, Object> hashAuto = null;

		if (result.next())
			hashAuto = AutoInfracaoBean.mapResultSetToHashMap(result);

		stm.close();
		conn.close();

		return hashAuto;
	}

	public List<AutoInfracaoBean> consultarAutosPor(String placa, String numAuto, String numProcesso, String orgao,
			String codNatureza, String dataInicial, String dataFinal) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select *" + " from tsmi_auto_infracao ai" + " where ai.cd_natureza = " + codNatureza
				+ " and ai.cod_orgao = " + orgao;

		if (!placa.isEmpty())
			query += " and ai.num_placa = '" + placa.trim().toUpperCase() + "'";

		if (!numAuto.isEmpty())
			query += " and ai.num_auto_infracao = '" + numAuto.trim().toUpperCase() + "'";

		if (!numProcesso.isEmpty())
			query += " and ai.num_processo = '" + numProcesso.trim().toUpperCase() + "'";

		query += " and ai.dat_infracao between '" + dataInicial.trim().toUpperCase() + "' and '"
				+ dataFinal.trim().toUpperCase() + "'";

		ResultSet result = stm.executeQuery(query);

		List<AutoInfracaoBean> lista = new ArrayList<AutoInfracaoBean>();

		while (result.next()) {
			HashMap<String, Object> hash = AutoInfracaoBean.mapResultSetToHashMap(result);
			AutoInfracaoBean auto = AutoInfracaoBean.mapHashMapToEntity(hash);

			lista.add(auto);
		}

		stm.close();
		conn.close();

		return lista;
	}

	public boolean verificarNumeroProcesso(String numeroProcesso, String numeroAuto) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select * from tsmi_auto_infracao ai where ai.num_processo = '" + numeroProcesso + "'"
				+ " and ai.num_auto_infracao <> '" + numeroAuto + "'";
		ResultSet result = stm.executeQuery(query);
		boolean jaExiste = false;

		if (result.next())
			jaExiste = true;

		stm.close();
		conn.close();

		return jaExiste;
	}

	public void atualizarAutoInfracao(AutoInfracaoBean auto) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "update tsmi_auto_infracao SET" + "       num_auto_infracao = '" + auto.getNumAutoInfracao()
				+ "', " + "       dat_infracao = '" + ControladorRecGeneric.dateToDateBr(auto.getDatInfracao()) + "', "
				+ "       val_hor_infracao = '" + auto.getValHorInfracao().replace(".", ",") + "', "
				+ "       dsc_local_infracao = '" + auto.getDscLocalInfracao() + "', " + "       num_placa = '"
				+ auto.getNumPlaca() + "', " + "       num_processo = '" + auto.getNumProcesso() + "', "
				+ "       dat_processo = '" + ControladorRecGeneric.dateToDateBr(auto.getDatProcesso()) + "', "
				+ "       cod_agente = '" + auto.getCodAgente() + "', " + "       cod_orgao = '" + auto.getCodOrgao()
				+ "', " + "       dsc_local_aparelho = '" + auto.getDscLocalAparelho() + "', "
				+ "       num_certif_afer_aparelho = '" + auto.getNumCertAferAparelho() + "', "
				+ "       dat_ult_afer_aparelho = '" + ControladorRecGeneric.dateToDateBr(auto.getDatUltAferAparelho())
				+ "', " + "       num_ident_aparelho = '" + auto.getNumIdentAparelho() + "', "
				+ "       num_inmetro_aparelho = '" + auto.getNumInmetroAparelho() + "', "
				+ "       cod_tipo_disp_registrador = '" + auto.getCodTipDispRegistrador() + "', "
				+ "       dat_vencimento = '" + ControladorRecGeneric.dateToDateBr(auto.getDatVencimento()) + "', "
				+ "       cod_municipio = '" + auto.getCodMunicipio() + "', " + "       val_veloc_permitidaa = '"
				+ auto.getValVelocPermitida().replace(".", ",") + "', " + "       val_veloc_aferida = '"
				+ auto.getValVelocAferida().replace(".", ",") + "', " + "       val_veloc_considerada = '"
				+ auto.getValVelocConsiderada().replace(".", ",") + "', " + "       dsc_situacao = '"
				+ auto.getDscSituacao() + "', " + "       ind_condutor_identificado = '"
				+ auto.getIndCondutorIdentificado() + "', " + "       num_lote = '" + auto.getCodLote() + "', "
				+ "       cod_status = '" + auto.getCodStatus() + "', " + "       dat_status = '"
				+ ControladorRecGeneric.dateToDateBr(auto.getDatStatus()) + "', " + "       dat_pubdo = '"
				+ ControladorRecGeneric.dateToDateBr(auto.getDatPublPDO()) + "', " + "       cod_envio_pubdo = '"
				+ auto.getCodEnvioDO() + "', " + "       dat_envdo = '"
				+ ControladorRecGeneric.dateToDateBr(auto.getDatEnvioDO()) + "', " + "       ind_tp_cv = '"
				+ auto.getIndTPCV() + "', " + "       cod_result_pubdo = '" + auto.getCodResultPubDO() + "', "
				+ "       cod_barra = '" + auto.getCodBarra() + "', " + "       ind_situacao = '"
				+ auto.getIndSituacao() + "', " + "       ind_fase = '" + auto.getIndFase() + "', "
				+ "       ind_pago = '" + auto.getIndPago() + "', " + "       num_ident_orgao = '"
				+ auto.getIdentOrgao() + "', " + "       dsc_resumo_infracao = '" + auto.getDscResumoInfracao() + "', "
				+ "       dsc_marca_modelo_aparelho = '" + auto.getDscMarcaModeloAparelho() + "', "
				+ "       dsc_local_infracao_notif = '" + auto.getDscLocalInfracaoNotificacao() + "', "
				+ "       cod_infracao = '" + auto.getInfracao().getCodInfracao() + "', " + "       dsc_infracao = '"
				+ auto.getInfracao().getDscInfracao() + "', " + "       dsc_enquadramento = '"
				+ auto.getInfracao().getDscEnquadramento() + "', " + "       val_real = '"
				+ auto.getInfracao().getValReal().replace(".", ",") + "', " + "       num_ponto = '"
				+ auto.getInfracao().getNumPonto() + "', " + "       dsc_competencia = '"
				+ auto.getInfracao().getDscCompetencia() + "', " + "       ind_cond_prop = '', "
				+ "       ind_suspensao = '" + auto.getInfracao().getIndSDD() + "', " + "       val_real_desconto = '"
				+ auto.getInfracao().getValRealDesconto().replace(".", ",") + "', " + "       cod_marca_modelo = '"
				+ auto.getVeiculo().getCodMarcaModelo() + "', " + "       cod_especie = '"
				+ auto.getVeiculo().getCodEspecie() + "', " + "       cod_categoria = '"
				+ auto.getVeiculo().getCodCategoria() + "', " + "       cod_tipo = '" + auto.getVeiculo().getCodTipo()
				+ "', " + "       cod_cor = '" + auto.getVeiculo().getCodCor() + "', " + "       nom_proprietario = '"
				+ auto.getProprietario().getNomResponsavel() + "', " + "       tip_doc_proprietario = '"
				+ auto.getProprietario().getIndCpfCnpj() + "', " + "       num_doc_proprietario = '"
				+ auto.getProprietario().getNumCpfCnpj() + "', " + "       tip_cnh_proprietario = '"
				+ auto.getProprietario().getIndTipoCnh() + "', " + "       num_cnh_proprietario = '"
				+ auto.getProprietario().getNumCnh() + "', " + "       dsc_end_proprietario = '"
				+ auto.getProprietario().getEndereco().getTxtEndereco() + "', " + "       num_end_proprietario = '"
				+ auto.getProprietario().getEndereco().getNumEndereco() + "', " + "       dsc_comp_proprietario = '"
				+ auto.getProprietario().getEndereco().getTxtComplemento() + "', " + "       cod_mun_proprietario = '"
				+ auto.getProprietario().getEndereco().getCodCidade() + "', " + "       num_cep_proprietario = '"
				+ auto.getProprietario().getEndereco().getNumCEP() + "', " + "       uf_proprietario = '"
				+ auto.getProprietario().getEndereco().getCodUF() + "', " + "       nom_condutor = '"
				+ auto.getCondutor().getNomResponsavel() + "', " + "       tip_doc_condutor = '"
				+ auto.getCondutor().getIndCpfCnpj() + "', " + "       num_doc_condutor = '"
				+ auto.getCondutor().getNumCpfCnpj() + "', " + "       tip_cnh_condutor = '"
				+ auto.getCondutor().getIndTipoCnh() + "', " + "       num_cnh_condutor = '"
				+ auto.getCondutor().getNumCnh() + "', " + "       dsc_end_condutor = '"
				+ auto.getCondutor().getEndereco().getTxtEndereco() + "', " + "       num_end_condutor = '"
				+ auto.getCondutor().getEndereco().getNumEndereco() + "', " + "       dsc_comp_condutor = '"
				+ auto.getCondutor().getEndereco().getTxtComplemento() + "', " + "       cod_mun_condutor = '"
				+ auto.getCondutor().getEndereco().getCodCidade() + "', " + "       num_cep_condutor = '"
				+ auto.getCondutor().getEndereco().getNumCEP() + "', " + "       uf_condutor = '"
				+ auto.getCondutor().getEndereco().getCodUF() + "', " + "       num_notif_autuacao = '"
				+ auto.getNumNotifAut() + "', " + "       dat_notif_autuacao = '"
				+ ControladorRecGeneric.dateToDateBr(auto.getDatNotifAut()) + "', " + "       cod_ent_autuacao = '', "
				+ "       num_lote_autuacao = '', " + "       num_notif_penalidade = '" + auto.getNumNotifPen() + "', "
				+ "       dat_notif_penalidade = '" + ControladorRecGeneric.dateToDateBr(auto.getDatNotifPen()) + "', "
				+ "       cod_ent_penalidade = '', " + "       num_lote_penalidade = '', " + "       dsc_agente = '"
				+ auto.getDscAgente() + "', " + "       dsc_unidade = '" + auto.getDscUnidade() + "', "
				+ "       dat_pagamento = '" + ControladorRecGeneric.dateToDateBr(auto.getDatPag()) + "', "
				+ "       num_auto_origem = '" + auto.getNumAutoOrigem() + "', " + "       cod_infracao_origem = '"
				+ auto.getNumInfracaoOrigem() + "', " + "       ind_tipo_auto = '', " + "       num_auto_renainf = '"
				+ auto.getNumInfracaoRenainf() + "', " + "       txt_email = '" + auto.getTxtEmail() + "', "
				+ "       dat_inclusao = '" + ControladorRecGeneric.dateToDateBr(auto.getDatInclusao()) + "', "
				+ "       dat_alteracao = '" + ControladorRecGeneric.dateToDateBr(auto.getDatAlteracao()) + "', "
				+ "       sit_ai_digitalizado = '', " + "       sit_ar_digitalizado = '', "
				+ "       dat_digitalizacao = '"
				+ ControladorRecGeneric.dateToDateBr(auto.getDatDigitalizacao().toLocaleString().substring(0, 10))
				+ "', " + "       dat_registro = '" + ControladorRecGeneric.dateToDateBr(auto.getDatRegistro()) + "', "
				+ "       sit_digitalizacao = '" + auto.getSit_digitaliza() + "', " + "       val_veloc_permitida = '"
				+ auto.getValVelocPermitida().replace(".", ",") + "', " + "       num_renavam = '"
				+ auto.getNumRenavam() + "', " + "       ident_orgao = '" + auto.getIdentOrgao() + "', "
				+ "       cod_acesso_web = '" + auto.getCodAcessoWeb() + "', " + "       obs_condutor = '"
				+ auto.getObsCondutor() + "', " + "       ind_pontuacao = '" + auto.getIndPontuacao() + "', "
				+ "       ind_parcelamento = '" + auto.getIndParcelamento() + "', " + "       cod_ata = '"
				+ auto.getCodAta() + "', " + "       ds_ata = '" + auto.getDsAta() + "' where cod_auto_infracao = "
				+ auto.getCodAutoInfracao();

		ResultSet result = stm.executeQuery(query);

		stm.close();
		conn.close();
	}
}