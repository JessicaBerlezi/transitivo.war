package ACSS;

import java.util.ArrayList;
import java.util.List;

import sys.BeanException;

/**
* <b>Title:</b>        Bloqueio Bean<br>
* <b>Description:</b>  Bean de Bloqueio - Classe Pai<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class BloqueioSenhaBean extends sys.HtmlPopupBean  {

	private String codBloqueio;
	private String codUsuario;
	private String codOrgao;
	private String dataIni;
	private String dataFim;
	private String indMotivo;
	
	private List dados;
	private String motivoBloqueio;
	private String nomUserName;
	
	
	public BloqueioSenhaBean(){
		dados = new ArrayList();
		codBloqueio = "0";
		codUsuario = "0";
		dataIni = "00/00/0000";
		dataFim = "00/00/0000";
		indMotivo = "0";
		motivoBloqueio = "";
		nomUserName = "";
		codOrgao = "0";
	}
	
	public void setCodBloqueio(String codBloqueio){
		this.codBloqueio = codBloqueio;
		if (codBloqueio == null) codBloqueio = "0";
	}
	public String getCodBloqueio(){
		return codBloqueio;
	}
	
	public void setCodOrgao(String codOrgao){
		this.codOrgao = codOrgao;
		if (codOrgao == null) codOrgao = "0";
	}
	public String getCodOrgao(){
		return codOrgao;
	}
	
	public void setCodUsuario(String codUsuario){
		this.codUsuario = codUsuario;
		if (codUsuario == null) codUsuario = "0";
	}
	public String getCodUsuario(){
		return codUsuario;
	}
	
	public void setNomUserName(String nomUserName){
		this.nomUserName = nomUserName;
		if (nomUserName == null) codBloqueio = "";
	}
	public String getNomUserName(){
		return nomUserName;
	}
	
	public void setDataIni(String dataIni){
		this.dataIni = dataIni;
		if (dataIni == null) dataIni = "00/00/0000";
	}
	public String getDataIni(){
		return dataIni;
	}
	
	public void setDataFim(String dataFim){
		this.dataFim = dataFim;
		if (dataFim == null) dataFim = "00/00/0000";
	}
	public String getDataFim(){
		return dataFim;
	}
	    
	public void setTxtMotivo(String motivoBloqueio){
		this.motivoBloqueio = motivoBloqueio;
		if (motivoBloqueio == null) motivoBloqueio = "";
	}
	public String getTxtMotivo(){
		return motivoBloqueio;
	}
			
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}


  //Desbloquear Senha
  public void desbloqueaSenha(String  codUsuario, UsuarioBean UsrLogado) throws TAB.DaoException, BeanException{
	  Dao dao = Dao.getInstance();
	  if (dao.desbloquearSenha(codUsuario, UsrLogado)== false)
		this.setMsgErro("Erro ao executar desbloqueio da senha.");
	  else this.setMsgErro("Senha desbloqueada com sucesso!");	
  }

  //Bloquear Senha
  public void bloqueaSenha(UsuarioBean ursBean,String motivo, String nomUserName) throws TAB.DaoException{
	  Dao dao = Dao.getInstance();
	  if (dao.bloquearSenha(ursBean, motivo, nomUserName)== false)   
		this.setMsgErro("Erro ao executar bloqueio da senha.");
	  else this.setMsgErro("Senha bloqueada com sucesso!");
  }
  
  //Consulta todos os logins contidos na tabela TCAU_BLOQUEIO
  public void consultaBloqueios(UsuarioBean usrBean) throws TAB.DaoException{
	  Dao dao = Dao.getInstance();
	  if (dao.consultarBloqueios(this,usrBean)== false)
		this.setMsgErro("Erro ao executar consulta dos dados do log.");

  }
  
}
