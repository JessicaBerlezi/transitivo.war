package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de OperacoesSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar OperacoesSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class OperacoesSistemaCommand extends sys.Command {

    private static final String jspPadrao="/ACSS/operacoesSistema.jsp";  
  private String next;

  public OperacoesSistemaCommand() {
    next             = jspPadrao ;
  }

  public OperacoesSistemaCommand(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		// verifica se o Bean do Usuario ja existe - se nao cria
		HttpSession session = req.getSession() ;	  

		// cria os Beans do Usuario, se n�o existir
		ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
		if (SistId==null)        SistId = new ACSS.SistemaBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   
	    String codSistema = req.getParameter("codSistema"); 
	    if(codSistema==null)       codSistema ="";  
		
	    String atualizarDependente = req.getParameter("atualizarDependente"); 
	    if(atualizarDependente==null)       atualizarDependente ="N";   		 
	    SistId.Le_Sistema(codSistema,0) ;				  					
		SistId.setAtualizarDependente(atualizarDependente);				
	    if ("buscaSistema".indexOf(acao)>=0) {
	      SistId.getFuncoes(20,5) ;	
		  SistId.setAtualizarDependente("S");	
	    }	 
		Vector vErro =new Vector(); 
	    if(acao.compareTo("A")==0){	
			String[] sigFuncao = req.getParameterValues("sFuncao");
			if(sigFuncao==null)  sigFuncao = new String[0];  
			String[] numOrdem = req.getParameterValues("numOrdem");
			if(numOrdem==null)  numOrdem = new String[0];         
			String[] sigFuncaoPai = req.getParameterValues("sigFuncaoPai");
			if(sigFuncaoPai==null)  sigFuncaoPai = new String[0];         
			String[] nomOperacao = req.getParameterValues("nomOperacao");
			if(nomOperacao==null)  nomOperacao = new String[0];         
			String[] nomDescricao = req.getParameterValues("nomDescricao");
			if(nomDescricao==null)  nomDescricao = new String[0];         
			String[] nomExecuta = req.getParameterValues("nomExecuta");
			if(nomExecuta==null)  nomExecuta = new String[0];         
			String[] nomApresentacao = req.getParameterValues("nomApresentacao");
			if(nomApresentacao==null)  nomApresentacao = new String[0];  
			String[] sitBiometria = req.getParameterValues("sitBiometria");
			if(sitBiometria==null)  sitBiometria = new String[0];                			       
			String[] codOperacao = req.getParameterValues("codOperacao");
			if(codOperacao==null)  codOperacao = new String[0];         
			String[] codOperacaoFk = req.getParameterValues("codOperacaoFk");
			if(codOperacaoFk==null)  codOperacaoFk = new String[0];         
			vErro      = isParametros(sigFuncao,numOrdem,sigFuncaoPai,nomOperacao,nomDescricao,nomExecuta,nomApresentacao,codOperacao,codOperacaoFk) ;
            if (vErro.size()==0) {
				Vector funcoes = new Vector(); 
				for (int i = 0; i<sigFuncao.length;i++) {
					OperacaoBean myOper = new OperacaoBean() ;		
					myOper.setNumOrdem(numOrdem[i]);
					myOper.setNomExecuta(nomExecuta[i]);
					myOper.setSigFuncao(sigFuncao[i]);
					myOper.setSigFuncaoPai(sigFuncaoPai[i]);			  
					myOper.setNomOperacao(nomOperacao[i]);
					myOper.setNomDescricao(nomDescricao[i]);	  
					myOper.setNomApresentacao(nomApresentacao[i]);
					myOper.setSitBiometria(sitBiometria[i].toUpperCase());			
				    myOper.setCodSistema(codSistema) ;	
				    myOper.setCodOperacao(codOperacao[i]) ;											
				    myOper.setCodOperacaoFk(codOperacaoFk[i]) ;																															
					funcoes.addElement(myOper) ; 					
				}
				SistId.setFuncoes(funcoes) ;
				SistId.isInsertFuncoes() ;
			    SistId.getFuncoes(20,5) ;						
			}
			else SistId.setMsgErro(vErro) ;  	      
	    }
		req.setAttribute("SistId",SistId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("OperacoesSistemaCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}

  private Vector isParametros(String[] sigFuncao,String[] numOrdem,String[] sigFuncaoPai,String[] nomOperacao,String[] nomDescricao,String[] nomExecuta,String[] nomApresentacao,String[] codOperacao,String[] codOperacaoFk) {
		Vector vErro = new Vector() ;
		if ((sigFuncao.length!=numOrdem.length) ||(sigFuncao.length!=sigFuncaoPai.length) || (sigFuncao.length!=numOrdem.length) || 
			(sigFuncao.length!=nomOperacao.length) || (sigFuncao.length!=nomDescricao.length)  ||
			(sigFuncao.length!=codOperacao.length) || (sigFuncao.length!=codOperacaoFk.length)  ||					
			(sigFuncao.length!=nomExecuta.length) || (sigFuncao.length!=nomApresentacao.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  }
 
}