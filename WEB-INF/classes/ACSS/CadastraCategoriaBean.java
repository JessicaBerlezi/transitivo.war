package ACSS;

import java.util.Vector;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class CadastraCategoriaBean extends sys.HtmlPopupBean 
{ 
	private String codCat;
	private String desCat;
	private String emailCat;

	private Vector vetcadcats;
	private String msgErro;
	

	public CadastraCategoriaBean()
	{
		super();
		setTabela("TCAU_CATEGORIA_DUVIDA");	
		setPopupWidth(35);
		codCat     = "";	  	
		desCat     = "";
		emailCat   = "";
		vetcadcats = new Vector();
		msgErro    ="";
	}
	//--------------------------------------------------------------------------
	public void setMsgErro( Vector vErro ) {
	  for (int j=0; j<vErro.size(); j++)
			 this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
		  this.msgErro = msgErro;
	}
    public String getMsgErro() { return this.msgErro; }
	//----------------------------------------------------------------------------
	public void setCodCategoria (String s )  {
		this.codCat = s;
		if( codCat == null ) this.codCat = "";
	}
	public String getCodCategoria()  { return this.codCat; }
	//----------------------------------------------------------------------------
	public void setDesCat(String s){
		this.desCat = s;
		if (desCat == null) this.desCat = "";
	}
	public String getDesCat()  { return this.desCat; }
	//----------------------------------------------------------------------------
	public void setEmailCat(String s)  {
		this.emailCat = s ;
		if (emailCat == null) this.emailCat ="" ;
	}  
	public String getEmailCat()  { return this.emailCat; }
	//----------------------------------------------------------------------------
	public boolean isValidCadCat(Vector vErro, Vector myCadCats) 
	{
	  if("0".equals(this.codCat) )
	     vErro.addElement("Categoria n�o selecionado. \n") ;
	  	
	  String sDesc = "";
	  String sDescAUX = "";
	  for(int i = 0 ; i<myCadCats.size(); i++) {
		  CadastraCategoriaBean myCadCat =(CadastraCategoriaBean)myCadCats.get(i);
		  sDesc = myCadCat.getDesCat();

		  if((sDesc.length()==0) ||("".equals(sDesc)))  continue;
	  		
		  // verificar duplicata 
		  for (int m = 0 ; m <myCadCats.size(); m++) {
			  CadastraCategoriaBean myCadCatOutra =(CadastraCategoriaBean)myCadCats.get(m);
			  sDescAUX = myCadCatOutra.getDesCat();
			  
			  if( sDescAUX.equals("") ) break; 
			  
			  if((m!=i) && (sDescAUX.trim().length()!=0) && (sDescAUX.equals(sDesc)) ) {
				  vErro.addElement("Categoria duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
			  }
		  }
	  }
	  setMsgErro(vErro);
	  return (vErro.size() == 0);  
	}
	//----------------------------------------------------------------------------
	public void isInsertCadCat(Vector vErro, Vector myCadCat ) {
	  if( isValidCadCat( vErro, myCadCat ) ) {
		  try {
			    Dao dao = Dao.getInstance();
			    if( dao.InsertCat( vErro, myCadCat ) )
					vErro.addElement("Opera��o efetuada com sucesso!\n");
				else
					vErro.addElement("Opera��o n�o foi efetuada sucesso!\n");							    
		  }
		  catch (Exception e) {
			     vErro.addElement("Categoria "+ desCat +" n�o atualizados:"+ " \n") ;
			     vErro.addElement("Erro na opera��o: "+ e.getMessage() +"\n");
		  }
	  }
	  else  
	  	  vErro.addElement("Erro na valida��o!\n");

  	  setMsgErro(vErro);
	}
   //------------------------------------------------------------------------------------------------------
   public void ListaCadCat()  {
   try {
vetcadcats.clear();
		Dao dao = Dao.getInstance();
		dao.ListaCadCat(this);
   }
   catch(Exception e){ }
   }
  //------------------------------------------------------------------------------------------------------
  public void setVetCadCats(Vector vecCadCats) { this.vetcadcats = vecCadCats; }
  public Vector getVetCadCats(){ return this.vetcadcats; } 
  public CadastraCategoriaBean getCadCat(int i) { return (CadastraCategoriaBean)this.vetcadcats.elementAt(i); }   
}