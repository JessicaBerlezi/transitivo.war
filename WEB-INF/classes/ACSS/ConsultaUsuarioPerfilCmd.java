package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - consulta Usuario do Perfil Cmd<br>
* <b>Description:</b>  Traz os usu�rios do sistema de acordo com o perfil<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/


public class ConsultaUsuarioPerfilCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ConsultaUsuarioPerfil.jsp";
	private String next;

	public ConsultaUsuarioPerfilCmd() {
		next = jspPadrao;
	}

	public ConsultaUsuarioPerfilCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = consultaUsrPerfil(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaUsuarioPerfilCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = jspPadrao;
		try {
			
			nextRetorno = consultaUsrPerfil(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaUsuarioPerfilCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

  public String consultaUsrPerfil(HttpServletRequest req, String nextRetorno) throws CommandException{
		try {
				// cria os Beans, se n�o existir
				HttpSession session = req.getSession();
				UsuarioBean UsrLogado = (UsuarioBean) session.getAttribute("UsuarioBeanId");
				if (UsrLogado == null)UsrLogado = new UsuarioBean();

				// cria os Beans de Usuario e Perfil, se n�o existir
				OperacaoBean OperacaoBeanId = (OperacaoBean) req.getAttribute("OperacaoBeanId");
				if (OperacaoBeanId == null) OperacaoBeanId = new OperacaoBean();

				UsuarioBean UsrBeanId =(UsuarioBean) req.getAttribute("UsrBeanId");
				if (UsrBeanId == null)UsrBeanId = new UsuarioBean();

				SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
				if (SistBeanId==null) SistBeanId = new SistemaBean() ;
			
				PerfilBean PerfBeanId = (PerfilBean)req.getAttribute("PerfBeanId") ;
				if (PerfBeanId==null) PerfBeanId = new PerfilBean() ;	  				

				ConsultaUsuarioPerfilBean ConsultaUsuarioPerfilBeanId =(ConsultaUsuarioPerfilBean) session.getAttribute("ConsultaUsuarioPerfilBeanId");
				if (ConsultaUsuarioPerfilBeanId == null)ConsultaUsuarioPerfilBeanId = new ConsultaUsuarioPerfilBean();


				// obtem e valida os parametros recebidos					
				String acao = req.getParameter("acao");
				if (acao == null) acao = " ";
			
				String codSistema = req.getParameter("codSistema"); 
				if(codSistema==null) codSistema =""; 
			
				String codPerfil = req.getParameter("codPerfil"); 		
				if(codPerfil==null) codPerfil =""; 
				
				SistBeanId.setCodSistema(codSistema) ;
				PerfBeanId.setCodPerfil(codPerfil);
	
				if (acao.equals("buscaUsuariosPerfil")) {
					SistBeanId.setAtualizarDependente("S");
					SistBeanId.Le_Sistema(codSistema,0) ;
					PerfBeanId.Le_Perfil(0);
					
					String tipoUsuario = req.getParameter("tipoUsuario");
					if (tipoUsuario == null) tipoUsuario = "";
					ConsultaUsuarioPerfilBeanId.setTipoUsuario(tipoUsuario);
					session.setAttribute("tipoUsuario", tipoUsuario);
					
					ConsultaUsuarioPerfilBeanId.Le_UsuarioDoPerfil(codSistema,codPerfil);
					if(tipoUsuario.equals("A"))
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("ATIVOS");
					else if (tipoUsuario.equals("I"))
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("INATIVOS");
					else 
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("ATIVOS /INATIVOS");
				}
			
				if (acao.equals("Classifica")) {
					SistBeanId.Le_Sistema(codSistema,0) ;
					PerfBeanId.Le_Perfil(0);
					
					String tipoUsuarioClass = req.getParameter("tipoUsuarioClass");
					if (tipoUsuarioClass == null) tipoUsuarioClass = "";
					ConsultaUsuarioPerfilBeanId.setTipoUsuario(tipoUsuarioClass);
					
					ConsultaUsuarioPerfilBeanId.Le_UsuarioDoPerfil(codSistema, codPerfil);	
					ConsultaUsuarioPerfilBeanId.Classifica(req.getParameter("ordem"));
					if(tipoUsuarioClass.equals("A"))
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("ATIVOS");
					else if (tipoUsuarioClass.equals("I"))
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("INATIVOS");
					else 
						ConsultaUsuarioPerfilBeanId.setTipoUsuario("ATIVOS /INATIVOS");
				
					SistBeanId.setAtualizarDependente("S");
			    }
			
				if (acao.equals("V")) {
					  SistBeanId.setAtualizarDependente("N");
					  SistBeanId.setCodSistema("") ;
				}
			
				if (acao.equals("ImprimeResultConsulta")) {
					  //Titulo do Relatorio de impressao
					  String tituloConsulta = "CONSULTA USU�RIOS POR PERFIL";
					  req.setAttribute("tituloConsulta", tituloConsulta);
			  	
					  ConsultaUsuarioPerfilBeanId.setDados(ConsultaUsuarioPerfilBeanId.getDados());
					  nextRetorno="/ACSS/ConsultaUsuarioPerfilImp.jsp";
				}
	
				req.setAttribute("OperacaoBeanId", OperacaoBeanId);
				req.setAttribute("SistBeanId", SistBeanId);
				req.setAttribute("UsrBeanId", UsrBeanId); 
				req.setAttribute("PerfBeanId", PerfBeanId);
			    req.setAttribute("ConsultaUsuarioPerfilBeanId",ConsultaUsuarioPerfilBeanId);
			}
			catch (Exception ue) {
				throw new sys.CommandException("ConsultaUsuarioPerfilCmd: "+ ue.getMessage());
			}
			return nextRetorno;
  }



}