package ACSS;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sys.CommandException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class CadastraNoticiaCmd extends sys.Command 
{
	private static final String jspPadrao = "/ACSS/CadastraNoticia.jsp";
	private String next;
	public CadastraNoticiaCmd() { next = jspPadrao; }
	public CadastraNoticiaCmd(String next) { this.next = next; }
	
 
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = cadastraNoticia(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("CadastraNoticiaCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
 
 
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{		String nextRetorno = jspPadrao;        
		try {
			nextRetorno = cadastraNoticia(req,nextRetorno);
		} 
		catch (Exception e) {	throw new sys.CommandException("CadastraNoticiaCmd: " + e.getMessage()); }		
		return nextRetorno;
	}
	
	public String cadastraNoticia(HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
	  UsuarioBean UsuarioBeanId = (UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
	  UsuarioFuncBean UsuarioFuncBeanId = (UsuarioFuncBean) req.getSession().getAttribute("UsuarioFuncBeanId");

			ACSS.NoticiaBean NoticiaBeanId = (ACSS.NoticiaBean) req.getAttribute("NotBeanId");
			if( NoticiaBeanId == null ) NoticiaBeanId = new ACSS.NoticiaBean();
		
			
	  String acao = req.getParameter("acao");
	  if( acao == null ) acao = "";
	  else acao = acao.trim();

			String datFimNot = sys.Util.formatedToday().substring(0,10); 
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.MONTH, -2);
			Date date = cal.getTime();
			cal.setTime(date);
			String dia = cal.get(Calendar.DAY_OF_MONTH) + "";
			String mes = cal.get(Calendar.MONTH)+ 1 + ""; 
			String ano = cal.get(Calendar.YEAR) + "";
			String datIniNot = dia +"/"+ mes +"/"+ ano;     

			NoticiaBeanId.setDatFimNoticia(datFimNot);
			NoticiaBeanId.setDatInicioNoticia(datIniNot);
            
	  if ( acao.equals("") ) {
				NoticiaBeanId.Publicada(UsuarioBeanId,1);
				
				nextRetorno = "/ACSS/CadastraNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if ( acao.equals("P") ) {
				NoticiaBeanId.Publicada(UsuarioBeanId,1);
				
				nextRetorno = "/ACSS/CadastraNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if ( acao.equals("PA") ) {
				datFimNot = sys.Util.formatedToday().substring(0,10); 
				cal = new GregorianCalendar();
				cal.setTime(new Date());
				cal.add(Calendar.MONTH, -5);
				date = cal.getTime();
				cal.setTime(date);
				dia = cal.get(Calendar.DAY_OF_MONTH) + "";
				mes = cal.get(Calendar.MONTH)+ 1 + ""; 
				ano = cal.get(Calendar.YEAR) + "";
				datIniNot = dia +"/"+ mes +"/"+ ano;     

				NoticiaBeanId.setDatFimNoticia(datFimNot);
				NoticiaBeanId.setDatInicioNoticia(datIniNot);
				
				NoticiaBeanId.Publicada(UsuarioBeanId,2);
				
				nextRetorno = "/ACSS/CadastraNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}

			else if ( acao.equals("PublicarNoticia") ) {
//				codNoticia so usado na alterar

				String dscNoticia    = req.getParameter("dscNoticia");
				if( dscNoticia == null ) dscNoticia ="";
				String datInicio     = req.getParameter("datInicio");
				String datFim        = req.getParameter("datFim");
				String indPrioridade = req.getParameter("indPriH");
				String textoNoticia  = req.getParameter("textoNoticia");
				if( textoNoticia == null ) textoNoticia ="";
								
//				NoticiaBean NoticiaBeanId = new NoticiaBean();	
				NoticiaBeanId.setDscNoticia( dscNoticia );

				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				Date data = df.parse(datInicio);
				NoticiaBeanId.setDatInicio( data );

				SimpleDateFormat dfs = new SimpleDateFormat("dd/MM/yyyy");
				Date datas = dfs.parse(datFim);
				NoticiaBeanId.setDatFim( datas );
				
				NoticiaBeanId.setIndPrioridade( indPrioridade );
				NoticiaBeanId.setTxtNoticia( textoNoticia );
				NoticiaBeanId.setIndPublicacao( "0" );

				NoticiaBeanId.CadastraNoticia(UsuarioBeanId);

				NoticiaBeanId.Publicada(UsuarioBeanId,1);
				
				nextRetorno = "/ACSS/CadastraNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if ( acao.equals("AlterarNoticia") ) {
				String codNoticia    = req.getParameter("codNoticia");
				if( codNoticia == null ) codNoticia ="0";
				NoticiaBeanId.setCodNoticia( Integer.parseInt(codNoticia) );

				String dscNoticia    = req.getParameter("dscNoticia");
				if( dscNoticia == null ) dscNoticia ="";
				String datInicio     = req.getParameter("datInicio");
				String datFim        = req.getParameter("datFim");
				String indPrioridade = req.getParameter("indPriH");
				String textoNoticia  = req.getParameter("textoNoticia");
				if( textoNoticia == null ) textoNoticia ="";
								

				NoticiaBeanId.setDscNoticia( dscNoticia );

				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					Date data = df.parse(datInicio);
					NoticiaBeanId.setDatInicio( data );
				SimpleDateFormat dfs = new SimpleDateFormat("dd/MM/yyyy");
					Date datas = dfs.parse(datFim);
					NoticiaBeanId.setDatFim( datas );
				
				NoticiaBeanId.setIndPrioridade( indPrioridade );
				NoticiaBeanId.setTxtNoticia( textoNoticia );
				NoticiaBeanId.setIndPublicacao( "0" );

				NoticiaBeanId.AlteraNoticia(UsuarioBeanId);

				NoticiaBeanId.Publicada(UsuarioBeanId,1);
				
				nextRetorno = "/ACSS/CadastraNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}

		} catch (Exception e) {	throw new sys.CommandException("CadastraNoticiaCmd: " + e.getMessage()); }		
		return nextRetorno;
	}
}