package ACSS;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Troca de Senha<br>
* <b>Description:</b>  Troca de Senha<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class trocaSenhaCommand extends sys.Command {

  private static final String jspPadrao="/ACSS/trocaSenha.jsp" ;  
  private String next;

  public trocaSenhaCommand() {
    next             =  jspPadrao;
  }

  public trocaSenhaCommand(String next) {
	this() ;
    this.next = next;
  }
  public void setNext(String next){ 
  	  this.next= next ;
    return; 
  }
  
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno = jspPadrao;
	try {
		nextRetorno = executaTrocaSenha(req,nextRetorno);
		
	}
	catch (Exception ue) {
		throw new sys.CommandException("ConsultaUsuarioLogadoCmd 001: "+ ue.getMessage());
	}
	return nextRetorno;
}
  
  public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	{
			String nextRetorno = jspPadrao;
			try {
				nextRetorno = executaTrocaSenha(req,nextRetorno);
			}
			catch (Exception ue) {
				throw new sys.CommandException("ConsultaUsuarioLogadoCmd: "+ ue.getMessage());
			}
			return nextRetorno;
}
  
  
  public String executaTrocaSenha(HttpServletRequest req,String nextRetorno) throws CommandException{
	
    try {      	
		// verifica se o Bean do Usuario ja existe - se nao cria

		HttpSession session = req.getSession() ;	  

		// cria os Beans do Usuario, se n�o existir
		ACSS.UsuarioBean UsrBeanId = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
		if (UsrBeanId==null)        UsrBeanId = new ACSS.UsuarioBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   
		String pkid           = req.getParameter("pkid");   
		if(pkid==null)       pkid  ="0";  		  	  	    	  
		String codUsuario    = req.getParameter("codUsuario");
		if(codUsuario==null)   codUsuario="0";  		  	  	    		 	
		String codSenha       = req.getParameter("codSenha");
		if(codSenha==null)      codSenha="";  
		String novaSenha       = req.getParameter("novaSenha");
		if(novaSenha==null)      novaSenha="";  
		String digSenha       = req.getParameter("digSenha");
		if(digSenha==null)      digSenha="";  
  	   	if ("alterar".equals(acao)) {		
			//		Setar o(s) objetos para executar a acao requerida
        	UsrBeanId.setCodUsuario(pkid) ;		
        	UsrBeanId.setMsgErro("") ;									
        	UsrBeanId.Le_Usuario(0) ;
			Vector     vErro      = new Vector();
			boolean bOk = false ;
			if 	(UsrBeanId.getMsgErro().length()==0)	bOk=UsrBeanId.isTrocaSenha(vErro,novaSenha,digSenha,codSenha) ;			
		 }
		 req.setAttribute("UsrBeanId",UsrBeanId) ;
    }
    catch (Exception ue) {
      throw new sys.CommandException("UsuarioCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}

}