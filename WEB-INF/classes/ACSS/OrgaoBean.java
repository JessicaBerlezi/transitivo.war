package ACSS;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
* <b>Title:</b>        	SMIT - Bean de Orgaos<br>
* <b>Description:</b>  	Bean dados dos Orgaos - Tabela de Orgaos<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Luiz Medronho
* @version 				1.0
* @Updates
*/

public class OrgaoBean extends sys.HtmlPopupBean { 

	private String codOrgao;
	private String nomOrgao;
	private String sigOrgao;
	private String datCadastro;
	private String nomContato;
	private String integrada;
	private String vex;
	private sys.EnderecoBean endereco;
	private sys.TelefoneBean telefone;
	private String codGrupo;

	private String atualizarDependente;
	private Vector parametros;  
	private Vector junta;   
	private Vector motivo; 
		
	private String popupMulti;
	private Connection conn;
	private String txtEmail;
	private List autos;
	private String indAutua;
	private String indSmitDol;
	private ArrayList listOrgaos;
		
	public OrgaoBean()   throws sys.BeanException {

	  	super();
	  	setTabela("TSMI_ORGAO");	
	  	setPopupWidth(29);
	  	codOrgao	= "";	
	  	sigOrgao	= "";
		nomOrgao	= "";
		datCadastro = sys.Util.formatedToday().substring(0,10);
		nomContato  = " ";
		integrada  = "";
		vex  = "";
		txtEmail = "";
		atualizarDependente="N";		
		endereco    = new sys.EnderecoBean() ;
		telefone    = new sys.TelefoneBean() ;
		codGrupo    = "";
		parametros  = new Vector() ;
		junta		= new Vector() ;
		motivo      = new Vector();
		autos       = new ArrayList() ;		
		indAutua    = "";
		indSmitDol  = "";
		listOrgaos  = new ArrayList();
	}
	
	
	public void setListOrgaos(ArrayList listOrgaos) {
		this.listOrgaos = listOrgaos;
	}
	
	public ArrayList getListOrgaos() {
		return listOrgaos;
	}
	
	public void setAutos(List autos)  {
		this.autos=autos ;	
	}
	
	public List getAutos()  {
		return this.autos;
	}

	public void setCodOrgao(String codOrgao)  {
		this.codOrgao=codOrgao ;
		if (codOrgao==null) this.codOrgao="" ;
	}  
	public String getCodOrgao()  {
		return this.codOrgao;
	}

	public void setNomOrgao(String nomOrgao)  {
		this.nomOrgao = nomOrgao;
		if (nomOrgao==null) this.nomOrgao="" ;	
	}  
	public String getNomOrgao()  {
		return this.nomOrgao;
	}
	
	public void setNomContato(String nomContato)  {
		this.nomContato = nomContato;
		if (nomContato==null) this.nomContato="" ;	
	}  
	public String getNomContato()  {
		return this.nomContato;
	}
	
	public void setCodGrupo(String codGrupo)  {
		this.codGrupo = codGrupo;
	if (codGrupo==null) this.codGrupo="" ;	
	}  
	public String getCodGrupo()  {
		return this.codGrupo;
	}

	//--------------------------------------------------------------------------
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
    }
	public String getAtualizarDependente() {	return this.atualizarDependente; }
	
	public void setEndereco(sys.EnderecoBean endereco)  {
		this.endereco = endereco;
		if (endereco==null) this.endereco= new sys.EnderecoBean() ;	
	}  
	public sys.EnderecoBean getEndereco()  {
		return this.endereco;
	}
	
	public void setTelefone(sys.TelefoneBean telefone)  {
		this.telefone = telefone;
		if (telefone==null) this.telefone= new sys.TelefoneBean() ;	
	}  
	public sys.TelefoneBean getTelefone()  {
		return this.telefone;
	}
	
	public void setSigOrgao(String sigOrgao)  {
		this.sigOrgao = sigOrgao;
		if (sigOrgao==null) this.sigOrgao="" ;	
	}  
	public String getSigOrgao()  {
		return this.sigOrgao;
	}
	public void setDatCadastro(String datCadastro)  {
		if (datCadastro==null) datCadastro=" " ;
		if (datCadastro.length() < 10) datCadastro = (datCadastro+"          ").substring(0,10) ;
		this.datCadastro = datCadastro;
	}
	public String getDatCadastro()  {
		return this.datCadastro;
	}
	
	public void setIntegrada(String integrada)  {
		this.integrada = integrada;
		if (integrada==null) this.integrada="" ;	
	}  
	public String getIntegrada()  {
		return this.integrada;
	}
	
	public void setVex(String vex)  {
		this.vex = vex;
		if (vex==null) this.vex="" ;	
	}  
	public String getVex()  {
		return vex;
	}
	
	public void setConn(Connection conn)  {
		this.conn = conn;
	}  
	public Connection getConn()  {
		return this.conn;
	}


	public void buscaCodOrgao(String codusu, String Acao,ACSS.UsuarioBean myUsrLog)  {
		int pos = "Top,Proximo,Anterior,Fim,Pkid".indexOf(Acao) ;
		if (pos < 0) return ;
		try {
			if (pos==25) this.codOrgao = codusu ;
			else this.nomOrgao = codusu;
			Dao dao = Dao.getInstance();
			if (dao.buscaOrgao(this,pos,myUsrLog)==false) {
				this.codOrgao = "" ;
				setPkid("0") ;
	        }
		} // fim do try
	    catch (Exception e) {
			setMsgErro("Erro na localiza��o do �rg�o"+e.getMessage()) ;
     		this.codOrgao = "" ;
	    	setPkid("0") ;
    	}	// fim do catch
	}
public boolean isValid(Vector vErro,int acao) {
	boolean bOk = true ;
	if (this.nomOrgao.length()==0)  vErro.addElement("Nome do �rg�o n�o preenchido. \n") ;
	if (this.sigOrgao.length()==0)  vErro.addElement("Sigla do �rg�o n�o preenchida. \n") ;
	return (vErro.size()==0) ;
  }
  /**
   * M�todo para inserir o �rg�o na base de dados
   * Valida primeiro se os dados est�o preenchidos.
   */
  public boolean isInsert() {
  	boolean bOk = true ;
  	Vector vErro = new Vector() ;
  	if (isValid(vErro,1))   	{
  		try {
  			Dao dao = Dao.getInstance();
  			if (dao.OrgaoExiste(this,1)) {
  				vErro.addElement("Org�o j� cadastrado: "+this.sigOrgao+" \n") ;
  				bOk = false ;
  			}
  			else {
  				if (dao.OrgaoInsert(this,vErro))
  					vErro.addElement("Org�o incluido: " +this.sigOrgao+" \n") ;
  				else 					   
  					vErro.addElement("Org�o n�o incluido: " +this.sigOrgao+" \n") ;
  			}
  	   	}
  	    catch (Exception e) {
  	        vErro.addElement("Org�o n�o incluido: " +this.sigOrgao+" \n") ;
  	        vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
   			bOk = false ;
  	    }	// fim do catch
  	}
     setMsgErro(vErro) ;
      return bOk;  
  }
  /**
   * M�todo para atualizar os dados do �rg�o na base de dados
   * Valida primeiro se os dados est�o preenchidos e se o �rg�o existe.
   */
  
  /**
   * M�todo para remover o �rg�o na base de dados
   * Valida se o �rg�o existe.
   */
public boolean isAltera() {
  boolean bOk = true ;
  Vector vErro = new Vector() ;
  if (isValid(vErro,2))   	{
  	try 	{
  		Dao dao = Dao.getInstance();
  		if (dao.OrgaoExiste(this,0)==false) {
  			vErro.addElement("�rg�o n�o cadastrado com "+this.sigOrgao+" \n") ;
  			bOk = false ;
  		}
  		else {
  			if (dao.OrgaoExisteOutro(this)) {
  				vErro.addElement("Existe outro �rg�o cadastrado com: "+this.sigOrgao+" \n") ;
  				bOk = false ;
  			}
  			else {
  				if (dao.OrgaoUpdate(this,vErro))
  					vErro.addElement("Org�o alterado: " +this.sigOrgao+" \n") ;
  				else vErro.addElement("Org�o n�o alterado: " +this.sigOrgao+" \n") ;
  	  	    }
  		}
  	}
      catch (Exception e) {
        vErro.addElement("Org�o n�o alterado: " +this.sigOrgao+" \n") ;
        vErro.addElement("Erro na altera��o: "+e.getMessage()+" \n");
    	  bOk = false ;
      }// fim do catch
  }
  setMsgErro(vErro) ;
  return bOk;
}
public boolean isDelete() {
  boolean bOk = true ;
  Vector vErro = new Vector() ;
  try {
  	Dao dao = Dao.getInstance();
  	if (dao.OrgaoExiste(this,0)==false) {
  		vErro.addElement("Org�o n�o cadastrado com "+this.sigOrgao+" \n") ;
  		bOk = false ;
  	}
  	else {
  		if (dao.OrgaoDelete(this,vErro))
  			vErro.addElement("Org�o excluido: " +this.sigOrgao+" \n") ;
  		else  
  			vErro.addElement("Org�o n�o excluido: " +this.sigOrgao+" \n") ;
  	 }
  }
  catch (Exception e) {
  	vErro.addElement("Org�o n�o excluido: " +this.sigOrgao+" \n") ;
  	vErro.addElement("Erro na exclus�o: "+e.getMessage()+" \n");
  	bOk = false ;
  }	// fim do catch
  setMsgErro(vErro) ;
  return bOk;
}


//--------------------------------------------------------------------------
public String getOrgaos(UsuarioBean myUsr){
    String myOrgaos = "" ; 
    try    {
       Dao dao = Dao.getInstance();
	   myOrgaos = dao.getOrgaos(myUsr,"") ;
	}
	catch(Exception e){	}	
	return myOrgaos ;
}

public String getOrgaosAtuacao(UsuarioBean myUsr,ACSS.ParamSistemaBean myParamSistema){
    String myOrgaos = "" ; 
    try    {
       Dao dao = Dao.getInstance();
	   myOrgaos = dao.getOrgaosAtuacao(myUsr,myParamSistema) ;
	}
	catch(Exception e){	}	
	return myOrgaos ;
}



public String getOrgaosACSS(UsuarioBean myUsr){
    String myOrgaos = "" ; 
    try    {
       Dao dao = Dao.getInstance();
	   myOrgaos = dao.getOrgaosACSS(myUsr,"") ;
	}
	catch(Exception e){	}	
	return myOrgaos ;
}


//--------------------------------------------------------------------------
public void Le_Orgao(String tpOrg, int pos)  {
     try { 
	       Dao dao = Dao.getInstance();
 			if (pos==0) this.codOrgao=tpOrg;		 	
			else 	    this.nomOrgao=tpOrg;	
    	   if ( dao.OrgaoLeBean(this, pos) == false) {
			    this.codOrgao	  = "";
				setPkid("0") ;		
           }
     }// fim do try
     catch (Exception e) {
          	this.codOrgao = "" ;
			setPkid("0") ;		        			
            System.err.println("Orgao001: " + e.getMessage());
     }
  }



//--------------------------------------------------------------------------
public void setParametros(Vector parametros)  {
   	this.parametros = parametros ;
   }

public Vector getParametros(int min,int livre)  {
      try  {
		REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();
		daoTabela.OrgaoGetParam(this);
	   	if (this.parametros.size()<min-livre) livre = min-parametros.size() ;		  
   		for (int i=0;i<livre; i++) 	{
   			this.parametros.addElement(new REC.ParamOrgBean()) ;
	   	}
      }
      catch (Exception e) {
           setMsgErro("Parametro007 - Leitura: " + e.getMessage());
      }//fim do catch
    return this.parametros ;
   }

public Vector getParametros()  {
      return this.parametros;
   }

public REC.ParamOrgBean getParametros(int i)  { return (REC.ParamOrgBean)this.parametros.elementAt(i); }
 

public boolean isValidParametros(Vector vErro) {
	   if ("0".equals(this.codOrgao)) vErro.addElement("Parametro n�o selecionado. \n") ;
	   String sDesc   = ""  ;
	   for (int i = 0 ; i<this.parametros.size(); i++) {
		   	REC.ParamOrgBean myParam = (REC.ParamOrgBean)this.parametros.elementAt(i) ;
		    sDesc = myParam.getNomDescricao();
		   	if ((sDesc.length()==0) ||("".equals(sDesc))) continue ;
		   	// verificar duplicata 
		   	for (int m = 0 ; m<this.parametros.size(); m++) {
		   		REC.ParamOrgBean myParamOutra = (REC.ParamOrgBean)this.parametros.elementAt(m) ;		
		   	   if ((m!=i) && (myParamOutra.getNomDescricao().trim().length()!=0) && (myParamOutra.getNomDescricao().equals(sDesc))) {
		   		 vErro.addElement("Parametro em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
		   	   }
		   	}
	   }
	   return (vErro.size()==0) ;  
   }
 
public boolean isInsertParametros()   {
	boolean retorno = true;
	Vector vErro = new Vector() ;
    // Verificada a valida��o dos dados nos campos
    if( isValidParametros(vErro) )   {
       try  {
            REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
          if (daoTabela.OrgaoInsertParam(this,vErro))    {					   
               vErro.addElement("Parametros do �rg�o "+this.sigOrgao+" atualizados:"+ " \n");
   	   }
   		   else    {			   		   
   		      vErro.addElement("Parametros do �rg�o "+this.sigOrgao+" n�o atualizados:"+ " \n");     	   
   		   }
       }
       catch (Exception e) {
            vErro.addElement("Parametros do �rg�o "+this.sigOrgao+" n�o atualizados:"+ " \n") ;
            vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
        	   retorno = false;
       }//fim do catch
    }
	else retorno = false;   
    setMsgErro(vErro);
    return retorno;
  }
  /**-------------------------------------------------------------------
  /*  C�digo Relativo a Junta (em REC)
  /*--------------------------------------------------------------------
  **/
  
  	public Vector getJunta(int min,int livre)  {
       try  {
  
			REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();
			daoTabela.OrgaoGetJunta(this);
			 	if (this.junta.size()<min-livre) 
				livre = min-junta.size() ;	  	
					for (int i=0;i<livre; i++) 	{
						this.junta.addElement(new REC.JuntaBean()) ;
			 	}
       }
       catch (Exception e) {
            setMsgErro("Orgao007 - Leitura: " + e.getMessage());
       }//fim do catch
    return this.junta ;
    	}
  
  public Vector getJunta()  {
       return this.junta;
  	}   
  
  public REC.JuntaBean getJunta(int i)  { 
    return (REC.JuntaBean)this.junta.elementAt(i);
  }
 
  public void setJunta(Vector junta)  {
    		this.junta = junta ;
  }  
 
  public boolean isInsertJunta()   {
  	boolean retorno = true;
  	Vector vErro = new Vector() ;
     	// Verificada a valida��o dos dados nos campos
     	if( isValidJunta(vErro) )   {
        		try  {
             	REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
          		if (daoTabela.OrgaoInsertJunta(this,vErro))    {	
                		vErro.addElement("Junta do Org�o "
                		     +this.sigOrgao+" atualizados:"+ " \n");
    	   			}
    		        else{		
    		     		 vErro.addElement("Junta do Org�o "
    		     		     +this.sigOrgao+" n�o atualizados:"+ " \n");     	   
    		   		}
        		}
        catch (Exception e) {
             vErro.addElement("Junta do Org�o "+this.sigOrgao+" n�o atualizados:"+ " \n") ;
             vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
             retorno = false;
        }//fim do catch
     }
  else retorno = false;   
     setMsgErro(vErro);
     return retorno;
   }
   
   public boolean isValidJunta(Vector vErro) {
  if("0".equals(this.codOrgao)) 
  	vErro.addElement("Orgao n�o selecionado. \n") ;
  	
     String sDesc = "";
     for (int i = 0 ; i< this.junta.size(); i++) {
        REC.JuntaBean myJunta = (REC.JuntaBean)this.junta.elementAt(i) ;
     sDesc = myJunta.getNomJunta();
     if((sDesc.length()==0) ||("".equals(sDesc)))
       continue ;
  		  
       // verificar duplicata 
    for (int m = 0 ; m <this.junta.size(); m++) {
     	 REC.JuntaBean myJuntaOutra = (REC.JuntaBean)this.junta.elementAt(m) ;		
  	 if ((m!=i) && (myJuntaOutra.getNomJunta().trim().length()!=0) && (myJuntaOutra.getNomJunta().equals(sDesc))) {
  	   	 vErro.addElement("Junta em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
  	 }
    }
     }
     return (vErro.size() == 0) ;  
   }

   /** Atualiza o codigo html do popup.
   *   @param opcoes (String).  
   *   <p>Obs.: formato do parametro:
   *   <br>String contendo nome da coluna e a clausula where, separados por virgula.
   *   <br>Ex.: "Doc_Descricao,SELECT pro_doc.Doc_Pk_Id,pro_doc.Doc_Descricao FROM pro_dsass INNER JOIN pro_doc ON pro_dsass.Dsass_Fk_Doc = pro_doc.Doc_Pk_Id WHERE (pro_dsass.Dsass_Fk_Sass = 2)".
   */
/*
   public void setPopupMulti(String opcoes) throws SQLException {
   	String aux="";
     for(int f=0;f<getPopupWidth();f++)
     	aux=aux+"&nbsp;";
	    popupMulti="<select style='border-style: outset;' name='"+getPopupNome()+"' size='"+getPopupSize()+"' "+getPopupExt()+"><OPTION VALUE='0'>"+aux+"</OPTION></SELECT>";
     try{
     String nome_coluna, sqlCmd;	  
     int loc_virg = opcoes.indexOf(",");
     if (loc_virg==-1){
       nome_coluna=opcoes;
   	    sqlCmd = "select "+getColunaValue()+","+nome_coluna+" from "+getTabela();
     }
     else{
     	nome_coluna = opcoes.substring(0,loc_virg);
   		sqlCmd      = opcoes.substring(loc_virg+1,opcoes.length());
     }
     String valor_pk="0";
     String valor_coluna;
     StringBuffer popupBuffer = new StringBuffer();
     Connection connTabela =null ;					
     try {
     	connTabela = getServiceloc().getConnection(getJ_abrevSist()) ;
     }
     catch (Exception ex) {
     	throw new sys.ServiceLocatorException(ex.getMessage());
     }
       Statement stmt = connTabela.createStatement();
       ResultSet rs   = stmt.executeQuery(sqlCmd);
       popupBuffer.append("<select style='border-style: outset;' name='"+getPopupNome()+"' size='"+getPopupSize()+"' "+getPopupExt()+"> \n");	  
//       popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");	    			  
     boolean not_eof=rs.next();
     int conta=0;
	 aux = "" ;
     String seleciona="";
     if(not_eof){
   	    while(not_eof){
	   	  aux = "000000"+rs.getString(getColunaValue()) ;
   		  aux = aux.substring(aux.length()-6,aux.length());
	   	  seleciona    = (getChecked().indexOf(aux)>=0) ? "selected" : "";
       	  popupBuffer.append("<OPTION VALUE='"+rs.getString(getColunaValue())+"' "+seleciona+" >"+rs.getString(nome_coluna).trim()+"</OPTION> \n");
          not_eof=rs.next();		
       }
     }	  	  
     popupBuffer.append("</SELECT> \n");	  
     rs.close();
     stmt.close();
     getServiceloc().setReleaseConnection(connTabela);
     popupMulti=popupBuffer.toString();
     }catch (Exception e){
     System.err.println("OrgaoBean 01 - m�todo setPopupString:"+e.getMessage());
     }
   }
   public String getPopupMulti()  {
   	return this.popupMulti ;
   }
*/

   public void verificaOrgao(Connection conn) throws sys.BeanException {
	   Vector vErro = new Vector();
	   try {
		   this.setConn(conn);
		   Dao dao = Dao.getInstance();
		   if (dao.OrgaoExiste(this,0) == false) {
			   isInsert();
		   }
		   else {
			   isAltera();
		   }
	   }
	   catch (Exception e) {
		   throw new sys.BeanException("Erro ao efetuar verifica��o: " + e.getMessage());
	   }
	   setMsgErro(vErro);
   }


   public int contador(int cont) {
	   cont += 1;
	   return cont;
   }



	/**-------------------------------------------------------------------
	 C�digo Relativo a Junta Broker
	 --------------------------------------------------------------------**/
    
	   public Vector getJuntaBroker(int min,int livre)  {
		try  { 
		   Vector vErro = new Vector();
		   REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();
//		   if( !daoTabela.OrgaoGetJuntaBroker(this) ){
//			   setMsgErro("Orgao009 - Erro de Leitura: ");
//			   setMsgErro(vErro);
//		   }
			   if (this.junta.size()<min-livre) 
			   livre = min-junta.size() ;	  	
				   for (int i=0;i<livre; i++) 	{
					   this.junta.addElement(new REC.JuntaBean()) ;
			   }
				   setMsgErro(vErro);
		}
		catch (Exception e) {
			 setMsgErro("Orgao007 - Leitura: " + e.getMessage());
		}//fim do catch
	 return this.junta ;
	   }

 
	public String getTxtEmail() {
		return txtEmail;
	}
	public void setTxtEmail(String txtEmail) {
		if (txtEmail == null) txtEmail = "";
		else this.txtEmail = txtEmail;
	}


	public void setIndAutua(String indAutua) {
		if(indAutua == null)
			this.indAutua = "";
		else
		    this.indAutua = indAutua;
	}

	public String getIndAutua() {
		return indAutua;
	}

	public void setIndSmitDol(String indSmitDol) {
		if(indSmitDol == null)
			this.indSmitDol = "";
		else
		     this.indSmitDol = indSmitDol;
	}
	
	public String getIndSmitDol() {
		return indSmitDol;
	}




  



}


