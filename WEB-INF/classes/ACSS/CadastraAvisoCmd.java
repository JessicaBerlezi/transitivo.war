package ACSS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sys.CommandException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class CadastraAvisoCmd extends sys.Command 
{
	private static final String jspPadrao = "/ACSS/CadastraAviso.jsp";
	private String next;
	public CadastraAvisoCmd() { next = jspPadrao; }
	public CadastraAvisoCmd(String next) { this.next = next; }

    public void carregaBean( AvisoBean AvisoBeanId, HttpServletRequest req )
    {
    	
    	String dscAviso    = req.getParameter("dscAviso");
		if( dscAviso == null ) dscAviso ="";
		AvisoBeanId.setDscAviso(dscAviso);

		String textoAviso  = req.getParameter("textoAviso");
		if( textoAviso == null ) textoAviso ="";
		AvisoBeanId.setTxtAviso(textoAviso);

		try{
			String datInicio    = sys.Util.formatedToday().substring(0,10);
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			Date data = df.parse(datInicio);
			AvisoBeanId.setDatEnvio(data);
		}
		catch (Exception e) {}

		String indPrioridade = req.getParameter("indPriH");
		AvisoBeanId.setIndPrioridade(indPrioridade);

    }

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = jspPadrao;        
		try {
			nextRetorno = cadastraAviso(req,nextRetorno);           
		} catch (Exception e) {	throw new sys.CommandException("CadastraAvisoCmd: " + e.getMessage()); }		

		return nextRetorno;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = cadastraAviso(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("CadastraAvisoCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
	
	public String cadastraAviso(HttpServletRequest req, String nextRetorno) throws CommandException{
		  try {
		       UsuarioBean UsuarioBeanId = (UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
				   UsuarioFuncBean UsuarioFuncBeanId = (UsuarioFuncBean) req.getSession().getAttribute("UsuarioFuncBeanId");
			
				   AvisoBean AvisoBeanId = (AvisoBean) req.getAttribute("AvisoBeanId");
				   if( AvisoBeanId == null ) AvisoBeanId = new AvisoBean();
			    
				   int id=0;
				   String acao = req.getParameter("acao");
				   if( acao == null ) acao = "";
           
				   String ORGAO = req.getParameter("ORGAO");
				   if( ORGAO == null ) ORGAO = "";
				   AvisoBeanId.setOrgao(ORGAO);

				   String PERFIL = req.getParameter("PERFIL");
				   if( PERFIL == null ) PERFIL = "";
				   AvisoBeanId.setSistemPerfil(PERFIL);
           
				   if ( acao.equals("") ) 
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   else if(acao.equals("SISTEMA"))	{
					   carregaBean(AvisoBeanId,req);
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,4);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("ORGAO")) {
					   carregaBean(AvisoBeanId,req);
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,3);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("PERFIL")) {
					   carregaBean(AvisoBeanId,req);
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,2);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("OLU")) {
			
					   carregaBean(AvisoBeanId,req);
								
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,1);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("orgaoUsuario")) {

					   carregaBean(AvisoBeanId,req);
				
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,1);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("perfilSistemaUsuario")) {
					   carregaBean(AvisoBeanId,req);
					   AvisoBeanId.PegaOrgaoUsuario(UsuarioBeanId,2);

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);
				   }
				   else if(acao.equals("C")) {
					   Vector vErro = new Vector();

					   ORGAO = req.getParameter("ORGAO");
						   if( ORGAO == null ) ORGAO = "";
						   AvisoBeanId.setOrgao(ORGAO);
					
					   PERFIL = req.getParameter("PERFIL");
						   if( PERFIL == null ) PERFIL = "";
						   AvisoBeanId.setSistemPerfil(PERFIL);

					   String[] codGenerico   = req.getParameterValues("variosH");
						   if( codGenerico == null)  codGenerico = new String[0];
	            
					   if(codGenerico.length > 0 )  {	            
						   Vector vetGenerico = new Vector(); 
						   for (int i = 0; i < codGenerico.length;i++) {
							   if(codGenerico[i].equals("") ) break;	
							   AvisoBean myBean = new AvisoBean();	
							   myBean.setCodGenerico( codGenerico[i] );
							   vetGenerico.add( myBean );
						   }
					   AvisoBeanId.setVetItens( vetGenerico );	
					   }
					   carregaBean(AvisoBeanId, req);

					   String tipoH = req.getParameter("tipoH");
					   if( (tipoH == null)|| (tipoH.equals("")) ) tipoH = "0";
					   else id = Integer.parseInt(tipoH);

					   if(codGenerico.length > 0 )
						   if( AvisoBeanId.isInsertAviso(UsuarioBeanId,vErro,id ) )
							   vErro.addElement("Aviso cadastrado com Sucesso!\n");
						   else    
							   vErro.addElement("Aviso n�o Alterado!\n");
					   else
					   vErro.addElement("N�o foi poss�vel cadastrar pois n�o foi selecionado d!\n");
				
					   AvisoBeanId.setMsgErro( vErro );

					   nextRetorno = "/ACSS/CadastraAviso.jsp";
					   req.setAttribute("AvisoBeanId", AvisoBeanId);

				   } 
      }catch (Exception e) {	
      	throw new sys.CommandException("CadastraAvisoCmd: " + e.getMessage()); 
      }		
	    return nextRetorno;
	}
}