package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class MensagemSistemaCommand extends sys.Command
{
  private static final String jspPadrao="/ACSS/MensagemSistema.jsp";
  private String next;

  public MensagemSistemaCommand() { 
     next = jspPadrao; }
  public MensagemSistemaCommand(String next) { 
     this.next = jspPadrao; }

  public String execute(HttpServletRequest req) throws sys.CommandException {
     String nextRetorno = jspPadrao;
     try {
          // cria os Beans , se n�o existir
          ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
      	  if( SistId == null ) SistId = new ACSS.SistemaBean();

       	  // obtem e valida os parametros recebidos
          String acao = req.getParameter("acao");
          if( acao == null ) acao =" ";
		  
          String codSistema = req.getParameter("codSistema");
          SistId.setCodSistema(codSistema);
          String atualizarDependente = req.getParameter("atualizarDependente"); 
          if(atualizarDependente==null)       atualizarDependente ="N";   		 
          SistId.Le_Sistema(codSistema,0) ;				  					
          SistId.setAtualizarDependente(atualizarDependente);				
          if( "buscaSistema".indexOf(acao) >= 0 ) {
               SistId.getMensagens(20,5);
               SistId.setAtualizarDependente("S");	
          }
          Vector vErro = new Vector();
          if( acao.compareTo("A") == 0) {
              String[] codMensagemSistema = req.getParameterValues("codMensagemSistema");
              if( codMensagemSistema == null ) codMensagemSistema = new String[0];
			  String[] codMensagemInterno = req.getParameterValues("codMensagemInterno");
              if( codMensagemInterno == null ) codMensagemInterno = new String[0];
			  String[] dscMensagem = req.getParameterValues("dscMensagem");
              if( dscMensagem == null )  dscMensagem = new String[0];
			  String[] dscMotivo = req.getParameterValues("dscMotivo");
              if ( dscMotivo == null )  dscMotivo = new String[0];
              String[] dscAcao = req.getParameterValues("dscAcao");
              if( dscAcao == null )  dscAcao = new String[0];
			  String[] codTipMensagem = req.getParameterValues("codTipMensagem");
              if( codTipMensagem==null)  codTipMensagem = new String[0];
			  vErro = isParametros(codMensagemInterno, dscMensagem, codTipMensagem);
              if (vErro.size()==0) {
				  Vector mensagens = new Vector();
                  for (int i = 0; i < codMensagemSistema.length; i++)   {
                   	   MensagemSistemaBean myMsg = new MensagemSistemaBean();
                       myMsg.setCodMensagemSistema( codMensagemSistema[i] );
					   myMsg.setCodMensagemInterno( codMensagemInterno[i] );
                       myMsg.setDscMensagem( dscMensagem[i] );
					   myMsg.setCodSistema( codSistema );
    			       myMsg.setDscMotivo( dscMotivo[i] );
					   myMsg.setDscAcao( dscAcao[i] );					   
  					   myMsg.setCodTipMensagem( codTipMensagem[i] );
			           mensagens.addElement(myMsg);
    			  }

                  //criar no Bean de sistema esses metodos
				  SistId.setMensagens( mensagens );
				  SistId.isInsertMensagemSistema();				  
			   }
			    else SistId.setMsgErro(vErro);
	          }
        	  req.setAttribute("SistId", SistId);
     }
     catch (Exception ue) {
            throw new sys.CommandException("MensagemSistemaCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
  private Vector isParametros(String[] codMensagemInterno, String[] dscMensagem, String[] codTipMensagem) 
  {
	Vector vErro = new Vector();
    if( (dscMensagem.length != codMensagemInterno.length) || (codTipMensagem.length != codMensagemInterno.length) ) 
	    vErro.addElement("Parametros inconsistentes");
	return vErro;
   }
}