package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - consulta Funcao do Perfil Bean<br>
* <b>Description:</b>  Traz as fun��es do sistema de acordo com o perfil<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ConsultaFuncoesPerfilBean extends PerfilBean {

	
	private List dados;
	private String nomOperacao; 
	private String sigFuncao;
	private String nomExecuta; 
	private String codOperacaoFk;
	private String codOperacao;
	private String codPerfil;
	
  //lista de ordenacao
  private String ordClass ;
  private String ordem ;


	public ConsultaFuncoesPerfilBean() throws sys.BeanException {
		dados = new ArrayList();
		nomOperacao = "";
		sigFuncao = "";
		nomExecuta = "";
		codOperacaoFk = "";
		codOperacao = "";
		
		ordClass = "ascendente";
		ordem = "Data";
		codPerfil       = "";
	}

	public void setNomExecuta(String nomExecuta) {
		this.nomExecuta = nomExecuta;
		if (nomExecuta == null)
			this.nomExecuta = "";
	}
	public String getNomExecuta() {
		return this.nomExecuta;
	}
	
	public void setCodOperacaoFk(String codOperacaoFk) {
		this.codOperacaoFk = codOperacaoFk;
		if (codOperacaoFk == null)
			this.codOperacaoFk = "";
	}
	public String getCodOperacaoFk() {
		return this.codOperacaoFk;
	}
	
	public void setCodOperacao(String codOperacao) {
		this.codOperacao = codOperacao;
		if (codOperacao == null)
			this.codOperacao = "";
	}
	public String getCodOperacao() {
		return this.codOperacao;
	}
	public void setCodPerfil(String codPerfil)  {
		this.codPerfil=codPerfil ;
	    if (codPerfil==null) this.codPerfil="" ;
	  }  
	  public String getCodPerfil()  {
	  	return this.codPerfil;
	  }
	
	public void setNomOperacao(String nomOperacao) {
		this.nomOperacao = nomOperacao;
		if (nomOperacao == null)
			this.nomOperacao = "";
	}
	public String getNomOperacao() {
		return this.nomOperacao;
	}
	
	public void setSigFuncao(String sigFuncao) {
		this.sigFuncao = sigFuncao;
		if (sigFuncao == null)
			this.sigFuncao = "";
	}
	public String getSigFuncao() {
		return this.sigFuncao;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public OperacaoBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new OperacaoBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (OperacaoBean)this.dados.get(i);
	 }
	
//	--------------------------  Metodos da Bean ----------------------------------


	public void Le_FuncoesDoPerfil(String codSistema, String codPerfil, SistemaBean SistBeanId)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.consultaFuncoesDoPerfil(this, codSistema, codPerfil, SistBeanId);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass==null) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
//	  --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "Data";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
		public String getNomOrdem()  {
			String nomOrdem = "Sigla da Fun��o e Fun��o" ;
			if (this.ordem.equals("SigFuncao"))    nomOrdem = "Sigla da Fun��o e Fun��o" ;
			if (this.ordem.equals("Funcao"))     nomOrdem = "Fun��o e Sigla da Fun��o" ;   
			return nomOrdem+ " ("+getOrdClass()+")" ;
	  }
//	  --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
			if (ordemSol.equals("SigFuncao")) ord = 1 ;
			if (ordemSol.equals("Funcao"))    ord = 2 ;   
			
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			if (ordemSol.equals(getOrdem()))   {
				if ( getOrdClass().equals("ascendente")) {
					trocaasc = false ;
					trocades = true ;
					setOrdClass("descendente"); 
				} 
				else {
					trocaasc = true ;
					trocades = false;
					setOrdClass("ascendente"); 
				}
			}
			else setOrdClass("ascendente");
			setOrdem(ordemSol) ;
			int tam = getDados().size() ;
			OperacaoBean tmp = new OperacaoBean();
			String func1,func2,sigFunc1,sigFunc2 ;  
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					// "Fun��o e Sigla da Fun��o" ;
					case 0:
					  func1 = getDados(i).getNomOperacao();
					  func2 = getDados(j).getNomOperacao(); 
					  sigFunc1 = getDados(i).getSigFuncao() ;
					  sigFunc2 = getDados(j).getSigFuncao() ;

						if ((func1+sigFunc1).compareTo(func2+sigFunc2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					
					// "Sigla Funcao" ;   
					case 1:
						if (getDados(i).getSigFuncao().compareTo(getDados(j).getSigFuncao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
						
         //"Fun��o" ;
				 case 2:
					 if(getDados(i).getNomOperacao().compareTo(getDados(j).getNomOperacao())>0) troca=trocaasc;
						  else troca=trocades ;
					 break;
					 
					// "Sigla Funcao" ;	
					default:
					if(getDados(i).getSigFuncao().compareTo(getDados(j).getSigFuncao())>0) troca=trocaasc;
					 else troca=trocades ;
				   break;
				}	
				if (troca) 	{		
				   tmp = (OperacaoBean)getDados(i);
				   getDados().set(i,getDados(j));
				   getDados().set(j,tmp);
				}
			}
		}
  }	




}