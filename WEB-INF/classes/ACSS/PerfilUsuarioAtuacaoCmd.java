
package ACSS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios/Orgao Atuacao<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Perfil do Usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class PerfilUsuarioAtuacaoCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/perfilUsuarioAtuacao.jsp";
	private String next;

	public PerfilUsuarioAtuacaoCmd() {
		next = jspPadrao;
	}

	public PerfilUsuarioAtuacaoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			// cria os Beans de Usuario e Perfil, se n�o existir
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

			ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			ACSS.PerfilUsuarioAtuacaoBean PerfilAtuBeanId =(ACSS.PerfilUsuarioAtuacaoBean) req.getAttribute("PerfilAtuBeanId");
			if (PerfilAtuBeanId == null)PerfilAtuBeanId = new ACSS.PerfilUsuarioAtuacaoBean();

			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  			


			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			// Orgao de Lotacao do Usuario
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";
			// Usuario Selecionado (pertence ao Orgao de Lotacao)
			String codUsuario = req.getParameter("codUsuario");
			if (codUsuario == null)	codUsuario = "0";
			
			String codSistema = req.getParameter("codSistema"); 
			if(codSistema == null) codSistema = "0";  

			String nomUserNameOper      = UsrLogado.getNomUserName();	     
			String codOrgaoLotacaoOper  = UsrLogado.getOrgao().getCodOrgao();
			
			if (acao.equals("buscaUsuarios")) {
				OrgaoBeanId.setCodOrgao(codOrgao);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			}

			if (acao.equals("buscaSistemas")) {
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				OrgaoBeanId.setAtualizarDependente("S");
				SistBeanId.setCodSistema(codSistema);
				SistBeanId.Le_Sistema(codSistema,0) ;
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				PerfilAtuBeanId.setCodUsuario(codUsuario);
				PerfilAtuBeanId.Le_OrgaoAtuacao(codSistema,UsrLogado,codOrgao);
			}

			if (acao.equals("A")) {
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				OrgaoBeanId.setAtualizarDependente("S");
				SistBeanId.Le_Sistema(codSistema,0) ;
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);

				String codPerfilNov[] = req.getParameterValues("codPerfilNov");
				if (codPerfilNov == null)codPerfilNov = new String[0];

    			String codOrgaoAtuacao[] = req.getParameterValues("codOrgaoAtuacao");
				if (codOrgaoAtuacao == null) codOrgaoAtuacao = new String[0];
				
				if (codOrgaoAtuacao.length != codPerfilNov.length){
					OrgaoBeanId.setMsgErro("Parametros invalidos.");
				}
				else {
					PerfilAtuBeanId.setCodOrgAtuMarcados(codOrgaoAtuacao);
					PerfilAtuBeanId.setCodUsuario(codUsuario);

					PerfilAtuBeanId.setNomUserNameOper(nomUserNameOper);
					PerfilAtuBeanId.setOrgaoLotacaoOper(codOrgaoLotacaoOper);

					
					PerfilAtuBeanId.atualPerfil(codPerfilNov,codOrgaoAtuacao,codOrgao,codSistema);
					// Sistema, Usuario Logado e Orgao de Lotacao
					PerfilAtuBeanId.Le_OrgaoAtuacao(codSistema,UsrLogado,codOrgao);
					OrgaoBeanId.setMsgErro(PerfilAtuBeanId.getMsgErro());
				}
			}
			if (acao.equals("V")) {
				OrgaoBeanId.setAtualizarDependente("N");
				OrgaoBeanId.setCodOrgao(codOrgao);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			}
			req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			req.setAttribute("SistBeanId", SistBeanId);
			req.setAttribute("UsrBeanId", UsrBeanId);
			req.setAttribute("PerfilAtuBeanId",PerfilAtuBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("PerfisUsuarioAtuacaoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}