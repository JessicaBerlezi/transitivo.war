package ACSS;
import java.util.Vector;

/*
* <b>Title:</b>        Erro<br>
* <b>Description:</b>  Tabela de Erro<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ErroBean extends sys.HtmlPopupBean {
  
  private String codErro;
  private String nomErro;
  private String codOperacao;
   
  private String msgErro ;  
  
  
  public ErroBean(){

    super();
    setTabela("TCAU_ERRO");	
    setPopupWidth(35);
	
    codErro   	  = "0";
	nomErro       = "";
	codOperacao  = "";
    msgErro       = "" ;	
  }
  
  public void setCodErro(String codErro)  {
//	Le_Usuario(codErro,0) ;
  }
  
  public String getCodErro()  {
  	return this.codErro;
  }
  
  public void setNomErro(String nomErro)  {
  	this.nomErro    = nomErro;
  }
  
  public String getNomErro()  {
  	return this.nomErro;
  }

  
  public void setCodOperacao(String codOperacao)  {
  	this.codOperacao= codOperacao;
  }
  
  public String getCodOperacao() {
  	return this.codOperacao;
  }
  
  
public void setMsgErro(Vector vErro)   {
	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
	   }
   }
   
public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
   }   

public String getMsgErro()   {
   	return this.msgErro;
   }   
   
  

  
}