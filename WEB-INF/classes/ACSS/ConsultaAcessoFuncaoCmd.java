package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios/Orgao Atuacao<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Perfil do Usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ConsultaAcessoFuncaoCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ConsultaAcessoFuncao.jsp";
	private String next;

	public ConsultaAcessoFuncaoCmd() {
		next = jspPadrao;
	}

	public ConsultaAcessoFuncaoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = consultaAcessoFuncao(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaAcessoFuncaoCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
	 {
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaAcessoFuncao(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaAcessoFuncaoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String consultaAcessoFuncao( HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			// cria os Beans, se n�o existir

			ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	
			
			OperacaoBean OperBeanId = (OperacaoBean)req.getAttribute("OperBeanId") ;
			if (OperBeanId==null) OperBeanId = new OperacaoBean() ;	  			
  			

			ACSS.ConsultaAcessoFuncaoBean ConsultaAcessoFuncaoBeanId =(ACSS.ConsultaAcessoFuncaoBean) session.getAttribute("ConsultaAcessoFuncaoBeanId");
			if (ConsultaAcessoFuncaoBeanId == null) ConsultaAcessoFuncaoBeanId = new ACSS.ConsultaAcessoFuncaoBean();
			

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "0";
			
			String codOperacao = req.getParameter("codOperacao");
			if (codOperacao == null) codOperacao = "0";
			
			String verTable = req.getParameter("verTable");
			if (verTable == null) verTable = "N";
			
			String tipoUsuario=req.getParameter("tipoUsuario");
			if (tipoUsuario == null) tipoUsuario = "";
			
			
			if (acao.equals("buscaFuncao")) {
				SistBeanId.setCodSistema(codSistema);
				OperBeanId.setCodSistema(codSistema);
			}
			
			if (acao.equals("buscaDados")){
				verTable = "S";	
				SistBeanId.Le_Sistema(codSistema,0) ;
				OperBeanId.Le_Operacao(codSistema,codOperacao,0);
				ConsultaAcessoFuncaoBeanId.setIndUsuario(tipoUsuario);
				ConsultaAcessoFuncaoBeanId.Le_AcessoFuncao(codOperacao,codSistema);		
			}
			
			if (acao.equals("Classifica")) {
				SistBeanId.Le_Sistema(codSistema,0) ;
				OperBeanId.Le_Operacao(codSistema,codOperacao,0);
				ConsultaAcessoFuncaoBeanId.Le_AcessoFuncao(codOperacao,codSistema);	
				ConsultaAcessoFuncaoBeanId.Classifica(req.getParameter("ordem"));
				verTable = "S";	
			}
			
			if (acao.equals("ImprimeResultConsulta")) {
				//Titulo do Relatorio de impressao
				String tituloConsulta = "CONSULTA ACESSO A FUN��O";
				if (ConsultaAcessoFuncaoBeanId.getIndUsuario().equals("A"))
					tituloConsulta+=" - USU�RIOS : ATIVOS";
				if (ConsultaAcessoFuncaoBeanId.getIndUsuario().equals("I"))
					tituloConsulta+=" - USU�RIOS : INATIVOS";
				if (ConsultaAcessoFuncaoBeanId.getIndUsuario().equals("T"))
					tituloConsulta+=" - USU�RIOS : TODOS";
				req.setAttribute("tituloConsulta", tituloConsulta);
								
				ConsultaAcessoFuncaoBeanId.setDados(ConsultaAcessoFuncaoBeanId.getDados());		
				nextRetorno="/ACSS/ConsultaAcessoFuncaoImp.jsp";
			}
			
		if (acao.equals("V")) {
				verTable = "N";
			}

			req.setAttribute("verTable", verTable);
			session.setAttribute("SistBeanId", SistBeanId);
			session.setAttribute("UsrBeanId", UsrBeanId);
			session.setAttribute("OperBeanId", OperBeanId);
			session.setAttribute("ConsultaAcessoFuncaoBeanId",ConsultaAcessoFuncaoBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaAcessoFuncaoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
		
	}

}