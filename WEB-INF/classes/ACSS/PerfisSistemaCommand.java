package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar PerfisSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class PerfisSistemaCommand extends sys.Command {

  private static final String jspPadrao="/ACSS/perfisSistema.jsp";    
  private String next;

  public PerfisSistemaCommand() {
    next             =  jspPadrao;
  }

  public PerfisSistemaCommand(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		// cria os Beans do Usuario, se n�o existir
		ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
		if (SistId==null)        SistId = new ACSS.SistemaBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   
	    String codSistema = req.getParameter("codSistema"); 		
		if(codSistema==null)       codSistema =""; 
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null)       atualizarDependente ="N";   		 
	    SistId.Le_Sistema(codSistema,0) ;
		SistId.setAtualizarDependente(atualizarDependente);			  						  		
	    if ("buscaSistema".indexOf(acao)>=0) {
	      SistId.getPerfis(20,5) ;	
		  SistId.setAtualizarDependente("S");			  	
	    }	 
		Vector vErro =new Vector(); 
	    if(acao.compareTo("A")==0){
		
			String[] nomDescricao = req.getParameterValues("nomDescricao");
			if(nomDescricao==null)  nomDescricao = new String[0];         
			String[] codPerfil = req.getParameterValues("codPerfil");
			if(codPerfil==null)  codPerfil = new String[0];

			String[] indVisEndereco = req.getParameterValues("indVisEndereco");
			if(indVisEndereco==null)  indVisEndereco = new String[nomDescricao.length];   
			String[] indVis2via = req.getParameterValues("indVis2via");
			if(indVis2via==null)  indVis2via = new String[nomDescricao.length]; 
			String[] indRecebNot = req.getParameterValues("indRecebNot");
			if(indRecebNot==null) indRecebNot = new String[nomDescricao.length];
			String[] indVisAr = req.getParameterValues("indVisAr");
			if(indVisAr==null)  indVisAr = new String[nomDescricao.length];   
			String[] indVisAuto = req.getParameterValues("indVisAuto");
			if(indVisAuto==null)  indVisAuto = new String[nomDescricao.length];
            String[] indVisFoto = req.getParameterValues("indVisFoto");
            if(indVisFoto==null)  indVisFoto = new String[nomDescricao.length];

			String[] indVisReq = req.getParameterValues("indVisReq");
			if(indVisReq==null)  indVisReq = new String[nomDescricao.length];   
			String[] indVisHist = req.getParameterValues("indVisHist");
			if(indVisHist==null)  indVisHist = new String[nomDescricao.length];         
			String[] indVisRej = req.getParameterValues("indVisRej");
			if(indVisRej==null)  indVisRej = new String[nomDescricao.length];   
			String[] indVisImpr = req.getParameterValues("indVisImpr");
			if(indVisImpr==null)  indVisImpr = new String[nomDescricao.length];
			String[] indVisTodosOrg = req.getParameterValues("indVisTodosOrg");
			if(indVisTodosOrg==null)  indVisTodosOrg = new String[nomDescricao.length];

			for (int i = 0; i<indVisEndereco.length;i++) {
				if (indVisEndereco[i]==null)  indVisEndereco[i]="N";											
				if (indVis2via[i]==null)      indVis2via[i]="N";
				if (indRecebNot[i]==null)      indRecebNot[i]="N";
				if (indVisAr[i]==null)        indVisAr[i]="N";											
				if (indVisAuto[i]==null)      indVisAuto[i]="N";
                if (indVisFoto[i]==null)      indVisFoto[i]="N";
				if (indVisReq[i]==null)       indVisReq[i]="N";											
				if (indVisHist[i]==null)      indVisHist[i]="N";
				if (indVisRej[i]==null)       indVisRej[i]="N";											
				if (indVisImpr[i]==null)      indVisImpr[i]="N";	
				if (indVisTodosOrg[i]==null)  indVisTodosOrg[i]="N";	
			}

			vErro      = isParametros(nomDescricao,codPerfil) ;
            if (vErro.size()==0) {
				Vector perfis = new Vector(); 
				for (int i = 0; i<nomDescricao.length;i++) {
					PerfilBean myPerf = new PerfilBean() ;	
					myPerf.setNomDescricao(nomDescricao[i]);	  
				    myPerf.setCodSistema(codSistema) ;	
				    myPerf.setCodPerfil(codPerfil[i]) ;
				    myPerf.setIndVisEndereco(indVisEndereco[i]) ;
					myPerf.setIndVis2via(indVis2via[i]) ;
					myPerf.setIndRecebNot(indRecebNot[i]) ;					
					myPerf.setIndVisAr(indVisAr[i]) ;											
					myPerf.setIndVisAuto(indVisAuto[i]) ;
                    myPerf.setIndVisFoto(indVisFoto[i]) ;
					myPerf.setIndVisReq(indVisReq[i]) ;											
					myPerf.setIndVisHist(indVisHist[i]) ;
					myPerf.setIndVisRej(indVisRej[i]) ;											
					myPerf.setIndVisImpr(indVisImpr[i]) ;											
					myPerf.setIndVisTodosOrg(indVisTodosOrg[i]) ;												
					perfis.addElement(myPerf) ; 					
				}
				SistId.setPerfis(perfis) ;
				SistId.isInsertPerfis() ;
			    SistId.getPerfis(20,5) ;						
			}
			else SistId.setMsgErro(vErro) ;  	      
	    }
		req.setAttribute("SistId",SistId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("PerfisSistemaCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
  private Vector isParametros(String[] nomDescricao,String[] codPerfil) {
		Vector vErro = new Vector() ;		 
		if ( (nomDescricao.length!=codPerfil.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 
}