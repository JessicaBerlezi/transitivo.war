package ACSS;

import java.util.List;
import java.util.Vector;

import sys.BeanException;

/**
 * <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
 * <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Marcelo de Souza Domingues
 * @version 1.0
 * @Updates
 */

public class LogAcessoBean extends sys.HtmlPopupBean {
	
	private String codOrgaoLotacao;
	private String codAcesso;
	private String nomUserName;
	private String nomAbrev;
	private String nomSistema;
	private String nomOperacao;
	private String sigFuncao;
	private String sigOrgao;
	private String datOperacao;
	private String datIni;
	private String datFim;
	private String codOrgaoAtuacao;
	private String codSistema;
	private String acao;
	private String ordem ;	
	private List logAcesso;
	private List LstUsrSistema;
	private String atualizarTela;
	private String ordClass ; 
	
	private String 	next;
	private List 	lstRegTotal;
	private List 	lstRegPage;
	private List	lstPage;
	private int		iniCont;
	private int		fimCont;
	private int		tamTotal;
	private int  	totalGeral;
	private String msgErro;
	
	private String[] codOrgAtuMarcados;
	private List perfisOrgao;
	private List perfis;  
	private String sigOrgaoAtuacao; 
	private String nomDescricao; 
	
	private boolean indLogBKP;
	
	public LogAcessoBean() throws sys.BeanException {
		codOrgaoLotacao	=	"";
		codAcesso		=	"";
		nomUserName		=	"";
		nomAbrev		=	"";
		nomOperacao		=	"";
		sigFuncao		=	"";
		sigOrgao = "";
		datOperacao		=	"";
		codOrgaoAtuacao	=	"";
		atualizarTela 	= 	"N";
		acao			=  	"";
		
		ordClass = "ascendente";
		ordem = "Data";
		msgErro = "";
		indLogBKP = false;
	}
	
	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if (codOrgaoLotacao == null)
			this.codOrgaoLotacao = "";
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	
	public void setCodAcesso(String codAcesso) {
		this.codAcesso = codAcesso;
		if (codAcesso == null)
			this.codAcesso = "";
	}
	public String getCodOrgao() {
		return this.codAcesso;
	}
	
	public void setNomUserName(String nomUserName) {
		this.nomUserName = nomUserName;
		if (nomUserName == null)
			this.nomUserName = "";
	}
	public String getNomUserName() {
		return this.nomUserName;
	}
	
	public void setNomAbrev(String nomAbrev) {
		this.nomAbrev = nomAbrev;
		if (nomAbrev == null)
			this.nomAbrev = "";
	}
	public String getNomAbrev() {
		return this.nomAbrev;
	}
	
	public void setNomOperacao(String nomOperacao) {
		if (nomOperacao == null)	nomOperacao = "LOGIN";
		this.nomOperacao = nomOperacao;			
	}
	public String getNomOperacao() {
		return this.nomOperacao;
	}
	
	public void setSigFuncao(String sigFuncao) {
		if (sigFuncao == null)	sigFuncao = "LOGIN";
		this.sigFuncao = sigFuncao;				
	}
	public String getSigFuncao() {
		return this.sigFuncao;
	}
	
	public void setDatOperacao(String datOperacao) {
		this.datOperacao = datOperacao;
		if (datOperacao == null)
			this.datOperacao = datOperacao;
	}
	public String getDatOperacao() {
		return this.datOperacao;
	}
	
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao == null)
			this.codOrgaoAtuacao = codOrgaoAtuacao;
	}
	
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setLogAcesso(List logAcesso) {
		this.logAcesso = logAcesso;
	}
	public List getPerfisOrgao() {
		return this.logAcesso;
	}
	
	public void setSigOrgao(String sigOrgao){
		if (sigOrgao==null) sigOrgao = "";				 
		this.sigOrgao = sigOrgao;				
	}	
	public String getSigOrgao(){ return this.sigOrgao;}	
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao){
		if (sigOrgaoAtuacao==null) sigOrgaoAtuacao = "";				 
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;		
	}	
	
	public String getSigOrgaoAtuacao()
	{		  
		return this.sigOrgaoAtuacao;
	}
	
	public void setAtualizarTela(String atualizarTela)   {
		this.atualizarTela = atualizarTela ;
	}	
	public String getAtualizarTela() {			
		return this.atualizarTela; 
	}
	
	public void setUsuariosDoSistema(List LstUsrSistema)
	{		
		this.LstUsrSistema = LstUsrSistema;
	}
	public List getUsuariosDoSistema() {return this.LstUsrSistema;}
	
	public void setAcao(String acao)
	{		
		this.acao = acao;
	}
	public String getAcao() {	return this.acao; }
	
	public void setDatIni(String dataIni){
		this.datIni = dataIni;
	}
	public String getDatIni() { return this.datIni; }
	
	public void setDatFim(String dataFim){
		this.datFim = dataFim;
	}
	public String getDatFim(){ return this.datFim; }
	
	public void setListaTotal(List listaTotal){
		this.lstRegTotal = listaTotal;
	}
	
	public List getListaTotal(){
		return this.lstRegTotal;
	}	
	
	public void setListaPage(List listaPage){
		this.lstRegPage = listaPage;
	}
	
	public List getListaPage(){
		return this.lstRegPage;
	}
	
	public void setPage(List pList){
		this.lstPage = pList;
	}
	
	public List getPage(){
		return this.lstPage;
	}
	
	public void setIniCont(int pIniCont){
		this.iniCont = pIniCont;		 		
	}
	
	public int getIniCont(){
		return this.iniCont;		 		
	}	
	
	public void setFimCont(int pFimCont){
		this.fimCont = pFimCont; 		 		
	}
	
	public int getFimCont(){
		return this.fimCont;		 		
	}
	
	public void setTamTotal(int pTamTotal){	
		this.tamTotal = pTamTotal;
	}
	
	public int getTamTotal(){	
		return this.tamTotal;
	}
	
	public void setNomSistema(String pNomsistema){
		this.nomSistema = pNomsistema;
	}	
	
	public String getNomSistema(){
		return this.nomSistema;
	}
	
	public void setCodSistema(String pCodSistema){
		this.codSistema = pCodSistema;
	}	
	
	public String getCodSistema(){
		return this.codSistema;
	}	
	
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j) ;
		}
	}
	public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
	}   
	public String getMsgErro()   {
		return this.msgErro;
	} 
	
	public boolean getIndLogBKP() {
		return indLogBKP;
	}
	
	public void setIndLogBKP(boolean indLogBKP) {
		this.indLogBKP = indLogBKP;
	}
	
	    
	
//	------------------------------  Metodos do Bean ----------------------------------	
	
	public boolean contaRegistros(SistemaBean sistBeanId,	UsuarioBean	usuarioBeanId,
			OrgaoBean orgaoBeanId, LogAcessoBean logAcessoBean)throws BeanException{
		boolean ok = true;
		try	{
			Dao dao = Dao.getInstance();
			int totalRegistros = dao.verificaNumRegistro(sistBeanId, usuarioBeanId,  orgaoBeanId,logAcessoBean);
			//Mostra consulta do registro se for menor que 300
			if (totalRegistros < 301)	
				dao.Le_LogAcesso(this, usuarioBeanId, sistBeanId, orgaoBeanId);
			else ok = false;
			return ok;    
		}
		catch (Exception e)	{
			throw new sys.BeanException(e.getMessage());			
		}		
	}
	
	
	public void Le_LogAcesso(	SistemaBean sistBeanId,	UsuarioBean	usuarioBean,
			OrgaoBean   orgaoBeanId)throws BeanException{
		try	{
			Dao dao = Dao.getInstance();
			if (!dao.Le_LogAcesso(this, usuarioBean, sistBeanId, orgaoBeanId)) 
				this.setMsgErro("Nenhum Registro Encontrado");
			
		}
		catch (Exception e)	{
			throw new sys.BeanException(e.getMessage());			
		}		
	}
	
	
	public LogAcessoBean getListaTotal(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.lstRegTotal.size()) ) return (new LogAcessoBean()) ;
		}
		catch (Exception e) { 			
			throw new sys.BeanException(e.getMessage());
		}
		return (LogAcessoBean)this.lstRegTotal.get(i);
	}	
	
	
	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
	
	public void setOrdClass(String ordClass)  {
		this.ordClass=ordClass ;
		if (ordClass==null) this.ordClass= "ascendente";
	} 
	public String getOrdClass()  {
		return this.ordClass;
	}
//	--------------------------------------------------------------------------
	public void setOrdem(String ordem)  {
		this.ordem=ordem ;
		if (ordem==null) this.ordem= "Data";
	}  
	public String getOrdem()  {
		return this.ordem;
	}
	public String getNomOrdem()  {
		String nomOrdem = "�rg�o, Usu�rio, Sistema, Fun��o, Data Opera��o e Atua��o" ;
		if (this.ordem.equals("Orgao"))    nomOrdem = "�rg�o, Usu�rio e Data Opera��o" ;
		if (this.ordem.equals("Usuario"))  nomOrdem = "Usu�rio,�rg�o e Data Opera��o" ;   
		if (this.ordem.equals("Sistema"))  nomOrdem = "Sistema, Data e Atua��o" ;   
		if (this.ordem.equals("Operacao")) nomOrdem = "Fun��o, Data e Atua��o" ;   
		if (this.ordem.equals("DatOperacao")) nomOrdem = "Data Opera��o, �rg�o, Usuario e Atua��o";
		if (this.ordem.equals("Atuacao")) nomOrdem = "Atua��o, Data de Opera��o, Org�o e Usu�rio";
		return nomOrdem+ " ("+getOrdClass()+")" ;
	}
	
	
//	--------------------------------------------------------------------------
	
	public void Classifica(String ordemSol) throws sys.BeanException {
		int ord = 0;
		if ((ordemSol==null) || (ordemSol.equals(""))) ord = 0 ;
		if (ordemSol.equals("Orgao")) ord = 1 ;
		if (ordemSol.equals("Usuario")) ord = 2 ;   
		if (ordemSol.equals("Sistema")) ord = 3 ;
		if (ordemSol.equals("Operacao"))ord = 4 ;		
		if (ordemSol.equals("DatOperacao"))ord = 5 ; 
		if (ordemSol.equals("Atuacao"))ord = 6 ; 			
		
		boolean troca    = false ;
		boolean trocaasc = true ;
		boolean trocades = false ;
		if (ordemSol.equals(getOrdem()))   {
			if ( getOrdClass().equals("ascendente")) {
				trocaasc = false ;
				trocades = true ;
				setOrdClass("descendente"); 
			} 
			else {
				trocaasc = true ;
				trocades = false;
				setOrdClass("ascendente"); 
			}
		}
		else setOrdClass("ascendente");
		setOrdem(ordemSol) ;
		int tam = getListaTotal().size() ;
		LogAcessoBean tmp = new LogAcessoBean();
		String usr1,usr2,d1,d2,oLot1,oLot2,op1,op2,s1,s2,oAtu1,oAtu2;  
		for (int i=0; i<tam; i++)  {  
			for (int j=i+1; j<tam; j++)  {
				troca=false;
				// compara os campos
				switch (ord) {
				// "Org�o, Usuario e Data Operacao" ;
				case 0:
					oLot1 = getListaTotal(i).getSigOrgao();									
					oLot2 = getListaTotal(j).getSigOrgao();
					usr1 = getListaTotal(i).getNomUserName();
					usr2 = getListaTotal(j).getNomUserName();
					s1 = getListaTotal(i).getNomAbrev();
					s2 = getListaTotal(j).getNomAbrev();
					op1 = getListaTotal(i).getNomOperacao();
					op2 = getListaTotal(j).getNomOperacao();
					d1 = getListaTotal(i).getDatOperacao(); 
					d2 = getListaTotal(j).getDatOperacao();
					oAtu1 = getListaTotal(i).getCodOrgaoAtuacao(); 
					oAtu2 = getListaTotal(j).getCodOrgaoAtuacao();	
					
					if ((oLot1+usr1+s1+op1+d1+oAtu1).compareTo(oLot2+usr2+s2+op2+d2+oAtu2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;
					
					// "Org�o, Usuario e Data Operacao" ;
				case 1:
					oLot1 = getListaTotal(i).getSigOrgao();									
					oLot2 = getListaTotal(j).getSigOrgao();
					usr1 = getListaTotal(i).getNomUserName();
					usr2 = getListaTotal(j).getNomUserName();
					d1 = getListaTotal(i).getDatOperacao(); 
					d2 = getListaTotal(j).getDatOperacao();					
					if ((oLot1+usr1+d1).compareTo(oLot2+usr2+d2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;			
					
					// "Usu�rio,Org�o e Data Operacao" ;				
				case 2:
					usr1 = getListaTotal(i).getNomUserName();
					usr2 = getListaTotal(j).getNomUserName();		
					oLot1 = getListaTotal(i).getSigOrgao();									
					oLot2 = getListaTotal(j).getSigOrgao();
					d1 = getListaTotal(i).getDatOperacao();
					d2 = getListaTotal(j).getDatOperacao();
					
					if ((usr1+oLot1+d1).compareTo(usr2+oLot2+d2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;
					
					// "Sistema, Data, Orgao de Atuacao" ;								
				case 3:					
					s1 = getListaTotal(i).getNomAbrev();
					s2 = getListaTotal(j).getNomAbrev();
					d1 = getListaTotal(i).getDatOperacao();
					d2 = getListaTotal(j).getDatOperacao(); 
					oAtu1 = getListaTotal(i).getCodOrgaoAtuacao(); 
					oAtu2 = getListaTotal(j).getCodOrgaoAtuacao();								
					if ((s1+d1+oAtu1).compareTo(s2+d2+oAtu2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;			
					
					// "Operacao, Data e OrgaoAtuacao" ;								
				case 4:					
					op1 = getListaTotal(i).getNomOperacao();
					op2 = getListaTotal(j).getNomOperacao();
					d1 = getListaTotal(i).getDatOperacao(); 
					d2 = getListaTotal(j).getDatOperacao();
					oAtu1 = getListaTotal(i).getSigOrgaoAtuacao();									
					oAtu2 = getListaTotal(j).getSigOrgaoAtuacao();
					if ((op1+d1+oAtu1).compareTo(op2+d2+oAtu2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;			
					
					// "Data Operacao,Orgao Lotacao, Usuario e OrgaoAtuacao" ;								
				case 5:					
					op1 = getListaTotal(i).getDatOperacao();
					op2 = getListaTotal(j).getDatOperacao();
					oLot1 = getListaTotal(i).getSigOrgao();									
					oLot2 = getListaTotal(j).getSigOrgao();																	
					usr1 = getListaTotal(i).getNomUserName();
					usr2 = getListaTotal(j).getNomUserName();
					oAtu1 = getListaTotal(i).getSigOrgaoAtuacao(); 						
					oAtu2 = getListaTotal(j).getSigOrgaoAtuacao();					
					if ((op1+oLot1+usr1+oAtu1).compareTo(op2+oLot2+usr2+oAtu2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;													
					
					// "OrgaoAtuacao, Data de Operacao, Org�o e Usu�rio" ;								
				case 6:					
					oAtu1 = getListaTotal(i).getSigOrgaoAtuacao();									
					oAtu2 = getListaTotal(j).getSigOrgaoAtuacao();				
					d1 = getListaTotal(i).getDatOperacao(); 
					d2 = getListaTotal(j).getDatOperacao();
					oLot1 = getListaTotal(i).getSigOrgao(); 						
					oLot2 = getListaTotal(j).getSigOrgao();
					usr1 = getListaTotal(i).getNomUserName();
					usr2 = getListaTotal(j).getNomUserName();					
					if ((oAtu1+d1+oLot1+usr1).compareTo(oAtu2+d2+oLot2+usr2)>0 ) troca=trocaasc;
					else troca=trocades ;
					break;								
				}	
				if (troca) 	{		
					tmp = (LogAcessoBean)getListaTotal().get(i);
					getListaTotal().set(i,getListaTotal().get(j));
					getListaTotal().set(j,tmp);
				}
			}
			
		}
	}	
	
	public boolean verificaDataInicial(LogAcessoBean logAcessoBean) throws TAB.DaoException{
		boolean bOk = false;
		try {
			Dao dao = Dao.getInstance();
			bOk = dao.verificaDataInicial(this);

		} 
		catch (TAB.DaoException e) 
		{
			throw new TAB.DaoException("Erro:"+ e.getMessage());
		}
		
		return bOk;
		
	}
	
	
	
	
}