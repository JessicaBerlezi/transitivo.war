package ACSS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;


import sys.BeanException;
import ACSS.DaoException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class AvisoBean extends sys.HtmlPopupBean {
    
    private int         codAviso;    
    private String      dscAviso;
    private String      txtAviso;
    private String      indPrioridade;
    private UsuarioBean usuario;
    private Date        datEnvio;
	private String      datInicioAviso;
	private String      datFimAviso;
    private AvisoBean[] Avisos;
	private String      msgErro;

	private String codGenerico;
	private String nomGenerico;

	private String orgao;
	private String sistemPerfil;

	private Vector vetItens;
		
    public AvisoBean() throws BeanException {        
        super();
        codAviso = 0;
        dscAviso = "";
        txtAviso = "";
        indPrioridade = "N";
        usuario = new UsuarioBean();
        datEnvio = new Date();
        
		datInicioAviso="";
		datFimAviso="";
		Avisos   = new AvisoBean[0];
		vetItens = new Vector();
		msgErro  ="";
		orgao    ="";		
		codGenerico ="";
		nomGenerico ="";
		sistemPerfil="";
    }
	//-------------------------------------------------------------------------
	public void setSistemPerfil(String s)  {
		this.sistemPerfil = s ;
		if( sistemPerfil == null ) this.sistemPerfil = "0";
	}
	public String getSistemPerfil()  { return this.sistemPerfil;  }
	//-------------------------------------------------------------------------
	public void setCodGenerico(String codGenerico)  {
		this.codGenerico = codGenerico ;
		if( codGenerico == null ) this.codGenerico = "";
	}
	public String getCodGenerico()  { return this.codGenerico;  }
	//-------------------------------------------------------------------------
	public void setNomGenerico(String nomGenerico)  {
		this.nomGenerico    = nomGenerico;
		if( nomGenerico == null) this.nomGenerico = "";
	}
	public String getNomGenerico()  { return this.nomGenerico; }
    //-------------------------------------------------------------------------
	public String getOrgao() {  return orgao;  }
	public void setOrgao(String orgao) {
	  this.orgao = orgao;
	  if( orgao == null) this.orgao = "";
	}
	//-------------------------------------------------------------------------
	public void   setVetItens(Vector vetItens) {		
		this.vetItens = vetItens;
		if( vetItens == null) this.vetItens = new Vector();
	}
	public Vector getVetItens(){ return this.vetItens; }
	//-------------------------------------------------------------------------   
	public AvisoBean getVetItens(int i) { 
		 return (AvisoBean)this.vetItens.elementAt(i); 
	}   
    //------------------------------------------------------------------------- 
	public void setMsgErro( Vector vErro ) {
	   for (int j=0; j<vErro.size(); j++)
			  this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
	   this.msgErro = msgErro;
	}
	public String getMsgErro() {	return this.msgErro; }
	//------------------------------------------------------------------------
    public AvisoBean(String dscAviso, String txtAviso, String indPrioridade, UsuarioBean usuario,
    	Date datEnvio) throws BeanException {
        this();
        this.dscAviso = dscAviso;
        this.txtAviso = txtAviso;
        this.indPrioridade = indPrioridade;
        this.usuario = usuario;
        this.datEnvio = datEnvio;
    }
	//------------------------------------------------------------------------ 
	  public String getDatInicioAviso() {  return datInicioAviso;  }
	  public void setDatInicioAviso(String datInicioAviso) {
		this.datInicioAviso = datInicioAviso;
		if( datInicioAviso == null) this.datInicioAviso = "";
	  }
	//------------------------------------------------------------------------ 
	  public String getDatFimAviso() {  return datFimAviso;  }
	  public void setDatFimAviso(String datFimAviso) {
		this.datFimAviso = datFimAviso;	
		if( datFimAviso == null) this.datFimAviso = "";
	  }
	//------------------------------------------------------------------------ 
    public int getCodAviso() {  return codAviso;  }
    public void setCodAviso(int codAviso) {
	  this.codAviso = codAviso;
    }
	//------------------------------------------------------------------------    
    public String getIndPrioridade() { return indPrioridade; }
    public void setIndPrioridade(String indPrioridade) {
        this.indPrioridade = indPrioridade;
    }
	//------------------------------------------------------------------------
    public String getDscAviso() { return dscAviso; }
    public void setDscAviso(String dscAviso) {
        this.dscAviso = dscAviso;
        if( dscAviso == null) this.dscAviso = "";
    }
	//------------------------------------------------------------------------
    public String getTxtAviso() { return txtAviso; }
    public void setTxtAviso(String txtAviso) {
        this.txtAviso = txtAviso;
        if( txtAviso == null) this.txtAviso = "";
    }
	//------------------------------------------------------------------------
	public Date getDatEnvio() { return datEnvio; }
	public void setDatEnvio(Date date) {
		datEnvio = date;		
	}
	//------------------------------------------------------------------------
	public String getDatEnvioStr() {
		String retorno = "";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		if (datEnvio != null) retorno = df.format(datEnvio);
		return retorno;
	}
	//------------------------------------------------------------------------
	public UsuarioBean getUsuario() { return usuario; }
	public void setUsuario(UsuarioBean bean) {
		usuario = bean;
	}
	//------------------------------------------------------------------------
    public AvisoBean[] getAvisos() { return Avisos; }
    public void setAvisos(AvisoBean[] Avisos) {
        this.Avisos = Avisos;
        if( Avisos == null) this.Avisos = new AvisoBean[0];
    }
	//------------------------------------------------------------------------
	public void carrega(UsuarioBean usuario, UsuarioFuncBean usuarioFunc) throws TAB.DaoException {        
		this.Avisos = Dao.getInstance().carregaAvisos(usuario, usuarioFunc, true);
	}
    
    public void carregaNaoLidos(UsuarioBean usuario, UsuarioFuncBean usuarioFunc) throws TAB.DaoException {        
        this.Avisos = Dao.getInstance().carregaAvisos(usuario, usuarioFunc, true);
    }
    	    
    public static AvisoBean consulta(int codAviso) throws TAB.DaoException {        
        return Dao.getInstance().consultaAviso(codAviso);
    }
    
	public static AvisoBean[] consultaNaoLidas(UsuarioBean usuario, UsuarioFuncBean usuarioFunc) throws TAB.DaoException {        
		return Dao.getInstance().carregaAvisos(usuario, usuarioFunc, true);
	}
    
    public void le(UsuarioBean usuario) throws TAB.DaoException {        
        Dao.getInstance().leAviso(this, usuario);
    }
    
	public boolean lido(UsuarioBean usuario) throws TAB.DaoException {        
		return Dao.getInstance().avisoLido(this, usuario);
	}

    //----------------------------------------------------------------------
	public static AvisoBean[] pegaprioridadeAviso(UsuarioBean usuario, UsuarioFuncBean usuarioFunc) throws TAB.DaoException {        
			return Dao.getInstance().pegaprioridadeAviso(usuario, usuarioFunc, true);
	}
    //	----------------------------------------------------------------------
	public void MostraAvisosEntreDatas(UsuarioBean usuario,String dat1,String dat2) throws TAB.DaoException {        
		this.Avisos = Dao.getInstance().MostraAvisosEntreDatas(usuario,dat1,dat2);
	}
	//---------------------------------------------------------------------------
//	novo metodo	
	public void PegaOrgaoUsuario(UsuarioBean usuario,int tipo) throws TAB.DaoException {        
		Vector vErro = new Vector(); 
		Dao dao = ACSS.Dao.getInstance();
		dao.PegaOrgaoUsuario(this,usuario, vErro,tipo);
	}
	//---------------------------------------------------------------------------
	public boolean isInsertAviso(UsuarioBean usuario,Vector vErro,int tipo ) throws TAB.DaoException {
	  boolean retorno = true;
		  Dao dao = Dao.getInstance();
		  if( dao.InsertAviso( this,usuario, vErro, tipo ) ){}
		  else retorno = false;   
		
		  this.vetItens.clear();
		
		  setMsgErro(vErro);
		  return retorno;		
	}
	//---------------------------------------------------------------------------
}




