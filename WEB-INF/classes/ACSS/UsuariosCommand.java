package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BiometriaCmd;
import sys.Command;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Usuarios<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Usuarios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class UsuariosCommand extends sys.Command {

  private static final String jspPadrao="/ACSS/Usuario.jsp" ;   
  private String next;

  public UsuariosCommand() {
    next             = jspPadrao ;
  }

  public UsuariosCommand(String next) {
    this.next = next;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {  
	    // cria os Beans, se n�o existir
    	HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			

		// cria os Beans do Usuario, se n�o existir
		ACSS.UsuarioBean UsrBeanId = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
		if (UsrBeanId==null)        UsrBeanId = new ACSS.UsuarioBean() ;
		
		// cria os Beans de Usuario e Perfil, se n�o existir
		ACSS.OrgaoBean OrgaoBeanId = new ACSS.OrgaoBean();  
		
		// cria os Beans de Usuario e Perfil, se n�o existir
		ACSS.ProtocoloUpoBean protocoloUpoBeanId = new ACSS.ProtocoloUpoBean();  
		
		Vector vErro = new Vector();			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   

		String pkid = req.getParameter("pkid");   
		if(pkid==null)          pkid  ="0";  		  	  	    	  
		String codOrgao       = req.getParameter("codOrgao");	     
		if(codOrgao==null)      codOrgao=""; 
		String nomUsuario     = req.getParameter("nomUsuario");	     
		if(nomUsuario==null)    nomUsuario=" "; 
		String numCpf         = req.getParameter("numCpf");
		if(numCpf==null)        numCpf="";
		String nomUserName    = req.getParameter("nomUserName");	     
		if(nomUserName==null)   nomUserName=""; 
		String codSenha       = req.getParameter("codSenha");
		if(codSenha==null)      codSenha="";  
		String datValidade    = req.getParameter("datValidade");
		if(datValidade==null)   datValidade="";  
		String horaValidade	  = req.getParameter("horaValidade");
		if(horaValidade==null)  horaValidade="";
		String codUsrResp     = req.getParameter("codUsrResp");
		if(codUsrResp==null)    codUsrResp="";  
		String codUsrRespAlt  = req.getParameter("codUsrRespAlt");
		if(codUsrRespAlt==null) codUsrRespAlt="";  
		String email  = req.getParameter("email");
		if(email==null) email="";
		String localTrabalho  = req.getParameter("localTrabalho");
		if(localTrabalho==null) localTrabalho=""; 
		String codProtUpo = req.getParameter("codProtUpo");
		if(codProtUpo==null) codProtUpo=""; 
		
		// Preencher o Bean
		UsrBeanId.setPkid(pkid) ;
		UsrBeanId.setCodUsuario(pkid) ;			
		UsrBeanId.getOrgao().setCodOrgao(codOrgao);				
		UsrBeanId.setNomUsuario(nomUsuario);		
		UsrBeanId.setNumCpfEdt(numCpf);			
		UsrBeanId.setNomUserName(nomUserName);					
		UsrBeanId.setCodSenha(codSenha);		
		UsrBeanId.setDatValidade(datValidade);	
		UsrBeanId.setHoraValidade(horaValidade);
		UsrBeanId.setCodUsrResp(codUsrResp);		
		UsrBeanId.setCodUsrRespAlt(codUsrRespAlt);	
		UsrBeanId.setEmail(email);
		UsrBeanId.setLocalTrabalho(localTrabalho);
		UsrBeanId.getProtocoloUpoBean().setCodProtocoloUpo(codProtUpo);
		//
		OrgaoBeanId.Le_Orgao(codOrgao, 0);
		UsrBeanId.setSigOrgaoAtuacao(OrgaoBeanId.getSigOrgao());
		
		protocoloUpoBeanId.Le_ProtocoloUpo(codProtUpo, 0);
		
		String nomUserNameOper      = UsrLogado.getNomUserName();	     
		String codOrgaoLotacaoOper  = UsrLogado.getOrgao().getCodOrgao();

		UsrBeanId.setNomUserNameOper(nomUserNameOper);
		UsrBeanId.setOrgaoLotacaoOper(codOrgaoLotacaoOper);
		
			
		String DatCadastro = req.getParameter("DatCadastro");  
		if(DatCadastro==null)  DatCadastro  = "";
	    if (DatCadastro.trim().length()==0) {
	    	DatCadastro = sys.Util.formatedToday().substring(0,10);
			UsrBeanId.setDatCadastro(DatCadastro);		
		}
		String DatCadastroAlt = req.getParameter("DatCadastroAlt");
		if(DatCadastroAlt==null)             DatCadastroAlt            = "";
		if (DatCadastroAlt.trim().length()==0) {
			DatCadastroAlt = sys.Util.formatedToday().substring(0,10);
			UsrBeanId.setDatCadastroAlt(DatCadastroAlt);		
		}
		
		if ("UsuariosConsultaCommand".indexOf(acao) >= 0) return processaUsuariosConsulta(req,nextRetorno,UsrBeanId,UsrLogado) ;

		if ("Top,Proximo,Anterior,Fim,Pkid".indexOf(acao) >= 0) {
            if ("Pkid".indexOf(acao)>=0) UsrBeanId.buscaCodUsuario(pkid,acao,UsrLogado);		
			else                         UsrBeanId.buscaCodUsuario(nomUsuario,acao,UsrLogado);
		}
		if ("Novo".indexOf(acao)>=0) {	  
          UsrBeanId.setPkid("0");
          UsrBeanId.setCodUsuario("0");						  		
		  UsrBeanId.setNomUserName("");
		  UsrBeanId.setNomUsuario("");	
		  UsrBeanId.setNumCpf("");	
		  UsrBeanId.setCodSenha("");			
		  UsrBeanId.setDatCadastro(sys.Util.formatedToday().substring(0,10));
		  UsrBeanId.setHoraValidade("");		
		  UsrBeanId.setEmail("");
		  UsrBeanId.setLocalTrabalho("");
		  UsrBeanId.getOrgao().setCodOrgao("") ;
		  UsrBeanId.getProtocoloUpoBean().setCodProtocoloUpo("") ;
		}	  
	   	if ("123".indexOf(acao)>=0) {
			//		Setar o(s) objetos para executar a acao requerida
		 	if ("1".indexOf(acao)>=0) {
				UsrBeanId.setDatCadastro(DatCadastro);
				UsrBeanId.setCodUsrResp(UsrLogado.getCodUsuario());	
		 	}
		 	if ("12".indexOf(acao)>=0) {
				UsrBeanId.setDatCadastroAlt(sys.Util.formatedToday().substring(0,10));
				UsrBeanId.setCodUsrRespAlt(UsrLogado.getCodUsuario());		
		 	}
		 	UsrBeanId.setMsgErro("") ;  	      
			if(acao.compareTo("1")==0) UsrBeanId.isInsert() ;
			if(acao.compareTo("2")==0) UsrBeanId.isAltera() ;		
			if(acao.compareTo("3")==0) UsrBeanId.isDelete() ; 
		 }
		 
		if (acao.equals("cadBiometria")) {
			BiometriaCmd BiometriaCommand = new BiometriaCmd(next);
			Command command = BiometriaCommand;
			nextRetorno  = command.execute(req);
			UsrBeanId = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
		}
		
		if (acao.equals("Biometria"))
		{
			nextRetorno="/sys/CadastraBiometria.jsp";
		}
					
		 req.setAttribute("UsrBeanId",UsrBeanId) ;
    }
    catch (Exception ue) {
      throw new sys.CommandException("UsuarioCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
	private String processaUsuariosConsulta(HttpServletRequest req,String nextRetorno,ACSS.UsuarioBean UsrBeanId,ACSS.UsuarioBean UsrLogado) throws sys.CommandException	{
		HttpSession session   = req.getSession() ;
		String acaoConsulta = req.getParameter("acaoConsulta"); 
		if (acaoConsulta==null) acaoConsulta="" ;
		// inicio da consulta
        if (acaoConsulta.equals("")) {	
			// colocar os Beans a serem preservados na sessao
			session.setAttribute("UsrBeanId",UsrBeanId) ;	
			// Nome para guardar o Bean de Consultana sessao
			session.setAttribute("nomeBeanConsulta","ConsultaUsrBeanId") ;						
        }
		
		// fim da consuta - fica neste comando
	    if ((acaoConsulta.equals("pkid")) || (acaoConsulta.equals("Retornar"))) {									
		
			//restaurar da  sessao os Bean preservados
			String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");			
			session.removeAttribute(nomeBeanConsulta); 						
			session.removeAttribute("nomeBeanConsulta"); 			
			try 	{
				// recupera o(s) Bean(s) na sessao 
				UsrBeanId = (ACSS.UsuarioBean)session.getAttribute("UsrBeanId") ;		
				if (UsrBeanId==null) UsrBeanId = new ACSS.UsuarioBean() ;
			}
			catch (Exception e){
			  throw new sys.CommandException("UsuarioCommand 003: " + e.getMessage());
			}			
						

			// remove o Bean da sessao
			session.removeAttribute("UsrBeanId"); 				
			
			//setar os Beans desejados				
			if (acaoConsulta.equals("pkid")==true) 	{
				String pkidConsulta = req.getParameter("pkidConsulta"); 		
				if (pkidConsulta==null) pkidConsulta="" ;
				UsrBeanId.buscaCodUsuario(pkidConsulta,"Pkid",UsrLogado) ;
				req.setAttribute("UsrBeanId",UsrBeanId) ;					        						
			}
	    }
		else 		{
			try 	{
				// executar o comando de Consulta
				sys.Command cmd = (sys.Command)Class.forName("ACSS.UsuariosConsultaCommand").newInstance() ;							
				nextRetorno = cmd.execute(req);		
			}
			catch (Exception e){
		      throw new sys.CommandException("UsuarioCommand 002: " + e.getMessage());
			}			
		}
	
		return nextRetorno ;
	}
}