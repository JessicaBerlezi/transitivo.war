package ACSS;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author dgeraldes
* @version 1.0
* @Updates
*/

public class ConsultaUsuarioLogadoBean extends sys.HtmlPopupBean {

	private String dataSessao;
	private String Ip; 
	private String nomUserName;
	private String datBloqueio;
	public  String num_minutos;
	private String codOrgaoAtuacao;
	private String codOrgaoLotacao;
	private String codSistema;
	private String codUsuario;
	private String sistema;
	private String flBloqueado;
	private String sigFuncao;
	private String nom_operacao;
	private String dat_operacao;
	private String sigOrgaoAtuacao;
	public  String nom_abrev;
	private String numSessao;
	private List dados;
	
	//lista de ordenacao
	private String ordClass ;
	private String ordem ;
	

	public ConsultaUsuarioLogadoBean() throws sys.BeanException {
		numSessao = "";
		nomUserName = "";
		Ip = "";
		dataSessao = "";
		datBloqueio = "";
		num_minutos = "";
		codOrgaoAtuacao = "";
		codOrgaoLotacao = "";
		codSistema = "";
		codUsuario = "";
		sistema = "";
		flBloqueado = "";
		sigFuncao = "";
		dat_operacao = "";
		nom_operacao = "";
		nom_abrev = "";
		sigOrgaoAtuacao = "";
	
		dados = new ArrayList();
	
		//ordClass = "ascendente";
		//ordem = "DAT_SESSAO";
	}
	
	public void setNumSessao(String numSessao) {
		this.numSessao = numSessao;
		if (numSessao == null)
			this.numSessao = "";
	}
	public String getNumSessao() {
		return this.numSessao;
	}

	public void setNomUserName(String nomUserName) {
		this.nomUserName = nomUserName;
		if (nomUserName == null)
			this.nomUserName = "";
	}
	public String getNomUserName() {
		return this.nomUserName;
	}
	
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() {
		return this.codUsuario;
	}

	public void setIp(String Ip) {
		this.Ip = Ip;
		if (Ip == null)
			this.Ip = "";
	}
	public String getIp() {
		return this.Ip;
	}

	public void setDataSessao(String dataSessao) {
		this.dataSessao = dataSessao;
		if (dataSessao == null)
			this.dataSessao = "";
	}
	public String getdataSessao() {
		return this.dataSessao;
	}
	
	public void setdatBloqueio(String datBloqueio) {
		this.datBloqueio = datBloqueio;
		if (datBloqueio == null)
			this.datBloqueio = "";
	}
	public String getdatBloqueio() {
		return this.datBloqueio;
	}
	
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if ((codOrgaoAtuacao == null) || (codOrgaoAtuacao.equals("")))
			this.codOrgaoAtuacao = "0";
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if ((codOrgaoLotacao == null) || (codOrgaoLotacao.equals("")))
			this.codOrgaoLotacao = "0";
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	
	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if ((codSistema == null) || (codSistema.equals("")))
			this.codSistema = "0";
	}
	public String getCodSistema() {
		return this.codSistema;
	}
	
	public void setSistema(String sistema) {
		this.sistema = sistema;
		if (sistema == null)
			this.sistema = "";
	}
	public String getSistema() {
		return this.sistema;
	}
	
	public void setNumMinutos(String num_minutos) {
		this.num_minutos = num_minutos;
		if (num_minutos == null)
			this.num_minutos = "";
	}
	public String getNumMinutos() {
		return this.num_minutos;
	}
	

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setflBloqueado(String flBloqueado) {
		this.flBloqueado = flBloqueado;
		if (flBloqueado == null)
			this.flBloqueado = "";
	}
	public String getflBloqueado() {
		return this.flBloqueado;
	}
	
	public void setSigFuncao(String sigFuncao) {
		this.sigFuncao = sigFuncao;
		if (sigFuncao == null)
			this.sigFuncao = "";
	}
	public String getSigFuncao() {
		return this.sigFuncao;
	}
	
	public void setNomAbrev(String nom_abrev) {
		this.nom_abrev = nom_abrev;
		if (nom_abrev == null)
			this.nom_abrev = "";
	}
	public String getNomAbrev() {
		return this.nom_abrev;
	}
	
	public void setNomOperacao(String nom_operacao) {
		this.nom_operacao = nom_operacao;
		if (nom_operacao == null)
			this.nom_operacao = "";
	}
	public String getNomOperacao() {
		return this.nom_operacao;
	}
	
	public void setDatOperacao(String dat_operacao) {
		this.dat_operacao = dat_operacao;
		if (dat_operacao == null)
			this.dat_operacao = null;
	}
	public String getDatOperacao() {
		return this.dat_operacao;
	}
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;
		if (sigOrgaoAtuacao == null)
			this.sigOrgaoAtuacao = "";
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}
	
	public ConsultaUsuarioLogadoBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaUsuarioLogadoBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaUsuarioLogadoBean)this.dados.get(i);
	 }
	
	
//	--------------------------  Metodos da Bean ----------------------------------


	public void Le_DadosUsuarioLogado()
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaUsuarioLogado(this);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}
	
	public void Le_CodUsuario(String[] codUsuarios, String[] nom_UserName, String[] codOrgaoLotacao)
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		dao.CarregaCodUsuario(codUsuarios, nom_UserName, codOrgaoLotacao);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
}
	
	public void BloqueiaUsuario(UsuarioBean myUsr, String[] array)
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		dao.InsereUsuarioBloqueado(this, myUsr, array);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	}
}
	
	public boolean VerificaBloqueio(String codUsuario)
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		dao.VerificaBloqueio(this, codUsuario);
		
		if(this.getdatBloqueio().equals("") == false)
		{
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
			java.util.Date dtIniBloqueio = df.parse(this.getdatBloqueio());
			int num_minutos = Integer.parseInt(this.getNumMinutos());
		
			Calendar calendar = new GregorianCalendar();
			calendar.setTime(dtIniBloqueio);
			calendar.add(Calendar.MINUTE, num_minutos);
			java.util.Date dtFimBloqueio = calendar.getTime();
	    
			df = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
			String Hoje = df.format(new Date());
			java.util.Date dtHoje = df.parse(Hoje);
	    
			if((dtHoje.after(dtIniBloqueio)) && (dtHoje.before(dtFimBloqueio)))
				return true;
	     
		}
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	}
     
     return false;
}	
	
	public void Le_LogAcesso()
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		dao.ConsultaLogAcesso(this);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	}
}

	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass.equals("")) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
//	  --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "DAT_SESSAO";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
		public String getNomOrdem()  {
			String nomOrdem = "USU�RIO, SISTEMA, DATA DE ENTRADA, IP, FUNC. EM USO, ORG�O ATUA��O" ;
			if (this.ordem.equals("NOM_USERNAME"))    nomOrdem = "USU�RIO, SISTEMA, DATA DE ENTRADA, IP, FUNC. EM USO, ORG�O ATUA��O" ;
			if (this.ordem.equals("DAT_SESSAO"))     nomOrdem = "DATA DE ENTRADA, USU�RIO, SISTEMA, IP, FUNC. EM USO, ORG�O ATUA��O" ;   
			if (this.ordem.equals("IP"))    nomOrdem = "IP, USU�RIO, SISTEMA, DATA DE ENTRADA, FUNC. EM USO, ORG�O ATUA��O" ;
			if (this.ordem.equals("SISTEMA"))    nomOrdem = "SISTEMA, USU�RIO, DATA DE ENTRADA, IP, FUNC. EM USO, ORG�O ATUA��O" ;
			if (this.ordem.equals("SIGFUNCAO"))    nomOrdem = "FUNC. EM USO, USU�RIO, SISTEMA, DATA DE ENTRADA, IP, FUNC. EM USO, ORG�O ATUA��O" ;
			if (this.ordem.equals("ORGAO_ATUA"))    nomOrdem = "ORG�O ATUA��O, USU�RIO, SISTEMA, DATA DE ENTRADA, IP, FUNC. EM USO" ;
			return nomOrdem+ " ("+getOrdClass()+")" ;
	  }
// --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
			if (ordemSol.equals("NOM_USERNAME"))  ord = 1 ;   
			if (ordemSol.equals("DAT_SESSAO"))  ord = 2 ;   
			if (ordemSol.equals("IP")) ord = 3 ; 
			if (ordemSol.equals("SISTEMA")) ord = 4 ; 
			if (ordemSol.equals("SIGFUNCAO")) ord = 5 ; 
			if (ordemSol.equals("ORGAO_ATUA")) ord = 6 ; 
			
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			if (ordemSol.equals(getOrdem()))   {
				if ( getOrdClass().equals("ascendente")) {
					trocaasc = false ;
					trocades = true ;
					setOrdClass("descendente"); 
				} 
				else {
					trocaasc = true ;
					trocades = false;
					setOrdClass("ascendente"); 
				}
			}
			else 
				setOrdClass("ascendente");
			setOrdem(ordemSol) ;
			int tam = getDados().size() ;
			ConsultaUsuarioLogadoBean tmp = new ConsultaUsuarioLogadoBean();
			String nom_username1, nom_username2, data_sessao1, data_sessao2, ip1, ip2, sistema1, sistema2, orgAtu1, orgAtu2, sigFuncao1, sigFuncao2;  
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					// "USU�RIO, SISTEMA, DATA DE ENTRADA, IP, FUNC. EM USO, ORG�O ATUA��O" ;
					case 0:
						nom_username1 = getDados(i).getNomUserName();
						nom_username2 = getDados(j).getNomUserName();
						data_sessao1 = getDados(i).getdataSessao();
						data_sessao2 = getDados(j).getdataSessao();
						ip1 = getDados(i).getIp();
						ip2 = getDados(j).getIp();
						sistema1 = getDados(i).getSistema();
						sistema2 = getDados(j).getSistema();
						orgAtu1 = getDados(i).getSigOrgaoAtuacao() ;
						orgAtu2 = getDados(j).getSigOrgaoAtuacao();
						sigFuncao1 = getDados(i).getSigFuncao() ;
						sigFuncao2 = getDados(j).getSigFuncao();
						
						
						if ((nom_username1+sistema1+data_sessao1+ip1).compareTo(nom_username2+sistema1+data_sessao2+ip2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "USU�RIO" ;
					case 1:
						if(getDados(i).getNomUserName().compareTo(getDados(j).getNomUserName())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
					//"USU�RIO" ;   
					case 2:
						if(getDados(i).getdataSessao().compareTo(getDados(j).getdataSessao())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
					// "DATA DE ENTRADA DO SISTEMA" ;   
					case 3:
						if (getDados(i).getIp().compareTo(getDados(j).getIp())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "IP" ;
					case 4:
						if (getDados(i).getSistema().compareTo(getDados(j).getSistema())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "SISTEMA" ; 	
					case 5:
						if (getDados(i).getSigFuncao().compareTo(getDados(j).getSigFuncao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "SIGFUNCAO" ;
					case 6:
						if (getDados(i).getSigOrgaoAtuacao().compareTo(getDados(j).getSigOrgaoAtuacao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "ORGAO_ATUA" ; 	
				}	
				if (troca) 	{		
				   tmp = (ConsultaUsuarioLogadoBean)getDados(i);
				   getDados().set(i,getDados(j));
				   getDados().set(j,tmp);
				}
			}
		}
	  }	


}