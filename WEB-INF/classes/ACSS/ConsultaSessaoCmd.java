package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Consulta Sess�o<br>
* <b>Description:</b>  verifica todas as sess�os que est�o ativas<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ConsultaSessaoCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ConsultaSessao.jsp";
	private String next;

	public ConsultaSessaoCmd() {
		next = jspPadrao;
	}

	public ConsultaSessaoCmd(String next) {
		this.next = next;
	}
  
  //Esta assinatura do m�todo execute s� funciona para o m�dulo GER
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaSessao(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaSessaoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = consultaSessao(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaSessaoCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
	
	public String consultaSessao(HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			// cria os Beans, se n�o existir

			ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	
			
			OrgaoBean orgaoBeanId = (OrgaoBean)req.getAttribute("orgaoBeanId") ;
			if (orgaoBeanId==null) orgaoBeanId = new OrgaoBean() ;	  			
 
			ConsultaSessaoBean consultaSessaoBean =(ConsultaSessaoBean) req.getAttribute("consultaSessaoBean");
			if (consultaSessaoBean == null) consultaSessaoBean = new ConsultaSessaoBean();


			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "0";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";
			
			String datIni = req.getParameter("De");
			if (datIni == null) datIni = "";
			
			String datFim = req.getParameter("Ate");
			if (datFim == null) datFim = "";
			
			String terminoAnormal = req.getParameter("termAnormal");
			if (terminoAnormal == null) terminoAnormal = "0";
			
			String termino = req.getParameter("terminoAnormal");
			if (termino == null) termino = "0";
			
			String verTable = req.getParameter("verTable");
			if (verTable == null) verTable = "N";
			
			String ordClass = req.getParameter("ordClass");
			if (ordClass == null) ordClass = "descendente";
			
			String ordem = req.getParameter("ordem");

			consultaSessaoBean.setCodOrgaoAtuacao(codOrgao);
			consultaSessaoBean.setCodSistema(codSistema);
			consultaSessaoBean.setDataIni(datIni);
			consultaSessaoBean.setDataFim(datFim);
			consultaSessaoBean.setTermAnormal(terminoAnormal);
			consultaSessaoBean.setOrdClass(ordClass);
			consultaSessaoBean.setOrdem(ordem);
							
			if (acao.equals("")){
				consultaSessaoBean = new ConsultaSessaoBean();				
				String dataHoje = sys.Util.getDataHoje();
				verTable = "N";
				dataHoje = dataHoje.replaceAll("-","/");  
				consultaSessaoBean.setDataIni(dataHoje);
				consultaSessaoBean.setDataFim(dataHoje);				
			}		
			
			if (acao.equals("consultSessoesAtivas")){ 
				verTable = "S";	
				SistBeanId.Le_Sistema(codSistema,0) ;
				orgaoBeanId.Le_Orgao(codOrgao,0);
				consultaSessaoBean.consultaSessoesAtivas(consultaSessaoBean);
				consultaSessaoBean.Classifica(ordem);
			}
			
			if (acao.equals("Classifica")) {
				SistBeanId.Le_Sistema(codSistema,0) ;
				orgaoBeanId.Le_Orgao(codOrgao,0);
				consultaSessaoBean.setTermAnormal(termino);
				consultaSessaoBean.consultaSessoesAtivas(consultaSessaoBean);		
				consultaSessaoBean.Classifica(ordem);
				verTable = "S";	
			}
			
			if (acao.equals("ImprimeResultConsulta")) {
				//Titulo do Relatorio de impressao
				String tituloConsulta = "CONSULTA HIST�RICO DE SESS�ES";
				String msg = "";
				req.setAttribute("tituloConsulta", tituloConsulta);
				
				consultaSessaoBean.setTermAnormal(termino);
				consultaSessaoBean.consultaSessoesAtivas(consultaSessaoBean);
				
				if(consultaSessaoBean.getDados().size() == 0)
					req.setAttribute("msg", "N�o existem informa��es");
				else
				{
					if(consultaSessaoBean.getOrdClass().equals("ascendente"))
						consultaSessaoBean.setOrdClass("descendente");
					else
						consultaSessaoBean.setOrdClass("ascendente");
					
					consultaSessaoBean.Classifica(ordem);
					consultaSessaoBean.setDados(consultaSessaoBean.getDados());
				}
				
				nextRetorno="/ACSS/ConsultaSessaoImp.jsp";
			}
			
		if (acao.equals("V")) {
				verTable = "N";
			}

			req.setAttribute("verTable", verTable);
			req.setAttribute("SistBeanId", SistBeanId);
			session.setAttribute("UsrBeanId", UsrBeanId);
			req.setAttribute("orgaoBeanId", orgaoBeanId);
			req.setAttribute("consultaSessaoBean",consultaSessaoBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaSessaoCmd 001: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}