package ACSS;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import sys.CommandException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/


public class InformacoesCmd extends sys.Command 
{
	private static final String jspPadrao = "/ACSS/Informacoes.jsp";
	private String next;
	public InformacoesCmd() { next = jspPadrao; }
	public InformacoesCmd(String next) { this.next = next; }

	private String 	status="0";
	

	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = jspPadrao ;
	  try {      	
			nextRetorno = informacoes(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("InformacoesCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}


	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = jspPadrao;        
		try {
			nextRetorno = informacoes(req,nextRetorno);
		} 
		catch (Exception e) {
			throw new sys.CommandException("InformacoesCmd: " + e.getMessage());
		}		
		return nextRetorno;
	}
	
	public String informacoes(HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
			UsuarioBean UsuarioBeanId = (UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
			UsuarioFuncBean UsuarioFuncBeanId = (UsuarioFuncBean) req.getSession().getAttribute("UsuarioFuncBeanId");

			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			else acao = acao.trim();
			//-------------------------
			String acaoNA = req.getParameter("acaoNA");
				if( acaoNA == null) acaoNA ="";
				else
				if( acaoNA.equals("") ) acaoNA ="";
				else acao = acaoNA.trim();
			//-------------------------
			if ( acao.equals("") ) 
			{
				NoticiaBean NoticiaBeanId = new NoticiaBean();
				NoticiaBeanId.carregaNaoLidas(UsuarioBeanId);
                
				AvisoBean AvisoBeanId = new AvisoBean();
				AvisoBeanId.carregaNaoLidos(UsuarioBeanId, UsuarioFuncBeanId);
                
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
				req.setAttribute("AvisoBeanId", AvisoBeanId);
			}
			else if(acao.equals("histAviso")) 
			{
				String datIniNot = req.getParameter("datInicioAviso");
				String datFimNot = req.getParameter("datFimAviso");

				AvisoBean AvisoBeanId = new AvisoBean();
				
				if( (datIniNot==null || datFimNot == null)||(datIniNot.equals("")||datFimNot.equals("")) )
				{ 
					datFimNot = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime(new Date());
					cal.add(Calendar.MONTH, -2);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + ""; 
					String ano = cal.get(Calendar.YEAR) + "";
					datIniNot = dia +"/"+ mes +"/"+ ano;     
				}  
				
				AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);

				String codigo="";
				for(int i=0; i<AvisoBeanId.getAvisos().length; i++)
					codigo = String.valueOf(AvisoBeanId.getAvisos()[i].getCodAviso());

				if (!codigo.equals("")) {
					int codAviso = Integer.valueOf(codigo).intValue();                                
					AvisoBeanId = AvisoBean.consulta(codAviso);
					AvisoBeanId.le(UsuarioBeanId);

					AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);
				}
				else { AvisoBeanId.setUsuario(UsuarioBeanId);	}

				AvisoBeanId.setDatFimAviso(datFimNot);
				AvisoBeanId.setDatInicioAviso(datIniNot);
				
				nextRetorno = "/ACSS/InformacoesAvisoHist.jsp";
				req.setAttribute("AvisoBeanId", AvisoBeanId);
			}
			else if( acao.equals("histNoticia") )
			{
				String datInicioNoticia = req.getParameter("datInicioNoticia");
				String datFimNoticia = req.getParameter("datFimNoticia");
                 
				if( (datInicioNoticia==null || datFimNoticia == null)||(datInicioNoticia.equals("")||datFimNoticia.equals("")) )
				{ 
					datFimNoticia = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime(new Date());
					cal.add(Calendar.MONTH, -2);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
					String ano = cal.get(Calendar.YEAR) + "";
					datInicioNoticia = dia +"/"+ mes +"/"+ ano;               
				}   

				NoticiaBean NoticiaBeanId = new NoticiaBean();
				NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datInicioNoticia,datFimNoticia);

				String codigo="";
				for(int i=0; i<NoticiaBeanId.getNoticias().length; i++)
					codigo = String.valueOf(NoticiaBeanId.getNoticias()[i].getCodNoticia());

				if (!codigo.equals("")) {
					int codNoticia = Integer.valueOf(codigo).intValue();                                
					NoticiaBeanId = NoticiaBean.consulta(codNoticia);
					NoticiaBeanId.le(UsuarioBeanId);
			
					NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datInicioNoticia,datFimNoticia );
				} 
				else {  NoticiaBeanId.setUsuario(UsuarioBeanId); }
				
				NoticiaBeanId.setDatFimNoticia( datFimNoticia );
				NoticiaBeanId.setDatInicioNoticia( datInicioNoticia );
			
				nextRetorno = "/ACSS/InformacoesNoticiaHist.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if( acao.equals("mostraNoticiaHist") ) 
			{                
						String codigo = req.getParameter("codigo");
						if (codigo == null) codigo = "";
            	
						String codigo_cab = req.getParameter("codigo_cab");
						if ( (codigo_cab == null)||(codigo_cab.compareTo("")==0)) codigo_cab = "";
						else  codigo = codigo_cab;
				
						NoticiaBean NoticiaBeanId = new NoticiaBean();
			
						String datInicioNoticia = req.getParameter("datInicioNoticia");
						String datFimNoticia    = req.getParameter("datFimNoticia");
                 
						if( (datInicioNoticia==null || datFimNoticia == null)||(datInicioNoticia.equals("")||datFimNoticia.equals("")) )
						{ 
							datFimNoticia = sys.Util.formatedToday().substring(0,10); 
							Calendar cal = new GregorianCalendar();
							cal.setTime(new Date());
							cal.add(Calendar.MONTH, -2);
							Date date = cal.getTime();
							cal.setTime(date);
							String dia = cal.get(Calendar.DAY_OF_MONTH) + "";
							String mes = cal.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
							String ano = cal.get(Calendar.YEAR) + "";
							datInicioNoticia = dia +"/"+ mes +"/"+ ano;               
						}
						NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datInicioNoticia,datFimNoticia);

						if (!codigo.equals("")) {
							int codNoticia = Integer.valueOf(codigo).intValue();                                
							NoticiaBeanId = NoticiaBean.consulta(codNoticia);
							NoticiaBeanId.le(UsuarioBeanId);
							NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datInicioNoticia,datFimNoticia );
						} 
						else {  NoticiaBeanId.setUsuario(UsuarioBeanId); }

						NoticiaBeanId.setDatFimNoticia( datFimNoticia );
						NoticiaBeanId.setDatInicioNoticia( datInicioNoticia );

						nextRetorno = "/ACSS/InformacoesNoticiaHist.jsp";
						req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if( acao.equals("mostraAvisoHist") ) 
			{
					String codigo = req.getParameter("codigo");
					if (codigo == null) codigo = "";
	
					String codigo_cab = req.getParameter("codigo_cab");
					if ( (codigo_cab == null)||(codigo_cab.compareTo("")==0)) codigo_cab = "";
					else codigo = codigo_cab;
				
					AvisoBean AvisoBeanId = new AvisoBean();
	
					String datIniNot = req.getParameter("datInicioAviso");
					String datFimNot = req.getParameter("datFimAviso");

					if( (datIniNot==null || datFimNot == null)||(datIniNot.equals("")||datFimNot.equals("")) )
					{ 
						datFimNot = sys.Util.formatedToday().substring(0,10);
						Calendar cal = new GregorianCalendar();
						cal.setTime(new Date());
						cal.add(Calendar.MONTH, -2);
						Date date = cal.getTime();
						cal.setTime(date);
						String dia = cal.get(Calendar.DAY_OF_MONTH) + "";
						String mes = cal.get(Calendar.MONTH)+ 1 + ""; 
						String ano = cal.get(Calendar.YEAR) + "";
						datIniNot = dia +"/"+ mes +"/"+ ano;               
					}   
					AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);

					if (!codigo.equals("")) {
						int codAviso = Integer.valueOf(codigo).intValue();                                
						AvisoBeanId = AvisoBean.consulta(codAviso);
						AvisoBeanId.le(UsuarioBeanId);
						AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);
					}
					else { AvisoBeanId.setUsuario(UsuarioBeanId);	}


					AvisoBeanId.setDatFimAviso( datFimNot );
					AvisoBeanId.setDatInicioAviso( datIniNot );
	
					nextRetorno = "/ACSS/InformacoesAvisoHist.jsp";
					req.setAttribute("AvisoBeanId", AvisoBeanId);
			}
			else if( acao.equals("mostraNoticia") ) 
			{                
				String codigo = req.getParameter("codigo");
				if (codigo == null) codigo = "";
				//------------------------------
				String codigo_cab = req.getParameter("codigo_cab");
				if ( (codigo_cab == null)||(codigo_cab.compareTo("")==0)) codigo_cab = "";
				else  codigo = codigo_cab;
				//------------------------------
				NoticiaBean NoticiaBeanId = new NoticiaBean();
				if (!codigo.equals("")) {
					int codNoticia = Integer.valueOf(codigo).intValue();                                
					NoticiaBeanId = NoticiaBean.consulta(codNoticia);
					NoticiaBeanId.le(UsuarioBeanId);
				}
				NoticiaBeanId.carrega(UsuarioBeanId);
                
				nextRetorno = "/ACSS/InformacoesNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
                
			} 
			else if(acao.equals("Avisos"))
			{
				String datIniNot = req.getParameter("datInicioAviso");
				String datFimNot = req.getParameter("datFimAviso");

				AvisoBean AvisoBeanId = new AvisoBean();
				AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);

				String codigo="";
				for(int i=0; i<AvisoBeanId.getAvisos().length; i++)
					codigo = String.valueOf(AvisoBeanId.getAvisos()[i].getCodAviso());

				if (!codigo.equals("")) {
					int codAviso = Integer.valueOf(codigo).intValue();                                
					AvisoBeanId = AvisoBean.consulta(codAviso);
					AvisoBeanId.le(UsuarioBeanId);
				
//					AvisoBeanId.carrega(UsuarioBeanId);
					AvisoBeanId.MostraAvisosEntreDatas(UsuarioBeanId,datIniNot,datFimNot);
				}else{
					  AvisoBeanId.setUsuario(UsuarioBeanId);
				}
				nextRetorno = "/ACSS/InformacoesAviso.jsp";
				req.setAttribute("AvisoBeanId", AvisoBeanId);
            
			}
			else if(acao.equals("Noticias") )
			{
				String datIniNot = req.getParameter("datInicioNoticia");
				String datFimNot = req.getParameter("datFimNoticia");

				NoticiaBean NoticiaBeanId = new NoticiaBean();
				NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datIniNot,datFimNot );

				String codigo="";
				for(int i=0; i<NoticiaBeanId.getNoticias().length; i++)
					codigo = String.valueOf(NoticiaBeanId.getNoticias()[i].getCodNoticia());

				if (!codigo.equals("")) {
					int codNoticia = Integer.valueOf(codigo).intValue();                                
					NoticiaBeanId = NoticiaBean.consulta(codNoticia);
					NoticiaBeanId.le(UsuarioBeanId);
				
					NoticiaBeanId.carrega(UsuarioBeanId);
					NoticiaBeanId.MostraNoticiasEntreDatas(UsuarioBeanId,datIniNot,datFimNot );
				}else{
					  NoticiaBeanId.setUsuario(UsuarioBeanId);				
				}
				nextRetorno = "/ACSS/InformacoesNoticia.jsp";
				req.setAttribute("NoticiaBeanId", NoticiaBeanId);
			}
			else if (acao.equals("mostraAviso")) {
				String codigo = req.getParameter("codigo");
				if (codigo == null) codigo = "";
				//-----------------------------
				String codigo_cab = req.getParameter("codigo_cab");
				if ( (codigo_cab == null)||(codigo_cab.compareTo("")==0)) codigo_cab = "";
				else codigo = codigo_cab;
				//-----------------------------
				AvisoBean AvisoBeanId = new AvisoBean();
				if (!codigo.equals("")) {
					int codAviso = Integer.valueOf(codigo).intValue();
					AvisoBeanId = AvisoBean.consulta(codAviso);
					AvisoBeanId.le(UsuarioBeanId);
				}
				AvisoBeanId.carrega(UsuarioBeanId, UsuarioFuncBeanId);
                
				nextRetorno = "/ACSS/InformacoesAviso.jsp";
				req.setAttribute("AvisoBeanId", AvisoBeanId);
                
			} 
			else if (acao.equals("enviaEmail")) 
			{
				try {
					String nome      = req.getParameter("nome");
					String email     = req.getParameter("mandante");
					String emailDest = req.getParameter("destinatario");
					
					String assunto   = req.getParameter("assunto");	if( assunto == null ) assunto = "";
					String mensagem  = req.getParameter("comment"); if( mensagem == null ) mensagem = "";
	                
					String mensagemFinal = "Mensagem :\n" + mensagem;
	
					sys.EmailBean EmailBeanId = new sys.EmailBean();
					EmailBeanId.setDestinatario( emailDest.toLowerCase() );
					EmailBeanId.setRemetente(nome +"<"+ email +">" );
					EmailBeanId.setAssunto( assunto );
					EmailBeanId.setConteudoEmail( mensagemFinal );
	                
					EmailBeanId.sendEmail();
					status="1";
					
					req.setAttribute("status",status);
					req.setAttribute("emailEnviado", new Boolean(true));
					nextRetorno = "/ACSS/InformacoesEmail.jsp";
				}
				catch(Exception e){
					status="2";
					throw new sys.CommandException("InformacoesCmd: " + e.getMessage()); }
			}
			else if (acao.equals("faleConosco")) { nextRetorno = "/ACSS/InformacoesEmail.jsp";  }
            
		} catch (Exception e) {
			throw new sys.CommandException("InformacoesCmd: " + e.getMessage());
		}		
		return nextRetorno;
	}
}