package ACSS;

import java.util.Vector;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades<br>
* <b>Description:</b>  Bean com Funcionalidades dos Usuarios - Tabela de Funcionalidades por Usuarios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/


public class UsuarioFuncBean  extends sys.HtmlPopupBean{ 

  private String codUsuario;
  private String codSistema;
  private String abrevSistema;  
  private String nomSistema;  
  private String nomPerfil;
  private String codPerfil;  
  private String codNivel;  
  private String indVisEndereco;
  private String indRecebNot;
  private String indVis2via;  
  private String indVisAr;
  private String indVisAuto;
  private String indVisFoto;
  private String indVisHist;  
  private String indVisImpr;
  private String indVisRej;  
  private String indVisReq;  
  private String indVisTodosOrg;  
    
  private String j_sigFuncao;    
  private String j_nomFuncao;
  private String j_sitBiometria;
  private boolean j_jaAutenticouBiometria;
  private String j_nomFuncaoPai;                             
  private String j_cmdFuncao;        
  private String j_jspOrigem;   
  private String j_token;           
  private String j_abrevSist;
  private Vector funcoes;  	    

  private String codOrgaoAtuacao;
  
  
  private String msgErro ;  
    
  public UsuarioFuncBean()  {

    super();

    codUsuario       = "" ;	
    codSistema       = "" ;
    abrevSistema     = "" ;  
	nomSistema       = "" ;
    nomPerfil        = "" ;
    codPerfil        = "" ; 
    codNivel         = "0"; 

    indVisEndereco   = "0";
    indVis2via       = "0"; 
	indRecebNot      = "0";
    indVisAr         = "0";
    indVisAuto       = "0";
    indVisFoto       = "0";
    indVisHist       = "0";  
    indVisImpr       = "0";
    indVisRej        = "0";  
    indVisReq        = "0";  
    indVisTodosOrg   = "0";  

	j_sigFuncao      = "" ;	 
	j_nomFuncao      = "" ;
	j_sitBiometria   = "I";
	j_nomFuncaoPai   = "" ;	 	  		  
	j_cmdFuncao      = "login" ;
	j_jspOrigem      = "" ;
	j_token          = "" ;    
    j_abrevSist      = "ACSS"; 
	funcoes          = new Vector() ;
	j_jaAutenticouBiometria = false;

    codOrgaoAtuacao       = "" ;	

    msgErro          = "" ;	

  }
  
  public void setCodUsuario(String codUsuario)  {
	this.codUsuario = codUsuario ;
    if (this.codUsuario==null) this.codUsuario="0" ;
  }  
  public String getCodUsuario()  {
  	return this.codUsuario;
  } 
  public void setCodSistema(String codSistema)  {
	this.codSistema = codSistema ;
	if (this.codSistema==null) this.codSistema="0" ;  
  }
  public String getCodSistema()  {
  	return this.codSistema;
  }
  public void setAbrevSistema(String abrevSistema)  {
	this.abrevSistema = abrevSistema ;
	if (this.abrevSistema==null) this.abrevSistema="" ;    
  }
  public String getAbrevSistema()  {
  	return this.abrevSistema;
  }
  public void setNomSistema(String nomSistema)  {
	this.nomSistema = nomSistema ;
	if (this.nomSistema==null) this.nomSistema="" ;    
  }
  public String getNomSistema()  {
  	return this.nomSistema;
  }
  
  public void setNomPerfil(String nomPerfil)  {
	this.nomPerfil = nomPerfil ;
	if (this.nomPerfil==null) this.nomPerfil="" ;        
  }
  public String getNomPerfil()  {
  	return this.nomPerfil;	
  }
  public void setCodPerfil(String codPerfil)  {
	this.codPerfil = codPerfil ;
	if (this.codPerfil==null) this.codPerfil="0" ;      
  }
  public String getCodPerfil()  {
  	return this.codPerfil;
  }
  public void setIndRecebNot(String indRecebNot)  {
  	this.indRecebNot = indRecebNot ;	
	if (indRecebNot==null) indRecebNot="0" ;      
  }
  public String getIndRecebNot()  {
  	return this.indRecebNot;
  }
  public void setIndVisEndereco(String indVisEndereco)  {
  this.indVisEndereco = indVisEndereco ;
  if (this.indVisEndereco==null) this.indVisEndereco="0" ;      
  }
  public String getIndVisEndereco()  {
  	return this.indVisEndereco;
  }
  public void setIndVis2via(String indVis2via)  {
  this.indVis2via = indVis2via ;
  if (this.indVis2via==null) this.indVis2via="0" ;      
  }
  public String getIndVis2via()  {
  	return this.indVis2via;
  }
  public void setIndVisAr(String indVisAr)  {
  this.indVisAr = indVisAr ;
  if (this.indVisAr==null) this.indVisAr="0" ;      
  }
  public String getIndVisAr()  {
  	return this.indVisAr;
  }
  public void setIndVisAuto(String indVisAuto)  {
  this.indVisAuto = indVisAuto ;
  if (this.indVisAuto==null) this.indVisAuto="0" ;      
  }
  public String getIndVisAuto()  {
  	return this.indVisAuto;
  }
  public void setIndVisFoto(String indVisFoto)  {
  this.indVisFoto = indVisFoto ;
  if (this.indVisFoto==null) this.indVisFoto="0" ;      
  }
  public String getIndVisFoto()  {
    return this.indVisFoto;
  }  
  public void setIndVisHist(String indVisHist)  {
  this.indVisHist = indVisHist ;
  if (this.indVisHist==null) this.indVisHist="0" ;      
  }
  public String getIndVisHist()  {
  	return this.indVisHist;
  }
  public void setIndVisImpr(String indVisImpr)  {
  this.indVisImpr = indVisImpr ;
  if (this.indVisImpr==null) this.indVisImpr="0" ;      
  }
  public String getIndVisImpr()  {
  	return this.indVisImpr;
  }
  public void setIndVisRej(String indVisRej)  {
 	 this.indVisRej = indVisRej ;
  if (this.indVisRej==null) this.indVisRej="0" ;      
  }
  public String getIndVisRej()  {
  	return this.indVisRej;
  }
  public void setIndVisReq(String indVisReq)  {
   this.indVisReq = indVisReq ;
  if (this.indVisReq==null) this.indVisReq="0" ;      
  }
  public String getIndVisReq()  {
  	return this.indVisReq;
  }
  public void setIndVisTodosOrg(String indVisTodosOrg)  {
  this.indVisTodosOrg = indVisTodosOrg ;
  if (this.indVisTodosOrg==null) this.indVisTodosOrg="0" ;      
  }
  public String getIndVisTodosOrg()  {
  	return this.indVisTodosOrg;
  }
  
  public void setJ_sigFuncao(String sigFuncao)  {
	    this.j_sigFuncao = sigFuncao ;
	    
	    /*Guia de Distribui��o Manual / � Partir de Guia de Remessa*/
	    if ( (this.j_sigFuncao.equals("REC0249")) || (this.j_sigFuncao.equals("REC0352")) )
	    	return;
	    
	    
		this.j_nomFuncao="";
		for (int i=0;i<funcoes.size();i++)		{
			OperacaoBean myOper = (OperacaoBean)funcoes.elementAt(i) ;
			if (j_sigFuncao.equals(myOper.getSigFuncao())) 		{
				this.j_nomFuncao=myOper.getNomOperacao() ;
				this.j_sitBiometria=myOper.getSitBiometria();
				this.j_nomFuncaoPai=myOper.getNomFuncaoPai() ;				
				break;
			}
		}	
  }
  public String getJ_sigFuncao()  {
  	return this.j_sigFuncao;
  }
  public String getJ_nomFuncao()  {
  	return this.j_nomFuncao;
  }
  public String getJ_nomFuncaoPai()  {
  	return this.j_nomFuncaoPai;
  }
  public void setJ_nomFuncaoPai(String j_nomFuncaoPai)  {
  	this.j_nomFuncaoPai = j_nomFuncaoPai;
  }
  
  public void setJ_cmdFuncao(String cmdFuncao)  {
  this.j_cmdFuncao = cmdFuncao ;
  }
  public String getJ_cmdFuncao()  {
  	return this.j_cmdFuncao;
  }

  public void setJ_jspOrigem(String j_jspOrigem)  {
  this.j_jspOrigem = j_jspOrigem ;
  }
  public String getJ_jspOrigem()  {
  	return this.j_jspOrigem;
  }  
  
  public void setJ_token(String j_token)  {
  this.j_token = j_token ;
  }
  public String getJ_token()  {
  	return this.j_token;
  }
  public void setCodNivel(String codNivel)  {
  this.codNivel = codNivel ;
  }
  public String getCodNivel()  {
  	return this.codNivel;
  }

  //--------------------------------------------------------------------------

  public void setJ_abrevSist(String j_abrevSist)  {
  this.j_abrevSist = j_abrevSist ;
  }
  public String getJ_abrevSist()  {
  	return this.j_abrevSist;
  }

  //--------------------------------------------------------------------------
  public void setFuncoes(Vector funcoes)  {
  	this.funcoes = funcoes ;
  }
  //--------------------------------------------------------------------------
  public Vector getFuncoes()  {
     return this.funcoes;
  }
  //--------------------------------------------------------------------------
  public Vector getFuncoes(UsuarioFuncBean myUsrFunc)  {
     try  {
        Dao dao = Dao.getInstance();
        dao.UsuarioFuncGetFuncoes(myUsrFunc);
     }
     catch (Exception e) {
          this.msgErro = "UsuarioFunc006 - Leitura: " + e.getMessage();
     }//fim do catch
   return this.funcoes ;
  }
  
  public OperacaoBean getFuncoes(int i)  { return (OperacaoBean)this.funcoes.elementAt(i); }
  
public void setMsgErro(Vector vErro)   {
	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
	   }
   }
public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
   }   
public String getMsgErro()   {
   	return this.msgErro;
   }      
public void Le_PerfilUsuario(UsuarioBean myUsr,String abrevSistema)  {
	  try {
		  	Dao dao = Dao.getInstance();		
		  	if (dao.UsuarioFuncLePerfilUsuario(this,myUsr,abrevSistema)==false) this.codPerfil = "0" ;								
	  } // fim do try
	  catch (Exception e) { 
	     	this.codPerfil = "0" ;			
	  }	// fim do catch 
      return;	       	   	    	       	   
}
public int isFuncaoUsuario(String sigFuncao)  {
	/* procura a funcao solicitada*/
	int i = -1 ;
	for (int j=0; j<funcoes.size(); j++)   {
	   if (getFuncoes(j).getSigFuncao().equals(sigFuncao)) {
			i = j ;
			break ;
	   }
	}
	return i ;
}

public void setJsitBiometria(String j_sitBiometria)  {
  this.j_sitBiometria = j_sitBiometria ;
}
public String getJsitBiometria()  {
  return this.j_sitBiometria;
}

public void setJaAutenticouBiometria(boolean j_jaAutenticouBiometria)  {
  this.j_jaAutenticouBiometria = j_jaAutenticouBiometria ;
}
public boolean getJaAutenticouBiometria()  {
  return this.j_jaAutenticouBiometria;
}

public void setCodOrgaoAtuacao(String codOrgaoAtuacao)  {
	this.codOrgaoAtuacao = codOrgaoAtuacao ;
    if (this.codOrgaoAtuacao==null) this.codOrgaoAtuacao="" ;
  }  
  public String getCodOrgaoAtuacao()  {
  	return this.codOrgaoAtuacao;
  } 


}
