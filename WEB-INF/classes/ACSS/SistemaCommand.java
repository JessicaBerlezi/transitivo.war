package ACSS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Sistemas<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Sistemas<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class SistemaCommand extends sys.Command {

    private static final String jspPadrao="/ACSS/Sistema.jsp";        
  private String next;


  public SistemaCommand() {
    next             = jspPadrao ;
  }

  public SistemaCommand(String next) {
    this.next = next;
  }

//--------------------------------------------------------------------------
public String execute(HttpServletRequest req) throws sys.CommandException  {
    String nextRetorno  = jspPadrao ;
    try {							
           	// cria os Beans do Sistema, se n�o existir
	        ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
	        if( SistId == null ) SistId = new ACSS.SistemaBean();
			//obtem e valida os parametros recebidos
            String acao = req.getParameter("acao");
            if( acao == null) acao =" ";
            String pkid = req.getParameter("pkid");
            if(pkid==null)       pkid  ="0";
            String codSistema = req.getParameter("codSistema");
            if( codSistema == null ) codSistema = "";
            String sigVersao = req.getParameter("sigVersao");
            if( sigVersao == null )  sigVersao = "";
            String nomSistema = req.getParameter("nomSistema");
            if( nomSistema == null )  nomSistema = " ";
            String nomAbrev    = req.getParameter("nomAbrev");
            if( nomAbrev == null )  nomAbrev = "";

			// Preencher o Bean			
			SistId.setPkid(pkid);
			SistId.setCodSistema(pkid);
			SistId.setSigVersao(sigVersao);
			SistId.setNomSistema(nomSistema);
			SistId.setNomAbrev(nomAbrev);
			SistId.setMsgErro("");
			if ("SistemasConsultaCommand".indexOf(acao) >= 0) return processaSistemasConsulta(req,nextRetorno,SistId) ;

            if ("Top,Proximo,Anterior,Fim,Pkid".indexOf(acao) >= 0)  {
                if ( "Pkid".indexOf(acao) >= 0 )  SistId.buscaCodSistema ( pkid, acao );
                else		                      SistId.buscaCodSistema( nomSistema, acao );
       		}					
            if( "Novo".indexOf(acao) >= 0 )   {
				// objetivo e limpar o Bean
                SistId.setPkid("0");
                SistId.setCodSistema("0");	
                SistId.setNomSistema("0");				
                SistId.setNomAbrev("");				
                SistId.setSigVersao("0");																			
      		}
			
            if( "123".indexOf(acao) >= 0 )  {
             if(acao.compareTo("1")==0) SistId.isInsert();
             if(acao.compareTo("2")==0) SistId.isAltera();
             if(acao.compareTo("3")==0) SistId.isDelete();
		 }
		 req.setAttribute("SistId",SistId) ;
    }
    catch (Exception ue) {
      throw new sys.CommandException("SistemaCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
 } 

 private String processaSistemasConsulta(HttpServletRequest req,String nextRetorno,ACSS.SistemaBean SistId) throws sys.CommandException	{
 	HttpSession session   = req.getSession() ;
 	String acaoConsulta = req.getParameter("acaoConsulta"); 
 	if (acaoConsulta==null) acaoConsulta="" ;
 	// inicio da consulta
     if (acaoConsulta.equals("")) {	
 		// colocar os Beans a serem preservados na sessao
 		session.setAttribute("SistId",SistId) ;	
 		// Nome para guardar o Bean de Consultana sessao
 		session.setAttribute("nomeBeanConsulta","ConsultaUsrBeanId") ;						
     }
 	// fim da consuta - fica neste comando
     if ((acaoConsulta.equals("pkid")) || (acaoConsulta.equals("Retornar"))) {									
 	
 		//restaurar da  sessao os Bean preservados
 		String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");			
 		session.removeAttribute(nomeBeanConsulta); 						
 		session.removeAttribute("nomeBeanConsulta"); 			

 		// recupera o(s) Bean(s) na sessao 
 		SistId = (ACSS.SistemaBean)session.getAttribute("SistId") ;		
 		if (SistId==null) SistId = new ACSS.SistemaBean() ;
 		// remove o Bean da sessao
 		session.removeAttribute("SistId"); 				
 		
 		//setar os Beans desejados				
 		if (acaoConsulta.equals("pkid")==true) 	{
 			String pkidConsulta = req.getParameter("pkidConsulta"); 		
 			if (pkidConsulta==null) pkidConsulta="" ;
 			SistId.buscaCodSistema(pkidConsulta,"Pkid") ;			
 			req.setAttribute("SistId",SistId) ;					        						
 		}
     }
 	else 		{
 		try 	{
 			// executar o comando de Consulta
 			sys.Command cmd = (sys.Command)Class.forName("ACSS.SistemasConsultaCommand").newInstance() ;							
 			nextRetorno = cmd.execute(req);		
 		}
 		catch (Exception e){
 		}			
 	}
 
 	return nextRetorno ;
 }


}

