package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - consulta Funcao do Perfil Cmd<br>
* <b>Description:</b>  Traz as fun��es do sistema de acordo com o perfil<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/


public class ConsultaFuncoesPerfilCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ConsultaFuncoesPerfil.jsp";
	private String next;

	public ConsultaFuncoesPerfilCmd() {
		next = jspPadrao;
	}

	public ConsultaFuncoesPerfilCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaFuncPerfil(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaFuncoesPerfilCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = jspPadrao;
		try {
			nextRetorno = consultaFuncPerfil(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaFuncoesPerfilCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String consultaFuncPerfil(HttpServletRequest req,String nextRetorno) throws CommandException{
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			UsuarioBean UsrLogado = (UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new UsuarioBean();
			
			// cria os Beans de Usuario e Perfil, se n�o existir
			OperacaoBean OperacaoBeanId = (OperacaoBean) req.getAttribute("OperacaoBeanId");
			if (OperacaoBeanId == null) OperacaoBeanId = new OperacaoBean();
			
			UsuarioBean UsrBeanId =(UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new UsuarioBean();
			
			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;
			
			PerfilBean PerfBeanId = (PerfilBean)req.getAttribute("PerfBeanId") ;
			if (PerfBeanId==null) PerfBeanId = new PerfilBean() ;	  				
			
			ConsultaFuncoesPerfilBean ConsultaFuncoesPerfilBeanId =(ConsultaFuncoesPerfilBean) session.getAttribute("ConsultaFuncoesPerfilBeanId");
			if (ConsultaFuncoesPerfilBeanId == null)ConsultaFuncoesPerfilBeanId = new ConsultaFuncoesPerfilBean();
			
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codSistema = req.getParameter("codSistema"); 
			if(codSistema==null) codSistema =""; 
			SistBeanId.setCodSistema(codSistema) ;
			SistBeanId.Le_Sistema(codSistema,0);
			
			String codPerfil = req.getParameter("codPerfil"); 		
			if(codPerfil==null) codPerfil =""; 
			PerfBeanId.setCodPerfil(codPerfil);		  
			PerfBeanId.Le_Perfil(0) ;				
			
			PerfBeanId.setCodPerfil(codPerfil);
			ConsultaFuncoesPerfilBeanId.setCodPerfil(codPerfil);
			if (acao.equals("buscaFuncaoPerfil")) {
				SistBeanId.setAtualizarDependente("S");				
				
				if ((codPerfil.length()==0) || (codSistema.length()==0)) {		
					PerfBeanId.setMsgErro("Sistema ou Perfil n�o selecionado.");
				}
				else {
					SistBeanId.setCodSistema(codSistema) ;
					SistBeanId.getFuncoes(0,0) ;	
					SistBeanId.setAtualizarDependente("S");			  	
					PerfBeanId.setCodSistema(codSistema);
					PerfBeanId.setCodPerfil(codPerfil);		  
					PerfBeanId.getFuncoes(0,0) ;
					ConsultaFuncoesPerfilBeanId.setDados(PerfBeanId.getFuncoes());
				}
			}
			
			if (acao.equals("V")) {
				SistBeanId.setAtualizarDependente("N");
				SistBeanId.setCodSistema("") ;
			}
			
			if (acao.equals("ImprimeResultConsulta")) {
				//Titulo do Relatorio de impressao
				String tituloConsulta = "CONSULTA FUN��ES POR PERFIL";
				req.setAttribute("tituloConsulta", tituloConsulta);
				PerfBeanId.getFuncoes(0,0) ;
				ConsultaFuncoesPerfilBeanId.setDados(PerfBeanId.getFuncoes());		
				nextRetorno="/ACSS/ConsultaFuncoesPerfilImp.jsp";
			}
			
			if (acao.equals("Classifica")) {
				SistBeanId.Le_Sistema(codSistema,0) ;
				PerfBeanId.Le_Perfil(0);
				ConsultaFuncoesPerfilBeanId.Le_FuncoesDoPerfil(codSistema,codPerfil,SistBeanId);	
				ConsultaFuncoesPerfilBeanId.Classifica(req.getParameter("ordem"));
				SistBeanId.setAtualizarDependente("S");
			}
			
			req.setAttribute("OperacaoBeanId", OperacaoBeanId);
			req.setAttribute("SistBeanId", SistBeanId);
			req.setAttribute("PerfBeanId", PerfBeanId);
			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("UsrBeanId", UsrBeanId);
			session.setAttribute("ConsultaFuncoesPerfilBeanId",ConsultaFuncoesPerfilBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaFuncoesPerfilCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}