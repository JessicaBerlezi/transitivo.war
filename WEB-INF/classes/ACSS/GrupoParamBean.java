package ACSS;

import java.sql.Connection;
import java.util.Vector;

/**
* <b>Title:</b>        SMIT - Grupo de Par�metros Bean<br>
* <b>Description:</b>  Grupo de Par�metros Bean<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 		Luiz Medronho
* @version 1.0
* @Updates
*/
     
public class GrupoParamBean extends sys.HtmlPopupBean {

	private String atualizarDependente;
	private String codGrupo;
	private String dscGrupo;
	
	private Connection conn;
	private int totalReg;
	private int totalIns;
	private int totalAtu;
	private Vector grupo;
	private Vector grupoGravado;
      
	public GrupoParamBean(){

		super();
		setTabela("TCAU_GRUPO_PARAM");	
		setPopupWidth(35);
		codGrupo	= "";	
		dscGrupo	= "";
		
		grupo = new Vector();
		grupoGravado = new Vector();

	}

	public void setConn(Connection conn)  {
		this.conn=conn ;
	}  

	public Connection getConn()  {
		return this.conn;
	}

	public void setCodGrupo(String codGrupo)  {
		this.codGrupo=codGrupo ;
		if (codGrupo==null) this.codGrupo="" ;
	}  

	public String getCodGrupo()  {
		return this.codGrupo;
	}

	public void setDscGrupo(String dscGrupo)  {
		this.dscGrupo = dscGrupo;
	if (dscGrupo==null) this.dscGrupo="" ;	
	}  

	public String getDscGrupo()  {
		return this.dscGrupo;
	}

	public void setTotalReg(int totalReg) {
		this.totalReg = totalReg;
		if (totalReg == 0)
			this.totalReg = 0;
	}
	public int getTotalReg() {
		return this.totalReg;
	}

	public void setTotalIns(int totalIns) {
		this.totalIns = totalIns;
		if (totalIns == 0)
			this.totalIns = 0;
	}
	public int getTotalIns() {
		return this.totalIns;
	}

	public void setTotalAtu(int totalAtu) {
		this.totalAtu = totalAtu;
		if (totalAtu == 0)
			this.totalAtu = 0;
	}
	public int getTotalAtu() {
		return this.totalAtu;
	}
	
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
	}
	public String getAtualizarDependente() {
		return this.atualizarDependente; 
	}


	/**
	 * M�todo para inserir o Grupo de Par�metros na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsert() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if (this.getCodGrupo() != null) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.GrupoParamExiste(this) == true) 
				{
					vErro.addElement("Grupo de Par�metros j� est� cadastrado: " +this.getCodGrupo() + " - " + 
						this.getDscGrupo()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na inclus�o do Grupo de Par�metros: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodGrupo() == null) 
		{
		vErro.addElement("C�digo do Grupo de Par�metros n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (this.getDscGrupo() == null) 
		{
		vErro.addElement("Descri��o do Grupo de Par�metros n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		if (bOk == true) 
		{
			try 
			{
				Dao dao = Dao.getInstance();
				dao.GrupoParamInsert(this) ;
				vErro.addElement("Grupo de Par�metros inclu�do: " +this.getCodGrupo() + " - " + 
						this.getDscGrupo()+" \n") ;
			}
			catch (Exception e) { 
				vErro.addElement("Grupo de Par�metros n�o inclu�do: " +this.getCodGrupo() + " - " + 
						this.getDscGrupo()+" \n") ;
				vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		setMsgErro(vErro) ;		
		return bOk;  
		
	}

	/**
	 * M�todo para atualizar os dados do grupo de par�metros do Auto na base de dados
	 * Valida primeiro se os dados est�o preenchidos e se o grupo existe.
	 */
	
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodGrupo() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.GrupoParamExiste(this) == false) 
				{
					vErro.addElement("Grupo n�o existe: " +this.getCodGrupo() + " - " + 
						this.getDscGrupo()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Grupo de Par�metros: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodGrupo() == null) 
		{
			vErro.addElement("C�digo do Grupo de Par�metros n�o preenchido" +" \n") ;
			bOk = false ;
		}
		
		if (this.getDscGrupo() == null) 
		{
			vErro.addElement("Descri��o do Grupo de Par�metros n�o preenchida" +" \n") ;
			bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.GrupoParamUpdate(this) ; 
		}
		catch (Exception e) { 
			vErro.addElement("Grupo de Par�metros n�o atualizado: " + this.getCodGrupo()+" \n") ;
			vErro.addElement("Erro na Atualiza��o : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}

	/**
	 * M�todo para remover o Grupo de Par�metros da base de dados
	 * Valida se o Grupo existe.
	 */
   
	public boolean isDelete() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		
		if ((this.getCodGrupo() == null) == false)
		{
			try 
			{
				Dao dao = Dao.getInstance();
				if (dao.GrupoParamExiste(this) == false) 
				{
					vErro.addElement("Grupo de Par�metros n�o existe: " +this.getCodGrupo() + " - " + 
						this.getDscGrupo()+" \n") ;
					bOk = false ;
				
				} 
			}
			catch (Exception e) { 
				vErro.addElement("Erro na consulta ao Grupo de Par�metros: "+e.getMessage()+" \n");
				bOk = false ;
			}
		}
		
		if (this.getCodGrupo() == null) 
		{
		vErro.addElement("C�digo do Grupo de Par�metros n�o preenchido" +" \n") ;
		bOk = false ;
		}
		
		try {
			Dao dao = Dao.getInstance();
			dao.GrupoParamDelete(this) ; 
			vErro.addElement("Grupo de Par�metros excluido: " + this.getCodGrupo()+"\n") ;
		}
		catch (Exception e) { 
			vErro.addElement("Grupo de Par�metros n�o excluido: " + this.getCodGrupo()+" \n") ;
			vErro.addElement("Erro na exclus�o  : " + e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}
	
	public void verificaOperacao(Connection conn) throws sys.BeanException {
		try {
			this.setConn(conn);
			Dao dao = Dao.getInstance();
			if (dao.GrupoParamExiste(this) == false){
				isInsert();
				totalIns = contador(totalIns);
				setTotalIns(totalIns);
			}
			else{ 
				isUpdate();  
				totalAtu = contador(totalAtu);
				setTotalAtu(totalAtu);
			}
			
		} catch (Exception e) {
			throw new sys.BeanException("GrupoParamBean - Erro ao efetuar verifica��o: "+e.getMessage());
		}
	 
	}

	public int contador(int cont) {
		cont += 1;
		return cont;
	}
	
	
	public Vector getGrupo(int min,int livre)  {
		   try  {
 	
				Dao dao = Dao.getInstance();
				dao.getGrupoParam(this);
				if (this.grupo.size()<min-livre) 
					livre = min-grupo.size() ;	  	
					for (int i=0;i<livre; i++) 	{
						this.grupo.addElement(new GrupoParamBean()) ;
				}
		   }
		   catch (Exception e) {
				setMsgErro("GrupoParam007 - Leitura: " + e.getMessage());
		   }
		  return this.grupo ;
		}


	public Vector getGrupo()  {
	   return this.grupo;
	}   
  
	public GrupoParamBean getGrupo(int i)  { 
	   return (GrupoParamBean)this.grupo.elementAt(i);
	}
 
	public void setGrupo(Vector grupo)  {
			this.grupo = grupo ;
	}  
	
	public void setGrupoGravado(Vector grupoGravado ){
		this.grupoGravado = grupoGravado;
	}
	public Vector getGrupoGravado(){
		return this.grupoGravado;
	}
	
	public boolean isInsertGrupo()   {
	   boolean retorno = true;
	   Vector vErro = new Vector() ;
		   // Verificada a valida��o dos dados nos campos
		   if( isValidStatus(vErro) )   {
				   try  {
					Dao dao = Dao.getInstance();	   
				   if (dao.InsertGrupoParam(this,vErro))    {	
						   vErro.addElement("Grupos "
								+this.dscGrupo+" atualizados:"+ " \n");
					   }
					   else{		
							vErro.addElement("Grupos "
								+this.dscGrupo+" n�o atualizados:"+ " \n");     	   
					   }
				   }
		   catch (Exception e) {
				vErro.addElement("Grupos "+this.dscGrupo+" n�o atualizados:"+ " \n") ;
				vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
				retorno = false;
		   }//fim do catch
		}
	 else retorno = false;   
		setMsgErro(vErro);
		return retorno;
	  }
   
	  public boolean isValidStatus(Vector vErro) {
 	
		String sDesc = "";
		for (int i = 0 ; i< this.grupo.size(); i++) {
		   GrupoParamBean myGrupo = (GrupoParamBean)this.grupo.elementAt(i) ;
		sDesc = myGrupo.getCodGrupo();
		if((sDesc.length()==0) ||("".equals(sDesc)))
		  continue ;
  		  
		  // verificar duplicata 
	   for (int m = 0 ; m <this.grupo.size(); m++) {
		   GrupoParamBean myGrupoOutro = (GrupoParamBean)this.grupo.elementAt(m) ;		
		if ((m!=i) && (myGrupoOutro.getCodGrupo().trim().length()!=0) && (myGrupoOutro.getCodGrupo().equals(sDesc))) {
			vErro.addElement("Grupo em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
		}
	   }
		}
		return (vErro.size() == 0) ;  
	  }

}
