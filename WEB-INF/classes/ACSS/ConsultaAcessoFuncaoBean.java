package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ConsultaAcessoFuncaoBean extends sys.HtmlPopupBean {

	private String codUsuario;
	private String indUsuario; /*Ativo/Inativo/Todos*/
	private String codPerfil;
	private String codOrgaoLotacao;
	private String codOrgaoAtuacao;
	private String codSistema;
	
	private List dados;
	private String sigOrgaoLotacao; 
	private String sigOrgaoAtuacao;
	private String nomDescricao; 
	private String nomUsuario;
	private String nomAbrev;
	
	//lista de ordenacao
	private String ordClass ;
	private String ordem ;
	

	public ConsultaAcessoFuncaoBean() throws sys.BeanException {
		codUsuario = "";
		indUsuario = "";
		codPerfil = "";
		codOrgaoLotacao = "";
		codOrgaoAtuacao = "";
		codSistema = "";
		
		dados = new ArrayList();
		sigOrgaoLotacao = "";
		sigOrgaoAtuacao = "";
		nomDescricao = "";
		nomAbrev = "";
		nomUsuario = "";
		
		ordClass = "ascendente";
		ordem = "Data";
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() {
		return this.codUsuario;
	}
	
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
		if (nomUsuario == null)
			this.nomUsuario = "";
	}
	public String getNomUsuario() {
		return this.nomUsuario;
	}

	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if (codSistema == null)
			this.codSistema = "";
	}
	public String getCodSistema() {
		return this.codSistema;
	}
	
	public void setNomAbrev(String nomAbrev) {
		this.nomAbrev = nomAbrev;
		if (nomAbrev == null)
			this.nomAbrev = "";
	}
	public String getNomAbrev() {
		return this.nomAbrev;
	}

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
		if (codPerfil == null)
			this.codPerfil = "";
	}
	public String getCodPerfil() {
		return this.codPerfil;
	}

	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao == null)
			this.codOrgaoAtuacao = "";
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;
		if (sigOrgaoAtuacao == null)
		this.sigOrgaoAtuacao = "";
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}
	
	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
			this.codOrgaoLotacao = codOrgaoLotacao;
			if (codOrgaoLotacao == null)
				this.codOrgaoLotacao = "";
		}
		public String getCodOrgaoLotacao() {
			return this.codOrgaoLotacao;
		}
	
		public void setSigOrgaoLotacao(String sigOrgaoLotacao) {
			this.sigOrgaoLotacao = sigOrgaoLotacao;
			if (sigOrgaoLotacao == null)
			this.sigOrgaoLotacao = "";
		}
		public String getSigOrgaoLotacao() {
			return this.sigOrgaoLotacao;
		}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public ConsultaAcessoFuncaoBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaAcessoFuncaoBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaAcessoFuncaoBean)this.dados.get(i);
	 }
// --------------------------  Metodos da Bean ----------------------------------


	public void Le_AcessoFuncao(String codOperacao, String codSistema)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaAcessoFuncao(this, codOperacao, codSistema);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	
	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass==null) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
//	  --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "Data";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
		public String getNomOrdem()  {
			String nomOrdem = "�rg�o Lota��o, Usu�rio, �rg�o Atua��o e Perfil" ;
			if (this.ordem.equals("OrgaoLot"))    nomOrdem = "�rg�o Lota��o, �rg�o Atua��o, Usu�rio e Perfil" ;
			if (this.ordem.equals("Usuario"))     nomOrdem = "Usu�rio, Perfil, �rg�o Lota��o e �rg�o Atua��o" ;   
			if (this.ordem.equals("OrgaoAtu"))    nomOrdem = "�rg�o Atua��o, �rg�o Lota��o, Usu�rio e Perfil" ;   
			if (this.ordem.equals("Perfil"))      nomOrdem = "Perfil,Usu�rio, �rg�o Lota��o e �rg�o Atua��o" ;   
			return nomOrdem+ " ("+getOrdClass()+")" ;
	  }
// --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
			if (ordemSol.equals("OrgaoLot")) ord = 1 ;
			if (ordemSol.equals("Usuario"))  ord = 2 ;   
			if (ordemSol.equals("OrgaoAtu")) ord = 3 ;  
			if (ordemSol.equals("Perfil")) ord = 4 ; 
			
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			if (ordemSol.equals(getOrdem()))   {
				if ( getOrdClass().equals("ascendente")) {
					trocaasc = false ;
					trocades = true ;
					setOrdClass("descendente"); 
				} 
				else {
					trocaasc = true ;
					trocades = false;
					setOrdClass("ascendente"); 
				}
			}
			else setOrdClass("ascendente");
			setOrdem(ordemSol) ;
			int tam = getDados().size() ;
			ConsultaAcessoFuncaoBean tmp = new ConsultaAcessoFuncaoBean();
			String orgLot1,orgLot2,orgAtu1,orgAtu2,usr1,usr2,perf1,perf2 ;  
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					// "�rg�o Lota��o, Usu�rio, �rg�o Atua��o e Perfil" ;
					case 0:
						orgLot1 = getDados(i).getSigOrgaoLotacao();
						orgLot2 = getDados(j).getSigOrgaoLotacao(); 
						orgAtu1 = getDados(i).getSigOrgaoAtuacao() ;
						orgAtu2 = getDados(j).getSigOrgaoAtuacao() ;
						usr1 = getDados(i).getNomUsuario();
						usr2 = getDados(j).getNomUsuario();
						perf1 =getDados(i).getNomDescricao();
						perf2 =getDados(j).getNomDescricao();
						
						if ((orgLot1+orgAtu1+usr1+perf1).compareTo(orgLot2+orgAtu2+usr2+perf2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "�rg�o Lota��o" ;
					case 1:
						if(getDados(i).getSigOrgaoLotacao().compareTo(getDados(j).getSigOrgaoLotacao())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
					// "Usu�rio" ;   
					case 2:
						if (getDados(i).getNomUsuario().compareTo(getDados(j).getNomUsuario())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "�rg�o Atua��o" ;   
					case 3:
						if (getDados(i).getSigOrgaoAtuacao().compareTo(getDados(j).getSigOrgaoAtuacao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Perfil" ; 
					case 4:
					  if (getDados(i).getNomDescricao().compareTo(getDados(j).getNomDescricao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "�rg�o Lota��o" ;	
					default:
					if(getDados(i).getSigOrgaoLotacao().compareTo(getDados(j).getSigOrgaoLotacao())>0) troca=trocaasc;
					 else troca=trocades ;
				   break;
				}	
				if (troca) 	{		
				   tmp = (ConsultaAcessoFuncaoBean)getDados(i);
				   getDados().set(i,getDados(j));
				   getDados().set(j,tmp);
				}
			}
		}
	  }

		public String getIndUsuario() {
			return indUsuario;
		}

		public void setIndUsuario(String indUsuario) {
			if (indUsuario==null) indUsuario="";
			this.indUsuario = indUsuario;
		}
		
		public String getDescIndUsuario()	{
			String descIndUsuario="";
			if (this.indUsuario.equals("A")) descIndUsuario="ATIVOS";
			if (this.indUsuario.equals("I")) descIndUsuario="INATIVOS";
			if (this.indUsuario.equals("T")) descIndUsuario="TODOS";			
			return descIndUsuario;
		}

}