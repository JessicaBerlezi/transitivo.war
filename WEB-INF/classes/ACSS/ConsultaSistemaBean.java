package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ConsultaSistemaBean extends sys.HtmlPopupBean {

	private String codUsuario;
	private String codPerfil;
	private String codOrgaoAtuacao;
	private String codSistema;
	
	private List dados;
	private String sigOrgaoAtuacao; 
	private String nomDescricao; 
	private String nomUsuario;
	private String nomAbrev;
	private String tipoUsuario;

	//lista de ordenacao
	private String ordClass ;
	private String ordem ;
	

	public ConsultaSistemaBean() throws sys.BeanException {
		codUsuario = "";
		codPerfil = "";
		codOrgaoAtuacao = "";
		codSistema = "";
		
		dados = new ArrayList();
		sigOrgaoAtuacao = "";
		nomDescricao = "";
		nomAbrev = "";
		nomUsuario = "";
		tipoUsuario = "";
		
		ordClass = "ascendente";
		ordem = "Data";
		
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() {
		return this.codUsuario;
	}
	
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
		if (nomUsuario == null)
			this.nomUsuario = "";
	}
	public String getNomUsuario() {
		return this.nomUsuario;
	}

	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if (codSistema == null)
			this.codSistema = "";
	}
	public String getCodSistema() {
		return this.codSistema;
	}
	
	public void setNomAbrev(String nomAbrev) {
		this.nomAbrev = nomAbrev;
		if (nomAbrev == null)
			this.nomAbrev = "";
	}
	public String getNomAbrev() {
		return this.nomAbrev;
	}

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
		if (codPerfil == null)
			this.codPerfil = "";
	}
	public String getCodPerfil() {
		return this.codPerfil;
	}

	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao == null)
			this.codOrgaoAtuacao = "";
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;
		if (sigOrgaoAtuacao == null)
		this.sigOrgaoAtuacao = "";
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setTipoUsuario(String tipoUsuario) {
		if(tipoUsuario == null) tipoUsuario = "";
		else this.tipoUsuario = tipoUsuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	
	public ConsultaSistemaBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaSistemaBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaSistemaBean)this.dados.get(i);
	 }

	public void Le_DadosSist(String codOrgao, String codUsuario, UsuarioBean myUsr)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaDadosSis(this, codOrgao, codUsuario, myUsr);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	/**--------------------------------------------------------------------
		 * M�todo de ordena��o de uma lista 
		 *----------------------------------------------------------------------
		 */
//		  --------------------------------------------------------------------------
			public void setOrdClass(String ordClass)  {
				this.ordClass=ordClass ;
				if (ordClass==null) this.ordClass= "ascendente";
			} 
			public String getOrdClass()  {
				return this.ordClass;
			}
//		  --------------------------------------------------------------------------
			public void setOrdem(String ordem)  {
				this.ordem=ordem ;
				if (ordem==null) this.ordem= "Data";
			}  
			public String getOrdem()  {
				return this.ordem;
			}
			public String getNomOrdem()  {
				String nomOrdem = "�rg�o Atua��o, Sistema, Perfil e Usu�rio" ;
				if (this.ordem.equals("OrgaoAtu"))    nomOrdem = "�rg�o Atua��o, Sistema, Perfil e Usu�rio" ;
				if (this.ordem.equals("Sistema"))     nomOrdem = "Sistema, �rg�o Atua��o, Perfil e Usu�rio" ;
				if (this.ordem.equals("Perfil"))      nomOrdem = "Perfil, �rg�o Atua��o e Sistema" ;
				return nomOrdem+ " ("+getOrdClass()+")" ;
		  }
// --------------------------------------------------------------------------
			public void Classifica(String ordemSol) throws sys.BeanException {
				int ord = 0;
				if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
				if (ordemSol.equals("OrgaoAtu"))    ord = 1 ;
				if (ordemSol.equals("Sistema"))     ord = 2 ;
				if (ordemSol.equals("Perfil"))      ord = 3 ;   
				
				boolean troca    = false ;
				boolean trocaasc = true ;
				boolean trocades = false ;
				if (ordemSol.equals(getOrdem()))   {
					if ( getOrdClass().equals("ascendente")) {
						trocaasc = false ;
						trocades = true ;
						setOrdClass("descendente"); 
					} 
					else {
						trocaasc = true ;
						trocades = false;
						setOrdClass("ascendente"); 
					}
				}
				else setOrdClass("ascendente");
				setOrdem(ordemSol) ;
				int tam = getDados().size() ;
				ConsultaSistemaBean tmp = new ConsultaSistemaBean();
				String orgAtu1,orgAtu2,sist1,sist2, perf1, perf2 ;  
				for (int i=0; i<tam; i++)  {  
					for (int j=i+1; j<tam; j++)  {
						troca=false;
						// compara os campos
						switch (ord) {
						// "�rg�o Atua��o, Sistema e Perfil"
						case 0:
							orgAtu1 = getDados(i).getCodOrgaoAtuacao();
							orgAtu2 = getDados(j).getCodOrgaoAtuacao(); 
							sist1 = getDados(i).getCodSistema();
							sist2 = getDados(j).getCodSistema() ;
							perf1 = getDados(i).getCodPerfil();
							perf2 = getDados(j).getCodPerfil() ;
						
							if ((orgAtu1+sist1+perf1).compareTo(orgAtu2+sist2+perf2)>0 ) troca=trocaasc;
							else troca=trocades ;
							break;
							
							// "�rg�o Atua��o"
						case 1:
							if (getDados(i).getCodOrgaoAtuacao().compareTo(getDados(j).getCodOrgaoAtuacao())>0 ) troca=trocaasc;
							else troca=trocades ;	
							break;
							
							// "Sistema"
						case 2:
							if(getDados(i).getCodSistema().compareTo(getDados(j).getCodSistema())>0) troca=trocaasc;
							else troca=trocades ;
							break;
							
							// "Tipo Usuario
						case 4:
							if (getDados(i).getTipoUsuario().compareTo(getDados(j).getTipoUsuario())>0 ) troca=trocaasc;
							else troca=trocades ;	
							break;
							
							// "Perfil"
						default:
							if(getDados(i).getCodPerfil().compareTo(getDados(j).getCodPerfil())>0) troca=trocaasc;
							else troca=trocades ;
						break;
						}	
						if (troca) 	{		
							tmp = (ConsultaSistemaBean)getDados(i);
							getDados().set(i,getDados(j));
							getDados().set(j,tmp);
						}
					}
				}
			}	

}