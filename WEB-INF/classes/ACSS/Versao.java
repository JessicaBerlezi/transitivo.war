package ACSS;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Versao {
	/*
	 * Somente o nrVersao deve ser alterado a cada vers�o efetuada
	 */
	static String nrVersao = "01";
	
	static long yourmilliseconds = System.currentTimeMillis();
	static SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd."); 
	static Date resultdate = new Date(yourmilliseconds);
	static String retorno = sdf.format(resultdate);
	
	public static String callVersao(){
		return retorno+nrVersao;
	}
	
}