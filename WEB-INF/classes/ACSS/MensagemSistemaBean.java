package ACSS;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class MensagemSistemaBean extends sys.HtmlPopupBean
{
   private String codSistema;
   private String codMensagemSistema; //seq
   private String codMensagemInterno; 
   private String dscMensagem;
   private String dscMotivo;
   private String dscAcao;
   private String codTipMensagem;

  public MensagemSistemaBean()
  {
     super();
     setTabela("TCAU_MENSAGEM_SISTEMA");
     setPopupWidth(35);

     codSistema         = "0";
     codMensagemSistema = "0";
	 codMensagemInterno = "0";
     dscMensagem        = "";
     dscMotivo          = "";
     dscAcao            = "";
     codTipMensagem     = "0";
  }
  //-------------------------------------------------------------------------
  public void setCodSistema(String codSistema) {
     if( codSistema == null ) this.codSistema = "0";
     else
         this.codSistema = codSistema;
  }
  public String getCodSistema() { return this.codSistema; }
  //-------------------------------------------------------------------------
  public void setCodMensagemSistema( String codMensagemSistema ) {
     if( codMensagemSistema == null ) this.codMensagemSistema = "0";
	   else
         this.codMensagemSistema = codMensagemSistema;
  }
  public String getCodMensagemSistema() { return this.codMensagemSistema; }
  //-------------------------------------------------------------------------
  public void setCodMensagemInterno( String codMensagemInterno ) {
     if( codMensagemInterno == null ) this.codMensagemInterno = "0";
	   else
         this.codMensagemInterno = codMensagemInterno;
  }
  public String getCodMensagemInterno() { return this.codMensagemInterno; }
  //-------------------------------------------------------------------------
  public void setDscMensagem (String dscMensagem) 
  {
     if (dscMensagem == null) this.dscMensagem = "";
     else
         this.dscMensagem = dscMensagem;
  }
  public String getDscMensagem() { return this.dscMensagem; }
  //-------------------------------------------------------------------------
  public void setDscMotivo (String dscMotivo) {
     if( dscMotivo == null )  this.dscMotivo = "";
     else
       	 this.dscMotivo = dscMotivo;
  }
  public String getDscMotivo() { return this.dscMotivo; }
  //-------------------------------------------------------------------------
  public void setDscAcao  (String dscAcao ) {
     if( dscAcao == null )  this.dscAcao = "";
     else
         this.dscAcao = dscAcao;
  }
  public String getDscAcao () {	return this.dscAcao; }
  //--------------------------------------------------------------------------
  public void setCodTipMensagem( String codTipMensagem ) {
     if( codTipMensagem == null )  this.codTipMensagem = "0";
     else
         this.codTipMensagem = codTipMensagem;
  }
  public String getCodTipMensagem () {	return this.codTipMensagem; }
  //--------------------------------------------------------------------------

  public String getMensagens() {
     String myMsgSis = "" ;
     try {
          Dao dao = Dao.getInstance();
          myMsgSis = dao.getMensagensSistemas();
   }                      
   catch(Exception e){	}
   return myMsgSis;
 }

   //--------------------------------------------------------------------------
}
