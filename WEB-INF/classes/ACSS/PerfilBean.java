package ACSS;

import java.util.Vector;


/**
 * <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
 * <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Sergio Monteiro
 * @version 1.0
 * @Updates
 */

public class PerfilBean extends sys.HtmlPopupBean { 
	
	private String codSistema;
	private String codPerfil;
	private String nomDescricao;  
	
	private String indVisEndereco;
	private String indVis2via;
	private String indRecebNot;
	private String indVisAr;
	private String indVisAuto;
	private String indVisFoto;
	private String indVisHist;  
	private String indVisImpr;
	private String indVisRej;  
	private String indVisReq;  
	private String indVisTodosOrg;
	private String txtJustificativa;
	
	
	private String acaoAuditoria;
	private String nomUserNameOper;
	private String datProcOper;
	private String orgaoLotacaoOper;
	private String sigOrgaoLotacaoOper;
	
	
	private Vector funcoes;   
	
	public PerfilBean()  {
		
		super();
		setTabela("TCAU_PERFIL");	
		setPopupWidth(35);
		codSistema      = "";	
		codPerfil       = "";
		nomDescricao	= "";
		
		indVisEndereco   = "";
		indVis2via       = "";  
		indRecebNot      = "";
		indVisAr         = "";
		indVisAuto       = "";
		indVisFoto       = "";
		indVisHist       = "";  
		indVisImpr       = "";
		indVisRej        = "";  
		indVisReq        = ""; 
		indVisTodosOrg   = "";
		txtJustificativa = ""; 
		
		acaoAuditoria   = "";
		nomUserNameOper   = "";
		datProcOper   = "";
		orgaoLotacaoOper   = "";
		sigOrgaoLotacaoOper   = "";
		
		funcoes         = new Vector() ;	
		
	}
	
	public void setCodSistema(String codSistema)  {
		this.codSistema=codSistema ;
		if (codSistema==null) this.codSistema="" ;
	}  
	public String getCodSistema()  {
		return this.codSistema;
	}
	
	public void setCodPerfil(String codPerfil)  {
		this.codPerfil=codPerfil ;
		if (codPerfil==null) this.codPerfil="" ;
	}  
	public String getCodPerfil()  {
		return this.codPerfil;
	}
	
	public void setNomDescricao(String nomDescricao)  {
		if (nomDescricao==null) nomDescricao="" ;	
		this.nomDescricao = nomDescricao;
	}  
	public String getNomDescricao()  {
		return this.nomDescricao;
	}
	public void setIndVisEndereco(String indVisEndereco)  {
		if (indVisEndereco==null) indVisEndereco="" ;      
		if ((indVisEndereco.length()>0) && ("Ss1".indexOf(indVisEndereco)>=0)) indVisEndereco="S" ;
		else indVisEndereco="" ;
		this.indVisEndereco = indVisEndereco ;	 
	}
	public String getIndVisEndereco()  {
		return this.indVisEndereco;
	}
	public void setIndVis2via(String indVis2via)  {
		if (indVis2via==null) indVis2via="" ;      
		if ((indVis2via.length()>0) && ("Ss1".indexOf(indVis2via)>=0)) indVis2via="S" ; 
		else indVis2via="" ;	
		this.indVis2via = indVis2via ;	 
	}
	public String getIndVis2via()  {
		return this.indVis2via;
	}
	
	public void setIndRecebNot(String indRecebNot)  {
		if (indRecebNot==null) indRecebNot="" ;      
		if ((indRecebNot.length()>0) && ("Ss1".indexOf(indRecebNot)>=0)) indRecebNot="S" ; 
		else indRecebNot="" ;	
		this.indRecebNot = indRecebNot ;	 
	}
	public String getIndRecebNot()  {
		return this.indRecebNot;
	}
	
	public void setIndVisAr(String indVisAr)  {
		if (indVisAr==null) indVisAr="" ;      
		if ((indVisAr.length()>0) && ("Ss1".indexOf(indVisAr)>=0)) indVisAr="S" ;  
		else indVisAr="" ;  	
		this.indVisAr = indVisAr ;	  
	}
	public String getIndVisAr()  {
		return this.indVisAr;
	}
	public void setIndVisAuto(String indVisAuto)  {
		if (indVisAuto==null) indVisAuto="" ; 
		if ((indVisAuto.length()>0) && ("Ss1".indexOf(indVisAuto)>=0)) indVisAuto="S" ;  
		else indVisAuto="" ;  	  
		this.indVisAuto = indVisAuto ;	         
	}
	public String getIndVisAuto()  {
		return this.indVisAuto;
	}
	public void setIndVisFoto(String indVisFoto)  {
		if (indVisFoto==null) indVisFoto="" ; 
		if ((indVisFoto.length()>0) && ("Ss1".indexOf(indVisFoto)>=0)) indVisFoto="S" ;  
		else indVisFoto="" ;      
		this.indVisFoto = indVisFoto ;             
	}
	public String getIndVisFoto()  {
		return this.indVisFoto;
	}  
	public void setIndVisHist(String indVisHist)  {
		if (indVisHist==null) indVisHist="" ;  
		if ((indVisHist.length()>0) && ("Ss1".indexOf(indVisHist)>=0)) indVisHist="S" ;
		else  indVisHist="" ;
		this.indVisHist = indVisHist ;      	  
	}
	public String getIndVisHist()  {
		return this.indVisHist;
	}
	public void setIndVisImpr(String indVisImpr)  {
		if (indVisImpr==null) indVisImpr="" ;  
		if ((indVisImpr.length()>0) && ("Ss1".indexOf(indVisImpr)>=0)) indVisImpr="S" ;  
		else indVisImpr="" ;   	   
		this.indVisImpr = indVisImpr ;	     
	}
	public String getIndVisImpr()  {
		return this.indVisImpr;
	}
	public void setIndVisRej(String indVisRej)  {
		if (indVisRej==null) indVisRej="" ; 
		if ((indVisRej.length()>0) && ("Ss1".indexOf(indVisRej)>=0) ) indVisRej="S" ;  
		else indVisRej="" ;  
		this.indVisRej = indVisRej ;
	}
	public String getIndVisRej()  {
		return this.indVisRej;
	}
	public void setIndVisReq(String indVisReq)  {
		if (indVisReq==null) indVisReq="" ; 
		if ((indVisReq.length()>0) && ("Ss1".indexOf(indVisReq)>=0) ) indVisReq="S" ;  
		else indVisReq="" ;  	  
		this.indVisReq = indVisReq ;	         
	}
	public String getIndVisReq()  {
		return this.indVisReq;
	}
	public void setIndVisTodosOrg(String indVisTodosOrg)  {
		if (indVisTodosOrg==null) indVisTodosOrg="" ;      
		if ((indVisTodosOrg.length()>0) && ("Ss1".indexOf(indVisTodosOrg)>=0)) indVisTodosOrg="S" ;
		else indVisTodosOrg="" ;
		this.indVisTodosOrg = indVisTodosOrg ;	 
	}
	public String getIndVisTodosOrg()  {
		return this.indVisTodosOrg;
	}
	
	public void setTxtJustificativa(String txtJustificativa) {
		if(txtJustificativa == null) txtJustificativa = "";
		else this.txtJustificativa = txtJustificativa;
	}
	public String getTxtJustificativa() {
		return txtJustificativa;
	}
	
	
	public void setAcaoAuditoria(String acaoAuditoria)  {
		if (acaoAuditoria==null) acaoAuditoria="" ;
		this.acaoAuditoria = acaoAuditoria;
	}
	public String getAcaoAuditoria()  {
		return this.acaoAuditoria;
	}
	
	public void setNomUserNameOper(String nomUserNameOper)  {
		if (nomUserNameOper==null) nomUserNameOper="" ;
		this.nomUserNameOper = nomUserNameOper;
	}
	public String getNomUserNameOper()  {
		return this.nomUserNameOper;
	}
	
	public void setDatProcOper(String datProcOper)  {
		if (datProcOper==null) datProcOper="" ;
		this.datProcOper = datProcOper;
	}
	public String getDatProcOper()  {
		return this.datProcOper;
	}
	
	public void setOrgaoLotacaoOper(String orgaoLotacaoOper)  {
		if (orgaoLotacaoOper==null) orgaoLotacaoOper="" ;
		this.orgaoLotacaoOper = orgaoLotacaoOper;
	}
	public String getOrgaoLotacaoOper()  {
		return this.orgaoLotacaoOper;
	}
	
	public void setSigOrgaoLotacaoOper(String sigOrgaoLotacaoOper)  {
		if (sigOrgaoLotacaoOper==null) sigOrgaoLotacaoOper="" ;
		this.sigOrgaoLotacaoOper = sigOrgaoLotacaoOper;
	}
	public String getSigOrgaoLotacaoOper()  {
		return this.sigOrgaoLotacaoOper;
	}
	
	
	public void Le_Perfil(int tp)  {
		try {
			Dao dao = Dao.getInstance();
			if (dao.PerfilLeBean(this,tp)==false) {		
				this.codPerfil = "0" ;
				setPkid("0") ;		
			}	
		} // fim do try
		catch (Exception e) { 
			this.codPerfil = "0" ;
			setPkid("0") ;		
			setMsgErro("Erro na leitura do Perfil: " + e.getMessage()) ;
		}	// fim do catch 
	}
	
	//--------------------------------------------------------------------------
	public void setFuncoes(Vector funcoes)  {
		this.funcoes = funcoes ;
	}
	//--------------------------------------------------------------------------
	
	public Vector getFuncoesBiometria(int min,int livre)  {
		try  {
			Dao dao = Dao.getInstance();
			dao.BiometriaGetFuncoes(this);
		}
		catch (Exception e) {
			setMsgErro("Perfil006 - Leitura: " + e.getMessage());
		}//fim do catch
		return this.funcoes ;
	}
	
	
	public Vector getFuncoes(int min,int livre)  {
		try  {
			Dao dao = Dao.getInstance();
			dao.PerfilGetFuncoes(this);
		}
		catch (Exception e) {
			setMsgErro("Perfil006 - Leitura: " + e.getMessage());
		}//fim do catch
		return this.funcoes ;
	}
	//--------------------------------------------------------------------------
	public Vector getFuncoes()  {
		return this.funcoes;
	}
	public OperacaoBean getFuncoes(int i)  { return (OperacaoBean)this.funcoes.elementAt(i); }
	//--------------------------------------------------------------------------
	public boolean isInsertFuncoes()   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		// Verificada a valida��o dos dados nos campos
		try  {
			Dao dao = Dao.getInstance();		   
			if (dao.PerfilInsertFuncoes(this,vErro))    { 					   
				vErro.addElement("Opera��es/Funcionalidades no Perfil "+this.nomDescricao+" atualizadas:"+ " \n");
			}
			else    {			   	   		   
				vErro.addElement("Opera��es/Funcionalidades no Perfil "+this.nomDescricao+" n�o atualizadas:"+ " \n");     	   
			}
		}
		catch (Exception e) {
			vErro.addElement("Opera��es/Funcionalidades no Perfil "+this.nomDescricao+" n�o atualizadas:"+ " \n") ;
			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
			retorno = false;
		}//fim do catch  
		setMsgErro(vErro);
		return retorno;
	}
	
	//--------------------------------------------------------------------------
	public String getPerfis(){
		String myPerfis = "" ; 
		try    {
			Dao dao = Dao.getInstance();
			myPerfis = dao.getPerfis() ;
		}
		catch(Exception e){
		}	
		return myPerfis ;
	}
	//--------------------------------------------------------------------------
	
	public boolean isInsertUsuario(UsuarioBean myUsr) {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.PerfilUsuarioInsert(myUsr,this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Perfil n�o incluido para Usu�rio: " +myUsr.getNomUsuario()+" \n") ;
			vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
			bOk = false ;
		}
		setMsgErro(vErro) ;		
		return bOk;  
	}
	public boolean isDeleteUsuario(UsuarioBean myUsr) {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.PerfilUsuarioDelete(myUsr,this) ; 
		}
		catch (Exception e) { 
			vErro.addElement("Perfil n�o excluido para Usu�rio: " +myUsr.getNomUsuario()+" \n") ;
			vErro.addElement("Erro na exclus�o: "+e.getMessage()+" \n");
			bOk = false ;
		}	// fim do catch 	
		setMsgErro(vErro) ;		
		return bOk;  
	}
	
	public boolean isInsertFuncoesBiometria()   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		// Verificada a valida��o dos dados nos campos
		try  {
			Dao dao = Dao.getInstance();		   
			if (dao.BiometriaInsertFuncoes(this,vErro))    { 					   
				vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" atualizadas:"+ " \n");
			}
			else    {			   	   		   
				vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" n�o atualizadas:"+ " \n");     	   
			}
		}
		catch (Exception e) {
			vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" n�o atualizadas:"+ " \n") ;
			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
			retorno = false;
		}//fim do catch  
		setMsgErro(vErro);
		return retorno;
	}
	
//	--------------------------------------------------------------------------
	public boolean isInsertFuncoesBiometriaTodos(String[] ArraycodOrgao, String tipoOperacao)   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		// Verificada a valida��o dos dados nos campos
		try  {
			Dao dao = Dao.getInstance();		   
			if (dao.BiometriaInsertFuncoesTodos(this,vErro, ArraycodOrgao, tipoOperacao))    { 					   
				vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" atualizadas:"+ " \n");
			}
			else    {			   	   		   
				vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" n�o atualizadas:"+ " \n");     	   
			}
		}
		catch (Exception e) {
			vErro.addElement("Opera��es/Funcionalidades na Biometria "+this.nomDescricao+" n�o atualizadas:"+ " \n") ;
			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
			retorno = false;
		}//fim do catch  
		setMsgErro(vErro);
		return retorno;
	}
	
	
}