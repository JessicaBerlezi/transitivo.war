package ACSS;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar OperacoesSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class PerfilUsuarioCommand extends sys.Command {

    private static final String jspPadrao="/ACSS/perfilUsuario.jsp";      
  private String next;

  public PerfilUsuarioCommand() {
    next             =  jspPadrao;
  }

  public PerfilUsuarioCommand(String next) {
    this.next = next;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {     
	    // cria os Beans, se n�o existir
    	HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			

	 	
		// cria os Beans de Usuario e Perfil, se n�o existir
		ACSS.UsuarioBean UsrBeanId = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
		if (UsrBeanId==null)        UsrBeanId = new ACSS.UsuarioBean() ;	  			
		ACSS.PerfilBean PerfBeanId = (ACSS.PerfilBean)req.getAttribute("PerfBeanId") ;
		if (PerfBeanId==null)        PerfBeanId = new ACSS.PerfilBean() ;	  			
				
		// obtem e valida os parametros recebidos					
		String acao          = req.getParameter("acao");  
		if(acao==null) acao  =" ";
		String codOrgao      = req.getParameter("codOrgao"); 		
		if (codOrgao==null) codOrgao="0";
		String codSistema    = req.getParameter("codSistema"); 		
		if (codSistema==null) codSistema="0";
		String codUsuario    = req.getParameter("codUsuario"); 		
		if (codUsuario==null) codUsuario="0";		
		String codPerfil     = req.getParameter("codPerfil"); 		
		if (codPerfil==null) codPerfil="0";
		String sigNivel      = req.getParameter("sigNivel"); 		
		if (sigNivel==null) sigNivel="0";
		String pkid          = req.getParameter("pkid");   
		if(pkid==null) pkid  ="0";  
		UsrBeanId.getOrgao().setCodOrgao(codOrgao);
		UsrBeanId.setCodUsuario(codUsuario);				
					
		PerfBeanId.setCodSistema(codSistema);
		PerfBeanId.setCodPerfil(codPerfil);
		if (acao.equals("buscaUsuarios")) {
//			UsrBeanId.UsuarioOrgaos()	;
		}		
		if (acao.equals("buscaPerfis")) {
			// Preparar o Bean de Perfil		  	  	    	  
			UsrBeanId.UsuarioPerfil(PerfBeanId)	;
		}
	   	if ("13".indexOf(acao)>=0) {			
	   		//		Setar o(s) objetos para executar a acao requerida
	   	 	PerfBeanId.setMsgErro("") ;  	      			
	   		if (acao.compareTo("1")==0)	{
		   		PerfBeanId.Le_Perfil(0);		
				PerfBeanId.isInsertUsuario(UsrBeanId) ;
	   		}
	   		if (acao.compareTo("3")==0)	{
		   		PerfBeanId.Le_Perfil(0);					
				PerfBeanId.isDeleteUsuario(UsrBeanId) ; 
				PerfBeanId.setCodSistema("");
				PerfBeanId.setNomDescricao("");				
				PerfBeanId.setPkid("0");
	   		}			
		}   
		req.setAttribute("UsrBeanId",UsrBeanId) ;
		req.setAttribute("PerfBeanId",PerfBeanId) ;		  				  
    }
    catch (Exception ue) {
      throw new sys.CommandException("PerfisUsuarioCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
 
}