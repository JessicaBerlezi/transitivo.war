package ACSS;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import TAB.MunicipioBean;

/**
* <b>Title:</b>        Consulta - Consultas Orgao<br>
* <b>Description:</b>  Comando para Consultar Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br> 
* @author Wellem 
* @version 1.0
* @Updates
*/

public class OrgaoCmd extends sys.Command 
{

  private static final String jspPadrao="/ACSS/Orgao.jsp";   
      
  
  private String next;
  
  public OrgaoCmd() {
    next             = jspPadrao ;
  }
  
  public OrgaoCmd(String next)  {
  	this.next = next;
  }
 
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = next ;
    try {      	
	    HttpSession session   = req.getSession() ;								
    	ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  				
	    // cria os Beans do Orgao, se n�o existir
	    ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean)req.getAttribute("OrgaoBeanId") ;
	    if (OrgaoBeanId==null)        OrgaoBeanId = new ACSS.OrgaoBean() ;	 
	    
		  sys.UFBean UFCNHId = (sys.UFBean)req.getAttribute("codUfCNHTR") ;
		  if (UFCNHId==null)UFCNHId = new sys.UFBean() ;	
	    
	 
	    MunicipioBean MunicipioId = (MunicipioBean)req.getAttribute("MunicipioId") ;
	    if (MunicipioId==null)        
	    	MunicipioId = new MunicipioBean() ;	
	    
	    String codUF = req.getParameter("codUfCNHTR");
	    String codMunicipio = req.getParameter("codMunicipio");	

		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)          acao =" ";  
		String pkid           = req.getParameter("pkid");   
		if(pkid==null)          pkid  ="";  		  	  	    	  
		String codOrgao       = req.getParameter("codOrgao");	     
		if(codOrgao==null)      codOrgao="";
		if(!acao.equals("Z"))
			pkid = codOrgao; 
		String nomOrgao       = req.getParameter("nomOrgao");	     
		if(nomOrgao==null)      nomOrgao=""; 
		String sigOrgao       = req.getParameter("sigOrgao");
		if(sigOrgao==null)      sigOrgao="";		
		String nomEndereco    = req.getParameter("nomEndereco");
		if(nomEndereco==null)   nomEndereco=""; 
		String numEndereco    = req.getParameter("numEndereco");
		if(numEndereco==null)   numEndereco=""; 
		String compEndereco   = req.getParameter("compEndereco");
		if(compEndereco==null)  compEndereco=""; 
		String nomBairro      = req.getParameter("nomBairro");
		if(nomBairro==null)     nomBairro=""; 
		String codCidade      = req.getParameter("codMunicipio");
		if(codCidade==null)     codCidade=""; 
		String nomUF          = req.getParameter("codUfCNHTR");
		if(nomUF==null)         nomUF="";
		String numCEP         = req.getParameter("numCEP");
		if(numCEP==null)        numCEP="";
		String nomContato     = req.getParameter("nomContato");
		if(nomContato==null)    nomContato="";
		String integrada      = req.getParameter("integrada");
		if(integrada==null)     integrada="";
		String vex    		  = req.getParameter("vex");
		if(vex==null)           vex="";
		String numDDD         = req.getParameter("numDDD");
		if(numDDD==null)        numDDD="";
		String numTelefone    = req.getParameter("numTelefone");
		if(numTelefone==null)   numTelefone="";
		String seqEndereco    = req.getParameter("seqEndereco");
		if(seqEndereco==null)   seqEndereco="";
		String seqTelefone    = req.getParameter("seqTelefone");
		if(seqTelefone==null)   seqTelefone="";	
		/*Luciana*/
		String txtEmail       = req.getParameter("txtEmail");
		if(txtEmail == null)
			txtEmail = "";
		String indAutua       = req.getParameter("indAutua");
		if (indAutua == null)
			indAutua = "";
		String indSmitDol     = req.getParameter("indSmitDol");
		if (indSmitDol == null)
			indSmitDol = "";
		

		// Preenche os valores referentes ao set de EnderecoBean
		if (OrgaoBeanId.getEndereco()==null) OrgaoBeanId.setEndereco(new sys.EnderecoBean()) ;	  			
		OrgaoBeanId.getEndereco().setTxtEndereco(nomEndereco);
		OrgaoBeanId.getEndereco().setNumEndereco(numEndereco);
		OrgaoBeanId.getEndereco().setTxtComplemento(compEndereco);
		OrgaoBeanId.getEndereco().setNomBairro(nomBairro);
		OrgaoBeanId.getEndereco().setCodCidade(codCidade);
		OrgaoBeanId.getEndereco().setCodUF(nomUF);
		OrgaoBeanId.getEndereco().setNumCEP(numCEP);
		OrgaoBeanId.getEndereco().setSeqEndereco(seqEndereco);

		// Preenche os valores referentes ao set de TelefoneBean
		if (OrgaoBeanId.getTelefone()==null) OrgaoBeanId.setTelefone(new sys.TelefoneBean()) ;	  			
		OrgaoBeanId.getTelefone().setCodDDD(numDDD);
		OrgaoBeanId.getTelefone().setNumTelResidencial(numTelefone);
		OrgaoBeanId.getTelefone().setSeqTelefone(seqTelefone);
		// Preencher o Bean
		OrgaoBeanId.setPkid(pkid) ;
		OrgaoBeanId.setCodOrgao(codOrgao);							
		OrgaoBeanId.setNomOrgao(nomOrgao);					
		OrgaoBeanId.setSigOrgao(sigOrgao);
		OrgaoBeanId.setNomContato(nomContato);
		OrgaoBeanId.setIntegrada(integrada);
		OrgaoBeanId.setVex(vex);
		OrgaoBeanId.setTxtEmail(txtEmail);
		OrgaoBeanId.setIndAutua(indAutua);
		OrgaoBeanId.setIndSmitDol(indSmitDol);
		String DatCadastro  = req.getParameter("DatCadastro"); 
		if(DatCadastro==null)  DatCadastro  = "";
		if (DatCadastro.trim().length()==0) {
			DatCadastro       = sys.Util.formatedToday().substring(0,10);
			OrgaoBeanId.setDatCadastro(DatCadastro);		
		}
		
		if ("OrgaoConsultaCmd".indexOf(acao) >= 0) {
			return processaOrgaoConsulta(req,OrgaoBeanId,UsrLogado); 	
		}
		if ("Top,Proximo,Anterior,Fim,Pkid".indexOf(acao) >= 0)  {
		   if ( "Pkid".indexOf(acao) >= 0 ) OrgaoBeanId.buscaCodOrgao ( pkid, acao,UsrLogado ) ;
		   else		                        OrgaoBeanId.buscaCodOrgao( nomOrgao, acao,UsrLogado );
		}				
	   	if ("123".indexOf(acao)>=0) {
			//		Setar o(s) objetos para executar a nomacao requerida
		 	if ("1".indexOf(acao)>=0) OrgaoBeanId.setDatCadastro(DatCadastro);		
		 	OrgaoBeanId.setMsgErro("") ;  	      
			if(acao.compareTo("1")==0){
				if(codMunicipio.length()==0) OrgaoBeanId.setMsgErro("O campo 'Cidade' n�o pode ser vazio!");
				else if( !OrgaoBeanId.isInsert() ) 
					OrgaoBeanId.setPkid("0");
			}
			if(acao.compareTo("2")==0){
				OrgaoBeanId.isAltera();	
			}
			if(acao.compareTo("3")==0){
				if( OrgaoBeanId.isDelete() ) 
					acao = "Novo";
			}
		}
		if ("Novo".indexOf(acao)>=0) {	  
			  OrgaoBeanId.setPkid("");
			  OrgaoBeanId.setCodOrgao("") ;
			  OrgaoBeanId.setSigOrgao("");
			  OrgaoBeanId.setNomOrgao("");
			  OrgaoBeanId.setNomContato("");		  
			  OrgaoBeanId.setDatCadastro(sys.Util.formatedToday().substring(0,10));	
			  OrgaoBeanId.getEndereco().setTxtEndereco("");
			  OrgaoBeanId.getEndereco().setNumEndereco("");
			  OrgaoBeanId.getEndereco().setTxtComplemento("");
			  OrgaoBeanId.getEndereco().setNomBairro("");
			  OrgaoBeanId.getEndereco().setNomCidade("");
			  OrgaoBeanId.getEndereco().setCodUF("");
			  OrgaoBeanId.getEndereco().setNumCEP("");
			  OrgaoBeanId.getEndereco().setSeqEndereco("");
			  OrgaoBeanId.getTelefone().setCodDDD("");
			  OrgaoBeanId.getTelefone().setNumTelResidencial("");
			  OrgaoBeanId.getTelefone().setSeqTelefone("");	
			  OrgaoBeanId.setTxtEmail("");
			  OrgaoBeanId.setIndAutua("");
			  OrgaoBeanId.setIndSmitDol("");
			  codUF = "";
			}	  
	    if( acao.equals("buscaMunicipio")|| codUF != null){
	    	MunicipioId.setChecked(OrgaoBeanId.getEndereco().getCodCidade());
	    	codUF = OrgaoBeanId.getEndereco().getCodUF();
	    	MunicipioId.setJ_abrevSist("ACSS");
	    	MunicipioId.setColunaValue("cod_municipio");
	    	MunicipioId.setPopupNome("codMunicipio");
	    	MunicipioId.setPopupString("nom_municipio,SELECT cod_municipio,nom_municipio FROM TSMI_MUNICIPIO WHERE cod_UF = '"+codUF+"' " + "ORDER BY nom_municipio" );
	    	OrgaoBeanId.setPkid("0");
	    }
	 
		req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;
		req.setAttribute("MunicipioId",MunicipioId) ;
		req.setAttribute("codEstado",codUF);
		req.setAttribute("UFCNHId",UFCNHId);
    }
    catch (Exception ue) {
      throw new sys.CommandException("OrgaoCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}
	private String processaOrgaoConsulta(HttpServletRequest req,ACSS.OrgaoBean OrgaoBeanId,ACSS.UsuarioBean UsrLogado) throws sys.CommandException	{
		HttpSession session   = req.getSession() ;
		String acaoConsulta = req.getParameter("acaoConsulta"); 
		if (acaoConsulta==null) acaoConsulta="" ;
		// inicio da consulta
        if (acaoConsulta.equals("")) {	
			// colocar os Beans a serem preservados na sessao
			session.setAttribute("OrgaoBeanId",OrgaoBeanId) ;	
			// Nome para guardar o Bean de Consultana sessao
			session.setAttribute("nomeBeanConsulta","ConsultaOrgaoBeanId") ;						
        }
        // fim da consuta - fica neste comando
        if ((acaoConsulta.equals("pkid")) || (acaoConsulta.equals("Retornar"))) {									        
        	//restaurar da  sessao os Bean preservados
        	String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");			
        	session.removeAttribute(nomeBeanConsulta); 						
        	session.removeAttribute("nomeBeanConsulta"); 
        	try 	{
	        	// recupera o(s) Bean(s) na sessao 
    	    	OrgaoBeanId = (ACSS.OrgaoBean)session.getAttribute("OrgaoBeanId") ;		
        		if (OrgaoBeanId==null) OrgaoBeanId = new ACSS.OrgaoBean() ;
        	}
        	catch (Exception e){
        	  throw new sys.CommandException("OrgaoCommand 003: " + e.getMessage());
        	}			
						
        	// remove o Bean da sessao
        	session.removeAttribute("OrgaoBeanId"); 				
        	//setar os Beans desejados				
        	if (acaoConsulta.equals("pkid")==true) 	{
        		String pkidConsulta = req.getParameter("pkidConsulta"); 		
        		if (pkidConsulta==null) pkidConsulta="" ;
        		OrgaoBeanId.buscaCodOrgao(pkidConsulta,"Pkid",UsrLogado) ;
        		try{
        	    	MunicipioBean MunicipioId = new MunicipioBean() ;	
            		MunicipioId.setChecked(OrgaoBeanId.getEndereco().getCodCidade());
        	    	String codUF = OrgaoBeanId.getEndereco().getCodUF();
        	    	MunicipioId.setJ_abrevSist("ACSS");
        	    	MunicipioId.setColunaValue("cod_municipio");
        	    	MunicipioId.setPopupNome("codMunicipio");
        	    	MunicipioId.setPopupString("nom_municipio,SELECT cod_municipio,nom_municipio FROM TSMI_MUNICIPIO WHERE cod_UF = '"+codUF+"' " + "ORDER BY nom_municipio" );	
            		req.setAttribute("OrgaoBeanId",OrgaoBeanId); 	
            		req.setAttribute("MunicipioId",MunicipioId);
            		req.setAttribute("codEstado",codUF);
        		}
        		catch(Exception e){
        			throw new sys.CommandException("OrgaoCommand 002: " + e.getMessage());
        		}
        	}
        }
		else 		{
			try 	{
				// executar o comando de Consulta
				sys.Command cmd = (sys.Command)Class.forName("ACSS.OrgaoConsultaCmd").newInstance() ;							
				return cmd.execute(req);		
			}
			catch (Exception e){
		      throw new sys.CommandException("OrgaoCommand 002: " + e.getMessage());
			}			
		}
        return jspPadrao;
	}
} 
 
