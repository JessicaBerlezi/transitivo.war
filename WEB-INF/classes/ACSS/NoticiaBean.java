package ACSS;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import TAB.DaoException;

import sys.BeanException;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class NoticiaBean extends sys.HtmlPopupBean {
    
    private int codNoticia;    
    private String dscNoticia;
    private String txtNoticia;
    private String indPrioridade;
    private Date datInicio;
    private Date datFim;
	private UsuarioBean usuario;
	private Date datEnvio;
	private String datInicioNoticia;
	private String datFimNoticia;
	private String msgErro;
	private String indPublicacao;
    private NoticiaBean[] noticias;

	private Vector vetnoticias;
	   
    public NoticiaBean() throws BeanException {        
        super();
        codNoticia       = 0;
        dscNoticia       = "";
        txtNoticia       = "";
        indPrioridade    = "N";
        datInicio        = new Date();
        datFim           = new Date();
		usuario          = new UsuarioBean();
		datEnvio         = new Date();
		msgErro          =  "";		
		datInicioNoticia = "";
		datFimNoticia    = "";
		indPublicacao    = "";
		noticias         = new NoticiaBean[0];
		vetnoticias      = new Vector();
    }
    
    public NoticiaBean(String dscNoticia, String txtNoticia, String indPrioridade, UsuarioBean usuario,
    	Date datEnvio) throws BeanException {
        this();
        this.dscNoticia = dscNoticia;
        this.txtNoticia = txtNoticia;
        this.indPrioridade = indPrioridade;
		this.usuario = usuario;
		this.datEnvio = datEnvio;
    }
	//------------------------------------------------------------------------ 
	public void   setVetNoticias(Vector vetnoticias) { 
		this.vetnoticias = vetnoticias;
		if( vetnoticias == null) this.vetnoticias = new Vector();
	}
	public Vector getVetNoticias(){ return this.vetnoticias; }
    //--------------------------------------------------------------------------   
	public NoticiaBean getVetNoticias(int i) { 
		 return (NoticiaBean)this.vetnoticias.elementAt(i); 
	}   
	//------------------------------------------------------------------------
	public void setMsgErro( Vector vErro ) {
	   for (int j=0; j<vErro.size(); j++)
			  this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
	   this.msgErro = msgErro;
	   if( msgErro == null) this.msgErro = "";
	}
	public String getMsgErro() {	return this.msgErro; }
	//------------------------------------------------------------------------ 
	 public String getIndPublicacao() { return indPublicacao; }
	
	  public void setIndPublicacao(String indPublicacao) {
		this.indPublicacao = indPublicacao;
		if( indPublicacao == null) this.indPublicacao = "";
	  }
	//------------------------------------------------------------------------ 
	  public String getDatInicioNoticia() { return datInicioNoticia; }
	  public void setDatInicioNoticia(String datInicioNoticia) {
		this.datInicioNoticia = datInicioNoticia;
		if( datInicioNoticia == null) this.datInicioNoticia = "";
	  }
	//------------------------------------------------------------------------ 
	  public String getDatFimNoticia() { return datFimNoticia; }
	  public void setDatFimNoticia(String datFimNoticia) {
		this.datFimNoticia = datFimNoticia;
		if( datFimNoticia == null) this.datFimNoticia = "";
	  }
	//------------------------------------------------------------------------
    public int getCodNoticia() { return codNoticia; }
    public void setCodNoticia(int codNoticia) {
        this.codNoticia = codNoticia;        
    }
	//------------------------------------------------------------------------
    public String getIndPrioridade() { return indPrioridade; }
    public void setIndPrioridade(String indPrioridade) {
        this.indPrioridade = indPrioridade;
        if( indPrioridade == null) this.indPrioridade = "";
    }
	//------------------------------------------------------------------------    
    public String getDscNoticia() { return dscNoticia; }
    public void setDscNoticia(String dscNoticia) {
        this.dscNoticia = dscNoticia;
        if( dscNoticia == null) this.dscNoticia = "";
    }
	//------------------------------------------------------------------------
    public String getTxtNoticia() { return txtNoticia;  }
    public void setTxtNoticia(String txtNoticia) {
        this.txtNoticia = txtNoticia;
        if( txtNoticia == null) this.txtNoticia = "";
    }
	//------------------------------------------------------------------------
	public String getDatInicioStr() {
		String retorno = "";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if (datInicio != null) 
			retorno = df.format(datInicio);
		return retorno;
	}
	//------------------------------------------------------------------------
	public String getDatFimStr() {
		String retorno = "";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if (datFim != null) 
			retorno = df.format(datFim);
		return retorno;
	}
    //------------------------------------------------------------------------
    public Date getDatInicio() { return datInicio; }
    public void setDatInicio(Date datInicio) {
        this.datInicio = datInicio;      
        
    }
	//------------------------------------------------------------------------
    public Date getDatFim() { return datFim; }
    public void setDatFim(Date datFim) {
        this.datFim = datFim;
    }
	//------------------------------------------------------------------------    
	public Date getDatEnvio() { return datEnvio; }
	public void setDatEnvio(Date date) {
		datEnvio = date;
	}
	//------------------------------------------------------------------------
	public String getDatEnvioStr() {
		String retorno = "";
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy kk:mm:ss");
		if (datEnvio != null) 
			retorno = df.format(datEnvio);
		return retorno;
	}
	//------------------------------------------------------------------------
	public UsuarioBean getUsuario() { return usuario; }
	public void setUsuario(UsuarioBean bean) {
		usuario = bean;
	}
	//------------------------------------------------------------------------
    public NoticiaBean[] getNoticias() { return noticias; }
    public void setNoticias(NoticiaBean[] noticias) {
        this.noticias = noticias;
        if( noticias == null) this.noticias = new NoticiaBean[0];
    }
	//------------------------------------------------------------------------    
    public static NoticiaBean consulta(int codNoticia) throws DaoException  {        
        return Dao.getInstance().consultaNoticia(codNoticia);
    }
    
	public static NoticiaBean[] consultaNaoLidas(UsuarioBean usuario) throws DaoException {        
		return Dao.getInstance().carregaNoticias(usuario);
	}
    
    public void le(UsuarioBean usuario) throws DaoException {        
        Dao.getInstance().leNoticia(this, usuario);
    }
    
	public boolean lida(UsuarioBean usuario) throws DaoException {        
		return Dao.getInstance().noticiaLida(this, usuario);
	}
	
	public void carrega(UsuarioBean usuario) throws DaoException {        
			this.noticias = Dao.getInstance().carregaNoticias(usuario);
	}
	public void carregaNaoLidas(UsuarioBean usuario) throws DaoException {        
		this.noticias = Dao.getInstance().carregaNoticias(usuario);
	}
   //----------------------------------------------------------------------------
	public static NoticiaBean[] pegaprioridadeNoticias(UsuarioBean usuario) throws DaoException {        
			return Dao.getInstance().pegaprioridadeNoticias(usuario, true);
	}
   //----------------------------------------------------------------------------
	public void MostraNoticiasEntreDatas(UsuarioBean usuario,String dat1,String dat2) throws DaoException {        
		this.noticias = Dao.getInstance().MostraNoticiasEntreDatas(usuario,dat1,dat2);
	}
	//---------------------------------------------------------------------------
	public void CadastraNoticia(UsuarioBean usuario) throws DaoException {   
		Vector vErro = new Vector();     
		Dao.getInstance().CadastraNoticia(this, usuario, vErro);
	}
	//---------------------------------------------------------------------------
//Novo metodo
	public void Publicada(UsuarioBean usuario,int bol) throws DaoException	{
		Dao dao = Dao.getInstance();
		dao.Publicada(this,usuario,bol);
	}
	//---------------------------------------------------------------------------
	public void AlteraNoticia(UsuarioBean usuario) throws DaoException	{
		Vector vErro = new Vector(); 
		Dao dao = Dao.getInstance();
		dao.AlteraNoticia(this,usuario, vErro);
	}

	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------

}