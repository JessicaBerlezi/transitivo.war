package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso -  Consulta Sistema por Usuario<br>
* <b>Description:</b>  Comando para consulta sistemas por usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ConsultaSistemaCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/consultaSistema.jsp";
	private String next;

	public ConsultaSistemaCmd() {
		next = jspPadrao;
	}

	public ConsultaSistemaCmd(String next) {
		this.next = next;
	}


	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = consultaSistema(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaSistemaCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}


	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = next;
		try {      	
			  nextRetorno = consultaSistema(req,nextRetorno);
		}
		catch (Exception ue) {
		  throw new sys.CommandException("ConsultaSistemaCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
  
  public String consultaSistema(HttpServletRequest req, String nextRetorno) throws CommandException{
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
	
			// cria os Beans de Usuario e Perfil, se n�o existir
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
	
			ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();
	
			SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  			
	
			ACSS.ConsultaSistemaBean ConsultaSistemaBeanId =(ACSS.ConsultaSistemaBean) session.getAttribute("ConsultaSistemaBeanId");
			if (ConsultaSistemaBeanId == null)ConsultaSistemaBeanId = new ACSS.ConsultaSistemaBean();
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
				
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "0";
				
			String codUsuario = req.getParameter("codUsuario");
			if (codUsuario == null)	codUsuario = "0";
				
			if (acao.equals("buscaUsuarios")) {
				OrgaoBeanId.setCodOrgao(codOrgao);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
			}
			
			if (acao.equals("buscaDados")){
				OrgaoBeanId.setAtualizarDependente("S");	
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				ConsultaSistemaBeanId.Le_DadosSist(codOrgao, codUsuario,UsrLogado);
			}
			
			if (acao.equals("V")) {
				OrgaoBeanId.setAtualizarDependente("N");
			}
				
			if (acao.equals("Classifica")) {
				OrgaoBeanId.Le_Orgao(codOrgao, 0);
				UsrBeanId.getOrgao().setCodOrgao(codOrgao);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);
				ConsultaSistemaBeanId.Le_DadosSist(codOrgao, codUsuario,UsrLogado);		
				ConsultaSistemaBeanId.Classifica(req.getParameter("ordem"));
				OrgaoBeanId.setAtualizarDependente("S");
			}
	
			if (acao.equals("ImprimeResultConsulta")) {
				//Titulo do Relatorio de impressao
				String tituloConsulta = "CONSULTA SISTEMA POR USU�RIO";
				req.setAttribute("tituloConsulta", tituloConsulta);
				UsrBeanId.setCodUsuario(codUsuario);
				UsrBeanId.Le_Usuario(0);					
				ConsultaSistemaBeanId.setDados(ConsultaSistemaBeanId.getDados());		
				nextRetorno="/ACSS/ConsultaSistemaImp.jsp";
			}
				
			req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			req.setAttribute("SistBeanId", SistBeanId);
			req.setAttribute("UsrBeanId", UsrBeanId);
			session.setAttribute("ConsultaSistemaBeanId",ConsultaSistemaBeanId);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaSistemaCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	  }
}