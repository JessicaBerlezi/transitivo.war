package ACSS;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Sistemas<br>
* <b>Description:</b>  Comando que executa a consulta na Tabela de Sistemas<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class SistemasConsultaCommand extends sys.Command {

  private static final String jspPadrao="/ACSS/Sistema.jsp";
  private String next;
  private String nomConsulta;  
  
  public SistemasConsultaCommand() {
	//TROCAR para os valores correspondentes da Funcao
    next             = jspPadrao ;
	nomConsulta	     = "Tabela de Sistemas" ;
	//FIM
  }

  public SistemasConsultaCommand(String next) {
	this()  ;
    this.next = next;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      							
	    // resgatar da sessao o Bean de consulta enviado, se houver
	    HttpSession session = req.getSession() ;
		String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");		
	    sys.ConsultaBean ConsultaBeanId = (sys.ConsultaBean)session.getAttribute(nomeBeanConsulta) ;		
	    if (ConsultaBeanId==null) ConsultaBeanId = new sys.ConsultaBean() ;
		
		// obtem e valida os parametros recebidos
		String pkidConsulta = req.getParameter("pkidConsulta"); 		
		if (pkidConsulta==null) pkidConsulta="" ;	
		String acaoConsulta = req.getParameter("acaoConsulta"); 
		if (acaoConsulta==null) acaoConsulta="" ;
		String acao = req.getParameter("acao"); 
		if (acao==null) acao="" ;
						
		// se acaoConsulta � nula a origem do comando foi a funcao (Sistemas)
        if (acaoConsulta.equals("")) {			
	        // preenche o Bean de Consulta com os prametros
	        ConsultaBeanId.setCmdOrigem(acao);    						
	        ConsultaBeanId.setNomConsulta(nomConsulta);    
	        ConsultaBeanId.setMaxReg(20) ;
		//TROCAR para os parametros correspondentes da Funcao		  	  	    		 	
			String nomAbrev    = req.getParameter("nomAbrev_consulta");
			if(nomAbrev==null) nomAbrev="";  		  	  	    		 	
			String nomSistema    = req.getParameter("nomSistema_consulta");
			if(nomSistema==null) nomSistema="";  		  	  	    		 	
			String ordemSistema = req.getParameter("ordemSistema_consulta");		
			if(ordemSistema==null) ordemSistema="Nome Sistema";  		  	  	    		 	
		// FIM
		//TROCAR para o Dao correspondente
			Dao dao = Dao.getInstance();
			String[][] wk = dao.SistemaConsultaMontaSelect(ordemSistema,nomAbrev,nomSistema);
			// FIM	
				ConsultaBeanId.setParam0(wk[0][0])    ;
				ConsultaBeanId.setParam1(wk[1][0])    ;						
				ConsultaBeanId.setExprOrdem(wk[2][0]) ;
				ConsultaBeanId.setCmdSelect(wk[3][0]) ;    
				ConsultaBeanId.setColunas(wk[4]);
				ConsultaBeanId.setCabecalho(wk[5]);	
				ConsultaBeanId.setColPerct(wk[6]);    
				ConsultaBeanId.setDirecao("");    	    					    			
			}

		// a origem foi o consulta.jsp
		else {
	        if (acaoConsulta.equals("ProxAnt")) {			
	        	// setar a direcao Proximos ou Anteriores
	        	if ("ProxAnt".indexOf(pkidConsulta)>=0)	ConsultaBeanId.setDirecao(pkidConsulta) ;			
	        }
        }      

		// preparar para sys/Consulta
		if ((acaoConsulta.equals("")) || (acaoConsulta.equals("ProxAnt"))) {			
			// executar o select e prepar o resultado no Bean
			ConsultaBeanId.setTabRes();							
			// coloca o Bean na req e na sessao
			// o nome TBconsId � obrigatorio pois sys.consulta.jsp o utiliza
			session.setAttribute(nomeBeanConsulta,ConsultaBeanId); 
			req.setAttribute("TBconsId",ConsultaBeanId); 			
			// o destino sera sempre sys.Consulta
			nextRetorno = "/sys/Consulta.jsp" ;				
		}
    }
    catch (Exception ue) {
      throw new sys.CommandException("SistemasConsultaCommand: " + ue.getMessage());
    }
	return nextRetorno;
}

}