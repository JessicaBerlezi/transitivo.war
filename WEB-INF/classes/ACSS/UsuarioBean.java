package ACSS;

import java.util.Vector;

import javax.servlet.http.Cookie;

import sys.BeanException;
import sys.CommandException;
import sys.Util;

import TAB.DaoException;


/**
 * <b>Title:</b>        Controle de Acesso - Bean de Usuarios<br>
 * <b>Description:</b>  Bean dados dos Usuarios - Tabela de Usuarios<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Sergio Monteiro
 * @version 1.0
 * @Updates
 */

public class UsuarioBean extends sys.HtmlPopupBean {
    
    private String codUsuario;
    private String nomUsuario;
    private String numCpf;
    private String nomUserName;
    private String codSenha;
    private OrgaoBean   orgao; 
    private String codOrgaoAtuacao ;
    private String sigOrgaoAtuacao ;
    private String codOrgaoDestino;
    
    private String datValidade;
    private String horaValidade;
    
    private String codUsrResp;
    private String datCadastro;
    private String codUsrRespAlt;
    private String datCadastroAlt;
    
    private String acaoAuditoria;
    private String nomUserNameOper;
    private String datProcOper;
    private String orgaoLotacaoOper;
    private String sigOrgaoLotacaoOper;

    private String datSenhaAlterada;
    private String datBioAlterada;
    
    private String nomPerg;
    private String nomResp;
    private String bloqPor;
    private String indBloqueio;
    
    private String email;
    private String localTrabalho;
    
    private ProtocoloUpoBean protocoloUpoBean;
    
    private Cookie cookieUF;
    private Cookie cookieCodOrgaoLotacao;
    private Cookie cookieCodOrgaoAtuacao;
	private Cookie cookieEnabled;
	private String codUF;
    
    public final String IND_BLOQ_OK= "0";
    public final String IND_BLOQ_3TENTATIVAS = "1";
    public final int MAX_TENTATIVAS = 5 ;
    
	private NoticiaBean[] noticias;
	private AvisoBean[] Avisos;
   
	public int cdNatureza;
   
    public UsuarioBean()   throws sys.BeanException  {
        super();
        setTabela("TCAU_USUARIO");
        setPopupWidth(35);
		noticias =null;
		Avisos =null;
        codUsuario     	  = "0";
        nomUsuario    	  = "";
        numCpf 		      = "";
        nomUserName    	  = "";
        codSenha      	  = "";
        datCadastro       = sys.Util.formatedToday().substring(0,10);
        codUsrResp     	  = "0";
        datCadastroAlt    = sys.Util.formatedToday().substring(0,10);
        codUsrRespAlt  	  = "0";

        acaoAuditoria   = "";
        nomUserNameOper   = "";
        datProcOper   = "";
        orgaoLotacaoOper   = "";
        sigOrgaoLotacaoOper   = "";
        
        datSenhaAlterada   = "XX/XX/XXXX";
        datBioAlterada     = "XX/XX/XXXX";

        orgao      		  = new OrgaoBean() ;
        codOrgaoAtuacao   = ""; 	
        sigOrgaoAtuacao   = ""; 
        codOrgaoDestino   = "";
        try {
			datValidade       = Util.addData(sys.Util.formatedToday().substring(0,10),90) ;
		} catch (Exception e) {
			datValidade=sys.Util.formatedToday().substring(0,10);
		}
        horaValidade	  = "";
        nomPerg           = "";   
        nomResp           = "";
        indBloqueio		  = "";	
        bloqPor			  = "";
        email             = "";
        localTrabalho     = "";
		codUF			  = "";
		
		protocoloUpoBean      		  = new ProtocoloUpoBean() ;
		

    }
	//---------------------------------------------------------
	public void setNoticias(NoticiaBean[] noticias) { this.noticias = noticias;	}
	public NoticiaBean[] getNoticias() { return noticias; }
	
	public void setAvisos(AvisoBean[] Avisos) {	this.Avisos = Avisos; }	
	public AvisoBean[] getAvisos() { return Avisos;	}
	//---------------------------------------------------------
    public void setEmail(String email)  {
        this.email    = email;
        if (email==null) this.email="" ;
    }
    
    public String getEmail()  {
        return this.email;
    }
    
    public void setLocalTrabalho(String localTrabalho)  {
        this.localTrabalho    = localTrabalho;
        if (localTrabalho==null) this.localTrabalho="" ;
    }
    
    public String getLocalTrabalho()  {
        return this.localTrabalho;
    }
    
    
    public void setCodUsuario(String codUsuario)  {
        this.codUsuario=codUsuario ;
        if (codUsuario==null) this.codUsuario="0" ;
    }
    public String getCodUsuario()  {
        return this.codUsuario;
    }
    
    public void setNomUsuario(String nomUsuario)  {
        this.nomUsuario    = nomUsuario;
        if (nomUsuario==null) this.nomUsuario="" ;
    }
    
    public String getNomUsuario()  {
        return this.nomUsuario;
    }
    
    public void setNomUserName(String nomUserName)  {
        this.nomUserName = nomUserName;
        if (nomUserName==null) this.nomUserName="" ;
    }
    
    public String getNomUserName()  {
        return this.nomUserName;
    }
    public void setIndBloqueio(String indBloqueio){
        this.indBloqueio = indBloqueio;
    }
    public String getIndBloqueio(){
        return this.indBloqueio;
    }
    
    public void setNumCpf(String numCpf)  {
        if (numCpf == null)	 numCpf="" ;
        this.numCpf = numCpf ;
    }  
    public String getNumCpf()  {
        return this.numCpf;
    }
    
    public void setNumCpfEdt(String numCpf)  {
        if (numCpf==null)      numCpf = "";
        if (numCpf.length()<14) numCpf += "               ";
        this.numCpf=numCpf.substring(0,3)
        + numCpf.substring(4,7) 
        + numCpf.substring(8,11)
        + numCpf.substring(12,14);
    }  
    public String getNumCpfEdt()  {
        if ("".equals(this.numCpf.trim())) return this.numCpf;
        if (this.numCpf.length()<11) this.numCpf=sys.Util.lPad(this.numCpf.trim(),"0",11);  
        return this.numCpf.substring(0,3)+ "." + this.numCpf.substring(3,6) + "." + this.numCpf.substring(6,9) + "-" + this.numCpf.substring(9,11) ;	
    }
    
    public void setCodSenha(String codSenha)  {
        this.codSenha= codSenha;
        if (codSenha==null) this.codSenha="" ;
    }
    public String getCodSenha() {
        return this.codSenha;
    }
    public void setDatCadastro(String datCadastro)  {
        if (datCadastro==null) datCadastro=" " ;
        if (datCadastro.length() < 10) datCadastro = (datCadastro+"          ").substring(0,10) ;
        this.datCadastro = datCadastro;
    }
    public String getDatCadastro()  {
        return this.datCadastro;
    }
    public void setCodUsrResp(String codUsrResp)  {
        this.codUsrResp=codUsrResp ;
        if (codUsrResp==null) this.codUsrResp="0" ;
    }
    public String getCodUsrResp()  {
        return this.codUsrResp;
    }
    public void setDatCadastroAlt(String datCadastroAlt)  {
        if (datCadastroAlt==null) datCadastroAlt=" " ;
        if (datCadastroAlt.length() < 10) datCadastroAlt = (datCadastroAlt+"          ").substring(0,10) ;
        this.datCadastroAlt = datCadastroAlt;
    }
    public String getDatCadastroAlt()  {
        return this.datCadastroAlt;
    }
    public void setCodUsrRespAlt(String codUsrRespAlt)  {
        this.codUsrRespAlt=codUsrRespAlt ;
        if (codUsrRespAlt==null) this.codUsrRespAlt="0" ;
    }
    public String getCodUsrRespAlt()  {
        return this.codUsrRespAlt;
    }
    
    public void setAcaoAuditoria(String acaoAuditoria)  {
        if (acaoAuditoria==null) acaoAuditoria="" ;
        this.acaoAuditoria = acaoAuditoria;
    }
    public String getAcaoAuditoria()  {
        return this.acaoAuditoria;
    }

    public void setNomUserNameOper(String nomUserNameOper)  {
        if (nomUserNameOper==null) nomUserNameOper="" ;
        this.nomUserNameOper = nomUserNameOper;
    }
    public String getNomUserNameOper()  {
        return this.nomUserNameOper;
    }
    
    public void setDatProcOper(String datProcOper)  {
        if (datProcOper==null) datProcOper="" ;
        this.datProcOper = datProcOper;
    }
    public String getDatProcOper()  {
        return this.datProcOper;
    }
    
    public void setOrgaoLotacaoOper(String orgaoLotacaoOper)  {
        if (orgaoLotacaoOper==null) orgaoLotacaoOper="" ;
        this.orgaoLotacaoOper = orgaoLotacaoOper;
    }
    public String getOrgaoLotacaoOper()  {
        return this.orgaoLotacaoOper;
    }
    
    public void setSigOrgaoLotacaoOper(String sigOrgaoLotacaoOper)  {
        if (sigOrgaoLotacaoOper==null) sigOrgaoLotacaoOper="" ;
        this.sigOrgaoLotacaoOper = sigOrgaoLotacaoOper;
    }
    public String getSigOrgaoLotacaoOper()  {
        return this.sigOrgaoLotacaoOper;
    }

    public void setDatSenhaAlterada(String datSenhaAlterada)  {
        if (datSenhaAlterada==null) datSenhaAlterada="XX/XX/XXXX" ;
        this.datSenhaAlterada = datSenhaAlterada;
    }
    public String getDatSenhaAlterada()  {
        return this.datSenhaAlterada;
    }

    public void setDatBioAlterada(String datBioAlterada)  {
        if (datBioAlterada==null) datBioAlterada="XX/XX/XXXX" ;
        this.datBioAlterada = datBioAlterada;
    }
    public String getDatBioAlterada()  {
        return this.datBioAlterada;
    }
    
    
    public void setOrgao(OrgaoBean orgao)   throws sys.BeanException  {
        this.orgao=orgao ;
        if (orgao==null) this.orgao= new OrgaoBean() ;
    }  
    public OrgaoBean getOrgao()  {
        return this.orgao;
    }
    
    public ProtocoloUpoBean getProtocoloUpoBean() {
		return protocoloUpoBean;
	}
    
	public void setProtocoloUpoBean(ProtocoloUpoBean protocoloUpoBean) throws BeanException {
		this.protocoloUpoBean = protocoloUpoBean;
		if (protocoloUpoBean==null) this.protocoloUpoBean= new ProtocoloUpoBean() ;
	}
	
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao)  {
        this.codOrgaoAtuacao=codOrgaoAtuacao ;
        if (codOrgaoAtuacao==null) this.codOrgaoAtuacao="" ;
    }
    public String getCodOrgaoAtuacao()  {
        return this.codOrgaoAtuacao;
    }
    public void setSigOrgaoAtuacao(String sigOrgaoAtuacao)  {
        this.sigOrgaoAtuacao=sigOrgaoAtuacao ;
        if (sigOrgaoAtuacao==null) this.sigOrgaoAtuacao="" ;
    }
    public String getSigOrgaoAtuacao()  {
        return this.sigOrgaoAtuacao;
    }
    
    public void setCodOrgaoDestino(String codOrgaoDestino)
    {
		this.codOrgaoDestino=codOrgaoDestino ;
		if (codOrgaoDestino==null) this.codOrgaoDestino="" ;
    }
	public String getCodOrgaoDestino()
	{
		return this.codOrgaoDestino;
	}
    
    public void setDatValidade(String datValidade)  {
        if (datValidade==null) datValidade=" " ;
        if (datValidade.length() < 10)
        	try {
    			datValidade       = Util.addData(sys.Util.formatedToday().substring(0,10),90) ;
    		} catch (Exception e) {
    			datValidade=sys.Util.formatedToday().substring(0,10);
    		}
        this.datValidade = datValidade;
    }
    
    public String getDatValidade()  {
        return this.datValidade;
    }
    
    public void setHoraValidade(String horaValidade)  {
        if (horaValidade==null) horaValidade=" " ;
        if (horaValidade.length() < 4) horaValidade = "" ;
        this.horaValidade = horaValidade;
    }
    
    public String getHoraValidade()  {
        return this.horaValidade;
    }
    
    public String getHoraValidadeEdt()  {
        if (this.horaValidade.length() == 4) this.horaValidade = this.horaValidade.substring(0,2)+":"+this.horaValidade.substring(2,4); 
        return this.horaValidade;
    }

    public void setNomPerg(String nomPerg)  {
        this.nomPerg    = nomPerg;
        if (nomPerg==null) this.nomPerg="" ;
    }
    public String getNomPerg()  {
        return this.nomPerg;
    }
    
    public void setNomResp(String nomResp)  {
        this.nomResp    = nomResp;
        if (nomResp==null) this.nomResp="" ;
    }
    public String getNomResp()  {
        return this.nomResp;
    }
    
    public void setBloqueadoPor(String bloqPor){
        this.bloqPor = bloqPor;
        if (bloqPor == null) this.bloqPor = "";
    }
    public String getBloqueadoPor(){
        return this.bloqPor;
    }
    
    public void setcookieUF(Cookie cookieUF){
        this.cookieUF = cookieUF;
    }
    public Cookie getcookieUF(){
        return this.cookieUF;
    }
    
    public void setcookieCodOrgaoLotacao(Cookie cookieCodOrgaoLotacao){
        this.cookieCodOrgaoLotacao = cookieCodOrgaoLotacao;
    }
    public Cookie getcookieCodOrgaoLotacao(){
        return this.cookieCodOrgaoLotacao;
    }
    
    public void setcookieCodOrgaoAtuacao(Cookie cookieCodOrgaoAtuacao){
        this.cookieCodOrgaoAtuacao = cookieCodOrgaoAtuacao;
    }
    public Cookie getcookieCodOrgaoAtuacao(){
        return this.cookieCodOrgaoAtuacao;
    }
    
    
    public void Le_PergResp(String mySist)  {
        try {
            Dao dao = Dao.getInstance();
            dao.UsuarioLePergResp(this,mySist);
        } // fim do try
        catch (Exception e) {
        }	// fim do catch
    }
    
    public void Le_Usuario(int tp)  {
        try {
            Dao dao = Dao.getInstance();
            if (dao.UsuarioLeBean(this,tp)==false) {
                this.codUsuario = "0" ;
                setPkid("0") ;
            }
        } // fim do try
        catch (Exception e) {
            this.codUsuario = "0" ;
            setPkid("0") ;
            setMsgErro("Erro na leitura do Usuario: " + e.getMessage()) ;
        }	// fim do catch
    }
    
    
    public void buscaCodUsuario(String codusu, String Acao,UsuarioBean myUsrLog)  {
        int pos = "Top,Proximo,Anterior,Fim,Pkid".indexOf(Acao) ;
        if (pos < 0) return ;
        try {
            if (pos==25) this.codUsuario = codusu ;
            else this.nomUsuario = codusu;
            Dao dao = Dao.getInstance();
            if (dao.buscaUsuario(this,pos,myUsrLog)==false) {
                this.codUsuario = "0" ;
                setPkid("0") ;
            }
        } // fim do try
        catch (Exception e) {
            setMsgErro("Erro na localiza��o do Usu�rio"+e.getMessage()) ;
            this.codUsuario = "0" ;
            setPkid("0") ;
        }	// fim do catch
    }
    
    public boolean isValid(Vector vErro,int acao) {
        boolean bOk = true ;
        if ((acao==1) && (this.datCadastro.length()<10)) vErro.addElement("Data de Cria��o deve ser no formato dd/mm/aaaa. \n") ;
        if ("0".equals(this.orgao.getCodOrgao()))      vErro.addElement("�rg�o n�o preenchido. \n") ;
        if (this.nomUsuario.length()==0)      vErro.addElement("Nome do Usu�rio n�o preenchido. \n") ;
        if (this.codSenha.length()==0)  	  vErro.addElement("Senha n�o preenchida. \n") ;
        if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),this.datValidade)<0) vErro.addElement("Data de Validade menor que "+sys.Util.formatedToday().substring(0,10)+". \n") ;
        
        return (vErro.size()==0) ;
    }
    
    
    public boolean isInsert() {
        boolean bOk = true ;
        Vector vErro = new Vector() ;
        if (isValid(vErro,1))   	{
            try {
                Dao dao = Dao.getInstance();
                if (dao.UsuarioExiste(this,1)) {
                    vErro.addElement("Usuario j� cadastrado com este Login "+this.nomUserName+" \n") ;
                    bOk = false ;
                }
                else {
                    if (dao.UsuarioInsert(this)) vErro.addElement("Usuario incluido: " +this.nomUsuario+" \n") ;
                    else 						 vErro.addElement("Usuario n�o incluido: " +this.nomUsuario+" \n") ;
                }
            }
            catch (Exception e) {
                vErro.addElement("Usuario n�o incluido: " +this.nomUsuario+" \n") ;
                vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
                bOk = false ;
            }	// fim do catch
        }
        setMsgErro(vErro) ;
        return bOk;
    }
    
    
    public boolean isAltera() {
        boolean bOk = true ;
        Vector vErro = new Vector() ;
        if (isValid(vErro,2))   	{
            try 	{
                Dao dao = Dao.getInstance();
                if (dao.UsuarioExiste(this,0)==false) {
                    vErro.addElement("Usuario n�o cadastrado com "+this.nomUserName+" \n") ;
                    bOk = false ;
                }
                else {
                    if (dao.UsuarioExisteOutro(this)) {
                        vErro.addElement("Existe outro Usuario cadastrado com Login: "+this.nomUserName+" \n") ;
                        bOk = false ;
                    }
                    else {
                        if (dao.UsuarioUpdate(this))
                            vErro.addElement("Usuario alterado: " +this.nomUserName+" \n") ;
                        else vErro.addElement("Usuario n�o alterado: " +this.nomUserName+" \n") ;
                    }
                }
            }
            catch (Exception e) {
                vErro.addElement("Usuario n�o alterado: " +this.nomUserName+" \n") ;
                vErro.addElement("Erro na altera��o: "+e.getMessage()+" \n");
                bOk = false ;
            }// fim do catch
        }
        setMsgErro(vErro) ;
        return bOk;
    }
    public boolean isDelete() {
        boolean bOk = true ;
        Vector vErro = new Vector() ;
        try {
            Dao dao = Dao.getInstance();
            if (dao.UsuarioExiste(this,0)==false) {
                vErro.addElement("Usuario n�o cadastrado com "+this.nomUserName+" \n") ;
                bOk = false ;
            }
            else {
                if (dao.UsuarioDelete(this))
                    vErro.addElement("Usuario excluido " +this.nomUserName+" \n") ;
                
                else vErro.addElement("Usuario n�o excluido: " +this.nomUserName+" \n") ;
            }
        }
        catch (Exception e) {
            vErro.addElement("Usuario n�o excluido: " +this.nomUserName+" \n") ;
            vErro.addElement("Erro na exclus�o: "+e.getMessage()+" \n");
            bOk = false ;
        }	// fim do catch
        setMsgErro(vErro) ;
        return bOk;
    }
    
    public boolean isTrocaSenha(Vector vErro,String newSenha,String digSenha,String atualSenha) {
        boolean bOk = false  ;
        try {
            Dao dao = Dao.getInstance();
            if (dao.UsuarioExiste(this,0)==false) {
                vErro.addElement("Usuario n�o cadastrado com "+this.nomUserName+" \n") ;
            }
            else {
                if (isTrocaValid(vErro,newSenha,digSenha,atualSenha))   {
                    if (dao.UsuarioUpdateSenha(this,digSenha)) {
                        bOk = true ;
                        this.codSenha = digSenha ;
                        vErro.addElement("Senha alterada com sucesso para o Usu�rio " +this.nomUserName+" \n") ;
                    }
                    else vErro.addElement("Senha n�o alterada: " +this.nomUserName+" \n") ;
                }
            }
        }
        catch (Exception e) {
            vErro.addElement("Senha n�o alterada: " +this.nomUserName+" \n") ;
            vErro.addElement("Erro: "+e.getMessage()+" \n");
        }// fim do catch
        setMsgErro(vErro) ;
        return bOk;
    }
    
    public boolean isTrocaValid(Vector vErro, String newSenha,String digSenha,String atualSenha) {
        if (newSenha.length()==0)              vErro.addElement("Nova Senha n�o preenchida. \n") ;
        if (digSenha.length()==0)              vErro.addElement("Senha redigitada n�o preenchida. \n") ;
        if (newSenha.equals(digSenha)==false)  vErro.addElement("Senha redigitada n�o confere com a nova Senha. \n") ;
        if (newSenha.equals(atualSenha))       vErro.addElement("Nova Senha identica a atual. \n") ;
        try {
            String hashSenha = "";
            String chashSenha = "";
            String indSituacao = "";
            if (this.getCodUsuario().equals("0"))	{
                vErro.addElement("Usu�rio n�o identificado  \n") ;
            }
            else 	{
                Dao dao = Dao.getInstance();
                if (dao.UsuarioLeSenha(this.codUsuario,atualSenha)==false)   {
                    vErro.addElement("Senha atual n�o confere  \n");
                }
            }
        }
        catch (Exception e) {
            vErro.addElement("Senha n�o alterada: " +this.nomUserName+" \n") ;
            vErro.addElement("Erro: "+e.getMessage()+" \n");
        }// fim do catch
        return vErro.size()==0;
    }
    
    
    public boolean isSenhaValid(String senha){
        Vector vErro = new Vector() ;
        setMsgErro("") ;
        try {
            Dao dao = Dao.getInstance();		
            if (dao.verificaIndBloqueio(this)== false){		
                int    dtValid    = 0 ;
                String wk = this.datValidade.substring(6,10)+this.datValidade.substring(3,5)+this.datValidade.substring(0,2);
                try {   dtValid = Integer.parseInt(wk); }
                catch (Exception e) { }
                int    hoje       = 20091231 ;
                String dtHoje     = sys.Util.formatedToday() ;
                dtHoje = dtHoje.substring(6,10)+dtHoje.substring(3,5)+dtHoje.substring(0,2);
                try {   hoje = Integer.parseInt(dtHoje); }
                catch (Exception e) { }       															;
                
                if (this.getCodUsuario().equals("0"))	{
                    vErro.addElement("Usu�rio n�o autorizado  \n") ;
                }
                else {
                    if (dao.UsuarioLeSenha(this.codUsuario,senha)==false)   {
                        vErro.addElement("Senha inv�lida  \n") ;
                    }
                    if (hoje > dtValid)	vErro.addElement("Usu�rio com data de Validade expirada  \n") ;
                }
            }
            else{
                vErro.addElement("Sua senha est� bloqueada. \n Consulte o administrador do sistema para desbloqueio. \n") ;
            }	
        }
        catch (Exception e) {
            vErro.addElement("Senha inv�lida (Exception)  \n") ;
        }
        setMsgErro(vErro) ;
        return (vErro.size()==0) ;
    }
    
    public void UsuarioPerfil(PerfilBean myPerf) {
        String codPerf ="0" ;
        try {
            Dao dao = Dao.getInstance();
            dao.UsuarioPerfil(this,myPerf);
        }
        catch (Exception e) { 	}
        return ;
        
    }
    //--------------------------------------------------------------------------
    //Busca todos usuarios
    public String getUsuarios(){
        String myUsrs = "" ; 
        try    {
            Dao dao = Dao.getInstance();		
            myUsrs = dao.getUsuarios(this,1) ;
        }
        catch(Exception e){
        }	
        return myUsrs ;
    }
    
    //Busca Usuarios Bloqueados
    public String getUsuariosBloqueados(){
        String myUsrs = "" ; 
        try    {
            Dao dao = Dao.getInstance();		
            myUsrs = dao.getUsuarios(this,2) ;
        }
        catch(Exception e){
        }	
        return myUsrs ;
    }
    
    
    
    //bloqueia senha do usuario ap�s erro nas 3 tentativas
    public boolean bloqueiaSenha(){
        boolean ok = false;
        Vector vErro = new Vector() ;
        String motivo ="BLOQUEIO AUTOMATICO DO SISTEMA";
        String nomUserName = "LOGIN-SMIT";
        try{
            Dao dao = Dao.getInstance();
            if(dao.bloquearSenha(this,motivo,nomUserName)==true) ok = true;
            else vErro.addElement("Erro ao bloqueiar Senha do usu�rio: " +this.nomUserName+" \n") ;
        }
        catch(Exception e){
            vErro.addElement("Erro ao bloqueiar Senha do usu�rio: " +this.nomUserName+" \n") ;
        }
        return ok;
    }
    
    
    //Verifica se senha do usuario est� bloqueada
    public boolean verificaSenhaBloqueada() throws DaoException{
        boolean ok = false;
        Vector vErro = new Vector() ;
        Dao dao = Dao.getInstance();
        if (dao.verificaIndBloqueio(this)== true){
            vErro.addElement("Sua senha est� bloqueada. \n Consulte o administrador do sistema para desbloqueio. \n") ;
            ok = true;
        }	
        return ok;	
    }
    
    public boolean possuiInformacao(UsuarioFuncBean usuarioBean) 
    {
        boolean possui = false;		
        try {
            if (AvisoBean.consultaNaoLidas(this, usuarioBean).length > 0)
                possui = true;
            if (NoticiaBean.consultaNaoLidas(this).length > 0)
                possui = true;
        } catch (Exception e) {
            possui = false;	
        }			
        return possui;
    }
	//--------------------------------------------------------------------------
	public String prioridade(UsuarioFuncBean usuarioBean) 
	 {
		noticias = null;
		Avisos = null;
		String str="N";
		int num=0;
  	 try {
  	 	      if( AvisoBean.pegaprioridadeAviso(this, usuarioBean).length > 0 ) {
  	 	          num = 1;
				  setAvisos( AvisoBean.pegaprioridadeAviso(this, usuarioBean) );
  	          }
  	 	      if( NoticiaBean.pegaprioridadeNoticias(this).length > 0 ) {
  	 	          num = 1;
  	 	          setNoticias(NoticiaBean.pegaprioridadeNoticias(this));
  	 	      }

              if (num == 1) str = "A";    
              else str="N";
    	 }
		 catch (Exception e) 
		 {
		 }			

       return str;
	 }
	 
	public void setCookieEnabled(Cookie cookieEnabled) {
		this.cookieEnabled = cookieEnabled;
	}
	
	public Cookie getCookieEnabled() {
		return cookieEnabled;
	}
	
	public void setCodUF(String codUF) {
		this.codUF = codUF;
		if (codUF == null)	this.codUF = "";
	}
	
	public String getCodUF() {
		return codUF;
	}
	
}