package ACSS;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
* <b>Title:</b>        	SMIT - Bean de Orgaos<br>
* <b>Description:</b>  	Bean dados dos Orgaos - Tabela de Orgaos<br>
* <b>Copyright:</b>    	Copyright (c) 2012<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Luiz Medronho
* @version 				1.0
* @Updates
*/

public class ProtocoloUpoBean extends sys.HtmlPopupBean { 

	private String codProtocoloUpo;
	private String nomProtocoloUpo;
	private String nomAbrevProtocoloUpo;
	private Connection conn;
	private ArrayList listProtocolosUpo;
		
	public ProtocoloUpoBean()   throws sys.BeanException {

	  	super();
	  	setTabela("TSMI_PROT_UPO");	
	  	setPopupWidth(29);
	  	codProtocoloUpo	= "";	
	  	nomProtocoloUpo	= "";
	  	nomAbrevProtocoloUpo	= "";
	  	listProtocolosUpo  = new ArrayList();
	}
	
	
	public void setListProtocolosUpo(ArrayList listProtocolosUpo) {
		this.listProtocolosUpo = listProtocolosUpo;
	}
	
	public ArrayList getListProtocolosUpo() {
		return listProtocolosUpo;
	}
	

	

	public String getCodProtocoloUpo() {
		return codProtocoloUpo;
	}


	public void setCodProtocoloUpo(String codProtocoloUpo) {
		this.codProtocoloUpo = codProtocoloUpo;
		if (codProtocoloUpo==null) this.codProtocoloUpo="" ;
	}


	public String getNomProtocoloUpo() {
		return nomProtocoloUpo;
		
	}


	public void setNomProtocoloUpo(String nomProtocoloUpo) {
		this.nomProtocoloUpo = nomProtocoloUpo;
		if (nomProtocoloUpo==null) this.nomProtocoloUpo="" ;
	}


	public String getNomAbrevProtocoloUpo() {
		return nomAbrevProtocoloUpo;
	}


	public void setNomAbrevProtocoloUpo(String nomAbrevProtocoloUpo) {
		this.nomAbrevProtocoloUpo = nomAbrevProtocoloUpo;
		if (nomAbrevProtocoloUpo==null) this.nomAbrevProtocoloUpo="" ;
	}

	public void setConn(Connection conn)  {
		this.conn = conn;
	}  
	public Connection getConn()  {
		return this.conn;
	}


	public void buscaCodProtUpo(String codusu, String Acao,ACSS.UsuarioBean myUsrLog)  {
		int pos = "Top,Proximo,Anterior,Fim,Pkid".indexOf(Acao) ;
		if (pos < 0) return ;
		try {
			if (pos==25) this.codProtocoloUpo = codusu ;
			else this.nomProtocoloUpo = codusu;
			Dao dao = Dao.getInstance();
			if (dao.buscaProtocoloUpo(this,pos,myUsrLog)==false) {
				this.codProtocoloUpo = "" ;
				setPkid("0") ;
	        }
		} // fim do try
	    catch (Exception e) {
			setMsgErro("Erro na localiza��o do �rg�o"+e.getMessage()) ;
     		this.codProtocoloUpo = "" ;
	    	setPkid("0") ;
    	}	// fim do catch
	}
	
	public boolean isValid(Vector vErro,int acao) {
		boolean bOk = true ;
		if (this.nomProtocoloUpo.length()==0)  vErro.addElement("Nome do Protocolo UPO n�o preenchido. \n") ;
		return (vErro.size()==0) ;
	}
	
  /**
   * M�todo para inserir o protocolo upo na base de dados
   * Valida primeiro se os dados est�o preenchidos.
   */
  public boolean isInsert() {
  	boolean bOk = true ;
  	Vector vErro = new Vector() ;
  	if (isValid(vErro,1))   	{
  		try {
  			Dao dao = Dao.getInstance();
  			if (dao.protocoloUpoExiste(this,1)) {
  				vErro.addElement("Protocolo UPO j� cadastrado: "+this.nomProtocoloUpo+" \n") ;
  				bOk = false ;
  			}
  			else {
  				if (dao.ProtocoloUpoInsert(this,vErro))
  					vErro.addElement("Protocolo UPO incluido: " +this.nomProtocoloUpo+" \n") ;
  				else 					   
  					vErro.addElement("Protocolo UPO n�o incluido: " +this.nomProtocoloUpo+" \n") ;
  			}
  	   	}
  	    catch (Exception e) {
  	        vErro.addElement("Protocolo UPO n�o incluido: " +this.nomProtocoloUpo+" \n") ;
  	        vErro.addElement("Erro na inclus�o: "+e.getMessage()+" \n");
   			bOk = false ;
  	    }	// fim do catch
  	}
     setMsgErro(vErro) ;
      return bOk;  
  }
  /**
   * M�todo para atualizar os dados do protocolo upo na base de dados
   * Valida primeiro se os dados est�o preenchidos e se o �rg�o existe.
   */
  
  /**
   * M�todo para remover o �rg�o na base de dados
   * Valida se o �rg�o existe.
   */
public boolean isAltera() {
  boolean bOk = true ;
  Vector vErro = new Vector() ;
  if (isValid(vErro,2))   	{
  	try 	{
  		Dao dao = Dao.getInstance();
  		if (dao.protocoloUpoExiste(this,0)==false) {
  			vErro.addElement("Protocolo UPO n�o cadastrado com "+this.nomProtocoloUpo+" \n") ;
  			bOk = false ;
  		}
  		else {
//  			if (dao.OrgaoExisteOutro(this)) {
//  				vErro.addElement("Existe outro �rg�o cadastrado com: "+this.sigOrgao+" \n") ;
//  				bOk = false ;
//  			}
//  			else {
  				if (dao.ProtocoloUpoUpdate(this,vErro))
  					vErro.addElement("Protocolo UPO alterado: " +this.nomProtocoloUpo+" \n") ;
  				else vErro.addElement("Protocolo UPO n�o alterado: " +this.nomProtocoloUpo+" \n") ;
//  	  	    }
  		}
  	}
      catch (Exception e) {
        vErro.addElement("Protocolo UPO n�o alterado: " +this.nomProtocoloUpo+" \n") ;
        vErro.addElement("Erro na altera��o: "+e.getMessage()+" \n");
    	  bOk = false ;
      }// fim do catch
  }
  setMsgErro(vErro) ;
  return bOk;
}


public boolean isDelete() {
  boolean bOk = true ;
  Vector vErro = new Vector() ;
  try {
  	Dao dao = Dao.getInstance();
  	if (dao.protocoloUpoExiste(this,0)==false) {
  		vErro.addElement("Protocolo UPO n�o cadastrado com "+this.nomProtocoloUpo+" \n") ;
  		bOk = false ;
  	}
  	else {
  		if (dao.ProtocoloUpoDelete(this,vErro))
  			vErro.addElement("Protocolo UPO excluido: " +this.nomProtocoloUpo+" \n") ;
  		else  
  			vErro.addElement("Protocolo UPO n�o excluido: " +this.nomProtocoloUpo+" \n") ;
  	 }
  }
  catch (Exception e) {
  	vErro.addElement("Org�o n�o excluido: " +this.nomProtocoloUpo+" \n") ;
  	vErro.addElement("Erro na exclus�o: "+e.getMessage()+" \n");
  	bOk = false ;
  }	// fim do catch
  setMsgErro(vErro) ;
  return bOk;
}


//--------------------------------------------------------------------------
public String getProtocolosUpo(UsuarioBean myUsr){
    String myOrgaos = "" ; 
    try    {
       Dao dao = Dao.getInstance();
	   myOrgaos = dao.getProtocolosUpo(myUsr,"") ;
	}
	catch(Exception e){	}	
	return myOrgaos ;
}


//--------------------------------------------------------------------------
public void Le_ProtocoloUpo(String tpProt, int pos)  {
     try { 
	       Dao dao = Dao.getInstance();
 			if (pos==0) this.codProtocoloUpo=tpProt;		 	
			else 	    this.nomProtocoloUpo=tpProt;	
    	   if ( dao.ProtocoloUpoLeBean(this, pos) == false) {
			    this.codProtocoloUpo	  = "";
				setPkid("0") ;		
           }
     }// fim do try
     catch (Exception e) {
          	this.codProtocoloUpo = "" ;
			setPkid("0") ;		        			
            System.err.println("ProtocoloUpo001: " + e.getMessage());
     }
  }
 
//   public void verificaOrgao(Connection conn) throws sys.BeanException {
//	   Vector vErro = new Vector();
//	   try {
//		   this.setConn(conn);
//		   Dao dao = Dao.getInstance();
//		   if (dao.OrgaoExiste(this,0) == false) {
//			   isInsert();
//		   }
//		   else {
//			   isAltera();
//		   }
//	   }
//	   catch (Exception e) {
//		   throw new sys.BeanException("Erro ao efetuar verifica��o: " + e.getMessage());
//	   }
//	   setMsgErro(vErro);
//   }

//   public int contador(int cont) {
//	   cont += 1;
//	   return cont;
//   }

}