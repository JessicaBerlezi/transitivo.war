package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Perfis dos Usuarios/Orgao Atuacao<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Perfil do Usuario<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ConsultaUsuarioCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/consultaUsuario.jsp";
	private String next;

	public ConsultaUsuarioCmd() {
		next = jspPadrao;
	}

	public ConsultaUsuarioCmd(String next) {
		this.next = next;
	}
      
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = consultaUsuario(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaUsuarioCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
  
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = next;
		try {
			
			nextRetorno = consultaUsuario(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaUsuarioCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

  public String consultaUsuario(HttpServletRequest req, String nextRetorno) throws CommandException{
	  try {
       //	cria os Beans, se n�o existir
			 HttpSession session = req.getSession();
			 ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			 if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			 // cria os Beans de Usuario e Perfil, se n�o existir
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

			 ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			 if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			 SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			 if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  	
			
			 ACSS.ConsultaUsuarioBean ConsultaUsuarioBeanId =(ACSS.ConsultaUsuarioBean) session.getAttribute("ConsultaUsuarioBeanId");
			 if (ConsultaUsuarioBeanId == null)ConsultaUsuarioBeanId = new ACSS.ConsultaUsuarioBean();
		

			 // obtem e valida os parametros recebidos					
			 String acao = req.getParameter("acao");
			 if (acao == null) acao = " ";
			
			 String codSistema = req.getParameter("codSistema"); 
			 if(codSistema == null) codSistema = "0";  
			 SistBeanId.setCodSistema(codSistema);
			 
			 if (acao.equals("buscaSistemas")) {
				 OrgaoBeanId.setAtualizarDependente("S");
				 SistBeanId.Le_Sistema(codSistema,0) ;
				
				//L� os dados do usuario
				String tipoUsuario = req.getParameter("tipoUsuario");
				if (tipoUsuario == null) tipoUsuario = "";
				ConsultaUsuarioBeanId.setTipoUsuario(tipoUsuario);
				session.setAttribute("tipoUsuario", tipoUsuario);

				 ConsultaUsuarioBeanId.Le_DadosUsr(codSistema,UsrLogado);
				 if(tipoUsuario.equals("A"))
					 ConsultaUsuarioBeanId.setTipoUsuario("ATIVOS");
					else if (tipoUsuario.equals("I"))
						ConsultaUsuarioBeanId.setTipoUsuario("INATIVOS");
					else 
						ConsultaUsuarioBeanId.setTipoUsuario("ATIVOS /INATIVOS");
			 }
			
			 if (acao.equals("Classifica")) {
				   SistBeanId.Le_Sistema(codSistema,0) ;
				   
				   String tipoUsuarioClass = req.getParameter("tipoUsuarioClass");
				   if (tipoUsuarioClass == null) tipoUsuarioClass = "";
				   ConsultaUsuarioBeanId.setTipoUsuario(tipoUsuarioClass);

				   ConsultaUsuarioBeanId.Le_DadosUsr(codSistema,UsrLogado);
				   ConsultaUsuarioBeanId.Classifica(req.getParameter("ordem"));
				   OrgaoBeanId.setAtualizarDependente("S");
				   if(tipoUsuarioClass.equals("A"))
					 ConsultaUsuarioBeanId.setTipoUsuario("ATIVOS");
					else if (tipoUsuarioClass.equals("I"))
						ConsultaUsuarioBeanId.setTipoUsuario("INATIVOS");
					else 
						ConsultaUsuarioBeanId.setTipoUsuario("ATIVOS /INATIVOS");
		
			 }
			
			 if (acao.equals("ImprimeResultConsulta")) {
				 //Titulo do Relatorio de impressao
				 String tituloConsulta = "CONSULTA USU�RIOS POR SISTEMA";
				 req.setAttribute("tituloConsulta", tituloConsulta);
				
				 ConsultaUsuarioBeanId.setDados(ConsultaUsuarioBeanId.getDados());		
				 nextRetorno="/ACSS/ConsultaUsuarioImp.jsp";
			 }

			 if (acao.equals("V")) {
				 OrgaoBeanId.setAtualizarDependente("N");
			 }
			
			 req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			 req.setAttribute("SistBeanId", SistBeanId);
			 req.setAttribute("UsrBeanId", UsrBeanId);
			 session.setAttribute("ConsultaUsuarioBeanId",ConsultaUsuarioBeanId);
	   }
		 catch (Exception ue) {
			  throw new sys.CommandException("ConsultaUsuarioCmd: "+ ue.getMessage());
		 }			 
		 return nextRetorno;
  }
}