package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ConsultaUsuarioBean extends sys.HtmlPopupBean {

	private String codUsuario;
	private String codPerfil;
	private String codOrgaoAtuacao;
	private String codOrgaoLotacao;
	
	private String sigOrgaoAtuacao; 
	private String sigOrgaoLotacao;
	private String nomDescricao; 
	private String nomUsuario;	
	private List dados;
	
	private String tipoUsuario;
	
	//lista de ordenacao
	private String ordClass ;
	private String ordem ;
	

	public ConsultaUsuarioBean() throws sys.BeanException {
		codUsuario = "";
		codPerfil = "";
		codOrgaoAtuacao = "";
		codOrgaoLotacao = "";
		
		dados = new ArrayList();
		sigOrgaoAtuacao = "";
		sigOrgaoLotacao = "";
		nomDescricao = "";
		nomUsuario = "";
		tipoUsuario = "";
		
		ordClass = "ascendente";
		ordem = "Data";
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() {
		return this.codUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
		if (nomUsuario == null)
			this.nomUsuario = "";
	}
	public String getNomUsuario() {
		return this.nomUsuario;
	}

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
		if (codPerfil == null)
			this.codPerfil = "";
	}
	public String getCodPerfil() {
		return this.codPerfil;
	}

	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if (codOrgaoLotacao == null)
			this.codOrgaoLotacao = "";
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	
	public void setSigOrgaoLotacao(String sigOrgaoLotacao) {
			this.sigOrgaoLotacao = sigOrgaoLotacao;
			if (sigOrgaoLotacao == null)
				this.sigOrgaoLotacao = "";
	}
	public String getSigOrgaoLotacao() {
			return this.sigOrgaoLotacao;
	}
		
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao == null)
			this.codOrgaoAtuacao = "";
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
			this.sigOrgaoAtuacao = sigOrgaoAtuacao;
			if (sigOrgaoAtuacao == null)
				this.sigOrgaoAtuacao = "";
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setTipoUsuario(String tipoUsuario) {
		if(tipoUsuario == null) tipoUsuario = "";
		else this.tipoUsuario = tipoUsuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	
	public ConsultaUsuarioBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaUsuarioBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaUsuarioBean)this.dados.get(i);
	 }
	
	
//	--------------------------  Metodos da Bean ----------------------------------


	public void Le_DadosUsr(String codSistema,UsuarioBean myUsr)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaDadosUsr(this, codSistema, myUsr);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass==null) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
//	  --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "Data";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
		public String getNomOrdem()  {
			String nomOrdem = "�rg�o Lota��o, Usu�rio, �rg�o Atua��o e Perfil" ;
			if (this.ordem.equals("OrgaoLot"))    nomOrdem = "�rg�o Lota��o, �rg�o Atua��o, Usu�rio e Perfil" ;
			if (this.ordem.equals("Usuario"))     nomOrdem = "Usu�rio, Perfil, �rg�o Lota��o e �rg�o Atua��o" ;   
			if (this.ordem.equals("OrgaoAtu"))    nomOrdem = "�rg�o Atua��o, �rg�o Lota��o, Usu�rio e Perfil" ;   
			if (this.ordem.equals("Perfil"))      nomOrdem = "Perfil,Usu�rio, �rg�o Lota��o e �rg�o Atua��o" ; 
			if (this.ordem.equals("tipoUsuario")) nomOrdem = "Usu�rio, Perfil, �rg�o Lota��o e �rg�o Atua��o" ;   
			return nomOrdem+ " ("+getOrdClass()+")" ;
	  }
// --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
			if (ordemSol.equals("OrgaoLot"))    ord = 1 ;
			if (ordemSol.equals("Usuario"))     ord = 2 ;   
			if (ordemSol.equals("OrgaoAtu"))    ord = 3 ;  
			if (ordemSol.equals("Perfil"))      ord = 4 ; 
			if (ordemSol.equals("tipoUsuario")) ord = 5 ; 
			
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			if (ordemSol.equals(getOrdem()))   {
				if ( getOrdClass().equals("ascendente")) {
					trocaasc = false ;
					trocades = true ;
					setOrdClass("descendente"); 
				} 
				else {
					trocaasc = true ;
					trocades = false;
					setOrdClass("ascendente"); 
				}
			}
			else setOrdClass("ascendente");
			setOrdem(ordemSol) ;
			int tam = getDados().size() ;
			ConsultaUsuarioBean tmp = new ConsultaUsuarioBean();
			String orgLot1,orgLot2,orgAtu1,orgAtu2,usr1,usr2,perf1,perf2,tipoUsr1,tipoUsr2 ;  
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					// "�rg�o Lota��o, Usu�rio, �rg�o Atua��o e Perfil" ;
					case 0:
						orgLot1 = getDados(i).getSigOrgaoLotacao();
						orgLot2 = getDados(j).getSigOrgaoLotacao(); 
						orgAtu1 = getDados(i).getSigOrgaoAtuacao() ;
						orgAtu2 = getDados(j).getSigOrgaoAtuacao() ;
						usr1 = getDados(i).getNomUsuario();
						usr2 = getDados(j).getNomUsuario();
						perf1 =getDados(i).getNomDescricao();
						perf2 =getDados(j).getNomDescricao();
						tipoUsr1 = getDados(i).getTipoUsuario();
						tipoUsr2 = getDados(j).getTipoUsuario();
						
						if ((orgLot1+orgAtu1+usr1+perf1+tipoUsr1).compareTo(orgLot2+orgAtu2+usr2+perf2+tipoUsr2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "�rg�o Lota��o" ;
					case 1:
						if(getDados(i).getSigOrgaoLotacao().compareTo(getDados(j).getSigOrgaoLotacao())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
					// "Usu�rio" ;   
					case 2:
						if (getDados(i).getNomUsuario().compareTo(getDados(j).getNomUsuario())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "�rg�o Atua��o" ;   
					case 3:
						if (getDados(i).getSigOrgaoAtuacao().compareTo(getDados(j).getSigOrgaoAtuacao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Perfil" ; 
					case 4:
					    if (getDados(i).getNomDescricao().compareTo(getDados(j).getNomDescricao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Tipo Usuario
					case 5:
						if (getDados(i).getTipoUsuario().compareTo(getDados(j).getTipoUsuario())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
				    // "�rg�o Lota��o" ;	
					default:
					if(getDados(i).getSigOrgaoLotacao().compareTo(getDados(j).getSigOrgaoLotacao())>0) troca=trocaasc;
					 else troca=trocades ;
				   break;
				}	
				if (troca) 	{		
				   tmp = (ConsultaUsuarioBean)getDados(i);
				   getDados().set(i,getDados(j));
				   getDados().set(j,tmp);
				}
			}
		}
	  }	


}