package ACSS;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Usuarios<br>
* <b>Description:</b>  Comando que executa a consulta na Tabela de Usuarios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class UsuariosConsultaCommand extends sys.Command {

  private static final String jspPadrao="/ACSS/Usuarios.jsp" ;     
  private String next;
  private String nomConsulta;  
  
  public UsuariosConsultaCommand() {
	//TROCAR para os valores correspondentes da Funcao
    next             = jspPadrao ;
	nomConsulta	     = "Cadastro de Usuarios" ;
	//FIM
  }

  public UsuariosConsultaCommand(String next) {
	this()  ;
    this.next = next;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      								
	    // resgatar da sessao o Bean de consulta enviado, se houver
	    HttpSession session    = req.getSession() ;
	    ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
	
		String nomeBeanConsulta = (String)session.getAttribute("nomeBeanConsulta");
	    sys.ConsultaBean ConsultaBeanId = (sys.ConsultaBean)session.getAttribute(nomeBeanConsulta) ;		

	    if (ConsultaBeanId==null) 	{
			ConsultaBeanId = new sys.ConsultaBean() ;
	    }
		//PROCESSAMENTO
		// obtem e valida os parametros recebidos
		String pkidConsulta = req.getParameter("pkidConsulta"); 		
		if (pkidConsulta==null) pkidConsulta="" ;	
		String acaoConsulta = req.getParameter("acaoConsulta"); 
		if (acaoConsulta==null) acaoConsulta="" ;
		String acao = req.getParameter("acao"); 
		if (acao==null) acao="" ;
						
		// se acaoConsulta � nula a origem do comando foi o JSP da funcao (Usuarios)
        if (acaoConsulta.equals("")) {			
	        // preenche o Bean de Consulta com os parametros
	        ConsultaBeanId.setCmdOrigem(acao);    			
	        ConsultaBeanId.setNomConsulta(nomConsulta);    
	        ConsultaBeanId.setMaxReg(13) ;
		//TROCAR para os parametros correspondentes da Funcao	
			String sigOrgao    = req.getParameter("sigOrgao_consulta");
			if(sigOrgao==null) sigOrgao=UsrLogado.getSigOrgaoAtuacao();  
			String nomUserName   = req.getParameter("nomUserName_consulta");
			if(nomUserName==null) nomUserName="";  		  	  	    		 	
			String nomUsuario    = req.getParameter("nomUsuario_consulta");
			if(nomUsuario==null) nomUsuario="";  		  	  	    		 	
			String ordemUsuario = req.getParameter("ordemUsuario_consulta");
			if(ordemUsuario==null) ordemUsuario="Nome Usuario";  		  	  	    		 	

		//TROCAR para o Dao correspondente
			Dao dao = Dao.getInstance();
			String[][] wk = dao.UsuarioConsultaMontaSelect(UsrLogado,ordemUsuario,sigOrgao,nomUserName,nomUsuario);
		// FIM
			ConsultaBeanId.setTelaConsLeft("35")    ;
			ConsultaBeanId.setTelaConsTop("90")    ;
			ConsultaBeanId.setParam0(wk[0][0])    ;
			ConsultaBeanId.setParam1(wk[1][0])    ;						
			ConsultaBeanId.setExprOrdem(wk[2][0]) ;
			ConsultaBeanId.setCmdSelect(wk[3][0]) ;    
			ConsultaBeanId.setColunas(wk[4]);
			ConsultaBeanId.setCabecalho(wk[5]);	
			ConsultaBeanId.setColPerct(wk[6]);    
			ConsultaBeanId.setDirecao("");    	    					    			
        }
        else {
	        if (acaoConsulta.equals("ProxAnt")) {			
	        	// setar a direcao Proximos ou Anteriores
	        	if ("ProxAnt".indexOf(pkidConsulta)>=0)	ConsultaBeanId.setDirecao(pkidConsulta) ;			
	        }
        }      
		// preparar para sys/Consulta
		if ((acaoConsulta.equals("")) || (acaoConsulta.equals("ProxAnt"))) {			
			// executar o select e prepar o resultado no Bean
			ConsultaBeanId.setTabRes();							
			// coloca o Bean na req e na sessao
			// o nome TBconsId � obrigatorio pois sys.consulta.jsp o utiliza
			session.setAttribute(nomeBeanConsulta,ConsultaBeanId); 
			req.setAttribute("TBconsId",ConsultaBeanId); 			
			// o destino sera sempre sys.Consulta
			nextRetorno = "/sys/Consulta.jsp" ;				
		}
    }
    catch (Exception ue) {
      throw new sys.CommandException("UsuariosConsultaCommand: " + ue.getMessage());
    }
	return nextRetorno;
}

}