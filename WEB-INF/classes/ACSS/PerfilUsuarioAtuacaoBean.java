package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class PerfilUsuarioAtuacaoBean extends sys.HtmlPopupBean {

	private String codUsuario;
	private String codPerfilOrgao;
	private String codPerfil;
	private String codOrgaoAtuacao;
	
    private String acaoAuditoria;
    private String nomUserNameOper;
    private String datProcOper;
    private String orgaoLotacaoOper;
    private String sigOrgaoLotacaoOper;

	
	private String[] codOrgAtuMarcados;
	private List perfisOrgao;
//	private List perfisSistema;
	private List perfis;  
	private String sigOrgaoAtuacao; 
	private String nomDescricao; 
	

	public PerfilUsuarioAtuacaoBean() throws sys.BeanException {
		codUsuario = "";
		codPerfilOrgao = "";
		codPerfil = "";
		codOrgaoAtuacao = "";

        nomUserNameOper   = "";
        datProcOper   = "";
        orgaoLotacaoOper   = "";
		
		
		codOrgAtuMarcados = new String[0];
		perfis = new ArrayList();  
		perfisOrgao = new ArrayList();
//		perfisSistema = new ArrayList();
		sigOrgaoAtuacao = "";
		nomDescricao = "";
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() {
		return this.codUsuario;
	}

	public void setCodPerfilOrgao(String codPerfilOrgao) {
		this.codPerfilOrgao = codPerfilOrgao;
		if (codPerfilOrgao == null)
			this.codPerfilOrgao = "";
	}
	public String getCodPerfilOrgao() {
		return this.codPerfilOrgao;
	}
	
	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setCodPerfil(String codPerfil) {
		this.codPerfil = codPerfil;
		if (codPerfil == null)
			this.codPerfil = "";
	}
	public String getCodPerfil() {
		return this.codPerfil;
	}

	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao == null)
			this.codOrgaoAtuacao = "";
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	
	public void setSigOrgaoAtuacao(String sigOrgaoAtuacao) {
			this.sigOrgaoAtuacao = sigOrgaoAtuacao;
			if (sigOrgaoAtuacao == null)
				this.sigOrgaoAtuacao = "";
		}
		public String getSigOrgaoAtuacao() {
			return this.sigOrgaoAtuacao;
		}

		
	    public void setAcaoAuditoria(String acaoAuditoria)  {
	        if (acaoAuditoria==null) acaoAuditoria="" ;
	        this.acaoAuditoria = acaoAuditoria;
	    }
	    public String getAcaoAuditoria()  {
	        return this.acaoAuditoria;
	    }

	    public void setNomUserNameOper(String nomUserNameOper)  {
	        if (nomUserNameOper==null) nomUserNameOper="" ;
	        this.nomUserNameOper = nomUserNameOper;
	    }
	    public String getNomUserNameOper()  {
	        return this.nomUserNameOper;
	    }
	    
	    public void setDatProcOper(String datProcOper)  {
	        if (datProcOper==null) datProcOper="" ;
	        this.datProcOper = datProcOper;
	    }
	    public String getDatProcOper()  {
	        return this.datProcOper;
	    }
	    
	    public void setOrgaoLotacaoOper(String orgaoLotacaoOper)  {
	        if (orgaoLotacaoOper==null) orgaoLotacaoOper="" ;
	        this.orgaoLotacaoOper = orgaoLotacaoOper;
	    }
	    public String getOrgaoLotacaoOper()  {
	        return this.orgaoLotacaoOper;
	    }
	    
	    public void setSigOrgaoLotacaoOper(String sigOrgaoLotacaoOper)  {
	        if (sigOrgaoLotacaoOper==null) sigOrgaoLotacaoOper="" ;
	        this.sigOrgaoLotacaoOper = sigOrgaoLotacaoOper;
	    }
	    public String getSigOrgaoLotacaoOper()  {
	        return this.sigOrgaoLotacaoOper;
	    }
		
	public void setCodOrgAtuMarcados(String[] codOrgAtuMarcados) {
		this.codOrgAtuMarcados = codOrgAtuMarcados;
		if (codOrgAtuMarcados == null)
			codOrgAtuMarcados = new String[0];
	}
	public String[] getCodOrgAtuMarcados() {
		return this.codOrgAtuMarcados;
	}

	public void setPerfis(List perfis) {
		this.perfis = perfis;
	}
	public List getPerfis() {
		return this.perfis;
	}
	public void setPerfisOrgao(List perfisOrgao) {
		this.perfisOrgao = perfisOrgao;
	}
	public List getPerfisOrgao() {
		return this.perfisOrgao;
	}

/*	public void setPerfisSistema(List perfisSistema) {
		this.perfisSistema = perfisSistema;
	}
	public List getPerfisSistema() {
		return this.perfisSistema;
	}*/


	public void Le_OrgaoAtuacao(String codSistema,UsuarioBean myUsr,String orgLot)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.PerfilAtuacaoLeBean(this, codSistema, myUsr, orgLot);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}
	
	public void atualPerfil(String[] codPerf,String[] codOrgaoAtuacao,String orgLot,String cSist)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.atualPerfilAtuacao(this, codPerf, codOrgaoAtuacao, orgLot,cSist);
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

}