package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de OperacoesSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar OperacoesSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class OperacoesBiometriaCommand extends sys.Command {
  
  private static final String jspPadrao="/ACSS/operacoesBiometria.jsp" ;  
  private String next;

  public OperacoesBiometriaCommand() {
    next = jspPadrao;
  }

  public OperacoesBiometriaCommand(String next) {
    this.next = next;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		// cria os Beans de Sistema, se n�o existir
    	HttpSession session   = req.getSession() ;		
    	String[] ArraycodOrgao = null;

		ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
		if (SistId==null)        SistId = new ACSS.SistemaBean() ;	  			
		// cria os Beans de Perfil, se n�o existir
		ACSS.PerfilBean PerfBeanId = (ACSS.PerfilBean)req.getAttribute("PerfBeanId") ;
		if (PerfBeanId==null)        PerfBeanId = new ACSS.PerfilBean() ;	  			
		
	    ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			

		// cria os Beans de Usuario e Perfil, se n�o existir
		ACSS.OrgaoBean OrgaoBeanId  = (ACSS.OrgaoBean)req.getAttribute("OrgaoBeanId") ;
		if (OrgaoBeanId==null)        OrgaoBeanId = new ACSS.OrgaoBean() ;	  			
    	
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)          acao =" ";   
	    String codSistema     = req.getParameter("codSistema"); 
		if(codSistema==null)    codSistema ="";   				
	    SistId.setCodSistema(codSistema) ;
	    SistId.Le_Sistema(codSistema,0) ;				  					
	    String codOrgao      = req.getParameter("codOrgao"); 		
		if(codOrgao==null)       codOrgao ="";   
	    String dscOrgao      = req.getParameter("dscOrgao"); 		
		if(dscOrgao==null)       dscOrgao ="";   
		
	    String tipoOperacao      = req.getParameter("tipoOperacao"); 		
		if(tipoOperacao==null)       tipoOperacao ="";   

		PerfBeanId.setCodSistema(codSistema);
	    PerfBeanId.setCodPerfil(codOrgao);
	    PerfBeanId.setNomDescricao(dscOrgao);
	    
		if (PerfBeanId.getCodPerfil().equals("TODOS")){
			String tar= OrgaoBeanId.getOrgaosACSS(UsrLogado);
			ArraycodOrgao = tar.split(",");   
		}	
	    
		String nomUserNameOper      = UsrLogado.getNomUserName();	     
		String codOrgaoLotacaoOper  = UsrLogado.getOrgao().getCodOrgao();

		PerfBeanId.setNomUserNameOper(nomUserNameOper);
		PerfBeanId.setOrgaoLotacaoOper(codOrgaoLotacaoOper);

		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null)       atualizarDependente ="N";   		 
		SistId.setAtualizarDependente(atualizarDependente);			  						  		
	    if ("buscaOperacoes".indexOf(acao)>=0) {	  
		    if ((codOrgao.length()==0) || (codSistema.length()==0)) {		
		    	PerfBeanId.setMsgErro("Sistema ou Perfil n�o selecionado.");
	    	}
			else {
			  SistId.setCodSistema(codSistema) ;
			  SistId.getFuncoes(0,0) ;	
			  SistId.setAtualizarDependente("S");			  	
		      PerfBeanId.setCodSistema(codSistema);
		      PerfBeanId.setCodPerfil(codOrgao);		  
		      if (!PerfBeanId.getCodPerfil().equals("TODOS")) PerfBeanId.getFuncoesBiometria(0,0) ;				  	
			}
	    }	 
		Vector vErro =new Vector(); 
	    if(acao.compareTo("A")==0){
		    if ((codOrgao.length()==0) || (codSistema.length()==0)) {		
		    	PerfBeanId.setMsgErro("Sistema ou �rg�o n�o selecionado.");
		    }
		    else {		
				String[] sigFuncao = req.getParameterValues("perfOperacoes");
				if(sigFuncao==null)  sigFuncao = new String[0];  
	            if (vErro.size()==0) {
					Vector funcoes = new Vector(); 
					for (int i = 0; i<sigFuncao.length;i++) {
						OperacaoBean myOper = new OperacaoBean() ;
						myOper.Le_Operacao(codSistema,sigFuncao[i],1) ;	 											
						funcoes.addElement(myOper) ; 					
					}
					PerfBeanId.setCodSistema(codSistema);
					PerfBeanId.setCodPerfil(codOrgao);	
	  				PerfBeanId.setFuncoes(funcoes) ;  						
					
	  				if (PerfBeanId.getCodPerfil().equals("TODOS"))
	  				    PerfBeanId.isInsertFuncoesBiometriaTodos(ArraycodOrgao, tipoOperacao) ;
	  				else
	  				    PerfBeanId.isInsertFuncoesBiometria() ;
					SistId.setAtualizarDependente("S");			  	
					SistId.setCodSistema(codSistema);
					SistId.getFuncoes(0,0) ;	
//System.err.println("execute SistId.getFuncoes(): "+SistId.getFuncoes().size());
					
				}
				else PerfBeanId.setMsgErro(vErro) ;  	      
		    }
	    }
		req.setAttribute("SistId",SistId) ;	
		req.setAttribute("PerfBeanId",PerfBeanId) ;	
		req.setAttribute("OrgaoBeanId",OrgaoBeanId) ;

    }
    catch (Exception ue) {
      throw new sys.CommandException("OperacoesPerfilCommand 001: " + ue.getMessage());
    }
	return nextRetorno;
}

}