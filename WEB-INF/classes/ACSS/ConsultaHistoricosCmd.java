package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        Controle de Acesso - Consulta de Hist�ricos<br>
* <b>Description:</b>  Consulta de Hist�ricos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class ConsultaHistoricosCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/consultaHistUsuario.jsp";
	private String next;

	public ConsultaHistoricosCmd() {
		next = jspPadrao;
	}

	public ConsultaHistoricosCmd(String next) {
		this.next = next;
	}
      
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = consultaUsuario(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("ConsultaUsuarioCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
  
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = next;
		try {
			
			nextRetorno = consultaUsuario(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaUsuarioCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

  public String consultaUsuario(HttpServletRequest req, String nextRetorno) throws CommandException{
	  try {
       //	cria os Beans, se n�o existir
			 HttpSession session = req.getSession();
			 ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			 if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			 // cria os Beans de Usuario e Perfil, se n�o existir
			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

			 ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			 if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();

			 SistemaBean SistBeanId = (SistemaBean)req.getAttribute("SistBeanId") ;
			 if (SistBeanId==null) SistBeanId = new SistemaBean() ;	  	
			
			 ACSS.ConsultaHistoricosBean ConsultaHistoricosBeanId =(ACSS.ConsultaHistoricosBean) session.getAttribute("ConsultaHistoricosBeanId");
			 if (ConsultaHistoricosBeanId == null)ConsultaHistoricosBeanId = new ACSS.ConsultaHistoricosBean();
		

			 // obtem e valida os parametros recebidos					
			 String acao = req.getParameter("acao");
			 if (acao == null) acao = " ";
			
			 String codOrgao = req.getParameter("codOrgao"); 
			 if(codOrgao == null) codOrgao = "0";  

			 String desOrgao = req.getParameter("desOrgao"); 
			 if(desOrgao == null) desOrgao = "";  

			 
			 session.setAttribute("codOrgao",codOrgao);
			 session.setAttribute("desOrgao",desOrgao);

			 
			 String codSistema = req.getParameter("codSistema"); 
			 if(codSistema == null) codSistema = "0";  
			 SistBeanId.setCodSistema(codSistema);
			 
	    	 String j_sigFuncao = req.getParameter("j_sigFuncao");
	    	 if (j_sigFuncao==null) j_sigFuncao="";
	    	 else
	    		 j_sigFuncao=j_sigFuncao.trim();

			 String datIni = req.getParameter("datIni"); 
			 if(datIni == null) datIni = "";  
			 String datFim = req.getParameter("datFim"); 
			 if(datFim == null) datFim = "";  
			 
			 ConsultaHistoricosBeanId.setDatInicio(datIni);
			 ConsultaHistoricosBeanId.setDatFim(datFim);
			 
	       if ("GER0485".equals(j_sigFuncao)){
	            	nextRetorno = "/ACSS/consultaHistUsuario.jsp";
	    	}   

	        if ("GER0490".equals(j_sigFuncao)){
            	nextRetorno = "/ACSS/ConsultaHistPerfilOrgao.jsp";
    	    }   
			 
	        if ("GER0495".equals(j_sigFuncao)){
            	nextRetorno = "/ACSS/ConsultaHistPerfilFuncao.jsp";
    	    }   
			 
	        if ("GER0110".equals(j_sigFuncao)){
            	nextRetorno = "/ACSS/ConsultaHistCancAuto.jsp";
    	    }   

	        if (acao.equals("buscaSistemasHistUsu")) {
				 OrgaoBeanId.setAtualizarDependente("S");
				 SistBeanId.Le_Sistema(codSistema,0) ;
				//L� os dados do usuario

				 ConsultaHistoricosBeanId.Le_DadosUsr(codSistema,UsrLogado);
			 }
			
			 if (acao.equals("ImprimeResultConsultaHistUsu")) {
				 //Titulo do Relatorio de impressao
				 String tituloConsulta = "CONSULTA HIST�RICO USU�RIOS POR SISTEMA";
				 req.setAttribute("tituloConsulta", tituloConsulta);
				
				 ConsultaHistoricosBeanId.setDados(ConsultaHistoricosBeanId.getDados());		
				 nextRetorno="/ACSS/ConsultaHistUsuarioImp.jsp";
			 }


			 if (acao.equals("buscaSistemasHistPerOrg")) {
				 OrgaoBeanId.setAtualizarDependente("S");
				 SistBeanId.Le_Sistema(codSistema,0) ;
				//L� os dados do usuario

				 ConsultaHistoricosBeanId.Le_DadosPerOrg(codSistema,UsrLogado);
			 }
			
			 if (acao.equals("ImprimeResultConsultaHistPerOrg")) {
				 //Titulo do Relatorio de impressao
				 String tituloConsulta = "CONSULTA HIST�RICO USU�RIOS X ORG�O ATUA��O POR SISTEMA";
				 req.setAttribute("tituloConsulta", tituloConsulta);
				
				 ConsultaHistoricosBeanId.setDados(ConsultaHistoricosBeanId.getDados());		
				 nextRetorno="/ACSS/ConsultaHistPerfilOrgaoImp.jsp";
			 }
			 
			 if (acao.equals("buscaSistemasHistPerFunc")) {
				 OrgaoBeanId.setAtualizarDependente("S");
				 SistBeanId.Le_Sistema(codSistema,0) ;
				//L� os dados do usuario

				 ConsultaHistoricosBeanId.Le_DadosPerFunc(codSistema,UsrLogado);
			 }
			
			 if (acao.equals("ImprimeResultConsultaHistPerFunc")) {
				 //Titulo do Relatorio de impressao
				 String tituloConsulta = "CONSULTA HIST�RICO PERFIL X FUN��O POR SISTEMA";
				 req.setAttribute("tituloConsulta", tituloConsulta);
				
				 ConsultaHistoricosBeanId.setDados(ConsultaHistoricosBeanId.getDados());		
				 nextRetorno="/ACSS/ConsultaHistPerfilFuncaoImp.jsp";
			 }

			 
			 if (acao.equals("buscaSistemasHistCancAuto")) {
				 OrgaoBeanId.setAtualizarDependente("S");

				 ConsultaHistoricosBeanId.Le_DadosCancAuto(codOrgao,UsrLogado);
			 }
			
			 if (acao.equals("ImprimeResultConsultaHistCancAuto")) {
				 //Titulo do Relatorio de impressao
				 String tituloConsulta = "CONSULTA HIST�RICO DE CANCELAMENTO DE AUTO INFRA��O POR ORG�O";
				 req.setAttribute("tituloConsulta", tituloConsulta);
				
				 ConsultaHistoricosBeanId.setDados(ConsultaHistoricosBeanId.getDados());		
				 nextRetorno="/ACSS/ConsultaHistCancAutoImp.jsp";
			 }
			 
			 
			 req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			 req.setAttribute("SistBeanId", SistBeanId);
			 req.setAttribute("UsrBeanId", UsrBeanId);
			 session.setAttribute("ConsultaHistoricosBeanId",ConsultaHistoricosBeanId);
	   }
		 catch (Exception ue) {
			  throw new sys.CommandException("ConsultaHistUsuarioCmd: "+ ue.getMessage());
		 }			 
		 return nextRetorno;
  }
}