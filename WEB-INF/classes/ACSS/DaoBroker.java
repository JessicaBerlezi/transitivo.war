/*
 * Created on 22/09/2004
 *
 */
package ACSS;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import sys.ServiceLocator;
import sys.Util;

/**
* <b>Title:</b>        <br>
* <b>Description:</b>  <br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 
* @version 1.0
* @Updates
*/

public class DaoBroker {
	private static final String MYABREVSIST = "ACSS";
	  
    public DaoBroker() {
    }
    /**************************************
     * 
     * @param codOrgao C�digo da Orgao. 999999 = Todos
     * @param indContinua C�digo do Orgao a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    
    public String Transacao083(OrgaoBean OrgaoId,String codAcao,String indContinua) throws sys.DaoException{
        
        String resultado = "";
        boolean transOK = true;
        BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
        
        //Cr�tica do C�digo do Tipo 		
        if ((OrgaoId.getCodOrgao().equals(null)) ||(OrgaoId.getCodOrgao().trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo do �rg�o n�o preenchido";
        }
        //Cr�tica da A��o 		
        if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo do A��o n�o preenchido";
        }
        if ("12345".indexOf(codAcao) < 0){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: C�digo da A��o inv�lido";
        }
        //Cr�tica do Indicador de Continuidade 		
        if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
            indContinua = "      ";
        }
        if (indContinua.length() != 6){
            transOK=false;
            resultado = "ERR-DaoBroker.Transacao083: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
        }
        
        if (transOK) {
            String parteVar = Util.lPad(OrgaoId.getCodOrgao(),"0",6) + codAcao + indContinua 
            + Util.rPad(OrgaoId.getNomOrgao()," ",30) + Util.rPad(OrgaoId.getEndereco().getTxtEndereco()," ",27)
            + Util.rPad(OrgaoId.getEndereco().getNumEndereco()," ",5) + Util.rPad(OrgaoId.getEndereco().getTxtComplemento()," ",11) 
            + Util.rPad(OrgaoId.getEndereco().getNomBairro()," ",20) + Util.lPad(OrgaoId.getEndereco().getNumCEP(),"0",8)
            + Util.lPad(OrgaoId.getEndereco().getCodCidade(),"0",4) + Util.lPad(OrgaoId.getTelefone().getCodDDD(),"0",3)
            + Util.rPad(OrgaoId.getTelefone().getNumTelResidencial()," ",15) + Util.rPad(OrgaoId.getTelefone().getNumTelResidencial()," ",15)
            + Util.rPad(OrgaoId.getSigOrgao()," ",10) + Util.rPad(OrgaoId.getNomContato()," ",45);  
            ;
            transBRK.setParteFixa("083");
            transBRK.setParteVariavel(parteVar);
            try {
                resultado = transBRK.getResultado();
            } catch (Exception e) {
                throw new sys.DaoException (e.getMessage());
            }
        }
        
        return resultado;
    }
    
    public String vErroBroker(String codErro) throws DaoException {
    	Connection conn = null;
    	ServiceLocator serviceloc = null;
    	try{
    		serviceloc = ServiceLocator.getInstance();
    		conn = serviceloc.getConnection(MYABREVSIST) ;
    		codErro = vErroBroker(conn,codErro);
    	}
    	catch (Exception ex) {
    		throw new DaoException(ex.getMessage());
    	}
    	finally {
    		if (conn != null) {
    		  try { 
    		  	serviceloc.setReleaseConnection(conn); 
    		  }
    		  catch (Exception ey) { }
    		}
    	}
		return codErro;
    }
    
	public String vErroBroker(Connection conn, String codErro) throws DaoException {
		try {
			String dscErro = "";
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())dscErro = rs.getString("dsc_erro");
			rs.close();
			stmt.close();
			return dscErro;
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
	  	}  
	}

}
