package ACSS;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.BeanException;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confiança<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ParamSistemaBean extends sys.HtmlPopupBean {

	private String codSistema;
	private String codParametro;

	private String nomParam;
	private String nomDescricao;
	private String valParametro;
	private String numOrdem;

	private List nomParamSist;
	private List valParamSist;
	private String codSistemaCp;

	public ParamSistemaBean() {
		super();
		setTabela("TSMI_PARAM_SISTEMA");
		setPopupWidth(35);
		codSistema = "";
		codParametro = "";
		nomParam = "";
		nomDescricao = "";
		valParametro = "";
		numOrdem = "";

		nomParamSist = new ArrayList();
		valParamSist = new ArrayList();
		codSistemaCp = "";
	}

	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
		if (codSistema == null)
			this.codSistema = "";
	}
	public String getCodSistema() {
		return this.codSistema;
	}

	public void setCodSistemaCp(String codSistemaCp) {
		this.codSistemaCp = codSistemaCp;
		if (codSistemaCp == null)
			this.codSistemaCp = "";
	}
	public String getCodSistemaCp() {
		return this.codSistemaCp;
	}

	public void setCodParametro(String codParametro) {
		this.codParametro = codParametro;
		if (codParametro == null)
			this.codParametro = "";
	}
	public String getCodParametro() {
		return this.codParametro;
	}

	public void setNomParam(String nomParam) {
		this.nomParam = nomParam;
		if (nomParam == null)
			this.nomParam = "";
	}
	public String getNomParam() {
		return this.nomParam;
	}

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setValParametro(String valParametro) {
		this.valParametro = valParametro;
		if (valParametro == null)
			this.valParametro = "";
	}
	public String getValParametro() {
		return this.valParametro;
	}

	public void setNumOrdem(String numOrdem) {
		this.numOrdem = numOrdem;
		if (numOrdem == null)
			this.numOrdem = "";
	}
	public String getNumOrdem() {
		return this.numOrdem;
	}

	public void Le_ParamSist() {
		try {
			Dao dao = Dao.getInstance();
			if (dao.ParamSistLeBean(this) == false) {
				this.codParametro = "0";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codParametro = "0";
			setPkid("0");
			setMsgErro(
				"Erro na leitura de Parametros do Sistema: " + e.getMessage());
		} // fim do catch 
	}
	//--------------------------------------------------------------------------
	public void PreparaParam(Connection conn)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.PreparaParam(conn, this);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}
	public void PreparaParam()
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.PreparaParam(this);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}	

	//--------------------------------------------------------------------------
	public void setNomParamSist(List nomParamSist) {
		this.nomParamSist = nomParamSist;
	}

	public List getNomParamSist() {
		return this.nomParamSist;
	}
	public String getParamSist(String myNomParam) throws sys.BeanException {
		try {
			String myVal = "";
			String re = "";
			Iterator itx = getNomParamSist().iterator();
			int i = 0;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomParam)) {
					myVal = getValParamSist(i);
					break;
				}
				i++;
			}

			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
	//--------------------------------------------------------------------------
	public void setValParamSist(List valParamSist) {
		this.valParamSist = valParamSist;
	}

	public List getValParamSist() {
		return this.valParamSist;
	}

	public String getValParamSist(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValParamSist().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
 
	//--------------------------------------------------------------------------

	//Verifica Se Parametro Existe e Copia
		public void CopiaParamSistema(SistemaBean mySist, ParamSistemaBean myParam) throws BeanException {
			Vector vErro = new Vector();
			try {
				Dao dao = Dao.getInstance();
				dao.CopiaParam(mySist, myParam);
			}
			catch (Exception e) {
				throw new sys.BeanException(
					"Erro ao efetuar verificação: " + e.getMessage());
			}
			setMsgErro(vErro);
		}
		
		public void atualizaParamSit(ParamSistemaBean myParam) throws BeanException {
			Vector vErro = new Vector();
			try {
				Dao dao = Dao.getInstance();
				dao.ParamUpdate(myParam);
			}
			catch (Exception e) {
				throw new sys.BeanException(
					"Erro ao efetuar verificação: " + e.getMessage());
			}
			setMsgErro(vErro);
		}
	
}