package ACSS;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
 * <b>Title:</b> Controle de Acesso - Consulta Hist�rico dos Par�metros por
 * �rg�o Bean<br>
 * <b>Description:</b> Comando para consultar os historicos de cada inclus�o/altera��o/exclus�o<br>
 * <b>Copyright:</b> Copyright (c) 2006<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author loliveira
 * @version 1.0
 * @Updates
 */

public class ConsultaHistoricoParamOrgCmd extends sys.Command {

    private static final String jspPadrao = "/ACSS/ConsultaHistoricoParamOrg.jsp";

    private String next;

    public ConsultaHistoricoParamOrgCmd() {
        next = jspPadrao;
    }

    public ConsultaHistoricoParamOrgCmd(String next) {
        this.next = next;
    }

    public String execute(HttpServletRequest req) throws sys.CommandException {
        String nextRetorno = next;
        try {
            nextRetorno = consultaHistorico(req, nextRetorno);
        } catch (Exception ue) {
            throw new sys.CommandException("ConsultaHistoricoParamOrgCmd: " + ue.getMessage());
        }
        return nextRetorno;
    }

    public String execute(ServletContext contexto, HttpServletRequest req,
            HttpServletResponse res) throws sys.CommandException {
        String nextRetorno = jspPadrao;
        try {
            nextRetorno = consultaHistorico(req, nextRetorno);
        } catch (Exception ue) {
            throw new sys.CommandException("ConsultaHistoricoParamOrgCmd: " + ue.getMessage());
        }
        return nextRetorno;
    }

    public String consultaHistorico(HttpServletRequest req, String nextRetorno)
            throws CommandException {
        try {
            // cria os Beans, se n�o existir
            HttpSession session = req.getSession();
            UsuarioBean UsrLogado = (UsuarioBean) session.getAttribute("UsuarioBeanId");
            if (UsrLogado == null)
                UsrLogado = new UsuarioBean();

            // cria os Beans de Usuario e Perfil, se n�o existir
            OrgaoBean OrgaoBeanId = (OrgaoBean) req.getAttribute("OrgaoBeanId");
            if (OrgaoBeanId == null)
                OrgaoBeanId = new ACSS.OrgaoBean();

            UsuarioBean UsrBeanId = (UsuarioBean) req.getAttribute("UsrBeanId");
            if (UsrBeanId == null)
                UsrBeanId = new UsuarioBean();

            ConsultaHistoricoParamOrgBean consultHistParamOrgBean = (ConsultaHistoricoParamOrgBean) session.getAttribute("consultHistParamOrgBean");
            if (consultHistParamOrgBean == null)
                consultHistParamOrgBean = new ConsultaHistoricoParamOrgBean();

            // obtem e valida os parametros recebidos
            String acao = req.getParameter("acao");
            if (acao == null)
                acao = "";

            String codOrgao = req.getParameter("codOrgao");
            if ((codOrgao == null) || (codOrgao.equals("")))
                codOrgao = "0";
            
            String dataFim = req.getParameter("datFim");
            if ((dataFim == null) || (dataFim.equals("")))
                dataFim = sys.Util.formatedToday().substring(0, 10);

            String dataIni = req.getParameter("datInicio");
            if ((dataIni == null) || (dataIni.equals("")))
                dataIni = sys.Util.formatedToday().substring(0, 10);

            String numGrupo = req.getParameter("numGrupo");
            if ((numGrupo == null) || (numGrupo.equals("")))
                numGrupo = "";

            // Prepara beans
            consultHistParamOrgBean.setDatIni(dataIni);
            consultHistParamOrgBean.setDatFim(dataFim);
            consultHistParamOrgBean.setAtualizarTela("N");
            consultHistParamOrgBean.setCodOrgao(codOrgao);
            consultHistParamOrgBean.setGrupo(numGrupo);
            UsrBeanId.getOrgao().setCodOrgao(codOrgao);
            OrgaoBeanId.setCodOrgao(codOrgao);

            if (acao.equals("consultaLogParam")) {
                OrgaoBeanId.Le_Orgao(codOrgao,0);
                UsrBeanId.Le_Usuario(0);
                consultHistParamOrgBean.setAtualizarTela("S");
                consultHistParamOrgBean.consultaParamOrg();
                if (consultHistParamOrgBean.getDados().size() == 0)
                    req.setAttribute("semLog","N�O EXISTEM DADOS NESTE PER�ODO DE DIAS.");
            }
            else if (acao.equals("V")) {
                OrgaoBeanId.setCodOrgao(codOrgao);
                UsrBeanId.getOrgao().setCodOrgao(codOrgao);
             }
            else if (acao.equals("Classifica")) {
                OrgaoBeanId.Le_Orgao(codOrgao,0);
                UsrBeanId.Le_Usuario(0);
                consultHistParamOrgBean.Classifica(req.getParameter("ordem"));
                consultHistParamOrgBean.setAtualizarTela("S");
            }
            else if (acao.equals("ImprimeResultConsulta")) {
                OrgaoBeanId.Le_Orgao(codOrgao,0);
                String tituloConsulta = "CONSULTA LOG DE PAR�METROS POR �RG�O <BR>" +
                        "PER�ODO: "+consultHistParamOrgBean.getDatIni()+ " a "+
                        consultHistParamOrgBean.getDatFim();
                req.setAttribute("tituloConsulta", tituloConsulta);

                if (consultHistParamOrgBean.getDados().size() == 0)
                    req.setAttribute("semLog","N�O EXISTEM DADOS NESTE PER�ODO DE DIAS.");
                nextRetorno = "/ACSS/ConsultaHistoricoParamOrgImp.jsp";
            }
            req.setAttribute("OrgaoBeanId", OrgaoBeanId);
            req.setAttribute("UsrBeanId", UsrBeanId);
            session.setAttribute("consultHistParamOrgBean", consultHistParamOrgBean);
        } catch (Exception ue) {
            throw new sys.CommandException("ConsultaHistoricoParamOrgCmd: " + ue.getMessage());
        }
        return nextRetorno;
    }

}