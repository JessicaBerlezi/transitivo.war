package ACSS;


import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Grupo de Par�metros<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Grupos de Par�metros<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luiz Medronho
* @version 1.0
* @Updates
*/

public class GrupoParamCmd extends sys.Command {

  private static final String jspPadrao="/REC/GrupoParam.jsp";    
  private String next;

  public GrupoParamCmd() {
    next = jspPadrao;
  }

  public GrupoParamCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do Usuario, se n�o existir
		GrupoParamBean GrupoParamId = (GrupoParamBean)req.getAttribute("GrupoParamId") ;
		if(GrupoParamId == null) GrupoParamId = new GrupoParamBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null) acao =" ";   
		  
	   
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null || atualizarDependente.equals(""))
		  atualizarDependente ="N";  

		GrupoParamId.setAtualizarDependente(atualizarDependente);
		
		GrupoParamId.getGrupo(20,5) ;

		Vector vErro = new Vector(); 
         
		 
	    if(acao.compareTo("A") == 0){
			String[] codGrupo= req.getParameterValues("codGrupo");
			if(codGrupo == null) codGrupo = new String[0];  
			  
			String[] nomGrupo = req.getParameterValues("dscGrupo");
			if(nomGrupo == null)  nomGrupo = new String[0];         
    
			  
			vErro = isParametros(codGrupo,nomGrupo) ;
						 
            if (vErro.size()==0) {
				Vector grupo = new Vector(); 
				for (int i = 0; i < nomGrupo.length;i++) {
					GrupoParamBean myGrupo = new GrupoParamBean() ;		
					myGrupo.setDscGrupo(nomGrupo[i]);
					myGrupo.setCodGrupo(codGrupo[i]);	  
					grupo.addElement(myGrupo) ; 					
				}
				GrupoParamId.setGrupo(grupo) ;
				GrupoParamId.isInsertGrupo() ;
				GrupoParamId.getGrupo(20,5) ;						
			}
			else GrupoParamId.setMsgErro(vErro) ;  	
	    }
	    
	    if(acao.compareTo("I") == 0){
	    	req.setAttribute("param",GrupoParamId.getGrupo(0,0));
	    	nextRetorno = "/REC/GrupoParamImp.jsp";
	    }
	    
		req.setAttribute("GrupoParamId",GrupoParamId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("GrupoParamCmd 001: " + ue.getMessage()); 
    }
	return nextRetorno; 
  }
  private Vector isParametros(String[] codGrupo,String[] dscGrupo) {
		Vector vErro = new Vector() ;		 
		if (dscGrupo.length!=codGrupo.length) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 
}


