package ACSS;

import java.util.Vector;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de OperacoesSistema<br>
* <b>Description:</b>  Comando para Imprimir as Opera��es de um determinado Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class ImprimirOperacoesCmd extends sys.Command {

    private static final String jspPadrao="/ACSS/ImprimirOperacoes.jsp";  
  private String next;

  public ImprimirOperacoesCmd() {
    next             = jspPadrao ;
  }

  public ImprimirOperacoesCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		  nextRetorno = imprimirOperacao(req,nextRetorno);
    }
    catch (Exception ue) {
      throw new sys.CommandException("ImprimirOperacoesCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }
  

  public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
	try 
	{
		nextRetorno = imprimirOperacao(req,nextRetorno);
	}
	catch (Exception ue) {
	  throw new sys.CommandException("ImprimirOperacoesCmd: " + ue.getMessage());
	}
	return nextRetorno;
  }

  public String imprimirOperacao(HttpServletRequest req, String nextRetorno){
			// verifica se o Bean do Usuario ja existe - se nao cria
			HttpSession session = req.getSession() ;	  
						
			// cria os Beans do Usuario, se n�o existir
			ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
			if (SistId==null)        SistId = new ACSS.SistemaBean() ;	  			
													
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao =" ";
								   
			String codSistema = req.getParameter("codSistema"); 
			if(codSistema==null) codSistema ="";
							    
			SistId.Le_Sistema(codSistema,0);				  									
			Vector vErro =new Vector();
								 
			if(acao.compareTo("Imprimir")==0){
					SistId.getFuncoes(0,0) ;
					nextRetorno="/ACSS/ImprimirOperacoesImp.jsp";
			}
			req.setAttribute("SistId",SistId) ;
			return nextRetorno;
  }

    
 
}