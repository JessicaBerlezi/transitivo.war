package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de OperacoesSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar OperacoesSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class HabilitaOperSistCmd extends sys.Command {

    private static final String jspPadrao="/ACSS/HabilitaOperSist.jsp";  
  private String next;

  public HabilitaOperSistCmd() {
    next             = jspPadrao ;
  }

  public HabilitaOperSistCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		// verifica se o Bean do Usuario ja existe - se nao cria
		HttpSession session = req.getSession() ;	  

		// cria os Beans do Usuario, se n�o existir
		ACSS.SistemaBean SistId = (ACSS.SistemaBean)req.getAttribute("SistId") ;
		if (SistId==null)        SistId = new ACSS.SistemaBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   
	    String codSistema = req.getParameter("codSistema"); 
	    if(codSistema==null)       codSistema ="";  
		
	    String atualizarDependente = req.getParameter("atualizarDependente"); 
	    if(atualizarDependente==null)       atualizarDependente ="N";   		 
	    SistId.Le_Sistema(codSistema,0) ;				  					
		SistId.setAtualizarDependente(atualizarDependente);				
	    if ("buscaSistema".indexOf(acao)>=0) {
	      SistId.getFuncoes(20,5) ;	
		  SistId.setAtualizarDependente("S");	
	    }	 	
	    if(acao.compareTo("A")==0){	
			String[] sigFuncao = req.getParameterValues("sFuncao");
			if(sigFuncao==null)  sigFuncao = new String[0];  	
			String[] ativa = req.getParameterValues("ativa");
			if(ativa==null)  ativa = new String[0];				
			String[] nomDescricao = req.getParameterValues("nomDescricao");
			if(nomDescricao==null)  nomDescricao = new String[0]; 			     			       
			String[] codOperacao = req.getParameterValues("codFuncao");
			if(codOperacao==null)  codOperacao = new String[0]; 			
	
				Vector <OperacaoBean> funcoes = new Vector <OperacaoBean> (); 
				for (int i = 0; i<sigFuncao.length-1;i++) {
					OperacaoBean myOper = new OperacaoBean() ;					
					if(!"".equals(ativa[i])&& !"".equals(nomDescricao[i])){
						myOper.setSigFuncao(sigFuncao[i]);					
						myOper.setNomDescricao(nomDescricao[i]);				  
					    myOper.setCodOperacao(codOperacao[i]) ;	
					    myOper.setHabilita(ativa[i]) ;	
					    funcoes.addElement(myOper) ;
					}
				}
				SistId.setFuncoes(funcoes) ;
				SistId.isHabilitaFuncoes() ;
			    SistId.getFuncoes(20,5) ;						
		
	    }
		req.setAttribute("SistId",SistId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("HabilitaOperSistCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
}
}