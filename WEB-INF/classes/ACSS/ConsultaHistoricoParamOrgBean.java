package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Title:</b> Controle de Acesso - Consulta Hist�rico dos Par�metros por
 * �rg�o Bean<br>
 * <b>Description:</b> Comando para consultar os historicos de cada
 * inclus�o/altera��o/exclus�o<br>
 * <b>Copyright:</b> Copyright (c) 2006<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author loliveira
 * @version 1.0
 * @Updates
 */

public class ConsultaHistoricoParamOrgBean extends sys.HtmlPopupBean {
    private static final String GRUPO_DP = "1";
    
    private static final String GRUPO_1A = "3";
    
    private static final String GRUPO_2A = "4";
    
    private static final String GRUPO_PROC = "2";
    
    private static final String GRUPO_GERAL = "7";
    

    private String codParamOld;

    private String codParamOrgLog;

    private String codParamNew;
    
    private String codOrgao;

    private String nomParamOld;

    private String nomParamNew;
    
    private String nomParam;

    private String valParamOld;

    private String valParamNew;

    private String codOrgOld;

    private String codOrgNew;

    private String nomDescOld;

    private String nomDescNew;

    private String numOrdemOld;

    private String numOrdemNew;

    private String datLog;

    private String nomUsrName;
    
    
    private String ordClass;
    
    private String ordem;
    
    private String datIni;
    
    private String datFim;
    
    private String atualizarTela;
    
    private String grupo;
    
    private List dados;


    public ConsultaHistoricoParamOrgBean() throws sys.BeanException {
        this.atualizarTela = "";
        this.codOrgNew = "";
        this.codOrgOld = "";
        this.codParamNew = "";
        this.codParamOld = "";
        this.codParamOrgLog = "";
        this.codOrgao = "";
        this.dados = new ArrayList();
        this.datLog = "";
        this.datFim = sys.Util.formatedToday().substring(0, 10);
        this.datIni = sys.Util.formatedToday().substring(0, 10);
        this.grupo = "7";
        this.nomDescNew = "";
        this.nomDescOld = "";
        this.nomParamNew = "";
        this.nomParamOld = "";
        this.nomParam = "";
        this.nomUsrName = "";
        this.numOrdemNew = "";
        this.numOrdemOld = "";
        this.ordem = "";
        this.ordClass = "ascendente";
    }

    
    public void setCodOrgNew(String codOrgNew) {
        this.codOrgNew = codOrgNew;
        if(codOrgNew == null)
            this.codOrgNew = "";
    }
    
    public String getCodOrgNew() {
        return codOrgNew;
    }

    public void setCodOrgOld(String codOrgOld) {
        this.codOrgOld = codOrgOld;
        if(codOrgOld == null)
            this.codOrgOld = "";
    }
    
    public String getCodOrgOld() {
        return codOrgOld;
    }

    public void setCodParamNew(String codParamNew) {
        this.codParamNew = codParamNew;
        if(codParamNew == null)
            this.codParamNew = "";
    }

    public String getCodParamNew() {
        return codParamNew;
    }

    public void setCodParamOld(String codParamOld) {
        this.codParamOld = codParamOld;
        if (codParamOld == null)
            this.codParamOld = "";
    }

    public String getCodParamOld() {
        return codParamOld;
    }

    public void setCodParamOrgLog(String codParamOrgLog) {
        this.codParamOrgLog = codParamOrgLog;
        if (codParamOrgLog == null)
            this.codParamOrgLog = "";
    }
    
    public String getCodParamOrgLog() {
        return codParamOrgLog;
    }

    public void setDados(List dados) {
        this.dados = dados;
    }

    public List getDados() {
        return dados;
    }

    public void setDatLog(String datLog) {
        this.datLog = datLog;
        if(datLog == null)
            this.datLog = "";
    }

    public String getDatLog() {
        return datLog;
    }

    public void setNomDescNew(String nomDescNew) {
        this.nomDescNew = nomDescNew;
        if(nomDescNew == null)
            this.nomDescNew = "";
    }

    public String getNomDescNew() {
        return nomDescNew;
    }

    public void setNomDescOld(String nomDescOld) {
        this.nomDescOld = nomDescOld;
        if(nomDescOld == null)
            this.nomDescOld = "";
    }

    public String getNomDescOld() {
        return nomDescOld;
    }

    public void setNomParamNew(String nomParamNew) {
        this.nomParamNew = nomParamNew;
        if(nomParamNew == null)
            this.nomParamNew = "";
    }

    public String getNomParamNew() {
        return nomParamNew;
    }

    public void setNomParamOld(String nomParamOld) {
        this.nomParamOld = nomParamOld;
        if(nomParamOld == null)
            this.nomParamOld = "";
    }

    public String getNomParamOld() {
        return nomParamOld;
    }

    public void setNomUsrName(String nomUsrName) {
        this.nomUsrName = nomUsrName;
        if(nomUsrName == null)
            this.nomUsrName = "-";
    }

    public String getNomUsrName() {
        return nomUsrName;
    }

    public void setNumOrdemNew(String numOrdemNew) {
        this.numOrdemNew = numOrdemNew;
        if(numOrdemNew == null)
            this.numOrdemNew = "";
    }

    public String getNumOrdemNew() {
        return numOrdemNew;
    }

    public void setNumOrdemOld(String numOrdemOld) {
        this.numOrdemOld = numOrdemOld;
        if(numOrdemOld == null)
            this.numOrdemOld = "";
    }

    public String getNumOrdemOld() {
        return numOrdemOld;
    }

    public void setValParamNew(String valParamNew) {
        this.valParamNew = valParamNew;
        if(valParamNew == null)
            this.valParamNew = "";
    }

    public String getValParamNew() {
        return valParamNew;
    }

    public void setValParamOld(String valParamOld) {
        this.valParamOld = valParamOld;
        if(valParamOld == null)
            this.valParamOld = "";
    }

    public String getValParamOld() {
        return valParamOld;
    }

    public void setDatFim(String datFim) {
        this.datFim = datFim;
        if(datFim == null)
            this.datFim = sys.Util.formatedToday().substring(0, 10);
    }
    
    public String getDatFim() {
        return datFim;
    }

    public void setDatIni(String datIni) {
        this.datIni = datIni;
        if(datIni == null)
            this.datIni = sys.Util.formatedToday().substring(0, 10);
    }
    
    public String getDatIni() {
        return datIni;
    }
    
    public void setAtualizarTela(String atualizarTela) {
        this.atualizarTela = atualizarTela;
        if (atualizarTela == null)
            this.atualizarTela = "";
    }   

    public String getAtualizarTela() {
        return atualizarTela;
    }
    
    
    public void setNomParam(String nomParam) {
        this.nomParam = nomParam;
        if(nomParam == null)
            this.nomParam = "";
    }

    public String getNomParam() {
        return nomParam;
    }
    
    
    public void setCodOrgao(String codOrgao) {
        this.codOrgao = codOrgao;
        if(codOrgao == null)
            this.codOrgao = "";
    }
    
    public String getCodOrgao() {
        return codOrgao;
    }

    public ConsultaHistoricoParamOrgBean getDados(int i)
            throws sys.BeanException {
        try {
            if ((i < 0) || (i >= this.dados.size()))
                return (new ConsultaHistoricoParamOrgBean());
        } catch (Exception e) {
            throw new sys.BeanException(e.getMessage());
        }
        return (ConsultaHistoricoParamOrgBean) this.dados.get(i);
    }
 
    public void setOrdClass(String ordClass) {
        this.ordClass = ordClass;
        if (ordClass.equals(""))
            this.ordClass = "ascendente";
    }

    public String getOrdClass() {
        return this.ordClass;
    }

    public void setOrdem(String ordem) {
        this.ordem = ordem;
        if (ordem == null)
            this.ordem = "DAT_SESSAO";
    }

    public String getOrdem() {
        return this.ordem;
    }

    public String getNomOrdem() {
        String nomOrdem = "Data";
        if (this.ordem.equals("NOM_USERNAME"))
            nomOrdem = "Usu�rio";
        if (this.ordem.equals("NOM_DESC_NEW"))
            nomOrdem = "Par�metro";
        if (this.ordem.equals("VAL_PARAM_OLD"))
            nomOrdem = "Valor Anterior";
        if (this.ordem.equals("VAL_PARAM_NEW"))
            nomOrdem = "Valor Atual";
        return nomOrdem + " (" + getOrdClass() + ")";
    }
    
    
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
    
    public String getGrupo() {
        return grupo;
    }
    public String getGrupo(String valor) {
        if(valor.equals(GRUPO_DP))
            grupo = "DEFESA PR�VIA";
        else if(valor.equals(GRUPO_1A))
            grupo = "1� INST�NCIA";
        else if(valor.equals(GRUPO_2A))
            grupo = "2� INST�NCIA";
        else if(valor.equals(GRUPO_PROC))
            grupo = "PROCESSO";
        else if(valor.equals(GRUPO_GERAL))
            grupo = "GERAL";
        return grupo;
    }

    // --------------------------------------------------------------------------
    public void Classifica(String ordemSol) throws sys.BeanException {
        int ord = 0;
        if ((ordemSol == null) || (ordem.equals("")))
            ord = 0;
        if (ordemSol.equals("DAT_LOG"))
            ord = 1;
        if (ordemSol.equals("NOM_DESC_NEW"))
            ord = 2;
        if (ordemSol.equals("NOM_USERNAME"))
            ord = 3;
        if (ordemSol.equals("VAL_PARAM_OLD"))
            ord = 4;
        if (ordemSol.equals("VAL_PARAM_NEW"))
            ord = 5;
 
        boolean troca = false;
        boolean trocaasc = true;
        boolean trocades = false;
        if (ordemSol.equals(getOrdem())) {
            if (getOrdClass().equals("ascendente")) {
                trocaasc = false;
                trocades = true;
                setOrdClass("descendente");
            } else {
                trocaasc = true;
                trocades = false;
                setOrdClass("ascendente");
            }
        } else
            setOrdClass("ascendente");
        setOrdem(ordemSol);
        int tam = getDados().size();
        ConsultaHistoricoParamOrgBean tmp = new ConsultaHistoricoParamOrgBean();
        String nom_username1, nom_username2, paramDsc1, paramDsc2, dat1, dat2,  
           val_ant1, val_ant2, val_atu1, val_atu2;
        for (int i = 0; i < tam; i++) {
            for (int j = i + 1; j < tam; j++) {
                troca = false;
                // compara os campos
                switch (ord) {
                case 0:
                    paramDsc1 = getDados(i).getNomDescNew();
                    paramDsc2 = getDados(j).getNomDescNew();
                    dat1 = getDados(i).getDatLog();
                    dat2 = getDados(j).getDatLog();    
                    nom_username1 = getDados(i).getNomUsrName();
                    nom_username2 = getDados(j).getNomUsrName();
                    val_ant1 = getDados(i).getValParamOld();
                    val_ant2 = getDados(j).getValParamOld();
                    val_atu1 = getDados(i).getValParamNew();
                    val_atu2 = getDados(j).getValParamNew();

                    if ((nom_username1 + val_ant1 + val_atu1 + dat1 + paramDsc1)
                            .compareTo(nom_username2 +val_ant2 + val_atu2 + dat2 + paramDsc2) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;
                // "Data" ;
                case 1:
                    if (getDados(i).getDatLog().compareTo(
                            getDados(j).getDatLog()) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;                    
                // "Par�metro" ;
                case 2:
                    if (getDados(i).getNomDescNew().compareTo(
                            getDados(j).getNomDescNew()) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;
                // "Usu�rio" ;
                case 3:
                    if (getDados(i).getNomUsrName().compareTo(
                            getDados(j).getNomUsrName()) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;
                // "Valor Anterior" ;
                case 4:
                    if (getDados(i).getValParamOld().compareTo(
                            getDados(j).getValParamOld()) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;
                // "Valor Atual" ;
                case 5:
                    if (getDados(i).getValParamNew().compareTo(
                            getDados(j).getValParamNew()) > 0)
                        troca = trocaasc;
                    else
                        troca = trocades;
                    break;
                }

                if (troca) {
                    tmp = (ConsultaHistoricoParamOrgBean) getDados(i);
                    getDados().set(i, getDados(j));
                    getDados().set(j, tmp);
                }
                
            }
        }
    }

    public void consultaParamOrg() throws TAB.DaoException {
        Dao dao = Dao.getInstance();
        try {
            dao.consultaParamOrg(this);
        } catch (TAB.DaoException e) {
           throw new TAB.DaoException("Erro"+ e.getMessage());
        }
    }
}