package ACSS;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Sistema<br>
* <b>Description:</b>  Comando para Manter os Parametros por Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
* @Updates
*/

public class ParamSistemaCmd extends sys.Command {

	private static final String jspPadrao = "/ACSS/ParamSistema.jsp";
	private String next;

	public ParamSistemaCmd() {
		next = jspPadrao;
	}

	public ParamSistemaCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans do Usuario, se n�o existir
			SistemaBean SistId = (SistemaBean) req.getAttribute("SistId");
			if (SistId == null)	SistId = new SistemaBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";

			String codSistema = req.getParameter("codSistema");
			if (codSistema == null) codSistema = "";

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)
				atualizarDependente = "N";

			SistId.Le_Sistema(codSistema, 0);
			SistId.setAtualizarDependente(atualizarDependente);

			if ("buscaSistema".indexOf(acao) >= 0) {
				SistId.getParametros(20, 6);
				SistId.setAtualizarDependente("S");
			}

			if (acao.compareTo("G") == 0) {
				String codSistemaCp = req.getParameter("codSistemaCp");
				ParamSistemaBean paramId = new ParamSistemaBean();
				paramId.setCodSistemaCp(codSistemaCp);
				paramId.CopiaParamSistema(SistId, paramId);
			}

			Vector vErro = new Vector();

			if (acao.compareTo("A") == 0) {
				String[] nomParametro = req.getParameterValues("nomParametro");
				if (nomParametro == null) nomParametro = new String[0];

				String[] nomDescricao = req.getParameterValues("nomDescricao");
				if (nomDescricao == null) nomDescricao = new String[0];

				String[] codParametro = req.getParameterValues("codParametro");
				if (codParametro == null) codParametro = new String[0];

				String[] numOrdem = req.getParameterValues("numOrdem");
				if (numOrdem == null) numOrdem = new String[0];

				String[] valParametro = req.getParameterValues("valParametro");
				if (valParametro == null) valParametro = new String[0];

				vErro =	isParametros(nomParametro,nomDescricao,codParametro,valParametro,numOrdem);
				if (vErro.size() == 0) {
					Vector param = new Vector();
					for (int i = 0; i < nomParametro.length; i++) {
						ParamSistemaBean myParam = new ParamSistemaBean();
						myParam.setNomParam(nomParametro[i]);
						myParam.setNomDescricao(nomDescricao[i]);
						myParam.setCodSistema(codSistema);
						myParam.setCodParametro(codParametro[i]);
						myParam.setValParametro(valParametro[i]);
						myParam.setNumOrdem(numOrdem[i]);
						param.addElement(myParam);
					}
					SistId.setParametros(param);
					SistId.isInsertParametros();
					SistId.getParametros(20, 6);
				}
				else
					SistId.setMsgErro(vErro);
			}
			if ("I".equals(acao)) {
				SistId.getParametros(100,0);
				String tituloConsulta = "PAR�METROS DE SISTEMA   - ";
				req.setAttribute("tituloConsulta",tituloConsulta);
				nextRetorno = "/ACSS/ParamSistImp.jsp";	
			}			
			req.setAttribute("SistId", SistId);
		}
		catch (Exception ue) {
			throw new sys.CommandException(
				"ParamSistemaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private Vector isParametros(String[] nomParametro, String[] nomDescricao,
		String[] codParametro,String[] valParametro,String[] numOrdem) {
		Vector vErro = new Vector();
				
		if ((valParametro.length != nomDescricao.length)
			|| (nomParametro.length != nomDescricao.length)
			|| (nomDescricao.length != codParametro.length)
			|| (numOrdem.length != nomDescricao.length))
			vErro.addElement("Parametros inconsistentes");
		return vErro;
	}
}