package ACSS;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Usu�rios - Tabela de Hist�rico de Usu�rios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class ConsultaHistoricosBean extends sys.HtmlPopupBean {

	private String datInicio;
	private String datFim;
	
	
	private List dados;
	
	private String tipoUsuario ;
	

	public ConsultaHistoricosBean() throws sys.BeanException {
		
		dados = new ArrayList();
 
		datInicio = "";
		datFim = "";
		
		tipoUsuario = "";
	}


	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setDatInicio(String datInicio) {
		if(datInicio == null) datInicio = "";
		else this.datInicio = datInicio;
	}
	public String getDatInicio() {
		return datInicio;
	}

	public void setDatFim(String datFim) {
		if(datFim == null) datFim = "";
		else this.datFim = datFim;
	}
	public String getDatFim() {
		return datFim;
	}

	public void setTipoUsuario(String tipoUsuario) {
		if(tipoUsuario == null) tipoUsuario = "";
		else this.tipoUsuario = tipoUsuario;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	
	public ConsultaHistoricosBean getDados(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.dados.size()) ) return (new ConsultaHistoricosBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (ConsultaHistoricosBean)this.dados.get(i);
	 }
	
	
//	--------------------------  Metodos da Bean ----------------------------------


	public void Le_DadosUsr(String codSistema,UsuarioBean myUsr)
		throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaDadosHistUsr(this, codSistema, myUsr);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}


	public void Le_DadosPerOrg(String codSistema,UsuarioBean myUsr)
	throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaDadosHistPerOrg(this, codSistema, myUsr);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}
	
	public void Le_DadosPerFunc(String codSistema,UsuarioBean myUsr)
	throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.ConsultaDadosHistPerFunc(this, codSistema, myUsr);
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
	}

	public void Le_DadosCancAuto(String codOrgao,UsuarioBean myUsr)
	throws sys.BeanException {
	try {
		Dao dao = Dao.getInstance();
		dao.ConsultaDadosHistCancAuto(this, codOrgao, myUsr);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
}
	
	
}