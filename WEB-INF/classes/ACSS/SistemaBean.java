package ACSS;

import java.util.ArrayList;
import java.util.Vector;

import GER.NivelProcessoSistemaBean;
import GER.ProcessoSistemaBean;
import TAB.DaoException;


/*
* <b>Title:</b>        Sistema<br>
* <b>Description:</b>  Tabela de Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/



public class SistemaBean extends sys.HtmlPopupBean {
	private String codSistema;
	private String codProcesso;
	private String nomProcesso;
	private String sigVersao;
	private String nomSistema;
	private String nomAbrev;
	private String codUF;
	private Vector perfis;
	private Vector funcoes;
	private Vector mensagens;
	private Vector parametros;
	private Vector processos;
	private Vector nivelprocessos;


	private String atualizarDependente;

	
	public SistemaBean() {
		super();
		setTabela("TCAU_SISTEMA");
		setPopupWidth(35);
		codSistema = "0";
		codProcesso= "0";
		sigVersao = "0";
		nomSistema = " ";
		nomAbrev = "";
		codUF = "";
		perfis         = new Vector();
		funcoes        = new Vector();
		mensagens      = new Vector();
		parametros     = new Vector();
		processos      = new Vector();		
		nivelprocessos = new Vector();

		atualizarDependente = "N";
	}
	//--------------------------------------------------------------------------
	public void setCodSistema(String codSistema) {
		if (codSistema == null)   
			this.codSistema = "0";
		else
			this.codSistema = codSistema;
	}
	public String getCodSistema() {
		return this.codSistema;
	}
	
	public void setCodProcesso(String codProcesso) {
		if (codProcesso == null) this.codProcesso = "0";
		else
			this.codProcesso = codProcesso;
	}
	public String getCodProcesso() {
		return this.codProcesso;
	}
	
	public void setNomProcesso(String nomProcesso) {
		if (nomProcesso == null) this.nomProcesso = "";
		else
			this.nomProcesso = nomProcesso;
	}
	public String getNomProcesso() {
		return this.nomProcesso;
	}
	
//	--------------------------------------------------------------------------
	public void setCodUF(String codUF) {
		if (codUF == null)this.codUF = "";
		else this.codUF = codUF;
	}
	public String getCodUF() {
		return this.codUF;
	}

	//--------------------------------------------------------------------------
	public void setSigVersao(String sigVersao) {
		if (sigVersao == null)
			this.sigVersao = "0";
		else
			this.sigVersao = sigVersao;
	}
	public String getSigVersao() {
		return this.sigVersao;
	}

	//--------------------------------------------------------------------------
	public void setNomSistema(String nomSistema) {
		if (nomSistema == null)	this.nomSistema = " ";
		else					this.nomSistema = nomSistema;
	}
	public String getNomSistema() {
		return this.nomSistema;
	}

	//--------------------------------------------------------------------------
	public void setNomAbrev(String nomAbrev) {
		if (nomAbrev == null)	this.nomAbrev = "";
		else					this.nomAbrev = nomAbrev;
	}
	public String getNomAbrev() {
		return this.nomAbrev;
	}

	//--------------------------------------------------------------------------
	public void setAtualizarDependente(String atualizarDependente) {
		this.atualizarDependente = atualizarDependente;
	}
	public String getAtualizarDependente() {
		return this.atualizarDependente;
	}
	//--------------------------------------------------------------------------

	public void setFuncoes(Vector funcoes) {
		this.funcoes = funcoes;
	}

	public Vector getFuncoes(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.SistemaGetFuncoes(this);
			if (this.funcoes.size() < min - livre)
				livre = min - funcoes.size();
			for (int i = 0; i < livre; i++) {
				this.funcoes.addElement(new OperacaoBean());
			}
		}
		catch (Exception e) {
			setMsgErro("Sistema006 - Leitura: " + e.getMessage());
		} //fim do catch
		return this.funcoes;
	}

	public Vector getFuncoes() {
		return this.funcoes;
	}
	
	public OperacaoBean getFuncoes(int i) {
		return (OperacaoBean) this.funcoes.elementAt(i);
	}

	public boolean isValidFuncoes(Vector vErro) {
		if ("0".equals(this.codSistema))
			vErro.addElement("Sistema n�o selecionado. \n");
		String sFunc = "";
		String pai = "";
		boolean faltaPai = true;
		boolean temFilho = false;
		for (int i = 0; i < this.funcoes.size(); i++) {
			OperacaoBean myOper = (OperacaoBean) this.funcoes.elementAt(i);
			sFunc = myOper.getSigFuncao();
			pai = myOper.getSigFuncaoPai();
			if ((sFunc.length() == 0) || ("".equals(sFunc)))
				continue;
			// verificar se � filho dele mesmo 				
			if (pai.equals(sFunc))
				vErro.addElement(
					"Fun��o igual a Fun��o Pai: (linha: "
						+ i
						+ ") "
						+ sFunc
						+ " \n");
			// verificar duplicata 
			faltaPai = true;
			temFilho = false;
			for (int m = 0; m < this.funcoes.size(); m++) {
				OperacaoBean myOperOutra =
					(OperacaoBean) this.funcoes.elementAt(m);
				if ((m != i)
					&& (myOperOutra.getSigFuncao().trim().length() != 0)
					&& (myOperOutra.getSigFuncao().equals(sFunc))) {
					vErro.addElement(
						"Fun��o em duplicata: (linhas: "
							+ i
							+ ","
							+ m
							+ ") "
							+ sFunc
							+ " \n");
				}
				if (pai.length() != 0) {
					// validar existencia do pai 
					if ((m != i) && (pai.equals(myOperOutra.getSigFuncao())))
						faltaPai = false;
				}
				else
					faltaPai = false;
				// validar existencia de filho -->   			   
				if ((m != i) && (sFunc.equals(myOperOutra.getSigFuncaoPai())))
					temFilho = true;
			}
			if (faltaPai == true)
				vErro.addElement(
					"Fun��o Pai (Fun Pai) n�o existente: (linha: "
						+ i
						+ ") "
						+ pai
						+ " \n");
			if (temFilho == true) {
				// validar executa e apresentacao deve ser brancos -->   		
				if (myOper.getNomExecuta().trim().length() != 0)
					vErro.addElement(
						"Fun��o quando tem 'filhos' n�o pode ter 'Executa' preenchido (linha: "
							+ i
							+ ") "
							+ sFunc
							+ " \n");
				if (myOper.getNomApresentacao().trim().length() != 0)
					vErro.addElement(
						"Fun��o quando tem 'filhos' n�o pode ter 'Apresenta��o' preenchido (linha: "
							+ i
							+ ") "
							+ sFunc
							+ " \n");
			}
			else {
				// validar executa e apresentacao deve ser preenchidos 
				if (myOper.getNomExecuta().trim().length() == 0)
					vErro.addElement(
						"Fun��o quando n�o tem 'filhos', 'Executa' deve ser preenchido (linha: "
							+ i
							+ ") "
							+ sFunc
							+ " \n");
				if (myOper.getNomApresentacao().trim().length() == 0)
					vErro.addElement(
						"Fun��o quando n�o tem 'filhos', 'Apresenta��o' deve ser preenchido (linha: "
							+ i
							+ ") "
							+ sFunc
							+ " \n");
			}
			// verificar se o texto esta preenchido 
			if (myOper.getNomOperacao().trim().length() == 0)
				vErro.addElement(
					"Nome n�o preenchido (linha: " + i + ") " + sFunc + " \n");
		}
		return (vErro.size() == 0);
	}

	public boolean isInsertFuncoes() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidFuncoes(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaInsertFuncoes(this, vErro)) {
					vErro.addElement(
						"Funcionalidades do Sistema "
							+ this.nomAbrev
							+ " atualizadas."
							+ " \n");
				}
				else {
					vErro.addElement(
						"Funcionalidades do Sistema "
							+ this.nomAbrev
							+ " n�o atualizadas."
							+ " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement(
					"Funcionalidades do Sistema "
						+ this.nomAbrev
						+ " n�o atualizadas."
						+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} //fim do catch
		}
		else
			retorno = false;

		setMsgErro(vErro);
		return retorno;
	}

	public boolean isHabilitaFuncoes() {
		boolean retorno = true;
		Vector <String> vErro = new Vector <String> ();
		// Verificada a valida��o dos dados nos campos
		
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaHabilitaFuncoes(this, vErro)) {
					vErro.addElement(
						"Funcionalidades do Sistema "
							+ this.nomAbrev
							+ " atualizadas."
							+ " \n");
				}
				else {
					vErro.addElement(
						"Funcionalidades do Sistema "
							+ this.nomAbrev
							+ " n�o atualizadas."
							+ " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement(
					"Funcionalidades do Sistema "
						+ this.nomAbrev
						+ " n�o atualizadas."
						+ " \n");
				vErro.addElement("Erro na Atualiza��o: " + e.getMessage() + "\n");
				retorno = false;
			} //fim do catch		
		setMsgErro(vErro);
		return retorno;
	}

	//--------------------------------------------------------------------------
	public void setPerfis(Vector perfis) {
		this.perfis = perfis;
	}

	public Vector getPerfis(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.SistemaGetPerfis(this);
			if (this.perfis.size() < min - livre)
				livre = min - perfis.size();
			for (int i = 0; i < livre; i++) {
				this.perfis.addElement(new PerfilBean());
			}
		}
		catch (Exception e) {
			setMsgErro("Sistema007 - Leitura: " + e.getMessage());
		} //fim do catch
		return this.perfis;
	}

	public Vector getPerfis() {
		return this.perfis;
	}

	public PerfilBean getPerfis(int i) {
		return (PerfilBean) this.perfis.elementAt(i);
	}

	public boolean isValidPerfis(Vector vErro) {
		if ("0".equals(this.codSistema))
			vErro.addElement("Sistema n�o selecionado. \n");
		String sDesc = "";
		for (int i = 0; i < this.perfis.size(); i++) {
			PerfilBean myPerf = (PerfilBean) this.perfis.elementAt(i);
			sDesc = myPerf.getNomDescricao();
			if ((sDesc.length() == 0) || ("".equals(sDesc)))
				continue;
			// verificar duplicata 
			for (int m = 0; m < this.perfis.size(); m++) {
				PerfilBean myPerfOutra = (PerfilBean) this.perfis.elementAt(m);
				if ((m != i)
					&& (myPerfOutra.getNomDescricao().trim().length() != 0)
					&& (myPerfOutra.getNomDescricao().equals(sDesc))) {
					vErro.addElement(
						"Perfil em duplicata: (linhas: "+i+","+m+ ") "+sDesc+ " \n");
				}
			}
		    if ( (myPerf.getIndVisEndereco()==null) || ("S".equals(myPerf.getIndVisEndereco())==false) )
				myPerf.setIndVisEndereco("");
			if ( (myPerf.getIndVis2via()==null) || ("S".equals(myPerf.getIndVis2via())==false) )
				myPerf.setIndVis2via("");
			if ( (myPerf.getIndVisAr()==null) || ("S".equals(myPerf.getIndVisAr())==false) )
				myPerf.setIndVisAr("");
			if ( (myPerf.getIndVisAuto()==null) || ("S".equals(myPerf.getIndVisAuto())==false) )
				myPerf.setIndVisAuto("");
            if ( (myPerf.getIndVisFoto()==null) || ("S".equals(myPerf.getIndVisFoto())==false) )
                myPerf.setIndVisFoto("");            
			if ( (myPerf.getIndVisReq()==null) || ("S".equals(myPerf.getIndVisReq())==false) )
				myPerf.setIndVisReq("");
			if ( (myPerf.getIndVisHist()==null) || ("S".equals(myPerf.getIndVisHist())==false) )
				myPerf.setIndVisHist("");
			if ( (myPerf.getIndVisRej()==null) || ("S".equals(myPerf.getIndVisRej())==false) )
				myPerf.setIndVisRej("");
			if ( (myPerf.getIndVisImpr()==null) || ("S".equals(myPerf.getIndVisImpr())==false) )
				myPerf.setIndVisImpr("");
			if ( (myPerf.getIndVisTodosOrg()==null) || ("S".equals(myPerf.getIndVisTodosOrg())==false) )
				myPerf.setIndVisTodosOrg("");
				
		}
		return (vErro.size() == 0);
	}
	public boolean isInsertPerfis() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidPerfis(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaInsertPerfis(this, vErro)) {
					vErro.addElement("Perfis do Sistema "
							+ this.nomAbrev+ " atualizados."+ " \n");
				}
				else {
					vErro.addElement("Perfis do Sistema "
							+ this.nomAbrev+ " n�o atualizados."+ " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement("Perfis do Sistema "+ this.nomAbrev+ " n�o atualizados."+ " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} //fim do catch
		}
		else
			retorno = false;
		setMsgErro(vErro);
		return retorno;
	}

	//--------------------------------------------------------------------------
	public void Le_Sistema(String tpsist, int pos) {
		try {
			Dao dao = Dao.getInstance();
			if (pos == 0)
				this.codSistema = tpsist;
			else
				this.nomSistema = tpsist;
			if (dao.SistemaLeBean(this, pos) == false) {
				this.codSistema = "0";
				this.nomSistema = "";
				this.nomAbrev = "";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codSistema = "0";
			setPkid("0");
			System.err.println("Sistema001: " + e.getMessage());
		}
	}

	//--------------------------------------------------------------------------
	public void buscaCodSistema(String codsist, String Acao) {
		int pos = "Top,Proximo,Anterior,Fim,Pkid".indexOf(Acao);
		if (pos < 0)
			return;
		try {
			Dao dao = Dao.getInstance();
			if (pos == 25)
				this.codSistema = codsist;
			else
				this.nomSistema = codsist;
			if (dao.BuscaSistema(this, pos) == false) {
				setPkid("0");
				this.codSistema = "0";
			}
		} //fim do try
		catch (Exception e) {
			this.codSistema = "0";
			setPkid("0");
			System.err.println("Sistema003: " + e.getMessage());
		} //fim do catch
	}

	//--------------------------------------------------------------------------
	public boolean isValid(Vector vErro, int acao) {
		boolean bOk = true;
		if (this.nomSistema.length() == 0)
			vErro.addElement("Nome do Sistema n�o preenchido. \n");
		if (this.nomAbrev.length() == 0)
			vErro.addElement("Nome Abreviado n�o preenchido. \n");
		return (vErro.size() == 0);
	}

	//--------------------------------------------------------------------------
	public boolean isInsert() {
		boolean retorno = true;
		// Verificada a valida��o dos dados nos campos
		Vector vErro = new Vector();
		if (isValid(vErro, 1)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaExiste(this, 0)) {
					vErro.addElement(
						"Sistema j� cadastrado com "
							+ this.codSistema
							+ " ou "
							+ this.nomSistema
							+ " \n");
					retorno = false;
				}
				else {
					if (dao.SistemaInsert(this))
						vErro.addElement(
							"Sistema incluido com sucesso:"
								+ this.nomSistema
								+ " \n");
					else
						vErro.addElement(
							"Sistema n�o incluido:" + this.nomSistema + " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement(
					"Sistema n�o incluido: " + this.codSistema + " \n");
				vErro.addElement(
					"Erro na inclus�o do Banco de Dados: "
						+ e.getMessage()
						+ "\n");
				retorno = false;
			} //fim do catch
		}
		setMsgErro(vErro);
		return retorno;
	}

	//--------------------------------------------------------------------------
	public boolean isAltera() {
		boolean retorno = true;
		Vector vErro = new Vector();
		if (isValid(vErro, 2)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaExiste(this, 0) == false) {
					vErro.addElement(
						"Sistema n�o cadastrado com "
							+ this.codSistema
							+ " \n");
					retorno = false;
				}
				else {
					if (dao.SistemaUpdate(this))
						vErro.addElement(
							"Sistema alterado: " + this.codSistema + " \n");
					else
						vErro.addElement(
							"Sistema n�o alterado: " + this.codSistema + " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement(
					"Sistema n�o alterado: " + this.codSistema + " \n");
				retorno = false;
			}
		}
		setMsgErro(vErro);
		return retorno;
	}

	//--------------------------------------------------------------------------
	public boolean isDelete() {
		boolean retorno = false;
		Vector vErro = new Vector();
		try {
			Dao dao = Dao.getInstance();
			if (dao.SistemaExiste(this, 0) == false) {
				vErro.addElement(
					"Sistema n�o cadastrado: " + this.codSistema + " \n");
				retorno = false;
			}
			else {
				if (dao.SistemaDelete(this)) {
					vErro.addElement(
						"Sistema excuido com sucesso:"
							+ this.codSistema
							+ " \n");
					setPkid("0");
					this.codSistema = "0";
					retorno = true;
				}
				else
					vErro.addElement(
						"Sistema n�o excuido: " + this.codSistema + " \n");
			}
		}
		catch (Exception e) {
			vErro.addElement(
				"Sistema n�o excluido: " + this.codSistema + " \n");
			vErro.addElement("Erro na exclus�o: " + e.getMessage() + " \n");
			retorno = false;
		} //fim do catch
		setMsgErro(vErro);
		return retorno;
	}

	//--------------------------------------------------------------------------
	public String getSistemas(String codUsr) {
		String mySistemas = "";
		try {
			Dao dao = Dao.getInstance();
			mySistemas = dao.getSistemas(codUsr);
		}
		catch (Exception e) {
		}
		return mySistemas;
	}
	
	public String getProcessos(String codSistema) {
		String mySistemas = "";
		try {
			Dao dao = Dao.getInstance();
			mySistemas = dao.getProcessos(codSistema);
		}
		catch (Exception e) {
		}
		return mySistemas;
	}
	
	

	/*************************************************************************/
	public void setMensagens(Vector mensagens) {
		this.mensagens = mensagens;
	}
	public Vector getMensagens() {
		return this.mensagens;
	}
	//--------------------------------------------------------------------------
	public Vector getMensagens(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.MensagemGetSistemas(this);
			if (this.mensagens.size() < min - livre)
				livre = min - mensagens.size();
			for (int i = 0; i < livre; i++)
				this.mensagens.addElement(new MensagemSistemaBean());
		}
		catch (Exception e) {
			setMsgErro("Sistema007 - Leitura: " + e.getMessage());
		} //fim do catch
		return this.mensagens;
	}
	//--------------------------------------------------------------------------
	public MensagemSistemaBean getMensagens(int i) {
		return (MensagemSistemaBean) this.mensagens.elementAt(i);
	}
	//--------------------------------------------------------------------------
	public boolean isValidMensagens(Vector vErro) {
		if ("0".equals(this.codSistema))
			vErro.addElement("Sistema n�o selecionado. \n");
		String sDesc = "";
		for (int i = 0; i < this.mensagens.size(); i++) {
			MensagemSistemaBean myMsgSis =
				(MensagemSistemaBean) this.mensagens.elementAt(i);
			sDesc = myMsgSis.getDscMensagem();
			if ((sDesc.length() == 0) || ("".equals(sDesc)))
				continue;
			// verificar duplicata 
			for (int m = 0; m < this.mensagens.size(); m++) {
				MensagemSistemaBean myMsgSisOutra =
					(MensagemSistemaBean) this.mensagens.elementAt(m);
				if ((m != i)
					&& (myMsgSisOutra.getDscMensagem().trim().length() != 0)
					&& (myMsgSisOutra.getDscMensagem().equals(sDesc))) {
					vErro.addElement(
						"Mensagem em duplicata: (linhas: "
							+ i
							+ ","
							+ m
							+ ") "
							+ sDesc
							+ " \n");
				}
			}
		}
		return (vErro.size() == 0);
	}
	//--------------------------------------------------------------------------
	public boolean isInsertMensagemSistema() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidMensagens(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.MensagemInsertSistemas(this, vErro))
					vErro.addElement("Mensagens do Sistema atualizadas.\n");
				else
					vErro.addElement("Mensagens do Sistema n�o atualizados.\n");
			}
			catch (Exception e) {
				vErro.addElement(
					"Mensagens do Sistema n�o atualizadas." + " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} //fim do catch
		}
		else
			retorno = false;
		setMsgErro(vErro);
		return retorno;
	}
	/*************************************************************************/
	
	/*-------------------------------------------------------------
	*  Metodos relativos aos Parametros do Sistema (ParamSistema)
	*-------------------------------------------------------------*/
	public void setParametros(Vector parametros) {
		this.parametros = parametros;
	}
	public Vector getParametros(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.SistGetParam(this);
			if (this.parametros.size() < min - livre)
				livre = min - parametros.size();
			for (int i = 0; i < livre; i++) {
				this.parametros.addElement(new ParamSistemaBean());
			}
		}
		catch (Exception e) {
			setMsgErro("Processo007 - Leitura: " + e.getMessage());
		} 
		return this.parametros;
	}
	
	
	public void setProcessos(Vector processos) {
		this.processos = processos;
	}

	public Vector getProcessos(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.SistGetProcesso(this);
			if (this.processos.size() < min - livre)
				livre = min - processos.size();
			for (int i = 0; i < livre; i++) {
				this.processos.addElement(new ProcessoSistemaBean());
			}
		}
		catch (Exception e) {
			setMsgErro("ParametroProcesso007 - Leitura: " + e.getMessage());
		} 
		return this.processos;
	}



	public void setNivelProcessos(Vector nivelprocessos) {
		this.nivelprocessos = nivelprocessos;
	}
	
	public Vector getNivelProcessos(int min, int livre) {
		try {
			Dao dao = Dao.getInstance();
			dao.SistGetNivelProcesso(this);
			if (this.nivelprocessos.size() < min - livre)
				livre = min - nivelprocessos.size();
			for (int i = 0; i < livre; i++) {
				this.nivelprocessos.addElement(new NivelProcessoSistemaBean());
			}
		}
		catch (Exception e) {
			setMsgErro("ParametroNivelProcesso007 - Leitura: " + e.getMessage());
		} 
		return this.nivelprocessos;
	}


	
	public Vector getParametros() {
		return this.parametros;
	}

	public Vector getProcessos() {
		return this.processos;
	}
	
	public Vector getNivelProcessos() {
		return this.nivelprocessos;
	}


	public ParamSistemaBean getParametros(int i) {
			return (ParamSistemaBean) this.parametros.elementAt(i);
	}

	public ProcessoSistemaBean getProcessos(int i) {
			return (ProcessoSistemaBean) this.processos.elementAt(i);
	}

	public NivelProcessoSistemaBean getNivelProcessos(int i) {
			return (NivelProcessoSistemaBean) this.nivelprocessos.elementAt(i);
	}


	public boolean isValidParametros(Vector vErro) {
			if ("0".equals(this.codSistema)) vErro.addElement("Parametro n�o selecionado. \n");
				
			String sDesc = "";
			for (int i = 0; i < this.parametros.size(); i++) {
				ParamSistemaBean myParam =(ParamSistemaBean) this.parametros.elementAt(i);
				sDesc = myParam.getNomDescricao();
				
				if ((sDesc.length() == 0) || ("".equals(sDesc)))
					continue;
				// verificar duplicata 
				for (int m = 0; m < this.parametros.size(); m++) {
					ParamSistemaBean myParamOutra =	(ParamSistemaBean) this.parametros.elementAt(m);
					if ((m != i)
						&& (myParamOutra.getNomDescricao().trim().length() != 0)
						&& (myParamOutra.getNomDescricao().equals(sDesc))) {
						vErro.addElement("Parametro em duplicata: (linhas: "
								+ (i + 1)+ "," + (m + 1)+ ") "+ sDesc + " \n");
					}
				}
			}
			return (vErro.size() == 0);
		}

 

	public boolean isInsertParametros() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidParametros(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaInsertParam(this, vErro)) {
					vErro.addElement("Parametros do Sistema "
							+ this.nomSistema + " atualizados." + " \n");
				}
				else {
					vErro.addElement("Parametros do Sistema "
							+ this.nomSistema + " n�o atualizados." + " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement("Parametros do Sistema "
						+ this.nomSistema + " n�o atualizados." + " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} 
		}
		else
			retorno = false;
		setMsgErro(vErro);
		return retorno;
	}

	public void Le_Processo(String tpsist, int pos) {
		try {
			Dao dao = Dao.getInstance();
			if (pos == 0)
				this.codProcesso = tpsist;
			else
				this.nomProcesso = tpsist;
			if (dao.ProcessoLeBean(this, pos) == false) {
				this.codProcesso = "0";
				this.nomProcesso = "";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codProcesso = "0";
			setPkid("0");
			System.err.println("Sistema001: " + e.getMessage());
		}
	}	

	public boolean isValidProcessos(Vector vErro) {
		if ("0".equals(this.codSistema)) vErro.addElement("Processo n�o selecionado. \n");
		
		String sDesc = "";
		for (int i = 0; i < this.processos.size(); i++) {
			ProcessoSistemaBean myProcesso =(ProcessoSistemaBean) this.processos.elementAt(i);
			sDesc = myProcesso.getNomDescricao();
			
			if ((sDesc.length() == 0) || ("".equals(sDesc)))
				continue;
			// verificar duplicata 
			for (int m = 0; m < this.processos.size(); m++) {
				ProcessoSistemaBean myProcessoOutro =	(ProcessoSistemaBean) this.processos.elementAt(m);
				if ((m != i)
						&& (myProcessoOutro.getNomDescricao().trim().length() != 0)
						&& (myProcessoOutro.getNomDescricao().equals(sDesc))) {
					vErro.addElement("Processo em duplicata: (linhas: "
							+ (i + 1)+ "," + (m + 1)+ ") "+ sDesc + " \n");
				}
			}
		}
		return (vErro.size() == 0);
	}

	public boolean isValidNivelProcessos(Vector vErro) {
		if ( ("0".equals(this.codSistema)) || ("0".equals(this.codProcesso)) ) vErro.addElement("Nivel Processo n�o selecionado. \n");
			
		String sDesc = "";
		for (int i = 0; i < this.nivelprocessos.size(); i++) {
			NivelProcessoSistemaBean NivelmyProcesso =(NivelProcessoSistemaBean) this.nivelprocessos.elementAt(i);
			sDesc = NivelmyProcesso.getNomNivelProcesso();
			
			if ((sDesc.length() == 0) || ("".equals(sDesc)))
				continue;
			// verificar duplicata 
			for (int m = 0; m < this.nivelprocessos.size(); m++) {
				NivelProcessoSistemaBean myNivelProcessoOutro =	(NivelProcessoSistemaBean) this.nivelprocessos.elementAt(m);
				if ((m != i)
					&& (myNivelProcessoOutro.getNomNivelProcesso().trim().length() != 0)
					&& (myNivelProcessoOutro.getNomNivelProcesso().equals(sDesc))) {
					vErro.addElement("Nivel de Processo em duplicata: (linhas: "
							+ (i + 1)+ "," + (m + 1)+ ") "+ sDesc + " \n");
				}
			}
		}
		return (vErro.size() == 0);
	}

	public boolean isInsertProcessos() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidProcessos(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaInsertProcesso(this, vErro)) {
					vErro.addElement("Processos do Sistema "
							+ this.nomSistema + " atualizados." + " \n");
				}
				else {
					vErro.addElement("Processos do Sistema "
							+ this.nomSistema + " n�o atualizados." + " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement("Processos do Sistema "
						+ this.nomSistema + " n�o atualizados." + " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} 
		}
		else
			retorno = false;
		setMsgErro(vErro);
		return retorno;
	}
		
	public boolean isInsertNivelProcessos() {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		if (isValidNivelProcessos(vErro)) {
			try {
				Dao dao = Dao.getInstance();
				if (dao.SistemaInsertNivelProcesso(this, vErro)) {
					vErro.addElement("Niveis de Processos do Sistema "
							+ this.nomSistema + " atualizados." + " \n");
				}
				else {
					vErro.addElement("Niveis de Processos do Sistema "
							+ this.nomSistema + " n�o atualizados." + " \n");
				}
			}
			catch (Exception e) {
				vErro.addElement("Niveis Processos do Sistema "
						+ this.nomSistema + " n�o atualizados." + " \n");
				vErro.addElement("Erro na inclus�o: " + e.getMessage() + "\n");
				retorno = false;
			} 
		}
		else
			retorno = false;
		setMsgErro(vErro);
		return retorno;
	}

	public ArrayList getDestinatariosPorNivel(ParamSistemaBean mySistema, String parametro) throws DaoException{
		ArrayList destporNivel = new ArrayList();
		
		try {
			destporNivel = Dao.getInstance().getDestinatariosPorNivel(mySistema,parametro);
		} 
		catch (DaoException e) {
			setMsgErro("Erro m�todo getDestinatariosPorNivel : " + e.getMessage());
		}
       return destporNivel;
		
	}
	
	
}
