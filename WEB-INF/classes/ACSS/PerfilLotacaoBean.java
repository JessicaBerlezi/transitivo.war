package ACSS;

import java.util.ArrayList;
import java.util.List;


/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Perfils - Tabela de Operacoes<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
* @Updates
*/

public class PerfilLotacaoBean  extends sys.HtmlPopupBean  { 

  private String codUsuario ;
  private String codOrgao ;
  private String codSistema;
  private String nomSistema;  
  private String codPerfilUsr;
  private String codNivelUsr;

  private String nomUserNameOper;
  private String datProcOper;
  private String orgaoLotacaoOper;

  private String codNivelUsrLogado;  
  private String codUsrLogado;               
  private List perfis;   

  private List perfisLotacao;   
      
  public PerfilLotacaoBean() throws sys.BeanException {

    codUsuario   = "";
    codOrgao     = "";
    codSistema   = "";
    nomSistema   = "";	
    codPerfilUsr = "";
	codNivelUsr  = "";
	
    nomUserNameOper   = "";
    datProcOper   = "";
    orgaoLotacaoOper   = "";
	
	codNivelUsrLogado = "";  
	codUsrLogado = "";  

	perfis       = new ArrayList() ;	
	perfisLotacao= new ArrayList() ;
  }
  //--------------------------------------------------------------------------
  public void setCodUsuario(String codUsuario)  {
  this.codUsuario=codUsuario ;
    if (codUsuario==null) this.codUsuario="" ;
  }  
  public String getCodUsuario()  {
  	return this.codUsuario;
  }
  //--------------------------------------------------------------------------
  public void setCodOrgao(String codOrgao)  {
  this.codOrgao=codOrgao ;
    if (codOrgao==null) this.codOrgao="" ;
  }  
  public String getCodOrgao()  {
  	return this.codOrgao;
  }
  //--------------------------------------------------------------------------
  public void setCodSistema(String codSistema)  {
  this.codSistema=codSistema ;
    if (codSistema==null) this.codSistema="" ;
  }  
  public String getCodSistema()  {
  	return this.codSistema;
  }
  //--------------------------------------------------------------------------
  public void setNomSistema(String nomSistema)  {
  this.nomSistema=nomSistema ;
    if (nomSistema==null) this.nomSistema="" ;
  }  
  public String getNomSistema()  {
  	return this.nomSistema;
  }
  //-------------------------------------------------------------------------- 
  public void setCodPerfilUsr(String codPerfilUsr)  {
	this.codPerfilUsr=codPerfilUsr ;
    if (codPerfilUsr==null) this.codPerfilUsr="" ;
  }  
  public String getCodPerfilUsr()  {
  	return this.codPerfilUsr;
  }
  //--------------------------------------------------------------------------  
  public void setCodNivelUsr(String codNivelUsr)  {
  this.codNivelUsr=codNivelUsr ;
    if (codNivelUsr==null) this.codNivelUsr="" ;
  }  
  public String getCodNivelUsr()  {
  	return this.codNivelUsr;
  }
  //--------------------------------------------------------------------------
  
  public void setNomUserNameOper(String nomUserNameOper)  {
      if (nomUserNameOper==null) nomUserNameOper="" ;
      this.nomUserNameOper = nomUserNameOper;
  }
  public String getNomUserNameOper()  {
      return this.nomUserNameOper;
  }
  //--------------------------------------------------------------------------
  
  public void setDatProcOper(String datProcOper)  {
      if (datProcOper==null) datProcOper="" ;
      this.datProcOper = datProcOper;
  }
  public String getDatProcOper()  {
      return this.datProcOper;
  }
  
  //--------------------------------------------------------------------------
  public void setOrgaoLotacaoOper(String orgaoLotacaoOper)  {
      if (orgaoLotacaoOper==null) orgaoLotacaoOper="" ;
      this.orgaoLotacaoOper = orgaoLotacaoOper;
  }
  public String getOrgaoLotacaoOper()  {
      return this.orgaoLotacaoOper;
  }
  
  
  //--------------------------------------------------------------------------
  public void setCodNivelUsrLogado(String codNivelUsrLogado)  {
  this.codNivelUsrLogado=codNivelUsrLogado ;
    if (codNivelUsrLogado==null) this.codNivelUsrLogado="" ;
  }  
  public String getCodNivelUsrLogado()  {
	 return this.codNivelUsrLogado;
  }
  //--------------------------------------------------------------------------
  public void setCodUsrLogado(String codUsrLogado)  {
  this.codUsrLogado=codUsrLogado ;
    if (codUsrLogado==null) this.codUsrLogado="" ;
  }  
  public String getCodUsrLogado()  {
   return this.codUsrLogado;
  }
  //--------------------------------------------------------------------------
  public void setPerfis(List perfis)  {
  	this.perfis = perfis ;
  }
  public List getPerfis()  {
     return this.perfis;
  }
 //--------------------------------------------------------------------------

  public void setPerfisLotacao(List perfisLotacao)  {
  	this.perfisLotacao = perfisLotacao ;
  }
  public List getPerfisLotacao()  {
     return this.perfisLotacao;
  }
  //--------------------------------------------------------------------------
 
  public void Le_PerfilLotacao()  throws sys.BeanException  {
    try {
		Dao dao = Dao.getInstance();
		dao.PerfilLotacaoLeBean(this);	
    } 
    catch (Exception e) { 
 	   throw new sys.BeanException(e.getMessage());	
    }	
  }
  public void atualPerfil(String[] codSist,String[] codPerf,String[] codNiv	)  throws sys.BeanException  {
	try {
	  	Dao dao = Dao.getInstance();
	  	dao.atualPerfilLotacao(this,codSist,codPerf,codNiv);
    } 
    catch (Exception e) { 
     throw new sys.BeanException(e.getMessage());	
    }	
  }

  public void gravaHistorico(String[] codSist,String[] codPerf,String[] codNiv,
	String[] txtJustificativa)  throws sys.BeanException  {
  
    try {
	  	Dao dao = Dao.getInstance();
	  	dao.gravaHistorico(this,codSist,codPerf,codNiv,txtJustificativa);
    } 
    catch (Exception e) { 
     throw new sys.BeanException(e.getMessage());	
    }	
  }



}