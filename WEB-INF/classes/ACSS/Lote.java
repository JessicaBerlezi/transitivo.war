package ACSS;

import java.util.ArrayList;
import java.util.HashMap;

public class Lote extends  sys.HtmlPopupBean  {
		private String selectLote;
		public Lote() {
			
		}
		public String getSelectLote() {
			return selectLote;
		}
		public void setSelectLote(String selectLote) {
			this.selectLote = selectLote;
		}
		
		public boolean getDbLink(String codOrgao, String CONSTANTE){ //Verifica se existe a necessidade de acesso via data base link
			boolean result = false;
			try{
				Dao dao = Dao.getInstance();
				result = dao.getDbLink(codOrgao, CONSTANTE);
			}catch(Exception e){
				e.getMessage().toString();
			}
			return result;
		}

		
		public String getNomeDbLink(String codOrgao, String CONSTANTE){//Recupera o nome da data base link
			String result = null;
			try{
				Dao dao = Dao.getInstance();
				result = dao.getNomeDbLink(codOrgao, CONSTANTE);
			}catch(Exception e){
				e.getMessage().toString();
			}
			return result;
		}
		
		
		public ArrayList<HashMap<String, String>> getLoteAuto(String nomeDbLink, String dsLote, String donoTabela){
			ArrayList<HashMap<String, String>> loteAutos = new ArrayList<HashMap<String, String>>();
			
			try{
				loteAutos = Dao.getInstance().getLoteAuto(nomeDbLink, dsLote, donoTabela);
			}
			catch(Exception ex){
				System.out.println("ERRO NA CLASS GET LOTE AUTO - M�TODO: getLoteAuto(): " + ex.getMessage());
			}
			
			return loteAutos;
		}
		
		
		public String getSelectLoteAuto(String nomeDbLink, String donoTabela, String nameSelect){
				String lotes = "";
			try{
				lotes = Dao.getInstance().getSelectLoteAuto(nomeDbLink, donoTabela, nameSelect);
			}
			catch(Exception ex){
				System.out.println("ERRO NA CLASS GET LOTE AUTO - M�TODO: getLoteAuto(): " + ex.getMessage());
			}
			
			return lotes;
		}
		
		public String getDonoTabela(String codOrgao, String CONSTANTE){//Recupera o nome da tabela
			String result = null;
			try{
				Dao dao = Dao.getInstance();
				result = dao.getDonoTabela(codOrgao, CONSTANTE);
			}catch(Exception e){
				e.getMessage().toString();
			}
			return result;
		}
		
		
		public boolean processaLoteCmd( ArrayList<HashMap<String, String>> processos, String dsAcao, String codOrgao){
			boolean result = false;
			try{
				Dao dao = Dao.getInstance();
				result = dao.processaLoteCmd(processos, dsAcao, codOrgao );
			}catch(Exception e){
				e.getMessage().toString();
			}
			return result;
		}

		public String montaSelectSqlLote(String codOrgao, String nameSelect){
			String sqlOpt ="";
			try{
				Dao dao = Dao.getInstance();
				sqlOpt = dao.montaSelectSqlLote(codOrgao, nameSelect);
			}
			catch(Exception e){
			}	
			return sqlOpt ;
		}
		
		
		public String callStatus(String numAuto){
			String result = "";
			try{
				Dao dao = Dao.getInstance();
				result = dao.callStatus(numAuto);
			}catch(Exception e){
				e.getMessage().toString();
			}
			return result;
		}
		
}
