package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;
import sys.Util;
import ACSS.ParamSistemaBean;

public class EnviaEmailEmiteVexRob {
    
	private static final String NOM_ROBOT = "EnviaEmailEmiteVexRob";
  
	public EnviaEmailEmiteVexRob() throws RobotException, DaoException {
        
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
		try 
		{     
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
            
			try 
			{               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			monitor.gravaMonitor("Inicio do processamento do rob� EnviaEmailEmitVexRob", monitor.MSG_INICIO);
			System.out.println("Inicio do processamento do rob� EnviaEmailEmitVexRob");
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			//L� o par�metro da �ltima data processada
			monitor.gravaMonitor("L� o par�metro da �ltima data processada", monitor.MSG_INFO);
			System.out.println("L� o par�metro da �ltima data processada");
			String datParametro = param.getParamSist("DAT_ENVIO_EMAIL_EMITEVEX");

			//Carregando Destinat�rios
            ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"EMAIL_EMITEVEX");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");		

			//Verifica qtd do Emitivex do dia
			GER.RelatArqNotificacaoBean notifBean = new GER.RelatArqNotificacaoBean();
			notifBean.setDatRecebimento(datParametro);
			Dao.getInstance().getDadosEmitVex(notifBean, conn);
			
			//Prepara e envia e-mail
			if (!notifBean.getQtdReg().equals(""))
			{
				String sConteudoEmail = " Arquivo: "+ notifBean.getNomArquivo()+ " - Data: "+
		        datParametro +" - QTD Registros Enviados: "+
		        notifBean.getQtdReg()+"\n\n OBS.: Ser� encaminhado, logo em seguida, um" +
		        " outro e-mail contendo o resultado do processamento do arquivo, " +
		        " juntamente com o acumulado ate o mes." +
		        "\n\n Atenciosamente, \n Auditoria SMIT";
				
			    monitor.gravaMonitor("Preparando e enviando e-mail", monitor.MSG_INFO);
			    System.out.println("Preparando e enviando e-mail");
			    ArrayList usuariosBloqueados = preparaEmail(ListDestinatarios, sConteudoEmail);
			    enviaEmail(usuariosBloqueados, emailBroker, datParametro) ;
			
	            //Atualiza o valor do par�metro de data
				monitor.gravaMonitor("Atualizando o valor do par�metro de data", monitor.MSG_INFO);
				System.out.println("Atualizando o valor do par�metro de data");
				String ultimaData = Util.addData(datParametro,1);
				
				ParamSistemaBean paramBean = new ParamSistemaBean();
				paramBean.setNomParam("DAT_ENVIO_EMAIL_EMITEVEX");
				paramBean.setValParametro(ultimaData);
				paramBean.setCodSistema(param.getCodSistema());
				Dao.getInstance().AtualizaParametro(paramBean, conn);
			}
			conn.commit();
			monitor.gravaMonitor("Fim do processamento do rob� EnviaEmailEmitVexRob ", monitor.MSG_FIM);
			System.out.println("Fim do processamento do rob� EnviaEmailEmitVexRob");

		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}

	 
	
	private ArrayList preparaEmail(ArrayList ListDestinatarios, String sConteudoEmail){
		// Prepara Email    
		ArrayList usuariosBloqueados = new ArrayList();
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(nivelProcesso.getTipoMsg().equals("M")){
							destinatario.getMsg().add(sConteudoEmail);
						}

						if(destinatario.getMsg().size()>0)
							lDestinatarios.add(destinatario);
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
					usuariosBloqueados.add(nivelProcesso);
			   }
			}
		  }
          return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuariosBloqueados, String emailBroker, String data) throws Exception{	
		if (usuariosBloqueados.size()>0)
		{
			Iterator it = usuariosBloqueados.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
				{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							String sConteudo="";
							Iterator ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso() + data);
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				   }
				}
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new EnviaEmailEmiteVexRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	

}