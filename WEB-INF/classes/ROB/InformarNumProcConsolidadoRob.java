package ROB;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.BeanException;
import sys.DaoException;
import sys.ServiceLocatorException;
import ACSS.UsuarioBean;
import REC.AutoInfracaoBean;
import REC.GuiaDistribuicaoBean;
import REC.ParamOrgBean;

/**
 * <b>Title:</b> SMIT - MODULO ROBOT<br>
 * <b>Description:</b> Rob� de auditoria da quantidade de processos na sala de guias<br>
 * <b>Copyright:</b> Copyright (c) 2006<br>
 * <b>Company:</b> MIND Informatica <br>
 * @author Luciana Rocha
 * @version 1.0
 */

public class InformarNumProcConsolidadoRob {
	
	private static final String NOM_ROBOT = "InformarNumProcConsolidadoRob";
	private static final String quebraLinha = "<br>";
	sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
	
	public InformarNumProcConsolidadoRob() throws RobotException, ROB.DaoException, DaoException {
		
		Connection conn  = null;
		Statement trava = null;
		
		try {            			
			conn = Dao.getInstance().getConnection();
			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			// N�o permite que mais de uma instancia do robo rodem ao mesmo tempo 
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				System.out.println("Erro: "+e.getMessage());
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			// Carrega Parametros 
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); // M�DULO REGISTRO 
			param.PreparaParam(conn);
			ArrayList ListDestinatarios = ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"QTD_PROCESSO_GUIA");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");
			
			StringBuffer msg = new StringBuffer();
			String datInicioRemessa = "";
			String procRobConsolidado = "";
			
			ACSS.OrgaoBean[] orgaos = Dao.getInstance().BuscarOrgaosIntegrados("0");
			String [] sigFuncao = {"REC0230","REC0336","REC0436"};
			String [] indFase = {" Defesa Previa ", " 1a. Instancia", "2a. Instancia"};
			for (int j = 0; j < sigFuncao.length; j++ )
			{
				monitor.gravaMonitor("Inicio do Processamento do robo InformarNumProcGuiaRob", monitor.MSG_INICIO);
				monitor.gravaMonitor("Verificando quantidade de processos de "+indFase[j], monitor.MSG_INFO);
				for (int i = 0; i < orgaos.length; i++) 
				{
                    monitor.gravaMonitor("Processando dados de "+indFase[j]+" do �rg�o: "+orgaos[i].getCodOrgao(), monitor.MSG_INFO);
            		ParamOrgBean ParamOrgaoId = new ParamOrgBean();
        			GuiaDistribuicaoBean GuiaDistribuicaoId  = new GuiaDistribuicaoBean() ;
        			UsuarioBean UsrLogado = new UsuarioBean () ;
					GuiaDistribuicaoId.setJ_sigFuncao(sigFuncao[j],ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
					UsrLogado.setCodOrgaoAtuacao(orgaos[i].getCodOrgao());
                    ParamOrgaoId.PreparaParam(UsrLogado);
					datInicioRemessa = ParamOrgaoId.getParamOrgao("DAT_INICIO_REMESSA","","10");
					procRobConsolidado = ParamOrgaoId.getParamOrgao("PROCESSA_ROB_CONSOLIDADO","","10");
					
					if(procRobConsolidado.equals("S"))
					{
						// Verifica quantos processos est�o na sala
						GuiaDistribuicaoId.PreparaGuia(UsrLogado,datInicioRemessa,"99999");
						GuiaDistribuicaoId.Classifica("DatProcesso");
						
						if(GuiaDistribuicaoId.getMsgErro().length()>0)
							monitor.gravaMonitor("Erro ao Preparar Guia - BROKER: " + GuiaDistribuicaoId.getMsgErro(), monitor.MSG_ERRO);
						else
						{
							monitor.gravaMonitor("Processando - "+indFase[j]+" - "+i+" de "+orgaos.length+" �rg�os...", monitor.MSG_INFO);
							System.out.println("Processando - "+indFase[j]+" - "+i+" de "+orgaos.length+" �rg�os...");
							msg.append("Prezado(a) Senhor(a), " + quebraLinha + quebraLinha +
							"Informamos que a quantidade de processos de "+ indFase[j] +
							" a serem distribuidos, at� a presente data, � de <b>" + GuiaDistribuicaoId.getAutos().size() + "</b>."+
	                        quebraLinha + quebraLinha + "Consolida��o por m�s: "+quebraLinha );
							
							ArrayList qtdProcessoSala = new ArrayList();
							//Prepara Email  
	                        monitor.gravaMonitor("Preparando e-mail...", monitor.MSG_INFO);
							if (ListDestinatarios.size()>0 && GuiaDistribuicaoId.getAutos().size()>0)
								qtdProcessoSala = preparaEmail(ListDestinatarios,GuiaDistribuicaoId,UsrLogado,msg, conn);
							//Enviar Email 
	                        monitor.gravaMonitor("Enviando e-mail...", monitor.MSG_INFO);
							if (qtdProcessoSala.size()>0)
								enviaEmail (qtdProcessoSala,GuiaDistribuicaoId,UsrLogado,emailBroker);
						}
						msg.delete(0, msg.length());
					}
				}
			}
			monitor.gravaMonitor("Final do Processamento do robo InformarNumProcGuiaRob", monitor.MSG_FIM);
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Monitorar a quantidade de Processados nas guias: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new InformarNumProcConsolidadoRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	public ArrayList preparaEmail(ArrayList ListDestinatarios,GuiaDistribuicaoBean GuiaDistribuicaoId,
			UsuarioBean UsrLogado, StringBuffer msg, Connection conn) throws BeanException, DaoException, ServiceLocatorException
	{
		ArrayList qtdProcessoSala = new ArrayList();
        
		//Preparar Mensagem
		consolidaQTDMes(GuiaDistribuicaoId, msg);
		msg.append(quebraLinha + quebraLinha +"Atenciosamente,"+ quebraLinha +"Auditoria SMIT");
        
		//Verifico para qual nivel enviar o e-mail
		Iterator it = ListDestinatarios.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			Iterator itx = nivelProcesso.getListNivelDestino().iterator();
			GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
			ArrayList lDestinatarios = new ArrayList();
			while (itx.hasNext())
			{
				destinatario=(GER.CadastraDestinatarioBean)itx.next();
				if(destinatario.getCodOrgao().equals(UsrLogado.getCodOrgaoAtuacao()))
				{
					if(nivelProcesso.getValNivelProcesso().equals(GuiaDistribuicaoId.getTipoReqValidos()))
					{
                        destinatario.getMsg().add(msg);
                        lDestinatarios.add(destinatario);
					}
				}
			}
			nivelProcesso.setListNivelDestino(lDestinatarios);
			qtdProcessoSala.add(nivelProcesso);
		}
		return qtdProcessoSala;
	}
	
	
	public void enviaEmail (ArrayList  qtdProcessoSala,GuiaDistribuicaoBean GuiaDistribuicaoId,
			UsuarioBean UsrLogado, String emailBroker ) throws DaoException
			{
		Iterator it = qtdProcessoSala.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			Iterator itx = nivelProcesso.getListNivelDestino().iterator();
			GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
			while (itx.hasNext())
			{
				destinatario=(GER.CadastraDestinatarioBean)itx.next();
				if(destinatario.getCodOrgao().equals(UsrLogado.getCodOrgaoAtuacao()))
				{
					if(nivelProcesso.getValNivelProcesso().equals(GuiaDistribuicaoId.getTipoReqValidos()))
					{
						sys.EmailBean EmailBeanId = new sys.EmailBean();
						EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
						EmailBeanId.setRemetente( emailBroker );
						EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso());
						EmailBeanId.setConteudoEmail( ((StringBuffer)destinatario.getMsg().get(0)).toString() );
						try {
							EmailBeanId.sendEmailHtml();
						} catch (Exception e) {
							monitor.gravaMonitor("Erro ao enviar e-mail: " + e.getMessage(), monitor.MSG_ERRO);
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	public void consolidaQTDMes(GuiaDistribuicaoBean GuiaDistribuicaoId, StringBuffer msg) throws BeanException{
		    int nProcessos=0;
		    int mes = 0;
	        String mesLido = "";
	        String anoLido = ""; 
	        StringBuffer msgCont = new StringBuffer();
	        boolean bInicializa = true;
	        double percentual = 0.0d;
	        BigDecimal arredonda = new BigDecimal(0);
	        
			Iterator itAutos = GuiaDistribuicaoId.getAutos().iterator();
	        AutoInfracaoBean myAuto = new AutoInfracaoBean();

	        while (itAutos.hasNext())
	        {
	            myAuto = (AutoInfracaoBean)itAutos.next();
	            if(bInicializa){
	               mesLido = myAuto.getDatProcesso().substring(3,5);
	               bInicializa = false;
	            }
	               
	            if (myAuto.getDatProcesso().substring(3,5).equals(mesLido))
	            {
	            	nProcessos++;
	            	mesLido = myAuto.getDatProcesso().substring(3,5);
	            	anoLido = myAuto.getDatProcesso().substring(6,10);
	            }
	            else{
	            	mes = Integer.parseInt(mesLido);
	            	percentual = (Double.parseDouble(Integer.toString(nProcessos)) * 100)/GuiaDistribuicaoId.getAutos().size();
	            	arredonda = new BigDecimal(percentual);
	            	arredonda = arredonda.setScale(2,BigDecimal.ROUND_HALF_DOWN);
	            	msgCont.append("* "+mesExtenso(mes)+ "/"+ anoLido + " - " + nProcessos +"&nbsp;&nbsp;("+arredonda+"%)"+quebraLinha) ;
	            	nProcessos = 1;
	            	mesLido = myAuto.getDatProcesso().substring(3,5);
	            }
	        }
	        //grava a �ltima contagem do loop
	        mes = Integer.parseInt(mesLido);
	        percentual = (Double.parseDouble(Integer.toString(nProcessos)) * 100)/GuiaDistribuicaoId.getAutos().size();
	        arredonda = new BigDecimal(percentual);
        	arredonda = arredonda.setScale(2,BigDecimal.ROUND_HALF_DOWN);
	        msgCont.append("* "+mesExtenso(mes)+ "/"+ anoLido + " - " + nProcessos +"&nbsp;&nbsp;("+arredonda+"%)" + quebraLinha) ;
	        msg.append(msgCont.toString());
	}
	
	public String mesExtenso(int mes)
	{
		String nomMesExtenso = "";
		String [] meses = {"","JANEIRO","FEVEREIRO","MAR�O","ABRIL","MAIO","JUNHO",
				           "JULHO","AGOSTO","SETEMBRO","OUTUBRO","NOVEMBRO","DEZEMBRO"};
		nomMesExtenso = meses[mes];
		return nomMesExtenso;
	}
	
	
}