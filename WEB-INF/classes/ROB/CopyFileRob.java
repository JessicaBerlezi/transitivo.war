package ROB;


import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.apache.commons.io.FileUtils;

import REG.ArquivoRecebidoBean;
import REG.LinhaArquivoRec;



import sys.DaoException;

public class CopyFileRob {

	private static final String NOM_ROBOT = "CopyFileRob";



	public CopyFileRob() throws RobotException, DaoException, ROB.DaoException {

		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);

		try {  
			conn = Dao.getInstance().getConnection();

			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);

			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}

			/*Bean de Usu�rio*/
			ACSS.UsuarioBean usrLogado = new ACSS.UsuarioBean();
			usrLogado.getOrgao().setCodOrgao("1");
			usrLogado.getOrgao().setSigOrgao("DETRAN");
			usrLogado.setCodOrgaoAtuacao("119100");
			usrLogado.setSigOrgaoAtuacao("PMERJ");
			usrLogado.setNomUserName("SMIT");

			/*Bean de Par�metro por �rg�o*/
			REC.ParamOrgBean ParamOrgaoId = ParamOrgaoId = new REC.ParamOrgBean() ;
			ParamOrgaoId.setCodOrgao("119100");
			ParamOrgaoId.PreparaParam(usrLogado);

			
			/*Bean de Sistema*/
			ACSS.ParamSistemaBean parSis = new ACSS.ParamSistemaBean();
			parSis.setCodSistema("40"); /*Registro*/
			parSis.PreparaParam(conn);


			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;

			String diretorio="C:/gaidefdp";
			String j_sigFuncao="";
			File file = new File(diretorio); 
			File afile[] = file.listFiles(); 
			int i = 0;
			REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			for (int j = afile.length; i < j; i++) {
				File arquivos = afile[i];
				if (arquivos.getName().toUpperCase().indexOf("EMITEPER")>=0) {
					j_sigFuncao="REG2110";
				}
				
				/*Verificar se existe o arquivo na TSMI_ARQUIVO_RECEBIDO*/
				arqRec.setArqFuncao(j_sigFuncao);
				System.out.println(arquivos.getName());
				BufferedReader br = null;				
				try {
					br = new BufferedReader(new FileReader(arquivos));
					String auxLinha ;
					List lstLinhas = new ArrayList();
					int prim = 0;
					while ((auxLinha = br.readLine()) != null) {
						auxLinha = auxLinha.replaceAll("'", " ");
						if (prim == 0) {
							prim = 1;                            
							arqRec.setNomArquivo(arquivos.getName().toUpperCase());                            
							if (arqRec.isIndProcessamento())
								arqRec.setCodStatus("0"); //Ser� processado
							else
								arqRec.setCodStatus("1"); //N�o ser� processado

							arqRec.setCodOrgaoLotacao(usrLogado.getOrgao().getCodOrgao());
							arqRec.setSigOrgaoLotacao(usrLogado.getOrgao().getSigOrgao());
							arqRec.setCodOrgaoAtuacao(usrLogado.getCodOrgaoAtuacao());
							arqRec.setCodOrgaoAtuacao(usrLogado.getCodOrgaoAtuacao());
							arqRec.setSigOrgaoAtuacao(usrLogado.getSigOrgaoAtuacao());
							arqRec.setNomUsername(usrLogado.getNomUserName());
							arqRec.setDatRecebimento(sys.Util.formatedToday().substring(0,10));                            
							arqRec.carregarHeader(auxLinha, usrLogado);
							arqRec.setNumProtocolo();

						} else {
							LinhaArquivoRec linha = new LinhaArquivoRec();
							linha.setDscLinhaArqRec(auxLinha);
							lstLinhas.add(linha);
						}												
					}
					arqRec.setLinhaArquivoRec(lstLinhas);
					arqRec.isInsert(ParamOrgaoId);
					String NomeArquivo=arqRec.getNomArquivoDir();
					if (NomeArquivo.lastIndexOf(".")== - 1)
						NomeArquivo+=".txt";
					else
						NomeArquivo = NomeArquivo.substring(0,NomeArquivo.lastIndexOf(".")) + ".txt";
					if ( arqRec.getArqFuncao().equals("EMITEVEX") || arqRec.getArqFuncao().equals("EMITEPER") ){
						String arqOrigem = file.getPath();
						String arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
						FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));
						
						/*S� deletar se copiar com sucesso*/
						File destino =  new File(arqDestino);
						if (destino.exists()) arquivos.delete();
					}					
				} catch (IOException e) {
					arqRec.setMsgErro("UpLoad ==> Erro ao ler arquivo: "+ e);
				}
				finally
				{
					if (br!=null) br.close();
				}
			}
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}




	public static void main(String[] args) throws RobotException {

		try {
			new CopyFileRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}


}



















