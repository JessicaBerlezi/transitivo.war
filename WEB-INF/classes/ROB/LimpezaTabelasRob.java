package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;

public class LimpezaTabelasRob {
    
    private static final String NOM_ROBOT = "LimpezaTabelasRob";
    
    public LimpezaTabelasRob() throws RobotException, DaoException {
        
        Connection conn  = null;
        Connection connLog  = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {            			
            conn = Dao.getInstance().getConnection();
            conn.setAutoCommit(false);
            connLog = Dao.getInstance().getConnection();
            
            monitor.setConn(connLog);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }   
            
            // Executa a consulta das tabelas a serem Limpas
            ArrayList listTab = new ArrayList();	            
            Dao.getInstance().ConsultaTabelasLimpeza(conn, null, listTab);          
            
            if (listTab.size() > 0) {
                LimpezaTabBean TabBean = new ROB.LimpezaTabBean();
                
                boolean processoParado = false;
                
                Iterator itx = listTab.iterator();
                while (itx.hasNext()) {
                    
                    //Verifica se o processo foi marcado para parar
                    if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT))
                        break;
                    
                    TabBean = (ROB.LimpezaTabBean) itx.next();
                    try {
                        TabBean.executarLimpezaNova(conn, monitor);
                        conn.commit();                        
                    } catch (RobotException e) {
                        conn.rollback();
                        monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);
                    }    
                }																			
            }            
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao efetuar a Limpeza de Tabelas: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }
            if (connLog != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(connLog);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }
            if (trava != null) {
                try { 
                    Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
                } catch (DaoException ed) {
                    throw new RobotException(ed.getMessage());
                }
            }
        }
    }
    
    public static void main(String[] args) throws RobotException {
        
        try {
            new LimpezaTabelasRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    
}
