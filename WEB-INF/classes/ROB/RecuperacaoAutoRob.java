package ROB;

import java.sql.Connection;
import java.util.Vector;


public class RecuperacaoAutoRob {
	
	private static final String NOM_ROBOT = "RecuperaAutoRob";
	private static final int MAX_REG_PROCESSAR = 50000;
	private static final int DIAS_AVANCAR = 30;
	
	public RecuperacaoAutoRob() throws RobotException {
		
		Connection conn = null;
		Connection connLog = null;
		String codRetornoProc = "";	
		String datProcessamento = sys.Util.formatedToday().substring(0,10);	
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);		
		try {
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			Dao dao = Dao.getInstance();
			REC.DaoBroker daoREC=REC.DaoBrokerFactory.getInstance();			
			
			REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean (); 
			ACSS.UsuarioBean usuario = new ACSS.UsuarioBean();
			usuario.setCodOrgaoAtuacao("999999");
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	
			
			REG.ControleVexBean regRecebido = new REG.ControleVexBean (); 
			
			boolean processoParado = false;		    
	
			//Limpa a mem�ria
			System.gc();
	
            //Busca o cod. retorno que ser� processado
            codRetornoProc = dao.BuscaCodRetornoProc(conn); 
            //atualiza a data in�cio do processamento
            dao.GravaInicioProcRecuperacao(conn,codRetornoProc);
            conn.commit();
            monitor.gravaMonitor("Inicio Recupera��o Arquivo Vex ", monitor.MSG_INICIO);
            System.out.println("Inicio Recupera��o Arquivo Vex ");
            int iContador=0;
            System.out.println("Contador==>"+iContador);
            while (true) {
    			//Verifica se foi marcado para parar
    			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
    				processoParado = true;
    			}
    			
                // Prepara lista de notifica��es que est�o retornados por cod de retorno de Credito
                // e ainda n�o foram recuperadas
    			
                Vector resRegRecuperacao = dao.BuscarListRegRecuperacao(codRetornoProc,conn,MAX_REG_PROCESSAR);
                if ((processoParado) || (resRegRecuperacao.size()==0)) break;
            
    		    boolean brecupera = false;
   			    for (int j = 0; j < resRegRecuperacao.size(); j++) {
   			    	iContador++;
   			    	if (iContador%200==0) System.out.println("Registros Lidos :"+iContador);
   			    	
				   //Verifica se foi marcado para parar
				   regRecebido = (REG.ControleVexBean) resRegRecuperacao.get(j);														
				   myAuto.setNumAutoInfracao(regRecebido.getNumAutoInfracao());
				   myAuto.setNumPlaca(regRecebido.getNumPlaca());				   
				   /*myAuto.LeAutoInfracaoLocal("auto");*/
				   myAuto.LeAutoInfracao("auto", usuario);
				   /*
				   if ( (myAuto.getNumPlaca().length()==0) ||
						(myAuto.getCodStatus().length()==0)
					  )continue;
				   */

				   System.out.println("Registro Lido :"+iContador+" +C�digo Retorno : "+codRetornoProc+" PK_COD_CONTROLE_VEX_MASTER : "+regRecebido.getPkCodControleVex()+" "+myAuto.getNumAutoInfracao());
				   brecupera = false;
                   // Notifica��o autua��o e Cod Status >2
				   if ((regRecebido.getTipNotificacao().equals("1")) && 
					   (Integer.parseInt(myAuto.getCodStatus())>2))  brecupera = true;
                   // Notifica��o penalidade e Cod Status >12
				   if ((regRecebido.getTipNotificacao().equals("1")==false) &&
					   (Integer.parseInt(myAuto.getCodStatus())>12)) brecupera = true; 
				   
				   if (myAuto.getIndPago().equals("2")) 
					   brecupera = true;
				   
				  
                   //	atualiza registro recuperado	
				   if (brecupera)   dao.GravaRecuperacao(regRecebido,conn,datProcessamento,myAuto.getCodStatus());
				   else 			dao.AtualizaDataProxRecuperacao(conn,regRecebido,DIAS_AVANCAR);
				   
				   conn.commit();					
			    }  // Fim do FOR	
				monitor.gravaMonitor(" Auto: " + 
				regRecebido.getNumAutoInfracao(), monitor.MSG_INFO);	
				/*Retirar*/
				break;
            }  // Fim While			
			if (processoParado) {
				monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+ regRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
			} else {
	            dao.GravaFinalProcRecuperacao(conn,codRetornoProc);
	            conn.commit();						            
				monitor.gravaMonitor("T�rmino Recupera��o Credito por Servi�o "+codRetornoProc+" ", 
							             monitor.MSG_INTERRUPCAO);
	            System.out.println("T�rmino Recupera��o Credito por Servi�o "+codRetornoProc+" ");				
			}					
		// Fim do Try
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Recuperar Credito por Servi�o: "+codRetornoProc+" " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			
			
		}
	}
	
	public static void main(String args[])  throws RobotException {
		
		try {
			new RecuperacaoAutoRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}