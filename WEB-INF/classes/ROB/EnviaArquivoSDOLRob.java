package ROB;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import sys.DaoException;
import sys.MonitorLog;

public class EnviaArquivoSDOLRob {
    
    private static final String NOM_ROBOT = "EnviaArquivoSDOLRob";
    private static final String quebraLinha = "\r";
    
    public EnviaArquivoSDOLRob() throws RobotException {
        
        Connection conn  = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {            			
            conn = Dao.getInstance().getConnection();
            REG.Dao dao = REG.Dao.getInstance();
            
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); //M�DULO REGISTRO	
            param.PreparaParam(conn);
            
            String dirDownload = param.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			//String dirDownload = "C:\\Temp";

            while (true) {                
                if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break; 
                
                //Busca os arquivos com status 1 - enviados ao Detran
                Vector resArquivos = dao.BuscarArquivosRecebidos("'SDOL'", "1", conn);
                if (resArquivos.size() == 0) break;
                
                REG.ArquivoRecebidoBean arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(0);
                
                monitor.setNomObjeto(arqRecebido.getNomArquivo());
                monitor.setCodOrgao(arqRecebido.getCodOrgao());
                monitor.gravaMonitor("Inicio do Envio do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
                System.out.println("Inicio do Envio do Arquivo " + arqRecebido.getNomArquivo());
                
                dao.CarregarLinhasArquivo(arqRecebido, conn);
                                
                //Gera o arquivo txt do arquivo antigo
                gerarArquivo(arqRecebido, dirDownload, monitor, conn);
                
                arqRecebido.setConn(conn);
                arqRecebido.setCodStatus("2");
                arqRecebido.setDatDownload(sys.Util.fmtData(new Date()));
                if (!arqRecebido.isUpdate())
                    throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
                
                monitor.setNomObjeto(arqRecebido.getNomArquivo());
                monitor.setCodOrgao(arqRecebido.getCodOrgao());
                monitor.gravaMonitor("T�rmino do Envio do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);                
            }
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Enviar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                try { 
                    Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }
        }
    }
    
    private void gerarArquivo(REG.ArquivoRecebidoBean arquivo, String dirDownload, MonitorLog monitor, 
        Connection conn) throws RobotException {
        
        try {
            FileWriter writer = null;
            writer = criarArquivo(arquivo, dirDownload);
            
            int QtdLinProc = 0;
            Iterator ite = arquivo.getLinhaArquivoRec().iterator();                    
            while (ite.hasNext()) {
                
                if ((QtdLinProc%200) == 0)
                    monitor.gravaMonitor("Arquivo: " + arquivo.getNomArquivo() + " Linhas: " +    
                        QtdLinProc+"/"+ arquivo.getLinhaArquivoRec().size(), monitor.MSG_INFO);
                
                REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();                    
                gravarLinha(writer, arquivo, linhaArq, conn);
                QtdLinProc++;										
            }
            
            if (writer != null)	writer.close();
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
    }    
    
    public FileWriter criarArquivo(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
        throws RobotException {
        
        String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();			
        
        //Cria o arquivo
        FileWriter writer;
        try {			
            File dir = new File(caminho.substring(0, caminho.lastIndexOf("/")));
            dir.mkdirs();
            File arq = new File(caminho);
            arq.createNewFile();
            writer = new FileWriter(arq);
            
            //Grava o header do arquivo	
            if (!arqRecebido.getHeaderGrv().equals("")) {
                writer.write(arqRecebido.getHeaderGrv());
                writer.write(quebraLinha);
            }
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
        return writer;
    }
    
    private void gravarLinha(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
        REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
        
        try {
            if (writer != null) {			
                writer.write(linhaArq.getLinhaGrv(arqRecebido, conn));
                writer.write(quebraLinha);
            }
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
    }
    
    public static void main(String[] args) throws RobotException {
        
        try {
            new EnviaArquivoSDOLRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    
}
