package ROB;

import java.sql.Connection;
import java.sql.Statement;

import UTIL.LogDeRecuperacao;


public class SincronizacaoRequerimentoMultaRob {
		
	private static final String NOM_ROBOT = "SincronizacaoReqMultaRob";
	
	public static void main(String[] args) throws RobotException {
		LogDeRecuperacao log = new LogDeRecuperacao();
		
		try {
		
			Connection conn  = null;
			Connection connLog  = null;
			Statement trava = null;
			sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
			
			
			try {            			
				conn = Dao.getInstance().getConnection();
				conn.setAutoCommit(false);
				connLog = Dao.getInstance().getConnection();
				
				monitor.setConn(connLog);
				monitor.setDestino(monitor.DES_BASE);
				monitor.setNomProcesso(NOM_ROBOT);
				
				try {               
					trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
				} catch (Exception e) {
					monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
					return;
				}
				
				ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
				paramReg.setCodSistema("24"); //MÓDULO GERENCIAL	
				paramReg.PreparaParam(conn);
				
				String dataUltimaSincronizacao   = paramReg.getParamSist("DT_ULT_SINC_REQ_ROB");
				
				ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean() ;
				
				if (dataUltimaSincronizacao.length() > 0){
					log.gravaMensagem("Inicio da sincronização ORACLE X ADABAS do dia");
//					Dao.getInstance().sincronizaRequerimentosMulta(dataUltimaSincronizacao, UsuarioBeanId,log);
					log.gravaMensagem("Final da sincronização ORACLE X ADABAS do dia");
				}
				
				log.gravaMensagem("Inicio insert na tabela TGER_REQ_RESULTADO");
				Dao.getInstance().incrementaTabelaTgerReqResultado(dataUltimaSincronizacao, UsuarioBeanId,log);
				log.gravaMensagem("Fim insert na tabela TGER_REQ_RESULTADO");
				
				
				
			} catch (Exception e) {
				
				throw new RobotException(e.getMessage());            
				
			} finally {
				if (conn != null) {
					try { 
						Dao.getInstance().setReleaseConnection(conn);
					} catch (Exception e) {
						throw new RobotException(e.getMessage());
					}
				}
				if (connLog != null) {
					try { 
						Dao.getInstance().setReleaseConnection(connLog);
					} catch (Exception e) {
						throw new RobotException(e.getMessage());
					}
				}             
				if (trava != null) {
					try {
						Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
					} catch (sys.DaoException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
					 
		} catch (Exception e) { 
			log.gravaMensagem("Erro de execução...."); 
		} 
	}
	
}