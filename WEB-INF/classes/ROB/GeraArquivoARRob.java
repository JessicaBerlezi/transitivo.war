package ROB;


import java.sql.Connection;
import java.sql.Statement;

import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;
import ACSS.UsuarioBean;
import REG.ArquivoRecebidoBean;
import ROB.Dao;


public class GeraArquivoARRob {
	
	private static final String MYABREVSIST = "ROB";
	private static final String NOM_ROBOT = "ROB.GeraArquivoARRob";
	public  static final String QUEBRA_LINHA = "\n";
	private static final String NOM_USERNAME = "GerArqARRobot";	
	private static final int    NUM_MAX_RETORNOS = 1000;
	
	public GeraArquivoARRob() throws DaoException, ServiceLocatorException, RobotException, ROB.DaoException, DaoException {
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		REG.Dao dao = REG.Dao.getInstance();
		try {				
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	

			ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
			paramReg.setCodSistema("40"); //M�DULO REGISTRO	
			paramReg.PreparaParam(conn);
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			UsuarioBean UsuarioBeanId = new UsuarioBean();
			UsuarioBeanId.getOrgao().setCodOrgao(paramReg.getParamSist("ORGAO_CODRET"));
			UsuarioBeanId.setCodOrgaoAtuacao(paramReg.getParamSist("ORGAO_CODRET")); 
			UsuarioBeanId.setNomUserName(NOM_USERNAME);

			REC.ParamOrgBean ParamOrgaoId = ParamOrgaoId = new REC.ParamOrgBean();
			ParamOrgaoId.setCodOrgao(paramReg.getParamSist("ORGAO_CODRET"));
			ParamOrgaoId.PreparaParam(UsuarioBeanId);
			
			String dirDownload = paramReg.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			String qtdReg = null; // Par�metro criado para manipular a quantidade de registros do arquivo.
			
			monitor.gravaMonitor("In�cio Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);
			
			while (true) {                
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					monitor.gravaMonitor("Fim Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);					
					break;
				}
				
				ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
				arqRecebido.setCodIdentArquivo("CODRET");
				arqRecebido.setArqFuncao("REG2017");
				arqRecebido.calcProximoArquivo(ParamOrgaoId,UsuarioBeanId.getCodOrgaoAtuacao());
				arqRecebido.setNomArquivo(arqRecebido.getProximoArquivo());
				arqRecebido.setCodStatus("0");
				arqRecebido.setCodArquivo("0");
				arqRecebido.setCodOrgao(paramReg.getParamSist("ORGAO_CODRET"));
				arqRecebido.setCodOrgaoLotacao(UsuarioBeanId.getOrgao().getCodOrgao());
				arqRecebido.setSigOrgaoLotacao(UsuarioBeanId.getOrgao().getSigOrgao());
				arqRecebido.setCodOrgaoAtuacao(UsuarioBeanId.getCodOrgaoAtuacao());
				arqRecebido.setSigOrgaoAtuacao(UsuarioBeanId.getSigOrgaoAtuacao());
				arqRecebido.setNomUsername(NOM_USERNAME);
				arqRecebido.setDatRecebimento(sys.Util.formatedToday().substring(0,10));
				arqRecebido.setNumAnoExercicio(arqRecebido.getDatRecebimento().substring(6,10));
				arqRecebido.setNumProtocolo();
				arqRecebido.setNumControleArq(Util.lPad(ParamOrgaoId.getParamOrgao("PROXIMO_CODRET"),"0",arqRecebido.getTamNumControle()));
				

				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo());
				
				dao.CarregarLinhasArquivoAR(arqRecebido,conn,NUM_MAX_RETORNOS);
				if (arqRecebido.getLinhaArquivoRec().size() < NUM_MAX_RETORNOS ) {
					monitor.gravaMonitor("T�rmino da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);
					break;
				}
				
				/*Gravar Master / Detail*/
				arqRecebido.setQtdReg(String.valueOf(arqRecebido.getLinhaArquivoRec().size()));
				arqRecebido.setTipHeader("3");
				if (!arqRecebido.isInsert(ParamOrgaoId))
				  throw new DaoException("Erro ao Gerar Arquivo CODRET : " + arqRecebido.getNomArquivo() + " !");
				dao.AtualizarLinhasArquivoAR(arqRecebido,conn);
				conn.commit();	

				
				monitor.gravaMonitor("T�rmino da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
			}
			
			monitor.gravaMonitor("Fim Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);			
			
			
		} catch (Exception e) {
			try {				
				conn.rollback();
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}             
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	
	public static void main(String args[])  throws DaoException {		
		try {			
			new GeraArquivoARRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}