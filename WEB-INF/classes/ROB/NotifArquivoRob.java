package ROB;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import sys.DaoException;
import sys.Util;
import REG.ArquivoRecebidoBean;
import REG.LinhaArquivoRec;

public class NotifArquivoRob {
	
	private static final String NOM_ROBOT = "NotifArquivoRob";
	
	public NotifArquivoRob() throws RobotException, DaoException {
		
		Connection conn  = null;
		Connection connLog  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		FileWriter writer = null;
		File zip = null;
		
		try {
			
			monitor.gravaMonitor("In�cio da Verifica��o de Pend�ncias. "+ new Date(), monitor.MSG_INICIO);
			System.out.println("In�cio da Verifica��o de Pend�ncias.");

			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);			
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
			paramReg.setCodSistema("40"); //M�DULO REGISTRO	
			paramReg.PreparaParam(conn);
			
			String dtInicioPendencia = paramReg.getParamSist("DATA_CONTROLE_EMITEVEX");
			
			String emailBroker   = paramReg.getParamSist("EMAIL_LOG_BROKER");
			ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(paramReg,"PENDENCIA_EMITEVEX");			
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			String dirDownload = paramReg.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;                 
			
			REG.ArquivoRecebidoBean arqRecebido = (new REG.ArquivoRecebidoBean());
			LinhaArquivoRec linhaRec = new REG.LinhaArquivoRec();
			linhaRec.setCodStatus("8");
			linhaRec.setCodRetorno("");
			arqRecebido.setCodIdentArquivo("EMITEPER");
			ArrayList listFotos = null;
			
			// Carrega todas as linhas que est�o ajustadas prontas para envio.
			monitor.gravaMonitor("In�cio da Verifica��o de Pend�ncias. CarregarLinhasAjustadas...(Status=8)", monitor.MSG_INICIO);
			System.out.println("In�cio da Verifica��o de Pend�ncias. CarregarLinhasAjustadas...(Status=8)");
			dao.CarregarLinhasAjustadas(arqRecebido, linhaRec, conn,dtInicioPendencia);
			

			
			/* In�cio l�gico do bloco de emiss�o de linhas com foto
			* Esse Bloco l�gico verifica a exist�ncia de foto para linhas com status 9 e
			* acrescentando, em caso positivo, essa linha na lista do objeto arqRecebido. 
			*/
			
			ArrayList Pendencias       = new ArrayList();
			ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			linhaRec.setCodStatus("9");
			linhaRec.setCodRetorno("N02");
			arqRec.setCodIdentArquivo("EMITEPER");
			monitor.gravaMonitor("In�cio da Verifica��o de Pend�ncias. CarregarLinhasAjustadas...(Status=9)", monitor.MSG_INICIO);
			System.out.println("In�cio da Verifica��o de Pend�ncias. CarregarLinhasAjustadas...(Status=9)");
			dao.CarregarLinhasAjustadas(arqRec, linhaRec, conn,dtInicioPendencia);
			
			dao.verificaNotificacoes(arqRec, conn);
			System.out.println("VerificaNotificacoes...");
			
			String [] listPrazo = {"5","15","30"};
			
			for(int x=0; x<arqRec.getLinhaArquivoRec().size();x++){
				// Verifica se a linha passou sem erros no filtro de verifica��es das notifica��es.
				// Verifica se foto pendente de foto
				for (int iPrazo=0;iPrazo<listPrazo.length;iPrazo++) {
					
					if ((linhaRec.getCodRetorno().equals("N02")) && 
						(Util.DifereDias(Util.formatedToday().substring(0,10),linhaRec.getDatRecebimento())==Integer.parseInt(listPrazo[iPrazo])) ) {
						
						if (ListDestinatarios.size()>0)
						{
							Iterator it = ListDestinatarios.iterator();
							GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
							while (it.hasNext())
							{
								nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
								if (nivelProcesso.getValNivelProcesso().equals("S"))
								{
									Iterator itx = nivelProcesso.getListNivelDestino().iterator();
									GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
									ArrayList lDestinatarios = new ArrayList();
									while (itx.hasNext())
									{
										destinatario=(GER.CadastraDestinatarioBean)itx.next();
										if(destinatario.getCodOrgao().equals(linhaRec.getCodOrgao()))
										{
											//Mensagem de Pendencias
											String msg = "";
											if(nivelProcesso.getTipoMsg().equals("M"))
											{
												msg = "Cod.Orgao.......: "+ arqRecebido.getCodOrgao()+" Auto.........: "+ linhaRec.getNumAutoInfracao()+" \n" +
												"Placa...........: "+ linhaRec.getDscLinhaArqRec().substring(125,132)+" Data Infracao: "+ 
												Util.formataDataDDMMYYYY(linhaRec.getDscLinhaArqRec().substring(212,220))+"\n"+
												"Hora Infracao...: "+ linhaRec.getDscLinhaArqRec().substring(220,225)+" Municipio: "+ linhaRec.getDscLinhaArqRec().substring(225,275)+"\n"+
												"Motivo Pendencia: "+ linhaRec.getNomStatusErro("REG0510")+"\n"+
												"-------------------------------------------------------------------------- \n";
											}  
											destinatario.getMsg().add(msg);
											lDestinatarios.add(destinatario);
										}
									}
									nivelProcesso.setListNivelDestino(lDestinatarios);
									Pendencias.add(nivelProcesso);
								}
							}
						}
					}
				}
				
				
				// Verifica se a linha passou sem erros no filtro de verifica��es das notifica��es.				
				if(!((LinhaArquivoRec)arqRec.getLinhaArquivoRec().get(x)).getCodStatusCorrecao().equals("0")){
					arqRec.getLinhaArquivoRec().remove(x);
					x--;
					// Atualiza a linha com o novo erro identificado.
					if(!(linhaRec.getCodRetorno().equals("N02"))) {
						linhaRec.setConn(conn);
						dao.atualizaStatusNotif(linhaRec);
					}
				}
			}
			for(int y=0;y<arqRec.getLinhaArquivoRec().size();y++){
				arqRecebido.getLinhaArquivoRec().add(arqRec.getLinhaArquivoRec().get(y));
			}
			// Fim do Bloco l�gico do envio de linhas com foto.
			
			/*
			 * Verifica se existem notifica��es a serem enviadas.
			 * Caso n�o exista nenhuma notifica��o a ser enviada a execu��o do Rob�
			 * � interrompida.
			 */
			
			
			
			/*Enviar Email*/
			if (Pendencias.size()>0)
			{  
				int contNivel = 0;
				Iterator it = Pendencias.iterator();
				GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
				while (it.hasNext())
				{
					contNivel++;
					nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
					if (nivelProcesso.getValNivelProcesso().equals("S") && (contNivel <= ListDestinatarios.size()))
					{
						int contDestinatarios = 0;
						Iterator itx = nivelProcesso.getListNivelDestino().iterator();
						GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
						while (itx.hasNext())
						{
							contDestinatarios++;
							destinatario=(GER.CadastraDestinatarioBean)itx.next();
							if(destinatario.getMsg().size()>0 && (contDestinatarios <= nivelProcesso.getListNivelDestino().size()))
							{
								String sConteudo="";
								Iterator ity = destinatario.getMsg().iterator();			
								while (ity.hasNext()) 
									sConteudo+=(String)ity.next();
								
								sys.EmailBean EmailBeanId = new sys.EmailBean();
								EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
								EmailBeanId.setRemetente(emailBroker);
								EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+ 
										" - Arquivo: "+arqRecebido.getNomArquivo()+ 
										" - Data: "+arqRecebido.getDatRecebimento());
								EmailBeanId.setConteudoEmail(sConteudo);
								EmailBeanId.sendEmail();
							}
						}
					}
				}
			}
			
			if (arqRecebido.getLinhaArquivoRec().size() == 0) {
				monitor.gravaMonitor("T�rmino da Verifica��o de Pend�ncias: N�o existem linhas para atualizar !", monitor.MSG_FIM);
				return;
			} else {				
				monitor.gravaMonitor("T�rmino da Verifica��o de Pend�ncias: " + arqRecebido.getLinhaArquivoRec().size() + " linhas para atualizar !", monitor.MSG_FIM);
				System.out.println("T�rmino da Verifica��o de Pend�ncias: " + arqRecebido.getLinhaArquivoRec().size() + " linhas para atualizar !");
			}
			
			// Associa os arquivos aos quais as linhas fazem parte.
			List arquivos = dao.AssociarLinhas(arqRecebido, conn);
			
			// Ajusta o nome dos arquivos, quando necess�rio.
			acertarNomeArquivo(arquivos);          
			
			/*
			 * Executa um loop com intuito de gerar todos os novos arquivos a serem
			 * enviados pelo Rob�
			 */ 
			String qtdReg;
			for(int pos = 0; pos<arquivos.size(); pos++){
				arqRecebido = (ArquivoRecebidoBean)arquivos.get(pos);
				
				// Inicia o monitoramento de gera��o de um arquivo
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo());    
				
				qtdReg = arqRecebido.getQtdReg();
				arqRecebido.setQtdReg(String.valueOf(arqRecebido.getLinhaArquivoRec().size())) ;
				writer = EmpacotaArquivoRob.criarArquivoGrv(arqRecebido, dirDownload);
				
				int QtdLinProc = 0;
				listFotos = new ArrayList();
				HashMap listFotosHash = new HashMap();				
				Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();                
				while (ite.hasNext()) {
					
					if ((QtdLinProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
								QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size(), monitor.MSG_INFO);
					
					REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
					Dao.getInstance().ControleEmiteVEXV1(linhaArq);					
					
					
					
					EmpacotaArquivoRob.gravaGrv(writer, arqRecebido, linhaArq, conn);
					EmpacotaArquivoRob.verificaFoto(paramRec, linhaArq, listFotos,listFotosHash);
					QtdLinProc++;										
				}
				
				if (writer != null)	writer.close();
				
				//Cria o arquivo zip com as fotos
				if (listFotos.size() > 0)
					zip = EmpacotaArquivoRob.criarArquivoZip(conn, arqRecebido, dirDownload, listFotos,listFotosHash);                
				
				arqRecebido.setCodStatus("2");
				arqRecebido.setDatRecebimento(sys.Util.fmtData(new Date()));
				arqRecebido.setQtdReg(qtdReg);
				/*arqRecebido.setQtdFoto(listFotos.size());*/
				arqRecebido.setQtdFoto(listFotosHash.size());				
				if (!arqRecebido.isAtualizado(conn))
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getMsgErro());
				
				conn.commit();
				
				monitor.gravaMonitor("T�rmino da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
			}
		} 
		catch (Exception e) {
			System.out.println("Mensagem:"+e.getMessage());			
			try {
				if (writer != null) {
					writer.close();
					writer.getFile().delete();
				}
				if (zip != null) zip.delete();
				
				conn.rollback();
				monitor.gravaMonitor("Erro ao Gerar os Novos Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				System.out.println("Mensagem:"+ed.getMessage());				
				throw new RobotException(ed.getMessage());
			}
			System.out.println("Mensagem:"+e.getMessage());			
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try {
					monitor.gravaMonitor("T�rmino da Verifica��o de Pend�ncias. "+ new Date(), monitor.MSG_INICIO);					
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					System.out.println("Mensagem:"+e.getMessage());					
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					System.out.println("Mensagem:"+e.getMessage());					
					throw new RobotException(e.getMessage());
				}
			}			
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) 
				{
					System.out.println("Mensagem:"+ed.getMessage());
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	/**
	 * M�todo utilizado para gerar o nome do novo arquivo
	 * @param arquivos
	 */
	private void acertarNomeArquivo(List arquivos) throws Exception{		
		try{
			for (int pos = 0; pos < arquivos.size(); pos++) {
				ArquivoRecebidoBean arqAux = (ArquivoRecebidoBean)arquivos.get(pos);				
				String ultArquivo = REG.Dao.getInstance().buscaNomeArquivo(arqAux);				
				if (!ultArquivo.equals("")) {
					String nomArquivo = "";
					if (ultArquivo.length() == 21)
						nomArquivo = ultArquivo.substring(0,17) + "_001." + arqAux.getSufixoRetornoGrv();
					else {
						int seq = Integer.parseInt(ultArquivo.substring(18,21));
						StringBuffer buffer = new StringBuffer(ultArquivo);
						nomArquivo = buffer.replace(18, 21, Util.lPad(String.valueOf(++seq), "0", 3)).toString();
					}
					arqAux.setNomArquivo(nomArquivo);
				}				
			}		
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}	
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new NotifArquivoRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}	
	
}
