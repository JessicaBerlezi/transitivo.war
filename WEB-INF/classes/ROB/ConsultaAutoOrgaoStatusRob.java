package ROB;

import java.sql.Connection;
import java.sql.Statement;


public class ConsultaAutoOrgaoStatusRob {
    
    private static final String NOM_ROBOT = "ConsultaAutoOrgaoStatusRob";

    public ConsultaAutoOrgaoStatusRob() throws RobotException, DaoException {
        
        Connection conn  = null;
		    Connection connLog  = null;
		    Statement trava = null;
		    String indContinua = ""; 
		    Statement stmt = null;
		    
		    String resultado = "";
		    String linha = "";
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		    ControleRobAutoBean controleRobAutoBean = new ControleRobAutoBean();
		    boolean continua = true; 
		    int numAutoProcess = 0;
		         
        try {
						conn = Dao.getInstance().getConnection();
			      connLog = Dao.getInstance().getConnection();
						conn.setAutoCommit(false);
						monitor.setConn(connLog);
						monitor.setDestino(monitor.DES_BASE);
						monitor.setNomProcesso(NOM_ROBOT);	
			      controleRobAutoBean.setDatInicio(sys.Util.getDataHoje());
								                   			
			      try {               
							trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
						} catch (Exception e) {
							monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
							return;
						}
		         
		        //Executa transacao 043 - CONSULTA AUTOS P/ORGAO-STATUS
			      ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			      param.setCodSistema("24"); //M�DULO GERENCIAL	
			      param.PreparaParam(conn);
            int diasCarga = Integer.parseInt(param.getParamSist("DIAS_CARGA_TABELA"));
			      
			      //Verifica ultima data
			      String ultimaData = Dao.getInstance().verificaUltData();
			      if (ultimaData == null)ultimaData = "";
			      if(ultimaData.equals("")) ultimaData = sys.Util.getDataHoje();
			      
			      //verifica se o indContinua 
			      indContinua = Dao.getInstance().verificaIndContinua(ultimaData);
			      if (indContinua == null)indContinua = "";
			      if (indContinua.equals("")){
			      	 indContinua = sys.Util.lPad("", " ", 12);
					     ultimaData = sys.Util.addData(ultimaData,1);
			      }
			      
            // Volta 7 dias de acordo com a ultima Data
						String dataInfracao = sys.Util.formataDataYYYYMMDD(sys.Util.addData(ultimaData,diasCarga));
			      	
			      String parteVar = sys.Util.lPad("", "9", 9); ; 

          
			      while (continua){
								//Verifica se foi marcado para parar
								if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)){
									 controleRobAutoBean.setDatFim(sys.Util.getDataHoje());
				           controleRobAutoBean.setSitProces(controleRobAutoBean.SIT_PARADA);
						       controleRobAutoBean.setQtdAutosProces(numAutoProcess);
							     controleRobAutoBean.setTxtProces("Processamento interrompido - PARADA.");
							     controleRobAutoBean.gravaDadosControle(controleRobAutoBean);
									 break; 
								}
							  
					      //Chama a Transacao
								REC.DaoBroker daoBroker = REC.DaoBrokerFactory.getInstance();			
								resultado = daoBroker.chamaTransBRK("043",parteVar+indContinua,"");
  
					      if (REC.DaoBrokerFactory.getInstance().verErro(null,resultado,"Erro na Transa��o 043.",stmt)==false) break; 
					        
 						    for (int i=0; i < 38; i++) {
										linha = resultado.substring((i*83)+3,(i+1)*83+3);
								    REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
								    daoBroker.setPosIni(0);											
										myAuto.setNumAutoInfracao(daoBroker.getPedaco(linha,12," "," "));
										myAuto.setCodStatus(daoBroker.getPedaco(linha,3," "," "));
								    myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(daoBroker.getPedacoDt(linha,8)));
								    myAuto.getInfracao().setCodInfracao(daoBroker.getPedaco(linha,3," "," "));
								    myAuto.getInfracao().setDscEnquadramento(daoBroker.getPedaco(linha,20," "," "));
								    myAuto.getInfracao().setNumPonto(daoBroker.getPedacoNum(linha,1,0,"."));
								    myAuto.getInfracao().setIndTipo(daoBroker.getPedaco(linha,1," ", " "));
								    myAuto.setCodOrgao(daoBroker.getPedacoNum(linha,6,0,"."));
								    myAuto.getInfracao().setValReal(daoBroker.getPedacoNum(linha,9,2,","));
								    myAuto.setDatVencimento(sys.Util.formataDataDDMMYYYY(daoBroker.getPedacoDt(linha,8)));
								    myAuto.setCodMunicipio(daoBroker.getPedacoNum(linha,4,0,"."));
							    	myAuto.setNumPlaca(daoBroker.getPedaco(linha,7," "," "));
								    myAuto.getInfracao().setValRealDesconto(daoBroker.getPedacoNum(linha,9,2,","));
								    //grava dados da transacao na tabela
								    Dao.getInstance().carregaTabela(myAuto);
								    numAutoProcess ++ ;
								    controleRobAutoBean.setQtdAutosProces(numAutoProcess);
								 }
					       conn.commit();		
						     daoBroker.setPosIni(3+(38*83));
								 indContinua = daoBroker.getPedaco(resultado,12," "," ") ;		
								 if (indContinua.trim().length()<=0) continua = false;								 
					  }
			      controleRobAutoBean.setDatFim(sys.Util.getDataHoje());
			      controleRobAutoBean.setIndContinua("");
			      controleRobAutoBean.setTxtProces("Opera��o realizada com sucesso.");
			      controleRobAutoBean.setSitProces(controleRobAutoBean.SIT_OK);
			      controleRobAutoBean.gravaDadosControle(controleRobAutoBean);

        } catch (Exception e) {
            try {
                monitor.gravaMonitor("ConsultaAutoOrgaoStatusRob - Erro ao Efetuar Carga  : " + e.getMessage(), monitor.MSG_ERRO);
				        controleRobAutoBean.setDatFim(sys.Util.getDataHoje());
					      controleRobAutoBean.setSitProces(controleRobAutoBean.SIT_ERRO);
						    controleRobAutoBean.setQtdAutosProces(numAutoProcess);
				        controleRobAutoBean.setIndContinua(indContinua);
				        if(e.getMessage()== null)controleRobAutoBean.setTxtProces(resultado.substring(0,49));
				        else controleRobAutoBean.setTxtProces(e.getMessage());
				        controleRobAutoBean.gravaDadosControle(controleRobAutoBean);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
        }
    }
    
    public static void main(String[] args) throws RobotException {
        
        try {
            new ConsultaAutoOrgaoStatusRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    
}
