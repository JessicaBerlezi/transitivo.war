package ROB;

import java.sql.Connection;
import java.sql.Statement;

public class AtualizarTabRob {
    
    private static final String NOM_ROBOT = "AtualizarTabRob";
    
    public AtualizarTabRob() throws RobotException, sys.DaoException {
        
        Connection conn = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {
            conn = Dao.getInstance().getConnection();
            
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            } 
            
            monitor.gravaMonitor("Inicio da Carga das Tabelas", monitor.MSG_INICIO);
            
            TAB.MarcaModeloCmd marca = new TAB.MarcaModeloCmd();
            marca.carregaTab(conn, monitor);
            
            TAB.TipoCmd tipo = new TAB.TipoCmd();
            tipo.carregaTab(conn, monitor);
            
            TAB.MunicipioCmd municipio = new TAB.MunicipioCmd();
            municipio.carregaTab(conn, monitor);
            
            TAB.CoresCmd cor = new TAB.CoresCmd();
            cor.carregaTab(conn, monitor);
            
            TAB.EspecieCmd especie = new TAB.EspecieCmd();
            especie.carregaTab(conn, monitor);
            
            TAB.CategoriaCmd categoria = new TAB.CategoriaCmd();
            categoria.carregaTab(conn, monitor);
            
            TAB.InfracoesCmd infracao = new TAB.InfracoesCmd();
            infracao.carregaTab(conn, monitor);
            
            TAB.UnidadesCmd unidade = new TAB.UnidadesCmd();
            unidade.carregaTab(conn, monitor);
            
            TAB.AgentesCmd agente = new TAB.AgentesCmd();
            agente.carregaTab(conn, monitor);
            
            monitor.gravaMonitor("Fim da Carga das Tabelas", monitor.MSG_FIM);
            
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro na Carga das Tabelas: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }
    
    public static void main(String args[]) {
        
        try {
            new AtualizarTabRob();
        } catch (Exception e) {
            System.out.println(e);			
        }
    }
    
}