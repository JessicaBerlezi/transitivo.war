package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;
import sys.Util;
import REG.ArquivoRecebidoBean;

public class EnvioArquivoAuditoriaRob {
    
    private static final String NOM_ROBOT = "EnvioArquivoAuditoriaRob";
	private static final String quebraLinha = "\n";
    
    public EnvioArquivoAuditoriaRob() throws RobotException, ROB.DaoException, DaoException {
        
        Connection conn  = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {            			
            conn = Dao.getInstance().getConnection();
                        
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            // N�o permite que mais de uma instancia do robo rodem ao mesmo tempo 
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            // Carrega Parametros 
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); // M�DULO REGISTRO 
            param.PreparaParam(conn);
            ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"PROCESSAMENTO_ARQUIVO");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");		

          	// Verifica quais arquivos ainda n�o foram processados 		
			ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
			ArrayList arqNaoProcessado = Dao.getInstance().verificaArqNaoProcessados(arqRecebido,conn);
			
			// Prepara Email  
			String msg = "";
			String corpoMsg = "Listagem de Arquivos nao Processados: "+ quebraLinha ;
			ArrayList naoProcessados = new ArrayList();
			int nDiasSemProc = 0;
			if (ListDestinatarios.size()>0)
			{
				Iterator it = ListDestinatarios.iterator();
				GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
				while (it.hasNext())
				{
					nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
					if (nivelProcesso.getValNivelProcesso().equals("S"))
			     	{
						Iterator itx = nivelProcesso.getListNivelDestino().iterator();
						GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
						ArrayList lDestinatarios = new ArrayList();
						while (itx.hasNext())
						{
							destinatario=(GER.CadastraDestinatarioBean)itx.next();
							Iterator ity = arqNaoProcessado.iterator();
							ArquivoRecebidoBean arquivos= new ArquivoRecebidoBean();
							while (ity.hasNext())
							{
								arquivos=(ArquivoRecebidoBean)ity.next();
								if(destinatario.getCodOrgao().equals(arquivos.getCodOrgao()))
								{
									//Mensagem de Arquivos n�o processados
									if(nivelProcesso.getTipoMsg().equals("M"))
									{
										nDiasSemProc = Util.DifereDias(Util.fmtData(arquivos.getDatRecebimento()).replaceAll("-","/"),Util.getDataHoje().replaceAll("-","/"));
										msg =" Nome Arquivo: "+ arquivos.getNomArquivo()+ quebraLinha +
										   " Data Recebimento: "+ Util.fmtData(arquivos.getDatRecebimento()).replaceAll("-","/")+ quebraLinha +
										   " QTD. dias sem processar: "+nDiasSemProc+ quebraLinha +
										   "--------------------------------------------------------------------------"+quebraLinha;
									   destinatario.getMsg().add(msg);
									}  
								   
								}
							}
							if(destinatario.getMsg().size()>0)
						      lDestinatarios.add(destinatario);
					   }
					   nivelProcesso.setListNivelDestino(lDestinatarios);
					   naoProcessados.add(nivelProcesso);
				}
			  }
			}
			
			
			/* Enviar Email */
			if (naoProcessados.size()>0)
			{
				Iterator it = naoProcessados.iterator();
				GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
				while (it.hasNext())
				{
					nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
					if (nivelProcesso.getValNivelProcesso().equals("S"))
					{
						Iterator itx = nivelProcesso.getListNivelDestino().iterator();
						GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
						while (itx.hasNext())
						{
							destinatario=(GER.CadastraDestinatarioBean)itx.next();
							if(destinatario.getMsg().size()>0)
							{
								String sConteudo="";
								Iterator ity = destinatario.getMsg().iterator();			
								while (ity.hasNext()) 
								  sConteudo+=(String)ity.next();
								
								sys.EmailBean EmailBeanId = new sys.EmailBean();
								EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
								EmailBeanId.setRemetente(emailBroker);
								EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+" : "+destinatario.getCodOrgao()+" - "+destinatario.getSigOrgao());
								EmailBeanId.setConteudoEmail(corpoMsg+sConteudo);
								EmailBeanId.sendEmail();
							}
					   }
					}
				}
			}

			
	
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Monitorar os Arquivos n�o Processados: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }

 
    
  public static void main(String[] args) throws RobotException {
        
        try {
            new EnvioArquivoAuditoriaRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    

}