package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;

import REG.ArquivoRecebidoBean;

import ACSS.ParamSistemaBean;

/**
 * <b>Title:</b> SMIT - MODULO ROBOT<br>
 * <b>Description:</b> Rob� de Envio de Arquivos para o Detran<br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luiz Medronho
 * @version 1.0
 */

public class ProcessaRelatArDigitTMPRob {
	
	private static String NOM_ROBOT = "EnviaArqDetranRob";
	
	public ProcessaRelatArDigitTMPRob(String tipArquivo) throws RobotException, DaoException {
		
		//Define o nome especial do rob� pelo tipo passado
		if (tipArquivo.equals("MR")) NOM_ROBOT = NOM_ROBOT + "_MR";
		else if (tipArquivo.equals("ME")) NOM_ROBOT = NOM_ROBOT + "_ME";
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			
			ParamSistemaBean param = new ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO	
			param.PreparaParam(conn);
			
			REG.ArquivoRecebidoBean arqRecebido = new REG.ArquivoRecebidoBean ();			
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			/*
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             
			*/
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			boolean processoParado = false;
			String identArquivo = "'CODRET'";
			
			Vector resArquivos = dao.BuscarListArquivosRecebidos(identArquivo, "2", conn);
			REG.LinhaArquivoRec linhaArq=null;
			for (int i = 0; i < resArquivos.size(); i++) {			
				//Limpa a mem�ria
				System.gc();
				
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;			    
				
				//Busca os arquivos recebidos pelo tipo passado

				arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(i);
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio do Envio do Arquivo "+arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio do Envio do Arquivo: " + arqRecebido.getNomArquivo());
				
				arqRecebido.setConn(conn);				
				
				dao.CarregarLinhasArquivo(arqRecebido, conn);				
				
				boolean houveErroBroker = false;
				int QtdErros = Integer.parseInt(arqRecebido.getQtdLinhaErro());
				int QtdLinProc = 0;
				Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();
				int iLinha=0;
				while (ite.hasNext()) {				
					
					//Verifica se foi marcado para parar
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					}					
					
					if ((QtdLinProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
								QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size() + 
								" Linhas com Erro: "+QtdErros, monitor.MSG_INFO);
					
					linhaArq = (REG.LinhaArquivoRec) ite.next();
					iLinha++;					
					System.out.println("Processando..."+iLinha);

					
				
					//Verifica se a linha j� foi processada								
					if ("1".indexOf(linhaArq.getCodStatus()) >= 0) {
						
						linhaArq.processaArDigTMP(arqRecebido);
						
					}
					QtdLinProc++;
				} 
				monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
						QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size()
						, monitor.MSG_INFO);
				
				if (processoParado) {
					monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+arqRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
				} else {
					monitor.gravaMonitor("T�rmino do ProcessaARDigitTMP do Arquivo "+arqRecebido.
							getNomArquivo(), monitor.MSG_FIM);
				}											
				
				if (processoParado) break;
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Enviar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public static void main(String args[])  throws RobotException {
		
		try {
			String tipArquivo = "";
			if (args.length > 0) tipArquivo = args[0];
			
			new ProcessaRelatArDigitTMPRob(tipArquivo);
			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}