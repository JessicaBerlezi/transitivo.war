package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import sys.BeanException;
import sys.DaoException;

public class CargaSaldoOrgaoRob {
	
	private static String NOM_ROBOT = "CargaSaldoOrgaoRob";
	
	public CargaSaldoOrgaoRob() throws RobotException, DaoException {
		
		Connection conn = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date datProc = new Date();
			
			monitor.gravaMonitor("Inicio do carregamento de Saldos", monitor.MSG_INICIO);
			System.out.println(df.format(datProc) + " - Inicio do carregamento de Saldos");							
			
			boolean processoParado = false;
			
			List listOrgao = Dao.getInstance().ListarOrgaos();
			List listStatus = listarStatus();
			//List listStatus = Dao.getInstance().ListarStatus();
			
			Iterator itrOrgao = listOrgao.iterator();			
			while (itrOrgao.hasNext()) {
				
				//Verifica se foi marcado para parar				
				if (processoParado || Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					processoParado = true;
					break;
				}
				
				ACSS.OrgaoBean orgao = (ACSS.OrgaoBean) itrOrgao.next();
				
				monitor.setNomObjeto("Org�o: " + orgao.getCodOrgao());
				monitor.setCodOrgao(orgao.getCodOrgao());
				monitor.gravaMonitor("Inicio do carregamento dos Saldos do Org�o " + orgao.getCodOrgao(), 
						monitor.MSG_INICIO);
				System.out.println(df.format(new Date()) + " - Inicio do carregamento dos Saldos do Org�o "
						+ orgao.getCodOrgao());
				
				Iterator itrStatus = listStatus.iterator();
				while (itrStatus.hasNext()) {
					
					// Verifica se foi marcado para parar				
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					}
					
					REC.StatusBean status = (REC.StatusBean) itrStatus.next();
					Calendar cal = new GregorianCalendar();
					Date datAtual = new Date();
					
					int statusInt = Integer.parseInt(status.getCodStatus());
					if ((statusInt >= 0) && (statusInt <= 9)) { //Fase de Autua��o
						
						//M�s atual
						cal.setTime(datAtual);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						Date datInicial = cal.getTime();
						Date datFinal = datAtual;
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
						//M�s atual - 1
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -1);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
						//M�s atual - 2
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -2);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);					
						
						//M�s atual - 3 ate 01/08/2004
						cal.setTime(datAtual);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						cal.set(Calendar.MONTH, 7);
						cal.set(Calendar.YEAR, 2004);
						datInicial = cal.getTime();
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -3);
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
					} /*else if ( ((statusInt >= 10) && (statusInt <= 19))     //Fase de Penalidade
							||  ((statusInt >= 30) && (statusInt <= 39)) ) { //Fase de Recursos
						
						//M�s atual
						cal.setTime(datAtual);
						cal.set(Calendar.DAY_OF_MONTH, 1);						
						Date datInicial = cal.getTime();
						Date datFinal = datAtual;
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
						//M�s atual - 1
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -1);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
						//M�s atual - 2
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -2);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);					
						
						//M�s atual - 3
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -3);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);							
						
						//M�s atual - 4
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -4);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						datInicial = cal.getTime();
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);							
						
						//M�s atual - 5 ate 01/08/2004
						cal.setTime(datAtual);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						cal.set(Calendar.MONTH, 7);
						cal.set(Calendar.YEAR, 2004);
						datInicial = cal.getTime();
						cal.setTime(datAtual);
						cal.add(Calendar.MONTH, -5);
						cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));						
						datFinal = cal.getTime();
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
						
					} else if ((statusInt >= 90) && (statusInt <= 99)) { //Fase de Transito em Julgado
						
						//M�s atual ate 01/08/2004
						cal.setTime(datAtual);
						cal.set(Calendar.DAY_OF_MONTH, 1);
						cal.set(Calendar.MONTH, 7);
						cal.set(Calendar.YEAR, 2004);
						Date datInicial = cal.getTime();						
						Date datFinal = datAtual;
						this.carregar(conn, orgao, status, datInicial, datFinal, datProc);
					
					} else 
						continue;*/
				}
				
				monitor.gravaMonitor("Fim do carregamento dos Saldos do Org�o " + orgao.getCodOrgao(), 
						monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - Fim do carregamento dos Saldos do Org�o "
						+ orgao.getCodOrgao());
			}
			
			monitor.setNomObjeto("");
			monitor.setCodOrgao("");
			if (processoParado) {
				monitor.gravaMonitor("T�rmino solicitado pelo Gerente", monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - T�rmino solicitado pelo Gerente");
			} else {
				monitor.gravaMonitor("Fim do carregamento de Saldos", monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - Fim do carregamento des Saldos");
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao carregar Saldos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	private void carregar(Connection conn, ACSS.OrgaoBean orgao, REC.StatusBean status, Date datInicial, 
			Date datFinal, Date datProc) throws RobotException {
		try {			
			String resultado = DaoBroker.getInstance().TransacaoSaldoOrgao(orgao.getCodOrgao().toString(), status.getCodStatus(),
					datInicial, datFinal, conn);
			String codRetorno = resultado.substring(0, 3);
			String qtdAutos = resultado.substring(3, 10);
			
			if (codRetorno.equals("000")) {
				GER.SaldoOrgaoBean saldo = new GER.SaldoOrgaoBean();
				saldo.setDatProcessamento(datProc);
				saldo.setOrgao(orgao);    
				saldo.setStatus(status);			
				saldo.setQtdAutos(Integer.valueOf(qtdAutos));
				saldo.setDatInfracaoInicial(datInicial);
				saldo.setDatInfracaoFinal(datFinal);
				saldo.grava();
			}
		} catch (Exception e) {
			throw new RobotException(e.getMessage()); 
		}
	}
	
	private List listarStatus() throws BeanException {
		List listStatus = null;
		try {
			listStatus = new ArrayList();
			for (int i = 0; i < 10; i++) {
				REC.StatusBean status = new REC.StatusBean();
				status.setCodStatus(String.valueOf(i));
				listStatus.add(status);
			}
			return listStatus;
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}

	
	public static void main(String args[])  throws RobotException {		
		try {			
			new CargaSaldoOrgaoRob();			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}