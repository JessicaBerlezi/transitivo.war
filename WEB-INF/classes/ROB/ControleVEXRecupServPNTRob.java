package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import sys.DaoException;
import sys.Util;

public class ControleVEXRecupServPNTRob {
	
	private static final String NOM_ROBOT = "ControleVEXRecupServPNTRob";
	private static final String quebraLinha = "\n";
	
	public ControleVEXRecupServPNTRob() throws RobotException, ROB.DaoException, DaoException {
		
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {            			
			conn = Dao.getInstance().getConnection();
			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();
			
			String datRecupServPNTVex   = Util.formatedToday().substring(0,10);
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			
			Dao.getInstance().ControleVEXRecupServPNT(datRecupServPNTVex);

		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro processar ControleEmiteVEXRob: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new ControleVEXRecupServPNTRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	
}