package ROB;

public class RobotException extends Exception {

    public RobotException() {
        super();
    }

    public RobotException(String msg) {
        super(msg);
    }
}
