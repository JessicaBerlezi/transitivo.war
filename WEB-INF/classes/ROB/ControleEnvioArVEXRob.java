package ROB;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import sys.DaoException;
import sys.ServiceLocatorException;

import REG.RelatArqRetornoBean;


public class ControleEnvioArVEXRob {
	
	private static final String NOM_ROBOT = "ControleEnvioArVEXRob";
	private static final String quebraLinha = "\n";
	
	public ControleEnvioArVEXRob() throws DaoException, ServiceLocatorException, RobotException, ROB.DaoException, DaoException {
		
		
		Connection conn  = null;
		Connection connLog = null;

		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {            			
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();

			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
//				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();
			
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			
			monitor.gravaMonitor("Inicio Gera��o Controle Envio AR VEX : Dt Proc:"+datProc, monitor.MSG_ERRO);			
			
			Dao.getInstance().ControleEnvioArVEX(conn);
			
			conn.commit();
			
			monitor.gravaMonitor("Fim Gera��o Controle Envio AR VEX : Dt Proc:"+datProc, monitor.MSG_ERRO);
				
		} catch (Exception e) {
			try {				
				conn.rollback();
				monitor.gravaMonitor("Erro no Robot ControleEnvioArVEXRob: " + e.getMessage(), monitor.MSG_ERRO);
				System.out.println("Erro :"+e.getMessage());
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new ControleEnvioArVEXRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	
}