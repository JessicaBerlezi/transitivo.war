package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import sys.DaoException;

public class Leprocessos {
    
	private static final String NOM_ROBOT = "Leprocessos";
	private static final String quebraLinha = "\n";
    
	public Leprocessos() throws RobotException, DaoException {
        
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
		try {         
			conn = Dao.getInstance().getConnection();
			REG.Dao dao = REG.Dao.getInstance();
            
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
            
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
System.out.println("Inicio do Rob�");            
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean() ;
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();

			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date());
			String datProcIni   = param.getParamSist("DAT_PROC_REQ_INI");
			String datProcFim   = param.getParamSist("DAT_PROC_REQ_FIM");
			if (datProcFim.length()==0)
			  datProcFim=datProc;			
System.out.println("datProcLog -->"+datProcIni);			
System.out.println("datProc ---> "+datProcFim);
		    if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			UsuarioBeanId.setCodOrgaoAtuacao("999999");
		    Dao.getInstance().VerificaRequerimento(datProcIni,datProcFim,UsuarioBeanId);
			param.setNomParam("DAT_PROC_REQ");			
			ACSS.Dao.getInstance().ParamSistLeBean(param);
			param.setValParametro(datProcFim);
			ACSS.Dao.getInstance().ParamUpdate(param);
			System.out.println("Termino==>"+new Date());
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
    


    
  public static void main(String[] args) throws RobotException {
        
		try {
			new Leprocessos();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
    

}