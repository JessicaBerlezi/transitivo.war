package ROB;

import javax.imageio.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import sys.DaoException;
import sys.Util;
import REC.AutoInfracaoBean;
import REG.ArquivoRecebidoBean;
import REG.LinhaArquivoRec;
import REG.NotifControleBean;

public class GeraCodigoBarraProcessoPassadoPNTRob {
	
	private static final String NOM_ROBOT = "GeraCodigoBarraProcessoRob";
	
	public GeraCodigoBarraProcessoPassadoPNTRob() throws RobotException, DaoException {
		
		Connection conn  = null;
		Connection connLog  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		try {            		
			conn = Dao.getInstance().getConnection();
			
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
			
			connLog = Dao.getInstance().getConnection();
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			AutoInfracaoBean myAutoList = new AutoInfracaoBean();
			
			int iAltura=70;
			boolean bImprimeTexto=true;
			String sPath="",sFormataProc="",sAno="";
			
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();
			REG.Dao dao = REG.Dao.getInstance();
			
			SimpleDateFormat df  = new SimpleDateFormat("dd/MM/yyyy");			
			String sDatProc      = df.format(new Date());
			
			dao.CarregarPassadoCBPNT(myAutoList);
			if (myAutoList.getAutos().size()==0)
				return;
			Iterator it = myAutoList.getAutos().iterator();
			while (it.hasNext()) {
				myAutoList = (AutoInfracaoBean) it.next();
				sFormataProc=myAutoList.getNumProcesso().trim();
				sPath=getArquivo(paramRec,sDatProc,myAutoList.getNumNotificacao());
				try
				{
					if(drawingBarcodeDirectToGraphics(sPath,sFormataProc,iAltura,bImprimeTexto));
				}
				catch (Exception e) {
					monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras dos Processos (Autua��o). "+sFormataProc, monitor.MSG_INICIO);						
				}
			}
			monitor.gravaMonitor("T�rmino da gera��o do C�digo de Barras dos Processos", monitor.MSG_FIM);
		} 
		catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro na gera��o do C�digo de Barras dos Processos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}			
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public boolean drawingBarcodeDirectToGraphics(String sPath,String sTexto,int iAltura,boolean bImprimeTexto)
	throws BarcodeException,ServletException,IOException {
		try
		{
			Barcode barcode = BarcodeFactory.createCode128B(sTexto);
			barcode.setBarHeight((double)iAltura);
			barcode.setDrawingText(bImprimeTexto);
			BufferedImage image = getImage(barcode);
			File dir = new File(sPath);
			dir.mkdirs();    			
			ImageIO.write(image, "jpg", dir);			
		}
		catch (Exception e) 
		{
		}    	
		return true;
	}    
	
	public BufferedImage getImage(Barcode barcode) {
		
		BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bi.createGraphics();
		barcode.draw(g, 0, 0);
		bi.flush();
		return bi;
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new GeraCodigoBarraProcessoPassadoPNTRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	public String getParametro(ACSS.ParamSistemaBean param,String sData) {        
		String parametro = "DIR_CODIGO_BARRA_PROCESSO";
		/*Novo Path*/
		parametro+="_"+sData.substring(6,10);
		return parametro;
	}
	
	public String getArquivo(ACSS.ParamSistemaBean param,String sData,String texto) {
		String pathArquivo;
		try {
			pathArquivo = "C:/IMAGENS/digitalizado/"+getImagem(sData)+
					"/CodigoBarra/"+texto+ ".jpg";				
			
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}    
	
	public String getImagem(String sData) {
		String pathImagem = "";
		pathImagem =sData.substring(6,10)+ 
		sData.substring(3,5) +
		"/" + sData.substring(0,2); 
		return pathImagem;	
	}
	
	
	public String MontaNumProcesso(REC.ParamOrgBean myParam,int iSeqProcAtual) throws DaoException {
		String nProc="";
		try {
			nProc = "000000"+iSeqProcAtual ;
			nProc = myParam.getParamOrgao("COD_SECRETARIA","SEC","2")+"/"+nProc.substring(nProc.length()-6,nProc.length())+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2");
			if(nProc.length()>20) nProc=nProc.substring(0,20);
		}	
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}		
		return nProc;		
	}
	
	
	
}
