package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;
import ACSS.UsuarioBean;
import REC.GuiaDistribuicaoBean;
import REC.ParamOrgBean;

public class GuiaDistrAuditoria1aRob {
    
    private static final String NOM_ROBOT = "GuiaDistrAuditoria1aRob";
    private static final String quebraLinha = "\n";
    sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
      
    public GuiaDistrAuditoria1aRob() throws RobotException, ROB.DaoException, DaoException {
        
        Connection conn  = null;
        Statement trava = null;

        try {            			
            conn = Dao.getInstance().getConnection();
                        
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            // N�o permite que mais de uma instancia do robo rodem ao mesmo tempo 
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            // Carrega Parametros 
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); // M�DULO REGISTRO 
            param.PreparaParam(conn);
            ArrayList ListDestinatarios = ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"QTD_PROCESSO_SALA_1A");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");
			
		   	//Verifico o valor maximo dos n�veis
	        ListDestinatarios = getValProcMax(ListDestinatarios);
	  
			
			ACSS.OrgaoBean[] orgaos = Dao.getInstance().BuscarOrgaosIntegrados("0");
			for (int j = 0; j < orgaos.length; j++) 
			{
				//Verifica quantos processos est�o na sala
				GuiaDistribuicaoBean GuiaDistribuicaoId  = new GuiaDistribuicaoBean() ;
				UsuarioBean UsrLogado = new UsuarioBean () ;
				ParamOrgBean ParamOrgaoId = new ParamOrgBean() ;
				String datInicioRemessa = ParamOrgaoId.getParamOrgao("DAT_INICIO_REMESSA","","10");
				GuiaDistribuicaoId.setJ_sigFuncao("REC0336",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
				UsrLogado.setCodOrgaoAtuacao(orgaos[j].getCodOrgao());
				GuiaDistribuicaoId.PreparaGuia(UsrLogado,datInicioRemessa,"99999");	

				String msg = "Prezado(a) Senhor(a), "+quebraLinha+
				    "Informamos que a quantidade de processos na Guia de Distribuicao 1�Instancia superior a ";
				
				ArrayList qtdProcessoSala = new ArrayList();
                //Prepara Email  
				if (ListDestinatarios.size()>0)
		    		qtdProcessoSala = preparaEmail(ListDestinatarios,GuiaDistribuicaoId,UsrLogado,msg);
				//Enviar Email 
				if (qtdProcessoSala.size()>0)
		    		enviaEmail (qtdProcessoSala,GuiaDistribuicaoId,UsrLogado,emailBroker);
				msg = "";
			}

	
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Monitorar a quantidade de Processados nas guias: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }

    public static void main(String[] args) throws RobotException {
    	
    	try {
    		new GuiaDistrAuditoria1aRob();			 
    	} catch (Exception e) { 
    		System.out.println(e); 
    	} 
    }
     
    public ArrayList preparaEmail(ArrayList ListDestinatarios,GuiaDistribuicaoBean GuiaDistribuicaoId,
    		UsuarioBean UsrLogado, String msg)
    {
		ArrayList qtdProcessoSala = new ArrayList();
		
		//Verifico para qual nivel enviar o e-mail
		Iterator it = ListDestinatarios.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			if(  (GuiaDistribuicaoId.getAutos().size() >= Integer.parseInt(nivelProcesso.getValNivelProcesso())&&
			       GuiaDistribuicaoId.getAutos().size() <= Integer.parseInt(nivelProcesso.getValNivelProcessoMax())) )
			{				
				    Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getCodOrgao().equals(UsrLogado.getCodOrgaoAtuacao()))
						{
							if(nivelProcesso.getTipoMsg().equals("M"))
									destinatario.getMsg().add(msg+nivelProcesso.getValNivelProcesso()+".");
 
								if (destinatario.getMsg().size() > 0)
								  lDestinatarios.add(destinatario);
							}
						}
						nivelProcesso.setListNivelDestino(lDestinatarios);
						qtdProcessoSala.add(nivelProcesso);
						break;
			  }
		 }
		 return qtdProcessoSala;
    }
    
    
    public void enviaEmail (ArrayList  qtdProcessoSala,GuiaDistribuicaoBean GuiaDistribuicaoId,
    		UsuarioBean UsrLogado, String emailBroker ) throws DaoException
    {
		Iterator it = qtdProcessoSala.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			if( (GuiaDistribuicaoId.getAutos().size() >= Integer.parseInt(nivelProcesso.getValNivelProcesso())&&
				 GuiaDistribuicaoId.getAutos().size() <= Integer.parseInt(nivelProcesso.getValNivelProcessoMax())) )
			{				

				Iterator itx = nivelProcesso.getListNivelDestino().iterator();
				GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
				while (itx.hasNext())
				{
					destinatario=(GER.CadastraDestinatarioBean)itx.next();
					if(destinatario.getMsg().size()>0)
					{
						String sConteudo="";
						Iterator ity = destinatario.getMsg().iterator();			
						while (ity.hasNext()) 
						  sConteudo+=(String)ity.next();
						
						sys.EmailBean EmailBeanId = new sys.EmailBean();
						EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
						EmailBeanId.setRemetente( emailBroker );
						EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso());
						EmailBeanId.setConteudoEmail( sConteudo );
						try {
							EmailBeanId.sendEmail();
						} catch (Exception e) {
							 monitor.gravaMonitor("Erro ao enviar e-mail: " + e.getMessage(), monitor.MSG_ERRO);
							e.printStackTrace();
						}
					}
			   }
			}
		}
		
    }
    
    public ArrayList getValProcMax(ArrayList ListDestinatarios)
    {
		ArrayList destPorNivel = new ArrayList();
		int qtdNivel = 0;
		int cont = 0;
		int i = 0;
		
		// Calcular o valor maximo para cada nivel
		Iterator itx = ListDestinatarios.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (itx.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)itx.next();
		    Iterator it = ListDestinatarios.iterator();
		    GER.NivelProcessoSistemaBean nivelProcessoCompara = new GER.NivelProcessoSistemaBean();
		    qtdNivel++;
			while (it.hasNext())
			{
				nivelProcessoCompara = (GER.NivelProcessoSistemaBean)it.next();
				cont++;
				if(qtdNivel == ListDestinatarios.size())
				{
                   nivelProcesso.setValNivelProcessoMax("999999");
                   destPorNivel.add(nivelProcesso);
                   break;
				}
				else if(cont>i+1)
				{
                   nivelProcesso.setValNivelProcessoMax(Integer.toString(Integer.parseInt(nivelProcessoCompara.getValNivelProcesso())-1));
	               destPorNivel.add(nivelProcesso);
	               i++;
	               break;
				}
			}
            cont = 0;  
		}
	
		return destPorNivel;
    }
}