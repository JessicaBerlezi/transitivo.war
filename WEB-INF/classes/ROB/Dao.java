package ROB;


import java.io.File;
import java.io.FileFilter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import sys.BeanException;
import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;
import ACSS.OrgaoBean;
import GER.RequerimentoResultado;
import REC.AutoInfracaoBean;
import REC.DaoBroker;
import REC.DaoBrokerFactory;
import REC.GuiaDaoBroker;
import REC.GuiaDistribuicaoBean;
import REC.InfracaoBean;
import REC.RequerimentoBean;
import REC.ResponsavelBean;
import REG.ArquivoRecebidoBean;
import REG.RelatArqNotificacaoBean;
import REG.RelatArqRetornoBean;
import UTIL.LogDeRecuperacao;

public class Dao {
	
	private static final String MYABREVSIST = "ROB";
	private static Dao instance;
	private sys.ServiceLocator serviceloc ;
	public int posIni;	
	
	private Dao() throws DaoException 
	{
		try 
		{
			serviceloc =  sys.ServiceLocator.getInstance();
			if ((serviceloc.getQualBanco() == null) || serviceloc.getQualBanco().equals(""))
				serviceloc.initParams("../web.xml");
		}
		catch (Exception e) 
		{
			throw new DaoException(e.getMessage());
		}
	}
	
	public static Dao getInstance() throws DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}
	
	public Connection getConnection() throws sys.ServiceLocatorException {
		return serviceloc.getConnection(MYABREVSIST);
	}
	
	public void setReleaseConnection(Connection conn) throws sys.ServiceLocatorException {
		serviceloc.setReleaseConnection(conn);
	}
	
	public boolean pararProcesso(Connection conn, String nomRobot) throws RobotException{
		
		boolean parar = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT IND_CONTINUA FROM TSMI_GERENCIA_ROBO WHERE NOM_ROBO = '" + nomRobot + "'"
			+ " AND COD_TIPO_REGISTRO = 1";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				parar = (rs.getInt("IND_CONTINUA") == 1);
			rs.close();
			stmt.close();
			return parar;
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public Statement travarExecucao(String nomRobot) throws SQLException, RobotException {
		
		Statement stmt = null;
		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_GERENCIA_ROBO WHERE NOM_ROBO = '" + nomRobot + "' AND" +
			" COD_TIPO_REGISTRO = 0 FOR UPDATE  NOWAIT";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (!rs.next()) {
				rs.close();
				throw new RobotException("Erro ao carregar Rob�: N�o existe o registro de ger�ncia !");
			}
			rs.close();
			
			return stmt;
			
		} catch (SQLException e) {
			throw new SQLException("Erro ao carregar Rob�: J� existe outra C�pia processando !");
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public void destravarExecucao(String nomRobot, Statement stmt) throws RobotException {
		try {
			stmt.close();
			setReleaseConnection(stmt.getConnection());
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public boolean verificarExecucao(String nomRobot) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			boolean retorno = false;
			
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_GERENCIA_ROBO WHERE NOM_ROBO = '" + nomRobot + "' AND" +
			" COD_TIPO_REGISTRO = 0 FOR UPDATE";
			ResultSet rs = null;
			try {
				rs = stmt.executeQuery(sCmd);
			} catch (SQLException e) {
				retorno = true;
			}
			rs.close();
			stmt.close();
			
			return retorno;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public Vector VerificaLogTransacao(String sDataProc,String sOrgao,REG.ArquivoRecebidoBean arqRecebido,Vector vResultado) throws SQLException, RobotException {
		
		Connection conn = null;
		Vector vLinha = new Vector();
		try {
			
			String sCodOrgao="";
			String sEntrada="";
			String sSaida="";
			String sCodTransacao="";
			String sDataTransacao="";
			String sDataTransacaoFmt="";
			String sHoraTransacao="";
			String sLinhaEntradaFmt="";
			String sLinhaSaidaFmt="";
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs = null, rs2 = null;
			try
			{
				String sCmd = "SELECT * FROM TSMI_LOG_BROKER WHERE COD_TRANSACAO in ("+arqRecebido.getNumTransBroker()+") and "+
				" DAT_PROC>=to_date('"+sDataProc+"','dd/mm/yyyy') and  "+
				" DAT_PROC<SYSDATE and substr(TXT_RESULTADO,2,3)='000' "+
				" ORDER BY DAT_PROC";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					boolean bGravaLinha=true;					
					sEntrada=rs.getString("TXT_PARTE_VARIAVEL");
					sSaida=rs.getString("TXT_RESULTADO");
					sCodTransacao=rs.getString("COD_TRANSACAO");
					sDataTransacao=rs.getString("DAT_PROC");
					sDataTransacaoFmt=sDataTransacao.substring(0,10).replaceAll("-","");
					sDataTransacaoFmt.replaceAll("/","");
					sHoraTransacao=sDataTransacao.substring(11,16).replaceAll(":","");
					
					if ("078".equals(sCodTransacao))
						sCodOrgao=sEntrada.substring(1359,1365);
					
					if ("204,324,206,326,334,207,208,328,335,209,329,336,210,330,337,214,215,226,350,351,352,238,250,410,411,413,414,415".indexOf(sCodTransacao)>=0)
						sCodOrgao=sEntrada.substring(19,25);
					
					if ("211,331,338,261,381,388".indexOf(sCodTransacao)>=0)
						sCodOrgao=sEntrada.substring(73,79);
					
					if ("213,333,340,263,383,390".indexOf(sCodTransacao)>=0)
						sCodOrgao=sEntrada.substring(53,59);
					
					if ("406,407,412".indexOf(sCodTransacao)>=0)
						sCodOrgao=sEntrada.substring(20,26);
					
					if ("400,408".indexOf(sCodTransacao)>=0)
						sCodOrgao=sEntrada.substring(27,33);
					
					sLinhaEntradaFmt=sEntrada;
					sLinhaSaidaFmt=sSaida;
					if ( (arqRecebido.getNumTransBroker().indexOf(sCodTransacao)) >=0)
					{
						/*Transa��o RECJARI*/
						if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
						{
							String numProcesso    = sEntrada.substring(25,45);  
							String numAuto        = sEntrada.substring(0,12);
							String sRecurso       = "";							
							String transacao      = "";
							String resultado      = "";
							String responsavel    = sEntrada.substring(62,82);
							String nomRequerente  = Util.lPad(""," ",40);
							String datProc        = Util.lPad(""," ",8);
							String numPlaca       = Util.lPad(""," ",7);
							String datRS          = Util.lPad(""," ",8);
							String sDataPub       = Util.lPad(""," ",8);
							String numSessao      = Util.lPad(""," ",7);
							String relator        = Util.lPad("","0",11);
							String motivoResultado= Util.lPad(""," ",1000);
							
							if ("206,208,209,210,226".indexOf(sCodTransacao)>=0)
								sRecurso="DP";
							if ("326,328,329,330,350".indexOf(sCodTransacao)>=0)
								sRecurso="1P";
							if ("334,335,336,337,351".indexOf(sCodTransacao)>=0)
								sRecurso="2P";
							
							
							if ("206,326,334".indexOf(sCodTransacao)>=0)//Abrir Recurso
							{
								transacao    = "01";
								nomRequerente= sEntrada.substring(128,168);
								datProc      = sEntrada.substring(120,128);
								numPlaca     = sEntrada.substring(12,19);
							}
							
							if ("208,328,335".indexOf(sCodTransacao)>=0)//Informar Relator
							{
								sCmd="SELECT T1.NUM_SESSAO,T2.DAT_PROCESSO FROM "+
								" TSMI_LOTE_RELATOR T1,TSMI_ITEM_LOTE_RELATOR T2 "+
								" WHERE T1.COD_LOTE_RELATOR=T2.COD_LOTE_RELATOR AND "+
								" T2.NUM_AUTO_INFRACAO='"+numAuto.trim()+"'";
								rs2 = stmt2.executeQuery(sCmd);
								if (rs2.next())
								{
									numSessao = rs2.getString("NUM_SESSAO");
									if (numSessao==null) numSessao="";
									numSessao = Util.lPad(numSessao,"0",7);
									datProc   = Util.converteData(rs2.getDate("DAT_PROCESSO"));
									if (datProc==null) datProc="";
									datProc  = Util.formataDataYYYYMMDD(datProc);
								}
								transacao    = "08";
								numPlaca     = sEntrada.substring(12,19);
								relator=Util.lPad(sEntrada.substring(132),"0",11);
							}
							
							else if ("209,329,336".indexOf(sCodTransacao)>=0)//Informar Resultado
							{
								resultado = sEntrada.substring(126,127);
								if(resultado.equals("D")) transacao = "02";
								else transacao = "03";
								
								datRS = sEntrada.substring(118,126);
								motivoResultado = Util.rPad(sEntrada.substring(127)," ",1000);
								
							}
							else if ("226,350,351".indexOf(sCodTransacao)>=0)//Estorno do Recurso
								transacao = "05";
							else if ("210,330,337".indexOf(sCodTransacao)>=0)//Estorno de Informar Resultado
							{
								resultado = sEntrada.substring(126,127);
								if(resultado.equals("D")) transacao = "06";
								else transacao = "07";
							}
							
							sLinhaEntradaFmt="02"+ numProcesso+ 
							numAuto+ 
							transacao+
							nomRequerente+
							datProc +
							numPlaca +
							datRS + 
							responsavel +
							Util.lPad(""," ",70)+
							"00"+relator+motivoResultado+
							numSessao+
							sDataPub+sRecurso; 
						}
						
						/*Transa��o MF01*/
						if (arqRecebido.getCodIdentArquivo().equals("MF01"))
						{
							String numAutoInfracao  = sEntrada.substring(33,45);  
							String numPlaca         = sEntrada.substring(45,52);  
							String numProcesso      = sEntrada.substring(59,79);
							String responsavel      = sEntrada.substring(79,87);
							String operacaoBanerj   = sEntrada.substring(87,88);
							sLinhaEntradaFmt="1"+ numAutoInfracao+ 
							numPlaca+ 
							Util.lPad(""," ",7)+
							Util.lPad(""," ",45)+
							numProcesso+
							responsavel+
							operacaoBanerj+
							Util.lPad(""," ",16)+
							Util.lPad(""," ",406); 
						}						
						/*Transa��o MZ01*/
						if (arqRecebido.getCodIdentArquivo().equals("MZ01"))
						{
							String numAutoInfracao  = sEntrada.substring(0,12);  
							String numPlaca         = sEntrada.substring(12,19);  
							String numProcesso      = sEntrada.substring(25,45);
							String responsavel      = sEntrada.substring(62,82);
							String nomCondutor      = sEntrada.substring(107,147);
							String cpfCondutor      = sEntrada.substring(96,107);
							String cnhCondutor      = sEntrada.substring(225,236);
							String ufCnhCondutor    = sEntrada.substring(236,238);
							String endCondutor      = sEntrada.substring(147,190);
							String cepCondutor      = sEntrada.substring(216,224);
							String municipioCondutor= sEntrada.substring(210,214);
							sLinhaEntradaFmt="1"+ numAutoInfracao+
							numPlaca+
							numProcesso+
							responsavel+
							Util.lPad(""," ",187)+
							nomCondutor+
							cpfCondutor+
							cnhCondutor+
							ufCnhCondutor+
							endCondutor+
							cepCondutor+
							municipioCondutor+
							Util.lPad(""," ",60);
						}
						/*Transa��o MX01*/
						if (arqRecebido.getCodIdentArquivo().equals("MX01"))
						{
							String numAutoInfracao  = sEntrada.substring(0,12);  
							String numPlaca         = sEntrada.substring(12,19);  
							String numProcesso      = sEntrada.substring(25,45);
							String responsavel      = sEntrada.substring(62,70);
							String resultado        = sEntrada.substring(126,127);
							if (resultado.equals("D")==false)
								bGravaLinha=false;
							
							sLinhaEntradaFmt="1"+numAutoInfracao+
							numPlaca+
							numProcesso+
							responsavel+
							Util.lPad(""," ",388);
						}
						/*Transa��es MR01*/
						if (arqRecebido.getCodIdentArquivo().equals("MR01"))
						{
							sLinhaEntradaFmt="1"+sEntrada.substring(33);
							sSaida=sSaida.substring(4);
							sEntrada=sEntrada.substring(32);
							//Montar linha para gerar arquivo MD01
							REG.LinhaArquivoRec linhaRec = new REG.LinhaArquivoRec();
							String numAutoInfracao  =   sEntrada.substring(1,13);
							String numPlaca         =   sEntrada.substring(13,20);
							String codRenavam       =   sSaida.substring(24,33);
							String dscMarca         =   sSaida.substring(129,154);
							String codMarca         =   sys.Util.lPad(linhaRec.buscarCodMarcaModelo(conn,dscMarca),"0",7);
							String local            =   sEntrada.substring(27,72);            
							String datInfracao      =   sEntrada.substring(76,80) + sEntrada.substring(74,76) + sEntrada.substring(72,74);
							String horInfracao      =   sEntrada.substring(80,84);
							String codMunicipio     =   sEntrada.substring(84,88);
							String codInfracao      =   sys.Util.lPad(sEntrada.substring(88,92),"0",5);
							String valMulta         =   sys.Util.lPad(sEntrada.substring(93,99),"0",9);
							String datVencimento    =   sSaida.substring(233,237) + sSaida.substring(231,233)
							+ sSaida.substring(229,231);
							String codCancelamento  =   sSaida.substring(0,3);
							String matAgente        =   sEntrada.substring(109,119);
							String velPermitida     =   sEntrada.substring(119,122);
							String velAferida       =   sEntrada.substring(122,126);
							String orgAutu          =   sys.Util.lPad(arqRecebido.getCodOrgao(),"0",6);
							String codAgente        =   sEntrada.substring(107,109);
							String nomPropietario   =   sys.Util.rPad(sSaida.substring(189,229), " ", 45);
							
							String numCPF           =   sys.Util.lPad("","0",11);
							String numCGC           =   sys.Util.lPad("","0",14);
							String CPFouCGC         =   sSaida.substring(188,189);            
							if (CPFouCGC.equals("1"))
								numCPF                =   sSaida.substring(177,188);
							else if (CPFouCGC.equals("2"))
								numCGC                =   sSaida.substring(174,188);
							
							String logEndereco      =   sSaida.substring(33,58);
							String numEndereco      =   sSaida.substring(73,78);
							String compEndereco     =   sSaida.substring(103,114);
							
							String dscMunEndereco   =   sSaida.substring(78,101);
							String ufEndereco		  =   sSaida.substring(101,103).trim();				
							
							OrgaoBean orgaoBean = new OrgaoBean();
							orgaoBean.Le_Orgao(arqRecebido.getCodOrgao(),0);
							if (ufEndereco.equals("")) 	ufEndereco = orgaoBean.getEndereco().getCodUF();
							String codMunEndereco   =   sys.Util.lPad(linhaRec.buscarCodMunicipio(conn,dscMunEndereco,ufEndereco),"0",4);
							
							String cepEndereco      =   sSaida.substring(121,129);
							String datCorreio       =   sys.Util.todayYYYYMMDD();
							String datBanco         =   sys.Util.todayYYYYMMDD();
							String statusDoc        =   "1";
							String datInclusao      =   sys.Util.todayYYYYMMDD();
							String vago             =   sys.Util.lPad("", " ", 8);
							String respInfracao     =   "P";
							
							/*Pegar Dados de Marca Modelo*/
							sCmd="SELECT COD_MARCA_MODELO,COD_ESPECIE,COD_CATEGORIA, "+
							"COD_TIPO,COD_COR FROM TSMI_AUTO_INFRACAO WHERE "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao.trim()+"'";
							rs2 = stmt2.executeQuery(sCmd);
							String sCod_Marca     = Util.lPad(rs2.getString("COD_MARCA_MODELO"),"0",7); 
							String sCod_Especie   = Util.lPad(rs2.getString("COD_ESPECIE"),"0",2); 
							String sCod_Categoria = Util.lPad(rs2.getString("COD_CATEGORIA"),"0",2); 
							String sCod_Tipo      = Util.lPad(rs2.getString("COD_TIPO"),"0",3); 
							String sCod_Cor       = Util.lPad(rs2.getString("COD_COR"),"0",3); 
							
							sLinhaSaidaFmt = numAutoInfracao+numPlaca+codRenavam+codMarca+local+datInfracao+horInfracao    
							+codMunicipio+codInfracao+valMulta+datVencimento+codCancelamento+matAgente      
							+velPermitida+velAferida+orgAutu+codAgente+nomPropietario+numCPF+numCGC+logEndereco    
							+numEndereco+compEndereco+codMunEndereco+cepEndereco+datCorreio+datBanco+statusDoc      
							+datInclusao+vago+respInfracao+sCod_Marca+sCod_Especie+sCod_Categoria+sCod_Tipo+sCod_Cor;   
						}
					}
					if ( (sCodOrgao.equals(sOrgao)) && (bGravaLinha) )
					{
						if (arqRecebido.getNumTransBroker().indexOf(sCodTransacao)>=0)
						{
							REG.LinhaArquivoRec linhaRecEntrada = new REG.LinhaArquivoRec();
							linhaRecEntrada.setDscLinhaArqRec(sLinhaEntradaFmt);
							vLinha.add(linhaRecEntrada);
							if (arqRecebido.getCodIdentArquivo().equals("MR01"))
							{
								REG.LinhaArquivoRec linhaRecSaida = new REG.LinhaArquivoRec();
								linhaRecSaida.setDscLinhaArqRec(sLinhaSaidaFmt);
								vResultado.add(linhaRecSaida);
							}
						}
					}
				}
				/*Transa��o RECJARI*/
				if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
				{
					sCmd = "SELECT * FROM TSMI_LOTE_RELATOR T1,TSMI_ITEM_LOTE_RELATOR T2" +
					" WHERE T1.COD_LOTE_RELATOR=T2.COD_LOTE_RELATOR AND "+
					" T1.COD_ORGAO_LOTACAO='"+sOrgao+"' AND T1.COD_STATUS='9' AND "+
					" DAT_PUBDO>=to_date('"+sDataProc+"','dd/mm/yyyy') AND "+
					" DAT_PUBDO<SYSDATE ";
					rs = stmt.executeQuery(sCmd);
					while (rs.next())
					{
						String numProcesso    = Util.rPad(rs.getString("NUM_PROCESSO")," ",20); 
						String numAuto        = Util.rPad(rs.getString("NUM_AUTO_INFRACAO")," ",12);
						String transacao      = "09";
						String responsavel    = Util.rPad(rs.getString("NOM_USERNAME")," ",20); 
						String nomRequerente  = Util.lPad(""," ",40);
						String datProc        = Util.formataDataYYYYMMDD(Util.converteData(rs.getDate("DAT_PROCESSO")));
						String numPlaca       = Util.rPad(rs.getString("NUM_PLACA")," ",7);
						String datRS          = Util.lPad(""," ",8);
						String sDataPub       = Util.formataDataYYYYMMDD(Util.converteData(rs.getDate("DAT_PUBDO")));
						String numSessao      = Util.lPad(""," ",7);
						String relator        = Util.lPad("","0",11);
						String motivoResultado= Util.lPad(""," ",1000);
						sLinhaEntradaFmt="02"+ numProcesso+ 
						numAuto+ 
						transacao+
						nomRequerente+
						datProc +
						numPlaca +
						datRS + 
						responsavel +
						Util.lPad(""," ",70)+
						"00"+relator+
						motivoResultado+
						numSessao+
						sDataPub;
						REG.LinhaArquivoRec linhaRecEntrada = new REG.LinhaArquivoRec();
						linhaRecEntrada.setDscLinhaArqRec(sLinhaEntradaFmt);
						vLinha.add(linhaRecEntrada);
					}
				}			    
				arqRecebido.setLinhaArquivoRec(vLinha);
			} catch (SQLException e) {}
			if (rs!=null)
				rs.close();
			if (rs2!=null)
				rs2.close();
			stmt.close();
			stmt2.close();
			return vResultado;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public Vector VerificaLogTransacaoNIT(String sDataProc,String sOrgao,REG.ArquivoRecebidoBean arqRecebido,Vector vResultado) throws SQLException, RobotException {
		
		Connection conn = null;
		Vector vLinha = new Vector();
		try {
			int iCont=0;
			String sCodOrgao="";
			String sEntrada="";
			String sSaida="";
			String sCodTransacao="";
			String sDataTransacao="";
			String sDataTransacaoFmt="";
			String sHoraTransacao="";
			String sLinhaEntradaFmt="";
			String sLinhaSaidaFmt="";
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs = null, rs2 = null;
			try
			{
				System.out.println("INICIO");
				String sCmd = "SELECT * "+
				"FROM TSMI_REQUERIMENTO T1,TSMI_AUTO_INFRACAO T2 "+
				"WHERE T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO "+
				"AND T1.COD_ORGAO_LOTACAO_DP=258650 "+
				"AND T1.COD_TIPO_SOLIC IN ('DP','DC','1P','1C','2P','2C') "+
				"AND DAT_PROC_DP>=to_date('01/04/2006','dd/mm/yyyy') "+
				"ORDER BY T1.DAT_PROC_DP ";				
				rs = stmt.executeQuery(sCmd);
				System.out.println("Comando==>"+sCmd);
				while (rs.next())
				{
					iCont++;
					System.out.println("DADOS DO REQUERIMENTO ==>"+iCont+" "+rs.getString("DAT_PROC_DP"));
					
					String numProcesso    = rs.getString("NUM_PROCESSO");
					if (numProcesso==null) numProcesso="";
					numProcesso = Util.rPad(numProcesso," ",20);
					
					String numAuto        = Util.rPad(rs.getString("NUM_AUTO_INFRACAO")," ",12);
					String sStatus_Requerimento = rs.getString("COD_STATUS_REQUERIMENTO");
					String sRecurso       = Util.rPad(rs.getString("COD_TIPO_SOLIC")," ",2);							
					String transacao      = "99";
					String resultado      = rs.getString("COD_RESULT_RS");
					if (resultado==null) resultado="";
					resultado =	Util.rPad(resultado," ",1);						
					
					
					if (sStatus_Requerimento.equals("0")) 
						transacao="01";
					if (sStatus_Requerimento.equals("2")) 
						transacao="08";
					if (sStatus_Requerimento.equals("7")) 
						transacao="09";
					
					
					
					
					if(resultado.equals("D")) transacao = "02";
					if(resultado.equals("I")) transacao = "03";
					
					String sR1=rs.getString("NOM_USERNAME_JU");
					if (sR1==null) sR1="";
					String sR2=rs.getString("NOM_USERNAME_RS");
					if (sR2==null) sR2="";						
					String sR=sR1;
					
					if (sR2.trim().length()>0)
						sR=sR2;
					
					String responsavel    = Util.rPad(sR," ",20);
					String nomRequerente  = Util.lPad(""," ",40);
					
					if (sRecurso.substring(1,2).equals("P"))
						nomRequerente=Util.rPad(rs.getString("NOM_PROPRIETARIO")," ",40);
					else
						nomRequerente=Util.rPad(rs.getString("NOM_CONDUTOR")," ",40);							
					
					String datProc        = Util.converteData(rs.getDate("DAT_PROCESSO"));
					if (datProc==null) datProc="";
					datProc  = Util.formataDataYYYYMMDD(datProc);
					String numPlaca       = Util.rPad(rs.getString("NUM_PLACA")," ",7);
					String datRS          = Util.converteData(rs.getDate("DAT_RS"));
					if (datRS==null) datRS="";
					datRS  = Util.formataDataYYYYMMDD(datRS);
					String sDataPub       = Util.lPad(""," ",8);
					String numSessao      = Util.lPad(""," ",7);
					String relator        = rs.getString("COD_RELATOR_JU");
					if (relator==null) relator="";
					relator        = Util.lPad(relator,"0",11);						
					
					
					String motivoResultado=rs.getString("TXT_MOTIVO_RS");
					if (motivoResultado==null) motivoResultado="";
					motivoResultado = Util.rPad(motivoResultado," ",1000);
					
					sCmd="SELECT T1.NUM_SESSAO,T2.DAT_PROCESSO FROM "+
					" TSMI_LOTE_RELATOR T1,TSMI_ITEM_LOTE_RELATOR T2 "+
					" WHERE T1.COD_LOTE_RELATOR=T2.COD_LOTE_RELATOR AND "+
					" T2.NUM_AUTO_INFRACAO='"+numAuto.trim()+"'";
					rs2 = stmt2.executeQuery(sCmd);
					if (rs2.next())
					{
						numSessao = rs2.getString("NUM_SESSAO");
						if (numSessao==null) numSessao="";
						numSessao = Util.lPad(numSessao,"0",7);
						datProc   = Util.converteData(rs2.getDate("DAT_PROCESSO"));
						if (datProc==null) datProc="";
						datProc  = Util.formataDataYYYYMMDD(datProc);
					}
					sLinhaEntradaFmt="02"+ numProcesso+ 
					numAuto+ 
					transacao+
					nomRequerente+
					datProc +
					numPlaca +
					datRS + 
					responsavel +
					Util.lPad(""," ",70)+
					"00"+relator+motivoResultado+
					numSessao+
					sDataPub+sRecurso; 
					
					REG.LinhaArquivoRec linhaRecEntrada = new REG.LinhaArquivoRec();
					linhaRecEntrada.setDscLinhaArqRec(sLinhaEntradaFmt);
					vLinha.add(linhaRecEntrada);
					
					
				}
				
				/*Transa��o RECJARI*/
				if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
				{
					sCmd = "SELECT * FROM TSMI_LOTE_RELATOR T1,TSMI_ITEM_LOTE_RELATOR T2" +
					" WHERE T1.COD_LOTE_RELATOR=T2.COD_LOTE_RELATOR AND "+
					" T1.COD_ORGAO_LOTACAO='"+sOrgao+"' AND T1.COD_STATUS='9' AND "+
					" DAT_PUBDO>=to_date('01/09/2005','dd/mm/yyyy') AND "+
					" DAT_PUBDO<SYSDATE ";
					rs = stmt.executeQuery(sCmd);
					while (rs.next())
					{
						iCont++;
						System.out.println("DATA PUBLICA��O ==>"+iCont);
						String numProcesso    = Util.rPad(rs.getString("NUM_PROCESSO")," ",20); 
						String numAuto        = Util.rPad(rs.getString("NUM_AUTO_INFRACAO")," ",12);
						String transacao      = "09";
						String responsavel    = Util.rPad(rs.getString("NOM_USERNAME")," ",20); 
						String nomRequerente  = Util.lPad(""," ",40);
						String datProc        = Util.formataDataYYYYMMDD(Util.converteData(rs.getDate("DAT_PROCESSO")));
						String numPlaca       = Util.rPad(rs.getString("NUM_PLACA")," ",7);
						String datRS          = Util.lPad(""," ",8);
						String sDataPub       = Util.formataDataYYYYMMDD(Util.converteData(rs.getDate("DAT_PUBDO")));
						String numSessao      = Util.lPad(""," ",7);
						String relator        = Util.lPad("","0",11);
						String motivoResultado= Util.lPad(""," ",1000);
						sLinhaEntradaFmt="02"+ numProcesso+ 
						numAuto+ 
						transacao+
						nomRequerente+
						datProc +
						numPlaca +
						datRS + 
						responsavel +
						Util.lPad(""," ",70)+
						"00"+relator+
						motivoResultado+
						numSessao+
						sDataPub;
						REG.LinhaArquivoRec linhaRecEntrada = new REG.LinhaArquivoRec();
						linhaRecEntrada.setDscLinhaArqRec(sLinhaEntradaFmt);
						vLinha.add(linhaRecEntrada);
					}
				}
				
				arqRecebido.setLinhaArquivoRec(vLinha);
			} catch (SQLException e) {}
			if (rs!=null)  rs.close();
			if (rs2!=null) rs2.close();
			stmt.close();
			stmt2.close();
			return vResultado;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void VerificaPenalidade(String sDataProc,String sOrgao,REG.ArquivoRecebidoBean arqRecebido,Vector vResultado) throws SQLException, RobotException 
	{
		Connection conn = null;
		Vector vLinha = new Vector();
		String tipoNotificacao = "";
		String dtVencimento    = "";
		try {
			String sCodStatus="2";
			String sCodIdentArquivo="EMITEVEX";
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 " +
				" WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO and "+
				" T1.DAT_RECEBIMENTO>=to_date('"+sDataProc+"','dd/mm/yyyy') and  "+
				" T1.DAT_RECEBIMENTO<SYSDATE and " +
				" T1.COD_STATUS='"+sCodStatus+"' and "+
				" T1.COD_IDENT_ARQUIVO='"+sCodIdentArquivo+"' and "+ 
				" T2.COD_ORGAO='"+sOrgao+"' and "+
				" T2.COD_RETORNO is null ";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					tipoNotificacao = rs.getString("DSC_LINHA_ARQ_REC");
					dtVencimento=tipoNotificacao.substring(70,78);
					if (tipoNotificacao.substring(479,480).equals("3"))
					{
						myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
						if ( dao.LeAutoInfracaoLocal(myAuto,"auto") )
						{
							String sLinha="1"+sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
							sys.Util.rPad(myAuto.getNumPlaca()," ",7)+
							sys.Util.lPad(myAuto.getVeiculo().getCodMarcaModelo(),"0",7)+
							sys.Util.rPad(myAuto.getDscLocalInfracao()," ",45)+
							Util.formataDataYYYYMMDD(myAuto.getDatInfracao())+ 
							Util.Tiraedt(sys.Util.rPad(myAuto.getValHorInfracao()," ",4),4)+ 
							sys.Util.lPad(myAuto.getCodMunicipio(),"0",4)+
							sys.Util.lPad(myAuto.getInfracao().getCodInfracao(),"0",4)+
							sys.Util.lPad(Util.SemEdt(myAuto.getInfracao().getValReal(),'.'),"0",7)+							
							dtVencimento+
							sys.Util.lPad(myAuto.getCodAgente(),"0",2)+
							sys.Util.rPad(myAuto.getDscAgente()," ",10)+
							sys.Util.lPad(Util.SemEdt(myAuto.getValVelocPermitida(),'.'),"0",3)+
							sys.Util.lPad(Util.SemEdt(myAuto.getValVelocAferida(),'.'),"0",4)+
							sys.Util.lPad(Util.SemEdt(myAuto.getValVelocConsiderada(),'.'),"0",4)+							
							sys.Util.lPad(myAuto.getCodTipDispRegistrador()," ",1)+
							sys.Util.lPad(myAuto.getNumIdentAparelho()," ",15)+
							sys.Util.lPad(myAuto.getNumInmetroAparelho(),"0",9)+
							Util.formataDataYYYYMMDD(myAuto.getDatUltAferAparelho())+  
							sys.Util.rPad(myAuto.getNumCertAferAparelho()," ",10)+
							sys.Util.lPad(myAuto.getDscLocalAparelho()," ",40)+
							sys.Util.lPad(myAuto.getVeiculo().getCodEspecie(),"0",2)+
							sys.Util.lPad(myAuto.getVeiculo().getCodCategoria(),"0",2)+
							sys.Util.lPad(myAuto.getVeiculo().getCodTipo(),"0",3)+
							sys.Util.lPad(myAuto.getVeiculo().getCodCor(),"0",3)+
							sys.Util.rPad(myAuto.getCondutor().getNomResponsavel()," ",45)+
							sys.Util.lPad(myAuto.getCondutor().getNumCpfCnpj(),"0",11)+
							sys.Util.lPad(myAuto.getCondutor().getNumCnh(),"0",11)+
							sys.Util.lPad(myAuto.getCondutor().getEndereco().getCodUF()," ",2)+
							sys.Util.rPad(myAuto.getCondutor().getEndereco().getTxtEndereco()," ",25)+
							sys.Util.rPad(myAuto.getCondutor().getEndereco().getNumEndereco()," ",5)+
							sys.Util.rPad(myAuto.getCondutor().getEndereco().getTxtComplemento()," ",11)+
							sys.Util.lPad(myAuto.getCondutor().getEndereco().getNumCEP(),"0",8)+
							sys.Util.lPad(myAuto.getCondutor().getEndereco().getCodCidade()," ",4)+
							sys.Util.lPad("", " ", 69);
							REG.LinhaArquivoRec linhaRecEntrada = new REG.LinhaArquivoRec();
							linhaRecEntrada.setDscLinhaArqRec(sLinha);
							vLinha.add(linhaRecEntrada);
						}
					}
				}
				arqRecebido.setLinhaArquivoRec(vLinha);
			} catch (SQLException e) {}
			rs.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	/**
	 * Testar o identificador de arquivos do objeto RelatArqNotificacaoBean
	 * 
	 */
	public boolean ControleEmiteVEX(RelatArqNotificacaoBean ControleEmiteVEXID) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);     
			conn.setAutoCommit(false);
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs = null;
			ResultSet rs_VEX   = null;
			
			String codArquivo      = "";
			String datEnvio        = "";
			String datGeracao      = "";
			String datLiberacao    = "";
			String datRetorno      = "";
			String codigoRetorno   = "";
			String numAutoInfracao = "";
			String numPlaca        = "";
			String datInfracao     = "";
			String numNotificacao  = "";
			String seqNotificacao  = "";
			String tipNotificacao  = "";
			String codOrgao        = "";
			
			String codArquivoAux      = "";
			String datEnvioAux        = "";
			String datGeracaoAux      = "";
			String datLiberacaoAux    = "";
			String numAutoInfracaoAux = "";
			String numPlacaAux        = "";
			String datInfracaoAux     = "";
			String numNotificacaoAux  = "";
			String seqNotificacaoAux  = "";
			String tipNotificacaoAux  = "";
			String codOrgaoAux        = "";
			
			
			String mesDatEnvio  = "";
			String anoDatEnvio  = "";
			
			String mesDatRetorno  = "";
			String anoDatRetorno  = "";
			String codigoRetornoEnvio = "000";
			String indEntrega      = "";
			String indFaturado     = "";
			
			String numCaixa           = "";
			String numLote            = "";
			
			String codControleVEX_MASTER = "";
			String codControleVEX_DETAIL = "";
			String tipReemissao = "";
			
			boolean inclui = true;
			boolean incluiRemi = false;
			boolean altera = false;
			
			String numArq       = "";
			long inumArq        = 0;
			
			
			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				
				sCmd="SELECT * FROM TSMI_CONTROLE_VEX_MASTER_V1 WHERE COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"'";
				rs = stmt.executeQuery(sCmd);
				if (rs.next()) return false;
				
				
				sCmd = "SELECT "+
				"T1.COD_ARQUIVO AS COD_ARQUIVO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T1.DAT_RECEBIMENTO AS DAT_GERACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,361,12) AS NUM_AUTO_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,126,7) AS NUM_PLACA, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,213,8) AS DAT_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,7,9) AS NUM_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,572,1) AS TIP_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,516,2) AS COD_UF, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,458,8) AS NUM_CEP, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,226,50) AS NOM_MUNICIPIO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,568,1) AS TIP_REEMISSAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,580,6) AS COD_ORGAO "+
				"FROM "+
				"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
				"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "+
				"T1.COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"' AND "+
				"T2.COD_RETORNO IS NULL";				
				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					
					codArquivo      = "";
					datEnvio        = "";
					datGeracao      = "";
					datLiberacao    = "";
					datRetorno      = "";
					codigoRetorno   = "";
					numAutoInfracao = "";
					numPlaca        = "";
					datInfracao     = "";
					numNotificacao  = "";
					seqNotificacao  = "";
					tipNotificacao  = "";
					codOrgao        = "";
					
					
					codArquivoAux      = "";
					datEnvioAux        = "";
					datGeracaoAux      = "";
					datLiberacaoAux    = "";
					numAutoInfracaoAux = "";
					numPlacaAux        = "";
					datInfracaoAux     = "";
					numNotificacaoAux  = "";
					seqNotificacaoAux  = "";
					tipNotificacaoAux  = "";
					codOrgaoAux        = "";
					
					
					mesDatEnvio  = "";
					anoDatEnvio  = "";
					
					mesDatRetorno  = "";
					anoDatRetorno  = "";
					codigoRetornoEnvio = "000";
					indEntrega      = "";
					indFaturado     = "";
					numCaixa           = "";
					numLote            = "";
					
					codControleVEX_MASTER = "";
					codControleVEX_DETAIL = "";
					tipReemissao = "";
					
					inclui = true;
					incluiRemi = false;
					altera = false;
					
					numArq       = "";
					inumArq        = 0;
					
					
					
					codArquivo      = rs.getString("COD_ARQUIVO");
					datEnvio        = sys.Util.formatedToday().substring(0,10);
					datGeracao      = Util.converteData(rs.getDate("DAT_GERACAO"));
					mesDatEnvio     = sys.Util.formatedToday().substring(3,5);
					anoDatEnvio     = sys.Util.formatedToday().substring(6,10);
					datRetorno      = "";
					codigoRetorno   = "";
					indEntrega      = "";
					indFaturado     = "";
					codigoRetornoEnvio = "000";
					numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO").trim();
					numPlaca        = rs.getString("NUM_PLACA").trim();
					datInfracao     = sys.Util.formataDataDDMMYYYY(rs.getString("DAT_INFRACAO"));
					numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					seqNotificacao  = "0";
					tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					tipReemissao    = rs.getString("TIP_REEMISSAO");
					codOrgao        = rs.getString("COD_ORGAO");
					/*Avisos-Retornar*/					
					if ( (tipNotificacao.equals("0")) ||
							(tipNotificacao.equals("2"))
					)
					{
						datRetorno        = datEnvio;
						codigoRetorno     = "03 ";
						indEntrega        = "S";
						indFaturado       = "S";
						mesDatRetorno     = mesDatEnvio;
						anoDatRetorno     = anoDatEnvio;	
						
					}else{
						sCmd = "SELECT " +
						"PK_COD_CONTROLE_VEX_MASTER_V1, " +
						"COD_ARQUIVO, " +
						"DAT_ENVIO, " +
						"DAT_LIBERACAO, " +
						"DAT_GERACAO, " +
						"NUM_AUTO_INFRACAO, " +
						"NUM_PLACA, " +
						"DAT_INFRACAO, " +
						"NUM_NOTIFICACAO, " +
						"SEQ_NOTIFICACAO, " +
						"TIP_NOTIFICACAO, " +
						"COD_ORGAO " +
						"FROM TSMI_CONTROLE_VEX_MASTER_V1 "+
						"WHERE NUM_NOTIFICACAO = '"+numNotificacao+"' "+
						"AND SEQ_NOTIFICACAO = '"+seqNotificacao+"'";
						
						rs_VEX=stmt_VEX.executeQuery(sCmd);
						if (rs_VEX.next()){
							
							codControleVEX_MASTER   = rs_VEX.getString("PK_COD_CONTROLE_VEX_MASTER_V1");
							codArquivoAux           = rs_VEX.getString("COD_ARQUIVO");
							datEnvioAux             = Util.converteData(rs_VEX.getDate("DAT_ENVIO"));
							datLiberacaoAux         = Util.converteData(rs_VEX.getDate("DAT_LIBERACAO"));
							datGeracaoAux           = Util.converteData(rs_VEX.getDate("DAT_GERACAO"));
							numAutoInfracaoAux      = rs_VEX.getString("NUM_AUTO_INFRACAO").trim();
							numPlacaAux             = rs_VEX.getString("NUM_PLACA").trim();
							datInfracaoAux          = Util.converteData(rs_VEX.getDate("DAT_INFRACAO"));
							numNotificacaoAux       = rs_VEX.getString("NUM_NOTIFICACAO");
							seqNotificacaoAux       = rs_VEX.getString("SEQ_NOTIFICACAO");
							tipNotificacaoAux       = rs_VEX.getString("TIP_NOTIFICACAO");
							codOrgaoAux             = rs_VEX.getString("COD_ORGAO");
							
							inclui = false;
							//altera = true;
							incluiRemi = true;
						}
						
					}
					
					//rs.close();
					
					if (altera){
						
						altera_TSMI_CONTROLE_VEX_MASTER_V1(conn,
								codControleVEX_MASTER,
								codArquivo,
								datEnvio,
								numAutoInfracao,
								numPlaca,
								datInfracao,
								numNotificacao,
								seqNotificacao,
								tipNotificacao,
								datGeracao,
								codOrgao);
						
						codArquivo = codArquivoAux;
						numNotificacao = numNotificacaoAux;
						seqNotificacao = seqNotificacaoAux;					
						tipNotificacao = tipNotificacaoAux;
						numAutoInfracao = numAutoInfracaoAux;
						datInfracao = datInfracaoAux;					
						numPlaca = numPlacaAux;
						codOrgao = codOrgaoAux;
						datGeracao = datGeracaoAux;					
						datEnvio = datEnvioAux;					
						datLiberacao = datLiberacaoAux;					
						
					}					
					
					
					if (inclui){
						
						/*Avisos-Retornar*/	
						if ( (tipNotificacao.equals("0")) ||(tipNotificacao.equals("2"))){
							//Busca Numero Notifica��o p/ os avisos;
							numNotificacao = buscaNumNotificacaoAviso(conn);
						}
						
						
						/*Inclui na Tabela de Controle Master*/
						codControleVEX_MASTER = insert_TSMI_CONTROLE_VEX_MASTER_V1(conn,codControleVEX_MASTER,codArquivo,datEnvio,numAutoInfracao,numPlaca,datInfracao,numNotificacao,
								seqNotificacao,tipNotificacao,datGeracao,codOrgao);
						
						/*Avisos-Retornar*/					
						if ( (tipNotificacao.equals("0")) ||(tipNotificacao.equals("2"))){
							String statusNew = "18";
							GravaProcRetornadasVexMaster(conn,statusNew,Integer.parseInt(codControleVEX_MASTER));
						}
						
						/*Totalizar Emiss�es*/
						//totalizaEmissoes(conn,
						//          anoDatEnvio,
						//          mesDatEnvio,
						//          codigoRetornoEnvio,
						//          indFaturado);
						
						
						
						/*Totalizar Emiss�es Vers�o 1*/
						totalizaEmissoesV1(conn,datEnvio,codigoRetornoEnvio,indFaturado,"QE",mesDatEnvio,anoDatEnvio, ControleEmiteVEXID.getIdentificadorArquivo());
						
						/*Avisos-Retornar*/					
						if ( (tipNotificacao.equals("0")) ||(tipNotificacao.equals("2"))){
							
							/*Inclui na Tabela de Controle Detalhe*/
							insert_TSMI_AR_DIGITALIZADO(conn,numAutoInfracao,numCaixa,numLote,numNotificacao,datRetorno,codigoRetorno);	
													
							/*Totaliza Retorno Data Emiss�o Vers�o 1*/
							totalizaRetornoDatEmissaoV1(conn,codigoRetorno,datEnvio,datRetorno,indFaturado,indEntrega,mesDatEnvio,anoDatEnvio, ControleEmiteVEXID.getIdentificadorArquivo());
							
							/*Totaliza Retorno Data Retorno Vers�o 1*/
							totalizaRetornoDatRetornoV1(conn,codigoRetorno,datEnvio,datRetorno,indFaturado,indEntrega,"QRAV",mesDatRetorno,anoDatRetorno, ControleEmiteVEXID.getIdentificadorArquivo());	
							
							//vincula as Notifica��o da MASTER com os Ar's retornados p/ data emiss�o
							totalizaRetornoDatEmissaoDetalheV1(conn,"",String.valueOf(codControleVEX_MASTER),codigoRetorno,datEnvio,datRetorno,indFaturado,
									indEntrega,mesDatEnvio,anoDatEnvio);
							
							//vincula as Notifica��o da MASTER com os Ar's retornados p/ data retorno
							totalizaRetornoDatRetornoDetalheV1(conn,"",String.valueOf(codControleVEX_MASTER),codigoRetorno,datRetorno,indFaturado,indEntrega,mesDatRetorno,anoDatRetorno);	
							
						}
					}//Fim inclui
					
					if (incluiRemi){
						
						
						/*Inclui na Tabela de Reemiss�o*/
						insert_TSMI_CONT_VEX_MASTER_REMI(conn,codControleVEX_MASTER,codArquivo,datEnvio,numAutoInfracao,numPlaca,datInfracao,numNotificacao,seqNotificacao,
								tipNotificacao,datGeracao,codOrgao);
						
						/*Totalizar Emiss�es*/
						//totalizaEmissoes(conn,
						//          anoDatEnvio,
						//          mesDatEnvio,
						//          codigoRetornoEnvio,
						//          indFaturado);
						/*Totalizar Emiss�es Vers�o 1*/
						totalizaEmissoesV1(conn,datEnvio,codigoRetornoEnvio,indFaturado,"QR",mesDatEnvio,anoDatEnvio, ControleEmiteVEXID.getIdentificadorArquivo());
					}
				}
			} catch (Exception e) { 
				conn.rollback();
				return false;
			}
			
			conn.commit();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();			
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	public void ControleVEXRecupServPNT(String datRecupServPNTVex) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);     
			conn.setAutoCommit(false);
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			Statement stmt_NOT = conn.createStatement();
			Statement stmt_RECUP = conn.createStatement();			
			
			String sCmd        = null;
			String sCmd_VEX    = null;
			String sCmd_NOT    = null;
			String sCmd_RECUP  = null;
			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;
			ResultSet rs_NOT   = null;			
			ResultSet rs_RECUP = null;			
			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT DISTINCT(COD_ENVIO_DO) AS COD_ENVIO_DO,TIP_ATA " +
				"FROM TPNT_ATA_DO " +
				"WHERE DAT_PUBL_ATA IS NOT NULL AND " +
				"SIT_VEX IS NULL ORDER BY COD_ENVIO_DO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codEnvioDO      = rs.getString("COD_ENVIO_DO");
					String tipAtaDO        = rs.getString("TIP_ATA");
					sCmd_VEX = "SELECT COD_PROCESSO " +
					"FROM TPNT_ATA_DO "+
					"WHERE COD_ENVIO_DO='"+codEnvioDO+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					while (rs_VEX.next())
					{
						String codProcesso = rs_VEX.getString("COD_PROCESSO");
						System.out.println("Processando...Processo:"+codProcesso);
						
						sCmd_NOT = "SELECT NUM_NOTIFICACAO,SEQ_NOTIFICACAO,DAT_RETORNO,IND_ENTREGUE "+
						"FROM TPNT_NOTIFICACAO "+
						"WHERE COD_PROCESSO='"+codProcesso+"' AND "+
						"TIP_NOTIFICACAO='"+tipAtaDO+"' AND " +
						"(DAT_RETORNO IS NULL OR IND_ENTREGUE='N')";
						rs_NOT=stmt_NOT.executeQuery(sCmd_NOT);
						while (rs_NOT.next())
						{
							String numNotificacao        = rs_NOT.getString("NUM_NOTIFICACAO");
							String seqnumNotificacao     = rs_NOT.getString("SEQ_NOTIFICACAO");
							String datRetorno            = Util.converteData(rs_NOT.getDate("DAT_RETORNO"));
							String indEntregue           = rs_NOT.getString("IND_ENTREGUE");
							String codControleVEX_MST    = "";
							String codControleVEX_DETAIL = "";
							String codArquivo            = "";
							String codigoRetorno         = "99";
							String mesDatRecup           = datRecupServPNTVex.substring(3,5);
							String anoDatRecup           = datRecupServPNTVex.substring(6,10);							
							String indEntrega            = "S";
							String indFatura             = "S";
							
							
							
							sCmd_RECUP="SELECT PK_COD_CONTROLE_VEX_MASTER "+
							"FROM TSMI_CONTROLE_VEX_MASTER WHERE "+
							"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
							"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
							rs_RECUP=stmt_RECUP.executeQuery(sCmd_RECUP);
							if (rs_RECUP.next()) codControleVEX_MST=rs_RECUP.getString("PK_COD_CONTROLE_VEX_MASTER");
							
							
							sCmd_RECUP = "SELECT SEQ_TSMI_CONTROLE_VEX_DETAIL.NEXTVAL CODIGO FROM DUAL";
							rs_RECUP=stmt_RECUP.executeQuery(sCmd_RECUP);
							if (rs_RECUP.next()) codControleVEX_DETAIL=rs_RECUP.getString("CODIGO");
							
							sCmd_RECUP="INSERT INTO TSMI_CONTROLE_VEX_DETAIL ( "+
							"PK_COD_CONTROLE_VEX_DETAIL,PK_COD_CONTROLE_VEX_MASTER,COD_ARQUIVO,DAT_RETORNO,"+
							"COD_RETORNO,IND_ENTREGUE_RETORNO,IND_FATURADO_RETORNO) VALUES ("+
							"'"+codControleVEX_DETAIL+"',"+
							"'"+codControleVEX_MST+"',"+   
							"'"+codArquivo+"',"+    
							"TO_DATE('"+datRecupServPNTVex+"','DD/MM/YYYY'),"+
							"'"+codigoRetorno+"', "+            
							"'"+indEntrega+"', "+
							"'"+indFatura+"') ";
							stmt_RECUP.executeQuery(sCmd_RECUP);
							
							
							
							if (datRetorno.length()==0) {
								sCmd_RECUP="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
								"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
								"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
								"PK_COD_CONTROLE_VEX_DETAIL_RET='"+codControleVEX_DETAIL+"', "+
								"DAT_RETORNO=TO_DATE('"+datRecupServPNTVex+"','DD/MM/YYYY'), "+
								"COD_RETORNO='"+codigoRetorno+"', "+            
								"IND_ENTREGUE_RETORNO='"+indEntrega+"', "+
								"IND_FATURADO_RETORNO='"+indFatura+"', "+
								"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
								"DAT_RETORNO_RECUPERACAO=TO_DATE('"+datRecupServPNTVex+"','DD/MM/YYYY'), "+
								"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"', "+            
								"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
								"IND_FATURADO_RECUPERACAO='"+indFatura+"' "+		
								"WHERE " +
								"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
								"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
								stmt_RECUP.executeQuery(sCmd_RECUP);
							}
							else
							{
								sCmd_RECUP="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
								"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
								"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
								"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
								"DAT_RETORNO_RECUPERACAO=TO_DATE('"+datRecupServPNTVex+"','DD/MM/YYYY'), "+
								"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"', "+            
								"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
								"IND_FATURADO_RECUPERACAO='"+indFatura+"' "+		
								"WHERE " +
								"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
								"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
								stmt_RECUP.executeQuery(sCmd_VEX);
							}
							
							
							/*Totalizar Retornos-por data de retorno*/
							sCmd_RECUP="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
							"WHERE "+
							"ANO_ENVIO='"+anoDatRecup+"' AND "+
							"MES_ENVIO='"+mesDatRecup+"' AND "+
							"ANO_RETORNO='"+anoDatRecup+"' AND "+
							"MES_RETORNO='"+mesDatRecup+"' AND "+
							"COD_RETORNO='"+codigoRetorno+"' AND "+
							"IND_ENTREGUE='"+indEntrega+"'";
							rs_RECUP=stmt_RECUP.executeQuery(sCmd_RECUP);		
							if (!rs_RECUP.next())
							{
								sCmd_RECUP="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
								"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
								"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
								"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
								"QTD_DIAS_RETORNO) VALUES ("+
								"'"+anoDatRecup+"',"+
								"'"+mesDatRecup+"',"+
								"'"+anoDatRecup+"',"+
								"'"+mesDatRecup+"',"+
								"'"+codigoRetorno+"',"+
								"'"+indEntrega+"',"+							
								"'"+indFatura+"',0,0,0,0,0,0)";						
								stmt_RECUP.executeQuery(sCmd_RECUP);					
							}
							sCmd_RECUP="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
							"SET "+
							"QTD_RETORNO=QTD_RETORNO+1 "+
							"WHERE "+
							"ANO_ENVIO='"+anoDatRecup+"' AND "+
							"MES_ENVIO='"+mesDatRecup+"' AND "+
							"ANO_RETORNO='"+anoDatRecup+"' AND "+
							"MES_RETORNO='"+mesDatRecup+"' AND "+
							"COD_RETORNO='"+codigoRetorno+"' AND "+
							"IND_ENTREGUE='"+indEntrega+"'";
							stmt_RECUP.executeQuery(sCmd_RECUP);
							
							sCmd_RECUP="UPDATE TPNT_ATA_DO "+
							"SET "+
							"SIT_VEX='S' "+
							"WHERE COD_PROCESSO='"+codProcesso+"' AND "+
							"COD_ENVIO_DO='"+codEnvioDO+"'";
							stmt_RECUP.executeQuery(sCmd_RECUP);							
							
						}
						conn.commit();						
					}
				}
				if (rs!=null)       rs.close();
				if (rs_VEX!=null)   rs_VEX.close();
				if (rs_NOT!=null)   rs_NOT.close();
				if (rs_RECUP!=null) rs_RECUP.close();
				
				stmt_RECUP.close();
				stmt_NOT.close();
				stmt_VEX.close();
				stmt_VEX.close();
				stmt.close();
				return;
				
				
				
			} catch (Exception e) { conn.rollback();}
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	
	public void ControleRetorno_VEX() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM TSMI_CONTROLE_VEX "+
					"WHERE "+ 
					"DAT_ENVIO>='01/01/2005' AND "+ 
					"DAT_ENVIO<='31/12/2005' AND "+
					"COD_RETORNO IS NULL "+
					"ORDER BY DAT_ENVIO ";					
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					
					
					String dat_Retorno_VEX="";
					String ind_Entrega_VEX="";
					String cod_Retorno_VEX="";
					boolean bAtualiza     =false;
					
					/*"NUM_CODIGO_RETORNO IS NOT NULL "+*/
					/*"IND_ENTREGA='S' "+*/
					
					sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,NUM_CODIGO_RETORNO "+ 
					"FROM TSMI_CONTROLE_NOTIFICACOES "+ 
					"WHERE "+
					"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
					"IND_ENTREGA='S' "+
					"GROUP BY DAT_RETORNO,IND_ENTREGA,NUM_CODIGO_RETORNO";					
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next())
					{
						
						
						dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
						if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
						
						ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
						if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
						
						cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
						if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						
					}
					
					if (dat_Retorno_VEX.length()>0) bAtualiza = true;
					
					if (bAtualiza)
					{
						System.out.println("Processando...data:"+datEnvio+" notifica��o:"+numNotificacao);						
						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX SET "+
						"DAT_RETORNO=TO_DATE('"+dat_Retorno_VEX+"','DD/MM/YYYY'),"+
						"MES_DAT_RETORNO='"+dat_Retorno_VEX.substring(3,5)+"', "+        
						"ANO_DAT_RETORNO='"+dat_Retorno_VEX.substring(6,10)+"', "+        
						"COD_RETORNO='"+cod_Retorno_VEX+"', "+            
						"IND_ENTREGUE='"+ind_Entrega_VEX+"' "+           						
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
				}
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	public void ControleRetorno__VEX() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM TSMI_CONTROLE_VEX "+
					"WHERE "+ 
					"DAT_ENVIO>='01/01/2004' AND "+ 
					"DAT_ENVIO<='31/12/2004' AND "+
					"COD_RETORNO IS NULL "+
					"ORDER BY DAT_ENVIO ";					
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					
					System.out.println("Lendo...data retorno:"+datEnvio+" notifica��o:"+numNotificacao);
					
					String dat_Retorno_VEX="";
					String ind_Entrega_VEX="";
					String cod_Retorno_VEX="";
					String sInd_Cancelado ="";
					boolean bAtualiza     =false;
					
					
					sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
					"FROM TAB_RETORNO_2004 "+ 
					"WHERE "+
					"NUMNOTIFICACAO='"+numNotificacao+"' AND "+
					"IND_ENTREGA='S' "+
					"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next())
					{
						dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
						if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
						
						ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
						if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
						
						cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
						if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
					}
					
					if (dat_Retorno_VEX.length()==0) {
						sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
						"FROM TAB_RETORNO_2004 "+ 
						"WHERE "+
						"NUMNOTIFICACAO='"+numNotificacao+"' "+
						"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
						{
							dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
							if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
							
							ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
							if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
							
							cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
							if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						}
					}
					
					
					if (dat_Retorno_VEX.length()==0) {
						
						sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
						"FROM TAB_RETORNO "+ 
						"WHERE "+
						"NUMNOTIFICACAO='"+numNotificacao+"' AND "+
						"IND_ENTREGA='S' "+
						"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
						{
							dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
							if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
							
							ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
							if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
							
							cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
							if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						}
					}
					
					if (dat_Retorno_VEX.length()==0) {
						
						sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
						"FROM TAB_RETORNO "+ 
						"WHERE "+
						"NUMNOTIFICACAO='"+numNotificacao+"' "+
						"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
						{
							dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
							if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
							
							ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
							if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
							
							cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
							if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						}
					}
					
					
					if (dat_Retorno_VEX.length()==0) {
						sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
						"FROM TAB_RETORNO_2007 "+ 
						"WHERE "+
						"NUMNOTIFICACAO='"+numNotificacao+"' AND "+
						"IND_ENTREGA='S' "+
						"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
						{
							dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
							if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
							
							ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
							if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
							
							cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
							if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						}
					}
					
					if (dat_Retorno_VEX.length()==0) {
						sCmd_VEX="SELECT MAX(DAT_RETORNO) AS DAT_RETORNO,IND_ENTREGA,COD_RETORNO AS NUM_CODIGO_RETORNO "+ 
						"FROM TAB_RETORNO_2007 "+ 
						"WHERE "+
						"NUMNOTIFICACAO='"+numNotificacao+"' "+
						"GROUP BY DAT_RETORNO,IND_ENTREGA,COD_RETORNO ";				
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
						{
							dat_Retorno_VEX=Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
							if (dat_Retorno_VEX==null) dat_Retorno_VEX="";
							
							ind_Entrega_VEX=rs_VEX.getString("IND_ENTREGA");
							if (ind_Entrega_VEX==null) ind_Entrega_VEX="";
							
							cod_Retorno_VEX=rs_VEX.getString("NUM_CODIGO_RETORNO");
							if (cod_Retorno_VEX==null) cod_Retorno_VEX="";
						}
						
					}
					if (dat_Retorno_VEX.length()==0) {
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_NOTIFICACOES "+ 
						"WHERE "+
						"NUM_NOTIFICACAO='"+numNotificacao+"' AND COD_STATUS_NOTIF=10";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next())
							sInd_Cancelado="S";
					}
					
					
					if (dat_Retorno_VEX.length()>0) bAtualiza = true;
					
					
					if (bAtualiza)
					{
						System.out.println("Processando...data retorno:"+datEnvio+" notifica��o:"+numNotificacao);						
						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX SET "+
						"DAT_RETORNO=TO_DATE('"+dat_Retorno_VEX+"','DD/MM/YYYY'),"+
						"MES_DAT_RETORNO='"+dat_Retorno_VEX.substring(3,5)+"', "+        
						"ANO_DAT_RETORNO='"+dat_Retorno_VEX.substring(6,10)+"', "+        
						"COD_RETORNO='"+cod_Retorno_VEX +"', "+            
						"IND_ENTREGUE='"+ind_Entrega_VEX+"'  "+
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
					if (sInd_Cancelado.equals("S"))
					{
						System.out.println("Cancelando...data retorno:"+datEnvio+" notifica��o:"+numNotificacao);						
						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX SET "+
						"SIT_CANC='"+sInd_Cancelado+"' "+						
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
					
				}
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	public void ControleRetorno___VEX() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM TSMI_CONTROLE_VEX "+
					"WHERE "+ 
					"(TIP_NOTIFICACAO='0' OR TIP_NOTIFICACAO='2') " +
					"AND SUBSTR(NOM_ARQUIVO,1,8)='EMITEVEX' AND "+
					"DAT_ENVIO>='01/01/2006' AND "+
					"DAT_ENVIO<='31/12/2007' "+
					"ORDER BY DAT_ENVIO"; 
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = rs.getString("COD_ARQUIVO");
					String nomArquivo      = rs.getString("NOM_ARQUIVO");
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String mesDatEnvio     = rs.getString("MES_DAT_ENVIO");
					String anoDatEnvio     = rs.getString("ANO_DAT_ENVIO");
					String datRetorno      = "";
					String mesDatRetorno   = "";
					String anoDatRetorno   = "";
					String codigoRetorno   = "";
					String indEntrega      = "";
					String codControleVEX  = rs.getString("PK_COD_CONTROLE_VEX");					
					String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO");
					String numPlaca        = rs.getString("NUM_PLACA");
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					String seqNotificacao  = "0";
					String tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					String codUF           = rs.getString("COD_UF");
					String numCEP          = rs.getString("NUM_CEP");
					String nomMunicipio    = rs.getString("NOM_MUNICIPIO");
					
					
					/*Avisos-Retornar*/					
					if ( (tipNotificacao.equals("0")) ||
							(tipNotificacao.equals("2"))
					)
					{
						
						System.out.println("Processando...arquivo:"+nomArquivo+
								"   data:"+datEnvio+" auto:"+numAutoInfracao);
						
						datRetorno      = datEnvio;
						mesDatRetorno   = mesDatEnvio;
						anoDatRetorno   = anoDatEnvio;
						codigoRetorno   = "03 ";
						indEntrega      = "S";
						
						String indCSS="";
						sCmd_VEX="SELECT IND_CSS "+
						"FROM TSMI_CODIGO_RETORNO WHERE "+
						"NUM_CODIGO_RETORNO='"+codigoRetorno+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
						if (rs_VEX.next()) indCSS=rs_VEX.getString("IND_CSS");
						if (indCSS==null) indCSS="";
						
						sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_DET ( "+
						"PK_COD_CONTROLE_VEX_DET,PK_COD_CONTROLE_VEX,COD_ARQUIVO_RETORNO,NOM_ARQUIVO_RETORNO,DAT_RETORNO,"+
						"MES_DAT_RETORNO,ANO_DAT_RETORNO,COD_RETORNO,"+
						"IND_ENTREGUE,IND_CSS) VALUES (SEQ_TSMI_CONTROLE_VEX_DET.NEXTVAL,"+
						"'"+codControleVEX+"',"+   
						"'"+codArquivo+"',"+    
						"'"+nomArquivo+"',"+    
						"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
						"'"+mesDatRetorno+"', "+        
						"'"+anoDatRetorno+"', "+        
						"'"+codigoRetorno+"', "+            
						"'"+indEntrega+"', "+
						"'"+indCSS+"') ";
						stmt_VEX.executeQuery(sCmd_VEX);
						
					}
				}
				
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	public void ControleRetorno___CTRVEX() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM TSMI_CONTROLE_VEX WHERE DAT_ENVIO>='01/01/2004' AND " +
					"DAT_ENVIO<='31/12/2006' AND "+
					"DAT_RETORNO IS NOT NULL "+
					"ORDER BY DAT_ENVIO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = rs.getString("COD_ARQUIVO_RETORNO");
					if (codArquivo==null) codArquivo="";
					
					String nomArquivo      = rs.getString("NOM_ARQUIVO_RETORNO");
					if (nomArquivo==null) nomArquivo="";
					
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String mesDatEnvio     = rs.getString("MES_DAT_ENVIO");
					String anoDatEnvio     = rs.getString("ANO_DAT_ENVIO");
					/*Retorno*/
					String datRetorno      = Util.converteData(rs.getDate("DAT_RETORNO"));
					if (datRetorno==null) datRetorno="";
					
					String mesDatRetorno   = rs.getString("MES_DAT_RETORNO");
					if (mesDatRetorno==null) mesDatRetorno="";
					
					String anoDatRetorno   = rs.getString("ANO_DAT_RETORNO");
					if (anoDatRetorno==null) anoDatRetorno="";
					
					String codigoRetorno   = rs.getString("COD_RETORNO");
					if (codigoRetorno==null) codigoRetorno="";
					
					String indEntrega      = rs.getString("IND_ENTREGUE");
					if (indEntrega==null) indEntrega="";
					
					String codControleVEX  = rs.getString("PK_COD_CONTROLE_VEX");
					String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO");
					String numPlaca        = rs.getString("NUM_PLACA");
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					String seqNotificacao  = "0";
					String tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					String codUF           = rs.getString("COD_UF");
					String numCEP          = rs.getString("NUM_CEP");
					String nomMunicipio    = rs.getString("NOM_MUNICIPIO");
					
					if (codigoRetorno.trim().equals("09"))
					{
						codigoRetorno="03 ";
						indEntrega   ="S";
					}
					
					sCmd_VEX="SELECT PK_COD_CONTROLE_VEX "+
					"FROM TSMI_CTR_VEX_DET WHERE "+
					"PK_COD_CONTROLE_VEX='"+codControleVEX+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) continue;
					
					
					
					System.out.println("Processando...arquivo:"+nomArquivo+
							"   data:"+datEnvio+" auto:"+numAutoInfracao+" pk="+codControleVEX);
					
					
					String indCSS="";
					sCmd_VEX="SELECT IND_CSS "+
					"FROM TSMI_CODIGO_RETORNO WHERE "+
					"NUM_CODIGO_RETORNO='"+codigoRetorno+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) indCSS=rs_VEX.getString("IND_CSS");
					if (indCSS==null) indCSS="";
					
					sCmd_VEX="INSERT INTO TSMI_CTR_VEX_DET ( "+
					"PK_COD_CTR_VEX_DET,PK_COD_CONTROLE_VEX,COD_ARQUIVO_RETORNO,NOM_ARQUIVO_RETORNO,DAT_RETORNO,"+
					"MES_DAT_RETORNO,ANO_DAT_RETORNO,COD_RETORNO,"+
					"IND_ENTREGUE,IND_CSS," +
					"CODARQUIVORETORNO,NOMARQUIVORETORNO," +
					"DATRETORNO,MESDATRETORNO,ANODATRETORNO) "+
					"VALUES (SEQ_TSMI_CTR_VEX_DET.NEXTVAL,"+
					"'"+codControleVEX+"',"+   
					"'"+codArquivo+"',"+    
					"'"+nomArquivo+"',"+    
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
					"'"+mesDatRetorno+"', "+        
					"'"+anoDatRetorno+"', "+        
					"'"+codigoRetorno+"', "+            
					"'"+indEntrega+"', "+
					"'"+indCSS+"', "+
					"'"+codArquivo+"',"+    
					"'"+nomArquivo+"',"+    
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
					"'"+mesDatRetorno+"', "+        
					"'"+anoDatRetorno+"')";        
					stmt_VEX.executeQuery(sCmd_VEX);
					
				}
				
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			System.out.println("ERRO:"+e.getMessage());				
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	public void Acerta_Fechamento() throws DaoException 
	{
		try {
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			String datControleVex = "01/01/2007";
			datControleVex = "01/06/2007";
			datControleVex = "21/11/2007";
			datProc        = "21/11/2007";
			
			RelatArqNotificacaoBean ControleEmiteVEXID = new RelatArqNotificacaoBean();
			REG.Dao.getInstance().ConsultaRelDiario(ControleEmiteVEXID,datControleVex,datProc,"EMITEVEX");
			for (int i=0;i<ControleEmiteVEXID.getListaArqs().size();i++) {
				System.out.println("Processando..."+ControleEmiteVEXID.getListaArqs(i).getNomArquivo());
				if ( (ControleEmiteVEXID.getListaArqs(i).getSitControleVEX().length()==0) ||
						(!ControleEmiteVEXID.getListaArqs(i).getCodStatus().equals("P"))
				)
				{
					System.out.println("Bypassando..."+ControleEmiteVEXID.getListaArqs(i).getNomArquivo());
					continue;
				}
				if (ControleEmiteVEXID.getListaArqs(i).getNomArquivo().indexOf("EMITEPONTO")<0)
					Dao.getInstance().ControleRetornoVEX_MDB(ControleEmiteVEXID.getListaArqs(i));
			}
		}
		catch (Exception e) {System.out.println("Erro:"+e.getMessage());}
	}
	
	
	public void Grava_Diretorio1() throws SQLException, RobotException 
	{ 
		Properties props = new Properties();
		props.setProperty("workDir","D://");
		File workDir = new File(props.getProperty("workDir"));
		
		FiltroArqTIF filtroArq = new FiltroArqTIF();
		FiltroDir filtroDir = new FiltroDir();
		Connection conn = null;
		try {
			
			File[] caixas = workDir.listFiles();
			for (int i = 0; i < caixas.length; i++) {
				
				File[] lotes = caixas[i].listFiles();
				for (int j = 0; j < lotes.length; j++) {
					
					File[] arquivos = lotes[j].listFiles(); /*filtroArq*/
					for (int l = 0; l < arquivos.length; l++) {
						
						/*
						 String nomArquivo = formatNomArquivo(arquivos[l].getName());
						 */
						String numLote = lotes[j].getName().toUpperCase();
						String numCaixa = caixas[i].getName().toUpperCase();
						System.out.println("Lote: "+numLote);
						
						/*
						 DataHandler arquivo = new DataHandler(new FileDataSource(arquivos[l]));
						 */
					}
				}
			}
			
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void Grava_Diretorio() throws SQLException, RobotException 
	{ 
		
		try {   
			FileList fl = new FileList();   
			fl.listFiles( new File("D://Temp"), true );   
		} catch( Exception e ) {}   
	}   
	
	
	
	public void listFiles( File root, boolean recursive ) {   
		if( root==null ) return;   
		if( !root.exists() ) {   
			System.out.println("File does not exist!");   
		}   
		
		if( root.isFile() ) {   
			System.out.println( root.getAbsolutePath() );   
			return;   
		}   
		System.out.println( root.getAbsolutePath() );   
		
		File[] files = root.listFiles();   
		if( files!=null ) {   
			for(int i=0; i<files.length; i++) {   
				if( files[i].isFile() ) {   
					System.out.println( files[i].getAbsolutePath() );   
				}   
				
				if( recursive && files[i].isDirectory() ) {   
					listFiles( files[i], recursive );   
				}   
			}   
		}   
	}   
	
	public void listFiles( File root ) {   
		listFiles( root, false );   
	}   
	
	
	
	
	
	
	
	
	private class FiltroDir implements FileFilter {
		public boolean accept(File filename) {
			if (filename.isDirectory())
				return true;
			else
				return false;
		}
	}
	private class FiltroArqTIF implements FileFilter {
		public boolean accept(File filename) {
			if (filename.isFile() && (filename.getName().toLowerCase().indexOf(".tif") >= 0))
				return true;
			else
				return false;
		}
	}
	private class FiltroArqJPG implements FileFilter {
		public boolean accept(File filename) {
			if (filename.isFile() && (filename.getName().toLowerCase().indexOf(".jpg") >= 0))
				return true;
			else
				return false;
		}
	}
	
	
	
	public void Grava_Consolidado() throws SQLException, RobotException 
	{ 
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);     
			conn.setAutoCommit(false);
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs = null;
			ResultSet rs_VEX   = null;
			String dtInicio    = "01/01/2007";
			String dtFim       = "31/12/2007";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT DISTINCT(NUM_AUTO_INFRACAO) AS NUM_AUTO_INFRACAO,DAT_INFRACAO " +
				"FROM " +
				"TSMI_CONTROLE_VEX_MASTER WHERE " +
				"DAT_INFRACAO>=TO_DATE('"+dtInicio+"','DD/MM/YYYY') AND " +
				"DAT_INFRACAO<=TO_DATE('"+dtFim+"','DD/MM/YYYY') " +
				"ORDER BY DAT_INFRACAO";
				
				/*"DAT_INFRACAO<=TO_DATE('"+dtFim+"','DD/MM/YYYY') AND IND_PROCESSADO IS NULL " +*/
				rs = stmt.executeQuery(sCmd);				
				while (rs.next())
				{
					int iQtdInfracao=1;
					String numAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");
					String datInfracao    = Util.converteData(rs.getDate("DAT_INFRACAO"));					
					
					sCmd_VEX="SELECT * FROM " +
					"TSMI_CONTROLE_VEX_MASTER WHERE "+
					"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' "+
					"ORDER BY NUM_NOTIFICACAO";
					rs_VEX = stmt_VEX.executeQuery(sCmd_VEX);
					
					int iQtdNotificacao=0,
					iQtdAutuacao=0,iQtdAvisoAutuacao=0,
					iQtdPenalidade=0,iQtdAvisoPenalidade=0,iQtdPontuacao=0,
					iQtdAntigo=0,iTempoAutuacao=0,iQtdAutosAutuacao=0,
					iTempoPenalidade=0,iQtdAutosPenalidade=0;
					boolean bAutuacao=true,bPenalidade=true;
					while (rs_VEX.next())
					{
						System.out.println("Auto de Infracao..."+numAutoInfracao+" Data Infra��o..."+datInfracao);
						String datEnvio        = Util.converteData(rs_VEX.getDate("DAT_ENVIO"));
						String tipNotificacao  = rs_VEX.getString("TIP_NOTIFICACAO");
						iQtdNotificacao++;
						if (tipNotificacao.equals("0")) iQtdAvisoAutuacao++;
						if (tipNotificacao.equals("1")) iQtdAutuacao++;
						if (tipNotificacao.equals("2")) iQtdAvisoPenalidade++;
						if (tipNotificacao.equals("3"))	iQtdPenalidade++;						
						if (tipNotificacao.equals("9")) iQtdAntigo++;
						
						if ( (bAutuacao) && (tipNotificacao.equals("1"))) {
							iTempoAutuacao=Util.DifereDias(datInfracao,datEnvio);
							iQtdAutosAutuacao=1;
							bAutuacao=false;
						}
						
						if ( (bPenalidade) && (tipNotificacao.equals("3"))) {
							iTempoPenalidade=Util.DifereDias(datInfracao,datEnvio);
							iQtdAutosPenalidade=1;							
							bPenalidade=false;
						}
					}
					/*
					 sCmd_VEX="UPDATE TAB_AUTOS "+
					 "SET "+
					 "QTD_INFRACAO           = QTD_INFRACAO           +  "+iQtdInfracao+", "+
					 "TOTAL_NOTIFICACAO      = TOTAL_NOTIFICACAO      +  "+iQtdNotificacao+", "+ 
					 "QTD_AUTUACAO           = QTD_AUTUACAO           +  "+iQtdAutuacao+", "+   
					 "QTD_AVISO_AUTUACAO     = QTD_AVISO_AUTUACAO     +  "+iQtdAvisoAutuacao+", "+       
					 "QTD_PENALIDADE         = QTD_PENALIDADE         +  "+iQtdPenalidade+", "+    
					 "QTD_AVISO_PENALIDADE   = QTD_AVISO_PENALIDADE   +  "+iQtdAvisoPenalidade+", "+         
					 "QTD_ANTIGO             = QTD_ANTIGO             +  "+iQtdAntigo+", "+         
					 "QTD_PONTUACAO          = QTD_PONTUACAO          +  "+iQtdPontuacao+", "+ 
					 "TEMPO_MEDIO_AUTUACAO   = TEMPO_MEDIO_AUTUACAO   +  "+iTempoAutuacao+", "+        
					 "TEMPO_MEDIO_PENALIDADE = TEMPO_MEDIO_PENALIDADE +  "+iTempoPenalidade+", "+        
					 "QTD_AUTOS_AUTUACAO     = QTD_AUTOS_AUTUACAO     +  "+iQtdAutosAutuacao+", "+   
					 "QTD_AUTOS_PENALIDADE   = QTD_AUTOS_PENALIDADE   +  "+iQtdAutosPenalidade;    
					 stmt_VEX.executeQuery(sCmd_VEX);
					 
					 sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER "+
					 "SET "+
					 "IND_PROCESSADO='S' "+
					 "WHERE "+
					 "NUM_AUTO_INFRACAO='"+numAutoInfracao+"'";
					 stmt_VEX.executeQuery(sCmd_VEX);
					 conn.commit();
					 */		
					if (bAutuacao) {
						System.out.println("Auto de Infra��o S/ Autua��o..."+numAutoInfracao);						
						sCmd_VEX="INSERT INTO TAB_AUTOS_SATU (NUM_AUTO_INFRACAO) VALUES "+
						"('"+numAutoInfracao+"')";
						stmt_VEX.executeQuery(sCmd_VEX);
						conn.commit();
					}
					
					
				}
			} catch (Exception e) { conn.rollback();}
			
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	
	public void Acerta_DiasFechamento(String sDtRef,String sDtInicial,String sDtFinal) throws DaoException {
		Connection conn = null;
		String sCmd      = null;
		String sCmd_VEX  = null;			
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt      = conn.createStatement();
			Statement stmt_VEX  = conn.createStatement();			
			ResultSet rs     = null;
			ResultSet rs_VEX = null;			
			
			String sMes=sDtInicial.substring(3,5);			
			String sAno=sDtInicial.substring(6,10);
			
			sCmd="SELECT DISTINCT VEX.NUM_NOTIFICACAO,VEX.SEQ_NOTIFICACAO FROM "+ 
			"TSMI_CONTROLE_VEX_DET DET,TSMI_CONTROLE_VEX VEX "+
			"WHERE DET.PK_COD_CONTROLE_VEX=VEX.PK_COD_CONTROLE_VEX AND "+
			"VEX.DAT_ENVIO>=TO_DATE('"+sDtRef+"','DD/MM/YYYY') AND "+
			"DET.DAT_RETORNO>=TO_DATE('"+sDtInicial+"','DD/MM/YYYY') AND "+
			"DET.DAT_RETORNO< TO_DATE('"+sDtFinal+"','DD/MM/YYYY') "+		    
			"ORDER BY VEX.NUM_NOTIFICACAO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				String numNotificacao = rs.getString("NUM_NOTIFICACAO");
				if (numNotificacao.trim().length()==0) continue;
				
				String seqNotificacao = rs.getString("SEQ_NOTIFICACAO");
				if (seqNotificacao==null) seqNotificacao="0";
				
				
				
				sCmd_VEX="SELECT VEX.PK_COD_CONTROLE_VEX,VEX.INDFATURADO,VEX.DAT_ENVIO,DET.PK_COD_CONTROLE_VEX_DET,DET.DAT_RETORNO,DET.COD_RETORNO,DET.IND_ENTREGUE,DET.IND_FATURA FROM "+ 
				"TSMI_CONTROLE_VEX_DET DET,TSMI_CONTROLE_VEX VEX "+
				"WHERE DET.PK_COD_CONTROLE_VEX=VEX.PK_COD_CONTROLE_VEX AND "+
				"VEX.NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
				"VEX.SEQ_NOTIFICACAO='"+seqNotificacao+"' AND "+
				"VEX.DAT_ENVIO>=TO_DATE('"+sDtRef+"','DD/MM/YYYY') AND "+
				"DET.DAT_RETORNO>=TO_DATE('"+sDtInicial+"','DD/MM/YYYY') AND "+
				"DET.DAT_RETORNO< TO_DATE('"+sDtFinal+"','DD/MM/YYYY') "+		    
				"ORDER BY VEX.NUM_NOTIFICACAO,DET.IND_FATURA DESC,DET.COD_RETORNO,DET.DAT_RETORNO";
				rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
				if (rs_VEX.next()) 
				{
					
					String codControleVex   = rs_VEX.getString("PK_COD_CONTROLE_VEX");
					String codControleVexDet= rs_VEX.getString("PK_COD_CONTROLE_VEX_DET");					
					String indFaturado      = rs_VEX.getString("INDFATURADO");
					if (indFaturado==null) indFaturado="";
					if (indFaturado.equals("S")) continue;
					
					String datEnvio         = Util.converteData(rs_VEX.getDate("DAT_ENVIO"));
					String datRetorno       = Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
					
					String numCodigoRetorno = rs_VEX.getString("COD_RETORNO");
					String indEntrega       = rs_VEX.getString("IND_ENTREGUE");
					if (numCodigoRetorno.trim().equals("09"))
					{
						numCodigoRetorno="03 ";
						indEntrega      ="S";
					}
					String indFatura        = rs_VEX.getString("IND_FATURA");
					
					System.out.println("Fechamento Mensal...data Retorno..."+datRetorno+" notificacao..."+numNotificacao);
					
					/*N�o Faturado*/
					if (indFaturado.equals("N"))
					{
						if (indFatura.equals("N")) continue;
						else
						{
							numCodigoRetorno="998"; /*Recuperado Entrega*/
							indEntrega      ="S";
						}
					}
					
					
					int iDias=sys.Util.DifereDias(datEnvio,datRetorno);
					if (iDias<0) iDias=0;
					
					
					
					
					
					sCmd_VEX="UPDATE TSMI_VEX_RETORNO "+
					"SET "+
					"NUM_DIAS=NUM_DIAS+1 "+
					"WHERE "+
					"ANO_RETORNO='"+sAno+"' AND "+
					"MES_RETORNO='"+sMes+"' AND "+
					"COD_RETORNO='"+numCodigoRetorno+"' AND "+
					"IND_ENTREGUE='"+indEntrega+"'";
					stmt_VEX.executeQuery(sCmd_VEX);					
					
					
					sCmd_VEX="UPDATE TSMI_CONTROLE_VEX "+
					"SET "+
					"INDFATURADO='"+indFatura+"',"+
					"DATRETORNO=TO_DATE('"+datRetorno+"','DD/MM/YYYY') "+
					"WHERE "+
					"PK_COD_CONTROLE_VEX='"+codControleVex+"'";
					stmt_VEX.executeQuery(sCmd_VEX);					
					
					
					sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_DET "+
					"SET "+
					"IND_FECHAMENTO='S' "+
					"WHERE "+
					"PK_COD_CONTROLE_VEX_DET='"+codControleVexDet+"'";
					stmt_VEX.executeQuery(sCmd_VEX);					
					
					
					
				}
			}
			if (rs!=null) rs.close();
			if (rs_VEX!=null) rs_VEX.close();			
			stmt_VEX.close();
			stmt.close();
		}
		catch(Exception e){
			System.out.println("Comando sCmd : "+sCmd);
			System.out.println("Comando sCmd_VEX : "+sCmd_VEX);			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	
	public  boolean GuiaPrepara () throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			
			GuiaDistribuicaoBean myGuia = new GuiaDistribuicaoBean();
			myGuia.setCFuncao("REC0251");
			myGuia.setStatusValido("5");
			myGuia.setStatusTPCVValido("0,1,2,3,4") ;
			myGuia.setNFuncao("PREPARA GUIA DE DISTRIBUI��O - DEFESA PR�VIA (WEB)") ;
			myGuia.setTipoReqValidos("DP") ;
			myGuia.setStatusReqValido("01") ;
			myGuia.setOrigemEvento("100");
			myGuia.setCodEvento("208");
			
			
			
			
			
			ACSS.UsuarioBean myUsrLog   = new ACSS.UsuarioBean();
			myUsrLog.setCodOrgaoAtuacao("119100");
			
			String datInicioRemessa     = "";
			String numMaxProc           = "20";  
			
			
			
			DaoBroker dao = DaoBrokerFactory.getInstance();			
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String parteVar    = "";
			String tpJunta     = "1";	
			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
			else {
				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
				else tpJunta="3" ;				
			}
			if (myGuia.getCFuncao().equals("REC0251")) tpJunta="4";
			
			String indContinua = sys.Util.rPad(" "," ",27);
			List autos = new ArrayList();
			int reg=0;			
			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
				indContinua = sys.Util.rPad(" "," ",27);
				continua   = true;				  
				while (continua)
				{
					/*
					 parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					 sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					 tpJunta ;
					 */
					
					/*DPWEB*/
					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					tpJunta;
					
					resultado = GuiaDaoBroker.getInstance().chamaTransBRK("049",parteVar,indContinua);
					AutoInfracaoBean myAt = new AutoInfracaoBean();							
					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
						myGuia.setMsgErro(myAt.getMsgErro());
						return false;					
					}
					String linha = "";
					int i = 0;
					for (i=0; i < 28; i++) {				
						/*linha = resultado.substring((i*112)+3,(i+1)*112+3);*/
						
						/*DPWEB*/
						linha = resultado.substring((i*113)+3,(i+1)*113+3);
						
						AutoInfracaoBean myAuto = new AutoInfracaoBean();
						RequerimentoBean myReq = new RequerimentoBean();
						
						myAuto.setInfracao(new InfracaoBean());	
						myAuto.setProprietario(new ResponsavelBean());
						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
						setPosIni(0);											
						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
						if (myAuto.getNumAutoInfracao().trim().length()>0) {
							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
							/*Retirar*/
							GuiaDaoBroker.getInstance().VerificaAutoOracle(myAuto,conn);								
							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
							myAuto.getInfracao().setCodInfracao(getPedaco(linha,3," "," "));			
							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
							// Buscar Resultado
							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
							
							/*DPWEB*/
							String sIndReqWEB=getPedaco(linha,1," "," ");
							
							
							//								myAuto.setEventoOK("1");
							
							myReq.setNumRequerimento(myAuto.getNumRequerimento());
							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
							dao.LeRequerimentoLocal(myReq,conn); /*07/08/2007*/
							
							/*DPWEB*/
							myReq.setIndReqWEB(sIndReqWEB);		
							
							
							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");							
							Date dat_InicioRemessa = new Date();
							Date dat_Requerimento  = new Date();
							boolean bProtocolo=true;
							
							if(datInicioRemessa.length()>0)
							{
								/*dao.LeRequerimentoLocal(myReq,conn);*/
								dat_InicioRemessa = df.parse(datInicioRemessa);								
								if (myReq.getDatRequerimento().length()==0)
									myReq.setDatRequerimento("26/07/2004");
								
								dat_Requerimento = df.parse(myReq.getDatRequerimento());
								if (dat_Requerimento.after(dat_InicioRemessa))
								{
									if ( (myReq.getSitProtocolo().length()==0) || (myReq.getSitProtocolo().equals("0")) )
										bProtocolo=false;
								}
							}
							
//							Verifica se tem AI Digitalizado
							sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
							" WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
							rs    = stmt.executeQuery(sCmd) ;
							while (rs.next()) {
								myAuto.setTemAIDigitalizado(true);			
							}				
							myAuto.setEventoOK("0");
							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
							{
								if (bProtocolo) {
									myAuto.getRequerimentos().add(myReq);
									autos.add(myAuto) ;
								}
							}
						}										
					}
					/*setPosIni(3+(28*112));*/
					
					/*DPWEB*/
					setPosIni(3+(28*113));
					if (reg>Integer.parseInt(numMaxProc)) break;					
					
					indContinua = getPedaco(resultado,27," "," ");
					if (indContinua.trim().length()<=0) continua=false;
					reg++;
				}			    
			}
			myGuia.setAutos(autos);	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	
	
	public void Acerta_DP_WEB() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT SUBSTR(TXT_PARTE_VARIAVEL,1,12) AS NUM_AUTO_INFRACAO, "+
					"SUBSTR(TXT_PARTE_VARIAVEL,89,30) AS NUM_REQUERIMENTO, "+
					"SUBSTR(TXT_PARTE_VARIAVEL,129,40) AS NOM_RESPONSAVEL, "+
					"TO_DATE(DAT_PROC,'DD/MM/YYYY') AS DAT_PROC, "+
					"SUBSTR(TXT_PARTE_VARIAVEL,184,4000) AS TXT_MOTIVO "+
					"FROM TSMI_LOG_BROKER WHERE DAT_PROC>'01/08/2007' AND NOM_USERNAME='SMITWEB' AND "+
					"COD_TRANSACAO='206' AND TXT_RESULTADO=' 000' "+
					"ORDER BY DAT_PROC";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codAutoInfracao = "";
					String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO");
					String codRequerimento = "";
					String numRequerimento = rs.getString("NUM_REQUERIMENTO");
					String nomResponsavel  = rs.getString("NOM_RESPONSAVEL");
					String datProc         = Util.converteData(rs.getDate("DAT_PROC"));
					String txtMotivo       = rs.getString("TXT_MOTIVO");
					RequerimentoBean myReq = new RequerimentoBean();
					
					
					
					
					sCmd_VEX="SELECT COD_AUTO_INFRACAO "+
					"FROM TSMI_AUTO_INFRACAO WHERE "+
					"NUM_AUTO_INFRACAO='"+numAutoInfracao.trim()+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) codAutoInfracao = rs_VEX.getString("COD_AUTO_INFRACAO");
					
					
					sCmd_VEX="SELECT COD_REQUERIMENTO "+
					"FROM TSMI_REQUERIMENTO WHERE "+
					"COD_AUTO_INFRACAO='"+codAutoInfracao+"' AND "+
					"NUM_REQUERIMENTO='"+numRequerimento.trim()+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) continue;
					
					System.out.println("Processando...auto:"+numAutoInfracao);					
					
					sCmd_VEX = "SELECT SEQ_TSMI_REQUERIMENTO.NEXTVAL CODIGO FROM DUAL";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) codRequerimento = rs_VEX.getString("CODIGO");
					
					
					myReq.setCodAutoInfracao(codAutoInfracao);
					myReq.setCodTipoSolic("DP");
					myReq.setCodRequerimento(codRequerimento);
					myReq.setNumRequerimento(numRequerimento);
					myReq.setDatRequerimento(datProc);
					myReq.setCodStatusRequerimento("0");
					myReq.setNomUserNameDP("SMITWEB");
					myReq.setCodOrgaoLotacaoDP("1");
					myReq.setNomResponsavelDP("");
					myReq.setDatProcDP(datProc);
					myReq.setTxtJustificativa(txtMotivo);
					myReq.setNomRequerente(nomResponsavel);
					
					sCmd_VEX  =  "INSERT INTO TSMI_REQUERIMENTO " +
					"(COD_REQUERIMENTO," +
					"COD_AUTO_INFRACAO," +
					"COD_TIPO_SOLIC," +
					"NUM_REQUERIMENTO," +
					"DAT_REQUERIMENTO," +
					"COD_STATUS_REQUERIMENTO," +
					"NOM_USERNAME_DP," +
					"COD_ORGAO_LOTACAO_DP," +
					"NOM_RESP_DP," +			
					"DAT_PROC_DP) "+
					" VALUES (" +
					" '"+myReq.getCodRequerimento()+"'," +
					" '"+myReq.getCodAutoInfracao()+"'," +
					"'"+myReq.getCodTipoSolic()+"'," +
					" '"+myReq.getNumRequerimento()+"'," +
					" to_date('"+myReq.getDatRequerimento()+"','dd/mm/yyyy')," +
					" '"+myReq.getCodStatusRequerimento()+"'," +
					" '"+myReq.getNomUserNameDP()+"'," +
					" '"+myReq.getCodOrgaoLotacaoDP()+"'," +
					" '"+myReq.getNomResponsavelDP()+"',"+
					" to_date('"+myReq.getDatProcDP()+"','dd/mm/yyyy') )";
					stmt_VEX.execute(sCmd_VEX);
					
					
					sCmd_VEX  =  "INSERT INTO TSMI_REQUERIMENTO_WEB " +
					"(COD_AUTO_INFRACAO," +
					"NUM_REQUERIMENTO,"+
					"COD_REQUERIMENTO," +
					"TXT_EMAIL,"+
					"NOM_REQUERENTE," +
					"TXT_MOTIVO," +
					"IND_WEB) " +
					" VALUES (" +
					" '"+myReq.getCodAutoInfracao()+"'," +
					" '"+myReq.getNumRequerimento()+"'," +
					" '"+myReq.getCodRequerimento()+"'," +
					" '"+myReq.getTxtEmail()+"'," +
					" '"+myReq.getNomRequerente()+"'," +
					" '"+myReq.getTxtJustificativa()+"'," +
					" '"+myReq.getIndReqWEB()+"')";
					stmt_VEX.execute(sCmd_VEX);
					
					
					
					
				}
				
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			System.out.println("ERRO:"+e.getMessage());				
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	public void ControleRetorno___CTRVEXT1() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM T1 " +
					"ORDER BY DAT_ENVIO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					
					String codControleVEX  = rs.getString("PK_COD_CONTROLE_VEX");					
					
					sCmd_VEX="SELECT * "+
					"FROM T2 WHERE "+
					"PK_COD_CONTROLE_VEX='"+codControleVEX+"' "+
					"ORDER BY DAT_RETORNO";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) 
					{
						System.out.println("Processando...:"+codControleVEX);
						
						
						String datRetorno      = Util.converteData(rs_VEX.getDate("DAT_RETORNO"));
						if (datRetorno==null) datRetorno="";
						
						String codArquivo      = rs_VEX.getString("COD_ARQUIVO_RETORNO");
						if (codArquivo==null) codArquivo="";
						
						String nomArquivo      = rs_VEX.getString("NOM_ARQUIVO_RETORNO");
						if (nomArquivo==null) nomArquivo="";
						
						String mesDatRetorno   = rs_VEX.getString("MES_DAT_RETORNO");
						if (mesDatRetorno==null) mesDatRetorno="";
						
						String anoDatRetorno   = rs_VEX.getString("ANO_DAT_RETORNO");
						if (anoDatRetorno==null) anoDatRetorno="";
						
						
						sCmd_VEX="UPDATE TSMI_CTR_VEX_DET SET "+
						"CODARQUIVORETORNO='"+codArquivo+"', "+    
						"NOMARQUIVORETORNO='"+nomArquivo+"', "+    
						"DATRETORNO=TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
						"MESDATRETORNO='"+mesDatRetorno+"', "+        
						"ANODATRETORNO='"+anoDatRetorno+"' "+        
						"WHERE PK_COD_CONTROLE_VEX='"+codControleVEX+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
					}
					
					
				}
				
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			System.out.println("ERRO:"+e.getMessage());				
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	
	public void ControleRetorno___PNT() throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd =
					"SELECT * FROM TSMI_CONTROLE_VEX "+
					"WHERE "+ 
					"(TIP_NOTIFICACAO='0' OR TIP_NOTIFICACAO='2') " +
					"AND SUBSTR(NOM_ARQUIVO,1,10) LIKE '%EMITEPONTO%' AND "+
					"DAT_ENVIO>='01/01/2007' AND DAT_RETORNO IS NOT NULL "+
					"ORDER BY DAT_ENVIO"; 
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo        = "";
					String nomArquivo        = "";
					String datRetorno        = Util.converteData(rs.getDate("DAT_RETORNO"));
					String mesDatRetorno     = rs.getString("MES_DAT_RETORNO");
					String anoDatRetorno     = rs.getString("ANO_DAT_RETORNO");
					String codigoRetorno     = rs.getString("COD_RETORNO");
					String indEntrega        = rs.getString("IND_ENTREGUE");
					String codControleVEX  = rs.getString("PK_COD_CONTROLE_VEX");
					String numNotificacao    = rs.getString("NUM_NOTIFICACAO");
					String seqnumNotificacao = rs.getString("SEQ_NOTIFICACAO");
					
					
					
					
					String indCSS="";
					sCmd_VEX="SELECT IND_CSS "+
					"FROM TSMI_CODIGO_RETORNO WHERE "+
					"NUM_CODIGO_RETORNO='"+codigoRetorno+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) indCSS=rs_VEX.getString("IND_CSS");
					if (indCSS==null) indCSS="";					
					
					
					sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_DET ( "+
					"PK_COD_CONTROLE_VEX_DET,PK_COD_CONTROLE_VEX,COD_ARQUIVO_RETORNO,NOM_ARQUIVO_RETORNO,DAT_RETORNO,"+
					"MES_DAT_RETORNO,ANO_DAT_RETORNO,COD_RETORNO,"+
					"IND_ENTREGUE,IND_CSS) VALUES (SEQ_TSMI_CONTROLE_VEX_DET.NEXTVAL,"+
					"'"+codControleVEX+"',"+   
					"'"+codArquivo+"',"+    
					"'"+nomArquivo+"',"+    
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
					"'"+mesDatRetorno+"', "+        
					"'"+anoDatRetorno+"', "+        
					"'"+codigoRetorno+"', "+            
					"'"+indEntrega+"', "+
					"'"+indCSS+"') ";
					stmt_VEX.executeQuery(sCmd_VEX);					
					
				}
				
			} catch (SQLException e) {
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	
	public void ControleRetornoVEX_MDB(RelatArqNotificacaoBean ControleEmiteVEXID) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);     
			conn.setAutoCommit(false);
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs = null;
			ResultSet rs_VEX   = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT "+ 
				"T1.COD_ARQUIVO AS COD_ARQUIVO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T1.DAT_RECEBIMENTO AS DAT_ENVIO, "+
				"TO_CHAR(T1.DAT_RECEBIMENTO,'MM') AS MES_DAT_ENVIO, "+
				"TO_CHAR(T1.DAT_RECEBIMENTO,'YYYY') AS ANO_DAT_ENVIO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,281,12) AS NUM_AUTO_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,111,7) AS NUM_PLACA, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,163,8) AS DAT_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,7,9) AS NUM_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,480,1) AS TIP_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,426,2) AS COD_UF, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,378,8) AS NUM_CEP, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,386,40) AS NOM_MUNICIPIO "+
				"FROM "+
				"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
				"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND " +
				"T1.COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"' AND "+
				"T2.COD_RETORNO IS NULL";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = rs.getString("COD_ARQUIVO");
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String mesDatEnvio     = rs.getString("MES_DAT_ENVIO");
					String anoDatEnvio     = rs.getString("ANO_DAT_ENVIO");
					String datRetorno      = "";
					String mesDatRetorno   = "";
					String anoDatRetorno   = "";
					String codigoRetornoEnvio = "000";
					String codigoRetorno   = "";
					String indEntrega      = "";
					String indFaturado     = "";
					String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO").trim();
					String numPlaca        = rs.getString("NUM_PLACA").trim();
					String datInfracao     = sys.Util.formataDataDDMMYYYY(rs.getString("DAT_INFRACAO"));
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO").trim();
					String seqNotificacao  = "0";
					String tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					System.out.println("Processando...codArquivo :"+codArquivo+" numNotificacao :"+numNotificacao);
					
					
					
					if ( (tipNotificacao.equals("0")) ||
							(tipNotificacao.equals("2"))
					)
					{
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER "+
						"SET " +
						"NUM_PLACA='"+numPlaca+"', "+
						"DAT_INFRACAO=TO_DATE('"+datInfracao+"','DD/MM/YYYY') "+
						"WHERE NUM_AUTO_INFRACAO='"+numAutoInfracao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					else
					{
						if (numNotificacao.length()==0) 
							continue;
						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER "+
						"SET " +
						"NUM_PLACA='"+numPlaca+"', "+
						"DAT_INFRACAO=TO_DATE('"+datInfracao+"','DD/MM/YYYY') "+
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
				}
			} catch (Exception e) { conn.rollback();}
			
			conn.commit();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	public boolean ControleRetornoVEX(RelatArqRetornoBean ControleEmiteVEXID) throws SQLException, RobotException 
	{
		Connection conn = null;
		String sCmd        = "";
		String sCmd_VEX    = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);			
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			try
			{
				
				REG.LinhaArquivoRec linhaArq = new REG.LinhaArquivoRec();
				
				
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT "+ 
				"T2.COD_ARQUIVO_RETORNO, "+
				"T2.COD_LINHA_ARQ_REC_RETORNO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T2.DAT_RETORNO, "+
				"TO_CHAR(T2.DAT_RETORNO,'MM') AS MES_DAT_RETORNO, "+
				"TO_CHAR(T2.DAT_RETORNO,'YYYY') AS ANO_DAT_RETORNO, "+
				"T2.NUM_CODIGO_RETORNO AS CODIGO_RETORNO, "+
				"T2.IND_ENTREGA, "+
				"T2.NUM_NOTIFICACAO "+
				"FROM "+
				"TSMI_ARQUIVO_RECEBIDO T1,TSMI_CONTROLE_NOTIFICACOES T2 "+
				"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO_RETORNO AND " +
				"T1.COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"'";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = rs.getString("COD_ARQUIVO_RETORNO");
					String codLinhaArquivo = rs.getString("COD_LINHA_ARQ_REC_RETORNO");
					String datRetorno      = Util.converteData(rs.getDate("DAT_RETORNO"));
					String mesDatRetorno   = rs.getString("MES_DAT_RETORNO");
					String anoDatRetorno   = rs.getString("ANO_DAT_RETORNO");
					String codigoRetorno   = rs.getString("CODIGO_RETORNO");
					String indEntrega      = rs.getString("IND_ENTREGA");
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					String indFatura       = "";
					
					sCmd_VEX="SELECT IND_FATURA FROM TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+codigoRetorno+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) {
						indFatura=rs_VEX.getString("IND_FATURA");
						if (indFatura==null) indFatura="";		
					}
					
					if (codigoRetorno.trim().equals("09"))
					{
						codigoRetorno="03 ";
						indEntrega   ="S";
						indFatura    ="S";
					}
					
					System.out.println("Processando...arquivo:"+ControleEmiteVEXID.getNomArquivo()+
							"   data:"+datRetorno+" notifica��o:"+numNotificacao);
					
					String codControleVEX_MST    ="";
					String codControleVEX_DETAIL ="";
					String dat_Envio_MST         ="";
					String mes_Dat_Envio_MST     =""; 
					String ano_Dat_Envio_MST     ="";
					String dat_Retorno_MST       ="";
					String ind_Entregue_Notificacao_MST   ="";
					
					
					sCmd_VEX="SELECT PK_COD_CONTROLE_VEX_MASTER,DAT_ENVIO, " +
					"TO_CHAR(DAT_ENVIO,'MM') AS MES_DAT_ENVIO,TO_CHAR(DAT_ENVIO,'YYYY') AS ANO_DAT_ENVIO, "+
					"DAT_RETORNO,IND_ENTREGUE_NOTIFICACAO "+
					"FROM TSMI_CONTROLE_VEX_MASTER WHERE "+
					"NUM_NOTIFICACAO='"+numNotificacao+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next())
					{
						codControleVEX_MST=rs_VEX.getString("PK_COD_CONTROLE_VEX_MASTER");
						
						dat_Envio_MST=Util.converteData(rs_VEX.getDate("DAT_ENVIO"));
						
						if (dat_Envio_MST==null) dat_Envio_MST="";
						
						mes_Dat_Envio_MST=rs_VEX.getString("MES_DAT_ENVIO");
						if (mes_Dat_Envio_MST==null) mes_Dat_Envio_MST="";
						
						ano_Dat_Envio_MST=rs_VEX.getString("ANO_DAT_ENVIO");
						if (ano_Dat_Envio_MST==null) ano_Dat_Envio_MST="";
						
						dat_Retorno_MST=rs_VEX.getString("DAT_RETORNO");
						if (dat_Retorno_MST==null) dat_Retorno_MST="";
						
						ind_Entregue_Notificacao_MST=rs_VEX.getString("IND_ENTREGUE_NOTIFICACAO");
						if (ind_Entregue_Notificacao_MST==null) ind_Entregue_Notificacao_MST="";
					}
					if (codControleVEX_MST.length()==0){
						
						// 03 - Nao existe
						linhaArq.setCodLinhaArqRec(codLinhaArquivo);
						linhaArq.setCodRetorno("03");
						linhaArq.processaRetornoArDigTMP();
						
						continue;
					}
					
					
					boolean bRetorno=false,bRecupera=false;
					/* N�o Retornou*/
					if (dat_Retorno_MST.length()==0) bRetorno=true;
					else
					{
						if (indEntrega.equals("N")) bRecupera=false;
						else
							if (ind_Entregue_Notificacao_MST.equals("S")) bRecupera=false;
							else bRecupera=true;
					}
					
					/*Todos Retornos*/
					
					sCmd_VEX = "SELECT SEQ_TSMI_CONTROLE_VEX_DETAIL.NEXTVAL CODIGO FROM DUAL";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) codControleVEX_DETAIL=rs_VEX.getString("CODIGO");
					
					
					sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_DETAIL ( "+
					"PK_COD_CONTROLE_VEX_DETAIL,PK_COD_CONTROLE_VEX_MASTER,COD_ARQUIVO,DAT_RETORNO,"+
					"COD_RETORNO,IND_ENTREGUE_RETORNO,IND_FATURADO_RETORNO) VALUES ("+
					"'"+codControleVEX_DETAIL+"',"+
					"'"+codControleVEX_MST+"',"+   
					"'"+codArquivo+"',"+    
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
					"'"+codigoRetorno+"', "+            
					"'"+indEntrega+"', "+
					"'"+indFatura+"') ";
					stmt_VEX.executeQuery(sCmd_VEX);
					
					if (bRetorno) {
						
						// 01 - Apropriados 						
						linhaArq.setCodLinhaArqRec(codLinhaArquivo);
						linhaArq.setCodRetorno("01");
						linhaArq.processaRetornoArDigTMP();
						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
						"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
						"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
						"PK_COD_CONTROLE_VEX_DETAIL_RET='"+codControleVEX_DETAIL+"', "+
						"DAT_RETORNO=TO_DATE('"+datRetorno+"','DD/MM/YYYY'), "+
						"COD_RETORNO='"+codigoRetorno+"', "+            
						"IND_ENTREGUE_RETORNO='"+indEntrega+"', "+
						"IND_FATURADO_RETORNO='"+indFatura+"' "+		
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						
						/*Totalizar Retornos-por data de emiss�o*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						int iDias=Util.DifereDias(dat_Envio_MST,datRetorno);						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_EMITIDO_RETORNO=QTD_EMITIDO_RETORNO+1, "+
						"QTD_DIAS_RETORNO=QTD_DIAS_RETORNO+"+iDias+" "+						
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						/*Totalizar Retornos-por data de retorno*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_RETORNO=QTD_RETORNO+1 "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					else{ 
						// 02 - Duplicados
						linhaArq.setCodLinhaArqRec(codLinhaArquivo);
						linhaArq.setCodRetorno("02");
						linhaArq.processaRetornoArDigTMP();
						
					}
					
					if ( (bRecupera) && (indFatura.equals("N")) ) {
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
						"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
						"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
						"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
						"DAT_RETORNO_RECUPERACAO=TO_DATE('"+datRetorno+"','DD/MM/YYYY'), "+
						"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"', "+            
						"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
						"IND_FATURADO_RECUPERACAO='"+indFatura+"' "+		
						"WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						
						/*Totalizar Retornos-por data de retorno*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_RECUPERADA_RETORNO=QTD_RECUPERADA_RETORNO+1 "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
				}
				sCmd_VEX="UPDATE TSMI_ARQUIVO_RECEBIDO " +
				"SET SIT_VEX='S' "+
				"WHERE COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"'";
				stmt_VEX.executeQuery(sCmd_VEX);
			} catch (Exception e) {
				conn.rollback();
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				return false;
			}
			conn.commit();
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();
			System.out.println("Comando :"+sCmd);
			System.out.println("Comando_VEX :"+sCmd_VEX);
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	public void ControleRetornoVEXPNT(String dat_Retorno) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);			
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;		
			String sCmd        = "";
			String sCmd_VEX    = "";
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd= "SELECT " +
				"DAT_RETORNO, "+
				"TO_CHAR(DAT_RETORNO,'MM') AS MES_DAT_RETORNO, "+
				"TO_CHAR(DAT_RETORNO,'YYYY') AS ANO_DAT_RETORNO, "+
				"COD_RETORNO as CODIGO_RETORNO, "+
				"IND_ENTREGUE AS IND_ENTREGA, "+ 
				"NUM_NOTIFICACAO, "+
				"SEQ_NOTIFICACAO "+
				"FROM "+
				"TPNT_NOTIFICACAO "+
				"WHERE SIT_VEX IS NULL AND "+
				"DAT_RETORNO>=TO_DATE('"+dat_Retorno+"','DD/MM/YYYY') "+					
				"ORDER BY DAT_RETORNO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = "";
					String datRetorno      = Util.converteData(rs.getDate("DAT_RETORNO"));
					String mesDatRetorno   = rs.getString("MES_DAT_RETORNO");
					String anoDatRetorno   = rs.getString("ANO_DAT_RETORNO");
					String codigoRetorno   = rs.getString("CODIGO_RETORNO");
					String indEntrega      = rs.getString("IND_ENTREGA");
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					String seqnumNotificacao = rs.getString("SEQ_NOTIFICACAO");					
					String indFatura       = "";
					sCmd_VEX="SELECT IND_FATURA FROM TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+codigoRetorno+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) {
						indFatura=rs_VEX.getString("IND_FATURA");
						if (indFatura==null) indFatura="";		
					}
					
					
					System.out.println("Processando...data:"+datRetorno+" notifica��o:"+numNotificacao);
					
					String codControleVEX_MST    ="";
					String codControleVEX_DETAIL ="";
					String dat_Envio_MST         ="";
					String mes_Dat_Envio_MST     =""; 
					String ano_Dat_Envio_MST     ="";
					String dat_Retorno_MST       ="";
					String ind_Entregue_Notificacao_MST   ="";
					
					
					sCmd_VEX="SELECT PK_COD_CONTROLE_VEX_MASTER,DAT_ENVIO, " +
					"TO_CHAR(DAT_ENVIO,'MM') AS MES_DAT_ENVIO,TO_CHAR(DAT_ENVIO,'YYYY') AS ANO_DAT_ENVIO, "+
					"DAT_RETORNO,IND_ENTREGUE_NOTIFICACAO "+
					"FROM TSMI_CONTROLE_VEX_MASTER WHERE "+
					"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
					"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next())
					{
						codControleVEX_MST=rs_VEX.getString("PK_COD_CONTROLE_VEX_MASTER");
						
						dat_Envio_MST=rs_VEX.getString("DAT_ENVIO");
						if (dat_Envio_MST==null) dat_Envio_MST="";
						
						mes_Dat_Envio_MST=rs_VEX.getString("MES_DAT_ENVIO");
						if (mes_Dat_Envio_MST==null) mes_Dat_Envio_MST="";
						
						ano_Dat_Envio_MST=rs_VEX.getString("ANO_DAT_ENVIO");
						if (ano_Dat_Envio_MST==null) ano_Dat_Envio_MST="";
						
						dat_Retorno_MST=rs_VEX.getString("DAT_RETORNO");
						if (dat_Retorno_MST==null) dat_Retorno_MST="";
						
						ind_Entregue_Notificacao_MST=rs_VEX.getString("IND_ENTREGUE_NOTIFICACAO");
						if (ind_Entregue_Notificacao_MST==null) ind_Entregue_Notificacao_MST="";
					}
					
					boolean bRetorno=false,bRecupera=false;
					/* N�o Retornou*/
					if (dat_Retorno_MST.length()==0) bRetorno=true;
					else
					{
						if (indEntrega.equals("N")) bRecupera=false;
						else
							if (ind_Entregue_Notificacao_MST.equals("S")) bRecupera=false;
							else bRecupera=true;
					}
					
					/*Todos Retornos*/
					
					sCmd_VEX = "SELECT SEQ_TSMI_CONTROLE_VEX_DETAIL.NEXTVAL CODIGO FROM DUAL";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) codControleVEX_DETAIL=rs_VEX.getString("CODIGO");
					
					
					sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_DETAIL ( "+
					"PK_COD_CONTROLE_VEX_DETAIL,PK_COD_CONTROLE_VEX_MASTER,COD_ARQUIVO,DAT_RETORNO,"+
					"COD_RETORNO,IND_ENTREGUE_RETORNO,IND_FATURADO_RETORNO) VALUES ("+
					"'"+codControleVEX_DETAIL+"',"+
					"'"+codControleVEX_MST+"',"+   
					"'"+codArquivo+"',"+    
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
					"'"+codigoRetorno+"', "+            
					"'"+indEntrega+"', "+
					"'"+indFatura+"') ";
					stmt_VEX.executeQuery(sCmd_VEX);
					
					
					if (bRetorno) {
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
						"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
						"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
						"PK_COD_CONTROLE_VEX_DETAIL_RET='"+codControleVEX_DETAIL+"', "+
						"DAT_RETORNO=TO_DATE('"+datRetorno+"','DD/MM/YYYY'), "+
						"COD_RETORNO='"+codigoRetorno+"', "+            
						"IND_ENTREGUE_RETORNO='"+indEntrega+"', "+
						"IND_FATURADO_RETORNO='"+indFatura+"' "+		
						"WHERE " +
						"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
						"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						
						/*Totalizar Retornos-por data de emiss�o*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						int iDias=Util.DifereDias(dat_Envio_MST,datRetorno);						
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_EMITIDO_RETORNO=QTD_EMITIDO_RETORNO+1, "+
						"QTD_DIAS_RETORNO=QTD_DIAS_RETORNO+"+iDias+" "+						
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						/*Totalizar Retornos-por data de retorno*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_RETORNO=QTD_RETORNO+1 "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
					if ( (bRecupera) && (indFatura.equals("N")) ) {
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_MASTER SET "+
						"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
						"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+						
						"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
						"DAT_RETORNO_RECUPERACAO=TO_DATE('"+datRetorno+"','DD/MM/YYYY'), "+
						"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"', "+            
						"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
						"IND_FATURADO_RECUPERACAO='"+indFatura+"' "+		
						"WHERE " +
						"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
						"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
						
						
						/*Totalizar Retornos-por data de retorno*/
						sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
						if (!rs_VEX.next())
						{
							sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
							"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
							"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
							"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
							"QTD_DIAS_RETORNO) VALUES ("+
							"'"+ano_Dat_Envio_MST+"',"+
							"'"+mes_Dat_Envio_MST+"',"+
							"'"+anoDatRetorno+"',"+
							"'"+mesDatRetorno+"',"+
							"'"+codigoRetorno+"',"+
							"'"+indEntrega+"',"+							
							"'"+indFatura+"',0,0,0,0,0,0)";						
							stmt_VEX.executeQuery(sCmd_VEX);					
						}
						sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
						"SET "+
						"QTD_RECUPERADA_RETORNO=QTD_RECUPERADA_RETORNO+1 "+
						"WHERE "+
						"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
						"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
						"ANO_RETORNO='"+anoDatRetorno+"' AND "+
						"MES_RETORNO='"+mesDatRetorno+"' AND "+
						"COD_RETORNO='"+codigoRetorno+"' AND "+
						"IND_ENTREGUE='"+indEntrega+"'";
						stmt_VEX.executeQuery(sCmd_VEX);
					}
					
					sCmd_VEX="UPDATE TPNT_NOTIFICACAO SET "+
					"SIT_VEX='S' "+					
					"WHERE "+
					"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
					"SEQ_NOTIFICACAO='"+seqnumNotificacao+"'";
					stmt_VEX.executeQuery(sCmd_VEX);
					
					
					
					conn.commit();					
					
					
				}
			} catch (Exception e) {
				conn.rollback();
				System.out.println("Comando :"+sCmd);
				System.out.println("Comando_VEX :"+sCmd_VEX);
				
			}
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return;
		} catch (Exception e) {
			conn.rollback();
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	public boolean ControleEmiteVEXPNT(RelatArqNotificacaoBean ControleEmiteVEXID) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);   
			conn.setAutoCommit(false);			
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs     = null;
			ResultSet rs_VEX = null;
			try
			{
				
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT "+ 
				"T1.COD_ARQUIVO AS COD_ARQUIVO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T1.DAT_RECEBIMENTO AS DAT_ENVIO, "+
				"TO_CHAR(T1.DAT_RECEBIMENTO,'MM') AS MES_DAT_ENVIO, "+
				"TO_CHAR(T1.DAT_RECEBIMENTO,'YYYY') AS ANO_DAT_ENVIO, "+
				"T2.NUM_NOTIFICACAO,T2.SEQ_NOTIFICACAO, "+
				"T2.TIP_NOTIFICACAO, "+
				"T3.UF_CNH_CONDUTOR AS COD_UF, "+
				"T3.NUM_CEP_CONDUTOR AS NUM_CEP, "+
				"T3.NOM_MUNIC_CONDUTOR AS NOM_MUNICIPIO "+
				"FROM TPNT_ARQUIVO T1,TPNT_NOTIFICACAO T2, "+
				"TPNT_PROCESSO T3 "+
				"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "+
				"T2.COD_PROCESSO=T3.COD_PROCESSO AND "+
				"T1.COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"'";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codArquivo      = rs.getString("COD_ARQUIVO");
					String datEnvio        = Util.converteData(rs.getDate("DAT_ENVIO"));
					String mesDatEnvio     = rs.getString("MES_DAT_ENVIO");
					String anoDatEnvio     = rs.getString("ANO_DAT_ENVIO");
					String datRetorno      = "";
					String mesDatRetorno   = "";
					String anoDatRetorno   = "";
					String codigoRetornoEnvio = "000";
					String codigoRetorno   = "";
					String indEntrega      = "";
					String indFaturado     = "";
					String numAutoInfracao = "";
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					String seqNotificacao  = rs.getString("SEQ_NOTIFICACAO");
					String tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					
					
					System.out.println("Processando...arquivo:"+ControleEmiteVEXID.getNomArquivo()+
							"   data:"+datEnvio+" notifica��o:"+numNotificacao);
					
					
					String codControleVEX_MASTER="",codControleVEX_DETAIL="";
					sCmd_VEX = "SELECT SEQ_TSMI_CONTROLE_VEX_MASTER.NEXTVAL CODIGO FROM DUAL";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);
					if (rs_VEX.next()) codControleVEX_MASTER=rs_VEX.getString("CODIGO");
					
					
					sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_MASTER "+
					"(" +
					"PK_COD_CONTROLE_VEX_MASTER, "+
					"COD_ARQUIVO, "+
					"NUM_NOTIFICACAO, "+
					"SEQ_NOTIFICACAO, "+
					"TIP_NOTIFICACAO, "+
					"DAT_ENVIO, "+
					"NUM_AUTO_INFRACAO, "+
					"IND_ENTREGUE_NOTIFICACAO, "+
					"IND_FATURADO_NOTIFICACAO, "+					
					"PK_COD_CONTROLE_VEX_DETAIL_RET, "+
					"DAT_RETORNO, "+
					"COD_RETORNO, "+
					"IND_ENTREGUE_RETORNO, "+
					"IND_FATURADO_RETORNO) VALUES "+ 
					"('"+codControleVEX_MASTER+"',"+
					"'"+codArquivo+"',"+
					"'"+numNotificacao+"',"+
					"'"+seqNotificacao+"',"+					
					"'"+tipNotificacao+"',"+
					"TO_DATE('"+datEnvio+"','DD/MM/YYYY'),"+
					"'"+numAutoInfracao+"',"+
					"'"+indEntrega+"',"+
					"'"+indFaturado+"',"+					
					"'"+codControleVEX_DETAIL+"',"+
					"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+					
					"'"+codigoRetorno+"',"+
					"'"+indEntrega+"',"+
					"'"+indFaturado+"')";					
					stmt_VEX.executeQuery(sCmd_VEX);
					
					/*Totalizar Emiss�es*/
					sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
					"WHERE "+
					"ANO_ENVIO='"+anoDatEnvio+"' AND "+
					"MES_ENVIO='"+mesDatEnvio+"' AND "+
					"COD_RETORNO='"+codigoRetornoEnvio+"'";
					rs_VEX=stmt_VEX.executeQuery(sCmd_VEX);		
					if (!rs_VEX.next())
					{
						sCmd_VEX="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
						"MES_ENVIO,COD_RETORNO,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
						"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
						"QTD_DIAS_RETORNO) VALUES ("+
						"'"+anoDatEnvio+"',"+
						"'"+mesDatEnvio+"',"+
						"'"+codigoRetornoEnvio+"',"+
						"'"+indFaturado+"',0,0,0,0,0,0)";						
						stmt_VEX.executeQuery(sCmd_VEX);					
					}
					sCmd_VEX="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
					"SET "+
					"QTD_EMITIDO=QTD_EMITIDO+1 "+
					"WHERE "+
					"ANO_ENVIO='"+anoDatEnvio+"' AND "+
					"MES_ENVIO='"+mesDatEnvio+"' AND "+
					"COD_RETORNO='"+codigoRetornoEnvio+"'";
					stmt_VEX.executeQuery(sCmd_VEX);					
					
				}
				sCmd_VEX="UPDATE TPNT_ARQUIVO " +
				"SET SIT_VEX='S' "+
				"WHERE COD_ARQUIVO='"+ControleEmiteVEXID.getCodArquivo()+"'";
				stmt_VEX.executeQuery(sCmd_VEX);
				conn.commit();
			} catch (Exception e) {
				conn.rollback();
				return false;
			}
			rs.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	
	
	
	
	
	
	
	
	public void VerificaRequerimento(String sDataProcIni,String sDataProcFim,ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			String sTransacao="'206','326','334','208','328','335'";
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "select DISTINCT substr(txt_parte_variavel,1,12) as NUM_AUTO, "+
				"substr(txt_parte_variavel,20,6) as cod_orgao, "+
				"substr(txt_parte_variavel,13,7) as placa "+
				"from tsmi_log_broker_bkp_2 where "+
				" DAT_PROC>=to_date('"+sDataProcIni+"','dd/mm/yyyy') and  "+
				" DAT_PROC<to_date('"+sDataProcFim+"','dd/mm/yyyy') and  "+
				" COD_TRANSACAO IN ("+sTransacao+")";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO"));
					myAuto.setNumPlaca(rs.getString("PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						
						
						
						if ("0,1,2,3,4,9".indexOf(myReq.getCodStatusRequerimento())>0 && "DP,DC,1C,1P,2C,2P".indexOf(myReq.getCodTipoSolic())>0)
							dao.GravarRequerimento(myReq,conn);
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void VerificaProcesso(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			int i=0;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				String sCmd="SELECT NUM_AUTO_INFRACAO,NUM_PLACA,NUM_REQUERIMENTO FROM TSMI_AUTO_INFRACAO T1,TSMI_REQUERIMENTO T2 WHERE "+ 
				"T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO AND SIT_PROTOCOLO IS NULL AND T1.COD_STATUS in ('5','15','35') "+
				"AND T2.COD_TIPO_SOLIC IN ('DP','DC','1P','1C','2P','2C') "+
				"AND T2.COD_STATUS_REQUERIMENTO='0' "+
				"AND T1.COD_ORGAO=258650 "+
				"AND T2.DAT_REQUERIMENTO>=TO_DATE('01/10/2005','DD/MM/YYYY') "+
				"ORDER BY NUM_PROCESSO";
				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void AtualizaNIT(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			int i=0;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				String sCmd2="";
				String sCmd="select num_auto_infracao,dsc_resumo_infracao,dsc_local_infracao_notif "+ 
				"from temp_nit_radar order by num_auto_infracao ";
				
				rs = stmt.executeQuery(sCmd);
				String sNumAutoInfracao;
				String sCampo1="";
				String sCampo2="";				
				while (rs.next())
				{
					sNumAutoInfracao=rs.getString("num_auto_infracao").trim();
					sCampo1=rs.getString("dsc_resumo_infracao");
					sCampo2=rs.getString("dsc_local_infracao_notif");
					
					sCmd2="update tsmi_auto_infracao set "+
					" dsc_resumo_infracao     ='"+sCampo1+"',"+
					" dsc_local_infracao_notif='"+sCampo2+"' "+
					" where num_auto_infracao='"+sNumAutoInfracao+"'";
					stmt2.executeQuery(sCmd2);
					i++;					
					System.out.println("Contando=="+i);
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void AtualizaNIT2(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			int i=0;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				String sCmd2="";
				String sCmd="select num_auto_infracao,dsc_marca_modelo_aparelho,dsc_local_infracao_notif "+ 
				"from temp_nit_eletronica order by num_auto_infracao ";
				
				rs = stmt.executeQuery(sCmd);
				String sNumAutoInfracao;
				String sCampo1="";
				String sCampo2="";				
				while (rs.next())
				{
					sNumAutoInfracao=rs.getString("num_auto_infracao").trim();
					sCampo1=rs.getString("dsc_marca_modelo_aparelho");
					sCampo2=rs.getString("dsc_local_infracao_notif");
					
					sCmd2="update tsmi_auto_infracao set "+
					" dsc_marca_modelo_aparelho ='"+sCampo1+"',"+
					" dsc_local_infracao_notif  ='"+sCampo2+"' "+
					" where num_auto_infracao   ='"+sNumAutoInfracao+"'";
					stmt2.executeQuery(sCmd2);
					i++;					
					System.out.println("Contando=="+i);
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void VerificaRequerimentoGuia(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			int i=0;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TSMI_AUTO_INFRACAO "+
				" WHERE "+
				" NUM_AUTO_INFRACAO='N31163327' ";
				/*
				 "(NUM_AUTO_INFRACAO='N31161692' OR "+
				 " NUM_AUTO_INFRACAO='N31032930' OR "+				
				 " NUM_AUTO_INFRACAO='N31029636' OR "+
				 " NUM_AUTO_INFRACAO='N31054732' OR "+
				 " NUM_AUTO_INFRACAO='N30999772' OR "+
				 " NUM_AUTO_INFRACAO='N31693843' OR "+
				 " NUM_AUTO_INFRACAO='N31103556' OR "+				
				 " NUM_AUTO_INFRACAO='N31034073' OR "+
				 " NUM_AUTO_INFRACAO='N31682119' OR "+				
				 " NUM_AUTO_INFRACAO='N31061403' ) ";
				 */				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
					
					
					
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						dao.GravarRequerimento(myReq,conn);
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void VerificaRequerimentoDTRSBranco(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			int i=0;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT DISTINCT T2.NUM_AUTO_INFRACAO,T2.NUM_PLACA FROM TSMI_LOTE_RELATOR T1,TSMI_ITEM_LOTE_RELATOR T2 "+
				" WHERE T1.COD_LOTE_RELATOR=T2.COD_LOTE_RELATOR AND "+
				" T1.DAT_RS IS NULL "+
				" ORDER BY T2.NUM_AUTO_INFRACAO ";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
					
					
					
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						dao.GravarRequerimento(myReq,conn);
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void VerificaRequerimentoBranco(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT DISTINCT NUM_AUTO_INFRACAO,NUM_PLACA FROM TSMI_REQUERIMENTO T1, "+
				"TSMI_AUTO_INFRACAO T2 WHERE T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO "+
				"AND T1.COD_TIPO_SOLIC IS NULL " +
				/*" OR COD_TIPO_SOLIC>=2 "+*/
				/*" AND T2.NUM_AUTO_INFRACAO>'C30151707' "+*/
				"ORDER BY T2.NUM_AUTO_INFRACAO ";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();					
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						dao.GravarRequerimento(myReq,conn);
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void VerificaSITPAGONIT(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TAB_NIT@SMITD WHERE COD_STATUS='010' AND PROCESSADO IS NULL "+ 
				"ORDER BY NUM_AUTO_INFRACAO";
				System.out.println(sCmd);				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					String sCmd2="UPDATE TAB_NIT@SMITD SET IND_PAGO='"+myAuto.getIndPago()+"',DAT_PAGTO='"+myAuto.getDatPag()+"',"+
					"PROCESSADO='S' WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"'";
					stmt2.execute(sCmd2);
					i++;
					System.out.println("Auto de Infracao==> "+i+" "+myAuto.getNumAutoInfracao());
				}
			} catch (SQLException e) {System.out.println("Erro==>"+e.getMessage());
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void AcertaTXTCOMPLEMENTO01PNT(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT COD_PROCESSO,NUM_NOTIFICACAO,SEQ_NOTIFICACAO,to_char(DAT_RETORNO,'dd/mm/yyyy') as DAT_RETORNO FROM TPNT_NOTIFICACAO WHERE DAT_RETORNO IS NOT NULL " +
				"ORDER BY COD_PROCESSO,NUM_NOTIFICACAO,SEQ_NOTIFICACAO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String codProcesso=rs.getString("COD_PROCESSO");
					String sNumNotificacao=rs.getString("NUM_NOTIFICACAO");
					String sSeqNotificacao=rs.getString("SEQ_NOTIFICACAO");
					sNumNotificacao=sys.Util.lPad(sNumNotificacao,"0",9);
					sSeqNotificacao=sys.Util.lPad(sSeqNotificacao,"0",3);
					String sNum=sNumNotificacao+"-"+sSeqNotificacao;
					String sDataRetorno=rs.getString("DAT_RETORNO");
					
					
					String sCodHistoricoProcesso="";
					sCmd="SELECT * FROM TPNT_HISTORICO_PROCESSO "+
					"WHERE COD_PROCESSO='"+codProcesso+"' AND TXT_COMPLEMENTO_01 IS NULL AND "+
					"COD_EVENTO=8 AND ROWNUM=1 ORDER BY COD_HISTORICO_PROCESSO";
					rs2=stmt2.executeQuery(sCmd);
					if (rs2.next())
						sCodHistoricoProcesso=rs2.getString("COD_HISTORICO_PROCESSO");						
					
					sCmd="UPDATE TPNT_HISTORICO_PROCESSO SET TXT_COMPLEMENTO_01='"+sNum+"', "+
					"TXT_COMPLEMENTO_02='"+sDataRetorno+"' "+
					"WHERE COD_HISTORICO_PROCESSO='"+sCodHistoricoProcesso+"'";
					stmt2.execute(sCmd);
					i++;
					System.out.println("Notificacao==> "+sNum);
				}
			} catch (SQLException e) {System.out.println("Erro==>"+e.getMessage());
			}
			rs2.close();
			rs.close();
			stmt2.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void VerificaSITEMITEVEX(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				/*
				 String sCmd  = "SELECT T2.NUM_AUTO_INFRACAO,T1.NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
				 "WHERE "+
				 "T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "+
				 "T1.DAT_RECEBIMENTO>='01/06/2008' AND "+
				 "T1.COD_IDENT_ARQUIVO='EMITEVEX' AND T2.COD_RETORNO='N02' AND "+
				 "T1.COD_STATUS='2' AND T1.COD_ARQUIVO>=76027 ORDER BY T1.COD_ARQUIVO DESC ";
				 */
				
				String sCmd="SELECT * FROM TSMI_AUTO_INFRACAO WHERE COD_AUTO_INFRACAO>=7400000 AND SUBSTR(NUM_AUTO_INFRACAO,1,1)='N' AND DAT_INFRACAO IS NULL ORDER BY COD_AUTO_INFRACAO";
				System.out.println(sCmd);				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					
					String sCmd_Placa="SELECT NUM_PLACA FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"'";
					rs2 = stmt2.executeQuery(sCmd_Placa);
					if (rs2.next())
						myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
					
					System.out.println("Auto de Infracao==> "+i+" "+myAuto.getNumAutoInfracao());					
					dao.LeAutoInfracaoLocal(myAuto,"auto");
					if (myAuto.getDatInfracao().length()>0) continue;
					
					
					
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
					
				}
			} catch (SQLException e) {System.out.println("Erro==>"+e.getMessage());
			}
			rs2.close();			
			rs.close();
			stmt2.close();			
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void VerificaNUMPROCESSO(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				/*
				 String sCmd  = "SELECT T2.NUM_AUTO_INFRACAO,T1.NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
				 "WHERE "+
				 "T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "+
				 "T1.DAT_RECEBIMENTO>='01/06/2008' AND "+
				 "T1.COD_IDENT_ARQUIVO='EMITEVEX' AND T2.COD_RETORNO='N02' AND "+
				 "T1.COD_STATUS='2' AND T1.COD_ARQUIVO>=76027 ORDER BY T1.COD_ARQUIVO DESC ";
				 */
				
				String sCmd="SELECT * FROM TSMI_AUTO_INFRACAO WHERE COD_AUTO_INFRACAO>=7400000 AND SUBSTR(NUM_AUTO_INFRACAO,1,1)='N' AND DAT_INFRACAO IS NULL ORDER BY COD_AUTO_INFRACAO";
				System.out.println(sCmd);				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					
					String sCmd_Placa="SELECT NUM_PLACA FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"'";
					rs2 = stmt2.executeQuery(sCmd_Placa);
					if (rs2.next())
						myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
					
					System.out.println("Auto de Infracao==> "+i+" "+myAuto.getNumAutoInfracao());					
					dao.LeAutoInfracaoLocal(myAuto,"auto");
					if (myAuto.getDatInfracao().length()>0) continue;
					
					
					
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					i++;
					
				}
			} catch (SQLException e) {System.out.println("Erro==>"+e.getMessage());
			}
			rs2.close();			
			rs.close();
			stmt2.close();			
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	
	
	
	public void VerificaDuplicata(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TSMI_CONTROLE_VEX_MASTER WHERE "+
				"DAT_RETORNO>='01/04/2007' AND DAT_RETORNO<='31/12/2007' "+ 
				"ORDER BY NUM_NOTIFICACAO";
				System.out.println(sCmd);				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sDatRetorno            =Util.converteData(rs.getDate("DAT_RETORNO"));
					String numNotificacao         =rs.getString("NUM_NOTIFICACAO");
					String numAutoInfracao        =rs.getString("NUM_AUTO_INFRACAO");
					if (numAutoInfracao==null) numAutoInfracao="";
					int iPkCodControleVexMaster   =rs.getInt("PK_COD_CONTROLE_VEX_MASTER");
					int iPkCodControleVexDetailRet=rs.getInt("PK_COD_CONTROLE_VEX_DETAIL_RET");
					String tipNotificacao         =rs.getString("TIP_NOTIFICACAO");
					int iContador                 =0;
					System.out.println("Notifica��o:"+numNotificacao+" Pk==>"+iPkCodControleVexMaster);
					
					/*Avisos*/
					if (numAutoInfracao.length()>0) {
						if ( (tipNotificacao.equals("0")) || (tipNotificacao.equals("2")) ) continue;
					}
					
					String sCmd2="SELECT * FROM TSMI_CONTROLE_VEX_DETAIL WHERE " +
					"PK_COD_CONTROLE_VEX_MASTER="+iPkCodControleVexMaster;
					rs2 = stmt2.executeQuery(sCmd2);
					while (rs2.next())
					{
						int iPkCodControleVexDetail=rs2.getInt("PK_COD_CONTROLE_VEX_DETAIL");
						String codRetorno          =rs2.getString("COD_RETORNO");
						if (iPkCodControleVexDetail==iPkCodControleVexDetailRet) continue;
						if (codRetorno.trim().equals("99"))                      continue;
						
						iContador++;
					}
					if (iContador>1) 
						System.out.println("Achei");
					
				}
			} catch (SQLException e) {System.out.println("Erro==>"+e.getMessage());
			}
			rs2.close();			
			rs.close();
			stmt2.close();			
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void VerificaAutosPorStatus(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			REC.consultaAutoBean myConsult = new REC.consultaAutoBean();
			myConsult.setOrgaosEscolhidos("119100");
			myConsult.setStatusEscolhidos("002");
			myConsult.setDatReferencia(sys.Util.formatedTodayDDMMAAAA().substring(0,10));
			REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
			dao.StatusAuto(myConsult,new ACSS.UsuarioBean());

			
			
			
			
			
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	

	public void TestarThread(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
	}
		
	
	
	
	public void VerificaRequerimento(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException, DaoException {
		/*
		
		String linha="DE011000FRANCISCO           2586701258670F28598109   LPL40520000000P�A.MARCILIO DIAS EM F/AO N.56                                                  181020111207058675452600000000000000002680068         000000000000000000000000000                                        00000000                                                                      0000000000                                                            0000000000000000000000                                           0000000000000";
		String nomUsername="RAFAEL";
		String codOrgaoLotacao="119100";
		String codOrgao="119100";
		String anoLote="2011";
		String tipArq="7";
		String numLote="99999";
		String codUnidade="9999";
		String operador="RAFAEL"; 
		String datLote="00000000";
		
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);				
		String parteVar = sys.Util.rPad(nomUsername, " ", 20)
		+ sys.Util.lPad(codOrgaoLotacao,"0",6) + tipArq
		+ sys.Util.lPad(codOrgao,"0",6)	+ linha 
		+ sys.Util.lPad(anoLote, "0", 4) + sys.Util.lPad(numLote, "0", 5)
		+ sys.Util.lPad(codUnidade, "0", 4) + sys.Util.lPad(datLote, "0", 8)
		+ operador;

		
		*/
		
		
		
		
		
		Connection conn = null;
		try {
  

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			ResultSet rs = null;
			
			String sData="";
			int i=0;
			try
			{
				LogDeRecuperacao log = new LogDeRecuperacao();
				
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT SUBSTR(TXT_PARTE_VARIAVEL,1,12) AS NUM_AUTO_INFRACAO,SUBSTR(TXT_PARTE_VARIAVEL,13,7) AS NUM_PLACA,DAT_PROC as DAT_PROC FROM TSMI_LOG_BROKER WHERE DAT_PROC>to_date('18/09/2011','dd/mm/yyyy') AND "+
				"( "+
				"COD_TRANSACAO='209' OR "+ 
				"COD_TRANSACAO='329' OR "+
				"COD_TRANSACAO='336'    "+				
				") "+
				"ORDER BY DAT_PROC";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					UsuarioBeanId.setCodOrgaoAtuacao("999999");
					sData=rs.getString("DAT_PROC");
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					
					System.out.println("Atualizando..."+i+" - "+myAuto.getNumAutoInfracao()+" Data de processamento:"+sData);
					
					log.gravaMensagem("Atualizando..."+i+" - "+myAuto.getNumAutoInfracao()+" Data de processamento:"+sData);
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					
					i++;
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();					
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						if (
								(myReq.getCodTipoSolic().equals("DI")) ||
								(myReq.getCodTipoSolic().equals("1I")) ||
								(myReq.getCodTipoSolic().equals("2I")) 								
						) continue;
						
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						dao.GravarRequerimento(myReq,conn);
					}

				}
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void sincronizaRequerimentosMulta (String dataUltimaSincronizacao, ACSS.UsuarioBean UsuarioBeanId, LogDeRecuperacao log) throws SQLException, RobotException, DaoException {	
		Connection conn = null;
		try {
  

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			
			String sData="";
			int i=0;
			try
			{
				
				
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT SUBSTR(TXT_PARTE_VARIAVEL,1,12) AS NUM_AUTO_INFRACAO,SUBSTR(TXT_PARTE_VARIAVEL,13,7) AS NUM_PLACA,DAT_PROC as DAT_PROC FROM TSMI_LOG_BROKER WHERE DAT_PROC > to_date('"+dataUltimaSincronizacao+"','dd/mm/yyyy') AND "+
				"( "+
				"COD_TRANSACAO='209' OR "+ 
				"COD_TRANSACAO='329' OR "+
				"COD_TRANSACAO='336'    "+				
				") "+
				"ORDER BY DAT_PROC";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					UsuarioBeanId.setCodOrgaoAtuacao("999999");
					sData=rs.getString("DAT_PROC");
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					
					log.gravaMensagem("Sincronizando..."+i+" - "+myAuto.getNumAutoInfracao()+" Data de processamento:"+sData);
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					
					i++;
					dao.LeRequerimento(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean myReq  = new REC.RequerimentoBean();					
					while (it.hasNext())
					{
						myReq = (REC.RequerimentoBean)it.next();
						if (
								(myReq.getCodTipoSolic().equals("DI")) ||
								(myReq.getCodTipoSolic().equals("1I")) ||
								(myReq.getCodTipoSolic().equals("2I")) 								
						) continue;
						
						dao.LeAutoInfracaoLocal(myAuto,"auto");
						if (sys.Util.DifereDias("01/10/2005",myReq.getDatRequerimento())<0)
							myReq.setSitProtocolo("1");
						dao.GravarRequerimento(myReq,conn);
					}

				}
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void incrementaTabelaTgerReqResultado (String dataUltimaSincronizacao, ACSS.UsuarioBean UsuarioBeanId, LogDeRecuperacao log) throws SQLException, RobotException, DaoException {	
		Connection conn = null;
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			
			Statement stmtResultado = conn.createStatement();
			ResultSet rsResultado = null;
			
			String sData="";
			
			try
			{				
				
				String sCmd = "SELECT C.COD_UF as COD_UF, D.COD_ORGAO as COD_ORGAO,	a.nom_orgao as TXT_ORGAO,b.cod_tipo_solic as COD_TIPO_SOLIC, " +
						"B.COD_STATUS_REQUERIMENTO as COD_STATUS_REQUERIMENTO, TO_DATE(b.dat_rs, 'DD/MM/YY') as DAT_RESULTADO,	" +
						"sum(nvl(decode(b.cod_result_rs, 'D', 1),0)) as QTD_DEFERIDOS, " +
						"sum(nvl(decode(b.cod_result_rs, 'I', 1),0)) as QTD_INDEFERIDOS, D.COD_INFRACAO as COD_INFRACAO, D.DSC_INFRACAO as TXT_INFRACAO, D.DSC_ENQUADRAMENTO as TXT_ARTIGO "
				+"FROM SMIT.TSMI_ORGAO A, SMIT.TSMI_REQUERIMENTO B, SMIT.TSMI_MUNICIPIO C,	SMIT.TSMI_AUTO_INFRACAO D"
				+" WHERE B.DAT_RS >= to_date('"+dataUltimaSincronizacao+"','dd/mm/yyyy') AND A.COD_ORGAO = D.COD_ORGAO AND C.COD_MUNICIPIO = D.COD_MUNICIPIO AND B.COD_AUTO_INFRACAO = D.COD_AUTO_INFRACAO " +
				"GROUP BY c.COD_UF,	D.COD_ORGAO, a.nom_orgao, b.cod_tipo_solic, b.cod_status_requerimento, TO_DATE(b.dat_rs, 'DD/MM/YY'), D.COD_INFRACAO, D.DSC_INFRACAO,D.DSC_ENQUADRAMENTO ";

				rs = stmt.executeQuery(sCmd);
				
				while (rs.next())
				{
					
					RequerimentoResultado requerimentoResultado = new RequerimentoResultado();
					requerimentoResultado.setCodigoUf(rs.getString("COD_UF"));
					requerimentoResultado.setCodigoOrgao(rs.getString("COD_ORGAO"));
					requerimentoResultado.setTextoOrgao(rs.getString("TXT_ORGAO"));
					requerimentoResultado.setCodigoTipoSolicitacao(rs.getString("COD_TIPO_SOLIC"));
					requerimentoResultado.setCodigoStatusRequerimento(rs.getString("COD_STATUS_REQUERIMENTO"));
					requerimentoResultado.setDataResultado(rs.getDate("DAT_RESULTADO"));
					requerimentoResultado.setQuantidadeDeferidos(rs.getInt("QTD_DEFERIDOS"));
					requerimentoResultado.setQuantidadeIndeferidos(rs.getInt("QTD_INDEFERIDOS"));
					requerimentoResultado.setCodigoInfracao(rs.getString("COD_INFRACAO"));
					requerimentoResultado.setTextoInfracao(rs.getString("TXT_INFRACAO"));
					requerimentoResultado.setTextoArtigo(rs.getString("TXT_ARTIGO"));
					
					
					
					String sCmdResultado = "select COD_TIPO_SOLIC,COD_INFRACAO,DAT_RESULTADO " +
							"FROM TGER_REQ_RESULTADO " +
							"WHERE COD_TIPO_SOLIC = '"+requerimentoResultado.getCodigoTipoSolicitacao() +" ' AND COD_INFRACAO = '"+requerimentoResultado.getCodigoInfracao() +" ' AND DAT_RESULTADO = '"+requerimentoResultado.getDataResultado()+" '";
					
					rsResultado = stmtResultado.executeQuery(sCmdResultado);
					if (!rsResultado.next()){
						
						
						String sCmdInsert = "INSERT INTO SMIT.TGER_REQ_RESULTADO (COD_UF,COD_ORGAO,TXT_ORGAO,COD_TIPO_SOLIC,COD_STATUS_REQUERIMENTO,DAT_RESULTADO,QTD_DEFERIDOS,QTD_INDEFERIDOS,COD_INFRACAO,TXT_INFRACAO,TXT_ARTIGO) " +
								"VALUES ('"+requerimentoResultado.getCodigoUf()+"'," +
										"'"+requerimentoResultado.getCodigoOrgao()+"'," +
												"'"+requerimentoResultado.getTextoOrgao()+"'," +
												"'"+requerimentoResultado.getCodigoTipoSolicitacao()+"'," +
												"'"+requerimentoResultado.getCodigoStatusRequerimento()+"'," +
												"'"+requerimentoResultado.getDataResultado()+"'," +
												""+requerimentoResultado.getQuantidadeDeferidos()+"," +
												""+requerimentoResultado.getQuantidadeIndeferidos()+"," +
												"'"+requerimentoResultado.getCodigoInfracao()+"'," +
												"'"+requerimentoResultado.getTextoInfracao()+"'," +
												"'"+requerimentoResultado.getTextoArtigo()+"')";
						stmtResultado.execute(sCmdInsert);
						
						log.gravaMensagem("Inserindo :" + requerimentoResultado.getCodigoInfracao() +" Data Resultado: "+requerimentoResultado.getDataResultado() );
						
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			rsResultado.close();
			stmtResultado.close();
			
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void VerificaLogUsuario(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			Statement stmt3 = conn.createStatement();			
			ResultSet rs = null;
			ResultSet rs2 = null;
			String sUsuario="";
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT * FROM MICHEL0101";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					sUsuario=rs.getString("NOM_USERNAME");
					String sComando2="SELECT DAT_OPERACAO FROM TCAU_LOG_ACESSO WHERE NOM_USERNAME='"+sUsuario+"'";
					rs2 = stmt2.executeQuery(sComando2);
					while (rs2.next()) {
						i++;
						String sDataOperacao=converteData(rs2.getDate("DAT_OPERACAO"));
						System.out.println(i+ " Usuario:"+sUsuario+" "+sDataOperacao);
						String sComando3="INSERT INTO MICHEL0103 (NOM_USERNAME,DAT_OPERACAO) VALUES ('"+sUsuario+"','"+sDataOperacao+"')";
						stmt3.executeQuery(sComando3);
					}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			rs2.close();
			stmt.close();
			stmt2.close();
			stmt3.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	

	public void VerificaEMITEVEX(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
		
						
			int i=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT * FROM LIN"; 
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					
					System.out.println("Atualizando..."+i+" ("+myAuto.getNumAutoInfracao()+") "+" ("+myAuto.getNumPlaca()+")");
					UsuarioBeanId.setCodOrgaoAtuacao("999999");
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					System.out.println("Atualizando..."+i);
					
					sCmd="UPDATE lin SET cod_status_auto='"+myAuto.getCodStatus()+"', "+
					"cod_infracao='"+myAuto.getInfracao().getCodInfracao()+"',"+
					"ind_fase='"+myAuto.getIndFase()+"', "+
					"pago='"+myAuto.getIndPago()+"' "+
					"WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"'";
					
					stmt2.executeQuery(sCmd);
					i++;
					
					
					
				}
				
			} catch (SQLException e) {
			}
			stmt2.close();
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void importaBaseDados(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs    = null;
			ResultSet rs2   = null;
			int i=0;
			try
			{

				String sCmd  = "SELECT * FROM TAB_TAB02 ";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sNome        = rs.getString("NOME");
					if (sNome==null)    sNome="";
				    sNome=sNome.replaceAll("'","");
					
				    String sEndereco     = rs.getString("ENDERECO");
				    if (sEndereco==null) sEndereco="";
				    sEndereco=sEndereco.replaceAll("'","");
				    
				    
				    String sNumero       = rs.getString("NUMERO");
				    if (sNumero==null)   sNumero="";
				    
				    String sBairro       = rs.getString("BAIRRO");
				    if (sBairro==null)   sBairro="";
				    
				    String sCidade       = rs.getString("CIDADE");
				    if (sCidade==null)   sCidade="";
				    
				    String sEstado       = rs.getString("ESTADO");
				    if (sEstado==null)   sEstado="";
				    
				    String sCep          = rs.getString("CEP");
				    if (sCep==null)      sCep="";
				    
				    String sCodigo       = "";
				    
				    /*CODIGO*/
				    String sCmd2="SELECT SEQ_DDE_PES_PESSOA.NEXTVAL as CODIGO FROM DUAL";
					rs2 = stmt2.executeQuery(sCmd2);
					if (rs2.next())
					  sCodigo=rs2.getString("CODIGO");
				    
					/*TDDE_PES_PESSOA*/
				    sCmd2 = "INSERT INTO TDDE_PES_PESSOA (ID_PESSOA,NOM_PESSOA,TIP_PESSOA,IND_ATIVO) VALUES ("+
				    "'"+sCodigo+"','"+sNome+"','F','S')";
				    stmt2.executeQuery(sCmd2);
				    
				    /*TDDE_PES_PESSOA_FISICA*/
				    sCmd2 = "INSERT INTO TDDE_PES_PESSOA_FISICA (ID_PESSOA,IND_ACIDENTE,IND_ACIDENTE_SEQUELA, "+
				            "IND_MULTIPLICADOR) VALUES ("+
				    "'"+sCodigo+"','N','N','N')";
				    stmt2.executeQuery(sCmd2);

				    /*TDDE_PES_ENDERECO*/
				    sCmd2 = "INSERT INTO TDDE_PES_ENDERECO (ID_ENDERECO,ID_PESSOA,ID_TIPO_ENDERECO,DES_LOGRADOURO_EXT, "+
				            "NUM_ENDERECO,DES_COMPLEMENTO,DES_CIDADE_EXT,COD_UF_EXT,NUM_CEP_EXT) VALUES ("+
				    "SEQ_DDE_PES_ENDERECO.NEXTVAL,'"+sCodigo+"','2','"+sEndereco+"','"+
				    sNumero+"','"+sBairro+"','"+sCidade+"','"+sEstado+"','"+sCep+"')";
				    stmt2.executeQuery(sCmd2);
				    if (i>611)
				    	System.out.println("Parei");
				    	
				    i++;
		
				    System.out.println("Inserindo Registro : "+i);
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			rs2.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void DeletarLogBroker(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt         = conn.createStatement();
			Statement stmt2        = conn.createStatement();
			Statement stmtVerifica = conn.createStatement();
			String sCmd          = "";
			String sCmd2         = "";
			String sCmd_Verifica = "";
			ResultSet rs         = null;
			ResultSet rsVerifica = null;
			int i=0,idel=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd  = "SELECT " +
				"to_char(DAT_PROC, 'DD/MM/YYYY HH24:MI:SS') AS DAT_PROC,"+
				"COD_TSMI_LOG_BROKER,"+
				"COD_TRANSACAO,"+          
				"TXT_PARTE_FIXA,"+         
				"TXT_PARTE_VARIAVEL,"+     
				"TXT_RESULTADO,"+          
				"COD_RETORNO,"+            
				"IND_INC,"+                
				"NUM_TRANSACAO,"+          
				"to_char(DAT_ENVIO_REQ, 'DD/MM/YYYY HH24:MI:SS') AS DAT_ENVIO_REQ,"+
				"to_char(DAT_RETORNO_REQ, 'DD/MM/YYYY HH24:MI:SS') AS DAT_RETORNO_REQ,"+
				"to_char(DAT_ENVIO_BROKER, 'DD/MM/YYYY HH24:MI:SS') AS DAT_ENVIO_BROKER,"+
				"to_char(DAT_RETORNO_BROKER, 'DD/MM/YYYY HH24:MI:SS') AS DAT_RETORNO_BROKER,"+
				"COD_ORGAO_LOTACAO,"+      
				"COD_ORGAO_ATUACAO,"+      
				"NOM_USERNAME "+     
				"FROM TSMI_LOG_BROKER " +
				"WHERE trunc(dat_proc)< to_date('01/04/2011','dd/mm/yyyy')";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					i++;
					System.out.println("Atualizando..."+i);
					String sCampo01=rs.getString("DAT_PROC");            
					String sCampo02=rs.getString("COD_TSMI_LOG_BROKER");                    
					String sCampo03=rs.getString("COD_TRANSACAO");                          
					String sCampo04=rs.getString("TXT_PARTE_FIXA");                         
					String sCampo05=rs.getString("TXT_PARTE_VARIAVEL");                     
					String sCampo06=rs.getString("TXT_RESULTADO");                          
					String sCampo07=rs.getString("COD_RETORNO");                            
					String sCampo08=rs.getString("IND_INC");                                
					String sCampo09=rs.getString("NUM_TRANSACAO");                          
					String sCampo10=rs.getString("DAT_ENVIO_REQ");                          
					String sCampo11=rs.getString("DAT_RETORNO_REQ");                        
					String sCampo12=rs.getString("DAT_ENVIO_BROKER");                       
					String sCampo13=rs.getString("DAT_RETORNO_BROKER");                     
					String sCampo14=rs.getString("COD_ORGAO_LOTACAO");
					String sCampo15=rs.getString("COD_ORGAO_ATUACAO");
					String sCampo16=rs.getString("NOM_USERNAME");

					if (sCampo01==null) sCampo01="";					
					if (sCampo02==null) sCampo02="";
					if (sCampo03==null) sCampo03="";
					if (sCampo04==null) sCampo04="";
					if (sCampo05==null) sCampo05="";
					if (sCampo06==null) sCampo06="";
					if (sCampo07==null) sCampo07="";
					if (sCampo08==null) sCampo08="";
					if (sCampo09==null) sCampo09="";
					if (sCampo10==null) sCampo10="";
					if (sCampo11==null) sCampo11="";					
					if (sCampo12==null) sCampo12="";
					if (sCampo13==null) sCampo13="";
					if (sCampo14==null) sCampo14="";
					if (sCampo15==null) sCampo15="";
					if (sCampo16==null) sCampo16="";
					
					
					
					sCmd_Verifica="SELECT * FROM TSMI_LOG_BROKER_BKP_2011@SMITPBKP WHERE COD_TSMI_LOG_BROKER='"+sCampo02+"'";
					rsVerifica = stmtVerifica.executeQuery(sCmd_Verifica);
					
					if (rsVerifica.next()) {
						sCmd2="DELETE FROM TSMI_LOG_BROKER WHERE COD_TSMI_LOG_BROKER='"+sCampo02+"'";
						stmtVerifica.executeQuery(sCmd2);
						continue;
					}

					
					sCmd2="INSERT INTO SMIT.TSMI_LOG_BROKER_BKP_2011@SMITPBKP (" +
					"DAT_PROC,"+                       
					"COD_TSMI_LOG_BROKER,"+            
					"COD_TRANSACAO,"+                  
					"TXT_PARTE_FIXA,"+                 
					"TXT_PARTE_VARIAVEL,"+             
					"TXT_RESULTADO,"+                  
					"COD_RETORNO,"+                    
					"IND_INC,"+                        
					"NUM_TRANSACAO,"+                  
					"DAT_ENVIO_REQ,"+                  
					"DAT_RETORNO_REQ,"+                
					"DAT_ENVIO_BROKER,"+               
					"DAT_RETORNO_BROKER,"+             
					"COD_ORGAO_LOTACAO,"+              
					"COD_ORGAO_ATUACAO,"+              
					"NOM_USERNAME) VALUES ("+
					"TO_DATE('"+sCampo01+"','DD/MM/YYYY HH24:MI:SS'),"+
					"'"+sCampo02+"',"+
					"'"+sCampo03+"',"+
					"'"+sCampo04+"',"+
					"'"+sCampo05+"',"+
					"'"+sCampo06+"',"+
					"'"+sCampo07+"',"+
					"'"+sCampo08+"',"+
					"'"+sCampo09+"',"+
					"TO_DATE('"+sCampo10+"','DD/MM/YYYY HH24:MI:SS'),"+
					"TO_DATE('"+sCampo11+"','DD/MM/YYYY HH24:MI:SS'),"+
					"TO_DATE('"+sCampo12+"','DD/MM/YYYY HH24:MI:SS'),"+
					"TO_DATE('"+sCampo13+"','DD/MM/YYYY HH24:MI:SS'),"+
					"'"+sCampo14+"',"+
					"'"+sCampo15+"',"+					
					"'"+sCampo16+"')";
					stmt2.executeQuery(sCmd2);
					
					sCmd2="DELETE FROM TSMI_LOG_BROKER WHERE COD_TSMI_LOG_BROKER='"+sCampo02+"'";
					stmt2.executeQuery(sCmd2);
					conn.commit();
					
					
					

				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs.close();
			rsVerifica.close();
			stmt2.close();
			stmtVerifica.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}


	
	public void DeletarBD(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt         = conn.createStatement();
			Statement stmt2        = conn.createStatement();
			Statement stmtVerifica = conn.createStatement();
			String sCmd          = "";
			String sCmd2         = "";
			String sCmd_Verifica = "";
			ResultSet rs         = null;
			ResultSet rsVerifica = null;
			int i=0,idel=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd  = "SELECT * FROM TCAU_LOG_ACESSO WHERE ROWNUM<=10000 ORDER BY DAT_OPERACAO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					i++;
					System.out.println("Atualizando..."+i);
					String sCodigoAcesso=rs.getString("COD_ACESSO");            
					
					sCmd2="DELETE FROM TCAU_LOG_ACESSO WHERE COD_ACESSO="+sCodigoAcesso;
					stmtVerifica.executeQuery(sCmd2);
					conn.commit();

				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			rs.close();
			rsVerifica.close();
			stmt2.close();
			stmtVerifica.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}


	
	public void analiseEMITEVEXAR1(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {

		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt         = conn.createStatement();
			Statement stmt2        = conn.createStatement();
			Statement stmtVerifica = conn.createStatement();
			String sCmd          = "";
			String sCmd2         = "";
			String sCmd_Verifica = "";
			ResultSet rs         = null;
			ResultSet rsVerifica = null;
			int i=0,idel=0;
			String numeroAR_AvisoAutuacao="";
			String numeroAR_Autuacao="";
			String numeroAR_AvisoPenalidade="";
			String numeroAR_Penalidade="";


			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd  = "SELECT T1.DAT_RECEBIMENTO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM TSMI_ARQUIVO_RECEBIDO@SMITPBKP T1,TSMI_LINHA_ARQUIVO_REC@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('19/07/2004','DD/MM/YYYY') AND "+
						"TO_DATE('30/07/2012','DD/MM/YYYY') "+						
						"ORDER BY DAT_RECEBIMENTO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					i++;
					String codOrgao=rs.getString("COD_ORGAO");            
					String numAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");                    
					String numNotificacao=rs.getString("NUM_NOTIFICACAO");                          
					String dscLinha=rs.getString("DSC_LINHA_ARQ_REC"); if (dscLinha==null) dscLinha="";
					if (dscLinha.trim().length()<572)
						continue;
					

					/*
					System.out.println("Atualizando... N�mero notifica��o:"+numNotificacao+" Data envio:"+datEnvio);
					*/					
					String tipoNotificacao=dscLinha.substring(571,572);
					numeroAR_AvisoAutuacao   = "";
					numeroAR_Autuacao        = "";
					numeroAR_AvisoPenalidade = "";
					numeroAR_Penalidade      = "";



					sCmd_Verifica="SELECT * FROM TAB_EMITEVEX_AR WHERE COD_ORGAO='"+codOrgao+"' AND "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
							"NUM_NOTIFICACAO='"+numNotificacao+"'";
					rsVerifica = stmtVerifica.executeQuery(sCmd_Verifica);

					if (!rsVerifica.next()) {
						sCmd2="INSERT INTO TAB_EMITEVEX_AR (" +
								"COD_ORGAO,"+    
								"NUM_AUTO_INFRACAO,"+            
								"NUM_NOTIFICACAO,"+                  
								"TIPO_NOTIFICACAO) VALUES ("+                 
								"'"+codOrgao+"',"+
								"'"+numAutoInfracao+"',"+
								"'"+numNotificacao+"',"+
								"'"+tipoNotificacao+"')";
						stmt2.executeQuery(sCmd2);
					}
					else
					{
						numeroAR_AvisoAutuacao   = rsVerifica.getString("NUM_NOTIFICACAO_AUT_AVISO");
						numeroAR_Autuacao        = rsVerifica.getString("NUM_NOTIFICACAO_AUT");
						numeroAR_AvisoPenalidade = rsVerifica.getString("NUM_NOTIFICACAO_PEN_AVISO");
						numeroAR_Penalidade      = rsVerifica.getString("NUM_NOTIFICACAO_AUT");
						
					}


					sCmd_Verifica="SELECT * FROM TSMI_AR_DIGITALIZADO WHERE "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
							"NUM_NOTIFICACAO='"+numNotificacao+"'";
					rsVerifica = stmtVerifica.executeQuery(sCmd_Verifica);
					if (rsVerifica.next()) {
						{
							if (tipoNotificacao.equals("1")) numeroAR_AvisoAutuacao=numNotificacao;
							if (tipoNotificacao.equals("2")) numeroAR_Autuacao=numNotificacao;
							if (tipoNotificacao.equals("3")) numeroAR_AvisoPenalidade=numNotificacao;
							if (tipoNotificacao.equals("4")) numeroAR_Penalidade=numNotificacao;
							sCmd2="UPDATE TAB_EMITEVEX_AR SET " +
									"NUM_NOTIFICACAO_AUT_AVISO='"+numeroAR_AvisoAutuacao+"'," +
									"NUM_NOTIFICACAO_AUT      ='"+numeroAR_Autuacao+"'," +
									"NUM_NOTIFICACAO_PEN_AVISO='"+numeroAR_AvisoPenalidade+"'," +
									"NUM_NOTIFICACAO_PEN      ='"+numeroAR_Penalidade+"', " +
									"IND_AR                   ='S' "+
									"WHERE COD_ORGAO='"+codOrgao+"' AND "+
									"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
									"NUM_NOTIFICACAO='"+numNotificacao+"'";

							stmt2.executeQuery(sCmd2);

						}
					}
				}

			} catch (Exception e) {
				System.out.println("Comando1:"+sCmd2+" Verifica:"+sCmd_Verifica);
				e.printStackTrace();
			}
			rs.close();
			rsVerifica.close();
			stmt2.close();
			stmtVerifica.close();
			stmt.close();
			return;

		} catch (Exception e) {
			throw new RobotException(e.getMessage());

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}

	
	public void analiseEMITEVEXAR(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {

		Connection conn      = null;
		String sCmd          = "";
		String sCmd2         = "";
		String sCmd_Verifica = "";

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt         = conn.createStatement();
			Statement stmt2        = conn.createStatement();
			Statement stmtVerifica = conn.createStatement();
			ResultSet rs         = null;
			ResultSet rsVerifica = null;
			int i=0,idel=0;
			String numeroAR_AvisoAutuacao="";
			String numeroAR_Autuacao="";
			String numeroAR_AvisoPenalidade="";
			String numeroAR_Penalidade="";


			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd  = "SELECT TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO@SMITPBKP T1,TSMI_LINHA_ARQUIVO_REC@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('24/11/2006','DD/MM/YYYY') AND "+
						"TO_DATE('31/12/2006','DD/MM/YYYY') "+						
						"ORDER BY DAT_RECEBIMENTO";
				
				/*SEM P59*/
				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP T1,TSMI_LINHA_ARQUIVO_REC_BKP@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('24/11/2006','DD/MM/YYYY') AND "+
						"TO_DATE('31/12/2006','DD/MM/YYYY') AND IND_P59 IS NULL "+						
						"ORDER BY DAT_RECEBIMENTO";
				

				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP T1,TSMI_LINHA_ARQUIVO_REC_BKP@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('29/12/2006','DD/MM/YYYY') AND "+
						"TO_DATE('31/12/2008','DD/MM/YYYY') AND IND_P59 IS NULL "+						
						"ORDER BY DAT_RECEBIMENTO";

				
				/*BUSCA FINAL PEGAR TODOS SEM P59*/
				
				
				
				
				/*TERCEIRO*/
				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('19/07/2004','DD/MM/YYYY') AND "+
						"TO_DATE('30/07/2012','DD/MM/YYYY')  "+						
						"ORDER BY DAT_RECEBIMENTO";   /*ORIGINAL - RODAR NOVAMENTE*/
				
				

				
				
				/*PRIMEIRO - FIZ*/
				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC_BKP2009@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('30/12/2009','DD/MM/YYYY') AND "+
						"TO_DATE('30/07/2012','DD/MM/YYYY') "+						
						"ORDER BY DAT_RECEBIMENTO";   /*ORIGINAL*/

				
				/*TERCEIRO - FIZ*/
				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('25/06/2012','DD/MM/YYYY') AND "+
						"TO_DATE('30/07/2012','DD/MM/YYYY')  "+						
						"ORDER BY DAT_RECEBIMENTO";   /*ORIGINAL - RODAR NOVAMENTE*/
				
				
				/*SEGUNDO*/
				sCmd  = "SELECT T1.IND_P59 AS IND_P59,TO_CHAR(T1.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_ENVIO,T2.COD_ORGAO,T2.NUM_AUTO_INFRACAO,T2.NUM_NOTIFICACAO,T2.DSC_LINHA_ARQ_REC FROM " +
						"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC_BKP2008@SMITPBKP T2 "+
						"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND T1.COD_IDENT_ARQUIVO='EMITEVEX' AND "+ 
						"T1.COD_STATUS='2' AND "+
						"T1.DAT_RECEBIMENTO between TO_DATE('01/06/2008','DD/MM/YYYY') AND "+
						"TO_DATE('30/07/2012','DD/MM/YYYY') "+						
						"ORDER BY DAT_RECEBIMENTO";   /*ORIGINAL*/
				
				
				
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					i++;
					
					String indP59          = rs.getString("IND_P59");
					if (indP59==null) indP59="";
					
					if (indP59.trim().length()>0) {
						System.out.println("Achei Portaria P59");
						//break;
					}
					
					
					
					String codOrgao        = rs.getString("COD_ORGAO");            
					String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO");                    
					
					String numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					if (numNotificacao==null) numNotificacao="";
					numNotificacao=numNotificacao.trim();
					if (numNotificacao.length()==0) 
						continue; 
					
					

					
					
					String dscLinha        = rs.getString("DSC_LINHA_ARQ_REC"); if (dscLinha==null) dscLinha="";
					String datEnvio        = rs.getString("DAT_ENVIO");
					if (dscLinha.trim().length()<572)
						continue;
					
					if (dscLinha.trim().length()>679) 
						System.out.println("Linhas Diferentes");
						

					System.out.println("Atualizando... N�mero notifica��o:"+numNotificacao+" Data envio:"+datEnvio);					
					String tipoNotificacao=dscLinha.substring(571,572);
					tipoNotificacao=dscLinha.substring(479,480);
					
					
					numeroAR_AvisoAutuacao   = "";
					numeroAR_Autuacao        = "";
					numeroAR_AvisoPenalidade = "";
					numeroAR_Penalidade      = "";



					sCmd_Verifica="SELECT * FROM TAB_EMITEVEX_AR WHERE COD_ORGAO='"+codOrgao+"' AND "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
							"NUM_NOTIFICACAO='"+numNotificacao+"'";
					rsVerifica = stmtVerifica.executeQuery(sCmd_Verifica);

					if (!rsVerifica.next()) {
						sCmd2="INSERT INTO TAB_EMITEVEX_AR (" +
								"COD_ORGAO,"+    
								"NUM_AUTO_INFRACAO,"+            
								"NUM_NOTIFICACAO,"+                  
								"TIPO_NOTIFICACAO,DAT_ENVIO) VALUES ("+                 
								"'"+codOrgao+"',"+
								"'"+numAutoInfracao+"',"+
								"'"+numNotificacao+"',"+
								"'"+tipoNotificacao+"',"+
								"TO_DATE('"+datEnvio+"','DD/MM/YYYY'))";
						stmt2.executeQuery(sCmd2);
					}


					/*
					sCmd_Verifica="SELECT TO_CHAR(DAT_RETORNO,'DD/MM/YYYY') AS DAT_RETORNO FROM TSMI_AR_DIGITALIZADO WHERE "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
							"NUM_NOTIFICACAO='"+numNotificacao+"'";
							*/
					sCmd_Verifica="SELECT TO_CHAR(DAT_DIGITALIZACAO,'DD/MM/YYYY') AS DAT_DIGITALIZACAO, " +
							             "TO_CHAR(DAT_RETORNO,'DD/MM/YYYY') AS DAT_RETORNO "+
							             "FROM TSMI_AR_DIGITALIZADO WHERE "+
							"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND "+
							"NUM_NOTIFICACAO='"+numNotificacao+"'";
					
					rsVerifica = stmtVerifica.executeQuery(sCmd_Verifica);
					if (rsVerifica.next()) {
						{
							String datDigitalizacao=rsVerifica.getString("DAT_DIGITALIZACAO");
							String datRetorno=rsVerifica.getString("DAT_RETORNO");
							if (datRetorno==null) datRetorno="";
							
							sCmd2="UPDATE TAB_EMITEVEX_AR SET " +
				                                         "DAT_DIGITALIZACAO = TO_DATE('"+datDigitalizacao+"','DD/MM/YYYY'), " +
									                     "DAT_RETORNO       = TO_DATE('"+datRetorno+"','DD/MM/YYYY'), " +
				                            			 "IND_AR            = 'S' "+
									                     "WHERE COD_ORGAO   = '"+codOrgao+"' AND "+
									                     "NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' AND "+
									                     "NUM_NOTIFICACAO   = '"+numNotificacao+"'";
							stmt2.executeQuery(sCmd2);

						}
					}
				}

			} catch (Exception e) {
				System.out.println("Comando1:"+sCmd2+" Verifica:"+sCmd_Verifica);
				e.printStackTrace();
			}
			System.out.println("Terminei");
			
			rs.close();
			rsVerifica.close();
			stmt2.close();
			stmtVerifica.close();
			stmt.close();
			return;

		} catch (Exception e) {
			System.out.println("Comando1:"+sCmd+" Comando 2:"+sCmd2+" Verifica:"+sCmd_Verifica);
			throw new RobotException(e.getMessage());

		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}

	

	public void DeletarAutoInfracao(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs = null;
			int i=0,idel=0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TSMI_AUTO_INFRACAO WHERE DAT_INFRACAO IS NULL AND ROWNUM<=100000 "+
				"ORDER BY COD_AUTO_INFRACAO";
				
				sCmd  = "SELECT * FROM TSMI_AUTO_INFRACAO WHERE (COD_ORGAO=119100 OR COD_ORGAO=119200) AND ROWNUM<=500000 "+
				"ORDER BY COD_AUTO_INFRACAO";

				while (true) {
				
				sCmd  = "SELECT * FROM TSMI_AUTO_INFRACAO WHERE (COD_ORGAO=119100 OR COD_ORGAO=119200) AND ROWNUM<=500000 "+
				"ORDER BY COD_AUTO_INFRACAO";
				
				REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					i++;
					myAuto.setCodAutoInfracao(rs.getString("COD_AUTO_INFRACAO"));					
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					dao.LeRequerimentoLocal(myAuto,conn);
					System.out.println("Atualizando..."+i+" codAutoInfracao:"+myAuto.getCodAutoInfracao()+" Deletados..."+idel);
					if (myAuto.getRequerimentos().size()==0) {
						idel++;
						String sCmd2="DELETE FROM TSMI_HISTORICO_AUTO WHERE "+
						"COD_AUTO_INFRACAO='"+myAuto.getCodAutoInfracao()+"'";
						stmt2.executeQuery(sCmd2);
						
						
						sCmd2="DELETE FROM TSMI_REQUERIMENTO WHERE "+
						"COD_AUTO_INFRACAO='"+myAuto.getCodAutoInfracao()+"'";
						stmt2.executeQuery(sCmd2);

						
						sCmd2="DELETE FROM TSMI_AUTO_INFRACAO WHERE "+
						"COD_AUTO_INFRACAO='"+myAuto.getCodAutoInfracao()+"'";
						stmt2.executeQuery(sCmd2);
					}
				}
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt2.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void VerificaGuia(String sDataProcIni,String sDataProcFim,ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			String sTransacao="'208','328','335'";
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "select DISTINCT substr(txt_parte_variavel,1,12) as NUM_AUTO, "+
				"substr(txt_parte_variavel,20,6) as cod_orgao, "+
				"substr(txt_parte_variavel,13,7) as placa "+
				"from tsmi_log_broker where "+
				" DAT_PROC>=to_date('20/10/2005','dd/mm/yyyy') and  "+
				" DAT_PROC<=to_date('22/10/2005','dd/mm/yyyy') and  "+
				" COD_TRANSACAO IN ("+sTransacao+")";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO"));
					myAuto.setNumPlaca(rs.getString("PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					String sCmd2="UPDATE TSMI_ITEM_LOTE_RELATOR SET "+
					"DAT_INFRACAO=to_date('"+myAuto.getDatInfracao()+"','dd/mm/yyyy'),"+
					"DSC_INFRACAO='"+myAuto.getInfracao().getDscInfracao()+"',"+
					"DSC_LOCAL_INFRACAO='"+myAuto.getDscLocalInfracao()+"', "+
					"NOM_PROPRIETARIO='"+myAuto.getProprietario().getNomResponsavel()+"', "+
					"DSC_ENQUADRAMENTO='"+myAuto.getInfracao().getDscEnquadramento()+"' "+
					"WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"' and dat_infracao is null";
					stmt2.executeQuery(sCmd2);
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void VerificaDatAutoInfracao(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = " SELECT NUM_AUTO_INFRACAO,NUM_PLACA FROM TSMI_AUTO_INFRACAO T1,TSMI_REQUERIMENTO T2 WHERE "+ 
				" T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO AND T1.DAT_INFRACAO IS NULL ";
				rs = stmt.executeQuery(sCmd);
				int iContador=0;
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					iContador++;
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void AcertaQTDFotos(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO "+
				"WHERE COD_IDENT_ARQUIVO='EMITEVEX' ORDER BY COD_ARQUIVO DESC";
				rs = stmt.executeQuery(sCmd);
				int iContador=0,iCodArquivo=0,iQtdFotos=0;
				while (rs.next())
				{
					iCodArquivo=rs.getInt("COD_ARQUIVO");
					iQtdFotos=0;
					sCmd = "SELECT COUNT(*) as QTD FROM "+
					"(SELECT DISTINCT NUM_AUTO_INFRACAO "+
					"FROM TSMI_LINHA_ARQUIVO_REC "+
					"WHERE COD_ARQUIVO = "+iCodArquivo+" AND COD_STATUS = 1 AND "+
					"TRIM(SUBSTR(DSC_LINHA_ARQ_REC, 540, 15)) IS NOT NULL)";
					rs2 = stmt2.executeQuery(sCmd);
					if (rs2.next())
						iQtdFotos=rs2.getInt("QTD");
					
					
					sCmd="UPDATE TSMI_ARQUIVO_RECEBIDO SET QTD_FOTO="+iQtdFotos+" WHERE "+
					"COD_ARQUIVO="+iCodArquivo;
					stmt2.executeQuery(sCmd);					
					
					iContador++;
					System.out.println("EMITEVEX : "+iCodArquivo+ " QTD Fotos :"+iQtdFotos);
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void AcertaDadosUsuarioLOG_BROKER(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			String codTransacao   = 
				"'206','326','334','236','356','364','238','204','324','208'," +
				"'328','335','209','329','336','210','330','337','214','215'," +
				"'226','350','351','236','356','364','237','239','250','261'," +
				"'381','388','263','383','390','264','384','391','352','382'," +
				"'389','400','401','402','405','408','403','406','407','410'," +
				"'411','412','413','414','415'";
			String datInicial     = "01/11/2006";
			String datFinal       = sys.Util.formatedToday().substring(0,10);
			String codLogBroker   = "";
			String sEntrada       = "";
			String sSaida         = "";
			String sCodTransacao  = "";
			
			String codOrgao       = "";
			String codOrgaoLotacao= "";
			String nomUserName    = "";
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			Statement stmt_LOG  = conn.createStatement();
			ResultSet rs = null;
			int iContador = 0;
			try
			{
				String sCmd = "SELECT * FROM TSMI_LOG_BROKER WHERE " +
				"COD_TRANSACAO in ("+codTransacao+") and "+
				"DAT_PROC>=to_date('"+datInicial+"','DD/MM/YYYY') and  "+
				"DAT_PROC<=to_date('"+datFinal+"','DD/MM/YYYY') and  "+
				"substr(TXT_RESULTADO,2,3)='000' and "+
				"COD_ORGAO_LOTACAO is NULL "+
				"ORDER BY DAT_PROC";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					iContador++;
					System.out.println("Sequencial ==>"+iContador);
					codLogBroker   = rs.getString("COD_TSMI_LOG_BROKER");
					sEntrada       = rs.getString("TXT_PARTE_VARIAVEL");
					sSaida         = rs.getString("TXT_RESULTADO");
					sCodTransacao  = rs.getString("COD_TRANSACAO");
					codOrgao       = "0";
					codOrgaoLotacao= "0";
					nomUserName    = "";
					
					
					/*Orgao
					 206,326,334-Abrir Defesa Previa
					 236,356,364-Indeferimento de Plano
					 238-Renuncia de Defesa Previa
					 204,324-Receber Notifica��o
					 208,328,335-Atualiza Junta Relator
					 209,329,336-Atualiza Resultado
					 210,330,337-Estorno de Resultado
					 214-TRI
					 215-Troca Junta Relator
					 226,350,351-Estorno Defesa Previa
					 236,356,364-Indeferimento de Plano
					 237-Estorno Indeferimento de Plano
					 239-Estorno Renuncia Defesa Previa
					 250-Atualiza Data Vencimento
					 264,384,391-Revis�o Requerimento
					 352-Estorno TRI
					 410-Atualizar Processo
					 411-Ajuste Status
					 413-Ajuste Manual Respons�vel Pontos
					 414-Ajuste CPF
					 415-Ajuste Manual do Propriet�rio
					 */
					if ("206,326,334,236,356,364,238,204,324,208,328,335,209,329,336,210,330,337,214,215,226,350,351,236,356,364,237,239,250,264,384,391,352,410,411,413,414,415".indexOf(sCodTransacao)>=0) 
						codOrgao=sEntrada.substring(19,25);
					/*261,381,388-Envio Defesa Previa DO*/
					if ("261,381,388".indexOf(sCodTransacao)>=0)
						codOrgao=sEntrada.substring(73,79);
					/*382,389-Estorno Envio Publica��o*/
					if ("382,389".indexOf(sCodTransacao)>=0)
						codOrgao=sEntrada.substring(53,59);
					/*400,401,402,405,408,407-MR1,MX1,ME1,MZ1,MC1,DOL,MOVIMENTO BANC�RIO*/
					if ("400,401,402,405,408,407".indexOf(sCodTransacao)>=0)
						codOrgao=sEntrada.substring(27,33);
					/*403-RECJARI*/					
					if ("403".indexOf(sCodTransacao)>=0)
						codOrgao=sEntrada.substring(26,32);
					
					
					/*Usu�rio
					 206,326,334-Abrir Defesa Previa
					 236,356,364-Indeferimento de Plano
					 238-Renuncia de Defesa Previa
					 208,328,335-Atualiza Junta Relator
					 209,329,336-Atualiza Resultado
					 210,330,337-Estorno de Resultado
					 214-TRI
					 215-Troca Junta Relator
					 226,350,351-Estorno Defesa Previa
					 236,356,364-Indeferimento de Plano
					 237-Estorno Indeferimento de Plano
					 239-Estorno Renuncia Defesa Previa
					 250-Atualiza Data Vencimento
					 264,384,391-Revis�o Requerimento
					 352-Estorno TRI
					 410-Atualizar Processo
					 413-Ajuste Manual Respons�vel Pontos
					 414-Ajuste CPF
					 415-Ajuste Manual do Propriet�rio
					 
					 */
					if ("206,326,334,236,356,364,238,208,328,335,209,329,336,210,330,337,214,215,226,350,351,236,356,364,237,239,250,352,410,413,414,415".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(62,82);
					/*204,324-Receber Notifica��o
					 */
					if ("204,324".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(42,62);
					/*261,381,388-Envio Defesa Previa DO*/
					if ("261,381,388".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(28,48);
					/*263,383,390-Atualiza Data Publica��o DO*/
					if ("263,383,390".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(8,28);
					/*264,384,391-Revis�o Requerimento*/
					if ("264,384,391".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(54,74);
					/*382,389-Estorno Envio Publica��o*/
					if ("382,389".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(8,28);
					/*400,401,402,405,408,406,407,412-MR1,MX1,ME1,MZ1,MC1,DOL,RECJARI,RETORNO AR VEX,MOVIMENTO BANC�RIO,AJUSTE AUTOMATICO CR DA VENDA*/
					if ("400,401,402,405,408,403,406,407,412".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(0,20);
					/*411-Ajuste Status*/
					if ("411".indexOf(sCodTransacao)>=0)
						nomUserName=sEntrada.substring(25,45);
					
					
					
					/*Lota��o
					 206,326,334-Abrir Defesa Previa
					 236,356,364-Indeferimento de Plano
					 238-Renuncia de Defesa Previa
					 208,328,335-Atualiza Junta Relator
					 209,329,336-Atualiza Resultado
					 210,330,337-Estorno de Resultado
					 214-TRI
					 215-Troca Junta Relator
					 226,350,351-Estorno Defesa Previa
					 236,356,364-Indeferimento de Plano
					 237-Estorno Indeferimento de Plano
					 239-Estorno Renuncia Defesa Previa
					 250-Atualiza Data Vencimento
					 264,384,391-Revis�o Requerimento
					 352-Estorno TRI
					 410-Atualizar Processo
					 413-Ajuste Manual Respons�vel Pontos
					 414-Ajuste CPF
					 415-Ajuste Manual do Propriet�rio
					 */
					if ("206,326,334,236,356,364,238,208,328,335,209,329,336,210,330,337,214,215,226,350,351,236,356,364,237,239,250,352,410,413,414,415".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(82,88);
					/*204,324-Receber Notifica��o
					 */
					if ("204,324".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(62,68);
					/*261,381,388-Envio Defesa Previa DO*/					
					if ("261,381,388".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(48,54);
					/*263,383,390-Atualiza Data Publica��o DO*/
					if ("263,383,390".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(28,34);
					/*264,384,391-Revis�o Requerimento*/
					if ("264,384,391".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(74,80);						
					/*382,389-Estorno Envio Publica��o*/
					if ("382,389".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(28,34);
					/*400,401,402,405,408,406,407,412-MR1,MX1,ME1,MZ1,MC1,DOL,RECJARI,RETORNO AR VEX,MOVIMENTO BANC�RIO,AJUSTE AUTOMATICO CR DA VENDA*/
					if ("400,401,402,405,408,403,406,407,412".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(20,26);
					/*411-Ajuste Status*/
					if ("411".indexOf(sCodTransacao)>=0)
						codOrgaoLotacao=sEntrada.substring(45,51);
					
					
					try
					{
						Integer.parseInt(codOrgao);
						Integer.parseInt(codOrgaoLotacao);						
					}
					catch (Exception e) 
					{
						codOrgao       ="";
						codOrgaoLotacao="";
						nomUserName    ="";
						
					}
					
					
					
					sCmd = "UPDATE TSMI_LOG_BROKER SET "+
					"COD_ORGAO_LOTACAO='"+codOrgaoLotacao+"',"+
					"COD_ORGAO_ATUACAO='"+codOrgao+"',"+
					"NOM_USERNAME='"+nomUserName+"' "+
					"WHERE "+
					"COD_TSMI_LOG_BROKER='"+codLogBroker+"'";
					stmt_LOG.executeQuery(sCmd);	
					conn.commit();
				}			    
			} catch (SQLException e) {}
			
			if (rs!=null) rs.close();
			stmt.close();
			stmt_LOG.close();
			
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void AcertaDadosCEP(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			Statement stmt_RET  = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_RET    = null;
			int iContador = 0;
			try
			{
				String sCmd = "SELECT * FROM TAB_RETORNO_06 "+
				"WHERE "+
				"(NUM_CEP IS NULL OR NUM_CEP='00000000') " +
				/*
				 "WHERE "+
				 "(NUM_CEP IS NOT NULL AND MUNICIPIO IS NULL) " +
				 */
				"ORDER BY DAT_RETORNO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sNumCep="";
					String sMunicipio="";
					String sNumAutoInfracao=rs.getString("NUM_AUTO_INFRACAO").trim();
					String sDataRetorno=rs.getString("DAT_RETORNO");
					iContador++;
					System.out.println("Executando==> "+sDataRetorno+" ("+iContador+")");				
					sCmd="SELECT NUM_CEP,MUNICIPIO FROM "+
					"TAB_NOTIFICACOES WHERE NUM_AUTO_INFRACAO='"+sNumAutoInfracao+"' AND "+
					"NUM_CEP<>'00000000'";
					rs_RET = stmt_RET.executeQuery(sCmd);
					if (rs_RET.next())
					{
						sNumCep   =rs_RET.getString("NUM_CEP");
						sMunicipio=rs_RET.getString("MUNICIPIO");
					}
					
					
					
					sCmd = "UPDATE TAB_RETORNO_06 SET "+
					"NUM_CEP='"+sNumCep+"'," +
					"MUNICIPIO='"+sMunicipio+"' " +
					"WHERE NUM_AUTO_INFRACAO='"+sNumAutoInfracao+"'";
					stmt_RET.executeQuery(sCmd);	
					conn.commit();
				}			    
			} catch (SQLException e) {}
			
			if (rs!=null) rs.close();
			stmt.close();
			stmt_RET.close();
			
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void AcertaDadosCEPUF(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			Statement stmt_RET  = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_RET    = null;
			int iContador = 0;
			try
			{
				String sCmd = "SELECT * FROM TAB_CEP ORDER BY NUM_CEP";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sNumCep=rs.getString("NUM_CEP");
					String sCodUf ="";
					iContador++;
					System.out.println("Executando==> ("+iContador+")");				
					sCmd="SELECT COD_UF FROM TAB_NOTIFICACOES WHERE "+
					"NUM_CEP='"+sNumCep+"'";
					rs_RET = stmt_RET.executeQuery(sCmd);
					if (rs_RET.next())
						sCodUf   =rs_RET.getString("COD_UF");
					
					sCmd = "UPDATE TAB_CEP SET "+
					"COD_UF='"+sCodUf+"' " +
					"WHERE NUM_CEP='"+sNumCep+"'";
					stmt_RET.executeQuery(sCmd);	
					conn.commit();
				}			    
			} catch (SQLException e) {}
			
			if (rs!=null) rs.close();
			stmt.close();
			stmt_RET.close();
			
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void AcertaDadosMUNCEP(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			Statement stmt_RET  = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_RET    = null;
			int iContador = 0;
			try
			{
				String sCmd = "SELECT SUBSTR(NUM_CEP,1,5) AS NUM_CEP FROM TAB_CEP GROUP BY SUBSTR(NUM_CEP,1,5) ORDER BY SUBSTR(NUM_CEP,1,5)";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sNumCep=rs.getString("NUM_CEP").substring(0,5);
					String sMunicipio="";
					iContador++;
					
					sCmd="SELECT MUNICIPIO FROM "+
					"TAB_RETORNO_07 WHERE NUM_CEP5='"+sNumCep+"'";
					rs_RET = stmt_RET.executeQuery(sCmd);
					if (rs_RET.next())
						sMunicipio=rs_RET.getString("MUNICIPIO");
					
					System.out.println("Executando==> "+iContador+" "+sMunicipio);				
					
					sCmd = "UPDATE TAB_CEP SET "+
					"MUNICIPIO='"+sMunicipio+"' " +
					"WHERE SUBSTR(NUM_CEP,1,5)='"+sNumCep+"'";
					stmt_RET.executeQuery(sCmd);	
					conn.commit();
				}			    
			} catch (SQLException e) {}
			
			if (rs!=null) rs.close();
			stmt.close();
			stmt_RET.close();
			
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void AcertaDadosCEP_BROKER(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			Statement stmt_RET  = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_RET    = null;
			int iContador = 0;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd = "SELECT * FROM TAB_RETORNO_07 WHERE NUM_CEP='null' ORDER BY DAT_RETORNO";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					String sNumCep="";
					String sNumAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");
					String sNumPlaca=rs.getString("NUM_PLACA");
					
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					sNumCep=myAuto.getProprietario().getEndereco().getNumCEP();
					String sDataRetorno=rs.getString("DAT_RETORNO");
					iContador++;
					System.out.println("Executando==> "+sDataRetorno+" ("+iContador+")");				
					
					
					
					sCmd = "UPDATE TAB_RETORNO_07 SET "+
					"NUM_CEP='"+sNumCep+"' WHERE NUM_AUTO_INFRACAO='"+sNumAutoInfracao+"'";
					stmt_RET.executeQuery(sCmd);	
					conn.commit();
				}			    
			} catch (SQLException e) {}
			
			if (rs!=null) rs.close();
			stmt.close();
			stmt_RET.close();
			
			return;
		} catch (Exception e) {
			System.out.println(e.getMessage());			
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	
	
	public void AcertaQTDFotosVEX(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();			
			ResultSet rs  = null;
			ResultSet rs2 = null;			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO "+
				"WHERE COD_IDENT_ARQUIVO='EMITEVEX' AND COD_STATUS='2' ORDER BY COD_ARQUIVO DESC";
				rs = stmt.executeQuery(sCmd);
				int iContador=0,iCodArquivo=0,iQtdFotos=0,iCodArquivoVEX=0;
				String sNomeArquivoOri="";
				String sNomeArquivoDes="";
				while (rs.next())
				{
					System.out.println("Entrei 1");					
					sNomeArquivoOri=rs.getString("NOM_ARQUIVO");
					sNomeArquivoDes="FOTOVEX_"+sNomeArquivoOri.substring(9);
					sNomeArquivoDes=sNomeArquivoDes.replaceAll("TXT","ZIP");
					iCodArquivo=rs.getInt("COD_ARQUIVO");					
					System.out.println("Entrei 2");					
					iQtdFotos=0;
					sCmd = "SELECT COUNT(*) as QTD FROM "+
					"(SELECT DISTINCT NUM_AUTO_INFRACAO "+
					"FROM TSMI_LINHA_ARQUIVO_REC "+
					"WHERE COD_ARQUIVO = "+iCodArquivo+" AND COD_STATUS = 1 AND "+
					"TRIM(SUBSTR(DSC_LINHA_ARQ_REC, 540, 15)) IS NOT NULL)";
					System.out.println("Entrei 3");					
					rs2 = stmt2.executeQuery(sCmd);
					if (rs2.next())
						iQtdFotos=rs2.getInt("QTD");
					
					
					sCmd  = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO "+
					"WHERE NOM_ARQUIVO='"+sNomeArquivoDes+"'";
					System.out.println("Entrei 4");					
					rs2 = stmt2.executeQuery(sCmd);
					if (rs2.next())
						iCodArquivoVEX=rs2.getInt("COD_ARQUIVO");
					
					sCmd="UPDATE TSMI_ARQUIVO_RECEBIDO SET QTD_REG="+iQtdFotos+" WHERE "+
					"COD_ARQUIVO="+iCodArquivoVEX;
					System.out.println("Entrei 5");					
					stmt2.executeQuery(sCmd);					
					
					iContador++;
					System.out.println("FOTOVEX : "+sNomeArquivoDes+ " QTD Fotos :"+iQtdFotos);
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void VerificaCodIdentOrgaoInfracao(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = " SELECT NUM_AUTO_INFRACAO,NUM_PLACA FROM TAB_NIT_MDB"; 
				rs = stmt.executeQuery(sCmd);
				int iContador=0;
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					iContador++;
					System.out.println("Auto de Infra��o : "+myAuto.getNumAutoInfracao());
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	
	
	public void AtualizaHistorico(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = " SELECT NUM_AUTO_INFRACAO,NUM_PLACA FROM TSMI_AUTO_INFRACAO WHERE "+
				" DAT_INFRACAO>=to_date('01/01/2005','dd/mm/yyyy') and "+
				" DAT_INFRACAO<=to_date('01/03/2005','dd/mm/yyyy') and DAT_INCLUSAO IS NULL "+
				" ORDER BY NUM_AUTO_INFRACAO";				               
				rs = stmt.executeQuery(sCmd);
				int iContador=0;
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					dao.LeHistorico(myAuto,UsuarioBeanId);
					Iterator it = myAuto.getHistoricos().iterator();
					REC.HistoricoBean myHis  = new REC.HistoricoBean();
					String sData="";
					while (it.hasNext())
					{
						myHis = (REC.HistoricoBean)it.next();
						if (myHis.getCodStatus().equals("0"))
							sData=myHis.getDatStatus();
						break;
					}
					
					if (sData.length()>0)
					{					
						String sCmd2="UPDATE TSMI_AUTO_INFRACAO SET "+
						"DAT_INCLUSAO=to_date('"+sData+"','dd/mm/yyyy') "+
						"WHERE NUM_AUTO_INFRACAO='"+myAuto.getNumAutoInfracao()+"' and dat_infracao is null";
						stmt2.executeQuery(sCmd2);
						iContador++;
					}
					
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	public void AtualizaHistoricoAjuste(String sDataProcIni,String sDataProcFim,ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		Connection conn = null;
		try {
			String sTransacao="'405','411','412','413','414'";		
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				String sCmd  = "select DISTINCT substr(txt_parte_variavel,1,12) as NUM_AUTO, "+
				"substr(txt_parte_variavel,20,6) as cod_orgao, "+
				"substr(txt_parte_variavel,13,7) as placa "+
				"from tsmi_log_broker where "+
				" DAT_PROC>=to_date('"+sDataProcIni+"','dd/mm/yyyy') and  "+
				" DAT_PROC<=to_date('"+sDataProcFim+"','dd/mm/yyyy') and  "+
				" COD_TRANSACAO IN ("+sTransacao+")";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO"));
					myAuto.setNumPlaca(rs.getString("PLACA"));
					
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					dao.LeHistorico(myAuto,UsuarioBeanId);
					dao.GravarHistorico(myAuto);
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	public void AtualizaHistoricoCancelamento(String sDataProcIni,String sDataProcFim,ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				String sCmd  = " SELECT NUM_AUTO_INFRACAO,NUM_PLACA FROM TSMI_AUTO_INFRACAO WHERE COD_STATUS=90 AND "+
				" DAT_STATUS>=to_date('"+sDataProcIni+"','dd/mm/yyyy') and  "+
				" DAT_STATUS<=to_date('"+sDataProcFim+"','dd/mm/yyyy') ";
				rs = stmt.executeQuery(sCmd);
				int iContador=0;
				while (rs.next())
				{
					REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
					if (dao.LeAutoInfracao(myAuto,"codigo",UsuarioBeanId)==false)
						continue;
					
					dao.GravaAutoInfracaoLocal(myAuto,conn);
					dao.LeHistorico(myAuto,UsuarioBeanId);
					dao.GravarHistorico(myAuto);
					iContador++;
				}
				
			} catch (SQLException e) {System.out.println(e.getMessage());
			}
			rs.close();
			stmt.close();
			stmt2.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	
	
	/**************************************
	 * M�todo para listar os org�os com o flag "ind_integrada" marcado, menos o org�o Detran
	 * @author Glaucio Jannotti
	 * @param O c�digo do org�o a ser excluido da busca
	 * @return Retorna um array de OrgaoBean com os orgaos encontrados
	 */
	public ACSS.OrgaoBean[] BuscarOrgaosIntegrados(String codOrgao) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " SELECT COD_ORGAO, NOM_ORGAO, SIG_ORGAO FROM TSMI_ORGAO"
				+ " WHERE IND_INTEGRADA = 'S' AND COD_ORGAO <> " + codOrgao;
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetOrgaos = new Vector();
			while (rs.next()) {
				ACSS.OrgaoBean orgao = new ACSS.OrgaoBean();
				orgao.setCodOrgao(rs.getString("COD_ORGAO"));
				orgao.setNomOrgao(rs.getString("NOM_ORGAO"));
				orgao.setSigOrgao(rs.getString("SIG_ORGAO"));
				vetOrgaos.add(orgao);
			}
			
			rs.close();
			stmt.close();
			
			ACSS.OrgaoBean[] colOrgaos = new ACSS.OrgaoBean[vetOrgaos.size()];
			vetOrgaos.toArray(colOrgaos);
			return colOrgaos;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**************************************
	 * M�todo para duplicado um arquivo TVEP para outro org�o
	 * @author Glaucio Jannotti
	 * @param auxClasse Arquivo a ser duplicado
	 * @param codOrgao �rg�o de destino do arquivo copiado
	 * @return Retorna o objeto do novo arquivo criado
	 */
	public REG.ArquivoRecebidoBean DuplicarArquivo(REG.ArquivoRecebidoBean arquivo, ACSS.OrgaoBean orgao)
	throws DaoException {
		
		//Verifica se o arquivo � TVEP
		if (!arquivo.getCodIdentArquivo().equals("TVEP")) return null;
		
		REG.ArquivoRecebidoBean arqDuplicado = null;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			
			//Gera o nome do novo arquivo
			String nomDuplicado = arquivo.getCodIdentRetornoGrv() + arquivo.getNumAnoExercicio()
			+ sys.Util.lPad(orgao.getCodOrgao(), "0", 6) + sys.Util.lPad(arquivo.getNumControleArq(), "0", 5)
			+ "." + arquivo.getSufixoRetornoGrv();
			
			//Duplica os dados do arquivo
			arqDuplicado = new REG.ArquivoRecebidoBean();
			arqDuplicado.setNomArquivo(nomDuplicado);
			arqDuplicado.setCodOrgao(orgao.getCodOrgao());
			arqDuplicado.setCodOrgaoLotacao(arquivo.getCodOrgaoLotacao());
			arqDuplicado.setSigOrgaoLotacao(arquivo.getSigOrgaoLotacao());
			arqDuplicado.setDatRecebimento(arquivo.getDatRecebimento());
			arqDuplicado.setDatProcessamentoDetran(arquivo.getDatProcessamentoDetran());
			arqDuplicado.setCodIdentArquivo(arquivo.getCodIdentArquivo());
			arqDuplicado.setNumAnoExercicio(arquivo.getNumAnoExercicio());
			arqDuplicado.setNumControleArq(arquivo.getNumControleArq());
			arqDuplicado.setCodStatus(arquivo.getCodStatus());
			arqDuplicado.setQtdReg(arquivo.getQtdReg());
			arqDuplicado.setNomUsername(arquivo.getNomUsername());
			arqDuplicado.setNumProtocolo(arquivo.getNumProtocolo());
			arqDuplicado.setIdentMovimento(arquivo.getIdentMovimento());
			arqDuplicado.setLinhaArquivoRec(arquivo.getLinhaArquivoRec());
			
			//Verifica se o arquivo j� existe
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO "
				+ "WHERE NOM_ARQUIVO = '" + arqDuplicado.getNomArquivo() + "'" + " AND COD_STATUS <> '9'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			if (rs.next()) return null;
			
			//Busca codigo do arquivo
			sCmd = "SELECT SEQ_TSMI_ARQUIVO_RECEBIDO.NEXTVAL COD_ARQUIVO FROM DUAL";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) arqDuplicado.setCodArquivo(rs.getString("cod_arquivo"));
			
			//Inseri o arquivo na tabela
			sCmd = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (COD_ARQUIVO,NOM_ARQUIVO,DAT_RECEBIMENTO,DAT_PROCESSAMENTO_DETRAN,"
				+ "COD_IDENT_ARQUIVO,NUM_ANO_EXERCICIO,NUM_CONTROLE_ARQUIVO,NUM_PROTOCOLO,QTD_REG,COD_ORGAO,"
				+ "COD_ORGAO_LOTACAO,SIG_ORGAO_LOTACAO,NOM_USERNAME,COD_STATUS,COD_IDENT_MOVIMENTO,"
				+ "IND_PRIORIDADE) VALUES ("
				+ arqDuplicado.getCodArquivo()+", "
				+ "'"+arqDuplicado.getNomArquivo()+"', "
				+ "TO_DATE('"+arqDuplicado.getDatRecebimento()+"','DD/MM/YYYY'), "
				+ "TO_DATE('"+arqDuplicado.getDatProcessamentoDetran()+"','DD/MM/YYYY'), "
				+ "'"+arqDuplicado.getCodIdentArquivo()+"', "
				+ "'"+arqDuplicado.getNumAnoExercicio()+"', "
				+ "'"+arqDuplicado.getNumControleArq()+"', "
				+ "'"+arqDuplicado.getNumProtocolo()+"', "
				+ "'"+arqDuplicado.getQtdReg()+"', "
				+ "'"+arqDuplicado.getCodOrgao()+"', "
				+ "'"+arqDuplicado.getCodOrgaoLotacao()+"', "
				+ "'"+arqDuplicado.getSigOrgaoLotacao()+"', "
				+ "'"+arqDuplicado.getNomUsername()+"', "
				+ "'"+arqDuplicado.getCodStatus()+"', "
				+ "'"+arqDuplicado.getIdentMovimento()+"', 1)";
			stmt.execute(sCmd);
			
			stmt.close();
			rs.close();
			conn.commit();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return arqDuplicado;
	}
	
	/**************************************
	 * M�todo para gerar um arquivo Zip de Fotos a partir de um EMITEVEX
	 * @author Glaucio Jannotti
	 * @param auxClasse Arquivo a ser duplicado
	 * @return Retorna o objeto do novo arquivo criado
	 */
	public REG.ArquivoRecebidoBean EmpacotarArquivo(Connection conn, REG.ArquivoRecebidoBean arquivo, int qtd) throws DaoException {
		
		//Verifica se o arquivo � EMTIEVEX
		if (!arquivo.getCodIdentArquivo().equals("EMITEVEX")) return null;
		
		REG.ArquivoRecebidoBean arqDuplicado = null;
		try {
			//Duplica os dados do arquivo
			arqDuplicado = new REG.ArquivoRecebidoBean();
			arqDuplicado.setCodIdentArquivo("FOTOVEX");
			arqDuplicado.setCodOrgao(arquivo.getCodOrgao());
			arqDuplicado.setCodOrgaoLotacao(arquivo.getCodOrgaoLotacao());
			arqDuplicado.setSigOrgaoLotacao(arquivo.getSigOrgaoLotacao());
			arqDuplicado.setDatRecebimento(sys.Util.fmtData(new Date()));
			arqDuplicado.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
			arqDuplicado.setNumAnoExercicio(arquivo.getNumAnoExercicio());
			arqDuplicado.setCodStatus("2");
			arqDuplicado.setQtdReg(String.valueOf(qtd));
			arqDuplicado.setNomUsername(arquivo.getNomUsername());
			
			//Gera o nome do novo arquivo
			String nomArquivo = arquivo.getNomArquivo().replaceAll(".TXT","");
			String sufixo = "";
			if(nomArquivo.length() > 17)
				sufixo = nomArquivo.substring(17);
			String nomDuplicado = arqDuplicado.getCodIdentRetornoGrv() + "_" + arqDuplicado.getNumAnoExercicio()+sufixo
			+ "." + arqDuplicado.getSufixoRetornoGrv();
			arqDuplicado.setNomArquivo(nomDuplicado);
			
			//Verifica se o arquivo j� existe
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO "
				+ "WHERE NOM_ARQUIVO = '" + arqDuplicado.getNomArquivo() + "'" + " AND COD_STATUS <> '9'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			if (rs.next()) return null;
			
			//Busca codigo do arquivo
			sCmd = "SELECT SEQ_TSMI_ARQUIVO_RECEBIDO.NEXTVAL COD_ARQUIVO FROM DUAL";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) arqDuplicado.setCodArquivo(rs.getString("cod_arquivo"));
			
			//Inseri o arquivo na tabela
			sCmd = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (COD_ARQUIVO,NOM_ARQUIVO,DAT_RECEBIMENTO,DAT_PROCESSAMENTO_DETRAN,"
				+ "COD_IDENT_ARQUIVO,NUM_ANO_EXERCICIO,QTD_REG,COD_ORGAO,COD_ORGAO_LOTACAO,"
				+ "SIG_ORGAO_LOTACAO,NOM_USERNAME,COD_STATUS,COD_IDENT_MOVIMENTO,IND_PRIORIDADE) VALUES ("
				+ arqDuplicado.getCodArquivo()+", "
				+ "'"+arqDuplicado.getNomArquivo()+"', "
				+ "TO_DATE('"+arqDuplicado.getDatRecebimento()+"','DD/MM/YYYY'), "
				+ "TO_DATE('"+arqDuplicado.getDatProcessamentoDetran()+"','DD/MM/YYYY'), "
				+ "'"+arqDuplicado.getCodIdentArquivo()+"', "
				+ "'"+arqDuplicado.getNumAnoExercicio()+"', "
				+ "'"+arqDuplicado.getQtdReg()+"', "
				+ "'"+arqDuplicado.getCodOrgao()+"', "
				+ "'"+arqDuplicado.getCodOrgaoLotacao()+"', "
				+ "'"+arqDuplicado.getSigOrgaoLotacao()+"', "
				+ "'"+arqDuplicado.getNomUsername()+"', "
				+ "'"+arqDuplicado.getCodStatus()+"', "
				+ "'"+arqDuplicado.getIdentMovimento()+"', 1)";
			stmt.execute(sCmd);
			
			stmt.close();
			rs.close();
			return arqDuplicado;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * -----------------------------------------------------------
	 * DAO relativo �s Tabelas que ser�o Limpas
	 * -----------------------------------------------------------
	 * @author Wellem Mello
	 * @version 1.0
	 */
	public boolean ConsultaTabelasLimpeza(Connection conn, LimpezaTabBean tab, ArrayList tabelas)
	throws DaoException {
		
		boolean existe = false;
		try {
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT NOM_TABELA,CAMPO_FILTRO,VAL_FILTRO,NOM_TABELA_PAI,CAMPO_TABELA_PAI,CAMPO_TABELA_PK "
				+ "FROM TSMI_LIMPEZA ";
			if (tab != null)
				sCmd += "WHERE NOM_TABELA_PAI = '" + tab.getNomTabela() + "'";
			else
				sCmd += "WHERE IND_ATIVO = 'S' AND NOM_TABELA_PAI IS NULL ";
			sCmd += "ORDER BY NOM_TABELA";
			
			ResultSet rs   = stmt.executeQuery(sCmd);
			while (rs.next()) {
				LimpezaTabBean bean = new LimpezaTabBean();
				bean.setNomTabela(rs.getString("NOM_TABELA"));
				bean.setCampoFiltro(rs.getString("CAMPO_FILTRO"));
				bean.setValFiltro(rs.getString("VAL_FILTRO"));
				bean.setNomTabelaPai(rs.getString("NOM_TABELA_PAI"));
				bean.setCampoTabelaPai(rs.getString("CAMPO_TABELA_PAI"));
				bean.setCampoTabelaPK(rs.getString("CAMPO_TABELA_PK"));
				bean.setPai(tab);
				tabelas.add(bean);
				existe = true;
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return existe;
	}
	
	/**
	 * -----------------------------------------------------------
	 * DAO relativo �s Tabelas que ser�o Limpas
	 * -----------------------------------------------------------
	 * @author Wellem Mello
	 * @version 1.0
	 */
	public boolean VerificaIntegridadeLimpeza(Connection conn, LimpezaTabBean tab)
	throws DaoException {
		
		boolean iguais  = true;
		try {
			Statement stmt = conn.createStatement();
			DatabaseMetaData meta = conn.getMetaData();
			ResultSet rs = meta.getColumns(null, null, tab.getNomTabela(), null);
			
			while(rs.next()) {
				boolean camposIguais = false;
				ResultSet rsBkp = meta.getColumns(null, null, tab.getNomTabela() + "_BKP", null);
				while (rsBkp.next())
				{
					if (rs.getString("COLUMN_NAME").equals(rsBkp.getString("COLUMN_NAME"))
							&& rs.getString("DATA_TYPE").equals(rsBkp.getString("DATA_TYPE"))
							&& (rs.getInt("COLUMN_SIZE")==rsBkp.getInt("COLUMN_SIZE"))) {
						camposIguais = true;
						break;
					}
				}
				rsBkp.close();
				if (!camposIguais) {
					iguais = false;
					break;
				}
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return iguais;
	}
	
	/**
	 * -----------------------------------------------------------
	 * DAO relativo �s Tabelas que ser�o Limpas
	 * -----------------------------------------------------------
	 * @author Wellem Mello
	 * @version 1.0
	 */
	public String ListaCamposLimpeza(Connection conn, LimpezaTabBean tab)
	throws DaoException {
		
		String campos = "";
		try {
			Statement stmt = conn.createStatement();
			DatabaseMetaData meta = conn.getMetaData();
			ResultSet rs = meta.getColumns(null, null, tab.getNomTabela(), null);
			
			while(rs.next()) {
				campos = campos + rs.getString("COLUMN_NAME");
				campos = campos + ",";
			}
			campos = campos.substring(0, campos.length() - 1);
			
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return campos;
	}
	
	/**
	 * -----------------------------------------------------------
	 * DAO relativo �s Tabelas que ser�o Limpas
	 * -----------------------------------------------------------
	 * @author Wellem Mello
	 * @version 1.0
	 */
	public int IncluiDadosLimpeza(Connection conn, LimpezaTabBean tab) throws DaoException {
		
		int registros = 0;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "INSERT INTO " + tab.getNomTabela() + "_BKP@SMITPBKP" + "(" + tab.getCampos(conn) + ")";
			sCmd += " SELECT " + tab.getCampos(conn) + " FROM " + tab.getNomTabela();
			
			if ((tab.getNomTabelaPai()== null) || (tab.getNomTabelaPai()== ""))
				sCmd += " WHERE " + tab.getCampoFiltro() + " < (TRUNC(SYSDATE) - " + tab.getValFiltro() + ")";
			else
				sCmd += " WHERE " + tab.getCampoTabelaPai() + " IN " + MontaClausulaLimpeza(conn, tab);
			
			registros = stmt.executeUpdate(sCmd);
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return registros;
	}
	
	public int IncluiDadosLimpezaNova(Connection conn) throws DaoException {
		String sComand="";		
		int registros = 0;
		try {
			Statement stmt     =  conn.createStatement();
			Statement stmt_bkp = conn.createStatement();			
			Statement stmt2    = conn.createStatement();
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            
            Date dataHoje = new Date();
            String dataHojeString=dataHoje.toString();
            System.out.println("Dia:("+dataHojeString.substring(8,10)+")");
            System.out.println(dataHojeString);            
            
                        
            
			String sCmd = "SELECT " +
			"TO_CHAR(DAT_PROC, 'DD/MM/YYYY') AS DAT_PROC,"+				
			"COD_TSMI_LOG_BROKER,"+    
			"COD_TRANSACAO,"+          
			"TXT_PARTE_FIXA,"+         
			"TXT_PARTE_VARIAVEL,"+     
			"TXT_RESULTADO,"+          
			"COD_RETORNO,"+            
			"IND_INC,"+                
			"NUM_TRANSACAO,"+          
			"DAT_ENVIO_REQ,"+          
			"DAT_RETORNO_REQ,"+        
			"TO_CHAR(DAT_ENVIO_BROKER, 'DD/MM/YYYY') AS DAT_ENVIO_BROKER,"+
			"TO_CHAR(DAT_RETORNO_BROKER, 'DD/MM/YYYY') AS DAT_RETORNO_BROKER,"+			
			"COD_ORGAO_LOTACAO,"+      
			"COD_ORGAO_ATUACAO,"+      
			"NOM_USERNAME"+           			
		    " FROM TSMI_LOG_BROKER WHERE trunc(dat_proc)< to_date('31/12/2010','dd/mm/yyyy')";
			ResultSet rs       = stmt.executeQuery(sCmd);
			ResultSet rs_bkp   = null;
			while (rs.next()){
				
				/*
	            dataHoje = new Date();
	            dataHojeString=dataHoje.toString();
	            if (dataHojeString.substring(8,10).equals("28")) break;
	            */
				
				registros++;
				String sCampo01=rs.getString("DAT_PROC");
				String sCampo02=rs.getString("COD_TSMI_LOG_BROKER");
				
				System.out.println("Limpando o codigo: "+sCampo02);
				
				String sCampo03=rs.getString("COD_TRANSACAO");          
				String sCampo04=rs.getString("TXT_PARTE_FIXA");         
				String sCampo05=rs.getString("TXT_PARTE_VARIAVEL");
				if (sCampo05==null) sCampo05=""; 
				sCampo05=sCampo05.replaceAll("'","");
								
				String sCampo06=rs.getString("TXT_RESULTADO");
				if (sCampo06==null) sCampo06="";				
				sCampo06=sCampo06.replaceAll("'","");
				
				
				String sCampo07=rs.getString("COD_RETORNO");            
				String sCampo08=rs.getString("IND_INC");                
				String sCampo09=rs.getString("NUM_TRANSACAO");          
				String sCampo10=rs.getString("DAT_ENVIO_REQ");          
				String sCampo11=rs.getString("DAT_RETORNO_REQ");        
				String sCampo12=rs.getString("DAT_ENVIO_BROKER");       
				String sCampo13=rs.getString("DAT_RETORNO_BROKER");     
				String sCampo14=rs.getString("COD_ORGAO_LOTACAO");      
				String sCampo15=rs.getString("COD_ORGAO_ATUACAO");      
				String sCampo16=rs.getString("NOM_USERNAME");
				
				if (sCampo01==null) sCampo01="";
				if (sCampo02==null) sCampo02="";
				if (sCampo03==null) sCampo03="";
				if (sCampo04==null) sCampo04="";
				if (sCampo05==null) sCampo05="";
				if (sCampo06==null) sCampo06="";
				if (sCampo07==null) sCampo07="";
				if (sCampo08==null) sCampo08="";
				if (sCampo09==null) sCampo09="";
				if (sCampo10==null) sCampo10="";
				if (sCampo11==null) sCampo11="";
				if (sCampo12==null) sCampo12="";
				if (sCampo13==null) sCampo13="";
				if (sCampo14==null) sCampo14="";
				if (sCampo15==null) sCampo15="";
				if (sCampo16==null) sCampo16="";
				
				sCmd="SELECT * FROM SMIT.TSMI_LOG_BROKER_BKP@SMITPBKP WHERE COD_TSMI_LOG_BROKER="+sCampo02;
				rs_bkp= stmt_bkp.executeQuery(sCmd);				
				if (rs_bkp.next()) {
					System.out.println("Pulando LOG_BROKER..."+registros+" "+sCampo01);
					continue;
				}


				
				
				
				sComand="INSERT INTO TSMI_LOG_BROKER_BKP_2010@SMITPBKP "+
				"(DAT_PROC,"+               
				"COD_TSMI_LOG_BROKER,"+    
				"COD_TRANSACAO,"+          
				"TXT_PARTE_FIXA,"+         
				"TXT_PARTE_VARIAVEL,"+     
				"TXT_RESULTADO,"+          
				"COD_RETORNO,"+            
				"IND_INC,"+                
				"NUM_TRANSACAO,"+          
				"DAT_ENVIO_REQ,"+          
				"DAT_RETORNO_REQ,"+        
				"DAT_ENVIO_BROKER,"+       
				"DAT_RETORNO_BROKER,"+     
				"COD_ORGAO_LOTACAO,"+      
				"COD_ORGAO_ATUACAO,"+      
				"NOM_USERNAME) VALUES ("+
				"to_date('"+sCampo01+"','DD/MM/YYYY'),"+
				"'"+sCampo02+"',"+    
				"'"+sCampo03+"',"+          
				"'"+sCampo04+"',"+         
				"'"+sCampo05+"',"+     
				"'"+sCampo06+"',"+          
				"'"+sCampo07+"',"+            
				"'"+sCampo08+"',"+               
				"'"+sCampo09+"',"+          
				"'"+sCampo10+"',"+          
				"'"+sCampo11+"',"+
				"to_date('"+sCampo12+"','DD/MM/YYYY'),"+
				"to_date('"+sCampo13+"','DD/MM/YYYY'),"+
				"'"+sCampo14+"',"+      
				"'"+sCampo15+"',"+      
				"'"+sCampo16+"')";
				stmt2.execute(sComand);
				
				sComand="DELETE FROM TSMI_LOG_BROKER WHERE COD_TSMI_LOG_BROKER="+sCampo02;
				stmt2.execute(sComand);
				

								
				
			    if ((registros%100)==0) {
				  System.out.println("Deletando LOG_BROKER..."+registros+" "+sCampo01);
				  conn.commit();
			    }
			}
				
				
				
			
			
			
			stmt2.close();
			stmt.close();
			
		} catch (Exception e) {
			System.out.println("Erro==>"+sComand+"-->"+e.getMessage());
			throw new DaoException(e.getMessage());
		}
		return registros;
	}


	public int IncluiDadosLimpezaLinha(Connection conn, LimpezaTabBean tab) throws DaoException, SQLException {
		
		
		String sCmd="";		
		int registros = 0;
		int arquivos = 0;
		try {
			Statement stmt_arq     = conn.createStatement();
			Statement stmt_lin     = conn.createStatement();
			Statement stmt_log     = conn.createStatement();			
			Statement stmt_ins_del = conn.createStatement();			
			Statement stmt_ins_del_log = conn.createStatement();			
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            
            Date dataHoje = new Date();
            String dataHojeString=dataHoje.toString();
            
                        
            String sDataInicial="01/01/2009";
            String sDataFinal="31/12/2009";
            int MaxArquivos=55000;
            String stipoArquivo="'CODRET','LOG01','MC01','MZ01','MX01','RECJARI','TVEP'";

            /*
            stipoArquivo="'MR01'";
            stipoArquivo="'ME01'";            
            stipoArquivo="'FOTOVEX'";
            stipoArquivo="'EMITEVEX'";
            */
            System.out.println("Inicio Processamento..."+new Date());
            
            stipoArquivo="'ME01'";
 
            stipoArquivo="'EMITEVEX'";
            
            
			sCmd = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO "+
			"WHERE " +
			"DAT_RECEBIMENTO>='"+sDataInicial+"' AND DAT_RECEBIMENTO<='"+sDataFinal+"' "+
			/*
			"COD_IDENT_ARQUIVO IN ("+stipoArquivo+") "+
			*/
			"ORDER BY DAT_RECEBIMENTO";
			ResultSet rs_arq   = stmt_arq.executeQuery(sCmd);
			ResultSet rs_lin   = null;
			ResultSet rs_log   = null;
			
			while ( rs_arq.next() && (arquivos<=MaxArquivos))
			{
				String sCod_Arquivo=rs_arq.getString("COD_ARQUIVO");
				String sNom_Arquivo=rs_arq.getString("NOM_ARQUIVO");
				String sQtdReg=rs_arq.getString("QTD_REG");
				String sDataRecebimento=rs_arq.getString("DAT_RECEBIMENTO");
//				System.out.println("Processando dele��o... Quantidade de Arquivos.."+arquivos+"...Quantidade de registros:"+registros+ " "+ new Date()+" Total de Registros do Arquivo:"+sQtdReg);				
				sCmd="SELECT * FROM TSMI_LINHA_ARQUIVO_REC WHERE COD_ARQUIVO='"+sCod_Arquivo+"'";
				rs_lin = stmt_lin.executeQuery(sCmd);
				boolean bAcheiDados=false;
				int registrosarq=0;
				while (rs_lin.next()){
					bAcheiDados=true;
					dataHoje = new Date();
					dataHojeString=dataHoje.toString();

					/*
					if (dataHojeString.substring(8,10).equals("25")) break;
					*/
					
					registros++;
					registrosarq++;
					
				    if ((registrosarq%200)==0)
					System.out.println("Dele��o... Arquivo "+sNom_Arquivo+" Data : "+sDataRecebimento+" ("+registrosarq+"/"+sQtdReg+") Quantidade de Arquivos.."+arquivos+"...registros:"+registros+ " "+ new Date());
					
					
					String sCampo01=rs_lin.getString("DSC_LINHA_ARQ_REC");              
					String sCampo02=rs_lin.getString("COD_LINHA_ARQ_REC");              
					String sCampo03=rs_lin.getString("COD_ARQUIVO");                    
					String sCampo04=rs_lin.getString("NUM_SEQ_LINHA");                  
					String sCampo05=rs_lin.getString("COD_STATUS");                     
					String sCampo06=rs_lin.getString("DSC_LINHA_ARQ_RETORNO");          
					String sCampo07=rs_lin.getString("COD_RETORNO");                    
					String sCampo08=rs_lin.getString("COD_RETORNO_BATCH");              
					String sCampo09=rs_lin.getString("NUM_AUTO_INFRACAO");              
					String sCampo10=rs_lin.getString("NUM_NOTIFICACAO");                
					String sCampo11=rs_lin.getString("COD_ORGAO");                      
					String sCampo12=rs_lin.getString("IND_FASE");                       
					String sCampo13=rs_lin.getString("NUM_PLACA");                      				
					if (sCampo01==null) sCampo01="";
					if (sCampo02==null) sCampo02="";
					if (sCampo03==null) sCampo03="";
					if (sCampo04==null) sCampo04="";
					if (sCampo05==null) sCampo05="";
					if (sCampo06==null) sCampo06="";
					if (sCampo07==null) sCampo07="";
					if (sCampo08==null) sCampo08="";
					if (sCampo09==null) sCampo09="";
					if (sCampo10==null) sCampo10="";
					if (sCampo11==null) sCampo11="";
					if (sCampo12==null) sCampo12="";
					if (sCampo13==null) sCampo13="";
					


					sCmd="SELECT " +
					"COD_PROC_LINHA_ARQ_LOG,"+
                    "TO_CHAR(DAT_INICIO_LINHA, 'DD/MM/YYYY HH24:MI:SS') AS DAT_INICIO_LINHA,"+
                    "TO_CHAR(DAT_INICIO_BROKER, 'DD/MM/YYYY HH24:MI:SS') AS DAT_INICIO_BROKER,"+
                    "TO_CHAR(DAT_FIM_BROKER, 'DD/MM/YYYY HH24:MI:SS') AS DAT_FIM_BROKER,"+
                    "TO_CHAR(DAT_FIM_LINHA, 'DD/MM/YYYY HH24:MI:SS') AS DAT_FIM_LINHA,"+                    
                    "TXT_RETORNO_BROKER,"+
                    "COD_LINHA_ARQ_REC "+                                                       
                    "FROM TSMI_PROC_LINHA_ARQ_LOG WHERE COD_LINHA_ARQ_REC='"+sCampo02+"'";
					rs_log = stmt_log.executeQuery(sCmd);
					while (rs_log.next()){
						String sCampoLog01=rs_log.getString("COD_PROC_LINHA_ARQ_LOG");
						String sCampoLog02=rs_log.getString("DAT_INICIO_LINHA");
						String sCampoLog03=rs_log.getString("DAT_INICIO_BROKER");
						String sCampoLog04=rs_log.getString("DAT_FIM_BROKER");
						String sCampoLog05=rs_log.getString("DAT_FIM_LINHA");
						String sCampoLog06=rs_log.getString("TXT_RETORNO_BROKER");
						String sCampoLog07=rs_log.getString("COD_LINHA_ARQ_REC");
						
						sCmd="INSERT INTO TSMI_PROC_LINHA_ARQ_LOGBKP2009@SMITPBKP "+
						"(COD_PROC_LINHA_ARQ_LOG,"+         
						"DAT_INICIO_LINHA,"+               
						"DAT_INICIO_BROKER,"+              
						"DAT_FIM_BROKER,"+                 
						"DAT_FIM_LINHA,"+                  
						"TXT_RETORNO_BROKER,"+             
						"COD_LINHA_ARQ_REC) VALUES ("+              				
						"'"+sCampoLog01+"',"+
						"to_date('"+sCampoLog02+"','DD/MM/YYYY HH24:MI:SS'),"+
						"to_date('"+sCampoLog03+"','DD/MM/YYYY HH24:MI:SS'),"+
						"to_date('"+sCampoLog04+"','DD/MM/YYYY HH24:MI:SS'),"+
						"to_date('"+sCampoLog05+"','DD/MM/YYYY HH24:MI:SS'),"+
						"'"+sCampoLog06+"',"+            
						"'"+sCampoLog07+"')";      
						stmt_ins_del_log.execute(sCmd);
						
						sCmd="DELETE FROM TSMI_PROC_LINHA_ARQ_LOG WHERE COD_PROC_LINHA_ARQ_LOG='"+sCampoLog01+"'";
						stmt_ins_del_log.execute(sCmd);
					}
					
					sCmd="INSERT INTO TSMI_LINHA_ARQUIVO_REC_BKP2009@SMITPBKP "+
					"(DSC_LINHA_ARQ_REC,"+      
					"COD_LINHA_ARQ_REC,"+      
					"COD_ARQUIVO,"+            
					"NUM_SEQ_LINHA,"+          
					"COD_STATUS,"+             
					"DSC_LINHA_ARQ_RETORNO,"+
					"COD_RETORNO,"+            
					"COD_RETORNO_BATCH,"+      
					"NUM_AUTO_INFRACAO,"+      
					"NUM_NOTIFICACAO,"+        
					"COD_ORGAO,"+              
					"IND_FASE,"+               
					"NUM_PLACA) VALUES ("+              				
					"'"+sCampo01+"',"+    
					"'"+sCampo02+"',"+          
					"'"+sCampo03+"',"+         
					"'"+sCampo04+"',"+     
					"'"+sCampo05+"',"+          
					"'"+sCampo06+"',"+            
					"'"+sCampo07+"',"+               
					"'"+sCampo08+"',"+          
					"'"+sCampo09+"',"+          
					"'"+sCampo10+"',"+
					"'"+sCampo11+"',"+
					"'"+sCampo12+"',"+					
					"'"+sCampo13+"')";      
					stmt_ins_del.execute(sCmd);
					
					sCmd="DELETE FROM TSMI_LINHA_ARQUIVO_REC WHERE COD_LINHA_ARQ_REC='"+sCampo02+"'";
					stmt_ins_del.execute(sCmd);
				}
				if (bAcheiDados) arquivos++;
				if (arquivos>0){ 
				    if ((arquivos%5)==0)
					System.out.println("Processando dele��o... Quantidade de Arquivos.."+arquivos+"...Quantidade de registros:"+registros+ " "+ new Date());
				}
				conn.commit();
			}
			stmt_ins_del.close();
			stmt_lin.close();
			stmt_arq.close();
			
		} 
		catch (SQLException e) 
		{
			conn.rollback();
			System.out.println("Erro==>"+sCmd+"-->"+e.getMessage());
			throw new DaoException(e.getMessage());
		}
		
		
		return registros;
	}
	
	
	
	/**
	 * -----------------------------------------------------------
	 * DAO relativo �s Tabelas que ser�o Limpas
	 * -----------------------------------------------------------
	 * @author Wellem Mello
	 * @version 1.0
	 */
	public int ExcluiDadosLimpeza(Connection conn, LimpezaTabBean tab) throws DaoException {
		
		int registros = 0;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "DELETE FROM " + tab.getNomTabela();
			
			if ((tab.getNomTabelaPai()== null) || (tab.getNomTabelaPai()== ""))
				sCmd += " WHERE " + tab.getCampoFiltro() + " < (TRUNC(SYSDATE) - " + tab.getValFiltro() + ")";
			else
				sCmd += " WHERE " + tab.getCampoTabelaPai() + " IN " + MontaClausulaLimpeza(conn, tab);
			
			registros = stmt.executeUpdate(sCmd);
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return registros;
	}
	
	public String MontaClausulaLimpeza(Connection conn, LimpezaTabBean tab) {
		
		String clausula = "";
		LimpezaTabBean pai = tab.getPai();
		if (pai != null) {
			if (pai.getPai() != null)
				clausula = "(SELECT " + pai.getCampoTabelaPK() + " FROM " + pai.getNomTabela()
				+ " WHERE " + pai.getCampoTabelaPai() + " IN" + MontaClausulaLimpeza(conn, pai) + ")";
			else
				clausula = "(SELECT " + pai.getCampoTabelaPK() + " FROM " + pai.getNomTabela()
				+ " WHERE " + pai.getCampoFiltro() + " < (TRUNC(SYSDATE) - " + pai.getValFiltro() + "))";
		}
		return clausula;
	}
	
	
	/**
	 * Metodo que efetua carga na tabela
	 * atraves da transacao 043
	 * @author Luciana Rocha
	 * @param myAuto
	 * @throws DaoException
	 */
	public void carregaTabela(REC.AutoInfracaoBean myAuto)
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		boolean existe = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtVer = conn.createStatement();
			conn.setAutoCommit(false);
			
			//Veridica se n� auto ja foi inserido
			sCmd = " SELECT * FROM TGER_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
			ResultSet rs = stmtVer.executeQuery(sCmd);
			
			while (!rs.next()){
				existe = false;
			}
			rs.close();
			stmtVer.close();
			
			//Inseri dados da transacao 043 na tabela
			if (!existe){
				sCmd = "INSERT INTO TGER_AUTO_INFRACAO(COD_ORGAO,COD_MUNICIPIO,NUM_AUTO_INFRACAO,DAT_INFRACAO,"
					+ "COD_INFRACAO,TIP_INFRACAO,NUM_PONTOS,STATUS_AUTO,VAL_INTEGRAL,VAL_DESCONTO) VALUES ("
					+ "'"+ myAuto.getCodOrgao()+"', "
					+ "'"+ myAuto.getCodMunicipio()+"', "
					+ "'"+ myAuto.getNumAutoInfracao()+"', "
					+ "TO_DATE('"+ myAuto.getDatInfracao()+"','DD/MM/YYYY'), "
					+ "'"+ myAuto.getInfracao().getCodInfracao()+"', "
					+ "'"+ myAuto.getInfracao().getIndTipo()+"', "
					+ "'"+ myAuto.getInfracao().getNumPonto()+"', "
					+ "'"+ myAuto.getCodStatus()+"', "
					+ "'"+ myAuto.getInfracao().getValReal()+"', "
					+ "'"+ myAuto.getInfracao().getValRealDesconto()+"')";
				stmt.execute(sCmd);
				stmt.close();
				conn.commit();
			}
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Grava os dados para controle de processamento de
	 * carga de tabela.
	 * @author Luciana Rocha
	 * @param controleRobAutoBean
	 * @throws DaoException
	 */
	public void gravaDadosControle(ControleRobAutoBean controleRobAutoBean)
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			
			//Inseri dados na tabela de controle
			sCmd = "INSERT INTO TGER_CTROB_AUTO(NUM_CONTROLE,DAT_INICIO,DAT_FIM,TXT_PROCESSAMENTO,"
				+ "QTD_AUTOS_PROC,SIT_PROC) VALUES ("
				+ "SEQ_TGER_CTROB_AUTO.nextval,"
				+ "TO_DATE('"+ controleRobAutoBean.getDatInicio()+"','DD/MM/YYYY'), "
				+ "TO_DATE('"+ controleRobAutoBean.getDatFim()+"','DD/MM/YYYY'), "
				+ "'"+ controleRobAutoBean.getTxtProces()+"', "
				+ "'"+ controleRobAutoBean.getQtdAutosProces()+"', "
				+ "'"+ controleRobAutoBean.getSitProces()+"')";
			
			stmt.execute(sCmd);
			
			stmt.close();
			conn.commit();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * Verifica ultima data do processamento
	 * @author Luciana Rocha
	 * @return ultimaData
	 * @throws DaoException
	 */
	public String verificaUltData()throws DaoException {
		Connection conn = null;
		String sCmd = "";
		String ultimaData = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			
			sCmd = "SELECT MAX(TO_CHAR(DAT_INICIO,'DD/MM/YYYY'))AS DATA_INICIO FROM TGER_CTROB_AUTO";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				ultimaData = rs.getString("DATA_INICIO");
			}
			
			stmt.close();
			conn.commit();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return ultimaData;
	}
	
	/**
	 * M�todo verifica o indContinuidade, caso tenha sido
	 * interrompido ou n�o o processo.
	 * @param ultimaData
	 * @return indContiua
	 * @throws DaoException
	 * @throws RobotException
	 */
	
	public String verificaIndContinua(String ultimaData)throws DaoException, RobotException {
		Connection conn = null;
		String sCmd = "";
		String indContiua = "";
		ControleRobAutoBean controleRobAutoBean = new ControleRobAutoBean();
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			conn.setAutoCommit(false);
			
			sCmd = "SELECT IND_CONTINUIDADE FROM TGER_CTROB_AUTO WHERE DAT_INICIO = TO_DATE('"+ultimaData+"','DD/MM/YYYY') " +
			" AND SIT_PROC <> '"+controleRobAutoBean.SIT_OK+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				indContiua = rs.getString("IND_CONTINUIDADE");
			}
			
			stmt.close();
			conn.commit();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return indContiua;
	}
	
	/**************************************
	 * M�todo para listar os org�os cadastrados no sistema
	 * @author Glaucio Jannotti
	 * @return Retorna um List de OrgaoBean com os orgaos encontrados
	 */
	public List ListarOrgaos() throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " SELECT COD_ORGAO, NOM_ORGAO, SIG_ORGAO FROM TSMI_ORGAO WHERE IND_INTEGRADA = 'S'";
			sql += " ORDER BY IND_SALDO DESC";
			ResultSet rs = stmt.executeQuery(sql);
			
			List listOrgaos = new ArrayList();
			while (rs.next()) {
				ACSS.OrgaoBean orgao = new ACSS.OrgaoBean();
				orgao.setCodOrgao(rs.getString("COD_ORGAO"));
				orgao.setNomOrgao(rs.getString("NOM_ORGAO"));
				orgao.setSigOrgao(rs.getString("SIG_ORGAO"));
				listOrgaos.add(orgao);
			}
			
			stmt.close();
			return listOrgaos;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**************************************
	 * M�todo para listar os status de auto cadastrados no sistema
	 * @author Glaucio Jannotti
	 * @return Retorna um List de StatusBean com os status encontrados
	 */
	public List ListarStatus() throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " SELECT COD_STATUS, NOM_STATUS FROM TSMI_STATUS_AUTO ORDER BY COD_STATUS";
			ResultSet rs = stmt.executeQuery(sql);
			
			List listStatus = new ArrayList();
			while (rs.next()) {
				REC.StatusBean status = new REC.StatusBean();
				status.setCodStatus(rs.getString("COD_STATUS"));
				status.setNomStatus(rs.getString("NOM_STATUS"));
				listStatus.add(status);
			}
			
			stmt.close();
			return listStatus;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**************************************
	 * M�todo para atualizar o valor de um par�metro de sistema
	 * @author Glaucio Jannotti
	 */
	public void AtualizaParametro(ACSS.ParamSistemaBean bean, Connection conn) throws DaoException {
		try {
			String sqlSel = "select cod_parametro from tcau_param_sistema where nom_parametro = :1 and cod_sistema = :2";
			PreparedStatement stmSel = conn.prepareStatement(sqlSel);
			stmSel.setString(1, bean.getNomParam());
			stmSel.setInt(2, Integer.parseInt(bean.getCodSistema()));
			ResultSet rsSel = stmSel.executeQuery();
			
			if (rsSel.next()) {
				String sqlUpd = "update tcau_param_sistema set val_parametro = :1 where cod_parametro = :2";
				PreparedStatement stmUpd = conn.prepareStatement(sqlUpd);
				stmUpd.setString(1, bean.getValParametro());
				stmUpd.setInt(2, rsSel.getInt("cod_parametro"));
				stmUpd.executeUpdate();
				stmUpd.close();
				
			} else {
				String sqlIns = "insert into tcau_param_sistema (cod_parametro, nom_parametro, val_parametro, cod_sistema)";
				sqlIns += " values (seq_tcau_param_sistema.nextval, :1, :2, :3)";
				PreparedStatement stmIns = conn.prepareStatement(sqlIns);
				stmIns.setString(1, bean.getNomParam());
				stmIns.setString(2, bean.getValParametro());
				stmIns.setInt(3, Integer.parseInt(bean.getCodSistema()));
				stmIns.executeUpdate();
				stmIns.close();
			}
			stmSel.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public ArrayList verificaArqNaoProcessados(REG.ArquivoRecebidoBean ArqRecebido, 
			Connection conn) throws DaoException {
		ArrayList arqNaoProcessado = new ArrayList();
		try {
			String sCmd = "SELECT * FROM TSMI_ARQUIVO_RECEBIDO WHERE COD_STATUS <> 2 " +
			" AND COD_STATUS <> 9 AND  "+
			" DAT_PROCESSAMENTO_DETRAN IS NULL AND " +
			" DAT_RECEBIMENTO < '"+Util.getDataHoje().replaceAll("-","/")+"'"+
			" ORDER BY COD_IDENT_ARQUIVO,DAT_RECEBIMENTO";
			Statement stmt = conn.prepareStatement(sCmd);
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				REG.ArquivoRecebidoBean arqRecedido = new REG.ArquivoRecebidoBean();
				arqRecedido.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				arqRecedido.setCodOrgao(rs.getString("COD_ORGAO"));
				arqRecedido.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));
				arqNaoProcessado.add(arqRecedido);
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return arqNaoProcessado;
	}
	
	public String bloqueiaUsuario(String nDias) throws SQLException, RobotException {
		
		Connection conn = null;
		String sCorpoEmail = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			ResultSet rs = null;
			String sql = "SELECT  T1.NOM_USERNAME,T2.COD_USUARIO,MAX(T1.DAT_OPERACAO) AS DAT_OPERACAO FROM "+ 
			"TCAU_LOG_ACESSO T1,TCAU_USUARIO T2 "+
			"WHERE T1.NOM_USERNAME=T2.NOM_USERNAME AND "+ 
			"(T2.IND_BLOQUEIO IS NULL OR T2.IND_BLOQUEIO = 0) "+
			"GROUP BY T1.NOM_USERNAME,T2.COD_USUARIO "+
			"ORDER BY T1.NOM_USERNAME,T2.COD_USUARIO";
			
			Statement stm=conn.createStatement();
			PreparedStatement stmSelc = conn.prepareStatement(sql);
			rs = stmSelc.executeQuery();
			int nDiasSemAcesso = 0;
			String datOperacao = "";
			String nomUsuario  = "";
			String codUsuario  = "";
			
			while (rs.next())
			{
				datOperacao = converteData(rs.getDate("DAT_OPERACAO"));
				nomUsuario = rs.getString("NOM_USERNAME");
				codUsuario = rs.getString("COD_USUARIO");
				nDiasSemAcesso = Util.DifereDias(datOperacao,Util.getDataHoje().replaceAll("-","/"));
				if (nDiasSemAcesso >= Integer.parseInt(nDias))
				{
					sql = "UPDATE TCAU_USUARIO SET IND_BLOQUEIO = 1 WHERE " +
					" COD_USUARIO = "+ codUsuario;
					stm.execute(sql); 
					sCorpoEmail +="Login : "+nomUsuario+" - " +
					"ultimo acesso: "+datOperacao+"\n";
					sql = "INSERT INTO TCAU_BLOQUEIO (cod_tcau_bloqueio,dat_ini,txt_motivo,nom_username, cod_usuario) VALUES " +
					" (SEQ_TCAU_BLOQUEIO.NEXTVAL,'"+Util.getDataHoje().replaceAll("-","/")+"', " +
					" 'BLOQUEIO AUTOM�TICO DO SISTEMA.','"+nomUsuario+"','"+codUsuario+"')";
					stm.execute(sql); 
				}
			}
			conn.commit(); 
			stm.close();
			stmSelc.close();
			rs.close();
			
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
		return sCorpoEmail;
	}
	
	public void AcertaQuantidadeRegDol(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			try
			{
				REG.Dao daoreg=REG.Dao.getInstance();
				String sCmd  = "SELECT COD_LOTE,COD_STATUS FROM TSMI_LOTE_DOL";
				rs = stmt.executeQuery(sCmd);
				int i=0;
				while (rs.next())
				{
					REG.LoteDolBean myLote = new REG.LoteDolBean();
					myLote.setCodLote(rs.getString("COD_LOTE"));
					System.out.println("Lote : "+i+" "+myLote.getCodLote());					
					myLote.setCodStatus(rs.getString("COD_STATUS"));
					daoreg.LoteUpdateDol(myLote,"");
					i++;
					
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public void AcertaAutosRenainf(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt  = conn.createStatement();
			
			ResultSet rs = null;
			int i=0;
			try
			{
				REG.Dao daoreg=REG.Dao.getInstance();
				
				String sCmd  = "SELECT COD_LINHA_LOTE, DSC_LINHA_LOTE, NUM_LOTE, NUM_ANO_EXERCICIO, ";
				sCmd  += "COD_UNIDADE, DAT_LOTE ";
				sCmd  += "FROM TSMI_LOTE_DOL L,TSMI_LINHA_LOTE LI ";
				sCmd  += "WHERE L.COD_LOTE = LI.COD_LOTE ";
				sCmd  += "AND (LI.COD_RETORNO = '497')";
				
				rs = stmt.executeQuery(sCmd);
				int iCont=0;
				while (rs.next())
				{
					REG.LoteDolBean myLote = new REG.LoteDolBean();
					
					String LinhaLote = sys.Util.rPad(rs.getString("DSC_LINHA_LOTE"), " ", 445);
					String IdLinha = (rs.getString("COD_LINHA_LOTE"));
					String anoLote = (rs.getString("NUM_ANO_EXERCICIO"));
					String numLote = (rs.getString("NUM_LOTE"));
					String codUnidade = (rs.getString("COD_UNIDADE"));
					String datLote = (rs.getString("DAT_LOTE"));
					
					System.out.println("Acertando Auto "+iCont+" "+LinhaLote.substring(41,53));
					iCont++;
					
					
					
					String codMarcaModelo = LinhaLote.substring(60,67);
					if ("       ".equals(codMarcaModelo)) codMarcaModelo ="0000000";
					
					LinhaLote = LinhaLote.substring(0,34)+"7"+LinhaLote.substring(35,60)+codMarcaModelo+LinhaLote.substring(67,445);
					LinhaLote =	LinhaLote.substring(8,445);
					
					LinhaLote = sys.Util.rPad(LinhaLote," ",437);
					
					String operador = LinhaLote.substring(0, 10);
					
					String parteVar = LinhaLote + sys.Util.lPad(anoLote, "0", 4) + sys.Util.lPad(numLote, "0", 5)
					+ sys.Util.lPad(codUnidade, "0", 4) + sys.Util.lPad(datLote, "0", 8)
					+ operador;
					
					BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);				
					transBRK.setParteFixa("408");
					transBRK.setParteVariavel(parteVar);
					String resultado = transBRK.getResultado(conn);
					
					/*Gravar Dados no TSMI_LOG_BROKER*/					
					String codRetorno = resultado.substring(0,3);					
					transBRK.GravarBrokerROB(resultado,codRetorno,UsuarioBeanId.getCodOrgaoAtuacao());
					
					
					//Trocar as aspas simples por aspas no padrao oracle
					resultado = resultado.replaceAll("'", "''");
					//Pega os retornos da linha
					codRetorno = resultado.substring(0, 3);
					String linhaRetorno = "";
					if (!sys.Util.isNumber(codRetorno)) {
						linhaRetorno = resultado.substring(3);
					} else {
						linhaRetorno = resultado.substring(5);
					}
					if (linhaRetorno.length() > 900)
						linhaRetorno = linhaRetorno.substring(0, 900);
					linhaRetorno = linhaRetorno.trim();
					//continua Broker
					
					boolean loteComErro = false;			
					if (!codRetorno.equals("000"))				
						loteComErro = true;
					
					//codStatus 0=recebido 1=processado 9=erro no acesso ao Broker
					String codStatus = "9";
					if (sys.Util.isNumber(codRetorno)) {
						if (codRetorno.equals("000")
								|| (Integer.parseInt(codRetorno) >= 401)
								&& (Integer.parseInt(codRetorno) <= 599))
							codStatus = "1";				
					}
					Statement stmt2 = conn.createStatement();					
					sCmd = "";
					//Gravar Status da Linha
					if(sys.Util.isNumber(codRetorno))
					{
						sCmd =
							"UPDATE TSMI_LINHA_LOTE SET COD_STATUS = "
							+ codStatus
							+ ", "
							+ " COD_RETORNO = '"
							+ codRetorno
							+ "', "
							+ " DSC_LINHA_LOTE_RETORNO = '"
							+ linhaRetorno
							+ "'"
							+ " WHERE COD_LINHA_LOTE = "
							+ IdLinha;
						//monitor
						stmt2.execute(sCmd);
						//Gravar Log de Processamento da Linha
						sCmd =
							"INSERT INTO TSMI_PROC_LINHA_LOTE_LOG_TMP (COD_PROC_LINHA_LOTE_LOG, "
							+ "DAT_PROC, COD_RETORNO, DSC_LINHA_LOTE_RETORNO, COD_LINHA_LOTE) VALUES(SEQ_TSMI_PROC_LINHA_LOTE_LOG.NEXTVAL, "
							+ "SYSDATE, '"
							+ codRetorno
							+ "', "
							+ "'"
							+ linhaRetorno
							+ "', "
							+ IdLinha
							+ ")";
						
						//monitor.gravaMonitor("Inclus�o log linha: " + sCmd);
						stmt2.execute(sCmd);
					}
					stmt2.close();
				}
				
			} catch (SQLException e) {
			}
			rs.close();
			stmt.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}
	
	public boolean AcertarAutosPagos(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		boolean bOk=false;
		boolean incluido = false;
		Connection conn = null;
		int numLinha;
		String exercicio = "";
		BigDecimal divisor;
		BigDecimal valPag;
		AutoInfracaoBean auto;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);		
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();	
			Statement stmtLinha = conn.createStatement();
			Statement stmtGrava = conn.createStatement();
			ResultSet rs = null;
			ResultSet rsLinha = null;
			String sql = "SELECT COD_ARQUIVO, NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO WHERE "+
			"COD_IDENT_ARQUIVO = 'PAGOS/VEX' AND COD_STATUS IN (0,7,8) ";
			rs = stmt.executeQuery(sql);
			String sCodArquivo="";
			while(rs.next())
			{
				sCodArquivo = rs.getString("COD_ARQUIVO");				
				sql = " SELECT COD_LINHA_ARQ_REC,DSC_LINHA_ARQ_REC FROM TSMI_LINHA_ARQUIVO_REC WHERE " +
				"COD_ARQUIVO = "+sCodArquivo;
				rsLinha = stmtLinha.executeQuery(sql);
				numLinha = 0;
				exercicio = rs.getString("NOM_ARQUIVO").substring(10,12)+
				"/"+rs.getString("NOM_ARQUIVO").substring(12,16);
				
				while(rsLinha.next())
				{
					String codLinhaArquivoRec = rsLinha.getString("COD_LINHA_ARQ_REC");
					if (!"3".equals(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(0,1)))
					{
						auto  = new AutoInfracaoBean();
						valPag = new BigDecimal ((double) 0); 
						valPag = new BigDecimal(Double.parseDouble(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(45,56)));		
						valPag = valPag.setScale(2,BigDecimal.ROUND_DOWN);
						divisor = new BigDecimal(100);
						valPag = valPag.divide(divisor,BigDecimal.ROUND_DOWN);
						auto.setNumAutoInfracao(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(1,13).trim());
						auto.setDatPag(sys.Util.formataDataDDMMYYYY(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(13,21)).trim());
						auto.setCodOrgao(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(21,27).trim());
						auto.setCodAgente(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(27,29).trim());
						auto.setDatVencimento(sys.Util.formataDataDDMMYYYY(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(29,37)).trim());
						auto.setDatInfracao(sys.Util.formataDataDDMMYYYY(rsLinha.getString("DSC_LINHA_ARQ_REC").substring(37,45)).trim());								
						auto.getInfracao().setValReal(valPag.toString().trim());
						
						if (!autoJaExisteNoGerencial(conn,auto,exercicio)){						
							incluido = GravaLinhasAutosPagos(conn,exercicio,auto,stmtGrava,numLinha);
							if(!incluido)
								throw new DaoException("Erro ao gravar AutosPagos !");	
						}else{
							if (codLinhaArquivoRec != null){
								cancelaLinhaDoArquivo(conn,codLinhaArquivoRec);
								incluido = true;
							}
						}
					}
					numLinha++;
				}
				
			}		
			if(incluido)
			{
				sql = "UPDATE tsmi_arquivo_recebido SET cod_status = '2' "+
				" WHERE cod_arquivo = "+ sCodArquivo;
				stmtLinha.executeUpdate(sql);
				conn.commit();
			}
			stmt.close();
			stmtLinha.close();
			stmtGrava.close();
			rs.close();
			if(rsLinha != null)rsLinha.close();
			bOk=true;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
		return bOk;	
	}

	
	
	public boolean AcertarJulgamento(ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException {
		boolean bOk=false;
		boolean incluido = false;
		Connection conn = null;
		int numLinha;
		String exercicio = "";
		BigDecimal divisor;
		BigDecimal valPag;
		AutoInfracaoBean auto;
		String sCmd="";
		try {
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 

			
			conn = serviceloc.getConnection(MYABREVSIST);		
			conn.setAutoCommit(false);
			Statement stmt  = conn.createStatement();	
			Statement stmtLinha = conn.createStatement();
			Statement stmtResumo = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rsLinha   = null;
			ResultSet rsResumo  = null;
			
			String sql = "SELECT COD_ARQUIVO, NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO WHERE "+
			"COD_IDENT_ARQUIVO = 'JULG/DET' AND COD_STATUS IN (0,7,8) ";
			rs = stmt.executeQuery(sql);
			String sCodArquivo="";
			while(rs.next())
			{
				sCodArquivo = rs.getString("COD_ARQUIVO");				
				sql = " SELECT COD_LINHA_ARQ_REC,DSC_LINHA_ARQ_REC FROM TSMI_LINHA_ARQUIVO_REC WHERE " +
				"COD_ARQUIVO = "+sCodArquivo;
				rsLinha = stmtLinha.executeQuery(sql);
				numLinha = 0;
				exercicio = rs.getString("NOM_ARQUIVO").substring(10,12)+
				"/"+rs.getString("NOM_ARQUIVO").substring(12,16);
				int i=0;
				while(rsLinha.next())
					
				{
					String codLinhaArquivoRec = rsLinha.getString("COD_LINHA_ARQ_REC");
					String sDescricaoLinha=rsLinha.getString("DSC_LINHA_ARQ_REC");
					if (!"3".equals(sDescricaoLinha.substring(0,1)))
					{
						String numAutoInfracao   = sDescricaoLinha.substring(1,13);
					    String numProcesso       = sDescricaoLinha.substring(13,33);
					    String numRequerimento   = sDescricaoLinha.substring(33,63);
					    String sCodTipoSolic     = sDescricaoLinha.substring(63,65);
					    String codOrgao          = sDescricaoLinha.substring(65,71);
					    String codInfracao       = sDescricaoLinha.substring(71,76);
					    String codEnquadramento  = sDescricaoLinha.substring(76,96);
					    String codJunta          = sDescricaoLinha.substring(96,102);
					    String codRelator        = sDescricaoLinha.substring(102,113);
					    String datResultado      = sDescricaoLinha.substring(113,121);
					    datResultado             = datResultado.substring(6,8)+"/"+datResultado.substring(4,6)+"/"+datResultado.substring(0,4);
					    int iDias=sys.Util.DifereDias(datResultado,datProc);
					    if (iDias<0) continue;
					    
						System.out.println("Incluido...."+i);
						i++;
					    
					    
					    if (datResultado.length()<10)
					    	continue;
					    String codResultado      = sDescricaoLinha.substring(121,122);
					    if (codResultado.equals("1")) codResultado="D";
					    else                          codResultado="I";
						if (!autoJulgado(conn,numAutoInfracao,datResultado))
						{
							sCmd = "INSERT INTO TGER_JULGAMENTO (COD_ORGAO,NUM_AUTO_INFRACAO,NUM_PROCESSO,NUM_REQUERIMENTO,COD_INFRACAO,TXT_ENQUADRAMENTO,"+
									"CODIGO_JUNTA,COD_RELATOR,COD_TIPO_SOLIC,DAT_RESULTADO,IND_RESULTADO) VALUES ("+
									"'"+codOrgao+"',"+
									"'"+numAutoInfracao+"',"+
									"'"+numProcesso+"',"+
									"'"+numRequerimento+"',"+
									"'"+codInfracao+"',"+
									"'"+codEnquadramento+"',"+
									"'"+codJunta+"',"+
									"'"+codRelator+"',"+
									"'"+sCodTipoSolic+"',"+
									"TO_DATE('"+datResultado+"','DD/MM/YYYY'), "+
									"'"+codResultado+"')";
								stmt.execute(sCmd);
								

								sCmd="SELECT * FROM TGER_RESUMO_JULGAMENTO WHERE COD_ORGAO='"+codOrgao+"' AND "+
										                                        "DAT_RESULTADO=TO_DATE('"+datResultado+"','DD/MM/YYYY') AND "+
								                                                "COD_TIPO_SOLIC='"+sCodTipoSolic+"' AND "+
										                                        "COD_INFRACAO='"+codInfracao+"' AND "+
								                                                "TXT_ENQUADRAMENTO='"+codEnquadramento+"'";
								rsResumo = stmtResumo.executeQuery(sCmd);
								if (!rsResumo.next()) {
								  sCmd = "INSERT INTO TGER_RESUMO_JULGAMENTO (COD_ORGAO,DAT_RESULTADO,COD_TIPO_SOLIC,COD_INFRACAO,TXT_ENQUADRAMENTO,QTD_DEFERIDOS,QTD_INDEFERIDOS) VALUES ("+
											"'"+codOrgao+"',"+
											"TO_DATE('"+datResultado+"','DD/MM/YYYY'), "+
											"'"+sCodTipoSolic+"',"+
											"'"+codInfracao+"',"+
											"'"+codEnquadramento+"',0,0)";
								  stmtResumo.execute(sCmd);								  
								}
								if (codResultado.equals("D")) {
									sCmd = "UPDATE TGER_RESUMO_JULGAMENTO SET "+
										   "QTD_DEFERIDOS=QTD_DEFERIDOS+1 WHERE COD_ORGAO='"+codOrgao+"' AND "+
										                                        "DAT_RESULTADO=TO_DATE('"+datResultado+"','DD/MM/YYYY') AND "+
								                                                "COD_TIPO_SOLIC='"+sCodTipoSolic+"' AND "+
										                                        "COD_INFRACAO='"+codInfracao+"' AND "+
								                                                "TXT_ENQUADRAMENTO='"+codEnquadramento+"'";
								}
								else {
									sCmd = "UPDATE TGER_RESUMO_JULGAMENTO SET "+
											   "QTD_INDEFERIDOS=QTD_INDEFERIDOS+1 WHERE COD_ORGAO='"+codOrgao+"' AND "+
											                                        "DAT_RESULTADO=TO_DATE('"+datResultado+"','DD/MM/YYYY') AND "+
									                                                "COD_TIPO_SOLIC='"+sCodTipoSolic+"' AND "+
											                                        "COD_INFRACAO='"+codInfracao+"' AND "+
									                                                "TXT_ENQUADRAMENTO='"+codEnquadramento+"'";
								}
								stmtResumo.execute(sCmd);
						}else{
							if (codLinhaArquivoRec != null){
								cancelaLinhaDoArquivo(conn,codLinhaArquivoRec);
								incluido = true;
							}
						}
					}
					numLinha++;
				}
				
			}		
			if(incluido)
			{
				sql = "UPDATE tsmi_arquivo_recebido SET cod_status = '2' "+
				" WHERE cod_arquivo = "+ sCodArquivo;
				stmtLinha.executeUpdate(sql);
				conn.commit();
			}
			conn.commit();
			stmt.close();
			stmtLinha.close();
			stmtResumo.close();
			rs.close();
			if(rsLinha != null)rsLinha.close();
			if(rsResumo!= null)rsResumo.close();
			bOk=true;
		} catch (Exception e) {
			System.out.println("Comando:"+sCmd);
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					System.out.println("Comando:"+sCmd);
					throw new RobotException(es.getMessage());
				}
			}
		}
		return bOk;	
	}
	
	
	public boolean VerificaTabela(String nomtabela) throws RobotException, ServiceLocatorException{
		Connection conn = null;
		boolean existe = false;
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TGER_AUTOS_PAGOS WHERE DAT_MES_REF ='"+nomtabela+"'";		
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				existe = true;
			rs.close();
			stmt.close();
			return existe;
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	
	
	public boolean GravaLinhasAutosPagos(Connection conn,String Exercicio,REC.AutoInfracaoBean Autos,Statement stmt,int numlinha) throws RobotException, ServiceLocatorException, BeanException{
		boolean ok = false;
		try {
			
			String sCmd = "INSERT INTO TGER_AUTOS_PAGOS (NUM_AUTO_INFRACAO,DAT_PAGTO,COD_ORGAO,"
				+ "COD_AGENTE,DAT_VENCIMENTO,DAT_INFRACAO,VALOR_PAGO,DAT_MES_REF)"
				+ " VALUES ('"
				+ Autos.getNumAutoInfracao()+"', "			
				+ "TO_DATE('"+Autos.getDatPag()+"','DD/MM/YYYY'), "
				+ "'"+Autos.getCodOrgao()+"', "
				+ "'"+Autos.getCodAgente()+"', "
				+ "TO_DATE('"+Autos.getDatVencimento()+"','DD/MM/YYYY'), "		
				+ "TO_DATE('"+Autos.getDatInfracao()+"','DD/MM/YYYY'), "
				+ Autos.getInfracao().getValReal()+","
				+ "'"+Exercicio+"') ";
			stmt.execute(sCmd);
			ok=true;
			
			return ok;
		} catch (SQLException e) {
			ok=false;
			throw new RobotException(e.getMessage());
		} 
	}
	
	public boolean autoJaExisteNoGerencial(Connection conn,REC.AutoInfracaoBean auto, String mesAnoReferencia) throws RobotException, ServiceLocatorException, BeanException{
		boolean existe = false;
		try {
			
			Statement stmt = conn.createStatement();
			String sCmd = "select * from tger_autos_pagos where num_auto_infracao = '"+auto.getNumAutoInfracao()+"' and dat_mes_ref = '"+mesAnoReferencia+"'";	
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				existe = true;
			rs.close();
			stmt.close();
			return existe;
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}

	
	public boolean autoJulgado(Connection conn,String numAutoInfracao,String dtJulgamento) throws RobotException, ServiceLocatorException, BeanException{
		boolean existe = false;
		try {
			
			Statement stmt = conn.createStatement();
			String sCmd = "select * from tger_julgamento where num_auto_infracao = '"+numAutoInfracao+"' and dat_resultado = '"+dtJulgamento+"'";	
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				existe = true;
			rs.close();
			stmt.close();
			return existe;
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	
	
	public void cancelaLinhaDoArquivo(Connection conn, String codLinhaArquivoRec) throws RobotException, ServiceLocatorException{
		try {
			
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE tsmi_linha_arquivo_rec SET cod_status = '9'  WHERE cod_arquivo = "+ codLinhaArquivoRec;
			stmt.executeUpdate(sCmd);
			conn.commit();			
			stmt.close();
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	
	private String converteData(java.sql.Date data) {
		String Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = df.format(data);
		} catch (Exception e) {
			Retorno = "";
		}
		return Retorno;
	}	
	
	public void getDadosEmitVex(GER.RelatArqNotificacaoBean bean, Connection conn) throws DaoException {
		try {
			
			Statement stmt = conn.createStatement();
			String sql = "select nom_arquivo, qtd_reg from tsmi_arquivo_recebido_tmp" +
			" where dat_recebimento = '"+bean.getDatRecebimento()+"' and cod_ident_arquivo = 'EMITEVEX' ";
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.next())
			{
				bean.setNomArquivo(rs.getString("nom_arquivo"));
				bean.setQtdReg(rs.getString("qtd_reg"));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}	
	
	public void getDadosEmitVexAcumulado(GER.RelatArqNotificacaoBean bean, Connection conn) throws DaoException {
		try {
			
			Statement stmt = conn.createStatement();
			String sql = "select nom_arquivo, qtd_reg, qtd_linha_pendente  from tsmi_arquivo_recebido" +
			" where dat_recebimento = '"+bean.getDatRecebimento()+"' and cod_ident_arquivo = 'EMITEVEX' " +
			" and cod_status = 2";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			if (rs.next())
			{
				bean.setNomArquivo(rs.getString("nom_arquivo"));
				bean.setQtdReg(rs.getString("qtd_reg"));
				bean.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				bean.setQtdRecebido(Integer.toString(rs.getInt("qtd_reg")-rs.getInt("qtd_linha_pendente")));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public ArrayList verificaQtdReq(Connection conn, ACSS.OrgaoBean Orgao, 
			String codTipoSolic) throws DaoException {
		
		ArrayList dados = new ArrayList();
		
		try {
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			String sql = "select /*rule*/ to_char(t.dat_proc_ju,'dd/mm/yyyy') as dat_proc_ju from " +
			" tsmi_requerimento t, tsmi_auto_infracao a where " +
			" t.cod_status_requerimento = 2 "+
			" and a.cod_orgao = "+Orgao.getCodOrgao()+
			" and t.cod_tipo_solic in("+ codTipoSolic +") " +
			" and a.cod_auto_infracao = t.cod_auto_infracao " +
			" order by t.dat_proc_ju";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				dados.add(rs.getString("dat_proc_ju"));
			}
			
			rs.close();
			stmt.close();
			stmt2.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return dados;
	}
	
	
	public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg,Statement stmt) throws sys.DaoException {
		boolean bOk = false ;
		try {
			if ((resultado.substring(0,3).equals("000")==false) &&
					(resultado.substring(0,3).equals("037")==false)) {
				if ( (resultado.substring(0,3).equals("ERR")) || (sys.Util.isNumber(resultado.substring(0,3))==false) ) {
					myInf.setMsgErro(msg+" Erro: "+resultado) ;		
				}
				else {
					myInf.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+resultado.substring(0,3)+"'" ;
					ResultSet rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myInf.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					}
					rs.close();
				}
			}
			else   bOk = true ;
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;  
	}
	public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg) throws sys.DaoException {
		Connection conn = null ;
		boolean bOK = false ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			bOK = verErro(myInf,resultado,msg,stmt);
			stmt.close();		
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOK;
	}
	
	public void setPosIni(int posIni) {
		this.posIni=posIni;
	}
	public int getPosIni() {
		return this.posIni ;
	}
	
	public String getPedaco(String origem,int tam,String dirpad,String carpad) {
		
		if (carpad==null) carpad=" ";
		if (dirpad==null) dirpad="R";
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		if ((result.length()<tam) && (" ".equals(dirpad)==false)) {
			if (dirpad.equals("R")) result=sys.Util.rPad(result,carpad,tam);
			else result=sys.Util.lPad(result,carpad,tam);
		} 
		this.posIni+=tam;		
		return result ;
	}
	public String getPedacoNum(String origem,int tam,int dec,String ptdec) {
		
		if (dec>tam)  		   dec = 0;
		if (ptdec==null)  		ptdec=".";		
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		result = sys.Util.SemZeros(result) ;
		if (sys.Util.isNumber(result)) 	{
			if (result.length()<(dec+1))	{
				if (result.length()<(dec)) result=sys.Util.rPad(result,"0",dec) ;
				if (dec>0)		result="0"+result;
			}
			// colocar ponto decimal
			if (dec>0) result=result.substring(0,result.length()-dec)+ptdec+result.substring(result.length()-dec,result.length());
		}
		else result = null;	
		this.posIni+=tam;
		return result ;
	}
	
	public String getPedacoDt(String origem,int tam) {			
		
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		if ((result.length()<8) && (result.length() > 0)) {
			result=sys.Util.rPad(result," ",8); 
			result = result.substring(6,8)+"/"+ result.substring(4,6)+"/"+result.substring(0,4);
		}
		this.posIni+=tam;				
		return result ;
	}
	
	public String BuscaCodRetornoProc(Connection conn) throws DaoException {
		String vRetorno = "";			
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM TSMI_ORD_CONTROLE_VEX ";            
			sCmd+="ORDER BY DAT_FIM_ULT_PROC  ";			
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				vRetorno = rs.getString("COD_RETORNO");
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
//	Bahia Robot  Recuperacao
	public Vector BuscarListRegRecuperacao(String codRetorno, Connection conn, int maxRegProcessar) throws DaoException {
		Vector vRetorno = new Vector();			
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM TSMI_CONTROLE_VEX_MASTER "+            
			"WHERE COD_RETORNO = '"+codRetorno+"' "+
			"AND DAT_ENVIO>=TO_DATE('01/04/2007','DD/MM/YYYY') AND IND_FATURADO_NOTIFICACAO='N' AND ROWNUM<="+maxRegProcessar+" "+
			"AND (DAT_PROX_RECUPERACAO IS NULL "+
			"OR DAT_PROX_RECUPERACAO<to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')) "+
			" ORDER BY DAT_RETORNO";	
			
			/*
			 sCmd="SELECT T2.NUM_AUTO_INFRACAO,T2.NUM_PLACA,T2.NUM_NOTIFICACAO,T2.SEQ_NOTIFICACAO,T2.TIP_NOTIFICACAO, "+
			 "T2.DAT_RETORNO,T2.PK_COD_CONTROLE_VEX_MASTER FROM MDB_24012008 T1,TSMI_CONTROLE_VEX_MASTER T2 WHERE "+
			 "TO_CHAR(T1.NUM_NOTIFICACAO)=T2.NUM_NOTIFICACAO";
			 */
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				REG.ControleVexBean arqRec = new REG.ControleVexBean();
				arqRec.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				arqRec.setNumPlaca(rs.getString("NUM_PLACA"));
				arqRec.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				arqRec.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));	
				arqRec.setTipNotificacao(rs.getString("TIP_NOTIFICACAO"));
				arqRec.setDatRetorno(sys.Util.converteData(rs.getDate("DAT_RETORNO")));
				arqRec.setPkCodControleVex(rs.getString("PK_COD_CONTROLE_VEX_MASTER"));
				vRetorno.addElement(arqRec);
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	public Vector BuscarListRegRecuperacaoPNT(String codRetorno, Connection conn, int maxRegProcessar) throws DaoException {
		Vector vRetorno = new Vector();
		/*
		 try {        
		 Statement stmt = conn.createStatement();
		 String sCmd ="SELECT * FROM TSMI_CONTROLE_VEX_MASTER "+            
		 "WHERE COD_RETORNO = '"+codRetorno+"' "+
		 "AND DAT_ENVIO>=TO_DATE('01/04/2007','DD/MM/YYYY') AND IND_FATURADO_NOTIFICACAO='N' AND NUM_AUTO_INFRACAO IS NULL AND ROWNUM<="+maxRegProcessar+" "+
		 "AND (DAT_PROX_RECUPERACAO IS NULL "+
		 "OR DAT_PROX_RECUPERACAO<to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')) "+
		 " ORDER BY DAT_RETORNO";			
		 ResultSet rs = stmt.executeQuery(sCmd);
		 while (rs.next()) {
		 REG.ControleVexBean arqRec = new REG.ControleVexBean();
		 arqRec.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
		 arqRec.setNumPlaca(rs.getString("NUM_PLACA"));
		 arqRec.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
		 arqRec.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));	
		 arqRec.setTipNotificacao(rs.getString("TIP_NOTIFICACAO"));
		 arqRec.setDatRetorno(sys.Util.converteData(rs.getDate("DAT_RETORNO")));
		 arqRec.setPkCodControleVex(rs.getString("PK_COD_CONTROLE_VEX_MASTER"));
		 vRetorno.addElement(arqRec);
		 }	
		 
		 rs.close();
		 stmt.close();
		 } catch (Exception e) {			
		 throw new DaoException(e.getMessage());
		 }
		 */
		return vRetorno;
	}
	
	public Vector BuscarListRegRecuperacaoPNTNR(Connection conn, int maxRegProcessar) throws DaoException {
		Vector vRetorno = new Vector();
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM TSMI_CONTROLE_VEX_MASTER "+            
			"WHERE "+
			"DAT_ENVIO>=TO_DATE('01/04/2007','DD/MM/YYYY') AND DAT_RETORNO IS NULL AND NUM_AUTO_INFRACAO IS NULL AND ROWNUM<="+maxRegProcessar+" "+
			"AND (DAT_PROX_RECUPERACAO IS NULL "+
			"OR DAT_PROX_RECUPERACAO<to_date('"+Util.addData(Util.formatedToday().substring(0,10),180)+"','dd/mm/yyyy')) "+
			" ORDER BY NUM_NOTIFICACAO,SEQ_NOTIFICACAO";			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				REG.ControleVexBean arqRec = new REG.ControleVexBean();
				arqRec.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				arqRec.setNumPlaca(rs.getString("NUM_PLACA"));
				arqRec.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				arqRec.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));	
				arqRec.setTipNotificacao(rs.getString("TIP_NOTIFICACAO"));
				arqRec.setDatRetorno(sys.Util.converteData(rs.getDate("DAT_RETORNO")));
				arqRec.setPkCodControleVex(rs.getString("PK_COD_CONTROLE_VEX_MASTER"));
				vRetorno.addElement(arqRec);
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	
	public void GravaRecuperacao(REG.ControleVexBean regConVex,Connection conn,String datProcessamento,String sCodStatus) throws DaoException, SQLException, RobotException{
		String sCmd 	= null;	
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs_VEX     = null;
			String codigoRetorno = "99";
			String ano_Dat_Envio_MST = datProcessamento.substring(6,10);
			String mes_Dat_Envio_MST = datProcessamento.substring(3,5);
			String anoDatRetorno     = datProcessamento.substring(6,10);
			String mesDatRetorno     = datProcessamento.substring(3,5);
			String indEntrega        = "S";
			String indFatura         = "S";
			String codArquivo        = "";
			String codControleVEX_DETAIL="";
			
			sCmd="INSERT INTO TAB_MICHEL ( "+
			"NUM_AUTO_INFRACAO, "+
			"COD_STATUS, "+
			"NUM_NOTIFICACAO," +
			"PK_COD_CONTROLE_VEX_MASTER) VALUES "+
			"('"+regConVex.getNumAutoInfracao()+"',"+
			"'"+sCodStatus+"',"+			
			"'"+regConVex.getNumNotificacao()+"',"+
			"'"+regConVex.getPkCodControleVex()+"')";
			stmt.executeQuery(sCmd);
			
			
			sCmd = "SELECT SEQ_TSMI_CONTROLE_VEX_DETAIL.NEXTVAL CODIGO FROM DUAL";
			rs_VEX=stmt.executeQuery(sCmd);
			if (rs_VEX.next()) codControleVEX_DETAIL=rs_VEX.getString("CODIGO");
			
			sCmd="INSERT INTO TSMI_CONTROLE_VEX_DETAIL ( "+
			"PK_COD_CONTROLE_VEX_MASTER, "+
			"PK_COD_CONTROLE_VEX_DETAIL, "+
			"COD_ARQUIVO, "+
			"DAT_RETORNO, "+
			"COD_RETORNO, "+
			"IND_ENTREGUE_RETORNO, "+
			"IND_FATURADO_RETORNO) VALUES "+
			"('"+regConVex.getPkCodControleVex()+"',"+
			"'"+codControleVEX_DETAIL+"',"+
			"'"+codArquivo+"',"+
			"TO_DATE('"+datProcessamento+"','DD/MM/YYYY'),"+					
			"'"+codigoRetorno+"',"+
			"'"+indEntrega+"',"+
			"'"+indFatura+"')";					
			stmt.executeQuery(sCmd);
			
			sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER SET " +
			"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
			"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+
			"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
			"IND_FATURADO_RECUPERACAO='"+indFatura+"', "+		
			"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
			"DAT_RETORNO_RECUPERACAO = TO_DATE('"+datProcessamento+"','DD/MM/YYYY'), " +
			"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"' "+    
			"WHERE PK_COD_CONTROLE_VEX_MASTER = "+regConVex.getPkCodControleVex();
			stmt.execute(sCmd);
			
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
			"WHERE "+
			"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
			"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			rs_VEX=stmt.executeQuery(sCmd);		
			if (!rs_VEX.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
				"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+ano_Dat_Envio_MST+"',"+
				"'"+mes_Dat_Envio_MST+"',"+
				"'"+anoDatRetorno+"',"+
				"'"+mesDatRetorno+"',"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFatura+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
			"SET QTD_RETORNO=QTD_RETORNO+1 "+
			"WHERE ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
			"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			stmt.close();
			rs_VEX.close();
			
			return;
		} catch (Exception e) {
			System.out.println("Comando :"+sCmd);
			throw new RobotException(e.getMessage());
		}
	}		
	
	
	public void GravaRecuperacaoPNT(REG.ControleVexBean regConVex,Connection conn,String datProcessamento,String sCodStatus) throws DaoException, SQLException, RobotException{
		String sCmd 	= null;	
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs_VEX     = null;
			String codigoRetorno = "99";
			String ano_Dat_Envio_MST = datProcessamento.substring(6,10);
			String mes_Dat_Envio_MST = datProcessamento.substring(3,5);
			
			
/*---------------------------------------------------------------------*/
			
			String anoDatRetorno     = datProcessamento.substring(6,10);
			String mesDatRetorno     = datProcessamento.substring(3,5);
			String indEntrega        = "S";
			String indFatura         = "S";
			String codArquivo        = "";
			String codControleVEX_DETAIL="";
			
			sCmd="INSERT INTO TAB_MICHEL ( "+
			"NUM_AUTO_INFRACAO, "+
			"COD_STATUS, "+
			"NUM_NOTIFICACAO," +
			"PK_COD_CONTROLE_VEX_MASTER) VALUES "+
			"('"+regConVex.getNumAutoInfracao()+"',"+
			"'"+sCodStatus+"',"+			
			"'"+regConVex.getNumNotificacao()+"',"+
			"'"+regConVex.getPkCodControleVex()+"')";
			stmt.executeQuery(sCmd);
			
			
			sCmd = "SELECT SEQ_TSMI_CONTROLE_VEX_DETAIL.NEXTVAL CODIGO FROM DUAL";
			rs_VEX=stmt.executeQuery(sCmd);
			if (rs_VEX.next()) codControleVEX_DETAIL=rs_VEX.getString("CODIGO");
			
			sCmd="INSERT INTO TSMI_CONTROLE_VEX_DETAIL ( "+
			"PK_COD_CONTROLE_VEX_MASTER, "+
			"PK_COD_CONTROLE_VEX_DETAIL, "+
			"COD_ARQUIVO, "+
			"DAT_RETORNO, "+
			"COD_RETORNO, "+
			"IND_ENTREGUE_RETORNO, "+
			"IND_FATURADO_RETORNO) VALUES "+
			"('"+regConVex.getPkCodControleVex()+"',"+
			"'"+codControleVEX_DETAIL+"',"+
			"'"+codArquivo+"',"+
			"TO_DATE('"+datProcessamento+"','DD/MM/YYYY'),"+					
			"'"+codigoRetorno+"',"+
			"'"+indEntrega+"',"+
			"'"+indFatura+"')";					
			stmt.executeQuery(sCmd);
			
			sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER SET " +
			"IND_ENTREGUE_NOTIFICACAO='"+indEntrega+"', "+
			"IND_FATURADO_NOTIFICACAO='"+indFatura+"', "+
			"IND_ENTREGUE_RETORNO='"+indEntrega+"', "+
			"IND_FATURADO_RETORNO='"+indFatura+"', "+		
			"PK_COD_CONTROLE_VEX_DETAIL_RET='"+codControleVEX_DETAIL+"', "+
			"DAT_RETORNO = TO_DATE('"+datProcessamento+"','DD/MM/YYYY'), " +
			"COD_RETORNO ='"+codigoRetorno+"', "+    
			"IND_ENTREGUE_RECUPERACAO='"+indEntrega+"', "+
			"IND_FATURADO_RECUPERACAO='"+indFatura+"', "+		
			"PK_COD_CONTROLE_VEX_DETAIL_REC='"+codControleVEX_DETAIL+"', "+
			"DAT_RETORNO_RECUPERACAO = TO_DATE('"+datProcessamento+"','DD/MM/YYYY'), " +
			"COD_RETORNO_RECUPERACAO='"+codigoRetorno+"' "+    
			"WHERE PK_COD_CONTROLE_VEX_MASTER = "+regConVex.getPkCodControleVex();
			stmt.execute(sCmd);
			
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO "+
			"WHERE "+
			"ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
			"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			rs_VEX=stmt.executeQuery(sCmd);		
			if (!rs_VEX.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO (ANO_ENVIO,"+
				"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+ano_Dat_Envio_MST+"',"+
				"'"+mes_Dat_Envio_MST+"',"+
				"'"+anoDatRetorno+"',"+
				"'"+mesDatRetorno+"',"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFatura+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO "+
			"SET QTD_RETORNO=QTD_RETORNO+1 "+
			"WHERE ANO_ENVIO='"+ano_Dat_Envio_MST+"' AND "+
			"MES_ENVIO='"+mes_Dat_Envio_MST+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			stmt.close();
			rs_VEX.close();
			
			return;
		} catch (Exception e) {
			System.out.println("Comando :"+sCmd);
			throw new RobotException(e.getMessage());
		}
	}		
	
	
	public void AtualizaDataProxRecuperacao(Connection conn,REG.ControleVexBean regConVex,int diasAvancar) throws DaoException{
		try {
			String datProxRecup = sys.Util.addData(sys.Util.formatedToday().substring(0,10),diasAvancar);			
			Statement stmt = conn.createStatement();		
			String  sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER SET " +
			"DAT_PROX_RECUPERACAO = TO_DATE('"+datProxRecup+"','DD/MM/YYYY') " +
			"WHERE PK_COD_CONTROLE_VEX_MASTER = "+regConVex.getPkCodControleVex();			
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	public void GravaInicioProcRecuperacao(Connection conn,String codRetorno) throws DaoException{
		try {			
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_ORD_CONTROLE_VEX SET DAT_INI_ULT_PROC = TO_DATE('"+sys.Util.formatedToday()+"','DD/MM/YYYY:HH24:MI:SS') "
			+ "WHERE COD_RETORNO = "+codRetorno;
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}	
	public void GravaFinalProcRecuperacao(Connection conn,String codRetorno) throws DaoException{
		try {
			Statement stmt = conn.createStatement();			
			String sCmd = "UPDATE TSMI_ORD_CONTROLE_VEX SET DAT_FIM_ULT_PROC = TO_DATE('"+sys.Util.formatedToday()+"','DD/MM/YYYY:HH24:MI:SS') "
			+ "WHERE COD_RETORNO = "+codRetorno;
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/*Bahia 23/09/2008*/
	public void ControleEnvioArVEX(Connection conn) throws SQLException, RobotException 
	{
		
		try {
			
			
			Statement stmt        = conn.createStatement();
			Statement stmt01      = conn.createStatement();			
			Statement stmtSQL     = conn.createStatement();			
			ResultSet rs          = null;
			ResultSet rs01        = null;
			
			boolean testaRecServ = false;
			String sCmd           = "";
			String numNotif       = "";
			String codArDig       = "";
			
			String datEnvio       = "";
			String codigoRetorno  = "";
			String codigoRetornoRel  = "";
			int retornados        = 0;
			int apropriados       = 0;
			int duplicados        = 0;
			int recuperadosServ   = 0;
			int naoEncontrados    = 0;
			int Outros            = 0;
			
			sCmd= "SELECT " +
			"NUM_AUTO_INFRACAO, "+
			"NUM_NOTIFICACAO, "+
			"COD_AR_DIGITALIZADO, "+
			"COD_RETORNO_DETRAN, "+
			"DAT_DIGITALIZACAO "+
			"FROM "+
			"TSMI_AR_DIGITALIZADO_TMP "+
			"WHERE IND_PROC_SOMATORIO IS NULL "+
			"AND COD_RETORNO_DETRAN IS NOT NULL "+
			"ORDER BY DAT_DIGITALIZACAO";
			rs = stmt.executeQuery(sCmd);
			while (rs.next())
			{
				
				
				retornados        = 0;
				apropriados       = 0;
				duplicados        = 0;
				recuperadosServ   = 0;
				naoEncontrados    = 0;
				Outros            = 0;
				sCmd              = "";
				numNotif          = "";
				codArDig          = "";
				
				datEnvio          = "";
				codigoRetorno     = "";
				codigoRetornoRel  = "";
				
				testaRecServ = false;
				
				
				numNotif          = rs.getString("NUM_NOTIFICACAO");
				codArDig          = rs.getString("COD_AR_DIGITALIZADO");
				codigoRetorno     = rs.getString("COD_RETORNO_DETRAN");  
				datEnvio          = Util.converteData(rs.getDate("DAT_DIGITALIZACAO"));
				System.out.println("Processando... Data de Envio:"+datEnvio);
				
				retornados++;
				
				//vejo se a notifica��o foi recuperada por servi�o sen�o v� qual categoria
				try
				{
					testaRecServ = testaRecServArVEX(numNotif, conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
				
				if (testaRecServ){
					recuperadosServ++;
					codigoRetornoRel = "";
				}
				else{
					
					codigoRetornoRel = codigoRetorno;
					if (codigoRetorno.equals("000")) apropriados++;  
					else if (codigoRetorno.equals("632")) naoEncontrados++;  
					else if (codigoRetorno.equals("633")) duplicados++;
					else Outros++;
				}	
				
				
				//vejo se j� existe cabe�alho do relat�rio para essa data
				sCmd="SELECT * FROM TSMI_REL_AR_DIG_TMP "+
				"WHERE DAT_ENVIO=to_date('"+datEnvio+"','dd/mm/yyyy')";
				
				rs01=stmt01.executeQuery(sCmd);		
				if (!rs01.next()){
					
					sCmd="INSERT INTO TSMI_REL_AR_DIG_TMP ("+
					"COD_REL_AR_DIG ," +
					"DAT_ENVIO," +
					"RETORNADO, "+
					"APROPRIADO, "+
					"DUPLICADO, "+
					"RECUPERADOSERV, "+
					"NAOENCONTRADO, "+
					"OUTROS"+
					")VALUES("+
					"SEQ_TSMI_REL_AR_DIG_TMP.nextval, "+
					"to_date('"+datEnvio+"','dd/mm/yyyy'), "+
					""+retornados+", "+
					""+apropriados+", "+
					""+duplicados+", "+
					""+recuperadosServ+", "+
					""+naoEncontrados+", "+
					""+Outros+")";
					stmtSQL.executeQuery(sCmd);					
					
				}else{
					
					
					retornados += rs01.getInt("RETORNADO");
					apropriados += rs01.getInt("APROPRIADO");
					duplicados += rs01.getInt("DUPLICADO");
					recuperadosServ += rs01.getInt("RECUPERADOSERV");
					naoEncontrados += rs01.getInt("NAOENCONTRADO");
					Outros += rs01.getInt("OUTROS");
					
					
					sCmd="UPDATE TSMI_REL_AR_DIG_TMP SET "+
					"RETORNADO = "+ retornados+", "+
					"APROPRIADO = "+ apropriados+", "+
					"DUPLICADO = "+ duplicados+", "+
					"RECUPERADOSERV = "+ recuperadosServ+", "+
					"NAOENCONTRADO = "+ naoEncontrados+", "+
					"OUTROS = "+ Outros+" "+
					"WHERE DAT_ENVIO=to_date('"+datEnvio+"','dd/mm/yyyy')";
					
					stmtSQL.executeQuery(sCmd);					
					
				}
				
				
				//seto o registro como processado
				
				sCmd="UPDATE TSMI_AR_DIGITALIZADO_TMP SET "+
				"IND_PROC_SOMATORIO = 'S', "+
				"COD_RETORNO_DETRAN_REL = '"+codigoRetornoRel+"' "+
				"WHERE COD_AR_DIGITALIZADO="+codArDig;
				
				stmtSQL.executeQuery(sCmd);
				conn.commit();
				
				
				
			}
			
			if (rs!=null)   rs.close();
			if (rs01!=null) rs01.close();
			stmt.close();
			stmt01.close();
			stmtSQL.close();
			return;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		}
	}		
	
	public boolean testaRecServArVEX(String numNotif, Connection conn) throws SQLException, RobotException 
	{
		
		try {
			
			
			Statement stmt        = conn.createStatement();
			ResultSet rs          = null;
			boolean achou         = false;
			
			String sCmd           = "";
			
			sCmd= "SELECT " +
			"COD_ARQUIVO, "+
			"COD_RETORNO, "+
			"COD_RETORNO_RECUPERACAO "+
			"FROM TSMI_CONTROLE_VEX_MASTER "+
			"WHERE NUM_NOTIFICACAO = '"+numNotif+"'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next()){
				String sCodigoRet     =rs.getString("COD_RETORNO");
				if (sCodigoRet==null) sCodigoRet="";
				
				String sCodigoRetRecup=rs.getString("COD_RETORNO_RECUPERACAO");
				if (sCodigoRetRecup==null) sCodigoRetRecup="";
				
				if (sCodigoRet.equals("99") && sCodigoRetRecup.equals("99"))
					achou = true;
				
			}
			
			if (rs!=null)   rs.close();
			stmt.close();
			return achou;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		}
	}		
	
	
	public ArrayList verificaDownloadEfetuado(Connection conn, ACSS.OrgaoBean Orgao,String codIdentArquivo,String dataInicial,String dataFinal) throws DaoException {	
		
		ArrayList dados = new ArrayList();
		
		try {
			ArquivoRecebidoBean relClasse;
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT DISTINCT TO_CHAR(A.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
			" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, NUM_ANO_EXERCICIO, A.QTD_LINHA_PENDENTE, A.QTD_LINHA_CANCELADA" +
			" FROM TSMI_ARQUIVO_RECEBIDO A" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
			" on (a.cod_arquivo=G.cod_arquivo)" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
			" on (a.cod_arquivo=E.cod_arquivo)" +
			" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
			
			sCmd += " AND COD_ORGAO=" + Orgao.getCodOrgao() ;
			sCmd+=" AND A.DAT_RECEBIMENTO BETWEEN '" + dataInicial + "' AND '" + dataFinal + "'" +
			" UNION "+
			"SELECT DISTINCT TO_CHAR(A.DAT_RECEBIMENTO,'DD/MM/YYYY') AS DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
			" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, NUM_ANO_EXERCICIO, A.QTD_LINHA_PENDENTE, A.QTD_LINHA_CANCELADA" +
			" FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP A" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
			" on (a.cod_arquivo=G.cod_arquivo)" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
			" on (a.cod_arquivo=E.cod_arquivo)" +
			" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
			
			//EMITEVEX e EMITEPER
			if ((!"EMITEVEX".equals(codIdentArquivo)) || (!"EMITEPER".equals(codIdentArquivo))) 
				sCmd += " AND COD_ORGAO=" + Orgao.getCodOrgao() ;
			
			sCmd += " AND A.DAT_RECEBIMENTO BETWEEN '" + dataInicial + "' AND '" + dataFinal + "'" +			
			" ORDER BY 1 DESC";         
			ResultSet rs = stmt.executeQuery(sCmd);             
			while (rs.next())   {
				relClasse = new ArquivoRecebidoBean();				
				relClasse.setCodArquivo(rs.getString("cod_arquivo"));
				relClasse.setNomArquivo(rs.getString("nom_arquivo"));
				relClasse.setDatRecebimento(rs.getString("dat_recebimento"));                  
				relClasse.setCodStatus(rs.getString("cod_Status"));
				relClasse.setQtdReg(rs.getString("qtd_reg"));
				relClasse.setQtdLinhaErro(rs.getString("QTD_LINHA_ERRO"));
				relClasse.setDatDownloadGravado(rs.getString("DAT_DOWNLOAD_GRAV"));                 
				relClasse.setDatDownloadErro(rs.getString("DAT_DOWNLOAD_ERRO"));
				relClasse.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));
				relClasse.setQtdLinhaPendente(rs.getString("QTD_LINHA_PENDENTE"));
				relClasse.setQtdLinhaCancelada(rs.getString("QTD_LINHA_CANCELADA"));		
				dados.add(relClasse);           
			}  
			rs.close();
			stmt.close();
			
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return dados;
	}


	/**
	 * Altera��es realizadas para o contrato Perrone
	 * Identificando o tipo do arquivo por Perrone e Vex
	 */
	public boolean ControleEmiteVEXV1(REG.LinhaArquivoRec linhaArq) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);     
			conn.setAutoCommit(false);
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs       = null;
			ResultSet rs_VEX   = null;
			String codArquivo      = "";
			String datEnvio        = "";
			String datGeracao      = "";
			String datLiberacao    = "";
			String datRetorno      = "";
			String codigoRetorno   = "";
			String numAutoInfracao = "";
			String numPlaca        = "";
			String datInfracao     = "";
			String numNotificacao  = "";
			String seqNotificacao  = "";
			String tipNotificacao  = "";
			String codOrgao        = "";
			
			
			String codArquivoAux      = "";
			String datEnvioAux        = "";
			String datGeracaoAux      = "";
			String datLiberacaoAux    = "";
			String numAutoInfracaoAux = "";
			String numPlacaAux        = "";
			String datInfracaoAux     = "";
			String numNotificacaoAux  = "";
			String seqNotificacaoAux  = "";
			String tipNotificacaoAux  = "";
			String codOrgaoAux        = "";
			
			
			String mesDatEnvio  = "";
			String anoDatEnvio  = "";
			
			String mesDatRetorno  = "";
			String anoDatRetorno  = "";
			String codigoRetornoEnvio = "000";
			String indEntrega      = "";
			String indFaturado     = "";
			
			String numCaixa           = "";
			String numLote            = "";
			
			String codControleVEX_MASTER = "";
			String codControleVEX_DETAIL = "";
			String tipReemissao = "";
			
			//Altera��o para o contrato Perrone
			String codIdentArquivo = "";
			
			boolean inclui = true;
			boolean incluiRemi = false;
			boolean altera = false;
			
			String numArq       = "";
			long inumArq        = 0;
			
			try
			{
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				
				sCmd = "SELECT "+
				"T1.COD_ARQUIVO AS COD_ARQUIVO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T1.DAT_RECEBIMENTO AS DAT_GERACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,361,12) AS NUM_AUTO_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,126,7) AS NUM_PLACA, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,213,8) AS DAT_INFRACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,7,9) AS NUM_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,572,1) AS TIP_NOTIFICACAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,516,2) AS COD_UF, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,458,8) AS NUM_CEP, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,226,50) AS NOM_MUNICIPIO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,568,1) AS TIP_REEMISSAO, "+
				"SUBSTR(T2.DSC_LINHA_ARQ_REC,580,6) AS COD_ORGAO, "+
				"T1.COD_IDENT_ARQUIVO "+
				"FROM "+
				"TSMI_ARQUIVO_RECEBIDO T1,TSMI_LINHA_ARQUIVO_REC T2 "+
				"WHERE COD_LINHA_ARQ_REC = "+linhaArq.getCodLinhaArqReq()+" "+
				"AND T1.COD_ARQUIVO=T2.COD_ARQUIVO";
				
				rs = stmt.executeQuery(sCmd);
				if (rs.next())
				{
					codArquivo      = rs.getString("COD_ARQUIVO");
					datEnvio        = sys.Util.formatedToday().substring(0,10);
					datGeracao      = Util.converteData(rs.getDate("DAT_GERACAO"));
					mesDatEnvio     = sys.Util.formatedToday().substring(3,5);
					anoDatEnvio     = sys.Util.formatedToday().substring(6,10);
					datRetorno      = "";
					codigoRetorno   = "";
					indEntrega      = "";
					indFaturado     = "";
					codigoRetornoEnvio = "000";
					numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO").trim();
					numPlaca        = rs.getString("NUM_PLACA").trim();
					datInfracao     = sys.Util.formataDataDDMMYYYY(rs.getString("DAT_INFRACAO"));
					numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					seqNotificacao  = "0";
					tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					tipReemissao    = rs.getString("TIP_REEMISSAO");
					codOrgao        = rs.getString("COD_ORGAO");
					codIdentArquivo = rs.getString("COD_IDENT_ARQUIVO");
				}	
				
				rs.close();
				
				/*Avisos-Retornar
					0= Aviso autua��o 
					1= Notifica��o de autua��o
					2= Aviso de penalidade
					3= Notifica��o de penalidade
					9= Auto antigo 
				 */
				if ( (tipNotificacao.equals("0")) || (tipNotificacao.equals("2")))	{
					datRetorno        = datEnvio;
					codigoRetorno     = "03 ";
					indEntrega        = "S";
					indFaturado       = "S";
					mesDatRetorno     = mesDatEnvio;
					anoDatRetorno     = anoDatEnvio;	
					
				}else{
					
					sCmd = "SELECT " +
					"PK_COD_CONTROLE_VEX_MASTER_V1, " +
					"COD_ARQUIVO, " +
					"DAT_ENVIO, " +
					"DAT_LIBERACAO, " +
					"DAT_GERACAO, " +
					"NUM_AUTO_INFRACAO, " +
					"NUM_PLACA, " +
					"DAT_INFRACAO, " +
					"NUM_NOTIFICACAO, " +
					"SEQ_NOTIFICACAO, " +
					"TIP_NOTIFICACAO, " +
					"COD_ORGAO " +
					"FROM TSMI_CONTROLE_VEX_MASTER_V1 "+
					"WHERE NUM_NOTIFICACAO = '"+numNotificacao+"' "+
					"AND SEQ_NOTIFICACAO = '"+seqNotificacao+"'";
					
					rs=stmt.executeQuery(sCmd);
					if (rs.next()){
						
						codControleVEX_MASTER = rs.getString("PK_COD_CONTROLE_VEX_MASTER_V1");
						codArquivoAux      = rs.getString("COD_ARQUIVO");
						datEnvioAux        = Util.converteData(rs.getDate("DAT_ENVIO"));
						datLiberacaoAux    = Util.converteData(rs.getDate("DAT_LIBERACAO"));
						datGeracaoAux      = Util.converteData(rs.getDate("DAT_GERACAO"));
						numAutoInfracaoAux = rs.getString("NUM_AUTO_INFRACAO").trim();
						numPlacaAux        = rs.getString("NUM_PLACA").trim();
						datInfracaoAux     = Util.converteData(rs.getDate("DAT_INFRACAO"));
						numNotificacaoAux  = rs.getString("NUM_NOTIFICACAO");
						seqNotificacaoAux  = rs.getString("SEQ_NOTIFICACAO");
						tipNotificacaoAux  = rs.getString("TIP_NOTIFICACAO");
						codOrgaoAux        = rs.getString("COD_ORGAO");
						
						inclui = false;
						//altera = true;
						incluiRemi = true;
					}
					
				}
				
				rs.close();
				
				if (altera){
					
					altera_TSMI_CONTROLE_VEX_MASTER_V1(conn,
							codControleVEX_MASTER,
							codArquivo,
							datEnvio,
							numAutoInfracao,
							numPlaca,
							datInfracao,
							numNotificacao,
							seqNotificacao,
							tipNotificacao,
							datGeracao,
							codOrgao);
					
					codArquivo = codArquivoAux;
					numNotificacao = numNotificacaoAux;
					seqNotificacao = seqNotificacaoAux;					
					tipNotificacao = tipNotificacaoAux;
					numAutoInfracao = numAutoInfracaoAux;
					datInfracao = datInfracaoAux;					
					numPlaca = numPlacaAux;
					codOrgao = codOrgaoAux;
					datGeracao = datGeracaoAux;					
					datEnvio = datEnvioAux;					
					datLiberacao = datLiberacaoAux;					
					
				}					
				
				
				if (inclui){
					
					/*Avisos-Retornar*/	
					if ( (tipNotificacao.equals("0")) || (tipNotificacao.equals("2"))){
						//Busca Numero Notifica��o p/ os avisos;
						numNotificacao = buscaNumNotificacaoAviso(conn);
					}
					
					/*Inclui na Tabela de Controle Master*/
					codControleVEX_MASTER = insert_TSMI_CONTROLE_VEX_MASTER_V1(conn,codControleVEX_MASTER,codArquivo,datEnvio,numAutoInfracao,numPlaca,datInfracao,
							numNotificacao,seqNotificacao,tipNotificacao,datGeracao,codOrgao);
					
					/*Avisos-Retornar*/
					if ( (tipNotificacao.equals("0")) ||(tipNotificacao.equals("2"))){
						String statusNew = "18";
						GravaProcRetornadasVexMaster(conn,statusNew,Integer.parseInt(codControleVEX_MASTER));
					}
					
					/*Totalizar Emiss�es*/
					//totalizaEmissoes(conn,
					//          anoDatEnvio,
					//          mesDatEnvio,
					//          codigoRetornoEnvio,
					//          indFaturado);
					
					
					
					/*Totalizar Emiss�es Vers�o 1*/
					totalizaEmissoesV1(conn,
							datEnvio,
							codigoRetornoEnvio,
							indFaturado,
							"QE",
							mesDatEnvio,
							anoDatEnvio,
							codIdentArquivo);
					
					
					
					/*Avisos-Retornar*/					
					if ( (tipNotificacao.equals("0")) ||(tipNotificacao.equals("2"))){
												
						/*Inclui na Tabela de Controle Detalhe*/
						insert_TSMI_AR_DIGITALIZADO(conn,
								numAutoInfracao, 
								numCaixa,
								numLote, 
								numNotificacao,
								datRetorno,
								codigoRetorno);	
						
						
						
						/*Totaliza Retorno Data Emiss�o Vers�o 1*/
						totalizaRetornoDatEmissaoV1(conn,
								codigoRetorno,
								datEnvio,
								datRetorno,
								indFaturado,
								indEntrega,
								mesDatEnvio,
								anoDatEnvio,
								codIdentArquivo);
						
						
						
						/*Totaliza Retorno Data Retorno Vers�o 1*/
						totalizaRetornoDatRetornoV1(conn,
								codigoRetorno,
								datEnvio,
								datRetorno,
								indFaturado,
								indEntrega,
								"QRAV",
								mesDatRetorno,
								anoDatRetorno,
								codIdentArquivo);	
						
						
						//vincula as Notifica��o da MASTER com os Ar's retornados p/ data emiss�o
						totalizaRetornoDatEmissaoDetalheV1(conn,
								"",
								String.valueOf(codControleVEX_MASTER),
								codigoRetorno,
								datEnvio,
								datRetorno,
								indFaturado,
								indEntrega,
								mesDatEnvio,
								anoDatEnvio
						);
						
						//vincula as Notifica��o da MASTER com os Ar's retornados p/ data retorno
						totalizaRetornoDatRetornoDetalheV1(conn,
								"",
								String.valueOf(codControleVEX_MASTER),
								codigoRetorno,
								datRetorno,
								indFaturado,
								indEntrega,
								mesDatRetorno,
								anoDatRetorno
						);	
						
						
						
					}
					
				}
				
				
				if (incluiRemi){
					
					
					/*Inclui na Tabela de Reemiss�o*/
					insert_TSMI_CONT_VEX_MASTER_REMI(conn,
							codControleVEX_MASTER,
							codArquivo,
							datEnvio,
							numAutoInfracao,
							numPlaca,
							datInfracao,
							numNotificacao,
							seqNotificacao,
							tipNotificacao,
							datGeracao,
							codOrgao);
					
					/*Totalizar Emiss�es*/
					//totalizaEmissoes(conn,
					//          anoDatEnvio,
					//          mesDatEnvio,
					//          codigoRetornoEnvio,
					//          indFaturado);
					
					
					
					/*Totalizar Emiss�es Vers�o 1*/
					totalizaEmissoesV1(conn,
							datEnvio,
							codigoRetornoEnvio,
							indFaturado,
							"QR",
							mesDatEnvio,
							anoDatEnvio,
							codIdentArquivo);
					
				}
				
				
			} catch (Exception e) { 
				conn.rollback();
				return false;
			}
			
			conn.commit();
			if (rs_VEX!=null) rs_VEX.close();
			if (rs!=null)     rs.close();
			stmt_VEX.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();			
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}		
	
	/*
	public boolean ControleEmiteVEXPNTV1(ArquivoBean arqRecebido) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);   
			conn.setAutoCommit(false);			
			Statement stmt     = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			String sCmd        = null;
			String sCmd_VEX    = null;			
			ResultSet rs     = null;
			ResultSet rs_VEX = null;
			
			boolean inclui = true;
			boolean incluiRemi = false;
			boolean altera = false;
			
			
			String codArquivo      = "";
			String datEnvio        = "";
			String datGeracao      = "";
			String datLiberacao    = "";
			String datRetorno      = "";
			String codigoRetorno   = "";
			String numAutoInfracao = "";
			String numPlaca        = "";
			String datInfracao     = "";
			String numNotificacao  = "";
			String seqNotificacao  = "";
			String tipNotificacao  = "";
			String codOrgao        = "";
			
			String codArquivoAux      = "";
			String datEnvioAux        = "";
			String datGeracaoAux      = "";
			String datLiberacaoAux    = "";
			String numAutoInfracaoAux = "";
			String numPlacaAux        = "";
			String datInfracaoAux     = "";
			String numNotificacaoAux  = "";
			String seqNotificacaoAux  = "";
			String tipNotificacaoAux  = "";
			String codOrgaoAux        = "";
			
			
			String mesDatEnvio  = "";
			String anoDatEnvio  = "";
			
			String codigoRetornoEnvio = "000";
			String indEntrega      = "";
			String indFaturado     = "";
			
			String codControleVEX_MASTER = "";			
			
			try
			{
				
				REC.DaoBroker dao=REC.DaoBrokerFactory.getInstance();
				sCmd = "SELECT "+ 
				"T1.COD_ARQUIVO AS COD_ARQUIVO, "+
				"T1.NOM_ARQUIVO AS NOM_ARQUIVO, "+
				"T1.DAT_RECEBIMENTO AS DAT_GERACAO, "+
				"T2.NUM_NOTIFICACAO,T2.SEQ_NOTIFICACAO, "+
				"T2.TIP_NOTIFICACAO, "+
				"T3.UF_CNH_CONDUTOR AS COD_UF, "+
				"T3.NUM_CEP_CONDUTOR AS NUM_CEP, "+
				"T3.NOM_MUNIC_CONDUTOR AS NOM_MUNICIPIO "+
				"FROM TPNT_ARQUIVO T1,TPNT_NOTIFICACAO T2, "+
				"TPNT_PROCESSO T3 "+
				"WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "+
				"T2.COD_PROCESSO=T3.COD_PROCESSO AND "+
				"T1.COD_ARQUIVO='"+arqRecebido.getCodArquivo()+"'";
				rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					
					inclui = true;
					
					codArquivo      = rs.getString("COD_ARQUIVO");
					datEnvio        = sys.Util.formatedToday().substring(0,10);
					datLiberacao    = sys.Util.formatedToday().substring(0,10);
					datGeracao      = Util.converteData(rs.getDate("DAT_GERACAO"));
					mesDatEnvio     = sys.Util.formatedToday().substring(3,5);
					anoDatEnvio     = sys.Util.formatedToday().substring(6,10);
					datRetorno      = "";
					codigoRetorno   = "";
					datRetorno      = "";
					codigoRetorno   = "";
					indEntrega      = "";
					indFaturado     = "";
					codigoRetornoEnvio = "000";
					numAutoInfracao = "";
					numNotificacao  = rs.getString("NUM_NOTIFICACAO");
					seqNotificacao  = rs.getString("SEQ_NOTIFICACAO");
					tipNotificacao  = rs.getString("TIP_NOTIFICACAO");
					
					
					sCmd = "SELECT " +
					"PK_COD_CONTROLE_VEX_MASTER_V1, " +
					"COD_ARQUIVO, " +
					"DAT_ENVIO, " +
					"DAT_LIBERACAO, " +
					"DAT_GERACAO, " +
					"NUM_AUTO_INFRACAO, " +
					"NUM_PLACA, " +
					"DAT_INFRACAO, " +
					"NUM_NOTIFICACAO, " +
					"SEQ_NOTIFICACAO, " +
					"TIP_NOTIFICACAO, " +
					"COD_ORGAO " +
					"FROM TSMI_CONTROLE_VEX_MASTER_V1 "+
					"WHERE NUM_NOTIFICACAO = '"+numNotificacao+"' "+
					"AND SEQ_NOTIFICACAO = '"+seqNotificacao+"'";
					
					rs=stmt.executeQuery(sCmd);
					if (rs.next()){
						
						codControleVEX_MASTER = rs.getString("PK_COD_CONTROLE_VEX_MASTER_V1");
						codArquivoAux      = rs.getString("COD_ARQUIVO");
						datEnvioAux        = Util.converteData(rs.getDate("DAT_ENVIO"));
						datLiberacaoAux    = Util.converteData(rs.getDate("DAT_LIBERACAO"));
						datGeracaoAux      = Util.converteData(rs.getDate("DAT_GERACAO"));
						numAutoInfracaoAux = rs.getString("NUM_AUTO_INFRACAO").trim();
						numPlacaAux        = rs.getString("NUM_PLACA").trim();
						datInfracaoAux     = Util.converteData(rs.getDate("DAT_INFRACAO"));
						numNotificacaoAux  = rs.getString("NUM_NOTIFICACAO");
						seqNotificacaoAux  = rs.getString("SEQ_NOTIFICACAO");
						tipNotificacaoAux  = rs.getString("TIP_NOTIFICACAO");
						codOrgaoAux        = rs.getString("COD_ORGAO");
						
						inclui = false;
						altera = true;
						incluiRemi = true;
					}
					
					rs.close();
					
					if (altera){
						altera_TSMI_CONTROLE_VEX_MASTER_V1(conn,
								codControleVEX_MASTER,
								codArquivo,
								datEnvio,
								numAutoInfracao,
								numPlaca,
								datInfracao,
								numNotificacao,
								seqNotificacao,
								tipNotificacao,
								datGeracao,
								codOrgao);
						
						
						
						codArquivo = codArquivoAux;
						numNotificacao = numNotificacaoAux;
						seqNotificacao = seqNotificacaoAux;					
						tipNotificacao = tipNotificacaoAux;
						numAutoInfracao = numAutoInfracaoAux;
						datInfracao = datInfracaoAux;					
						numPlaca = numPlacaAux;
						codOrgao = codOrgaoAux;
						datGeracao = datGeracaoAux;					
						datEnvio = datEnvioAux;					
						datLiberacao = datLiberacaoAux;					
						
					}					
					
					
					if (inclui){
						
						
						//Inclui na Tabela de Controle Master
						codControleVEX_MASTER = insert_TSMI_CONTROLE_VEX_MASTER_V1(conn,
								codControleVEX_MASTER,
								codArquivo,
								datEnvio,
								numAutoInfracao,
								numPlaca,
								datInfracao,
								numNotificacao,
								seqNotificacao,
								tipNotificacao,
								datGeracao,
								codOrgao);
						
						
						totalizaEmissoesV1(conn,
								datEnvio,
								codigoRetornoEnvio,
								indFaturado,
								"QE",
								mesDatEnvio,
								anoDatEnvio);
						
					}	
					
					
					
					if (incluiRemi){
						
						
						//Inclui na Tabela de Reemiss�o
						insert_TSMI_CONT_VEX_MASTER_REMI(conn,
								codControleVEX_MASTER,
								codArquivo,
								datEnvio,
								numAutoInfracao,
								numPlaca,
								datInfracao,
								numNotificacao,
								seqNotificacao,
								tipNotificacao,
								datGeracao,
								codOrgao);
						
						totalizaEmissoesV1(conn,
								datEnvio,
								codigoRetornoEnvio,
								indFaturado,
								"QR",
								mesDatEnvio,
								anoDatEnvio,
								codIdentArquivo);
					}
					
					
				}
				
				conn.commit();
			} catch (Exception e) {
				conn.rollback();
				return false;
			}
			rs.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}	*/
	
	
	public void ConsultaArsRetornadasVex(REC.ARDigitalizadoBean ran,int maxRegProcessar) throws DaoException
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();			
			
			String sCmd = "SELECT * "
				+ "FROM TSMI_AR_DIGITALIZADO_TMP "
				+ "WHERE IND_COD_RET_V1  = 'N' AND ROWNUM<="+maxRegProcessar;
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector listaArq = new Vector();
			while( rs.next() ) {
				REC.ARDigitalizadoBean myBean = new REC.ARDigitalizadoBean();
				myBean.setCodARDigitalizado(rs.getInt("COD_AR_DIGITALIZADO"));				
				myBean.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));				
				myBean.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));				
				myBean.setCodRetorno(rs.getString("NUM_CODIGO_RETORNO"));				
				myBean.setNumLote(rs.getString("NUM_LOTE"));				
				myBean.setNumCaixa(rs.getString("NUM_CAIXA"));	
				myBean.setDatEntrega(Util.converteData(rs.getDate("DAT_ENTREGA")));	
				myBean.setDatDigitalizacao(rs.getDate("DAT_DIGITALIZACAO"));	
				listaArq.addElement(myBean);
			}
			
			ran.setListaArqs( listaArq );
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
	}
	
	
	
	
	public boolean ProcessArsRetornadasVex(REC.ARDigitalizadoBean ran, ACSS.UsuarioBean UsuarioBeanId) throws SQLException, RobotException 
	{
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);   
			conn.setAutoCommit(false);			
			Statement stmt = conn.createStatement();
			ResultSet rs           = null;
			String sCmd            = "";
			String status          = "";
			String statusNew       = "";
			String tipNotificacao  = "";
			String codOrgao        = "";
			int pkNumControle      = 0;
			String dscLinha        = "";
			String vago            = "";
			String resultado       = "";
			String datRetorno      = "";
			String mesDatRetorno   = "";
			String anoDatRetorno   = "";
			String datEnvio        = "";
			String mesDatEnvio     = "";
			String anoDatEnvio     = "";
			String indEntrega      = "S";
			String indFaturado     = "S";
			
			String codRetorno       = "";
			String codRetornoBroker = "";
			String tipoRetorno      = "";
			String codRetornobroker = "";
			String codIdentArquivo  = "";
			
			boolean erro           = false;
			
			try
			{
				ROB.DaoBroker broker = ROB.DaoBroker.getInstance();
				BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();	
				
				sCmd = "SELECT PK_COD_CONTROLE_VEX_MASTER_V1, M.NUM_NOTIFICACAO, M.STATUS, "+
				"M.TIP_NOTIFICACAO, M.COD_ORGAO, M.DAT_ENVIO, R.COD_IDENT_ARQUIVO "+
				"FROM TSMI_CONTROLE_VEX_MASTER_V1 M INNER JOIN TSMI_ARQUIVO_RECEBIDO R ON R.COD_ARQUIVO = M.COD_ARQUIVO "+
				"WHERE NUM_NOTIFICACAO = '"+ran.getNumNotificacao()+"'";
				rs = stmt.executeQuery(sCmd);
				
				if (rs.next())
				{
					codRetornoBroker = "";
					pkNumControle  = rs.getInt("PK_COD_CONTROLE_VEX_MASTER_V1");
					status         = rs.getString("STATUS");
					codIdentArquivo = rs.getString("COD_IDENT_ARQUIVO");// Altera��o para captar o arquivo por tipo
					
					if (status == null) status = "11";
					if (status.equals("")) status = "11";
					
					tipNotificacao = rs.getString("TIP_NOTIFICACAO");
					codOrgao       = rs.getString("COD_ORGAO");
					datEnvio       = Util.converteData(rs.getDate("DAT_ENVIO"));
					mesDatEnvio    = datEnvio.substring(3,5);
					anoDatEnvio    = datEnvio.substring(6,10);
					datRetorno     = ran.getDataDigitalizacaoString().substring(0,10);
					mesDatRetorno  = ran.getDataDigitalizacaoString().substring(3,5);
					anoDatRetorno  = ran.getDataDigitalizacaoString().substring(6,10);
					
					//resultado = "000";
					
					if (DeParaCodRetorno(conn,ran)){
						
						codRetorno  = ran.getCodRetornoVex();
						indEntrega  = ran.getIndEntrega();
						indFaturado = ran.getIndFatura();
						
						//Prepara linha da transa��o 406
						dscLinha="40592CRCR"+ran.getNumNotificacao()+"BRNotificacao Multa"+
						Util.formataDataYYYYMMDD(ran.getDatEntrega())+ran.getCodRetornoVex()+ran.getNumCaixa()+ran.getNumLote()+
						Util.rPad(vago," ",40)+Util.rPad(vago,"0",12)+Util.rPad(vago," ",1)+
						Util.rPad(vago," ",19)+Util.rPad(ran.getNumAutoInfracao()," ",12)+tipNotificacao+codOrgao;
						
						if (status.equals("11")){
							
							resultado = broker.TransacaoRetornoV1(UsuarioBeanId.getNomUserName(),UsuarioBeanId.getCodOrgaoAtuacao(),UsuarioBeanId.getOrgao().getCodOrgao(), dscLinha, conn, transBRKDOL);
							codRetornobroker = resultado.substring(0, 3);
							if (!codRetornoBroker.equals("ERR")){
								
								if (ran.getIndEntrega().equals("S")){
									statusNew = "12"; //Ent C/S
									tipoRetorno = "QR12";
								}
								else if (ran.getIndFatura().equals("N")){
									statusNew = "13"; //Ent S/S Cred = S
									tipoRetorno = "QR13";
								}
								else if (ran.getIndFatura().equals("S")){
									statusNew = "14"; // Ent S/S Cred = N
									tipoRetorno = "QR14";
								}
								
								/*Totaliza Retorno Data Emiss�o Vers�o 1*/
								totalizaRetornoDatEmissaoV1(conn,
										codRetorno,
										datEnvio,
										datRetorno,
										indFaturado,
										indEntrega,
										mesDatEnvio,
										anoDatEnvio,
										codIdentArquivo);
								
								
								/*Totaliza Retorno Data Retorno Vers�o 1*/
								totalizaRetornoDatRetornoV1(conn,
										codRetorno,
										datEnvio,
										datRetorno,
										indFaturado,
										indEntrega,
										tipoRetorno,
										mesDatRetorno,
										anoDatRetorno,
										codIdentArquivo);
								
								
								
								//vincula as Notifica��o da MASTER com os Ar's retornados p/ data emiss�o
								totalizaRetornoDatEmissaoDetalheV1(conn,
										String.valueOf(ran.getCodARDigitalizado()),
										String.valueOf(pkNumControle),
										codRetorno,
										datEnvio,
										datRetorno,
										indFaturado,
										indEntrega,
										mesDatEnvio,
										anoDatEnvio);
								
								//vincula as Notifica��o da MASTER com os Ar's retornados p/ data retorno
								totalizaRetornoDatRetornoDetalheV1(conn,
										String.valueOf(ran.getCodARDigitalizado()),
										String.valueOf(pkNumControle),
										codRetorno,
										datRetorno,
										indFaturado,
										indEntrega,
										mesDatRetorno,
										anoDatRetorno
								);	
								
								GravaProcRetornadasVexTMP(conn,"A",ran.getCodARDigitalizado());
								GravaProcRetornadasVexMaster(conn,statusNew,pkNumControle);
								/*Inclui na Tabela de Controle Detalhe*/
								insert_TSMI_AR_DIGITALIZADO(conn,
										ran.getNumAutoInfracao(), 
										ran.getNumCaixa(),
										ran.getNumLote(), 
										ran.getNumNotificacao(),
										ran.getDataDigitalizacaoString(),
										codRetorno);	
								
								
							}else{
								GravaProcRetornadasVexTMP(conn,"N",ran.getCodARDigitalizado());
							}
							
							
						}else if (status.equals("12") || status.equals("15") || status.equals("16") || status.equals("17") || status.equals("18") ){
							
							GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
							/*Totaliza Retorno Data Retorno Vers�o 1*/
							totalizaRetornoDatRetornoV1(conn,
									codRetorno,
									datEnvio,
									datRetorno,
									indFaturado,
									indEntrega,
									"QD",
									mesDatRetorno,
									anoDatRetorno,
									codIdentArquivo
									);
							
							
						}else if (status.equals("13")){
							
							if (ran.getIndEntrega().equals("S")){
								resultado = broker.TransacaoRetornoV1(UsuarioBeanId.getNomUserName(),UsuarioBeanId.getCodOrgaoAtuacao(),UsuarioBeanId.getOrgao().getCodOrgao(), dscLinha, conn, transBRKDOL);
								codRetornobroker = resultado.substring(0, 3);
								if (!codRetornoBroker.equals("ERR")){
									statusNew = "15";
									
									codRetorno = "98";
									
									GravaProcRetornadasVexTMP(conn,"A",ran.getCodARDigitalizado());
									GravaProcRetornadasVexMaster(conn,statusNew,pkNumControle);
									/*Inclui na Tabela de Controle Detalhe*/
									insert_TSMI_AR_DIGITALIZADO(conn,
											ran.getNumAutoInfracao(), 
											ran.getNumCaixa(),
											ran.getNumLote(), 
											ran.getNumNotificacao(),
											ran.getDataDigitalizacaoString(),
											codRetorno);	
									
									/*Totaliza Retorno Data Retorno*/
									totalizaRetornoDatRetornoV1(conn,
											codRetorno,
											datEnvio,
											datRetorno,
											indFaturado,
											indEntrega,
											"QRR",
											mesDatRetorno,
											anoDatRetorno,
											codIdentArquivo);
															
									
									
									//vincula as Notifica��o da MASTER com os Ar's retornados p/ data retorno
									totalizaRetornoDatRetornoDetalheV1(conn,
											String.valueOf(ran.getCodARDigitalizado()),
											String.valueOf(pkNumControle),
											codRetorno,
											datRetorno,
											indFaturado,
											indEntrega,
											mesDatRetorno,
											anoDatRetorno
									);	
									
									
									
								}else{
									GravaProcRetornadasVexTMP(conn,"N",ran.getCodARDigitalizado());
								}
								
							}else{
								GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
								/*Totaliza Retorno Data Retorno Vers�o 1*/
								totalizaRetornoDatRetornoV1(conn,
										codRetorno,
										datEnvio,
										datRetorno,
										indFaturado,
										indEntrega,
										"QD",
										mesDatRetorno,
										anoDatRetorno,
										codIdentArquivo); 
							}
							
						}else if (status.equals("14")){
							if (ran.getIndEntrega().equals("S")){
								resultado = broker.TransacaoRetornoV1(UsuarioBeanId.getNomUserName(),UsuarioBeanId.getCodOrgaoAtuacao(),UsuarioBeanId.getOrgao().getCodOrgao(), dscLinha, conn, transBRKDOL);
								codRetornobroker = resultado.substring(0, 3);
								if (!codRetornoBroker.equals("ERR")){
									statusNew = "17";
									
									GravaProcRetornadasVexMaster(conn,statusNew,pkNumControle);
									GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
									/*Totaliza Retorno Data Retorno Vers�o 1*/
									totalizaRetornoDatRetornoV1(conn,
											codRetorno,
											datEnvio,
											datRetorno,
											indFaturado,
											indEntrega,
											"QD",
											mesDatRetorno,
											anoDatRetorno,
											codIdentArquivo); //TODO:Testar
									
								}else{
									GravaProcRetornadasVexTMP(conn,"N",ran.getCodARDigitalizado());
								}
								
							}else{
								
								GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
								/*Totaliza Retorno Data Retorno Vers�o 1*/
								totalizaRetornoDatRetornoV1(conn,
										codRetorno,
										datEnvio,
										datRetorno,
										indFaturado,
										indEntrega,
										"QD",
										mesDatRetorno,
										anoDatRetorno,
										codIdentArquivo
										);						
							}
							
						}						
						
						
					}else{
						
						/*Totaliza Retorno Data Retorno Vers�o 1*/
						totalizaRetornoDatRetornoV1(conn,
								codRetorno,
								datEnvio,
								datRetorno,
								indFaturado,
								indEntrega,
								"QD",
								mesDatRetorno,
								anoDatRetorno,
								codIdentArquivo);
									
						GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
					}
					
				}
//				else{
//					
//					/*Totaliza Retorno Data Retorno Vers�o 1*/
//					totalizaRetornoDatRetornoV1(conn,
//							codRetorno,
//							datEnvio,
//							datRetorno,
//							indFaturado,
//							indEntrega,
//							"QD",
//							mesDatRetorno,
//							anoDatRetorno,
//							//codIdentArquivo
//							);					
//					
//					GravaProcRetornadasVexTMP(conn,"D",ran.getCodARDigitalizado());
//				}
				
				conn.commit();
			} catch (Exception e) {
				conn.rollback();
				return false;
			}
			rs.close();
			stmt.close();
			return true;
		} catch (Exception e) {
			conn.rollback();
			return false;
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}
	}	
	
	
	public void GravaProcRetornadasVexTMP(Connection conn,String indcodProcess,int pkId) throws DaoException{
		try {
			int diasProcess = 0;
			String datProcess   = sys.Util.formatedToday().substring(0,10);
			Statement stmt = conn.createStatement();	
			
			String sCmd = "UPDATE TSMI_AR_DIGITALIZADO_TMP SET "
				+ "IND_COD_RET_V1 = '" + indcodProcess + "', "
				+ "DAT_PROC = TO_DATE('"+datProcess+"','DD/MM/YYYY') ";
			sCmd +=  "WHERE COD_AR_DIGITALIZADO = " + pkId;
			
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	
	
	public boolean DeParaCodRetorno(Connection conn,REC.ARDigitalizadoBean ran) throws DaoException{
		
		String codRetornoVEX = "";
		String sCmd_VEX      = "";
		String sCmd          = "";
		String indFatura     = "";
		String indEntregue   = "";
		ResultSet rs_VEX     = null;
		boolean achou = false;
		
		try {
			
			Statement stmt = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();
			
			sCmd_VEX =  "SELECT CR.NUM_CODIGO_RETORNO, CR.IND_FATURA, CR.IND_ENTREGUE " +
			"FROM TSMI_CODIGO_RETORNO CR, TSMI_CODIGO_RETORNO_VEX CRV " +
			"WHERE "+
			"CRV.NUM_CODIGO_RETORNO_VEX='"+ran.getCodRetorno()+"' "+
			"AND CR.COD_CODIGO_RETORNO = CRV.FK_COD_CODIGO_RETORNO ";
			rs_VEX    = stmt_VEX.executeQuery(sCmd_VEX) ;			
			if (rs_VEX.next()) {
				
				achou = true;
				codRetornoVEX =rs_VEX.getString("NUM_CODIGO_RETORNO");
				indFatura = rs_VEX.getString("IND_FATURA");
				indEntregue = rs_VEX.getString("IND_ENTREGUE");
			}
			
			if (achou){
				sCmd  = "UPDATE TSMI_AR_DIGITALIZADO_TMP "+
				"SET "+
				"CODIGO_RETORNO_VEX='"+codRetornoVEX+"' "+
				"WHERE COD_AR_DIGITALIZADO='"+ran.getCodARDigitalizado()+"'";
				stmt.execute(sCmd);
				ran.setCodRetornoVex(codRetornoVEX);
				ran.setIndFatura(indFatura);
				ran.setIndEntrega(indEntregue);
			}
			
			stmt_VEX.close();
			stmt.close();
			return achou ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	public static String insert_TSMI_CONTROLE_VEX_MASTER_V1(Connection conn,String pkCodcontroleVexMax,String codArquivo,String datEnvio,String numAutoInfracao,
			String numPlaca,String datInfracao,String numNotificacao,String seqNotificacao,String tipNotificacao,String datGeracao,String codOrgao) throws Exception, DaoException{
		
		String sCmd     = "";
		Statement stmt  = conn.createStatement();
		ResultSet rs    = null;
		
		try{
			
			sCmd = "SELECT SEQ_TSMI_CONT_VEX_MASTER_V1.NEXTVAL CODIGO FROM DUAL";
			rs=stmt.executeQuery(sCmd);
			if (rs.next()) pkCodcontroleVexMax=rs.getString("CODIGO");
			
			
			sCmd="INSERT INTO TSMI_CONTROLE_VEX_MASTER_V1 "+
			"(" +
			"PK_COD_CONTROLE_VEX_MASTER_V1, "+
			"COD_ARQUIVO, "+
			"NUM_NOTIFICACAO, "+
			"SEQ_NOTIFICACAO, "+
			"TIP_NOTIFICACAO, "+
			"NUM_AUTO_INFRACAO, "+
			"DAT_INFRACAO, "+
			"NUM_PLACA, "+
			"COD_ORGAO, "+
			"DAT_GERACAO, "+
			"DAT_ENVIO) VALUES "+ 
			"('"+pkCodcontroleVexMax+"',"+
			"'"+codArquivo+"',"+
			"'"+numNotificacao+"',"+
			"'"+seqNotificacao+"',"+					
			"'"+tipNotificacao+"',"+
			"'"+numAutoInfracao+"',"+
			"TO_DATE('"+datInfracao+"','DD/MM/YYYY'),"+					
			"'"+numPlaca+"',"+
			"'"+codOrgao+"',"+
			"TO_DATE('"+datGeracao+"','DD/MM/YYYY'),"+					
			"TO_DATE('"+datEnvio+"','DD/MM/YYYY'))";					
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}catch(SQLException e){
			
			System.out.println("Erro na fun��o: insert_TSMI_CONTROLE_VEX_MASTER_V1(): " + e.getMessage());
			throw new RobotException(e.getMessage());
		}
		
		return pkCodcontroleVexMax;
			
	}
	
	public static void insert_TSMI_CONTROLE_VEX_DETAIL_V1(Connection conn,
			String codControleVEX_DETAIL,
			String codControleVEX_MASTER,
			String datRetorno,
			String codigoRetorno) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			sCmd = "SELECT SEQ_TSMI_CONT_VEX_DETAIL_V1.NEXTVAL CODIGO FROM DUAL";
			rs=stmt.executeQuery(sCmd);
			if (rs.next()) codControleVEX_DETAIL=rs.getString("CODIGO");
			
			
			sCmd="INSERT INTO TSMI_CONTROLE_VEX_DETAIL_V1 ( "+
			"PK_COD_CONTROLE_VEX_DETAIL_V1, "+
			"PK_COD_CONTROLE_VEX_MASTER_V1, "+
			"DAT_RETORNO, "+
			"COD_RETORNO) VALUES "+
			"('"+codControleVEX_DETAIL+"',"+
			"'"+codControleVEX_MASTER+"',"+
			"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+					
			"'"+codigoRetorno+"')";					
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: insert_TSMI_CONTROLE_VEX_DETAIL_V1(): " + e.getMessage());
		}
			}
	
	public static void insert_TSMI_CONT_VEX_MASTER_REMI(Connection conn,
			String codControleVEX_REMI,
			String codArquivo,
			String datEnvio,
			String numAutoInfracao,
			String numPlaca,
			String datInfracao,
			String numNotificacao,
			String seqNotificacao,
			String tipNotificacao,
			String datGeracao,
			String codOrgao) throws Exception, DaoException
			{
		String sCmd     = "";
		Statement stmt  = conn.createStatement();
		ResultSet rs    = null;
		
		try
		{
			
			sCmd = "SELECT SEQ_TSMI_CONT_VEX_MASTER_REMI.NEXTVAL CODIGO FROM DUAL";
			rs=stmt.executeQuery(sCmd);
			if (rs.next()) codControleVEX_REMI=rs.getString("CODIGO");
			
			
			sCmd="INSERT INTO TSMI_CONT_VEX_MASTER_REMI "+
			"(" +
			"PK_COD_CONT_VEX_MASTER_REMI, "+
			"COD_ARQUIVO, "+
			"NUM_NOTIFICACAO, "+
			"SEQ_NOTIFICACAO, "+
			"TIP_NOTIFICACAO, "+
			"NUM_AUTO_INFRACAO, "+
			"DAT_INFRACAO, "+
			"NUM_PLACA, "+
			"COD_ORGAO, "+
			"DAT_GERACAO, "+
			"DAT_ENVIO) VALUES "+ 
			"('"+codControleVEX_REMI+"',"+
			"'"+codArquivo+"',"+
			"'"+numNotificacao+"',"+
			"'"+seqNotificacao+"',"+					
			"'"+tipNotificacao+"',"+
			"'"+numAutoInfracao+"',"+
			"TO_DATE('"+datInfracao+"','DD/MM/YYYY'),"+					
			"'"+numPlaca+"',"+
			"'"+codOrgao+"',"+
			"TO_DATE('"+datGeracao+"','DD/MM/YYYY'),"+					
			"TO_DATE('"+datEnvio+"','DD/MM/YYYY'))"+					
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: insert_TSMI_CONT_VEX_MASTER_REMI(): " + e.getMessage());
			
		}
			}
	
	
	public static void altera_TSMI_CONTROLE_VEX_MASTER_V1(Connection conn,
			String pkCodcontroleVexMax,
			String codArquivo,
			String datEnvio,
			String numAutoInfracao,
			String numPlaca,
			String datInfracao,
			String numNotificacao,
			String seqNotificacao,
			String tipNotificacao,
			String datGeracao,
			String codOrgao) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		
		try
		{
			
			sCmd="UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET "+
			"COD_ARQUIVO = '"+codArquivo+"', "+
			"NUM_NOTIFICACAO = '"+numNotificacao+"', "+
			"SEQ_NOTIFICACAO = '"+seqNotificacao+"', "+
			"TIP_NOTIFICACAO = '"+tipNotificacao+"', "+
			"NUM_AUTO_INFRACAO = '"+numAutoInfracao+"', "+
			"DAT_INFRACAO = TO_DATE('"+datInfracao+"','DD/MM/YYYY'), "+
			"NUM_PLACA = '"+numPlaca+"', "+
			"COD_ORGAO = '"+codOrgao+"', "+
			"DAT_GERACAO = TO_DATE('"+datGeracao+"','DD/MM/YYYY'), "+
			"DAT_ENVIO = TO_DATE('"+datEnvio+"','DD/MM/YYYY')) "+
			"WHERE PK_COD_CONTROLE_VEX_MASTER_V1 = '"+pkCodcontroleVexMax+"' ";
			
			stmt.executeQuery(sCmd);
			
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: altera_TSMI_CONTROLE_VEX_MASTER_V1(): " + e.getMessage());
			
		}
			}
	
	
	public static void totalizaEmissoes(Connection conn,
			String anoDatEnvio,
			String mesDatEnvio,
			String codigoRetornoEnvio,
			String indFaturado) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Emiss�es*/
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"COD_RETORNO='"+codigoRetornoEnvio+"'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO_V1 (ANO_ENVIO,"+
				"MES_ENVIO,COD_RETORNO,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+anoDatEnvio+"',"+
				"'"+mesDatEnvio+"',"+
				"'"+codigoRetornoEnvio+"',"+
				"'"+indFaturado+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"SET "+
			"QTD_EMITIDO=QTD_EMITIDO+1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"COD_RETORNO='"+codigoRetornoEnvio+"'";
			stmt.executeQuery(sCmd);					
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaEmissoes(): " + e.getMessage());
		}
			}
	
	
	public static void totalizaRetornoDatEmissao(Connection conn,
			String anoDatEnvio,
			String mesDatEnvio,
			String anoDatRetorno,
			String mesDatRetorno,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Retornos-por data de emiss�o*/
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO_V1 (ANO_ENVIO,"+
				"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+anoDatEnvio+"',"+
				"'"+mesDatEnvio+"',"+
				"'"+anoDatRetorno+"',"+
				"'"+mesDatRetorno+"',"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			int iDias=Util.DifereDias(datEnvio,datRetorno);						
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"SET "+
			"QTD_EMITIDO_RETORNO=QTD_EMITIDO_RETORNO+1, "+
			"QTD_DIAS_RETORNO=QTD_DIAS_RETORNO+"+iDias+" "+						
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaEmissoes(): " + e.getMessage());
		}
			}
	
	public static void totalizaRetornoDatRetorno(Connection conn,
			String anoDatEnvio,
			String mesDatEnvio,
			String anoDatRetorno,
			String mesDatRetorno,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Retornos-por data de retorno*/
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO_V1 (ANO_ENVIO,"+
				"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+anoDatEnvio+"',"+
				"'"+mesDatEnvio+"',"+
				"'"+anoDatRetorno+"',"+
				"'"+mesDatRetorno+"',"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"SET "+
			"QTD_RETORNO=QTD_RETORNO+1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaEmissoes(): " + e.getMessage());
		}
			}
	
	
	
	public static String buscaNumNotificacaoAviso(Connection conn) throws Exception, DaoException
	{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		String numNotificacao = "";
		long inumArq = 0;
		
		try
		{
			
			//Pego Sequencia do Arquivo
			sCmd ="SELECT VAL_PARAMETRO ";
			sCmd+="FROM TCAU_PARAM_SISTEMA ";
			sCmd+="WHERE NOM_PARAMETRO = 'NUM_NOTIF_AVISO' ";
			rs=stmt.executeQuery(sCmd);
			if (rs.next()){
				inumArq = rs.getLong("VAL_PARAMETRO");
				numNotificacao = String.valueOf(inumArq);
			}
			rs.close();
			
			//Incremento Sequencia do Num. da notifica�a� do Aviso
			sCmd = "UPDATE TCAU_PARAM_SISTEMA SET VAL_PARAMETRO='"+(inumArq+1)+"' ";								  
			sCmd+= "WHERE NOM_PARAMETRO = 'NUM_NOTIF_AVISO' ";
			stmt.executeUpdate(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: insert_TSMI_CONTROLE_VEX_DETAIL_V1(): " + e.getMessage());
		}
		
		return numNotificacao;	
	}
	
	public static void totalizaRetornoDatRetornoRecupera(Connection conn,
			String anoDatEnvio,
			String mesDatEnvio,
			String anoDatRetorno,
			String mesDatRetorno,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Retornos-por data de retorno/recupera��o*/
			sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONTROLE_VEX_RESUMO_V1 (ANO_ENVIO,"+
				"MES_ENVIO,ANO_RETORNO,MES_RETORNO,COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_RETORNO,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT," +
				"QTD_DIAS_RETORNO) VALUES ("+
				"'"+anoDatEnvio+"',"+
				"'"+mesDatEnvio+"',"+
				"'"+anoDatRetorno+"',"+
				"'"+mesDatRetorno+"',"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',0,0,0,0,0,0)";						
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONTROLE_VEX_RESUMO_V1 "+
			"SET "+
			"QTD_RECUPERADA_RETORNO=QTD_RECUPERADA_RETORNO+1 "+
			"WHERE "+
			"ANO_ENVIO='"+anoDatEnvio+"' AND "+
			"MES_ENVIO='"+mesDatEnvio+"' AND "+
			"ANO_RETORNO='"+anoDatRetorno+"' AND "+
			"MES_RETORNO='"+mesDatRetorno+"' AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaEmissoes(): " + e.getMessage());
		}
			}
	
	/**************************/
	
	public void GravaProcRetornadasVexTMP(Connection conn,String indcodProcess,String DscindcodProcess,int pkId) throws DaoException{
		try {
			
			String datProcess   = sys.Util.formatedToday().substring(0,10);
			Statement stmt = conn.createStatement();			
			String sCmd = "UPDATE TSMI_AR_DIGITALIZADO_TMP SET "
				+ "IND_COD_RET_V1 = '" + indcodProcess + "', "
				+ "DSC_IND_COD_RET_V1 = '" + DscindcodProcess + "', "
				+ "DAT_PROC = TO_DATE('"+datProcess+"','DD/MM/YYYY') "			
				+ "WHERE COD_AR_DIGITALIZADO = " + pkId;
			
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	
	public void incluiProcRetornadasVexDetail(Connection conn,String datRetorno, String codRetorno, int pkId) throws DaoException{
		try {
			
			String datStatus   = sys.Util.formatedToday().substring(0,10);
			Statement stmt = conn.createStatement();			
			String sCmd="INSERT INTO TSMI_CONTROLE_VEX_DETAIL_V1 ( "+
			"PK_COD_CONTROLE_VEX_DETAIL_V1, "+
			"PK_COD_CONTROLE_VEX_MASTER_V1, "+
			"DAT_RETORNO, "+
			"COD_RETORNO) VALUES "+
			"(SEQ_TSMI_CONT_VEX_DETAIL_V1.NEXTVAL,"+
			"'"+pkId+"',"+
			"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+					
			"'"+codRetorno+"')";					
			stmt.executeQuery(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	
	public Vector BuscarListRegRecuperacaoV1(String codRetorno, Connection conn, int maxRegProcessar) throws DaoException {
		Vector vRetorno = new Vector();			
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM TSMI_CONTROLE_VEX_MASTER_V1 "+            
			"WHERE STATUS = '13' "+
			"AND DAT_ENVIO>=TO_DATE('01/04/2007','DD/MM/YYYY') AND ROWNUM<="+maxRegProcessar+" "+
			"AND (DAT_PROX_RECUPERACAO IS NULL "+
			"OR DAT_PROX_RECUPERACAO<to_date('"+Util.formatedToday().substring(0,10)+"','dd/mm/yyyy')) ";
			/*"AND IND_CONTADOR <= 6 "+*/	
			sCmd+=" ORDER BY DAT_RETORNO";	
			
			/*
			 sCmd="SELECT T2.NUM_AUTO_INFRACAO,T2.NUM_PLACA,T2.NUM_NOTIFICACAO,T2.SEQ_NOTIFICACAO,T2.TIP_NOTIFICACAO, "+
			 "T2.DAT_RETORNO,T2.PK_COD_CONTROLE_VEX_MASTER FROM MDB_24012008 T1,TSMI_CONTROLE_VEX_MASTER T2 WHERE "+
			 "TO_CHAR(T1.NUM_NOTIFICACAO)=T2.NUM_NOTIFICACAO";
			 */
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				REG.ControleVexBean arqRec = new REG.ControleVexBean();
				arqRec.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				arqRec.setNumPlaca(rs.getString("NUM_PLACA"));
				arqRec.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				arqRec.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));	
				arqRec.setTipNotificacao(rs.getString("TIP_NOTIFICACAO"));
				arqRec.setDatEnvio(sys.Util.converteData(rs.getDate("DAT_ENVIO")));
				arqRec.setDatRetorno(sys.Util.converteData(rs.getDate("DAT_RETORNO")));
				arqRec.setPkCodControleVex(rs.getString("PK_COD_CONTROLE_VEX_MASTER_V1"));
				arqRec.setIndContador(rs.getString("IND_CONTADOR"));
				vRetorno.addElement(arqRec);
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	
	/*
	public void GravaRecuperacaoV1(REG.ControleVexBean regConVex,Connection conn,String datProcessamento,String sCodStatus) throws DaoException, SQLException, RobotException{
		
		try {
			
			
			String codigoRetorno     = "99";
			String statusNew         = "16";
			String DatRetorno        = datProcessamento.substring(0,10);
			String anoDatRetorno     = datProcessamento.substring(6,10);
			String mesDatRetorno     = datProcessamento.substring(3,5);
			String indEntrega        = "S";
			String indFatura         = "S";
			
			
			GravaProcRetornadasVexMaster(conn,statusNew,Integer.parseInt(regConVex.getPkCodControleVex()));
			
			
			insert_TSMI_AR_DIGITALIZADO(conn,
					regConVex.getNumAutoInfracao(), 
					"",
					"", 
					regConVex.getNumNotificacao(),
					DatRetorno,
					codigoRetorno);	
			
			//Totaliza Retorno Data Retorno
			totalizaRetornoDatRetornoV1(conn,
					codigoRetorno,
					regConVex.getDatEnvio(),
					DatRetorno,
					indFatura,
					indEntrega,
					"QRS",
					mesDatRetorno,
					anoDatRetorno);		
			
			
			//vincula as Notifica��o da MASTER com os Ar's retornados p/ data retorno
			totalizaRetornoDatRetornoDetalheV1(conn,
					"",
					String.valueOf(regConVex.getPkCodControleVex()),
					codigoRetorno,
					DatRetorno,
					indFatura,
					indEntrega,
					mesDatRetorno,
					anoDatRetorno
			);	
			
			
			
			
			return;
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}		
	
	*/
	
	public void GravaProcRetornadasVexMaster(Connection conn,String status,int pkId) throws DaoException{
		try {
			
			String datStatus   = sys.Util.formatedToday().substring(0,10);
			Statement stmt = conn.createStatement();			
			String sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET ";
			sCmd += "STATUS = '" + status + "', "
			+ "DAT_STATUS = TO_DATE('"+datStatus+"','DD/MM/YYYY') "			
			+ "WHERE PK_COD_CONTROLE_VEX_MASTER_V1 = " + String.valueOf(pkId);
			
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	public void AtualizaDataProxRecuperacaoV1(Connection conn,REG.ControleVexBean regConVex,int diasAvancar) throws DaoException{
		try {
			
			int indContador = Integer.parseInt(regConVex.getIndContador());
			
			indContador++;
			
			String datProxRecup = sys.Util.addData(sys.Util.formatedToday().substring(0,10),(diasAvancar*indContador));			
			Statement stmt = conn.createStatement();		
			String  sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET " +
			"DAT_PROX_RECUPERACAO = TO_DATE('"+datProxRecup+"','DD/MM/YYYY'), " +
			"IND_CONTADOR = "+indContador+" " +
			"WHERE PK_COD_CONTROLE_VEX_MASTER_V1 = "+regConVex.getPkCodControleVex();			
			stmt.execute(sCmd);
			stmt.close();
			return ;
		}	
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 * Altera��es realizadas no m�todo para o contrato Perrone
	 * 01/06/2012
	 * Identificador do Arquivo, diferenciando a VEX da Perrone
	 */
	public static void totalizaEmissoesV1(Connection conn,
			String DatEnvio,
			String codigoRetornoEnvio,
			String indFaturado,
			String tipoEnv,
			String mesDatEnvio,
			String anoDatEnvio,
			String codIdentArquivo) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Emiss�es*/
			sCmd="SELECT * FROM TSMI_CONT_VEX_TOT_ENV_V1 "+
			"WHERE "+
			"DAT_ENVIO="+"TO_DATE('"+DatEnvio+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetornoEnvio+ "' AND " +
			"COD_IDENT_ARQUIVO='" +codIdentArquivo+ "'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONT_VEX_TOT_ENV_V1 (DAT_ENVIO,"+
				"COD_RETORNO,IND_FATURADO,MES_ENVIO,ANO_ENVIO,QTD_EMITIDO,QTD_EMITIDO_RETORNO,QTD_REEMISSAO,COD_IDENT_ARQUIVO" +
				") VALUES ("+
				"TO_DATE('"+DatEnvio+"','DD/MM/YYYY'),"+
				"'"+codigoRetornoEnvio+"',"+
				"'"+indFaturado+"',"+
				"'"+mesDatEnvio+"',"+
				"'"+anoDatEnvio+"',0,0,0," +
				"'"+codIdentArquivo+"')"; //Adicionado campo novo para identificador do arquivo
				stmt.executeQuery(sCmd);					
			}
			sCmd="UPDATE TSMI_CONT_VEX_TOT_ENV_V1 "+
			"SET ";
			if (tipoEnv.equals("QE"))
				sCmd += "QTD_EMITIDO=QTD_EMITIDO+1 ";
			if (tipoEnv.equals("QR"))
				sCmd += "QTD_REEMISSAO=QTD_REEMISSAO+1 ";
			sCmd += "WHERE "+
			"DAT_ENVIO="+"TO_DATE('"+DatEnvio+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetornoEnvio+"'";
			stmt.executeQuery(sCmd);					
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaEmissoesV1: " + e.getMessage());
		}
	}
	
	
	public static void insert_TSMI_AR_DIGITALIZADO(Connection conn,
			String numAutoInfracao,
			String numCaixa,
			String numLote,
			String numNotificacao,
			String datDigitalizacao,
			String codigoRetorno) throws Exception, DaoException
			{
		String sCmd    = "";
		String codArDigitalizado    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			sCmd = "SELECT SEQ_TSMI_AR_DIGITALIZADO.NEXTVAL CODIGO FROM DUAL";
			rs=stmt.executeQuery(sCmd);
			if (rs.next()) codArDigitalizado=rs.getString("CODIGO");
			
			
			sCmd="INSERT INTO TSMI_AR_DIGITALIZADO ( "+
			"COD_AR_DIGITALIZADO, "+
			"NUM_AUTO_INFRACAO, "+
			"NUM_LOTE, "+
			"NUM_CAIXA, "+
			"NUM_NOTIFICACAO, "+
			"DAT_DIGITALIZACAO, "+
			"COD_RETORNO) VALUES "+
			"('"+codArDigitalizado+"',"+
			"'"+numAutoInfracao+"',"+
			"'"+numLote+"',"+
			"'"+numCaixa+"',"+
			"'"+numNotificacao+"',"+
			"TO_DATE('"+datDigitalizacao+"','DD/MM/YYYY'),"+					
			"'"+codigoRetorno+"')";					
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: insert_TSMI_AR_DIGITALIZADO(): " + e.getMessage());
			throw new RobotException(e.getMessage());
		}
			}
	
	public static void totalizaRetornoDatEmissaoV1(Connection conn,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega,
			String mesDatEnvio,
			String anoDatEnvio,
			String codIdentArquivo) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Retornos-por data de emiss�o*/
			sCmd="SELECT * FROM TSMI_CONT_VEX_TOT_ENV_V1 "+
			"WHERE "+
			"DAT_ENVIO="+"TO_DATE('"+datEnvio+"','DD/MM/YYYY')"+" AND "+
			"DAT_RETORNO="+"TO_DATE('"+datRetorno+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"' AND " +
			"COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONT_VEX_TOT_ENV_V1 (DAT_ENVIO,DAT_RETORNO,"+
				"COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,MES_ENVIO,ANO_ENVIO,QTD_EMITIDO,QTD_EMITIDO_RETORNO," +
				"QTD_DIAS_RETORNO,QTD_REEMISSAO,COD_IDENT_ARQUIVO) VALUES ("+
				"TO_DATE('"+datEnvio+"','DD/MM/YYYY'),"+
				"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',"+							
				"'"+mesDatEnvio+"',"+							
				"'"+anoDatEnvio+"',0,0,0,0,"+
				"'"+codIdentArquivo+"')";
				
				stmt.executeQuery(sCmd);					
			}
			int iDias=Util.DifereDias(datEnvio,datRetorno);						
			sCmd="UPDATE TSMI_CONT_VEX_TOT_ENV_V1 "+
			"SET "+
			"QTD_EMITIDO_RETORNO=QTD_EMITIDO_RETORNO+1, "+
			"QTD_DIAS_RETORNO=QTD_DIAS_RETORNO+"+iDias+" "+						
			"WHERE "+
			"DAT_ENVIO="+"TO_DATE('"+datEnvio+"','DD/MM/YYYY')"+" AND "+
			"DAT_RETORNO="+"TO_DATE('"+datRetorno+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"'";
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaRetornoDatEmissaoV1: " + e.getMessage());
		}
	}		
	
	
	public static void totalizaRetornoDatRetornoV1(Connection conn,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega,
			String tipoRet,
			String mesDatRetorno,
			String anoDatRetorno,
			String codIdentArquivo) throws Exception, DaoException
			{
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
			/*Totalizar Retornos-por data de retorno*/
			sCmd="SELECT * FROM TSMI_CONT_VEX_TOT_RET_V1 "+
			"WHERE "+
			"DAT_RETORNO="+"TO_DATE('"+datRetorno+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"' AND " +
			"COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
			rs=stmt.executeQuery(sCmd);		
			if (!rs.next())
			{
				sCmd="INSERT INTO TSMI_CONT_VEX_TOT_RET_V1 (DAT_RETORNO,"+
				"COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,MES_RETORNO,ANO_RETORNO," +
				"QTD_DIAS_RETORNO,QTD_RETORNO_AVISO,QTD_RETORNO_12,QTD_RETORNO_13,QTD_RETORNO_14," +
				"QTD_DESPREZADA,QTD_RECUPERADA_RETORNO,QTD_RECUPERADA_SMIT,COD_IDENT_ARQUIVO) VALUES ("+
				"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',"+							
				"'"+mesDatRetorno+"',"+							
				"'"+anoDatRetorno+"',0,0,0,0,0,0,0,0,"+
				"'"+codIdentArquivo+"')";
					
				
				stmt.executeQuery(sCmd);					
			}
			
			int iDias=Util.DifereDias(datEnvio,datRetorno);		
			sCmd="UPDATE TSMI_CONT_VEX_TOT_RET_V1 "+
			"SET ";
			if (tipoRet.equals("QRAV"))
				sCmd += "QTD_RETORNO_AVISO=QTD_RETORNO_AVISO+1 ";
			if (tipoRet.equals("QR12"))
				sCmd += "QTD_RETORNO_12=QTD_RETORNO_12+1 ";
			if (tipoRet.equals("QR13"))
				sCmd += "QTD_RETORNO_13=QTD_RETORNO_13+1 ";
			if (tipoRet.equals("QR14"))
				sCmd += "QTD_RETORNO_14=QTD_RETORNO_14+1 ";
			if (tipoRet.equals("QD"))
				sCmd += "QTD_DESPREZADA=QTD_DESPREZADA+1 ";
			if (tipoRet.equals("QRR"))
				sCmd += "QTD_RECUPERADA_RETORNO=QTD_RECUPERADA_RETORNO+1 ";
			if (tipoRet.equals("QRS"))
				sCmd += "QTD_RECUPERADA_SMIT=QTD_RECUPERADA_SMIT+1 ";
			sCmd += ",QTD_DIAS_RETORNO=QTD_DIAS_RETORNO+"+iDias+" ";						
			sCmd += "WHERE "+
			"DAT_RETORNO="+"TO_DATE('"+datRetorno+"','DD/MM/YYYY')"+" AND "+
			"COD_RETORNO='"+codigoRetorno+"' AND "+
			"IND_ENTREGUE='"+indEntrega+"' AND "+
			"COD_IDENT_ARQUIVO ='"+codIdentArquivo+"'";
			stmt.executeQuery(sCmd);
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaRetornoDatRetorno(): " + e.getMessage());
		}
			}
	
	
	public static void totalizaRetornoDatEmissaoDetalheV1(Connection conn,
			String codArDigitalizado,
			String codControleVEX_MASTER,
			String codigoRetorno,
			String datEnvio,
			String datRetorno,
			String indFaturado,
			String indEntrega,
			String mesDatEnvio,
			String anoDatEnvio) throws Exception, DaoException
			{
		
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			
				sCmd="INSERT INTO TSMI_CONT_TOT_ENV_DETAIL_V1(DAT_ENVIO,DAT_RETORNO,"+
				"COD_RETORNO," +
				"IND_ENTREGUE,IND_FATURADO,MES_ENVIO,ANO_ENVIO," +
				"PK_COD_CONTROLE_VEX_MASTER_V1,COD_AR_DIGITALIZADO) VALUES ("+
				"TO_DATE('"+datEnvio+"','DD/MM/YYYY'),"+
				"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
				"'"+codigoRetorno+"',"+
				"'"+indEntrega+"',"+							
				"'"+indFaturado+"',"+							
				"'"+mesDatEnvio+"',"+							
				"'"+anoDatEnvio+"',"+						
				"'"+codControleVEX_MASTER+"',"+						
				"'"+codArDigitalizado+"')";						
				stmt.executeQuery(sCmd);					

				
			
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaRetornoDatEmissaoDetalheV1: " + e.getMessage());
		}
			}
	
	
	
	public static void totalizaRetornoDatRetornoDetalheV1(Connection conn,
			String codArDigitalizado,
			String codControleVEX_MASTER,
			String codigoRetorno,
			String datRetorno,
			String indFaturado,
			String indEntrega,
			String mesDatRetorno,
			String anoDatRetorno) throws Exception, DaoException
			{
		
		String sCmd    = "";
		Statement stmt = conn.createStatement();
		ResultSet rs       = null;
		
		try
		{
			sCmd="INSERT INTO TSMI_CONT_TOT_RET_DETAIL_V1(DAT_RETORNO,"+
			"COD_RETORNO," +
			"IND_ENTREGUE,IND_FATURADO,MES_RETORNO,ANO_RETORNO," +
			"PK_COD_CONTROLE_VEX_MASTER_V1,COD_AR_DIGITALIZADO) VALUES ("+
			"TO_DATE('"+datRetorno+"','DD/MM/YYYY'),"+
			"'"+codigoRetorno+"',"+
			"'"+indEntrega+"',"+							
			"'"+indFaturado+"',"+							
			"'"+mesDatRetorno+"',"+							
			"'"+anoDatRetorno+"',"+						
			"'"+codControleVEX_MASTER+"',"+						
			"'"+codArDigitalizado+"')";						
			stmt.executeQuery(sCmd);					
			if (rs!=null)     rs.close();
			stmt.close();
			
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: totalizaRetornoDatEmissaoDetalheV1: " + e.getMessage());
		}
	}
	
	
	
	
	
	
}