package ROB;


import java.sql.Connection;
import java.sql.Statement;

import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;
import ACSS.UsuarioBean;
import REG.ArquivoRecebidoBean;
import ROB.Dao;


public class GeraArquivoARPNTRob {
	
	private static final String MYABREVSIST = "ROB";
	private static final String NOM_ROBOT = "ROB.GeraArquivoARPNTRob";
	public  static final String QUEBRA_LINHA = "\n";
	private static final String NOM_USERNAME = "GerArqARRobot";	
	private static final int    NUM_MAX_RETORNOS = 500;
	
	public GeraArquivoARPNTRob() throws DaoException, ServiceLocatorException, RobotException, ROB.DaoException, DaoException {
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		REG.Dao dao = REG.Dao.getInstance();
		try {				
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	

			ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
			paramReg.setCodSistema("40"); //M�DULO REGISTRO	
			paramReg.PreparaParam(conn);
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			UsuarioBean UsuarioBeanId = new UsuarioBean();
			UsuarioBeanId.getOrgao().setCodOrgao(paramReg.getParamSist("ORGAO_CODRET"));
			UsuarioBeanId.setCodOrgaoAtuacao(paramReg.getParamSist("ORGAO_CODRET")); 
			UsuarioBeanId.setNomUserName(NOM_USERNAME);

			REC.ParamOrgBean ParamOrgaoId = ParamOrgaoId = new REC.ParamOrgBean();
			ParamOrgaoId.setCodOrgao(paramReg.getParamSist("ORGAO_CODRET"));
			ParamOrgaoId.PreparaParam(UsuarioBeanId);
			
			String dirDownload = paramReg.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			String qtdReg = null; // Par�metro criado para manipular a quantidade de registros do arquivo.
			
			monitor.gravaMonitor("In�cio Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);
			
			while (true) {                
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					monitor.gravaMonitor("Fim Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);					
					break;
				}
				
				

				monitor.gravaMonitor("Inicio da Gera��o do Arquivo" , monitor.MSG_INICIO);

				ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
				dao.CarregarLinhasArquivoARPNT(arqRecebido,UsuarioBeanId,conn,NUM_MAX_RETORNOS);
				if (arqRecebido.getLinhaArquivoRec().size() < NUM_MAX_RETORNOS ) {
					monitor.gravaMonitor("T�rmino da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);
					break;
				}
				
				dao.AtualizarLinhasArquivoARPNT(arqRecebido,conn);
				conn.commit();	

				
				monitor.gravaMonitor("T�rmino da Gera��o do Arquivo CODRET : " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
			}
			
			monitor.gravaMonitor("Fim Robot "+NOM_ROBOT+" "+Util.formatedToday(), monitor.MSG_FIM);			
			
			
		} catch (Exception e) {
			try {				
				conn.rollback();
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}             
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	
	public static void main(String args[])  throws DaoException {		
		try {			
			new GeraArquivoARPNTRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}