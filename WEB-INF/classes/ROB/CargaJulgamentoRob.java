package ROB;

import java.sql.Connection;
import java.sql.Statement;

import sys.DaoException;

public class CargaJulgamentoRob {
    
	private static final String NOM_ROBOT = "CargaJulgamentoRob";
	
	
	
	public CargaJulgamentoRob() throws RobotException, DaoException, ROB.DaoException {
        
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
		try {  
			conn = Dao.getInstance().getConnection();
            
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
            
			/*
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			*/
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
		    if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			UsuarioBeanId.setCodOrgaoAtuacao("999999");
			Dao.getInstance().AcertarJulgamento(UsuarioBeanId);
			


		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
    


    
  public static void main(String[] args) throws RobotException {
        
		try {
			new CargaJulgamentoRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
    

}