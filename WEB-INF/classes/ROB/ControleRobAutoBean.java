package ROB;

import sys.DaoException;


public class ControleRobAutoBean extends sys.HtmlPopupBean{

	  public  final String SIT_OK = "1";
	  public  final String SIT_ERRO = "2";
	  public  final String SIT_PARADA = "3";
    
	  private String numControle; 
	  private String datInicio; 
	  private String datFim;
	  private String txtProces;
	  private int qtdAutosProces;
	  private String sitProces;    
	  private String indContinua;
   
  	public ControleRobAutoBean()  throws RobotException {
			numControle = "";
			datInicio = "";
			datFim = "";
			txtProces = "";
			qtdAutosProces = 0;
			sitProces = "";
		  indContinua = "";
    }
    
    public void setNumControle(String numControle){
        this.numControle = numControle;
        if (this.numControle == null) this.numControle = "";
    }
    public String getNumControle(){
        return this.numControle;
    }
    
    public void setDatInicio(String datInicio){
        this.datInicio = datInicio;
        if (this.datInicio == null) this.datInicio = "";
    }
    public String getDatInicio(){
        return this.datInicio;
    }
    
    public void setDatFim(String datFim){
        this.datFim = datFim;
        if(this.datFim == null) this.datFim = "";
    }
    public String getDatFim(){
        return this.datFim;
    }
    
    public void setTxtProces(String txtProces){
        this.txtProces = txtProces;
        if(this.txtProces == null) this.txtProces = "";
    }
    public String getTxtProces(){
        return this.txtProces;
    }
    
    public void setQtdAutosProces(int qtdAutosProces){
        this.qtdAutosProces = qtdAutosProces;
        if(this.qtdAutosProces == 0) this.qtdAutosProces = 0;
    }
    public int getQtdAutosProces(){
        return this.qtdAutosProces;
    }
    
		public void setSitProces(String sitProces){
			this.sitProces = sitProces;
			if(this.sitProces == null) this.sitProces = "";
		}
		public String getSitProces(){
			return this.sitProces;
		}
		
	  public void setIndContinua(String indContinua){
			this.indContinua = indContinua;
			if(this.indContinua == null) this.indContinua = "";
		}
		public String getIndContinua(){
			return this.indContinua;
		}
		
		
		public void gravaDadosControle(ControleRobAutoBean controleRobAutoBean) throws DaoException, DaoException{
			 
			Dao.getInstance().gravaDadosControle(controleRobAutoBean);
		}
   
    
 }
