package ROB;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import sys.DaoException;

public class DownLoadArquivoRob {
    
    private static final String NOM_ROBOT = "DownLoadArquivoRob";
    private static final String quebraLinha = "\n";
    
    public DownLoadArquivoRob() throws RobotException, DaoException {
        
        Connection conn  = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {            			
            conn = Dao.getInstance().getConnection();
            REG.Dao dao = REG.Dao.getInstance();
            
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); //M�DULO REGISTRO	
            param.PreparaParam(conn);
            
            String dirDownload = param.getParamSist("DOWN_ARQUIVO_RECEBIDO");
            
            while (true) {                
                if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;                 
                
                String tipos = "'MR01','MX01','ME01','RECJARI','MZ01','MC01','CODRET','MF01','MA01'";
                Vector resArquivos = dao.BuscarArquivosRecebidos(tipos, "1", conn);
                if (resArquivos.size() == 0) break;
                
                REG.ArquivoRecebidoBean arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(0);
                monitor.setNomObjeto(arqRecebido.getNomArquivo());
                monitor.setCodOrgao(arqRecebido.getCodOrgao());
                monitor.gravaMonitor("Inicio da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
                System.out.println("Inicio da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo());
                
                dao.CarregarLinhasArquivo(arqRecebido, conn);
                
                FileWriter wrtRej = null;
                FileWriter wrtGrv = null;                
                // Verifica se tem apenas gravados, apenas rejeitados ou os dois
                if ("".equals(arqRecebido.getCodIdentRetornoRej()))
                    wrtGrv = criarArquivoGrv(arqRecebido, dirDownload);
                else if ("".equals(arqRecebido.getCodIdentRetornoGrv()))
                    wrtRej = criarArquivoRej(arqRecebido, dirDownload);
                else {			
                    if (!arqRecebido.getQtdLinhaErro().equals("0")) 
                        wrtRej = criarArquivoRej(arqRecebido, dirDownload);			
                    if (!arqRecebido.getQtdLinhaErro().equals(arqRecebido.getQtdReg()))	
                        wrtGrv = criarArquivoGrv(arqRecebido, dirDownload);
                }										
                
                int QtdLinProc = 0;
                Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();                
                while (ite.hasNext()) {
                    
                    if ((QtdLinProc%200)==0)
                        monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
                            QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size(), monitor.MSG_INFO);
                    
                    REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
                    
                    // Verifica se tem apenas gravados, apenas rejeitados ou os dois
                    if ("".equals(arqRecebido.getCodIdentRetornoRej()))
                        gravaGrv(wrtGrv, arqRecebido, linhaArq, conn);                    
                    else if ("".equals(arqRecebido.getCodIdentRetornoGrv()))
                        gravaRej(wrtRej, arqRecebido, linhaArq, conn);
                    else {
                        if (linhaArq.getCodRetorno().equals("000"))
                            gravaGrv(wrtGrv, arqRecebido, linhaArq, conn);
                        else
                            gravaRej(wrtRej, arqRecebido, linhaArq, conn);
                    }
                    QtdLinProc++;										
                }
                
                if (wrtRej != null)	wrtRej.close();
                if (wrtGrv != null)	wrtGrv.close();
                
                arqRecebido.setConn(conn);
                arqRecebido.setCodStatus("2");
                arqRecebido.setDatDownload(sys.Util.fmtData(new Date()));
                if (!arqRecebido.isUpdate())
                    throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
                
                monitor.gravaMonitor("T�rmino da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);
                
            }
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                try { 
                    Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
                } catch (DaoException ed) {
                    throw new RobotException(ed.getMessage());
                }
            }
        }
    }
    
    public static FileWriter criarArquivoGrv(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
    throws RobotException {
        
        String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();			
        
        //Cria o arquivo
        FileWriter wrtGrv;
        try {			
            File dirGrv = new File(caminho.substring(0, caminho.lastIndexOf("/")));
            dirGrv.mkdirs();
            
            File arqGrv = new File(caminho);
            arqGrv.createNewFile();
            wrtGrv = new FileWriter(arqGrv);
            
            //Grava o header do arquivo	
            if (!arqRecebido.getHeaderGrv().equals("")) {
                wrtGrv.write(arqRecebido.getHeaderGrv());
                wrtGrv.write(quebraLinha);
            }
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
        return wrtGrv;
    }
    
    public static FileWriter criarArquivoRej(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
    throws RobotException {
        
        String caminho = dirDestino + "/" + arqRecebido.getNomArquivoRej();
        
        //Cria o arquivo
        FileWriter wrtRej;
        try {
            File dirRej = new File(caminho.substring(0, caminho.lastIndexOf("/")));
            dirRej.mkdirs();
            File arqRej = new File(caminho);			
            arqRej.createNewFile();
            wrtRej = new FileWriter(arqRej);
            
            //Grava o header do arquivo
            if (!arqRecebido.getHeaderRej().equals("")) {
                wrtRej.write(arqRecebido.getHeaderRej());
                wrtRej.write(quebraLinha);
            }
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
        
        return wrtRej;
    }	
    
    private static void gravaRej(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
            REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
        
        try {
            if (writer != null) {
                writer.write(linhaArq.getLinhaRej(arqRecebido, conn));
                writer.write(quebraLinha);
            }
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
    }
    
    private static void gravaGrv(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
            REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
        
        try {
            if (writer != null) {			
                
            	if (arqRecebido.getDscPortaria59().equals("P59"))
            	   writer.write(linhaArq.getLinhaGrvP59(arqRecebido, conn));
            	else
       	           writer.write(linhaArq.getLinhaGrv(arqRecebido, conn));
            		
                
                writer.write(quebraLinha);
            }
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
    }
    
    public static void main(String[] args) throws RobotException {
        
        try {
            new DownLoadArquivoRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    
}
