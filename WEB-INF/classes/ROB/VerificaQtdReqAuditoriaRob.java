package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import ACSS.OrgaoBean;

import sys.DaoException;
import sys.Util;

public class VerificaQtdReqAuditoriaRob {
    
    private static final String NOM_ROBOT = "VerificaQtdReqAuditoriaRob";
    private static final String quebraLinha = "\n";
    sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
      
    public VerificaQtdReqAuditoriaRob() throws RobotException, ROB.DaoException, DaoException {
        
        Connection conn  = null;
        Statement trava = null;

        try {            			
            conn = Dao.getInstance().getConnection();
                        
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            // N�o permite que mais de uma instancia do robo rodem ao mesmo tempo 
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            // Carrega Parametros 
            monitor.gravaMonitor("*** INICIO *** Robo: VerificaQtdReqAuditoriaRob", monitor.MSG_INICIO);
            System.out.println("*** INICIO *** Robo: VerificaQtdReqAuditoriaRob"); 
            
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); // M�DULO REGISTRO 
            param.PreparaParam(conn);
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");
			
			ArrayList qtdReqDefinirRelator = new ArrayList();
			ArrayList ListDestinatarios = new ArrayList();
			ArrayList qtdRegEncontrado = new ArrayList();
			ArrayList qtdReg = new ArrayList();
			
			int tamMsgIni = 0;
			
			StringBuffer msg = new StringBuffer();
			String 	dataHoje = Util.getDataHoje().replace("-","/");
			String indPos = "";
			String indPosProx = "";
			String codSolicLido = "";
			
		    String [] niveis = null;
			String [] codTipoSolic = {"'DP','DC'","'1P','1C'","'2P','2C'"};
			String [] nomFase = {"DP","1A","2A"};
			String nomProcessoDest [] = {"QTD_REQ_DEF_RELATOR_DP","QTD_REQ_DEF_RELATOR_1A","QTD_REQ_DEF_RELATOR_2A"};
						
			//verifica qtd de registro para cada �rg�o
			ACSS.OrgaoBean[] orgaos = Dao.getInstance().BuscarOrgaosIntegrados("0");
			for (int j = 0; j < orgaos.length; j++) 
			{
				if (orgaos[j].getCodOrgao().equals("258650")==false) continue;
				monitor.gravaMonitor("verificando qtd para o org�o >> "+orgaos[j].getSigOrgao()+" <<", monitor.MSG_INFO);
				System.out.println("verificando qtd para o org�o >> "+orgaos[j].getSigOrgao()+" <<"); 
				
				for (int i = 0; i < codTipoSolic.length; i++)
				{
					//verifica destinat�rios 
					monitor.gravaMonitor("Verificando destinat�rios", monitor.MSG_INFO);
					System.out.println("Verificando destinat�rios"); 
					ListDestinatarios = ACSS.Dao.getInstance().getDestinatariosPorNivel(param,nomProcessoDest[i]);
					
					//Prepara inicio da mensagem
					msg.append("Prezado(a) Senhor(a), "+quebraLinha+
					    "Informamos que a quantidade de requerimentos que estao na " +
					    "fase de Definir Relator a mais de ");
					tamMsgIni = msg.length();

					qtdReqDefinirRelator = new ArrayList();
					if (ListDestinatarios.size()>0)
					{
						//Verifica o valor dias de cada nivel
						monitor.gravaMonitor("Verificando o valor dias de cada nivel "+nomFase[i], monitor.MSG_INFO);
						System.out.println("Verificando o valor dias de cada nivel "+nomFase[i]); 
						niveis = verificaValorsPorNivel(ListDestinatarios);
						
						//Verifica quantidade por nivel
						monitor.gravaMonitor("Verificando quantidade por nivel", monitor.MSG_INFO);
						System.out.println("Verificando quantidade por nivel"); 
						for(int z = 0; z<niveis.length; z++)
						{
							indPos = niveis[z];
							if(z == niveis.length-1)
							    indPosProx = niveis[z];
							else
								indPosProx = niveis[z+1];
							if(qtdRegEncontrado.size() == 0 && !codSolicLido.equals(codTipoSolic[i]))
							{
								qtdRegEncontrado = Dao.getInstance().verificaQtdReq(conn,orgaos[j],
									codTipoSolic[i]);
								codSolicLido = codTipoSolic[i];
							}
							qtdReg = comparaDatas(orgaos[j],dataHoje,codTipoSolic[i],indPos,
									indPosProx, z ,niveis.length-1,qtdRegEncontrado);


							if(qtdReg.size()>0)
							{
							    //Acrescentando o restante da mensagem para cada n�vel
					  		    msg.append(qtdReg.get(0)+" dias e de "+qtdReg.get(1)+" requerimentos."+
										quebraLinha+quebraLinha+"Atenciosamente,"+quebraLinha+"Auditoria SMIT");
								
								// Prepara Email 
					  		    monitor.gravaMonitor("Preparando Email ..."+nomFase[i], monitor.MSG_INFO);
					  		    System.out.println("Preparando Email ..."+nomFase[i]); 
						    	qtdReqDefinirRelator = preparaEmail(ListDestinatarios,msg,(String)qtdReg.get(0),orgaos[j]);
							
					    		// Enviar Email 
						    	monitor.gravaMonitor("Enviando Email ..."+nomFase[i], monitor.MSG_INFO);
						    	System.out.println("Enviando Email ..."+nomFase[i]); 
								if (qtdReqDefinirRelator.size()>0)
								{
						    		enviaEmail (qtdReqDefinirRelator,emailBroker, (String)qtdReg.get(0));
								}
								
								//Deleta parte da msg que foi acrescentada acima
								msg.delete(tamMsgIni,msg.length());
							}
						}
					}
					msg.delete(0,msg.length());
					qtdRegEncontrado = new ArrayList();
				}
				monitor.gravaMonitor("*** FIM *** Robo: VerificaQtdReqAuditoriaRob", monitor.MSG_FIM);
				System.out.println("*** FIM *** Robo: VerificaQtdReqAuditoriaRob"); 
			}

	
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Monitorar a quantidade de Processados nas guias: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }

    public static void main(String[] args) throws RobotException {
    	
    	try {
    		new VerificaQtdReqAuditoriaRob();			 
    	} catch (Exception e) { 
    		System.out.println(e); 
    	} 
    }
	private ArrayList preparaEmail(ArrayList ListDestinatarios, StringBuffer sConteudoEmail,
			String sValNivelProcesso, OrgaoBean myOrg) throws NumberFormatException, DaoException{
 
		ArrayList usuariosBloqueados = new ArrayList();

		
		// Prepara Email   
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{

				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals(sValNivelProcesso))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(nivelProcesso.getTipoMsg().equals("M")&& destinatario.getCodOrgao().equals(myOrg.getCodOrgao()))
						{
								destinatario.getMsg().add(sConteudoEmail.toString());
						 }
						 if(destinatario.getMsg().size()>0)
							lDestinatarios.add(destinatario);
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
				    usuariosBloqueados.add(nivelProcesso);
		     	}
			}
		}
        return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuarios, String emailBroker, String sValNivelProcesso) throws Exception{	
		if (usuarios.size()>0)
		{
			String sConteudo="";
			Iterator ity = null;
			Iterator it = usuarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals(sValNivelProcesso))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							sConteudo="";
							ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso());
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				     }
		     	}
			}
		}
	}
	
	private String[] verificaValorsPorNivel(ArrayList ListDestinatarios)
	{
		Vector valNivel = new Vector();
		Iterator it = ListDestinatarios.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			valNivel.add(nivelProcesso.getValNivelProcesso());
	    }
		
		String [] nivelDias = new String[valNivel.size()];
		valNivel.toArray(nivelDias);
		return nivelDias;
	}
	
	private ArrayList comparaDatas(ACSS.OrgaoBean Orgao, String dataHoje,
			String codTipoSolic, String sDiasNivel, String sDiasNivelProx, int indPos, 
			int inPosUltima, ArrayList qtdRegEncontrado)  {
		
		ArrayList qtdReg = new ArrayList();
		int qtdReqDefRel = 0;
		Iterator it = qtdRegEncontrado.iterator();
		String registro = "";
		while (it.hasNext())
		{
			registro = (String)it.next();
			if( (indPos == inPosUltima) && (Util.DifereDias(registro,dataHoje)>=
				 Integer.parseInt(sDiasNivel)) )
			{
				qtdReqDefRel++;
			}
			else if( (Util.DifereDias(registro,dataHoje)>=
				 Integer.parseInt(sDiasNivel))&&
			    (Util.DifereDias(registro,dataHoje)< 
			      Integer.parseInt(sDiasNivelProx)) )
			{
				qtdReqDefRel++;
			}
		}
		if(qtdReqDefRel > 0)
		{
			qtdReg.add(sDiasNivel);
			qtdReg.add(qtdReqDefRel);
			qtdReqDefRel = 0;
		}
		return qtdReg;
	}

}