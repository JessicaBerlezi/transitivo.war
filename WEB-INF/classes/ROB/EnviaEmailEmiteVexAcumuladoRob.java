package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

import sys.DaoException;
import sys.Util;
import ACSS.ParamSistemaBean;

public class EnviaEmailEmiteVexAcumuladoRob {
    
	private static final String NOM_ROBOT = "EnviaEmailEmiteVexAcumuladoRob";
  
	public EnviaEmailEmiteVexAcumuladoRob() throws RobotException, DaoException {
        
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
		try 
		{     
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
            
			try 
			{               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			monitor.gravaMonitor("Inicio do processamento do rob� EnviaEmailEmiteVexAcumuladoRob", monitor.MSG_INICIO);
			System.out.println("Inicio do processamento do rob� EnviaEmailEmiteVexAcumuladoRob");
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			//L� o par�metro da �ltima data processada
			monitor.gravaMonitor("L� o par�metro da �ltima data processada", monitor.MSG_INFO);
			System.out.println("L� o par�metro da �ltima data processada");
			String datFimParam = param.getParamSist("DAT_INICIO_EMITEVEX_ACUMULADO");
			
			
			//Controla data caso n�o tenha processado arquivo no dia anterior
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			String diaHoje = Util.getDataHoje().replace("-","/");
			String datInicio = "01"+ diaHoje.substring(2,5) + diaHoje.substring(5);
			if(df.parse(datFimParam).before(df.parse(diaHoje)))
			{
				datFimParam = diaHoje;
			}
			
			//Carrega destinat�rios
            ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"EMAIL_EMITEVEX_ACUMULADO");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");		

			//Verifica total acumulado
			Vector erro = new Vector();
			GER.RelatArqNotificacaoBean arqNotifBean = new GER.RelatArqNotificacaoBean();
			arqNotifBean.ConsultaRelDiario(datInicio,datFimParam,erro);
			
			//Verifica total do dia
			arqNotifBean.setDatRecebimento(datFimParam);
			Dao.getInstance().getDadosEmitVexAcumulado(arqNotifBean, conn);

			//Prepara e envia e-mail
			if (!arqNotifBean.getQtdReg().equals(""))
			{
				String sConteudoEmail = " \n Total do Dia:"+
		        " \n ------------------"+
		        " \n Qtd Registradas: "+ Util.formataIntero(arqNotifBean.getQtdReg())+
		        " \n Qtd Pendente...: "+ Util.formataIntero(arqNotifBean.getQtdLinhaPendente())+
		        " \n Qtd Recebida...: "+ Util.formataIntero(arqNotifBean.getQtdRecebido())+
		        " \n" +
		        " \n Total Acumulado Ate a Data: " +
		        " \n ------------------------------------------- " +
		        " \n Qtd Registrada: " +Util.formataIntero(arqNotifBean.getTotalQtdReg())+
		        " \n Qtd Pendente..: "+Util.formataIntero(arqNotifBean.getTotalQtdLinhaPendente())+
		        " \n Qtd Recebida..: "+Util.formataIntero(arqNotifBean.getTotalQtdRecebido())+
		        " \n\n Atenciosamente, \n Auditoria SMIT";
				
				monitor.gravaMonitor("Preparando e enviando e-mail", monitor.MSG_INFO);
				System.out.println("Preparando e enviando e-mail");
				ArrayList usuariosBloqueados = preparaEmail(ListDestinatarios, sConteudoEmail);
				enviaEmail(usuariosBloqueados, emailBroker,arqNotifBean.getDatRecebimento()) ;

	            //Atualiza o valor do par�metro de data
				monitor.gravaMonitor("Atualizando o valor do par�metro de data", monitor.MSG_INFO);
				System.out.println("Atualizando o valor do par�metro de data");
				ParamSistemaBean paramBean = new ParamSistemaBean();
				paramBean.setNomParam("DAT_INICIO_EMITEVEX_ACUMULADO");
				String datParametro = Util.addData(datFimParam,1);
				paramBean.setValParametro(datParametro);
				paramBean.setCodSistema(param.getCodSistema());
				Dao.getInstance().AtualizaParametro(paramBean, conn);
			}
			conn.commit();
			monitor.gravaMonitor("Fim do processamento do rob� EnviaEmailEmiteVexAcumuladoRob ", monitor.MSG_FIM);
			System.out.println("Fim do processamento do rob� EnviaEmailEmiteVexAcumuladoRob");

		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}

	 
	
	private ArrayList preparaEmail(ArrayList ListDestinatarios, String sConteudoEmail){
		// Prepara Email    
		ArrayList usuariosBloqueados = new ArrayList();
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(nivelProcesso.getTipoMsg().equals("M"))
						{
							destinatario.getMsg().add(sConteudoEmail);
						}

						if(destinatario.getMsg().size()>0)
							lDestinatarios.add(destinatario);
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
					usuariosBloqueados.add(nivelProcesso);
			   }
			}
		  }
          return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuariosBloqueados, String emailBroker, String data) throws Exception{	
		if (usuariosBloqueados.size()>0)
		{
			Iterator it = usuariosBloqueados.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
				{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							String sConteudo="";
							Iterator ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+ data);
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				   }
				}
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new EnviaEmailEmiteVexAcumuladoRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	

}