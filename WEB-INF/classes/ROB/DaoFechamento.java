package ROB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import sys.ServiceLocator;
import sys.Util;
import ACSS.ParamSistemaBean;
import REC.AutoInfracaoBean;
import REC.ParamUFBean;


public class DaoFechamento {
	
	private static final String MYABREVSIST = "ROB";
	private static DaoFechamento instance;
	private ServiceLocator serviceloc ;
	
	private DaoFechamento() throws DaoException {
		try {	
			serviceloc =  ServiceLocator.getInstance();
			serviceloc.initParams("../web.xml");
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	public static DaoFechamento getInstance() throws DaoException {
		if (instance == null)
			instance = new DaoFechamento();
		return instance;
	}   
	
	public Connection getConnection() throws sys.ServiceLocatorException {
		return serviceloc.getConnection(MYABREVSIST);
	}
	
	public void setReleaseConnection(Connection conn) throws sys.ServiceLocatorException {
		serviceloc.setReleaseConnection(conn);
	}
	
	public boolean pararProcesso(Connection conn, String nomRobot) throws RobotException{
		
		boolean parar = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT IND_CONTINUA FROM TSMI_GERENCIA_ROBO WHERE NOM_ROBO = '" + nomRobot + "'"
			+ " AND COD_TIPO_REGISTRO = 1";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				parar = (rs.getInt("IND_CONTINUA") == 1);
			rs.close();
			stmt.close();           
			return parar;
		} catch (SQLException e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public Statement travarExecucao(String nomRobot) throws SQLException, RobotException {
		
		Statement stmt = null;
		try {
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_GERENCIA_ROBO WHERE NOM_ROBO = '" + nomRobot + "' AND" +
			" COD_TIPO_REGISTRO = 0 FOR UPDATE  NOWAIT";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (!rs.next()) {
				rs.close();
				throw new RobotException("Erro ao carregar Rob�: N�o existe o registro de ger�ncia !");
			}
			rs.close();
			
			return stmt;
			
		} catch (SQLException e) {
			throw new SQLException("Erro ao carregar Rob�: J� existe outra C�pia processando !");
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public void destravarExecucao(String nomRobot, Statement stmt) throws RobotException {
		try {
			stmt.close();
			setReleaseConnection(stmt.getConnection());
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}        
	}
	
	/**
	 * Atualiza o status do Auto de Infra��o
	 * @author Wellem Mello
	 * @since 13/05/2005
	 * @param auto
	 * @return
	 * @throws SQLException
	 * @throws RobotException
	 * @version 1.0
	 */
	private boolean atualizaStatusAuto(String auto) throws SQLException, RobotException {
		
		Connection conn = null;
		try {
			boolean retorno = true;
			
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			
			try {               
				String sCmd = "UPDATE TBRK_AUTO_INFRACAO SET COD_STATUS  = '" + 999 + "'" +
				" WHERE NUM_AUTO = '"+ auto+"'";
				rs = stmt.executeQuery(sCmd);
				
			} catch (SQLException e) {
				retorno = false;
			}
			rs.close();
			stmt.close();
			conn.commit();
			return retorno;
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
			
		} finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception es) {
					throw new RobotException(es.getMessage());
				}
			}
		}		
	}
	
	
	/**
	 * M�todo para mudan�a de status dos autos com prescri��o.
	 * @author Wellem Mello
	 * @since 13/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void PrescreveAuto(Connection conn) throws RobotException{		
		try {            			
			String sCmd        = "";
			Statement stmt = conn.createStatement();
			ArrayList autos = new ArrayList();  
			
			              
				sCmd  = " SELECT t.num_auto,e.num_auto_infracao,e.ind_fase_auto_infr,t.cod_orgao,t.ind_fase, to_char(t.dat_status,'dd/mm/yyyy')as datStatus, t.cod_infracao, t.txt_infracao, to_char(t.dat_infracao,'dd/mm/yyyy') as datInfracao, t.num_pontos";
				sCmd += " FROM tbrk_auto_infracao t, tpnt_extrato e  ";
				sCmd += " WHERE e.NUM_AUTO_INFRACAO = T.NUM_AUTO";
				sCmd += " AND e.ind_fase_auto_infr =3";
				
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					AutoInfracaoBean myAuto = new AutoInfracaoBean();
					myAuto.setNumAutoInfracao(rs.getString("num_auto"));
					myAuto.setCodOrgao(rs.getString("cod_orgao"));
					myAuto.setDatStatus(rs.getString("datStatus"));
					myAuto.getInfracao().setCodInfracao(rs.getString("cod_infracao"));
					myAuto.getInfracao().setDscInfracao(rs.getString("txt_infracao"));
					myAuto.setDatInfracao(rs.getString("datInfracao"));
					myAuto.getInfracao().setNumPonto(rs.getString("num_pontos"));
					myAuto.setIndFase(rs.getString("ind_fase_auto_infr"));					
					autos.add(myAuto);
				}
				if (autos.size()> 0 ){						
					Iterator it = autos.iterator() ;
					AutoInfracaoBean auto =  new AutoInfracaoBean() ;
					String Hoje  = Util.formatedToday().substring(0,10);
					int dias = 0;
					while (it.hasNext()) {
						// lista os dados		
						auto = (AutoInfracaoBean)it.next() ;						
						dias = Util.DifereDias(auto.getDatStatus(),Hoje);						
						if (dias > 365){       
							try{
								atualizaStatusAuto(auto.getNumAutoInfracao());                                          
							}
							catch (Exception e) {
									throw new DaoException("Erro ao atualizar Auto: " + auto.getNumAutoInfracao() + " !");             
							}	
						}
					}					
				}			
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	}
	
	/**
	 * M�todo para implementa��o do fechamento di�rio 3.2
	 * Atualiza a tabela de saldos com de acordo com os autos associados a um determinado
	 * CNH.
	 * Os status dos autos trabalhados por esse m�todo s�o 0,10,99.
	 * 
	 * @author Sergio Roberto Junior
	 * @since 13/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void AtualizaSaldo(Connection conn) throws RobotException{
		try {            			
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String campoQuantidade = null;
			String campoSomatorio = null;
			int numCnh = 0;
			int quantidadeAut = 0;
			int quantidadePen = 0;
			int quantidadeImp = 0;
			int somatorioAut = 0;
			int somatorioPen = 0;
			int somatorioImp = 0;
			int quantidade = 0;
			int somatorio = 0;
			boolean flagUpdate = true;
			
			String sCmd = "SELECT DISTINCT(NUM_CNH), COD_STATUS, NUM_PONTOS FROM TBRK_AUTO_INFRACAO JOIN TPNT_EXTRATO"+
			" ON NUM_AUTO = NUM_AUTO_INFRACAO"+
			" WHERE DAT_STATUS = '"+sdf.format(new Date())+"'"+
			" AND (COD_STATUS = 0 OR COD_STATUS = 10 OR COD_STATUS = 99)"+
			" ORDER BY NUM_CNH ASC, COD_STATUS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			while(rs.next()) {  
				//Verifica se est� na intera��o de uma nova CNH.
				if(numCnh != rs.getInt("NUM_CNH")){
					quantidadeAut = quantidadePen = quantidadeImp = somatorioAut = somatorioPen = somatorioImp = 0;
					numCnh = rs.getInt("NUM_CNH");
					sCmd = "SELECT * FROM TPNT_SALDOS WHERE NUM_CNH = "+numCnh;
					ResultSet auxRs = auxStmt.executeQuery(sCmd);
					/*
					 * Verifica se ser� realizada uma a��o de INSERT ou UPDATE para o saldo
					 * da CNH.
					 */
					if(auxRs.next()){
						quantidadeAut = auxRs.getInt("QTD_AUTO_AUTUACAO");
						somatorioAut =	auxRs.getInt("NUM_SOMA_PTS_AUT");
						quantidadePen = auxRs.getInt("QTD_AUTO_PENALIDADE");
						somatorioPen = auxRs.getInt("NUM_SOMA_PTS_PENAL");
						quantidadeImp = auxRs.getInt("QTD_AUTO_IMPUTADOS");
						somatorioImp = auxRs.getInt("NUM_SOMA_PTS_IMPUT");
						flagUpdate = true;
					}
					else 
						flagUpdate = false;
				}
				
				/*
				 * Verifica os campos que ter�o os registros acrescidos, dependendo
				 *do status do auto.
				 */ 
				int codStatus = rs.getInt("COD_STATUS");
				if(codStatus == 0){
					campoQuantidade = "QTD_AUTO_AUTUACAO";
					campoSomatorio = "NUM_SOMA_PTS_AUT";
					quantidade = ++quantidadeAut;
					somatorioAut = somatorioAut + rs.getInt("NUM_PONTOS");
					somatorio = somatorioAut;
				}
				else if(codStatus == 10){
					campoQuantidade = "QTD_AUTO_PENALIDADE";
					campoSomatorio = "NUM_SOMA_PTS_PENAL";
					quantidade = ++quantidadePen;
					somatorioPen = somatorioPen + rs.getInt("NUM_PONTOS");
					somatorio = somatorioPen;
				}
				else{
					campoQuantidade = "QTD_AUTO_IMPUTADOS";
					campoSomatorio = "NUM_SOMA_PTS_IMPUT";
					quantidade = ++quantidadeImp;
					somatorioImp = somatorioImp + rs.getInt("NUM_PONTOS");
					somatorio = somatorioImp;
				}
				
				// Autualiza a base com os campos a que sofreram acr�scimo de valor.
				if(!flagUpdate){
					sCmd = "INSERT INTO TPNT_SALDOS"+
					"(ID_SALDO,NUM_CNH,TIP_CNH,"+campoQuantidade+","+campoSomatorio+")"+
					"VALUES (SEQ_PNT_SALDOS.NEXTVAL,"+numCnh+",1,"+quantidade+","+somatorio+")";
					auxStmt.execute(sCmd);
					flagUpdate = true;
					continue;
				}
				sCmd = "UPDATE TPNT_SALDOS SET "+campoQuantidade+" = "+quantidade+","+campoSomatorio+" = "+somatorio+
				" WHERE NUM_CNH = "+numCnh;
				auxStmt.executeUpdate(sCmd);
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		} 
	}
	
	
	/**
	 * M�todo para implementa��o do fechamento di�rio 3.3
	 * Localiza CNHs que alca�aram 13 pontos e que ainda n�o foram advertidas, e 
	 * providencia uma notifica��o.
	 * 
	 * @author Sergio Roberto Junior
	 * @since 17/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected List Localiza13Pontos(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String tipo = null;
			String header = null;
			List retorno = null;
			int contador = 0;
			String sCmd = "SELECT * FROM TSMI_MUNICIPIO M JOIN TSMI_ENDERECO E JOIN TSMI_RESPONSAVEL R JOIN TPNT_SALDOS S"+
			" ON R.NUM_CNH = S.NUM_CNH ON E.SEQ_ENDERECO = R.SEQ_ENDERECO ON M.COD_MUNICIPIO = E.COD_MUNICIPIO"+
			" WHERE DAT_ADVERTENCIA IS NULL"+
			" AND NUM_SOMA_PTS_AUT >= 13"+
			" ORDER BY R.NUM_CNH ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			//Carrega os valores necess�rio para criar o header do arquivo.
			if(rs.isBeforeFirst()){
				sCmd = "SELECT SEQ_TSMI_COD_ARQUIVO.NEXTVAL AS IDENTIFICACAO FROM DUAL";
				auxRs = auxStmt.executeQuery(sCmd);
				auxRs.next();
				String identArquivo = auxRs.getString("IDENTIFICACAO");
				
				retorno = new ArrayList();
				retorno.add(identArquivo);
			}
			while(rs.next()) {  
				//Atualiza a data de advertencia para a respectiva CNH.
				sCmd = "UPDATE TPNT_SALDOS SET DAT_ADVERTENCIA = '"+sdf.format(new Date())+"'"+
				" WHERE ID_SALDO = "+rs.getString("ID_SALDO");
				auxStmt.executeUpdate(sCmd);
				
				//Verifica se o CNH possui auto de vinte pontos emitidos no �ltimo ano.
				sCmd = "SELECT MAX(DAT_PROCESSO) AS DAT_PROCESSO FROM TPNT_AUTO_20_PTS WHERE NUM_CNH = "+rs.getString("NUM_CNH");
				auxRs = auxStmt.executeQuery(sCmd);
				if(auxRs.next()){
					if(Util.DifereDias(sdf.format(new Date()),sdf.format(auxRs.getDate("DAT_PROCESSO"))) > 365)
						tipo = "2";
					else
						tipo = "1";
				}
				else
					tipo = "1";
				
				//Acrescenta as linhas e incrementa o contador para o arquivo Emitevex.
				addLinhas(auxStmt,rs,retorno,contador,"4",tipo);	
			}
			if(retorno != null)
				retorno.add((new Integer(contador)).toString());
			return retorno;
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	}
	
	/**
	 * M�todo para implementa��o do fechamento di�rio 3.4
	 * Localiza CNHs que alca�aram 20 pontos imputados e gera o auto de vinte pontos.
	 * 
	 * @author Sergio Roberto Junior
	 * @since 18/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void Localiza20Pontos(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Consulta o diret�rio onde ser�o gravados os arquivos EMITEVEX.
			ParamSistemaBean paramReg = new ParamSistemaBean();
			paramReg.setCodSistema("21"); //M�DULO PONTUA��O	
			paramReg.PreparaParam(conn);
			String numPontosCnh = paramReg.getParamSist("NUM_PONTOS_CNH");
			
			String sCmd = "SELECT NUM_CNH, (SELECT MAX(ID_EXTRATO) FROM TPNT_EXTRATO E2"+			
			" WHERE E.NUM_CNH = E2.NUM_CNH AND ROWNUM = 1) AS ID_EXTRATO"+
			" FROM TBRK_AUTO_INFRACAO A JOIN TPNT_EXTRATO E"+
			" ON E.NUM_AUTO_INFRACAO = A.NUM_AUTO"+
			" WHERE ID_AUTO_20_PTS IS NULL"+
			" GROUP BY NUM_CNH"+
			" HAVING SUM(NUM_PONTOS) >= "+numPontosCnh;
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				
				//Guardar da chave prim�ria que ser� criada para o auto de entrega de CNH.
				sCmd = "SELECT SEQ_PNT_AUTO_20_PTS.NEXTVAL AS SEQ FROM DUAL";
				auxRs = auxStmt.executeQuery(sCmd);
				auxRs.next();
				String seq = auxRs.getString("SEQ");
				
				//Busca as informa��es necess�rias para gerar um auto de entrega de CNH.
				sCmd = "SELECT * FROM TPNT_EXTRATO E JOIN TBRK_AUTO_INFRACAO A" +
				" ON E.NUM_AUTO_INFRACAO = A.NUM_AUTO" +
				" WHERE ID_EXTRATO = "+rs.getString("ID_EXTRATO");
				auxRs = auxStmt.executeQuery(sCmd);
				
				/*
				 * Interrompe essa intera��o para n�o gerar uma exception no momento
				 * da gera��o do auto de entrega de CNH.
				 */
				if(!auxRs.next())
					continue;
				
				//Gera o n�mero para o auto de entrega de CNH.
				String numAuto20Pts = "2"+auxRs.getString("NUM_AUTO_INFRACAO").substring(1);
				String numProcesso = auxRs.getString("NUM_PROCESSO");		
				
				//Realiza a inser��o de uma auto de vinte pontos para a CNH respectiva.
				sCmd = "INSERT INTO TPNT_AUTO_20_PTS (ID_AUTO_20_PTS,NUM_CNH,TIP_CNH,"+
				"NUM_AUTO_INFRACAO,NUM_PROCESSO,DAT_PROCESSO,DAT_STATUS,COD_STATUS,"+
				"COD_ORGAO_LOTACAO,TXT_USERNAME,DAT_PROCESSAMENTO)"+
				" VALUES("+seq+","+rs.getString("NUM_CNH")+",'"+
				auxRs.getString("TIP_CNH")+"','"+numAuto20Pts+"','"+
				numProcesso+"','"+sdf.format(auxRs.getDate("DAT_PROCESSO"))+"','"+
				sdf.format(new Date())+"',100,1,'SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);
				
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 
				
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+numAuto20Pts+"','"+
				numProcesso+"','100','"+sdf.format(new Date())+"','500','012','"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+
				sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);
				
				//Atualiza a referencia para o auto de entrega de CNH para a tabela de extrato.
				sCmd = "UPDATE TPNT_EXTRATO SET ID_AUTO_20_PTS = '"+seq+"',"+
				" NUM_AUTO_20_PTS = '"+numAuto20Pts+"'"+
				" WHERE ID_AUTO_20_PTS IS NULL"+
				" AND NUM_CNH = "+rs.getString("NUM_CNH");
				auxStmt.executeUpdate(sCmd);
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		} 
	}
	
	/**
	 * M�todo para implementa��o do fechamento di�rio 3.5
	 * Preparar os arquivos com as notifica��es geradas pelos autos de 20 pontos.
	 * 
	 * @author Sergio Roberto Junior
	 * @since 19/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected List PreparaNotif20Pontos(Connection conn) throws RobotException{
		List retorno = null;
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			int contador = 0;
			String sCmd = "SELECT * FROM TSMI_MUNICIPIO M JOIN TSMI_ENDERECO E JOIN TSMI_RESPONSAVEL R JOIN TPNT_AUTO_20_PTS A"+
			" ON R.NUM_CNH = A.NUM_CNH ON E.SEQ_ENDERECO = R.SEQ_ENDERECO ON M.COD_MUNICIPIO = E.COD_MUNICIPIO"+
			" WHERE COD_STATUS = 100"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			//Carrega os valores necess�rio para criar o header do arquivo.
			if(rs.isBeforeFirst()){
				sCmd = "SELECT SEQ_TSMI_COD_ARQUIVO.NEXTVAL AS IDENTIFICACAO FROM DUAL";
				auxRs = auxStmt.executeQuery(sCmd);
				auxRs.next();
				String identArquivo = auxRs.getString("IDENTIFICACAO");
				
				retorno = new ArrayList();
				retorno.add(identArquivo);
			}
			
			while(rs.next()){
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = 101, DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
				
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 				
			
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','101','"+sdf.format(new Date())+"','501','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);

				//Acrescenta as linhas e incrementa o contador para o arquivo Emitevex.
				addLinhas(auxStmt,rs,retorno,contador,"3","2");	
			}
			
			//Acrescenta o total de autos a serem incluidos no arquivo de envio.
			if(retorno != null)
				retorno.add((new Integer(contador)).toString());
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
		return retorno;
	}
	
	/**
	 * M�todo para implementa��o do fechamento di�rio 3.6
	 * Verificar� os Autos relativos a Pontua��o que estejam com status 104 
	 * e tiveram decurso de prazo (15 dias � par�metro) ou que tiveram 
	 * recurso de justificativa de pontua��o e foram indeferidos 
	 * (status 105 com resultado do recurso indeferido) e os converte para o status 108 
	 * (Aguardando determino do Presidente).
	 * 
	 * @author Sergio Roberto Junior
	 * @since 20/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void AguardaDeterminoAuto(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Carrega os dias a serem utilizados para verifica��o do decurso de prazo.
			ParamSistemaBean paramReg = new ParamSistemaBean();
			paramReg.setCodSistema("21"); //M�DULO PONTUA��O	
			paramReg.PreparaParam(conn);
			int DIAS_DEF_PREVIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_DEF_PREVIA_PNT"));
			int DIAS_FOLGA_DEFPREVIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_FOLGA_DEFPREVIA_PNT"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH,-(DIAS_DEF_PREVIA_PNT+DIAS_FOLGA_DEFPREVIA_PNT));
			int contador = 0;
			
			String sCmd = "SELECT * FROM TPNT_AUTO_20_PTS A"+
			" WHERE (COD_STATUS = 104 AND DAT_STATUS < '"+sdf.format(calendar.getTime())+"')"+
			" OR (COD_STATUS = 105 AND ID_AUTO_20_PTS = " +
			" (SELECT ID_AUTO_20_PTS FROM TPNT_REQUERIMENTO R"+
			"  WHERE R.ID_AUTO_20_PTS = A.ID_AUTO_20_PTS AND TXT_RESULTADO = 'I'))"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = 108, DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
                
				//Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 
				
			    //Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','108','"+sdf.format(new Date())+"','502','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);		
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	}

	/**
	 * Localizar os Autos 20 Pontos que estejam com status de Penalidade de 
	 * Pontua��o Registrada (110), preparar o arquivo com as notifica��es 
	 * a serem emitidas pela VEX e mudar o Status do Auto 20 Pontos para 111.
	 * 
	 * @author Sergio Roberto Junior
	 * @since 20/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected List PreparaNotifVEX(Connection conn) throws RobotException{
		List retorno = null;
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			int contador = 0;
			String sCmd = "SELECT * FROM TSMI_MUNICIPIO M JOIN TSMI_ENDERECO E JOIN TSMI_RESPONSAVEL R JOIN TPNT_AUTO_20_PTS A"+
			" ON R.NUM_CNH = A.NUM_CNH ON E.SEQ_ENDERECO = R.SEQ_ENDERECO ON M.COD_MUNICIPIO = E.COD_MUNICIPIO"+
			" WHERE COD_STATUS = 110"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			//Carrega os valores necess�rio para criar o header do arquivo.
			if(rs.isBeforeFirst()){
				sCmd = "SELECT SEQ_TSMI_COD_ARQUIVO.NEXTVAL AS IDENTIFICACAO FROM DUAL";
				auxRs = auxStmt.executeQuery(sCmd);
				auxRs.next();
				String identArquivo = auxRs.getString("IDENTIFICACAO");
				
				retorno = new ArrayList();
				retorno.add(identArquivo);
			}
			
			while(rs.next()){
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = 111, DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 
			
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','111','"+sdf.format(new Date())+"','503','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);	
				
				//Acrescenta as linhas e incrementa o contador para o arquivo Emitevex.
				addLinhas(auxStmt,rs,retorno,contador,"6","2");
			}
			
			//Acrescenta o total de autos a serem incluidos no arquivo de envio.
			if(retorno != null)
				retorno.add((new Integer(contador)).toString());
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
		return retorno;
	}
	
	/**
	 * Verificar� os Autos relativos a Pontua��o que estejam com status 145 
	 * e tiveram decurso de prazo em Recurso de 1� Inst�ncia (5 dias � par�metro) 
	 * e os converte para o status 146 (Aguardando prazo de apreens�o).
	 * 
	 * @author Sergio Roberto Junior
	 * @since 24/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void AguardandoApreensaoAuto(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Carrega os dias a serem utilizados para verifica��o do decurso de prazo.
			ParamSistemaBean paramReg = new ParamSistemaBean();
			paramReg.setCodSistema("21"); //M�DULO PONTUA��O	
			paramReg.PreparaParam(conn);
			int DIAS_1A_INSTANCIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_1A_INSTANCIA_PNT"));
			int DIAS_FOLGA_1AINSTANCIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_FOLGA_1AINSTANCIA_PNT"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH,-(DIAS_1A_INSTANCIA_PNT+DIAS_FOLGA_1AINSTANCIA_PNT));
			int contador = 0;
						
			String sCmd = "SELECT * FROM TPNT_AUTO_20_PTS A"+
			" WHERE COD_STATUS = 145 AND DAT_STATUS < '"+sdf.format(calendar.getTime())+"'"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = 146, DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
				
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 				
			
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','146','"+sdf.format(new Date())+"','504','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);		
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	}

	/**
	 * Localizar os Autos 20 Pontos que estejam com status de Aguardando 
	 * prazo de apreens�o  (146) e os converte para o status 150 (CNH a ser devolvida).
	 * 
	 * @author Sergio Roberto Junior
	 * @since 24/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void DevolveCNH(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Carrega os dias a serem utilizados para verifica��o do decurso de prazo.
			ParamSistemaBean paramReg = new ParamSistemaBean();
			paramReg.setCodSistema("21"); //M�DULO PONTUA��O	
			paramReg.PreparaParam(conn);
			int DIAS_APREENSAO_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_APREENSAO_PNT"));
			int DIAS_FOLGA_APREENSAO_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_FOLGA_APREENSAO_PNT"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH,-(DIAS_APREENSAO_PNT+DIAS_FOLGA_APREENSAO_PNT));
			int contador = 0;
			
			String sCmd = "SELECT * FROM TPNT_AUTO_20_PTS A"+
			" WHERE COD_STATUS = 146 AND DAT_STATUS < '"+sdf.format(calendar.getTime())+"'"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);  
			
			while(rs.next()){
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = 150, DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
				
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 
			
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','150','"+sdf.format(new Date())+"','505','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);		
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	}

	/**
	 * Verificar� os Autos relativos a Pontua��o que estejam com status 130 
	 * e tiveram decurso de prazo em Recurso de 2� Inst�ncia (5 dias � par�metro)
	 *  e os converte para o status 
	 * 
	 * @author Sergio Roberto Junior
	 * @since 24/05/2005
	 * @param conn
	 * @throws RobotException
	 * @version 1.0
	 */
	protected void VerifDec2Instancia(Connection conn) throws RobotException{
		try {            	
			Statement stmt = conn.createStatement();
			Statement auxStmt = conn.createStatement();
			ResultSet auxRs = null;
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			//Carrega os dias a serem utilizados para verifica��o do decurso de prazo.
			ParamSistemaBean paramReg = new ParamSistemaBean();
			paramReg.setCodSistema("21"); //M�DULO PONTUA��O	
			paramReg.PreparaParam(conn);
			int DIAS_2A_INSTANCIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_2A_INSTANCIA_PNT"));
			int DIAS_FOLGA_2AINSTANCIA_PNT = Integer.parseInt(paramReg.getParamSist("DIAS_FOLGA_2AINSTANCIA_PNT"));
			
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH,-(DIAS_2A_INSTANCIA_PNT+DIAS_FOLGA_2AINSTANCIA_PNT));
			int contador = 0;
			
			String sCmd = "SELECT * FROM TPNT_AUTO_20_PTS A"+
			" WHERE COD_STATUS = 130" +
			" AND (DAT_STATUS < '"+sdf.format(calendar.getTime())+"'"+
			" OR DAT_STATUS < '"+sdf.format(calendar.getTime())+"')"+
			" ORDER BY ID_AUTO_20_PTS ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next()){
				String codStatus = null;
				if(rs.getDate("DAT_STATUS").compareTo(new Date())>30)
					codStatus = "150";
				else
					codStatus = "146";
				// Realiza a atualiza��o sobre o status do referido auto de 20 pontos.
				sCmd = "UPDATE TPNT_AUTO_20_PTS SET COD_STATUS = "+codStatus+", DAT_STATUS = '"+sdf.format(new Date())+"',"+
				" DAT_PROCESSAMENTO = '"+sdf.format(new Date())+"'"+
				" WHERE ID_AUTO_20_PTS = "+rs.getString("ID_AUTO_20_PTS");
				auxStmt.executeUpdate(sCmd);
			
                //Busca o �rg�o via par�metro
				ACSS.OrgaoBean orgaoBeanId = new ACSS.OrgaoBean();
				orgaoBeanId.Le_Orgao(rs.getString("COD_ORGAO_LOTACAO"),0);
			    ParamUFBean paramUFBeanId = new ParamUFBean();
			    paramUFBeanId.PreparaParam(orgaoBeanId.getEndereco().getCodUF()); 
				
				
				//Realiza a inser��o de um registro sobre a cria��o do auto na tabela de hist�rico.
				sCmd = "INSERT INTO TPNT_HISTORICO (ID_HISTORICO,NUM_AUTO_INFRACAO,NUM_PROCESSO,"+
				"COD_STATUS,DAT_STATUS,COD_EVENTO,NUM_ORIGEM_EVENTO,COD_ORGAO_LOTACAO,TXT_USERNAME,"+
				"DAT_PROCESSAMENTO)"+
				" VALUES(SEQ_PNT_HISTORICO.NEXTVAL,'"+rs.getString("ID_AUTO_20_PTS")+"','"+
				rs.getString("NUM_PROCESSO")+"','"+codStatus+"','"+sdf.format(new Date())+"','505','015',"+
				"'"+paramUFBeanId.getParamUF("TXT_ORGAO")+"','SMIT','"+sdf.format(new Date())+"')";
				auxStmt.execute(sCmd);		
			}
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());            
		}   
	} 
	
	
	/**
	 * M�todo criado para definir uma linha para arquivos do tipo Emitevex e eliminar
	 * a replica��o de codifica��o das intru��es na classe.
	 * @param auxStmt
	 * @param rs
	 * @param retorno
	 * @param contador
	 * @author Sergio Roberto Junior
	 * @since 20/05/2005
	 * @throws SQLException
	 * @version 1.0
	 */
	private void addLinhas(Statement auxStmt, ResultSet rs, List retorno, int contador,String tipAdvertencia, String tipNotificacao) throws SQLException{
		//L� os valores necess�rios para a cria��o do arquivo.
		String sCmd = "SELECT SEQ_TSMI_NOTIFICACAO.NEXTVAL AS NOTIFICACAO FROM DUAL";
		ResultSet auxRs = auxStmt.executeQuery(sCmd);
		auxRs.next();
		String numNotificacao = auxRs.getString("NOTIFICACAO");
		
		String linha = "4100  "+Util.lPad(numNotificacao," ",9)+"00"+Util.lPad("","0",8)+rs.getString("NOM_RESPONSAVEL")+
		Util.rPad("","0",15)+Util.rPad(""," ",77)+Util.rPad("","0",8)+Util.rPad(""," ",45)+
		Util.rPad("","0",5)+Util.rPad(""," ",97)+Util.rPad(rs.getString("TXT_ENDERECO")," ",60)+
		Util.lPad(rs.getString("NUM_CEP"),"0",8)+Util.rPad(rs.getString("NOM_MUNICIPIO")," ",40)+
		Util.rPad(rs.getString("COD_UF")," ",2)+Util.rPad(""," ",48)+"    "+tipNotificacao+tipAdvertencia+Util.rPad(""," ",6)+
		Util.rPad("","0",6)+Util.rPad(""," ",12)+Util.rPad("","0",11)+Util.rPad(""," ",7)+
		Util.rPad("","0",15)+Util.rPad(""," ",25)+Util.rPad("","0",8)+Util.rPad(""," ",119)+
		Util.rPad(rs.getString("IND_TIPO_CNH")," ",1)+Util.rPad(rs.getString("NUM_CNH")," ",11);				
		retorno.add(linha);
		contador++;		
	}
	
}
