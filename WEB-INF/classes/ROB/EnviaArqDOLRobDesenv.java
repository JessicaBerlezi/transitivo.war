package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;

import ACSS.ParamSistemaBean;


public class EnviaArqDOLRobDesenv {
	
	private static final String NOM_ROBOT = "EnviaArqDOLRob";
	
	
	public EnviaArqDOLRobDesenv() throws RobotException {
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		ArrayList lNivel = new ArrayList();
		String codRetorno = "";		
		
		try {
			
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			DaoBroker broker = DaoBroker.getInstance();
			
			ParamSistemaBean param = new ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO	
			param.PreparaParam(conn);
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	
			String emailBroker   = param.getParamSist("EMAIL_LOG_BROKER");
			String codigoErros   = param.getParamSist("COD_ERRO_RETORNO_DOL");
			REG.ArquivoDolBean arqRecebido = new REG.ArquivoDolBean (); 
			
			ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"SMIT_DOL");
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			boolean processoParado = false;		
			Vector resArquivos = dao.BuscarListArquivosDOL("0,7,8", conn);
			for (int i = 0; i < resArquivos.size(); i++) {			
				//Limpa a mem�ria
				System.gc();
				
				//Verifica se foi marcado para parar
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;				
				
				arqRecebido = (REG.ArquivoDolBean) resArquivos.get(i);				
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio do Envio do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio do Envio do Arquivo: " + arqRecebido.getNomArquivo());
				
				
				arqRecebido.setConn(conn);				
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setCodStatus("7"); //Em processamento							
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();				
				
				dao.CarregarLinhasArquivoDOL(arqRecebido, conn);
				
				//Preparar a transa��o para envio
				boolean erroBrokerArq = false;                
				int QtdLoteErro = Integer.parseInt(arqRecebido.getQtdLoteErro());
				
				Iterator ite = arqRecebido.getLote().iterator();
				List lResultado = new ArrayList();				
				//Processa todos os lotes do arquivo
				while (ite.hasNext()) {
					
					//Verifica se foi marcado para parar
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					} 
					
					REG.LoteDolBean loteDol = (REG.LoteDolBean) ite.next();
					
					//Verifica se o lote j� foi processado								
					if ("078".indexOf(loteDol.getCodStatus()) >= 0) {
						
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Lote: " + 
								loteDol.getNumLote(), monitor.MSG_INFO);
						
						loteDol.setConn(conn);
						loteDol.setCodStatus("7"); //Em processamento							
						if (!loteDol.isUpdate("E"))
							throw new DaoException("Erro ao atualizar Lote: " + loteDol.getNumLote() + " !");
						conn.commit();				
						
						boolean erroBrokerLote = false;
						int QtdLinhaErro = Integer.parseInt(loteDol.getQtdLinhaErro());
						int QtdLinProc = 0;
						
						Iterator iteLin = loteDol.getLinhaLoteDol().iterator();
						//Processa todas as linhas do lote
						String msgBroker = "";
						String msgRejeito = "";
						while (iteLin.hasNext()) {
							//Verifica se foi marcado para parar
							if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
								processoParado = true;
								break;
							} 
							
							if ((QtdLinProc%200)==0)
								monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Lote: " +    
										loteDol.getNumLote() + " Linhas: " + QtdLinProc + "/" + loteDol.
										getLinhaLoteDol().size() + " Linhas com Erro: " + QtdLinhaErro,
										monitor.MSG_INFO);
							
							REG.LinhaLoteDolBean linhaLote = (REG.LinhaLoteDolBean) iteLin.next();
							String VlCancelado=linhaLote.getDscLinhaLote().substring(34, 35);							
							String codOrgLotacaoLinha=linhaLote.getDscLinhaLote().substring(28, 34);
							String codOrgAtuacaoLinha=linhaLote.getDscLinhaLote().substring(35, 41);
							String Placa = linhaLote.getNumPlaca();
							System.out.println(QtdLinProc+" - "+linhaLote.getNumPlaca());
							
							String linha = "";
							if (arqRecebido.getDscPortaria59().endsWith("P59"))
								linha = sys.Util.rPad(linhaLote.getDscLinhaLote(), " ", 554).substring(0,554);
							else    
								linha = sys.Util.rPad(linhaLote.getDscLinhaLote(), " ", 445).substring(0,445);
							
							
							String codMarcaModelo = linha.substring(60,67);
							if ("       ".equals(codMarcaModelo)) codMarcaModelo ="0000000";
							
							linha = linha.substring(0,60)+codMarcaModelo+linha.substring(67,linha.length()); 
							linha =	linha.substring(8,linha.length());
							
							BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();							
							//Verifica se a linha j� foi processada								
							if ("09".indexOf(linhaLote.getCodStatus()) >= 0) {
								
								int QtdGrav=0,QtdCanc=0;
								if  (!"5".equals(VlCancelado))
								{									
									if("0000000".equals(codMarcaModelo))
										linha = VerificaMarcaModelo(linha,Placa,arqRecebido.getDscPortaria59());	
									
									String resultado = broker.TransacaoDOL(arqRecebido.getNomUsername(),
											codOrgLotacaoLinha, arqRecebido.getTipArqRetorno(),
											codOrgAtuacaoLinha, loteDol.getNumLote(), loteDol.getNumAnoExercicio(),
											loteDol.getDatLote(), loteDol.getCodUnidade(), linha, conn,transBRKDOL, arqRecebido.getDscPortaria59());
									
									if(QtdLinProc==9)
										System.out.println(resultado);
									//Trocar as aspas simples por aspas no padrao oracle
									resultado = resultado.replaceAll("'", " ");
									//Pega os retornos da linha
									codRetorno = "ERR";
									if (resultado.indexOf("ERR-") < 0)
										codRetorno = resultado.substring(0, 3); 
									
									
									/*Caso Erro BROKER - CodRetorno n�o num�rico*/
									try {
										Integer.parseInt(codRetorno.trim());
									} catch (Exception e) {codRetorno="999";}
									
									
									
									
									/*Categoria/Tipo/Cor*/
									if (sys.Util.isNumber(codRetorno)){									
										if ("455,456,457".indexOf(codRetorno)>=0)
										{
											String linhaAnt=linha;
											linha = VerificaMarcaModelo(linha,Placa,arqRecebido.getDscPortaria59());
											
											if(linhaAnt.equals(linha)==false){
												resultado = broker.TransacaoDOL(arqRecebido.getNomUsername(),
														codOrgLotacaoLinha, arqRecebido.getTipArqRetorno(),
														codOrgAtuacaoLinha, loteDol.getNumLote(), loteDol.getNumAnoExercicio(),
														loteDol.getDatLote(), loteDol.getCodUnidade(), linha, conn,transBRKDOL,arqRecebido.getDscPortaria59());
											
											}
											
											
										}
										resultado = resultado.replaceAll("'", " ");
										codRetorno = "ERR";
										if (resultado.indexOf("ERR-") < 0)
											codRetorno = resultado.substring(0, 3);
										
										
										/*Caso Erro BROKER - CodRetorno n�o num�rico*/
										try {
											Integer.parseInt(codRetorno.trim());
										} catch (Exception e) {codRetorno="999";}
										
										
										
									}
									/**********************/								
									/*Enviar email caso erro Broker*/
									if (ListDestinatarios.size()>0)
									{
										Iterator it = ListDestinatarios.iterator();
										GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
										lNivel = new ArrayList();
										while (it.hasNext())
										{
											nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
											if (nivelProcesso.getValNivelProcesso().indexOf(codRetorno)>=0)
											{
												int contDest = 0;
												Iterator itx = nivelProcesso.getListNivelDestino().iterator();
												GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
												ArrayList lDestinatarios = new ArrayList();
												while (itx.hasNext())
												{
													destinatario=(GER.CadastraDestinatarioBean)itx.next();
													if(destinatario.getCodOrgao().equals(arqRecebido.getCodOrgao()))
													{
														//Mensagem de Erro de Execu��o e Broker Fora
														if(nivelProcesso.getTipoMsg().equals("E"))
														{
															msgBroker ="Cod.Orgao.......: "+arqRecebido.getCodOrgao()+"         Auto............: "+linhaLote.getNumAuto()+"\n" +
															"Cod.Transacao/ID: " +transBRKDOL.getSeqTransacao().substring(6,9)+"/"+transBRKDOL.getSeqTransacao().substring(0,6)+
															"   Cod.LogBroker...: "+transBRKDOL.getCodLogBroker()+"\n" +
															"Data Transacao..: "+sys.Util.getDataHoje().replaceAll("-","/")+"\n" +
															"-------------------------------------------------------------------------- \n";
															destinatario.getMsg().add(msgBroker);
														}
														
														
														//Mensagem de Rejeito
														if(nivelProcesso.getTipoMsg().equals("M"))
														{
															String sMotivo=ROB.DaoBroker.getInstance().buscarErroBroker(conn,codRetorno);
															msgRejeito ="Cod.Orgao....: "+ arqRecebido.getCodOrgao()+"     Auto.........: "+ linhaLote.getNumAuto()+" \n" +
															"Placa........: "+ linhaLote.getNumPlaca()    +"     Data Infracao: "+ linhaLote.getDataInfracao()+"\n"+
															"Hora Infracao: "+ linhaLote.getHoraInfracao()+"     Cod.Municipio: "+ linhaLote.getCodMunicipio()+"\n"+
															"Motivo.......: "+ sMotivo +"\n"+
															"-------------------------------------------------------------------------- \n";
															destinatario.getMsg().add(msgRejeito);
														}  
														lDestinatarios.add(destinatario);
														contDest++;
													}
												}
												
												nivelProcesso.setListNivelDestino(lDestinatarios);
												lNivel.add(nivelProcesso);
											}
										}
									}
									
									String linhaRetorno = "";
									if (codRetorno.equals("999")) /*Broker fora do ar*/
										linhaRetorno = resultado;
									else
										linhaRetorno = resultado.substring(5);
									
									if (linhaRetorno.length() > 900)
										linhaRetorno = linhaRetorno.substring(0, 900);
									linhaRetorno = linhaRetorno.trim();
									
									//codStatus 0=recebido 1=processado 9=erro no acesso ao Broker
									String codStatus = "9";																
									if (sys.Util.isNumber(codRetorno))
									{
										codStatus = "1";
										/*Reprocessamento*/
										if (codigoErros.indexOf(codRetorno)>=0)
										{
											codStatus="9";
											erroBrokerLote = true;
											erroBrokerArq = true;
										}
									}
									else
									{
										erroBrokerLote = true;
										erroBrokerArq = true;
									}
									
									if (!codRetorno.equals("000") && !erroBrokerLote){
										QtdLinhaErro++;
									}
									else
									{									
										linhaLote.gravarAutoInfracao(arqRecebido);
										QtdGrav++;
									}								
									//Gravar Status da Linha
									linhaLote.setConn(conn);
									linhaLote.setCodStatus(codStatus);
									linhaLote.setCodRetorno(codRetorno);
									linhaLote.setDscLinhaLoteRetorno(linhaRetorno);
									if (!linhaLote.isUpdate("E"))
										throw new DaoException("Erro ao atualizar Linha - 1: " + linhaLote.getNumSeqLinha() + " !");
									//Logar Execu��o da Linha								
									if (!linhaLote.logar())
										throw new DaoException("Erro ao atualizar Linha - 2: " + linhaLote.getNumSeqLinha() + " !");
									loteDol.setQtdLinhaErro(String.valueOf(QtdLinhaErro));								
								}else{
									
									QtdCanc++;							
									String codStatus="1";
									linhaLote.setConn(conn);
									linhaLote.setCodStatus(codStatus);
									linhaLote.setCodRetorno("000");
									linhaLote.setDscLinhaLoteRetorno("");
									if (!linhaLote.isUpdate("C"))
										throw new DaoException("Erro ao atualizar Linha - 3: " + linhaLote.getNumSeqLinha() + " !");									
									
								}
								conn.commit();
							}
							QtdLinProc++;                        
						}
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Lote: " +    
								loteDol.getNumLote() + " Linhas: " + QtdLinProc + "/" + loteDol.
								getLinhaLoteDol().size() + " Linhas com Erro: " + QtdLinhaErro,
								monitor.MSG_INFO);
						
						loteDol.setQtdLinhaErro(String.valueOf(QtdLinhaErro));
						if (processoParado || erroBrokerLote)
							loteDol.setCodStatus("8"); //Processo n�o finalizado
						else {
							loteDol.setCodStatus("2"); //Processado
							if (QtdLinhaErro > 0) QtdLoteErro++;
						}
						if (!loteDol.isUpdate(""))
							throw new DaoException("Erro ao atualizar Lote: " + loteDol.getNumLote() + " !");
						conn.commit();                                        
					}
					if (processoParado) break;
				}
				
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setQtdLoteErro(String.valueOf(QtdLoteErro));
				if (processoParado) {
					arqRecebido.setCodStatus("8"); //Processo n�o finalizado
					monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+ arqRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
				} else if (erroBrokerArq) {
					arqRecebido.setCodStatus("8"); //Processo n�o finalizado
					monitor.gravaMonitor("T�rmino com Erros do Arquivo "+arqRecebido.
							getNomArquivo(), monitor.MSG_INTERRUPCAO);
				} else {
					arqRecebido.setCodStatus("2"); //Processado
					monitor.gravaMonitor("T�rmino do Envio do Arquivo "+arqRecebido.
							getNomArquivo(), monitor.MSG_INTERRUPCAO);
				}							
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();
				if (processoParado) break;
			}
			
			/*Enviar email caso erro Broker*/
			if (lNivel.size()>0)
			{
				Iterator it = lNivel.iterator();
				GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
				lNivel = new ArrayList();
				while (it.hasNext())
				{
					nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
					if (nivelProcesso.getValNivelProcesso().indexOf(codRetorno)>=0) 
					{
						Iterator itx = nivelProcesso.getListNivelDestino().iterator();
						GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
						while (itx.hasNext())
						{
							destinatario=(GER.CadastraDestinatarioBean)itx.next();
							if(destinatario.getMsg().size()>0)
							{
								String sConteudo="";
								Iterator ity = destinatario.getMsg().iterator();			
								while (ity.hasNext()) 
									sConteudo+=(String)ity.next();
								
								sys.EmailBean EmailBeanId = new sys.EmailBean();
								EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
								EmailBeanId.setRemetente(emailBroker);
								EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+ 
										" - Arquivo: "+arqRecebido.getNomArquivo()+ 
										" - Data: "+arqRecebido.getDatRecebimento());
								EmailBeanId.setConteudoEmail(sConteudo);
								EmailBeanId.sendEmail();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Enviar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			System.out.println("Erro:"+ e.getMessage());
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (trava != null) {
				try {
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (RobotException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String args[])  throws RobotException {
		
		try {
			new EnviaArqDOLRobDesenv();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
	public String VerificaMarcaModelo(String linha,String Placa, String indP59) throws sys.DaoException {
		try {
			REG.Dao daoreg = REG.Dao.getInstance();
			String resultTrans = "";
			String dscErro = "";
			resultTrans = daoreg.ConsultaMarcaBroker(Placa);
			String codRetorno = resultTrans.substring(0, 3);
			if (codRetorno.equals("000")) 
			{
				String linhaMontada="";
				String codMarcaBroker = resultTrans.substring(10,17);
				String codCorVeiculo = resultTrans.substring(17,20);
				String codTipoVeiculo = resultTrans.substring(20,23);
				String codCategoriaVeiculo = resultTrans.substring(23,25);
				String codEspecieVeiculo = resultTrans.substring(25,27);		
				
				if (indP59.equals("P59"))
					linhaMontada += linha.substring(0,52)+codMarcaBroker+linha.substring(59,338)+codEspecieVeiculo+codCategoriaVeiculo+codTipoVeiculo+codCorVeiculo+linha.substring(348,linha.length());
				else
					linhaMontada += linha.substring(0,52)+codMarcaBroker+linha.substring(59,245)+codEspecieVeiculo+codCategoriaVeiculo+codTipoVeiculo+codCorVeiculo+linha.substring(255,linha.length());
				
				
				
				
				linha = linhaMontada;
			}
			return linha;
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}  
	}
	
	
	
	
}