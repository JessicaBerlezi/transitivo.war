package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import sys.DaoException;

import ACSS.ParamSistemaBean;

public class CarregaEquipEletrRob {
	
	private static String NOM_ROBOT = "CarregaEquipEletrRob";
	
	public CarregaEquipEletrRob(String tipArquivo) throws RobotException, DaoException {
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			
			ParamSistemaBean param = new ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO	
			param.PreparaParam(conn);
			
			REG.ArquivoRecebidoBean arqRecebido = new REG.ArquivoRecebidoBean ();			
			
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
//			try {               
//				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
//			} catch (Exception e) {
//				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
//				return;
//			}             
			
			boolean processoParado = false;
			String identArquivo = "'MR01'";
			
			Vector resArquivos = dao.BuscarListArquivosRecebidos(identArquivo, "2", conn);			
			for (int i = 0; i < resArquivos.size(); i++) {			
				//Limpa a mem�ria
				System.gc();
				
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;			    
				
				//Busca os arquivos recebidos pelo tipo passado

				arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(i);
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio da Leitura do Arquivo "+arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio da Leitura do Arquivo: " + arqRecebido.getNomArquivo());
				
				arqRecebido.setConn(conn);				
				dao.CarregarLinhasArquivo(arqRecebido, conn);
				
				//Preparar a transa��o para envio
				int QtdErros = Integer.parseInt(arqRecebido.getQtdLinhaErro());
				int QtdLinProc = 0;
				Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();
				
				while (ite.hasNext()) {				
					//Marca o inicio do processamento da linha
					
					//Verifica se foi marcado para parar
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					}					
					
					if ((QtdLinProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
								QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size() + 
								" Linhas com Erro: "+QtdErros, monitor.MSG_INFO);
					
				REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
					
				if (linhaArq.getDscLinhaArqRec().substring(130,131).equals("F")){
					if (!linhaArq.carregaEquipEletr())
						throw new DaoException("Erro ao carregaEquipEletr: " + linhaArq.getNumSeqLinha() + " !");
				}
				
				monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
						QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size()+
						" Linhas com Erro: "+QtdErros, monitor.MSG_INFO);
				
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setQtdLinhaErro(String.valueOf(QtdErros));
				
				
				
				if (processoParado) {
					arqRecebido.setCodStatus("8"); //Processo n�o finalizado
					monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+arqRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
				}
				
				if (processoParado) break;
			  }
			}	
			
			System.out.println("Fim do Processamento...... ");

			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Enviar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public static void main(String args[])  throws RobotException {
		
		try {
			String tipArquivo = "";
			if (args.length > 0) tipArquivo = args[0];
			
			new CarregaEquipEletrRob(tipArquivo);
			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}