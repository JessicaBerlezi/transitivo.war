package ROB;

import java.io.File;
import java.io.FileWriter;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import REG.DaoException;

public class DownLoadLOGRob {
    
    private static final String NOM_ROBOT = "DownLoadLOGRob";
    private static final String quebraLinha = "\n";
    
    public DownLoadLOGRob() throws RobotException, ROB.DaoException, sys.DaoException {
        
        Connection conn  = null;
        Statement trava = null;
        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
        try {            			
            conn = Dao.getInstance().getConnection();
                        
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); //M�DULO REGISTRO
            param.PreparaParam(conn);
            
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();
			
			String dirDownload  = param.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			String datProcLog   = param.getParamSist("DAT_PROC_LOG");
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			ACSS.OrgaoBean[] orgaos = Dao.getInstance().BuscarOrgaosIntegrados("0");
			for (int i = 0; i < orgaos.length; i++) 
			{
			  if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;
				
			  UsuarioBeanId.setCodOrgaoAtuacao(orgaos[i].getCodOrgao());
			  ParamOrgBeanId.PreparaParam(UsuarioBeanId);
			  if ("N".equals(ParamOrgBeanId.getParamOrgao("PROCESSA_LOG_TRANSACAO","N","10"))) 
			    continue;
			  
			  //Carrega os tipos de arquivos em um vetor de ArquivoRecebidoBean
			  String[]identArquivo = {"MR01","MX01","MZ01","MF01","RECJARI","ME01"};
			  
			  for(int j = 0; j < identArquivo.length; j++) 
			  {
				 REG.ArquivoRecebidoBean arqRecBean = new REG.ArquivoRecebidoBean();
				 arqRecBean.setCodIdentArquivo(identArquivo[j]);
				 arqRecBean.setCodStatus("2");
				 arqRecBean.setNumAnoExercicio(datProc.substring(datProc.length()-4,datProc.length()));
				 arqRecBean.setCodOrgao(orgaos[i].getCodOrgao());
				 arqRecBean.setDatProcessamentoDetran(datProc);				 
				 if (arqRecBean.getCodIdentArquivo().equals("MR01"))
					 arqRecBean.setNumTransBroker("'400'");
				 if (arqRecBean.getCodIdentArquivo().equals("MX01"))
					 arqRecBean.setNumTransBroker("'209'");
				 if (arqRecBean.getCodIdentArquivo().equals("MZ01"))
					 arqRecBean.setNumTransBroker("'214'");
				 if (arqRecBean.getCodIdentArquivo().equals("MF01"))
					 arqRecBean.setNumTransBroker("'407'");
				 if (arqRecBean.getCodIdentArquivo().equals("RECJARI"))
					 arqRecBean.setNumTransBroker("'206','208','209','210','226','326','328','334','335','329','336','330','337','350','351'");
				 //Prepara arquivos
				 Vector vResultado = new Vector();
				 if (arqRecBean.getCodIdentArquivo().equals("ME01"))
					 Dao.getInstance().VerificaPenalidade(datProcLog,sys.Util.lPad(UsuarioBeanId.getCodOrgaoAtuacao(),"0",6),arqRecBean,vResultado);
				 else
					 vResultado = Dao.getInstance().VerificaLogTransacao(datProcLog,sys.Util.lPad(UsuarioBeanId.getCodOrgaoAtuacao(),"0",6),arqRecBean,vResultado);
			      
				 //Gera Arquivos
				 if (arqRecBean.getLinhaArquivoRec().size()>0)
				 {
					 String sNumeroControle= ParamOrgBeanId.getParamOrgao("PROXIMO_LOG01","00001","10");
					 arqRecBean.setNumControleArq(sNumeroControle);					 
			         boolean bGeraMD01=arqRecBean.getCodIdentArquivo().equals("MR01");
					 gerarArquivo(arqRecBean, monitor,ParamOrgBeanId,dirDownload, conn);
					 if (bGeraMD01)
					 {
						 sNumeroControle= ParamOrgBeanId.getParamOrgao("PROXIMO_LOG01","00001","10");
						 arqRecBean.setNumControleArq(sNumeroControle);					 
						 arqRecBean.setCodIdentArquivo("MD01");
						 arqRecBean.setLinhaArquivoRec(vResultado);
						 gerarArquivo(arqRecBean, monitor,ParamOrgBeanId,dirDownload, conn);
					 }
				 }
			  }
			}
			param.setNomParam("DAT_PROC_LOG");			
			ACSS.Dao.getInstance().ParamSistLeBean(param);
			param.setValParametro(datProc);
			ACSS.Dao.getInstance().ParamUpdate(param);
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }

   private void gerarArquivo(REG.ArquivoRecebidoBean arqRecBean, sys.MonitorLog monitor,
		   REC.ParamOrgBean ParamOrgBeanId,String dirDownload, Connection conn) throws RobotException, sys.DaoException, DaoException, ROB.DaoException
   {
	      String sTipoTransacao=arqRecBean.getCodIdentArquivo();
		  arqRecBean.setCodIdentArquivo("LOG01");
		  arqRecBean.setNomArquivo(arqRecBean.getCodIdentRetornoGrv()+arqRecBean.getNumAnoExercicio()+
		  sys.Util.lPad(arqRecBean.getCodOrgao(), "0",6)+sys.Util.lPad(arqRecBean.getNumControleArq(),"0",5)+sTipoTransacao+
		  "."+arqRecBean.getSufixoRetornoGrv());
		  arqRecBean.setQtdReg(String.valueOf(arqRecBean.getLinhaArquivoRec().size()));
		  REG.Dao.getInstance().ArquivoInsert(arqRecBean,ParamOrgBeanId);
		  monitor.setNomObjeto(arqRecBean.getNomArquivo());
		  monitor.setCodOrgao(arqRecBean.getCodOrgao());
		  try {
			monitor.gravaMonitor("Inicio do Envio do Arquivo " + arqRecBean.getNomArquivo(), monitor.MSG_INICIO);
		  } catch (sys.DaoException e) {
			  throw new sys.DaoException(e.getMessage());
		  }
		  System.out.println("Inicio do Envio do Arquivo " + arqRecBean.getNomArquivo());
        
		  //Gera o arquivo txt do arquivo antigo
		  gerarArquivo(arqRecBean, dirDownload, monitor, conn);
		  arqRecBean.setConn(conn);
		  arqRecBean.setDatDownload(sys.Util.fmtData(new Date()));
		  if (!arqRecBean.isUpdate())
			  throw new DaoException("Erro ao atualizar Arquivo: " + arqRecBean.getNomArquivo() + " !");
        
		  monitor.setNomObjeto(arqRecBean.getNomArquivo());
		  monitor.setCodOrgao(arqRecBean.getCodOrgao());
		  monitor.gravaMonitor("T�rmino do Envio do Arquivo " + arqRecBean.getNomArquivo(), monitor.MSG_FIM);
   }

   private void gerarArquivo(REG.ArquivoRecebidoBean arquivo, String dirDownload, sys.MonitorLog monitor, 
	Connection conn) throws RobotException {
        
	try {
		FileWriter writer = null;
		writer = criarArquivo(arquivo, dirDownload);
            
		int QtdLinProc = 0;
		Iterator ite = arquivo.getLinhaArquivoRec().iterator();                    
		while (ite.hasNext()) 
		{
                
			if ((QtdLinProc%200) == 0)
				monitor.gravaMonitor("Arquivo: " + arquivo.getNomArquivo() + " Linhas: " +    
					QtdLinProc+"/"+ arquivo.getLinhaArquivoRec().size(), monitor.MSG_INFO);
                
			REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();                    
			gravarLinha(writer, arquivo, linhaArq, conn);
			QtdLinProc++;										
		}
            
		if (writer != null)	writer.close();
            
	} catch (Exception e) {
		throw new RobotException(e.getMessage());
	}
   }    
    
   public FileWriter criarArquivo(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
	throws RobotException {
        
	String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();			
        
	//Cria o arquivo
	FileWriter writer;
	try {			
		File dir = new File(caminho.substring(0, caminho.lastIndexOf("/")));
		dir.mkdirs();
		File arq = new File(caminho);
		arq.createNewFile();
		writer = new FileWriter(arq);
            
		//Grava o header do arquivo	
		if (!arqRecebido.getHeaderGrv().equals("")) {
			writer.write(arqRecebido.getHeaderGrv());
			writer.write(quebraLinha);
		}
            
	} catch (Exception e) {
		throw new RobotException(e.getMessage());
	}
	return writer;
   }
    
   private void gravarLinha(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
	REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
        
	try {
		if (writer != null) {			
			writer.write(linhaArq.getLinhaGrv(arqRecebido, conn));
			writer.write(quebraLinha);
		}
	} catch (Exception e) {
		throw new RobotException(e.getMessage());
	}
   }

    
  public static void main(String[] args) throws RobotException {
        
        try {
            new DownLoadLOGRob();			 
        } catch (Exception e) { 
            System.out.println(e); 
        } 
    }
    

}