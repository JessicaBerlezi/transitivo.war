package ROB;

import java.sql.Connection;
import java.util.Vector;

import PNT.NotificacaoBean;
import PNT.ProcessoBean;


/**
 * <b>Title:</b> 		SMIT - MODULO ROBOT<br>
 * <b>Description:</b> 	Rob� de Recupera��o Controle Vex <br>
 * <b>Copyright:</b> 	Copyright (c) 2004<br>
 * <b>Company: </b> 	MIND Informatica <br>
 * @author 				Alexandre Bahia
 * @version 			1.0
 */

public class RecuperacaoAutoPNTRob {
	
	private static final String NOM_ROBOT = "RecuperaAutoPNTRob";
	private static final int MAX_REG_PROCESSAR = 50000;
	private static final int DIAS_AVANCAR = 30;
	
	public RecuperacaoAutoPNTRob() throws RobotException {
		
		Connection conn = null;
		Connection connLog = null;
		String codRetornoProc = "";	
		String datProcessamento = sys.Util.formatedToday().substring(0,10);	
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);		
		try {
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			Dao dao = Dao.getInstance();
			REC.DaoBroker daoREC=REC.DaoBrokerFactory.getInstance();			
			
			NotificacaoBean myNotif = new NotificacaoBean(); 
			ProcessoBean myProcesso = new ProcessoBean();			
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);	
			
			REG.ControleVexBean regRecebido = new REG.ControleVexBean (); 
			
			boolean processoParado = false;		    
	
			//Limpa a mem�ria
			System.gc();

            //Busca o cod. retorno que ser� processado
            codRetornoProc = dao.BuscaCodRetornoProc(conn); 
            //atualiza a data in�cio do processamento
            dao.GravaInicioProcRecuperacao(conn,codRetornoProc);
            conn.commit();
			
	
            monitor.gravaMonitor("Inicio Recupera��o Arquivo Vex ", monitor.MSG_INICIO);
            System.out.println("Inicio Recupera��o Arquivo Vex ");
            int iContador=0;
            System.out.println("Contador==>"+iContador);
            while (true) {
    			//Verifica se foi marcado para parar
    			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
    				processoParado = true;
    			} 
                // Prepara lista de notifica��es que est�o retornados por cod de retorno de Credito
                // e ainda n�o foram recuperadas
                Vector resRegRecuperacao = dao.BuscarListRegRecuperacaoPNT(codRetornoProc,conn,MAX_REG_PROCESSAR);
                if ((processoParado) || (resRegRecuperacao.size()==0)) break;
            
    		    boolean brecupera = false;
   			    for (int j = 0; j < resRegRecuperacao.size(); j++) {
   			    	iContador++;
   			    	if (iContador%200==0) System.out.println("Registros Lidos :"+iContador);
   			    	
				   //Verifica se foi marcado para parar
				   regRecebido = (REG.ControleVexBean) resRegRecuperacao.get(j);
				   if (regRecebido.getNumAutoInfracao().length()>0) continue;
				   
				   myNotif.setNumNotificacao(regRecebido.getNumNotificacao());
				   myNotif.setSeqNotificacao(regRecebido.getSeqNotificacao());
				   myNotif.LerNotificacao("notificacao");
				   myProcesso.setCodProcesso(myNotif.getCodProcesso());
				   myProcesso.LeProcesso("codigo");
				   

				   System.out.println("Registro Lido :"+iContador+" +C�digo Retorno : "+codRetornoProc+" PK_COD_CONTROLE_VEX_MASTER : "+regRecebido.getPkCodControleVex());
				   brecupera = false;
                   // Notifica��o autua��o e Cod Status >2
				   if ((regRecebido.getTipNotificacao().equals("0")) && 
					   (Integer.parseInt(myProcesso.getCodStatus())>2))  brecupera = true;
                   // Notifica��o penalidade e Cod Status >12
				   if ((regRecebido.getTipNotificacao().equals("2")) &&
					   (Integer.parseInt(myProcesso.getCodStatus())>32)) brecupera = true;   					
				  
                   //	atualiza registro recuperado	
				   if (brecupera)   dao.GravaRecuperacao(regRecebido,conn,datProcessamento,myProcesso.getCodStatus());
				   else 			dao.AtualizaDataProxRecuperacao(conn,regRecebido,DIAS_AVANCAR);
				   
				   conn.commit();					
			    }  // Fim do FOR	
				monitor.gravaMonitor(" Auto: " + 
								regRecebido.getNumAutoInfracao(), monitor.MSG_INFO);	
				/*Retirar*/
				break;
            }  // Fim While
            
            
            while (true) {
    			//Verifica se foi marcado para parar
    			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
    				processoParado = true;
    			} 
                // Prepara lista de notifica��es que est�o retornados por cod de retorno de Credito
                // e ainda n�o foram recuperadas
                Vector resRegRecuperacao = dao.BuscarListRegRecuperacaoPNTNR(conn,MAX_REG_PROCESSAR);
                if ((processoParado) || (resRegRecuperacao.size()==0)) break;
            
    		    boolean brecupera = false;
   			    for (int j = 0; j < resRegRecuperacao.size(); j++) {
   			    	iContador++;
   			    	if (iContador%200==0) System.out.println("Registros Lidos :"+iContador);
   			    	
				   //Verifica se foi marcado para parar
				   regRecebido = (REG.ControleVexBean) resRegRecuperacao.get(j);
				   
				   myNotif.setNumNotificacao(regRecebido.getNumNotificacao());
				   myNotif.setSeqNotificacao(regRecebido.getSeqNotificacao());
				   myNotif.LerNotificacao("notificacao");
				   myProcesso.setCodProcesso(myNotif.getCodProcesso());
				   myProcesso.LeProcesso("codigo");
				   
				   System.out.println("Registro Lido :"+iContador+" +C�digo Retorno : "+codRetornoProc+" PK_COD_CONTROLE_VEX_MASTER : "+regRecebido.getPkCodControleVex());
				   brecupera = false;
                   // Notifica��o autua��o e Cod Status >2
				   if ((regRecebido.getTipNotificacao().equals("0")) && 
					   (Integer.parseInt(myProcesso.getCodStatus())>2))  brecupera = true;
                   // Notifica��o penalidade e Cod Status >12
				   if ((regRecebido.getTipNotificacao().equals("2")) &&
					   (Integer.parseInt(myProcesso.getCodStatus())>32)) brecupera = true;   					
				  
                   //	atualiza registro recuperado	
				   if (brecupera)   dao.GravaRecuperacaoPNT(regRecebido,conn,datProcessamento,myProcesso.getCodStatus());
				   else 			dao.AtualizaDataProxRecuperacao(conn,regRecebido,DIAS_AVANCAR);
				   
				   conn.commit();					
			    }  // Fim do FOR	
				monitor.gravaMonitor(" Auto: " + 
								regRecebido.getNumAutoInfracao(), monitor.MSG_INFO);	
				break;
            }  // Fim While
            
            
            
            
            
            
            
            
            
            
            
            
            
            
			if (processoParado) {
				monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+ regRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
			} else {
	            dao.GravaFinalProcRecuperacao(conn,codRetornoProc);
	            conn.commit();						            
				monitor.gravaMonitor("T�rmino Recupera��o Credito por Servi�o "+codRetornoProc+" ", 
							             monitor.MSG_INTERRUPCAO);
	            System.out.println("T�rmino Recupera��o Credito por Servi�o "+codRetornoProc+" ");				
			}					
		// Fim do Try
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Recuperar Credito por Servi�o: "+codRetornoProc+" " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			
			
		}
	}
	
	public static void main(String args[])  throws RobotException {
		
		try {
			new RecuperacaoAutoPNTRob();
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}