package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import sys.DaoException;

import REG.RelatArqRetornoBean;




public class ControleRetornoVEXRob {
	
	private static final String NOM_ROBOT = "ControleRetornoVEXRob";
	private static final String quebraLinha = "\n";
	
	public ControleRetornoVEXRob() throws RobotException, ROB.DaoException, DaoException {
		
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {            			
			conn = Dao.getInstance().getConnection();
			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean();
			REC.ParamOrgBean       ParamOrgBeanId     = new REC.ParamOrgBean();
			
			String datControleVex   = param.getParamSist("DAT_RETORNO_VEX");
			
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			
			monitor.gravaMonitor("Inicio Gera��o Controle Retorno VEX : Data de Controle:"+datControleVex+" Dt Proc:"+datProc, monitor.MSG_ERRO);			
			
			RelatArqRetornoBean ControleRetornoVEXID = new RelatArqRetornoBean();
			REG.Dao.getInstance().ConsultaRelDiarioRet(ControleRetornoVEXID,datControleVex,datProc);

			boolean bOk=true;
			for (int i=0;i<ControleRetornoVEXID.getListaArqs().size();i++) {
				System.out.println("Processando..."+ControleRetornoVEXID.getListaArqs(i).getNomArquivo());
				if ( (ControleRetornoVEXID.getListaArqs(i).getSitControleVEX().length()>0) ||
					  (!ControleRetornoVEXID.getListaArqs(i).getCodStatus().equals("P"))
				   )
				{
					System.out.println("Bypassando..."+ControleRetornoVEXID.getListaArqs(i).getNomArquivo());
					continue;
				}
				
				monitor.gravaMonitor("Processando Controle Retorno VEX - Arquivo:"+ControleRetornoVEXID.getListaArqs(i).getNomArquivo(), monitor.MSG_ERRO);
				
				if (ControleRetornoVEXID.getListaArqs(i).getNomArquivo().indexOf("EMITEPONTO")<0)
					bOk=Dao.getInstance().ControleRetornoVEX(ControleRetornoVEXID.getListaArqs(i));
				
				if (!bOk) {
					monitor.gravaMonitor("Erro no Controle Retorno VEX - Arquivo:"+ControleRetornoVEXID.getListaArqs(i).getNomArquivo(), monitor.MSG_ERRO);
					break;
				}
				
			}
			Dao.getInstance().ControleRetornoVEXPNT(datControleVex);
			
			if (bOk) {
				param.setNomParam("DAT_RETORNO_VEX");
				monitor.gravaMonitor("Fim Gera��o Controle Retorno VEX : Data de Controle:"+datControleVex+" Dt Proc:"+datProc, monitor.MSG_ERRO);
			}
			else
				monitor.gravaMonitor("Fim com Erro Gera��o Controle Retorno VEX : Data de Controle:"+datControleVex+" Dt Proc:"+datProc, monitor.MSG_ERRO);
				
			ACSS.Dao.getInstance().ParamSistLeBean(param);
			param.setValParametro(datProc);
			ACSS.Dao.getInstance().ParamUpdate(param);
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro processar ControleEmiteVEXRob: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new ControleRetornoVEXRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	
}