package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;

public class VerificaAcessoUsrRob {
    
	private static final String NOM_ROBOT = "VerificaAcessoUsrRob";
  
	public VerificaAcessoUsrRob() throws RobotException, DaoException {
        
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
        
		try {     
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
            
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			monitor.gravaMonitor("Inicio do processamento do rob� VerificaAcessoUsrRob", monitor.MSG_INICIO);
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			String nDias = param.getParamSist("QTD_DIAS_LIMITE_ACESSO");	
            ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"BLOQUEIA_USUARIO");
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");		

			//Verifica espa�o de dias sem acesso e bloqueia o usuario
			String sConteudoEmail = "Conforme solicitado, segue no corpo do" +
			" e-mail a relacao dos usu�rios que tiveram seu acesso" +
			" bloqueado por ficar(em) "+nDias+" dias sem acessar o Sistema.\n\n";
			String sCorpoEmail =  Dao.getInstance().bloqueiaUsuario(nDias);
			
			//Prepara e envia e-mail
			if (!sCorpoEmail.equals(""))
			{
			  ArrayList usuariosBloqueados = preparaEmail(ListDestinatarios, sConteudoEmail+sCorpoEmail);
			  enviaEmail(usuariosBloqueados, emailBroker) ;
			}
			monitor.gravaMonitor("Fim do processamento do rob� VerificaAcessoUsrRob ", monitor.MSG_FIM);

		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}

	 
	
	private ArrayList preparaEmail(ArrayList ListDestinatarios, String sConteudoEmail){
		// Prepara Email    
		ArrayList usuariosBloqueados = new ArrayList();
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(nivelProcesso.getTipoMsg().equals("M"))
							destinatario.getMsg().add(sConteudoEmail);

					if(destinatario.getMsg().size()>0)
					   lDestinatarios.add(destinatario);
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
					usuariosBloqueados.add(nivelProcesso);
			   }
			}
		  }
          return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuariosBloqueados, String emailBroker) throws Exception{	
		if (usuariosBloqueados.size()>0)
		{
			Iterator it = usuariosBloqueados.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
				{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							String sConteudo="";
							Iterator ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso());
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				   }
				}
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new VerificaAcessoUsrRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	

}