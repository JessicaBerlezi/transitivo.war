package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

import ACSS.ParamSistemaBean;

public class EnviaArqDetranRob {
	
	private static String NOM_ROBOT = "EnviaArqDetranRob";
	
	public EnviaArqDetranRob(String tipArquivo) throws RobotException, DaoException {
		
		//Define o nome especial do rob� pelo tipo passado
		if (tipArquivo.equals("MR")) NOM_ROBOT = NOM_ROBOT + "_MR";
		else if (tipArquivo.equals("ME")) NOM_ROBOT = NOM_ROBOT + "_ME";
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			DaoBroker broker = DaoBroker.getInstance();
			
			ParamSistemaBean param = new ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO	
			param.PreparaParam(conn);
			
			String emailBroker   = param.getParamSist("EMAIL_LOG_BROKER");
			String codigoErros   = param.getParamSist("COD_ERRO_RETORNO_MR");	
			REG.ArquivoRecebidoBean arqRecebido = new REG.ArquivoRecebidoBean ();			
			
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			boolean processoParado = false;
			String identArquivo = "'MC01','MX01','MZ01','MA01','MF01','RECJARI','CODRET','PE01'";
			if (tipArquivo.equals("MR")) identArquivo = "'MR01'";
			else if (tipArquivo.equals("ME")) identArquivo = "'ME01'";
			
			Vector resArquivos = dao.BuscarListArquivosRecebidos(identArquivo, "0,7,8", conn);			
			for (int i = 0; i < resArquivos.size(); i++) {			
				//Limpa a mem�ria
				System.gc();
				
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;			    
				
				//Busca os arquivos recebidos pelo tipo passado

				arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(i);
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio do Envio do Arquivo "+arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio do Envio do Arquivo: " + arqRecebido.getNomArquivo());
				
				arqRecebido.setConn(conn);				
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setCodStatus("7"); //Em processamento
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();				
				
				dao.CarregarLinhasArquivo(arqRecebido, conn);				
				
				//Preparar a transa��o para envio
				String numTrans = arqRecebido.getNumTransBroker();
				String tipArq = arqRecebido.getTipArqRetorno();
				boolean houveErroBroker = false;
				int QtdErros = Integer.parseInt(arqRecebido.getQtdLinhaErro());
				int QtdLinProc = 0;
				Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();
				List lResultado = new ArrayList();
				
				while (ite.hasNext()) {				
					//Marca o inicio do processamento da linha
					Date iniLinha = new Date();				    
					
					//Verifica se foi marcado para parar
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					}
					
					
					
					if ((QtdLinProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
								QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size() + 
								" Linhas com Erro: "+QtdErros, monitor.MSG_INFO);
					
					REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
					BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();					
					
					String linha = sys.Util.rPad(linhaArq.getDscLinhaArqRec(), " ", 900);					
					if (!arqRecebido.getCodIdentArquivo().equals("CODRET"))
						linha = linha.substring(1);
					
					//Verifica se a linha j� foi processada								
					if ("09".indexOf(linhaArq.getCodStatus()) >= 0) {
						
						//Marca o inicio do processamento do Broker
						Date iniBroker = new Date();
						String resultado = "";
						if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
							resultado = broker.TransacaoJari(arqRecebido.getNomUsername(), 
									    arqRecebido.getCodOrgaoLotacao(),arqRecebido.getCodOrgao(),
									    arqRecebido.getResponsavelArq(),linha,conn,transBRKDOL, arqRecebido.getDscPortaria59());
						
						
						else if (arqRecebido.getCodIdentArquivo().equals("CODRET")){
							if("1".equals(param.getParamSist("HABILITA_CONTROLE_NOTIFICACAO"))){
								resultado = broker.TransacaoRetorno(arqRecebido.getNomUsername(),arqRecebido.getCodOrgao(),arqRecebido.getCodOrgaoLotacao(), linhaArq, conn, transBRKDOL, datProc);
							}
							else
								resultado = broker.TransacaoRetorno(arqRecebido.getNomUsername(),arqRecebido.getCodOrgao(),arqRecebido.getCodOrgaoLotacao(), linha, conn, transBRKDOL);
						}
						else	
						{
							/*Verificar Foto no MR*/
							if ( (arqRecebido.getCodIdentArquivo().equals("MR01")) || (arqRecebido.getCodIdentArquivo().equals("ME01")) )
							{
								 String numAparelho    =linha.substring(188,218).trim();
								 String tipoDispositivo=linha.substring(187,188);
								 String numAutoInfracao=linha.substring(0,12).trim();
								 ACSS.OrgaoBean OrgaoIntegrado_Isolado = new ACSS.OrgaoBean();
								 OrgaoIntegrado_Isolado.setCodOrgao(arqRecebido.getCodOrgao());
								 OrgaoIntegrado_Isolado.Le_Orgao(arqRecebido.getCodOrgao(),0);
								 if ( (numAparelho.length()>0) && (OrgaoIntegrado_Isolado.getIntegrada().equals("S")) && (tipoDispositivo.equals("F")))
								 {
									 
   								     if (linhaArq.buscarFoto(connLog, numAutoInfracao)==false)
   								     {
   								    	 resultado="900  ";
   								    	 resultado+=dao.verificaTabEvento(resultado);
   								     }
   								     else
   										resultado =	broker.TransacaoIsoladas(arqRecebido.getNomUsername(),
   												arqRecebido.getCodOrgaoLotacao(), numTrans, tipArq, arqRecebido.
   												getCodOrgao(), linha, conn,transBRKDOL,arqRecebido.getCodIdentArquivo(), arqRecebido.getDscPortaria59());
								 }
								 else
										resultado =	broker.TransacaoIsoladas(arqRecebido.getNomUsername(),
   												arqRecebido.getCodOrgaoLotacao(), numTrans, tipArq, arqRecebido.
   												getCodOrgao(), linha, conn,transBRKDOL,arqRecebido.getCodIdentArquivo(), arqRecebido.getDscPortaria59());
									 
									 
   								    	 
							}
							else
							resultado =	broker.TransacaoIsoladas(arqRecebido.getNomUsername(),
									arqRecebido.getCodOrgaoLotacao(), numTrans, tipArq, arqRecebido.
									getCodOrgao(), linha, conn,transBRKDOL,arqRecebido.getCodIdentArquivo(), arqRecebido.getDscPortaria59());
						}
						//Marca o fim do processamento do Broker
						Date fimBroker = new Date();
						
						//Trocar as aspas simples por aspas no padrao oracle
						resultado = resultado.replaceAll("'", " ");
						//Pega os retornos da linha
						String codRetorno = resultado.substring(0, 3);
						
                        /*Enviar email caso erro Broker*/

						String sCodigosErros="097,497,597,997";
						
						
						if ( sCodigosErros.indexOf(codRetorno)>=0) 
						{
							String paramIntervalo="";
		        			String ultimaHora="";
		        			String horaAtual="";
		        			long difMin=0;
		        			
		        			/**
		        			 * Trecho de c�digo criado para controlar o envio de email no intervalo de tempo 
		        			 * estipulado no par�metro:"INTERVALO_DE_ENVIO_EMAIL_ERRO"
		        			 */
		        			
		        			/*
		        			paramIntervalo=param.getParamSist("INTERVALO_DE_ENVIO_EMAIL_ERRO");
		        			ultimaHora=param.getParamSist("HORA_ULTIMO_ENVIO_EMAIL_ERRO");
		        			horaAtual= sys.Util.formatedToday();   	
		        			long difMin=Cal_Num_min(ultimaHora,horaAtual);
		        				
		        			
		        			if ((difMin)>= Integer.parseInt(paramIntervalo)) 
		        			{	
						    lResultado.add("C�d. Orgao: "+arqRecebido.getCodOrgao()+" \n " +
								   "Auto: "+linha.substring(0,12)+" \n " +
						           "C�d. Transac�o/ID: " +transBRKDOL.getSeqTransacao().substring(6,9)+"/"+transBRKDOL.getSeqTransacao().substring(0,6)+" \n " +
								   "C�d. LogBroker: "+transBRKDOL.getCodLogBroker()+" \n " +
							       "Data Transac�o: "+sys.Util.getDataHoje().replaceAll("-","/")+" \n " +
								   "Transac�o Broker: "+arqRecebido.getNomUsername()+ 
									arqRecebido.getCodOrgaoLotacao()+arqRecebido.getCodOrgao()+
									arqRecebido.getResponsavelArq()+linha+" \n " +
							       "Resultado: "+resultado+" \n " +
								   "Linha Enviada : "+linha+" \n" +
								   "************************************************************************* \n");
	//						Dao.getInstance().UpdateUltimaHora();
		        			}						    							
		        			/**
		        			 * =====================Fim do controle do envio de Email================================
		        			 */		
		        			
		        			}						    							
						String codRetornoBatch = "";							
						String linhaRetorno = "";
						if (codRetorno.equals("ERR")) {
							linhaRetorno = resultado.substring(3);								
						} else {
							codRetornoBatch = resultado.substring(3, 5);
							linhaRetorno = resultado.substring(5);
							/*Bahia 23/09/2008*/
							if (arqRecebido.getCodIdentArquivo().equals("CODRET")){
								  linhaArq.setCodRetorno(codRetorno);
							        linhaArq.processaRetornoArDigTMP();
							}
						}
						/*Erro de Execu��o*/
						if (codRetornoBatch.trim().equals("-"))
						  if (codRetorno.length()>=3)
						    codRetornoBatch=codRetorno.substring(1,3);
						
						if (linhaRetorno.length() > 900)
							linhaRetorno = linhaRetorno.substring(0, 900);
						linhaRetorno = linhaRetorno.trim();
						
						//codStatus 0=recebido 1=processado 9=erro no acesso ao Broker
						String codStatus = "9";																
						if (sys.Util.isNumber(codRetorno)){
							/*
							 * C�digo utilizado para manipular o status da linha para 
							 * controle das notifi��es dos arquivos CODRET.
							 * O acesso ao par�metro do sistema � usado para habilitar ou 
							 * n�o a nova funcionalidade.
							 */ 
							if("1".equals(param.getParamSist("HABILITA_CONTROLE_NOTIFICACAO"))){
								if(arqRecebido.getCodIdentArquivo().equals("CODRET")){
									if("000,632,633,634".indexOf(codRetorno)>=0)
										codStatus = "1";
								}
								else
									codStatus = "1";
							}
							else
								codStatus = "1";
							
                            /*Reprocessamento*/
							if (codigoErros.indexOf(codRetorno)>=0)
							{
							  codStatus="9";
							  houveErroBroker = true;
							}
						}
						else
							houveErroBroker = true;
						
						
						
						
						
						
						
						if (!codRetorno.equals("000") && !houveErroBroker){
							/*
							 * C�digo utilizado para manipular o status da linha para 
							 * controle das notifi��es dos arquivos CODRET.
							 */ 
							if("1".equals(param.getParamSist("HABILITA_CONTROLE_NOTIFICACAO"))){
								if(arqRecebido.getCodIdentArquivo().equals("CODRET")){
									/*
									 * Somente ser�o acrescentadas como notifica��es pendentes de retorno, 
									 * as linhas que apresentam retorno do broker diferente aos valores
									 * 632 ou 633 ou 634. Essas linhas n�o ser�o consideradas como linhas com
									 * erro, por�m ser�o contabilizadas como linhas pendentes.
									 */  
									if("632,633,634".indexOf(codRetorno)==-1){
										/*
										 * A verifica��o da condi��o de nula ou vazia da quantidade pendente �
										 * realizada somente para evitar poss�veis exceptions.
										 */ 
										if( (arqRecebido.getQtdLinhaPendente() == null) || arqRecebido.getQtdLinhaPendente().equals("") )
											arqRecebido.setQtdLinhaPendente("0");
										else
											arqRecebido.setQtdLinhaPendente(String.valueOf((Integer.parseInt(arqRecebido.getQtdLinhaPendente()))+1));										
									}
									else
										QtdErros++;
								}
								else
									QtdErros++;
							}
							else
								QtdErros++;
						} else
							linhaArq.gravarAutoInfracao(arqRecebido);
						
						//Gravar Status da Linha
						linhaArq.setConn(conn);
						linhaArq.setCodStatus(codStatus);
						
						/*Caso Erro BROKER - CodRetorno n�o num�rico*/
						try {
							Integer.parseInt(codRetorno.trim());
						} catch (Exception e) {codRetorno="999";}
						
						linhaArq.setCodRetorno(codRetorno);
						linhaArq.setCodRetornoBatch(codRetornoBatch);
						linhaArq.setDscLinhaArqRetorno(linhaRetorno);
						if (!linhaArq.isUpdate())
							throw new DaoException("Erro ao atualizar Linha: " + linhaArq.getNumSeqLinha() + " !");
						
						//Marca o fim do processamento da Linha
						Date fimLinha = new Date();
						//Logar Execu��o da Linha
						if (!linhaArq.logar(iniLinha, iniBroker, fimBroker, fimLinha))
							throw new DaoException("Erro ao atualizar Linha: " + linhaArq.getNumSeqLinha() + " !");
						
						arqRecebido.setQtdLinhaErro(String.valueOf(QtdErros));
						if (!arqRecebido.isUpdate())
							throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");						
						
						conn.commit() ;
						
					}
					QtdLinProc++;
					System.out.println(QtdLinProc);
				} 
				monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +    
						QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size()+
						" Linhas com Erro: "+QtdErros, monitor.MSG_INFO);
				
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setQtdLinhaErro(String.valueOf(QtdErros));
				if (processoParado) {
					arqRecebido.setCodStatus("8"); //Processo n�o finalizado
					monitor.gravaMonitor("T�rmino Solicitado pelo Gerente do Arquivo "
							+arqRecebido.getNomArquivo(), monitor.MSG_INTERRUPCAO);
				} else if (houveErroBroker) {
					arqRecebido.setCodStatus("8"); //Processo n�o finalizado
					monitor.gravaMonitor("T�rmino com Erros do Arquivo "+arqRecebido.
							getNomArquivo(), monitor.MSG_FIM);
				} else {
					arqRecebido.setCodStatus("1"); //Processado
					monitor.gravaMonitor("T�rmino do Envio do Arquivo "+arqRecebido.
							getNomArquivo(), monitor.MSG_FIM);
				}											
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();
				
				/*Enviar email caso erro Broker*/
                if (lResultado.size()>0)                	
                {
                	/*
                		String sConteudo="";
    					Iterator it = lResultado.iterator();			
    					while (it.hasNext()) 
    					{
    						sConteudo+=(String)it.next();
    					}
    				    sys.EmailBean EmailBeanId = new sys.EmailBean();
    				    EmailBeanId.setDestinatario( emailBroker.toLowerCase() );
    				    EmailBeanId.setRemetente(arqRecebido.getNomArquivo());
    				    EmailBeanId.setAssunto( NOM_ROBOT );
    				    EmailBeanId.setConteudoEmail(sConteudo);
    				    */
    					//EmailBeanId.sendEmail();        				
                }
				
				if (processoParado) break;
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao Enviar Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (trava != null) {
					try {
						Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
					} catch (DaoException ed) {
						throw new RobotException(ed.getMessage());
					}

			}
		}
	}
	
	public static long Cal_Num_min(String horaCalc1,String horaCalc2) throws ParseException{
		
		int ano=Integer.parseInt(horaCalc1.substring(6,10));
		int mes=Integer.parseInt(horaCalc1.substring(3,5));
		int dia=Integer.parseInt(horaCalc1.substring(0,2));
		int hora=Integer.parseInt(horaCalc1.substring(13,15));
		int minuto=Integer.parseInt(horaCalc1.substring(16,18));
		
		int ano2=Integer.parseInt(horaCalc2.substring(6,10));
		int mes2=Integer.parseInt(horaCalc2.substring(3,5));
		int dia2=Integer.parseInt(horaCalc2.substring(0,2));
		int hora2=Integer.parseInt(horaCalc2.substring(13,15));
		int minuto2=Integer.parseInt(horaCalc2.substring(16,18));		
	
		Calendar calendar1 = Calendar.getInstance();
	    Calendar calendar2 = Calendar.getInstance();
	    calendar1.set(ano, mes, dia,hora,minuto);
	    calendar2.set(ano2, mes2, dia2,hora2,minuto2);
//      long diffSeconds = diff / 1000;
	    long diffMinutes = (calendar2.getTimeInMillis() - calendar1.getTimeInMillis()) / (60 * 1000);
//	    long diffHours = diff / (60 * 60 * 1000);
//	    long diffDays = diff / (24 * 60 * 60 * 1000);
	   
//		num_min=( ( (ano)*12+mes )*31 ) + ( ( (dia)*24+hora )*60 )+ minuto;		
		
		return diffMinutes;		
		
	}	

	
	
	public static void main(String args[])  throws RobotException {
		
		try {
			String tipArquivo = "";
			if (args.length > 0) tipArquivo = args[0];
			
			new EnviaArqDetranRob(tipArquivo);
			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}