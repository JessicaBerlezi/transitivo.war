package ROB;

import java.io.File;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import REG.ArquivoRecebidoBean;

public class EmpacotaArquivoRob {
	
	/*
	 * 
	 * 
	 */
	
	private static final String NOM_ROBOT = "EmpacotaArquivoRob";
	private static final String quebraLinha = "\n";
	
	public EmpacotaArquivoRob() throws RobotException {
		
		Connection conn  = null;
		Connection connLog  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		FileWriter writer = null;
		File zip = null;
		
		String numAutoInfracao = "";
		
		try {            			
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			REG.Dao dao = REG.Dao.getInstance();
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
			paramReg.setCodSistema("40"); //M�DULO REGISTRO	
			paramReg.PreparaParam(conn);
			
			String emailBroker   = paramReg.getParamSist("EMAIL_LOG_BROKER");
			
			ArrayList ListDestinatariosVEX=ACSS.Dao.getInstance().getDestinatariosPorNivel(paramReg,"PEND_FOTO_PENALIDADE");
			ArrayList ListDestinatarios=ACSS.Dao.getInstance().getDestinatariosPorNivel(paramReg,"PENDENCIA_EMITEVEX");			
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			String dirDownload = paramReg.getParamSist("DOWN_ARQUIVO_RECEBIDO");
			String qtdReg = null; // Par�metro criado para manipular a quantidade de registros do arquivo.
			while (true) {                
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) break;                 
				
				String tipos = "'EMITEVEX','EMITEPER'";
				Vector resArquivos = dao.BuscarArquivosRecebidos(tipos, "1,7", conn);
				if (resArquivos.size() == 0) break;
				
				REG.ArquivoRecebidoBean arqRecebido = (REG.ArquivoRecebidoBean) resArquivos.get(0);
				monitor.setNomObjeto(arqRecebido.getNomArquivo());
				monitor.setCodOrgao(arqRecebido.getCodOrgao());
				monitor.gravaMonitor("Inicio da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
				System.out.println("Inicio da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo());
				
				arqRecebido.setConn(conn);
				arqRecebido.setDatProcessamentoDetran(sys.Util.fmtData(new Date()));
				arqRecebido.setCodStatus("7"); //Em processamento
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
				conn.commit();	
				
				dao.CarregarLinhasArquivo(arqRecebido, conn);
				
				// Valida as linhas presentes dentro do arquivo.
				if ("1".equals(paramReg.getParamSist("HABILITA_CONTROLE_NOTIFICACAO")))
					validarLinha(arqRecebido,conn,emailBroker,ListDestinatarios,ListDestinatariosVEX);
				
				/*
				 * A manipula��o da quantidade de registros presentes no arquivo recebido
				 * � realizado para inserir o valor total de linhas, pendentes ou n�o,
				 * na base de dados do SMIT. Outro valor � inserido dentro do arquivo TXT
				 * com todas as linhas que est�o livres de pend�ncias.
				 */ 
				qtdReg = arqRecebido.getQtdReg();
				arqRecebido.setQtdReg(String.valueOf(arqRecebido.getLinhaArquivoRec().size()));
				
				writer = criarArquivoGrv(arqRecebido, dirDownload);
				
				int QtdLinProc = 0;
				ArrayList listFotos = new ArrayList();
				HashMap listFotosHash = new HashMap();
				
				Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();                
				while (ite.hasNext()) {
					
					if ((QtdLinProc%200)==0)
						monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
								QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size(), monitor.MSG_INFO);
					
					REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
					
					numAutoInfracao = linhaArq.getNumNotificacao();
					/*Comentado*/
					Dao.getInstance().ControleEmiteVEXV1(linhaArq);

					gravaGrv(writer, arqRecebido, linhaArq, conn);
					verificaFoto(paramRec, linhaArq, listFotos,listFotosHash);
					QtdLinProc++;										
				}
				
				if (writer != null)	writer.close();
				
				//Cria o arquivo zip com as fotos
				/*
				if (listFotos.size() > 0)
					zip = criarArquivoZip(conn, arqRecebido, dirDownload, listFotos,listFotosHash); N�o gerar ZIP
				*/                
				
				arqRecebido.setCodStatus("2");
				arqRecebido.setDatDownload(sys.Util.fmtData(new Date()));
				arqRecebido.setQtdReg(qtdReg);
				/*arqRecebido.setQtdFoto(listFotos.size());*/
				arqRecebido.setQtdFoto(listFotosHash.size());				
				
				if (!arqRecebido.isUpdate())
					throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getMsgErro());
				
				conn.commit();
				monitor.gravaMonitor("T�rmino da Gera��o dos Retornos do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
			}
			
		} catch (Exception e) {
			try {				
				if (writer != null) {
					writer.close();
					writer.getFile().delete();
				}
				if (zip != null) zip.delete();
				
				conn.rollback();
				monitor.gravaMonitor("Erro ao Gerar Retornos de Arquivos: " + e.getMessage()+" "+numAutoInfracao, monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}             
			if (trava != null) {
				try {
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (sys.DaoException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public static FileWriter criarArquivoGrv(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
	throws RobotException {
		
		String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();			
		
		//Cria o arquivo
		FileWriter writer = null;
		try {			
			File dir = new File(caminho.substring(0, caminho.lastIndexOf("/")));
			dir.mkdirs();
			
			File arquivo = new File(caminho);
			arquivo.createNewFile();
			writer = new FileWriter(arquivo);
			
			//Grava o header do arquivo	
			if (!arqRecebido.getHeaderGrv().equals("")) {
				writer.write(arqRecebido.getHeaderGrv());
				writer.write(quebraLinha);
			}
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
		return writer;
	}
	
	public static void gravaGrv(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
			REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
		
		try {
			if (writer != null) {			
				writer.write(linhaArq.getLinhaGrv(arqRecebido, conn));
				writer.write(quebraLinha);
			}
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
	}
	
	public static boolean verificaFoto(ACSS.ParamSistemaBean param, REG.LinhaArquivoRec linhaArq, ArrayList listFotos,HashMap listFotosHash) {
		boolean retorno = false;
		try {
			String numAutoInfracao = linhaArq.getDscLinhaArqRec().substring(360, 372).trim();
			boolean possuiFoto = !linhaArq.getDscLinhaArqRec().substring(649, 679).trim().equals("");            
			
			if (possuiFoto) {            
				REC.FotoDigitalizadaBean foto = REC.DaoDigit.getInstance().ConsultaFotoDigitalizada(new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'"+numAutoInfracao+"'"});
				
				if (foto != null) {
					File arquivo = new File(foto.getArquivo(param));
					if (arquivo.exists()) {
						listFotos.add(arquivo.getPath());
						listFotosHash.put(numAutoInfracao,arquivo.getPath());						
						retorno = true;
					}
				}
			}
			
		} catch (Exception e) {
			retorno = false;
		}
		return retorno;
	}
	
	public static File criarArquivoZip(Connection conn, REG.ArquivoRecebidoBean arqRecebido, String dirDestino,
			ArrayList listFotos,HashMap listFotosHash) throws RobotException {
		
		File arquivo = null;
		try {
			REG.ArquivoRecebidoBean arqFoto = Dao.getInstance().EmpacotarArquivo(conn,arqRecebido,listFotosHash.size());
			
			if (arqFoto != null) {
				String caminho = dirDestino + "/" + arqFoto.getCodIdentArquivo() + "/" + arqFoto.getNomArquivo();         
				
				File dir = new File(caminho.substring(0, caminho.lastIndexOf("/")));
				dir.mkdirs();
				
				arquivo = new File(caminho);
				arquivo.createNewFile();
				
				String[] arrayFotos = new String[listFotos.size()];
				listFotos.toArray(arrayFotos);
				sys.IOUtil.compactarZip(arrayFotos, arquivo, false);
			}            
			
		} catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
		return arquivo;
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new EmpacotaArquivoRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	/**
	 * M�todo que faz a cr�tica de todos as linhas presentes dentro do arquivo de envio do 
	 * Detran. 
	 * Caso a Linha apresente problema, ela � retirada do objeto arqRecebido, sendo registrada
	 * na tabela TSMI_CONTROLE_NOTIFICACOES como uma pend�ncia.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 15/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	private void validarLinha(ArquivoRecebidoBean arqRecebido,Connection conn,String emailBroker,ArrayList ListDestinatarios,ArrayList ListDestinatariosVEX) throws Exception{
		try{
			ArrayList Pendencias = new ArrayList();
			ArrayList Pendencias_Penalidade = new ArrayList();
			
			
			REG.Dao dao = REG.Dao.getInstance();
			
			// Valida se as notifica��es ainda n�o foram enviadas. 
			dao.verificaNotificacoes(arqRecebido,conn);
			dao.insereNotificacoes(arqRecebido, conn, ListDestinatarios, ListDestinatariosVEX, Pendencias, Pendencias_Penalidade);
			
			/*Enviar Email*/
			if (Pendencias.size()>0)
			{  
				int contNivel = 0;
				Iterator it = Pendencias.iterator();
				GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
				while (it.hasNext())
				{
					contNivel++;
					nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
					if (nivelProcesso.getValNivelProcesso().equals("S") && (contNivel <= ListDestinatarios.size()))
					{
						int contDestinatarios = 0;
						Iterator itx = nivelProcesso.getListNivelDestino().iterator();
						GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
						while (itx.hasNext())
						{
							contDestinatarios++;
							destinatario=(GER.CadastraDestinatarioBean)itx.next();
							if(destinatario.getMsg().size()>0 && (contDestinatarios <= nivelProcesso.getListNivelDestino().size()))
							{
								String sConteudo="";
								Iterator ity = destinatario.getMsg().iterator();			
								while (ity.hasNext()) 
								  sConteudo+=(String)ity.next();
								
								sys.EmailBean EmailBeanId = new sys.EmailBean();
								EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
								EmailBeanId.setRemetente(emailBroker);
								EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+ 
										" - Arquivo: "+arqRecebido.getNomArquivo()+ 
										" - Data: "+arqRecebido.getDatRecebimento());
								EmailBeanId.setConteudoEmail(sConteudo);
//								EmailBeanId.sendEmail();
							}
					   }
					}
				}
				
				
				/*Enviar Email Pendencia Penalidade Sem Foto*/
				if (Pendencias_Penalidade.size()>0)
				{  
					contNivel = 0;
					it = Pendencias_Penalidade.iterator();
					nivelProcesso= new GER.NivelProcessoSistemaBean();
					while (it.hasNext())
					{
						contNivel++;
						nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
						if (nivelProcesso.getValNivelProcesso().equals("S") && (contNivel <= ListDestinatarios.size()))
						{
							int contDestinatarios = 0;
							Iterator itx = nivelProcesso.getListNivelDestino().iterator();
							GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
							while (itx.hasNext())
							{
								contDestinatarios++;
								destinatario=(GER.CadastraDestinatarioBean)itx.next();
								if(destinatario.getMsg().size()>0 && (contDestinatarios <= nivelProcesso.getListNivelDestino().size()))
								{
									String sConteudo="";
									Iterator ity = destinatario.getMsg().iterator();			
									while (ity.hasNext()) 
									  sConteudo+=(String)ity.next();
									
									sys.EmailBean EmailBeanId = new sys.EmailBean();
									EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
									EmailBeanId.setRemetente(emailBroker);
									EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+ 
											" - Arquivo: "+arqRecebido.getNomArquivo()+ 
											" - Data: "+arqRecebido.getDatRecebimento());
									EmailBeanId.setConteudoEmail(sConteudo);
//									EmailBeanId.sendEmail();
								}
						   }
						}
					}
				}
				
				
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
}
