package ROB;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.commons.io.FileUtils;

import sys.ServiceLocatorException;
import sys.Util;



public class FileList {
	
	private static final String MYABREVSIST = "ROB";
	
	public FileList() {}   
	
	/**  
	 * Esta fun��o lista toda a estrutura de diret�rio a partir de um File. Se  
	 * o File for um arquivo exibe apenas o nome, se for um diret�rio ele exibe  
	 * a estrutura de arquivo abaixo dele, sendo ou n�o recursivo (subdiret�rios).  
	 *  
	 * @param root File a ser listado, pode ser um arquivo ou diret�rio  
	 * @param recursive Se for true varre inclusive os subdiret�rios  
	 * @throws DaoException 
	 * @throws ServiceLocatorException 
	 * @throws SQLException 
	 **/   
	
	public void listFiles( File root, boolean recursive ) {   
		if( root==null ) return;   
		if( !root.exists() ) {   
			System.out.println("File does not exist!");   
		}   
		
		if( root.isFile() ) {   
			System.out.println( root.getAbsolutePath() );   
			return;   
		}   
		System.out.println( root.getAbsolutePath() );   
		
		File[] files = root.listFiles();   
		if( files!=null ) {   
			for(int i=0; i<files.length; i++) {   
				if( files[i].isFile() ) {   
					System.out.println( files[i].getAbsolutePath() );   
					
					
					
				}   
				
				if( recursive && files[i].isDirectory() ) {   
					listFiles( files[i], recursive );   
				}   
			}   
		}   
	}   
	
	public void listFiles( File root ) {   
		listFiles( root, false );   
	}   
	
	
	public void listFiles( File root, boolean recursive , Statement stm) throws ServiceLocatorException, DaoException, SQLException 
	{   
		
		
		
		if( root==null ) return;   
		if( !root.exists() ) {   
			System.out.println("File does not exist!");   
		}   
		
		if(root.isDirectory())
			if(root.getAbsolutePath().indexOf("ok")>0)
				return;
		
		if( root.isFile() ) 
		{
			
			//.out.println( root.getAbsolutePath() );   
			return;   
		}   
		//System.out.println( root.getAbsolutePath() );   
		
		File[] files = root.listFiles();   
		
		List <String[]> listaDeCampos = new ArrayList <String[]>();
		if( files!=null ) 
		{   
			for(int i=0; i<files.length; i++) 
			{   
				if( files[i].isFile() ) 
				{   
					//System.out.println( files[i].getAbsolutePath() );
					String absolutePath = files[i].getAbsolutePath();
					
					absolutePath = trocaContraBarraPorVirgula(absolutePath); 
					int pos = absolutePath.indexOf("RJVEX");					
					String absolutePathReduzido = absolutePath.substring(pos+6, absolutePath.length());
					
					String[] campos = absolutePathReduzido.split(",");
					if(campos.length==7)
					{
						int posTif = campos[6].toLowerCase().indexOf(".tif");
						if(posTif>0)
							listaDeCampos.add(campos);
					}
					
				}   
				
				if( recursive && files[i].isDirectory() ) {   
					listFiles( files[i], recursive , stm );   
				}   
			}   
		}   
		
		if(listaDeCampos.size()>0)
		{
			System.out.println("======================================================================================================================");
			try {
				insert_TAB_AR_DIR(listaDeCampos,stm);
			} catch (DaoException e) {
				System.out.println(e.getMessage());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
	}   
	
	public String trocaContraBarraPorVirgula(String linha)
	{
		String retorno = "";
		for(int i=0; i<linha.length(); i++)
		{
			String carac = linha.substring(i,i+1);
			if(carac.equals("\\"))
				carac = ",";
			retorno += carac; 
			
		}
		return retorno;
		
	}
	
	
	
	public static void insert_TAB_AR_DIR(List <String[]> listaDeCampos,Statement stmt) throws Exception, DaoException
	{
		try
		{
			for(int i=0; i<listaDeCampos.size(); i++)
			{
				String [] campos = listaDeCampos.get(i); 
				
				String dir = campos[0];
				String ano = campos[1];
				String mes = campos[2];
				String subDir01 = campos[3];
				String subDir02 = campos[4];
				String subDir03 = campos[5];
				String ar = campos[6];
				String numNotificacao="";
				String temFoto="",semRetorno="";				
				if (ar.trim().length()==14) numNotificacao=ar.substring(0,9);
				
				if (numNotificacao.length()>0)
				{
					String sComando = "SELECT * FROM TSMI_AR_DIGITALIZADO WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
					ResultSet rs  = stmt.executeQuery(sComando);
					if (rs.next()) temFoto="S";
					
					sComando="SELECT * FROM TSMI_CONTROLE_VEX_MASTER WHERE NUM_NOTIFICACAO='"+numNotificacao+"'"+
					" AND DAT_RETORNO IS NOT NULL";
					rs  = stmt.executeQuery(sComando);
					if (rs.next()) semRetorno="S";
					
					
				}
				
				
				
				
				String sCmd = "SELECT DIR, ANO, MES, SUBDIR01, SUBDIR02, SUBDIR03, AR " +
						" FROM TAB_AR_DIR " +
						" WHERE DIR = '" + dir + "'" + 
						" AND ANO = '" + ano + "'" +
						" AND MES = '" + mes + "'" +
						" AND SUBDIR01 = '" + subDir01 + "'" +
						" AND SUBDIR02 = '" + subDir02 + "'" +
						" AND SUBDIR03 = '" + subDir03 + "'" +
						" AND AR = '" + ar + "'" ;
				
				ResultSet rs  = stmt.executeQuery(sCmd);
				if (!rs.next())
				{ 
					sCmd = "INSERT INTO TAB_AR_DIR " +
					" (DIR, ANO, MES, SUBDIR01, SUBDIR02, SUBDIR03, AR, NUM_NOTIFICACAO,NUMNOTIFICACAO,TEM_FOTO,SEM_RETORNO)" +
					" VALUES " +
					" ( '" + dir + "'," + 
					"'" + ano + "'," +
					"'" + mes + "'," +
					"'" + subDir01 + "'," +
					"'" + subDir02 + "'," +
					"'" + subDir03 + "'," +
					"'" + ar +      "'," +
					"'" + numNotificacao  + "'," + 
					"'" + numNotificacao  + "'," +
					"'" + temFoto  + "'," + 
					"'" + semRetorno  + "')";					
					stmt.executeUpdate(sCmd);
					
					
					
					System.out.println(" INCLUINDO -> ( '" + dir + "'," + 
							"'" + ano + "'," +
							"'" + mes + "'," +
							"'" + subDir01 + "'," +
							"'" + subDir02 + "'," +
							"'" + subDir03 + "'," +
							"'" + ar + "'" + ")");
				}
				else
				{
					System.out.println(" J� EXISTE -> ( '" + dir + "'," + 
							"'" + ano + "'," +
							"'" + mes + "'," +
							"'" + subDir01 + "'," +
							"'" + subDir02 + "'," +
							"'" + subDir03 + "'," +
							"'" + ar + "'" + ")");
				}
				
				
			}
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e);
			
		}
	}
	
	
	public void montaDiretoriosARs(Statement stmt) throws Exception, DaoException
	{
		try
		{
			List <String[]> listaDePathDeARs = new ArrayList <String[]>();

			String sCmd = "SELECT DIR, ANO, MES, SUBDIR01, SUBDIR02, SUBDIR03, AR " +
					" FROM TAB_AR_DIR" +
					" WHERE " +
					"(TEM_FOTO IS NULL OR SEM_RETORNO IS NULL) AND NUM_NOTIFICACAO IS NOT NULL";
			ResultSet rs  = stmt.executeQuery(sCmd);
			while (rs.next())
			{
				String dir = rs.getString("DIR");
				String ano = rs.getString("ANO");
				String mes = rs.getString("MES");
				String subDir01 = rs.getString("SUBDIR01");
				String subDir02 = rs.getString("SUBDIR02");
				String subDir03 = rs.getString("SUBDIR03");
				String ar = rs.getString("AR");
				
				String pathOrigem  = "D:\\" + dir + "\\" + ano + "\\" + mes + "\\" + subDir01 + "\\" + subDir02 + "\\" + subDir03 + "\\" + ar; 
				String pathDestino = "C:\\ARQUIVOS\\" + subDir02 + "\\" + subDir03 + "\\" + ar;
				
				String pathAR[] = new String[2];
				pathAR[0] = pathOrigem;
				pathAR[1] = pathDestino;

				listaDePathDeARs.add(pathAR);
				
			}
			
			
			copiarArquivos(listaDePathDeARs);
		}
		catch(Exception e)
		{	
			System.out.println("exception 51: "+ e);
		}
	}
	
	
	
	private boolean copiarArquivos(List <String[]> listaDePathDeARs)
    {
        boolean retorno = true;
        try 
        {
        	int qtdCopiados = 0;
            for(int i=0; i<listaDePathDeARs.size(); i++)
            {
            	String pathAR[] = listaDePathDeARs.get(i);
            	
            	String nomPathOrigem = pathAR[0];
                String nomPathDestino = pathAR[1];
            	
            	FileUtils.copyFile(new File(nomPathOrigem), new File(nomPathDestino));
            	System.out.println("Copiando arquivo: " + nomPathDestino);
            	qtdCopiados++;
            }
            System.out.println(qtdCopiados + " de " + listaDePathDeARs.size()+ " arquivos copiados com SUCESSO!");
            
        } catch (Exception e) {
        	System.out.println("Erro ao copiar arquivo!");
        }
        return retorno;
    }

	public static void UPDATE_TAB_AR_DIR(Statement stmt,Statement stmt_VEX) throws Exception, DaoException
	{
		try
		{
			    String sComando_VEX = "SELECT * FROM TAB_AR_DIR WHERE LENGTH(TRIM(AR))=13";
				ResultSet rs_VEX  = stmt_VEX.executeQuery(sComando_VEX);
				while (rs_VEX.next()) 
				{
					String numNotificacao=rs_VEX.getString("NUM_NOTIFICACAO").trim();
					String temFoto="",semRetorno="";				

					
					if (numNotificacao.length()>0)
					{
						String sComando = "SELECT * FROM TSMI_AR_DIGITALIZADO WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						ResultSet rs  = stmt.executeQuery(sComando);
						if (rs.next()) temFoto="S";
						
						sComando="SELECT * FROM TSMI_CONTROLE_VEX_MASTER WHERE NUM_NOTIFICACAO='"+numNotificacao+"'"+
						" AND DAT_RETORNO IS NOT NULL";
						rs  = stmt.executeQuery(sComando);
						if (rs.next()) semRetorno="S";
						
						sComando="UPDATE TAB_AR_DIR "+
							     "SET TEM_FOTO='"+temFoto+"',"+
							     "    SEM_RETORNO='"+semRetorno+"' "+
							     " WHERE NUM_NOTIFICACAO='"+numNotificacao+"'";
						stmt.executeUpdate(sComando);						
					}
				}
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e);
			
		}
	}
	
	
	
	public static void update_TAB_NIT_P59(Statement stmt,Statement stmt2) throws Exception, DaoException
	{
		try
		{
			String sCmd = " SELECT NUM_AUTO_INFRACAO, NUM_IDENT_ORGAO FROM TAB_NIT_MDB ";
			
			int qtdProc = 0;
			ResultSet rs  = stmt.executeQuery(sCmd);
			while(rs.next())
			{ 
				String numAuto = rs.getString("NUM_AUTO_INFRACAO"); 
				String identOrgao = rs.getString("NUM_IDENT_ORGAO");
				if (identOrgao==null) identOrgao="";
				
				System.out.println(qtdProc+" ATUALIZANDO AUTO ( '" + numAuto + "')"+sCmd);				
				
				sCmd = "UPDATE TSMI_AUTO_INFRACAO SET " +
						" NUM_IDENT_ORGAO = '" + identOrgao.trim() + "'" +
						" WHERE NUM_AUTO_INFRACAO = '" + numAuto.trim() + "'" ;
			
				stmt2.executeUpdate(sCmd);
				qtdProc++;

			}
			
			System.out.println(" FIM DO PROCESSAMENTO DAS ATUALIZA��ES. ");
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e);
			
		}
	}
	

	public static void DeleteAutoInfracao(Statement stmt,Statement stmt2) throws Exception, DaoException
	{
		String codAuto="4541288";
		try
		{

			String codOrgao="";
			String datDigit="",datInfracao="";
			int qtdProc = 0,ivalCod=0;
			String sCmd="";
			
			for (int i=0;i<=105;i++) {
				
				System.out.println("Processando Vez... "+i);
				ivalCod=Integer.parseInt(codAuto)+50000;
				sCmd = "SELECT COD_AUTO_INFRACAO,COD_ORGAO,DAT_DIGITALIZACAO,TO_CHAR(DAT_INFRACAO,'DD/MM/YYYY') AS DAT_INFRACAO FROM TSMI_AUTO_INFRACAO " +
						"WHERE COD_AUTO_INFRACAO>="+codAuto+" AND COD_AUTO_INFRACAO<="+ivalCod+ 
								" ORDER BY COD_AUTO_INFRACAO";
				int qtdReg = 0;
				ResultSet rs  = stmt.executeQuery(sCmd);
				while(rs.next())
				{

					if (qtdReg%5000==0) System.out.println("Processando... "+qtdReg+" Deletados :"+qtdProc);
					
					codAuto = rs.getString("COD_AUTO_INFRACAO");
					codOrgao=rs.getString("COD_ORGAO");
					datDigit=rs.getString("DAT_DIGITALIZACAO");
					datInfracao=rs.getString("DAT_INFRACAO");
					if ( ((codOrgao==null) && (datDigit==null)) || (Util.DifereDias(datInfracao,"01/01/2008")>0) ||  
					                                               (datInfracao==null) ){					
						sCmd = "DELETE FROM TSMI_HISTORICO_AUTO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_REMESSA_ITEM WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_REQUERIMENTO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_AUTO_INFRACAO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						qtdProc++;
					}
					
					qtdReg++;
					
					
					
				}
				codAuto=String.valueOf(ivalCod);
			}
			System.out.println(" FIM DO PROCESSAMENTO DAS ATUALIZA��ES. "+codAuto);
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e +" codAuto:"+codAuto );
			
		}
	}
	
	public static void DeleteAutoInfracao_2(Statement stmt,Statement stmt2) throws Exception, DaoException
	{
		String codAuto="6699538";
		try
		{

			String codOrgao="";
			String datDigit="",datInfracao="";
			int qtdProc = 0,ivalCod=0;
			String sCmd="";
			ResultSet rs2=null;
			
			for (int i=0;i<=155;i++) {
				
				System.out.println("Processando Vez... "+i);
				ivalCod=Integer.parseInt(codAuto)+50000;
				sCmd = "SELECT COD_AUTO_INFRACAO,COD_ORGAO,DAT_DIGITALIZACAO,TO_CHAR(DAT_INFRACAO,'DD/MM/YYYY') AS DAT_INFRACAO FROM TSMI_AUTO_INFRACAO " +
						"WHERE COD_AUTO_INFRACAO>="+codAuto+" AND COD_AUTO_INFRACAO<="+ivalCod+ 
								" ORDER BY COD_AUTO_INFRACAO";
				int qtdReg = 0;
				ResultSet rs  = stmt.executeQuery(sCmd);
				while(rs.next())
				{

					if (qtdReg%5000==0) System.out.println("Processando... "+qtdReg+" Deletados :"+qtdProc);
					
					codAuto = rs.getString("COD_AUTO_INFRACAO");
					codOrgao=rs.getString("COD_ORGAO");
					datDigit=rs.getString("DAT_DIGITALIZACAO");
					datInfracao=rs.getString("DAT_INFRACAO");
					
				
					boolean bApaga=true;
					
					sCmd = "SELECT * FROM TSMI_REQUERIMENTO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
					rs2  = stmt2.executeQuery(sCmd);
					if (rs2.next()) bApaga=false;

					
					if ( (datInfracao==null) && (bApaga) ){					
						sCmd = "DELETE FROM TSMI_HISTORICO_AUTO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_REMESSA_ITEM WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_REQUERIMENTO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						
						sCmd = "DELETE FROM TSMI_AUTO_INFRACAO WHERE COD_AUTO_INFRACAO='"+codAuto+"'";
						stmt2.executeUpdate(sCmd);
						qtdProc++;
					}
					
					qtdReg++;
					
					
					
				}
				codAuto=String.valueOf(ivalCod);
			}
			System.out.println(" FIM DO PROCESSAMENTO DAS ATUALIZA��ES. "+codAuto);
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e +" codAuto:"+codAuto );
			
		}
	}
	
	
	
	public static void update_CONTROLE_VEX_MASTER(Statement stmt,Statement stmt2) throws Exception, DaoException
	{
		try
		{
			
			System.out.println(" SELECIONANDO AUTOS DA CONTROLE_VEX_MASTER QUE ESTAO NA TSMI_AUTO_INFRACAO COM NUM_AUTO_RENAINF NOT NULL...");
			
			String sCmd = " SELECT VM.PK_COD_CONTROLE_VEX_MASTER as pkMaster, VM.NUM_AUTO_INFRACAO as numAuto" +
					" FROM TSMI_CONTROLE_VEX_MASTER VM" +
					" 	WHERE VM.DAT_ENVIO>='01/01/2007'" +
					" 		AND VM.DAT_ENVIO<='31/12/2007'" +
					" 		AND VM.NUM_NOTIFICACAO IS NOT NULL" +
					" 		AND VM.DAT_RETORNO IS NULL" +					
					" 	ORDER BY NUM_NOTIFICACAO ";

			ResultSet rs  = stmt.executeQuery(sCmd);
			while(rs.next())
			{ 
				String pkMaster = rs.getString("pkMaster").trim(); 
				String numAuto = rs.getString("numAuto").trim();
				
				sCmd = " SELECT AI.NUM_AUTO_RENAINF" +
				" FROM TSMI_AUTO_INFRACAO AI" +
				" 	WHERE AI.NUM_AUTO_INFRACAO = '"+ numAuto +"'" +
				" 		AND AI.NUM_AUTO_RENAINF IS NOT NULL";
			
							
				ResultSet rs2  = stmt2.executeQuery(sCmd);
				if(rs2.next())
				{ 
					sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER SET " +
						" RENAINF = 'S'" +
						" WHERE PK_COD_CONTROLE_VEX_MASTER = '" + pkMaster + "'" ;
			
					stmt2.executeUpdate(sCmd);
					System.out.println(" ATUALIZANDO RENAINF = 'S' NA CONTROLE_VEX_MASTER ( AUTO => '" + numAuto + "')");
				}
				rs2.close();
			}
			rs.close();
			
			System.out.println(" FIM DO PROCESSAMENTO DAS ATUALIZA��ES. ");
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e);
			
		}
	}

	public static void update_CONTROLE_VEX_RESUMO(Statement stmt,Statement stmt2) throws Exception, DaoException
	{
		try
		{
			String sCmd = "SELECT * FROM TAB_RESUMO_CONTROLE_VEX_2007 ORDER BY MES_ENVIO,COD_RETORNO";
			ResultSet rs  = stmt.executeQuery(sCmd);
			while(rs.next())
			{ 
				String anoEnvio=rs.getString("ANO_ENVIO");
				String mesEnvio=rs.getString("MES_ENVIO");
				String codRetorno=rs.getString("COD_RETORNO");
				String anoRetorno=rs.getString("ANO_RETORNO");
				String mesRetorno=rs.getString("MES_RETORNO");
				int iValorEmitidoRetorno=rs.getInt("QTD_EMITIDO_RETORNO");
				
				sCmd = "UPDATE TSMI_CONTROLE_VEX_RESUMO SET " +
				" QTD_EMITIDO_RETORNO = QTD_EMITIDO_RETORNO+"+iValorEmitidoRetorno+" "+
				" WHERE "+
				"ANO_ENVIO='"+anoEnvio+"' AND "+
				"MES_ENVIO='"+mesEnvio+"' AND "+
				"COD_RETORNO='"+codRetorno+"' AND "+
				"ANO_RETORNO='"+anoRetorno+"' AND "+
				"MES_RETORNO='"+mesRetorno+"'";
				stmt2.executeUpdate(sCmd);

			}
			rs.close();
			
			System.out.println(" FIM DO PROCESSAMENTO DAS ATUALIZA��ES. ");
		}
		catch(SQLException e)
		{	
			System.out.println("exception 51: "+ e);
			
		}
	}
	
	
//	 Completa uma string com um caracter "0" (zero) a ESQUERDA, at� que este tenha o tamanho desejado.
	public static String  fFill(String strOriginal, int tamFinal)
	{
		if(strOriginal==null)
			strOriginal = "";
		
		String retorno = strOriginal;
		int tamanho = retorno.length(); 
		while(tamanho<tamFinal)
		{
		   retorno = "0" + retorno; 
		   tamanho++;
		}		

		return retorno;
	}

	
//	 Completa uma string com caracteres em "branco" a DIREITA, at� que este tenha o tamanho desejado.
	public static String  fTitaCharEsp(String strOriginal)
	{
		if(strOriginal==null)
			strOriginal = "";
		
		
		String strOriginalAux = "";
		int ascii = 0;
		char c = 'A';
		for (int s=0;s<strOriginal.length();s++){
			 c = strOriginal.charAt(s);
			 ascii = (int) c;
			 if ((ascii != 10) && (ascii != 13))
				 strOriginalAux += c; 
		}
		
		strOriginal = strOriginalAux;
		
		
		return strOriginal;
	}
	
	
	
//	 Completa uma string com caracteres em "branco" a DIREITA, at� que este tenha o tamanho desejado.
	public static String  fPadBranco(String strOriginal, int tamFinal)
	{
		if(strOriginal==null)
			strOriginal = "";
		
		String retorno = strOriginal;
		int tamanho = retorno.length(); 
		while(tamanho<tamFinal)
		{
		   retorno = retorno + " " ; 
		   tamanho++;
		}		

		return retorno;
	}

	public static String formataDataYYYYMMDD(String strData)
	{
		if(strData==null)
			return "00000000";
		
		if (strData.trim().equals("")) 
			return "00000000";	
		
		if (strData.length() < 10) 
			strData = fFill(strData,10);
		
		return strData.substring(6,10) + strData.substring(3,5) + strData.substring(0,2);
	}
	
	public static String formatedTodayAAAAMMDD(){
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length()<2) {dia = "0" + dia;}
		if (mes.length()<2) {mes = "0" + mes;}
		if (hor.length()<2) {hor = "0" + hor;}
		if (min.length()<2) {min = "0" + min;}
		if (seg.length()<2) {seg = "0" + seg;}
		// dd/mm/yyyy, hh24:mi:ss
		return ano+mes+dia;
		
	}  // Fim formatedToday
	
	Integer qtdTotal_TipoP_Tipo2 = 0;
	Integer qtdTotal_TipoP_Tipo4 = 0;
	Integer qtdTotal_TipoP_Tipo5 = 0;
	Integer qtdTotal_TipoP_Tipo6 = 0;
	Integer qtdTotal_TipoP_Tipo7 = 0;
	Integer qtdTotal_TipoP_Tipo8 = 0;
	
	Integer qtdTotal_TipoS_Tipo2 = 0;
	Integer qtdTotal_TipoS_Tipo4 = 0;
	Integer qtdTotal_TipoS_Tipo5 = 0;
	Integer qtdTotal_TipoS_Tipo6 = 0;
	Integer qtdTotal_TipoS_Tipo7 = 0;
	Integer qtdTotal_TipoS_Tipo8 = 0;
	
	
	public void exportaBasePNT(Statement stmt01, Statement stmt02, Statement stmt03, Statement stmt04, Statement stmt05, Statement stmt06) throws Exception, DaoException
	{
		try
		{
			System.out.println("========================================================================================================================");
			
			exportaBasePNT_TIPO_P(stmt01, stmt02, stmt03, stmt04, stmt05, stmt06);
			
			
			BufferedWriter arqTotais_TipoP = new BufferedWriter(new FileWriter("C:\\BASEDADOS_PNT\\Relatorio_TipoP.txt"));
			
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '2' (Processo) ====>" + fFill(qtdTotal_TipoP_Tipo2.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '4' (Multa) ====>" + fFill(qtdTotal_TipoP_Tipo4.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '5' (Requerimento) ====>" + fFill(qtdTotal_TipoP_Tipo5.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '6' (Hist�rico) ====>" + fFill(qtdTotal_TipoP_Tipo6.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '7' (Notifica��o) ====>" + fFill(qtdTotal_TipoP_Tipo7.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.write("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '8' (Ata DO) ====>" + fFill(qtdTotal_TipoP_Tipo8.toString(),6) );
			arqTotais_TipoP.write("\n");
			
			arqTotais_TipoP.close();
			
			System.out.println("========================================================================================================================");

			exportaBasePNT_TIPO_S(stmt01, stmt02, stmt03, stmt04, stmt05, stmt06);
			BufferedWriter arqTotais_TipoS = new BufferedWriter(new FileWriter("C:\\BASEDADOS_PNT\\Relatorio_TipoS.txt"));
			
			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '2' (Processo) ====>" + fFill(qtdTotal_TipoS_Tipo2.toString(),6) );
			arqTotais_TipoS.write("\n");
			
			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '4' (Multa) ====>" + fFill(qtdTotal_TipoS_Tipo4.toString(),6) );
			arqTotais_TipoS.write("\n");
			
			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '5' (Requerimento) ====>" + fFill(qtdTotal_TipoS_Tipo5.toString(),6) );
			arqTotais_TipoS.write("\n");

			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '6' (Hist�rico) ====>" + fFill(qtdTotal_TipoS_Tipo6.toString(),6) );
			arqTotais_TipoS.write("\n");

			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '7' (Notifica��o) ====>" + fFill(qtdTotal_TipoS_Tipo7.toString(),6) );
			arqTotais_TipoS.write("\n");

			arqTotais_TipoS.write("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '8' (Ata DO) ====>" + fFill(qtdTotal_TipoS_Tipo8.toString(),6) );
			arqTotais_TipoS.write("\n");
								
			arqTotais_TipoS.close();
			
			System.out.println("========================================================================================================================");
			
			BufferedWriter arqHeader = new BufferedWriter(new FileWriter("C:\\BASEDADOS_PNT\\BASEDADOS_PNT_HEADER.txt"));
			
			String linhaArqHeader = "1";
			
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo2.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo4.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo5.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo6.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo7.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoP_Tipo8.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo2.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo4.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo5.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo6.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo7.toString(),6);
			linhaArqHeader += fFill(qtdTotal_TipoS_Tipo8.toString(),6);
			
			linhaArqHeader += formatedTodayAAAAMMDD();
			
			arqHeader.write(linhaArqHeader);
			arqHeader.write("\n");
			arqHeader.close();
			
		/*	
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '2' (Processo) ====>" + fFill(qtdTotal_TipoP_Tipo2.toString(),6) );
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '4' (Multa) ====>" + fFill(qtdTotal_TipoP_Tipo4.toString(),6) );
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '5' (Requerimento) ====>" + fFill(qtdTotal_TipoP_Tipo5.toString(),6) );
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '6' (Hist�rico) ====>" + fFill(qtdTotal_TipoP_Tipo6.toString(),6) );
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '7' (Notifica��o) ====>" + fFill(qtdTotal_TipoP_Tipo7.toString(),6) );
			System.out.println("PROCESSOS TIPO 'P' => TOTAL REG. TIPO '8' (Ata DO) ====>" + fFill(qtdTotal_TipoP_Tipo8.toString(),6) );
			
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '2' (Processo) ====>" + fFill(qtdTotal_TipoS_Tipo2.toString(),6) );
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '4' (Multa) ====>" + fFill(qtdTotal_TipoS_Tipo4.toString(),6) );
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '5' (Requerimento) ====>" + fFill(qtdTotal_TipoS_Tipo5.toString(),6) );
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '6' (Hist�rico) ====>" + fFill(qtdTotal_TipoS_Tipo6.toString(),6) );
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '7' (Notifica��o) ====>" + fFill(qtdTotal_TipoS_Tipo7.toString(),6) );
			System.out.println("PROCESSOS TIPO 'S' => TOTAL REG. TIPO '8' (Ata DO) ====>" + fFill(qtdTotal_TipoS_Tipo8.toString(),6) );
			
			System.out.println("========================================================================================================================");
		*/
		}	
		catch(Exception e)
		{	
			System.out.println("Erro na exportaBasePNT: "+ e);
		}
	}
	
	public void exportaBasePNT_TIPO_P(Statement stmt01, Statement stmt02, Statement stmt03, Statement stmt04, Statement stmt05, Statement stmt06) throws Exception, DaoException
	{
		try
		{
			BufferedWriter arqBASE_PONTUACAO_20PT = new BufferedWriter(new FileWriter("C:\\BASEDADOS_PNT\\BASE_PONTUACAO_20PT.txt"));
			
			System.out.println("============================================================");
			System.out.println("INICIO DO PROCESSAMENTO ====> PROCESSOS TIPO 'P'<====");
			System.out.println("============================================================");
			
			exporta_PROCESSO(stmt01, stmt02, stmt03, stmt04, stmt05, stmt06, "P", arqBASE_PONTUACAO_20PT);
			
			System.out.println("============================================================");
			System.out.println("FIM DO PROCESSAMENTO ====> PROCESSOS TIPO 'P'<====");
			System.out.println("============================================================");
			
			
			arqBASE_PONTUACAO_20PT.close();
			
		}	
		catch(Exception e)
		{	
			System.out.println("Erro na exportaBasePNT_TIPO_P: "+ e);
			throw new Exception(e.getMessage());
		}
	}
	
	public void exportaBasePNT_TIPO_S(Statement stmt01, Statement stmt02, Statement stmt03, Statement stmt04, Statement stmt05, Statement stmt06) throws Exception, DaoException
	{
		try
		{
			BufferedWriter arqBASE_PONTUACAO_SDD = new BufferedWriter(new FileWriter("C:\\BASEDADOS_PNT\\BASE_PONTUACAO_SDD.txt"));
			
			System.out.println("============================================================");
			System.out.println("INICIO DO PROCESSAMENTO ====> PROCESSOS TIPO 'S'<====");
			System.out.println("============================================================");
			
			exporta_PROCESSO(stmt01, stmt02, stmt03, stmt04, stmt05, stmt06, "S", arqBASE_PONTUACAO_SDD);
			
			System.out.println("============================================================");
			System.out.println("FIM DO PROCESSAMENTO ====> PROCESSOS TIPO 'S'<====");
			System.out.println("============================================================");
			
			
			arqBASE_PONTUACAO_SDD.close();
		}	
		catch(Exception e)
		{	
			System.out.println("Erro na exportaBasePNT_TIPO_S: "+ e);
			throw new Exception(e.getMessage());
		}
	}
	
	private void exporta_PROCESSO(Statement stmt01, Statement stmt02, Statement stmt03, Statement stmt04, Statement stmt05, Statement stmt06,
			String tipoParam, BufferedWriter arqBASE_PONTUACAO) throws Exception, DaoException
	{
		try
		{
			String sCmd = "SELECT COD_PROCESSO, NUM_PROCESSO, " +
				" TIPO_PROCESSO, SIT_PROCESSO, TOT_PONTOS, " +
				" QTD_PROC_ANTERIOR, QTD_MULTA, NOM_CONDUTOR, NUM_CPF_CONDUTOR," +
				" TIP_CNH_CONDUTOR, NUM_CNH_CONDUTOR, UF_CNH_CONDUTOR," +
				" CATEGORIA_CONDUTOR, DSC_END_CONDUTOR, NUM_CEP_CONDUTOR," +
				" NOM_MUNIC_CONDUTOR, UF_CONDUTOR, " +
				" COD_STATUS, NOM_USERNAME, " +
				" COD_ORGAO_LOTACAO, COD_ORGAO, COD_ARQUIVO, " +
				" TXT_EMAIL, NOM_USERNAME_FIS, COD_ORGAO_LOTACAO_FIS, " +
				" COD_PENALIDADE, DSC_PENALIDADE, DSC_PENALIDADE_RES," +
				" NUM_PROTOCOLO_ENTREGA_CNH, " +
				" TO_CHAR(DAT_PROCESSO, 'dd/MM/yyyy') as DAT_PROCESSO, " +
				" TO_CHAR(DAT_EMISSAO_CONDUTOR, 'dd/MM/yyyy') as DAT_EMISSAO_CONDUTOR, " +
				" TO_CHAR(DAT_VALID_CONDUTOR, 'dd/MM/yyyy') as DAT_VALID_CONDUTOR, " +
				" TO_CHAR(DAT_STATUS, 'dd/MM/yyyy') as DAT_STATUS, " +
				" TO_CHAR(DAT_PROC, 'dd/MM/yyyy') as DAT_PROC, " +
				" TO_CHAR(ULT_DAT_DEFESA, 'dd/MM/yyyy') as ULT_DAT_DEFESA, " +
				" TO_CHAR(ULT_DAT_RECURSO, 'dd/MM/yyyy') as ULT_DAT_RECURSO, " +
				" TO_CHAR(ULT_DAT_ENTREGACNH, 'dd/MM/yyyy') as ULT_DAT_ENTREGACNH, " +
				" TO_CHAR(DAT_GERACAO, 'dd/MM/yyyy') as DAT_GERACAO, " +
				" TO_CHAR(DAT_PROCESSO_FIS, 'dd/MM/yyyy') as DAT_PROCESSO_FIS, " +
				" TO_CHAR(DAT_PROC_FIS, 'dd/MM/yyyy') as DAT_PROC_FIS, " +
				" TO_CHAR(DAT_ENTREGA_CNH, 'dd/MM/yyyy') as DAT_ENTREGA_CNH, " +
				" TO_CHAR(DAT_DEVOLUCAO_CNH, 'dd/MM/yyyy') as DAT_DEVOLUCAO_CNH, " +
				" TO_CHAR(DAT_ARQ_APREENSAO, 'dd/MM/yyyy') as DAT_ARQ_APREENSAO" +
				" FROM TPNT_PROCESSO" +
				" WHERE TIPO_PROCESSO  = '" + tipoParam + "'" +
				" AND NUM_PROCESSO='E12/353679/    /2007' ";
			/* 	" AND COD_PROCESSO >= 0  AND COD_PROCESSO <= 4485+12000	";*/	
			/*	" AND COD_PROCESSO > 4485+12000   AND COD_PROCESSO <= 4485+12000*2	";*/
			/*	" AND COD_PROCESSO > 4485+12000*2 AND COD_PROCESSO <= 4485+12000*3	";*/
				sCmd+="	ORDER BY COD_PROCESSO  ";
			
			int qtdReg=0;
			ResultSet rs  = stmt01.executeQuery(sCmd);
			while(rs.next())
			{
				qtdReg++;
				
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo2++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo2++;
				
				String linhaArq = "";
				
				String codRegistro = "2";
				linhaArq += codRegistro;
				
				String codProcesso = rs.getString("COD_PROCESSO");
				
				String numProcesso = rs.getString("NUM_PROCESSO");
				if(numProcesso==null)
					numProcesso = "";
				linhaArq += fPadBranco(numProcesso, 20) ;
				
				numProcesso = fPadBranco(numProcesso,20); /*Ricardo*/
				
				String datProcesso = rs.getString("DAT_PROCESSO");
				if(datProcesso==null)
					datProcesso = "";
				linhaArq += formataDataYYYYMMDD(datProcesso);
				
				datProcesso = formataDataYYYYMMDD(datProcesso); /*Ricardo*/
				
				String datProcFis = rs.getString("DAT_PROC_FIS");
				if(datProcFis==null)
					datProcFis = "";
				linhaArq += formataDataYYYYMMDD(datProcFis);
				
				String tipCNHCondutor = rs.getString("TIP_CNH_CONDUTOR");
				if(tipCNHCondutor==null)
					tipCNHCondutor = "";
				linhaArq += fPadBranco(tipCNHCondutor,1);
				
				tipCNHCondutor = fPadBranco(tipCNHCondutor,1); /*Ricardo*/
				
				String numCNHCondutor = rs.getString("NUM_CNH_CONDUTOR");
				if(numCNHCondutor==null)
					numCNHCondutor = "";
				linhaArq += fFill(numCNHCondutor,11); 
				
				numCNHCondutor = fFill(numCNHCondutor,11); /*Ricardo*/
				
				String UFCNHCondutor = rs.getString("UF_CNH_CONDUTOR");
				if(UFCNHCondutor==null)
					UFCNHCondutor = "";
				linhaArq += fPadBranco(UFCNHCondutor,2);
				
				String datEmissaoCondutor = rs.getString("DAT_EMISSAO_CONDUTOR");
				if(datEmissaoCondutor==null)
					datEmissaoCondutor = "";
				linhaArq += formataDataYYYYMMDD(datEmissaoCondutor);
				
				
				String datValidCondutor = rs.getString("DAT_VALID_CONDUTOR");
				if(datValidCondutor==null)
					datValidCondutor = "";
				linhaArq += formataDataYYYYMMDD(datValidCondutor);
				
				String codPenalidade = rs.getString("COD_PENALIDADE");
				if(codPenalidade==null)
					codPenalidade = "";
				linhaArq += fFill(codPenalidade,3);
				
				// ------------------------------------------------------------------------------------
				String prazoSuspensao = "";
				sCmd = "SELECT NUM_PRAZO as prazoSuspensao" +
						" FROM TPNT_PENALIDADE P" +
						" WHERE P.COD_PENALIDADE = '" + codPenalidade + "'";
				
				ResultSet rs2 = stmt02.executeQuery(sCmd);
				if(rs2.next())
					prazoSuspensao = rs2.getString("prazoSuspensao");
				if(prazoSuspensao==null)
					prazoSuspensao = "";
				linhaArq += fFill(prazoSuspensao,5);
				// ------------------------------------------------------------------------------------
				
				String totPontos = rs.getString("TOT_PONTOS");
				if(totPontos==null)
					totPontos = "";
				linhaArq += fFill(totPontos,5);
				
				totPontos = fFill(totPontos,6); /*Ricardo*/
				
				
				String qtdMulta = rs.getString("QTD_MULTA");
				if(qtdMulta==null)
					qtdMulta = "";
				linhaArq += fFill(qtdMulta,3); 
				
				// ------------------------------------------------------------------------------------
				String qtdNotificacoes = "";
				sCmd = " SELECT COUNT(*) as qtdNotificacoes" +
						" FROM TPNT_NOTIFICACAO N" +
						" WHERE N.COD_PROCESSO = '" + codProcesso + "'";
				
				rs2  = stmt02.executeQuery(sCmd);
				if(rs2.next())
					qtdNotificacoes = rs2.getString("qtdNotificacoes");
				if(qtdNotificacoes==null)
					qtdNotificacoes = "";
				linhaArq += fFill(qtdNotificacoes,3);
				// ------------------------------------------------------------------------------------
				
				String tipoProcesso = rs.getString("TIPO_PROCESSO");
				if(tipoProcesso==null)
					tipoProcesso = "";
				linhaArq += fPadBranco(tipoProcesso,1);
				
				tipoProcesso = fPadBranco(tipoProcesso,1); /*Ricardo*/
				
				
				String sitProcesso = rs.getString("SIT_PROCESSO");
				if(sitProcesso==null)
					sitProcesso = "";
				linhaArq += fPadBranco(sitProcesso,1);
				
				String formato = "";
				if(datProcFis.trim().equals("00000000"))
					formato = "L";
				else
					formato = "F";
				linhaArq += formato;
				
				String qtdProcAnterior = rs.getString("QTD_PROC_ANTERIOR");
				if(qtdProcAnterior==null)
					qtdProcAnterior = "";
				linhaArq += fFill(qtdProcAnterior,2);
				
				String nomCondutor = rs.getString("NOM_CONDUTOR");
				if(nomCondutor==null)
					nomCondutor = "";
				linhaArq += fPadBranco(nomCondutor,60);
				
				nomCondutor = fPadBranco(nomCondutor,60); /*Ricardo*/
				
				String numCPFCondutor = rs.getString("NUM_CPF_CONDUTOR");
				if(numCPFCondutor==null)
					numCPFCondutor = "";
				linhaArq += fFill(numCPFCondutor,11);
				
				numCPFCondutor = fFill(numCPFCondutor,11); /*Ricardo*/
				
				String dscEndCondutor = rs.getString("DSC_END_CONDUTOR");
				if(dscEndCondutor==null)
					dscEndCondutor = "";
				linhaArq += fPadBranco(dscEndCondutor,60);
				
				String nomMunicCondutor = rs.getString("NOM_MUNIC_CONDUTOR");
				if(nomMunicCondutor==null)
					nomMunicCondutor = "";
				linhaArq += fFill(nomMunicCondutor,50);
				
				String numCEPCondutor = rs.getString("NUM_CEP_CONDUTOR");
				if(numCEPCondutor==null)
					numCEPCondutor = "";
				linhaArq += fFill(numCEPCondutor,8);
				
				String UFCondutor = rs.getString("UF_CONDUTOR");
				if(UFCondutor==null)
					UFCondutor = "";
				linhaArq += fPadBranco(UFCondutor,2);
				
				String nomUsername = rs.getString("NOM_USERNAME");
				if(nomUsername==null) nomUsername = "";
				nomUsername = fPadBranco(nomUsername,20);
				
				String orgaoLotacao = rs.getString("COD_ORGAO_LOTACAO");
				if(orgaoLotacao==null) orgaoLotacao = "";
				
				orgaoLotacao = fFill(orgaoLotacao,6); /*Ricardo*/

				String codStatus = rs.getString("COD_STATUS");
				if(codStatus==null)
					codStatus = "";
				linhaArq += fFill(codStatus,3);
				
				codStatus = fFill(codStatus,3); /*Ricardo*/
				
				String datStatus = rs.getString("DAT_STATUS");
				if(datStatus==null)
					datStatus = "";
				linhaArq += formataDataYYYYMMDD(datStatus);
				
				datStatus = formataDataYYYYMMDD(datStatus); /*Ricardo*/
				
				
				String ultDatDefesa = rs.getString("ULT_DAT_DEFESA");
				if(ultDatDefesa==null)
					ultDatDefesa = "";
				linhaArq += formataDataYYYYMMDD(ultDatDefesa); 
				
				ultDatDefesa = formataDataYYYYMMDD(ultDatDefesa); /*Ricardo*/
				
				
				String ultDatRecurso = rs.getString("ULT_DAT_RECURSO");
				if(ultDatRecurso==null)
					ultDatRecurso = "";
				linhaArq += formataDataYYYYMMDD(ultDatRecurso); 
				
				ultDatRecurso = formataDataYYYYMMDD(ultDatRecurso); /*Ricardo*/
				
				
				String ultDatEntregaCNH = rs.getString("ULT_DAT_ENTREGACNH");
				if(ultDatEntregaCNH==null)
					ultDatEntregaCNH = "";
				linhaArq += formataDataYYYYMMDD(ultDatEntregaCNH);
				
				ultDatEntregaCNH = formataDataYYYYMMDD(ultDatEntregaCNH); /*Ricardo*/
				
				String codEventoArquivo="000";
				String origemEventoArquivo="000";
				String nomUsernameArquivo = nomUsername;
				String orgaoLotacaoArquivo = orgaoLotacao; 
					
				String tipoProcessoArquivo = tipoProcesso; 
				String formatoArquivo = formato; 
				String numProcessoArquivo = numProcesso; 
				String codStatusArquivo = codStatus; 
				String datStatusArquivo = datStatus; 
					
				String totPontosArquivo = totPontos;
				String nomCondutorArquivo = nomCondutor; 
				String numCPFCondutorArquivo = numCPFCondutor; 
				String tipCNHCondutorArquivo = tipCNHCondutor; 
					
				String numCNHCondutorArquivo = numCNHCondutor; 
				String datProcessoArquivo = datProcesso; 
				String horaProcessoArquivo="0200000";
				String numNotificacaoArquivo="000000000";
				String numSeqNotificacaoArquivo="000";
				
				String numCodigoRetornoArquivo="00";
				String datEntrNotificacaoArquivo="00000000";
				String ultDatDefesaArquivo = ultDatDefesa;
					
				String ultDatRecursoArquivo = ultDatRecurso; 
				String ultDatEntregaCNHArquivo = ultDatEntregaCNH; 
				String datEventoArquivo = "00000000";
				String horaEventoArquivo = "0000000";				
				
				String dscMotivoArquivo = fPadBranco("",1000);
				String tipoJuntaArquivo = "0";
				String codJuntaArquivo = fFill("",6);
					
				String cpfRelatorArquivo = fFill("",11);
				String codPenalidadeArquivo = "000";
				String tipoRequerimentoArquivo = fPadBranco("",2);
				
				String numRequerimentoArquivo = fPadBranco("",30);
				String statusRequerimentoArquivo = fFill("",2); 
				String codResultadoArquivo = fPadBranco("",2);
				String numDiasSuspensaoArquivo = fFill("",5);
					
				String numDocumentoDOArquivo = fPadBranco("",20);
				String tipoAtaArquivo = fPadBranco("",1);
				String datGeracaoAtaArquivo = "00000000";
				String statusAtaArquivo = fPadBranco("",3);
					
				String numeroAtaArquivo = fPadBranco("",20);
				
				String datPublicacaoArquivo = "00000000";
				String datEstornoArquivo    = "00000000";
				String datDevolucaoCNHArquivo = "00000000";
				String autosInfracaoArquivo = fPadBranco("",2400);
				
				linhaArq="1"+codEventoArquivo+origemEventoArquivo+nomUsernameArquivo+orgaoLotacaoArquivo+
				tipoProcessoArquivo+formatoArquivo+numProcessoArquivo+codStatusArquivo+datStatusArquivo+
				totPontosArquivo+nomCondutorArquivo+numCPFCondutorArquivo+tipCNHCondutorArquivo+
				numCNHCondutorArquivo+datProcessoArquivo+horaProcessoArquivo+numNotificacaoArquivo+numSeqNotificacaoArquivo+
				numCodigoRetornoArquivo+datEntrNotificacaoArquivo+ultDatDefesaArquivo+
				ultDatRecursoArquivo+ultDatEntregaCNHArquivo+datEventoArquivo+horaEventoArquivo+
				dscMotivoArquivo+tipoJuntaArquivo+codJuntaArquivo+
				cpfRelatorArquivo+codPenalidadeArquivo+tipoRequerimentoArquivo+
				numRequerimentoArquivo+statusRequerimentoArquivo+codResultadoArquivo+numDiasSuspensaoArquivo+
				numDocumentoDOArquivo+tipoAtaArquivo+datGeracaoAtaArquivo+statusAtaArquivo+
				numeroAtaArquivo+datPublicacaoArquivo+datEstornoArquivo+datDevolucaoCNHArquivo+autosInfracaoArquivo;

				

				if(qtdReg%200 == 0 )				
						System.out.println("EXPORTANDO PROCESSOS > Processo =>  " + codProcesso);
				
				
				rs2.close();
				arqBASE_PONTUACAO.write(linhaArq);
				arqBASE_PONTUACAO.write("\n"); 
		//		System.out.println("EXPORTANDO PROCESSOS > Processo =>  " + codProcesso);
				
				//exporta_MULTA(arqBASE_PONTUACAO, stmt03, stmt04, codProcesso, tipoParam); N�O FAZER */
				exporta_HISTORICO(arqBASE_PONTUACAO, stmt03, stmt04, stmt05, stmt06, codProcesso, tipoParam);
				
				/*
				if(qtdReg==10)
					break;
				*/
				
					
			}
			rs.close();
			
		//	System.out.println("---------------------------------------------------------------------------------------------");
			System.out.println("FIM DO PROCESSAMENTO DA EXPORTA��O. ");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_PROCESSO(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	
	
	private void exporta_MULTA(BufferedWriter bw, Statement stmt03, Statement stmt04, String codProcesso, String tipoParam) throws Exception, DaoException
	{
		try
		{
			String sCmd = "SELECT COD_MULTA, COD_PROCESSO, NUM_PROCESSO, " +
				" NUM_NOTIFICACAO, SEQ_NOTIFICACAO, SEQ_MULTA, " +
				" COD_ORGAO, SIG_ORGAO, NUM_AUTO_INFRACAO, " +
				" NUM_PLACA, VAL_HOR_INFRACAO, " +
				" DSC_LOCAL_INFRACAO, DSC_ENQUADRAMENTO, NUM_PONTO, " +
				" NOM_USERNAME, COD_ORGAO_LOTACAO, " +
				" DSC_INFRACAO, COD_INFRACAO, " +
				" TO_CHAR(DAT_PROCESSO, 'dd/MM/yyyy') as DAT_PROCESSO, " +
				" TO_CHAR(DAT_INFRACAO, 'dd/MM/yyyy') as DAT_INFRACAO,  " +
				" TO_CHAR(DAT_TRANS_JULGADO, 'dd/MM/yyyy') as DAT_TRANS_JULGADO, " +
				" TO_CHAR(DAT_PROC, 'dd/MM/yyyy') as DAT_PROC " +
				" FROM TPNT_MULTA " +
				"	WHERE COD_PROCESSO  = '" + codProcesso + "'" +
				"	ORDER BY NUM_PROCESSO  ";
			
			ResultSet rs  = stmt03.executeQuery(sCmd);
			while(rs.next())
			{ 
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo4++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo4++;
				
				String linhaArq = "";
				
				String codRegistro = "4";
				linhaArq += codRegistro;
				
				String codMulta = rs.getString("COD_MULTA");
				
				// ------------------------------------------------------------------------------------
				String numProcesso = "";
				sCmd = "SELECT NUM_PROCESSO as numProcesso" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				ResultSet rs2 = stmt04.executeQuery(sCmd);
				if(rs2.next())
					numProcesso = rs2.getString("numProcesso");
				if(numProcesso==null)
					numProcesso = "";
				linhaArq += fPadBranco(numProcesso,20);
				// ------------------------------------------------------------------------------------
				
				String numAutoInfracao = rs.getString("NUM_AUTO_INFRACAO");
				if(numAutoInfracao==null)
					numAutoInfracao = "";
				linhaArq += fPadBranco(numAutoInfracao, 12);
				
				String numPlaca = rs.getString("NUM_PLACA");
				if(numPlaca==null)
					numPlaca = "";
				linhaArq += fPadBranco(numPlaca, 7);

				String datInfracao = rs.getString("DAT_INFRACAO");
				if(datInfracao==null)
					datInfracao = "";
				linhaArq += formataDataYYYYMMDD(datInfracao);

				String datTransJulgado = rs.getString("DAT_TRANS_JULGADO");
				if(datTransJulgado==null)
					datTransJulgado = "";
				linhaArq += formataDataYYYYMMDD(datTransJulgado);

				String valHoraInfracao = rs.getString("VAL_HOR_INFRACAO");
				if(valHoraInfracao==null)
					valHoraInfracao = "";
				linhaArq += fFill(valHoraInfracao, 4);

				String codOrgao = rs.getString("COD_ORGAO");
				if(codOrgao==null)
					codOrgao = "";
				linhaArq += fFill(codOrgao, 6);

				String codInfracao = rs.getString("COD_INFRACAO");
				if(codInfracao==null)
					codInfracao = "";
				linhaArq += fFill(codInfracao, 5);

				String dscInfracao = rs.getString("DSC_INFRACAO");
				if(dscInfracao==null)
					dscInfracao = "";
				linhaArq += fPadBranco(dscInfracao, 80);

				String dscEnquadramento = rs.getString("DSC_ENQUADRAMENTO");
				if(dscEnquadramento==null)
					dscEnquadramento = "";
				linhaArq += fPadBranco(dscEnquadramento, 20);

				String numPonto = rs.getString("NUM_PONTO");
				if(numPonto==null)
					numPonto = "";
				linhaArq += fFill(numPonto, 1);

				String dscLocalInfracao = rs.getString("DSC_LOCAL_INFRACAO");
				if(dscLocalInfracao==null)
					dscLocalInfracao = "";
				linhaArq += fPadBranco(dscLocalInfracao, 80);

				bw.write(linhaArq);
				bw.write("\n");
				System.out.println("EXPORTANDO MULTAS > Multa =>  " + codMulta);
			}
			rs.close();
			
	//		System.out.println("---------------------------------------------------------------------------------------------");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_MULTA(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	
	private void exporta_REQUERIMENTO(BufferedWriter bw, Statement stmt05, Statement stmt06, String codProcesso, String tipoParam, String numRequerimentoParam) throws Exception, DaoException
	{
		try
		{
			String sCmd = "SELECT COD_REQUERIMENTO, COD_PROCESSO, COD_TIPO_SOLIC, " +
				" NUM_REQUERIMENTO, COD_STATUS_REQUERIMENTO," +
				" NOM_USERNAME_DEF, COD_ORGAO_LOTACAO_DEF, " +
				" COD_RELATOR_JUN, NOM_USERNAME_JUN, COD_ORGAO_LOTACAO_JUN," +
				" COD_RESULT_RES, TXT_MOTIVO_RES, " +
				" NOM_USERNAME_RES, COD_ORGAO_LOTACAO_RES, " +
				" TXT_MOTIVO_EST, NOM_USERNAME_EST, " +
				" COD_ORGAO_LOTACAO_EST, " +
				" NOM_USERNAME_EDO, COD_ORGAO_LOTACAO_EDO, " +
				" NOM_USERNAME_PDO, COD_ORGAO_LOTACAO_PDO, " +
				" COD_ENVIO_EDO, DSC_PENALIDADE, COD_PENALIDADE, " +
				"  TO_CHAR(DAT_REQUERIMENTO, 'dd/MM/yyyy') as DAT_REQUERIMENTO, " +
				"  TO_CHAR(DAT_PROC_DEF, 'dd/MM/yyyy') as DAT_PROC_DEF, " +
				"  TO_CHAR(DAT_JUN, 'dd/MM/yyyy') as DAT_JUN, " +
				"  TO_CHAR(DAT_PROC_JUN, 'dd/MM/yyyy') as DAT_PROC_JUN, " +
				"  TO_CHAR(DAT_RES, 'dd/MM/yyyy') as DAT_RES, " +
				"  TO_CHAR(DAT_PROC_RES, 'dd/MM/yyyy') as DAT_PROC_RES, " +
				"  TO_CHAR(DAT_EST, 'dd/MM/yyyy') as DAT_EST, " +
				"  TO_CHAR(DAT_PROC_EST, 'dd/MM/yyyy') as DAT_PROC_EST, " +
				"  TO_CHAR(DAT_EDO, 'dd/MM/yyyy') as DAT_EDO, " +
				"  TO_CHAR(DAT_PROC_EDO, 'dd/MM/yyyy') as DAT_PROC_EDO, " +
				"  TO_CHAR(DAT_PDO, 'dd/MM/yyyy') as DAT_PDO, " +
				"  TO_CHAR(DAT_PROC_PDO, 'dd/MM/yyyy') as DAT_PROC_PDO " +
				"  FROM TPNT_REQUERIMENTO " +
				"	WHERE COD_PROCESSO  = '" + codProcesso + "'" +
				"	AND NUM_REQUERIMENTO  = '" + numRequerimentoParam + "'" +
				"	ORDER BY COD_REQUERIMENTO ";
			
			ResultSet rs  = stmt05.executeQuery(sCmd);
			while(rs.next())
			{ 
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo5++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo5++;
				
				String linhaArq = "";
				
				String codRegistro = "5";
				linhaArq += codRegistro;
				
				String codRequerimento = rs.getString("COD_REQUERIMENTO");
				
				// ------------------------------------------------------------------------------------
				String numProcesso   = "";
				String codPenalidade = "";
				String dscPenalidade = "";
				sCmd = "SELECT NUM_PROCESSO as numProcesso,COD_PENALIDADE,DSC_PENALIDADE" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				ResultSet rs2 = stmt06.executeQuery(sCmd);
				if(rs2.next()) {
					numProcesso   = rs2.getString("numProcesso");
					codPenalidade = rs2.getString("COD_PENALIDADE");
					dscPenalidade = rs2.getString("DSC_PENALIDADE");
				}
				if(numProcesso==null) numProcesso = "";
				if(codPenalidade==null) codPenalidade = "";
				if(dscPenalidade==null) dscPenalidade = "";
				
				
				linhaArq += fPadBranco(numProcesso,20);
				// ------------------------------------------------------------------------------------
				
				
				String codTipoSolic = rs.getString("COD_TIPO_SOLIC");
				if(codTipoSolic==null)
					codTipoSolic = "";
				linhaArq += fPadBranco(codTipoSolic, 2);
				
				String numRequerimento = rs.getString("NUM_REQUERIMENTO");
				if(numRequerimento==null)
					numRequerimento = "";
				linhaArq += fPadBranco(numRequerimento, 20);
				
				// ------------------------------------------------------------------------------------
				String nomResponsavel = "";
				sCmd = "SELECT NOM_CONDUTOR as nomResponsavel" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				rs2 = stmt06.executeQuery(sCmd);
				if(rs2.next())
					nomResponsavel = rs2.getString("nomResponsavel");
				if(nomResponsavel==null)
					nomResponsavel = "";
				linhaArq += fPadBranco(nomResponsavel,60);
				// ------------------------------------------------------------------------------------
				
				
				String codStatusRequerimento = rs.getString("COD_STATUS_REQUERIMENTO");
				if(codStatusRequerimento==null)
					codStatusRequerimento = "";
				linhaArq += fFill(codStatusRequerimento, 1);
				
				String usernameAbertura = rs.getString("NOM_USERNAME_DEF");
				if(usernameAbertura==null)
					usernameAbertura = "";
				linhaArq += fPadBranco(usernameAbertura, 20);
				
				String codOrgaoLotacao_Abertura = rs.getString("COD_ORGAO_LOTACAO_DEF");
				if(codOrgaoLotacao_Abertura==null)
					codOrgaoLotacao_Abertura = "";
				linhaArq += fFill(codOrgaoLotacao_Abertura, 6);
				
				String datProcessamento_Abertura = rs.getString("DAT_PROC_DEF");
				if(datProcessamento_Abertura==null)
					datProcessamento_Abertura = "";
				linhaArq += formataDataYYYYMMDD(datProcessamento_Abertura);
				
				String datRequerimento_Abertura = rs.getString("DAT_REQUERIMENTO");
				if(datRequerimento_Abertura==null)
					datRequerimento_Abertura = "";
				linhaArq += formataDataYYYYMMDD(datRequerimento_Abertura);
				
				String datRelator = rs.getString("DAT_JUN");
				if(datRelator==null)
					datRelator = "";
				linhaArq += formataDataYYYYMMDD(datRelator);
				
				String codJuntaRelator = rs.getString("COD_RELATOR_JUN");
				if(codJuntaRelator==null)
					codJuntaRelator = "";
				linhaArq += fFill(codJuntaRelator,6);
				
				
				// ------------------------------------------------------------------------------------
				String siglaJuntaRelator = "";
				String tipoJuntaRelator = "";
				String CPFRelator = "";
				
				sCmd = "SELECT J.SIG_JUNTA as siglaJuntaRelator, J.IND_TIPO as tipoJuntaRelator, R.NUM_CPF as CPFRelator" +
						" FROM TPNT_JUNTA J " +
						" JOIN TPNT_RELATOR R" +
						" ON (J.COD_JUNTA = R.COD_JUNTA)" +
						" WHERE R.COD_JUNTA = '" + codJuntaRelator + "'";
				
				rs2 = stmt06.executeQuery(sCmd);
				if(rs2.next())
				{
					siglaJuntaRelator = rs2.getString("siglaJuntaRelator");
					if(siglaJuntaRelator==null)
						siglaJuntaRelator = "";
					
					tipoJuntaRelator = rs2.getString("tipoJuntaRelator");
					if(tipoJuntaRelator==null)
						tipoJuntaRelator = "";
					
					CPFRelator = rs2.getString("CPFRelator");
					if(CPFRelator==null)
						CPFRelator = "";
				}
				
				linhaArq += fPadBranco(siglaJuntaRelator,10);
				linhaArq += fFill(tipoJuntaRelator,1);
				linhaArq += fFill(CPFRelator,11);
					
				// ------------------------------------------------------------------------------------
				
				
				String username_Relator = rs.getString("NOM_USERNAME_JUN");
				if(username_Relator==null)
					username_Relator = "";
				linhaArq += fPadBranco(username_Relator, 20);
				
				String codOrgaoLotacao_Relator = rs.getString("COD_ORGAO_LOTACAO_JUN");
				if(codOrgaoLotacao_Relator==null)
					codOrgaoLotacao_Relator = "";
				linhaArq += fFill(codOrgaoLotacao_Relator, 6);
				
				String datProcessamento_Relator = rs.getString("DAT_PROC_JUN");
				if(datProcessamento_Relator==null)
					datProcessamento_Relator = "";
				linhaArq += formataDataYYYYMMDD(datProcessamento_Relator);
				
				String data_Resultado = rs.getString("DAT_RES");
				if(data_Resultado==null)
					data_Resultado = "";
				linhaArq += formataDataYYYYMMDD(data_Resultado);
				
				String codigo_Resultado = rs.getString("COD_RESULT_RES");
				if(codigo_Resultado==null)
					codigo_Resultado = "";
				linhaArq += fPadBranco(codigo_Resultado, 1);
				
				String motivo_Resultado = rs.getString("TXT_MOTIVO_RES");
				if(motivo_Resultado==null)
					motivo_Resultado = "";
				linhaArq += fPadBranco(motivo_Resultado, 1500);
				
				String codPenalidade_Resultado = rs.getString("COD_PENALIDADE");
				if(codPenalidade_Resultado==null)
					codPenalidade_Resultado = "";
				/*linhaArq += fFill(codPenalidade_Resultado, 3);*/
				linhaArq += fFill(codPenalidade, 3);				
				
				String dscPenalidade_Resultado = rs.getString("DSC_PENALIDADE");
				if(dscPenalidade_Resultado==null)
					dscPenalidade_Resultado = "";
				
				/*linhaArq += fPadBranco(dscPenalidade_Resultado, 120);*/
				linhaArq += fPadBranco(dscPenalidade, 120);
				
				String username_Resultado = rs.getString("NOM_USERNAME_RES");
				if(username_Resultado==null)
					username_Resultado = "";
				linhaArq += fPadBranco(username_Resultado, 20);
				
				String codOrgaoLotacao_Resultado = rs.getString("COD_ORGAO_LOTACAO_RES");
				if(codOrgaoLotacao_Resultado==null)
					codOrgaoLotacao_Resultado = "";
				linhaArq += fFill(codOrgaoLotacao_Resultado, 6);
				
				String datProcessamento_Resultado = rs.getString("DAT_PROC_RES");
				if(datProcessamento_Resultado==null)
					datProcessamento_Resultado = "";
				linhaArq += formataDataYYYYMMDD(datProcessamento_Resultado);
				
				bw.write(linhaArq);
				bw.write("\n");
	//			System.out.println("EXPORTANDO REQUERIMENTOS > Requerimento =>  " + codRequerimento);
			}
			rs.close();
	//		System.out.println("---------------------------------------------------------------------------------------------");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_REQUERIMENTO(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	
	private void exporta_HISTORICO(BufferedWriter bw, Statement stmt03, Statement stmt04, Statement stmt05, Statement stmt06, String codProcesso, String tipoParam) throws Exception, DaoException
	{
		String horaEvento="08";
		String minutoEvento="00";
		String segundoEvento="00";
		String milisegundoEvento="0";
		try
		{
			String sCmd = " SELECT COD_HISTORICO_PROCESSO, COD_PROCESSO, NUM_PROCESSO," +
					" COD_STATUS, COD_EVENTO, COD_ORIGEM_EVENTO," +
					" TXT_COMPLEMENTO_01, TXT_COMPLEMENTO_02, TXT_COMPLEMENTO_03, " +
					" TXT_COMPLEMENTO_04, TXT_COMPLEMENTO_05, TXT_COMPLEMENTO_06," +
					" TXT_COMPLEMENTO_07, TXT_COMPLEMENTO_08, TXT_COMPLEMENTO_09," +
					" TXT_COMPLEMENTO_10, TXT_COMPLEMENTO_11, TXT_COMPLEMENTO_12," +
					" NOM_USERNAME, COD_ORGAO_LOTACAO, COD_TRANSACAO, " +
					" NUM_SEQ_TRANSACAO, COD_RETORNO, " +
					" TO_CHAR(DAT_STATUS, 'dd/MM/yyyy') as DATSTATUS, " +
					" TO_CHAR(DAT_PROC, 'dd/MM/yyyy') as DAT_PROC, " +
					" TO_CHAR(DAT_TRANSACAO, 'dd/MM/yyyy') as DAT_TRANSACAO " +
					" FROM TPNT_HISTORICO_PROCESSO " +
					"	WHERE COD_PROCESSO  = '" + codProcesso + "'" +
					"	ORDER BY DAT_STATUS  ";
			
			ResultSet rs  = stmt03.executeQuery(sCmd);
			while(rs.next())
			{ 
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo6++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo6++;
				
				String linhaArq = "";
				
				String codRegistro = "6";
				linhaArq += codRegistro;
				
				String codHistoricoProcesso = rs.getString("COD_HISTORICO_PROCESSO");
				
				
				// ------------------------------------------------------------------------------------
				String numProcesso = "";
				sCmd = "SELECT NUM_PROCESSO as numProcesso" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				ResultSet rs2 = stmt04.executeQuery(sCmd);
				if(rs2.next())
					numProcesso = rs2.getString("numProcesso");
				if(numProcesso==null)
					numProcesso = "";
				linhaArq += fPadBranco(numProcesso,20);
				
				numProcesso = fPadBranco(numProcesso,20); /*Ricardo*/
				// ------------------------------------------------------------------------------------
				
				
				String codStatus = rs.getString("COD_STATUS");
				if(codStatus==null)
					codStatus = "";
				linhaArq += fFill(codStatus, 3);
				
				codStatus = fFill(codStatus, 3); /*Ricardo*/
				
				String datStatus = rs.getString("DATSTATUS");
				if(datStatus==null)
					datStatus = "";
				linhaArq += formataDataYYYYMMDD(datStatus);
				
				String dat_Status=datStatus;
				datStatus = formataDataYYYYMMDD(datStatus); /*Ricardo*/
				
				
				String codEvento = rs.getString("COD_EVENTO");
				if(codEvento==null)
					codEvento = "";
				linhaArq += fFill(codEvento, 3);
				
				codEvento = fFill(codEvento, 3); /*Ricardo*/
				
				String codOrigemEvento = rs.getString("COD_ORIGEM_EVENTO");
				if(codOrigemEvento==null)
					codOrigemEvento = "";
				linhaArq += fFill(codOrigemEvento, 3);
				
				codOrigemEvento = fFill(codOrigemEvento, 3); /*Ricardo*/
				
				
				String username = rs.getString("NOM_USERNAME");
				if(username==null)
					username = "";
				linhaArq += fPadBranco(username, 20);
				
				username = fPadBranco(username, 20); /*Ricardo*/
				
				String codOrgaoLotacao = rs.getString("COD_ORGAO_LOTACAO");
				if(codOrgaoLotacao==null)
					codOrgaoLotacao = "";
				linhaArq += fFill(codOrgaoLotacao, 6);
				
				codOrgaoLotacao = fFill(codOrgaoLotacao, 6); /*Ricardo*/
				
				String datProc = rs.getString("DAT_PROC");
				if(datProc==null)
					datProc = "";
				linhaArq += formataDataYYYYMMDD(datProc);
				
				datProc = formataDataYYYYMMDD(datProc); /*Ricardo*/
				
				String txtComplemento01 = rs.getString("TXT_COMPLEMENTO_01");
				if(txtComplemento01==null)
					txtComplemento01 = "";
				linhaArq += fPadBranco(txtComplemento01, 500);
				
				String txtComplemento02 = rs.getString("TXT_COMPLEMENTO_02");
				if(txtComplemento02==null)
					txtComplemento02 = "";
				linhaArq += fPadBranco(txtComplemento02, 500);
				
				String txtComplemento03 = rs.getString("TXT_COMPLEMENTO_03");
				if(txtComplemento03==null)
					txtComplemento03 = "";
				linhaArq += fPadBranco(txtComplemento03, 500);
				
				String txtComplemento04 = rs.getString("TXT_COMPLEMENTO_04");
				if(txtComplemento04==null)
					txtComplemento04 = "";
				linhaArq += fPadBranco(txtComplemento04, 1500);
				
				String txtComplemento05 = rs.getString("TXT_COMPLEMENTO_05");
				if(txtComplemento05==null)
					txtComplemento05 = "";
				linhaArq += fPadBranco(txtComplemento05, 500);
				
				String txtComplemento06 = rs.getString("TXT_COMPLEMENTO_06");
				if(txtComplemento06==null)
					txtComplemento06 = "";
				linhaArq += fPadBranco(txtComplemento06, 500);
				
				String txtComplemento07 = rs.getString("TXT_COMPLEMENTO_07");
				if(txtComplemento07==null)
					txtComplemento07 = "";
				linhaArq += fPadBranco(txtComplemento07, 500);
				
				String txtComplemento08 = rs.getString("TXT_COMPLEMENTO_08");
				if(txtComplemento08==null)
					txtComplemento08 = "";
				linhaArq += fPadBranco(txtComplemento08, 500);
				
				String txtComplemento09 = rs.getString("TXT_COMPLEMENTO_09");
				if(txtComplemento09==null)
					txtComplemento09 = "";
				linhaArq += fPadBranco(txtComplemento09, 500);
				
				String txtComplemento10 = rs.getString("TXT_COMPLEMENTO_10");
				if(txtComplemento10==null)
					txtComplemento10 = "";
				linhaArq += fPadBranco(txtComplemento10, 500);
				
				String txtComplemento11 = rs.getString("TXT_COMPLEMENTO_11");
				if(txtComplemento11==null)
					txtComplemento11 = "";
				linhaArq += fPadBranco(txtComplemento11, 500);
				
				String txtComplemento12 = rs.getString("TXT_COMPLEMENTO_12");
				if(txtComplemento12==null)
					txtComplemento12 = "";
				linhaArq += fPadBranco(txtComplemento12, 500);
				

				String codEventoArquivo=codEvento;
				String origemEventoArquivo=codOrigemEvento;
				String nomUsernameArquivo = username;
				String orgaoLotacaoArquivo = codOrgaoLotacao; 
					
				String tipoProcessoArquivo = fPadBranco("",1); 
				String formatoArquivo = fPadBranco("",1); 
				String numProcessoArquivo = numProcesso; 
				String codStatusArquivo = codStatus; 
				String datStatusArquivo = datStatus; 
					
				String totPontosArquivo = fFill("",6);
				String nomCondutorArquivo = fPadBranco("",60); 
				String numCPFCondutorArquivo = fFill("",11);
				String tipCNHCondutorArquivo =  fFill("",1);
					
				String numCNHCondutorArquivo = fFill("",11); 
				String datProcessoArquivo =  "00000000";
				String horaProcessoArquivo= "0000000";
				String numNotificacaoArquivo= "000000000";
				String numSeqNotificacaoArquivo="000";
				String numCodigoRetornoArquivo="00";
				String datEntrNotificacaoArquivo="00000000";
				if (
					(codEvento.equals("808")) ||
					(codEvento.equals("838")) ||
					(codEvento.equals("898")) 
				   ) {
					if (txtComplemento03.length()==0) {
						txtComplemento03=txtComplemento02;
						txtComplemento02="";
					}
					
					String numNotificacao=fFill(txtComplemento01.replaceAll("-","").trim(),12);
					numNotificacaoArquivo=numNotificacao.substring(0,9);
					numSeqNotificacaoArquivo=numNotificacao.substring(9,12);
					numCodigoRetornoArquivo=fFill(txtComplemento02.trim(),2);
					datEntrNotificacaoArquivo=formataDataYYYYMMDD(txtComplemento03.trim());
				}
				
				String ultDatDefesaArquivo = "00000000";
				String ultDatRecursoArquivo = "00000000"; 
				String ultDatEntregaCNHArquivo = "00000000"; 
				String datEventoArquivo = datProc;
				String horaEventoArquivo = horaEvento+minutoEvento+segundoEvento+milisegundoEvento;
				
				
				String dscMotivoArquivo = fPadBranco("",1000); dscMotivoArquivo=dscMotivoArquivo.substring(0,1000);
				String tipoJuntaArquivo = "0";
				String codJuntaArquivo = fFill("",6);
				String cpfRelatorArquivo = fFill("",11);
				String codPenalidadeArquivo = "000";
				String tipoRequerimentoArquivo = fPadBranco("",2);
				String numRequerimentoArquivo = fPadBranco("",30);
				String statusRequerimentoArquivo = fFill("",2); 
				String codResultadoArquivo = fPadBranco("",2);
				String numDiasSuspensaoArquivo = fFill("",5);
				
				
				String numDocumentoDOArquivo = fPadBranco("",20);
				String tipoAtaArquivo = fPadBranco("",1);
				String datGeracaoAtaArquivo = "00000000";
				String statusAtaArquivo = fPadBranco("",3);
				String numeroAtaArquivo = fPadBranco("",20);
				
				String datPublicacaoArquivo = "00000000";
				String datEstornoArquivo    = "00000000";
				String datDevolucaoCNHArquivo = "00000000";
				String autosInfracaoArquivo = fPadBranco("",3000);
				
				/*Abertura*/
				if (
						(codEvento.equals("810")) ||
						(codEvento.equals("840")) ||
						(codEvento.equals("870")) 
					   ) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="00"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);
					}
				}

				/*Relator*/
				if (
						(codEvento.equals("811")) ||
						(codEvento.equals("841")) ||
						(codEvento.equals("871")) 
					   ) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="02"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2); 						
						String codRelator=rs2.getString("COD_RELATOR_JUN");
						sCmd = "SELECT T1.COD_JUNTA,T1.NUM_CPF,T2.IND_TIPO FROM TPNT_RELATOR T1,TPNT_JUNTA T2 "+
						"WHERE COD_RELATOR='"+codRelator+"' AND T1.COD_JUNTA=T2.COD_JUNTA";
						rs2 = stmt04.executeQuery(sCmd);
						if (rs2.next()) {
							codJuntaArquivo = fFill(rs2.getString("COD_JUNTA"),6);
							cpfRelatorArquivo = fFill(rs2.getString("NUM_CPF"),11);
							tipoJuntaArquivo = fFill(rs2.getString("IND_TIPO"),1);
						}
					}
				}

				/*Resultado*/
				if (
						(codEvento.equals("812")) ||
						(codEvento.equals("842")) ||
						(codEvento.equals("872")) 
					   ) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="03"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						codResultadoArquivo = fPadBranco(rs2.getString("COD_RESULT_RES"),2);
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);

						//dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
					
						
						String codRelator=rs2.getString("COD_RELATOR_JUN");
						sCmd = "SELECT T1.COD_JUNTA,T1.NUM_CPF,T2.IND_TIPO FROM TPNT_RELATOR T1,TPNT_JUNTA T2 "+
						"WHERE COD_RELATOR='"+codRelator+"' AND T1.COD_JUNTA=T2.COD_JUNTA";
						rs2 = stmt04.executeQuery(sCmd);
						if (rs2.next()) {
							codJuntaArquivo = fFill(rs2.getString("COD_JUNTA"),6);
							cpfRelatorArquivo = fFill(rs2.getString("NUM_CPF"),11);
							tipoJuntaArquivo = fFill(rs2.getString("IND_TIPO"),1);
						}
						codPenalidadeArquivo=fFill(txtComplemento05.trim(),3);
						sCmd = "SELECT * FROM TPNT_PENALIDADE WHERE COD_PENALIDADE='"+codPenalidadeArquivo+"'";
						rs2 = stmt04.executeQuery(sCmd);
						if (rs2.next()) {
							int iDias=rs2.getInt("NUM_PRAZO")*30;
							numDiasSuspensaoArquivo = fFill(String.valueOf(iDias),5);
						}
					}
				}
					
					
					
				/*Processo F�sico*/
				if (codEvento.equals("815")) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="00"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);

						//dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
					}
				}
				
				/*Estorno de Resultado*/
				if (
						(codEvento.equals("817")) ||
						(codEvento.equals("847")) ||
						(codEvento.equals("877")) 
					   ) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="03"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						codResultadoArquivo = fPadBranco(rs2.getString("COD_RESULT_RES"),2);
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);

						//dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
					}
				}

				/*Estorno de Requerimento*/
				if (
						(codEvento.equals("819")) ||
						(codEvento.equals("849")) ||
						(codEvento.equals("879")) 
					   ) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					statusRequerimentoArquivo="03"; /*Status*/					
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);

						//dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
						
						/*
						dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
						dscMotivoArquivo = rs2.getString("TXT_MOTIVO_RES");
						if (dscMotivoArquivo!=null){							
							dscMotivoArquivo=fPadBranco(dscMotivoArquivo.trim(),1000);	
						}
						else
							dscMotivoArquivo=fPadBranco(dscMotivoArquivo,1000);
						*/	
					}
				}
				
				if (codEvento.equals("900")) {
					numRequerimentoArquivo=fPadBranco(txtComplemento01.trim(),30);
					sCmd = "SELECT * FROM TPNT_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+numRequerimentoArquivo.trim()+"'";
					rs2 = stmt04.executeQuery(sCmd);
					if (rs2.next()) {
						tipoRequerimentoArquivo = fPadBranco(rs2.getString("COD_TIPO_SOLIC"),2);
						dscMotivoArquivo = fTitaCharEsp(rs2.getString("TXT_MOTIVO_RES"));
						dscMotivoArquivo = fPadBranco(dscMotivoArquivo,1000);

						//dscMotivoArquivo = fPadBranco(rs2.getString("TXT_MOTIVO_RES"),1000);
					}
					datDevolucaoCNHArquivo = formataDataYYYYMMDD(txtComplemento05.trim());
				}
				
				/*Envio para o DO*/
				if (
						(codEvento.equals("803")) ||
						(codEvento.equals("833")) ||
						(codEvento.equals("893")) 
					   ) {
					if (codEvento.equals("803")) tipoAtaArquivo = "0";
					if (codEvento.equals("833")) tipoAtaArquivo = "2";
					if (codEvento.equals("893")) tipoAtaArquivo = "4";					
					datGeracaoAtaArquivo = formataDataYYYYMMDD(txtComplemento03.trim());
					statusAtaArquivo = fPadBranco("",3);
					numeroAtaArquivo = fPadBranco(txtComplemento02.trim(),20);
				}
				
				/*Publica��o DO*/
				if (
						(codEvento.equals("804")) ||
						(codEvento.equals("834")) ||
						(codEvento.equals("894")) 
					   ) {
					datPublicacaoArquivo = formataDataYYYYMMDD(txtComplemento03.trim());
				}
				
				/*Estorno Envio para o DO */
				if (
						(codEvento.equals("805")) ||
						(codEvento.equals("835")) ||
						(codEvento.equals("895")) 
					   ) {
					datEstornoArquivo = datProc;
				}
				
				/*Estorno Publica��o DO*/
				if (
						(codEvento.equals("806")) ||
						(codEvento.equals("836")) ||
						(codEvento.equals("896")) 
					   ) {
					datEstornoArquivo = datProc;
				}
				
				/*Instaura��o de Processo*/
				if (codEvento.equals("800")) {
					autosInfracaoArquivo="";
					sCmd = "SELECT * FROM TPNT_MULTA WHERE COD_PROCESSO='"+codProcesso+"' ORDER BY SEQ_MULTA";
					rs2 = stmt04.executeQuery(sCmd);
					while (rs2.next()) {
						String numAutoInfracao=fPadBranco(rs2.getString("NUM_AUTO_INFRACAO"),12);
						String seqMulta       =fFill(rs2.getString("SEQ_MULTA"),3);
						autosInfracaoArquivo+=numAutoInfracao+seqMulta;
					}
					autosInfracaoArquivo=fPadBranco(autosInfracaoArquivo,3000);
				}
					
				/*Instaura��o de Processo-Notifica��es*/
				if (
						(codEvento.equals("801")) ||
						(codEvento.equals("831")) ||
						(codEvento.equals("891")) 
					   ) {
					sCmd = "SELECT * FROM TPNT_NOTIFICACAO WHERE COD_PROCESSO='"+codProcesso+"' AND "+
					"DAT_PROC='"+dat_Status+"' ORDER BY NUM_NOTIFICACAO,SEQ_NOTIFICACAO";
					rs2 = stmt04.executeQuery(sCmd);
					while (rs2.next()) {
						numNotificacaoArquivo    = fFill(rs2.getString("NUM_NOTIFICACAO"),9);
						numSeqNotificacaoArquivo = fFill(rs2.getString("SEQ_NOTIFICACAO"),3);
						linhaArq="2"+codEventoArquivo+origemEventoArquivo+nomUsernameArquivo+orgaoLotacaoArquivo+
						tipoProcessoArquivo+formatoArquivo+numProcessoArquivo+codStatusArquivo+datStatusArquivo+
						totPontosArquivo+nomCondutorArquivo+numCPFCondutorArquivo+tipCNHCondutorArquivo+
						numCNHCondutorArquivo+datProcessoArquivo+horaProcessoArquivo+numNotificacaoArquivo+numSeqNotificacaoArquivo+
						numCodigoRetornoArquivo+datEntrNotificacaoArquivo+ultDatDefesaArquivo+
						ultDatRecursoArquivo+ultDatEntregaCNHArquivo+datEventoArquivo+horaEventoArquivo+
						dscMotivoArquivo+tipoJuntaArquivo+codJuntaArquivo+
						cpfRelatorArquivo+codPenalidadeArquivo+tipoRequerimentoArquivo+
						numRequerimentoArquivo+statusRequerimentoArquivo+codResultadoArquivo+numDiasSuspensaoArquivo+
						numDocumentoDOArquivo+tipoAtaArquivo+datGeracaoAtaArquivo+statusAtaArquivo+
						numeroAtaArquivo+datPublicacaoArquivo+datEstornoArquivo+datDevolucaoCNHArquivo+autosInfracaoArquivo;
						bw.write(linhaArq);
						bw.write("\n");
					}
				}
				else
				{
					linhaArq="2"+codEventoArquivo+origemEventoArquivo+nomUsernameArquivo+orgaoLotacaoArquivo+
					tipoProcessoArquivo+formatoArquivo+numProcessoArquivo+codStatusArquivo+datStatusArquivo+
					totPontosArquivo+nomCondutorArquivo+numCPFCondutorArquivo+tipCNHCondutorArquivo+
					numCNHCondutorArquivo+datProcessoArquivo+horaProcessoArquivo+numNotificacaoArquivo+numSeqNotificacaoArquivo+
					numCodigoRetornoArquivo+datEntrNotificacaoArquivo+ultDatDefesaArquivo+
					ultDatRecursoArquivo+ultDatEntregaCNHArquivo+datEventoArquivo+horaEventoArquivo+
					dscMotivoArquivo+tipoJuntaArquivo+codJuntaArquivo+
					cpfRelatorArquivo+codPenalidadeArquivo+tipoRequerimentoArquivo+
					numRequerimentoArquivo+statusRequerimentoArquivo+codResultadoArquivo+numDiasSuspensaoArquivo+
					numDocumentoDOArquivo+tipoAtaArquivo+datGeracaoAtaArquivo+statusAtaArquivo+
					numeroAtaArquivo+datPublicacaoArquivo+datEstornoArquivo+datDevolucaoCNHArquivo+autosInfracaoArquivo;
					
					bw.write(linhaArq);
					bw.write("\n");
				}
				
				minutoEvento=sys.Util.lPad(String.valueOf(Integer.parseInt(minutoEvento)+1),"0",2);
				segundoEvento=sys.Util.lPad(String.valueOf(Integer.parseInt(segundoEvento)+1),"0",2);
				
				int imilisegundo=Integer.parseInt(milisegundoEvento)+1;
				if (imilisegundo>9)
					imilisegundo=0;
				milisegundoEvento=String.valueOf(imilisegundo);
				
				
			//	System.out.println("EXPORTANDO HIST�RICOS > Hist�rico =>  " + codHistoricoProcesso);
				
				String numRequerimento = "";
				
				if(    codEvento.equals("810") || codEvento.equals("840") || codEvento.equals("870")
					|| codEvento.equals("811") || codEvento.equals("841") || codEvento.equals("871")
					|| codEvento.equals("812") || codEvento.equals("842") || codEvento.equals("872")
					|| codEvento.equals("817") || codEvento.equals("847") || codEvento.equals("877")
					|| codEvento.equals("819") || codEvento.equals("849") || codEvento.equals("879"))
				{
					numRequerimento = txtComplemento01.trim();
					/*exporta_REQUERIMENTO(bw, stmt05, stmt06, codProcesso, tipoParam, numRequerimento); N�O FAZER*/	
				}
				else if( codEvento.equals("813") || codEvento.equals("843") || codEvento.equals("873"))
				{
					numRequerimento = txtComplemento02.trim();
					/*exporta_REQUERIMENTO(bw, stmt05, stmt06, codProcesso, tipoParam, numRequerimento); N�O FAZER*/	
				}
				
				numRequerimento = "";
				if( codEvento.equals("808") || codEvento.equals("838") || codEvento.equals("898"))
					numRequerimento = txtComplemento01.trim();
				
				/*exporta_NOTIFICACAO(bw, stmt05, stmt06, codProcesso, tipoParam, codEvento, datStatus, numRequerimento); N�O FAZER*/	
				/*exporta_ATA_DO(bw, stmt05, stmt06, codProcesso, tipoParam, codEvento, datStatus);	 N�O FAZER*/
			}
			rs.close();
			
	//		System.out.println("---------------------------------------------------------------------------------------------");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_HISTORICO(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	
	private void exporta_NOTIFICACAO(BufferedWriter bw, Statement stmt05, Statement stmt06, String codProcesso, String tipoParam, String codEvento, String datStatus, String numNotificacaoParam) throws Exception, DaoException
	{
		try
		{
			String sWhere = "";
			
			if(codEvento.equals("800") || codEvento.equals("801") )
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '0' " +
						"	AND DAT_EMISSAO = '" + datStatus + "'";
			}
			else if(codEvento.equals("830") || codEvento.equals("831") )
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '2' " +
						"	AND DAT_EMISSAO = '" + datStatus + "'";
			}
			else if(codEvento.equals("890") || codEvento.equals("891") )
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '4' " +
						"	AND DAT_EMISSAO = '" + datStatus + "'";
			}
			else if(codEvento.equals("808"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '0' " +
						" 	AND lpad(NUM_NOTIFICACAO,9,'0')||'-'||lpad(SEQ_NOTIFICACAO,3,'0')='" + numNotificacaoParam + "'" + 
						"	AND DAT_PROC_RETORNO = '" + datStatus + "'";
			}
			else if(codEvento.equals("838"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '2' " +
						" 	AND lpad(NUM_NOTIFICACAO,9,'0')||'-'||lpad(SEQ_NOTIFICACAO,3,'0')='" + numNotificacaoParam + "'" +
						"	AND DAT_PROC_RETORNO = '" + datStatus + "'";
			}
			else if(codEvento.equals("898"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_NOTIFICACAO = '4' " +
						" 	AND lpad(NUM_NOTIFICACAO,9,'0')||'-'||lpad(SEQ_NOTIFICACAO,3,'0')='" + numNotificacaoParam + "'" +
						"	AND DAT_PROC_RETORNO = '" + datStatus + "'";
			}
			else
				return;
				
			
			String sCmd = "SELECT COD_NOTIFICACAO, NUM_NOTIFICACAO, TIP_NOTIFICACAO, SEQ_NOTIFICACAO, " +
				" NOM_USERNAME, COD_ORGAO_LOTACAO, COD_RETORNO, " +
				" NOM_USERNAME_RETORNO, COD_ORGAO_LOTACAO_RETORNO, IND_ENTREGUE, " +  
				" TO_CHAR(DAT_EMISSAO, 'dd/MM/yyyy') as DAT_EMISSAO2,  " +
				" TO_CHAR(DAT_PROC, 'dd/MM/yyyy') as DAT_PROC,  " +
				" TO_CHAR(DAT_PROC_RETORNO, 'dd/MM/yyyy') as DAT_PROC_RETORNO,  " +
				" TO_CHAR(DAT_RETORNO, 'dd/MM/yyyy') as DAT_RETORNO  " +
				" FROM TPNT_NOTIFICACAO " +	sWhere +
				"	ORDER BY DAT_EMISSAO, NUM_NOTIFICACAO, SEQ_NOTIFICACAO  ";
			
			ResultSet rs  = stmt05.executeQuery(sCmd);
			while(rs.next())
			{ 
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo7++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo7++;
				
				String linhaArq = "";
				
				String codRegistro = "7";
				linhaArq += codRegistro;
				
				String codNotificacao = rs.getString("COD_NOTIFICACAO");
				
				// ------------------------------------------------------------------------------------
				String numProcesso = "";
				sCmd = "SELECT NUM_PROCESSO as numProcesso" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				ResultSet rs2 = stmt06.executeQuery(sCmd);
				if(rs2.next())
					numProcesso = rs2.getString("numProcesso");
				if(numProcesso==null)
					numProcesso = "";
				linhaArq += fPadBranco(numProcesso,20);
				// ------------------------------------------------------------------------------------
				
				String numNotificacao = rs.getString("NUM_NOTIFICACAO");
				if(numNotificacao==null)
					numNotificacao = "";
				linhaArq += fFill(numNotificacao, 9);
				
				String seqNotificacao = rs.getString("SEQ_NOTIFICACAO");
				if(seqNotificacao==null)
					seqNotificacao = "";
				linhaArq += fFill(seqNotificacao, 3);
				
				String tipNotificacao = rs.getString("TIP_NOTIFICACAO");
				if(tipNotificacao==null)
					tipNotificacao = "";
				linhaArq += fPadBranco(tipNotificacao, 1);
				
				String datEmissao = rs.getString("DAT_EMISSAO2");
				if(datEmissao==null)
					datEmissao = "";
				linhaArq += formataDataYYYYMMDD(datEmissao);
				
				String username = rs.getString("NOM_USERNAME");
				if(username==null)
					username = "";
				linhaArq += fPadBranco(username, 20);
				
				String codOrgaoLotacao = rs.getString("COD_ORGAO_LOTACAO");
				if(codOrgaoLotacao==null)
					codOrgaoLotacao = "";
				linhaArq += fFill(codOrgaoLotacao, 6);
				
				String datProc = rs.getString("DAT_PROC");
				if(datProc==null)
					datProc = "";
				linhaArq += formataDataYYYYMMDD(datProc);
				
				String codRetorno = rs.getString("COD_RETORNO");
				if(codRetorno==null)
					codRetorno = "";
				linhaArq += fPadBranco(codRetorno, 3);
				
				String usernameRetorno = rs.getString("NOM_USERNAME_RETORNO");
				if(usernameRetorno==null)
					usernameRetorno = "";
				linhaArq += fPadBranco(usernameRetorno, 20);
				
				String codOrgaoLotacaoRetorno = rs.getString("COD_ORGAO_LOTACAO_RETORNO");
				if(codOrgaoLotacaoRetorno==null)
					codOrgaoLotacaoRetorno = "";
				linhaArq += fFill(codOrgaoLotacaoRetorno, 6);
				
				String datProcRetorno = rs.getString("DAT_PROC_RETORNO");
				if(datProcRetorno==null)
					datProcRetorno = "";
				linhaArq += formataDataYYYYMMDD(datProcRetorno);
				
				String indEntregue = rs.getString("IND_ENTREGUE");
				if(indEntregue==null)
					indEntregue = "";
				linhaArq += fPadBranco(indEntregue, 1);
				
				String datRetorno = rs.getString("DAT_RETORNO");
				if(datRetorno==null)
					datRetorno = "";
				linhaArq += formataDataYYYYMMDD(datRetorno);
				

				bw.write(linhaArq);
				bw.write("\n");
				System.out.println("EXPORTANDO NOTIFICA��ES > Notifica��o =>  " + codNotificacao);
			}
			rs.close();
			
	//		System.out.println("---------------------------------------------------------------------------------------------");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_NOTIFICACAO(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}
	
	
	private void exporta_ATA_DO(BufferedWriter bw, Statement stmt05, Statement stmt06, String codProcesso, String tipoParam, String codEvento, String datStatus) throws Exception, DaoException
	{
		try
		{
			String sWhere = "";
			
			if(codEvento.equals("803"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '0' " +
						"	AND DAT_EDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("833"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '2' " +
						"	AND DAT_EDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("893"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '4' " +
						"	AND DAT_EDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("804"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '0' " +
						"	AND DAT_PROC_PDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("834"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '2' " +
						"	AND DAT_PROC_PDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("894"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '4' " +
						"	AND DAT_PROC_PDO = '" + datStatus + "'";
			}
			else if(codEvento.equals("805"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '0' " +
						"	AND DAT_PROC_EST = '" + datStatus + "'";
			}
			else if(codEvento.equals("835"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '2' " +
						"	AND DAT_PROC_EST = '" + datStatus + "'";
			}
			else if(codEvento.equals("895"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '4' " +
						"	AND DAT_PROC_EST = '" + datStatus + "'";
			}
			else if(codEvento.equals("806"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '0' " +
						"	AND DAT_PROC_EST_PUBL_ATA = '" + datStatus + "'";
			}
			else if(codEvento.equals("836"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '2' " +
						"	AND DAT_PROC_EST_PUBL_ATA = '" + datStatus + "'";
			}
			else if(codEvento.equals("896"))
			{
				sWhere = "	WHERE COD_PROCESSO = '" + codProcesso + "'" +
						"   AND TIP_ATA = '4' " +
						"	AND DAT_PROC_EST_PUBL_ATA = '" + datStatus + "'";
			}
			else
				return;
			
			
			String sCmd = "SELECT COD_ATA_DO, TIP_ATA, " +
				" COD_ENVIO_DO,  " +
				" COD_ORGAO_LOTACAO_EDO, NOM_USERNAME_EDO, " +
				" COD_ORGAO_LOTACAO_PDO, NOM_USERNAME_PDO, " +
				" COD_ORGAO_LOTACAO_EST, NOM_USERNAME_EST, " +
				" COD_ORGAO_LOTACAO_EST_PUBL_ATA, NOM_USERNAME_EST_PUBL_ATA," +
				" TO_CHAR(DAT_EDO, 'dd/MM/yyyy') as DAT_EDO2,  " +
				" TO_CHAR(DAT_PROC_EDO, 'dd/MM/yyyy') as DAT_PROC_EDO,  " +
				" TO_CHAR(DAT_PUBL_ATA, 'dd/MM/yyyy') as DAT_PUBL_ATA,  " +
				" TO_CHAR(DAT_PROC_PDO, 'dd/MM/yyyy') as DAT_PROC_PDO,  " +
				" TO_CHAR(DAT_ESTORNO, 'dd/MM/yyyy') as DAT_ESTORNO,  " +
				" TO_CHAR(DAT_ESTORNO_PUBL_ATA, 'dd/MM/yyyy') as DAT_ESTORNO_PUBL_ATA  " +
				" FROM TPNT_ATA_DO " + sWhere + 
				"	ORDER BY DAT_EDO, COD_ENVIO_DO  ";
			
			ResultSet rs  = stmt05.executeQuery(sCmd);
			while(rs.next())
			{ 
				if(tipoParam.equalsIgnoreCase("P"))
					qtdTotal_TipoP_Tipo8++;
				else if(tipoParam.equalsIgnoreCase("S"))
					qtdTotal_TipoS_Tipo8++;
				
				String linhaArq = "";
				
				String codRegistro = "8";
				linhaArq += codRegistro;
				
				String codAtaDO = rs.getString("COD_ATA_DO");
				
				// ------------------------------------------------------------------------------------
				String numProcesso = "";
				sCmd = "SELECT NUM_PROCESSO as numProcesso" +
						" FROM TPNT_PROCESSO P" +
						" WHERE P.COD_PROCESSO = '" + codProcesso + "'";
				
				ResultSet rs2 = stmt06.executeQuery(sCmd);
				if(rs2.next())
					numProcesso = rs2.getString("numProcesso");
				if(numProcesso==null)
					numProcesso = "";
				linhaArq += fPadBranco(numProcesso,20);
				// ------------------------------------------------------------------------------------
				
				String tipAta = rs.getString("TIP_ATA");
				if(tipAta==null)
					tipAta = "";
				linhaArq += fPadBranco(tipAta, 1);
				
				String codEnvioDO = rs.getString("COD_ENVIO_DO");
				if(codEnvioDO==null)
					codEnvioDO = "";
				linhaArq += fPadBranco(codEnvioDO, 20);
				
				String datEDO = rs.getString("DAT_EDO2");
				if(datEDO==null)
					datEDO = "";
				linhaArq += formataDataYYYYMMDD(datEDO);
				
				String usernameEDO = rs.getString("NOM_USERNAME_EDO");
				if(usernameEDO==null)
					usernameEDO = "";
				linhaArq += fPadBranco(usernameEDO, 20);
				
				String codOrgaoLotacaoEDO = rs.getString("COD_ORGAO_LOTACAO_EDO");
				if(codOrgaoLotacaoEDO==null)
					codOrgaoLotacaoEDO = "";
				linhaArq += fFill(codOrgaoLotacaoEDO, 6);
				
				String datEstorno = rs.getString("DAT_ESTORNO");
				if(datEstorno==null)
					datEstorno = "";
				linhaArq += formataDataYYYYMMDD(datEstorno);
				
				String usernameEstorno = rs.getString("NOM_USERNAME_EST");
				if(usernameEstorno==null)
					usernameEstorno = "";
				linhaArq += fPadBranco(usernameEstorno, 20);
				
				String codOrgaoLotacaoEstorno = rs.getString("COD_ORGAO_LOTACAO_EST");
				if(codOrgaoLotacaoEstorno==null)
					codOrgaoLotacaoEstorno = "";
				linhaArq += fFill(codOrgaoLotacaoEstorno, 6);
				
				String datEstornoPubl = rs.getString("DAT_ESTORNO_PUBL_ATA");
				if(datEstornoPubl==null)
					datEstornoPubl = "";
				linhaArq += formataDataYYYYMMDD(datEstornoPubl);
				
				String usernameEstornoPubl = rs.getString("NOM_USERNAME_EST_PUBL_ATA");
				if(usernameEstornoPubl==null)
					usernameEstornoPubl = "";
				linhaArq += fPadBranco(usernameEstornoPubl, 20);
				
				String codOrgaoLotacaoEstornoPubl = rs.getString("COD_ORGAO_LOTACAO_EST_PUBL_ATA");
				if(codOrgaoLotacaoEstornoPubl==null)
					codOrgaoLotacaoEstornoPubl = "";
				linhaArq += fFill(codOrgaoLotacaoEstornoPubl, 6);
				
				String datProcEDO = rs.getString("DAT_PROC_EDO");
				if(datProcEDO==null)
					datProcEDO = "";
				linhaArq += formataDataYYYYMMDD(datProcEDO);
				
				String datPublAta = rs.getString("DAT_PUBL_ATA");
				if(datPublAta==null)
					datPublAta = "";
				linhaArq += formataDataYYYYMMDD(datPublAta);
				
				String usernamePDO = rs.getString("NOM_USERNAME_PDO");
				if(usernamePDO==null)
					usernamePDO = "";
				linhaArq += fPadBranco(usernamePDO, 20);
				
				String codOrgaoLotacaoPDO = rs.getString("COD_ORGAO_LOTACAO_PDO");
				if(codOrgaoLotacaoPDO==null)
					codOrgaoLotacaoPDO = "";
				linhaArq += fFill(codOrgaoLotacaoPDO, 6);
				
				String datProcPDO = rs.getString("DAT_PROC_PDO");
				if(datProcPDO==null)
					datProcPDO = "";
				linhaArq += formataDataYYYYMMDD(datProcPDO);
				
				bw.write(linhaArq);
				bw.write("\n");
				System.out.println("EXPORTANDO ATA�s DO > ATA DO =>  " + codAtaDO);
			}
			rs.close();
			
		//	System.out.println("---------------------------------------------------------------------------------------------");
		}
		catch(SQLException e)
		{	
			System.out.println("Erro na fun��o: exporta_ATA_DO(): " + e.getMessage());
			throw new Exception(e.getMessage());
		}
	}

	
}