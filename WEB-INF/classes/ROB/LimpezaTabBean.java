package ROB;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;

import sys.DaoException;

public class LimpezaTabBean extends sys.HtmlPopupBean{
    
    private String nomTabela; 
    private String campoFiltro; 
    private String valFiltro;
    private String nomTabelaPai;
    private String campoTabelaPai;
    private String campoTabelaPK;
    
    private LimpezaTabBean pai;
    private ArrayList filhos;
    
    public LimpezaTabBean()  throws sys.BeanException {
        nomTabela = "";
        campoFiltro = "";
        valFiltro = "";
        nomTabelaPai = "";
        campoTabelaPai = "";
        campoTabelaPK = "";
        
        pai = null;
        filhos = new ArrayList();
    }
    
    public void setNomTabela(String nomTabela){
        this.nomTabela = nomTabela;
        if (this.nomTabela == null) this.nomTabela = "";
    }
    public String getNomTabela(){
        return this.nomTabela;
    }
    
    public void setCampoFiltro(String CampoFiltro){
        this.campoFiltro = CampoFiltro;
        if (this.campoFiltro == null) this.campoFiltro = "";
    }
    public String getCampoFiltro(){
        return this.campoFiltro;
    }
    
    public void setNomTabelaPai(String nomTabelaaPai){
        this.nomTabelaPai = nomTabelaaPai;
        if(this.nomTabelaPai == null) this.nomTabelaPai = "";
    }
    public String getNomTabelaPai(){
        return this.nomTabelaPai;
    }
    
    public void setCampoTabelaPai(String campoTabelaPai){
        this.campoTabelaPai = campoTabelaPai;
        if(this.campoTabelaPai == null) this.campoTabelaPai = "";
    }
    public String getCampoTabelaPai(){
        return this.campoTabelaPai;
    }
    
    public void setCampoTabelaPK(String campoTabelaPK){
        this.campoTabelaPK = campoTabelaPK;
        if(this.campoTabelaPK == null) this.campoTabelaPK = "";
    }
    public String getCampoTabelaPK(){
        return this.campoTabelaPK;
    }
    
    public void setValFiltro(String valFiltro){
        this.valFiltro = valFiltro;
        if(this.valFiltro == null) this.valFiltro = "";
    }
    public String getValFiltro(){
        return this.valFiltro;
    }
    
    public void setPai(LimpezaTabBean pai){
        this.pai = pai;
    }
    public LimpezaTabBean getPai(){
        return this.pai;
    }     
    
    public void setFilhos(ArrayList filhos) {
        this.filhos = filhos;
    }
    public ArrayList getFilhos() {
        return this.filhos;
    }
    
    public String getCampos(Connection conn) throws DaoException, DaoException {
        return Dao.getInstance().ListaCamposLimpeza(conn, this);
    }
    
    public void acharFamilia(Connection conn)  throws DaoException {
        try {                   
            Dao.getInstance().ConsultaTabelasLimpeza(conn, this, this.filhos);
            Iterator itx = filhos.iterator();
            while (itx.hasNext()) {
                LimpezaTabBean filho = (LimpezaTabBean) itx.next();                         
                filho.acharFamilia(conn);
            }                       
        }
        catch (Exception e) {
            throw new DaoException(e.getMessage());
        }
    }      
    
    public void executarLimpeza(Connection conn, sys.MonitorLog monitor) throws RobotException {        
        try {            
            int qtdInc = 0;
            int qtdExc = 0;            
            monitor.gravaMonitor("Inicio da Limpeza da Tabela " + this.getNomTabela(), monitor.MSG_INICIO);
            System.out.println("Inicio da Limpeza da Tabela " + this.getNomTabela());            
            
            if (Dao.getInstance().VerificaIntegridadeLimpeza(conn, this)) {                
                this.acharFamilia(conn);
                qtdInc = Dao.getInstance().IncluiDadosLimpeza(conn, this);                    
                this.limparFamilia(conn);
                qtdExc = Dao.getInstance().ExcluiDadosLimpeza(conn, this);
                
            } else
                throw new RobotException("A estrutura da tabela " + this.getNomTabela() + 
                " est� diferente !");
            
            monitor.gravaMonitor("Termino da Limpeza da Tabela " + this.getNomTabela() +
                    " - Incluidos: " + qtdInc + " - Excluidos: " + qtdExc, monitor.MSG_FIM);
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }        
    }

    public void executarLimpezaNova(Connection conn, sys.MonitorLog monitor) throws RobotException {        
        try {            
            int qtdInc = 0;
            int qtdExc = 0;            
            monitor.gravaMonitor("Inicio da Limpeza da Tabela " + this.getNomTabela(), monitor.MSG_INICIO);
            System.out.println("Inicio da Limpeza da Tabela " + this.getNomTabela());            
            /*
            qtdInc = Dao.getInstance().IncluiDadosLimpezaNova(conn, this);
            */

            qtdInc = Dao.getInstance().IncluiDadosLimpezaLinha(conn, this);
            
            
            
            
            monitor.gravaMonitor("Termino da Limpeza da Tabela " + this.getNomTabela() +
                    " - Incluidos: " + qtdInc + " - Excluidos: " + qtdExc, monitor.MSG_FIM);
            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }        
    }
    
    
    public void limparFamilia(Connection conn) throws RobotException {
        try {
            Iterator itx = this.filhos.iterator();
            while (itx.hasNext()) {
                
                LimpezaTabBean filho = (LimpezaTabBean) itx.next();
                
                if (Dao.getInstance().VerificaIntegridadeLimpeza(conn, filho)) {
                    Dao.getInstance().IncluiDadosLimpeza(conn, filho);
                    filho.limparFamilia(conn);
                    Dao.getInstance().ExcluiDadosLimpeza(conn, filho);

                } else
                    throw new RobotException("A estrutura da tabela " + filho.getNomTabela() + 
                    " est� diferente !");
            }            
        } catch (Exception e) {
            throw new RobotException(e.getMessage());
        }
    }  
}
