package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import REG.ArquivoRecebidoBean;

import ACSS.OrgaoBean;

import sys.BeanException;
import sys.DaoException;
import sys.Util;

public class VerificaMarcaModeloDwRob {
    
    private static final String NOM_ROBOT 		= "VerificaMarcaModeloDwRob";
    private static final String quebraLinha 	= "\n";
    private static final String codIdentArquivo = "TVEP";
    sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
      
    public VerificaMarcaModeloDwRob() throws RobotException, ROB.DaoException, DaoException {
        
        Connection conn  = null;
        Statement trava = null;

        try {            			
            conn = Dao.getInstance().getConnection();
                        
            monitor.setConn(conn);
            monitor.setDestino(monitor.DES_BASE);
            monitor.setNomProcesso(NOM_ROBOT);
            
            // N�o permite que mais de uma instancia do robo rodem ao mesmo tempo 
            try {               
                trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
            } catch (Exception e) {
                monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
                return;
            }
            
            // Carrega Parametros 
            monitor.gravaMonitor("*** INICIO *** Robo: VerificaMarcaModeloDwRob", monitor.MSG_INICIO);
            System.out.println("*** INICIO *** Robo: VerificaMarcaModeloDwRob"); 
            
            ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
            param.setCodSistema("40"); // M�DULO REGISTRO 
            param.PreparaParam(conn);
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");
			
			ArrayList qtdDW = new ArrayList();
			ArrayList ListDestinatarios = new ArrayList();
			ArrayList qtdDwNaoEfetuado = new ArrayList();
			ArrayList qtdReg = new ArrayList();
			
			int tamMsgIni = 0;
			
			StringBuffer msg = new StringBuffer();
			String 	dataInicial = "01/01/2008";//param.getParamSist("DATA_INICIAL_MARCA_MODELO");
			
			String 	dataHoje = Util.getDataHoje().replace("-","/");
			String indPos = "";
			String indPosProx = "";			
		    String [] niveis = null;	
		  
			//verifica qtd de registro para cada �rg�o
			ACSS.OrgaoBean[] orgaos = Dao.getInstance().BuscarOrgaosIntegrados("0");
			for (int j = 0; j < orgaos.length; j++) 
			{
				if (orgaos[j].getCodOrgao().equals("258650")==false) continue;
				monitor.gravaMonitor("verificando qtd para o org�o >> "+orgaos[j].getSigOrgao()+" <<", monitor.MSG_INFO);
				System.out.println("verificando qtd para o org�o >> "+orgaos[j].getSigOrgao()+" <<"); 
				  String arquivos ="";
		//		  String orgaos =orgaos[j].getCodOrgao();
					//verifica destinat�rios 
					monitor.gravaMonitor("Verificando destinat�rios", monitor.MSG_INFO);
					System.out.println("Verificando destinat�rios"); 
					ListDestinatarios = ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"QTD_ARQ_MARCA_MODELO");
					
					//Prepara inicio da mensagem
					msg.append("Prezado(a) Senhor(a), "+quebraLinha+
					    "Em uma de nossas Verifica��es foram encontrados Arquivos de Marca/Modelo que n�o foram   " +
					    "baixados pelo seu �rg�o ");
					tamMsgIni = msg.length();

					qtdDW = new ArrayList();
					if (ListDestinatarios.size()>0)
					{
						//Verifica o valor dias de cada nivel
						monitor.gravaMonitor("Verificando o valor dias de cada nivel ", monitor.MSG_INFO);
						System.out.println("Verificando o valor dias de cada nivel "); 
						niveis = verificaValorsPorNivel(ListDestinatarios);
						
						//Verifica quantidade por nivel
						monitor.gravaMonitor("Verificando quantidade por nivel", monitor.MSG_INFO);
						System.out.println("Verificando quantidade por nivel"); 
						for(int z = 0; z<niveis.length; z++)
						{
							indPos = niveis[z];
							if(z == niveis.length-1)
							    indPosProx = niveis[z];
							else
								indPosProx = niveis[z+1];
							
							
							
							
							qtdDwNaoEfetuado = Dao.getInstance().verificaDownloadEfetuado(conn,orgaos[j],
									codIdentArquivo,dataInicial,dataHoje);								
							
							qtdReg = comparaDatas(orgaos[j],dataHoje,indPos,
									indPosProx, z ,niveis.length-1,qtdDwNaoEfetuado);
								
							for (int i = 0; i < qtdDwNaoEfetuado.size(); i++) {
								ArquivoRecebidoBean relClasse=new ArquivoRecebidoBean();
								relClasse = (ArquivoRecebidoBean) qtdDwNaoEfetuado.get(i);
								arquivos +=  relClasse.getNomArquivo()+",";
								
							}
							if(qtdDwNaoEfetuado.size()>0)
								arquivos=arquivos.substring(0,arquivos.length()-1);

							if(qtdReg.size()>0)
							{
							    //Acrescentando o restante da mensagem para cada n�vel
					  		    msg.append("Os arquivos s�o os seguintes:"+quebraLinha+arquivos+"."+
										quebraLinha+quebraLinha+"Atenciosamente,"+quebraLinha+"Auditoria SMIT");
								
								// Prepara Email 
					  		    monitor.gravaMonitor("Preparando Email ...", monitor.MSG_INFO);
					  		    System.out.println("Preparando Email ..."); 
						    	qtdDW = preparaEmail(ListDestinatarios,msg,(String)qtdReg.get(0),orgaos[j]);
							
					    		// Enviar Email 
						    	monitor.gravaMonitor("Enviando Email ...", monitor.MSG_INFO);
						    	System.out.println("Enviando Email ..."); 
								if (qtdDW.size()>0)
								{
						    		enviaEmail (qtdDW,emailBroker, (String)qtdReg.get(0));
								}
								
								//Deleta parte da msg que foi acrescentada acima
								msg.delete(tamMsgIni,msg.length());
							}
						}
					
					msg.delete(0,msg.length());
					qtdDwNaoEfetuado = new ArrayList();
				}
				monitor.gravaMonitor("*** FIM *** Robo: VerificaMarcaModeloDwRob", monitor.MSG_FIM);
				System.out.println("*** FIM *** Robo: VerificaMarcaModeloDwRob"); 
			}

	
        } catch (Exception e) {
            try {
                monitor.gravaMonitor("Erro ao Monitorar a quantidade de Processados nas guias: " + e.getMessage(), monitor.MSG_ERRO);
            } catch (sys.DaoException ed) {
                throw new RobotException(ed.getMessage());
            }
            throw new RobotException(e.getMessage());            
        } finally {
            if (conn != null) {
                try { 
                    Dao.getInstance().setReleaseConnection(conn);
                } catch (Exception e) {
                    throw new RobotException(e.getMessage());
                }
            }           
            if (trava != null) {
                Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
            }
        }
    }

    public static void main(String[] args) throws RobotException {
    	
    	try {
    		new VerificaMarcaModeloDwRob();			 
    	} catch (Exception e) { 
    		System.out.println(e); 
    	} 
    }
	private ArrayList preparaEmail(ArrayList ListDestinatarios, StringBuffer sConteudoEmail,
			String sValNivelProcesso, OrgaoBean myOrg) throws NumberFormatException, DaoException{
 
		ArrayList usuariosBloqueados = new ArrayList();

		
		// Prepara Email   
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{

				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals(sValNivelProcesso))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(nivelProcesso.getTipoMsg().equals("M")&& destinatario.getCodOrgao().equals(myOrg.getCodOrgao()))
						{
								destinatario.getMsg().add(sConteudoEmail.toString());
						 }
						 if(destinatario.getMsg().size()>0)
							lDestinatarios.add(destinatario);
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
				    usuariosBloqueados.add(nivelProcesso);
		     	}
			}
		}
        return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuarios, String emailBroker, String sValNivelProcesso) throws Exception{	
		if (usuarios.size()>0)
		{
			String sConteudo="";
			Iterator ity = null;
			Iterator it = usuarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals(sValNivelProcesso))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							sConteudo="";
							ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso());
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				     }
		     	}
			}
		}
	}
	
	private String[] verificaValorsPorNivel(ArrayList ListDestinatarios)
	{
		Vector valNivel = new Vector();
		Iterator it = ListDestinatarios.iterator();
		GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
		while (it.hasNext())
		{
			nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
			valNivel.add(nivelProcesso.getValNivelProcesso());
	    }
		
		String [] nivelDias = new String[valNivel.size()];
		valNivel.toArray(nivelDias);
		return nivelDias;
	}
	
	
	private static String formatedLastMonth() {

		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -30);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano;

	} // Fim formatedToday
	
	
	private ArrayList comparaDatas(ACSS.OrgaoBean Orgao, String dataHoje,
		String sDiasNivel, String sDiasNivelProx, int indPos, 
			int inPosUltima, ArrayList qtdDwNaoEfetuado) throws BeanException  {
		
		ArrayList qtdReg = new ArrayList();
		int qtdArqDW = 0;		
		
		Iterator it = qtdDwNaoEfetuado.iterator();
		ArquivoRecebidoBean arq  = new ArquivoRecebidoBean();		
		
		String registro = "";
		while (it.hasNext())
		{	arq   = (ArquivoRecebidoBean)it.next() ;	
			registro = (String)arq.getDatRecebimento();
			if( (indPos == inPosUltima) && (Util.DifereDias(registro,dataHoje)>=
				 Integer.parseInt(sDiasNivel)) )
			{
				qtdArqDW++;
			}
			else if( (Util.DifereDias(registro,dataHoje)>=
				 Integer.parseInt(sDiasNivel))&&
			    (Util.DifereDias(registro,dataHoje)< 
			      Integer.parseInt(sDiasNivelProx)) )
			{
				qtdArqDW++;
			}
		}
		if(qtdArqDW > 0)
		{
			qtdReg.add(sDiasNivel);
			qtdReg.add(qtdArqDW);
			qtdArqDW = 0;
		}
		return qtdReg;
	}

}