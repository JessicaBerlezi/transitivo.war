package ROB;

import javax.imageio.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import sys.DaoException;
import sys.Util;
import REG.ArquivoRecebidoBean;
import REG.LinhaArquivoRec;
import REG.NotifControleBean;

public class GeraCodigoBarraRob {
	
	private static final String NOM_ROBOT = "GeraCodigoBarraRob";
	
	public GeraCodigoBarraRob() throws RobotException, DaoException {
		
		Connection conn  = null;
		Connection connLog  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		
		try {            		
			String sDataInicio="01/01/2006";
			sDataInicio="01/12/2004";
			
			conn = Dao.getInstance().getConnection();
			REG.Dao dao = REG.Dao.getInstance();
			
			connLog = Dao.getInstance().getConnection();
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			Date dDatProc       = new Date();
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
			paramRec.setCodSistema("45"); //M�DULO RECURSO    
			paramRec.PreparaParam(conn);
			
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			String sPath="";
			
			NotifControleBean NotifControleID = new NotifControleBean();
			monitor.gravaMonitor("In�cio do Carregamento de Linhas para gera��o do C�digo de Barras.", monitor.MSG_INICIO);
			int h=70,flag=1;			
			REG.NotifControleBean linhaNotificacao = null;
			int iInicial=dao.PegarUltimoSeqGerado(datProc);
			for (int iSeq=iInicial;iSeq<600+iInicial;iSeq++)
			{
				// Carrega todas as linhas que est�o ajustadas prontas para envio.
				dao.CarregarLinhasGerarCB(NotifControleID,sDataInicio);
				if (NotifControleID.getListaNotifs().size()==0)
					break;
				Iterator it = NotifControleID.getListaNotifs().iterator();
				while (it.hasNext()) {
					linhaNotificacao = (REG.NotifControleBean) it.next();
					/*Gerar C�digo de Barras*/
					linhaNotificacao.setDatRegistroCB(dDatProc);	
					linhaNotificacao.setSeqRegistroCB(Integer.toString(iSeq));
					sPath=linhaNotificacao.getArquivo(paramRec,iSeq);
					try
					{
					drawingBarcodeDirectToGraphics(sPath,linhaNotificacao.getNumNotificacao(),h,flag);
					/*Atualizando Data Geracao*/
					dao.atualizaCodigoBarra(linhaNotificacao);					
					}
					catch (Exception e) {
						monitor.gravaMonitor("Problemas na gera��o do C�digo de Barras. "+linhaNotificacao.getNumAutoInfracao()+" "+linhaNotificacao.getNumNotificacao(), monitor.MSG_INICIO);						
					}

				}
			}	
				monitor.gravaMonitor("T�rmino da gera��o do C�digo de Barras ", monitor.MSG_FIM);
		} 
		catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro na gera��o do C�digo de Barras: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (Exception ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try { 
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}			
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public boolean drawingBarcodeDirectToGraphics(String sPath,String notificacao,int h,int flag)
	throws BarcodeException,ServletException,IOException {
		try
		{
			Barcode barcode = BarcodeFactory.createCode128B(notificacao);
			barcode.setBarHeight((double)h);
			boolean bFlag=true;
			if (flag==0)
				bFlag=false;		
			barcode.setDrawingText(bFlag);
			BufferedImage image = getImage(barcode);
			File dir = new File(sPath);
			dir.mkdirs();    			
			ImageIO.write(image, "jpg", dir);			
		}
		catch (Exception e) 
		{
		}    	
		return true;
	}    

	public BufferedImage getImage(Barcode barcode) {
		
		BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bi.createGraphics();
		barcode.draw(g, 0, 0);
		bi.flush();
		return bi;
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new GeraCodigoBarraRob();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	
}
