package ROB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import REC.CodigoRetornoBean;
import REC.ResponsavelBean;

import sys.BeanException;
import sys.Util;

public class DaoBroker {
	
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "ROB";
	private static DaoBroker instance;
	
	public static DaoBroker getInstance()
	throws sys.DaoException {
		if (instance == null)
			instance = new DaoBroker();
		return instance;
	}
	
	public DaoBroker() throws sys.DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	/**************************************
	 * 
	 * @param codOrgao C�digo do �rg�o do Arquivo
	 * @param indEmisDetran Indicador de Emiss�o de Notifica��o pelo DETRAN
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgaoLotacao �rg�o de lota��o do usu�rio que enviou o arquivo
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 */
	public Vector buscarTransPendente(Connection conn) throws DaoException {		
		try {
			Vector vetRet = new Vector();
			Statement stmt = conn.createStatement();
			String sCmd =
				"select cod_brkr_trans_pendente, cod_evento, txt_parte_variavel, dat_proc from TSMI_BRKR_TRANS_PENDENTE";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				TransacaoPendenteBean tp = new TransacaoPendenteBean();
				tp.setCodBrkrTransPendente(
						rs.getString("cod_brkr_trans_pendente"));
				tp.setCodEvento(rs.getString("cod_evento"));
				tp.setTxtParteVariavel(rs.getString("txt_parte_variavel"));
				tp.setDatProc(rs.getString("dat_proc"));
				vetRet.add(tp);
			}
			rs.close();
			stmt.close();
			stmt = null;
			return vetRet;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	
	/**************************************
	 * 
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgaoLotacao �rg�o de lota��o do usu�rio que enviou o arquivo
	 * @param codOrgao C�digo do �rg�o do Arquivo
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 */
	public String TransacaoIsoladas(String nomUsername,	String codOrgaoLotacao,	String numTrans,
			String tipArq, String codOrgao, String linhaArq, Connection conn,BRK.TransacaoBRKR transBRKDOL,String codIdentArquivo, String indP59) throws DaoException {
		String resultado = "";
		try {				
			//Cr�tica do �rg�o de lota��o
			if (codOrgaoLotacao.trim().equals(null)) 
				resultado =	"ERR-DaoBroker.TransacaoIsoladas: �rg�o de lota��o n�o preenchido";
			else {
				//Cr�tica do �rg�o
				if (codOrgao.trim().equals(null)) 
					resultado ="ERR-DaoBroker.TransacaoIsoladas: �rg�o n�o preenchido";
				else {
					BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
					
					String parteFunc = "";
					if ("6,8".indexOf(tipArq) >= 0) //MA01 e MF01
						parteFunc = linhaArq.substring(0, 19)
						+ sys.Util.lPad("", "0", 10)
						+ linhaArq.substring(26, 54) 
						+ " " 
						+ linhaArq.substring(54, 55);
					
					else if (tipArq.equals("4")) //MZ01

						if (indP59.equals("P59")){
							parteFunc = linhaArq.substring(0, 19)
							+ sys.Util.lPad("", "0", 10)
							+ linhaArq.substring(19, 47) 
							+ sys.Util.rPad("", " ", 258)
							+ linhaArq.substring(234, 373);
						}else{
							parteFunc = linhaArq.substring(0, 19)
							+ sys.Util.lPad("", "0", 10)
							+ linhaArq.substring(19, 47) 
							+ sys.Util.rPad("", " ", 165)
							+ linhaArq.substring(234, 356);
						}
					else if (tipArq.equals("3")) //MX01
						parteFunc = linhaArq.substring(0, 19)
						+ sys.Util.lPad("", "0", 10)
						+ linhaArq.substring(19, 47);
					
					else //MR01, ME01 e MC01
					{
						parteFunc = linhaArq;
					}
					
					
					String parteVar = sys.Util.rPad(nomUsername, " ", 20)
					+ sys.Util.lPad(codOrgaoLotacao, "0", 6)
					+ tipArq
					+ sys.Util.lPad(codOrgao, "0", 6);
					if(codIdentArquivo.equals("PE01"))
					{
						parteVar += formatarIsoladasPE01(tipArq, sys.Util.rPad(parteFunc, " ", 800),codIdentArquivo,codOrgao);
					}else{
						
						if (indP59.equals("P59")){
							  parteFunc = Util.rPad(linhaArq," ",513).substring(0,513);							
							  numTrans  = String.valueOf(Integer.parseInt(numTrans) + 50); // soma 50 a transa��o;
							  /*parteVar += formatarIsoladasP59(tipArq, sys.Util.rPad(parteFunc, " ", 800),codIdentArquivo,codOrgao);*/
							  if (codIdentArquivo.equals("MR01")){
								  parteVar += formatarIsoladasP59(tipArq, sys.Util.rPad(parteFunc, " ", 745),codIdentArquivo,codOrgao);
							  }else{
								  parteVar += formatarIsoladasP59(tipArq, sys.Util.rPad(parteFunc, " ", 682),codIdentArquivo,codOrgao);
							  }
						}else
						      parteVar += formatarIsoladas(tipArq, sys.Util.rPad(parteFunc, " ", 800),codIdentArquivo,codOrgao);
					}
					transBRK.setParteFixa(numTrans);
					transBRK.setParteVariavel(parteVar);
				    transBRK.setCodOrgaoAtuacao(codOrgao);
					transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
					transBRK.setNomUserName(nomUsername);
					resultado = transBRK.getResultado(conn);
					
					
					/*Gravar Dados no TSMI_LOG_BROKER*/					
					String codRetorno = resultado.substring(0,3);
					transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
					transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
					transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
					transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
				}
			}
		} catch (sys.DaoException e) {
			throw new DaoException(e.getMessage());
		}
		return resultado;
	}
		
	/**************************************
	 * 
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgaoLotacao �rg�o de lota��o do usu�rio que enviou o arquivo
	 * @param codOrgao C�digo do �rg�o do Arquivo
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 */
	
	public String TransacaoJari(String nomUsername,String codOrgaoLotacao,String codOrgao,String
			nomResponsavel,String linhaArq,Connection conn,BRK.TransacaoBRKR transBRKDOL, String indP59) throws DaoException {			
		String resultado = "";
		String numTrans = "403";

		try {			
			//Cr�tica do �rg�o de lota��o
			if (codOrgaoLotacao.trim().equals(null)) 
				resultado =	"ERR-DaoBroker.TransacaoJari: �rg�o de lota��o n�o preenchido";
			else {		
				//Cr�tica do �rg�o
				if (codOrgao.trim().equals(null)) 
					resultado = "ERR-DaoBroker.TransacaoJari: �rg�o n�o preenchido";
				else {
					String parteVar =sys.Util.rPad(nomUsername, " ", 20)
					+ sys.Util.lPad(codOrgaoLotacao,"0",6)+ sys.Util.lPad(codOrgao,"0",6)
					+ sys.Util.rPad(nomResponsavel," ",8)+ linhaArq.substring(33, 35)
					+ linhaArq.substring(1, 33)	+ linhaArq.substring(35);
					BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
					
					if (indP59.equals("P59"))
						  numTrans =  String.valueOf(Integer.parseInt(numTrans) + 50); // soma 50 a transa��o;	

					transBRK.setParteFixa(numTrans);
					transBRK.setParteVariavel(parteVar);
				    transBRK.setCodOrgaoAtuacao(codOrgao);
					transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
					transBRK.setNomUserName(nomUsername);
					resultado = transBRK.getResultado(conn);

					/*Gravar Dados no TSMI_LOG_BROKER*/					
					String codRetorno = resultado.substring(0,3);					
					transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
					transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
					transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
					transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
				}
			}
		} catch (sys.DaoException e) {
			throw new DaoException(e.getMessage());
		}
		return resultado;
	}
	
	
		
	/**************************************
	 * 
	 * @param codEvento C�digo da Transa��o que est� pendente
	 * @param parteVar Parte Vari�vel da transa��o que est� pendente
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 */
	public String TransPendente(String codEvento, String parteVar, Connection conn)
	throws DaoException {
		String resultado = "";
		try 
		{
			BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
			transBRK.setParteFixa(codEvento);
			transBRK.setParteVariavel(parteVar);
			resultado = transBRK.getResultado(conn);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return resultado;
	}
	
	/**************************************
	 * 
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgaoLotacao �rg�o de lota��o do usu�rio que enviou o arquivo
	 * @param codOrgao C�digo do �rg�o do Arquivo
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 * @throws BeanException 
	 */	
	public String TransacaoDOL(String nomUsername, String codOrgaoLotacao, String tipArq,
			String codOrgao, String numLote, String anoLote, String datLote, String codUnidade,
			String linhaArq, Connection conn,BRK.TransacaoBRKR transBRKDOL, String indP59,REG.LinhaLoteDolBean linhaLote) throws DaoException, BeanException  {
		
		String resultado = "";
		String numTrans = "408";
		//Cr�tica do �rg�o de lota��o
		if (codOrgaoLotacao.trim().equals(null)) 
			resultado =	"ERR-DaoBroker.TransacaoIsoladas: �rg�o de lota��o n�o preenchido";
		else {
			//Cr�tica do �rg�o
			if (codOrgao.trim().equals(null)) 
				resultado ="ERR-DaoBroker.TransacaoIsoladas: �rg�o n�o preenchido";
			else {
				String linha = linhaArq.substring(33); 
				
				String vago=sys.Util.rPad("0","0", 17); /*Email Ricardo-02/07/2007 (PMRJ)*/

				if (indP59.equals("P59")){
					  linha = atualizaMunicipio(linha);
					  numTrans =  String.valueOf(Integer.parseInt(numTrans) + 50); // soma 50 a transa��o;	
					  linha = formatarIsoladasP59(tipArq, sys.Util.rPad(linha, " ", 682),"",codOrgao);
				}else{
					linha = linha + vago;
				}
				
				StringBuffer linhaCamposRenainf = new StringBuffer(sys.Util.rPad(linha," ",713));
				
									
				// ALTERA��O CAMPOS NOVOS RENAINF 15/05/2012 @author jefferson.trindade
				// Adicionar os campos novos
				if ("".equals(linhaCamposRenainf.substring(682,683).trim())) //TIPO CNH CONDUTOR DOC COND - NAHB - CAMPO NOVO
					linhaCamposRenainf.replace(682,683,"0");
				
				if ("".equals(linhaCamposRenainf.substring(683,684).trim())) //TIPO DOC COND - NAHB - CAMPO NOVO
					linhaCamposRenainf.replace(683,684,"0");
									
				if ("".equals(linhaCamposRenainf.substring(684,698).trim())) //NUM DOC COND - NAHB - CAMPO NOVO
					linhaCamposRenainf.replace(684,698,"              ");
				
				if ("".equals(linhaCamposRenainf.substring(698,699).trim())) //TIPO DOC EMB - TRANSP - CAMPO NOVO
					linhaCamposRenainf.replace(698,699,"0");
				
				if ("".equals(linhaCamposRenainf.substring(699,713).trim())) //NUM DOC EMB - TRANSP - CAMPO NOVO
					linhaCamposRenainf.replace(699,713,"              ");
				
				// Atribuo a vari�vel apenas os campos novos, para adicionar na parte vari�vel
				String linhaCamposRenainfFormatados = linhaCamposRenainf.substring(682, 713);
				
				
				
				String operador = linhaArq.substring(0, 10);
				
				
				/*
				BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);				
				String parteVar = sys.Util.rPad(nomUsername, " ", 20)
				+ sys.Util.lPad(codOrgaoLotacao,"0",6) + tipArq
				+ sys.Util.lPad(codOrgao,"0",6)	+ linha 
				+ sys.Util.lPad(anoLote, "0", 4) + sys.Util.lPad(numLote, "0", 5)
				+ sys.Util.lPad(codUnidade, "0", 4) + sys.Util.lPad(datLote, "0", 8)
				+ operador + linhaCamposRenainfFormatados;
				transBRK.setParteFixa(numTrans);
				transBRK.setParteVariavel(parteVar);
			    transBRK.setCodOrgaoAtuacao(codOrgao);
				transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
				transBRK.setNomUserName(nomUsername);
				resultado = transBRK.getResultado(conn);
				String codRetorno = resultado.substring(0,3);					
				transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
				transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
				transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
				transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
				*/
			
				REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
				myAuto.setCodOrgao(linhaLote.getCodOrgao());
				myAuto.setNumPlaca(linhaLote.getNumPlaca());
				myAuto.getInfracao().setCodInfracao(linhaLote.getCodInfracao());
				myAuto.getInfracao().setValReal(linhaLote.getValorMulta());
				myAuto.setNumAutoInfracao(linhaLote.getNumAuto());
				myAuto.setDatInfracao(linhaLote.getDataInfracao());
				myAuto.setCodMunicipio(linhaLote.getCodMunicipio());
				myAuto.setValHorInfracao(linhaLote.getHoraInfracao());
				myAuto.setDscLocalInfracao(linhaLote.getLocalInfracao());
				myAuto.setDatVencimento(linhaLote.getDataVencimento());
				myAuto.setValVelocAferida(linhaLote.getVelocidadeAferida());
				myAuto.setValVelocConsiderada(linhaLote.getVelocidadeConsiderada());
				myAuto.setValVelocPermitida(linhaLote.getVelocidadePermitida());
				
				ResponsavelBean condutor = new ResponsavelBean();
				condutor.setNomResponsavel(linhaLote.getNomeCondutor());
				condutor.setNumCpfCnpj(linhaLote.getNumCpfCondutor());
				condutor.setNumCnh(linhaLote.getNumCnhCondutor());
				condutor.setCodUfCnh(linhaLote.getUFCnhCondutor());
				sys.EnderecoBean endereco = new sys.EnderecoBean();
				endereco.setTxtEndereco(linhaLote.getEnderecoCondutor());
				endereco.setNumEndereco(linhaLote.getNumEnderecoCondutor());
				endereco.setSeqEndereco(endereco.getNumEndereco());
				endereco.setTxtComplemento(linhaLote.getComplementoEnderecoCondutor());
				endereco.setNumCEP(linhaLote.getCepEndereco());
				condutor.setEndereco(endereco);
				myAuto.GravaAutoInfracaoLocal();
			}   
		}
		return resultado;
	}

	public String atualizaMunicipio(String linha) throws DaoException {
		Connection conn = null;
		String cod_munic = "";
		int icod_munic = 0;
		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);

			linha = "1" + linha;
			StringBuffer linhaTemp = new StringBuffer(linha);
			
			cod_munic = linhaTemp.substring(119,124).trim();
//			if (cod_munic==""); cod_munic="0";// tratamento para evitar erros
			icod_munic = Integer.parseInt(cod_munic);
			
			// coloca zero a esquerda de 4 posi��es
			cod_munic = Util.lPad(String.valueOf(icod_munic),"0",4);
			// coloca zero a direita de 5 posi��es
			cod_munic = Util.rPad(cod_munic,"0",5);
			
			linhaTemp.replace(119,124,cod_munic);

			//Statement stmt = conn.createStatement();
			
			/*
			 * Executa uma consulta na base do SMIT, para verificar se a notifica��o sofreu
			 * controle no envio do arquivo.
			 */ 
			//String sCmd = "SELECT CODMUNICIPIO FROM TSMI_MUNICIPIO"+
			//" WHERE COD_MUNICIPIO = "+icod_munic;
			
			//ResultSet rs = stmt.executeQuery(sCmd);
			//if(rs.next()){
			//	cod_munic = sys.Util.lPad(rs.getString("CODMUNICIPIO"), "0", 5);
			//	linhaTemp.replace(119,124,cod_munic);
 			//}


			cod_munic = linhaTemp.substring(448,453).trim(); 
//			if (cod_munic==""); cod_munic="0";// tratamento para evitar erros
			icod_munic = Integer.parseInt(cod_munic);

			// coloca zero a esquerda de 4 posi��es
			cod_munic = Util.lPad(String.valueOf(icod_munic),"0",4);
			// coloca zero a direita de 5 posi��es
			cod_munic = Util.rPad(cod_munic,"0",5);
			
			linhaTemp.replace(449,454,cod_munic);

			return linhaTemp.toString().substring(1);
			
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}  
		finally {
			if(conn!= null){
				try { 
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 * M�todo de chamada abrindo uma conex�o para o implementa��o de TransacaoRetorno().
	 * @param nomUsername
	 * @param codOrgao
	 * @param linhaArq
	 * @author Sergio Roberto Junior
	 * @since 21/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public String TransacaoRetorno(String nomUsername, String codOrgao, String codOrgaoLotacao, REG.LinhaArquivoRec linhaArq) throws DaoException {
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();
			String resultado = TransacaoRetorno(nomUsername, codOrgao, codOrgaoLotacao, linhaArq,conn,transBRKDOL,Util.formatedToday().substring(0,10));
			conn.commit();
			return resultado;
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}  
		finally {
			if(conn!= null){
				try { 
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
		
	/**************************************
	 * 
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgao �rg�o do usu�rio que enviou o arquivo
	 * @param linhaArq Linha contendo as informa��es do c�digo de retorno
	 * @return Retorna as 3 posi��es de Retorno do Broker concatenadas em um String
	 * @author Sergio Roberto Junior
	 * @since 28/01/2005
	 * @throws sys.DaoException
	 * @version 1.0
	 */  
	 public String TransacaoRetorno(String nomUsername, String codOrgao, String codOrgaoLotacao, REG.LinhaArquivoRec linhaArq, Connection conn,BRK.TransacaoBRKR transBRKDOL,String datProc) throws sys.DaoException {
		String resultado = "";    	
		String linha = sys.Util.rPad(linhaArq.getDscLinhaArqRec()," ",122);
		try{
			BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
			// Buscar dados da transa��o		 
			String parteVar = sys.Util.rPad(nomUsername, " ", 20) + sys.Util.lPad(codOrgao, "0", 6) + linha.substring(0,122);
			transBRK.setParteFixa("406");	
			transBRK.setParteVariavel(parteVar);
		    transBRK.setCodOrgaoAtuacao(codOrgao);
			transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
			transBRK.setNomUserName(nomUsername);
			resultado = transBRK.getResultado(conn);
			
			
			/*Gravar Dados no TSMI_LOG_BROKER*/					
			String codRetorno = resultado.substring(0,3);					
			transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
			transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
			transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
			transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
			
			if("000,632,633,634".indexOf(resultado.substring(0,3))>=0){
				linhaArq.setCodStatusCorrecao("9");
				REG.Dao dao = REG.Dao.getInstance();
				REG.NotifControleBean notificacao = new REG.NotifControleBean();
				notificacao.setCodLinhaArquivoRetorno(linhaArq.getCodLinhaArqReq());
				List notificacoes = dao.getNotificacoes(notificacao, conn);
				if(notificacoes.size()>0){
					notificacao = (REG.NotifControleBean)notificacoes.get(0);
					if( (notificacao.getNomUsuarioRetorno()!=null) && !(notificacao.getNomUsuarioRetorno().equals("")) )
						dao.atualizaRetornoOrig(notificacao, conn);
					atualizaContrNotif(nomUsername, codOrgao, linhaArq, conn, datProc);
				}
				else
					atualizaContrNotif(nomUsername, codOrgao, linhaArq, conn, datProc);				
			}
			else{
				linhaArq.setCodStatusCorrecao("4");      	
				REG.Dao dao = REG.Dao.getInstance();
				REG.NotifControleBean notifControle = new REG.NotifControleBean();
				notifControle.setCodLinhaArquivoRetorno(linhaArq.getCodLinhaArqReq());
				List notificacoes = dao.getNotificacoes(notifControle, conn);
				if(notificacoes.size()>0){
					notifControle = (REG.NotifControleBean)notificacoes.get(0);
					if( (notifControle.getNomUsuarioRetorno()!=null) && !(notifControle.getNomUsuarioRetorno().equals("")) )
						dao.atualizaRetornoOrig(notifControle, conn);
					atualizaContrNotif(nomUsername, codOrgao, linhaArq, conn, datProc);
				}
				else
					atualizaContrNotif(nomUsername, codOrgao, linhaArq, conn, datProc);	
			}
			
		}
		catch(Exception e){
			throw new sys.DaoException(e.getMessage());
		}
		return resultado;
	}
	
	
	/**************************************
	 * 
	 * @param nomUsername Login do Usu�rio que enviou o arquivo
	 * @param codOrgao �rg�o do usu�rio que enviou o arquivo
	 * @param linhaArq Linha contendo as informa��es do c�digo de retorno
	 * @return Retorna as 3 posi��es de Retorno do Broker concatenadas em um String
	 */  
	 public String TransacaoRetorno(String nomUsername, String codOrgao, String codOrgaoLotacao, String linhaArq, Connection conn, BRK.TransacaoBRKR transBRKDOL) 
		throws sys.DaoException {
		
			if (linhaArq.length() > 122) linhaArq = linhaArq.substring(0, 122);
		
			BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
			// Buscar dados da transa��o		 
			String parteVar = sys.Util.rPad(nomUsername, " ", 20) + sys.Util.lPad(codOrgao, "0", 6) + sys.Util.rPad(linhaArq, " ", 122);
			transBRK.setParteFixa("406");	
			transBRK.setParteVariavel(parteVar);
		    transBRK.setCodOrgaoAtuacao(codOrgao);
			transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
			transBRK.setNomUserName(nomUsername);
			String resultado = transBRK.getResultado(conn);
		
			/*Gravar Dados no TSMI_LOG_BROKER*/					
			String codRetorno = resultado.substring(0,3);					
			transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
			transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
			transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
			transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
			
			return resultado;
		}    
	
	/**************************************
	 * 
	 * @param codOrgao C�digo do �rg�o
	 * @param codStatus C�digo do Status
	 * @param datInicial Data de Infra��o Inicial
	 * @param datFinal Data de Infra��o Final
	 * @param conn Data de Infra��o Final   
	 * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	 */
	public String TransacaoSaldoOrgao(String codOrgao,String codStatus, Date datInicial, Date datFinal,
			Connection conn) throws DaoException {			
		String resultado = "";
		try {
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
			String datInicialTxt = df.format(datInicial);
			String datFinalTxt = df.format(datFinal);
			
			String parteVar = sys.Util.lPad(codOrgao,"0",6);
			parteVar += sys.Util.lPad(codStatus,"0",3);
			parteVar += sys.Util.lPad(datInicialTxt,"0",8);
			parteVar += sys.Util.lPad(datFinalTxt,"0",8);
			
			BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
			transBRK.setParteFixa("042");
			transBRK.setParteVariavel(parteVar);
			resultado = transBRK.getResultado(conn);
			
		} catch (sys.DaoException e) {
			throw new DaoException(e.getMessage());
		}
		return resultado;
	}	
	
	//Fun��es Utilit�rias
	
	public String buscarErroBroker(Connection conn, String codErro) throws sys.DaoException {
		try {
			String dscErro = "";
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+codErro+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())dscErro = rs.getString("dsc_erro");
			rs.close();
			stmt.close();
			return dscErro;
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}  
	}
	
	public String formatarIsoladas(String tipArquivo, String linha, String codIdentArquivo, String codOrgao) {
		
		StringBuffer linhaTemp = new StringBuffer(linha);
		
		if (("345".indexOf(tipArquivo) >= 0) && ("".equals(linhaTemp.substring(26,29).trim()))) //Motivo da invalida��o
			linhaTemp.replace(26,29,"000");
		
		if ("".equals(linhaTemp.substring(71,79).trim())) //Data da Infra��o
			linhaTemp.replace(71,79,"00000000");
		
		if ("".equals(linhaTemp.substring(79,83).trim())) //Hora da Infra��o
			linhaTemp.replace(79,83,"0000");
		
		if ("".equals(linhaTemp.substring(83,87).trim())) //Codigo do municipio
			linhaTemp.replace(83,87,"0000");
		
		if ("".equals(linhaTemp.substring(87,91).trim())) //Codigo da Infra��o
			linhaTemp.replace(87,91,"0000");
		
		if ("".equals(linhaTemp.substring(91,98).trim())) //Valora da Multa
			linhaTemp.replace(91,98,"0000000");
		
		if ("".equals(linhaTemp.substring(98,106).trim())) //Data do Vencimento
			linhaTemp.replace(98,106,"00000000");
		
		if ("".equals(linhaTemp.substring(106,108).trim())) //Codigo do Agente
			linhaTemp.replace(106,108,"00");
		
		if ("".equals(linhaTemp.substring(118,121).trim())) //Velocidade Permitida
			linhaTemp.replace(118,121,"000");
		
		if ("".equals(linhaTemp.substring(121,125).trim())) //Velocidade Aferida
			linhaTemp.replace(121,125,"0000");
		
		if ("".equals(linhaTemp.substring(125,129).trim())) //Velocidade Considerada
			linhaTemp.replace(125,129,"0000");
		
		if ("".equals(linhaTemp.substring(154,162).trim())) //Data da afericao
			linhaTemp.replace(154,162,"00000000");
		
		if ("".equals(linhaTemp.substring(212,214).trim())) //Especie do Veiculo
			linhaTemp.replace(212,214,"00");
		
		if ("".equals(linhaTemp.substring(214,216).trim())) //Categoria do Veiculo
			linhaTemp.replace(214,216,"00");
		
		if ("".equals(linhaTemp.substring(216,219).trim())) //Tipo do Veiculo
			linhaTemp.replace(216,219,"000");
		
		if ("".equals(linhaTemp.substring(219,222).trim())) //Cor do Veiculo
			linhaTemp.replace(219,222,"000");
		
		if ("".equals(linhaTemp.substring(267,278).trim())) //CPF do Condutor
			linhaTemp.replace(267,278,"00000000000");
		
		if ("".equals(linhaTemp.substring(278,289).trim())) //CNH do Condutor
			linhaTemp.replace(278,289,"00000000000");
		
		if ("".equals(linhaTemp.substring(332,340).trim())) //CEP do Ende�o
			linhaTemp.replace(332,340,"00000000");
		
		if ("".equals(linhaTemp.substring(340,344).trim())) //Municipio do Endere�o
			linhaTemp.replace(340,344,"0000");		

		String vago=sys.Util.rPad("0","0", 17); /*Email Ricardo-02/07/2007 (PMRJ)*/
		if  (codIdentArquivo.equals("MR01"))  
		  return linhaTemp.toString().substring(0,404)+vago;
		
		
		
		return linhaTemp.toString();
	}

	public String formatarIsoladasP59(String tipArquivo, String linha, String codIdentArquivo, String codOrgao) {
		
		linha = "1" + linha;
		
		StringBuffer linhaTemp = new StringBuffer(linha);
		
		/**
		 *ALTERA��ES PARA O RENAINF EM 15/05/2012
		 *@author Rodrigo.fonseca
		 *
		 */
		if("MR01".equals(codIdentArquivo)){

			String tipoCnh = linhaTemp.substring(398,399).trim();
			linhaTemp.replace(398,399,"");		
			
			if ("".equals(tipoCnh)){
				linhaTemp.replace(714,715,"0");
			}else{
				linhaTemp.replace(714,715,"0");
			}
			
			/**
			 * INICIO DAS ALTERA��ES DOS CAMPOS PARA O RENAINF EM 15/05/2012
			 */
			
			if ("".equals(linhaTemp.substring(683,687).trim())) //ANO CAPA LOTE
				linhaTemp.replace(683,687,"0000");
			
			if ("".equals(linhaTemp.substring(687,692).trim())) //NUM LOTE
				linhaTemp.replace(687,692,"00000");
			
			if ("".equals(linhaTemp.substring(692,696).trim())) //NUM UNIDADE
				linhaTemp.replace(692,696,"0000");
			
			if ("".equals(linhaTemp.substring(696,704).trim())) //DAT LOTE
				linhaTemp.replace(696,704,"00000000");
			
			if ("".equals(linhaTemp.substring(704,714).trim())) //NOM OPERADOR
				linhaTemp.replace(704,714,"          ");
			
			//IN�CIO DOS CAMPOS NOVOS PARA O RENAINF EM 15/05/2012
			
			if ("".equals(linhaTemp.substring(715,716).trim())) //TIPO DOC COND - NAHB - CAMPO NOVO
				linhaTemp.replace(715,716,"0");
			
			if ("".equals(linhaTemp.substring(716,730).trim())) //NUM DOC COND - NAHB - CAMPO NOVO
				linhaTemp.replace(716,730,"              ");
			
			if ("".equals(linhaTemp.substring(730,731).trim())) //TIPO DOC EMB - TRANSP - CAMPO NOVO
				linhaTemp.replace(730,731,"0");
			
			if ("".equals(linhaTemp.substring(731,745).trim())) //NUM DOC EMB - TRANSP - CAMPO NOVO
				linhaTemp.replace(731,745,"              ");
		}
		
		
		if (("345".indexOf(tipArquivo) >= 0) && ("".equals(linhaTemp.substring(26,29).trim()))) //Motivo da invalida��o
			linhaTemp.replace(26,29,"000");
		
		if ("".equals(linhaTemp.substring(107,115).trim())) //Data da Infra��o
			linhaTemp.replace(107,115,"00000000");
		
		if ("".equals(linhaTemp.substring(115,119).trim())) //Hora da Infra��o
			linhaTemp.replace(115,119,"0000");
		
		if ("".equals(linhaTemp.substring(119,124).trim())) //Codigo do municipio
			linhaTemp.replace(119,124,"00000");
		
		if ("".equals(linhaTemp.substring(124,129).trim())) //Codigo da Infra��o
			linhaTemp.replace(124,129,"00000");
		
		if ("".equals(linhaTemp.substring(129,136).trim())) //Valor da Multa
			linhaTemp.replace(129,136,"0000000");
		
		if ("".equals(linhaTemp.substring(136,144).trim())) //Data do Vencimento
			linhaTemp.replace(136,144,"00000000");
		
		if ("".equals(linhaTemp.substring(144,146).trim())) //Codigo do Agente
			linhaTemp.replace(144,146,"00");
		
		if ("".equals(linhaTemp.substring(161,170).trim())) //Velocidade Permitida
			linhaTemp.replace(161,170,"000000000");
		
		if ("".equals(linhaTemp.substring(170,179).trim())) //Velocidade Aferida
			linhaTemp.replace(170,179,"000000000");
		
		if ("".equals(linhaTemp.substring(179,188).trim())) //Velocidade Considerada
			linhaTemp.replace(179,188,"000000000");
		
		
		if ("".equals(linhaTemp.substring(228,236).trim())) //Data da afericao
			linhaTemp.replace(228,236,"00000000");
		
		if ("".equals(linhaTemp.substring(306,308).trim())) //Especie do Veiculo
			linhaTemp.replace(306,308,"00");
		
		if ("".equals(linhaTemp.substring(308,310).trim())) //Categoria do Veiculo
			linhaTemp.replace(308,310,"00");
		
		if ("".equals(linhaTemp.substring(310,313).trim())) //Tipo do Veiculo
			linhaTemp.replace(310,313,"000");
		
		if ("".equals(linhaTemp.substring(313,316).trim())) //Cor do Veiculo
			linhaTemp.replace(313,316,"000");
		
		if ("".equals(linhaTemp.substring(376,387).trim())) //CPF do Condutor
			linhaTemp.replace(376,387,"00000000000");
		
		if ("".equals(linhaTemp.substring(387,398).trim())) //CNH do Condutor
			linhaTemp.replace(387,398,"00000000000");
		
		
		if ("".equals(linhaTemp.substring(441,449).trim())) //CEP do Endere�o
			linhaTemp.replace(441,449,"00000000");
		
		if ("".equals(linhaTemp.substring(449,454).trim())) //Municipio do Endere�o
			linhaTemp.replace(449,454,"00000");		

		if ("".equals(linhaTemp.substring(514,516).trim())) //Cod. Pais
			linhaTemp.replace(514,516,"00");		

		if ("".equals(linhaTemp.substring(516,525).trim())) //Ident. PRF
			linhaTemp.replace(516,525,"000000000");		
		
		if ("".equals(linhaTemp.substring(525,533).trim())) //Dat_Emissao
			linhaTemp.replace(525,533,"00000000");
		
		if ("".equals(linhaTemp.substring(533,547).trim())) //CPF Embarcador
			linhaTemp.replace(533,547,"00000000000000");	
		
		if ("".equals(linhaTemp.substring(607,608).trim())) //Ststus CPF
			linhaTemp.replace(607,608,"0");
		
		if ("".equals(linhaTemp.substring(608,622).trim())) //CPF Transp.
			linhaTemp.replace(608,622,"00000000000000");	
		
		if ("".equals(linhaTemp.substring(682,683).trim())) //Ststus CPF
			linhaTemp.replace(682,683,"0");
		
//		String vago=sys.Util.rPad("0","0", 17); /*Email Ricardo-02/07/2007 (PMRJ)*/
//		if  (codIdentArquivo.equals("MR01"))  
//		  return linhaTemp.toString().substring(1,505)+vago;
		
		  return linhaTemp.toString().substring(1);
	}
	
	
	
	public String formatarIsoladasPE01(String tipArquivo, String linha, String codIdentArquivo, String codOrgao) {
		
		StringBuffer linhaTemp = new StringBuffer(linha);
		StringBuffer linhaNova = new StringBuffer();
		
		if (!"".equals(linhaTemp.substring(47,59).trim())) // N�mero do Auto
			linhaNova.append(Util.formataDataYYYYMMDD(linhaTemp.substring(47,59).trim()));
		else
			linhaNova.append(sys.Util.lPad(""," ",12));

		
		if (!"".equals(linhaTemp.substring(59,66).trim())) // N�mero Placa
		{
			linhaNova.append(Util.formataDataYYYYMMDD(linhaTemp.substring(59,66).trim()));
		}
		else{
			linhaNova.append(sys.Util.lPad("","0",7));
		}
		
		if (!"".equals(linhaTemp.substring(128,135).trim())) // Cod. Marca Modelo
		{
			linhaNova.append(Util.formataDataYYYYMMDD(linhaTemp.substring(128,135).trim()));
		}
		else{
			linhaNova.append(sys.Util.lPad("","0",7));
		}
		
		if (!"".equals(linhaTemp.substring(128,135).trim())) // Local de Infra��o
		{
			linhaNova.append(Util.formataDataYYYYMMDD(linhaTemp.substring(128,135).trim()));
		}
		else{
			linhaNova.append(sys.Util.lPad(""," ",45));
		}
		
		if (!"".equals(linhaTemp.substring(111,119).trim())) // Data da Infra��o
		{
			linhaNova.append(Util.formataDataYYYYMMDD(linhaTemp.substring(111,119).trim()));
		}
		else{
			linhaNova.append(sys.Util.lPad("","0",8));
		}

		if (!"".equals(linhaTemp.substring(119,123).trim())) //Hora da Infra��o
		{
			linhaNova.append(linhaTemp.substring(119,123).trim());
		}
		else{
			linhaNova.append(sys.Util.lPad("","0",4));
		}
	
		if (!"".equals(linhaTemp.substring(123,128).trim())) // C�digo do municipio
		{
			linhaNova.append(linhaTemp.substring(123,128).trim());
		}
		else{
			linhaNova.append(sys.Util.lPad("","0",4));
		}
		
		if ("".equals(linhaTemp.substring(66,69).trim()))//C�digo da Infra��o
		{
			linhaNova.append(linhaTemp.substring(123,128).trim());
		}
		else
		{
			linhaNova.append(sys.Util.lPad("","0",4));
		}
		
		linhaNova.append(sys.Util.lPad("","0",7));//Valor da Multa
		
		linhaNova.append(sys.Util.lPad("","0",8));//Data do Vencimento

		if (!"".equals(linhaTemp.substring(144,156).trim())) //C�digo do Agente
		{
			linhaNova.append(linhaTemp.substring(144,156).trim());
		}
		else
		{
			linhaNova.append(sys.Util.lPad("","0",4));
		}
			
		linhaNova.append(sys.Util.lPad(""," ",10)); //Matricula do Agente
		
		linhaNova.append(sys.Util.lPad("","0",3)); //Velocidade Permitida
		
		linhaNova.append(sys.Util.lPad("","0",4)); //Velocidade Aferida
		
		linhaNova.append(sys.Util.lPad("","0",4)); //Velocidade Considerada
		
		linhaNova.append(sys.Util.lPad(""," ",1)); //Tipo do dispositivo
		
		linhaNova.append(sys.Util.lPad(""," ",15)); //Identifica��o do aparelho
		
		linhaNova.append(sys.Util.lPad(""," ",9)); //N�mero do immetro do aparelho
		
		linhaNova.append(sys.Util.lPad("","0",8)); //Data da ultima afericao
		
		linhaNova.append(sys.Util.lPad(""," ",10)); //Num. Certificado
		
		linhaNova.append(sys.Util.lPad(""," ",40)); //Localiza��o do Aparelho
		
		if (!"".equals(linhaTemp.substring(135,137).trim())) //Especie do Veiculo
		{
			linhaNova.append(linhaTemp.substring(135,137).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad("","0",2));
		}
		
		if ("".equals(linhaTemp.substring(137,139).trim())) //Categoria do Veiculo
		{
			linhaNova.append(linhaTemp.substring(137,139).trim());
		}else 
		{
			linhaNova.append(sys.Util.lPad("","0",2));
		}
			
		if (!"".equals(linhaTemp.substring(139,142).trim())) //Tipo do Veiculo
		{
			linhaNova.append(linhaTemp.substring(139,142).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad("","0",3));
		}
		
		if (!"".equals(linhaTemp.substring(142,144).trim())) //Cor do Veiculo
		{
			linhaNova.append(linhaTemp.substring(142,144).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad("","0",3));
		}
		
		if (!"".equals(linhaTemp.substring(441,486).trim())) //Nome do Condutor
		{
			linhaNova.append(linhaTemp.substring(441,486).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad(""," ",45));
		}
		
		if (!"".equals(linhaTemp.substring(486,497).trim())) //CPF do Condutor
		{
			linhaNova.append(linhaTemp.substring(486,497).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad("","0",11));
		}
					
		if (!"".equals(linhaTemp.substring(497,508).trim())) //CNH do Condutor
		{
			linhaNova.append(linhaTemp.substring(497,508).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad("","0",11));
		}
		
		if (!"".equals(linhaTemp.substring(508,510).trim())) //UF CNH do Condutor
		{
			linhaNova.append(linhaTemp.substring(508,510).trim());
		}else
		{
			linhaNova.append(sys.Util.lPad(""," ",2));
		}
		
		linhaNova.append(sys.Util.lPad(""," ",25)); //Endere�o Condutor
		
		linhaNova.append(sys.Util.lPad(""," ",5)); //Numero do endere�o
		
		linhaNova.append(sys.Util.lPad(""," ",11)); //Complemento do endere�o
		
		linhaNova.append(sys.Util.lPad("","0",8)); //CEP
		
		linhaNova.append(sys.Util.lPad("","0",4)); //Municipio do Endere�o
		
		linhaNova.append(sys.Util.lPad(""," ",51)); //Obs
		
		linhaNova.append(sys.Util.lPad(""," ",9)); //Identifica��o do Org�o
		
		return linhaNova.toString();
	}	
	
	
	/**
	 * M�todo para atualiza��o do controle de notifica��es para o retorno dos arquivos
	 * recebidos.
	 * @param conn
	 * @param linhaArq
	 * @author Sergio Roberto Junior
	 * @since 19/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaContrNotif(String nomUsername, String codOrgao, REG.LinhaArquivoRec linhaArq,
			Connection conn, String datProc) throws DaoException{
		try {           
			Statement stmt = conn.createStatement();
			
			/*
			 * Executa uma consulta na base do SMIT, para verificar se a notifica��o sofreu
			 * controle no envio do arquivo.
			 */ 
			String sCmd = "SELECT COD_CONTROLE_NOTIF FROM TSMI_CONTROLE_NOTIFICACOES"+
			" WHERE NUM_NOTIFICACAO = "+linhaArq.getNumNotificacao()+" AND COD_STATUS_NOTIF = 2 "+
			" ORDER BY COD_CONTROLE_NOTIF DESC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			String datEntrega = linhaArq.getDscLinhaArqRec().substring(37,45);
			String codRetorno = linhaArq.getDscLinhaArqRec().substring(45,47);
			CodigoRetornoBean myRetorno = new CodigoRetornoBean();
			myRetorno.setNumRetorno(codRetorno);
			myRetorno.LeRetorno();
			
			/*
			 * Caso a notifica��o tenha sofrido controle � registrado o status da linha
			 * e seus dados de arquivo de retorno, e mais a data de retorno.
			 * O status da linha varia de acordo com o resultado da transa��o 406, realizada
			 * anteriormente a chamada desse m�todo.
			 */
			if(rs.next()){
				String codigo = rs.getString("COD_CONTROLE_NOTIF");
				sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES"
					+" SET COD_ARQUIVO_RETORNO = "+linhaArq.getCodArquivo()+","
					+" COD_STATUS_NOTIF = "+linhaArq.getCodStatusCorrecao()+","
					+" NOM_USERNAME_RETORNO = '"+nomUsername+"',"
					+" COD_ORGAO_RETORNO = '"+codOrgao+"',"
					+" DAT_RETORNO = TO_DATE('"+datProc+"','DD/MM/YYYY'),"   /*DAT_RETORNO = TRUNC(SYSDATE)*/
					+" NUM_CODIGO_RETORNO = '"+myRetorno.getNumRetorno()+"',"
					+" IND_ENTREGA = '"+(myRetorno.getIndEntregue()?"S":"N")+"',"
					+" DAT_ENTREGA = TO_DATE('"+datEntrega+"','YYYYMMDD'),"
					+" COD_LINHA_ARQ_REC_RETORNO = "+linhaArq.getCodLinhaArqReq()
					+" WHERE COD_CONTROLE_NOTIF = "+codigo;
				stmt.executeUpdate(sCmd);           	
			}
			
			/*
			 * Caso a notifica��o n�o tenha sofrido controle � criado um registro, para a mesma,
			 * na tabela TSMI_CONTROLE_NOTIFICACOES com todos as suas informa��es pertinentes
			 * preenchidas, mais seu status, que vai depender da transa��o 406 realizada num 
			 * passo anterior a chamada do m�todo.
			 */
			else{
				/*
				 * Caso a notifica��o tenha passado na cr�tica da transa��o 406, mas n�o tenha sofrido
				 * controle seu status ser� oito em vez de nove.
				 */      	
				if(linhaArq.getCodStatusCorrecao().equals("9"))
					linhaArq.setCodStatusCorrecao("8");
				
				/*
				 * A manipula��o do tipo de notifica��o � realizada para inserir no controle
				 * de notifica��o o status do auto, correspondente a notifica��o em quest�o.
				 */  
				String codStatusAuto = "";
				if(!(linhaArq.getDscLinhaArqRec().length()<139)){
					if(linhaArq.getDscLinhaArqRec().substring(139).equals("1"))
						codStatusAuto = "1";
					else if(linhaArq.getDscLinhaArqRec().substring(139).equals("3"))
						codStatusAuto = "11";
				}
				
				String codOrgaoAut = "NULL";
				if (linhaArq.getDscLinhaArqRec().length() > 141)
					codOrgaoAut =  linhaArq.getDscLinhaArqRec().substring(140, 146);

				sCmd = "INSERT INTO TSMI_CONTROLE_NOTIFICACOES"+
				" (COD_CONTROLE_NOTIF,NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,COD_STATUS,DAT_RETORNO,"+
				" NUM_CODIGO_RETORNO,IND_ENTREGA,"+
				" COD_ARQUIVO_RETORNO,COD_LINHA_ARQ_REC_RETORNO,COD_STATUS_NOTIF,COD_ORGAO_AUTUACAO)"+
				" VALUES(SEQ_TSMI_CONTROLE_NOTIFICACOES.NEXTVAL,'"+linhaArq.getNumAutoInfracao().trim()+"','"+
				linhaArq.getNumNotificacao()+"','"+codStatusAuto+"',TO_DATE('"+datProc+"','DD/MM/YYYY'),'"+     /*linhaArq.getNumNotificacao()+"','"+codStatusAuto+"',TRUNC(SYSDATE),'"+*/
				myRetorno.getNumRetorno()+"','"+(myRetorno.getIndEntregue()?"S":"N")+"','"+
				linhaArq.getCodArquivo()+"','"+linhaArq.getCodLinhaArqReq()+"','"+
				linhaArq.getCodStatusCorrecao()+"',"+codOrgaoAut+")";
				stmt.executeUpdate(sCmd);
			}        
			stmt.close();
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}       
	}
	
		
	/**
	 * M�todo de chamada abrindo uma conex�o para o implementa��o de atualizaContrNotif().
	 * @param nomUsername
	 * @param codOrgao
	 * @param linhaArq
	 * @author Sergio Roberto Junior
	 * @since 21/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaContrNotif(String nomUsername, String codOrgao, REG.LinhaArquivoRec linhaArq) throws DaoException{
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			atualizaContrNotif(nomUsername, codOrgao, linhaArq, conn, Util.formatedToday().substring(0,10));
			conn.commit();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if(conn!= null){
				try { 
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 * Prepara a transa��o 406 - Retorno de AR da VEX
	 * 
	 * @param nomUsername
	 * @param codOrgao
	 * @param codOrgaoLotacao
	 * @param linhaArq
	 * @param conn
	 * @param transBRKDOL
	 * @return
	 * @throws sys.DaoException
	 */
	 public String TransacaoRetornoV1(String nomUsername, String codOrgao, String codOrgaoLotacao, String linhaArq, Connection conn, BRK.TransacaoBRKR transBRKDOL) throws sys.DaoException {
			String resultado = "";    	
			String linha = sys.Util.rPad(linhaArq," ",122);
			try{
				BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR(serviceloc);
				// Buscar dados da transa��o		 
				String parteVar = sys.Util.rPad(nomUsername, " ", 20) + sys.Util.lPad(codOrgao, "0", 6) + linha.substring(0,122);
				transBRK.setParteFixa("406");	
				transBRK.setParteVariavel(parteVar);
			    transBRK.setCodOrgaoAtuacao(codOrgao);
				transBRK.setCodOrgaoLotacao(codOrgaoLotacao);
				transBRK.setNomUserName(nomUsername);
				resultado = transBRK.getResultado(conn);
				
				
				/*Gravar Dados no TSMI_LOG_BROKER*/					
				String codRetorno = resultado.substring(0,3);					
				transBRK.GravarBrokerROB(resultado,codRetorno,codOrgao);
				transBRKDOL.setParteVariavel(transBRK.getParteVariavel());
				transBRKDOL.setCodLogBroker(transBRK.getCodLogBroker());
				transBRKDOL.setSeqTransacao(transBRK.getParteFixa());
				
				
			}
			catch(Exception e){
				throw new sys.DaoException(e.getMessage());
			}
			return resultado;
		}
	
	
}


