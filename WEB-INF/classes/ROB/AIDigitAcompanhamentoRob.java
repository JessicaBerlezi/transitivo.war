package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import sys.DaoException;

import ACSS.ParamSistemaBean;
import REG.AIDigitAcompanhamentoBean;

public class AIDigitAcompanhamentoRob {
	private static String NOM_ROBOT = "AIDigitAcompanhamentoRob";
	public AIDigitAcompanhamentoRob() throws RobotException, DaoException {
		
		Connection conn = null;
		Connection connLog = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();
			conn.setAutoCommit(false);
			connLog = Dao.getInstance().getConnection();
			
			monitor.setConn(connLog);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			ParamSistemaBean param = new ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO	
			param.PreparaParam(conn);
						
			SimpleDateFormat dfLog = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date datProc = new Date();
			
			monitor.gravaMonitor("Inicio do carregamento de Totais", monitor.MSG_INICIO);
			System.out.println(dfLog.format(datProc) + " - Inicio do carregamento de Totais");
			
			boolean processoParado = false;
			List orgaos = Dao.getInstance().ListarOrgaos();
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_YEAR, -1);
			Date datFinal = cal.getTime();
			
			//L� o par�metro da �ltima data processada
			Date datInicial = null;
			String datParametro = param.getParamSist("DAT_AI_DIGIT_ACOMPANHAMENTO");
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			try {
				cal.setTime(df.parse(datParametro));
				cal.add(Calendar.DAY_OF_YEAR, 1);
				datInicial = cal.getTime();
			} catch (Exception e) {
				datInicial = cal.getTime();
			}
			
			cal.setTime(datInicial);			
			Date datAtual = cal.getTime();
			Date ultimaData = null;
			while(datAtual.compareTo(datFinal) <= 0) {
				
				ultimaData = new Date(datAtual.getTime());
				monitor.gravaMonitor("In�cio do carregamento dos Totais do dia " + df.format(ultimaData), monitor.MSG_INICIO);
				System.out.println(dfLog.format(new Date()) + " - In�cio do carregamento dos Totais do dia " + df.format(ultimaData));
				
				Iterator itr = orgaos.iterator();			
				while (itr.hasNext()) {
					ACSS.OrgaoBean orgao = (ACSS.OrgaoBean) itr.next();
					AIDigitAcompanhamentoBean bean = new AIDigitAcompanhamentoBean ();
					bean.getOrgao().setCodOrgao(orgao.getCodOrgao());
					bean.setDatProc(datAtual);
					if (bean.totaliza())
						bean.grava(conn);
				}				
				//Atualiza o valor do par�metro de data
				ParamSistemaBean paramBean = new ParamSistemaBean();
				paramBean.setNomParam("DAT_AI_DIGIT_ACOMPANHAMENTO");
				paramBean.setValParametro(df.format(ultimaData));
				paramBean.setCodSistema(param.getCodSistema());
				Dao.getInstance().AtualizaParametro(paramBean, conn);
				
				conn.commit();
				
				monitor.gravaMonitor("Fim    do carregamento dos Totais do dia " + df.format(ultimaData), monitor.MSG_FIM);
				System.out.println(dfLog.format(new Date()) + " - Fim    do carregamento dos Totais do dia " + df.format(ultimaData));
				
				//Verifica se foi marcado para parar				
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					processoParado = true;
					break;
				}
				
				cal.add(Calendar.DAY_OF_YEAR, 1);
				datAtual = cal.getTime();
			}
			
			monitor.setNomObjeto("");
			monitor.setCodOrgao("");
			if (processoParado) {
				monitor.gravaMonitor("T�rmino solicitado pelo Gerente", monitor.MSG_FIM);
				System.out.println(dfLog.format(new Date()) + " - T�rmino solicitado pelo Gerente");
			} else {
				monitor.gravaMonitor("Fim do carregamento de Totais", monitor.MSG_FIM);
				System.out.println(dfLog.format(new Date()) + " - Fim do carregamento dos Totais");
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao carregar Totais: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {				
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}
			if (connLog != null) {
				try {
					connLog.rollback();
					Dao.getInstance().setReleaseConnection(connLog);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			} 			
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	public static void main(String args[])  throws RobotException {		
		try {			
			new AIDigitAcompanhamentoRob();			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
}
