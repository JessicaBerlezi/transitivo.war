/*
 * Created on 04/05/2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package ROB;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
public class AtualizaDiretorioBean {

	public static Connection conn = null;
	public static Statement stm;
	private static final String MYABREVSIST = "ROB";

	public static void atualizaFoto(ACSS.ParamSistemaBean param){
		
		try{
			
			String sCmd = "select num_auto_infracao,dat_digitalizacao from tsmi_foto_digitalizada where dat_digitalizacao>to_date('15/05/2005','dd/mm/yyyy') ";
			stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sCmd);
			while(rs.next()){
				try{
					
					REC.FotoDigitalizadaRobBean foto = REC.DaoRobDigit.getInstance().ConsultaFotoDigitalizada( 
					new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'" + rs.getString("num_auto_infracao") + "'"});
				
					File arquivo = new File("D:/PROD-smit/digitalizado/Foto/"+foto.getNumAutoInfracao().substring(0,6)+"/"+foto.getNumAutoInfracao()+".jpg");
					if(arquivo.exists()){
						
						String imagOrigem = arquivo.getPath();
						FileUtils.copyFile(new File(imagOrigem), new File(foto.getArquivo(param)));
						System.out.println("Arquivo Foto "+ arquivo.getName()+" copiado.");
						/* 
						arquivo.delete();
						(new File(imagOrigem)).delete();*/
					}else System.out.println("Arquivo de Foto "+arquivo.getName()+" nao existe.");
				
				}catch(REC.DaoException e){System.out.println("exception 46: "+ e);
				}catch(IOException e){ System.out.println("exception 47: "+ e);}	
				System.gc();
			}
			stm.close();
		}catch(SQLException e){System.out.println("exception 51: "+ e);
		}
	}

	public static void atualizaAR(ACSS.ParamSistemaBean param){
		
		try{
		
			String sCmd = "select num_auto_infracao,dat_digitalizacao from tsmi_ar_digitalizado where dat_digitalizacao>to_date('15/05/2005','dd/mm/yyyy') ";
			stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sCmd);
			
			while(rs.next()){
				
				try{
					
					String sNumInfracao=rs.getString("NUM_AUTO_INFRACAO");
					if ( (sNumInfracao==null) || (sNumInfracao.equals("")))
					  continue;
					
					REC.ARDigitalizadoRobBean ar = REC.ARDigitalizadoRobBean.consultaAR(new String[] {"NUM_AUTO_INFRACAO",
					"DAT_DIGITALIZACAO" }, new String[] {"'" + sNumInfracao + "'", "'" + sys.Util.fmtData(
					rs.getDate("dat_digitalizacao")) + "'" });
					
					File arquivo = null;
					if (((ar.getNumNotificacao() == null) || ar.getNumNotificacao().equals("")  )
					&& (ar.getNumAutoInfracao().length() >= 6)) {
						SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
						arquivo = new File("D:/PROD-smit/digitalizado/AR/" + ar.getNumAutoInfracao().substring(0,6) + "/"+ar.getNumAutoInfracao() +"_"+df.format(ar.getDatDigitalizacao())+".gif");
					
					}else if (ar.getNumNotificacao().length() >= 5)
						arquivo = new File("D:/PROD-smit/digitalizado/AR/" + ar.getNumNotificacao().substring(0,5) + "/"+ar.getNumNotificacao() +".gif");
						

					if(arquivo.exists()){
						String imagOrigem = arquivo.getPath();
						FileUtils.copyFile(new File(imagOrigem), new File(ar.getArquivo(param)));
						System.out.println("Arquivo AR "+ arquivo.getName()+" copiado.");
						/* 
						arquivo.delete();
						(new File(imagOrigem)).delete();*/
						
					}else System.out.println("Arquivo AR "+arquivo.getName()+" nao existe.");
					
				}catch(IOException e){System.out.println("exception 81: "+ e);}	
				 catch (sys.BeanException e1) {System.out.println("exception 82: "+ e1);}		
				System.gc();
			}	
			stm.close();
		}catch(SQLException e){ System.out.println("exception 85: "+ e);
		}
	}

	public static void atualizaAI(ACSS.ParamSistemaBean param){
		
		try{
		
			String sCmd = "select num_auto_infracao,dat_digitalizacao from tsmi_ai_digitalizado where dat_digitalizacao>to_date('15/05/2005','dd/mm/yyyy') ";
			stm = conn.createStatement();
			ResultSet rs = stm.executeQuery(sCmd);
			
			while(rs.next()){
				
				try{
					
					REC.AIDigitalizadoRobBean ai = REC.DaoRobDigit.getInstance().ConsultaAIDigitalizado( 
					new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'" + rs.getString("num_auto_infracao") + "'"});
					
					File arquivo = new File("D:/PROD-smit/digitalizado/Auto/" + ai.getNumAutoInfracao().substring(0,6) + "/"+ai.getNumAutoInfracao() +".gif");
					if(arquivo.exists()){
						String imagOrigem = arquivo.getPath();
						FileUtils.copyFile(new File(imagOrigem), new File(ai.getArquivo(param)));
						System.out.println("Arquivo AI "+ arquivo.getName()+" copiado.");
						/* 
						arquivo.delete();
						(new File(imagOrigem)).delete();
						*/
					}else System.out.println("Arquivo AI "+arquivo.getName()+" nao existe.");
				}catch(IOException e){System.out.println("exception 112: "+ e);}	
				catch(REC.DaoException e){System.out.println("exception 113: "+ e);}
				System.gc();
			}	
			stm.close();
		}catch(SQLException e){System.out.println("exception 114: "+ e);
		}	
	}

	public static void main(String args[]){
		
		try{
			
			conn = Dao.getInstance().getConnection();
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			
			//atualiza diretorios das fotos
			atualizaFoto(param);
			atualizaAR(param);
			atualizaAI(param);
			
			System.out.println("Execu��o terminada em: "+ new Date());
			conn.close();
				
		}catch(Exception e){
			System.out.println("exception linha 136: " + e);
		}finally{
			try{
				stm.close();
				conn.close();
			}catch(SQLException e){}
		}
	}
}
