package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import sys.DaoException;

public class CargaSaldoStatusRob {
	
	private static String NOM_ROBOT = "CargaSaldoStatusRob";
	
	public CargaSaldoStatusRob() throws RobotException, DaoException {
		
		Connection conn = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {		    
			conn = Dao.getInstance().getConnection();			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}             
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			
			monitor.gravaMonitor("Inicio do carregamento de Saldos", monitor.MSG_INICIO);
			System.out.println(df.format(new Date()) + " - Inicio do carregamento de Saldos");							
			
			boolean processoParado = false;
			
			List listOrgao = Dao.getInstance().ListarOrgaos();
			List listStatus = Dao.getInstance().ListarStatus();
			
			Iterator itrOrgao = listOrgao.iterator();			
			while (itrOrgao.hasNext()) {
				
				//Verifica se foi marcado para parar				
				if (processoParado || Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					processoParado = true;
					break;
				}
				
				ACSS.OrgaoBean orgao = (ACSS.OrgaoBean) itrOrgao.next();
				
				monitor.setNomObjeto("Org�o: " + orgao.getCodOrgao());
				monitor.setCodOrgao(orgao.getCodOrgao());
				monitor.gravaMonitor("Inicio do carregamento dos Saldos do Org�o " + orgao.getCodOrgao(), 
						monitor.MSG_INICIO);
				System.out.println(df.format(new Date()) + " - Inicio do carregamento dos Saldos do Org�o "
						+ orgao.getCodOrgao());
				
				Iterator itrStatus = listStatus.iterator();
				while (itrStatus.hasNext()) {
					
					// Verifica se foi marcado para parar				
					if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
						processoParado = true;
						break;
					}
					
					REC.StatusBean status = (REC.StatusBean) itrStatus.next();
					Calendar cal = new GregorianCalendar();
					
					int statusInt = Integer.parseInt(status.getCodStatus());
					if ((statusInt >= 0) && (statusInt <= 9)) { //Fase de Autua��o
						Date datInicial = new Date();
						cal.setTime(datInicial);
						cal.add(Calendar.YEAR, -1);
						Date datFinal = cal.getTime();
						this.carregar(monitor, df, conn, orgao, status, datInicial, datFinal);
						
						//} else if ((statusInt >= 10) && (statusInt <= 19)) { //Fase de Penalidade
						
						//} else if ((statusInt >= 30) && (statusInt <= 39)) { //Fase de Penalidade
						
						//} else if ((statusInt >= 90) && (statusInt <= 99)) { //Fase de Penalidade
						
					} else {
						//monitor.gravaMonitor("Erro carregar Saldos do Org�o " + orgao.getCodOrgao() + " no Status "
						//+ status.getCodStatus() + ": Status n�o reconhecido", monitor.MSG_ERRO);
						//System.out.println(df.format(new Date()) + " - Erro carregar Saldos do Org�o " + orgao.
						//getCodOrgao() + " no Status " + status.getCodStatus() + ": Status n�o reconhecido");
						continue;
					}
				}
				
				monitor.gravaMonitor("Fim do carregamento dos Saldos do Org�o " + orgao.getCodOrgao(), 
						monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - Fim do carregamento dos Saldos do Org�o "
						+ orgao.getCodOrgao());
			}
			
			monitor.setNomObjeto("");
			monitor.setCodOrgao("");
			if (processoParado) {
				monitor.gravaMonitor("T�rmino solicitado pelo Gerente", monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - T�rmino solicitado pelo Gerente");
			} else {
				monitor.gravaMonitor("Fim do carregamento de Saldos", monitor.MSG_FIM);
				System.out.println(df.format(new Date()) + " - Fim do carregamento des Saldos");
			}
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro ao carregar Saldos: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				try { 
					Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
				} catch (DaoException ed) {
					throw new RobotException(ed.getMessage());
				}
			}
		}
	}
	
	private void carregar(sys.MonitorLog monitor, SimpleDateFormat df, Connection conn, ACSS.OrgaoBean orgao,
			REC.StatusBean status, Date datInicial, Date datFinal) throws RobotException {
		try {		
			String resultado = DaoBroker.getInstance().TransacaoSaldoOrgao(orgao.getCodOrgao().toString(), status.getCodStatus(),
					datInicial, datFinal, conn);					
			String codRetorno = resultado.substring(0, 3);
			String qtdAutos = resultado.substring(3, 10);
			
			if (codRetorno.equals("000")) {						
				GER.SaldoOrgaoBean saldo = new GER.SaldoOrgaoBean();
				saldo.setDatProcessamento(new Date());
				saldo.setOrgao(orgao);    
				saldo.setStatus(status);
				saldo.setQtdAutos(Integer.valueOf(qtdAutos));
				saldo.setDatInfracaoInicial(datInicial);
				saldo.setDatInfracaoFinal(datFinal);
				saldo.grava();
				
			} else {
				monitor.gravaMonitor("Erro carregar Saldos do Org�o " + orgao.getCodOrgao() + " no Status "
						+ status.getCodStatus() + ": " + codRetorno, monitor.MSG_ERRO);
				System.out.println(df.format(new Date()) + " - Erro carregar Saldos do Org�o " + orgao.
						getCodOrgao() + " no Status " + status.getCodStatus() + ": " + codRetorno);
			}
		} catch (Exception e) {
			throw new RobotException(e.getMessage()); 
		}
	}
	
	public static void main(String args[])  throws RobotException {		
		try {			
			new CargaSaldoStatusRob();			
		} catch (Exception e) {
			System.out.println(e);			
		}
	}
	
}