package ROB;

import java.sql.Connection;
import java.sql.SQLException;


public class Manipulador {
    
    
	public Manipulador() throws RobotException, SQLException {
        Connection conn  = null;
		try {

            conn = Dao.getInstance().getConnection();
            conn.setAutoCommit(true);

			ACSS.UsuarioBean       UsuarioBeanId      = new ACSS.UsuarioBean() ;
			/*
			Dao.getInstance().VerificaLogUsuario(UsuarioBeanId);
            Dao.getInstance().VerificaRequerimento(UsuarioBeanId);
            Dao.getInstance().IncluiDadosLimpezaNova(conn);
            Dao.getInstance().DeletarLogBroker(UsuarioBeanId);
            Dao.getInstance().analiseEMITEVEXAR(UsuarioBeanId);
            Dao.getInstance().VerificaEMITEVEX(UsuarioBeanId);
			Dao.getInstance().IncluiDadosLimpezaNova(conn);
			*/
            Dao.getInstance().DeletarBD(UsuarioBeanId);
            
            
            

			
			conn.commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			
			conn.rollback();
			throw new RobotException(e.getMessage());            
		} 
	}
    


    
  public static void main(String[] args) throws RobotException {
        
		try {
			new Manipulador();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
    

}
