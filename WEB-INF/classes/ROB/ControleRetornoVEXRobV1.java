package ROB;

import java.sql.Connection;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import sys.DaoException;



public class ControleRetornoVEXRobV1 {
	
	private static final String NOM_ROBOT = "CtrControleVEX";
	private static final String quebraLinha = "\n";
	private static final int MAX_REG_PROCESSAR = 500;
	
	public ControleRetornoVEXRobV1() throws RobotException, ROB.DaoException, DaoException {
		
		Connection conn  = null;
		Statement trava = null;
		sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
		
		try {            			
			conn = Dao.getInstance().getConnection();
			
			monitor.setConn(conn);
			monitor.setDestino(monitor.DES_BASE);
			monitor.setNomProcesso(NOM_ROBOT);
			
			/*Travar Execu��o*/
			try {               
				trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
			} catch (Exception e) {
				monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
				return;
			}
			
			
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("40"); //M�DULO REGISTRO
			param.PreparaParam(conn);
			
			
			ACSS.UsuarioBean UsuarioBeanId = new ACSS.UsuarioBean();
			UsuarioBeanId.getOrgao().setCodOrgao(param.getParamSist("ORGAO_CODRET"));
			UsuarioBeanId.setCodOrgaoAtuacao(param.getParamSist("ORGAO_CODRET")); 
			UsuarioBeanId.setNomUserName(NOM_ROBOT);
			
			
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");			
			String datProc      = df.format(new Date()); 
			
			if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;
			
			monitor.gravaMonitor("Inicio Gera��o Controle Retorno VEX V1 - Dt Proc:"+datProc, monitor.MSG_ERRO);			
			System.out.println("Inicio Gera��o Controle Retorno VEX V1 - Dt Proc:"+datProc);
			
			boolean processoParado = false;
			
			while (true)
            {
				//Limpa a mem�ria
				System.gc();
				
				//Verifica se foi marcado para parar
				if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) {
					processoParado = true;
				}
				
				REC.ARDigitalizadoBean ControleRetornoVEXID = new REC.ARDigitalizadoBean();
				Dao.getInstance().ConsultaArsRetornadasVex(ControleRetornoVEXID,MAX_REG_PROCESSAR);
                if ((processoParado) || (ControleRetornoVEXID.getListaArqs().size()==0)) break;
				
				for (int i=0;i<ControleRetornoVEXID.getListaArqs().size();i++) {
					//System.out.println("Processando..."+ControleRetornoVEXID.getListaArqs(i).getNumNotificacao());
					monitor.gravaMonitor("Processando Controle Retorno VEX V1 - Notif.:"+ControleRetornoVEXID.getListaArqs(i).getNumNotificacao(), monitor.MSG_ERRO);
					
					Dao.getInstance().ProcessArsRetornadasVex(ControleRetornoVEXID.getListaArqs(i),UsuarioBeanId);
					
				}
            }
			datProc      = df.format(new Date()); 
			
			monitor.gravaMonitor("Fim Gera��o Controle Retorno VEX V1 : Dt Proc:"+datProc, monitor.MSG_ERRO);
			System.out.println("Fim Gera��o Controle Retorno VEX V1 : Dt Proc:"+datProc);
			
		} catch (Exception e) {
			try {
				monitor.gravaMonitor("Erro processar ControleEmiteVEXRobV1: " + e.getMessage(), monitor.MSG_ERRO);
			} catch (sys.DaoException ed) {
				throw new RobotException(ed.getMessage());
			}
			throw new RobotException(e.getMessage());            
		} finally {
			if (conn != null) {
				try { 
					Dao.getInstance().setReleaseConnection(conn);
				} catch (Exception e) {
					throw new RobotException(e.getMessage());
				}
			}           
			if (trava != null) {
				Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
			}
		}
	}
	
	public static void main(String[] args) throws RobotException {
		
		try {
			new ControleRetornoVEXRobV1();			 
		} catch (Exception e) { 
			System.out.println(e); 
		} 
	}
	
	
}