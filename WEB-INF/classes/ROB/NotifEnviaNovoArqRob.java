//package ROB;
//
//import java.io.File;
//import java.io.FileWriter;
//import java.sql.Connection;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//
//import sys.Util;
//
//import REC.FotoDigitalizadaBean;
//import REG.ArquivoRecebidoBean;
//import REG.LinhaArquivoRec;
//
///**
//
//public class NotifEnviaNovoArqRob {
//    
//    private static final String NOM_ROBOT = "NotifEnviaNovoArqRob";
//    private static final String quebraLinha = "\n";
//    
//    public NotifEnviaNovoArqRob() throws RobotException {
//        
//        Connection conn  = null;
//        Statement trava = null;
//        sys.MonitorLog monitor = new sys.MonitorLog(NOM_ROBOT);
//        
//        try {            			
//        	conn = Dao.getInstance().getConnection();
//        	REG.Dao dao = REG.Dao.getInstance();
//        	
//        	monitor.setConn(conn);
//        	monitor.setDestino(monitor.DES_BASE);
//        	monitor.setNomProcesso(NOM_ROBOT);
//        	
//        	try {               
//        		trava = Dao.getInstance().travarExecucao(NOM_ROBOT);                                           
//        	} catch (Exception e) {
//        		monitor.gravaMonitor(e.getMessage(), monitor.MSG_ERRO);               
//        		return;
//        	}
//        	
//        	ACSS.ParamSistemaBean paramReg = new ACSS.ParamSistemaBean();
//        	paramReg.setCodSistema("40"); //M�DULO REGISTRO	
//        	paramReg.PreparaParam(conn);
//        	
//        	ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
//        	paramRec.setCodSistema("45"); //M�DULO RECURSO    
//        	paramRec.PreparaParam(conn);
//        	
//        	String dirDownload = paramReg.getParamSist("DOWN_ARQUIVO_RECEBIDO");
//        	
//        	if (Dao.getInstance().pararProcesso(conn, NOM_ROBOT)) return;                 
//        	
//        	REG.ArquivoRecebidoBean arqRecebido = (new REG.ArquivoRecebidoBean());
//        	arqRecebido.setCodStatus("8");
//        	arqRecebido.setCodIdentArquivo("EMITEVEX");
//        	ArrayList listFotos = null;
//        	
//        	// Carrega todas as linhas que est�o ajustadas prontas para envio.
//        	dao.CarregarLinhasAjustadas(arqRecebido, conn);
//        	
//        	/* In�cio l�gico do bloco de emiss�o de linhas com foto
//        	 * Esse Bloco l�gico verifica a exist�ncia de foto para linhas com status 9 e
//        	 * acrescentando, em caso positivo, essa linha na lista do objeto arqRecebido. 
//        	 */
//        	ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
//        	arqRec.setCodStatus("9");
//        	arqRec.setCodIdentArquivo("EMITEVEX");
//        	dao.CarregarLinhasAjustadas(arqRec, conn);
//        	for(int x=0; x<arqRec.getLinhaArquivoRec().size();x++){
//                FotoDigitalizadaBean foto = REC.DaoDigit.getInstance().ConsultaFotoDigitalizada(new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'"+((LinhaArquivoRec)arqRec.getLinhaArquivoRec().get(x)).getDscLinhaArqRec().substring(280,292).trim()+"'"});
//                if(foto == null){
//                	arqRec.getLinhaArquivoRec().remove(x);
//                	x--;
//                }
//        	}
//        	for(int y=0;y<arqRec.getLinhaArquivoRec().size();y++){
//        		arqRecebido.getLinhaArquivoRec().add(arqRec.getLinhaArquivoRec().get(y));
//        	}
//        	// Fim do Bloco l�gico do envio de linhas com foto.
//        	
//        	/*
//        	 * Verifica se existem notifica��es a serem enviadas.
//        	 * Caso n�o exista nenhuma notifica��o a ser enviada a execu��o do Rob�
//        	 * � interrompida.
//        	 */ 
//        	if(arqRecebido.getLinhaArquivoRec().size() == 0){
//        		monitor.gravaMonitor("T�rmino da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
//        		return;
//        	}
//        	// Associa os arquivos aos quais as linhas fazem parte.
//        	List arquivos = dao.AssociarLinhas(arqRecebido, conn);
//        	
//        	// Ajusta o nome dos arquivos, quando necess�rio.
//        	acertarNomeArquivo(arquivos);          
//        	
//        	/*
//        	 * Executa um loop com intuito de gerar todos os novos arquivos a serem
//        	 * enviados pelo Rob�
//        	 */ 
//        	String qtdReg;
//        	for(int pos = 0; pos<arquivos.size(); pos++){
//        		arqRecebido = (ArquivoRecebidoBean)arquivos.get(pos);
//        		
//        		// Inicia o monitoramento de gera��o de um arquivo
//            	monitor.setNomObjeto(arqRecebido.getNomArquivo());
//            	monitor.setCodOrgao(arqRecebido.getCodOrgao());
//            	monitor.gravaMonitor("Inicio da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_INICIO);
//            	System.out.println("Inicio da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo());    
//            	
//            	qtdReg = arqRecebido.getQtdReg();
//            	arqRecebido.setQtdReg(String.valueOf(arqRecebido.getLinhaArquivoRec().size())) ;
//        		FileWriter writer = criarArquivoGrv(arqRecebido, dirDownload);
//        		
//        		int QtdLinProc = 0;
//        		listFotos = new ArrayList();
//        		Iterator ite = arqRecebido.getLinhaArquivoRec().iterator();                
//        		while (ite.hasNext()) {
//        			
//        			if ((QtdLinProc%200)==0)
//        				monitor.gravaMonitor("Arquivo: " + arqRecebido.getNomArquivo() + " Linhas: " +
//        						QtdLinProc+"/"+ arqRecebido.getLinhaArquivoRec().size(), monitor.MSG_INFO);
//        			
//        			REG.LinhaArquivoRec linhaArq = (REG.LinhaArquivoRec) ite.next();
//        			gravaGrv(writer, arqRecebido, linhaArq, conn);
//        			verificaFoto(paramRec, linhaArq, listFotos);
//        			QtdLinProc++;										
//        		}
//        		
//        		if (writer != null)	writer.close();
//        		
//        		//Cria o arquivo zip com as fotos
//        		if (listFotos.size() > 0)
//        			criarArquivoZip(arqRecebido, dirDownload, listFotos);                
//        		
//        		arqRecebido.setConn(conn);
//        		arqRecebido.setCodStatus("2");
//        		arqRecebido.setQtdReg(qtdReg);
//        		arqRecebido.setDatDownload(sys.Util.fmtData(new Date()));
//        		if (!arqRecebido.isAtualizado())
//        			throw new DaoException("Erro ao atualizar Arquivo: " + arqRecebido.getNomArquivo() + " !");
//        		
//        		conn.commit();
//        		
//        		// Finaliza o monitoramento de gera��o de um arquivo
//        		monitor.gravaMonitor("T�rmino da Gera��o do Novo do Arquivo " + arqRecebido.getNomArquivo(), monitor.MSG_FIM);    
//        	}
//        } 
//        catch (Exception e) {
//            try {
//                monitor.gravaMonitor("Erro ao Gerar os Novos Arquivos: " + e.getMessage(), monitor.MSG_ERRO);
//            } catch (sys.DaoException ed) {
//                throw new RobotException(ed.getMessage());
//            }
//            throw new RobotException(e.getMessage());            
//        } finally {
//            if (conn != null) {
//                try { 
//                    Dao.getInstance().setReleaseConnection(conn);
//                } catch (Exception e) {
//                    throw new RobotException(e.getMessage());
//                }
//            }           
//            if (trava != null) {
//                try { 
//                    Dao.getInstance().destravarExecucao(NOM_ROBOT, trava);
//                } catch (DaoException ed) {
//                    throw new RobotException(ed.getMessage());
//                }
//            }
//        }
//    }
//    
//    public static FileWriter criarArquivoGrv(REG.ArquivoRecebidoBean arqRecebido, String dirDestino)
//    throws RobotException {
//                        
//        String caminho = dirDestino + "/" + arqRecebido.getNomArquivoGrv();			
//        
//        //Cria o arquivo
//        FileWriter wrtGrv;
//        try {			
//            File dirGrv = new File(caminho.substring(0, caminho.lastIndexOf("/")));
//            dirGrv.mkdirs();
//            
//            File arqGrv = new File(caminho);
//            arqGrv.createNewFile();
//            wrtGrv = new FileWriter(arqGrv);
//            
//            //Grava o header do arquivo	
//            if (!arqRecebido.getHeaderGrv().equals("")) {
//                wrtGrv.write(arqRecebido.getHeaderGrv());
//                wrtGrv.write(quebraLinha);
//            }
//            
//        } catch (Exception e) {
//            throw new RobotException(e.getMessage());
//        }
//        return wrtGrv;
//    }
//    
//    private static void gravaGrv(FileWriter writer, REG.ArquivoRecebidoBean arqRecebido,
//            REG.LinhaArquivoRec linhaArq, Connection conn) throws RobotException {
//        
//        try {
//            if (writer != null) {			
//                writer.write(linhaArq.getLinhaGrv(arqRecebido, conn));
//                writer.write(quebraLinha);
//            }
//        } catch (Exception e) {
//            throw new RobotException(e.getMessage());
//        }
//    }
//    
//    
//    private static boolean verificaFoto(ACSS.ParamSistemaBean param, REG.LinhaArquivoRec linhaArq, ArrayList
//            listFotos) {
//        boolean retorno = false;
//        try {
//            String numAutoInfracao = linhaArq.getDscLinhaArqRec().substring(280, 292).trim();
//            boolean possuiFoto = !linhaArq.getDscLinhaArqRec().substring(539, 554).trim().equals("");            
//            
//            if (possuiFoto) {            
//                REC.FotoDigitalizadaBean foto = REC.DaoDigit.getInstance().ConsultaFotoDigitalizada(new String[]
//                    {"NUM_AUTO_INFRACAO"}, new String[] {"'"+numAutoInfracao+"'"});
//                
//                if (foto != null) {
//                    listFotos.add(foto.getArquivo(param));
//                    retorno = true;
//                }
//            }
//            
//        } catch (Exception e) {
//            retorno = false;
//        }
//        return retorno;
//    }
//    
//    public static boolean criarArquivoZip(REG.ArquivoRecebidoBean arqRecebido, String dirDestino,
//            ArrayList listFotos) throws RobotException {
//
//        boolean retorno = false;
//        try {
//            REG.ArquivoRecebidoBean arqFoto = Dao.getInstance().EmpacotarArquivo(arqRecebido);
//
//            if (arqFoto != null) {
//                String caminho = dirDestino + "/" + arqFoto.getNomArquivoGrv();         
//                   
//                File dirZip = new File(caminho.substring(0, caminho.lastIndexOf("/")));
//                dirZip.mkdirs();
//            
//                File arqZip = new File(caminho);
//                arqZip.createNewFile();
//            
//                String[] arrayFotos = new String[listFotos.size()];
//                listFotos.toArray(arrayFotos);
//                sys.IOUtil.compactarZip(arrayFotos, arqZip, false);
//                
//                retorno =  true;
//            }            
//            
//        } catch (Exception e) {
//            throw new RobotException(e.getMessage());
//        }
//        return retorno;
//    }
//    
//    public static void main(String[] args) throws RobotException {
//        
//        try {
//            new NotifEnviaNovoArqRob();			 
//        } catch (Exception e) { 
//            System.out.println(e); 
//        } 
//    }
//    
//    
//    /**
//     * M�todo utilizado 
//     * @param arquivos
//     */
//    private void acertarNomeArquivo(List arquivos){
//    	String nomArquivo = null;
//    	int seq;
//    	StringBuffer buffer;
//    	for(int pos=0;pos<arquivos.size();pos++){
//    		nomArquivo = ((ArquivoRecebidoBean)arquivos.get(pos)).getNomArquivo().replaceAll(".TXT","");
//    		if(nomArquivo.length() == 17)
//    			nomArquivo += "_001."+((ArquivoRecebidoBean)arquivos.get(pos)).getSufixoRetornoGrv();
//    		else{
//    			seq = Integer.parseInt(nomArquivo.substring(18,21));
//    			seq++;
//    			buffer = new StringBuffer(nomArquivo);
//    			nomArquivo = buffer.replace(18,21,Util.lPad(String.valueOf(seq),"0",3)).toString()+"."+((ArquivoRecebidoBean)arquivos.get(pos)).getSufixoRetornoGrv();
//    		}
//    		((ArquivoRecebidoBean)arquivos.get(pos)).setNomArquivo(nomArquivo);
//    	}
//    }
//}
