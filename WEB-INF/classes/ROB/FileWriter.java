package ROB;

import java.io.File;
import java.io.IOException;

public class FileWriter extends java.io.FileWriter {

	private File file;
	
	public FileWriter(File file) throws IOException {
		super(file);
		this.file = file;
	}

	public File getFile() {
		return file;
	}
	
}
