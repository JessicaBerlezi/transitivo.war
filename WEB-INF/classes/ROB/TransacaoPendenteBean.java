package ROB;

public class TransacaoPendenteBean {

	String codBrkrTransPendente;
	String codEvento;
	String datProc;
	String txtParteVariavel;
	
	public TransacaoPendenteBean () {
		
	}
	public void setCodBrkrTransPendente(String codBrkrTransPendente) {
		if (codBrkrTransPendente == null) codBrkrTransPendente = "";
		this.codBrkrTransPendente = codBrkrTransPendente;
	}
	public String getCodBrkrTransPendente() {
		return codBrkrTransPendente;
	}
	public void setCodEvento(String codEvento) {
		if (codEvento == null) codEvento = "";
		this.codEvento = codEvento;
	}
	public String getCodEvento() {
		return codEvento;
	}
	public void setDatProc(String datProc) {
		if (datProc == null) datProc = "";
		this.datProc = datProc;
	}
	public String getDatProc() {
		return datProc;
	}
	public void setTxtParteVariavel(String txtParteVariavel) {
		if (txtParteVariavel == null) txtParteVariavel = "";
		this.txtParteVariavel = txtParteVariavel;
	}
	public String getTxtParteVariavel() {
		return txtParteVariavel;
	}
}
