package REG;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ARDigitAcompanhamentoXls {
	public void write(ARDigitAcompanhamentoBean ARDigitBeanId, String titulo, String arquivo) throws IOException, WriteException
	{
		
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("ACOMPANHAMENTO DE DIGITALIZA��O", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
					WritableFont.DEFAULT_POINT_SIZE,
					WritableFont.BOLD,
					false,
					UnderlineStyle.NO_UNDERLINE,
					Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTotal = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 6/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
			
			label = new Label(0, 2, "SEQ", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(0, 5);
			
			label = new Label(1, 2, "DATA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(1, 20);
			
			label = new Label(2, 2, "EMITIDOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 20);
			
			label = new Label(3, 2, "RETORNADOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 10);
			
			label = new Label(4, 2, "SALDO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 20);
			
			label = new Label(5, 2, "DIGITALIZADOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 20);
			
			label = new Label(6, 2, "TEMPO M�DIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(6, 20);
			
			label = new Label(7, 2, "PENDENTE", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(7, 20);
			
			Integer total = 0;
			Integer totalGeral = 0;
			Iterator it = ARDigitBeanId.getBeans().iterator();
			ARDigitAcompanhamentoBean relatorioImp  = new ARDigitAcompanhamentoBean();
			if( ARDigitBeanId.getBeans().size()>0 ) { 
				while (it.hasNext()) {
					relatorioImp = (ARDigitAcompanhamentoBean)it.next() ;
					
					label = new Label(0, cont, String.valueOf(seq)); 
					sheet.addCell(label);
					
					label = new Label(1, cont, df.format(relatorioImp.getDatProc())); 
					sheet.addCell(label);
					
					label = new Label(2, cont, relatorioImp.getQtdRecebido().toString()); 
					sheet.addCell(label);
					
					label = new Label(3, cont, relatorioImp.getQtdEntregue().toString()); 
					sheet.addCell(label);
					
					total = relatorioImp.getQtdRecebido()-relatorioImp.getQtdEntregue();
					totalGeral += total;
					label = new Label(4, cont, total.toString()); 
					sheet.addCell(label);
					
					label = new Label(5, cont, relatorioImp.getQtdDigitalizado().toString()); 
					sheet.addCell(label);
					
					label = new Label(6, cont, relatorioImp.getTempoMedio().toString()); 
					sheet.addCell(label);
					
					label = new Label(7, cont, relatorioImp.getQtdPendentes().toString()); 
					sheet.addCell(label);
					
					cont++;
					seq++;
				}
			}
			
			label = new Label(0, cont, "Total:", formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(2, cont, ARDigitBeanId.getTotalQtdRecebido().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(3, cont, ARDigitBeanId.getTotalQtdEntregue().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(4, cont, totalGeral.toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(5, cont, ARDigitBeanId.getTotalQtdDigitalizado().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(6, cont, ARDigitBeanId.getTotalTempoMedio().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(7, cont, ARDigitBeanId.getTotalQtdPendentes().toString(), formatoTotal); 
			sheet.addCell(label);

			
			workbook.write(); 
			workbook.close();
			
			
		}catch (Exception e) {	
			throw new IOException("GeraXls: " + e.getMessage());	}
		
	}
	
}
