package REG;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import ACSS.ParamSistemaBean;

/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX. 
 * 
 * @author Jos� Fernando
 * @since 22/03/2006
 */

public class ARDigitPendenciaCmd  extends sys.Command 
{
	private static final String jspPadrao="REG/ARDigitPendencia.jsp";    
	private String next;
	private String statusInteracao = null;
	
	public ARDigitPendenciaCmd() {
		next = jspPadrao;
	}
	
	public ARDigitPendenciaCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  
				UsrLogado = new ACSS.UsuarioBean() ;	  			
			
			// Variav�l usada para controlar intera��o o usu�rio com o sistema.
			statusInteracao = req.getParameter("statusInteracao");
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
			
			Map meses = new HashMap();
			meses.put("01", "JANEIRO");
			meses.put("02", "FEVEREIRO");
			meses.put("03", "MAR�O");
			meses.put("04", "ABRIL");
			meses.put("05", "MAIO");
			meses.put("06", "JUNHO");
			meses.put("07", "JULHO");
			meses.put("08", "AGOSTO");
			meses.put("09", "SETEMBRO");
			meses.put("10", "OUTUBRO");
			meses.put("11", "NOVEMBRO");
			meses.put("12", "DEZEMBRO");
			
			if(OrgaoId == null)
				OrgaoId = new ACSS.OrgaoBean() ;	 				
			String acao = req.getParameter("acao");  
			
			if(acao == null)
				acao = "";
			if(acao.equals("")){
				acao =" "; 
				statusInteracao = "1";
			}	
			
			// A��o realizada ap�s a sele��o de visualiza��o do relat�rio por parte do usu�rio.			  
			else if(acao.equalsIgnoreCase("visualizacao"))
			{
				// Recebe os par�metros passado pela interface com o usu�rio.
				String codOrgao = req.getParameter("codOrgao");
				String statusAuto = req.getParameter("statusAuto"); 
				String sitEntrega = req.getParameter("sitEntrega");
				String todosOrgaos = req.getParameter("todosOrgaos");
				String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
				
				String[] orgaos = null;
				// Verifica se a consulta ser� feita para todos os �rg�os
				if (todosOrgaos != null) 
				{
					if (todosOrgaos.equalsIgnoreCase("null")) 
					{
						OrgaoId.Le_Orgao(codOrgao,0);
						todosOrgaos = null;
						orgaos = new String[] {codOrgao};
					} 
					else 
					{
						OrgaoId.setSigOrgao("TODOS");
						orgaos = todosOrgaosCod;
					}
				} 
				else 
				{
					OrgaoId.Le_Orgao(codOrgao,0);
					orgaos = new String[] {codOrgao};
				}
				
				try
				{
					Dao dao = Dao.getInstance();
					NotifControleBean notifControle = new NotifControleBean();
					
					if(statusAuto.equalsIgnoreCase("autuacao"))
						notifControle.setCodStatusAuto("001");
					else if(statusAuto.equalsIgnoreCase("penalidade"))
						notifControle.setCodStatusAuto("011");
					
					String prazo = req.getParameter("prazo");
					List totalAnosMeses = dao.consultaARDigitPendenciaMes(notifControle, Integer.parseInt(prazo), orgaos, sitEntrega);
					
					req.setAttribute("totalAnosMeses",totalAnosMeses);
					req.setAttribute("statusAuto",statusAuto);
					req.setAttribute("sitEntrega",sitEntrega);					
					req.setAttribute("prazo", prazo);
					req.setAttribute("codOrgao", codOrgao);
					req.setAttribute("todosOrgaos",todosOrgaos);
				}
				catch(Exception e){
					OrgaoId.setMsgErro("ERRO: ERRO DE ACESSO A BASE DE DADOS !\n" + e.getMessage());
				}
				next = "REG/ARDigitPendenciaMes.jsp";
			}
			/*
			 * A��o realizada a ap�s a op��o de detalhamento dos itens selecionados
			 * na apresenta��o do relat�rio
			 */
			else if(acao.equalsIgnoreCase("detalhe")||acao.equalsIgnoreCase("Imprimir"))
			{				
				String detalhe = req.getParameter("detalhe");
				String statusAuto = req.getParameter("codStatusAuto");
				if (statusAuto == null) 
					statusAuto = req.getParameter("statusAuto");
				String sitEntrega = req.getParameter("sitEntrega");
				
				String totalMesAno = req.getParameter("totalMesAno");
				String codOrgao = req.getParameter("codOrgao");
				String todosOrgaos = req.getParameter("todosOrgaos");
				String sentido = req.getParameter("sentido");
				String prazoDigitado = req.getParameter("prazoDigitado");
				String primeiroRegistro = req.getParameter("primeiroRegistro");
				if (primeiroRegistro == null) primeiroRegistro = "";
				
				String primeiroAnterior = req.getParameter("primeiroAnterior");
				if (primeiroAnterior == null) primeiroAnterior = "";
				
				String ultimoRegistro = req.getParameter("ultimoRegistro");
				if (ultimoRegistro == null) ultimoRegistro = "";
				
				String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
				int prazo = Integer.parseInt(prazoDigitado);
				
				String numPag = req.getParameter("numPag");
				if (numPag == null) numPag = "0";
				
				String seq = req.getParameter("seq");
				
				
				String[] orgaos = null;
				// Verifica se a consulta ser� feita para todos os �rg�os
				if (todosOrgaos != null) 
				{
					if (todosOrgaos.equalsIgnoreCase("null")) 
					{
						OrgaoId.Le_Orgao(codOrgao,0);
						todosOrgaos = null;
						orgaos = new String[] {codOrgao};
					} 
					else 
					{
						OrgaoId.setSigOrgao("TODOS");
						orgaos = todosOrgaosCod;
					}
				} 
				else 
				{
					OrgaoId.Le_Orgao(codOrgao,0);
					orgaos = new String[] {codOrgao};
				}				
				
				try{			
					Dao dao = Dao.getInstance();
					NotifControleBean notifControle = (NotifControleBean) req.getSession().getAttribute("NotifControleId");
					if( notifControle == null ) 
						notifControle = new NotifControleBean();
					
					if(statusAuto.equalsIgnoreCase("autuacao"))
						notifControle.setCodStatusAuto("001");
					else if(statusAuto.equalsIgnoreCase("penalidade"))
						notifControle.setCodStatusAuto("011");
					
					notifControle.setCodOrgaoAutuacao(codOrgao);
					
					ParamSistemaBean parametro = (ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");
					String rowNum = parametro.getParamSist("LIMITE_REL_PENDENCIAS_AR");
					String qtdReg = rowNum;
					String rowNumImp = "";
					
					if (sentido == null) 
						sentido = "Prox";
					
					// Alterado em 29.06.2006 por Marlon Falzetta.
					// � desejado que a impressao seja para todos os dados e nao
					// somente para os visualizados na tela.
					HashMap proxAnt = new HashMap();
					if(sentido.equals("Prox"))
					{
						proxAnt.put("primeiroRegistro", primeiroRegistro);
						proxAnt.put("ultimoRegistro", ultimoRegistro);
						numPag = Integer.toString(Integer.parseInt(numPag)+1);
						if (seq == null) 
							seq = "1";
						else
					        seq = Integer.toString(Integer.parseInt(seq)+Integer.parseInt(qtdReg));
					}
					else
					{
						proxAnt.put("primeiroRegistro", ""); 
						proxAnt.put("ultimoRegistro", "");
						numPag = Integer.toString(Integer.parseInt(numPag)-1);
						seq = Integer.toString(Integer.parseInt(seq)-Integer.parseInt(qtdReg));
					}
					proxAnt.put("primeiroAnterior", primeiroAnterior);
					
					proxAnt.put("sentido", sentido);
					
					dao.consultaARDigitPendenciaDet(notifControle, prazo, totalMesAno, rowNum, proxAnt, orgaos, sitEntrega, rowNumImp);
					
					req.setAttribute("numPag",numPag);
					req.setAttribute("seq",seq);
					req.setAttribute("opcao",detalhe);
					req.setAttribute("NotifControleId",notifControle);
					req.setAttribute("statusAuto",statusAuto);
					req.setAttribute("sitEntrega",sitEntrega);
					req.setAttribute("codOrgao",codOrgao);
					req.setAttribute("prazo", String.valueOf(prazo));
					req.setAttribute("todosOrgaos",todosOrgaos);
					req.setAttribute("todosOrgaosCod",todosOrgaosCod);
					req.setAttribute("totalMesAno",totalMesAno);
					req.setAttribute("prazoDigitado",prazoDigitado);
					req.setAttribute("primeiroAnterior",primeiroRegistro);
					next = "/REG/ARDigitPendenciaDet.jsp";
					
					if (sentido.equals("Imprimir"))
					{
						GregorianCalendar calendario = new GregorianCalendar();
						calendario.setTime(new Date());
						next = "/REG/ARDigitPendenciaImp.jsp";
						rowNumImp = "-1";	
						dao.consultaARDigitPendenciaDet(notifControle, prazo, totalMesAno, rowNum, proxAnt, orgaos, sitEntrega, rowNumImp);
						String tituloConsulta = "RELAT�RIO - PEND�NCIAS DE DIGITALIZA��O DE AR - " + meses.get(totalMesAno.substring(0, 2)) + "/" + String.valueOf(calendario.get(GregorianCalendar.YEAR));
						req.setAttribute("tituloConsulta", tituloConsulta);
					}
				}
				catch(Exception e){
					throw new sys.CommandException(e.getMessage());	
				}
			}
			req.setAttribute("statusInteracao",statusInteracao);
			req.setAttribute("OrgaoId",OrgaoId);
			req.setAttribute("meses",meses);
		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
