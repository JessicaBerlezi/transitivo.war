package REG;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import REC.AutoInfracaoBean;
import ACSS.ParamSistemaBean;

/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * 
 * @author Jos� Fernando
 * @since 22/03/2006
 */

public class AIDigitPendenciaCmd  extends sys.Command 
{
	private static final String jspPadrao="REG/AIDigitPendencia.jsp";    
	private String next;
	private String statusInteracao = null;
	
	public AIDigitPendenciaCmd() {
		next = jspPadrao;
	}
	
	public AIDigitPendenciaCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  
				UsrLogado = new ACSS.UsuarioBean() ;	  			
			
			// Variav�l usada para controlar intera��o o usu�rio com o sistema.
			statusInteracao = req.getParameter("statusInteracao");
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
			
			Map meses = new HashMap();
			meses.put("01", "JANEIRO");
			meses.put("02", "FEVEREIRO");
			meses.put("03", "MAR�O");
			meses.put("04", "ABRIL");
			meses.put("05", "MAIO");
			meses.put("06", "JUNHO");
			meses.put("07", "JULHO");
			meses.put("08", "AGOSTO");
			meses.put("09", "SETEMBRO");
			meses.put("10", "OUTUBRO");
			meses.put("11", "NOVEMBRO");
			meses.put("12", "DEZEMBRO");
			
			if(OrgaoId == null)
				OrgaoId = new ACSS.OrgaoBean() ;	 				
			String acao = req.getParameter("acao");  
			
			if(acao == null)
				acao = "";
			if(acao.equals("")){
				acao =" "; 
				statusInteracao = "1";
			}	

			
			// A��o realizada ap�s a sele��o de visualiza��o do relat�rio por parte do usu�rio.			  
			else if(acao.equalsIgnoreCase("visualizacao"))
			{
				// Recebe os par�metros passado pela interface com o usu�rio.
				String codOrgao = req.getParameter("codOrgao");
				String statusAuto = req.getParameter("statusAuto"); 				
				OrgaoId.Le_Orgao(codOrgao,0);
				
				try
				{
					Dao dao = Dao.getInstance();
					AutoInfracaoBean autoInfracao = new AutoInfracaoBean();
					
					if(statusAuto.equalsIgnoreCase("autuacao"))
						autoInfracao.setCodStatus("001");
					else if(statusAuto.equalsIgnoreCase("penalidade"))
						autoInfracao.setCodStatus("011");
					
					String prazo = req.getParameter("prazo");
					List totalAnosMeses = dao.consultaAIDigitPendenciaMes(autoInfracao, Integer.parseInt(prazo), codOrgao);
					
					req.setAttribute("totalAnosMeses",totalAnosMeses);
					req.setAttribute("statusAuto",statusAuto);
					req.setAttribute("prazo", prazo);
					req.setAttribute("codOrgao", codOrgao);
				}
				catch(Exception e){
					OrgaoId.setMsgErro("ERRO: ERRO DE ACESSO A BASE DE DADOS !\n" + e.getMessage());
				}
				next = "REG/AIDigitPendenciaMes.jsp";
			}
			/*
			 * A��o realizada a ap�s a op��o de detalhamento dos itens selecionados
			 * na apresenta��o do relat�rio
			 */
			else if(acao.equalsIgnoreCase("detalhe"))
			{				
				String detalhe = req.getParameter("detalhe");
				String statusAuto = req.getParameter("codStatusAuto");
				if (statusAuto == null) 
					statusAuto = req.getParameter("statusAuto");
				
				String totalMesAno = req.getParameter("totalMesAno");
				String codOrgao = req.getParameter("codOrgao");
				String sentido = req.getParameter("sentido");
				String prazoDigitado = req.getParameter("prazoDigitado");
				String primeiroRegistro = req.getParameter("primeiroRegistro");
				if (primeiroRegistro == null) primeiroRegistro = "";
				
				String primeiroAnterior = req.getParameter("primeiroAnterior");
				if (primeiroAnterior == null) primeiroAnterior = "";
				
				String ultimoRegistro = req.getParameter("ultimoRegistro");
				if (ultimoRegistro == null) ultimoRegistro = "";
				
				String numPag = req.getParameter("numPag");
				if (numPag == null) numPag = "0";
				
				String seq = req.getParameter("seq");

				
				int prazo = Integer.parseInt(prazoDigitado);				
				OrgaoId.Le_Orgao(codOrgao,0);				
				
				try{			
					Dao dao = Dao.getInstance();
					AutoInfracaoBean autoInfracao = (AutoInfracaoBean) req.getSession().getAttribute("autoInfracaoId");
					if( autoInfracao == null ) 
						autoInfracao = new AutoInfracaoBean();
					
					if(statusAuto.equalsIgnoreCase("autuacao"))
						autoInfracao.setCodStatus("001");
					else if(statusAuto.equalsIgnoreCase("penalidade"))
						autoInfracao.setCodStatus("011");
					
					autoInfracao.setCodOrgao(codOrgao);
					
					ParamSistemaBean parametro = (ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");
					String rowNum = parametro.getParamSist("LIMITE_REL_PENDENCIAS_AI");
					String qtdReg = rowNum;
					String rowNumImp = "";
					
					if (sentido == null) 
						sentido = "Prox";
					
//					 Alterado em 11.07.2006 por Marlon Falzetta.
					// � desejado que a impressao seja para todos os dados e nao
					// somente para os visualizados na tela.
					HashMap proxAnt = new HashMap();
					if(sentido.equals("Prox")){
						proxAnt.put("primeiroRegistro", primeiroRegistro);
						proxAnt.put("ultimoRegistro", ultimoRegistro);
						numPag = Integer.toString(Integer.parseInt(numPag)+1);
						if (seq == null) 
							seq = "1";
						else
					        seq = Integer.toString(Integer.parseInt(seq)+Integer.parseInt(qtdReg));
					}
					else{
						proxAnt.put("primeiroRegistro", ""); 
						proxAnt.put("ultimoRegistro", "");
						numPag = Integer.toString(Integer.parseInt(numPag)-1);
						seq = Integer.toString(Integer.parseInt(seq)-Integer.parseInt(qtdReg));
						
					}			
				
					proxAnt.put("primeiroAnterior", primeiroAnterior);				
					proxAnt.put("sentido", sentido);
					
					dao.consultaAIDigitPendenciaDet(autoInfracao, prazo, totalMesAno, rowNum, proxAnt, codOrgao, rowNumImp);
					
					req.setAttribute("numPag",numPag);
					req.setAttribute("seq",seq);
					req.setAttribute("opcao",detalhe);
					req.setAttribute("autoInfracaoId",autoInfracao);
					req.setAttribute("statusAuto",statusAuto);
					req.setAttribute("codOrgao",codOrgao);
					req.setAttribute("prazo", String.valueOf(prazo));
					req.setAttribute("totalMesAno",totalMesAno);
					req.setAttribute("prazoDigitado",prazoDigitado);
					req.setAttribute("primeiroAnterior",primeiroRegistro);
					  
					next = "/REG/AIDigitPendenciaDet.jsp";
					
					if (sentido.equals("Imprimir"))
					{
						next = "/REG/AIDigitPendenciaImp.jsp";
						String tituloConsulta = "RELAT�RIO - PEND�NCIAS DE DIGITALIZA��O DE AUTOS - " + meses.get(totalMesAno.substring(0, 2));
						rowNumImp = "-1";	
						dao.consultaAIDigitPendenciaDet(autoInfracao, prazo, totalMesAno, rowNum, proxAnt, codOrgao, rowNumImp);
						req.setAttribute("tituloConsulta", tituloConsulta);
					}
				}
				catch(Exception e){
					throw new sys.CommandException(e.getMessage());	
				}
			}
			req.setAttribute("statusInteracao",statusInteracao);
			req.setAttribute("OrgaoId",OrgaoId);
			req.setAttribute("meses",meses);
		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
