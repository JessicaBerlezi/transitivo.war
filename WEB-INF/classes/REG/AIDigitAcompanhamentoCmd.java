package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Pedro N�brega
 *
 */

public class AIDigitAcompanhamentoCmd extends sys.Command{
	private static final String jspPadrao = "/REG/AIDigitAcompanhamento.jsp";
	
	public AIDigitAcompanhamentoCmd(){
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
				
		String datIniRel = "";
		String datFimRel = "";
		try {
			AIDigitAcompanhamentoBean AIDigitBeanId = new AIDigitAcompanhamentoBean();
			
			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if ( acao.equals("") ) {				
				datFimRel = sys.Util.formatedTodayDDMMAAAA(); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
				
				AIDigitBeanId.setDatInicial(datIniRel);
				AIDigitBeanId.setDatFinal(datFimRel);
				req.setAttribute("AIDigitBeanId", AIDigitBeanId);
			
			} else if(acao.equals("ImprimeRelatorio")) {
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");
												
				String codOrgao = req.getParameter("codOrgao");
				
				ACSS.OrgaoBean orgao = new ACSS.OrgaoBean();
				
				orgao.Le_Orgao(codOrgao,0);
				AIDigitBeanId.setOrgao(orgao);				
				
				if ( (datIniRel==null || datFimRel == null) || (datIniRel.equals("")||datFimRel.equals("")) ) { 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
				}
				AIDigitBeanId.setDatInicial(datIniRel);
				AIDigitBeanId.setDatFinal(datFimRel);				
				
				if( AIDigitBeanId.consultaAIDigitAcompanhamento(vErro)) {
					String tituloConsulta = "ACOMPANHAMENTO DE DIGITALIZA��O DE AUTO: " + datIniRel + " a " + datFimRel + "<br>  ORG�O: " + orgao.getSigOrgao();
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/REG/AIDigitAcompanhamentoImp.jsp";
				} else {
					AIDigitBeanId.setDatInicial(datIniRel);
					AIDigitBeanId.setDatFinal(datFimRel);
					nextRetorno = "/REG/AIDigitAcompanhamento.jsp";
				}
				
				req.setAttribute("AIDigitBeanId", AIDigitBeanId);					
			}
		} catch (Exception e) {
			throw new sys.CommandException("AIDigitAcompanhamentoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}
