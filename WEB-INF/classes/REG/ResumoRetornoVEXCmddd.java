package REG;


import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;
import REC.AutoInfracaoBean;
import REC.CodigoRetornoBean;
import ROB.Dao;

import sys.BeanException;
import sys.Util;


/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */
public class ResumoRetornoVEXCmddd  extends sys.Command {
	private static final String jspPadrao="/REG/ResumoRetorno.jsp";    
	private String next;
	
	public ResumoRetornoVEXCmddd() {
		next = jspPadrao;
	}
	
	public ResumoRetornoVEXCmddd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
			
			REG.ResumoRetornoPagBean ResumoRetornoPagBeanId =(REG.ResumoRetornoPagBean) session.getAttribute("ResumoRetornoPagBeanId");
			if (ResumoRetornoPagBeanId == null)ResumoRetornoPagBeanId = new REG.ResumoRetornoPagBean();

			REG.ControleVexBean ControleVexBeanId =(REG.ControleVexBean) session.getAttribute("ControleVexBeanId");
			if (ControleVexBeanId == null)ControleVexBeanId = new REG.ControleVexBean();

			String datRetornoVex      = parSis.getParamSist("DAT_RETORNO_VEX");
			String datReferenciaVex   = parSis.getParamSist("MESANO_RET_VEX");			
			
			ResumoRetornoBean ResumoRetornoBeanId = (ResumoRetornoBean)session.getAttribute("ResumoRetornoBeanId");
			if (ResumoRetornoBeanId==null)  ResumoRetornoBeanId = new ResumoRetornoBean();
			ResumoRetornoBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			

			if  (acao.equals("detalhe") || acao.equals("detalheResumo"))  {
				
				session.removeAttribute("ResumoRetornoPagBeanId");
				ResumoRetornoPagBeanId =(REG.ResumoRetornoPagBean) session.getAttribute("ResumoRetornoPagBeanId");
				if (ResumoRetornoPagBeanId == null)ResumoRetornoPagBeanId = new REG.ResumoRetornoPagBean();
				 
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);

				ResumoRetornoPagBeanId.setAno(sNumAno);

				String numRetorno    = req.getParameter("numRetorno");
				if( numRetorno == null ) numRetorno = "";
				 
				String indRet    = req.getParameter("indRet");
				if( indRet == null ) indRet = "";

				
				String mes    = req.getParameter("mes");
				if( mes == null ) mes = "";
				
				ResumoRetornoPagBeanId.setMes(Util.lPad(mes,"0",2));
				
				int imesFinal = Integer.parseInt(mes);
				int ianoFinal = Integer.parseInt(sNumAno);

	            if (imesFinal == 12){
	            	imesFinal = 1;
	            	ianoFinal = ianoFinal + 1;
	            }else{
	            	imesFinal = imesFinal + 1;
	            }
				
				String datIni = datIni = "01" + "/" + String.valueOf(mes) + "/" + sNumAno; 
				String datFim = "01" + "/" + Util.lPad(String.valueOf(imesFinal),"0",2) + "/" + String.valueOf(ianoFinal);
				  
				String qtdRegPag = ParamOrgaoId.getParamOrgao("QUANT_REG_PAGINA","50","7");
				String datEnvio = parSis.getParamSist("MESANO_RET_VEX");
				
				ResumoRetornoPagBeanId.setDatEnvio("01/"+datEnvio);
				 
				ResumoRetornoPagBeanId.setDatInicio(datIni);
				ResumoRetornoPagBeanId.setDatFim(datFim);
				 
				ResumoRetornoPagBeanId.setIndRet(indRet);
				 
				ResumoRetornoPagBeanId.setCodTransacao(numRetorno);
				ResumoRetornoPagBeanId.setAcao(acao);
				
				String dscResumo    = req.getParameter("dscResumo");
				if( dscResumo == null ) dscResumo = "";
				
				if (acao.equals("detalhe")){
					
					if (dscResumo.equals("CR�DITO")){
						ResumoRetornoPagBeanId.setDscTransacao(dscResumo.trim());
						
					}else{
						ControleVexBeanId.setCodRetorno(numRetorno);
						ResumoRetornoPagBeanId.setDscTransacao(ControleVexBeanId.getDscCodRetorno());
					}
					
				}else{
					ResumoRetornoPagBeanId.setDscTransacao(dscResumo.trim());
					
				}

				ResumoRetornoPagBeanId.setQtd_Registros_Rodada(qtdRegPag);
				ResumoRetornoPagBeanId.setTransRef("0");  
				ResumoRetornoPagBeanId.setDatProcRef("0");  
				 
				ResumoRetornoPagBeanId.setQtdParcialReg("0");
				ResumoRetornoPagBeanId.setTotalRegistros(ResumoRetornoPagBeanId.BuscaTotalReg(acao));
				ResumoRetornoPagBeanId.CarregaLista(acao);
				nextRetorno="/REG/ResumoRetornoAnoRet_Det.jsp";
			}	
			
			 if (acao.equals("Prox")) {
				 
				 if (ResumoRetornoPagBeanId.getPrimTrans().size()>0) {
					 int tam = ResumoRetornoPagBeanId.getPrimTrans().size();
					 ResumoRetornoPagBeanId.setTransRef(ResumoRetornoPagBeanId.getUltTrans(tam-1));  
					 ResumoRetornoPagBeanId.setDatProcRef(ResumoRetornoPagBeanId.getUltDatProc(tam-1));  
				 } else{
					 ResumoRetornoPagBeanId.setTransRef("0");  
					 ResumoRetornoPagBeanId.setDatProcRef("0");  
				 }
					 
					 
				 ResumoRetornoPagBeanId.CarregaLista(acao);
				 nextRetorno="/REG/ResumoRetornoAnoRet_Det.jsp";
			 
			 }

			 if (acao.equals("Ant")) {
				 
				 int qtdParcial = 0;
				 int qtd = 0;

				 if (ResumoRetornoPagBeanId.getPrimTrans().size()>0) {
					 int tam = ResumoRetornoPagBeanId.getPrimTrans().size();
					 if (ResumoRetornoPagBeanId.getPrimTrans().size()==1) {
						 ResumoRetornoPagBeanId.setTransRef("0");  
						 ResumoRetornoPagBeanId.setDatProcRef("0");  
					 }else {
						 if (!ResumoRetornoPagBeanId.getQtdReg(tam-1).equals("Ultima")){
							 qtd = Integer.parseInt(ResumoRetornoPagBeanId.getQtdReg(tam-1));
							 qtdParcial = Integer.parseInt(ResumoRetornoPagBeanId.getQtdParcialReg());
	
							 qtdParcial = qtdParcial - qtd;
							 ResumoRetornoPagBeanId.setQtdParcialReg(String.valueOf(qtdParcial));
						 }
						 
						 ResumoRetornoPagBeanId.delPrimTrans(tam-1);
						 ResumoRetornoPagBeanId.delPrimDatProc(tam-1);
						 ResumoRetornoPagBeanId.delUltTrans(tam-1);
						 ResumoRetornoPagBeanId.delUltDatProc(tam-1);
						 ResumoRetornoPagBeanId.delQtdReg(tam-1);
						 
						 tam = ResumoRetornoPagBeanId.getPrimTrans().size();
						 if (ResumoRetornoPagBeanId.getPrimTrans().size()==1) {
							 ResumoRetornoPagBeanId.setTransRef("0");  
							 ResumoRetornoPagBeanId.setDatProcRef("0");  
						 }else {

						 ResumoRetornoPagBeanId.setTransRef(ResumoRetornoPagBeanId.getPrimTrans(tam-1));  
						 ResumoRetornoPagBeanId.setDatProcRef(ResumoRetornoPagBeanId.getPrimDatProc(tam-1));  

						 }
					 }
				 }
				 
				 ResumoRetornoPagBeanId.CarregaLista(acao);
				 nextRetorno="/REG/ResumoRetornoAnoRet_Det.jsp";
			 }

			if  (acao.equals("consultarAno"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				String statusAuto = req.getParameter("statusAuto");
				if (statusAuto==null) statusAuto="";

				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.setDatContrato(datReferenciaVex);
				
				if(statusAuto.equalsIgnoreCase("autuacao")) ResumoRetornoBeanId.setIndFase("001");
				else if(statusAuto.equalsIgnoreCase("penalidade")) ResumoRetornoBeanId.setIndFase("011");
                
				/*Preparar Estrutura do Relat�rio*/
				montarEstruturaRelatorio(ResumoRetornoBeanId);
				ResumoRetornoBeanId.LeRetornos();
				montarPercentuais(ResumoRetornoBeanId);
				/*Apresenta��o*/
				String tituloConsulta = null;
				/*
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+datRetornoVex+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				*/
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				nextRetorno = "/REG/ResumoRetornoAnoRettt.jsp";
				req.setAttribute("tituloConsulta", tituloConsulta);
			}

			if  (acao.equals("imprimirAno"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				String statusAuto = req.getParameter("statusAuto");
				if (statusAuto==null) statusAuto="";

				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.setDatContrato(datReferenciaVex);
				
				if(statusAuto.equalsIgnoreCase("autuacao")) ResumoRetornoBeanId.setIndFase("001");
				else if(statusAuto.equalsIgnoreCase("penalidade")) ResumoRetornoBeanId.setIndFase("011");
                
				/*Preparar Estrutura do Relat�rio*/
				montarEstruturaRelatorio(ResumoRetornoBeanId);
				ResumoRetornoBeanId.LeRetornos();
				montarPercentuais(ResumoRetornoBeanId);
				/*Apresenta��o*/
				String tituloConsulta = null;
				/*
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+datRetornoVex+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				*/
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				nextRetorno = "/REG/ResumoRetornoAnoRetImp.jsp";
				req.setAttribute("tituloConsulta", tituloConsulta);
			}
			
			if  (acao.equals("Imprimir"))  {
				nextRetorno = "/REG/ResumoRetornoAnoRet_Imp.jsp";
			}
			
			// mostra AR Digitalizado		  			  	
			if  (acao.equals("ARDigitalizado"))  {
			  	
				AutoInfracaoBean  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
				PNT.ProcessoBean  ProcessoBeanId = new PNT.ProcessoBean() ;	  			
				
			  	String numNotificacao = req.getParameter("numNotificacao");
	  			if (numNotificacao == null) numNotificacao = "";
	  			
				
				String seqNotif = req.getParameter("seqNotif");
	  			if (seqNotif == null) seqNotif = "";

	  			
				String numAutoInfracao = req.getParameter("numAutoInfracao");
	  			if (numAutoInfracao == null) numAutoInfracao = "";
	  			
	  			if (numAutoInfracao.equals("")){
	  				String[] campos = new String[0];
	  				String[] valores = new String[0];

	  				campos  = new String[] {"NUM_NOTIFICACAO", "SEQ_NOTIFICACAO"};
					valores = new String[] {"'"+numNotificacao+"'", "'"+seqNotif+"'"};
					
					PNT.ARDigitalizadoBean ARDigitalizadoBeanId = new PNT.ARDigitalizadoBean();
                     
					Vector vetARs = new Vector();   
					vetARs.add(ARDigitalizadoBeanId.consultaAR(campos,valores));

					ProcessoBeanId.setARDigitalizado(vetARs);
					nextRetorno = "/REG/ResumoRetornoAnoRet_FotoAR_PNT.jsp" ;

	  			}else{
		  			AutoInfracaoBeanId.setNumNotificacao(numNotificacao);
					AutoInfracaoBeanId.LeARDigitalizado() ;	
					nextRetorno = "/REG/ResumoRetornoAnoRet_FotoAR.jsp" ;
	  			}
	  			
			  	
			  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;	
			  	req.setAttribute("ProcessoBeanId",ProcessoBeanId) ;	
			  	
			}


			session.setAttribute("ResumoRetornoPagBeanId",ResumoRetornoPagBeanId) ;  
			session.setAttribute("ResumoRetornoBeanId",ResumoRetornoBeanId) ; 
		
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("ResumoRetornoCmd: " + ue.getMessage());
		}
	}
	

	public void montarEstruturaRelatorio(ResumoRetornoBean ResumoRetornoBeanId)throws BeanException
	{
		/*Montar Totais Zerados de Enviados/N�o Retornados/Retornados*/
		ArrayList <ResumoRetornoBean>myListResumo = new ArrayList<ResumoRetornoBean>();
		for (int i=0;i<12;i++)
		{
			ResumoRetornoBean  myResumo = new ResumoRetornoBean();
			myResumo.setMes(Util.lPad(String.valueOf(i+1),"0",2));
			myListResumo.add(myResumo);
		}
		ResumoRetornoBeanId.setListRetornoMes(myListResumo);
		/*Montar Lista de C�digo de Retornos*/
		montarListaCodigoRetorno(ResumoRetornoBeanId,true);  /*Montar lista de c�digos de entrega="S"*/
		montarListaCodigoRetorno(ResumoRetornoBeanId,false); /*Montar lista de c�digos de entrega="N"*/
	}
	
	public void montarListaCodigoRetorno(ResumoRetornoBean ResumoRetornoBeanId, boolean bTipEntregue)throws BeanException
	{
		CodigoRetornoBean myRetorno= new CodigoRetornoBean();
		for (int i=0;i<myRetorno.getRetorno(0,0).size();i++) {
			if (myRetorno.getRetorno(i).getNumRetorno().trim().equals("09")) continue; /*Retirar Michel*/
			
			ResumoRetornoBean  myCodigoResumo = new ResumoRetornoBean();
			myCodigoResumo.getRetorno().setNumRetorno(myRetorno.getRetorno(i).getNumRetorno());
			myCodigoResumo.getRetorno().setDescRetorno(myRetorno.getRetorno(i).getDescRetorno());
			myCodigoResumo.getRetorno().setIndEntregue(bTipEntregue);			
			
			ArrayList <ResumoRetornoBean>myListCodigoResumo = new ArrayList<ResumoRetornoBean>();
			for (int z=0;z<12;z++)
			{
				ResumoRetornoBean  myCodResumo = new ResumoRetornoBean();
				myCodResumo.setMes(Util.lPad(String.valueOf(z+1),"0",2));
				myListCodigoResumo.add(myCodResumo);
			}
			myCodigoResumo.setListRetornoMes(myListCodigoResumo);				
			ResumoRetornoBeanId.getListCodigoRetornoMes().add(myCodigoResumo);
		}
	}
	
	public void montarPercentuais(ResumoRetornoBean ResumoRetornoBeanId)throws BeanException
	{
		BigDecimal bPercentual = new BigDecimal (0);
		/*Calcular Percentuais-C�digo de Retorno - Mensal*/
		for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
		{    
			for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++)
			{
				if (ResumoRetornoBeanId.getRetornos(j).getQtdTotalRetornada().equals("0"))
				  continue;
					
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(j).getQtdTotalRetornada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).setPercRetornada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-C�digo de Retorno - Geral*/			
			if (!ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getQtdTotalRetornada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getCodigoRetornos(i).setPercRetornada(String.valueOf(bPercentual));
			}
		}
		/*Calcular Percentuais-Enviados/Retornados/N�o Retornados-Geral*/
		for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
			/*Calcular Percentuais-Enviados-Geral*/
			if (!ResumoRetornoBeanId.getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercEnviada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-Retornados-C�digo de Retorno - Geral*/
			if (!ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercRetornada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-N�o Retornados-Geral*/
			if (!ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalnaoRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercnaoRetornada(String.valueOf(bPercentual));
			}
		}
	}
	
}






