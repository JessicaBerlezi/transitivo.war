package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Agente<br>
* <b>Description:</b>  Comando para Consultar Agentes<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class AgenteCmd extends sys.Command {

	private static final String jspPadrao = "/REG/AgenteConsulta.jsp";

	private String next;

	public AgenteCmd() {
		next = jspPadrao;
	}

	public AgenteCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {

		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();
		Vector vIdent = new Vector();
		Vector vMat = new Vector();
		Vector vCPF = new Vector();
		Vector vUnid = new Vector();
		Vector vCodInt = new Vector();

		try {
			TAB.AgenteBeanCol Agbc = new TAB.AgenteBeanCol();
			Agbc.carregarCol("TODOS",vCod,vNom);
			req.setAttribute("AgenteBeanColId", Agbc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;

	}

}
