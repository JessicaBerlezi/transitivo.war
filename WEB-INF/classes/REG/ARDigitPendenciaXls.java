package REG;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class ARDigitPendenciaXls {
	
	public void write(NotifControleBean NotifControleId, String titulo, String arquivo) throws IOException, WriteException
	{
		int seq = 1;
		int celulaTitulo = 0;
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio - Pend�ncias de Digitaliza��o de AR", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableFont fonteCampos = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.NO_BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoCampos = new WritableCellFormat(fonteCampos);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			formatoCampos.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 9/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);

		    //T�tulo do Relat�rio
		    label = new Label(0, 2, "SEQ", formatoTituloCampos);
		    sheet.addCell(label);
			sheet.setColumnView(0, 10);

			label = new Label(1, 2, "NOTIFICA��O", formatoTituloCampos);
		    sheet.addCell(label);
			sheet.setColumnView(1, 20);
			
			label = new Label(2, 2, "AUTO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 10);
			
			label = new Label(3, 2, "PLACA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 10);
			
			label = new Label(4, 2, "ARQUIVO RECEBIDO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 50);
			
			label = new Label(5, 2, "EMISS�O", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 15);
			
			label = new Label(6, 2, "ENVIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(6, 15);
			
			label = new Label(7, 2, "USU�RIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(7, 20);
			
			label = new Label(8, 2, "ENTREGA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(8, 15);

	
            //Linhas do Relat�rio			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Iterator it = NotifControleId.getListaNotifs().iterator();
			NotifControleBean relatorioImp = new NotifControleBean();
			seq = 1;
			int contLinha = 3;
			while (it.hasNext()) {
			   relatorioImp =(NotifControleBean)it.next();
			   
			   label = new Label(0, contLinha, String.valueOf(seq),formatoCampos); 
			   sheet.addCell(label);
			   
			   label = new Label(1, contLinha, relatorioImp.getNumNotificacao(),formatoCampos); 
			   sheet.addCell(label);
			   
			   label = new Label(2, contLinha, relatorioImp.getNumAutoInfracao(),formatoCampos); 
			   sheet.addCell(label);
			   
			   label = new Label(3, contLinha, relatorioImp.getNumPlaca(),formatoCampos); 
			   sheet.addCell(label);
			   
			   label = new Label(4, contLinha, relatorioImp.getNomArquivo(relatorioImp.getCodArquivoEnvio()),formatoCampos); 
			   sheet.addCell(label);

			   if(relatorioImp.getDatEmissao() != null)
			   {
				   label = new Label(5, contLinha, sdf.format(relatorioImp.getDatEmissao()),formatoCampos); 
				   sheet.addCell(label);
			   }
			   
			   if(relatorioImp.getDatEnvio() != null)
			   {
				   label = new Label (6, contLinha, sdf.format(relatorioImp.getDatEnvio()),formatoCampos);
				   sheet.addCell(label);
			   }
			   
			   label = new Label (7, contLinha, relatorioImp.getNomUsuarioEnvio()+"/"+relatorioImp.getSigCodOrgao(relatorioImp.getCodOrgaoEnvio()),formatoCampos);
			   sheet.addCell(label);
			   
			   if(relatorioImp.getDatEntrega() != null)
			   {
				   label = new Label (8, contLinha, sdf.format(relatorioImp.getDatEntrega()),formatoCampos);
				   sheet.addCell(label);
			   }

			   seq++;
			   contLinha++;
			}
			workbook.write(); 
			workbook.close();
			
			
		}catch (Exception e) {	
			throw new IOException("GeraXlsARDigitPendencia: " + e.getMessage());	}
	
	}

}
