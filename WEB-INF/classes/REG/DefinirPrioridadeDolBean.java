package REG;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import ROB.DaoException;



/**
* <b>Title:</b>        Definir Prioridade - Bean<br>
* <b>Description:</b>  Permite definir indice de prioridade do processamento do DOL<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
 */
public class DefinirPrioridadeDolBean extends ArquivoDolBean{
	
	private List dados;
	private Vector indPriori;
	
	public DefinirPrioridadeDolBean() throws sys.BeanException{
		super(); 
		dados = new ArrayList();
		indPriori = new Vector();	
	}

	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setVetorPrioridade(Vector indPriori)  {
		this.indPriori = indPriori ;
	} 
	public Vector getVetorPrioridade(){
		return  this.indPriori;
	}
	
	public String getCodStatusExtenso() {		
		String extenso = "";
		
		if (this.getCodStatus().equals("0"))
			extenso = "Recebido";
		else if (this.getCodStatus().equals("7") || this.getCodStatus().equals("8"))
			extenso = "Em Processamento";	
			
		return extenso;
	}
	
/**
* M�todo responsavel por consultar os arquivos 
* com status 0, 7 e 8 
* @param bean
 * @throws DaoException 
*/
	  public void consultaStatusArquivo(DefinirPrioridadeDolBean bean) throws sys.BeanException, DaoException{
		  try {
			 Dao dao = Dao.getInstance();
			 if(dao.consultaStatusArquivoDol(bean)==false)
			   this.setMsgErro("Erro ao efetuar consulta do Status do Arquivo");
			 
		  } 
		  catch (DaoException e) {
			throw new sys.BeanException(e.getMessage());
		  }
	  }

/**
* M�todo respons�vel por alterar o
* indice de prioridade do processamento
* do arquivo.
* @param bean
*/
	  public void alterarIndicePrioridade() throws sys.BeanException{
		  try {
			  Dao dao = Dao.getInstance();
			  if (dao.alterarIndicePrioridadeDol(this)== false)
				this.setMsgErro("Erro ao efetuar altera��o do indice de prioridade do Arquivo");
			  else 
				this.setMsgErro("Atualiza��o realizada com sucesso!");
		  } 
		  catch (DaoException e) {
			throw new sys.BeanException(e.getMessage());
		  }
	  }
   	




}   
