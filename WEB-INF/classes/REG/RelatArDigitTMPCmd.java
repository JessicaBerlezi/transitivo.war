package REG;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.CommandException;
import sys.Util;

/**
* <b>Title:</b>        Controle de Acesso - Consulta de Hist�ricos<br>
* <b>Description:</b>  Consulta de Hist�ricos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class RelatArDigitTMPCmd extends sys.Command {

	private static final String jspPadrao = "/REG/RelatArDigitTMP.jsp";
	private String next;

	public RelatArDigitTMPCmd() {
		next = jspPadrao;
	}

	public RelatArDigitTMPCmd(String next) {
		this.next = next;
	}
      
	public String execute(HttpServletRequest req) throws sys.CommandException {
	  String nextRetorno  = next ;
	  try {      	
			nextRetorno = Auditoria(req,nextRetorno);
	  }
	  catch (Exception ue) {
		throw new sys.CommandException("AuditoriaCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
	}
  
	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res) throws sys.CommandException 
		{
		String nextRetorno = next;
		try {
			
			nextRetorno = Auditoria(req,nextRetorno);
		}
		catch (Exception ue) {
			throw new sys.CommandException("AuditoriaCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

  public String Auditoria(HttpServletRequest req, String nextRetorno) throws CommandException{
	  try {
	       //	cria os Beans, se n�o existir
			 HttpSession session = req.getSession();
			 
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");

			 ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			 if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			 ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) session.getAttribute("OrgaoBeanId");
			 if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			 
	    	 REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    	 if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			

			 
			 ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		    	if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
			 
			 ACSS.UsuarioBean UsrBeanId =(ACSS.UsuarioBean) req.getAttribute("UsrBeanId");
			 if (UsrBeanId == null)UsrBeanId = new ACSS.UsuarioBean();


			 RelatArDigitTMPBean RelatArDigitTMPBeanId =(RelatArDigitTMPBean) session.getAttribute("RelatArDigitTMPBeanId");
			 if (RelatArDigitTMPBeanId == null)RelatArDigitTMPBeanId = new RelatArDigitTMPBean();
		
			 // obtem e valida os parametros recebidos
			 String acao = req.getParameter("acao");
			 if (acao == null) acao = " ";
			 
			 String j_sigFuncao = req.getParameter("j_sigFuncao");
	    	 if (j_sigFuncao==null) j_sigFuncao="";
	    	 else
	    		 j_sigFuncao=j_sigFuncao.trim();
	    	 
	    	 

			 String datIni = req.getParameter("datIni"); 
			 if(datIni == null) datIni = "";  
			 String datFim = req.getParameter("datFim"); 
			 if(datFim == null) datFim = "";  
			 String datEnvio = req.getParameter("datEnvio"); 
			 if(datEnvio == null) datEnvio = "";  
			 
			 
			 if (acao.equals("buscaARPeriodo")) {
				 
				RelatArDigitTMPBeanId.setDatInicio(datIni);
				RelatArDigitTMPBeanId.setDatFim(datFim);
			    RelatArDigitTMPBeanId.buscaARPeriodo();
			    nextRetorno="/REG/RelatArDigitTMP.jsp";

			 }

			 if (acao.equals(" ")) {

				 session.removeAttribute("RelatArDigitTMPBeanId");
				 RelatArDigitTMPBeanId =(RelatArDigitTMPBean) session.getAttribute("RelatArDigitTMPBeanId");
				 if (RelatArDigitTMPBeanId == null)RelatArDigitTMPBeanId = new RelatArDigitTMPBean();
				 OrgaoBeanId.setCodOrgao("0");
//				 RelatArDigitTMPBeanId.buscaEventos();
			 }			 
			 
			 if (acao.equals("B")) {

				 session.removeAttribute("RelatArDigitTMPBeanId");
				 RelatArDigitTMPBeanId =(RelatArDigitTMPBean) session.getAttribute("RelatArDigitTMPBeanId");
				 if (RelatArDigitTMPBeanId == null)RelatArDigitTMPBeanId = new RelatArDigitTMPBean();

				 String codEvento = req.getParameter("codEvento");
				 if (codEvento == null)	codEvento = "0";
				 
				 // obtem e valida os parametros recebidos
				 String sIN = "";
				 
				 if (codEvento.equals("R")) {
					 RelatArDigitTMPBeanId.setDscTransacao("Retornadas");
					 sIN = "";
				 }
				 if (codEvento.equals("A")){
					 RelatArDigitTMPBeanId.setDscTransacao("Apropriadas");
					 sIN = "'A'";
				 }
				 if (codEvento.equals("D")){//Desconsideradas
					 RelatArDigitTMPBeanId.setDscTransacao("Desconsideradas");
					 sIN = "'D'";
				 }

				 if (codEvento.equals("N")){
					 RelatArDigitTMPBeanId.setDscTransacao("N�o Processadas");
					 sIN = "'N'";
				 }

				 
				 RelatArDigitTMPBeanId.setsIN(sIN);
				 String qtdRegPag = ParamOrgaoId.getParamOrgao("QUANT_REG_PAGINA","50","7");
				
				 //Carrega Bean AuditoriaBean
				 RelatArDigitTMPBeanId.setDatEnvio(datEnvio);
				 
				 
				 //Pegar valor o Par�metro;
				 RelatArDigitTMPBeanId.setQtd_Registros_Rodada(qtdRegPag);
				 RelatArDigitTMPBeanId.setTransRef("0");  
				 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getDatEnvio());  
				 
				 RelatArDigitTMPBeanId.setQtdParcialReg("0");
				 RelatArDigitTMPBeanId.setTotalRegistros(RelatArDigitTMPBeanId.BuscaTotalReg());
				 RelatArDigitTMPBeanId.CarregaLista(acao);
				 nextRetorno="/REG/RelatArDigitTMP_Lista_Corpo.jsp";
			 }
			 
			 if (acao.equals("Prox")) {
				 
				 if (RelatArDigitTMPBeanId.getPrimTrans().size()>0) {
					 int tam = RelatArDigitTMPBeanId.getPrimTrans().size();
					 RelatArDigitTMPBeanId.setTransRef(RelatArDigitTMPBeanId.getUltTrans(tam-1));  
					 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getUltDatProc(tam-1));  
				 } else{
					 RelatArDigitTMPBeanId.setTransRef("0");  
					 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getDatInicio());  
				 }
					 
					 
				 RelatArDigitTMPBeanId.CarregaLista(acao);
				 nextRetorno="/REG/RelatArDigitTMP_Lista_Corpo.jsp";
			 
			 }

			 if (acao.equals("Ant")) {
				 
				 int qtdParcial = 0;
				 int qtd = 0;

				 if (RelatArDigitTMPBeanId.getPrimTrans().size()>0) {
					 int tam = RelatArDigitTMPBeanId.getPrimTrans().size();
					 if (RelatArDigitTMPBeanId.getPrimTrans().size()==1) {
						 RelatArDigitTMPBeanId.setTransRef("0");  
						 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getDatInicio());  
					 }else {
						 if (!RelatArDigitTMPBeanId.getQtdReg(tam-1).equals("Ultima")){
							 qtd = Integer.parseInt(RelatArDigitTMPBeanId.getQtdReg(tam-1));
							 qtdParcial = Integer.parseInt(RelatArDigitTMPBeanId.getQtdParcialReg());
	
							 qtdParcial = qtdParcial - qtd;
							 RelatArDigitTMPBeanId.setQtdParcialReg(String.valueOf(qtdParcial));
						 }
						 
						 RelatArDigitTMPBeanId.delPrimTrans(tam-1);
						 RelatArDigitTMPBeanId.delPrimDatProc(tam-1);
						 RelatArDigitTMPBeanId.delUltTrans(tam-1);
						 RelatArDigitTMPBeanId.delUltDatProc(tam-1);
						 RelatArDigitTMPBeanId.delQtdReg(tam-1);
						 
						 tam = RelatArDigitTMPBeanId.getPrimTrans().size();
						 if (RelatArDigitTMPBeanId.getPrimTrans().size()==1) {
							 RelatArDigitTMPBeanId.setTransRef("0");  
							 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getDatInicio());  
						 }else {

						 RelatArDigitTMPBeanId.setTransRef(RelatArDigitTMPBeanId.getPrimTrans(tam-1));  
						 RelatArDigitTMPBeanId.setDatProcRef(RelatArDigitTMPBeanId.getPrimDatProc(tam-1));  

						 }
					 }
				 }
				 
				 RelatArDigitTMPBeanId.CarregaLista(acao);
				 nextRetorno="/REG/RelatArDigitTMP_Lista_Corpo.jsp";
			 }
			 
			 
			 if (acao.equals("Imprimir")) {
				String NomTitulo =  UsuarioFuncBeanId.getJ_nomFuncao();
				req.setAttribute("tituloConsulta", NomTitulo);
			    nextRetorno="/REG/RelatArDigitTMP_Imp.jsp";
			 }
			 

			 if (acao.equals("ImprimirTotal")) {
					RelatArDigitTMPBeanId.setDatInicio(datIni);
					RelatArDigitTMPBeanId.setDatFim(datFim);
				    RelatArDigitTMPBeanId.buscaARPeriodo();
					String NomTitulo =  UsuarioFuncBeanId.getJ_nomFuncao();
					req.setAttribute("tituloConsulta", NomTitulo);
				    nextRetorno="/REG/RelatArDigitTMP_ImpTot.jsp";
				 }
			 
			 
			 if (acao.equals("buscaArDigitTMP")) {
				 // obtem e valida os parametros recebidos
				 String numNotif = req.getParameter("numNotif");
				 if (numNotif == null) numNotif = "";
				 RelatArDigitTMPBeanId.CarregaListaDet(numNotif);
				 nextRetorno="/REG/RelatArDigitTMP_Lista_CorpoDet.jsp";

			 }

			 if (acao.equals("ImprimirDet")) {
					String NomTitulo =  UsuarioFuncBeanId.getJ_nomFuncao();
					req.setAttribute("tituloConsulta", NomTitulo);
				    nextRetorno="/REG/RelatArDigitTMP_ImpDet.jsp";
				 }
			 
			 session.setAttribute("RelatArDigitTMPBeanId",RelatArDigitTMPBeanId);
	  
	  }
		 catch (Exception ue) {
			  throw new sys.CommandException("AuditoriaCmd: "+ ue.getMessage());
		 }			 
		 return nextRetorno;
  }
  

}