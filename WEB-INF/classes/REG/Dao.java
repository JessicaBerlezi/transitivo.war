package REG;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import sys.BeanException;
import sys.Util;
import ACSS.UsuarioBean;
import PNT.HistoricoBean;
import PNT.NotificacaoBean;
import PNT.NOT.DaoFactory;
import REC.AutoInfracaoBean;
import ROB.DaoException;

/**
 * <b>Title:</b>        SMIT - Acesso a Base de Dados<br>
 * <b>Description:</b>  Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luiz Medronho
 * @version 1.0
 */

public class Dao {
	
	private static final String MYABREVSIST = "REG";
	private static Dao instance;
	private sys.ServiceLocator serviceloc ;  
	
	public static Dao getInstance()
	throws DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}
	
	private Dao() throws DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	public boolean ArquivoInsert(ArquivoRecebidoBean auxClasse,REC.ParamOrgBean ParamOrgaoId, Connection conn)
	throws DaoException {
		String sCmd="";
		boolean existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			sCmd     = "SELECT COD_ARQUIVO from TSMI_ARQUIVO_RECEBIDO "+
			"WHERE NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "'" +
			" AND COD_STATUS <> '9'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				auxClasse.setCodStatus("9");
			}
			if ( ((!"9".equals(auxClasse.getCodStatus())) &&
					(auxClasse.getTamNumControle()>0)) ||
					(auxClasse.getCodIdentArquivo().equals("TVEP")) ||
					(auxClasse.getCodIdentArquivo().equals("XXX01") || auxClasse.getCodIdentArquivo().equals("JULG/DET") ||
							(auxClasse.getCodIdentArquivo().equals("PAGOS/VEX")    )       )) //Bacalhau do Bahia 
			{
				if ( (auxClasse.getCodIdentArquivo().equals("PAGOS/VEX")) || (auxClasse.getCodIdentArquivo().equals("JULG/DET")) ) 
				{
					// buscar o numero do proximo arquivo e atualizar
					sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
					" WHERE NOM_PARAMETRO = 'PROXIMO_PROCESSAMENTO_"+auxClasse.getCodIdentArquivo()+"' AND COD_ORGAO = '"
					+ auxClasse.getCodOrgaoAtuacao() + "' FOR UPDATE";
					stmt = conn.createStatement();          
					rs  = stmt.executeQuery(sCmd) ;
					String sProxMes = "";
					String sMes = "";
					String sAno = "";
					while (rs.next()) {
						sProxMes = rs.getString("VAL_PARAMETRO");     
						if(sProxMes.length()< 6) sProxMes = "0"+sProxMes;
						sMes=sProxMes.substring(0,2);
						sAno=sProxMes.substring(2,6);
						if ("12".equals(sMes))
						{
							sMes = "01";
							sAno = String.valueOf(Integer.parseInt(sAno)+1);
						}else
						{
							sMes =  String.valueOf(Integer.parseInt(sMes)+1);
							if (sMes.length()==1)
								sMes="0"+sMes;
						}
					}
					sCmd = "UPDATE TCAU_PARAM_ORGAO SET VAL_PARAMETRO = '" + sMes+sAno + "'"
					+ " WHERE NOM_PARAMETRO = 'PROXIMO_PROCESSAMENTO_"+auxClasse.getCodIdentArquivo()+"' AND COD_ORGAO = '"
					+ auxClasse.getCodOrgaoAtuacao() + "'";
					stmt.execute(sCmd);
					ParamOrgaoId.setValParamOrgao("PROXIMO_PROCESSAMENTO_"+auxClasse.getCodIdentArquivo(),sMes + sAno) ;
				}
				else
				{
					// buscar o numero do proximo arquivo e atualizar
					sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
					" WHERE NOM_PARAMETRO = 'PROXIMO_"+auxClasse.getCodIdentArquivo()+"' AND COD_ORGAO = '"
					+ auxClasse.getCodOrgao() + "' FOR UPDATE";
					stmt = conn.createStatement();          
					rs  = stmt.executeQuery(sCmd) ;
					int numUltSeq = 0;
					boolean naoExisteNumeracao  = true ;
					while (rs.next()) {
						numUltSeq = rs.getInt("VAL_PARAMETRO");              
						naoExisteNumeracao  = false;              
					}
					// atualizar numero de sequencia do arquivo
					String nSeq = "";
					
					//Bacalhau do Bahia
					if (auxClasse.getCodIdentArquivo().equals("TVEP")) {
						nSeq = sys.Util.lPad(String.valueOf(numUltSeq),"0",5);
						auxClasse.setNumControleArq(nSeq);
						auxClasse.setQtdReg(String.valueOf(auxClasse.getLinhaArquivoRec().size()));
					} else    
						nSeq = sys.Util.lPad(String.valueOf(numUltSeq),"0",auxClasse.getTamNumControle());
					

					/*N�o Criticar Numero de Controle EMITEVEX*/
//					if ( (auxClasse.getNumControleArq().equals(nSeq)==false) && (auxClasse.getCodIdentArquivo().equals("EMITEVEX")==false) && (auxClasse.getCodIdentArquivo().equals("EMITEPER")==false) ) {						auxClasse.setCodStatus("9");
//						auxClasse.getErroArquivo().addElement("Sequencia do Arquivo "+auxClasse.getNomArquivo()+" deveria ser: "+nSeq);
//					}
					
					
					
					
					
					if ((naoExisteNumeracao))   {
						auxClasse.setCodStatus("9");
						auxClasse.getErroArquivo().addElement("Parametros para numera��o do arquivo "+auxClasse.getNomArquivo()+" n�o localizado para o Org�o: "+auxClasse.getCodOrgao());
					}
					else {
						numUltSeq++;
						
						if (auxClasse.getCodIdentArquivo().equals("TVEP")) {
							nSeq = sys.Util.lPad(String.valueOf(numUltSeq),"0",5);
						} else    
							nSeq = sys.Util.lPad(String.valueOf(numUltSeq),"0",auxClasse.getTamNumControle());
						
						sCmd = "UPDATE TCAU_PARAM_ORGAO SET VAL_PARAMETRO = '" + nSeq + "'"
						+ " WHERE NOM_PARAMETRO = 'PROXIMO_" + auxClasse.getCodIdentArquivo() + "' AND COD_ORGAO = '"
						+ auxClasse.getCodOrgao() + "'";
						stmt.execute(sCmd);
						ParamOrgaoId.setValParamOrgao("PROXIMO_"+auxClasse.getCodIdentArquivo(),nSeq) ;                         
					}    
				}
			}
			
			// buscar codigo do arquivo
			sCmd  = "SELECT SEQ_TSMI_ARQUIVO_RECEBIDO.NEXTVAL COD_ARQUIVO FROM DUAL";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				auxClasse.setPkid(auxClasse.getCodArquivo());
			}
			// inserir o arquivo na tabela de controle
			sCmd  = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (COD_ARQUIVO,"+
			"NOM_ARQUIVO,DAT_RECEBIMENTO,DAT_PROCESSAMENTO_DETRAN,COD_IDENT_ARQUIVO, "+
			"NUM_ANO_EXERCICIO, NUM_CONTROLE_ARQUIVO,NUM_PROTOCOLO, "+
			"IND_EMISSAO_NOTIF_DETRAN, QTD_REG,COD_ORGAO,"+ 
			"COD_ORGAO_LOTACAO,SIG_ORGAO_LOTACAO,NOM_USERNAME,COD_STATUS,"+
			"NOM_RESPONSAVEL,IND_P59,COD_IDENT_MOVIMENTO,IND_PRIORIDADE)"+
			"VALUES (" + auxClasse.getCodArquivo() + ", " +
			"'"+auxClasse.getNomArquivo()+"', "+
			"to_date('"+auxClasse.getDatRecebimento()+"','dd/mm/yyyy'), " +
			"to_date('"+auxClasse.getDatProcessamentoDetran()+"','dd/mm/yyyy'), " +
			"'"+auxClasse.getCodIdentArquivo()+"', " +
			"'"+auxClasse.getNumAnoExercicio()+"', " +
			"'"+auxClasse.getNumControleArq()+"', " +
			"'"+auxClasse.getNumProtocolo()+"', " +
			"'"+auxClasse.getIndEmissaoNotifDetran()+"', " +
			"'"+auxClasse.getQtdReg()+"', " +
			"'"+auxClasse.getCodOrgao()+"', " +
			"'"+auxClasse.getCodOrgaoLotacao()+"', " +
			"'"+auxClasse.getSigOrgaoLotacao()+"', " +
			"'"+auxClasse.getNomUsername()+"', " +
			"'"+auxClasse.getCodStatus()+"', " +
			"'"+auxClasse.getResponsavelArq()+"', " +
			"'"+auxClasse.getDscPortaria59()+"', " +
			"'"+auxClasse.getIdentMovimento()+"', 1)";
			stmt.execute(sCmd);
			if ("9".equals(auxClasse.getCodStatus())==false) {          
				// Inserir as linhas
				Iterator it  = auxClasse.getLinhaArquivoRec().iterator() ;  
				int num_seq_linha = 0;
				LinhaArquivoRec linha;
				while (it.hasNext()) {
					num_seq_linha++;
					linha = (LinhaArquivoRec) it.next();
					linha.setNumSeqLinha(String.valueOf(num_seq_linha));
					linha.carregarCampos(auxClasse);
					sCmd  = "INSERT INTO TSMI_LINHA_ARQUIVO_REC (COD_LINHA_ARQ_REC,DSC_LINHA_ARQ_REC,"
						+ "NUM_SEQ_LINHA,COD_ARQUIVO,COD_STATUS,NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,COD_ORGAO)"
						+ " VALUES (SEQ_TSMI_LINHA_ARQUIVO_REC.NEXTVAL, "
						+ "'"+linha.getDscLinhaArqRec().trim()+"','"+linha.getNumSeqLinha()+"',"+auxClasse.getCodArquivo()
						+ ",0,'"+linha.getNumAutoInfracao()+"','"+linha.getNumNotificacao()+"','"+linha.getCodOrgao()+"')";
					stmt.execute(sCmd);
					

/*Bahia 23/09/2008*/
					if (auxClasse.getCodIdentArquivo().equals("CODRET")){
						// atualiza tabela tsmi_ar_digitalizado_tmp cod o cod. tsmi_linha_arquivo_recebido
						sCmd  = "SELECT SEQ_TSMI_LINHA_ARQUIVO_REC.CURRVAL COD_AR FROM DUAL";
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							atualizaArDigTMP(rs.getString("COD_AR"),linha.getCodLinhaArqReq(),conn);
						}
						
					}
					
				}
			}
			stmt.close();
			rs.close();
			existe = true ;
		}
		catch (Exception ex) {
			System.out.println("ERRO:"+ex.getMessage()+" Comando: "+sCmd);
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	
	public boolean ArquivoInsert(ArquivoRecebidoBean auxClasse,REC.ParamOrgBean ParamOrgaoId)
	throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);
			existe = ArquivoInsert(auxClasse, ParamOrgaoId, conn);
			conn.commit();
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return existe;
	}	
	
	/**
	 *-----------------------------------------------------------
	 * DAO VERIFICA SE JA EXISTE
	 *-----------------------------------------------------------
	 */
	public boolean ArquivoExisteP59(ArquivoRecebidoBean auxClasse) throws DaoException {
		boolean existeP59 = false;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO "+
			"WHERE IND_P59='P59' AND COD_IDENT_ARQUIVO='"+auxClasse.getCodIdentArquivo()+"'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				existeP59 = true;
			}
			rs.close();         
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return existeP59;
	}
	
	public boolean ArquivoExiste(ArquivoRecebidoBean auxClasse) throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO "+
			"WHERE NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "'" +
			" AND COD_STATUS <> '9'" ;
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				existe = true ;
			}
			rs.close();         
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return existe;
	}
	
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO BUSCA NUMERO DO PROTOCOLO
	 *-----------------------------------------------------------
	 */
	public void buscaProtocolo(ArquivoRecebidoBean auxClasse) throws DaoException {
		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			conn.setAutoCommit(false);
			// BUSCAR O NUMERO DO PROXIMO ARQUIVO E ATUALIZAR
			String sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			" WHERE NOM_PARAMETRO = 'NUM_PROT_ENVIO' AND COD_ORGAO = '" + auxClasse.getCodOrgaoLotacao() + "' FOR UPDATE";
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			int numUltProt = 0;
			while (rs.next()) { 
				numUltProt = rs.getInt("VAL_PARAMETRO");                 
			}
			boolean paramExiste = (numUltProt != 0);
			
			// atualizar NUMERO DE sequencia do arquivo
			String nSeq = sys.Util.lPad(String.valueOf(numUltProt), "0", 5);                
			auxClasse.setNumProtocolo(nSeq);
			
			numUltProt++;           
			nSeq = sys.Util.lPad(String.valueOf(numUltProt), "0", 5);       
			
			if (!paramExiste)   {
				sCmd = "INSERT INTO TCAU_PARAM_ORGAO (COD_PARAMETRO,";
				sCmd += "NOM_PARAMETRO,NOM_DESCRICAO,VAL_PARAMETRO,COD_ORGAO) ";
				sCmd += "VALUES (SEQ_TCAU_PARAM_ORGAO.NEXTVAL,'NUM_PROT_ENVIO',";
				sCmd += "'NUMERO DO PROTOCOLO DE ENVIO DE ARQUIVOS',";
				sCmd += "'" + nSeq + "'," ;
				sCmd += "'" + auxClasse.getCodOrgaoLotacao() + "') ";
			}
			else {
				sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='" + nSeq + "'" +
				" WHERE NOM_PARAMETRO = 'NUM_PROT_ENVIO' AND COD_ORGAO ='" + auxClasse.getCodOrgaoLotacao()+"'";
			}
			stmt.execute(sCmd);
			conn.commit();
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO ATUALIZAR STATUS DE ARQUIVO ENVIADO
	 *-----------------------------------------------------------
	 */	
	public void ArquivoUpdate(ArquivoRecebidoBean auxClasse) throws DaoException {		
		Connection conn = null;
		String sCmd="";
		boolean eConnLocal = false;
		
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			
			Statement stmt  = conn.createStatement();
			ResultSet rs = null;
			int nDias=2;
			String qtdGrav="0";
			
			/*Reprocessamento*/
			sCmd="select /*+rule*/ t1.cod_arquivo,t1.nom_arquivo, "+
			"(select count(*) from tsmi_linha_arquivo_rec t2 where "+
			"t1.cod_arquivo=t2.cod_arquivo and cod_retorno='000') as gravados, "+
			"(select count(*) from tsmi_linha_arquivo_rec t3 where "+
			"t1.cod_arquivo=t3.cod_arquivo and (cod_retorno<>'000' and cod_retorno not in ('N01','N02','N03','N04','N05','N06')) ) as erros "+
			"from "+
			"tsmi_arquivo_recebido t1 " +
			"where t1.cod_Arquivo="+auxClasse.getCodArquivo();			
			rs=stmt.executeQuery(sCmd);			
			if (rs.next())
			{
				qtdGrav =rs.getString("GRAVADOS");
				auxClasse.setQtdLinhaErro(rs.getString("ERROS"));
			}
			if ("8".equals(auxClasse.getCodStatus()) && Util.DifereDias(auxClasse.getDatRecebimento(),sys.Util.formatedToday().substring(0,10))>nDias)
				auxClasse.setCodStatus("1"); /*Colocar como Processado*/
			
			
			//System.out.println("Data DownLoad:"+auxClasse.getDatDownload());
			
			sCmd       = "UPDATE TSMI_ARQUIVO_RECEBIDO SET"
					+ " NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "',"
					+ " COD_STATUS = '" + auxClasse.getCodStatus() + "',";
			        if (auxClasse.getDatDownload().trim().length()>0) { 
				      sCmd+=" DAT_DOWNLOAD = to_date('" +  auxClasse.getDatDownload().substring(0,10) + "','dd-mm-yyyy'),";					  
			        }
					sCmd+=" DAT_PROCESSAMENTO_DETRAN = to_date('" +  auxClasse.getDatProcessamentoDetran() + "','dd/mm/yyyy'),"
					+ " QTD_LINHA_ERRO = " + auxClasse.getQtdLinhaErro()+"," 
					+ " QTD_LINHA_PENDENTE = "+auxClasse.getQtdLinhaPendente()+","
					+ " QTD_LINHA_CANCELADA = "+auxClasse.getQtdLinhaCancelada()+","
					+ " QTD_FOTO = "+auxClasse.getQtdFoto()
					+ " WHERE COD_ARQUIVO = " + auxClasse.getCodArquivo();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			System.out.println("Erro ArquivoUpdate :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			System.out.println("Erro ArquivoUpdate :"+sCmd);			
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					System.out.println("Erro ArquivoUpdate :"+sCmd);
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return;
	}
	
	public void ArquivoUpdateArqDetran(ArquivoRecebidoBean auxClasse) throws DaoException {		
		Connection conn = null;
		String sCmd="";
		boolean eConnLocal = false;
		
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			
			Statement stmt  = conn.createStatement();
			ResultSet rs = null;
			int nDias=2;
			String qtdGrav="0";
			
			/*Reprocessamento*/
			sCmd="select /*+rule*/ t1.cod_arquivo,t1.nom_arquivo, "+
			"(select count(*) from tsmi_linha_arquivo_rec t2 where "+
			"t1.cod_arquivo=t2.cod_arquivo and cod_retorno='000') as gravados, "+
			"(select count(*) from tsmi_linha_arquivo_rec t3 where "+
			"t1.cod_arquivo=t3.cod_arquivo and (cod_retorno<>'000' and cod_retorno not in ('N01','N02','N03','N04','N05','N06')) ) as erros "+
			"from "+
			"tsmi_arquivo_recebido t1 " +
			"where t1.cod_Arquivo="+auxClasse.getCodArquivo();			
			rs=stmt.executeQuery(sCmd);			
			if (rs.next())
			{
				qtdGrav =rs.getString("GRAVADOS");
				auxClasse.setQtdLinhaErro(rs.getString("ERROS"));
			}
			if ("8".equals(auxClasse.getCodStatus()) && Util.DifereDias(auxClasse.getDatRecebimento(),sys.Util.formatedToday().substring(0,10))>nDias)
				auxClasse.setCodStatus("1"); /*Colocar como Processado*/
			
			
			//System.out.println("Data DownLoad:"+auxClasse.getDatDownload());
			
			sCmd       = "UPDATE TSMI_ARQUIVO_RECEBIDO SET"
				+ " NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "',"
				+ " COD_STATUS = '" + auxClasse.getCodStatus() + "',";
			    if (auxClasse.getDatDownload().trim().length()>0) 
				sCmd+= " DAT_DOWNLOAD = to_date('" +  auxClasse.getDatDownload().substring(0,10) + "','yyyy-mm-dd'),";

				sCmd+= " DAT_PROCESSAMENTO_DETRAN = to_date('" +  auxClasse.getDatProcessamentoDetran() + "','dd/mm/yyyy'),"
				+ " QTD_LINHA_ERRO = " + auxClasse.getQtdLinhaErro()+"," 
				+ " QTD_LINHA_PENDENTE = "+auxClasse.getQtdLinhaPendente()+","
				+ " QTD_LINHA_CANCELADA = "+auxClasse.getQtdLinhaCancelada()+","
				+ " QTD_FOTO = "+auxClasse.getQtdFoto()
				+ " WHERE COD_ARQUIVO = " + auxClasse.getCodArquivo();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			System.out.println("Erro ArquivoUpdate :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			System.out.println("Erro ArquivoUpdate :"+sCmd);			
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					System.out.println("Erro ArquivoUpdate :"+sCmd);
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return;
	}
	/**
	 * M�todo de ajuste de uma linha especifica.
	 * Esse m�todo pode ser utilizado para diversas funcionalidades, porem
	 * sua modifica��o foi motivada para ajustar linhas que possuiam notifica��es
	 * pendentes de envio para a VEX, atrav�s do arquivo EMITEVEX. 
	 * @param auxClasse
	 * @author Sergio Roberto Junior
	 * @since 22/12/2004
	 * @throws DaoException
	 * @version 2.0
	 */
	public void LinhaArquivoUpdate(LinhaArquivoRec auxClasse) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;
		String sCmd = "";
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC SET"
				+ " COD_ARQUIVO = "+ auxClasse.getCodArquivo() + ", "
				+ " COD_STATUS = " + auxClasse.getCodStatus() + ", "
				+ " COD_RETORNO = '"+ auxClasse.getCodRetorno() + "', "
				+ " COD_RETORNO_BATCH = '"+ auxClasse.getCodRetornoBatch() + "', "
				+ " DSC_LINHA_ARQ_RETORNO = '"+ auxClasse.getDscLinhaArqRetorno() + "',"
				+ " NUM_AUTO_INFRACAO = '" + auxClasse.getNumAutoInfracao() + "',"
				+ " NUM_NOTIFICACAO = '" + auxClasse.getNumNotificacao() + "',"
				+ " DSC_LINHA_ARQ_REC = '" + auxClasse.getDscLinhaArqRec() + "'"
				+ " WHERE COD_LINHA_ARQ_REC = "+ auxClasse.getCodLinhaArqReq();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			System.out.println("Erro LinhaArquivoUpdate :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			System.out.println("Erro LinhaArquivoUpdate :"+sCmd);			
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					System.out.println("Erro LinhaArquivoUpdate :"+sCmd);
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void LogarLinhaArquivo(LinhaArquivoRec auxClasse, Date iniLinha, Date iniBroker, Date fimBroker, 
			Date fimLinha) throws DaoException {
		
		Connection conn = null;
		boolean eConnLocal = false;        
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}            
			Statement stmt  = conn.createStatement();
			//Gravar Log de Processamento da Linha
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			/*
			String sCmd = "INSERT INTO TSMI_PROC_LINHA_ARQ_LOG (COD_PROC_LINHA_ARQ_LOG, COD_LINHA_ARQ_REC, "
				+ "DAT_INICIO_LINHA, DAT_INICIO_BROKER, DAT_FIM_BROKER, DAT_FIM_lINHA, TXT_RETORNO_BROKER)"
				+ " VALUES (SEQ_TSMI_PROC_LINHA_ARQ_LOG.NEXTVAL, " 
				+ auxClasse.getCodLinhaArqReq() + ", " 
				+ "TO_DATE('" + df.format(iniLinha)  + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "TO_DATE('" + df.format(iniBroker) + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "TO_DATE('" + df.format(fimBroker) + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "TO_DATE('" + df.format(fimLinha)  + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "'" + auxClasse.getDscLinhaArqRetorno() + "')";
			stmt.execute(sCmd);
			*/
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscarArquivosRecebidos(String identArquivo, String status, Connection conn)
	throws DaoException {
		
		Vector vRetorno = new Vector();						
		try {
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM (SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
				+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo, ind_p59"
				+ " FROM TSMI_ARQUIVO_RECEBIDO"; 
			sCmd+=" WHERE cod_status IN (" + status + ") AND cod_ident_arquivo IN (" + identArquivo + ")";
			sCmd+=" ORDER BY ind_prioridade desc, cod_status desc, cod_arquivo) WHERE ROWNUM = 1";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setDatDownload(rs.getString("dat_download"));
				arqRec.setCodStatus(rs.getString("cod_status"));
				arqRec.setQtdReg(rs.getString("qtd_reg"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRec.setNumProtocolo(rs.getString("num_protocolo"));
				arqRec.setDscPortaria59(rs.getString("ind_p59"));
				vRetorno.addElement(arqRec);  			
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	public Vector BuscarListArquivosRecebidos(String identArquivo, String status, Connection conn)
	throws DaoException {
		
		Vector vRetorno = new Vector();						
		try {
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM (SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
				+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo, ind_p59"
				+ " FROM TSMI_ARQUIVO_RECEBIDO"; 
			sCmd+=" WHERE cod_status IN (" + status + ") AND cod_ident_arquivo IN (" + identArquivo + ")";
			sCmd+=" ORDER BY ind_prioridade desc, cod_status desc, cod_arquivo) ";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setDatDownload(rs.getString("dat_download"));
				arqRec.setCodStatus(rs.getString("cod_status"));
				arqRec.setQtdReg(rs.getString("qtd_reg"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRec.setNumProtocolo(rs.getString("num_protocolo"));
				arqRec.setDscPortaria59(rs.getString("ind_p59"));
				vRetorno.addElement(arqRec);  			
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	
	public List<ArquivoRecebidoBean> buscaArquivoPendenteEnvioDataDePostagem(String identArquivo, String status, Connection conn)
			throws DaoException {
				
				List<ArquivoRecebidoBean> arquivos = new ArrayList<ArquivoRecebidoBean>();						
				try {
										
					StringBuffer sCmd = new StringBuffer();
					
					sCmd.append(" SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio, ");
					sCmd.append(" num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,");
					sCmd.append(" qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,");
					sCmd.append(" qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,");
					sCmd.append(" to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,");
					sCmd.append(" to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,");
					sCmd.append(" cod_ident_movimento, num_protocolo, ind_p59,IND_ENVIO_DATA_POSTAGEM");
					sCmd.append(" FROM TSMI_ARQUIVO_RECEBIDO");
					sCmd.append(" WHERE cod_status IN (?) AND cod_ident_arquivo IN (?) AND ind_envio_data_postagem = 'N'");
					sCmd.append(" ORDER BY ind_prioridade desc, cod_status desc, cod_arquivo");

					PreparedStatement pStm = conn.prepareStatement(sCmd.toString());
					pStm.setString(1, status);
					pStm.setString(2, identArquivo);
					
					ResultSet rs = pStm.executeQuery();
					while (rs.next()) {
						if (this.fezDownloadDoArquivo(rs.getString("cod_arquivo"),rs.getString("dat_download"), conn)){
							ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
							arqRec.setCodArquivo(rs.getString("cod_arquivo"));
							arqRec.setNomArquivo(rs.getString("nom_arquivo"));
							arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
							arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
							arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
							arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
							arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
							arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
							arqRec.setCodOrgao(rs.getString("cod_orgao"));
							arqRec.setDatDownload(rs.getString("dat_download"));
							arqRec.setCodStatus(rs.getString("cod_status"));
							arqRec.setQtdReg(rs.getString("qtd_reg"));
							arqRec.setNomUsername(rs.getString("nom_username"));
							arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
							arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
							arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
							arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
							arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
							arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
							arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
							arqRec.setNumProtocolo(rs.getString("num_protocolo"));
							arqRec.setDscPortaria59(rs.getString("ind_p59"));
							arqRec.setIndicadorEnvioDataPostagem(rs.getString("IND_ENVIO_DATA_POSTAGEM"));
							arquivos.add(arqRec);  			
						}
					}			
					rs.close();
					pStm.close();
				} catch (Exception e) {			
					throw new DaoException(e.getMessage());
				}
				return arquivos;
			}
	
	
	public boolean fezDownloadDoArquivo(String codigoArquivo, String dataDownload, Connection conn) throws DaoException{
		try {
			
			StringBuffer sCmd = new StringBuffer();
			
			sCmd.append("select cod_arquivo from tsmi_download where cod_arquivo = ? ");

			PreparedStatement pStm = conn.prepareStatement(sCmd.toString());
			pStm.setString(1, codigoArquivo);	
			ResultSet rs = pStm.executeQuery();
			
			if (rs.next())	return true;
						
			rs.close();
			pStm.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return false;
	}
	
	
	
	
	
	public boolean CarregarLinhasArquivoAR(ArquivoRecebidoBean bean, Connection conn,int numMax) throws DaoException {		
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			String sCmd = "SELECT * FROM TSMI_AR_DIGITALIZADO_TMP "+
			"WHERE "+
			"IND_COD_RET IS NULL AND ROWNUM<="+numMax;
			String sCmd_VEX = null;
			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs_VEX = null;			
			Vector <LinhaArquivoRec>vetLinhas = new Vector<LinhaArquivoRec>();
			int numSeqLinha=0;
			while (rs.next()) {				
				numSeqLinha++;
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodLinhaArqRec(rs.getString("COD_AR_DIGITALIZADO"));
				String numNotificacao =sys.Util.lPad(rs.getString("NUM_NOTIFICACAO"),"0",9);
				String numAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");
				String datEntrega     =Util.converteData(rs.getDate("DAT_ENTREGA"));
				String codRetorno     =rs.getString("NUM_CODIGO_RETORNO").trim();
				codRetorno            =sys.Util.lPad(codRetorno,"0",2);
				String codRetornoVEX  = "00";
				String codCaixa       =sys.Util.lPad(rs.getString("NUM_CAIXA"),"0",4);
				String codLote        =sys.Util.lPad(rs.getString("NUM_LOTE"),"0",4);
				String tipoNotificacao="1"; /*Autua��o como default*/
				String codOrgao       ="";
				String vago           = "";
				
				if ( (codRetorno.equals("00")) || codRetorno.equals("17") ) codRetornoVEX="03";
				else
				{
					sCmd_VEX =  "SELECT NUM_CODIGO_RETORNO FROM TSMI_CODIGO_RETORNO " +
					"WHERE "+
					"NUM_CODIGO_RETORNO_VEX='"+codRetorno+"'";
					rs_VEX    = stmt_VEX.executeQuery(sCmd_VEX) ;			
					while (rs_VEX.next()) {
						codRetornoVEX =rs_VEX.getString("NUM_CODIGO_RETORNO").trim();
						codRetornoVEX =sys.Util.lPad(codRetornoVEX,"0",2); 						
					}
				}
				
				
				sCmd_VEX="SELECT * FROM TSMI_CONTROLE_VEX_MASTER "+
				"WHERE " +
				"NUM_NOTIFICACAO='"+numNotificacao+"'";
				rs_VEX = stmt_VEX.executeQuery(sCmd_VEX);
				while (rs_VEX.next())
				{
					tipoNotificacao=sys.Util.lPad(rs_VEX.getString("TIP_NOTIFICACAO"),"0",1);
				}
				
				sCmd_VEX="SELECT * FROM TSMI_CONTROLE_NOTIFICACOES "+
				"WHERE " +
				"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
				"NUM_AUTO_INFRACAO='"+numAutoInfracao+"' AND ROWNUM=1";
				rs_VEX = stmt_VEX.executeQuery(sCmd_VEX);
				while (rs_VEX.next())
				{
					codOrgao=rs_VEX.getString("COD_ORGAO_AUTUACAO");
					if (codOrgao==null) codOrgao="";
					codOrgao=sys.Util.lPad(codOrgao,"0",6);
				}
				
				
				String dscLinha="40592CRCR"+numNotificacao+"BRNotificacao Multa"+
				Util.formataDataYYYYMMDD(datEntrega)+codRetornoVEX+codCaixa+codLote+
				Util.rPad(vago," ",40)+Util.rPad(vago,"0",12)+Util.rPad(vago," ",1)+
				Util.rPad(vago," ",19)+Util.rPad(numAutoInfracao," ",12)+tipoNotificacao+codOrgao;
				
				linhaRec.setDscLinhaArqRec(dscLinha);
				linhaRec.setNumAutoInfracao(numAutoInfracao);
				linhaRec.setNumNotificacao(numNotificacao);
				linhaRec.setNumSeqLinha(String.valueOf(numSeqLinha));
				linhaRec.setCodRetorno(codRetornoVEX);				
				vetLinhas.add(linhaRec);				
				bOk = true;
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();  
			stmt_VEX.close();
			if (rs_VEX!=null) rs_VEX.close();
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}
	
	public boolean CarregarLinhasArquivoARPNT(ArquivoRecebidoBean bean, UsuarioBean UsuarioBeanId,Connection conn,int numMax) throws DaoException {
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			Statement stmt_VEX = conn.createStatement();			
			String sCmd = "SELECT * FROM TPNT_AR_DIGITALIZADO_TMP "+
			"WHERE "+
			"IND_COD_RET IS NULL AND ROWNUM<="+numMax;
			String sCmd_VEX = null;
			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs_VEX = null;			
			Vector <LinhaArquivoRec>vetLinhas = new Vector<LinhaArquivoRec>();
			int numSeqLinha=0;
			while (rs.next()) {				
				numSeqLinha++;
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodLinhaArqRec(rs.getString("COD_AR_DIGITALIZADO"));
				String numNotificacao =sys.Util.lPad(rs.getString("NUM_NOTIFICACAO"),"0",9);
				String seqNotificacao =sys.Util.lPad(rs.getString("SEQ_NOTIFICACAO"),"0",3);
				String datEntrega     =Util.converteData(rs.getDate("DAT_ENTREGA"));
				String codRetorno     =rs.getString("NUM_CODIGO_RETORNO").trim();
				codRetorno            =sys.Util.lPad(codRetorno,"0",2);
				String codRetornoVEX  = "00";
				String indEntregue    = "S";
				System.out.println("Atualizando..."+numNotificacao);

				
                String sEntregaComSucesso="";
				sCmd_VEX =  "SELECT * FROM TPNT_NOTIFICACAO " +
				"WHERE "+
				"NUM_NOTIFICACAO='"+numNotificacao+"' AND "+
				"SEQ_NOTIFICACAO='"+seqNotificacao+"'";
				rs_VEX    = stmt_VEX.executeQuery(sCmd_VEX);			
				while (rs_VEX.next()) {
					sEntregaComSucesso=rs_VEX.getString("IND_ENTREGUE");
					if (sEntregaComSucesso==null) sEntregaComSucesso="";
				}
				
				if (sEntregaComSucesso.length()==0)
				{
					if ( (codRetorno.equals("00")) || codRetorno.equals("17") ) codRetornoVEX="03";
					else
					{
						sCmd_VEX =  "SELECT NUM_CODIGO_RETORNO,IND_ENTREGUE FROM TSMI_CODIGO_RETORNO " +
						"WHERE "+
						"NUM_CODIGO_RETORNO_VEX='"+codRetorno+"'";
						rs_VEX    = stmt_VEX.executeQuery(sCmd_VEX) ;			
						while (rs_VEX.next()) {
							codRetornoVEX =rs_VEX.getString("NUM_CODIGO_RETORNO").trim();
							codRetornoVEX =sys.Util.lPad(codRetornoVEX,"0",2);
							indEntregue   = rs_VEX.getString("IND_ENTREGUE");
						}
					}
					
					
					PNT.NOT.Dao daoPNT = DaoFactory.getInstance();
					
					NotificacaoBean myNotif = new NotificacaoBean();
					LocalizarNotificacao(numNotificacao+"-"+seqNotificacao,myNotif);
					myNotif.getRetorno().setNumRetorno(codRetornoVEX);
					myNotif.getRetorno().setIndEntregue(indEntregue);
					myNotif.setDatRetorno(datEntrega);
					myNotif.setNomUserNameRetorno(UsuarioBeanId.getNomUserName());			
					myNotif.setCodOrgaoLotacaoRetorno(UsuarioBeanId.getOrgao().getCodOrgao());
					myNotif.setDatProcRetorno(sys.Util.formatedToday().substring(0,10));
					
					PNT.ProcessoBean myProc = new PNT.ProcessoBean();
					myProc.setCodProcesso(myNotif.getCodProcesso());
					myProc.LeProcesso();
					
					/*Autua��o*/
					if (myNotif.getTipoNotificacao().equals("0"))
					{
						if ("000,001,002".indexOf(myProc.getCodStatus())>=0)
						{
							if (indEntregue.equals("S"))
								myProc.setCodStatus("004");
							else
								myProc.setCodStatus("002");
						}
					}
					/*Penalidade*/
					if (myNotif.getTipoNotificacao().equals("2"))
					{			
						if ("030,031,032".indexOf(myProc.getCodStatus())>=0)
						{
							if (indEntregue.equals("S"))
								myProc.setCodStatus("034");
							else
								myProc.setCodStatus("032");
						}
					}
					/*Entrega CNH*/
					if (myNotif.getTipoNotificacao().equals("4"))
					{			
						if ("090,091,092".indexOf(myProc.getCodStatus())>=0)
						{
							if (indEntregue.equals("S"))
								myProc.setCodStatus("094");
							else
								myProc.setCodStatus("092");
						}
					}
					myProc.setDatStatus(sys.Util.formatedToday().substring(0,10));
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodProcesso(myProc.getCodProcesso());   
					myHis.setNumProcesso(myProc.getNumProcesso()); 
					myHis.setCodStatus(myProc.getCodStatus());  
					myHis.setDatStatus(myProc.getDatStatus());
					myHis.setCodOrigemEvento("100");
					if (myNotif.getTipoNotificacao().equals("0")) myHis.setCodEvento("808");
					if (myNotif.getTipoNotificacao().equals("2")) myHis.setCodEvento("838");
					if (myNotif.getTipoNotificacao().equals("4")) myHis.setCodEvento("898");
					myHis.setNomUserName(UsuarioBeanId.getNomUserName());
					myHis.setCodOrgaoLotacao(UsuarioBeanId.getOrgao().getCodOrgao());
					myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
					//Num Notificacao
					myHis.setTxtComplemento01(myNotif.getNumNotificacao()+"-"+myNotif.getSeqNotificacao());
					// Codigo Retorno
					myHis.setTxtComplemento02(codRetornoVEX);
					// Data Retorno
					myHis.setTxtComplemento03(datEntrega);
					// Ind Entrega
					myHis.setTxtComplemento04(indEntregue);
					daoPNT.RetornoNotificacao(myProc,myNotif,myHis);
				}
				
				
				linhaRec.setNumNotificacao(numNotificacao);
				linhaRec.setNumSeqLinha(String.valueOf(numSeqLinha));
				linhaRec.setCodRetorno(codRetornoVEX);				
				vetLinhas.add(linhaRec);				
				bOk = true;
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();  
			stmt_VEX.close();
			if (rs_VEX!=null) rs_VEX.close();
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}
	

	
	public void LocalizarNotificacao(String numNotificacao,NotificacaoBean myNotif) throws BeanException
	{
		int iPos1=numNotificacao.indexOf('-'),iPos2=0;
		if (iPos1>0) iPos2=numNotificacao.length();

		String sPedaco1=numNotificacao.substring(0,iPos1);
		sPedaco1 = "000000000"+sPedaco1;
		sPedaco1 = sPedaco1.substring(sPedaco1.length()-9,sPedaco1.length());
		String sPedaco2=numNotificacao.substring(iPos1+1,iPos2);
		sPedaco2 = "000"+sPedaco2;
		sPedaco2 = sPedaco2.substring(sPedaco2.length()-3,sPedaco2.length());
		myNotif.setNumNotificacao(sPedaco1);
		myNotif.setSeqNotificacao(sPedaco2);
		myNotif.LerNotificacao("notificacao");
	}
	
	
	
	
	
	public boolean AtualizarLinhasArquivoAR(ArquivoRecebidoBean bean, Connection conn) throws DaoException {		
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd=null;
			
			Iterator it  = bean.getLinhaArquivoRec().iterator() ;  
			int num_seq_linha = 0;
			LinhaArquivoRec linha;
			while (it.hasNext()) {
				num_seq_linha++;
				linha = (LinhaArquivoRec) it.next();
				sCmd  = "UPDATE TSMI_AR_DIGITALIZADO_TMP "+
				"SET "+
				"IND_COD_RET='S', "+
				"CODIGO_RETORNO_VEX='"+linha.getCodRetorno()+"', "+
				"DAT_ARQUIVO='"+bean.getDatRecebimento()+"', "+
				"NOM_ARQUIVO='"+bean.getNomArquivo()+"' "+
				"WHERE COD_AR_DIGITALIZADO='"+linha.getCodLinhaArqReq()+"'";
				stmt.execute(sCmd);
			}
			bOk = true;
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}
	
	
	public boolean AtualizarLinhasArquivoARPNT(ArquivoRecebidoBean bean, Connection conn) throws DaoException {		
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd=null;
			
			Iterator it  = bean.getLinhaArquivoRec().iterator() ;  
			int num_seq_linha = 0;
			LinhaArquivoRec linha;
			while (it.hasNext()) {
				num_seq_linha++;
				linha = (LinhaArquivoRec) it.next();
				sCmd  = "UPDATE TPNT_AR_DIGITALIZADO_TMP "+
				"SET "+
				"IND_COD_RET='S', "+
				"CODIGO_RETORNO_VEX='"+linha.getCodRetorno()+"', "+
				"DAT_ARQUIVO='"+bean.getDatRecebimento()+"', "+
				"NOM_ARQUIVO='"+bean.getNomArquivo()+"' "+
				"WHERE COD_AR_DIGITALIZADO='"+linha.getCodLinhaArqReq()+"'";
				stmt.execute(sCmd);
			}
			bOk = true;
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}

	
	
	
	public boolean CarregarLinhasArquivo(ArquivoRecebidoBean bean, Connection conn) throws DaoException {		
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_ARQUIVO, COD_LINHA_ARQ_REC, DSC_LINHA_ARQ_REC, NUM_SEQ_LINHA,"
				+ " COD_STATUS, COD_RETORNO, COD_RETORNO_BATCH, DSC_LINHA_ARQ_RETORNO, NUM_AUTO_INFRACAO,"
				+ " NUM_NOTIFICACAO, COD_ORGAO FROM TSMI_LINHA_ARQUIVO_REC"
				+ " WHERE COD_ARQUIVO = " + bean.getCodArquivo()
				+ " ORDER BY NUM_SEQ_LINHA";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetLinhas = new Vector();
			while (rs.next()) {				
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
				linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				linhaRec.setCodOrgao(rs.getString("cod_orgao"));
				vetLinhas.add(linhaRec);				
				bOk = true;
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}
	public boolean CarregarLinhasArquivo(ArquivoRecebidoBean bean) throws DaoException {
		Connection conn = null;
		try {
			conn = sys.ServiceLocator.getInstance().getConnection(MYABREVSIST);
			return CarregarLinhasArquivo(bean, conn);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); } 
				catch (Exception ey) { throw new DaoException(ey.getMessage()); }
			}
		}		
	}
	
	public void CarregaArqRecebido(ArquivoRecebidoBean auxClasse,String myOrgAtu,String mySigOrgAtu) 
	throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();			
			String sCmd = "SELECT NUM_PROTOCOLO,NOM_ARQUIVO,COD_ARQUIVO,COD_IDENT_ARQUIVO,NUM_ANO_EXERCICIO,NUM_CONTROLE_ARQUIVO,"; 
			sCmd += "to_char(dat_RECEBIMENTO,'dd/mm/yyyy')DAT_RECEBIMENTO,QTD_REG,NOM_USERNAME,COD_ORGAO_LOTACAO,SIG_ORGAO_LOTACAO,";
			sCmd += "IND_EMISSAO_NOTIF_DETRAN FROM TSMI_ARQUIVO_RECEBIDO WHERE COD_ARQUIVO = '" + auxClasse.getCodArquivo() + "'";			
			ResultSet rs   = stmt.executeQuery(sCmd) ;            
			while (rs.next()) { 
				auxClasse.setNumProtocolo((rs.getString("NUM_PROTOCOLO"))); 
				auxClasse.setNomArquivo((rs.getString("NOM_ARQUIVO")));
				auxClasse.setCodArquivo((rs.getString("COD_ARQUIVO")));
				auxClasse.setCodIdentArquivo((rs.getString("COD_IDENT_ARQUIVO")));
				auxClasse.setNumAnoExercicio((rs.getString("NUM_ANO_EXERCICIO")));
				auxClasse.setNumControleArq((rs.getString("NUM_CONTROLE_ARQUIVO"))); 
				auxClasse.setDatRecebimento((rs.getString("DAT_RECEBIMENTO")));
				auxClasse.setQtdReg((rs.getString("QTD_REG")));
				auxClasse.setNomUsername((rs.getString("NOM_USERNAME")));
				auxClasse.setCodOrgaoLotacao((rs.getString("COD_ORGAO_LOTACAO")));
				auxClasse.setSigOrgaoLotacao((rs.getString("SIG_ORGAO_LOTACAO")));
				auxClasse.setIndEmissaoNotifDetran((rs.getString("IND_EMISSAO_NOTIF_DETRAN")));
				auxClasse.setCodOrgaoAtuacao(myOrgAtu);
				auxClasse.setSigOrgaoAtuacao(mySigOrgAtu);							        
			}           
			stmt.execute(sCmd);            
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}            	
	
	public void CarregaArqRecebidoTemp(ArquivoRecebidoBean auxClasse) throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd ="SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, "
				+ " to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download,"
				+ " ind_prioridade, cod_ident_movimento, num_protocolo"
				+ " FROM tsmi_arquivo_recebido_tmp"; 
			sCmd+=" WHERE cod_arquivo = " + auxClasse.getCodArquivo();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				auxClasse.setNomArquivo(rs.getString("nom_arquivo"));
				auxClasse.setDatRecebimento(rs.getString("dat_recebimento"));  
				auxClasse.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				auxClasse.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				auxClasse.setNumControleArq(rs.getString("num_controle_arquivo")); 
				auxClasse.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				auxClasse.setCodOrgao(rs.getString("cod_orgao"));
				auxClasse.setDatDownload(rs.getString("dat_download"));
				auxClasse.setCodStatus(rs.getString("cod_status"));
				auxClasse.setQtdReg(rs.getString("qtd_reg"));
				auxClasse.setNomUsername(rs.getString("nom_username"));
				auxClasse.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				auxClasse.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				auxClasse.setResponsavelArq(rs.getString("nom_responsavel"));
				auxClasse.setIndPrioridade(rs.getString("ind_prioridade"));
				auxClasse.setIdentMovimento(rs.getString("cod_ident_movimento"));
				auxClasse.setNumProtocolo(rs.getString("num_protocolo"));
				auxClasse.setArqFuncao(auxClasse.getCodIdentArquivo());
				auxClasse.setDscPortaria59("IND_P59");
				auxClasse.setTipHeader("20"); 
			}            
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public boolean CarregarLinhasArquivoTemp(ArquivoRecebidoBean bean) throws DaoException {		
		boolean bOk = false;
		Connection conn = null;
		try {
			conn = sys.ServiceLocator.getInstance().getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_ARQUIVO, COD_LINHA_ARQ_REC, DSC_LINHA_ARQ_REC, NUM_SEQ_LINHA,"
				+ " COD_STATUS, COD_RETORNO, COD_RETORNO_BATCH, DSC_LINHA_ARQ_RETORNO, NUM_AUTO_INFRACAO,"
				+ " NUM_NOTIFICACAO FROM TSMI_LINHA_ARQUIVO_REC_TMP"
				+ " WHERE COD_ARQUIVO = " + bean.getCodArquivo()
				+ " ORDER BY NUM_SEQ_LINHA";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetLinhas = new Vector();
			while (rs.next()) {				
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
				linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				vetLinhas.add(linhaRec);				
				bOk = true;
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();               
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); } 
				catch (Exception ey) { throw new DaoException(ey.getMessage()); }
			}
		}
		return bOk;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscaArquivosProcessados(ACSS.UsuarioBean usrLogado, String codIdentArquivo, String dataIni, String dataFim,String Origem) throws DaoException {
		Vector vRetClasse   = new Vector();
		Connection conn = null;
		try {
			if (!"REG2066".equals(Origem)){
				ArquivoRecebidoBean relClasse;
				conn = serviceloc.getConnection(MYABREVSIST);
				
				ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
				ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
				ParamSistemaBeanId.PreparaParam();
				String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
				if (MYABREVSIST.equals("GER")) sHistorico="S";
				
				Statement stmt = conn.createStatement();
				String sCmd ="SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR,TO_CHAR(A.DAT_DOWNLOAD,'dd/mm/yy') as DAT_DOWNLOAD_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
				" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, NUM_ANO_EXERCICIO, A.QTD_LINHA_PENDENTE, A.QTD_LINHA_CANCELADA" +
				" FROM TSMI_ARQUIVO_RECEBIDO A" +
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
				" on (a.cod_arquivo=G.cod_arquivo)" +
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
				" on (a.cod_arquivo=E.cod_arquivo)" +
				" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
				
				//EMITEVEX e EMITEPER
				if ((!"EMITEVEX".equals(codIdentArquivo))|| (!"EMITEPER".equals(codIdentArquivo))) 
					sCmd += " AND COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() ;
				
				sCmd+=" AND A.DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" ;
				
				if (sHistorico.equals("S")) {				
					sCmd += " UNION "+
					"SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR,TO_CHAR(A.DAT_DOWNLOAD,'dd/mm/yy') as DAT_DOWNLOAD_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
					" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, NUM_ANO_EXERCICIO, A.QTD_LINHA_PENDENTE, A.QTD_LINHA_CANCELADA" +
					" FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP A" +
					" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
					" on (a.cod_arquivo=G.cod_arquivo)" +
					" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
					" on (a.cod_arquivo=E.cod_arquivo)" +
					" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'";
				}
				
				//EMITEVEX e EMITEPER
				if ((!"EMITEVEX".equals(codIdentArquivo)) || (!"EMITEPER".equals(codIdentArquivo))) 
					sCmd += " AND COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() ;
				
				sCmd += " AND A.DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" +			
				" ORDER BY 1 DESC";
				
				ResultSet rs = stmt.executeQuery(sCmd);             
				while (rs.next())   {
					relClasse = new ArquivoRecebidoBean();
					relClasse.setCodIdentArquivo(codIdentArquivo);
					relClasse.setCodArquivo(rs.getString("cod_arquivo"));
					relClasse.setNomArquivo(rs.getString("nom_arquivo"));
					relClasse.setDatRecebimento(rs.getString("dat_recebimento_char"));   
					relClasse.setDatDownload(rs.getString("dat_download_char"));
					relClasse.setCodStatus(rs.getString("cod_Status"));
					relClasse.setQtdReg(rs.getString("qtd_reg"));
					relClasse.setQtdLinhaErro(rs.getString("QTD_LINHA_ERRO"));
					relClasse.setDatDownloadGravado(rs.getString("DAT_DOWNLOAD_GRAV"));                 
					relClasse.setDatDownloadErro(rs.getString("DAT_DOWNLOAD_ERRO"));
					relClasse.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));
					relClasse.setQtdLinhaPendente(rs.getString("QTD_LINHA_PENDENTE"));
					relClasse.setQtdLinhaCancelada(rs.getString("QTD_LINHA_CANCELADA"));
					vRetClasse.addElement(relClasse);           
				}  
				rs.close();
				stmt.close();
			}else{
				ArquivoRecebidoBean arqRec;	
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				String sCmd ="SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, " +				
				" TO_CHAR(B.max_download,'dd/mm/yy') as DAT_DOWNLOAD_BKP,NUM_ANO_EXERCICIO,QTD_REG"+				
				" FROM TSMI_ARQUIVO_RECEBIDO A" +	
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='BKP' group by cod_arquivo) B" +
				" on (A.cod_arquivo=B.cod_arquivo)" +
				" WHERE COD_IDENT_ARQUIVO ='BKP'" +
				" AND COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() +
				" AND TRUNC(A.DAT_RECEBIMENTO) BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" +			
				" ORDER BY A.DAT_RECEBIMENTO DESC";         
				ResultSet rs = stmt.executeQuery(sCmd);             
				while (rs.next())   {
					arqRec = new ArquivoRecebidoBean();
					arqRec.setCodIdentArquivo(codIdentArquivo);
					arqRec.setCodArquivo(rs.getString("cod_arquivo"));
					arqRec.setNomArquivo(rs.getString("nom_arquivo"));
					arqRec.setDatRecebimento(rs.getString("dat_recebimento_char")); 
					arqRec.setDatDownload(rs.getString("DAT_DOWNLOAD_BKP"));
					arqRec.setCodStatus(rs.getString("cod_Status"));					
					arqRec.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));
					arqRec.setQtdReg(rs.getString("QTD_REG"));
					vRetClasse.addElement(arqRec);           
				}           
				rs.close();
				stmt.close();
			}
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO Insere Download feito pelo usu�rio
	 *-----------------------------------------------------------
	 */	
	public void insereDownloadArquivo(ACSS.UsuarioBean usrLogado, String codArquivo, String extensao) 
	throws DaoException {		
		Connection conn = null;	
		String sCmd     = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd ="INSERT INTO TSMI_DOWNLOAD(COD_DOWNLOAD,IND_EXTENSAO,DAT_DOWNLOAD,COD_ARQUIVO,NOM_USERNAME,COD_ORGAO_LOTACAO)" +
			" VALUES(SEQ_TSMI_DOWNLOAD.NEXTVAL,'" + extensao + "',sysdate,'" 
			+ codArquivo + "','" + usrLogado.getNomUserName() + "','" 
			+ usrLogado.getOrgao().getCodOrgao() + "')";  
			stmt.execute(sCmd);	
			stmt.close();			
		}
		catch (SQLException e) {
			System.out.println("insereDownloadArquivo :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO Busca c�digo do munic�pio baseado no nome e na UF do munic�pio
	 *-----------------------------------------------------------
	 */		
	public String buscarCodMunicipio(Connection conn, String nomMunicipio, String UF) throws sys.DaoException {
		try {
			String codMunicipio = "";
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_municipio from TSMI_MUNICIPIO WHERE TRIM(nom_municipio) = '" +
			nomMunicipio.trim() + "' AND COD_UF = '" + UF + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) codMunicipio = rs.getString("cod_municipio");
			rs.close();
			stmt.close();
			
			//Se n�o encontrou na UF
			if (codMunicipio.equals("")) {
				Statement stmtUF = conn.createStatement();
				String sCmd2 = "SELECT cod_municipio from TSMI_MUNICIPIO WHERE TRIM(nom_municipio) = '" +
				nomMunicipio.trim() + "' ORDER BY cod_municipio";
				ResultSet rsUF = stmtUF.executeQuery(sCmd2);
				while (rsUF.next()) codMunicipio = rsUF.getString("cod_municipio");
				rsUF.close();
				stmtUF.close();
			}
			
			return codMunicipio;
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}  
	}	
	public String buscarCodMunicipioVelho(Connection conn, String nomMunicipio) throws sys.DaoException {
		return buscarCodMunicipio(conn, nomMunicipio, "RJ");
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO Busca c�digo da marca/modelo na descri��o
	 *-----------------------------------------------------------
	 */		
	public String buscarCodMarcaModelo(Connection conn, String dscMarcaModelo) throws sys.DaoException {
		try {
			String codMarcaModelo = "";
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_MARCA_MODELO FROM TSMI_MARCA_MODELO WHERE UPPER(TRIM(DSC_MARCA_MODELO)) = '"
				+ dscMarcaModelo.trim().toUpperCase() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) codMarcaModelo = rs.getString("COD_MARCA_MODELO");
			rs.close();
			stmt.close();
			return codMarcaModelo;
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}  
	}
	
	
	public boolean buscarFoto(Connection conn, String numAutoInfracao) throws sys.DaoException {
		try {
			Statement stmt = conn.createStatement();
			boolean bFoto=false;
			String sCmd = "SELECT * FROM TSMI_FOTO_DIGITALIZADA WHERE NUM_AUTO_INFRACAO='"+numAutoInfracao+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) { 
				bFoto=true;
			}
			rs.close();
			stmt.close();
			return bFoto;
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}  
	}		
	
	
	
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos para Consulta - Filtros: Orgao,Tipo Arquivo,Data
	 *------------------------------------------------------------------------
	 */
	public Vector BuscaArquivosProcessadosConsulta(ACSS.UsuarioBean usrLogado, String codIdentArquivo, 
			String codOrgao, String dataIni, String dataFim,boolean bArqRecebidoBKP) throws DaoException {
		ArquivoRecebidoBean relClasse;
		Vector vRetClasse   = new Vector();     
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
			" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, " +
			" TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, QTD_LINHA_PENDENTE, QTD_LINHA_CANCELADA, " +
			" TO_CHAR(A.DAT_PROCESSAMENTO_DETRAN,'dd/mm/yy') as DAT_PROCESSAMENTO"+
			" FROM TSMI_ARQUIVO_RECEBIDO A" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
			" on (a.cod_arquivo=G.cod_arquivo)" +
			" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
			" on (a.cod_arquivo=E.cod_arquivo)" +
			" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'" + 
			" AND COD_ORGAO IN (" + codOrgao + ")" +
			" AND A.DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" ;
			if (sHistorico.equals("S")) 
			{
				sCmd +=" UNION " +
				"SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, A.QTD_REG,A.QTD_LINHA_ERRO," +
				" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV, " +
				" TO_CHAR(E.max_download,'dd/mm/yy') AS DAT_DOWNLOAD_ERRO, QTD_LINHA_PENDENTE, QTD_LINHA_CANCELADA, " +
				" TO_CHAR(A.DAT_PROCESSAMENTO_DETRAN,'dd/mm/yy') as DAT_PROCESSAMENTO"+
				" FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP A" +
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='GRV' group by cod_arquivo) G" +
				" on (a.cod_arquivo=G.cod_arquivo)" +
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TSMI_DOWNLOAD WHERE IND_EXTENSAO='REJ' group by cod_arquivo) E" +
				" on (a.cod_arquivo=E.cod_arquivo)" +
				" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'" + 
				" AND COD_ORGAO IN (" + codOrgao + ")" +
				" AND A.DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" ;
			}
			sCmd+=" ORDER BY DAT_RECEBIMENTO DESC";				
			ResultSet rs = stmt.executeQuery(sCmd);             
			while (rs.next())   {
				relClasse = new ArquivoRecebidoBean();
				relClasse.setCodIdentArquivo(codIdentArquivo);
				relClasse.setCodArquivo(rs.getString("cod_arquivo"));
				relClasse.setNomArquivo(rs.getString("nom_arquivo"));
				relClasse.setDatRecebimento(rs.getString("dat_recebimento_char"));                  
				relClasse.setCodStatus(rs.getString("cod_Status"));
				relClasse.setQtdReg(rs.getString("qtd_reg"));
				relClasse.setQtdLinhaErro(rs.getString("QTD_LINHA_ERRO"));
				relClasse.setDatDownloadGravado(rs.getString("DAT_DOWNLOAD_GRAV"));                 
				relClasse.setDatDownloadErro(rs.getString("DAT_DOWNLOAD_ERRO"));
				relClasse.setQtdLinhaPendente(rs.getString("QTD_LINHA_PENDENTE"));
				relClasse.setQtdLinhaCancelada(rs.getString("QTD_LINHA_CANCELADA"));
				relClasse.setDatProcessamentoDetran(rs.getString("DAT_PROCESSAMENTO"));
				vRetClasse.addElement(relClasse);           
			}           
			rs.close();
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	
	
	/**---------------------------------------
	 * DAO Relativo MonitoramentoBean
	 * @author Luciana Rocha
	 ----------------------------------------*/
	public boolean ConsultaDados(MonitoraBean auxClasse, String ordem) throws DaoException {		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		String orderBy = "";
		String condicao = "where 1=1 ";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());
			//Montar Order By
			if (ordem.equals("O"))  orderBy = " order by cod_orgao ";
			if (ordem.equals("A"))  orderBy = " order by nom_objeto ";
			if (ordem.equals("R"))  orderBy = " order by nom_processo" ;
			if (ordem.equals("D"))  orderBy = " order by dat_log" ;
			
			//Montar a condicao Where
			if (!auxClasse.getCodOrgao().equals("0")) condicao += " AND cod_orgao = '"+auxClasse.getCodOrgao()+"'" ;
			if (auxClasse.getCodOrgao().equals("0")) condicao += " AND m.cod_orgao = o.cod_orgao" ;
			if (!auxClasse.getNomObjeto().trim().equals("") ) condicao += " AND nom_objeto = '"+auxClasse.getNomObjeto()+"'" ;
			if (!auxClasse.getNomProcesso().trim().equals("")) condicao += " AND nom_processo= '"+auxClasse.getNomProcesso()+"'" ;
			if (!auxClasse.getDatIni().trim().equals("")) condicao += " AND to_char(dat_log,'dd/mm/yyyy') BETWEEN '" + auxClasse.getDatIni() + "' and '" + auxClasse.getDatFim() + "'";
			
			// prepara os dados sem orgao selecionado
			if (auxClasse.getCodOrgao().equals("0")){
				sCmd = "SELECT m.dat_log, m.txt_mensagem, m.cod_tipo_mensagem, m.cod_orgao," +
				"m.nom_objeto, m.nom_processo, o.sig_orgao FROM TSMI_MONITOR m, TSMI_ORGAO O " + condicao + orderBy;
			}
			//prepara os dados com orgao selecionado
			if (!auxClasse.getCodOrgao().equals("0")){
				sCmd = "SELECT dat_log, txt_mensagem, cod_tipo_mensagem, cod_orgao," +
				"nom_objeto, nom_processo FROM TSMI_MONITOR  " + condicao + orderBy;
				
			}
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				MonitoraBean dados = new MonitoraBean();
				dados.setDatLog(rs.getString("dat_log"));
				dados.setCodOrgao(rs.getString("cod_orgao"));
				dados.setNomObjeto(rs.getString("nom_objeto"));
				dados.setNomProcesso(rs.getString("nom_processo"));
				dados.setTxtMensagem(rs.getString("txt_mensagem"));
				dados.setCodTipoMensagem(rs.getString("cod_tipo_mensagem"));
				if (auxClasse.getCodOrgao().equals("0")) dados.setSigOrgao(rs.getString("sig_orgao"));
				if (!auxClasse.getCodOrgao().equals("0"))dados.setSigOrgao(auxClasse.getSigOrgao());
				auxClasse.getDados().add(dados);
				ok = true;
			}
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos DOL de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscarArquivosDOL(String status, Connection conn) throws DaoException {
		Vector vRetorno = new Vector();						
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM (SELECT cod_arquivo_dol, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, cod_orgao, cod_status,"
				+ " qtd_lote, nom_username, cod_orgao_lotacao, nom_responsavel, qtd_lote_erro,"
				+ " to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran"
				+ " FROM TSMI_ARQUIVO_DOL";            
			sCmd+=" WHERE cod_status IN (" + status + ")";
			sCmd+=" ORDER BY cod_status desc, cod_arquivo_dol) WHERE ROWNUM = 1";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoDolBean arqRec = new ArquivoDolBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo_dol"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setCodStatus(rs.getString("cod_Status"));
				arqRec.setQtdLote(rs.getString("qtd_lote"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLoteErro(rs.getString("qtd_lote_erro"));
				vRetorno.addElement(arqRec);  			
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	public Vector BuscarListArquivosDOL(String status, Connection conn) throws DaoException {
		Vector vRetorno = new Vector();						
		try {        
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM (SELECT cod_arquivo_dol, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, cod_orgao, cod_status,"
				+ " qtd_lote, nom_username, cod_orgao_lotacao, nom_responsavel, qtd_lote_erro,"
				+ " to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento, ind_p59, "
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran"
				+ " FROM TSMI_ARQUIVO_DOL";            
			sCmd+=" WHERE cod_status IN (" + status + ")";
			sCmd+=" ORDER BY cod_status desc, cod_arquivo_dol)";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoDolBean arqRec = new ArquivoDolBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo_dol"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setCodStatus(rs.getString("cod_Status"));
				arqRec.setQtdLote(rs.getString("qtd_lote"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLoteErro(rs.getString("qtd_lote_erro"));
				arqRec.setDscPortaria59(rs.getString("ind_p59"));
				vRetorno.addElement(arqRec);  			
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		return vRetorno;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todas as linhas dos Arquivos DOL de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */
	public void CarregarLinhasArquivoDOL(ArquivoDolBean bean, Connection conn) throws DaoException {
		try {										
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_arquivo_dol, cod_lote, cod_status,"
				+ " ind_emissao_notif_detran, cod_ident_arquivo, num_lote, cod_unidade,"
				+ " num_ano_exercicio, dat_lote, qtd_reg, qtd_linha_erro"
				+ " FROM TSMI_LOTE_DOL"
				+ " WHERE cod_arquivo_dol = " + bean.getCodArquivo()
				+ " ORDER BY num_lote";
			ResultSet rs = stmt.executeQuery(sCmd);
			LoteDolBean loteDol = new LoteDolBean();
			Vector vetLotes = new Vector(); 
			
			while (rs.next()) {
				loteDol = new LoteDolBean();
				loteDol.setCodArquivo(rs.getString("cod_arquivo_dol"));
				loteDol.setCodLote(rs.getString("cod_lote"));
				loteDol.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran"));
				loteDol.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				loteDol.setNumLote(rs.getString("num_lote"));
				loteDol.setCodUnidade(rs.getString("cod_unidade"));
				loteDol.setNumAnoExercicio(rs.getString("num_ano_exercicio"));
				loteDol.setDatLote(rs.getString("dat_lote"));
				loteDol.setQtdReg(rs.getString("qtd_reg"));
				loteDol.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				loteDol.setCodStatus(rs.getString("cod_status"));
				
				Statement stmtLin = conn.createStatement();
				String sCmdLin ="SELECT cod_lote, cod_linha_lote, cod_status,"
					+ " num_seq_linha, dsc_linha_lote,"
					+ " cod_retorno, dsc_linha_lote_retorno"
					+ " FROM TSMI_LINHA_LOTE where cod_lote = " + loteDol.getCodLote()
					+ " ORDER BY num_seq_linha";
				
				ResultSet rsLin = stmtLin.executeQuery(sCmdLin);
				Vector vetLinhas = new Vector();
				while(rsLin.next()) {					
					LinhaLoteDolBean linhaRec = new LinhaLoteDolBean();
					linhaRec.setCodArquivo(bean.getCodArquivo());
					linhaRec.setCodLote(rsLin.getString("cod_lote"));
					linhaRec.setCodLinhaLote(rsLin.getString("cod_linha_lote"));
					linhaRec.setDscLinhaLote(sys.Util.rPad(rsLin.getString("dsc_linha_lote")," ",554));
					linhaRec.setDscLinhaLoteNIT(sys.Util.rPad(rsLin.getString("dsc_linha_lote")," ",1000));					
					linhaRec.setNumSeqLinha(rsLin.getString("num_seq_linha"));
					linhaRec.setCodStatus(rsLin.getString("cod_status"));
					linhaRec.setCodRetorno(rsLin.getString("cod_retorno"));
					linhaRec.setDscLinhaLoteRetorno(rsLin.getString("dsc_linha_lote_retorno"));
					
					vetLinhas.add(linhaRec);					
				}
				loteDol.setLinhaLoteDol(vetLinhas);
				vetLotes.add(loteDol);
				stmtLin.close();
			}
			bean.setLote(vetLotes);
			
			rs.close();
			stmt.close();				
		} catch (Exception e) {			
			
			
			throw new DaoException(e.getMessage());
		}			
	}
	
	public void ArquivoUpdateDol(ArquivoDolBean auxClasse) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;
		
		try 
		{
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			ResultSet rs = null;
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			int nDias=5;
			
			String sCmd  = "SELECT A.COD_ARQUIVO_DOL,A.DAT_RECEBIMENTO,SUM(L.QTD_GRAV) GRAVADOS FROM TSMI_ARQUIVO_DOL A "
				+ " left join TSMI_LOTE_DOL L "				
				+ " on (A.COD_ARQUIVO_DOL = L.COD_ARQUIVO_DOL)"
				+ " where  A.COD_ARQUIVO_DOL = " + auxClasse.getCodArquivo()
				+ " group by A.COD_ARQUIVO_DOL,A.DAT_RECEBIMENTO";
			rs=stmt.executeQuery(sCmd);			
			
			if (rs.next()){				
				String datRecebimento = df.format(rs.getDate("DAT_RECEBIMENTO")).toString();
				String qtdGrav= rs.getString("GRAVADOS");
				if ("8".equals(auxClasse.getCodStatus()) && Integer.parseInt(qtdGrav)>0 && Util.DifereDias(datRecebimento,sys.Util.formatedToday().substring(0,10))>nDias)
					auxClasse.setCodStatus("2");					
			}
			
			sCmd  = "UPDATE TSMI_ARQUIVO_DOL SET"
				+ " COD_STATUS = '" + auxClasse.getCodStatus() + "',"
				+ " DAT_PROCESSAMENTO_DETRAN = to_date('" +  auxClasse.getDatProcessamentoDetran() + "','dd/mm/yyyy'),"
				+ " QTD_LOTE_ERRO = " + auxClasse.getQtdLoteErro() 
				+ " WHERE COD_ARQUIVO_DOL = " + auxClasse.getCodArquivo();
			stmt.execute(sCmd);
			
			sCmd  =   "UPDATE TSMI_ARQUIVO_RECEBIDO SET"
				+   " COD_STATUS = '" + auxClasse.getCodStatus() + "',"
				+   " DAT_PROCESSAMENTO_DETRAN = to_date('" +  auxClasse.getDatProcessamentoDetran() + "','dd/mm/yyyy')"
				+   " WHERE NOM_ARQUIVO = '" + auxClasse.getNomArquivo()+"'";
			stmt.execute(sCmd);			
			
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	public boolean ArquivoInsertDol(ArquivoDolBean auxClasse,REC.ParamOrgBean ParamOrgaoId, int seqLote,String Origem)
	throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			String sCmd="";
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			Statement stmt1  = conn.createStatement();
			if(!"BKP".equals(Origem)){
				sCmd   = "SELECT COD_ARQUIVO_DOL from TSMI_ARQUIVO_DOL ";
			}else{ 	
				sCmd   = "SELECT COD_ARQUIVO_DOL from TSMI_ARQUIVO_DOL_BKP ";
			}	
			sCmd +="WHERE nom_arquivo = '" + auxClasse.getNomArquivo() + "'" ;
			if(!"BKP".equals(Origem))
				sCmd +=" and cod_status<>'9'";
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) { 
				auxClasse.setCodStatus("9");
			}
			conn.setAutoCommit(false);
			if(!"BKP".equals(Origem)){
				if ( "9".equals(auxClasse.getCodStatus())==false ) {
					// BUSCAR O NUMERO DO PROXIMO ARQUIVO E ATUALIZAR
					sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
					" where nom_Parametro='PROXIMO_"+auxClasse.getCodIdentArquivo()+"' and cod_orgao='"+auxClasse.getCodOrgao()+"' FOR UPDATE";
					stmt = conn.createStatement();	
					rs  = stmt.executeQuery(sCmd) ;
					int numUltSeq = 0;
					boolean naoExisteNumeracao  = true ;
					while (rs.next()) {
						numUltSeq = rs.getInt("VAL_PARAMETRO");	  			 
						naoExisteNumeracao	= false;			  
					}
					// atualizar NUMERO DE sequencia do arquivo
					String nSeq = "00000"+String.valueOf(numUltSeq) ;			    
					nSeq = nSeq.substring(nSeq.length()-5,nSeq.length()) ;
					if (auxClasse.getNumControleArq().equals(nSeq)==false)	{
						auxClasse.setCodStatus("9");
						auxClasse.getErroArquivo().addElement("Sequencia do Arquivo "+auxClasse.getNomArquivo()+" deveria ser: "+nSeq);
					}
					if ((naoExisteNumeracao))	{
						auxClasse.setCodStatus("9");
						auxClasse.getErroArquivo().addElement("Parametros para numera��o do arquivo "+auxClasse.getNomArquivo()+" n�o localizado para o Org�o: "+auxClasse.getCodOrgao());
					}
					else {
						numUltSeq++;
						nSeq = "00000"+String.valueOf(numUltSeq) ;			    
						nSeq = nSeq.substring(nSeq.length()-5,nSeq.length()) ;
						sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+nSeq+"'" +
						" where nom_Parametro='PROXIMO_"+auxClasse.getCodIdentArquivo()+"' and cod_orgao='"+auxClasse.getCodOrgao()+"'";
						stmt.execute(sCmd);
						ParamOrgaoId.setValParamOrgao("PROXIMO_"+auxClasse.getCodIdentArquivo(),nSeq) ;		
						auxClasse.setProximoArquivo(auxClasse.getCodIdentArquivo() + auxClasse.getNumAnoExercicio() + auxClasse.getCodOrgao() + nSeq);
						nSeq = "00000"+String.valueOf(seqLote) ;			    
						nSeq = nSeq.substring(nSeq.length()-5,nSeq.length()) ;
						sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+nSeq+"'" +
						" where nom_Parametro='PROXIMO_"+auxClasse.getCodIdentArquivo()+"_LOTE' and cod_orgao='"+auxClasse.getCodOrgao()+"'";
						stmt.execute(sCmd);
					}
				}
			}
			// buscar codigo do arquivo
			if(!"BKP".equals(Origem))
				sCmd  = "select seq_TSMI_arquivo_dol.nextval cod_arquivo from dual";
			else
				sCmd  = "select seq_TSMI_arquivo_dol_bkp.nextval cod_arquivo from dual";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				auxClasse.setPkid(auxClasse.getCodArquivo());
			}
			sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " ;
			if(!"BKP".equals(Origem))
				sCmd +="WHERE nom_Parametro='NUM_PROT_ENVIO_DOL' and cod_orgao='"+auxClasse.getCodOrgao()+"' FOR UPDATE";
			else
				sCmd +="WHERE nom_Parametro='NUM_PROT_ENVIO_DOL_BKP' and cod_orgao='"+auxClasse.getCodOrgao()+"' FOR UPDATE";
			rs = stmt.executeQuery(sCmd) ;
			int numProtocolo = 0;
			while (rs.next()) {
				numProtocolo = rs.getInt("VAL_PARAMETRO");
			}
			if (numProtocolo == 0)
				if(!"BKP".equals(Origem))
					ParamOrgaoId.getParamOrgao("NUM_PROT_ENVIO_DOL", "1", "10");
				else
					ParamOrgaoId.getParamOrgao("NUM_PROT_ENVIO_DOL_BKP", "1", "10");
			
			auxClasse.setNumProtocolo(String.valueOf(numProtocolo));
			String nSeq = "00000"+(++numProtocolo);			    
			nSeq = nSeq.substring(nSeq.length()-5,nSeq.length()) ;
			sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+nSeq+"'";
			if(!"BKP".equals(Origem))
				sCmd +=" WHERE nom_Parametro='NUM_PROT_ENVIO_DOL' and cod_orgao='"+auxClasse.getCodOrgao()+"'";
			else
				sCmd +=" WHERE nom_Parametro='NUM_PROT_ENVIO_DOL_BKP' and cod_orgao='"+auxClasse.getCodOrgao()+"'";
			stmt.execute(sCmd);
			
			// inserir o arquivo na tabela de controle
			if(!"BKP".equals(Origem))
				sCmd  = "INSERT INTO TSMI_ARQUIVO_DOL (cod_arquivo_dol,";
			else
				sCmd  = "INSERT INTO TSMI_ARQUIVO_DOL_BKP (cod_arquivo_dol,";
			
			sCmd +="nom_arquivo,dat_recebimento, cod_ident_arquivo, ";
			sCmd +="num_ano_exercicio, num_controle_arquivo,num_protocolo, ";
			sCmd +="cod_orgao,cod_orgao_lotacao,sig_orgao_lotacao,"; 
			sCmd +="nom_username,cod_status,qtd_lote,qtd_lote_erro,nom_responsavel,ind_p59,num_versao)";
			sCmd +="VALUES (" + auxClasse.getCodArquivo() + ", " ;
			sCmd +="'"+auxClasse.getNomArquivo()+"', ";
			sCmd +="sysdate, " ;
			sCmd +="'"+auxClasse.getCodIdentArquivo()+"', " ;
			sCmd +="'"+auxClasse.getNumAnoExercicio()+"', " ;
			sCmd +="'"+auxClasse.getNumControleArq()+"', " ;
			sCmd +="'"+auxClasse.getNumProtocolo()+"', " ;
			sCmd +="'"+auxClasse.getCodOrgao()+"', " ;
			sCmd +="'"+auxClasse.getCodOrgaoLotacao()+"', " ;
			sCmd +="'"+auxClasse.getSigOrgaoLotacao()+"', " ;
			sCmd +="'"+auxClasse.getNomUsername()+"', " ;
			sCmd +="'"+auxClasse.getCodStatus()+"', " ;
			sCmd +="'"+auxClasse.getQtdLote()+"', " ;
			sCmd +="'"+auxClasse.getQtdLoteErro()+"', " ;
			sCmd +="'"+auxClasse.getResponsavelArq()+"'," ;	
			sCmd +="'"+auxClasse.getDscPortaria59()+"'," ;	
			sCmd +="'"+auxClasse.getNumVersao()+"')" ;	
			stmt.execute(sCmd);
			
			//passo a inserir os dados na TSMI_ARQUIVO_RECEBIDO
			
			sCmd  = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (cod_arquivo,qtd_reg,";			
			sCmd +="nom_arquivo,dat_recebimento, cod_ident_arquivo, ";
			sCmd +="num_ano_exercicio, num_controle_arquivo,num_protocolo, ";
			sCmd +="cod_orgao,cod_orgao_lotacao,sig_orgao_lotacao,"; 
			sCmd +="nom_username,cod_status,ind_p59, nom_responsavel)";
			sCmd +="VALUES (SEQ_TSMI_ARQUIVO_RECEBIDO.NEXTVAL,'"+auxClasse.getQtdReg()+"',";
			sCmd +="'"+auxClasse.getNomArquivo()+"', ";
			sCmd +="sysdate, " ;
			
			if(!"BKP".equals(Origem))
				sCmd +="'"+auxClasse.getCodIdentArquivo()+"', " ;
			else			
				sCmd +="'BKP', " ;
			
			sCmd +="'"+auxClasse.getNumAnoExercicio()+"', " ;
			sCmd +="'"+auxClasse.getNumControleArq()+"', " ;
			sCmd +="'"+auxClasse.getNumProtocolo()+"', " ;
			sCmd +="'"+auxClasse.getCodOrgao()+"', " ;
			sCmd +="'"+auxClasse.getCodOrgaoLotacao()+"', " ;
			sCmd +="'"+auxClasse.getSigOrgaoLotacao()+"', " ;
			sCmd +="'"+auxClasse.getNomUsername()+"', " ;
			sCmd +="'"+auxClasse.getCodStatus()+"', " ;			
			sCmd +="'"+auxClasse.getDscPortaria59()+"'," ;	
			sCmd +="'"+auxClasse.getResponsavelArq()+"')" ;	
			stmt1.execute(sCmd);			
			
			// Inserir Lotes
			List lotes = auxClasse.getLote();
			LoteDolBean lote;
			String arqdol="";
			int num_seq_linha;
			int quantlote=auxClasse.getLote().size();
			int contLote=0;
			while(lotes.isEmpty() == false){ 
				num_seq_linha = 0;
				lote = (LoteDolBean)lotes.remove(0);
				// buscar codigo do arquivo
				if(!"BKP".equals(Origem)){					
					arqdol=ApagaBackupDol(lote);
				}
				if(!"BKP".equals(Origem))
					sCmd  = "select seq_TSMI_lote_dol.nextval cod_lote from dual";				
				else
					sCmd  = "select seq_TSMI_lote_dol_bkp.nextval cod_lote from dual";
				
				rs    = stmt.executeQuery(sCmd) ;
				rs.next();
				lote.setCodLote(rs.getString("cod_lote"));
				lote.setCodArquivo(auxClasse.getCodArquivo());
				
				if ("9".equals(auxClasse.getCodStatus())==true){	
					lote.setCodStatus("9");
				}
				if(!"BKP".equals(Origem))
					sCmd= "INSERT INTO TSMI_LOTE_DOL (cod_arquivo_dol,";
				else
					sCmd= "INSERT INTO TSMI_LOTE_DOL_BKP (cod_arquivo_dol,";
				
				sCmd+="cod_lote,cod_ident_arquivo,num_ano_exercicio,dat_lote,";
				sCmd+="num_lote,qtd_reg,qtd_linha_erro,";
				
				if(!"BKP".equals(Origem))
					sCmd+="ind_emissao_notif_detran,cod_status,cod_unidade)";
				else
					sCmd+="ind_emissao_notif_detran,cod_status,cod_unidade,cod_lote_dol)";
				
				sCmd+="VALUES (" + lote.getCodArquivo() + ", " ;
				sCmd+="'"+lote.getCodLote()+"', " ;
				sCmd+="'"+lote.getCodIdentArquivo()+"', " ;
				sCmd+="'"+lote.getNumAnoExercicio()+"', " ;
				sCmd+="'"+lote.getDatLote()+"', " ;
				sCmd+="'"+lote.getNumLote()+"', " ;
				sCmd+="'"+lote.getQtdReg()+"', " ;
				sCmd+="'"+lote.getQtdLinhaErro()+"', " ;
				sCmd+="'"+lote.getIndEmissaoNotifDetran()+"', " ;
				sCmd+="'"+lote.getCodStatus()+"', " ;
				
				if(!"BKP".equals(Origem))
					sCmd+="'"+lote.getCodUnidade()+"')";
				else
					sCmd+="'"+lote.getCodUnidade()+"',"+lote.getCodLoteDol()+")";
				
				stmt.execute(sCmd);
				
				if ("9".equals(auxClasse.getCodStatus())==false){	
					// Inserir as linhas
					Iterator it  = lote.getLinhaLoteDol().iterator() ;	
					String linha = "";
					while (it.hasNext()) 
					{
						num_seq_linha++;
						linha =(String)it.next() ;
						String numAuto =  linha.substring(41,53);
						String numPlaca = linha.substring(53,60);
						if(!"BKP".equals(Origem))
							sCmd  = "INSERT INTO TSMI_LINHA_LOTE (cod_linha_lote, dsc_linha_lote, num_seq_linha, cod_lote, cod_status,num_auto_infracao,num_placa)";
						else
							sCmd  = "INSERT INTO TSMI_LINHA_LOTE_BKP (cod_linha_lote, dsc_linha_lote, num_seq_linha, cod_lote, cod_status,num_auto_infracao,num_placa)";
						
						if(!"BKP".equals(Origem))
							sCmd+="VALUES (seq_TSMI_linha_lote.nextval, " ;
						else
							sCmd+="VALUES (seq_TSMI_linha_lote_bkp.nextval, " ;
						
						sCmd+="'"+linha.trim()+"','"+num_seq_linha+"',"+lote.getCodLote()+",0,'"+numAuto.trim()+"','"+numPlaca+"')" ;						
						stmt.execute(sCmd);
					}
				}
				contLote++;
				if (contLote==quantlote && !"BKP".equals(Origem)){					
					sCmd  = "delete tsmi_arquivo_dol_bkp where cod_arquivo_dol='"+arqdol+"'";
					
					rs    = stmt.executeQuery(sCmd) ;
				}
			}
			stmt.close();
			stmt1.close();
			rs.close();
			conn.commit();
			existe = true ;
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if( existe == false )
						conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	
	public boolean ArquivoExisteDol(ArquivoDolBean auxClasse) throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT COD_ARQUIVO_DOL from TSMI_ARQUIVO_DOL "+
			"WHERE nom_arquivo = '" + auxClasse.getNomArquivo() + "'" +
			" and cod_status <> '9'" ;
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				existe = true ;
			}
			stmt.close();
			rs.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	
	
	
	public void LoteUpdateDol(LoteDolBean auxClasse) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;
		
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TSMI_LOTE_DOL SET"
				+ " COD_STATUS = " + auxClasse.getCodStatus() + ","
				+ " QTD_LINHA_ERRO = " + auxClasse.getQtdLinhaErro()
				+ " WHERE COD_LOTE = " + auxClasse.getCodLote();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	public void LinhaLoteUpdateDol(LinhaLoteDolBean auxClasse) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;        
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			String sCmd = "UPDATE TSMI_LINHA_LOTE SET"
				+ " COD_STATUS = " + auxClasse.getCodStatus() + ","
				+ " COD_RETORNO = '"+ auxClasse.getCodRetorno() + "',"
				+ " DSC_LINHA_LOTE_RETORNO = '"+ auxClasse.getDscLinhaLoteRetorno() + "'"
				+ " WHERE COD_LINHA_LOTE = "+ auxClasse.getCodLinhaLote();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	public void LogarLinhaLoteDol(LinhaLoteDolBean auxClasse) throws DaoException {
		
		Connection conn = null;
		boolean eConnLocal = false;        
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			String sCmd = "INSERT INTO TSMI_PROC_LINHA_LOTE_LOG"
				+ " (COD_PROC_LINHA_LOTE_LOG, DAT_PROC, COD_RETORNO, DSC_LINHA_LOTE_RETORNO, COD_LINHA_LOTE)"
				+ " VALUES (SEQ_TSMI_PROC_LINHA_LOTE_LOG.NEXTVAL, SYSDATE, '" + auxClasse.getCodRetorno()
				+ "', '" + auxClasse.getDscLinhaLoteRetorno() + "', " + auxClasse.getCodLinhaLote() + ")";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**---------------------------------------
	 * DAO Relativo MonitoramentoBean
	 * @author Luciana Rocha
	 ----------------------------------------*/	
	public boolean ConsultaDadosMonitor(MonitoraBean auxClasse, String ordem) throws DaoException {	
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		String orderBy = "";
		String condicao = "where 1=1 ";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			auxClasse.setDados(new ArrayList());
			//Montar Order By
			if (ordem.equals("O")&&auxClasse.getCodOrgao().equals("0")) orderBy = " order by o.sig_orgao ";
			if (ordem.equals("A"))  orderBy = " order by nom_objeto ";
			if (ordem.equals("R"))  orderBy = " order by nom_processo" ;
			if (ordem.equals("D"))  orderBy = " order by dat_log" ;
			
			//Montar a condicao Where
			if (!auxClasse.getCodOrgao().equals("0")) condicao += " AND cod_orgao = '"+auxClasse.getCodOrgao()+"'" ;
			if (auxClasse.getCodOrgao().equals("0")) condicao += " AND m.cod_orgao = o.cod_orgao" ;
			if (!auxClasse.getNomProcesso().trim().equals("")) condicao += " AND nom_processo= '"+auxClasse.getNomProcesso()+"'" ;
			if (!auxClasse.getDatIni().trim().equals("")) condicao += " AND to_char(dat_log,'dd/mm/yyyy') BETWEEN '" + auxClasse.getDatIni() + "' and '" + auxClasse.getDatFim() + "'";
			
			// prepara os dados sem orgao selecionado
			if (auxClasse.getCodOrgao().equals("0")){
				sCmd = "SELECT m.dat_log, m.txt_mensagem, m.cod_tipo_mensagem, m.cod_orgao," +
				"m.nom_objeto, m.nom_processo, o.sig_orgao FROM TSMI_MONITOR m, TSMI_ORGAO O " + condicao + orderBy;
			}
			//prepara os dados com orgao selecionado
			if (!auxClasse.getCodOrgao().equals("0")){
				sCmd = "SELECT dat_log, txt_mensagem, cod_tipo_mensagem, cod_orgao," +
				"nom_objeto, nom_processo FROM TSMI_MONITOR  " + condicao + orderBy;
			}
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				MonitoraBean dados = new MonitoraBean();
				dados.setDatLog(rs.getString("dat_log"));
				dados.setCodOrgao(rs.getString("cod_orgao"));
				dados.setNomObjeto(rs.getString("nom_objeto"));
				dados.setNomProcesso(rs.getString("nom_processo"));
				dados.setTxtMensagem(rs.getString("txt_mensagem"));
				dados.setCodTipoMensagem(rs.getString("cod_tipo_mensagem"));
				if (auxClasse.getCodOrgao().equals("0")) dados.setSigOrgao(rs.getString("sig_orgao"));
				if (!auxClasse.getCodOrgao().equals("0"))dados.setSigOrgao(auxClasse.getSigOrgao());
				auxClasse.getDados().add(dados);
				//Configura cores para as mensagens
				if(rs.getString("cod_tipo_mensagem").equals("ERRO"))dados.setCorMsg(dados.COR_MSG_ERRO);
				else dados.setCorMsg(dados.COR_MSG_OK);
				ok = true;
			}
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}
	
	/*--------------------------------
	 * DAO Relativo ao GerenciarRobotBean
	 * @author Luciana Rocha
	 --------------------------------- */
	public boolean consultaStatusRobot(String nomRobot, GerenciarRobotBean dados) throws DaoException {		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		GerenciarRobotBean robo = new GerenciarRobotBean();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			/* Verifica Status do Robot
			 * ind_Continua = 1 (robot parado)
			 * ind_Continua = 0 (robot liberado)  */
			sCmd = "SELECT G.IND_CONTINUA, R.DAT_ULTIMA_EXEC FROM TSMI_GERENCIA_ROBO G, TSMI_GERENCIA_ROBOT R  " +
			"WHERE G.COD_TIPO_REGISTRO = '"+ robo.COD_TIPO_REGISTRO+"'" +
			" and G.NOM_ROBO = '"  + nomRobot + "' AND G.NOM_ROBO = R.NOM_ROBOT";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				dados.setSituacaoRobo(rs.getString("IND_CONTINUA"));
				dados.setDatUltimaExec(rs.getString("DAT_ULTIMA_EXEC"));
			}
			rs.close();
			stmt.close();
			ok = true; 
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ok;
	}
	
	
	public boolean liberaRobot(String nomRobot) throws DaoException {		
		Connection conn = null;     
		String sCmd = "";
		boolean ok = false;
		GerenciarRobotBean robo = new GerenciarRobotBean();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TSMI_GERENCIA_ROBO  set ind_continua ='"+ robo.STATUS_LIBERADO+"'"+
			" WHERE nom_robo = '"+ nomRobot+"'" +
			" AND COD_TIPO_REGISTRO = "+ robo.COD_TIPO_REGISTRO;
			
			stmt.executeUpdate(sCmd);
			
			stmt.close();
			
			ok = true; 
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ok;
	}
	
	public boolean paraRobot(String nomRobot) throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		GerenciarRobotBean robo = new GerenciarRobotBean();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = "UPDATE TSMI_GERENCIA_ROBO  set ind_continua = '"+ robo.STATUS_PARADO+"'"+
			" WHERE nom_robo = '"+ nomRobot+"'"+
			" AND COD_TIPO_REGISTRO = " + robo.COD_TIPO_REGISTRO;
			
			stmt.execute(sCmd);
			
			stmt.close();
			ok = true; 
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ok;
	}
	
	/*-------------------------------------
	 * DAO Relativo ao DefinirPrioridadBean
	 * @author Luciana Rocha
	 --------------------------------------*/	  
	/**
	 * M�todo respons�vel por consultar arquivos
	 * com Status 0, 7 e 8
	 * @param bean
	 * @return ok
	 * @throws DaoException
	 */
	public boolean consultaStatusArquivo(DefinirPrioridadeBean bean) 
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			bean.setDados(new ArrayList());
			
			/* Verifica Arquivos com Status 0, 7 e 8 */
			sCmd = "SELECT nom_arquivo,cod_status, ind_prioridade  FROM TSMI_ARQUIVO_RECEBIDO " +
			"WHERE cod_ORGAO = '"+ bean.getCodOrgao()+"' AND cod_status in (0,7,8)";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				DefinirPrioridadeBean dados = new DefinirPrioridadeBean();	
				dados.setNomArquivo(rs.getString("nom_arquivo"));
				
				if(rs.getString("cod_status") == null) dados.setCodStatus(" ");
				else dados.setCodStatus(rs.getString("cod_status"));
				
				if(rs.getString("ind_prioridade") == null) dados.setIndPrioridade(" ");
				else dados.setIndPrioridade(rs.getString("ind_prioridade"));
				
				bean.getDados().add(dados);
			}
			rs.close();
			stmt.close();
			ok = true; 
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}	
	
	/**
	 * M�todo respons�vel pela atualiza
	 * o indice de prioridade de processamento 
	 * do Robo.
	 * @param bean
	 * @return ok
	 * @throws DaoException
	 */
	public boolean alterarIndicePrioridade(DefinirPrioridadeBean bean) 
	throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			Vector vPrioridade = bean.getVetorPrioridade();
			for (int i = 0 ; i< vPrioridade.size(); i++) {
				DefinirPrioridadeBean arqBean = (DefinirPrioridadeBean)vPrioridade.elementAt(i) ;
				sCmd = "UPDATE TSMI_ARQUIVO_RECEBIDO  " +
				"set ind_prioridade = '"+ arqBean.getIndPrioridade().trim()+"'"+
				" WHERE cod_orgao = '"+ arqBean.getCodOrgao()+"' and " +
				"nom_arquivo ='"+ arqBean.getNomArquivo() +"'";
				
				stmt.execute(sCmd);
				ok = true;
			} 
			stmt.close();
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}    
	
	/*-----------------------------------------
	 * DAO Relativo ao DefinirPrioridadeDolBean
	 * @author Luciana Rocha
	 ------------------------------------------*/
	
	/**
	 * M�todo respons�vel por consultar arquivos
	 * com Status 0, 7 e 8
	 * @param bean
	 * @return ok
	 * @throws DaoException
	 */
	public boolean consultaStatusArquivoDol(DefinirPrioridadeDolBean bean) 
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			bean.setDados(new ArrayList());
			
			/* Verifica Arquivos com Status 0, 7 e 8 */
			sCmd = "SELECT nom_arquivo,cod_status, ind_prioridade  FROM TSMI_ARQUIVO_DOL " +
			"WHERE cod_ORGAO = '"+ bean.getCodOrgao()+"' AND cod_status in (0,7,8)";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				DefinirPrioridadeBean dados = new DefinirPrioridadeBean();	
				dados.setNomArquivo(rs.getString("nom_arquivo"));
				
				if(rs.getString("cod_status") == null) dados.setCodStatus(" ");
				else dados.setCodStatus(rs.getString("cod_status"));
				
				if(rs.getString("ind_prioridade") == null) dados.setIndPrioridade(" ");
				else dados.setIndPrioridade(rs.getString("ind_prioridade"));
				
				bean.getDados().add(dados);
			}
			rs.close();
			stmt.close();
			ok = true; 
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}	
	
	/**
	 * M�todo respons�vel pela atualiza
	 * o indice de prioridade de processamento 
	 * do DOL.
	 * @param bean
	 * @return ok
	 * @throws DaoException
	 */
	public boolean alterarIndicePrioridadeDol(DefinirPrioridadeDolBean bean) 
	throws DaoException {
		
		Connection conn = null;
		String sCmd = "";
		boolean ok = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			Vector vPrioridade = bean.getVetorPrioridade();
			for (int i = 0 ; i< vPrioridade.size(); i++) {
				DefinirPrioridadeBean arqBean = (DefinirPrioridadeBean)vPrioridade.elementAt(i) ;
				sCmd = "UPDATE TSMI_ARQUIVO_DOL  " +
				"set ind_prioridade = '"+ arqBean.getIndPrioridade().trim()+"'"+
				" WHERE cod_orgao = '"+ arqBean.getCodOrgao()+"' and " +
				"nom_arquivo ='"+ arqBean.getNomArquivo() +"'";
				
				stmt.execute(sCmd);
				ok = true;
			} 
			stmt.close();
		}catch (SQLException e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			ok = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return ok;
	}	
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos para Consulta - Filtros: Orgao,Tipo Arquivo,Data
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscaArquivosDOL(ACSS.UsuarioBean usrLogado, String nomArquivo, String dataIni,
			String dataFim,boolean consulta) throws DaoException {
		
		ArquivoDolBean relClasse;
		LoteDolBean relClasseLote;
		Vector vRetClasse = new Vector();
		String sCmd ="";
		String filtro = "";
		Connection conn = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			Statement stmt = conn.createStatement();
			if (!sHistorico.equals("S")) {	
				sCmd =
					"SELECT DISTINCT A.COD_ARQUIVO_DOL,A.NOM_ARQUIVO,A.QTD_LOTE," 
					+ " A.DAT_RECEBIMENTO,A.COD_STATUS,to_char(A.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO_CHAR,"
					+ " to_char(A.DAT_PROCESSAMENTO_DETRAN,'dd/mm/yyyy') as DAT_PROCESSAMENTO"
					+ " FROM TSMI_ARQUIVO_DOL A"
					+ " WHERE COD_ORGAO = " + usrLogado.getCodOrgaoAtuacao()
					+ "	AND A.DAT_RECEBIMENTO BETWEEN"
					+ " to_date('" + dataIni + "','dd/mm/yyyy')" 
					+ " AND to_date('" + dataFim + "','dd/mm/yyyy') + 0.99999";
				if (!nomArquivo.equals("0")) {
					filtro += " AND A.NOM_ARQUIVO ='" + nomArquivo + "'";
				}
				sCmd += filtro + " ORDER BY A.DAT_RECEBIMENTO DESC";
			}else{
				sCmd =
					" SELECT DISTINCT A.COD_ARQUIVO_DOL,A.NOM_ARQUIVO,A.QTD_LOTE,A.DAT_RECEBIMENTO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO_CHAR,"
					+ " TO_CHAR(A.DAT_PROCESSAMENTO_DETRAN,'dd/mm/yyyy') as DAT_PROCESSAMENTO  "
					+ " FROM TSMI_ARQUIVO_DOL A"
					+ " WHERE A.COD_ORGAO = " + usrLogado.getCodOrgaoAtuacao()
					+ "	AND A.DAT_RECEBIMENTO BETWEEN"
					+ " to_date('" + dataIni + "','dd/mm/yyyy')" 
					+ " AND to_date('" + dataFim + "','dd/mm/yyyy') + 0.99999"+
					" UNION"+
					" SELECT DISTINCT B.COD_ARQUIVO_DOL,B.NOM_ARQUIVO,B.QTD_LOTE,B.DAT_RECEBIMENTO,B.COD_STATUS,TO_CHAR(B.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO_CHAR,"
					+ " TO_CHAR(B.DAT_PROCESSAMENTO_DETRAN,'dd/mm/yyyy') as DAT_PROCESSAMENTO"
					+ " FROM TSMI_ARQUIVO_DOL_BKP@SMITPBKP B"
					+ " WHERE B.COD_ORGAO = " + usrLogado.getCodOrgaoAtuacao()
					+ "	AND B.DAT_RECEBIMENTO BETWEEN"
					+ " to_date('" + dataIni + "','dd/mm/yyyy')" 
					+ " AND to_date('" + dataFim + "','dd/mm/yyyy') + 0.99999";
				if (!nomArquivo.equals("0")) {
					filtro += " AND NOM_ARQUIVO ='" + nomArquivo + "'";
				}
				sCmd += filtro + " ORDER BY DAT_RECEBIMENTO DESC";
			} 
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				relClasse = new ArquivoDolBean();
				relClasse.setCodArquivo(rs.getString("cod_arquivo_dol"));
				relClasse.setNomArquivo(rs.getString("nom_arquivo"));
				relClasse.setCodStatus(rs.getString("COD_STATUS"));
				relClasse.setQtdLote(rs.getString("QTD_LOTE"));
				relClasse.setDatRecebimento(rs.getString("dat_recebimento_char"));
				relClasse.setDatProcessamentoDetran(rs.getString("DAT_PROCESSAMENTO"));
				if (!sHistorico.equals("S")) {
					sCmd =
						"SELECT B.COD_LOTE,B.COD_STATUS,B.COD_IDENT_ARQUIVO,B.NUM_LOTE,B.QTD_REG,B.QTD_LINHA_ERRO,B.QTD_GRAV,B.QTD_CANC"
						+ " FROM TSMI_LOTE_DOL B"
						+ " WHERE COD_ARQUIVO_DOL="
						+ rs.getString("cod_arquivo_dol");
				}else {
					sCmd=" SELECT B.COD_LOTE,B.COD_STATUS,B.COD_IDENT_ARQUIVO,B.NUM_LOTE,B.QTD_REG,B.QTD_LINHA_ERRO,B.QTD_GRAV,B.QTD_CANC"+
					" FROM TSMI_LOTE_DOL B"+
					" WHERE COD_ARQUIVO_DOL="+
					rs.getString("cod_arquivo_dol")+
					" UNION"+
					" SELECT G.COD_LOTE,G.COD_STATUS,G.COD_IDENT_ARQUIVO,G.NUM_LOTE,G.QTD_REG,G.QTD_LINHA_ERRO,G.QTD_GRAV,G.QTD_CANC"+
					" FROM TSMI_LOTE_DOL_BKP@SMITPBKP G"+
					" WHERE COD_ARQUIVO_DOL="+ 
					rs.getString("cod_arquivo_dol");
				}
				Statement stmt2 = conn.createStatement();
				ResultSet rsLote = stmt2.executeQuery(sCmd);
				List lote = new ArrayList();
				while (rsLote.next()) {
					relClasseLote = new LoteDolBean();
					relClasseLote.setCodLote(rsLote.getString("COD_LOTE"));
					relClasseLote.setCodStatus(rsLote.getString("COD_STATUS"));
					relClasseLote.setCodIdentArquivo(rsLote.getString("COD_IDENT_ARQUIVO"));
					relClasseLote.setQtdReg(rsLote.getString("QTD_REG"));
					relClasseLote.setQtdLinhaErro(rsLote.getString("QTD_LINHA_ERRO"));
					relClasseLote.setQtdGrav(rsLote.getString("QTD_GRAV"));
					relClasseLote.setQtdCanc(rsLote.getString("QTD_CANC"));
					relClasseLote.setNumLote(rsLote.getString("NUM_LOTE"));
					lote.add(relClasseLote);
				}
				rsLote.close();
				stmt2.close();
				relClasse.setLote(lote);
				vRetClasse.addElement(relClasse);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vRetClasse;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca Arquivo Recebido recebendo COD_LINHA_LOTE
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public ArquivoDolBean BuscaArquivosDOLLinhaLote(
			String codLinhaLote)
	throws DaoException {
		ArquivoDolBean relClasse;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT C.COD_ARQUIVO_DOL,C.NOM_ARQUIVO,C.QTD_LOTE,C.DAT_RECEBIMENTO,C.COD_STATUS,TO_CHAR(C.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO_CHAR,c.qtd_lote_erro,C.COD_ORGAO_LOTACAO,C.NOM_USERNAME,C.COD_ORGAO"
				+ " FROM TSMI_LINHA_LOTE A INNER JOIN TSMI_LOTE_DOL B"
				+ " ON (A.COD_LOTE=B.COD_LOTE)"
				+ " INNER JOIN TSMI_ARQUIVO_DOL C"
				+ " ON (B.COD_ARQUIVO_DOL=C.COD_ARQUIVO_DOL)"
				+ " WHERE A.COD_LINHA_LOTE=" + codLinhaLote;
			
			relClasse = new ArquivoDolBean();
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {			
				relClasse.setCodArquivo(rs.getString("cod_arquivo_dol"));
				relClasse.setNomArquivo(rs.getString("nom_arquivo"));
				relClasse.setCodStatus(rs.getString("COD_STATUS"));
				relClasse.setQtdLote(rs.getString("QTD_LOTE"));
				relClasse.setDatRecebimento(rs.getString("dat_recebimento_char"));
				relClasse.setNomUsername(rs.getString("NOM_USERNAME"));
				relClasse.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				relClasse.setCodOrgao(rs.getString("COD_ORGAO"));
			}
			
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return relClasse;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca LINHAS de determinado LOTE do DOL - TSMI_LINHA_LOTE
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscaLinhaLoteDOL(String[] campos, String[] valores, String condicao,String tipoConsulta) throws DaoException {
		
		if (campos.length != valores.length)
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		Vector vetLinhas = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			 ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			 ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			 ParamSistemaBeanId.PreparaParam();
			 String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			 if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			String descerro = "";
			Statement stmtLin = conn.createStatement();
			String sCmdLin =
				"SELECT cod_lote, cod_linha_lote, cod_status, DSC_ERRO,"
				+ " num_seq_linha, dsc_linha_lote,SIT_CANC_DOL,"
				+ " cod_retorno, dsc_linha_lote_retorno"
				+ " FROM TSMI_LINHA_LOTE a LEFT JOIN TSMI_ERRO_BROKER B" 
				+ " ON (A.COD_RETORNO=B.COD_ERRO)"
				+ sys.Util.montarWHERE(campos, valores)+ condicao;
			if (sHistorico.equals("S")) 
			{
				sCmdLin+= " union "
					+ " SELECT cod_lote, cod_linha_lote, cod_status, DSC_ERRO,"
					+ " num_seq_linha, dsc_linha_lote,SIT_CANC_DOL,"
					+ " cod_retorno, dsc_linha_lote_retorno"
					+ " FROM TSMI_LINHA_LOTE_BKP@SMITPBKP a LEFT JOIN TSMI_ERRO_BROKER B" 
					+ " ON (A.COD_RETORNO=B.COD_ERRO)"
					+ sys.Util.montarWHERE(campos, valores)+ condicao;
			}
			ResultSet rsLin = stmtLin.executeQuery(sCmdLin);
			while (rsLin.next()) {
				descerro = rsLin.getString("dsc_erro");
				if (descerro == null) descerro = ""; 
				LinhaLoteDolBean linhaRec = new LinhaLoteDolBean();
				if (tipoConsulta.equals("ENVIADOS") || tipoConsulta.equals("GRAVADOS")){
					linhaRec.setCodLote(rsLin.getString("cod_lote"));
					linhaRec.setCodLinhaLote(rsLin.getString("cod_linha_lote"));
					linhaRec.setDscLinhaLote(sys.Util.rPad(rsLin.getString("dsc_linha_lote")," ",1094));
					linhaRec.setDscLinhaLoteNIT(linhaRec.getDscLinhaLote());
					linhaRec.setNumSeqLinha(rsLin.getString("num_seq_linha"));
					linhaRec.setCodStatus(rsLin.getString("cod_status"));
					linhaRec.setCodRetorno(rsLin.getString("cod_retorno"));
					linhaRec.setDscErro(descerro);
					linhaRec.setDscLinhaLoteRetorno(rsLin.getString("dsc_linha_lote_retorno"));
					vetLinhas.add(linhaRec);					
				}else {
					if(!descerro.equals("TRANSA��O REALIZADA COM SUCESSO")||tipoConsulta.equals("") || "S".equals(rsLin.getString("SIT_CANC_DOL"))){
						linhaRec.setCodLote(rsLin.getString("cod_lote"));
						linhaRec.setCodLinhaLote(rsLin.getString("cod_linha_lote"));
						linhaRec.setDscLinhaLote(sys.Util.rPad(rsLin.getString("dsc_linha_lote")," ",1094));
						linhaRec.setDscLinhaLoteNIT(linhaRec.getDscLinhaLote());
						linhaRec.setNumSeqLinha(rsLin.getString("num_seq_linha"));
						linhaRec.setCodStatus(rsLin.getString("cod_status"));
						linhaRec.setCodRetorno(rsLin.getString("cod_retorno"));
						linhaRec.setDscErro(descerro);
						linhaRec.setDscLinhaLoteRetorno(rsLin.getString("dsc_linha_lote_retorno"));
						vetLinhas.add(linhaRec);						
					}				
				}
			}
			rsLin.close();
			stmtLin.close();
			
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vetLinhas;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Combo utilizada na tela de acertar Auto
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public String carregaComboUF()
	throws DaoException {
		String comboUF="";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT COD_UF FROM TSMI_UF";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()){
				comboUF += ",\""+rs.getString("cod_uf") +"\"";
			}
			
			comboUF = comboUF.substring(1,comboUF.length()) ;
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return comboUF;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Lista os arquivos DOL
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public Vector carregaListaArquivosDOL(ACSS.UsuarioBean usrLogado, String dataIni, String dataFim,boolean consulta) throws DaoException {			
		Vector vRet   = new Vector();     
		Connection conn = null;
		String sCmd="";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			if (!consulta){
				sCmd ="SELECT DISTINCT NOM_ARQUIVO, DAT_RECEBIMENTO FROM TSMI_ARQUIVO_DOL" +
				" WHERE COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() +
				"	AND DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('"
				+ dataFim + "','dd/mm/yyyy') + 0.99999 ORDER BY DAT_RECEBIMENTO DESC";            
			}else 
				sCmd =
					" SELECT DISTINCT NOM_ARQUIVO, DAT_RECEBIMENTO FROM TSMI_ARQUIVO_DOL" +
					" WHERE COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() +				
					"   AND DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('"
					+ dataFim + "','dd/mm/yyyy') + 0.99999 "+    
					" union" +
					" SELECT DISTINCT NOM_ARQUIVO, DAT_RECEBIMENTO FROM TSMI_ARQUIVO_DOL_BKP"+
					" WHERE COD_ORGAO=" + usrLogado.getCodOrgaoAtuacao() +
					"	AND DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('"
					+ dataFim + "','dd/mm/yyyy') + 0.99999 ORDER BY DAT_RECEBIMENTO DESC"; 
			
			ResultSet rs = stmt.executeQuery(sCmd);             
			while (rs.next())   {					
				vRet.addElement(rs.getString("nom_arquivo"));           
			}           
			rs.close();
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRet;
	}
	
	
	/*------------------------------------------------------------------------
	 * Atualiza Auto em TSMI_LINHA_LOTE
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public void AcertaAutoDol(LinhaLoteDolBean LinhaLoteDolBean) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			String sCmd="";
			Statement stmt = conn.createStatement();
			sCmd="INSERT INTO TSMI_HIST_LINHA_LOTE" +
			" SELECT COD_LINHA_LOTE, SEQ_TSMI_HIST_LINHA_LOTE.NEXTVAL, NUM_SEQ_LINHA, DSC_LINHA_LOTE, COD_STATUS, DSC_LINHA_LOTE_RETORNO, COD_RETORNO,SYSDATE" +
			" FROM TSMI_LINHA_LOTE WHERE COD_LINHA_LOTE=" + LinhaLoteDolBean.getCodLinhaLote();
			stmt.executeQuery(sCmd);
			
			if (LinhaLoteDolBean.getInd_P59().equals("P59")){
				
				if (LinhaLoteDolBean.getCodOrgao().equals("258650")) { //Layout de Niteroi
					sCmd =	"UPDATE TSMI_LINHA_LOTE SET DSC_LINHA_LOTE='" + LinhaLoteDolBean.getDscLinhaLoteNITP59() + "'"
					+ " WHERE COD_LINHA_LOTE=" + LinhaLoteDolBean.getCodLinhaLote();
					stmt.executeQuery(sCmd);
				}else{
					sCmd =	"UPDATE TSMI_LINHA_LOTE SET DSC_LINHA_LOTE='" + LinhaLoteDolBean.getDscLinhaLoteP59() + "'"
					+ " WHERE COD_LINHA_LOTE=" + LinhaLoteDolBean.getCodLinhaLote();
					stmt.executeQuery(sCmd);}
			}else{
					if (LinhaLoteDolBean.getCodOrgao().equals("258650")) { //Layout de Niteroi
						sCmd =	"UPDATE TSMI_LINHA_LOTE SET DSC_LINHA_LOTE='" + LinhaLoteDolBean.getDscLinhaLoteNIT() + "'"
						+ " WHERE COD_LINHA_LOTE=" + LinhaLoteDolBean.getCodLinhaLote();
						stmt.executeQuery(sCmd);
					}else{
						sCmd =	"UPDATE TSMI_LINHA_LOTE SET DSC_LINHA_LOTE='" + LinhaLoteDolBean.getDscLinhaLote() + "'"
						+ " WHERE COD_LINHA_LOTE=" + LinhaLoteDolBean.getCodLinhaLote();
						stmt.executeQuery(sCmd);
						
					}
			}
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}

	
	/**---------------------------------------
	 * DAO para acertar o auto via BROKER
	 * @author Elisson Dias
	 * Alterada em 03/09/2004
	 ----------------------------------------*/
	public void AcertaAutoDolBroker (ACSS.UsuarioBean usrLogado, ArquivoDolBean ArquivoDolBeanId, LoteDolBean LoteDolBeanId, LinhaLoteDolBean LinhaLoteDolBeanId)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);			
			Statement stmt = conn.createStatement();			
			
			ROB.DaoBroker broker = ROB.DaoBroker.getInstance();
			
			String linha = "";
			
			   if (ArquivoDolBeanId.getDscPortaria59().endsWith("P59"))
			       linha = sys.Util.rPad(LinhaLoteDolBeanId.getDscLinhaLote(), " ", 554).substring(0,554);
			   else    
			       linha = sys.Util.rPad(LinhaLoteDolBeanId.getDscLinhaLote(), " ", 445).substring(0,445);
			
			
			String codMarcaModelo = linha.substring(60,67);
			if ("       ".equals(codMarcaModelo)) codMarcaModelo ="0000000";
			
			linha = linha.substring(0,60)+codMarcaModelo+linha.substring(67,linha.length()); 
			linha =	linha.substring(8,linha.length());
			
			//String linha = LinhaLoteDolBeanId.getDscLinhaLote().substring(8);
			BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();			
			String resultado = broker. TransacaoDOL(usrLogado.getNomUserName(),
					LinhaLoteDolBeanId.getCodOrgaoLotacao(), ArquivoDolBeanId.getTipArqRetorno(),
					LinhaLoteDolBeanId.getCodOrgao(), LoteDolBeanId.getNumLote(), LoteDolBeanId.getNumAnoExercicio(),
					LoteDolBeanId.getDatLote(), LoteDolBeanId.getCodUnidade(), linha, conn,transBRKDOL,ArquivoDolBeanId.getDscPortaria59());			
			
			//Trocar as aspas simples por aspas no padrao oracle
			resultado = resultado.replaceAll("'", "''");
			//Pega os retornos da linha
			String codRetorno = resultado.substring(0, 3);
			String linhaRetorno = "";
			if (!sys.Util.isNumber(codRetorno)) {
				linhaRetorno = resultado.substring(3);
			} else {
				linhaRetorno = resultado.substring(5);
			}
			if (linhaRetorno.length() > 900)
				linhaRetorno = linhaRetorno.substring(0, 900);
			linhaRetorno = linhaRetorno.trim();
			//continua Broker
			
			boolean loteComErro = false;			
			if (!codRetorno.equals("000"))				
				loteComErro = true;
			else
				LinhaLoteDolBeanId.gravarAutoInfracao(ArquivoDolBeanId);
			
			//codStatus 0=recebido 1=processado 9=erro no acesso ao Broker
			String codStatus = "9";
			if (sys.Util.isNumber(codRetorno)) {
				if (codRetorno.equals("000")
						|| (Integer.parseInt(codRetorno) >= 401)
						&& (Integer.parseInt(codRetorno) <= 599))
					codStatus = "1";				
			}
			
			String sCmd = "";
			//Gravar Status da Linha
			if(sys.Util.isNumber(codRetorno)){
				sCmd =
					"UPDATE TSMI_LINHA_LOTE SET COD_STATUS = "
					+ codStatus
					+ ", "
					+ " COD_RETORNO = '"
					+ codRetorno
					+ "', "
					+ " DSC_LINHA_LOTE_RETORNO = '"
					+ linhaRetorno
					+ "'"
					+ " WHERE COD_LINHA_LOTE = "
					+ LinhaLoteDolBeanId.getCodLinhaLote();
				//monitor
				
				stmt.execute(sCmd);
				//Gravar Log de Processamento da Linha
				sCmd =
					"INSERT INTO TSMI_PROC_LINHA_LOTE_LOG (COD_PROC_LINHA_LOTE_LOG, "
					+ "DAT_PROC, COD_RETORNO, DSC_LINHA_LOTE_RETORNO, COD_LINHA_LOTE) VALUES(SEQ_TSMI_PROC_LINHA_LOTE_LOG.NEXTVAL, "
					+ "SYSDATE, '"
					+ codRetorno
					+ "', "
					+ "'"
					+ linhaRetorno
					+ "', "
					+ LinhaLoteDolBeanId.getCodLinhaLote()
					+ ")";
				
				//monitor.gravaMonitor("Inclus�o log linha: " + sCmd);
				stmt.execute(sCmd);
				//Gravar Qtd de Linhas com erro no Lote
				
				//Testar se erro>=1
				if (!loteComErro) {
					sCmd =
						"UPDATE TSMI_LOTE_DOL SET QTD_LINHA_ERRO = QTD_LINHA_ERRO - 1"
						+ " WHERE COD_LOTE = "
						+ LinhaLoteDolBeanId.getCodLote()
						+ " AND QTD_LINHA_ERRO>=1";
					stmt.execute(sCmd);
					
				}
				//Testar se erro>=1
				if (!loteComErro) {
					sCmd =
						"UPDATE TSMI_ARQUIVO_DOL SET QTD_LOTE_ERRO = QTD_LOTE_ERRO - 1"
						+ " WHERE COD_ARQUIVO_DOL = "
						+ ArquivoDolBeanId.getCodArquivo()
						+ " AND QTD_LOTE_ERRO>=1";
					stmt.execute(sCmd);
				}
			}
			stmt.close();
			conn.commit();			
			
		} catch (SQLException e) {
			
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();					
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca Lote
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public void BuscaLoteDolBean(LoteDolBean myLote)		
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT NUM_LOTE,COD_LOTE,IND_EMISSAO_NOTIF_DETRAN,NUM_ANO_EXERCICIO,QTD_REG,QTD_LINHA_ERRO,COD_ARQUIVO_DOL,COD_UNIDADE," +
			" SUBSTR(DAT_LOTE,1,2) || SUBSTR(DAT_LOTE,3,2) || SUBSTR(DAT_LOTE,5,4) as DAT_LOTE" +
			" FROM TSMI_LOTE_DOL" +  
			" WHERE COD_LOTE=" + myLote.getCodLote();
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				myLote.setNumLote(rs.getString("NUM_LOTE"));
				myLote.setIndEmissaoNotifDetran(rs.getString("IND_EMISSAO_NOTIF_DETRAN"));
				myLote.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));
				myLote.setDatLote(rs.getString("DAT_LOTE"));
				myLote.setCodUnidade(rs.getString("COD_UNIDADE"));
				myLote.setCodArquivo(rs.getString("COD_ARQUIVO_DOL"));			
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Busca ArquivoDol
	 * @author Elisson Dias
	 *------------------------------------------------------------------------
	 */	
	public void BuscaArquivoDolBean(ArquivoDolBean myArq)		
	throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT NOM_ARQUIVO,COD_ARQUIVO_DOL,COD_STATUS,DAT_RECEBIMENTO,COD_IDENT_ARQUIVO,NUM_CONTROLE_ARQUIVO,NUM_ANO_EXERCICIO," +
			" QTD_LOTE,NOM_USERNAME,SIG_ORGAO_LOTACAO,NUM_PROTOCOLO,NOM_RESPONSAVEL,COD_ORGAO_LOTACAO,COD_ORGAO,QTD_LOTE_ERRO,IND_P59" +
			" FROM TSMI_ARQUIVO_DOL" + 
			" WHERE COD_ARQUIVO_DOL=" + myArq.getCodArquivo();
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				myArq.setNomUsername(rs.getString("NOM_USERNAME"));
				myArq.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myArq.setCodOrgao(rs.getString("COD_ORGAO"));
				myArq.setNomArquivo(rs.getString("NOM_ARQUIVO"));							
				myArq.setDscPortaria59(rs.getString("IND_P59"));							
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/*--------------------------------------------
	 * DAO relativo ao ConsultaBrokerTransXXXBean
	 * (Utilizado por todos os bean de transacao)
	 * @author Luciana Rocha
	 ----------------------------------------------*/	
	public String verificaTabEvento(String codErro) 
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		String dscErro = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = "SELECT dsc_erro FROM TSMI_ERRO_BROKER WHERE COD_ERRO = ' " + codErro + "'";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				dscErro = rs.getString("dsc_erro");
			}
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return dscErro;
	}
	
	public String verificaDscTransacao(String codTransacao) 
	throws DaoException {
		Connection conn = null;
		String sCmd = "";
		String dscTransacao = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd = "SELECT dsc_evento FROM TSMI_EVENTO WHERE COD_EVENTO = ' " + codTransacao + "'";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				dscTransacao = rs.getString("dsc_evento");
			}
			rs.close();
			stmt.close();
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return dscTransacao;
	}
	
	/**---------------------------------------
	 * DAO para consultar Marca/Modelo via BROKER
	 * @author Elisson Dias
	 * Alterada em 22/09/2004
	 ----------------------------------------*/	
	public String ConsultaMarcaBroker (String numPlaca)
	throws sys.DaoException {		
		
		String resultado = "";
		boolean transOK = true;
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		
		//Cr�tica da Placa		
		if ((numPlaca.equals(null)) ||(numPlaca.trim().equals(null))){
			transOK=false;
			resultado = "ERR-Dao.ConsultaMarcaBroker: Placa do Ve�culo n�o preenchida";
		}
		if (numPlaca.length() != 7){
			transOK=false;
			resultado = "ERR-Dao.ConsultaMarcaBroker: Placa do Ve�culo inv�lida";
		}
		
		if (transOK) {
			String parteVar = numPlaca + "4";
			transBRK.setParteFixa("127");
			transBRK.setParteVariavel(parteVar);
			try {
				resultado = transBRK.getResultado();
			} catch (Exception e) {
				throw new sys.DaoException (e.getMessage());
			}
		}       
		return resultado;
	}
	
	/**
	 * M�todo para verificar se a notifica��o j� foi enviada para a VEX.
	 * � feita uma consulta na tabela TSMI_CONTROLE_NOTIFICACOES para verificar se
	 * j� existe uma refer�ncia da notifica��o na mesma. De acordo com o status da notifica��o
	 * o c�digo status da corre��o recebe um valor diferente, com o respectivo erro verificado.
	 * @param linhaRecebida
	 * @author Sergio Roberto Junior
	 * @since 15/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	
	
	
	
	
	public void verificaNotificacoes(ArquivoRecebidoBean arqRecebido, Connection conn) throws Exception{
		Statement stmt = null;
		ResultSet rs = null;
		try{
			stmt = conn.createStatement();
			String sCmd = "";
			LinhaArquivoRec linhaRecebida = null;
			HashMap<String, LinhaArquivoRec> hash = new HashMap<String, LinhaArquivoRec>();
			List<LinhaArquivoRec> auxLista = new ArrayList<LinhaArquivoRec>();
			
			/*
			 * Bloco da c�digo criado para verificar replica��es de notifica��es em 
			 * um mesmo arquivo. N�o � realizado um acesso a base de dados para tal 
			 * verifica��o devido ao seu alto custo de processamento, j� que � necess�rio
			 * utilizar uma verifica��o pela descri��o da linha.
			 */
			for(int i=0;i<arqRecebido.getLinhaArquivoRec().size();i++){
				linhaRecebida = (LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i);
				if(("0,2".indexOf(linhaRecebida.getDscLinhaArqRec().substring(571,572))<0)&&
						!linhaRecebida.getNumNotificacao().trim().equals("")){
					if(!hash.containsKey(linhaRecebida.getNumNotificacao()))
						hash.put(linhaRecebida.getNumNotificacao(),linhaRecebida);
					else{
						linhaRecebida.setCodStatusCorrecao("1");
						linhaRecebida.setCodRetorno("N05");
						continue;
					}
				}
				auxLista.add(linhaRecebida);
			}
			
			//Inicia loop para valida��o de todas as linhas do arquivo.
			for(int i=0;i<auxLista.size();i++){
				linhaRecebida = (LinhaArquivoRec)auxLista.get(i);
				// Caso a linha possua a notifica��o do tipo 0 ou 2 ela n�o sofre verifica��o.
				if( ("0,2".indexOf(linhaRecebida.getDscLinhaArqRec().substring(571,572)))>=0 ){
					linhaRecebida.setCodStatusCorrecao("-1");
					continue;
				}
				
				/* Verifica se o n�mero de notifica��o � v�lido (diferente de branco) */
				if(linhaRecebida.getNumNotificacao().trim().equals("")) {
					linhaRecebida.setCodStatusCorrecao("1");
					linhaRecebida.setCodRetorno("N06");
					continue;
				}
				
				// Verifica se a linha possui notifica��o do tipo 1, 3, 9.
				if( "1,3,9".indexOf(linhaRecebida.getDscLinhaArqRec().substring(571,572))<0 ){
					linhaRecebida.setCodStatusCorrecao("1");
					linhaRecebida.setCodRetorno("N04");
					continue;
				}
				
				// Verifica se a linha � marcada como reemiss�o.
				if( linhaRecebida.getDscLinhaArqRec().substring(567,568).equals("R") )
					linhaRecebida.setIndReemissao(true);												
				
				/*
				 * Consulta a exist�ncia da notifica��o em arquivos j� retornados pela VEX.
				 * O motivo da consulta � verificar os casos em que foram realizados envios 
				 * anteriores a cria��o do controle sobre as notifica��es.
				 */
				if (!linhaRecebida.isIndReemissao()) {         
					sCmd = "SELECT COD_STATUS FROM TSMI_LINHA_ARQUIVO_REC L" +
					" WHERE COD_ARQUIVO IN (SELECT A.COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO A" +
					"                       WHERE L.COD_ARQUIVO = A.COD_ARQUIVO"+
                    "                       AND COD_IDENT_ARQUIVO in('EMITEVEX' ,'EMITEPER' ) "+				
					"						AND COD_STATUS <> 1)"+
					" AND NUM_NOTIFICACAO = '"+linhaRecebida.getDscLinhaArqRec().substring(6,15)+"'"+
					" AND COD_LINHA_ARQ_REC <> "+linhaRecebida.getCodLinhaArqReq()+
					" AND COD_ARQUIVO <> "+linhaRecebida.getCodArquivo()+
					" AND COD_STATUS <> 7"+
					" AND COD_STATUS <> 5";
					rs = stmt.executeQuery(sCmd);
					if(rs.next()){
						linhaRecebida.setCodStatusCorrecao("1");
						if( (rs.getString("COD_STATUS").equals("1")) || (rs.getString("COD_STATUS").equals("8")) )
							linhaRecebida.setCodRetorno("N01");
						else
							linhaRecebida.setCodRetorno("N05");
						continue;
					}
				}
				
				/* Consulta se o org�o atuador da notifica��o existe na base do SMIT.
				 * Caso n�o exista � identificado a pend�ncia.
				 */ 
				sCmd = "SELECT * FROM TSMI_ORGAO WHERE COD_ORGAO = "+linhaRecebida.getDscLinhaArqRec().substring(579,585);
				rs = stmt.executeQuery(sCmd);
				if(!rs.next()){
					linhaRecebida.setCodStatusCorrecao("1");
					linhaRecebida.setCodRetorno("N03");
					continue;
				}
				
				/* Verifica se para a notifica��o existe uma ou mais fotos presentes na
				 * base do SMIT.
				 * Caso n�o exista � identificada a pend�ncia.
				 */
				
				if(!linhaRecebida.getDscLinhaArqRec().substring(649,679).equals(sys.Util.rPad(""," ",30))) {
					ACSS.ParamSistemaBean paramRec = new ACSS.ParamSistemaBean();
					paramRec.setCodSistema("45"); //M�DULO RECURSO    
					paramRec.PreparaParam(conn);
					REC.FotoDigitalizadaBean foto = REC.DaoDigit.getInstance().ConsultaFotoDigitalizada(new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'"+linhaRecebida.getDscLinhaArqRec().substring(360, 372).trim()+"'"});
					if(foto == null){
						((LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i)).setCodStatusCorrecao("1");
						((LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i)).setCodRetorno("N02");
						continue;
					}
					else /*Verificar se existe fisicamente*/
					{
						File arquivo = new File(foto.getArquivo(paramRec));
						if (!arquivo.exists()) {
							((LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i)).setCodStatusCorrecao("1");
							((LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i)).setCodRetorno("N02");
							continue;
						}
					}
				}
				linhaRecebida.setCodStatusCorrecao("0");
			}
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo usado para inserir uma linha de um arquivo recebido na tabela de controle
	 * de notifica��es enviadas.
	 * As notifica��es que tiverem seu status de controle como 1 ser�o retiradas da lista
	 * de linhas para cria��o do arquivo, ficando pend�ntes para futuros ajustes.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 15/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	public void insereNotificacoes(ArquivoRecebidoBean arqRecebido, Connection conn,ArrayList ListDestinatarios, ArrayList ListDestinatariosVEX, ArrayList Pendencia,ArrayList Pendencia_Penalidade)throws Exception{
		try{
			Statement stmt = conn.createStatement();
			LinhaArquivoRec linhaRecebida = null;
			String sCmd = null;
			String codStatusAuto = "";
			int erro = 0;
			
			
			//Inicia loop para valida��o de todas as linhas do arquivo.
			for(int i=0;i<arqRecebido.getLinhaArquivoRec().size();i++){
				linhaRecebida = (LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i);
				
				/* Caso a linha possua seu codStatus como pendente ela � retirada 
				 * do arquivo de envio para VEX.
				 * Isso � feito para que as pendencias n�o sejam enviadas para a VEX antes 
				 * de serem corrigidas.
				 * Al�m disso tamb�m � atualizado o status da linha na tabela TSMI_LINHA_ARQUIVO_REC.
				 */
				if(linhaRecebida.getCodStatusCorrecao().equals("1")){
					sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC SET COD_STATUS ='9', COD_RETORNO = '"+linhaRecebida.getCodRetorno()+"'"+
					" WHERE COD_LINHA_ARQ_REC = "+linhaRecebida.getCodLinhaArqReq();
					stmt.executeUpdate(sCmd);
					
					String msg = "";
					if (ListDestinatarios.size()>0)
					{
						Iterator it = ListDestinatarios.iterator();
						GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
						while (it.hasNext())
						{
							nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
							if (nivelProcesso.getValNivelProcesso().equals("S"))
							{
								Iterator itx = nivelProcesso.getListNivelDestino().iterator();
								GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
								ArrayList lDestinatarios = new ArrayList();
								while (itx.hasNext())
								{
									destinatario=(GER.CadastraDestinatarioBean)itx.next();
									if(destinatario.getCodOrgao().equals(linhaRecebida.getCodOrgao()))
									{
										//Mensagem de Pendencias
										if(nivelProcesso.getTipoMsg().equals("M"))
										{
											msg = "Cod.Orgao.......: "+ arqRecebido.getCodOrgao()+" Auto.........: "+ linhaRecebida.getNumAutoInfracao()+" \n" +
											"Placa...........: "+ linhaRecebida.getDscLinhaArqRec().substring(125,132)+" Data Infracao: "+ 
											Util.formataDataDDMMYYYY(linhaRecebida.getDscLinhaArqRec().substring(212,220))+"\n"+
											"Hora Infracao...: "+ linhaRecebida.getDscLinhaArqRec().substring(220,225)+" Municipio: "+ linhaRecebida.getDscLinhaArqRec().substring(225,275)+"\n"+
											"Motivo Pendencia: "+ linhaRecebida.getNomStatusErro("REG0510")+"\n"+
											"-------------------------------------------------------------------------- \n";
										}  
										destinatario.getMsg().add(msg);
										lDestinatarios.add(destinatario);
									}
								}
								nivelProcesso.setListNivelDestino(lDestinatarios);
								Pendencia.add(nivelProcesso);
							}
						}
					}
					

					boolean bPenalidade=false;
					sCmd = "SELECT * FROM TSMI_CONTROLE_VEX_MASTER" +
					" WHERE NUM_NOTIFICACAO = '"+linhaRecebida.getNumNotificacao()+"' AND TIP_NOTIFICACAO='3'";
					ResultSet rs = stmt.executeQuery(sCmd);
					if (rs.next()) bPenalidade=true;

					
					if ( (ListDestinatariosVEX.size()>0) && (bPenalidade) )
					{
						Iterator it = ListDestinatariosVEX.iterator();
						GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
						while (it.hasNext())
						{
							nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
							if (nivelProcesso.getValNivelProcesso().equals("S"))
							{
								Iterator itx = nivelProcesso.getListNivelDestino().iterator();
								GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
								ArrayList lDestinatarios = new ArrayList();
								while (itx.hasNext())
								{
									destinatario=(GER.CadastraDestinatarioBean)itx.next();
									if(destinatario.getCodOrgao().equals(linhaRecebida.getCodOrgao()))
									{
										//Mensagem de Pendencias
										if(nivelProcesso.getTipoMsg().equals("M"))
										{
											msg = "Cod.Orgao.......: "+ arqRecebido.getCodOrgao()+" Auto.........: "+ linhaRecebida.getNumAutoInfracao()+" \n" +
											"Placa...........: "+ linhaRecebida.getDscLinhaArqRec().substring(125,132)+" Data Infracao: "+ 
											Util.formataDataDDMMYYYY(linhaRecebida.getDscLinhaArqRec().substring(212,220))+"\n"+
											"Hora Infracao...: "+ linhaRecebida.getDscLinhaArqRec().substring(220,225)+" Municipio: "+ linhaRecebida.getDscLinhaArqRec().substring(225,275)+"\n"+
											"Motivo Pendencia: "+ linhaRecebida.getNomStatusErro("REG0510")+"\n"+
											"-------------------------------------------------------------------------- \n";
										}  
										destinatario.getMsg().add(msg);
										lDestinatarios.add(destinatario);
									}
								}
								nivelProcesso.setListNivelDestino(lDestinatarios);
								Pendencia_Penalidade.add(nivelProcesso);
							}
						}
					}

					
					/*Carregar Linhas Pendentes para enviar email*/
					arqRecebido.getLinhaArquivoRec().remove(i);
					i--;
					erro++;
				}
				/*
				 * Caso a linha n�o possua nenhuma pend�ncia a linha tem seu status 
				 * alterado para 1 na base de dados do SMIT.
				 * Se a linha possui status para n�o ser controlada sobre a nova funcionalidade
				 * de controle de notifica��es, a mesma n�o � inserida na tabela 
				 * TSMI_CONTROLE_NOTIFICACOES, somente tendo seu status em 
				 * TSMI_LINHA_ARQUIVO_REC alterado para 5.
				 */              
				else if( linhaRecebida.getCodStatusCorrecao().equals("0") ) {
					sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC SET COD_STATUS = 1 , COD_RETORNO = NULL"+
					" WHERE COD_LINHA_ARQ_REC = "+linhaRecebida.getCodLinhaArqReq();
					stmt.executeUpdate(sCmd);
				}				
				else if( linhaRecebida.getCodStatusCorrecao().equals("-1") ){
					sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC SET COD_STATUS = 5 , COD_RETORNO = NULL"+ 
					" WHERE COD_LINHA_ARQ_REC = "+linhaRecebida.getCodLinhaArqReq();
					stmt.executeUpdate(sCmd);
					continue;
				}
				
				/*
				 * Se a linha for marcada como reemiss�o, � verifica se existe alguma linha
				 * com este notifica��o ainda n�o retornada. Caso encontre esta linha � cancelada
				 * para que a nova notifica��o possa ser cadastrada.
				 */
				if (linhaRecebida.isIndReemissao()) {
					sCmd = "SELECT COD_LINHA_ARQ_REC_ENVIO, COD_LINHA_ARQ_REC_RETORNO FROM TSMI_CONTROLE_NOTIFICACOES" +
					" WHERE NUM_NOTIFICACAO = '"+linhaRecebida.getNumNotificacao()+"'"+
					" AND COD_STATUS_NOTIF NOT IN (8,9)";
					ResultSet rs = stmt.executeQuery(sCmd);
					String codLinhaEnvio = "";
					String codLinhaRetorno = "";
					while (rs.next()) {
						codLinhaEnvio = rs.getString("COD_LINHA_ARQ_REC_ENVIO");
						codLinhaRetorno = rs.getString("COD_LINHA_ARQ_REC_RETORNO");
						
						if(!codLinhaEnvio.equals("")) {
							String[] linhas = new String[] {codLinhaEnvio};
							cancelarLinha(conn,linhas,"ENVIO",false);
						} else if(!codLinhaRetorno.equals("")) {
							String[] linhas = new String[] {codLinhaRetorno};
							cancelarLinha(conn,linhas,"RETORNO",false);
						}
					}
					rs.close();
				}
				
				/*
				 * Busca o c�digo do status do auto de infra��o atrav�s do campo tipo de 
				 * notifica��o presente na linha do arquivo.
				 * Caso ocorra algum tipo de erro no valor do tipo de notifica��o o codStatusAuto
				 * vai ficar com valor -1 para identifica��o do erro.
				 */ 
				if(linhaRecebida.getDscLinhaArqRec().substring(571,572).equals("1")) //Notifica��o de Autua��o
					codStatusAuto = "1";
				else if(linhaRecebida.getDscLinhaArqRec().substring(571,572).equals("3")) //Notifica��o de Penalidade
					codStatusAuto = "11";
				else if(linhaRecebida.getDscLinhaArqRec().substring(571,572).equals("9")) //Auto Antigo
					codStatusAuto = "11";
				
				String numAutoInfracao = linhaRecebida.getDscLinhaArqRec().substring(360,372).trim();
				String numNotificacao = linhaRecebida.getDscLinhaArqRec().substring(6,15).trim();
				String numPlaca = linhaRecebida.getDscLinhaArqRec().substring(125,132);
				String datEmissao = linhaRecebida.getDscLinhaArqRec().substring(17,25);
				String codOrgaoAutuacao = linhaRecebida.getDscLinhaArqRec().substring(579,585);
				
				sCmd = "INSERT INTO TSMI_CONTROLE_NOTIFICACOES"+
				" (COD_CONTROLE_NOTIF,NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,NUM_PLACA,COD_STATUS,DAT_ENVIO,"+
				" COD_ARQUIVO_ENVIO,COD_LINHA_ARQ_REC_ENVIO,NOM_USERNAME_ENVIO,COD_ORGAO_ENVIO,COD_STATUS_NOTIF,DAT_EMISSAO,COD_ORGAO_AUTUACAO)"+
				" VALUES(SEQ_TSMI_CONTROLE_NOTIFICACOES.NEXTVAL,'"+numAutoInfracao+"','"+numNotificacao+"','"+numPlaca+"','"+
				codStatusAuto+"',TRUNC(SYSDATE),'"+arqRecebido.getCodArquivo()+"','"+linhaRecebida.getCodLinhaArqReq()+"','"+
				arqRecebido.getNomUsername()+"','"+arqRecebido.getCodOrgao()+"','"+linhaRecebida.getCodStatusCorrecao()+"',"+
				"TO_DATE('"+datEmissao+"','YYYYMMDD'),"+codOrgaoAutuacao+")";
				stmt.execute(sCmd);
			}
			stmt.close();
			
			/*
			 * Armazena a quantidade de linhas com erro no arquivo recebido, para
			 * posterior update na tabela TSMI_ARQUIVO_RECEBIDO ao final da execu��o
			 * de cria��o do arquivo.
			 */ 
			arqRecebido.setQtdLinhaPendente(String.valueOf(erro));
			
		} catch(Exception e){
			conn.rollback();
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo usado para recuperar todos os registros de arquivos pendentes que possuam
	 * o c�digo do org�o informado no objeto arquivo recebido passado por par�metro.
	 * @param arquivoRec
	 * @return Lista de arquivos pendentes
	 * @author Sergio Roberto Junior
	 * @since 20/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	public List getArquivoPendenteEnvio(ArquivoRecebidoBean arquivoRec, String[] orgaos) throws Exception{
		Connection conn = null;
		List arquivosRec = new ArrayList();		
		try{
			String orgaosWhere = "(";
			for (int i = 0; i < orgaos.length; i++) {
				if (i == orgaos.length - 1)
					orgaosWhere += orgaos[i] + ")";
				else
					orgaosWhere += orgaos[i] + ",";
			}
			
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();			
			String sCmd = "select /*+rule*/ cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel,"
				+ " (select count(*) from tsmi_linha_arquivo_rec l where l.cod_arquivo = a.cod_arquivo"
				+ " and l.cod_status = 9 and l.cod_orgao in "+orgaosWhere+") as qtd_linha_pendente,"
				+ " qtd_linha_erro, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo"
				+ " from tsmi_arquivo_recebido a"
				+ " where cod_ident_arquivo ='"+arquivoRec.getCodIdentArquivo()+"' and cod_arquivo in"
				+ " (select l.cod_arquivo from tsmi_linha_arquivo_rec l where l.cod_arquivo ="
				+ " a.cod_arquivo and l.cod_status = 9 and l.cod_orgao in "+orgaosWhere+")";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setDatDownload(rs.getString("dat_download"));
				arqRec.setCodStatus(rs.getString("cod_status"));
				arqRec.setQtdReg(rs.getString("qtd_reg"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRec.setNumProtocolo(rs.getString("num_protocolo"));
				arquivosRec.add(arqRec);  			
			}			
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arquivosRec;
	}
	
	public List getArquivoPendenteRetorno(ArquivoRecebidoBean arquivoRec) throws Exception{
		Connection conn = null;
		List arquivosRec = new ArrayList();		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
				+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo"
				+ " FROM TSMI_ARQUIVO_RECEBIDO" 
				+ " WHERE cod_orgao = "+arquivoRec.getCodOrgao()
				+ " AND qtd_linha_pendente > 0"
				+ " AND cod_ident_arquivo ='"+arquivoRec.getCodIdentArquivo()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setDatDownload(rs.getString("dat_download"));
				arqRec.setCodStatus(rs.getString("cod_status"));
				arqRec.setQtdReg(rs.getString("qtd_reg"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRec.setNumProtocolo(rs.getString("num_protocolo"));
				arquivosRec.add(arqRec);  			
			}			
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return arquivosRec;
	}	
	
	/**
	 * M�todo retorna todas as linhas presente em um arquivo com um status definido.
	 * O status a ser utilizado � definido dentro do objeto arquivo, recebido por par�metro.
	 * @param bean
	 * @param conn
	 * @author Sergio Roberto Junior
	 * @since 20/12/2004
	 * @throws DaoException
	 * @version 1.0
	 */
	public void getArquivoLinhaEnvio(ArquivoRecebidoBean bean, String[] orgaos) throws DaoException {
		Connection conn = null;
		try {
			String orgaosWhere = "(";
			for (int i = 0; i < orgaos.length; i++) {
				if (i == orgaos.length - 1)
					orgaosWhere += orgaos[i] + ")";
				else
					orgaosWhere += orgaos[i] + ",";
			}
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_arquivo, cod_linha_arq_rec, dsc_linha_arq_rec, num_seq_linha,"
				+ " cod_status, cod_retorno, cod_retorno_batch, dsc_linha_arq_retorno, num_auto_infracao,"
				+ " num_notificacao, cod_orgao"
				+ " FROM TSMI_LINHA_ARQUIVO_REC"
				+ " WHERE cod_arquivo = " + bean.getCodArquivo() 
				+ " AND cod_status = '"+bean.getCodStatus() + "' AND cod_orgao in " + orgaosWhere
				+ " ORDER BY num_seq_linha";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetLinhas = new Vector();
			while (rs.next()) {           
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
				linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				linhaRec.setCodOrgao(rs.getString("cod_orgao"));
				vetLinhas.add(linhaRec);                    
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}   
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	public void getArquivoLinhaRetorno(ArquivoRecebidoBean bean) throws DaoException {
		Connection conn = null;
		try {       
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_arquivo, cod_linha_arq_rec, dsc_linha_arq_rec, num_seq_linha,"
				+ " cod_status, cod_retorno, cod_retorno_batch, dsc_linha_arq_retorno, num_auto_infracao,"
				+ " num_notificacao, cod_orgao"
				+ " FROM TSMI_LINHA_ARQUIVO_REC"
				+ " WHERE cod_arquivo = " + bean.getCodArquivo() 
				+ " AND cod_status = '"+bean.getCodStatus() + "'"
				+ " ORDER BY num_seq_linha";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetLinhas = new Vector();
			while (rs.next()) {           
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
				linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				linhaRec.setCodOrgao(rs.getString("cod_orgao"));
				vetLinhas.add(linhaRec);                    
			}
			bean.setLinhaArquivoRec(vetLinhas);
			rs.close();
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}   
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}	
	
	/**
	 * M�todo utilizado para armazenar o momento em que o arquivo EMITEVEX foi recebido
	 * pela VEX. Esse m�todo foi gerado devido a nova funcionalidade de controle 
	 * de notifica��es, e seus registros est�o presentes na tabela TSMI_CONTROLE_NOTIFICACOES.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 22/12/2004
	 * @throws DaoException
	 * @version 1.0
	 */
	public void recebeDownloadNotif(ArquivoRecebidoBean arqRecebido, ACSS.UsuarioBean usrLogado) throws DaoException{
		Connection conn = null;
		String sCmd     = null;		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			LinhaArquivoRec linhaRecebida = null;
			/*
			 * Inicia loop para valida��o E registro de todas as notifica��es presentes
			 * no arquivo de download.
			 */
			for(int i=0;i<arqRecebido.getLinhaArquivoRec().size();i++){
				linhaRecebida = (LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i);
				
				// Verifica se a linha sofre interven��o sobre a opera��o de controle de notifica��es.
				if( !(linhaRecebida.getCodStatus().equals("0")) && !(linhaRecebida.getCodStatus().equals("1")))
					continue;
				
				sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES"
					+ " SET COD_STATUS_NOTIF = '2', DAT_RECEB = TRUNC(SYSDATE),"
					+ " NOM_USERNAME_RECEB = '"+usrLogado.getNomUserName()+"',"
					+ " COD_ORGAO_RECEB = '"+usrLogado.getCodOrgaoAtuacao()+"' "
					+ " WHERE COD_LINHA_ARQ_REC_ENVIO = '"+linhaRecebida.getCodLinhaArqReq()+"' "
					+ " AND (COD_STATUS_NOTIF = 0 OR COD_STATUS_NOTIF = 1)";
				stmt.execute(sCmd);
			}
			stmt.close();
		}
		catch (SQLException e) {
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch(Exception e){
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo usado para buscar o registro de um determindo arquivo, presente na tabela
	 * TSMI_ARQUIVO_RECEBIDO, retornando todos os seus atributos mais todas as linhas
	 * presentes no arquivo em quest�o.
	 * A condi��o de busca do arquivo � atr�ves do seu c�digo, passado atr�ves
	 * do objeto arqRecebido.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 21/01/2005
	 * @throws DaoException
	 * @version 2.0
	 */
	public void buscaArquivoRecebido(ArquivoRecebidoBean arqRecebido) throws DaoException{
		Connection conn = null;						
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd ="SELECT * FROM (SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
				+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo"
				+ " FROM TSMI_ARQUIVO_RECEBIDO"
				+ " WHERE COD_ARQUIVO = '" + arqRecebido.getCodArquivo()+"')";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			// Condi��o realizada para receber o arquivo na clausula where da query.
			if(rs.next()){
				arqRecebido.setCodArquivo(rs.getString("cod_arquivo"));
				arqRecebido.setNomArquivo(rs.getString("nom_arquivo"));
				arqRecebido.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRecebido.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRecebido.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRecebido.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRecebido.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRecebido.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRecebido.setCodOrgao(rs.getString("cod_orgao"));
				arqRecebido.setDatDownload(rs.getString("dat_download"));
				arqRecebido.setCodStatus(rs.getString("cod_status"));
				arqRecebido.setQtdReg(rs.getString("qtd_reg"));
				arqRecebido.setNomUsername(rs.getString("nom_username"));
				arqRecebido.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRecebido.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRecebido.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRecebido.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRecebido.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRecebido.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRecebido.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRecebido.setNumProtocolo(rs.getString("num_protocolo"));
				
				// In�cio da consulta a todas as linhas presentes no arquivo de busca.
				sCmd = "SELECT cod_arquivo, cod_linha_arq_rec, dsc_linha_arq_rec, num_seq_linha,"
					+ " cod_status, cod_retorno, cod_retorno_batch, dsc_linha_arq_retorno, num_auto_infracao "
					+ " FROM TSMI_LINHA_ARQUIVO_REC"
					+ " WHERE cod_arquivo = '" + arqRecebido.getCodArquivo()+"' "
					+ " ORDER BY num_seq_linha";
				rs = stmt.executeQuery(sCmd);
				List vetLinhas = new ArrayList();
				LinhaArquivoRec linhaRec = null;
				// Loop para carregar todas as linhas presentes no arquivo de busca.
				while (rs.next()) {			  
					linhaRec = new LinhaArquivoRec();
					linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
					linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
					linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
					linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
					linhaRec.setCodStatus(rs.getString("cod_status"));
					linhaRec.setCodRetorno(rs.getString("cod_retorno"));
					linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
					linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
					linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
					vetLinhas.add(linhaRec);
				}
				// Insere todas as linhas da query realizada e insere no objeto arquivo recevido.
				arqRecebido.setLinhaArquivoRec(vetLinhas);
			}			
			rs.close();
			stmt.close();
		} 
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	
	/**
	 * M�todo usado para buscar o registro de um determindo arquivo, presente na tabela
	 * TSMI_ARQUIVO_RECEBIDO, retornando todos os seus atributos mais todas as linhas
	 * presentes no arquivo em quest�o.
	 * A condi��o de busca do arquivo � atr�ves do seu c�digo, passado atr�ves
	 * do objeto arqRecebido.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 21/01/2005
	 * @throws DaoException
	 * @version 2.0
	 * @throws SQLException 
	 */
	public void atualizaCodRetornoPostagemLinhaArquivo(LinhaArquivoRec linhaArqRecebido, String codigoRetorno) throws DaoException, SQLException{
		Connection conn = null;						
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			PreparedStatement pstm = null;
			
			String sCmd ="update tsmi_linha_arquivo_rec set cod_retorno_postagem = ? where cod_linha_arq_rec = ?";
			
			pstm = conn.prepareStatement(sCmd);
			pstm.setString(1, codigoRetorno);
			pstm.setString(2, linhaArqRecebido.getCodLinhaArqReq());
			pstm.executeUpdate();

			pstm.close();
			conn.commit();
			
		} 
		catch(Exception e){
			conn.rollback();
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo usado para buscar o registro de um determindo arquivo, presente na tabela
	 * TSMI_ARQUIVO_RECEBIDO, retornando todos os seus atributos mais todas as linhas
	 * presentes no arquivo em quest�o.
	 * A condi��o de busca do arquivo � atr�ves do seu c�digo, passado atr�ves
	 * do objeto arqRecebido.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 21/01/2005
	 * @throws DaoException
	 * @version 2.0
	 * @throws SQLException 
	 */
	public void atualizaIndicadorDataPostagemArquivo(ArquivoRecebidoBean arquivoRecebido) throws DaoException, SQLException{
		Connection conn = null;						
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			PreparedStatement pstm = null;
			
			String sCmd ="update tsmi_arquivo_recebido set ind_envio_data_postagem = ? where cod_arquivo = ? ";
			
			pstm = conn.prepareStatement(sCmd);
			pstm.setString(1, arquivoRecebido.getIndicadorEnvioDataPostagem());
			pstm.setString(2, arquivoRecebido.getCodArquivo());
			pstm.executeUpdate();

			pstm.close();
			conn.commit();
			
		} 
		catch(Exception e){
			conn.rollback();
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	
	/**
	 * M�todo para verifica��o se a notifica��o j� foi enviada para a VEX.
	 * M�todo vem a somente ser usado para gerar uma conex�o a ser enviada para o m�todo
	 * verificaNotificacoes(arqRecebido,conn).
	 * @param arqRecebido, conn
	 * @author Sergio Roberto Junior
	 * @since 28/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	public void verificaNotificacoes(ArquivoRecebidoBean arqRecebido) throws Exception{
		try{
			Connection conn = serviceloc.getConnection(MYABREVSIST);
			verificaNotificacoes(arqRecebido,conn);     
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo usado para retornar a linha respectiva ao c�digo da linha presente no objeto
	 * passado por par�metro.
	 * @param linhaRec
	 * @author Sergio Roberto Junior
	 * @since 28/12/2004
	 * @throws DaoException
	 * @version 1.0
	 */ 
	public void buscarLinha(LinhaArquivoRec linhaRec) throws DaoException {
		Connection conn = null;
		try {           
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_arquivo, cod_linha_arq_rec, dsc_linha_arq_rec, num_seq_linha, cod_status,"
				+ " cod_retorno, cod_retorno_batch, dsc_linha_arq_retorno, num_auto_infracao, num_notificacao,"
				+ " cod_orgao FROM TSMI_LINHA_ARQUIVO_REC"
				+ " WHERE cod_linha_arq_rec= " + linhaRec.getCodLinhaArqReq();
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()) {           
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno")); 
				linhaRec.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				linhaRec.setCodOrgao(rs.getString("cod_orgao"));
			}
			rs.close();
			stmt.close();               
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}       
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo atualiza as informa��es de envio de uma determinada linha de um arquivo
	 * recebido na tabela de controle TSMI_CONTROLE_NOTIFICACOES.
	 * @param linhaRec
	 * @param usrLogado
	 * @author Sergio Roberto Junior
	 * @since 28/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaEnvioNotif(LinhaArquivoRec linhaRec, ACSS.UsuarioBean usrLogado) throws DaoException{
		Connection conn = null;
		try {           
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES"
				+" SET COD_STATUS_NOTIF = "+linhaRec.getCodStatus()
				+" NOM_USERNAME_ENVIO = '"+usrLogado.getNomUserName()
				+" COD_ORGAO_ENVIO = "+usrLogado.getCodOrgaoAtuacao()
				+" DAT_ENVIO = TRUNC(SYSDATE)"
				+" WHERE COD_LINHA_ARQ_REC_ENVIO = "+linhaRec.getCodLinhaArqReq();
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}       
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo usado para cancelar todas as linhas pendentes de envio, presentes no arquivo
	 * Emitevex, que n�o seram mais ajustadas.
	 * @param linhas
	 * @author Sergio Roberto Junior
	 * @since 29/12/2004
	 * @throws Exception
	 * @version 1.0
	 */
	public void cancelarLinha(Connection conn, String[] codLinha, String j_sigFuncao, boolean atualizarArquivo)
	throws DaoException{
		try {			
			Statement stmt = conn.createStatement();
			
			for(int i=0;i<codLinha.length;i++){
				// Atualiza o status de todas as linhas na base de dados
				String sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC"
					+ " SET COD_STATUS = 7"
					+ " WHERE COD_LINHA_ARQ_REC = "+codLinha[i];
				stmt.executeUpdate(sCmd);
				
				/*
				 * Atualiza a tabela de controle de notifica��es com o status 10 de 
				 * acordo com o tipo de notifica��o que se esteja recebendo. Podendo 
				 * as notifica��es serem de envio ou retorno.
				 */ 
				sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES SET COD_STATUS_NOTIF = 10";
				if("ENVIO".equals(j_sigFuncao))	
					sCmd += " WHERE COD_LINHA_ARQ_REC_ENVIO = "+ codLinha[i];
				else if("RETORNO".equals(j_sigFuncao))
					sCmd += " WHERE COD_LINHA_ARQ_REC_RETORNO = "+ codLinha[i];
				stmt.executeUpdate(sCmd);
			}
			
			if (atualizarArquivo) {
				/*
				 * Recupera o n�mero de linhas que apresentam erro para um determinado arquivo.
				 * O c�digo da linha � usado j� que n�o se disp�e do c�digo do arquivo. Como
				 * o arquivo � o mesmo para qualquer linha a ser cancelada � escolhida sempre a
				 * �ltima da lista para se fazer refer�ncia ao arquivo.
				 */
				String sCmd = "SELECT A.COD_ARQUIVO,QTD_LINHA_PENDENTE,QTD_LINHA_CANCELADA FROM TSMI_ARQUIVO_RECEBIDO A JOIN TSMI_LINHA_ARQUIVO_REC L"
					+" ON A.COD_ARQUIVO = L.COD_ARQUIVO"
					+" WHERE COD_LINHA_ARQ_REC = "+codLinha[codLinha.length-1];
				ResultSet rs = stmt.executeQuery(sCmd);
				while (rs.next()) {
					String codArquivo = rs.getString("COD_ARQUIVO");
					int quantPend = rs.getInt("QTD_LINHA_PENDENTE");
					int quantCanc = rs.getInt("QTD_LINHA_CANCELADA");
					
					// Atualiza a quantidade de linhas com erros presentes dentro do arquivo.
					sCmd = "UPDATE TSMI_ARQUIVO_RECEBIDO SET QTD_LINHA_PENDENTE = " + (quantPend - codLinha.length)
					+ ", QTD_LINHA_CANCELADA = " + (quantCanc + codLinha.length) + " WHERE COD_ARQUIVO = " + codArquivo;
					stmt.executeUpdate(sCmd);
				}
			}
			stmt.close();
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}
	}
	public void cancelarLinha(String[] codLinha, String j_sigFuncao) throws DaoException{
		Connection conn = null;
		try {			
			conn = serviceloc.getConnection(MYABREVSIST);
			cancelarLinha(conn, codLinha, j_sigFuncao, true);
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}
	}
	
	/**
	 * M�todo usado para ajustar o dado de quantidade de linhas que apresentam erros para
	 * um determindado arquivo. 
	 * O registro com o c�digo do codArquivo passado por par�metro vai ter seus n�meros de 
	 * linhas com erros diminuidos ao n�mero rebido pela vari�vel quanrAcertos.
	 * @param codArquivo
	 * @param quantAcertos
	 * @author Sergio Roberto Junior
	 * @since 29/12/2004
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaErroLinha(String codArquivo, int quantAcertos, Connection conn) throws DaoException{
		try {
			/*
			 * Recupera o n�mero de linhas que apresentam erro para um determinado arquivo.
			 * O c�digo da linha � usado j� que n�o se disp�e do c�digo do arquivo. Como
			 * o arquivo � o mesmo para qualquer linha a ser cancelada � escolhida sempre a
			 * �ltima da lista para se fazer refer�ncia ao arquivo.
			 */
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT QTD_LINHA_PENDENTE FROM TSMI_ARQUIVO_RECEBIDO"
				+" WHERE COD_ARQUIVO = "+codArquivo;
			ResultSet rs = stmt.executeQuery(sCmd);
			rs.next();
			int quanterro = rs.getInt("QTD_LINHA_PENDENTE");
			rs.close();
			
			// Atualiza a quantidade de linhas com erros presentes dentro do arquivo.
			sCmd = "UPDATE TSMI_ARQUIVO_RECEBIDO SET QTD_LINHA_PENDENTE = " + (quanterro - quantAcertos)
			+ " WHERE COD_ARQUIVO = " + codArquivo;
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {	
			throw new DaoException(e.getMessage());
		}
	}
	
	public void atualizaErroLinha(String codArquivo, int quantAcertos) throws DaoException{
		Connection conn = null;
		try {			
			conn = serviceloc.getConnection(MYABREVSIST);
			atualizaErroLinha(codArquivo, quantAcertos, conn);				
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}	
	
	/**
	 * M�todo usado para retornar a linha e o arquivo para o c�digo de linha passado no 
	 * objeto passado por par�metro.
	 * @param linhaRec
	 * @author Sergio Roberto Junior
	 * @since 28/12/2004
	 * @throws DaoException
	 * @version 1.0
	 */	
	public void buscarArqLinha(ArquivoRecebidoBean arqRec) throws DaoException {
		Connection conn = null;
		LinhaArquivoRec linhaRec = (LinhaArquivoRec)arqRec.getLinhaArquivoRec().get(0);
		try {			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT A.cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
				+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, A.cod_status as statusLinha,"
				+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
				+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
				+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
				+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
				+ " cod_ident_movimento, num_protocolo, cod_linha_arq_rec, dsc_linha_arq_rec, num_seq_linha,"
				+ " L.cod_status statusArq, cod_retorno, cod_retorno_batch, dsc_linha_arq_retorno"
				+ " FROM TSMI_ARQUIVO_RECEBIDO A JOIN TSMI_LINHA_ARQUIVO_REC L"
				+ " ON A.COD_ARQUIVO = L.COD_ARQUIVO"
				+ " WHERE cod_linha_arq_rec= " + linhaRec.getCodLinhaArqReq();
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()) {	
				arqRec = new ArquivoRecebidoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
				arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
				arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
				arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
				arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
				arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
				arqRec.setCodOrgao(rs.getString("cod_orgao"));
				arqRec.setDatDownload(rs.getString("dat_download"));
				arqRec.setCodStatus(rs.getString("statusArq"));
				arqRec.setQtdReg(rs.getString("qtd_reg"));
				arqRec.setNomUsername(rs.getString("nom_username"));
				arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
				arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
				arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
				arqRec.setQtdLinhaPendente(rs.getString("qtd_linha_pendente"));
				arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
				arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
				arqRec.setNumProtocolo(rs.getString("num_protocolo"));
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("statusLinha"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
			}
			rs.close();
			stmt.close();				
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo criado para carregar todas as linhas que tiveram seus campos com 
	 * um determinado status.
	 * @param bean
	 * @param conn
	 * @author Sergio Roberto Junior
	 * @since 31/01/2005
	 * @throws DaoException
	 * @version 1.3
	 */
	public void CarregarLinhasAjustadas(ArquivoRecebidoBean bean, LinhaArquivoRec linhaRecebida, Connection conn,String dtInicioPendencia) throws DaoException {        
		try {               
			Statement stmt = conn.createStatement();
			
			ResultSet rs2 = null;
			Statement stmt2 = conn.createStatement();
			
			dtInicioPendencia="25/09/2012";

			/*
			String sCmd = "SELECT * FROM (" +
			"SELECT A.cod_arquivo, A.dat_recebimento, L.cod_linha_arq_rec, L.dsc_linha_arq_rec, L.num_seq_linha,";
			sCmd += " L.cod_status, L.cod_retorno, L.cod_retorno_batch, L.dsc_linha_arq_retorno,";
			sCmd += " L.num_auto_infracao, L.num_notificacao, L.cod_orgao";
			sCmd += " FROM TSMI_LINHA_ARQUIVO_REC L, TSMI_ARQUIVO_RECEBIDO A";
			sCmd += " WHERE A.cod_arquivo = L.cod_arquivo";
			
			if(!linhaRecebida.getCodRetorno().equals(""))
				sCmd += " AND L.cod_retorno = '"+linhaRecebida.getCodRetorno()+"'";

			sCmd += " AND L.cod_status = "+linhaRecebida.getCodStatus();
			sCmd += " AND A.cod_ident_arquivo = '"+bean.getCodIdentArquivo()+"' AND ROWNUM<=10000 AND DAT_RECEBIMENTO>='"+dtInicioPendencia+"'";
			sCmd += " ORDER BY A.cod_arquivo,L.num_seq_linha";
			sCmd += ") WHERE ROWNUM<=10000";
			*/
			String sCmd = "SELECT A.cod_arquivo, A.dat_recebimento, L.cod_linha_arq_rec, L.dsc_linha_arq_rec, L.num_seq_linha,";
			sCmd += " L.cod_status, L.cod_retorno, L.cod_retorno_batch, L.dsc_linha_arq_retorno,";
			sCmd += " L.num_auto_infracao, L.num_notificacao, L.cod_orgao";
			sCmd += " FROM TSMI_LINHA_ARQUIVO_REC L, TSMI_ARQUIVO_RECEBIDO A";
			sCmd += " WHERE A.cod_arquivo = L.cod_arquivo";
			
			if(!linhaRecebida.getCodRetorno().equals(""))
				sCmd += " AND L.cod_retorno = '"+linhaRecebida.getCodRetorno()+"'";

			sCmd += " AND L.cod_status = "+linhaRecebida.getCodStatus();
			sCmd += " AND A.cod_ident_arquivo = '"+bean.getCodIdentArquivo()+"' AND TRUNC(DAT_RECEBIMENTO)>=to_date('"+dtInicioPendencia+"','DD/MM/YYYY')";
			sCmd += " ORDER BY A.cod_arquivo,L.num_seq_linha";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector vetLinhas = new Vector();
			
			/*Verificar data inicio*/
			String numAutoInfracao="";
			while (rs.next()) {
				numAutoInfracao=rs.getString("num_auto_infracao");
				
				if (linhaRecebida.getCodRetorno().equals("N02"))
				{
					sCmd="SELECT COD_FOTO_DIGITALIZADA FROM TSMI_FOTO_DIGITALIZADA WHERE NUM_AUTO_INFRACAO='"+numAutoInfracao+"'";
					rs2 = stmt2.executeQuery(sCmd);
					if (!rs2.next()) continue;
				}
				
				
				
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodArquivo(rs.getString("cod_arquivo"));
				linhaRec.setDatRecebimento(rs.getString("dat_recebimento"));
				linhaRec.setCodLinhaArqRec(rs.getString("cod_linha_arq_rec"));
				linhaRec.setDscLinhaArqRec(rs.getString("dsc_linha_arq_rec"));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				linhaRec.setCodRetornoBatch(rs.getString("cod_retorno_batch"));
				linhaRec.setDscLinhaArqRetorno(rs.getString("dsc_linha_arq_retorno"));
				linhaRec.setNumAutoInfracao(numAutoInfracao);
				linhaRec.setNumNotificacao(rs.getString("num_notificacao"));
				linhaRec.setCodOrgao(rs.getString("cod_orgao"));
				vetLinhas.add(linhaRec);                    
			}
			bean.setLinhaArquivoRec(vetLinhas);
			if (rs2!=null) rs2.close();
			stmt2.close();
			rs.close();
			stmt.close();
			
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}           
	}
	
	public int PegarUltimoSeqGerado(String sData) throws DaoException {        
		Connection conn = null;
		int iUltimoSeqGerado=0;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT MAX(SEQ_GERACAO_CB_AI) as SEQ FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
			"DAT_GERACAO_CB_AI=to_date('"+sData.toString()+"','dd/mm/yyyy') ";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) 
				iUltimoSeqGerado=rs.getInt("SEQ")+1;
			rs.close();
			stmt.close();   
			return iUltimoSeqGerado;
		} catch (Exception e) {
			return 0;
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public void CarregarPassadoCBPNT(REC.AutoInfracaoBean myAutoList) throws DaoException {        
		Connection conn = null;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TPNT_NOTIFICACAO_MDB ORDER BY NUM_NOTIFICACAO,SEQ_NOTIFICACAO";
			ResultSet rs = stmt.executeQuery(sCmd);
			ArrayList <REC.AutoInfracaoBean>ListAuto = new ArrayList<REC.AutoInfracaoBean>();
			while (rs.next()) {           
				REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
				String sNotificacao=Util.lPad(rs.getString("NUM_NOTIFICACAO"),"0",9);
				String sSeq        =Util.lPad(rs.getString("SEQ_NOTIFICACAO"),"0",3);
				myAuto.setNumProcesso(sNotificacao+"-"+sSeq);
				myAuto.setNumNotificacao(sNotificacao+sSeq);
				ListAuto.add(myAuto);                    
			}
			myAutoList.setAutos(ListAuto);
			rs.close();
			stmt.close();  
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public void CarregarPassadoCB(REC.AutoInfracaoBean myAutoList) throws DaoException {        
		Connection conn = null;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_LOG_BROKER_CB ORDER BY DAT_PROC ASC";
			ResultSet rs = stmt.executeQuery(sCmd);
			ArrayList <REC.AutoInfracaoBean>ListAuto = new ArrayList<REC.AutoInfracaoBean>();
			while (rs.next()) {           
				REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
				myAuto.setCodOrgao(rs.getString("COD_ORGAO"));
				myAuto.setNumProcesso(rs.getString("PROCESSO"));
				ListAuto.add(myAuto);                    
			}
			myAutoList.setAutos(ListAuto);
			rs.close();
			stmt.close();  
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	
	public void CarregarLinhasGerarCB(NotifControleBean linhaNotif,String sDataInicio) throws DaoException {        
		Connection conn = null;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
			"DAT_ENVIO>=to_date('"+sDataInicio+"','dd/mm/yyyy') AND "+
			"DAT_GERACAO_CB_AI IS NULL AND ROWNUM<=800 ORDER BY DAT_ENVIO";
			ResultSet rs = stmt.executeQuery(sCmd);
			ArrayList <NotifControleBean>ListNotif = new ArrayList<NotifControleBean>();
			while (rs.next()) {           
				NotifControleBean linNotif = new NotifControleBean();
				linNotif.setCodControle(rs.getString("COD_CONTROLE_NOTIF"));
				linNotif.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				linNotif.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
				ListNotif.add(linNotif);                    
			}
			linhaNotif.setListaNotifs(ListNotif);
			rs.close();
			stmt.close();  
		} catch (Exception e) {         
			throw new DaoException(e.getMessage());
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	/**
	 * M�todo criado para associar um grupo de linhas com seus respectivos arquivos.
	 * Atrav�s desse m�todo se pode gerar um conjunto de arquivos a partir de um 
	 * conjunto de linhas j� existentes.
	 * Caso n�o seja encontrado nenhum arquivo para as linhas � retornado uma lista
	 * vazia.
	 * @param bean
	 * @param conn
	 * @return lista de arquivos.
	 * @author Sergio Roberto Junior
	 * @since 04/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public List AssociarLinhas(ArquivoRecebidoBean bean, Connection conn) throws DaoException {		
		List arquivos = new ArrayList();
		try {				
			Statement stmt = conn.createStatement();
			ResultSet rs = null;
			boolean flag = true;
			ArquivoRecebidoBean arqRec = null;
			List linhas = new ArrayList();
			String codArquivo = ((LinhaArquivoRec)bean.getLinhaArquivoRec().get(0)).getCodArquivo();
			for(int pos = 0; pos<bean.getLinhaArquivoRec().size(); pos++){
				if(flag){
					String sCmd ="SELECT cod_arquivo, nom_arquivo, cod_ident_arquivo, num_ano_exercicio,"
						+ " num_controle_arquivo, ind_emissao_notif_detran, cod_orgao, cod_status,"
						+ " qtd_reg, nom_username, cod_orgao_lotacao, sig_orgao_lotacao, nom_responsavel, qtd_linha_erro,"
						+ " qtd_linha_pendente, to_char(dat_recebimento,'dd/mm/yyyy') as dat_recebimento,"
						+ " to_char(dat_processamento_detran,'dd/mm/yyyy') as dat_processamento_detran,"
						+ " to_char(dat_download,'dd/mm/yyyy') as dat_download, ind_prioridade,"
						+ " cod_ident_movimento, num_protocolo"
						+ " FROM TSMI_ARQUIVO_RECEBIDO"
						+ " WHERE COD_ARQUIVO = "+codArquivo;
					rs = stmt.executeQuery(sCmd);
					if (rs.next()) {
						arqRec = new ArquivoRecebidoBean();
						arqRec.setCodArquivo(rs.getString("cod_arquivo"));
						arqRec.setNomArquivo(rs.getString("nom_arquivo"));
						arqRec.setDatRecebimento(rs.getString("dat_recebimento")); 
						arqRec.setDatProcessamentoDetran(rs.getString("dat_processamento_detran")); 
						arqRec.setCodIdentArquivo(rs.getString("cod_ident_arquivo"));
						arqRec.setNumAnoExercicio(rs.getString("num_ano_exercicio")); 
						arqRec.setNumControleArq(rs.getString("num_controle_arquivo")); 
						arqRec.setIndEmissaoNotifDetran(rs.getString("ind_emissao_notif_detran")); 
						arqRec.setCodOrgao(rs.getString("cod_orgao"));
						arqRec.setDatDownload(rs.getString("dat_download"));
						arqRec.setCodStatus(rs.getString("cod_status"));
						arqRec.setQtdReg(rs.getString("qtd_reg"));
						arqRec.setNomUsername(rs.getString("nom_username"));
						arqRec.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
						arqRec.setSigOrgaoLotacao(rs.getString("sig_orgao_lotacao"));
						arqRec.setResponsavelArq(rs.getString("nom_responsavel"));
						arqRec.setQtdLinhaErro(rs.getString("qtd_linha_erro"));
						arqRec.setQtdLinhaErro(rs.getString("qtd_linha_pendente"));
						arqRec.setIndPrioridade(rs.getString("ind_prioridade"));
						arqRec.setIdentMovimento(rs.getString("cod_ident_movimento"));
						arqRec.setNumProtocolo(rs.getString("num_protocolo"));		
					}	
					arquivos.add(arqRec);
					arqRec.setLinhaArquivoRec(linhas);
				}
				if( !codArquivo.equals(((LinhaArquivoRec)bean.getLinhaArquivoRec().get(pos)).getCodArquivo()) ){
					codArquivo = ((LinhaArquivoRec)bean.getLinhaArquivoRec().get(pos)).getCodArquivo();				
					linhas = new ArrayList();
					pos--;
					flag = true;
				}
				else{
					linhas.add(bean.getLinhaArquivoRec().get(pos));
					flag = false;	
				}
			}
			rs.close();
			stmt.close();
		} 
		catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}			
		return arquivos;
	}	
	
	/**
	 * M�todo para inser��o de um registro de um arquivo recebido na base do SMIT.
	 * @param auxClasse
	 * @author Sergio Roberto Junior
	 * @since 05/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void insertArquivoRec(ArquivoRecebidoBean auxClasse, Connection conn) throws DaoException{
		try {
			Statement stmt  = conn.createStatement();
			
			// buscar codigo do arquivo
			String sCmd  = "select seq_TSMI_arquivo_recebido.nextval cod_arquivo from dual";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				auxClasse.setPkid(auxClasse.getCodArquivo());
			}
			
			sCmd  = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (cod_arquivo,"+
			"nom_arquivo,dat_recebimento,dat_processamento_detran,cod_ident_arquivo, "+
			"num_ano_exercicio, num_controle_arquivo,num_protocolo, "+
			"ind_emissao_notif_detran, qtd_reg,cod_orgao,"+ 
			"cod_orgao_lotacao,sig_orgao_lotacao,nom_username,cod_status,"+
			"nom_responsavel,cod_ident_movimento,ind_prioridade,qtd_linha_erro,"+
			"qtd_linha_pendente,qtd_linha_cancelada,qtd_foto)"+
			"VALUES ("+auxClasse.getCodArquivo()+
			",'"+auxClasse.getNomArquivo()+"', "+
			"TO_DATE('"+auxClasse.getDatRecebimento()+"','dd/mm/yyyy'), " +
			"TRUNC(SYSDATE), " +
			"'"+auxClasse.getCodIdentArquivo()+"', " +
			"'"+auxClasse.getNumAnoExercicio()+"', " +
			"'"+auxClasse.getNumControleArq()+"', " +
			"'"+auxClasse.getNumProtocolo()+"', " +
			"'"+auxClasse.getIndEmissaoNotifDetran()+"', " +
			"'"+auxClasse.getQtdReg()+"', " +
			"'"+auxClasse.getCodOrgao()+"', " +
			"'"+auxClasse.getCodOrgaoLotacao()+"', " +
			"'"+auxClasse.getSigOrgaoLotacao()+"', " +
			"'"+auxClasse.getNomUsername()+"', " +
			"'"+auxClasse.getCodStatus()+"', " +
			"'"+auxClasse.getResponsavelArq()+"', " +
			"'"+auxClasse.getIdentMovimento()+"', 1,"+
			"'"+auxClasse.getQtdLinhaErro()+"'," +
			"'"+auxClasse.getQtdLinhaPendente()+"'," +
			"'"+auxClasse.getQtdLinhaCancelada()+"'," +
			+auxClasse.getQtdFoto()+")";
			stmt.execute(sCmd);		
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para alterar os dados para reenvio de uma linha.
	 * As novas informa��es s�o escritas no lugar dos dados de envio
	 * na tabela TSMI_CONTROLE_NOTIFICACOES, e as informa��es antigas s�o armazenadas
	 * em atributos do arquivo original da linha.
	 * @author Sergio Roberto Junior
	 * @since 05/01/2005
	 * @param auxClasse
	 * @throws DaoException
	 * @version 1.0
	 */
	public void linhaUpdateReenvio(LinhaArquivoRec auxClasse, ACSS.UsuarioBean UsrLogado) throws DaoException {      
		Connection conn = null;     
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String dat_envio=null, cod_arquivo_envio=null, nom_username_envio=null, cod_orgao_envio=null;
			String sCmd = "SELECT to_char(DAT_ENVIO,'dd/mm/yyyy') AS DATA_ENVIO, COD_ARQUIVO_ENVIO,"
				+ " NOM_USERNAME_ENVIO, COD_ORGAO_ENVIO"
				+ " FROM TSMI_CONTROLE_NOTIFICACOES"
				+ " WHERE COD_LINHA_ARQ_REC_ENVIO = "+auxClasse.getCodLinhaArqReq();
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()){
				dat_envio = rs.getString("DATA_ENVIO");
				cod_arquivo_envio = rs.getString("COD_ARQUIVO_ENVIO");
				nom_username_envio = rs.getString("NOM_USERNAME_ENVIO");
				cod_orgao_envio = rs.getString("COD_ORGAO_ENVIO");
			}
			
			sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES SET"
				+" NOM_USERNAME_ENVIO = '"+UsrLogado.getNomUserName()+"',"
				+" COD_ORGAO_ENVIO = "+UsrLogado.getCodOrgaoAtuacao()+","
				+" DAT_ENVIO = TRUNC(SYSDATE),"
				+" COD_STATUS_NOTIF = 0,"
				+" DAT_ENVIO_ORIG = '"+dat_envio+"',"
				+" COD_ARQUIVO_ENVIO_ORIG = "+cod_arquivo_envio+","
				+" NOM_USERNAME_ENVIO_ORIG = '"+nom_username_envio+"',"
				+" COD_ORGAO_ENVIO_ORIG = "+cod_orgao_envio
				+" WHERE COD_LINHA_ARQ_REC_ENVIO = "+auxClasse.getCodLinhaArqReq();
			stmt.executeUpdate(sCmd);
			stmt.close();
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (conn != null)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**
	 * M�todo de consulta da descri��o de um determinado erro de uma notifica��o.
	 * O m�todo deve receber um c�digo de erro v�lido.
	 * @param codErro
	 * @return descri��o do erro
	 * @author Sergio Roberto Junior
	 * @since 10/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public String getDescLinhaErro(String codErro) throws DaoException{
		Connection conn = null;     
		String retorno = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd = "SELECT DSC_ERRO FROM TSMI_ERRO_NOTIFICACAO"
				+ " WHERE COD_ERRO = '" + codErro + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()){
				retorno = rs.getString("DSC_ERRO");
			}
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (conn != null)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return retorno;
	}
	
	/**
	 * M�todo atualiza o status de uma determinada notifica��o e o c�digo do retorno, 
	 * passada atr�ves do objeto passado por par�metro.
	 * @param linhaRec
	 * @author Sergio Roberto Junior
	 * @since 11/05/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaStatusNotif(LinhaArquivoRec linhaRec) throws DaoException{
		Connection conn = null;
		boolean eConnLocal = false;        
		try {
			conn = linhaRec.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_LINHA_ARQUIVO_REC"
				+" SET COD_STATUS_NOTIF = "+linhaRec.getCodStatus()+","
				+" COD_RETORNO = '"+linhaRec.getCodRetorno()+"'"
				+" WHERE COD_LINHA_ARQ_REC_ENVIO = "+linhaRec.getCodLinhaArqReq();
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}		
	}
	
	/**
	 * M�todo para atualizar o c�digo do novo arquivo de envio de uma linha que possui
	 * seu status como pendente e foi ajustada.
	 * @param linhaRec
	 * @param codArquivoOrig
	 * @author Sergio Roberto Junior
	 * @since 11/05/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaArqEnvio(LinhaArquivoRec linhaRec, ArquivoRecebidoBean arqRecebido,String codArquivoOrig) throws DaoException{
		Connection conn = null;
		boolean eConnLocal = false;
		try {
			conn = linhaRec.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES"
				+" SET COD_ARQUIVO_ENVIO = "+linhaRec.getCodArquivo()+","
				+" COD_STATUS_NOTIF = "+linhaRec.getCodStatus()+","
				+" COD_ARQUIVO_ENVIO_ORIG = "+codArquivoOrig+","
				+" DAT_ENVIO_ORIG = to_date('"+arqRecebido.getDatRecebimento()+"','DD/MM/YYYY') "
				+" WHERE COD_LINHA_ARQ_REC_ENVIO = "+linhaRec.getCodLinhaArqReq();
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo de atualiza��o da quantidade de linhas presentes em um arquivo recebido.
	 * @param arqRecebido
	 * @author Sergio Roberto Junior
	 * @since 14/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaAQtdReg(ArquivoRecebidoBean arqRecebido, Connection conn) throws DaoException {
		try {
			Statement stmt  = conn.createStatement();
			String sCmd = "UPDATE TSMI_ARQUIVO_RECEBIDO SET"
				+" QTD_REG = '"+arqRecebido.getQtdReg()+"'"
				+" WHERE COD_ARQUIVO = "+arqRecebido.getCodArquivo();
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}    
	
	/**
	 * M�todo para consulta da quantidade de autos em um determinado status, 
	 * para determindado(s) �rg�es.
	 * Esse m�todo se utiliza do broker para realizar determinada consulta.
	 * @param codOrgao
	 * @param statusAuto
	 * @return C�digo do retorno da transa��o mais o n�mero de autos no determindado status.
	 * @author Sergio Roberto Junior
	 * @since 25/01/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public String consultaAutosStatus (String codOrgao, String statusAuto) throws DaoException {
		String resultado   = "";				
		try {
			String parteVar    = "";
			String indContinua = "            ";			
			parteVar = sys.Util.lPad(codOrgao,"0",6) + sys.Util.lPad(statusAuto,"0",3);
			resultado = chamaTransBRK("042",parteVar,indContinua);
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return resultado;
	}
	
	/**
	 * M�todo para realiza��o de transa��es diversas utilizando da funcionalidade do Broker.
	 * @param codTransacao
	 * @param opcEscolha
	 * @param indContinua
	 * @return String de retorno da opera��o realizada no Broker.
	 * @author Sergio Roberto Junior
	 * @version 25/01/2005
	 * @throws sys.DaoException
	 * @version 1.0
	 */
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua) throws sys.DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-Dao.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	/**
	 * M�todo de consulta aos registros presentes na tabela TSMI_CONTROLE_NOTIFICACAOES
	 * Esse m�todo � gen�rico para qualquer tipo de filtro sobre os campos presentes na tabela.
	 * Todos os atributos presentes no objeto notifControle, passado por par�metro, que 
	 * apresentarem seu conte�do diferente de vazio seram usados no filtro.
	 * At� o momento est�o implementados filtros por COD_STATUS_NOTIF, NUM_AUTO_INFRACAO,
	 * NUM_NOTIFICACAO, COD_STATUS E COD_LINHA_ARQ_REC_RETORNO.
	 * @param notifControle
	 * @return uma lista de notifica��es resultantes da consulta realizada.
	 * @author Sergio Roberto Junior
	 * @since 10/02/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public List getNotificacoes(NotifControleBean notifControle, Connection conn) throws DaoException{    
		List notificacoes = new ArrayList();
		boolean flag = true;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!notifControle.getCodStatusNotif().equals("")){
				sCmd += " WHERE COD_STATUS_NOTIF = "+notifControle.getCodStatusNotif();
				flag = false;
			}
			if(!notifControle.getNumAutoInfracao().equals("")){
				if(flag)
					sCmd += " WHERE NUM_AUTO_INFRACAO = '"+notifControle.getNumAutoInfracao()+"'";
				else
					sCmd += " AND NUM_AUTO_INFRACAO = '"+notifControle.getNumAutoInfracao()+"'";
				flag = false;	
			}
			if(!notifControle.getNumNotificacao().equals("")){
				if(flag)
					sCmd += " WHERE NUM_NOTIFICACAO = "+notifControle.getNumNotificacao();
				else
					sCmd += " AND NUM_NOTIFICACAO = "+notifControle.getNumNotificacao();
				flag = false;	
			}
			if(!notifControle.getCodStatusAuto().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				else
					sCmd += " AND COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				flag = false;	
			}
			if(!notifControle.getCodLinhaArquivoRetorno().equals("")){
				if(flag)
					sCmd += " WHERE COD_LINHA_ARQ_REC_RETORNO = "+notifControle.getCodLinhaArquivoRetorno();
				else
					sCmd += " AND COD_LINHA_ARQ_REC_RETORNO = "+notifControle.getCodLinhaArquivoRetorno();
				flag = false;	
			}
			ResultSet rs = stmt.executeQuery(sCmd);
			NotifControleBean notificacao = null;
			while(rs.next()){
				notificacao = new NotifControleBean();
				notificacao.setCodControle(rs.getString("COD_CONTROLE_NOTIF"));            	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));          	
				notificacao.setCodStatusAuto(rs.getString("COD_STATUS"));       	
				notificacao.setDatEnvio(rs.getDate("DAT_ENVIO"));            	
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO_ENVIO"));           	
				notificacao.setNomUsuarioEnvio(rs.getString("NOM_USERNAME_ENVIO"));            	
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO_ENVIO"));           	
				notificacao.setDatReceb(rs.getDate("DAT_RECEB"));         	
				notificacao.setNomUsuarioReceb(rs.getString("NOM_USERNAME_RECEB"));            	
				notificacao.setCodOrgaoReceb(rs.getString("COD_ORGAO_RETORNO"));            	
				notificacao.setDatRetorno(rs.getDate("DAT_RETORNO"));           	
				notificacao.setCodArquivoRetorno(rs.getString("COD_ARQUIVO_RETORNO"));           	
				notificacao.setNomUsuarioRetorno(rs.getString("NOM_USERNAME_RETORNO"));           	
				notificacao.setCodOrgaoRetorno(rs.getString("COD_ORGAO_RETORNO"));            	          	
				notificacao.setCodStatusNotif(rs.getString("COD_STATUS_NOTIF"));            	
				notificacao.setDatEnvioOrig(rs.getDate("DAT_ENVIO_ORIG"));           	
				notificacao.setCodArquivoEnvioOrig(rs.getString("COD_ARQUIVO_ENVIO_ORIG"));           	
				notificacao.setNomUsuarioEnvioOrig(rs.getString("NOM_USERNAME_ENVIO_ORIG"));           	
				notificacao.setCodOrgaoEnvioOrig(rs.getString("COD_ORGAO_ENVIO_ORIG"));           	
				notificacao.setDatRetornoOrig(rs.getDate("DAT_RETORNO_ORIG"));           	        	
				notificacao.setNomUsuarioRetornoOrig(rs.getString("NOM_USERNAME_RETORNO_ORIG"));            	
				notificacao.setCodOrgaoRetornoOrig(rs.getString("COD_ORGAO_RETORNO_ORIG"));	
				notificacao.setCodLinhaArquivoEnvio(rs.getString("COD_LINHA_ARQ_REC_ENVIO"));            	
				notificacao.setCodLinhaArquivoRetorno(rs.getString("COD_LINHA_ARQ_REC_RETORNO"));  
				
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		return notificacoes;
	}
	
	
	/**
	 * @author Pedro N�brega
	 * @param notifControle
	 * @param prazo
	 * @param totalMesAno
	 * @return
	 * @throws DaoException
	 */
	public List getNotificacoes(NotifControleBean notifControle, int prazo, String totalMesAno, 
			String rowNum, HashMap proxAnt, String[] orgaos, String rowNumImp) throws DaoException{
		Connection conn = null;    
		List notificacoes = new ArrayList();
		int totreg = 0;
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd = "SELECT num_notificacao, NUM_AUTO_INFRACAO, NUM_PLACA, COD_ARQUIVO_ENVIO, COD_ORGAO_ENVIO, ";
			sCmd += " DAT_EMISSAO, DAT_RECEB, NOM_USERNAME_ENVIO, to_char(dat_envio, 'mm'), to_char(dat_envio, 'yyyy') ";		
			sCmd += "FROM TSMI_CONTROLE_NOTIFICACOES ";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!notifControle.getCodStatusNotif().equals("")){
				sCmd += " WHERE COD_STATUS_NOTIF = "+notifControle.getCodStatusNotif();
				flag = false;
			}
			if(!notifControle.getCodStatusAuto().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				else
					sCmd += " AND COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				flag = false;	
			}
			
			if (orgaos.length > 1) {
				String orgaosWhere = "(";
				for (int i = 0; i < orgaos.length; i++) {
					if (i == orgaos.length - 1)
						orgaosWhere += orgaos[i] + ")";
					else
						orgaosWhere += orgaos[i] + ",";
				}				
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				else
					sCmd += " AND COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				flag = false;
				
			} else {
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO = "+orgaos[0];
				else
					sCmd += " AND COD_ORGAO_AUTUACAO = "+orgaos[0];
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE to_char(dat_envio, 'mm') = '" + totalMesAno.substring(0, 2) + "'";
			else
				sCmd += " AND to_char(dat_envio, 'mm') = '" + totalMesAno.substring(0, 2) + "'";
			
			//sCmd += " AND rownum <= '" + Integer.parseInt(rowNum) + "'";
			
			if (!proxAnt.get("primeiroRegistro").equals(""))
			{
				if (proxAnt.get("sentido").equals("Ant"))
				{
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("primeiroRegistro") + "'";
					/*if (!proxAnt.get("primeiroAnterior").equals(""))
					 {
					 sCmd += " AND NUM_NOTIFICACAO >= '" + (String)proxAnt.get("primeiroAnterior") + "'";
					 }*/
				}
				else if (proxAnt.get("sentido").equals("Prox"))                                         
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				else if (proxAnt.get("sentido").equals("Imprimir")) {
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("primeiroRegistro") + "'";
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				}
			}
			
			sCmd += " AND to_char(dat_envio, 'yyyy') = '" + totalMesAno.substring(3, 7) + "'";
			sCmd += " AND trunc(dat_envio) < (trunc(sysdate) - " + prazo + ") ";
			sCmd += " ORDER BY NUM_NOTIFICACAO ";
			ResultSet rs = stmt.executeQuery(sCmd);
			NotifControleBean notificacao = null;
			while(rs.next() && (rowNumImp.equals("-1") || totreg < Integer.parseInt(rowNum)))			
			{
				notificacao = new NotifControleBean();           	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));          	      	           	           	
				notificacao.setNomUsuarioEnvio(rs.getString("NOM_USERNAME_ENVIO"));            	           	
				notificacao.setDatReceb(rs.getDate("DAT_RECEB"));   
				notificacao.setDatEmissao((rs.getDate("DAT_EMISSAO")));
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO_ENVIO")); 
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO_ENVIO"));
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
				totreg++;
			}
			notifControle.setListaNotifs(notificacoes);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
		return notificacoes;
	}	
	
	
	public List getNotificacoes(NotifControleBean notifControle) throws DaoException{
		Connection conn = null;
		List retorno = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			retorno = getNotificacoes(notifControle, conn);
			conn.commit();
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}       
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {}
			}
		}
		return retorno;
	}	
	
	
	/**
	 * Consulta o nome do arquivo de maior sequ�ncia, referente a um grupo de arquivos
	 * gerados.
	 * @param auxClasse
	 * @author Sergio Roberto Junior
	 * @return
	 * @throws DaoException
	 * @since 05/04/2005
	 * @version 1.0
	 */
	public String buscaNomeArquivo(ArquivoRecebidoBean auxClasse)throws DaoException {
		Connection conn = null ;
		try {
			String retorno = "";
			String nomArquivo = auxClasse.getNomArquivo().substring(0,auxClasse.getNomArquivo().indexOf(
					"."+auxClasse.getSufixoRetornoGrv()));			
			conn = serviceloc.getConnection(MYABREVSIST) ;									
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT NOM_ARQUIVO from TSMI_ARQUIVO_RECEBIDO"+
			" WHERE NOM_ARQUIVO LIKE '" + nomArquivo + "%'" +
			" ORDER BY NOM_ARQUIVO";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				retorno = rs.getString("NOM_ARQUIVO");
			}
			rs.close();
			stmt.close();			
			return retorno;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { throw new DaoException(ey.getMessage()); }
			}
		}
	}
	
	/**
	 * M�todo utilizado para retornar a descri��o do status de um auto de acordo com
	 * o seu status passado por par�metro.
	 * @param codStatus
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public String getDescStatusAuto(String codStatus) throws DaoException{
		Connection conn = null;
		String resultado = null;
		try {           
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =  "SELECT * FROM TSMI_STATUS_AUTO " +
			" WHERE COD_STATUS = "+ codStatus;
			ResultSet rs = stmt.executeQuery(sCmd) ;
			if(rs.next())
				resultado = rs.getString("NOM_STATUS");
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}       
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return resultado;
	} 
	
	/**
	 * M�todo utilizado para retornar a descri��o do status de uma notifica��o de ]
	 * acordo com o seu status passado por par�metro.
	 * @param codNotificacao
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public String getStatusNotificacao(String codNotificacao) throws DaoException{
		Connection conn = null;
		String resultado = null;
		try {           
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =  "SELECT * FROM TSMI_STATUS_NOTIFICACAO " +
			" WHERE COD_NOTIFICACAO = "+ codNotificacao;
			ResultSet rs = stmt.executeQuery(sCmd) ;
			if(rs.next())
				resultado = rs.getString("DSC_NOTIFICACAO");
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}       
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return resultado;
	} 
	
	/**
	 * M�todo utilizado para atualizar os dados de retorno original da notifica��o
	 * @param notifControle
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws DaoException
	 * @version 1.0
	 */    
	public void atualizaRetornoOrig(NotifControleBean notifControle, Connection conn) throws DaoException{
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES"+
			" WHERE NUM_NOTIFICACAO = "+notifControle.getNumNotificacao()+
			" AND COD_STATUS_NOTIF = 4"; 
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				sCmd = "UPDATE TSMI_CONTROLE_NOTIFICACOES SET";
				sCmd += " NOM_USERNAME_RETORNO_ORIG = '" + rs.getString("NOM_USERNAME_RETORNO")+"',";
				sCmd += " COD_ORGAO_RETORNO_ORIG = '" + rs.getString("COD_ORGAO_RETORNO")+"',";
				sCmd += " DAT_RETORNO_ORIG = '"+ sdf.format(rs.getDate("DAT_RETORNO"))+"'";
				sCmd += " WHERE NUM_NOTIFICACAO = "+notifControle.getNumNotificacao();
			}
			rs.close();
			stmt.executeUpdate(sCmd);
			stmt.close();
		}
		catch (Exception e) {           
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 *------------------------------------------------------------------------
	 * DAO relativao ao envio de arquivo via webservice
	 *------------------------------------------------------------------------
	 */	
	
	public boolean ArquivoInsertTemp(ArquivoRecebidoBean auxClasse) throws DaoException {
		boolean existe = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;								
			conn.setAutoCommit(false);
			Statement stmt  = conn.createStatement();
			
			//verificar se o arquivo j� existe
			String sCmd = "SELECT /*+RULE*/ COD_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO_TMP WHERE NOM_ARQUIVO = '"
				+ auxClasse.getNomArquivo() + "'";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			if (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				existe = true;
			}
			rs.close();
			
			if (existe) {
				//buscar a ultima linha enviada
				sCmd = "SELECT /*+RULE*/ MAX(NUM_SEQ_LINHA) NUM_LINHA FROM TSMI_LINHA_ARQUIVO_REC_TMP WHERE COD_ARQUIVO = "
					+ auxClasse.getCodArquivo();
				rs = stmt.executeQuery(sCmd) ;
				if (rs.next()) {
					auxClasse.setUltLinha(rs.getInt("num_linha"));
				}
				rs.close();
				
			} else {			
				// buscar codigo do arquivo
				sCmd = "SELECT SEQ_TSMI_ARQUIVO_RECEBIDO_TMP.NEXTVAL COD_ARQUIVO FROM DUAL";
				rs = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
					auxClasse.setPkid(auxClasse.getCodArquivo());
				}
				rs.close();
				
				// inserir o arquivo na tabela de controle
				sCmd  = "INSERT INTO TSMI_ARQUIVO_RECEBIDO_TMP (COD_ARQUIVO," +
				"NOM_ARQUIVO,DAT_RECEBIMENTO,DAT_PROCESSAMENTO_DETRAN,COD_IDENT_ARQUIVO, " +
				"NUM_ANO_EXERCICIO, NUM_CONTROLE_ARQUIVO,NUM_PROTOCOLO, " +
				"IND_EMISSAO_NOTIF_DETRAN, QTD_REG,COD_ORGAO," + 
				"COD_ORGAO_LOTACAO,SIG_ORGAO_LOTACAO,NOM_USERNAME,COD_STATUS," +
				"NOM_RESPONSAVEL,COD_IDENT_MOVIMENTO,IND_PRIORIDADE,IND_P59)" +
				"VALUES (" + auxClasse.getCodArquivo() + ", " +
				"'"+auxClasse.getNomArquivo()+"', " +
				"to_date('"+auxClasse.getDatRecebimento()+"','dd/mm/yyyy'), " +
				"to_date('"+auxClasse.getDatProcessamentoDetran()+"','dd/mm/yyyy'), " +
				"'"+auxClasse.getCodIdentArquivo()+"', " +
				"'"+auxClasse.getNumAnoExercicio()+"', " +
				"'"+auxClasse.getNumControleArq()+"', " +
				"'"+auxClasse.getNumProtocolo()+"', " +
				"'"+auxClasse.getIndEmissaoNotifDetran()+"', " +
				"'"+auxClasse.getQtdReg()+"', " +
				"'"+auxClasse.getCodOrgaoAtuacao()+"', " +
				"'"+auxClasse.getCodOrgaoLotacao()+"', " +
				"'"+auxClasse.getSigOrgaoLotacao()+"', " +
				"'"+auxClasse.getNomUsername()+"', " +
				"'"+auxClasse.getCodStatus()+"', " +
				"'"+auxClasse.getResponsavelArq()+"', " +
				"'"+auxClasse.getIdentMovimento()+"', 1,"+
				"'"+auxClasse.getDscPortaria59()+"')";				
				stmt.executeUpdate(sCmd);
			}
			stmt.close();
			conn.commit();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try { 
					conn.rollback(); 
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return existe;
	}
	
	public boolean LinhaInsertTemp(ArquivoRecebidoBean auxClasse, String linha, int numLinha)
	throws DaoException {
		boolean bOk = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			conn.setAutoCommit(false);
			Statement stmt  = conn.createStatement();
			
			String sCmd = "SELECT /*+RULE*/ COD_LINHA_ARQ_REC FROM TSMI_LINHA_ARQUIVO_REC_TMP WHERE COD_ARQUIVO = " + 
			auxClasse.getCodArquivo() + " AND NUM_SEQ_LINHA = " + numLinha;
			ResultSet rs = stmt.executeQuery(sCmd);
			if (!rs.next()) {
				LinhaArquivoRec linhaBean = new LinhaArquivoRec();				
				linhaBean.setDscLinhaArqRec(linha.replaceAll("'", "\""));									
				linhaBean.setNumSeqLinha(String.valueOf(numLinha)); 
				linhaBean.carregarCampos(auxClasse);
				sCmd = "INSERT INTO TSMI_LINHA_ARQUIVO_REC_TMP (COD_LINHA_ARQ_REC,DSC_LINHA_ARQ_REC,"
					+ "NUM_SEQ_LINHA,COD_ARQUIVO,COD_STATUS,NUM_AUTO_INFRACAO,NUM_NOTIFICACAO)"
					+ " VALUES (SEQ_TSMI_LINHA_ARQUIVO_REC.NEXTVAL,"
					+ "'" + linhaBean.getDscLinhaArqRec().trim() + "','" + linhaBean.getNumSeqLinha() + "',"
					+ auxClasse.getCodArquivo() + ",0,'" + linhaBean.getNumAutoInfracao() + "','" +
					linhaBean.getNumNotificacao() + "')";  
				stmt.executeUpdate(sCmd);
			}
			stmt.close();
			conn.commit();
			bOk = true;
		}
		catch (Exception e) {
			bOk = false ;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {				
					serviceloc.setReleaseConnection(conn);
				} catch (Exception e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return bOk;
	}
	
	public void ArquivoMoveTemp(ArquivoRecebidoBean arquivo, REC.ParamOrgBean paramOrgaoId) throws DaoException {
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);
			//Guarda o codigo do arquivo temporario
			int codArquivo = Integer.parseInt(arquivo.getCodArquivo());			
			//Inclui na tabela de definitiva
			if (arquivo.isInsert(paramOrgaoId, conn)) {
				//Remove da tabela tempor�ria
				Dao dao = Dao.getInstance();
				dao.ArquivoDeleteTemp(codArquivo, conn);
			}
			conn.commit();
		}
		catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception ef) { throw new DaoException(ef.getMessage()); }
			}
		}
	}
	
	
	public boolean ArquivoDeleteTemp(int codArquivo, Connection conn) throws DaoException, SQLException {
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			
			String sCmd = "DELETE /*+RULE*/ FROM TSMI_LINHA_ARQUIVO_REC_TMP WHERE COD_ARQUIVO = " + codArquivo;				
			stmt.executeUpdate(sCmd);
			
			sCmd = "DELETE /*+RULE*/ FROM TSMI_ARQUIVO_RECEBIDO_TMP WHERE COD_ARQUIVO = " + codArquivo;				
			stmt.executeUpdate(sCmd);
			
			stmt.close();
			bOk = true;
		}
		catch (Exception e) {
			bOk = false;
			throw new DaoException(e.getMessage());
		}
		return bOk;
	}
	public boolean ArquivoDeleteTemp(int codArquivo) throws DaoException, SQLException {
		boolean bOk = false;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			ArquivoDeleteTemp(codArquivo, conn);
			conn.commit();
		}
		catch (Exception e) {
			bOk = false;
			conn.rollback();
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { throw new DaoException(ey.getMessage()); }
			}
		}
		return bOk;
	}	
	
	public boolean PegarOrgaoAutuacao(String codigo, ArquivoRecebidoBean auxClasse) throws DaoException {
		boolean ok = false ;
		Connection conn = null ;
		ResultSet rs =null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();						
			String sCmd = "SELECT COD_ORGAO, SIG_ORGAO from TSMI_ORGAO WHERE COD_ORGAO = " + codigo;			
			rs = stmt.executeQuery(sCmd) ;
			if (rs.next()) { 
				auxClasse.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				auxClasse.setSigOrgaoAtuacao(rs.getString("SIG_ORGAO"));	
				ok=true;
			} 						
			stmt.close();	
			rs.close();
		}
		catch (Exception ex) {
			ok = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { throw new DaoException(ey.getMessage()); }
			}
		}
		return ok;				
	}		
	
	public List BuscaArquivosTemp(ArquivoRecebidoBean auxClasse) throws DaoException {
		List vRetClasse   = new ArrayList();     		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT A.NOM_ARQUIVO, A.COD_ARQUIVO, A.QTD_REG, to_char(A.DAT_RECEBIMENTO,'dd/mm/yyyy') DAT_RECEBIMENTO,"+
			" (SELECT COUNT(*) FROM TSMI_LINHA_ARQUIVO_REC_TMP L WHERE L.COD_ARQUIVO = A.COD_ARQUIVO) QTD_ENVIADA" +
			" FROM TSMI_ARQUIVO_RECEBIDO_TMP A" +
			" WHERE A.COD_IDENT_ARQUIVO ='" + auxClasse.getCodIdentArquivo() + "'" +
			" AND A.COD_ORGAO = " + auxClasse.getCodOrgao() + 
			" ORDER BY A.COD_ARQUIVO";         
			ResultSet rs = stmt.executeQuery(sCmd);             
			while (rs.next())   {
				ArquivoRecebidoBean relClasse = new ArquivoRecebidoBean();
				relClasse.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				relClasse.setCodArquivo(rs.getString("COD_ARQUIVO"));				
				relClasse.setQtdReg(rs.getString("QTD_REG"));				
				relClasse.setQtdEnviada(rs.getInt("QTD_ENVIADA"));
				relClasse.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));
				vRetClasse.add(relClasse);           
			}
			stmt.close();
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { throw new DaoException(e.getMessage()); }
			}
		}       
		return vRetClasse;
	}
	
	/**
	 * @author Pedro N�brega
	 * @param notifControle
	 * @param prazo
	 * @return
	 * @throws DaoException
	 */
	public List getTotalMesAnoNotificacoes(NotifControleBean notifControle, int prazo, String[] orgaos) throws DaoException{
		Connection conn = null;    
		List totalAnosMeses = new ArrayList();
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd = "SELECT count(*) as total, to_char(dat_envio, 'mm') as mes, ";
			sCmd += "to_char(dat_envio, 'yyyy') as ano from TSMI_CONTROLE_NOTIFICACOES"; 
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!notifControle.getCodStatusNotif().equals("")){
				sCmd += " WHERE COD_STATUS_NOTIF = "+notifControle.getCodStatusNotif();
				flag = false;
			}
			if(!notifControle.getCodStatusAuto().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				else
					sCmd += " AND COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				flag = false;	
			}
			
			if (orgaos.length > 1) {
				String orgaosWhere = "(";
				for (int i = 0; i < orgaos.length; i++) {
					if (i == orgaos.length - 1)
						orgaosWhere += orgaos[i] + ")";
					else
						orgaosWhere += orgaos[i] + ",";
				}				
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				else
					sCmd += " AND COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				flag = false;
				
			} else {
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO = "+orgaos[0];
				else
					sCmd += " AND COD_ORGAO_AUTUACAO = "+orgaos[0];
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE trunc(dat_envio) < (trunc(sysdate) - " + prazo + ") ";
			else
				sCmd += " AND trunc(dat_envio) < (trunc(sysdate) - " + prazo + ") ";
			sCmd += "group by to_char(dat_envio, 'mm'), to_char(dat_envio, 'yyyy') ";
			sCmd += "order by ano,mes";
			ResultSet rs = stmt.executeQuery(sCmd); 
			while(rs.next())
				totalAnosMeses.add(rs.getString("mes").concat(" ").concat(rs.getString("ano")).concat(" ").concat(rs.getString("total")));	
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return totalAnosMeses;
		
	}
	
	public void LeBanlancoPendenciaAR(BalancoPendenciaARBean BalancoPendenciaARBeanID) throws DaoException {
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			ResultSet rs        = null;
			
			/*Enviados*/
			String sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO WHERE "+ 
			"ANO_ENVIO='"+BalancoPendenciaARBeanID.getAno()+"' AND "+
			"ANO_ENVIO||MES_ENVIO>='"+
			BalancoPendenciaARBeanID.getDatContrato().substring(3,7)+
			BalancoPendenciaARBeanID.getDatContrato().substring(0,2)+"' "+
			"ORDER BY ANO_ENVIO,MES_ENVIO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                = rs.getInt("MES_ENVIO");
				String sQtd_Enviado     = rs.getString("QTD_EMITIDO");
				String numCodigoRetorno = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				
				/*Enviados-Mensal-Geral*/
				if (numCodigoRetorno.equals("000")) {
					Long lTotalEnviado=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					/*Total Emitidos-Geral*/
					lTotalEnviado=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					BalancoPendenciaARBeanID.setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					continue;
				}
			}			

			/*Retornados*/
			sCmd="SELECT MES_RETORNO,QTD_RETORNO,QTD_RECUPERADA_RETORNO, "+
			"COD_RETORNO,IND_ENTREGUE,IND_FATURADO FROM TSMI_CONTROLE_VEX_RESUMO WHERE "+ 
			"ANO_RETORNO='"+BalancoPendenciaARBeanID.getAno()+"' AND "+
			"ANO_ENVIO||MES_ENVIO>='"+
			BalancoPendenciaARBeanID.getDatContrato().substring(3,7)+
			BalancoPendenciaARBeanID.getDatContrato().substring(0,2)+"' "+
			"ORDER BY ANO_RETORNO,MES_RETORNO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                   = rs.getInt("MES_RETORNO");
				String sQtd                = rs.getString("QTD_RETORNO");
				String sQtd_Recup_Entrega  = rs.getString("QTD_RECUPERADA_RETORNO");
				String numCodigoRetorno    = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				String indFaturado         = rs.getString("IND_FATURADO");
				
				if (numCodigoRetorno.trim().equals("000")) continue;
				
				
				if (numCodigoRetorno.length()>0) //Retornado
				{
					if (indFaturado.equals("N"))
					{
						//Total Credito - Mensal
						Long lTotalCredito=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalCredito())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalCredito(String.valueOf(lTotalCredito));
						
						//Total Recuperado-Geral
						lTotalCredito=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalCredito())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.setQtdTotalCredito(String.valueOf(lTotalCredito));
					}
					else
					{
						/*Total Faturar - Mensal*/
						Long lTotalFaturar=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalFaturar())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalFaturar(String.valueOf(lTotalFaturar));
						/*Total Faturar-Geral*/
						lTotalFaturar=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalFaturar())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.setQtdTotalFaturar(String.valueOf(lTotalFaturar));
					}
					
					if (numCodigoRetorno.equals("99"))
					{
						//Total Recuperado Entrega - Mensal
						Long lTotalRecuperado=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalRecuperadoServico())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalRecuperadoServico(String.valueOf(lTotalRecuperado));
						//Total Recuperado-Geral
						lTotalRecuperado=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalRecuperadoServico())+
						Long.parseLong(sQtd);
						BalancoPendenciaARBeanID.setQtdTotalRecuperadoServico(String.valueOf(lTotalRecuperado));
					}
					else
					{
						/*Recuperado Entrega*/
						Long lTotalRecuperadoEntrega=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						
						
						BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
						/*Total Recuperado-Geral*/
						lTotalRecuperadoEntrega=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						BalancoPendenciaARBeanID.setQtdTotalRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
					}
				}
				
				
				
				Long lTotalPrimeiroRetorno=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalFaturar())+
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalCredito())+
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalRecuperadoServico());
				BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalPrimeiroRetorno(String.valueOf(lTotalPrimeiroRetorno));
				
				Long lTotalRetornoDuplicata=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalRecuperadoEntrega())+
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(iMes-1).getQtdTotalDuplicata());
				BalancoPendenciaARBeanID.getRetornos(iMes-1).setQtdTotalRetornoDuplicata(String.valueOf(lTotalRetornoDuplicata));
				

				
				
				
			}
			
			Long lTotalGeralPrimeiroRetorno=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalFaturar())+
			Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalCredito())+
			Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalRecuperadoServico());
			BalancoPendenciaARBeanID.setQtdTotalPrimeiroRetorno(String.valueOf(lTotalGeralPrimeiroRetorno));
			
						
			//Saldo Anterior/Saldo Atual
			for (int i=0;i<BalancoPendenciaARBeanID.getListRetornoMes().size();i++)
			{
				Long lTotalSaldoAtual=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(i).getQtdTotalAnterior())+
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(i).getQtdTotalEnviada())-
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(i).getQtdTotalPrimeiroRetorno());
				BalancoPendenciaARBeanID.getRetornos(i).setQtdSaldoRetornar(String.valueOf(lTotalSaldoAtual));
				if (i>0)
				  BalancoPendenciaARBeanID.getRetornos(i).setQtdTotalAnterior(BalancoPendenciaARBeanID.getRetornos(i-1).getQtdSaldoRetornar());

				Long lTotalRetornado=Long.parseLong(BalancoPendenciaARBeanID.getRetornos(i).getQtdTotalPrimeiroRetorno())+
				Long.parseLong(BalancoPendenciaARBeanID.getRetornos(i).getQtdTotalRetornoDuplicata());
				BalancoPendenciaARBeanID.getRetornos(i).setQtdTotalRetorna(String.valueOf(lTotalRetornado));
				
				Long lTotalGeralRetornado=Long.parseLong(BalancoPendenciaARBeanID.getQtdTotalRetorna())+lTotalRetornado;
				BalancoPendenciaARBeanID.setQtdTotalRetorna(String.valueOf(lTotalGeralRetornado));
				
				
			}

			
			
			
			
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	
	public void LeRetornos(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException {
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			ResultSet rs        = null;
			
			String sCmd="SELECT MES_RETORNO,QTD_RETORNO_12,QTD_RETORNO_13,QTD_RETORNO_14,QTD_RECUPERADA_RETORNO,"+
			"QTD_RETORNO_AVISO,QTD_RECUPERADA_SMIT,COD_RETORNO,IND_ENTREGUE,IND_FATURADO FROM TSMI_CONT_VEX_TOT_RET_V1 WHERE "+ 
			"ANO_RETORNO='"+ResumoRetornoBeanId.getAno()+"'AND COD_IDENT_ARQUIVO='"+ResumoRetornoBeanId.getCodIdentArquivo()+"'" +
			//"' AND "+ "ANO_ENVIO||MES_ENVIO>='"+
			//ResumoRetornoBeanId.getDatContrato().substring(3,7)+
			//ResumoRetornoBeanId.getDatContrato().substring(0,2)+"' "+
			"ORDER BY ANO_RETORNO,MES_RETORNO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                   = rs.getInt("MES_RETORNO");
				int iQtd                   = rs.getInt("QTD_RETORNO_12")+
				                             rs.getInt("QTD_RETORNO_13")+
				                             rs.getInt("QTD_RETORNO_14")+
				                             rs.getInt("QTD_RETORNO_AVISO");
				                            // rs.getInt("QTD_RECUPERADA_RETORNO");
				
				String sQtd = String.valueOf(iQtd);
				                         
				String sQtd_Recup_Smit     = rs.getString("QTD_RECUPERADA_SMIT");
				if (sQtd_Recup_Smit==null) sQtd_Recup_Smit="0";
				String sQtd_Recup_Entrega  = rs.getString("QTD_RECUPERADA_RETORNO");
				String numCodigoRetorno    = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				String indEntrega          = rs.getString("IND_ENTREGUE");
				String indFaturado         = rs.getString("IND_FATURADO");
				
				if (numCodigoRetorno.trim().equals("000")) continue;
				
				
				if (numCodigoRetorno.length()>0) /*Retornado*/
				{
					if (indFaturado.equals("N"))
					{
						/*Total Credito - Mensal*/
						Long lTotalCredito=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdCredito())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdCredito(String.valueOf(lTotalCredito));
						/*Total Recuperado-Geral*/
						lTotalCredito=Long.parseLong(ResumoRetornoBeanId.getQtdCredito())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.setQtdCredito(String.valueOf(lTotalCredito));
					}
					
					
					if (numCodigoRetorno.equals("99"))
					{
						/*Total Recuperado Entrega - Mensal*/
						Long lTotalRecuperado=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdRecuperadoSistema())+
						Long.parseLong(sQtd) + Long.parseLong(sQtd_Recup_Smit);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdRecuperadoSistema(String.valueOf(lTotalRecuperado));
						/*Total Recuperado-Geral*/
						lTotalRecuperado=Long.parseLong(ResumoRetornoBeanId.getQtdRecuperadoSistema())+
						Long.parseLong(sQtd) + Long.parseLong(sQtd_Recup_Smit);
						ResumoRetornoBeanId.setQtdRecuperadoSistema(String.valueOf(lTotalRecuperado));
					}
					else
					{
						
						/*Recuperado Entrega*/
						Long lTotalRecuperadoEntrega=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
						/*Total Recuperado-Geral*/
						lTotalRecuperadoEntrega=Long.parseLong(ResumoRetornoBeanId.getQtdRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						ResumoRetornoBeanId.setQtdRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
						
						
						/*Total Retornado-Mensal*/
						Long lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalRetornada())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
						/*Total Retornado-Geral*/
						lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalRetornada())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.setQtdTotalRetornada(String.valueOf(lTotalRetornada));
						
						/*Total Retornado-C�digo de Retorno*/
						for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
						{     
							if ( (numCodigoRetorno.equals(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getNumRetorno())) &&
									indEntrega.equals((ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N"))
							)
							{
								/*Total Retornado-C�digo de Retorno-Mensal*/
								lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).getQtdTotalRetornada())+
								Long.parseLong(sQtd);
								ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
								/*Total Retornado-C�digo de Retorno-Geral*/
								lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())+
								Long.parseLong(sQtd);
								ResumoRetornoBeanId.getCodigoRetornos(i).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
								break;
							}
						}
						
					}
					
				}
			}
			
			/*CSS/CSS Anteriores/NCSS Anteriores/Faturamento*/
			for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++)
			{
				
				Long lTotalFaturamento=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())-
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdCredito())+
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoEntrega())+
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoSistema());
				ResumoRetornoBeanId.getRetornos(i).setQtdFaturamento(String.valueOf(lTotalFaturamento));
				
				Long lTotalGeralFaturamento=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdFaturamento());
				lTotalGeralFaturamento=Long.parseLong(ResumoRetornoBeanId.getQtdFaturamento())+
				lTotalGeralFaturamento;
				ResumoRetornoBeanId.setQtdFaturamento(String.valueOf(lTotalGeralFaturamento));
			}
			
			/*N�o Retornada Anos Anteriores*/
			sCmd="SELECT SUM(QTD_EMITIDO-QTD_EMITIDO_RETORNO) as QTD FROM TSMI_CONT_VEX_TOT_ENV_V1 " +
			"WHERE ANO_ENVIO='"+ResumoRetornoBeanId.getAnoAnt()+"'AND COD_IDENT_ARQUIVO='"+ResumoRetornoBeanId.getCodIdentArquivo()+"'";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				ResumoRetornoBeanId.setQtdTotalnaoRetornadaAnt(rs.getString("QTD"));
			}
			
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}


	
	public void LeRetornosEnvio(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException {
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt      = conn.createStatement();
			ResultSet rs        = null;
			String sCmd="SELECT * FROM TSMI_CONT_VEX_TOT_ENV_V1 WHERE "+ 
			"ANO_ENVIO='"+ResumoRetornoBeanId.getAno()+"' AND "+
			"COD_IDENT_ARQUIVO='"+ResumoRetornoBeanId.getCodIdentArquivo()+"'"+
			"ORDER BY ANO_ENVIO,MES_ENVIO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                = rs.getInt("MES_ENVIO");
				String sQtd_Enviado     = rs.getString("QTD_EMITIDO");
				String sQtd_Enviado_Ret = rs.getString("QTD_EMITIDO_RETORNO");
				String numCodigoRetorno = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				String indEntrega       = rs.getString("IND_ENTREGUE");
				
				
				/*Enviados-Mensal-Geral*/
				if (numCodigoRetorno.equals("000")) {
					Long lTotalEnviado=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					/*Total Emitidos-Geral*/
					lTotalEnviado=Long.parseLong(ResumoRetornoBeanId.getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					ResumoRetornoBeanId.setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					continue;
				}
				
				
				if (numCodigoRetorno.length()>0) /*Retornado*/
				{
					/*Total Retornado-Mensal*/
					Long lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalRetornada())+
					Long.parseLong(sQtd_Enviado_Ret);
					ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
					/*Total Retornado-Geral*/
					lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalRetornada())+
					Long.parseLong(sQtd_Enviado_Ret);
					ResumoRetornoBeanId.setQtdTotalRetornada(String.valueOf(lTotalRetornada));
					
					/*Total Retornado-C�digo de Retorno*/
					for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
					{     
						if ( (numCodigoRetorno.equals(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getNumRetorno())) &&
								indEntrega.equals((ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N"))
						)
						{
							/*Total Retornado-C�digo de Retorno-Mensal*/
							lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).getQtdTotalRetornada())+
							Long.parseLong(sQtd_Enviado_Ret);
							ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
							/*Total Retornado-C�digo de Retorno-Geral*/
							lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())+
							Long.parseLong(sQtd_Enviado_Ret);
							ResumoRetornoBeanId.getCodigoRetornos(i).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
							break;
						}
					}
				}
				
				/*N�o Retornados-Mensal-Geral*/				
				/*Total Retornado-Mensal*/
			}
			for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++)
			{
				Long lTotalnaoRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada())-
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada());
				ResumoRetornoBeanId.getRetornos(i).setQtdTotalnaoRetornada(String.valueOf(lTotalnaoRetornada));
				
				/*Total n�o Retornado-Geral*/
				lTotalnaoRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalnaoRetornada())+
				lTotalnaoRetornada;
				ResumoRetornoBeanId.setQtdTotalnaoRetornada(String.valueOf(lTotalnaoRetornada));
			}

			/*N�o Retornada Anos Anteriores*/
			sCmd="SELECT SUM(QTD_EMITIDO-QTD_EMITIDO_RETORNO) as QTD FROM TSMI_CONT_VEX_TOT_ENV_V1 " +
			"WHERE ANO_ENVIO='"+ResumoRetornoBeanId.getAnoAnt()+"'";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				ResumoRetornoBeanId.setQtdTotalnaoRetornadaAnt(rs.getString("QTD"));
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqNotificacaoBean
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaRelDiario(RelatArqNotificacaoBean ran,String datIni,String datFim, String identificadorArquivo) throws DaoException
	{
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			Statement stmt = conn.createStatement();
			
			if (identificadorArquivo.equals("EMITEVEX")){
			
				String sCmd = "SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
					+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
					+ " to_char(TAR.Dat_Recebimento,'DD/MM/YYYY') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
					+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
					+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
					+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
					+ " WHERE TAR.Cod_Ident_Arquivo = '"+ identificadorArquivo +"' "
					+ " AND TAR.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('"+ datFim +"','dd/mm/yyyy')"
					+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
					+ " AND TAR.Cod_Status <> 9 ";
				
				if (sHistorico.equals("S")) {
					sCmd += " UNION "
						+ " ("
						+ " SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
						+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
						+ " to_char(TAR.Dat_Recebimento,'dd/mm/yyyy') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
						+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
						+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
						+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
						+ " WHERE TAR.Cod_Ident_Arquivo = '"+ identificadorArquivo +"' "
						+ " AND TAR.Dat_Recebimento >= to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('" + datFim +"','dd/mm/yyyy')"
						+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
						+ " AND TAR.Cod_Status <> 9 " 
						+ " )";
				}
				sCmd += "ORDER BY 1";		
				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				Vector listaArq = new Vector();
				
				long totQtdReg =0;
				long totLinhPend =0;
				long totLinhCanc =0;
				long totQtdRec =0;			
				String qtdPendente, qtdRecebido, qtdCancelada;
				while( rs.next() ) {
					RelatArqNotificacaoBean myBean = new RelatArqNotificacaoBean();
					myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));
					myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
					myBean.setCodStatus(         rs.getString("Cod_status"));
					
					myBean.setQtdReg(            rs.getString("Qtd_Reg"));
					totQtdReg   += Long.parseLong(rs.getString("Qtd_Reg")); 
					
					if( rs.getString("Qtd_Linha_Pendente") == null )
						qtdPendente = "0";
					else
						qtdPendente = rs.getString("Qtd_Linha_Pendente");				
					myBean.setQtdLinhaPendente( qtdPendente );
					totLinhPend += Long.parseLong( qtdPendente );
					
					if( rs.getString("Qtd_Linha_Cancelada") == null )
						qtdCancelada = "0";
					else
						qtdCancelada = rs.getString("Qtd_Linha_Cancelada");				
					myBean.setQtdLinhaCancelada( qtdCancelada );
					totLinhCanc += Long.parseLong( qtdCancelada );
					
					if( rs.getString("Qtd_Recebido") == null )
						qtdRecebido = "0";
					else
						qtdRecebido = rs.getString("Qtd_Recebido");				
					myBean.setQtdRecebido( qtdRecebido );
					totQtdRec += Long.parseLong( qtdRecebido );
					
					myBean.setNumProtocolo(      rs.getString("Num_Protocolo"));
					myBean.setDatRecebimento(    rs.getString("Dat_Recebimento"));
					myBean.setTarNomUsername(    rs.getString("tarNomUserName"));
					myBean.setTarCodOrgaoLotacao(rs.getString("tarCodOrgaoLotacao"));
					myBean.setDatDownload(       rs.getString("Dat_Download"));
					myBean.setNomUsername(       rs.getString("tdNomUserName"));
					myBean.setCodOrgaoLotacao(   rs.getString("tdCodOrgaoLotacao"));
					myBean.setSitControleVEX(rs.getString("SIT_VEX"));
					listaArq.addElement( myBean );
					retorno = true;
				}
				
				sCmd ="SELECT DISTINCT a.NOM_ARQUIVO,DECODE(a.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"+
				"a.QTD_LINHA,a.QTD_LINHA_ERRO,a.QTD_LINHA_CANCELADA,a.NUM_PROTOCOLO, " +
				"(a.QTD_LINHA - a.QTD_LINHA_ERRO - a.QTD_LINHA_CANCELADA) AS Qtd_Recebido,"+
				" to_char(a.DAT_RECEBIMENTO,'dd/mm/yyyy') as DAT_RECEBIMENTO,"+
				" a.NOM_USERNAME,c.SIG_ORGAO,b.TOT_NOTIFICACOES,a.COD_ARQUIVO,a.SIT_VEX "+
				"  FROM TPNT_ARQUIVO a  , TSMI_ORGAO c, "+
				" (select count(*) as tot_notificacoes,cod_arquivo from tpnt_notificacao group by cod_arquivo) b "+
				" WHERE a.Cod_Ident_Arquivo = 'EMITEPONTO' AND a.COD_STATUS<> 9 " +
				" AND a.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') "+
				" AND a.Dat_Recebimento <=to_date('"+ datFim +"','dd/mm/yyyy') "+
				" AND a.cod_arquivo=b.cod_arquivo "+
				" AND a.Cod_orgao_lotacao=c.Cod_orgao "+
				" ORDER BY a.Nom_Arquivo";
				rs = stmt.executeQuery(sCmd);
				while( rs.next() ) {
					RelatArqNotificacaoBean myBean = new RelatArqNotificacaoBean();
					myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));
					myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
					myBean.setCodStatus(         rs.getString("Cod_status"));
					
					if( rs.getString("TOT_NOTIFICACOES") == null )
						qtdRecebido = "0";
					else	qtdRecebido = rs.getString("TOT_NOTIFICACOES");				
					myBean.setQtdReg(qtdRecebido);
					
					myBean.setQtdLinhaPendente( "0" );				
					
					totQtdReg   += Long.parseLong(qtdRecebido); 		
					myBean.setQtdRecebido( qtdRecebido );
					totQtdRec += Long.parseLong( qtdRecebido );
					
					myBean.setNumProtocolo(      rs.getString("NUM_PROTOCOLO"));
					myBean.setDatRecebimento(    rs.getString("DAT_RECEBIMENTO"));				
					myBean.setTarNomUsername(    rs.getString("NOM_USERNAME"));
					myBean.setTarCodOrgaoLotacao(rs.getString("SIG_ORGAO"));
					myBean.setSitControleVEX(rs.getString("SIT_VEX"));				
					listaArq.addElement( myBean );
					retorno = true;
				}			
				ran.setTotalQtdReg( String.valueOf(totQtdReg));
				ran.setTotalQtdLinhaPendente( String.valueOf(totLinhPend));
				ran.setTotalQtdLinhaCancelada( String.valueOf(totLinhCanc));
				ran.setTotalQtdRecebido( String.valueOf(totQtdRec));
				
				ran.setListaArqs( listaArq );
				rs.close();
				stmt.close();
				serviceloc.setReleaseConnection(conn);
				
			} else if (identificadorArquivo.equals("EMITEPER")){
				String sCmd = "SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
						+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
						+ " to_char(TAR.Dat_Recebimento,'DD/MM/YYYY') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
						+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
						+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
						+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
						+ " WHERE TAR.Cod_Ident_Arquivo = '"+ identificadorArquivo +"' "
						+ " AND TAR.Dat_Recebimento >=to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('"+ datFim +"','dd/mm/yyyy')"
						+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
						+ " AND TAR.Cod_Status <> 9 ";
					
					if (sHistorico.equals("S")) {
						sCmd += " UNION "
							+ " ("
							+ " SELECT DISTINCT TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'E', 8,'E') Cod_status,"
							+ " TAR.Qtd_Reg,TAR.Qtd_Linha_Pendente,TAR.Qtd_Linha_Cancelada,TAR.Num_Protocolo,(TAR.Qtd_Reg - TAR.Qtd_Linha_Pendente - TAR.Qtd_Linha_Cancelada) AS Qtd_Recebido,"
							+ " to_char(TAR.Dat_Recebimento,'dd/mm/yyyy') Dat_Recebimento,TAR.Nom_Username as tarNomUserName,TTA.SIG_ORGAO AS tarCodOrgaoLotacao,"
							+ " NVL(TD.Nom_Username, ' - ') AS tdNomUserName,nvl(to_char(TD.Dat_Download,'dd/mm/yyyy'), ' - ') Dat_Download, NVL(TTD.SIG_ORGAO, ' - ') AS tdCodOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
							+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP TAR LEFT outer join TSMI_DOWNLOAD TD JOIN TSMI_ORGAO TTD ON TD.COD_ORGAO_LOTACAO = TTD.COD_ORGAO"
							+ " ON TAR.COD_ARQUIVO = TD.COD_ARQUIVO ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
							+ " WHERE TAR.Cod_Ident_Arquivo = '"+ identificadorArquivo +"' "
							+ " AND TAR.Dat_Recebimento >= to_date('"+ datIni +"','dd/mm/yyyy') AND " + "TAR.Dat_Recebimento <= to_date('" + datFim +"','dd/mm/yyyy')"
							+ " AND (TD.Dat_download = (SELECT min(dat_download) FROM TSMI_DOWNLOAD A where td.cod_arquivo = a.cod_arquivo) OR  TD.dat_download is null)"
							+ " AND TAR.Cod_Status <> 9 " 
							+ " )";
					}
					sCmd += "ORDER BY 1";		
					
					
					ResultSet rs = stmt.executeQuery(sCmd);
					
					Vector listaArq = new Vector();
					
					long totQtdReg =0;
					long totLinhPend =0;
					long totLinhCanc =0;
					long totQtdRec =0;			
					String qtdPendente, qtdRecebido, qtdCancelada;
					while( rs.next() ) {
						RelatArqNotificacaoBean myBean = new RelatArqNotificacaoBean();
						myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));
						myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
						myBean.setCodStatus(         rs.getString("Cod_status"));
						
						myBean.setQtdReg(            rs.getString("Qtd_Reg"));
						totQtdReg   += Long.parseLong(rs.getString("Qtd_Reg")); 
						
						if( rs.getString("Qtd_Linha_Pendente") == null )
							qtdPendente = "0";
						else
							qtdPendente = rs.getString("Qtd_Linha_Pendente");				
						myBean.setQtdLinhaPendente( qtdPendente );
						totLinhPend += Long.parseLong( qtdPendente );
						
						if( rs.getString("Qtd_Linha_Cancelada") == null )
							qtdCancelada = "0";
						else
							qtdCancelada = rs.getString("Qtd_Linha_Cancelada");				
						myBean.setQtdLinhaCancelada( qtdCancelada );
						totLinhCanc += Long.parseLong( qtdCancelada );
						
						if( rs.getString("Qtd_Recebido") == null )
							qtdRecebido = "0";
						else
							qtdRecebido = rs.getString("Qtd_Recebido");				
						myBean.setQtdRecebido( qtdRecebido );
						totQtdRec += Long.parseLong( qtdRecebido );
						
						myBean.setNumProtocolo(      rs.getString("Num_Protocolo"));
						myBean.setDatRecebimento(    rs.getString("Dat_Recebimento"));
						myBean.setTarNomUsername(    rs.getString("tarNomUserName"));
						myBean.setTarCodOrgaoLotacao(rs.getString("tarCodOrgaoLotacao"));
						myBean.setDatDownload(       rs.getString("Dat_Download"));
						myBean.setNomUsername(       rs.getString("tdNomUserName"));
						myBean.setCodOrgaoLotacao(   rs.getString("tdCodOrgaoLotacao"));
						myBean.setSitControleVEX(rs.getString("SIT_VEX"));
						listaArq.addElement( myBean );
						retorno = true;
					}
					ran.setTotalQtdReg( String.valueOf(totQtdReg));
					ran.setTotalQtdLinhaPendente( String.valueOf(totLinhPend));
					ran.setTotalQtdLinhaCancelada( String.valueOf(totLinhCanc));
					ran.setTotalQtdRecebido( String.valueOf(totQtdRec));
					
					ran.setListaArqs( listaArq );
					rs.close();
					stmt.close();
					serviceloc.setReleaseConnection(conn);
			}
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqRetornoBean
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaRelDiarioRet(RelatArqRetornoBean ran,String datIni,String datFim) throws DaoException
	{
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();			
			
			String sCmd = "SELECT /*+RULE*/ TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'Em P', 8,'E') Cod_status,"
				+ " TAR.Qtd_Reg, TAR.Num_Protocolo, (SELECT  /*+NO_INDEX(TSMI_LINHA_ARQUIVO_REC)*/ COUNT(*) FROM TSMI_LINHA_ARQUIVO_REC TLN"
				+ " WHERE TLN.Cod_Status=1 AND TLN.Cod_Retorno='000' AND TLN.Cod_Arquivo = TAR.Cod_Arquivo) AS Qtd_Recebido,"
				+ " TO_CHAR(TAR.Dat_Recebimento,'DD/MM/YYYY') Dat_Recebimento,TAR.Nom_Username as nomUserName,TTA.SIG_ORGAO AS codOrgaoLotacao,TAR.COD_ARQUIVO,TAR.SIT_VEX"
				+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
				+ " WHERE TAR.Cod_Ident_Arquivo = 'CODRET' "
				+ " AND TAR.Dat_Recebimento >='"+ datIni +"' AND " + "TAR.Dat_Recebimento <='" + datFim +"'"
				+ " AND TAR.Cod_Status <> 9 ORDER BY TAR.Nom_Arquivo";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector listaArq = new Vector();
			long totQtdReg =0;
			long totQtdRec =0;
			String zero="";
			while( rs.next() ) {
				RelatArqRetornoBean myBean = new RelatArqRetornoBean();
				myBean.setCodArquivo(        rs.getString("Cod_Arquivo"));				
				myBean.setNomArquivo(        rs.getString("Nom_Arquivo"));
				myBean.setCodStatus(         rs.getString("Cod_status"));
				myBean.setQtdReg(            rs.getString("Qtd_Reg"));
				totQtdReg   += Long.parseLong(rs.getString("Qtd_Reg")); 

				if( rs.getString("Qtd_Recebido") == null )
					zero = "0";
				else
					zero = rs.getString("Qtd_Recebido");
				
				myBean.setQtdRecebido( zero );
				totQtdRec += Long.parseLong( zero );
				
				myBean.setNumProtocolo(      rs.getString("Num_Protocolo"));
				myBean.setDatRecebimento(    rs.getString("Dat_Recebimento"));
				myBean.setNomUsername(       rs.getString("nomUserName"));
				myBean.setCodOrgaoLotacao(   rs.getString("codOrgaoLotacao"));
				myBean.setSitControleVEX(rs.getString("SIT_VEX"));
				listaArq.addElement( myBean );
				retorno = true;
			}
			ran.setTotalQtdReg( String.valueOf(totQtdReg));
			ran.setTotalQtdRecebido( String.valueOf(totQtdRec));
			
			ran.setListaArqs( listaArq );
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return retorno;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqEntregaBean
	 *-----------------------------------------------------------
	 */
	public boolean ConsultaRelEntrega(RelatArqEntregaBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			//Verifica se vai montar as querys para todos os org�os ou um s�
			boolean todosOrgaos = false;
			String codOrgao = "";
			String codSistema = "";
			String codUsuario = "";
			if ((bean.getOrgao().getCodOrgao() == null) || bean.getOrgao().getCodOrgao().equals("")) {				
				codSistema = bean.getSistema().getCodSistema();
				codUsuario = bean.getUsuario().getCodUsuario();
				todosOrgaos = true;
			}
			else
				codOrgao = bean.getOrgao().getCodOrgao();
			
			String sql = "select sum(e.qtd_registrado) qtd_registrado, sum(e.qtd_recebido) qtd_recebido,";
			sql += " sum(e.qtd_entregue) qtd_entregue, sum(e.qtd_tempo) qtd_tempo, sum(e.qtd_pendente) qtd_pendente,";
			sql += " e.dat_proc from tsmi_entrega_notificacoes e";
			if (todosOrgaos) {
				sql += ", tcau_perfil p, tcau_perfil_orgao o ";
				sql += " where o.cod_perfil = p.cod_perfil and p.cod_sistema = "+codSistema+" and o.cod_usuario = "+codUsuario;
				sql += " and e.cod_orgao = o.cod_orgao_atuacao";
			} else
				sql += " where e.cod_orgao = "+codOrgao;
			sql += " and e.dat_proc between :1 and :2 group by e.dat_proc order by e.dat_proc";
			PreparedStatement stm = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stm.setDate(1, new java.sql.Date(bean.getDatInicial().getTime()));
			stm.setDate(2, new java.sql.Date(bean.getDatFinal().getTime()));
			ResultSet rs = stm.executeQuery();
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(bean.getDatInicial());
			Date datAtual = cal.getTime();
			
			int totRegistrado = 0;
			int totRecebido = 0;
			int totEntregue = 0;
			int totTempo = 0;
			
			List beans = new ArrayList();
			while(datAtual.compareTo(bean.getDatFinal()) <= 0) {
				RelatArqEntregaBean item = new RelatArqEntregaBean();				
				item.setDatProc(datAtual);				
				
				if (rs.next()) {
					Date data = rs.getDate("dat_proc");
					if (data.equals(item.getDatProc())) {
						item.setQtdRegistrado(new Integer(rs.getInt("qtd_registrado")));
						totRegistrado += rs.getInt("qtd_registrado");
						
						item.setQtdRecebido(new Integer(rs.getInt("qtd_recebido")));
						totRecebido += rs.getInt("qtd_recebido");
						
						item.setQtdEntregue(new Integer(rs.getInt("qtd_entregue")));
						totEntregue += rs.getInt("qtd_entregue");
						
						item.setQtdTempo(new Integer(rs.getInt("qtd_tempo")));
						totTempo += rs.getInt("qtd_tempo");
						
						item.setQtdPendente(new Integer(rs.getInt("qtd_pendente")));						
					} else
						rs.previous();
				}
				
				beans.add(item);
				
				cal.add(Calendar.DAY_OF_YEAR, 1);
				datAtual = cal.getTime();
			}
			bean.setBeans(beans);
			bean.setTotalQtdRegistrado(new Integer(totRegistrado));
			bean.setTotalQtdRecebido(new Integer(totRecebido));
			bean.setTotalQtdTempo(new Integer(totTempo));
			bean.setTotalQtdEntregue(new Integer(totEntregue));
			
			retorno = true;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public void GravaRelEntrega(RelatArqEntregaBean bean, Connection conn) throws DaoException {		
		try {
			String sqlSel = "select dat_proc from tsmi_entrega_notificacoes where dat_proc = :1 and cod_orgao = :2";
			PreparedStatement stmSel = conn.prepareStatement(sqlSel);
			stmSel.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
			stmSel.setInt(2, Integer.parseInt(bean.getOrgao().getCodOrgao()));
			ResultSet rsSel = stmSel.executeQuery();
			
			if (rsSel.next()) {
				String sqlUpd = "update tsmi_entrega_notificacoes set qtd_registrado = :1, qtd_recebido = :2,";
				sqlUpd += " qtd_entregue = :3, qtd_tempo = :4, qtd_pendente = :5";
				sqlUpd += " where dat_proc = :6 and cod_orgao = :7";
				PreparedStatement stmUpd = conn.prepareStatement(sqlUpd);
				stmUpd.setInt(1, bean.getQtdRegistrado().intValue());
				stmUpd.setInt(2, bean.getQtdRecebido().intValue());
				stmUpd.setInt(3, bean.getQtdEntregue().intValue());
				stmUpd.setInt(4, bean.getQtdTempo().intValue());
				stmUpd.setInt(5, bean.getQtdPendente().intValue());
				stmUpd.setDate(6, new java.sql.Date(bean.getDatProc().getTime()));
				stmUpd.setInt(7, Integer.parseInt(bean.getOrgao().getCodOrgao()));				
				stmUpd.executeUpdate();
				stmUpd.close();
				
			} else {
				String sqlIns = "insert into tsmi_entrega_notificacoes (dat_proc, qtd_registrado, qtd_recebido,";
				sqlIns += " qtd_entregue, qtd_tempo, qtd_pendente, cod_orgao) values (:1, :2, :3, :4, :5, :6, :7)";
				PreparedStatement stmIns = conn.prepareStatement(sqlIns);
				stmIns.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
				stmIns.setInt(2, bean.getQtdRegistrado().intValue());
				stmIns.setInt(3, bean.getQtdRecebido().intValue());
				stmIns.setInt(4, bean.getQtdEntregue().intValue());
				stmIns.setInt(5, bean.getQtdTempo().intValue());
				stmIns.setInt(6, bean.getQtdPendente().intValue());
				stmIns.setInt(7, Integer.parseInt(bean.getOrgao().getCodOrgao()));
				stmIns.executeUpdate();
				stmIns.close();
			}
			stmSel.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public boolean TotalizaEntrega(RelatArqEntregaBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			int codOrgao = Integer.parseInt(bean.getOrgao().getCodOrgao());
			java.sql.Date datProc = new java.sql.Date(bean.getDatProc().getTime());			
			
			//Registrados
			int qtdRegistrado = 0;
			
			//Arquivos MR
			String sqlRegistradoMR = "select sum(qtd_reg) qtd from tsmi_arquivo_recebido";					
			sqlRegistradoMR += " where cod_orgao = :1 and cod_ident_arquivo = 'MR01' and dat_recebimento = :2";
			PreparedStatement stmRegistradoMR = conn.prepareStatement(sqlRegistradoMR);
			stmRegistradoMR.setInt(1, codOrgao);
			stmRegistradoMR.setDate(2, datProc);
			ResultSet rsRegistradoMR = stmRegistradoMR.executeQuery();
			if (rsRegistradoMR.next())
				qtdRegistrado += rsRegistradoMR.getInt("qtd");
			stmRegistradoMR.close();
			
			//Arquivos DOL
			String sqlRegistradoDOL = "select sum(l.qtd_reg) qtd from tsmi_arquivo_dol a, tsmi_lote_dol l";
			sqlRegistradoDOL += " where a.cod_arquivo_dol = l.cod_arquivo_dol and a.cod_orgao = :1 and trunc(a.dat_recebimento) = :2";
			PreparedStatement stmRegistradoDOL = conn.prepareStatement(sqlRegistradoDOL);
			stmRegistradoDOL.setInt(1, codOrgao);
			stmRegistradoDOL.setDate(2, datProc);
			ResultSet rsRegistradoDOL = stmRegistradoDOL.executeQuery();
			if (rsRegistradoDOL.next())
				qtdRegistrado += rsRegistradoDOL.getInt("qtd");
			stmRegistradoDOL.close();
			
			bean.setQtdRegistrado(new Integer(qtdRegistrado));
			
			//Recebidos
			String sqlRecebido = "select count(*) qtd from tsmi_controle_notificacoes";
			sqlRecebido += " where cod_orgao_autuacao = :1 and dat_envio = :2";
			PreparedStatement stmRecebido = conn.prepareStatement(sqlRecebido);
			stmRecebido.setInt(1, codOrgao);
			stmRecebido.setDate(2, datProc);
			ResultSet rsRecebido = stmRecebido.executeQuery();
			if (rsRecebido.next())
				bean.setQtdRecebido(new Integer(rsRecebido.getInt("qtd")));
			stmRecebido.close();
			
			//Entregues
			String sqlEntregue = "select count(*) qtd from tsmi_controle_notificacoes";
			sqlEntregue += " where cod_orgao_autuacao = :1 and dat_retorno = :2";
			PreparedStatement stmEntregue = conn.prepareStatement(sqlEntregue);
			stmEntregue.setInt(1, codOrgao);
			stmEntregue.setDate(2, datProc);
			ResultSet rsEntregue = stmEntregue.executeQuery();
			if (rsEntregue.next())
				bean.setQtdEntregue(new Integer(rsEntregue.getInt("qtd")));
			stmEntregue.close();
			
			//Pendentes
			String rule = "/*+ full(tsmi_controle_notificacoes)*/";
			String sqlPendente = "select "+rule+" count(*) qtd from tsmi_controle_notificacoes";
			sqlPendente += " where cod_orgao_autuacao = :1 and dat_envio <= :2 and (cod_status_notif = '2' or dat_retorno > :2)";
			PreparedStatement stmPendente = conn.prepareStatement(sqlPendente);
			stmPendente.setInt(1, codOrgao);
			stmPendente.setDate(2, datProc);
			ResultSet rsPendente = stmPendente.executeQuery();
			if (rsPendente.next())
				bean.setQtdPendente(new Integer(rsPendente.getInt("qtd")));
			stmPendente.close();			
			
			//Tempo de Entrega
			String sqlTempo = "select sum(dat_retorno-dat_envio) qtd from tsmi_controle_notificacoes";
			sqlTempo += " where cod_orgao_autuacao = :1 and dat_retorno = :2";
			PreparedStatement stmTempo = conn.prepareStatement(sqlTempo);
			stmTempo.setInt(1, codOrgao);
			stmTempo.setDate(2, datProc);
			ResultSet rsTempo = stmTempo.executeQuery();
			if (rsTempo.next())
				bean.setQtdTempo(new Integer(rsTempo.getInt("qtd")));
			stmTempo.close();
			
			if ( (bean.getQtdRegistrado().intValue() > 0) ||
					(bean.getQtdRecebido().intValue() > 0) ||
					(bean.getQtdEntregue().intValue() > 0) ||
					(bean.getQtdPendente().intValue() > 0) )
				retorno = true;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public void CarregaArquivoDetalhe(LinhaArquivoRec LinhaArquivoRecId, String codArq, String codOrgao, boolean bArqRecebidoBKP) 
	throws DaoException {		
		Connection conn = null ;
		List listaArquivo = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;

			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";

			Statement stmt  = conn.createStatement();			
			String sCmd = "SELECT I.NUM_AUTO_INFRACAO, I.NUM_PLACA, TO_CHAR(I.DAT_INFRACAO, 'dd/mm/yyyy') AS DAT_INFRACAO, I.VAL_HOR_INFRACAO, I.COD_MUN_CONDUTOR, I.COD_INFRACAO"; 
			sCmd += " FROM TSMI_LINHA_ARQUIVO_REC A, TSMI_AUTO_INFRACAO I ";
			sCmd += " WHERE A.COD_ARQUIVO = '" + codArq + "' AND A.NUM_AUTO_INFRACAO = I.NUM_AUTO_INFRACAO";
			/*"' AND A.COD_ORGAO = '" + codOrgao +*/
			if (sHistorico.equals("S")) 
			{
				sCmd += " UNION "+
				"SELECT I.NUM_AUTO_INFRACAO, I.NUM_PLACA, TO_CHAR(I.DAT_INFRACAO, 'dd/mm/yyyy') AS DAT_INFRACAO, I.VAL_HOR_INFRACAO, I.COD_MUN_CONDUTOR, I.COD_INFRACAO"; 
				sCmd += " FROM TSMI_LINHA_ARQUIVO_REC_BKP@SMITPBKP A, TSMI_AUTO_INFRACAO I ";
				sCmd += " WHERE A.COD_ARQUIVO = '" + codArq + "' AND A.NUM_AUTO_INFRACAO = I.NUM_AUTO_INFRACAO";
				/*"' AND A.COD_ORGAO = '" + codOrgao +*/
			}
			sCmd += " ORDER BY DAT_INFRACAO";
			ResultSet rs   = stmt.executeQuery(sCmd) ;            
			while (rs.next()) {
				LinhaArquivoRec auxClasse = new LinhaArquivoRec();
				auxClasse.setNumAutoInfracao((rs.getString("NUM_AUTO_INFRACAO")));
				auxClasse.setNumPlaca((rs.getString("NUM_PLACA")));
				auxClasse.setDataInfracao((rs.getString("DAT_INFRACAO")));
				auxClasse.setHoraInfracao((rs.getString("VAL_HOR_INFRACAO")));
				auxClasse.setCodMunicipio((rs.getString("COD_MUN_CONDUTOR")));
				auxClasse.setCodInfracao((rs.getString("COD_INFRACAO")));
				listaArquivo.add(auxClasse);					        
			}
			LinhaArquivoRecId.setListaArquivos(listaArquivo);
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void LoteUpdateDol(LoteDolBean auxClasse,String Origem, String qtd) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;
		String canc ="";
		String grav ="";
		
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			
			String  sCmd = "SELECT QTD_CANC,QTD_GRAV FROM TSMI_LOTE_DOL WHERE COD_LOTE = "+auxClasse.getCodLote();					  			       					
			ResultSet	rs    = stmt.executeQuery(sCmd) ;	  
			if (rs.next()){
				canc =rs.getString("QTD_CANC");
				grav = rs.getString("QTD_GRAV");
				if(canc == null) canc = "0";
				if(grav == null) grav = "0";
			}
			
			sCmd  = "UPDATE TSMI_LOTE_DOL SET";
			sCmd  += " COD_STATUS = " + auxClasse.getCodStatus() + ",";
			if("E".equals(Origem)){
				sCmd  +=" QTD_GRAV = '" + (Integer.parseInt(qtd) + Integer.parseInt(grav))+"'";
			}else{					   
				sCmd  +=" QTD_CANC = '" + (Integer.parseInt(qtd) + Integer.parseInt(canc))+"'";
			}
			sCmd  +=" WHERE COD_LOTE = " + auxClasse.getCodLote();
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	public void LinhaLoteUpdateDol(LinhaLoteDolBean auxClasse,String Origem) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;        
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TSMI_LINHA_LOTE SET";
			sCmd += " COD_STATUS = " + auxClasse.getCodStatus() + ",";
			sCmd += " COD_RETORNO = '"+ auxClasse.getCodRetorno() + "',";
			sCmd += " DSC_LINHA_LOTE_RETORNO = '"+ auxClasse.getDscLinhaLoteRetorno() + "'";
			if ("C".equals(Origem)){
				sCmd += " ,SIT_CANC_DOL = 'S'";
			}
			sCmd += " WHERE COD_LINHA_LOTE = "+ auxClasse.getCodLinhaLote();				   
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}	
	
	
	public void LoteUpdateDol(LoteDolBean auxClasse,String Origem) throws DaoException {		
		Connection conn = null;
		boolean eConnLocal = false;
		String canc ="";
		String grav ="";
		String erro ="";
		
		try {
			conn = auxClasse.getConn();
			String sCmd="";
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}
			Statement stmt  = conn.createStatement();
			
			sCmd =  "select count(*) as qtd_canc from tsmi_linha_lote WHERE COD_LOTE = '"+auxClasse.getCodLote()+"'";					  			       					
			sCmd += " and cod_retorno='000' and sit_canc_dol is not null";
			ResultSet	rs    = stmt.executeQuery(sCmd) ;	  
			if (rs.next()){
				canc =rs.getString("QTD_CANC");				
				if(canc == null) canc = "0";
				
			}
			sCmd =  "select count(*) as qtd_grav from tsmi_linha_lote WHERE COD_LOTE = '"+auxClasse.getCodLote()+"'";					  			       					
			sCmd += " and cod_retorno='000' and sit_canc_dol is null";
			rs    = stmt.executeQuery(sCmd) ;	  
			if (rs.next()){
				grav = rs.getString("QTD_GRAV");
				if(grav == null) grav = "0";				
			}
			sCmd =  "select count(*) as qtd_erro from tsmi_linha_lote WHERE COD_LOTE = '"+auxClasse.getCodLote()+"'";					  			       					
			sCmd += " and cod_retorno<>'000'";
			rs    = stmt.executeQuery(sCmd) ;	  
			if (rs.next()){
				erro = rs.getString("QTD_ERRO");
				if(erro == null) erro = "0";				
			}
			
			int iQtdReg   =Integer.parseInt(grav)+Integer.parseInt(canc)+Integer.parseInt(erro);
			String sQtdReg=Integer.toString(iQtdReg);
			
			sCmd  = "UPDATE TSMI_LOTE_DOL SET";
			sCmd  +=" COD_STATUS = " + auxClasse.getCodStatus() + ",";
			sCmd  +=" QTD_REG  = '" + sQtdReg +"',";
			sCmd  +=" QTD_GRAV = '" + grav +"',";
			sCmd  +=" QTD_CANC = '" + canc+"',";
			sCmd  +=" QTD_LINHA_ERRO = '" + erro+"'";
			sCmd  +=" WHERE COD_LOTE = '" + auxClasse.getCodLote()+"'";
			stmt.execute(sCmd);
			auxClasse.setMsgErro("") ;
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a Estat�stica de Digitalizados
	 *-----------------------------------------------------------
	 */
	
	public void getRegistrados(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException {		
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();			
			String sCmd = "select a.cod_orgao COD_ORGAO, to_char(sum(a.qtd_reg)) QTD_MR01,'' QTD_SMIT_DOL "+
			"from smit.tsmi_arquivo_recebido a  "+
			"where a.cod_ident_arquivo = 'MR01' "+ 
			"and  a.dat_recebimento between '"+EstatisticaBean.getDatIncio()+"' and '"+EstatisticaBean.getDatFim()+"'"; 
			if(EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd+="and  a.cod_orgao = "+EstatisticaBean.getCodOrgao();
			sCmd += "group by a.cod_orgao union "+
			"select b.cod_orgao COD_ORGAO,'', to_char(sum(c.qtd_reg)) QTD_SMIT_DOL "+
			"from tsmi_arquivo_dol b, tsmi_lote_dol c "+
			"where b.cod_arquivo_dol = c.cod_arquivo_dol "+
			"and b.nom_username not in ('PSGDSC','PSLIGI') "+
			"and b.dat_recebimento between '"+EstatisticaBean.getDatIncio()+"' and '"+EstatisticaBean.getDatFim()+"' ";
			if(EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd+= "and b.cod_orgao = "+EstatisticaBean.getCodOrgao();
			sCmd += " group by b.cod_orgao ";
			
			ResultSet rs   = stmt.executeQuery(sCmd) ;      
			int QtdMR01 = 0;
			int QtdDOL = 0;
			while (rs.next()) {
				QtdMR01 += rs.getInt("QTD_MR01");
				QtdDOL  += rs.getInt("QTD_SMIT_DOL");
			}
			EstatisticaBean.setQtdMR01(QtdMR01);
			EstatisticaBean.setQtdDOL(QtdDOL);
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {                    
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getEmitidos(EstatisticaDigitalizaBean EstatisticaBean) throws Exception{
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			
			String sCmd = "select /*+rule*/  qtd_reg, "
				+ " (select count(*) from tsmi_linha_arquivo_rec l where l.cod_arquivo = a.cod_arquivo"
				+ " and l.cod_status = 9  ";
			
			if (EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd +=" and l.ind_fase in (0,1) ";
			else if (EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd +=" and l.ind_fase in (2,3) ";
			
			if (EstatisticaBean.getCodOrgao().length()>0)
				sCmd +=" and l.cod_orgao in "+EstatisticaBean.getCodOrgao();
			
			sCmd +=") as qtd_linha_pendente "
				+ " from tsmi_arquivo_recebido a"
				+ " where cod_ident_arquivo ='EMITEVEX'  and cod_arquivo in"
				+ " (select l.cod_arquivo from tsmi_linha_arquivo_rec l where l.cod_arquivo ="
				+ " a.cod_arquivo and l.cod_status = 9 "; 
			
			if (EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd +=" and l.ind_fase in (0,1) ";
			else if (EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd +=" and l.ind_fase in (2,3) ";
			
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd +=" and l.cod_orgao in "+EstatisticaBean.getCodOrgao();
			sCmd +=")";
			
			int QtdPendencia = 0;
			int QtdEmitidos = 0;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdPendencia += rs.getInt("qtd_linha_pendente");
				QtdEmitidos += rs.getInt("qtd_reg");
				
			}	
			EstatisticaBean.setQtdPendencia(QtdPendencia);
			EstatisticaBean.setQtdEmitidos(QtdEmitidos-QtdPendencia);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	public void getDigitalizados(EstatisticaDigitalizaBean EstatisticaBean) throws Exception{
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();			
			
			int QtdDigi = 0;
			int QtdNaoDigi = 0;
			
			//Verifica quantidade Digitalizados
			String sCmd = "Select * from tsmi_auto_infracao where sit_ai_digitalizado = '1'";
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd += " and cod_orgao = "+EstatisticaBean.getCodOrgao();
			if(EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd += " and cod_Status = 5";
			else if(EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd += " and cod_Status = 15";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdDigi ++;
			}	
			
			//Verifica quantidade de n�o digitalizados
			sCmd = "Select * from tsmi_auto_infracao where sit_ai_digitalizado is null";
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd += " and cod_orgao = "+EstatisticaBean.getCodOrgao();
			if(EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd += " and cod_Status = 5";
			else if(EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd += " and cod_Status = 15";
			
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdNaoDigi ++;
			}	
			EstatisticaBean.setQtdDigitalizados(QtdDigi);
			EstatisticaBean.setQtdNaoDigitalizados(QtdNaoDigi);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getArEntregues(EstatisticaDigitalizaBean EstatisticaBean) throws Exception{
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();			
			
			int QtdArRecebido = 0;
			int QtdArTotal = 0;
			
			String sCmd = "SELECT /*+RULE*/ TAR.Nom_Arquivo,DECODE(TAR.Cod_Status, 1,'R', 2,'P', 7,'Em P', 8,'E') Cod_status,"
				+ " TAR.Qtd_Reg, TAR.Num_Protocolo, (SELECT  /*+NO_INDEX(TSMI_LINHA_ARQUIVO_REC)*/ COUNT(*) FROM TSMI_LINHA_ARQUIVO_REC TLN"
				+ " WHERE TLN.Cod_Status=1 AND TLN.Cod_Retorno='000' AND TLN.Cod_Arquivo = TAR.Cod_Arquivo" ;
			
			if (EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd +=" and TLN.ind_fase in (0,1) ";
			else if (EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd +=" and TLN.ind_fase in (2,3) ";
			
			sCmd += ") AS Qtd_Recebido," 
				+ " TO_CHAR(TAR.Dat_Recebimento,'DD/MM/YYYY') Dat_Recebimento,TAR.Nom_Username as nomUserName,TTA.SIG_ORGAO AS codOrgaoLotacao"
				+ " FROM TSMI_ORGAO TTA JOIN TSMI_ARQUIVO_RECEBIDO TAR ON TAR.COD_ORGAO_LOTACAO = TTA.COD_ORGAO "
				+ " WHERE TAR.Cod_Ident_Arquivo = 'CODRET' "
				+ " AND TAR.Dat_Recebimento >='"+ EstatisticaBean.getDatIncio() +"' AND " + "TAR.Dat_Recebimento <='" + EstatisticaBean.getDatFim() +"'"
				+ " AND TAR.Cod_Status <> 9 ";
			
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd += " and TAR.cod_orgao = "+EstatisticaBean.getCodOrgao();
			sCmd += " ORDER BY TAR.Nom_Arquivo";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdArRecebido += rs.getInt("QTD_RECEBIDO");
				QtdArTotal += rs.getInt("QTD_REG");
			}	
			EstatisticaBean.setQtdArRecebido(QtdArRecebido);
			EstatisticaBean.setQtdArNaoRecebido(QtdArTotal-QtdArRecebido);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void getArDigitalizados(EstatisticaDigitalizaBean EstatisticaBean) throws Exception{
		Connection conn = null;
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();			
			
			int QtdDigi = 0;
			int QtdNaoDigi = 0;
			
			//Verifica quantidade Digitalizados
			String sCmd = "Select * from tsmi_auto_infracao where sit_ar_digitalizado = '1'";
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd += " and cod_orgao = "+EstatisticaBean.getCodOrgao();
			if(EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd += " and cod_Status = 5";
			else if(EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd += " and cod_Status = 15";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdDigi ++;
			}	
			
			//Verifica quantidade de n�o digitalizados
			sCmd = "Select * from tsmi_auto_infracao where sit_ar_digitalizado is null";
			if (EstatisticaBean.getTodosOrgaos().length()<=0)
				sCmd += " and cod_orgao = "+EstatisticaBean.getCodOrgao();
			if(EstatisticaBean.getTipoSolicitacao().equals("autuacao"))
				sCmd += " and cod_Status = 5";
			else if(EstatisticaBean.getTipoSolicitacao().equals("penalidade"))
				sCmd += " and cod_Status = 15";
			
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				QtdNaoDigi ++;
			}	
			EstatisticaBean.setQtdArDigitalizados(QtdDigi);
			EstatisticaBean.setQtdArNaoDigitalizados(QtdNaoDigi);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public String ApagaBackupDol(LoteDolBean auxClasse)
	throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			String sCmd="";
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();			
			ResultSet rs    = null;
			String codlote="";
			boolean temlinha = false;			
			sCmd  = "select cod_lote,cod_arquivo_dol from tsmi_lote_dol_bkp where cod_lote_dol="+auxClasse.getCodLoteDol();
			rs    = stmt.executeQuery(sCmd) ;
			if(rs.next()){
				auxClasse.setCodArquivoDol(rs.getString("cod_arquivo_dol"));
				codlote=rs.getString("cod_lote");
				temlinha=true;	
			}
			if(temlinha){
				sCmd  = "delete tsmi_linha_lote_bkp where cod_lote='"+codlote+"'";
				rs    = stmt.executeQuery(sCmd) ;
				sCmd  = "delete tsmi_lote_dol_bkp where cod_lote='"+codlote+"'";
				rs    = stmt.executeQuery(sCmd) ;					
			}	
			existe=true;
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if( existe == false )
						conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return auxClasse.getCodArquivoDol();
	}
	
	public Vector BuscaLotesDolBean(ConsultaArquivoDOLBean myLote)		
	throws DaoException {
		Connection conn = null;
		Vector lotes = new Vector();
		String cod_arquivo="";
		String ind_p59="";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT COD_ARQUIVO_DOL, IND_P59"+
			" FROM TSMI_ARQUIVO_DOL" +  
			" WHERE NOM_ARQUIVO='" + myLote.getNomArquivo()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				ind_p59=rs.getString("IND_P59");				
				cod_arquivo=rs.getString("COD_ARQUIVO_DOL");				
			}
			sCmd ="SELECT COD_LOTE,NUM_ANO_EXERCICIO,DAT_LOTE,NUM_LOTE,QTD_REG,COD_UNIDADE"+
			" FROM TSMI_LOTE_DOL" +  
			" WHERE COD_ARQUIVO_DOL=" + cod_arquivo;
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				LoteDolBean lote = new LoteDolBean();
				SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				String datalote=df.format(sdf.parse(rs.getString("DAT_LOTE"))); 
				lote.setCodLote(rs.getString("COD_LOTE"));
				lote.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));				 
				lote.setDatLote(datalote);				
				lote.setNumLote(rs.getString("NUM_LOTE"));
				lote.setQtdReg(rs.getString("QTD_REG"));
				lote.setCodUnidade(rs.getString("COD_UNIDADE"));
				String codlote = lote.getCodLote();
				String[] campos={"COD_LOTE"};
				String[] valores={codlote};		
				lote.setLinhaDol(BuscaLinhaLoteDOL(campos, valores,"",myLote.getNomConsulta()));
				
				Vector vetLinhas = new Vector();
			    Vector vetLinha = (Vector) lote.getLinhaDol(); 
			    int ocorAuto=lote.getLinhaDol().size();			   
			    for (int j=0; j<ocorAuto; j++) {			    	
			    	REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();      		
		      		LinhaBean=(REG.LinhaLoteDolBean)vetLinha.get(j);
		      		LinhaBean.setInd_P59(ind_p59);
		      		
		      		vetLinhas.add(LinhaBean);	
			    }
			    lote.setLinhaDol(vetLinhas);
			    
				lotes.addElement(lote); 
			}
			myLote.setVetConsulta(lotes);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return lotes;
	}
	
	public boolean ArquivoExisteDolbkp(ArquivoDolBean auxClasse) throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT COD_ARQUIVO_DOL from TSMI_ARQUIVO_DOL_BKP "+
			"WHERE nom_arquivo = '" + auxClasse.getNomArquivo() + "'" +
			" and cod_status <> '9'" ;
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				existe = true ;
			}
			stmt.close();
			rs.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo ao Controle de Digitaliza��o de AR
	 * @author Jos� Fernando
	 *-----------------------------------------------------------
	 */	
	
	public void atualizaDigitalizacao(NotifControleBean bean) throws DaoException {
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TSMI_CONTROLE_NOTIFICACOES SET " +
			" DAT_DIGITALIZACAO = TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'), " +
			" SIT_DIGITALIZACAO = '"+ bean.getSitDigitaliza()+"'"+
			" WHERE NUM_NOTIFICACAO = " + bean.getNumNotificacao()+
			" AND SIT_DIGITALIZACAO = 'N'";
			
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public void atualizaCodigoBarra(NotifControleBean myNotif) throws DaoException {
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TSMI_CONTROLE_NOTIFICACOES SET " +
			" DAT_GERACAO_CB_AI = TO_DATE('" + df.format(myNotif.getDatRegistroCB()) + "', 'DD/MM/YYYY'), " +
			" SEQ_GERACAO_CB_AI = "+myNotif.getSeqRegistroCB()+
			" WHERE NUM_NOTIFICACAO = " + myNotif.getNumNotificacao();
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	
	public List consultaARDigitPendenciaMes(NotifControleBean notifControle, int prazo, String[] orgaos, String sitEntrega) 
	throws DaoException {
		Connection conn = null;    
		List totalAnosMeses = new ArrayList();
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String filtroData = "DAT_ENTREGA";
			String filtroStatus = " (8,9) ";
			
			String sCmd = "SELECT COUNT(*) TOTAL, " +
			" TO_CHAR(" + filtroData + ", 'YYYY') ANO, " +
			" TO_CHAR(" + filtroData + ", 'MM') MES " +
			" FROM TSMI_CONTROLE_NOTIFICACOES ";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!notifControle.getCodStatusAuto().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				else
					sCmd += " AND COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				flag = false;	
			}
			
			if (orgaos.length > 1) 
			{
				String orgaosWhere = "(";
				for (int i = 0; i < orgaos.length; i++) 
				{
					if (i == orgaos.length - 1)
						orgaosWhere += orgaos[i] + ")";
					else
						orgaosWhere += orgaos[i] + ",";
				}				
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				else
					sCmd += " AND COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				flag = false;				
			} 
			else 
			{
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO = "+orgaos[0];
				else
					sCmd += " AND COD_ORGAO_AUTUACAO = "+orgaos[0];				
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE ";
			else
				sCmd += " AND ";
			sCmd += filtroData + " < SYSDATE - " + prazo + " AND SIT_DIGITALIZACAO = 'N' " +
			" AND COD_STATUS_NOTIF IN " + filtroStatus;
			
			sCmd += " GROUP BY TO_CHAR(" + filtroData + ", 'YYYY'), TO_CHAR(" + filtroData + ", 'MM') ";
			sCmd += " ORDER BY ANO, MES ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next())
				totalAnosMeses.add(rs.getString("MES").concat(" ").concat(rs.getString("ANO")).concat(" ").concat(rs.getString("TOTAL")));	
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return totalAnosMeses;
		
	}
	
	public List consultaARDigitPendenciaDet(NotifControleBean notifControle, int prazo, String totalMesAno, 
			String rowNum, HashMap proxAnt, String[] orgaos, String sitEntrega, String rowNumImp ) throws DaoException {
		Connection conn = null;    
		List notificacoes = new ArrayList();
		int totreg = 0;
		boolean flag = true;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String filtroData = "DAT_ENTREGA";
			String filtroStatus = " (8,9) ";
			
			String sCmd = "SELECT /*+INDEX(TSMI_CONTROLE_NOTIFICACOES IDX_1_TSMI_CTL_NOTIFICACOES)*/ " +
			" NUM_NOTIFICACAO, NUM_AUTO_INFRACAO, NUM_PLACA, " +
			" COD_ARQUIVO_ENVIO, COD_ORGAO_ENVIO," +
			" DAT_EMISSAO, DAT_ENVIO, DAT_ENTREGA, " +
			" DAT_RECEB, NOM_USERNAME_ENVIO, " +
			" TO_CHAR(" + filtroData + ", 'YYYY'),  " +
			" TO_CHAR(" + filtroData + ", 'MM') " +			
			" FROM TSMI_CONTROLE_NOTIFICACOES ";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!notifControle.getCodStatusAuto().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				else
					sCmd += " AND COD_STATUS = '"+notifControle.getCodStatusAuto()+"'";
				flag = false;	
			}
			
			if (orgaos.length > 1) {
				String orgaosWhere = "(";
				for (int i = 0; i < orgaos.length; i++) {
					if (i == orgaos.length - 1)
						orgaosWhere += orgaos[i] + ")";
					else
						orgaosWhere += orgaos[i] + ",";
				}				
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				else
					sCmd += " AND COD_ORGAO_AUTUACAO IN "+orgaosWhere;
				flag = false;
				
			} else {
				if(flag)
					sCmd += " WHERE COD_ORGAO_AUTUACAO = "+orgaos[0];
				else
					sCmd += " AND COD_ORGAO_AUTUACAO = "+orgaos[0];
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE ";
			else
				sCmd += " AND ";			
			sCmd += " TO_CHAR(" + filtroData + ", 'MM') = '" + totalMesAno.substring(0, 2) + "' AND COD_STATUS_NOTIF IN " 
			+ filtroStatus;
			
			if (!proxAnt.get("primeiroRegistro").equals(""))
			{
				if (proxAnt.get("sentido").equals("Ant"))
				{
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("primeiroRegistro") + "'";
				}
				else if (proxAnt.get("sentido").equals("Prox"))                                         
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				else if (proxAnt.get("sentido").equals("Imprimir")) {
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("primeiroRegistro") + "'";
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				}
			}
			
			sCmd += " AND SIT_DIGITALIZACAO = 'N' AND TO_CHAR(" + filtroData + ", 'YYYY') = '" + totalMesAno.substring(3, 7) + "'";
			sCmd += " ORDER BY NUM_NOTIFICACAO ";
			ResultSet rs = stmt.executeQuery(sCmd);
			NotifControleBean notificacao = null;
			
			while(rs.next() && (rowNumImp.equals("-1") || totreg < Integer.parseInt(rowNum)))			
			{
				notificacao = new NotifControleBean();           	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));          	      	           	           	
				notificacao.setNomUsuarioEnvio(rs.getString("NOM_USERNAME_ENVIO"));            	           	
				notificacao.setDatReceb(rs.getDate("DAT_RECEB"));   
				notificacao.setDatEmissao((rs.getDate("DAT_EMISSAO")));
				notificacao.setDatEnvio((rs.getDate("DAT_ENVIO")));
				notificacao.setDatEntrega((rs.getDate("DAT_ENTREGA")));
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO_ENVIO")); 
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO_ENVIO"));
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
				totreg++;
				
			}
			notifControle.setListaNotifs(notificacoes);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
		return notificacoes;
	}	
	
	public List consultaAIDigitPendenciaMes(REC.AutoInfracaoBean autoInfracao, int prazo, String orgao) 
	throws DaoException {
		Connection conn = null;    
		List totalAnosMeses = new ArrayList();
		boolean flag = true;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String sCmd = "SELECT COUNT(*) TOTAL, " +
			" TO_CHAR( DAT_REGISTRO, 'YYYY') ANO, " +		
			" TO_CHAR( DAT_REGISTRO, 'MM') MES " +
			" FROM TSMI_AUTO_INFRACAO ";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!autoInfracao.getCodStatus().equals(""))
			{
				if(flag)
					sCmd += " WHERE COD_STATUS = '" + autoInfracao.getCodStatus() + "'";
				else
					sCmd += " AND COD_STATUS = '" + autoInfracao.getCodStatus() + "'";
				flag = false;	
			}
			
			if(! orgao.equals("")){				
				if(flag)
					sCmd += " WHERE COD_ORGAO = " + orgao;
				else
					sCmd += " AND COD_ORGAO = " + orgao;				
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE ";
			else
				sCmd += " AND ";
			sCmd += " DAT_REGISTRO < SYSDATE - " + prazo + " AND SIT_DIGITALIZACAO = 'N' " ;
			sCmd += " GROUP BY TO_CHAR( DAT_REGISTRO, 'YYYY'), TO_CHAR( DAT_REGISTRO, 'MM') ";
			sCmd += " ORDER BY ANO, MES ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while(rs.next())
				totalAnosMeses.add(rs.getString("MES").concat(" ").concat(rs.getString("ANO")).concat(" ").concat(rs.getString("TOTAL")));	
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return totalAnosMeses;		
	}
	
	public List consultaAIDigitPendenciaDet(REC.AutoInfracaoBean autoInfracao, int prazo, String totalMesAno, 
			String rowNum, HashMap proxAnt, String orgao, String rowNumImp) throws DaoException {
		Connection conn = null;    
		ArrayList infracoes = new ArrayList();
		int totreg = 0;
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String sCmd = "SELECT NUM_AUTO_INFRACAO, NUM_PLACA, DAT_REGISTRO, DAT_INFRACAO, " +
			" DAT_PAGAMENTO, DAT_STATUS, DAT_ULT_AFER_APARELHO, DAT_VENCIMENTO, COD_ORGAO, " +
			" S.COD_STATUS , S.NOM_STATUS " +
			" FROM TSMI_AUTO_INFRACAO T LEFT JOIN TSMI_STATUS_AUTO S " +
			" ON (T.COD_STATUS = S.COD_STATUS) ";
			
			//Bloco de filtros para montar as cl�sulas de filtros da instru��o SQL.
			if(!autoInfracao.getCodStatus().equals("")){
				if(flag)
					sCmd += " WHERE COD_STATUS = '"+autoInfracao.getCodStatus()+"'";
				else
					sCmd += " AND COD_STATUS = '"+autoInfracao.getCodStatus()+"'";
				flag = false;	
			}
			
			if(! orgao.equals("")){
				if(flag)
					sCmd += " WHERE COD_ORGAO = " + orgao;
				else
					sCmd += " AND COD_ORGAO = "  + orgao;
				flag = false;
			}
			
			if(flag)
				sCmd += " WHERE ";
			else
				sCmd += " AND ";
			sCmd += " TO_CHAR( DAT_REGISTRO, 'MM') = '" + totalMesAno.substring(0, 2) + "' " ;
			
			
			if (!proxAnt.get("primeiroRegistro").equals(""))
			{
				if (proxAnt.get("sentido").equals("Ant"))
					sCmd += " AND NUM_AUTO_INFRACAO < '" + (String)proxAnt.get("primeiroRegistro") + "'";
				else if (proxAnt.get("sentido").equals("Prox"))                                         
					sCmd += " AND NUM_AUTO_INFRACAO > '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				else if (proxAnt.get("sentido").equals("Imprimir")) {
					sCmd += " AND NUM_AUTO_INFRACAO > '" + (String)proxAnt.get("primeiroRegistro") + "'";
					sCmd += " AND NUM_AUTO_INFRACAO < '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				}
			}
			
			sCmd += " AND TO_CHAR( DAT_REGISTRO, 'YYYY') = '" + totalMesAno.substring(3, 7) + "'";
			sCmd += " AND TRUNC( DAT_REGISTRO ) < (TRUNC(SYSDATE) - " + prazo + ") AND SIT_DIGITALIZACAO = 'N' ";
			sCmd += " ORDER BY NUM_AUTO_INFRACAO ";
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			
			ResultSet rs = stmt.executeQuery(sCmd);
			REC.AutoInfracaoBean myAutoInfracao = null;
			
			while(rs.next() && (rowNumImp.equals("-1") || totreg < Integer.parseInt(rowNum)))			
			{
				myAutoInfracao = new REC.AutoInfracaoBean();
				myAutoInfracao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				myAutoInfracao.setNumPlaca(rs.getString("NUM_PLACA"));
				myAutoInfracao.setNomStatus(rs.getString("NOM_STATUS"));
				myAutoInfracao.setCodStatus(rs.getString("COD_STATUS"));
				
				if(rs.getDate("DAT_REGISTRO")!=null)
					myAutoInfracao.setDatRegistro(sdf.format(rs.getDate("DAT_REGISTRO")));
				if(rs.getDate("DAT_INFRACAO")!=null)
					myAutoInfracao.setDatInfracao(sdf.format(rs.getDate("DAT_INFRACAO")));
				
				myAutoInfracao.setCodOrgao(rs.getString("COD_ORGAO"));
				ACSS.OrgaoBean myOrgao = new ACSS.OrgaoBean();				
				myOrgao.Le_Orgao(rs.getString("COD_ORGAO"),0);				
				myAutoInfracao.setOrgao(myOrgao);
				
				myAutoInfracao.setCodStatus(rs.getString("COD_STATUS"));
				
				infracoes.add(myAutoInfracao);
				totreg++;
			}
			autoInfracao.setAutos(infracoes);
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		
		return infracoes;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a ARDigitAcompanhamentoBean
	 * @author Pedro N�brega
	 *-----------------------------------------------------------
	 */
	
	public boolean consultaARDigitAcompanhamento(ARDigitAcompanhamentoBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			//Verifica se vai montar as querys para todos os org�os ou um s�
			boolean todosOrgaos = false;
			String codOrgao = "";
			String codSistema = "";
			String codUsuario = "";
			if ((bean.getOrgao().getCodOrgao() == null) || bean.getOrgao().getCodOrgao().equals("")) {				
				codSistema = bean.getSistema().getCodSistema();
				codUsuario = bean.getUsuario().getCodUsuario();
				todosOrgaos = true;
			}
			else
				codOrgao = bean.getOrgao().getCodOrgao();
			
			String sql = "select sum(d.qtd_digitalizado) qtd_digitalizado, sum(d.qtd_recebido) qtd_recebido,";
			sql += " sum(d.qtd_entregue) qtd_entregue, sum(d.qtd_tempo_medio) qtd_tempo, sum(d.qtd_pendente) qtd_pendentes,";
			sql += " d.dat_proc from tsmi_digit_notificacoes d";
			if (todosOrgaos) {
				sql += ", tcau_perfil p, tcau_perfil_orgao o ";
				sql += " where o.cod_perfil = p.cod_perfil and p.cod_sistema = "+codSistema+" and o.cod_usuario = "+codUsuario;
				sql += " and d.cod_orgao = o.cod_orgao_atuacao";
			} else
				sql += " where d.cod_orgao = "+codOrgao;
			sql += " and d.dat_proc between :1 and :2 group by d.dat_proc order by d.dat_proc";
			PreparedStatement stm = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stm.setDate(1, new java.sql.Date(bean.getDatInicial().getTime()));
			stm.setDate(2, new java.sql.Date(bean.getDatFinal().getTime()));
			ResultSet rs = stm.executeQuery();
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(bean.getDatInicial());
			Date datAtual = cal.getTime();
			
			int totRecebido = 0;
			int totEntregue = 0;
			int totDigitalizado = 0;
			int totTempo = 0;
			int totPendentes = 0;
			
			List beans = new ArrayList();
			while(datAtual.compareTo(bean.getDatFinal()) <= 0) {
				ARDigitAcompanhamentoBean item = new ARDigitAcompanhamentoBean();				
				item.setDatProc(datAtual);				
				
				if (rs.next()) {
					Date data = rs.getDate("dat_proc");
					if (data.equals(item.getDatProc())) {
						
						item.setQtdRecebido(new Integer(rs.getInt("qtd_recebido")));
						totRecebido += rs.getInt("qtd_recebido");
						
						item.setQtdEntregue(new Integer(rs.getInt("qtd_entregue")));
						totEntregue += rs.getInt("qtd_entregue");
						
						item.setQtdDigitalizado(new Integer(rs.getInt("qtd_digitalizado")));
						totDigitalizado += rs.getInt("qtd_digitalizado");
						
						item.setQtdPendentes(new Integer(rs.getInt("qtd_pendentes")));
						totPendentes += rs.getInt("qtd_pendentes");
						
						item.setQtdTempo(new Integer(rs.getInt("qtd_tempo")));
						totTempo += rs.getInt("qtd_tempo");
						
					} else
						rs.previous();
				}
				
				beans.add(item);
				
				cal.add(Calendar.DAY_OF_YEAR, 1);
				datAtual = cal.getTime();
			}
			bean.setBeans(beans);
			
			bean.setTotalQtdRecebido(new Integer(totRecebido));
			bean.setTotalQtdTempo(new Integer(totTempo));
			bean.setTotalQtdEntregue(new Integer(totEntregue));
			bean.setTotalQtdPendentes(new Integer(totPendentes));
			
			retorno = true;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public boolean totalizaARDigitAcompanhamento(ARDigitAcompanhamentoBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			int codOrgao = Integer.parseInt(bean.getOrgao().getCodOrgao());
			java.sql.Date datProc = new java.sql.Date(bean.getDatProc().getTime());			
			
			//Emitidos
			String sqlRecebido = "select /*+rule*/ count(*) qtd from tsmi_controle_notificacoes";
			sqlRecebido += " where cod_orgao_autuacao = :1 and dat_envio = :2";
			PreparedStatement stmRecebido = conn.prepareStatement(sqlRecebido);
			stmRecebido.setInt(1, codOrgao);
			stmRecebido.setDate(2, datProc);
			ResultSet rsRecebido = stmRecebido.executeQuery();
			if (rsRecebido.next())
				bean.setQtdRecebido(new Integer(rsRecebido.getInt("qtd")));
			stmRecebido.close();
			
			//Retornados
			String sqlEntregue = "select /*+rule*/ count(*) qtd from tsmi_controle_notificacoes";
			sqlEntregue += " where cod_orgao_autuacao = :1 and dat_retorno = :2";
			PreparedStatement stmEntregue = conn.prepareStatement(sqlEntregue);
			stmEntregue.setInt(1, codOrgao);
			stmEntregue.setDate(2, datProc);
			ResultSet rsEntregue = stmEntregue.executeQuery();
			if (rsEntregue.next())
				bean.setQtdEntregue(new Integer(rsEntregue.getInt("qtd")));
			stmEntregue.close();
			
			//Digitalizados
			String sqlDigitalizado = "select /*+rule*/ count(*) qtd from tsmi_controle_notificacoes";
			sqlDigitalizado += " where cod_orgao_autuacao = :1 and dat_digitalizacao = :2";
			PreparedStatement stmDigitalizado = conn.prepareStatement(sqlDigitalizado);
			stmDigitalizado.setInt(1, codOrgao);
			stmDigitalizado.setDate(2, datProc);
			ResultSet rsDigitalizado = stmDigitalizado.executeQuery();
			if (rsDigitalizado.next())
				bean.setQtdDigitalizado(new Integer(rsDigitalizado.getInt("qtd")));
			stmDigitalizado.close();
			
			// Tempo M�dio
			String sqlTempo = "select /*+rule*/ sum(dat_digitalizacao-dat_retorno) qtd from tsmi_controle_notificacoes";
			sqlTempo += " where cod_orgao_autuacao = :1 and dat_digitalizacao = :2";
			PreparedStatement stmTempo = conn.prepareStatement(sqlTempo);
			stmTempo.setInt(1, codOrgao);
			stmTempo.setDate(2, datProc);
			ResultSet rsTempo = stmTempo.executeQuery();
			if (rsTempo.next())
				bean.setQtdTempo(new Integer(rsTempo.getInt("qtd")));
			stmTempo.close();			
			
			//Pendentes 
			String sqlPendentes = "select /*+rule*/ count(*) qtd from tsmi_controle_notificacoes";
			sqlPendentes += " where cod_orgao_autuacao = :1 and (dat_digitalizacao > :2" +
			" or sit_digitalizacao = 'N') and dat_retorno <= :2";
			PreparedStatement stmPendentes = conn.prepareStatement(sqlPendentes);
			stmPendentes.setInt(1, codOrgao);
			stmPendentes.setDate(2, datProc);
			ResultSet rsEntreguePend = stmPendentes.executeQuery();
			if (rsEntreguePend.next())
				bean.setQtdPendentes(new Integer(rsEntreguePend.getInt("qtd")));
			stmPendentes.close();						
			
			if ((bean.getQtdDigitalizado().intValue() > 0) ||
					(bean.getQtdRecebido().intValue() > 0) ||
					(bean.getQtdEntregue().intValue() > 0) ||
					(bean.getQtdPendentes().intValue() > 0))
				retorno = true;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public void gravaARDigitAcompanhamento(ARDigitAcompanhamentoBean bean, Connection conn) throws DaoException {		
		try {
			String sqlSel = "select dat_proc from tsmi_digit_notificacoes where dat_proc = :1 and cod_orgao = :2";
			PreparedStatement stmSel = conn.prepareStatement(sqlSel);
			stmSel.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
			stmSel.setInt(2, Integer.parseInt(bean.getOrgao().getCodOrgao()));
			ResultSet rsSel = stmSel.executeQuery();
			
			if (rsSel.next()) {
				String sqlUpd = "update tsmi_digit_notificacoes set qtd_digitalizado = :1, qtd_recebido = :2,";
				sqlUpd += " qtd_entregue = :3, qtd_tempo_medio = :4, qtd_pendente = :5" ;
				sqlUpd += " where dat_proc = :6 and cod_orgao = :7";
				PreparedStatement stmUpd = conn.prepareStatement(sqlUpd);
				stmUpd.setInt(1, bean.getQtdDigitalizado().intValue());
				stmUpd.setInt(2, bean.getQtdRecebido().intValue());
				stmUpd.setInt(3, bean.getQtdEntregue().intValue());
				stmUpd.setInt(4, bean.getQtdTempo().intValue());
				stmUpd.setInt(5, bean.getQtdPendentes().intValue());
				stmUpd.setDate(6, new java.sql.Date(bean.getDatProc().getTime()));
				stmUpd.setInt(7, Integer.parseInt(bean.getOrgao().getCodOrgao()));				
				stmUpd.executeUpdate();
				stmUpd.close();
				
			} else {
				String sqlIns = "insert into tsmi_digit_notificacoes (dat_proc, qtd_digitalizado, qtd_recebido,";
				sqlIns += " qtd_entregue, qtd_tempo_medio, qtd_pendente, cod_orgao) " +
				"values (:1, :2, :3, :4, :5, :6, :7)";
				PreparedStatement stmIns = conn.prepareStatement(sqlIns);
				stmIns.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
				stmIns.setInt(2, bean.getQtdDigitalizado().intValue());
				stmIns.setInt(3, bean.getQtdRecebido().intValue());
				stmIns.setInt(4, bean.getQtdEntregue().intValue());
				stmIns.setInt(5, bean.getQtdTempo().intValue());
				stmIns.setInt(6, bean.getQtdPendentes().intValue());
				stmIns.setInt(7, Integer.parseInt(bean.getOrgao().getCodOrgao()));
				stmIns.executeUpdate();
				stmIns.close();
			}
			stmSel.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a AIDigitAcompanhamentoBean
	 * @author Pedro N�brega
	 *-----------------------------------------------------------
	 */
	
	public boolean consultaAIDigitAcompanhamento(AIDigitAcompanhamentoBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			//Verifica se vai montar as querys para todos os org�os ou um s�
			boolean todosOrgaos = false;
			String codOrgao = "";
			String codSistema = "";
			String codUsuario = "";
			if ((bean.getOrgao().getCodOrgao() == null) || bean.getOrgao().getCodOrgao().equals("")) {				
				codSistema = bean.getSistema().getCodSistema();
				codUsuario = bean.getUsuario().getCodUsuario();
				todosOrgaos = true;
			}
			else
				codOrgao = bean.getOrgao().getCodOrgao();
			
			String sql = "select sum(d.qtd_digitalizado) qtd_digitalizado,";
			sql += " sum(d.qtd_tempo_medio) qtd_tempo, sum(d.qtd_registrado) qtd_registrado,";
			sql += " d.dat_proc from tsmi_digit_autos d";
			if (todosOrgaos) {
				sql += ", tcau_perfil p, tcau_perfil_orgao o ";
				sql += " where o.cod_perfil = p.cod_perfil and p.cod_sistema = "+codSistema+" and o.cod_usuario = "+codUsuario;
				sql += " and d.cod_orgao = o.cod_orgao_atuacao";
			} else
				sql += " where d.cod_orgao = "+codOrgao;
			sql += " and d.dat_proc between :1 and :2 group by d.dat_proc order by d.dat_proc";
			PreparedStatement stm = conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			stm.setDate(1, new java.sql.Date(bean.getDatInicial().getTime()));
			stm.setDate(2, new java.sql.Date(bean.getDatFinal().getTime()));
			ResultSet rs = stm.executeQuery();
			
			Calendar cal = new GregorianCalendar();
			cal.setTime(bean.getDatInicial());
			Date datAtual = cal.getTime();
			
			int totRegistrado = 0;
			int totDigitalizado = 0;
			int totTempo = 0;
			
			List beans = new ArrayList();
			while(datAtual.compareTo(bean.getDatFinal()) <= 0) {
				AIDigitAcompanhamentoBean item = new AIDigitAcompanhamentoBean();				
				item.setDatProc(datAtual);				
				
				if (rs.next()) {
					Date data = rs.getDate("dat_proc");
					if (data.equals(item.getDatProc())) {
						
						item.setQtdDigitalizado(new Integer(rs.getInt("qtd_digitalizado")));
						totDigitalizado += rs.getInt("qtd_digitalizado");
						
						item.setQtdRegistrado(new Integer(rs.getInt("qtd_registrado")));
						totRegistrado += rs.getInt("qtd_registrado");
						
						item.setQtdTempo(new Integer(rs.getInt("qtd_tempo")));
						totTempo += rs.getInt("qtd_tempo");
						
					} else
						rs.previous();
				}
				
				beans.add(item);
				
				cal.add(Calendar.DAY_OF_YEAR, 1);
				datAtual = cal.getTime();
			}
			bean.setBeans(beans);
			
			bean.setTotalQtdTempo(new Integer(totTempo));
			bean.setTotalQtdRegistrado(new Integer(totRegistrado));
			bean.setTotalQtdDigitalizado(new Integer(totDigitalizado));
			
			retorno = true;
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public boolean totalizaAIDigitAcompanhamento(AIDigitAcompanhamentoBean bean) throws DaoException {
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);			
			int codOrgao = Integer.parseInt(bean.getOrgao().getCodOrgao());
			java.sql.Date datProc = new java.sql.Date(bean.getDatProc().getTime());			
			
			//Registrados
			String sqlRegistrado = "select /*+rule*/ count(*) qtd from tsmi_auto_infracao";
			sqlRegistrado += " where cod_orgao = :1 and dat_registro = :2";
			PreparedStatement stmRegistrado = conn.prepareStatement(sqlRegistrado);
			stmRegistrado.setInt(1, codOrgao);
			stmRegistrado.setDate(2, datProc);
			ResultSet rsRegistrado = stmRegistrado.executeQuery();
			if (rsRegistrado.next())
				bean.setQtdRegistrado(new Integer(rsRegistrado.getInt("qtd")));
			stmRegistrado.close();
			
			//Digitalizados
			String sqlDigitalizado = "select /*+rule*/ count(*) qtd from tsmi_auto_infracao";
			sqlDigitalizado += " where cod_orgao = :1 and dat_digitalizacao = :2";
			PreparedStatement stmDigitalizado = conn.prepareStatement(sqlDigitalizado);
			stmDigitalizado.setInt(1, codOrgao);
			stmDigitalizado.setDate(2, datProc);
			ResultSet rsDigitalizado = stmDigitalizado.executeQuery();
			if (rsDigitalizado.next())
				bean.setQtdDigitalizado(new Integer(rsDigitalizado.getInt("qtd")));
			stmDigitalizado.close();
			
			//Tempo de Entrega
			String sqlTempo = "select /*+rule*/ sum(dat_digitalizacao-dat_registro) qtd from tsmi_auto_infracao";
			sqlTempo += " where cod_orgao = :1 and dat_digitalizacao = :2";
			PreparedStatement stmTempo = conn.prepareStatement(sqlTempo);
			stmTempo.setInt(1, codOrgao);
			stmTempo.setDate(2, datProc);
			ResultSet rsTempo = stmTempo.executeQuery();
			if (rsTempo.next())
				bean.setQtdTempo(new Integer(rsTempo.getInt("qtd")));
			stmTempo.close();
			
			//Pendentes 
			String sqlPendentes = "select /*+rule*/ count(*) qtd from tsmi_auto_infracao";
			sqlPendentes += " where cod_orgao = :1 and (dat_digitalizacao > :2" +
			" or sit_digitalizacao = 'N') and dat_registro <= :2";
			PreparedStatement stmPendentes = conn.prepareStatement(sqlPendentes);
			stmPendentes.setInt(1, codOrgao);
			stmPendentes.setDate(2, datProc);
			ResultSet rsEntreguePend = stmPendentes.executeQuery();
			if (rsEntreguePend.next())
				bean.setQtdPendentes(new Integer(rsEntreguePend.getInt("qtd")));
			stmPendentes.close();
			
			if ( (bean.getQtdDigitalizado().intValue() > 0) 
					|| (bean.getQtdRegistrado().intValue() > 0)
					|| (bean.getQtdPendentes().intValue() > 0))
				retorno = true;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally { 
			if (conn != null) { 
				try { 
					serviceloc.setReleaseConnection(conn); 
				} catch (Exception e) {
					System.out.println(e.getMessage()); 
				} 
			}
		}
		return retorno;
	}
	
	public void gravaAIDigitAcompanhamento(AIDigitAcompanhamentoBean bean, Connection conn) throws DaoException {		
		try {
			String sqlSel = "select dat_proc from tsmi_digit_autos where dat_proc = :1 and cod_orgao = :2";
			PreparedStatement stmSel = conn.prepareStatement(sqlSel);
			stmSel.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
			stmSel.setInt(2, Integer.parseInt(bean.getOrgao().getCodOrgao()));
			ResultSet rsSel = stmSel.executeQuery();
			
			if (rsSel.next()) {
				String sqlUpd = "update tsmi_digit_autos set qtd_digitalizado = :1, qtd_registrado = :2,";
				sqlUpd += " qtd_tempo_medio = :3";
				sqlUpd += " where dat_proc = :4 and cod_orgao = :5";
				PreparedStatement stmUpd = conn.prepareStatement(sqlUpd);
				stmUpd.setInt(1, bean.getQtdDigitalizado().intValue());
				stmUpd.setInt(2, bean.getQtdRegistrado().intValue());
				stmUpd.setInt(3, bean.getQtdTempo().intValue());
				stmUpd.setDate(4, new java.sql.Date(bean.getDatProc().getTime()));
				stmUpd.setInt(5, Integer.parseInt(bean.getOrgao().getCodOrgao()));				
				stmUpd.executeUpdate();
				stmUpd.close();
				
			} else {
				String sqlIns = "insert into tsmi_digit_autos (dat_proc, qtd_digitalizado, qtd_registrado,";
				sqlIns += " qtd_tempo_medio, cod_orgao) " +
				"values (:1, :2, :3, :4, :5)";
				PreparedStatement stmIns = conn.prepareStatement(sqlIns);
				stmIns.setDate(1, new java.sql.Date(bean.getDatProc().getTime()));
				stmIns.setInt(2, bean.getQtdDigitalizado().intValue());
				stmIns.setInt(3, bean.getQtdRegistrado().intValue());
				stmIns.setInt(4, bean.getQtdTempo().intValue());
				stmIns.setInt(5, Integer.parseInt(bean.getOrgao().getCodOrgao()));
				stmIns.executeUpdate();
				stmIns.close();
			}
			stmSel.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}	
	
	/**
	 *----------------------------------------------------------------------------------------------
	 * DAO relativo ao Recebimento de Fotos via WebService
	 * @author Pedro N�brega
	 *----------------------------------------------------------------------------------------------
	 */
	
	public ws.ArquivoBean[] buscarArquivosRecebidos(ArquivoRecebidoBean arquivoRecebidoBean, String dataInicial, String dataFinal)
	throws DaoException {	
		List listaArquivosRecebidos = new ArrayList();
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;

			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM (" +
			"select cod_arquivo, nom_arquivo, (qtd_reg - qtd_linha_pendente) total_qtd_reg, qtd_foto, dat_recebimento from" +
			" tsmi_arquivo_recebido";
			sCmd += " where cod_status = 2 and cod_orgao = " + Integer.parseInt(arquivoRecebidoBean.getCodOrgaoAtuacao())
			+ " and cod_ident_arquivo = '" + arquivoRecebidoBean.getCodIdentArquivo() + "'";
			sCmd += " and dat_recebimento between to_date('" + dataInicial + "','dd/mm/yyyy')" +
			"and to_date('" + dataFinal + "','dd/mm/yyyy') ";

			if (sHistorico.equals("S")) {
				sCmd += "union ";
				sCmd +="select cod_arquivo, nom_arquivo, (qtd_reg - qtd_linha_pendente) total_qtd_reg, qtd_foto, dat_recebimento from" +
				" tsmi_arquivo_recebido_bkp@smitpbkp";
				sCmd += " where cod_status = 2 and cod_orgao = " + Integer.parseInt(arquivoRecebidoBean.getCodOrgaoAtuacao())
				+ " and cod_ident_arquivo = '" + arquivoRecebidoBean.getCodIdentArquivo() + "'";
				sCmd += " and dat_recebimento between to_date('" + dataInicial + "','dd/mm/yyyy')" +
				"and to_date('" + dataFinal + "','dd/mm/yyyy')";
			}
			 
			sCmd = sCmd + " ) " +
			"order by dat_recebimento, nom_arquivo";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				ws.ArquivoBean arqRec = new ws.ArquivoBean();
				arqRec.setCodArquivo(rs.getString("cod_arquivo"));
				arqRec.setNomArquivo(rs.getString("nom_arquivo"));
				arqRec.setQtdReg(rs.getInt("total_qtd_reg"));
				arqRec.setQtdFoto(rs.getInt("qtd_foto"));
				arqRec.setDatRecebimento(rs.getDate("dat_recebimento"));
				arquivoRecebidoBean.setCodArquivo(arqRec.getCodArquivo());
				listaArquivosRecebidos.add(arqRec);
			}
			ws.ArquivoBean[] ArrayarquivosRecebidos = new ws.ArquivoBean[listaArquivosRecebidos.size()];
			listaArquivosRecebidos.toArray(ArrayarquivosRecebidos);
			rs.close();
			stmt.close();
			return ArrayarquivosRecebidos;
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}		
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void listarLinhasComFoto(ArquivoRecebidoBean arquivoRecebidoBean) throws DaoException {
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd ="select distinct num_auto_infracao";
			sCmd = sCmd + " from tsmi_linha_arquivo_rec";
			sCmd = sCmd + " where cod_arquivo = " + arquivoRecebidoBean.getCodArquivo();
			sCmd = sCmd + " and rtrim(substr(dsc_linha_arq_rec, 650, 30)) is not null";
			sCmd = sCmd + " and cod_status = 1  " ;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()){
				LinhaArquivoRec linha = new LinhaArquivoRec();
				linha.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				arquivoRecebidoBean.getLinhaArquivoRec().add(linha);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {			
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}	
	
	public void getLotesProcessados(RelatLotesProcBean RelatLotesProcBean) throws DaoException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT  SUM((NVL(DOL.QTD_GRAV,0))+(NVL(DOL.QTD_CANC,0))" +
			" +(NVL(DOL.QTD_LINHA_ERRO,0)))QTD_REG,SUM(DOL.QTD_GRAV)QTD_GRAV, " +
			" SUM(DOL.QTD_CANC)QTD_CANC, SUM(DOL.QTD_LINHA_ERRO)QTD_LINHA_ERRO,   " +
			" TO_DATE(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy') DAT_RECEBIMENTO,ARQ.COD_ORGAO , O.SIG_ORGAO " +
			" FROM TSMI_LOTE_DOL DOL, TSMI_ARQUIVO_DOL ARQ, TSMI_ORGAO O  " +
			" WHERE ARQ.DAT_RECEBIMENTO > TO_DATE('"+RelatLotesProcBean.getDataIni()+"','dd/mm/yyyy') " +
			" AND ARQ.DAT_RECEBIMENTO < TO_DATE('"+RelatLotesProcBean.getDataFim()+"','dd/mm/yyyy') "+
			" AND ARQ.COD_ORGAO = "+RelatLotesProcBean.getCodOrgao()+
			" AND DOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_DOL "+
			" AND ARQ.COD_ORGAO = O.COD_ORGAO "+
			" GROUP BY  TO_DATE(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy'), ARQ.COD_ORGAO , O.SIG_ORGAO"+
			" UNION "+
			" SELECT  SUM((NVL(DOL.QTD_GRAV,0))+(NVL(DOL.QTD_CANC,0))" +
			" +(NVL(DOL.QTD_LINHA_ERRO,0)))QTD_REG,SUM(DOL.QTD_GRAV)QTD_GRAV, " +
			" SUM(DOL.QTD_CANC)QTD_CANC, SUM(DOL.QTD_LINHA_ERRO)QTD_LINHA_ERRO,   " +
			" TO_DATE(TO_CHAR(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy')) DAT_RECEBIMENTO,ARQ.COD_ORGAO , O.SIG_ORGAO " +
			" FROM TSMI_LOTE_DOL_BKP DOL, TSMI_ARQUIVO_DOL_BKP ARQ, TSMI_ORGAO O  " +
			" WHERE ARQ.DAT_RECEBIMENTO > TO_DATE('"+RelatLotesProcBean.getDataIni()+"','dd/mm/yyyy') " +
			" AND ARQ.DAT_RECEBIMENTO < TO_DATE('"+RelatLotesProcBean.getDataFim()+"','dd/mm/yyyy') "+
			" AND ARQ.COD_ORGAO = "+RelatLotesProcBean.getCodOrgao()+
			" AND DOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_DOL "+
			" AND ARQ.COD_ORGAO = O.COD_ORGAO "+
			" GROUP BY  to_char(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy'), ARQ.COD_ORGAO , O.SIG_ORGAO"+
			" ORDER BY  DAT_RECEBIMENTO,COD_ORGAO,SIG_ORGAO";
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				RelatLotesProcBean myRelatLotesProc = new RelatLotesProcBean();
				myRelatLotesProc.setDatLote(Util.converteData(rs.getDate("DAT_RECEBIMENTO")));
				myRelatLotesProc.setQtdReg(rs.getString("QTD_REG"));
				myRelatLotesProc.setQtdGrav(rs.getString("QTD_GRAV"));
				myRelatLotesProc.setQtdRej(rs.getString("QTD_LINHA_ERRO"));
				myRelatLotesProc.setQtdCancel(rs.getString("QTD_CANC"));
				myRelatLotesProc.setCodOrgao(rs.getString("COD_ORGAO"));
				myRelatLotesProc.setSigOrgao(rs.getString("SIG_ORGAO"));
				dados.add(myRelatLotesProc) ;
			}  
			RelatLotesProcBean.setDados(dados);
			
			rs.close();
			stmt.close();
			
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public void mostraDetalhe(RelatLotesProcBean RelatLotesProcBean) throws DaoException {
		Connection conn = null;
		ArrayList dados = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT L.DSC_LINHA_LOTE, L.COD_LOTE, L.COD_LINHA_LOTE, " +
			" L.NUM_SEQ_LINHA, L.COD_STATUS, L.COD_RETORNO," +
			" L.DSC_LINHA_LOTE_RETORNO ";
			if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += ",E.DSC_ERRO  ";
			sCmd += " FROM TSMI_LOTE_DOL DOL, TSMI_ARQUIVO_DOL ARQ,  " +
			" TSMI_ORGAO O, TSMI_LINHA_LOTE L " ;
			if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += " ,TSMI_ERRO_BROKER E  ";
			sCmd += " WHERE ARQ.DAT_RECEBIMENTO > TO_DATE('"+RelatLotesProcBean.getDataLote()+"','DD/MM/YYYY') " +
			" AND ARQ.DAT_RECEBIMENTO < TO_DATE('"+RelatLotesProcBean.getDataLote()+"','DD/MM/YYYY') "+
			" AND ARQ.COD_ORGAO = "+RelatLotesProcBean.getCodOrgao()+
			" AND DOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_DOL "+
			" AND ARQ.COD_ORGAO = O.COD_ORGAO  AND L.COD_LOTE = DOL.COD_LOTE  ";
			if(RelatLotesProcBean.getTipoConsulta().equals("GRAVADOS"))
				sCmd += " AND L.COD_RETORNO = '000' AND L.SIT_CANC_DOL IS NULL "; 
			else if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += " AND L.COD_RETORNO <> '000'  AND L.COD_RETORNO = E.COD_ERRO   ";
			else if(RelatLotesProcBean.getTipoConsulta().equals("CANCELADOS"))
				sCmd += " AND L.COD_RETORNO = '000' AND L.SIT_CANC_DOL IS NOT NULL ";
			sCmd += " UNION " +
			" SELECT L.DSC_LINHA_LOTE, L.COD_LOTE, L.COD_LINHA_LOTE, " +
			" L.NUM_SEQ_LINHA, L.COD_STATUS, L.COD_RETORNO," +
			" L.DSC_LINHA_LOTE_RETORNO ";
			if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += ",E.DSC_ERRO  ";
			sCmd += " FROM TSMI_LOTE_DOL_BKP DOL, TSMI_ARQUIVO_DOL_BKP ARQ,  " +
			" TSMI_ORGAO O, TSMI_LINHA_LOTE_BKP L " ;
			if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += " ,TSMI_ERRO_BROKER E  ";
			sCmd += " WHERE ARQ.DAT_RECEBIMENTO > TO_DATE('"+RelatLotesProcBean.getDataLote()+"','DD/MM/YYYY') " +
			" AND ARQ.DAT_RECEBIMENTO < TO_DATE('"+RelatLotesProcBean.getDataLote()+"','DD/MM/YYYY') "+
			" AND ARQ.COD_ORGAO = "+RelatLotesProcBean.getCodOrgao()+
			" AND DOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_DOL "+
			" AND ARQ.COD_ORGAO = O.COD_ORGAO  AND L.COD_LOTE = DOL.COD_LOTE  ";
			if(RelatLotesProcBean.getTipoConsulta().equals("GRAVADOS"))
				sCmd += " AND L.COD_RETORNO = '000' AND L.SIT_CANC_DOL IS NULL "; 
			else if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
				sCmd += " AND L.COD_RETORNO <> '000'  AND L.COD_RETORNO = E.COD_ERRO   ";
			else if(RelatLotesProcBean.getTipoConsulta().equals("CANCELADOS"))
				sCmd += " AND L.COD_RETORNO = '000' AND L.SIT_CANC_DOL IS NOT NULL "; 
			
			
			ResultSet rs = stmt.executeQuery(sCmd);     
			while( rs.next() ) {
				REG.LinhaLoteDolBean linhaRec = new REG.LinhaLoteDolBean();
				linhaRec.setCodLote(rs.getString("cod_lote"));
				linhaRec.setCodLinhaLote(rs.getString("cod_linha_lote"));
				linhaRec.setDscLinhaLote(sys.Util.rPad(rs.getString("dsc_linha_lote")," ",455));
				linhaRec.setNumSeqLinha(rs.getString("num_seq_linha"));
				linhaRec.setCodStatus(rs.getString("cod_status"));
				linhaRec.setCodRetorno(rs.getString("cod_retorno"));
				if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
					linhaRec.setDscErro(rs.getString("dsc_erro"));
				linhaRec.setDscLinhaLoteRetorno(rs.getString("dsc_linha_lote_retorno"));
				dados.add(linhaRec) ;
			}  
			RelatLotesProcBean.setDadosLote(dados);
			
			rs.close();
			stmt.close();
			
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public int VerificaProcessosNaoUtilizados(String sCodOrgao,String sAno,int iUltSeqParam) throws DaoException {        
		Connection conn = null;
		int iQtd=0;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COUNT(*) AS QTD FROM TSMI_CONTROLE_PROCESSO WHERE "+
			" COD_ORGAO='"+sCodOrgao+"' AND "+
			" SEQ>"+iUltSeqParam+" AND "+
			" ANO='"+sAno+"' AND "+
			" SIT_UTILIZADO IS NULL";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) 
				iQtd=rs.getInt("QTD");
			rs.close();
			stmt.close();   
			return iQtd;
		} catch (Exception e) {
			return 0;
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public int PegarUltimoSeqProcesso(String sCodOrgao,String sAno) throws DaoException {        
		Connection conn = null;
		int iUltSeq  = 0;
		try {        
			conn = serviceloc.getConnection(MYABREVSIST);			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT MAX(SEQ) AS SEQ FROM TSMI_CONTROLE_PROCESSO WHERE "+
			" COD_ORGAO='"+sCodOrgao+"' AND "+
			" ANO='"+sAno+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) iUltSeq = rs.getInt("SEQ");			
			rs.close();
			stmt.close();   
			return iUltSeq;
		} catch (Exception e) {
			return 0;
		}        
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	
	public void GravaCodigoBarraProcesso(String sCodOrgao,String sDatGeracao,String sNumProcesso,String sAno,int iSeq) throws DaoException {
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "INSERT INTO TSMI_CONTROLE_PROCESSO (COD_CONTROLE_PROCESSO, COD_ORGAO, "
				+ "DAT_GERACAO, NUM_PROCESSO, ANO, SEQ)"
				+ " VALUES (SEQ_TSMI_CONTROLE_PROCESSO.NEXTVAL,' " 
				+ sCodOrgao + "', " 
				+ "TO_DATE('"+ sDatGeracao+ "', 'DD/MM/YYYY'),'"
				+ sNumProcesso+"','"+sAno+"','"+iSeq+"')";
			
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	
	public Boolean validaNumeracaoRecebida(ArquivoRecebidoBean myArq,String codOrgaoAtuacao)  throws DaoException {
		Boolean valido = false;
		Connection conn =null ;
		int inumControle = 0;
		int inumSubPrefeitura = 0;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt    = conn.createStatement();
			String sCmd       = "";
			inumControle      = Integer.parseInt(myArq.getNumControleArq());
			inumSubPrefeitura = Integer.parseInt(myArq.getNumSubPrefeitura());
			
			
			ResultSet rs = null;
			sCmd =  "SELECT * FROM tsmi_setor ";
			sCmd +=  "Where "+String.valueOf(inumControle)+" BETWEEN faixa_de AND faixa_ate ";
			sCmd +=  "and "+String.valueOf(inumControle)+" = SEQ_ARQUIVO ";
			sCmd +=  "and COD_ORGAO='"+codOrgaoAtuacao+"' ";
			sCmd +=  "and COD_SETOR='"+String.valueOf(inumSubPrefeitura)+"' ";
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				valido = true;
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return valido;
	}
	
	
	
	
	public boolean ArquivoInsertSubPrefeitura(ArquivoRecebidoBean auxClasse,REC.ParamOrgBean ParamOrgaoId)
	throws DaoException {
		boolean existe = false ;
		Connection conn = null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);
			existe = ArquivoInsertSubPrefeitura(auxClasse, ParamOrgaoId, conn);
			conn.commit();
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return existe;
	}	
	
	
	
	public boolean ArquivoInsertSubPrefeitura(ArquivoRecebidoBean auxClasse,REC.ParamOrgBean ParamOrgaoId, Connection conn)
	throws DaoException {
		boolean existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd     = "SELECT COD_ARQUIVO from TSMI_ARQUIVO_RECEBIDO "+
			"WHERE NOM_ARQUIVO = '" + auxClasse.getNomArquivo() + "'" +
			" AND COD_STATUS <> '9'";
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) { 
				auxClasse.setCodStatus("9");
			}
			if ( ((!"9".equals(auxClasse.getCodStatus())) &&
					(auxClasse.getTamNumControle()>0))) 
			{
				// buscar o numero do proximo arquivo e atualizar
				sCmd = "SELECT SEQ_ARQUIVO FROM TSMI_SETOR " +
				" WHERE COD_SETOR = '"+auxClasse.getNumSubPrefeitura()+"' AND COD_ORGAO = '"
				+ auxClasse.getCodOrgao() + "' FOR UPDATE";
				stmt = conn.createStatement();          
				rs  = stmt.executeQuery(sCmd) ;
				int numUltSeq = 0;
				boolean naoExisteNumeracao  = true ;
				while (rs.next()) {
					numUltSeq = rs.getInt("SEQ_ARQUIVO");              
					naoExisteNumeracao  = false;              
				}
				// atualizar numero de sequencia do arquivo
				String nSeq = "";
				
				if ((naoExisteNumeracao))   {
					auxClasse.setCodStatus("9");
					auxClasse.getErroArquivo().addElement("Parametros para numera��o do arquivo "+auxClasse.getNomArquivo()+" n�o localizado para o Org�o: "+auxClasse.getCodOrgao());
				}
				else {
					numUltSeq++;
					
					
					sCmd = "UPDATE TSMI_SETOR SET SEQ_ARQUIVO = '" + numUltSeq + "'"
					+ " WHERE COD_SETOR = '" +  Integer.parseInt(auxClasse.getNumSubPrefeitura()) + "' AND COD_ORGAO = '"
					+ Integer.parseInt(auxClasse.getCodOrgao()) + "'";
					stmt.execute(sCmd);
				}    
			}
			
			// buscar codigo do arquivo
			sCmd  = "SELECT SEQ_TSMI_ARQUIVO_RECEBIDO.NEXTVAL COD_ARQUIVO FROM DUAL";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setCodArquivo(rs.getString("cod_arquivo"));
				auxClasse.setPkid(auxClasse.getCodArquivo());
			}
			// inserir o arquivo na tabela de controle
			sCmd  = "INSERT INTO TSMI_ARQUIVO_RECEBIDO (COD_ARQUIVO,"+
			"NOM_ARQUIVO,DAT_RECEBIMENTO,DAT_PROCESSAMENTO_DETRAN,COD_IDENT_ARQUIVO, "+
			"NUM_ANO_EXERCICIO, NUM_CONTROLE_ARQUIVO,NUM_PROTOCOLO, "+
			"IND_EMISSAO_NOTIF_DETRAN, QTD_REG,COD_ORGAO,"+ 
			"COD_ORGAO_LOTACAO,SIG_ORGAO_LOTACAO,NOM_USERNAME,COD_STATUS,"+
			"NOM_RESPONSAVEL,IND_P59, COD_IDENT_MOVIMENTO,IND_PRIORIDADE)"+
			"VALUES (" + auxClasse.getCodArquivo() + ", " +
			"'"+auxClasse.getNomArquivo()+"', "+
			"TO_DATE('"+auxClasse.getDatRecebimento()+"','dd/mm/yyyy'), " +
			"TO_DATE('"+auxClasse.getDatProcessamentoDetran()+"','DD/MM/YYYY'), " +
			"'"+auxClasse.getCodIdentArquivo()+"', " +
			"'"+auxClasse.getNumAnoExercicio()+"', " +
			"'"+auxClasse.getNumControleArq()+"', " +
			"'"+auxClasse.getNumProtocolo()+"', " +
			"'"+auxClasse.getIndEmissaoNotifDetran()+"', " +
			"'"+auxClasse.getQtdReg()+"', " +
			"'"+auxClasse.getCodOrgao()+"', " +
			"'"+auxClasse.getCodOrgaoLotacao()+"', " +
			"'"+auxClasse.getSigOrgaoLotacao()+"', " +
			"'"+auxClasse.getNomUsername()+"', " +
			"'"+auxClasse.getCodStatus()+"', " +
			"'"+auxClasse.getResponsavelArq()+"', " +
			"'"+auxClasse.getDscPortaria59()+"', " +
			"'"+auxClasse.getIdentMovimento()+"', 1)";
			stmt.execute(sCmd);
			if ("9".equals(auxClasse.getCodStatus())==false) {          
				// Inserir as linhas
				Iterator it  = auxClasse.getLinhaArquivoRec().iterator() ;  
				int num_seq_linha = 0;
				LinhaArquivoRec linha;
				while (it.hasNext()) {
					num_seq_linha++;
					linha = (LinhaArquivoRec) it.next();
					linha.setNumSeqLinha(String.valueOf(num_seq_linha));
					linha.carregarCampos(auxClasse);
					sCmd  = "INSERT INTO TSMI_LINHA_ARQUIVO_REC (COD_LINHA_ARQ_REC,DSC_LINHA_ARQ_REC,"
						+ "NUM_SEQ_LINHA,COD_ARQUIVO,COD_STATUS,NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,COD_ORGAO)"
						+ " VALUES (SEQ_TSMI_LINHA_ARQUIVO_REC.NEXTVAL, "
						+ "'"+linha.getDscLinhaArqRec().trim()+"','"+linha.getNumSeqLinha()+"',"+auxClasse.getCodArquivo()
						+ ",0,'"+linha.getNumAutoInfracao()+"','"+linha.getNumNotificacao()+"','"+linha.getCodOrgao()+"')";
					stmt.execute(sCmd);
				}
			}
			stmt.close();
			rs.close();
			existe = true ;
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	
	
	
	public String getSetor(ACSS.UsuarioBean myUsr,String nomArq)  throws DaoException {
		String myOrgs ="";
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			String sCmd     = "";
			ResultSet rs = null;
			sCmd =  "select o.SEQ_ARQUIVO,o.DSC_SETOR from TSMI_SETOR o where o.COD_ORGAO='"+myUsr.getCodOrgaoAtuacao()+"' ";
			sCmd += "order by o.DSC_SETOR";
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myOrgs += ",\""+nomArq+Util.lPad(rs.getString("SEQ_ARQUIVO"),"0",5)+"\"";
				myOrgs += ",\""+rs.getString("DSC_SETOR") +"\"";
			}
			myOrgs = myOrgs.substring(1,myOrgs.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myOrgs;
	}	
	
	public void CarregaLista(REG.ResumoRetornoPagBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		int prim = 1;
		int qtd = 0;
		int qtdParcial = 0;
		String trans = "";
		String datProc = "";
		
		try {
			
			if (!auxClasse.getDatProcRef().equals("Ultima")){		
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				
				ArrayList dados = new ArrayList();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd = "";
				
				if (auxClasse.getAcao().equals("detalhe")){					
					
					sCmd =	"SELECT PK_COD_CONTROLE_VEX_MASTER, NUM_NOTIFICACAO, SEQ_NOTIFICACAO, NUM_AUTO_INFRACAO, to_char(DAT_RETORNO,'dd/mm/yyyy') as DAT_RETORNO, "+ 
					"TIP_NOTIFICACAO, COD_RETORNO "+ 
					"FROM TSMI_CONTROLE_VEX_MASTER "; 
					
					if (auxClasse.getDscTransacao().equals("CR�DITO"))
						sCmd +=  "WHERE COD_RETORNO " +auxClasse.getCodTransacao()+" ";
					else
						sCmd +=  "WHERE COD_RETORNO = " +auxClasse.getCodTransacao()+" ";
					
					sCmd += "AND IND_ENTREGUE_RETORNO = '" +auxClasse.getIndRet()+"' "+
					"AND DAT_ENVIO >= TO_DATE('" + auxClasse.getDatEnvio() + "', 'DD/MM/YYYY') " +
					"AND DAT_RETORNO >= TO_DATE('" + auxClasse.getDatInicio() + "', 'DD/MM/YYYY') " +
					"AND DAT_RETORNO < TO_DATE('" + auxClasse.getDatFim() + "', 'DD/MM/YYYY') "+						
					"AND PK_COD_CONTROLE_VEX_MASTER > " + auxClasse.getDatProcRef() + " "+					
					"ORDER BY PK_COD_CONTROLE_VEX_MASTER ";
				}else{
					sCmd =	"SELECT MASTER.PK_COD_CONTROLE_VEX_MASTER, MASTER.NUM_NOTIFICACAO, MASTER.SEQ_NOTIFICACAO, MASTER.NUM_AUTO_INFRACAO, to_char(DETAIL.DAT_RETORNO,'dd/mm/yyyy') as DAT_RETORNO, "+ 
					"MASTER.TIP_NOTIFICACAO, DETAIL.COD_RETORNO "+ 
					"FROM TSMI_CONTROLE_VEX_MASTER MASTER, TSMI_CONTROLE_VEX_DETAIL DETAIL "+ 
					"WHERE DETAIL.COD_RETORNO " +auxClasse.getCodTransacao()+" "+
					"AND MASTER.PK_COD_CONTROLE_VEX_DETAIL_REC = DETAIL.PK_COD_CONTROLE_VEX_DETAIL "+
					"AND IND_FATURADO_NOTIFICACAO = 'S' "+
					"AND MASTER.DAT_ENVIO >= TO_DATE('" + auxClasse.getDatEnvio() + "', 'DD/MM/YYYY') " +
					"AND DETAIL.DAT_RETORNO >= TO_DATE('" + auxClasse.getDatInicio() + "', 'DD/MM/YYYY') " +
					"AND DETAIL.DAT_RETORNO < TO_DATE('" + auxClasse.getDatFim() + "', 'DD/MM/YYYY') "+						
					"AND MASTER.PK_COD_CONTROLE_VEX_DETAIL_REC > " + auxClasse.getDatProcRef() + " "+					
					"ORDER BY MASTER.PK_COD_CONTROLE_VEX_DETAIL_REC ";
					
				}
				
				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					ControleVexBean contVex = new ControleVexBean();
					String codRetorno = rs.getString("COD_RETORNO");
					String pkCodControle = rs.getString("PK_COD_CONTROLE_VEX_MASTER"); 
					contVex.setPkCodControleVex(pkCodControle);
					contVex.setCodRetorno(codRetorno);
					contVex.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));
					contVex.setSeqNotificacao(rs.getString("SEQ_NOTIFICACAO"));
					contVex.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
					contVex.setDatRetorno(rs.getString("DAT_RETORNO"));
					contVex.setTipNotificacao(rs.getString("TIP_NOTIFICACAO"));
					
					trans = codRetorno;
					datProc = pkCodControle;
					
					//datProc = datProc.substring(8,10) + "/" + datProc.substring(5,7) + "/" + datProc.substring(0,4) + ":" + datProc.substring(11,19);
					qtd++;
					
					if ((prim == 1) && (!acao.equals("Ant"))){
						auxClasse.getPrimTrans().add(auxClasse.getTransRef());
						auxClasse.getPrimDatProc().add(auxClasse.getDatProcRef());
						prim++;
					}
					
					dados.add(contVex);
					if (qtd == 50) break;
					
				}
				
				auxClasse.setDados(dados);
				
				qtdParcial = Integer.parseInt(auxClasse.getQtdParcialReg());
				
				if (qtd != 0){
					
					auxClasse.setQtdRegs(String.valueOf(qtd));
					if (acao.equals("Prox") || acao.equals("detalhe") || acao.equals("detalheResumo")) 
						qtdParcial = qtdParcial + qtd;
				}
				
				if ((prim > 1) && (!acao.equals("Ant"))){
					auxClasse.getUltTrans().add(trans);
					auxClasse.getUltDatProc().add(datProc);
					auxClasse.getQtdReg().add(String.valueOf(qtd));
				}
				
				if ((qtd == 0) && (!auxClasse.getDatProcRef().equals("Ultima"))){
					
					auxClasse.getPrimTrans().add("Ultima");
					auxClasse.getPrimDatProc().add("Ultima");
					
					auxClasse.getUltTrans().add("Ultima");
					auxClasse.getUltDatProc().add("Ultima");
					auxClasse.getQtdReg().add("Ultima");
				}
				
				if (!auxClasse.getDatProcRef().equals("Ultima")){
					auxClasse.setQtdParcialReg(String.valueOf(qtdParcial));
				}
				
				auxClasse.setDados(dados);
				
				rs.close();
				stmt.close();
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}
	
	public String BuscaTotalReg(REG.ResumoRetornoPagBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		String qtd = "0";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			// preparar lista os dados do Usuario por Sistema
			
			String sCmd = "";
			
			if (auxClasse.getAcao().equals("detalhe")){					
				
				sCmd = "SELECT COUNT(*) AS QTD "+ 
				"FROM TSMI_CONTROLE_VEX_MASTER "; 
				
				if (auxClasse.getDscTransacao().equals("CR�DITO"))
					sCmd +=  "WHERE COD_RETORNO " +auxClasse.getCodTransacao()+" ";
				else
					sCmd +=  "WHERE COD_RETORNO = " +auxClasse.getCodTransacao()+" ";
				
				sCmd += "AND IND_ENTREGUE_RETORNO = '" +auxClasse.getIndRet()+"' "+
				"AND DAT_ENVIO >= to_date('" + auxClasse.getDatEnvio() + "', 'dd/mm/yyyy') " +
				"AND DAT_RETORNO >= to_date('" + auxClasse.getDatInicio() + "', 'dd/mm/yyyy') " +
				"AND DAT_RETORNO <  to_daye('" + auxClasse.getDatFim() + "', 'dd/mm/yyyy') ";						
			}else{
				
				sCmd = "SELECT COUNT(*) AS QTD "+ 
				"FROM TSMI_CONTROLE_VEX_MASTER MASTER, TSMI_CONTROLE_VEX_DETAIL DETAIL "+ 
				"WHERE DETAIL.COD_RETORNO " +auxClasse.getCodTransacao()+" "+
				"AND MASTER.PK_COD_CONTROLE_VEX_DETAIL_REC = DETAIL.PK_COD_CONTROLE_VEX_DETAIL "+
				"AND IND_FATURADO_NOTIFICACAO = 'S' "+
				"AND MASTER.DAT_ENVIO >= to_date('" + auxClasse.getDatEnvio() + "', 'dd/mm/yyyy') " +
				"AND DETAIL.DAT_RETORNO >= to_date('" + auxClasse.getDatInicio() + "', 'dd/mm/yyyy') " +
				"AND DETAIL.DAT_RETORNO < to_date('" + auxClasse.getDatFim() + "', 'dd/mm/yyyy') ";						
			}			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getString("QTD");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	
	
	public String BuscaDscCodRetorno(String codRetorno)
	throws DaoException {
		
		Connection conn = null;
		String dsc = "";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			// preparar lista os dados do Usuario por Sistema
			
			String sCmd = "";
			
			
			sCmd = "SELECT DSC_CODIGO_RETORNO "+ 
			"FROM TSMI_CODIGO_RETORNO "+ 
			"WHERE NUM_CODIGO_RETORNO = " +codRetorno+" ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				dsc = rs.getString("DSC_CODIGO_RETORNO");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return dsc;
	}
	
	public boolean getTemARDigitalizado(String numNotificacao)
	throws DaoException {
		
		Connection conn = null;
		boolean achou = false;
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			// preparar lista os dados do Usuario por Sistema
			
			String sCmd = "";
			
			
			sCmd = "SELECT NUM_NOTIFICACAO "+ 
			"FROM TSMI_AR_DIGITALIZADO "+ 
			"WHERE NUM_NOTIFICACAO = " +numNotificacao+" ";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				achou = true;
			}
			
			
		}catch (SQLException e) {
			achou = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			achou = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return achou;
	}
	
	public boolean getTemARDigitalizado(ControleVexBean myBean)
	throws DaoException {
		
		Connection conn = null;
		boolean achou = false;
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			// preparar lista os dados do Usuario por Sistema
			
			String sCmd = "";
			
			if (!myBean.getNumAutoInfracao().equals("")){
				
				sCmd = "SELECT NUM_NOTIFICACAO "+ 
				"FROM TSMI_AR_DIGITALIZADO "+ 
				"WHERE NUM_NOTIFICACAO = " +myBean.getNumNotificacao()+" ";
				
			}else{
				
				sCmd = "SELECT NUM_NOTIFICACAO "+ 
				"FROM TPNT_AR_DIGITALIZADO "+ 
				"WHERE NUM_NOTIFICACAO = " +myBean.getNumNotificacao()+" "+
				"AND SEQ_NOTIFICACAO = " +myBean.getSeqNotificacao()+" ";
				
			}
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				achou = true;
			}
			
			
		}catch (SQLException e) {
			achou = false;
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			achou = false;
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return achou;
	}	
	
	
	public void carregaEquipEletr(LinhaArquivoRec auxClasse) throws DaoException {
		
		Connection conn = null;
		boolean achou = false;
		boolean eConnLocal = false;        
		try {
			conn = auxClasse.getConn();
			if (conn == null) {
				eConnLocal = true;
				conn = serviceloc.getConnection(MYABREVSIST) ;
			}            
			Statement stmt  = conn.createStatement();
			
			String linha = sys.Util.rPad(auxClasse.getDscLinhaArqRec(), " ", 900);					
			
			
			String identAparelho = linha.substring(131,146);
			identAparelho = identAparelho.trim(); 
			
			String sCmd = "SELECT ID_EQUIPAMENTO_ELETR "+ 
			"FROM TSMI_EQUIPAMENTO_ELETR "+ 
			"WHERE IDENT_APARELHO = '" + identAparelho + "' ";
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				achou = true;
			}
			
			String codMunicInf = linha.substring(84,88);
			String locAparelho = "";
			if (codMunicInf.equals("5865"))
				locAparelho = linha.substring(775,875);
			else       
				locAparelho = linha.substring(173,213);
			
			String velocPermit = linha.substring(119,122);
			
			
			if (!achou){
				//Gravar EQUIPAMENTO ELETR�NICO
				sCmd = "INSERT INTO TSMI_EQUIPAMENTO_ELETR (ID_EQUIPAMENTO_ELETR, COD_MUNICIPIO, "
					+ "LOCALIZACAO, VELOCIDADE, SENTIDO, IDENT_APARELHO)"
					+ "VALUES (SEQ_TSMI_EQUIPAMENTO_ELETR.NEXTVAL, " 
					+  codMunicInf + ", " 
					+ "'" + locAparelho + "', " 
					+ "'" + velocPermit + "', "
					+ "'D', "
					+ "'" + identAparelho + "')";
				stmt.execute(sCmd);
			}
			auxClasse.setMsgErro("") ;
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (eConnLocal)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO Insere Download feito pelo usu�rio
	 *-----------------------------------------------------------
	 */	
	public void insereDownloadArquivoPontuacao(ACSS.UsuarioBean usrLogado, String codArquivo, String extensao) 
	throws DaoException {		
		Connection conn = null;	
		String sCmd     = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd ="INSERT INTO TPNT_DOWNLOAD (COD_DOWNLOAD,DAT_DOWNLOAD,COD_ARQUIVO,NOM_USERNAME,COD_ORGAO_LOTACAO)" +
			" VALUES(SEQ_PNT_DOWNLOAD.NEXTVAL,sysdate,'" 
			+ codArquivo + "','" + usrLogado.getNomUserName() + "','" 
			+ usrLogado.getOrgao().getCodOrgao() + "')";  
			stmt.execute(sCmd);	
			stmt.close();			
		}
		catch (SQLException e) {
			System.out.println("insereDownloadArquivo :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}		
	
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */	
	public Vector BuscaArquivosProcessadosPontuacao(ACSS.UsuarioBean usrLogado, String codIdentArquivo, String dataIni, String dataFim,String Origem) throws DaoException {
		Vector vRetClasse   = new Vector();
		Connection conn = null;
		try {
				ArquivoRecebidoBean relClasse;
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				String sCmd ="SELECT DISTINCT A.DAT_RECEBIMENTO,A.NOM_ARQUIVO,A.COD_ARQUIVO,A.COD_STATUS,TO_CHAR(A.DAT_RECEBIMENTO,'dd/mm/yy') as DAT_RECEBIMENTO_CHAR, A.QTD_LINHA,A.QTD_LINHA_ERRO," +
				" TO_CHAR(G.max_download,'dd/mm/yy') as DAT_DOWNLOAD_GRAV,A.QTD_LINHA_CANCELADA" +
				" FROM TPNT_ARQUIVO A" +	
				" left join (SELECT cod_arquivo,max(dat_download) as max_download FROM TPNT_DOWNLOAD group by cod_arquivo) G" +
				" on (a.cod_arquivo=G.cod_arquivo)" +				
				" WHERE COD_IDENT_ARQUIVO ='" + codIdentArquivo + "'" +
				" AND A.DAT_RECEBIMENTO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" +			
				" ORDER BY A.DAT_RECEBIMENTO DESC";         
				ResultSet rs = stmt.executeQuery(sCmd);             
				while (rs.next())   {
					relClasse = new ArquivoRecebidoBean();
					relClasse.setCodIdentArquivo(codIdentArquivo);
					relClasse.setCodArquivo(rs.getString("cod_arquivo"));
					relClasse.setNomArquivo(rs.getString("nom_arquivo"));
					relClasse.setDatRecebimento(rs.getString("dat_recebimento_char"));                  
					relClasse.setCodStatus(rs.getString("cod_Status"));
					relClasse.setQtdReg(rs.getString("qtd_LINHA"));
					relClasse.setQtdLinhaErro(rs.getString("QTD_LINHA_ERRO"));
					relClasse.setDatDownloadGravado(rs.getString("DAT_DOWNLOAD_GRAV"));               
					relClasse.setQtdLinhaCancelada(rs.getString("QTD_LINHA_CANCELADA"));
					vRetClasse.addElement(relClasse);           
				}  
				rs.close();
				stmt.close();		
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	
	public boolean ConsultaacompPendEmitevex(acompPendEmitevexBean ran,String datIni,String datFim) throws DaoException
	{
		boolean retorno = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);

            ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
            ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
            ParamSistemaBeanId.PreparaParam();
            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
            if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM (" 
				+ " SELECT T1.NOM_ARQUIVO,T1.COD_ARQUIVO,to_char(T1.DAT_RECEBIMENTO,'dd/mm/yyyy') as dat_recebimento,T3.QTD_RETORNADA,'E' as TIPO,COUNT(*) AS QTD_ENVIADA FROM TSMI_ARQUIVO_RECEBIDO T1,TSMI_CONTROLE_VEX_MASTER_V1 T2"				
				+ " LEFT JOIN (SELECT COUNT(*) as QTD_RETORNADA,COD_ARQUIVO  from TSMI_CONTROLE_VEX_MASTER_V1 WHERE STATUS <> '11' GROUP BY COD_ARQUIVO) T3 on (T2.COD_ARQUIVO=T3.COD_ARQUIVO)"
				+ " WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND"
				+ " T1.DAT_RECEBIMENTO>=TO_DATE('"+datIni+"') AND"
				+ " T1.DAT_RECEBIMENTO<=TO_DATE('"+datFim+"')"
				+ " GROUP BY T1.NOM_ARQUIVO,T1.COD_ARQUIVO,T1.DAT_RECEBIMENTO,T3.QTD_RETORNADA ";
			if (sHistorico.equals("S")) {			
				sCmd += " UNION( "
				+ " SELECT T1.NOM_ARQUIVO,T1.COD_ARQUIVO,to_char(T1.DAT_RECEBIMENTO,'dd/mm/yyyy') as dat_recebimento,T3.QTD_RETORNADA,'E' as TIPO,COUNT(*) AS QTD_ENVIADA FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP T1,TSMI_CONTROLE_VEX_MASTER_V1 T2 "
				+ " LEFT JOIN (SELECT COUNT(*) as QTD_RETORNADA,COD_ARQUIVO  from TSMI_CONTROLE_VEX_MASTER_V1 WHERE STATUS <> '11' GROUP BY COD_ARQUIVO) T3 on (T2.COD_ARQUIVO=T3.COD_ARQUIVO) "
				+ " WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "
				+ " T1.DAT_RECEBIMENTO>=TO_DATE('"+datIni+"','dd/mm/yyyy') AND "
				+ " T1.DAT_RECEBIMENTO<=TO_DATE('"+datFim+"','dd/mm/yyyy')"
				+ " GROUP BY T1.NOM_ARQUIVO,T1.COD_ARQUIVO,T1.DAT_RECEBIMENTO,T3.QTD_RETORNADA )";
			}				
			sCmd += " UNION( "
				+ " SELECT T1.NOM_ARQUIVO,T1.COD_ARQUIVO,to_char(T1.DAT_RECEBIMENTO,'dd/mm/yyyy') as dat_recebimento,T3.QTD_RETORNADA,'P' as TIPO,COUNT(*) AS QTD_ENVIADA FROM TPNT_ARQUIVO T1,TSMI_CONTROLE_VEX_MASTER_V1 T2 "
				+ " LEFT JOIN (SELECT COUNT(*) as QTD_RETORNADA,COD_ARQUIVO from TSMI_CONTROLE_VEX_MASTER_V1 WHERE STATUS <> '11' GROUP BY COD_ARQUIVO) T3 on (T2.COD_ARQUIVO=T3.COD_ARQUIVO) "
				+ " WHERE T1.COD_ARQUIVO=T2.COD_ARQUIVO AND "
				+ " T1.DAT_RECEBIMENTO>=TO_DATE('"+datIni+"','dd/mm/yyyy') AND "
				+ " T1.DAT_RECEBIMENTO<=TO_DATE('"+datFim+"','dd/mm/yyyy')"
				+ " GROUP BY T1.NOM_ARQUIVO,T1.COD_ARQUIVO,T1.DAT_RECEBIMENTO,T3.QTD_RETORNADA )"
				+ " ) ORDER BY to_date(DAT_RECEBIMENTO,'dd/mm/yyyy'),3 ";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector listaArq = new Vector();
			long totQtdReg   =0;
			long totLinhPend =0;			
			while( rs.next() ) {
				acompPendEmitevexBean myBean = new acompPendEmitevexBean();
				myBean.setNomArquivo(rs.getString("Nom_Arquivo"));
				myBean.setQtdRetornada(rs.getString("QTD_RETORNADA"));
				myBean.setQtdEnviada(rs.getString("QTD_ENVIADA"));	
				myBean.setCodArquivo(rs.getString("COD_ARQUIVO"));	
				myBean.setDatRecebimento(rs.getString("DAT_RECEBIMENTO"));				
				listaArq.addElement(myBean);
				retorno = true;
			}			
			ran.setTotalQtdReg( String.valueOf(totQtdReg));
			ran.setTotalQtdLinhaPendente( String.valueOf(totLinhPend));	
			ran.setListaArqs( listaArq );
			rs.close();
			stmt.close();
			serviceloc.setReleaseConnection(conn);
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return retorno;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqNotificacaoBean
	 *-----------------------------------------------------------
	 */
	public List getacompPendEmitevex(NotifControleBean notifControle, String rowNum, HashMap proxAnt, String rowNumImp,String codArquivo) throws DaoException{
		Connection conn = null;    
		List notificacoes = new ArrayList();
		String qtdTotal = "0";
		int totreg=0;
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			Statement stmt  = conn.createStatement();
			Statement stmt_Det  = conn.createStatement();			
			
			
			String sCmd = "SELECT count(*)as qtdRegs FROM TSMI_CONTROLE_VEX_MASTER_V1"				
				+ " WHERE COD_ARQUIVO="+codArquivo
				+ " AND STATUS = '11'";
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if(rs.next()){
				 qtdTotal = rs.getString("qtdRegs");
			}
			
			
			sCmd = "SELECT NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,NUM_PLACA,DAT_INFRACAO,"
				+ " COD_ARQUIVO,COD_ORGAO FROM TSMI_CONTROLE_VEX_MASTER_V1"				
				+ " WHERE COD_ARQUIVO="+codArquivo
				+ " AND STATUS = '11'";
			
			if (!proxAnt.get("primeiroRegistro").equals(""))
			{
				if (proxAnt.get("sentido").equals("Ant"))
				{
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("primeiroRegistro") + "'";
					
				}
				else if (proxAnt.get("sentido").equals("Prox"))                                         
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				else if (proxAnt.get("sentido").equals("Imprimir")) {
					sCmd += " AND NUM_NOTIFICACAO > '" + (String)proxAnt.get("primeiroRegistro") + "'";
					sCmd += " AND NUM_NOTIFICACAO < '" + (String)proxAnt.get("ultimoRegistro") + "'" ;
				}
			}				
				sCmd += " ORDER BY NUM_NOTIFICACAO";
				
			rs = stmt.executeQuery(sCmd);			
			
			NotifControleBean notificacao = null;
			while(rs.next() && (rowNumImp.equals("-1") || totreg < Integer.parseInt(rowNum)))			
			{
				notificacao = new NotifControleBean();           	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));
				notificacao.setDatEmissao((rs.getDate("DAT_INFRACAO")));
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO")); 
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO"));
				
				String sCmd_Det="";
				ResultSet rs_Det=null;
				if (notificacao.getNumAutoInfracao().length()==0)
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TPNT_ARQUIVO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));				    
				}
				else
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
					else
					{
						if (sHistorico.equals("S")) {
							sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
							rs_Det = stmt_Det.executeQuery(sCmd_Det);
							if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
						}
					}
				}
				
				
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
				totreg++;
			}
			notifControle.setListaNotifs(notificacoes);
			notifControle.setQtdReg(qtdTotal);
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		
		return notificacoes;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo a RelatArqNotificacaoBean
	 *-----------------------------------------------------------
	 */
	public List getacompPendEmitevexXls(NotifControleBean notifControle, String rowNum, HashMap proxAnt, String rowNumImp,String codArquivo) throws DaoException{
		Connection conn = null;    
		List notificacoes = new ArrayList();
		String qtdTotal = "0";
		int totreg=0;
		boolean flag = true;
			
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";

			Statement stmt  = conn.createStatement();
			Statement stmt_Det  = conn.createStatement();	
			
			
			String sCmd ="SELECT NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,NUM_PLACA,DAT_INFRACAO,"
				+ " COD_ARQUIVO,COD_ORGAO FROM TSMI_CONTROLE_VEX_MASTER_V1"				
				+ " WHERE COD_ARQUIVO="+codArquivo
				+ " AND STATUS = '11' ";		
				sCmd += " ORDER BY NUM_NOTIFICACAO";
				
			ResultSet rs = stmt.executeQuery(sCmd);			
			
			NotifControleBean notificacao = null;
			while(rs.next())			
			{
				notificacao = new NotifControleBean();           	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));
				notificacao.setDatEmissao((rs.getDate("DAT_INFRACAO")));
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO")); 
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO"));
				
				
				String sCmd_Det="";
				ResultSet rs_Det=null;
				if (notificacao.getNumAutoInfracao().length()==0)
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TPNT_ARQUIVO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));				    
				}
				else
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
					else
					{
						if (sHistorico.equals("S")) {
							sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
							rs_Det = stmt_Det.executeQuery(sCmd_Det);
							if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
						}
					}
				}
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
				totreg++;
			}
			notifControle.setListaNotifs(notificacoes);
			notifControle.setQtdReg(qtdTotal);
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		return notificacoes;
	}
	
	public List getacompPendEmitevexXlsTodos(NotifControleBean notifControle) throws DaoException{
		Connection conn = null;    
		List notificacoes = new ArrayList();
		String qtdTotal = "0";
		int totreg=0;
		boolean flag = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;

			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			if (MYABREVSIST.equals("GER")) sHistorico="S";

			Statement stmt  = conn.createStatement();
			Statement stmt_Det  = conn.createStatement();			
			
			
			
			String sCmd ="SELECT NUM_AUTO_INFRACAO,NUM_NOTIFICACAO,NUM_PLACA,DAT_INFRACAO,"
						+ " COD_ARQUIVO,COD_ORGAO FROM TSMI_CONTROLE_VEX_MASTER_V1"		
						+ " WHERE STATUS = '11' ";		
				sCmd += " ORDER BY NUM_NOTIFICACAO";
				
			ResultSet rs = stmt.executeQuery(sCmd);			
			
			NotifControleBean notificacao = null;
			int contador =0;
			while(rs.next() && contador < 30000)			
			{	contador++;
				notificacao = new NotifControleBean();           	
				notificacao.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));           
				notificacao.setNumNotificacao(rs.getString("NUM_NOTIFICACAO"));           	
				notificacao.setNumPlaca(rs.getString("NUM_PLACA"));
				notificacao.setDatEmissao((rs.getDate("DAT_INFRACAO")));
				notificacao.setCodArquivoEnvio(rs.getString("COD_ARQUIVO")); 
				notificacao.setCodOrgaoEnvio(rs.getString("COD_ORGAO"));
		
				String sCmd_Det="";
				ResultSet rs_Det=null;
				if (notificacao.getNumAutoInfracao().length()==0)
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TPNT_ARQUIVO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));				    
				}
				else
				{
					sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
					rs_Det = stmt_Det.executeQuery(sCmd_Det);
					if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
					else
					{
						if (sHistorico.equals("S")) {						
							sCmd_Det="SELECT NOM_ARQUIVO FROM TSMI_ARQUIVO_RECEBIDO_BKP@SMITPBKP WHERE COD_ARQUIVO="+notificacao.getCodArquivoEnvio();
							rs_Det = stmt_Det.executeQuery(sCmd_Det);
							if (rs_Det.next()) notificacao.setNomArquivo(rs_Det.getString("NOM_ARQUIVO"));
						}
					}
				}
			
				// Acrescenta o elemento na lista de notifica��es a serem retornadas pelo m�todo.
				notificacoes.add(notificacao);
				totreg++;
			}
			notifControle.setListaNotifs(notificacoes);
			notifControle.setQtdReg(qtdTotal);
			rs.close();
			stmt.close();
			stmt_Det.close();
		}
		catch (SQLException sqle) { System.out.println( sqle.getMessage()); }
		catch (Exception e) {	   System.out.println( e.getMessage());    }
		finally { if (conn != null) { try { serviceloc.setReleaseConnection(conn); } catch (Exception ey) { } }}
		return notificacoes;
	}

	
	
/*Bahia 23/09/2008*/
	
	
	public void atualizaArDigTMP(String codLinhaArqReb, String codArDigTMP, Connection conn) throws DaoException {
		
		try {
			
			Statement stmt  = conn.createStatement();
			
			String	sCmd = "UPDATE TSMI_AR_DIGITALIZADO_TMP set COD_LINHA_ARQ_REC=" + codLinhaArqReb + " " +
			"WHERE COD_AR_DIGITALIZADO = " + codArDigTMP;
			
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	
	public void processaRetornoArDigTMP(LinhaArquivoRec auxClasse) throws DaoException {
		
		Connection conn = null;
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			
			String sCmd = "UPDATE TSMI_AR_DIGITALIZADO_TMP SET COD_RETORNO_DETRAN = '"+auxClasse.getCodRetorno()+"'" + 
			"WHERE COD_LINHA_ARQ_REC = " + auxClasse.getCodLinhaArqReq() ;
			
			
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	
	
	public void CarregaLista(REG.RelatArDigitTMPBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		int prim = 1;
		int qtd = 0;
		int qtdParcial = 0;
		int tamVector = auxClasse.getPrimTrans().size();
		String oper = "";
		String trans = "";
		String datProc = "";
		
		if (acao.equals("Prox")) oper = ">";
		else oper = ">=";
		
		
		try {
			
			if (!auxClasse.getDatProcRef().equals("Ultima")){		
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				
				ArrayList dados = new ArrayList();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd =
					"SELECT * "+ 
					"FROM TSMI_AR_DIGITALIZADO_TMP "+ 
					"WHERE COD_AR_DIGITALIZADO IS NOT NULL ";
				if (!auxClasse.getsIN().equals(""))	
					sCmd += "AND IND_COD_RET_V1 = "+auxClasse.getsIN()+" ";
				sCmd += "AND DAT_DIGITALIZACAO = to_date('" + auxClasse.getDatEnvio() + "', 'dd/mm/yyyy') ";						
				sCmd += "AND COD_AR_DIGITALIZADO > " + auxClasse.getTransRef() + " ";						
				sCmd += "ORDER BY COD_AR_DIGITALIZADO";
				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					trans = rs.getString("COD_AR_DIGITALIZADO");
					datProc = rs.getString("DAT_DIGITALIZACAO");
					datProc = datProc.substring(8,10) + "/" + datProc.substring(5,7) + "/" + datProc.substring(0,4) + ":" + datProc.substring(11,19);
					qtd++;
					
					if ((prim == 1) && (!acao.equals("Ant"))){
						auxClasse.getPrimTrans().add(auxClasse.getTransRef());
						auxClasse.getPrimDatProc().add(auxClasse.getDatProcRef());
						prim++;
					}
					
					String codRetorno = rs.getString("CODIGO_RETORNO_VEX");
					if (codRetorno == null) codRetorno = "";
					
					String dscRetorno = "";
					String numNotificacao = rs.getString("NUM_NOTIFICACAO");
					String numAutoInfracao = "";
					String sdados = "";
					
					dscRetorno = BuscaDscRetorno(codRetorno);
					numAutoInfracao = BuscaNumAutoInfracao(numNotificacao);
					
					
					sdados = Util.rPad(numNotificacao ," ",9);
					sdados += Util.rPad(numAutoInfracao ," ",12);
					sdados += Util.rPad(rs.getString("NUM_LOTE") ," ",3);
					sdados += Util.rPad(rs.getString("NUM_CAIXA") ," ",8);
					sdados += Util.rPad(codRetorno ," ",3);
					sdados += Util.rPad(dscRetorno ," ",100);
					
					dados.add(sdados);
					if (qtd == 50) break;
					
				}
				
				qtdParcial = Integer.parseInt(auxClasse.getQtdParcialReg());
				
				if (qtd != 0){
					
					auxClasse.setQtdRegs(String.valueOf(qtd));
					if (acao.equals("Prox") || acao.equals("B")) 
						qtdParcial = qtdParcial + qtd;
				}
				
				if ((prim > 1) && (!acao.equals("Ant"))){
					auxClasse.getUltTrans().add(trans);
					auxClasse.getUltDatProc().add(datProc);
					auxClasse.getQtdReg().add(String.valueOf(qtd));
				}
				
				if ((qtd == 0) && (!auxClasse.getTransRef().equals("Ultima"))){
					
					auxClasse.getPrimTrans().add("Ultima");
					auxClasse.getPrimDatProc().add("Ultima");
					
					auxClasse.getUltTrans().add("Ultima");
					auxClasse.getUltDatProc().add("Ultima");
					auxClasse.getQtdReg().add("Ultima");
				}
				
				if (!auxClasse.getTransRef().equals("Ultima")){
					auxClasse.setQtdParcialReg(String.valueOf(qtdParcial));
				}
				
				auxClasse.setDados(dados);
				
				rs.close();
				stmt.close();
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	
	public String BuscaTotalReg(REG.RelatArDigitTMPBean auxClasse)
	throws DaoException {
		
		Connection conn = null;
		String qtd = "0";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			ArrayList dados = new ArrayList();
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				"SELECT COUNT(*) AS QTD "+ 
				"FROM TSMI_AR_DIGITALIZADO_TMP "+ 
				"WHERE COD_AR_DIGITALIZADO IS NOT NULL ";
			if (!auxClasse.getsIN().equals(""))	
				sCmd += "AND IND_COD_RET_V1 = "+auxClasse.getsIN()+" ";
			sCmd += "AND DAT_DIGITALIZACAO = to_date('" + auxClasse.getDatEnvio() + "', 'dd/mm/yyyy') ";						
			//sCmd += "AND COD_AR_DIGITALIZADO > " + auxClasse.getTransRef() + " ";						
			sCmd += "ORDER BY COD_AR_DIGITALIZADO";
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getString("QTD");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	public String BuscaDscRetorno(String codRetorno)
	throws DaoException {
		
		Connection conn = null;
		String dsc = "C�DIGO N�O ENCONTRADO";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			ArrayList dados = new ArrayList();
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				"SELECT DSC_CODIGO_RETORNO "+ 
				"FROM TSMI_CODIGO_RETORNO "+ 
				"WHERE NUM_CODIGO_RETORNO = "+codRetorno;
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				dsc = rs.getString("DSC_CODIGO_RETORNO");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return dsc;
	}
	
/*Retirar ap�s processamento*/
	public void processaArDigTMP(LinhaArquivoRec auxClasse, ArquivoRecebidoBean arqRecebido) throws DaoException {
		
		Connection conn = null;
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String linha = sys.Util.rPad(auxClasse.getDscLinhaArqRec(), " ", 900);	
			String numNotif = linha.substring(9,18);
			
			String sCmd = "UPDATE TSMI_AR_DIGITALIZADO_TMP SET COD_RETORNO_DETRAN = '"+auxClasse.getCodRetorno()+"'," + 
			"COD_LINHA_ARQ_REC = " + auxClasse.getCodLinhaArqReq() +
			" WHERE " +
			"num_notificacao = '" + numNotif+"' AND " +
			"nom_arquivo = '" + arqRecebido.getNomArquivo()+"'" ;
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return ;
	}
	

	public void buscaARPeriodo(REG.RelatArDigitTMPBean auxClasse) 
	throws DaoException {
		
		Connection conn = null;
		
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			ArrayList dados = new ArrayList();
			int Retornado = 0;
			int Apropriado = 0;
			int Desprezados = 0;
			int NoaProcess = 0;
			int Avisos = 0;
			
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				"SELECT DAT_DIGITALIZACAO, COUNT(*) AS QTDR,"+ 
				"SUM(DECODE(IND_COD_RET_V1,'A',1,0)) AS  QTDA,"+ 
				"SUM(DECODE(IND_COD_RET_V1,'D',1,0)) AS  QTDD,"+
			    "SUM(DECODE(IND_COD_RET_V1,'N',1,0)) AS  QTDN "+
			    "FROM TSMI_AR_DIGITALIZADO_TMP "+
			    "WHERE IND_COD_RET_V1 <> '9' "+
		        "AND DAT_DIGITALIZACAO BETWEEN to_date('" + auxClasse.getDatInicio() + "','dd/mm/yyyy') AND to_date('" +  auxClasse.getDatFim()+"','dd/mm/yyyy') "+
			    "GROUP BY DAT_DIGITALIZACAO "+
		        "UNION "+
		        "select DAT_RETORNO,0,0,0,0 "+
		        "from tsmi_cont_vex_tot_ret_v1 "+
		        "WHERE DAT_RETORNO BETWEEN to_date('" + auxClasse.getDatInicio() + "','dd/mm/yyyy') AND to_date('" +  auxClasse.getDatFim()+"','dd/mm/yyyy') "+
		        "AND DAT_RETORNO NOT IN (SELECT DAT_DIGITALIZACAO "+
		        "                               FROM TSMI_AR_DIGITALIZADO_TMP "+ 
		        "                               WHERE IND_COD_RET_V1 <> '9' "+ 
		        "                               AND DAT_DIGITALIZACAO BETWEEN to_date('" + auxClasse.getDatInicio() + "','dd/mm/yyyy') AND to_date('" +  auxClasse.getDatFim()+"','dd/mm/yyyy') "+
		        "                               GROUP BY DAT_DIGITALIZACAO) "+
		        "GROUP BY DAT_RETORNO ";						
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				
				RelatArDigitTMPBean_Cont myBean = new RelatArDigitTMPBean_Cont();
				myBean.setDatEnvio(Util.converteData(rs.getDate("DAT_DIGITALIZACAO")));
				
				myBean.setQtdRetornados(rs.getString("QTDR"));
				Retornado += Integer.parseInt(myBean.getQtdRetornados()); 
				
				myBean.setQtdApropriado(rs.getString("QTDA"));
				Apropriado += Integer.parseInt(myBean.getQtdApropriado()); 
				
				myBean.setQtdDesprezados(rs.getString("QTDD"));
				Desprezados += Integer.parseInt(myBean.getQtdDesprezados()); 

				myBean.setQtdNaoProcessados(rs.getString("QTDN"));
				NoaProcess += Integer.parseInt(myBean.getQtdNaoProcessados()); 

				myBean.setQtdAvisos(String.valueOf(buscaTotalAviso(myBean)));
				Avisos += Integer.parseInt(myBean.getQtdAvisos()); 
				
				
				dados.add(myBean);
			}
			auxClasse.setQtdRetornadosTot(String.valueOf(Retornado));
			auxClasse.setQtdApropriadosTot(String.valueOf(Apropriado));
			auxClasse.setQtdDesprezadosTot(String.valueOf(Desprezados));
			auxClasse.setQtdNoaProcessTot(String.valueOf(NoaProcess));
			auxClasse.setQtdAvisoTot(String.valueOf(Avisos));
			
			auxClasse.setEventos(dados);
			
			rs.close();
			stmt.close();
			
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}		
		
	

	public int buscaTotalARPeriodo(RelatArDigitTMPBean_Cont myBean, REG.RelatArDigitTMPBean auxClasse, String codRetornoDetran)
	throws DaoException {
		
		Connection conn = null;
		int qtd = 0;
		
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				"SELECT count(*) as qtd "+ 
				"FROM TSMI_AR_DIGITALIZADO_TMP "+ 
				"WHERE COD_AR_DIGITALIZADO IS NOT NULL ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd +="AND DAT_DIGITALIZACAO >= to_date('" + auxClasse.getDatInicio() + "', 'dd/mm/yyyy') " ;
			if (!auxClasse.getDatFim().equals(""))
				sCmd +="AND DAT_DIGITALIZACAO <= to_date('" + auxClasse.getDatFim() + "', 'dd/mm/yyyy') ";						
			sCmd +="AND DAT_DIGITALIZACAO = to_date('" + myBean.getDatEnvio() + "', 'dd/mm/yyyy') ";
			if (!codRetornoDetran.equals(""))
			    sCmd +="AND STATUS_COD_RET_V1 IN ('" + codRetornoDetran + "') ";						
			sCmd += "GROUP BY DAT_DIGITALIZACAO ";
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getInt("qtd");
				
			}
			
			rs.close();
			stmt.close();
			
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	public void GravaDataDownload(ArquivoRecebidoBean arqRecebido, ACSS.UsuarioBean usrLogado) throws DaoException{
		Connection conn = null;
		String sCmd     = null;		
		String datDownload  = sys.Util.formatedToday().substring(0,10);
		
		try{
			conn = serviceloc.getConnection(MYABREVSIST);
//			conn.setAutoCommit(false);
			
			Statement stmt = conn.createStatement();
			LinhaArquivoRec linhaRecebida = null;
			/*
			 * Inicia loop para valida��o E registro de todas as notifica��es presentes
			 * no arquivo de download.
			 */
			for(int i=0;i<arqRecebido.getLinhaArquivoRec().size();i++){
				linhaRecebida = (LinhaArquivoRec)arqRecebido.getLinhaArquivoRec().get(i);
				
				boolean possuiFoto = false;
				String dscLinha = linhaRecebida.getDscLinhaArqRec();
				String tipoNotif = dscLinha.substring(571,572);
				String identAparelho = dscLinha.substring(649,679).trim();
				if (identAparelho.length() > 0) possuiFoto = true;
				
				
		    	sCmd = "UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET DAT_DOWNLOAD = "
					+ "to_date('"+ datDownload+ "', 'dd/mm/yyyy') ";
					if (!possuiFoto){ 
						sCmd += ", DAT_RECEBIMENTO_VEX = to_date('"+ datDownload+ "', 'dd/mm/yyyy') "; 
						if ((!tipoNotif.equals("0")) && (!tipoNotif.equals("2")))
							sCmd += ", STATUS = 11 "; 
					}
					sCmd += "WHERE  NUM_AUTO_INFRACAO = '"+linhaRecebida.getNumAutoInfracao()+"' "
				    + "AND COD_ARQUIVO = '"+arqRecebido.getCodArquivo()+"'";
				
				stmt.executeUpdate(sCmd);
				
		    	sCmd = "UPDATE TSMI_CONT_VEX_MASTER_REMI SET DAT_DOWNLOAD = "
					+ "to_date('"+ datDownload+ "', 'dd/mm/yyyy') " ;
					if (!possuiFoto){ 
						sCmd += ", DAT_RECEBIMENTO_VEX = to_date('"+ datDownload+ "', 'dd/mm/yyyy') "; 
						if ((!tipoNotif.equals("0")) && (!tipoNotif.equals("2")))
							sCmd += ", STATUS = 11 "; 
					}
					sCmd += "WHERE  NUM_AUTO_INFRACAO = '"+linhaRecebida.getNumAutoInfracao()+"' "
				    + "AND COD_ARQUIVO = '"+arqRecebido.getCodArquivo()+"'";
				
				stmt.executeUpdate(sCmd);

				
				if (possuiFoto)
				    testaMaiorDataDownload(linhaRecebida.getNumAutoInfracao(),arqRecebido.getCodArquivo());
				
			}
			stmt.close();
//			conn.commit();
			
		}
		catch (SQLException e) {
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch(Exception e){
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	
	public void GravaDataDownloadFoto(String numAutoInfracao, String codArquivo) throws DaoException {
		Connection conn = null;
		try {
			String datDownload  = sys.Util.formatedToday().substring(0,10);
			
			conn = serviceloc.getConnection(MYABREVSIST); 
			
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET DAT_DOWNLOAD_FOTO = "
				+ "to_date('"+ datDownload+ "', 'dd/mm/yyyy') "
				+ "WHERE  NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' "
		        + "AND COD_ARQUIVO = "+codArquivo+" ";
			stmt.executeUpdate(sql);
			
			sql = "UPDATE TSMI_CONT_VEX_MASTER_REMI SET DAT_DOWNLOAD_FOTO = "
				+ "to_date('"+ datDownload+ "', 'dd/mm/yyyy') "
				+ "WHERE  NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' "
		        + "AND COD_ARQUIVO = "+codArquivo+" ";
			stmt.executeUpdate(sql);
			
			
			testaMaiorDataDownload(numAutoInfracao,codArquivo);
			
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}	
	
	public void testaMaiorDataDownload(String numAutoInfracao, String codArquivo) throws DaoException{

		Connection conn = null;
		String sCmd     = null;		
		String datDownload  = "";
		String datDownloadFoto  = "";
		String datStatus  = sys.Util.formatedToday().substring(0,10);
		try{
			
			conn = serviceloc.getConnection(MYABREVSIST); 

			Statement stmt = conn.createStatement();

				
		    	sCmd = "SELECT * FROM TSMI_CONTROLE_VEX_MASTER_V1 "
					+ "WHERE  NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' "
				    + "AND COD_ARQUIVO = '"+codArquivo+"'";
		    	
		    	ResultSet rs = stmt.executeQuery(sCmd);
				
		    	if (rs.next()) {
					
		    		datDownload = Util.converteData(rs.getDate("DAT_DOWNLOAD"));
		    		if(datDownload == null) datDownload = ""; 
		    		datDownloadFoto = Util.converteData(rs.getDate("DAT_DOWNLOAD_FOTO"));
		    		if(datDownloadFoto == null) datDownloadFoto = ""; 
		    
		    		
		    	}
		    	
		    	if ((!datDownload.equals("")) && (!datDownloadFoto.equals(""))){
		    		
		    		String sql = "";
		    		
					sql = "UPDATE TSMI_CONTROLE_VEX_MASTER_V1 SET "
						+ "DAT_RECEBIMENTO_VEX = to_date('"+ Util.VerificaMaiorData(datDownload,datDownloadFoto)+ "', 'dd/mm/yyyy'), "
						+ "STATUS = 11, "
						+ "DAT_STATUS = to_date('"+ datStatus + "', 'dd/mm/yyyy') "
						+ "WHERE  NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' "
				        + "AND COD_ARQUIVO = '"+codArquivo+"'";
					    stmt.executeUpdate(sql);	
		    		
					sql = "UPDATE TSMI_CONT_VEX_MASTER_REMI SET "
						+ "DAT_RECEBIMENTO_VEX = to_date('"+ Util.VerificaMaiorData(datDownload,datDownloadFoto)+ "', 'dd/mm/yyyy'), "
						+ "STATUS = 11, "
						+ "DAT_STATUS = to_date('"+ datStatus + "', 'dd/mm/yyyy') "
						+ "WHERE  NUM_AUTO_INFRACAO = '"+numAutoInfracao+"' "
					    + "AND COD_ARQUIVO = '"+codArquivo+"'";
						stmt.executeUpdate(sql);	

		    	}
				
			    stmt.close();
		}
		catch (SQLException e) {
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		catch(Exception e){
			System.out.println("recebeDownloadNotif :"+sCmd);
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}
	
	public void LeRetornosAnt(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException {
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt      = conn.createStatement();
			ResultSet rs        = null;
			
			String sCmd="SELECT MES_RETORNO,QTD_RETORNO,QTD_RECUPERADA_RETORNO, "+
			"COD_RETORNO,IND_ENTREGUE,IND_FATURADO FROM TSMI_CONTROLE_VEX_RESUMO WHERE "+ 
			"ANO_RETORNO='"+ResumoRetornoBeanId.getAno()+"' AND "+
			"ANO_ENVIO||MES_ENVIO>='"+
			ResumoRetornoBeanId.getDatContrato().substring(3,7)+
			ResumoRetornoBeanId.getDatContrato().substring(0,2)+"' "+
			"ORDER BY ANO_RETORNO,MES_RETORNO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                   = rs.getInt("MES_RETORNO");
				String sQtd                = rs.getString("QTD_RETORNO");
				String sQtd_Recup_Entrega  = rs.getString("QTD_RECUPERADA_RETORNO");
				String numCodigoRetorno    = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				String indEntrega          = rs.getString("IND_ENTREGUE");
				String indFaturado         = rs.getString("IND_FATURADO");
				
				if (numCodigoRetorno.trim().equals("000")) continue;
				
				
				if (numCodigoRetorno.length()>0) /*Retornado*/
				{
					if (indFaturado.equals("N"))
					{
						/*Total Credito - Mensal*/
						Long lTotalCredito=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdCredito())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdCredito(String.valueOf(lTotalCredito));
						/*Total Recuperado-Geral*/
						lTotalCredito=Long.parseLong(ResumoRetornoBeanId.getQtdCredito())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.setQtdCredito(String.valueOf(lTotalCredito));
					}
					
					
					if (numCodigoRetorno.equals("99"))
					{
						/*Total Recuperado Entrega - Mensal*/
						Long lTotalRecuperado=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdRecuperadoSistema())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdRecuperadoSistema(String.valueOf(lTotalRecuperado));
						/*Total Recuperado-Geral*/
						lTotalRecuperado=Long.parseLong(ResumoRetornoBeanId.getQtdRecuperadoSistema())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.setQtdRecuperadoSistema(String.valueOf(lTotalRecuperado));
					}
					else
					{
						
						/*Recuperado Entrega*/
						Long lTotalRecuperadoEntrega=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
						/*Total Recuperado-Geral*/
						lTotalRecuperadoEntrega=Long.parseLong(ResumoRetornoBeanId.getQtdRecuperadoEntrega())+
						Long.parseLong(sQtd_Recup_Entrega);
						ResumoRetornoBeanId.setQtdRecuperadoEntrega(String.valueOf(lTotalRecuperadoEntrega));
						
						
						/*Total Retornado-Mensal*/
						Long lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalRetornada())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
						/*Total Retornado-Geral*/
						lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalRetornada())+
						Long.parseLong(sQtd);
						ResumoRetornoBeanId.setQtdTotalRetornada(String.valueOf(lTotalRetornada));
						
						/*Total Retornado-C�digo de Retorno*/
						for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
						{     
							if ( (numCodigoRetorno.equals(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getNumRetorno())) &&
									indEntrega.equals((ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N"))
							)
							{
								/*Total Retornado-C�digo de Retorno-Mensal*/
								lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).getQtdTotalRetornada())+
								Long.parseLong(sQtd);
								ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
								/*Total Retornado-C�digo de Retorno-Geral*/
								lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())+
								Long.parseLong(sQtd);
								ResumoRetornoBeanId.getCodigoRetornos(i).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
								break;
							}
						}
						
					}
					
				}
			}
			
			/*CSS/CSS Anteriores/NCSS Anteriores/Faturamento*/
			for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++)
			{
				
				Long lTotalFaturamento=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())-
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdCredito())+
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoEntrega())+
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoSistema());
				ResumoRetornoBeanId.getRetornos(i).setQtdFaturamento(String.valueOf(lTotalFaturamento));
				
				Long lTotalGeralFaturamento=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdFaturamento());
				lTotalGeralFaturamento=Long.parseLong(ResumoRetornoBeanId.getQtdFaturamento())+
				lTotalGeralFaturamento;
				ResumoRetornoBeanId.setQtdFaturamento(String.valueOf(lTotalGeralFaturamento));
			}
			
			/*N�o Retornada Anos Anteriores*/
			sCmd="SELECT SUM(QTD_EMITIDO-QTD_EMITIDO_RETORNO) as QTD FROM TSMI_CONTROLE_VEX_RESUMO " +
			"WHERE ANO_ENVIO<='"+ResumoRetornoBeanId.getAnoAnt()+"'";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				ResumoRetornoBeanId.setQtdTotalnaoRetornadaAnt(rs.getString("QTD"));
			}
			
			
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	public void LeRetornosEnvioAnt(ResumoRetornoBean ResumoRetornoBeanId) throws DaoException {
		Connection conn = null;    
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt      = conn.createStatement();
			ResultSet rs        = null;
			String sCmd="SELECT * FROM TSMI_CONTROLE_VEX_RESUMO WHERE "+ 
			"ANO_ENVIO='"+ResumoRetornoBeanId.getAno()+"' "+
			"ORDER BY ANO_ENVIO,MES_ENVIO,COD_RETORNO";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				int iMes                = rs.getInt("MES_ENVIO");
				String sQtd_Enviado     = rs.getString("QTD_EMITIDO");
				String sQtd_Enviado_Ret = rs.getString("QTD_EMITIDO_RETORNO");
				String numCodigoRetorno = rs.getString("COD_RETORNO");
				if (numCodigoRetorno==null) numCodigoRetorno="";
				String indEntrega       = rs.getString("IND_ENTREGUE");
				
				
				
				/*Enviados-Mensal-Geral*/
				if (numCodigoRetorno.equals("000")) {
					Long lTotalEnviado=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					/*Total Emitidos-Geral*/
					lTotalEnviado=Long.parseLong(ResumoRetornoBeanId.getQtdTotalEnviada())+
					Long.parseLong(sQtd_Enviado);
					ResumoRetornoBeanId.setQtdTotalEnviada(String.valueOf(lTotalEnviado));
					continue;
				}
				
				
				if (numCodigoRetorno.length()>0) /*Retornado*/
				{
					/*Total Retornado-Mensal*/
					Long lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(iMes-1).getQtdTotalRetornada())+
					Long.parseLong(sQtd_Enviado_Ret);
					ResumoRetornoBeanId.getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
					/*Total Retornado-Geral*/
					lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalRetornada())+
					Long.parseLong(sQtd_Enviado_Ret);
					ResumoRetornoBeanId.setQtdTotalRetornada(String.valueOf(lTotalRetornada));
					
					/*Total Retornado-C�digo de Retorno*/
					for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
					{     
						if ( (numCodigoRetorno.equals(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getNumRetorno())) &&
								indEntrega.equals((ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N"))
						)
						{
							/*Total Retornado-C�digo de Retorno-Mensal*/
							lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).getQtdTotalRetornada())+
							Long.parseLong(sQtd_Enviado_Ret);
							ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(iMes-1).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
							/*Total Retornado-C�digo de Retorno-Geral*/
							lTotalRetornada=Long.parseLong(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())+
							Long.parseLong(sQtd_Enviado_Ret);
							ResumoRetornoBeanId.getCodigoRetornos(i).setQtdTotalRetornada(String.valueOf(lTotalRetornada));
							break;
						}
					}
				}
				
				/*N�o Retornados-Mensal-Geral*/				
				/*Total Retornado-Mensal*/
			}
			for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++)
			{
				Long lTotalnaoRetornada=Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada())-
				Long.parseLong(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada());
				ResumoRetornoBeanId.getRetornos(i).setQtdTotalnaoRetornada(String.valueOf(lTotalnaoRetornada));
				
				/*Total n�o Retornado-Geral*/
				lTotalnaoRetornada=Long.parseLong(ResumoRetornoBeanId.getQtdTotalnaoRetornada())+
				lTotalnaoRetornada;
				ResumoRetornoBeanId.setQtdTotalnaoRetornada(String.valueOf(lTotalnaoRetornada));
			}
			/*N�o Retornada Anos Anteriores*/
			sCmd="SELECT SUM(QTD_EMITIDO-QTD_EMITIDO_RETORNO) as QTD FROM TSMI_CONTROLE_VEX_RESUMO " +
			"WHERE ANO_ENVIO<='"+ResumoRetornoBeanId.getAnoAnt()+"'";
			rs=stmt.executeQuery(sCmd);
			while (rs.next())
			{
				ResumoRetornoBeanId.setQtdTotalnaoRetornadaAnt(rs.getString("QTD"));
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}


	public String buscaTotalAviso(REG.RelatArDigitTMPBean_Cont myBean)
	throws DaoException {
		
		Connection conn = null;
		int qtd = 0;
		
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				          "select sum(qtd_retorno_aviso) as qtd_retorno_aviso " + 
				          "from tsmi_cont_vex_tot_ret_v1 " + 
				          "where cod_retorno = '03 ' " +
				          "and dat_retorno >= to_date('" + myBean.getDatEnvio() + "', 'dd/mm/yyyy') " +
				          "and dat_retorno <= to_date('" + myBean.getDatEnvio() + "', 'dd/mm/YYYY') " ;						
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getInt("qtd_retorno_aviso");
				
			}
			
			rs.close();
			stmt.close();
			
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return String.valueOf(qtd);
	}
		
	public String BuscaNumAutoInfracao(String numNotif)
	throws DaoException {
		
		Connection conn = null;
		String dsc = "";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			ArrayList dados = new ArrayList();
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
				"SELECT NUM_AUTO_INFRACAO "+ 
				"FROM TSMI_CONTROLE_VEX_MASTER_V1 "+ 
				"WHERE NUM_NOTIFICACAO = '"+numNotif+"'";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				dsc = rs.getString("NUM_AUTO_INFRACAO");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return dsc;
	}
	
/************************/
	public void CarregaListaRegistro(GER.AuditoriaBean auxClasse, String acao)
	throws DaoException {
		
		Connection conn = null;
		int prim = 1;
		int qtd = 0;
		int qtdParcial = 0;
		int tamVector = auxClasse.getPrimTrans().size();
		String oper = "";
		String trans = "";
		String datProc = "";		
		String result = "";
		
		if (acao.equals("Prox")) oper = ">";
		else oper = ">=";
		
		
		try {
			
			if (!auxClasse.getDatProcRef().equals("Ultima")){		
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				Statement stmt2 = conn.createStatement();
				ResultSet rs2=null;
				
				
				ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
	            ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
	            ParamSistemaBeanId.PreparaParam();
	            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
	            if (MYABREVSIST.equals("GER")) sHistorico="S";
				
				ArrayList dados = new ArrayList();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd = 
					"SELECT NUM_TRANSACAO, DAT_PROC, TXT_PARTE_VARIAVEL,TXT_RESULTADO "+ 
					"FROM TSMI_LOG_BROKER "+ 
					"WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";
					if (!auxClasse.getsIN().equals("0"))
						sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
				if (!auxClasse.getDatInicio().equals(""))
					sCmd +="AND DAT_PROC > '" + auxClasse.getDatInicio()+"' "  ;
				if (!auxClasse.getDatFim().equals(""))
					sCmd +="AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
				if (!auxClasse.getNomUserName().equals(""))
					sCmd +="AND trim(NOM_USERNAME) = '" + auxClasse.getNomUserName() + "' ";						
				if (!auxClasse.getCodOrgao().equals("0"))
					sCmd +="AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
				sCmd += "AND DAT_PROC > TO_DATE('" + auxClasse.getDatProcRef() + "', 'DD/MM/YYYY:HH24:MI:SS') ";										
				
				sCmd += " AND trim(TXT_RESULTADO) <> '000'";
				
				if (sHistorico.equals("S")) {
					sCmd += " UNION "
					+ " ("
					+ "SELECT NUM_TRANSACAO, DAT_PROC, TXT_PARTE_VARIAVEL,TXT_RESULTADO  "+ 
					" FROM TSMI_LOG_BROKER_BKP@SMITPBKP "+ 
					" WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";
					if (!auxClasse.getsIN().equals("0"))
						sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
					if (!auxClasse.getDatInicio().equals(""))
						sCmd +=" AND DAT_PROC > '" + auxClasse.getDatInicio()+"' "  ;
					if (!auxClasse.getDatFim().equals(""))
						sCmd +=" AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
					if (!auxClasse.getNomUserName().equals(""))
						sCmd +=" AND trim(NOM_USERNAME) = '" + auxClasse.getNomUserName() + "' ";						
					if (!auxClasse.getCodOrgao().equals("0"))
						sCmd +=" AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
					sCmd += " AND DAT_PROC > TO_DATE('" + auxClasse.getDatProcRef() + "', 'DD/MM/YYYY:HH24:MI:SS') ";										
					
					sCmd += " AND trim(TXT_RESULTADO) <> '000'";
					sCmd += " )";
				}
				
				sCmd += " ORDER BY DAT_PROC ";				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					trans = rs.getString("NUM_TRANSACAO");
					datProc = rs.getString("DAT_PROC");
					datProc = datProc.substring(8,10) + "/" + datProc.substring(5,7) + "/" + datProc.substring(0,4) + ":" + datProc.substring(11,19);
					qtd++;
					
					if ((prim == 1) && (!acao.equals("Ant"))){
						auxClasse.getPrimTrans().add(auxClasse.getTransRef());
						auxClasse.getPrimDatProc().add(auxClasse.getDatProcRef());
						prim++;
					}
					
					result = rs.getString("TXT_RESULTADO").trim();
					if(Util.isNumber(result.substring(0,3))){
						result=result.substring(0,3);
						String sCmd2="SELECT DSC_ERRO FROM TSMI_ERRO_BROKER WHERE COD_ERRO = '"+result+"'";
						rs2 = stmt2.executeQuery(sCmd2);
						if(rs2.next())
							result=result+" - "+rs2.getString("DSC_ERRO");						
					}
					
					dados.add(rs.getString("TXT_PARTE_VARIAVEL")+result);
					if (qtd == 50) break;
					
					result="";
						
					
					
				}
				
					
					
				
				
				qtdParcial = Integer.parseInt(auxClasse.getQtdParcialReg());
				
				if (qtd != 0){
					
					auxClasse.setQtdRegs(String.valueOf(qtd));
					if (acao.equals("Prox") || acao.equals("B")) 
						qtdParcial = qtdParcial + qtd;
				}
				
				if ((prim > 1) && (!acao.equals("Ant"))){
					auxClasse.getUltTrans().add(trans);
					auxClasse.getUltDatProc().add(datProc);
					auxClasse.getQtdReg().add(String.valueOf(qtd));
				}
				
				if ((qtd == 0) && (!auxClasse.getDatProcRef().equals("Ultima"))){
					
					auxClasse.getPrimTrans().add("Ultima");
					auxClasse.getPrimDatProc().add("Ultima");
					
					auxClasse.getUltTrans().add("Ultima");
					auxClasse.getUltDatProc().add("Ultima");
					auxClasse.getQtdReg().add("Ultima");
				}
				
				if (!auxClasse.getDatProcRef().equals("Ultima")){
					auxClasse.setQtdParcialReg(String.valueOf(qtdParcial));
				}
				
				auxClasse.setDados(dados);
				
				rs.close();
				stmt.close();
				if(stmt2!=null)
					stmt2.close();
				if(rs2!=null)
					rs2.close();
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}
	
	public String BuscaTotalRegRegistro(GER.AuditoriaBean auxClasse)
	throws DaoException {
		
		Connection conn = null;
		String qtd = "0";
		
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
            ParamSistemaBeanId.PreparaParam();
            String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
            if (MYABREVSIST.equals("GER")) sHistorico="S";
			
			ArrayList dados = new ArrayList();
			
			// preparar lista os dados do Usuario por Sistema
			String sCmd =
//			"SELECT COUNT(*) AS QTD "+ 
//			"FROM ( SELECT * FROM TSMI_LOG_BROKER ORDER BY DAT_PROC, NUM_TRANSACAO) "+ 
				sCmd ="SELECT COUNT(*) AS QTD "; 
				sCmd +="FROM TSMI_LOG_BROKER ";
				sCmd +=	"WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";			
			if (!auxClasse.getsIN().equals("0"))
				sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
			if (!auxClasse.getDatInicio().equals(""))
				sCmd +="AND DAT_PROC > '" + auxClasse.getDatInicio() + "' " ;
			if (!auxClasse.getDatFim().equals(""))
				sCmd +="AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
			if (!auxClasse.getNomUserName().equals(""))
				sCmd +="AND NOM_USERNAME = '" + auxClasse.getNomUserName() + "' ";						
			if (!auxClasse.getCodOrgao().equals("0"))
				sCmd +="AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
				
				sCmd += " AND trim(TXT_RESULTADO) <> '000'";
			
			if (sHistorico.equals("S")) {
				sCmd += " UNION ";
				sCmd += " (";
				sCmd +="SELECT COUNT(*) AS QTD"; 
				sCmd +=" FROM TSMI_LOG_BROKER_BKP@SMITPBKP "; 
				sCmd +=" WHERE COD_TRANSACAO = '" +auxClasse.getCodTransacao()+"' ";				
				if (!auxClasse.getsIN().equals("0"))
					sCmd +="AND COD_ORGAO_ATUACAO ='" +auxClasse.getsIN()+"' ";
				if (!auxClasse.getDatInicio().equals(""))
					sCmd +=" AND DAT_PROC > '" + auxClasse.getDatInicio() + "' " ;
				if (!auxClasse.getDatFim().equals(""))
					sCmd +=" AND DAT_PROC < '" + auxClasse.getDatFim() + "' ";						
				if (!auxClasse.getNomUserName().equals(""))
					sCmd +=" AND NOM_USERNAME = '" + auxClasse.getNomUserName() + "' ";						
				if (!auxClasse.getCodOrgao().equals("0"))
					sCmd +=" AND COD_ORGAO_LOTACAO = '" + auxClasse.getCodOrgao() + "' ";
				
					sCmd += " trim(TXT_RESULTADO) <> '000'";
			
				sCmd += " )";
			}
			
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			if (rs.next()) {
				
				qtd = rs.getString("QTD");
			}
			
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return qtd;
	}
	
	public ArrayList getEventosRegistro()  throws DaoException {
		Connection conn =null ;					
		ArrayList eventos = new ArrayList();
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();

			
			String sCmd = "Select cod_evento,dsc_evento,cod_evento_ref from tsmi_evento " +
				          "where cod_evento_ref is not null  and ind_auditoria = 'S' order by dsc_evento";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				
				REC.EventoBean myEvent = new REC.EventoBean();
				
				myEvent.setCodEvento(rs.getString("cod_evento"));
				myEvent.setDscEvento(rs.getString("dsc_evento"));
				myEvent.setCodEventoRef(rs.getString("cod_evento_ref"));
				eventos.add(myEvent);
			}

			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return eventos;
		
	}		
	
	public void CarregaListaDet(REG.RelatArDigitTMPBean auxClasse, String numNotif)
	throws DaoException {
		
		Connection conn = null;
		
		try {
			
				conn = serviceloc.getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();
				
				ArrayList dados = new ArrayList();
				
				// preparar lista os dados do Usuario por Sistema
				String sCmd =
					"SELECT * "+ 
					"FROM TSMI_AR_DIGITALIZADO_TMP "+ 
					"WHERE NUM_NOTIFICACAO = '"+numNotif+"' "+
				    "ORDER BY DAT_DIGITALIZACAO";
				
				
				ResultSet rs = stmt.executeQuery(sCmd);
				
				while (rs.next()) {
					
					
					String codRetorno = rs.getString("CODIGO_RETORNO_VEX");
					if (codRetorno == null) codRetorno = "";
					
					String dscRetorno = "";
					String numNotificacao = rs.getString("NUM_NOTIFICACAO");
					String datDigitalizacao = Util.converteData(rs.getDate("DAT_DIGITALIZACAO"));
					String datEntrega = Util.converteData(rs.getDate("DAT_ENTREGA"));
					String indCodRetV1 = rs.getString("IND_COD_RET_V1");
					String sdados = "";
					
					dscRetorno = BuscaDscRetorno(codRetorno);
					
					sdados = Util.rPad(numNotificacao ," ",9);
					sdados += Util.rPad(datDigitalizacao ," ",12);
					sdados += Util.rPad(rs.getString("NUM_LOTE") ," ",3);
					sdados += Util.rPad(rs.getString("NUM_CAIXA") ," ",8);
					sdados += Util.rPad(datEntrega ," ",12);
					sdados += Util.rPad(indCodRetV1 ," ",1);
					sdados += Util.rPad(codRetorno+" - "+dscRetorno ," ",100);
					
					dados.add(sdados);
					
				}
				auxClasse.setDadosDet(dados);
				
				rs.close();
				stmt.close();
			
		}catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}	
	
	/**
	 * M�todo criado para colocar a data do arquivo recebido como nula
	 * e liberar o download
	 * @author Rodrigo Cunha
	 * @since 12/12/2012
	 * @param String codigoArquivo
	 * @throws DaoException
	 * @version 1.0
	 */
	public boolean liberaDownloadArquivoReg(String codigoArquivo) throws DaoException {      
		Connection conn = null;     
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			String sCmd = "select cod_arquivo from tsmi_arquivo_recebido where cod_arquivo = ?";
			if (codigoArquivo != null || !"".equals(codigoArquivo.trim())){
				PreparedStatement pStm = conn.prepareStatement(sCmd);
				pStm.setString(1, codigoArquivo);
				ResultSet rs = pStm.executeQuery();
				if(rs.next()){
					String sCmdA = "UPDATE tsmi_arquivo_recebido SET dat_download = '' , IND_ENVIO_DATA_POSTAGEM = 'N' WHERE cod_arquivo = ?";
					PreparedStatement pStmA = conn.prepareStatement(sCmdA);
					pStmA.setString(1,codigoArquivo);
					pStmA.executeUpdate();
					pStmA.close();
				}
				rs.close();
				pStm.close();
				return true;
			}else{
				return false;
			}
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (conn != null)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	
	public boolean informaDataDownloadArquivo(String codigoArquivo) throws DaoException {      
		Connection conn = null;     
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			String sCmd = "select cod_arquivo from tsmi_arquivo_recebido where cod_arquivo = ?";
			if (codigoArquivo != null || !"".equals(codigoArquivo.trim())){
				PreparedStatement pStm = conn.prepareStatement(sCmd);
				pStm.setString(1, codigoArquivo);
				ResultSet rs = pStm.executeQuery();
				if(rs.next()){
					String sCmdA = "UPDATE tsmi_arquivo_recebido SET dat_download = sysdate WHERE cod_arquivo = ?";
					PreparedStatement pStmA = conn.prepareStatement(sCmdA);
					pStmA.setString(1,codigoArquivo);
					pStmA.executeUpdate();
					pStmA.close();
				}
				rs.close();
				pStm.close();
				return true;
			}else{
				return false;
			}
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if (conn != null)
						serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { 
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 * M�todo para informar a data de postagem para o M97
	 * @param codigoEvento
	 * @param numeroAuto
	 * @param placaVeiculo
	 * @param dataPostagem
	 * @return C�digo do retorno da transa��o.
	 * @author Rodrigo Cunha Neto da Fonseca
	 * @since 26/12/2012
	 * @throws DaoException
	 * @version 1.0
	 */
	public String informaDataPostagemBroker (String codigoEvento, String numeroAuto, String placaVeiculo, String dataPostagem) throws DaoException {
		String resultado   = "";				
		try {
			String parteVar    = "";
			String indContinua = "            ";			
			parteVar = sys.Util.lPad(codigoEvento,"0",3) + sys.Util.rPad(numeroAuto," ",12)+ sys.Util.rPad(placaVeiculo," ",7)+sys.Util.lPad(dataPostagem,"0",8);
			resultado = chamaTransBRK("416",parteVar,indContinua);
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return resultado;
	}
	
}