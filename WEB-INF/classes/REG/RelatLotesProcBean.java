package REG;

import java.util.ArrayList;

import sys.DaoException;

public class RelatLotesProcBean extends sys.HtmlPopupBean {

    private ArrayList dados;
    
    private ArrayList dadosLote;

    private String codOrgao;

    private String sigOrgao;

    private String dataIni;

    private String dataFim;

    private String codLote;

    private String datLote;

    private String qtdCancel;

    private String qtdReg;

    private String qtdGrav;

    private String qtdRej;

    private String codRetorno;
    
    private String tipoConsulta;

    public RelatLotesProcBean() throws sys.BeanException {
        dados = new ArrayList();
        dadosLote = new ArrayList();
        codOrgao = "";
        sigOrgao = "";
        dataIni = sys.Util.formatedToday().substring(0, 10);
        dataFim = sys.Util.formatedToday().substring(0, 10);
        codLote = "";
        datLote = "";
        qtdCancel = "0";
        qtdReg = "0";
        qtdGrav = "0";
        qtdRej = "0";
        tipoConsulta = "";
    }

    public void setCodOrgao(String codOrgao) {
        this.codOrgao = codOrgao;
        if (codOrgao == null)
            this.codOrgao = "";
    }

    public String getCodOrgao() {
        return this.codOrgao;
    }

    public void setSigOrgao(String sigOrgao) {
        this.sigOrgao = sigOrgao;
        if (sigOrgao == null)
            this.sigOrgao = "";
    }

    public String getSigOrgao() {
        return this.sigOrgao;
    }

    public void setDataIni(String dataIni) {
        this.dataIni = dataIni;
        if (dataIni == null)
            this.dataIni = "";
    }

    public String getDataIni() {
        return this.dataIni;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
        if (dataFim == null)
            this.dataFim = "";
    }

    public String getDataFim() {
        return this.dataFim;
    }

    public void setDados(ArrayList dados) {
        this.dados = dados;
    }

    public ArrayList getDados() {
        return this.dados;
    }
    
    public void setDadosLote(ArrayList dadosLote) {
        this.dadosLote = dadosLote;
    }

    public ArrayList getDadosLote() {
        return this.dadosLote;
    }

    public void setQtdCancel(String qtdCancel) {
        this.qtdCancel = qtdCancel;
        if (qtdCancel == null)
            this.qtdCancel = "0";
    }

    public String getQtdCancel() {
        return qtdCancel;
    }

    public void setQtdReg(String qtdReg) {
        this.qtdReg = qtdReg;
        if (qtdReg == null)
            this.qtdReg = "0";
    }

    public String getQtdReg() {
        return qtdReg;
    }

    public void setDatLote(String datLote) {
        this.datLote = datLote;
        if (datLote == null)
            this.datLote = "0";
    }

    public String getDataLote() {
        return datLote;
    }

    public void setQtdGrav(String qtdGrav) {
        this.qtdGrav = qtdGrav;
        if (qtdGrav == null)
            this.qtdGrav = "0";
    }

    public String getQtdGrav() {
        return qtdGrav;
    }

    public void setQtdRej(String qtdRej) {
        this.qtdRej = qtdRej;
        if (qtdRej == null)
            this.qtdRej = "0";
    }

    public String getQtdRej() {
        return qtdRej;
    }

    public void setCodLote(String codLote) {
        this.codLote = codLote;
        if (codLote == null)
            this.codLote = "";
    }

    public String getCodLote() {
        return codLote;
    }

    public void setCodRetorno(String codRetorno) {
        this.codRetorno = codRetorno;
        if (codRetorno == null)
            this.codRetorno = "";
    }

    public String getCodRetorno() {
        return codRetorno;
    }
    
    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
        if(tipoConsulta == null)
            this.tipoConsulta = "";
    }
    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void getLotesProcessados() throws DaoException {
        Dao dao;

            try {
                dao = Dao.getInstance();
                dao.getLotesProcessados(this);

            } catch (ROB.DaoException e) {
                throw new DaoException("Erro:" + e.getMessage());
            }
    }
       
    public void mostraDetalhe() throws DaoException  {
        Dao dao;
        try {
            dao = Dao.getInstance();
            dao.mostraDetalhe(this);
        } catch (ROB.DaoException e) {
            throw new DaoException("Erro:" + e.getMessage());
        }
    }

    public String formataData(String data) {
        String novaData = "";
        novaData = data.substring(0, 2) + "/" + data.substring(2, 4) + "/"
                + data.substring(4, 8);
        return novaData;
    }





}