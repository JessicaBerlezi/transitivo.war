package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import sys.BeanException;

/**
* <b>Title:</b>        Definir Prioridade - Command<br>
* <b>Description:</b>  Permite definir indice de prioridade do processamento dos Robos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class DefinirPrioridadeCmd extends sys.Command {
	private static final String jspPadrao = "/REG/DefinirPrioridade.jsp";
	private String next;

	public DefinirPrioridadeCmd() {
		next = jspPadrao;
	}

	public DefinirPrioridadeCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException   {
		String nextRetorno = jspPadrao;
		try {	
			//Cria  Bean, se n�o existir
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean)req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

			DefinirPrioridadeBean prioridadeBean = (DefinirPrioridadeBean) req.getAttribute("prioridadeBean");
			if (prioridadeBean == null) prioridadeBean = new DefinirPrioridadeBean();
			
			//Obter e validar os paramentros recebidos
			String acao = req.getParameter("acao");
			if (acao == null) acao = ""; 
			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)atualizarDependente = "N";
			
			//Recebe parametros da combo
			String codOrgao = req.getParameter("codOrgao");
			if((codOrgao == null)||(codOrgao.equals(""))) codOrgao = "0";
			OrgaoBeanId.Le_Orgao(codOrgao,0);
			
  			if(acao.equals("busca")){
  				OrgaoBeanId.setAtualizarDependente("S");
				prioridadeBean.setCodOrgao(codOrgao);
				prioridadeBean.consultaStatusArquivo(prioridadeBean);
			}
			
			if(acao.equals("atualizar")){
				//recebe parametros da tela
				String[] nomArquivo = req.getParameterValues("nomArquivo");
				if (nomArquivo == null) nomArquivo = new String[0]; 
				String[] indPrioridade = req.getParameterValues("indPrioridade");
				if (indPrioridade == null) indPrioridade = new String[0];  
				if(nomArquivo.length != indPrioridade.length) prioridadeBean.setMsgErro("Parametros inconsistentes.");
				
				//carregando esses parametros no vetor
				Vector vIndPriori = new Vector(); 
				for (int i = 0; i < nomArquivo.length;i++) {
					DefinirPrioridadeBean bean = new DefinirPrioridadeBean() ;		
					bean.setNomArquivo(nomArquivo[i]);
					bean.setIndPrioridade(indPrioridade[i]);	  
					bean.setCodOrgao(codOrgao) ;	
						
					vIndPriori.addElement(bean) ; 					
				}
				prioridadeBean.setVetorPrioridade(vIndPriori);
				//Atualiza dados
				prioridadeBean.alterarIndicePrioridade();
				//Retorna os dados na tela
				prioridadeBean.setCodOrgao(codOrgao);
				prioridadeBean.consultaStatusArquivo(prioridadeBean);
				OrgaoBeanId.setAtualizarDependente("S");
			}
			
			req.setAttribute("prioridadeBean", prioridadeBean);	
			req.setAttribute("OrgaoBeanId", OrgaoBeanId);	
			req.setAttribute("atualizarDependente", atualizarDependente);
		}
		catch (BeanException e) {
			throw new sys.CommandException("DefinirPrioridadeCmd 001: " + e.getMessage());
		} catch (sys.DaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nextRetorno;
		
	}
}
