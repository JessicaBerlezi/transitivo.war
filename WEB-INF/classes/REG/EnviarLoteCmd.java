package REG;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;

/**
 * <b>Title:</b>        SMIT - Upload de Arquivos de Multas<br>
 * <b>Description:</b>  Comando para Upload de Arquivos de Multas de Tr�nsito<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luiz Medronho
 * @version 1.0
 */

public class EnviarLoteCmd extends sys.Command {
    
    private static final String jspPadrao = "/REG/EnviarLote.jsp";
    private String next;
    
    public EnviarLoteCmd()  {
        next = jspPadrao;
    }
    
    public EnviarLoteCmd(String next) {
        this.next = next;
    }
    
    //--------------------------------------------------------------------------
    public String execute(HttpServletRequest req, sys.UploadFile upload)
    throws sys.CommandException {
        String nextRetorno = jspPadrao;
        try { 
            nextRetorno = jspPadrao;
            String acao = upload.getParameter("acao");
            if (acao == null) acao = "";
            HttpSession session = req.getSession();
            ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
            if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
            
            ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
			
			ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
            
			String Origem = usrFunc.getJ_sigFuncao();
//			Wellem 06-03-2006
			if("REG0120".equals(Origem))
				Origem="BKP";
			//==================daqui em diante toda apari��o de (BKP) refere-se ao tratamento dos arquivos de Backup do SMIT_DOL
			
            String codOrgaoAtuacao = usrLogado.getCodOrgaoAtuacao();
            if (codOrgaoAtuacao.length()<6) {
                codOrgaoAtuacao=("000000"+codOrgaoAtuacao);
                codOrgaoAtuacao = codOrgaoAtuacao.substring(codOrgaoAtuacao.length()-6,codOrgaoAtuacao.length());
            }
            REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
            if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			
            
            REG.ArquivoDolBean arqRec = new ArquivoDolBean();	
            if (!"BKP".equals(Origem))
            	arqRec.setProximoArquivo(proximoArquivo(ParamOrgaoId,codOrgaoAtuacao));
            
            if ("".equals(acao.toUpperCase())) {
                req.setAttribute("proximoArquivo",arqRec.getProximoArquivo());
            }
            if ("UPLOAD".equals(acao.toUpperCase())) {
                BufferedReader br = null;            	
                nextRetorno = "/REG/ProtocoloArqDol.jsp";
                //Criticar os arquivos enviados
                try {
                    int begin = 0;
                    int numControleLote=0;
                    if (!"BKP".equals(Origem))
                    	 numControleLote = Integer.parseInt(ParamOrgaoId.getParamOrgao("PROXIMO_DE01_LOTE","00001","10"));
                    
                    File file = upload.getFile(req,"arqUpload");
                    br = new BufferedReader(new FileReader(file));
                    String linha;
                    linha = br.readLine();
                    arqRec.setCodIdentArquivo(linha.substring(0, 4));
                    arqRec.setTipHeader(linha.substring(4, 8));
                    arqRec.setNumAnoExercicio(linha.substring(8, 12));
                    arqRec.setCodOrgao(linha.substring(12, 18));
                    arqRec.setNumControleArq(linha.substring(18, 23));
                    arqRec.setNumVersao(linha.substring(28, 38));
                    arqRec.setDscPortaria59(linha.substring(38, 41));
                    
                   
                    arqRec.setCodStatus("0");
                    arqRec.setCodOrgaoLotacao(usrLogado.getOrgao().getCodOrgao());
                    arqRec.setSigOrgaoLotacao(usrLogado.getOrgao().getSigOrgao());
                    arqRec.setNomUsername(usrLogado.getNomUserName());
                    arqRec.setNomArquivo(file.getName());
                    arqRec.setQtdLote(linha.substring(23, 28));
                    if (!"BKP".equals(Origem))
                    	arqRec.critica(ParamOrgaoId,codOrgaoAtuacao);
                    	
                    if (arqRec.getErroArquivo().size() > 0)
                        arqRec.setCodStatus("9");
                    LoteDolBean loteRec = null;
                    List lote = new ArrayList();
                    List vetLinha = null;
                    Vector erroLote = new Vector();
                    int TotReg=0;
                    while( (linha = br.readLine()) != null){
                        linha = linha.replaceAll("'", " ");
                        
                        if ("0000".compareTo(linha.substring(4,8)) == 0) {
                            // Verifica se � a primeira vez que executa o loop
                        	if (!"BKP".equals(Origem)){
	                        	if( begin == 1 ){
	                                // Critica a quantidade de linhas com o especificado no Header do Lote
	                                if(Integer.parseInt(loteRec.getQtdReg()) != vetLinha.size()){
	                                    arqRec.setCodStatus("9");
	                                    
	                                    if( Integer.parseInt(loteRec.getQtdReg()) < vetLinha.size())
	                                        arqRec.getErroArquivo().add(loteRec.getNumLote() + "|" + "024#N�mero de Linhas Inferior ao Especificado: " + loteRec.getQtdReg() + " \n Presente: " + vetLinha.size());
	                                    else
	                                        arqRec.getErroArquivo().add(loteRec.getNumLote() + "|" + "025#N�mero de Lotes Superior ao Especificado: " + loteRec.getQtdReg() + " | Presente: " + vetLinha.size());
	                                }
	                            }
                       	}	
                            loteRec = new LoteDolBean();
                            loteRec.setCodIdentArquivo(linha.substring(0,4));
                            loteRec.setNumAnoExercicio(linha.substring(8, 12));
                            loteRec.setNumLote(linha.substring(18, 23));
                            loteRec.setQtdReg(linha.substring(23, 28));
                            
                            loteRec.setCodUnidade(linha.substring(28, 32));
                            loteRec.setIndEmissaoNotifDetran(linha.substring(32,33));
                            loteRec.setDatLote(linha.substring(33,41));
                            loteRec.setCodLoteDol(linha.substring(41,46));
                            loteRec.setCodStatus("0");
                            
                            TotReg+=Integer.parseInt(loteRec.getQtdReg());
                            
                            
                            if (!"BKP".equals(Origem)){
	                            erroLote = loteRec.critica(ParamOrgaoId,codOrgaoAtuacao);
	                            
	                            // Carrega a lista de erros do arquivo
	                            for(int i=0;i<erroLote.size();i++)
	                                arqRec.getErroArquivo().add(erroLote.elementAt(i));
	                            
	                            // Critica o codOrg�o do Lote
	                            if (arqRec.getCodOrgao().equals(linha.substring(12, 18)) == false){
	                                if( arqRec.getErroArquivo().equals("9") == false)
	                                    arqRec.getErroArquivo().add(loteRec.getNumLote() + "|" + "026#�rg�o Autuador Inv�lido: '" + arqRec.getCodOrgao() + "'");			
	                            }
	                            
	                            if (loteRec.getErroArquivo().size() > 0)
	                                arqRec.setCodStatus("9");
                           } 
                            vetLinha = new ArrayList();
                            loteRec.setLinhaLoteDol(vetLinha);
                            lote.add(loteRec);
                            numControleLote++;
                            begin = 1;
                        } 
                        else vetLinha.add(linha);
                    }
                    if (!"BKP".equals(Origem)){
	                    // Critica a quantidade de lote com o especificado no Header do Arquivo
	                    if(Integer.parseInt(arqRec.getQtdLote()) != lote.size()){
	                        arqRec.setCodStatus("9");
	                        if( Integer.parseInt(arqRec.getQtdLote()) < lote.size())
	                            arqRec.getErroArquivo().add(arqRec.getNumControleArq() + "|" + "014#N�mero de Lotes Inferior ao Especificado: " + arqRec.getQtdLote() + " \n Presente: " + lote.size());
	                        else
	                            arqRec.getErroArquivo().add(arqRec.getNumControleArq() + "|" + "015#N�mero de Lotes Superior ao Especificado: " + arqRec.getQtdLote() + " | Presente: " + lote.size());
	                    }
                	}
                    arqRec.setLote(lote);
                    arqRec.setQtdReg(String.valueOf(TotReg));
                    if (!"BKP".equals(Origem))
                    {
                    	arqRec.isInsert(ParamOrgaoId,numControleLote,"");
//                    	Gravo a c�pia do arquivo que subiu
    					String NomeArquivo=arqRec.getNomArquivoDir();
    					if (NomeArquivo.lastIndexOf(".")== - 1)
    						NomeArquivo+=".txt";
    					else
    						NomeArquivo = NomeArquivo.substring(0,NomeArquivo.lastIndexOf(".")) + ".txt";					
                    	String	arqOrigem = file.getPath();
                    	String  arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
						FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));
                    }
                    else{
                    	if(arqRec.isInsert(ParamOrgaoId,numControleLote,"BKP")){                     
							String NomeArquivo=arqRec.getNomArquivoDir();
							if (NomeArquivo.lastIndexOf(".")== - 1)
								NomeArquivo = NomeArquivo + ".bkp";
							else
								NomeArquivo = NomeArquivo.substring(0,NomeArquivo.lastIndexOf(".")) + ".bkp";							
							String arqOrigem = file.getPath();
							String arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
							FileUtils.copyFile(new File(arqOrigem), new File(arqDestino)); 	
							
							//Gravo a c�pia do arquivo que subiu
							arqOrigem = file.getPath();
							arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + arqRec.getNomArquivoBkp();
							FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));
                    	}
                    }
                } catch (IOException e) {
                    arqRec.setMsgErro("UpLoad ==> Erro ao ler arquivo: "+ e);
                } //FIM try
                finally
                {
                	if (br!=null) br.close();
                }
            }
            req.setAttribute("ArquivoDolId",arqRec);
        } catch (Exception ue) {
            throw new sys.CommandException("UploadCommand 001: " + ue.getMessage());
        }
        return nextRetorno;
    }
    
    public String execute(HttpServletRequest req) throws sys.CommandException {
        String acao = req.getParameter("acao");
        if (acao == null) acao = "";
        try {
            HttpSession session = req.getSession();
            ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
            if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
            String codOrgaoAtuacao = usrLogado.getCodOrgaoAtuacao();
            if (codOrgaoAtuacao.length()<6) {
                codOrgaoAtuacao=("000000"+codOrgaoAtuacao);
                codOrgaoAtuacao = codOrgaoAtuacao.substring(codOrgaoAtuacao.length()-6,codOrgaoAtuacao.length());
            }
            REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
            if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			
            ArquivoDolBean arqRec = new ArquivoDolBean();		
            arqRec.setProximoArquivo(proximoArquivo(ParamOrgaoId,codOrgaoAtuacao));
            if ("".equals(acao.toUpperCase())) {
                req.setAttribute("proximoArquivo",arqRec.getProximoArquivo());
            }
            req.setAttribute("ArquivoDolId",arqRec);
        } catch (Exception ue) {
            throw new sys.CommandException("UploadCommand 000: " + ue.getMessage());
        }
        String nextRetorno = jspPadrao;
        return nextRetorno;
    }
    
    public String proximoArquivo(REC.ParamOrgBean myParam,String codOrgaoAtuacao) 
    throws sys.CommandException {
        String proxArq = "";
        try {
            proxArq = "DE01"+myParam.getParamOrgao("PROCESSO_ANO","2004","2")+codOrgaoAtuacao+myParam.getParamOrgao("PROXIMO_DE01","00001","10");
        }
        catch (Exception ue) {
            throw new sys.CommandException("Enviar Lote 002: " + ue.getMessage());
        }
        
        return proxArq ;
    }
}
