package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas UF<br>
* <b>Description:</b>  Comando para Consultar UF's<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class UFCmd extends sys.Command {

	private static final String jspPadrao = "/REG/UFConsulta.jsp";

	private String next;

	public UFCmd() {
		next = jspPadrao;
	}

	public UFCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();
		Vector vCodUF = new Vector();

		try {

			TAB.UFBeanCol UFbc = new TAB.UFBeanCol();
			UFbc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("UFBeanColId", UFbc);

		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
