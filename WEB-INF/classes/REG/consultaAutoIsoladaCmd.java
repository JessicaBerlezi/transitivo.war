package REG;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class consultaAutoIsoladaCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REG/consultaAutoIsolada.jsp" ;  
   
  public consultaAutoIsoladaCmd() {
    next             =  jspPadrao;
  }

  public consultaAutoIsoladaCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {

  	String nextRetorno  = next ;
    try {  
    	// cria os Beans de sessao, se n�o existir
    	HttpSession session   = req.getSession() ;								
    	ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
    	ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
    	if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  				
    	REC.ParamOrgBean ParamOrgaoId        = (REC.ParamOrgBean)session.getAttribute("REC.ParamOrgBeanId") ;
    	if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			
    	
    	//Carrego os Beans
    	REC.consultaAutoBean consultaAutoId                 = (REC.consultaAutoBean)session.getAttribute("consultaAutoId") ;
    	if (consultaAutoId==null)  	consultaAutoId       = new REC.consultaAutoBean() ;	  	
    	consultaAutoId.setAbrevSist(UsuarioFuncBeanId.getAbrevSistema());	
    	consultaAutoId.setSigFuncao(req.getParameter("j_sigFuncao"));			  		
    	
    	String acao    = req.getParameter("acao");  
    	if (acao==null)	{
    		session.removeAttribute("consultaAutoId") ;
    		consultaAutoId       = new REC.consultaAutoBean() ;
    		acao = " ";
    	}
    	String numAutoInfracao = req.getParameter("numAutoInfracao");
    	if (numAutoInfracao==null) numAutoInfracao="";
    	
    	consultaAutoId.setNumAutoInfracao(numAutoInfracao.trim());
    	consultaAutoId.setNumCPFEdt(req.getParameter("numCpf"));
    	consultaAutoId.setNumPlaca(req.getParameter("numPlaca"));
    	consultaAutoId.setNumRenavam(req.getParameter("numRenavam"));
    	consultaAutoId.setNumProcesso(req.getParameter("numProcesso"));
    	consultaAutoId.setDatInfracaoDe(req.getParameter("De"));
    	consultaAutoId.setDatInfracaoAte(req.getParameter("Ate"));
    	consultaAutoId.setIndFase(req.getParameter("indFase"));
    	String Sit9 = req.getParameter("Sit9");
    	if (Sit9==null) Sit9="";
    	if ("9".equals(Sit9)) consultaAutoId.setIndSituacao("9") ;
    	else {
    		String Sit1 = req.getParameter("Sit1");
    		if (Sit1==null) Sit1="";
    		String Sit2 = req.getParameter("Sit2");
    		if (Sit2==null) Sit2="";
    		String Sit3 = req.getParameter("Sit3");
    		if (Sit3==null) Sit3="";
    		String Sit4 = req.getParameter("Sit4");
    		if (Sit4==null) Sit4="";
    		consultaAutoId.setIndSituacao(Sit1+Sit2+Sit3+Sit4) ;					
    	}
    	consultaAutoId.setIndPago(req.getParameter("indPago"));
    	consultaAutoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
    	if (acao.equals("Classifica")) 
    		consultaAutoId.Classifica(req.getParameter("ordem"));
    	if (acao.equals("ConsultaAuto")) { 
    		consultaAutoId.ConsultaAutos(UsrLogado)	;
    		if (consultaAutoId.getAutos().size()==0)
    			req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");
    		
    		List AutosPendentes = new ArrayList();
    		AutosPendentes=consultaAutoId.getAutosPendentes(consultaAutoId.getNumPlaca(),consultaAutoId.getNumAutoInfracao());
    		if (AutosPendentes==null) AutosPendentes = new ArrayList();
    		
    		
    		List AutosPendentesDol = new ArrayList();
    		AutosPendentesDol=consultaAutoId.getAutosPendentesDol(consultaAutoId.getNumPlaca(),consultaAutoId.getNumAutoInfracao());
    		if (AutosPendentesDol==null) AutosPendentesDol = new ArrayList();
    		
    		req.setAttribute("AutosPendentes",AutosPendentes);
    		req.setAttribute("AutosPendentesDol",AutosPendentesDol);
    		session.setAttribute("consultaAutoId",consultaAutoId) ;
    		
    	}		 
    	if (acao.equals("Novo"))  		  consultaAutoId = new REC.consultaAutoBean() ;
    	if  (acao.equals("ImprimeAuto"))  {		
    		if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");				  	    
    		nextRetorno = "/REC/consultaAutoImp.jsp" ;
    	}	
    	if (acao.equals("R"))	{
    		nextRetorno ="" ;
    		consultaAutoId       = new REC.consultaAutoBean() ;	  				
    		session.removeAttribute("consultaAutoId") ;
    	}
    	else session.setAttribute("consultaAutoId",consultaAutoId) ;
	}
    catch (Exception ue) {
	      throw new sys.CommandException("consultaAutoIsoladaCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }  
  

	
}
