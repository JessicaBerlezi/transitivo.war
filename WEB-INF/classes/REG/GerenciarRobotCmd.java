package REG;

import javax.servlet.http.HttpServletRequest;

import sys.BeanException;


/**
* <b>Title:</b>        Para Robot - Command<br>
* <b>Description:</b>  Permite o parar o processamento dos Robot, bloqueando-os<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class GerenciarRobotCmd extends sys.Command {
	private static final String jspPadrao = "/REG/gerenciarRobot.jsp";
	private String next;

	public GerenciarRobotCmd() {
		next = jspPadrao;
	}

	public GerenciarRobotCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		GerenciarRobotBean paraRobot = (GerenciarRobotBean) req.getAttribute("paraRobot");
		if (paraRobot == null) paraRobot = new GerenciarRobotBean();
		
		//Obter e validar os paramentros recebidos
		String acao = req.getParameter("acao");
		if (acao == null) acao = ""; 
		String atualizarDependente = req.getParameter("atualizarDependente");
		if (atualizarDependente == null)atualizarDependente = "N";
		
		//Recebe parametros 
		String nomRobo = req.getParameter("nomRobo");
		if (nomRobo == null) nomRobo = "";
		String situacao = req.getParameter("situacao");
		if (situacao == null) situacao = "";
		String liberarBotao = req.getParameter("liberarBotao");
		if (liberarBotao == null) liberarBotao = "N";
		String pararBotao = req.getParameter("pararBotao");
		if (pararBotao == null) pararBotao = "N";


		//retorno do input na tela
		paraRobot.setNomProcesso(nomRobo);
	
		try {
			if(acao.equals("busca")) {
				paraRobot.consultaStatusRobot(nomRobo, paraRobot);
				if (paraRobot.getSituacaoRobo().equals("1")){
				   //gerencia visibilidade dos bot�es
				   liberarBotao ="S";
				   pararBotao = "N";
				}
				else{
                   //gerencia visibilidade dos bot�es
				   liberarBotao ="N";
				   pararBotao = "S";
				}
			} 
			
			if(acao.equals("liberar")){
				paraRobot.liberarRobot(nomRobo);
				paraRobot.consultaStatusRobot(nomRobo, paraRobot);
				atualizarDependente = "S";
 				//gerencia visibilidade dos bot�es
				liberarBotao ="N";
				pararBotao = "S";
			}
			
			if(acao.equals("parar")){
				paraRobot.pararRobot(nomRobo);
				paraRobot.consultaStatusRobot(nomRobo, paraRobot);
				atualizarDependente = "S";
                //gerencia visibilidade dos bot�es
				liberarBotao ="S";
				pararBotao = "N";
			}
			
			if (acao.equals("V")) {
				atualizarDependente = "N";
			}

			
		} catch (BeanException e) {
			throw new sys.CommandException("GerenciarRobotCmd 001: " + e.getMessage());
		}   
		req.setAttribute("liberarBotao", liberarBotao);
		req.setAttribute("pararBotao", pararBotao);
		req.setAttribute("nomRobo", nomRobo);
		req.setAttribute("paraRobot", paraRobot);
		req.setAttribute("situacao", situacao);	
		req.setAttribute("atualizarDependente", atualizarDependente);
		return nextRetorno;
		
	}
	
	
}
