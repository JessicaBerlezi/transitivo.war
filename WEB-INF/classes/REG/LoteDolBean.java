package REG;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Arquivo Recebido via Upload<br>
* <b>Description:</b>  	Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Luiz Medronho
* @version 				1.0
*/

public class LoteDolBean extends sys.HtmlPopupBean  {

	private String codArquivo;	
	private String tipHeader;	
	private String codIdentArquivo;
	private String numLote;
	//wellem 06-03-2006
	private String codArquivoDol;
	private String qtdGrav;
	private String qtdCanc;
	private Vector linhaDol;
	private String codLoteDol;
//==================================
	
	private String numAnoExercicio;
	private String datLote;
	private String indEmissaoNotifDetran;
	private String codStatus;  // 0 - Recebido, 9-Rejeitado
	private String codUnidade;
	
	private String qtdReg;
	private String qtdLinhaErro;
	private String codLote;
	private List linhaLoteDol;	
	private Vector erroArquivo;
	private Connection conn;

	public LoteDolBean()  throws sys.BeanException {

	  	super();
	  	setTabela("TSMI_LOTE_DOL");	
	  	setPopupWidth(35);
		
	  	codArquivo	           = "0";		
	  	tipHeader              = "9";
		
		codIdentArquivo        = "DE01";
		numLote			       = "0";
		numAnoExercicio        = "2004";
		datLote				   = "00000000";
		indEmissaoNotifDetran  = "E";
		
		codStatus              = "9";
		codUnidade			   = "0";
		qtdReg                 = "0";
		qtdLinhaErro           = "0";

		codLote				   = "0";
		linhaLoteDol        = new ArrayList();
		erroArquivo = new Vector();	
		
//		wellem 06-03-2006
		codArquivoDol	       = "0";
		qtdGrav                = "0";
		qtdCanc                = "0";
		linhaDol = new Vector();
		codLoteDol			   = "0";
//		==================================		
		
	}	
//--------------------------------------------------------------------------  
	public void setCodArquivo(String codArquivo)  {
		this.codArquivo=codArquivo ;
		if (codArquivo==null) this.codArquivo="" ;
	}  
	public String getCodArquivo()  {
		return this.codArquivo;
	}
//--------------------------------------------------------------------------  
	public void setTipHeader(String tipHeader)  {
		this.tipHeader=tipHeader ;
		if (tipHeader==null) this.tipHeader="9" ;
	}  
	public String getTipHeader()  {
		return this.tipHeader;
	}		
//---------------------------------------------------------------------------	
	public void setCodIdentArquivo(String codIdentArquivo){
		if (codIdentArquivo==null) this.codIdentArquivo = "";
		this.codIdentArquivo = codIdentArquivo;
	}
	
	public String getCodIdentArquivo() {
		return this.codIdentArquivo;
	}
//--------------------------------------------------------------------------  
	public void setNumLote (String numLote) {
		this.numLote = numLote;
		if (numLote==null) this.numLote="00000" ;
		if (this.numLote.length()<5) {
			this.numLote=("00000"+this.numLote);
			this.numLote=this.numLote.substring(this.numLote.length()-5,this.numLote.length());
		}
	}
	public String getNumLote() {
		return this.numLote;
	}
//--------------------------------------------------------------------------  
	public void setNumAnoExercicio (String numAnoExercicio)	{
		this.numAnoExercicio = numAnoExercicio;
		if (numAnoExercicio==null) this.numAnoExercicio="2004" ;
		if (this.numAnoExercicio.length()<4) {
			this.numAnoExercicio=("0000"+this.numAnoExercicio);
			this.numAnoExercicio=this.numAnoExercicio.substring(this.numAnoExercicio.length()-4,this.numAnoExercicio.length());
		}
	}
	public String getNumAnoExercicio() {
		return this.numAnoExercicio;
	}
//	--------------------------------------------------------------------------  
	public void setDatLote(String datLote)	{
		this.datLote = datLote;
		if (datLote == null) this.datLote="00000000";
		if (this.datLote.length() < 8)
			this.datLote = sys.Util.lPad(datLote, "0", 8);
	}
	public String getDatLote() {
		return this.datLote;
	}	
//--------------------------------------------------------------------------  
	public void setIndEmissaoNotifDetran (String indEmissaoNotifDetran) {
		this.indEmissaoNotifDetran = indEmissaoNotifDetran;
		if (indEmissaoNotifDetran==null) this.indEmissaoNotifDetran="E" ;	
	}
	public String getIndEmissaoNotifDetran() {
		return this.indEmissaoNotifDetran;
	}
//--------------------------------------------------------------------------  
	public void setCodStatus(String codStatus) 	{
		this.codStatus = codStatus;
		if (codStatus==null) this.codStatus="9" ;	
	}
	public String getCodStatus() {
		return this.codStatus;
	}
//--------------------------------------------------------------------------
	public void setCodUnidade(String codUnidade){
		this.codUnidade = codUnidade;
		if (codUnidade==null) this.codUnidade="0" ;		
	}
	public String getCodUnidade(){
		return codUnidade;
	}
//--------------------------------------------------------------------------
	public void setCodLote(String codLote){
		this.codLote = codLote;
		if (codLote==null) this.codLote="0" ;		
	}
	public String getCodLote(){
		return codLote;
	}
//--------------------------------------------------------------------------  
	public void setQtdReg (String qtdReg){
		this.qtdReg = qtdReg;
		if (qtdReg==null) this.qtdReg="0" ;	
	}
	public String getQtdReg() 	{
		return this.qtdReg;
	}
//--------------------------------------------------------------------------
	public void setQtdLinhaErro (String qtdLinhaErro){
		this.qtdLinhaErro = qtdLinhaErro;
		if (qtdLinhaErro==null) this.qtdLinhaErro="0" ;	
	}
	public String getQtdLinhaErro() 	{
		return this.qtdLinhaErro;
	}
//--------------------------------------------------------------------------  
	public void setLinhaLoteDol(List linhaLoteDol) {
		this.linhaLoteDol = linhaLoteDol;
		if (linhaLoteDol==null) this.linhaLoteDol= new Vector() ;	
	}
	public List getLinhaLoteDol() 	{
		return this.linhaLoteDol;
	}
//--------------------------------------------------------------------------  
	public void setErroArquivo(Vector erroArquivo) {
		this.erroArquivo = erroArquivo;
		if (this.erroArquivo==null) this.erroArquivo = new Vector();
	}
	public Vector getErroArquivo() 	{
		return erroArquivo;
	}
//	--------------------------------------------------------------------------  
	  public void setConn(Connection conn) {
		  this.conn = conn;
	  }
	  public Connection getConn() 	{
		  return conn;
	  }
	  
	  public String getCodArquivoDol() {
			return codArquivoDol;
		}
		public void setCodArquivoDol(String codArquivoDol) {
			this.codArquivoDol = codArquivoDol;
		}
	
		public String getQtdGrav() {
			return qtdGrav;
			
		}
		public void setQtdGrav(String qtdGrav) {
			this.qtdGrav = qtdGrav;
			if (qtdGrav==null) this.qtdGrav="0" ;
		}

		public String getQtdCanc() {
			return qtdCanc;
		}
		public void setQtdCanc(String qtdCanc) {
			this.qtdCanc = qtdCanc;
			if (qtdCanc==null) this.qtdCanc="0" ;
		}

		public void setLinhaDol(Vector linhaDol) {
			this.linhaDol = linhaDol;
			if (this.linhaDol==null) this.linhaDol = new Vector();
		}
		public Vector getLinhaDol() 	{
			return linhaDol;
		}
		
		public String getCodLoteDol() {
			return codLoteDol;
		}
		public void setCodLoteDol(String codLoteDol) {
			this.codLoteDol = codLoteDol;
			if (this.codLoteDol==null || this.codLoteDol.trim().length()==0) this.codLoteDol="0" ;
		}
	  
	/**
	 * M�todo para Alterar Lote na base de dados
	 */
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LoteUpdateDol(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Lote n�o atualizado: " + this.getCodLote() + " \n") ;
		    vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}	
//----------------------------------------------------------------------------	
	  
	/**
	* M�todo para Criticar Arquivo 
	* Gera vetor com mensagens de erro.
	*/
   public Vector critica(REC.ParamOrgBean paramOrgaoId, String codOrgao) throws Exception{
		Vector vetCrit = new Vector();
		boolean codOrgaoOK = false;
		//Critica do Header
		if ((this.getTipHeader().equals("9")) == false) {				
			vetCrit.addElement(numLote + "|" + "016#Tipo de Header do Lote inv�lido: '" + this.getTipHeader() + "'" );
		}
		// Critica do Ano do Exerc�cio
		if ((this.getNumAnoExercicio().trim().equals(null) == true)) {
			vetCrit.addElement(numLote + "|" + "017#Exerc�cio do Movimento n�o preenchido no Header: '" + this.getNumAnoExercicio() + "'");
		}
		else{	
			if (paramOrgaoId.getParamOrgao("PROCESSO_ANO","2004","2").equals(numAnoExercicio) == false) {
				vetCrit.addElement(numLote + "|" + "018#Exerc�cio do Movimento Inv�lido: '" + this.getNumAnoExercicio() + "'");
			}
		}
		//Critica o N�mero de Controle
		if ((this.getNumLote().trim().equals(null) == true)) {
			vetCrit.addElement(numLote + "|" + "019#N�mero de Controle do Lote n�o preenchido: '" + this.getNumLote() + "'");
		}
		else{ 
			if (sys.Util.isNumber(this.getNumLote()) == false) 
				vetCrit.addElement(numLote + "|" + "020#N�mero de Controle do Lote Inv�lido: '" + this.getNumLote() + "'");
		}		
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) 
			vetCrit.addElement(numLote + "|" + "022#Quantidade de Registros do Lote n�o preenchida: '" + this.getQtdReg() + "'");
		
		else{
			if (sys.Util.isNumber(this.getQtdReg()) == false) 
				vetCrit.addElement(numLote + "|" + "023#Quantidade de Registros do Lote Inv�lida: '" + this.getQtdReg() + "'");
		}
		// Critica a Data do Lote
		if ((this.getDatLote().trim().equals(null) == true)) {
			vetCrit.addElement(numLote + "|" + "024#Data do Lote n�o preenchida no Header: '" + this.getDatLote() + "'");
		}		
		
		setErroArquivo(vetCrit);
		return vetCrit;
   }

   public boolean isUpdate(String Origem){
	   boolean bOk = true ;
	   Vector vErro = new Vector() ;
	   try {
		   Dao dao = Dao.getInstance();
		   dao.LoteUpdateDol(this,Origem) ;
	   }
	   catch (Exception e) { 
		   vErro.addElement("Lote n�o atualizado: " + this.getCodLote() + " \n") ;
		   vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
		   bOk = false;
	   }
	   this.setMsgErro(vErro);
	   return bOk;  
   }

   public boolean isUpdate(String Origem, String canc){
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LoteUpdateDol(this,Origem,canc) ;
		}
		catch (Exception e) { 
			vErro.addElement("Lote n�o atualizado: " + this.getCodLote() + " \n") ;
		    vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}   
}