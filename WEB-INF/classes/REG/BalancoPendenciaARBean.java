package REG;

import java.util.ArrayList;
import java.util.List;


public class BalancoPendenciaARBean extends sys.HtmlPopupBean {
	private String datInicio;
	private String datBase;
	private String datFim;
	private String mes;
	private String ano;
	private String anoAnt;	
	private String datReferencia;
	private String datContrato;
	
	private String qtdTotalAnterior;
	private String qtdTotalEnviada;
	private String qtdTotalRetorna;
	private String qtdSaldoRetornar;
	
	private String qtdTotalFaturar;
	private String qtdTotalCredito;
	private String qtdTotalRecuperadoServico;
	private String qtdTotalPrimeiroRetorno;
	private String qtdTotalRecuperadoEntrega;
	private String qtdTotalDuplicata;
	private String qtdTotalRetornoDuplicata;
	private List<BalancoPendenciaARBean> ListRetornoMes;
	private List<BalancoPendenciaARBean> ListCodigoRetornoMes;
	
	public BalancoPendenciaARBean() throws sys.BeanException {
		super();
		datInicio                 = "";
		datBase                   = "01/04/2007";
		datFim                    = "";
		mes                       = "";
		ano                       = "";
		anoAnt                    = "";
		datReferencia             = sys.Util.formatedToday().substring(0,10);
		datContrato               = "";
		qtdTotalAnterior          = "0";
		qtdTotalEnviada           = "0";
		qtdTotalRetorna           = "0";
		qtdSaldoRetornar          = "0";
		qtdTotalFaturar           = "0";
		qtdTotalCredito           = "0";
		qtdTotalRecuperadoServico = "0";
		qtdTotalPrimeiroRetorno   = "0";
		qtdTotalRecuperadoEntrega = "0";
		qtdTotalDuplicata         = "0";
		qtdTotalRetornoDuplicata  = "0";
		ListCodigoRetornoMes   = new ArrayList<BalancoPendenciaARBean>(); 
		ListRetornoMes         = new ArrayList<BalancoPendenciaARBean>(); 
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		if (ano==null) ano="";
		this.ano = ano;
	}

	public String getAnoAnt() {
		return anoAnt;
	}

	public void setAnoAnt(String anoAnt) {
		if (anoAnt==null) anoAnt="";
		this.anoAnt = anoAnt;
	}
	
	public String getDatReferencia() {
		return datReferencia;
	}

	public void setDatReferencia(String datReferencia) {
		if (datReferencia==null) datReferencia="";
		this.datReferencia = datReferencia;
	}

	public List getListRetornoMes() {
		return ListRetornoMes;
	}

	public void setListRetornoMes(List<BalancoPendenciaARBean> listRetornoMes) {
		if (listRetornoMes==null) listRetornoMes=new ArrayList<BalancoPendenciaARBean>();
		ListRetornoMes = listRetornoMes;
	}

	public BalancoPendenciaARBean getRetornos(Integer i) 	{
		return this.ListRetornoMes.get(i);
	}

	public List<BalancoPendenciaARBean> getListCodigoRetornoMes() {
		return ListCodigoRetornoMes;
	}

	public void setListCodigoRetornoMes(List<BalancoPendenciaARBean> listCodigoRetornoMes) {
		if (listCodigoRetornoMes==null) listCodigoRetornoMes=new ArrayList<BalancoPendenciaARBean>();
		ListCodigoRetornoMes = listCodigoRetornoMes;
	}

	public BalancoPendenciaARBean getCodigoRetornos(Integer i) 	{
		return this.ListCodigoRetornoMes.get(i);
	}
	
	public String getQtdTotalEnviada() {
		return qtdTotalEnviada;
	}

	public void setQtdTotalEnviada(String qtdTotalEnviada) {
		if (qtdTotalEnviada==null) qtdTotalEnviada="0";
		this.qtdTotalEnviada = qtdTotalEnviada;
	}

	
	public void LeBanlancoPendenciaAR() throws sys.DaoException {
		try {
			Dao dao = Dao.getInstance();
			dao.LeBanlancoPendenciaAR(this);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}

	public String getDatFim() {
		return datFim;
	}

	public void setDatFim(String datFim) {
		if (datFim==null) datFim="";
		this.datFim = datFim;
	}

	public String getDatInicio() {
		return datInicio;
	}

	public void setDatInicio(String datInicio) {
		if (datInicio==null) datInicio="";
		this.datInicio = datInicio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		if (mes==null) mes="";
		this.mes = mes;
	}

	public String getDatBase() {
		return datBase;
	}

	public void setDatBase(String datBase) {
		if (datBase==null) datBase="";
		this.datBase = datBase;
	}

	public String getDatContrato() {
		return datContrato;
	}

	public void setDatContrato(String datContrato) {
		if (datContrato==null) datContrato="04/2007";
		this.datContrato = datContrato;
	}

	public String getQtdTotalAnterior() {
		return qtdTotalAnterior;
	}

	public void setQtdTotalAnterior(String qtdTotalAnterior) {
		if (qtdTotalAnterior==null) qtdTotalAnterior="0";
		this.qtdTotalAnterior = qtdTotalAnterior;
	}

	public String getQtdTotalCredito() {
		return qtdTotalCredito;
	}

	public void setQtdTotalCredito(String qtdTotalCredito) {
		if (qtdTotalCredito==null) qtdTotalCredito="0";
		this.qtdTotalCredito = qtdTotalCredito;
	}

	public String getQtdTotalDuplicata() {
		return qtdTotalDuplicata;
	}

	public void setQtdTotalDuplicata(String qtdTotalDuplicata) {
		if (qtdTotalDuplicata==null) qtdTotalDuplicata="0";
		this.qtdTotalDuplicata = qtdTotalDuplicata;
	}

	public String getQtdTotalFaturar() {
		return qtdTotalFaturar;
	}

	public void setQtdTotalFaturar(String qtdTotalFaturar) {
		if (qtdTotalFaturar==null)qtdTotalFaturar="0";
		this.qtdTotalFaturar = qtdTotalFaturar;
	}
	
	public String getQtdTotalPrimeiroRetorno() {
		return qtdTotalPrimeiroRetorno;
	}

	public void setQtdTotalPrimeiroRetorno(String qtdTotalPrimeiroRetorno) {
		if (qtdTotalPrimeiroRetorno==null) qtdTotalPrimeiroRetorno="0";
		this.qtdTotalPrimeiroRetorno = qtdTotalPrimeiroRetorno;
	}

	public String getQtdTotalRecuperadoEntrega() {
		return qtdTotalRecuperadoEntrega;
	}

	public void setQtdTotalRecuperadoEntrega(String qtdTotalRecuperadoEntrega) {
		if (qtdTotalRecuperadoEntrega==null) qtdTotalRecuperadoEntrega="0";
		this.qtdTotalRecuperadoEntrega = qtdTotalRecuperadoEntrega;
	}

	public String getQtdTotalRecuperadoServico() {
		return qtdTotalRecuperadoServico;
	}

	public void setQtdTotalRecuperadoServico(String qtdTotalRecuperadoServico) {
		if (qtdTotalRecuperadoServico==null) qtdTotalRecuperadoServico="0";
		this.qtdTotalRecuperadoServico = qtdTotalRecuperadoServico;
	}

	public String getQtdTotalRetorna() {
		return qtdTotalRetorna;
	}

	public void setQtdTotalRetorna(String qtdTotalRetorna) {
		if (qtdTotalRetorna==null) qtdTotalRetorna="0";
		this.qtdTotalRetorna = qtdTotalRetorna;
	}

	public String getQtdTotalRetornoDuplicata() {
		return qtdTotalRetornoDuplicata;
	}

	public void setQtdTotalRetornoDuplicata(String qtdTotalRetornoDuplicata) {
		if (qtdTotalRetornoDuplicata==null) qtdTotalRetornoDuplicata="0";
		this.qtdTotalRetornoDuplicata = qtdTotalRetornoDuplicata;
	}

	public String getQtdSaldoRetornar() {
		return qtdSaldoRetornar;
	}

	public void setQtdSaldoRetornar(String qtdSaldoRetornar) {
		if (qtdSaldoRetornar==null) qtdSaldoRetornar="0";
		this.qtdSaldoRetornar = qtdSaldoRetornar;
	}

}