package REG;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;

import sys.BeanException;

/**
* <b>Title:</b>        	SMIT - Bean de Consulta para os Arquivos Recebidos e Download<br>
* <b>Description:</b>  	<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Mind Informatica<br>
* @author  				Elisson Dias
* @version 				1.0
*/

public class ConsultaArquivoDOLBean extends sys.HtmlPopupBean {

	private String nomArquivo;
	private String datRecebimento;
	private String statusArquivo;

	private String dataIni;
	private String dataFim;
	private String datainiAnt;
	private String dataFimAnt;
	private String msgErro;
	private Vector vetArquivos;
	private Vector vetConsulta;
	private String nomConsulta;
	private String comboUF;
	
	private String codMarcaBroker;
	private String codEspecieVeiculo;
	private String codCategoriaVeiculo;
	private String codTipoVeiculo;
	private String codCorVeiculo;
	
	private boolean indMarcaBroker;
	private boolean indEspecieVeiculo; 
	private boolean indCategoriaVeiculo;
	private boolean indTipoVeiculo;
	private boolean indCorVeiculo;
	
	public ConsultaArquivoDOLBean() throws sys.BeanException {

		super();
		nomArquivo = "0";
		dataIni = formatedLast2Month().substring(0, 10);
		dataFim = sys.Util.formatedToday().substring(0, 10);
		datainiAnt=sys.Util.DiminuiDias(90);
		dataFimAnt=sys.Util.DiminuiDias(180);
		msgErro = "";
		vetArquivos = new Vector();
		vetConsulta = new Vector();
		nomConsulta = "";
		nomArquivo = "";
		datRecebimento = "";
		statusArquivo = "0";
		
		codMarcaBroker="";
		codEspecieVeiculo = "";
		codCategoriaVeiculo = "";
		codTipoVeiculo = "";
		codCorVeiculo = "";
		
		indMarcaBroker = false;
		indEspecieVeiculo = false;
		indCategoriaVeiculo = false;
		indTipoVeiculo = false;
		indCorVeiculo= false;

	}
	public String getDataFimAnt() {
		return dataFimAnt;
	}

	public String getDataIniAnt() {
		return datainiAnt;
	}
	public String getDataFim() {
		return dataFim;
	}

	public String getDataIni() {
		return dataIni;
	}

	public static String formatedLast2Month() {

		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -60);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;

	} // Fim formatedToday

	public String getNomStatus() {
		if (statusArquivo.equals("9")) {
			return "Rejeitado";
		} else if (
			statusArquivo.equals("8") || statusArquivo.equals("7") || statusArquivo.equals("1")) {
			return "Recebido em processamento";
		} else if (statusArquivo.equals("2")) {
			return "Processado";
		} else {
			return "Recebido";
		}
	}

	public void setDataFim(String string) {
		dataFim = string;
	}

	public void setDataIni(String string) {
		dataIni = string;
	}

	public String getMsgErro() {
		return msgErro;
	}

	public void setMsgErro(String string) {
		msgErro = string;
	}

	public void carregaListaArquivos(ACSS.UsuarioBean usrLogado,boolean cons) throws BeanException {
		try {
			Dao dao = Dao.getInstance();
			this.setVetArquivos(dao.carregaListaArquivosDOL(usrLogado, getDataIni(), getDataFim(),cons));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void LeConsulta(ACSS.UsuarioBean usrLogado,boolean consulta) throws BeanException {
		try {
			Dao dao = Dao.getInstance();
			this.setVetConsulta(
				dao.BuscaArquivosDOL(usrLogado, this.nomArquivo, this.dataIni, this.dataFim, consulta));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void LeConsulta(String[] campos, String[] valores, String condicao,String tipoConsulta) throws BeanException {

		try {
			Dao dao = Dao.getInstance();
			this.setVetConsulta(dao.BuscaLinhaLoteDOL(campos, valores,condicao,tipoConsulta));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void ConsultaLote(LoteDolBean myLote) throws BeanException {

		try {
			Dao dao = Dao.getInstance();
			dao.BuscaLoteDolBean(myLote);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

//	wellem 06-03-2006	
	public Vector VerificaLotes() throws BeanException {
		Vector lotes = new Vector();
		try {
			Dao dao = Dao.getInstance();
			lotes=dao.BuscaLotesDolBean(this);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return lotes;
	}
//	===================
	
	public void ConsultaArquivo(ArquivoDolBean myArq) throws BeanException {

		try {
			Dao dao = Dao.getInstance();
			dao.BuscaArquivoDolBean(myArq);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void AcertaAutoDol(LinhaLoteDolBean LinhaLoteBeanId) throws BeanException {

		try {
			Dao dao = Dao.getInstance();
			dao.AcertaAutoDol(LinhaLoteBeanId);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void AcertaAutoDolBroker(
		ACSS.UsuarioBean usrLogado,
		ArquivoDolBean ArquivoDolBeanId,
		LoteDolBean LoteDolBeanId,
		LinhaLoteDolBean LinhaLoteDolBeanId)
		throws BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.AcertaAutoDolBroker(usrLogado, ArquivoDolBeanId, LoteDolBeanId, LinhaLoteDolBeanId);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void AtualizaMarcaBroker(String numPlaca) throws BeanException {
	
		try {

			Dao dao = Dao.getInstance();
			String resultTrans = "";
			String dscErro = "";
			resultTrans = dao.ConsultaMarcaBroker(numPlaca);
			String codRetorno = resultTrans.substring(0, 3);
		
			if (!codRetorno.equals("000")) {
				boolean erro = sys.Util.isNumber(codRetorno);
				if (erro == true){
					dscErro = dao.verificaTabEvento(codRetorno);
					this.setMsgErro("Erro: " + dscErro);
				}
				else
				this.setMsgErro("Erro: " + resultTrans.substring(0));

			} else {				
				this.setMsgErro("Marca/Modelo compatível com base de dados DETRAN.");
				this.codMarcaBroker = resultTrans.substring(10,17);
				this.codCorVeiculo = resultTrans.substring(17,20);
			      this.codTipoVeiculo = resultTrans.substring(20,23);
				this.codCategoriaVeiculo = resultTrans.substring(23,25);
				this.codEspecieVeiculo = resultTrans.substring(25,27);
				
				this.indMarcaBroker=true;
				this.indCorVeiculo = true;
				this.indTipoVeiculo = true;
				this.indCategoriaVeiculo = true;
				this.indEspecieVeiculo = true;
			}

		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}

	}

	public void CarregaComboUF() throws BeanException {

		try {
			Dao dao = Dao.getInstance();
			comboUF = dao.carregaComboUF();
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}

	}

	public String getComboUF() {
		return comboUF;
	}

	public Vector getVetArquivos() {
		return vetArquivos;
	}

	public void setVetArquivos(Vector vector) {
		vetArquivos = vector;
	}

	public String getNomArquivo() {
		return nomArquivo;
	}
	public void setNomArquivo(String string) {
		nomArquivo = string;
	}

	public Vector getVetConsulta() {
		return vetConsulta;
	}
	public void setVetConsulta(Vector vector) {
		vetConsulta = vector;
	}
	


	/**
	 * @return
	 */
	public String getDatRecebimento() {
		return datRecebimento;
	}

	/**
	 * @return
	 */
	public String getNomConsulta() {
		return nomConsulta;
	}

	/**
	 * @return
	 */
	public String getStatusArquivo() {
		return statusArquivo;
	}

	/**
	 * @param string
	 */
	public void setComboUF(String string) {
		comboUF = string;
	}

	/**
	 * @param string
	 */
	public void setDatRecebimento(String string) {
		datRecebimento = string;
	}

	/**
	 * @param string
	 */
	public void setNomConsulta(String string) {
		nomConsulta = string;
	}

	/**
	 * @param string
	 */
	public void setStatusArquivo(String string) {
		statusArquivo = string;
	}

	/**
	 * @return
	 */
	public String getCodMarcaBroker() {
		return codMarcaBroker;
	}

	/**
	 * @param string
	 */
	public void setCodMarcaBroker(String string) {
		codMarcaBroker = string;
	}

	/**
	 * @return
	 */
	public boolean isIndMarcaBroker() {
		return indMarcaBroker;
	}
	public void setIndMarcaBroker(boolean b) {
		indMarcaBroker = b;
	}


//------------------------- Sets e gets --------------------------

   // Especie
	public void setEspecieVeiculo(String especie) {
		codEspecieVeiculo = especie;
	}
	public String getEspecieVeiculo() {
		return codEspecieVeiculo;
	}
	
	public boolean isIndEspecieVeiculo() {
		return indEspecieVeiculo;
	}
	public void setIndEspecieVeiculo(boolean b) {
		indEspecieVeiculo = b;
	}

    // Categoria
	public void setCategoriaVeiculo(String especie) {
		codCategoriaVeiculo = especie;
	}
	public String getCategoriaVeiculo() {
		return codCategoriaVeiculo;
	}
	
	public boolean isIndCategoriaVeiculo() {
		return indCategoriaVeiculo;
	}
	public void setIndCategoriaVeiculo(boolean b) {
		indCategoriaVeiculo = b;
	}

    // Tipo
	public void setTipoVeiculo(String tipo) {
		codTipoVeiculo = tipo;
	}
	public String getTipoVeiculo() {
		return codTipoVeiculo;
	}
	
	public boolean isIndTipoVeiculo() {
		return indTipoVeiculo;
	}
	public void setIndTipoVeiculo(boolean b) {
		indTipoVeiculo = b;
	}

    // Cor
	public void setCorVeiculo(String cor) {
		codCorVeiculo = cor;
	}
	public String getCorVeiculo() {
		return codCorVeiculo;
	}
	
	public boolean isIndCorVeiculo() {
		return indCorVeiculo;
	}
	public void setIndCorVeiculo(boolean b) {
		indCorVeiculo = b;
	}



}
