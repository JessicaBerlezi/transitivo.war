package REG;

import java.util.ArrayList;
import java.util.List;

import REC.CodigoRetornoBean;


public class ResumoRetornoBean extends sys.HtmlPopupBean {
	private CodigoRetornoBean retorno;
	private String indFase;
	private String datInicio;
	private String datBase;
	private String datFim;
	private String mes;
	private String ano;
	private String anoAnt;	
	private String datReferencia;
	private String datContrato;
	private String qtdTotalEnviada;
	private String percEnviada;
	private String qtdTotalRetornada;
	private String percRetornada;
	private String qtdTotalnaoRetornada;
	private String qtdTotalnaoRetornadaAnt;	
	private String percnaoRetornada;
	private String qtdTotalCSS;
	private String percCSS;
	private String qtdTotalCSS_Ant;
	private String qtdTotalNCSS;
	private String qtdFaturamento;
	private String qtdCredito;
	private String qtdRecuperadoEntrega;
	private String qtdRecuperadoSistema;
	
	private List<ResumoRetornoBean> ListRetornoMes;
	private List<ResumoRetornoBean> ListCodigoRetornoMes;
	
	private String codIdentArquivo;
	
	public ResumoRetornoBean() throws sys.BeanException {
		super();
		indFase                = "";
		retorno                = new CodigoRetornoBean();
		datInicio              = "";
		datBase                = "01/04/2007";
		datFim                 = "";
		mes                    = "";
		ano                    = "";
		anoAnt                 = "";
		datReferencia          = sys.Util.formatedToday().substring(0,10);
		qtdTotalEnviada        = "0";
		percEnviada            = "0";
		qtdTotalRetornada      = "0";
		percRetornada          = "0";
		qtdTotalnaoRetornada   = "0";
		qtdTotalnaoRetornadaAnt= "0";		
		percnaoRetornada       = "0";
		qtdTotalCSS            = "0";
		percCSS                = "0";
		qtdTotalCSS_Ant        = "0";
		qtdTotalNCSS           = "0";	
		qtdFaturamento         = "0";
		qtdCredito             = "0";
		qtdRecuperadoEntrega   = "0";
		qtdRecuperadoSistema   = "0";
		ListCodigoRetornoMes   = new ArrayList<ResumoRetornoBean>(); 
		ListRetornoMes         = new ArrayList<ResumoRetornoBean>(); 
	}

	public String getAno() {
		return ano;
	}

	public void setAno(String ano) {
		if (ano==null) ano="";
		this.ano = ano;
	}

	public String getAnoAnt() {
		return anoAnt;
	}

	public void setAnoAnt(String anoAnt) {
		if (anoAnt==null) anoAnt="";
		this.anoAnt = anoAnt;
	}
	
	public String getDatReferencia() {
		return datReferencia;
	}

	public void setDatReferencia(String datReferencia) {
		if (datReferencia==null) datReferencia="";
		this.datReferencia = datReferencia;
	}

	public List getListRetornoMes() {
		return ListRetornoMes;
	}

	public void setListRetornoMes(List<ResumoRetornoBean> listRetornoMes) {
		if (listRetornoMes==null) listRetornoMes=new ArrayList<ResumoRetornoBean>();
		ListRetornoMes = listRetornoMes;
	}

	public ResumoRetornoBean getRetornos(Integer i) 	{
		return this.ListRetornoMes.get(i);
	}

	public List<ResumoRetornoBean> getListCodigoRetornoMes() {
		return ListCodigoRetornoMes;
	}

	public void setListCodigoRetornoMes(List<ResumoRetornoBean> listCodigoRetornoMes) {
		if (listCodigoRetornoMes==null) listCodigoRetornoMes=new ArrayList<ResumoRetornoBean>();
		ListCodigoRetornoMes = listCodigoRetornoMes;
	}

	public ResumoRetornoBean getCodigoRetornos(Integer i) 	{
		return this.ListCodigoRetornoMes.get(i);
	}
	
	public String getQtdTotalEnviada() {
		return qtdTotalEnviada;
	}

	public void setQtdTotalEnviada(String qtdTotalEnviada) {
		if (qtdTotalEnviada==null) qtdTotalEnviada="0";
		this.qtdTotalEnviada = qtdTotalEnviada;
	}

	public String getQtdTotalnaoRetornada() {
		return qtdTotalnaoRetornada;
	}

	public void setQtdTotalnaoRetornada(String qtdTotalnaoRetornada) {
		if (qtdTotalnaoRetornada==null) qtdTotalnaoRetornada="0";
		this.qtdTotalnaoRetornada = qtdTotalnaoRetornada;
	}

	public String getQtdTotalRetornada() {
		return qtdTotalRetornada;
	}

	public void setQtdTotalRetornada(String qtdTotalRetornada) {
		if (qtdTotalRetornada==null) qtdTotalRetornada="0";
		this.qtdTotalRetornada = qtdTotalRetornada;
	}
	
	public void LeRetornosEnvio() throws sys.DaoException {
		try {
			Dao dao = Dao.getInstance();
			dao.LeRetornosEnvio(this);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}

	public void LeRetornosEnvioAnt() throws sys.DaoException {
		try {
			Dao dao = Dao.getInstance();
			dao.LeRetornosEnvio(this);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	public void LeRetornos() throws sys.DaoException {
		try {
			Dao dao = Dao.getInstance();
			dao.LeRetornos(this);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	public void LeRetornosAnt() throws sys.DaoException {
		try {
			Dao dao = Dao.getInstance();
			dao.LeRetornosAnt(this);
			return;
		}
		catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	
	public String getDatFim() {
		return datFim;
	}

	public void setDatFim(String datFim) {
		if (datFim==null) datFim="";
		this.datFim = datFim;
	}

	public String getDatInicio() {
		return datInicio;
	}

	public void setDatInicio(String datInicio) {
		if (datInicio==null) datInicio="";
		this.datInicio = datInicio;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		if (mes==null) mes="";
		this.mes = mes;
	}

    public void setRetorno(CodigoRetornoBean retorno)   throws sys.BeanException  {
        this.retorno=retorno ;
        if (retorno==null) this.retorno= new CodigoRetornoBean() ;
    }  
    public CodigoRetornoBean getRetorno()  {
        return this.retorno;
    }

	public String getPercEnviada() {
		return percEnviada;
	}

	public void setPercEnviada(String percEnviada) {
		if (percEnviada==null) percEnviada="0";
		this.percEnviada = percEnviada;
	}

	public String getPercnaoRetornada() {
		return percnaoRetornada;
	}

	public void setPercnaoRetornada(String percnaoRetornada) {
		if (percnaoRetornada==null) percnaoRetornada="0";
		this.percnaoRetornada = percnaoRetornada;
	}

	public String getPercRetornada() {
		return percRetornada;
	}

	public void setPercRetornada(String percRetornada) {
		if (percRetornada==null) percRetornada="0";
		this.percRetornada = percRetornada;
	}

	public String getIndFase() {
		return indFase;
	}

	public void setIndFase(String indFase) {
		if (indFase==null) indFase="";
		this.indFase = indFase;
	}

	public String getPercCSS() {
		return percCSS;
	}

	public void setPercCSS(String percCSS) {
		if (percCSS==null) percCSS="0";
		this.percCSS = percCSS;
	}

	public String getQtdTotalCSS() {
		return qtdTotalCSS;
	}

	public void setQtdTotalCSS(String qtdTotalCSS) {
		if (qtdTotalCSS==null) qtdTotalCSS="0";
		this.qtdTotalCSS = qtdTotalCSS;
	}

	public String getQtdTotalCSS_Ant() {
		return qtdTotalCSS_Ant;
	}

	public void setQtdTotalCSS_Ant(String qtdTotalCSS_Ant) {
		if (qtdTotalCSS_Ant==null) qtdTotalCSS_Ant="0";
		this.qtdTotalCSS_Ant = qtdTotalCSS_Ant;
	}

	public String getQtdTotalNCSS() {
		return qtdTotalNCSS;
	}

	public void setQtdTotalNCSS(String qtdTotalNCSS) {
		if (qtdTotalNCSS==null) qtdTotalNCSS="0";
		this.qtdTotalNCSS = qtdTotalNCSS;
	}

	public String getQtdFaturamento() {
		return qtdFaturamento;
	}

	public void setQtdFaturamento(String qtdFaturamento) {
		if (qtdFaturamento==null) qtdFaturamento="0";
		this.qtdFaturamento = qtdFaturamento;
	}

	public String getDatBase() {
		return datBase;
	}

	public void setDatBase(String datBase) {
		if (datBase==null) datBase="";
		this.datBase = datBase;
	}

	public String getQtdRecuperadoEntrega() {
		return qtdRecuperadoEntrega;
	}

	public void setQtdRecuperadoEntrega(String qtdRecuperadoEntrega) {
		if (qtdRecuperadoEntrega==null) qtdRecuperadoEntrega="0";
		this.qtdRecuperadoEntrega = qtdRecuperadoEntrega;
	}

	public String getQtdRecuperadoSistema() {
		return qtdRecuperadoSistema;
	}

	public void setQtdRecuperadoSistema(String qtdRecuperadoSistema) {
		if (qtdRecuperadoSistema==null) qtdRecuperadoSistema="0";
		this.qtdRecuperadoSistema = qtdRecuperadoSistema;
	}

	public String getQtdCredito() {
		return qtdCredito;
	}

	public void setQtdCredito(String qtdCredito) {
		if (qtdCredito==null) qtdCredito="0";
		this.qtdCredito = qtdCredito;
	}

	public String getDatContrato() {
		return datContrato;
	}

	public void setDatContrato(String datContrato) {
		if (datContrato==null) datContrato="04/2007";
		this.datContrato = datContrato;
	}

	public String getQtdTotalnaoRetornadaAnt() {
		return qtdTotalnaoRetornadaAnt;
	}

	public void setQtdTotalnaoRetornadaAnt(String qtdTotalnaoRetornadaAnt) {
		if (qtdTotalnaoRetornadaAnt==null) qtdTotalnaoRetornadaAnt="0";
		this.qtdTotalnaoRetornadaAnt = qtdTotalnaoRetornadaAnt;
	}

	public String getCodIdentArquivo() {
		return codIdentArquivo;
	}

	public void setCodIdentArquivo(String codIdentArquivo) {
		this.codIdentArquivo = codIdentArquivo;
	}




}