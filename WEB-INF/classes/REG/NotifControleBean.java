package REG;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ACSS.OrgaoBean;

/**
 * <b>Title:</b>        	SMIT - Bean de Arquivo Recebido via Upload<br>
 * <b>Description:</b>  	Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
 * @author  				Sergio Roberto Junior
 * @version 				1.0
 */

public class NotifControleBean extends sys.HtmlPopupBean {
	
	private String codControle;
	private String numAutoInfracao;
	private String numNotificacao;
	private String numPlaca;
	private String codStatusAuto;
	private Date datEnvio; 
	private String codArquivoEnvio;
	private String nomUsuarioEnvio;
	private String codOrgaoEnvio;
	private Date datReceb;
	private String nomUsuarioReceb;
	private String codOrgaoReceb;
	private Date datRetorno;
	private String codArquivoRetorno;
	private String nomUsuarioRetorno;
	private String codOrgaoRetorno;
	private String codStatusNotif; 
	private Date datEmissao;
	private List listaNotif; 
	private Date datDigitalizacao;
	private Date datRegistroCB;
	private String seqRegistroCB;
	private String nomArquivo;
	
	/* 0 � Notifica��o Enviada pelo Detran
	 1 � Notifica��o Pendente de Envio
	 2 � Notifica��o Recebida pela Empresa de Entrega
	 3 � Notifica��o Retornada pela Empresa de Entrega
	 4 � Notifica��o Pendente de Retorno
	 8 � Conclu�da Sem Envio
	 9 � Conclu�da Com Envio
	 */
	private Date datEnvioOrig;
	private String codArquivoEnvioOrig;
	private String nomUsuarioEnvioOrig;
	private String codOrgaoEnvioOrig;
	private Date datRetornoOrig;
	private String nomUsuarioRetornoOrig;
	private String codOrgaoRetornoOrig;
	private String codLinhaArquivoRetorno;
	private String codLinhaArquivoEnvio;
	private String codOrgaoAutuacao;
	private Date   datEntrega;
	private String sitDigitaliza;
	private String qtdReg;	
	
	public NotifControleBean() throws sys.BeanException {
		codControle = "";
		numAutoInfracao = "";
		numNotificacao = "";
		numPlaca = "";
		codStatusAuto = "";
		datEnvio = new Date();
		codArquivoEnvio = "";
		nomUsuarioEnvio = "";
		codOrgaoEnvio = "";
		datReceb = new Date();        
		nomUsuarioReceb = "";
		codOrgaoReceb = "";
		datRetorno = new Date();
		codArquivoRetorno = "";
		nomUsuarioRetorno = "";
		codOrgaoRetorno = "";
		codStatusNotif = "";
		datEnvioOrig = new Date();
		codArquivoEnvioOrig = "";
		nomUsuarioEnvioOrig = "";        
		codOrgaoEnvioOrig = ""; 	
		datRetornoOrig = new Date();
		nomUsuarioRetornoOrig = "";
		codOrgaoRetornoOrig = "";
		codLinhaArquivoRetorno = "";
		codLinhaArquivoEnvio = "";
		codOrgaoAutuacao =  "";
		datEntrega = new Date();
		listaNotif = new ArrayList();
		sitDigitaliza = "";
		datRegistroCB = new Date();
		seqRegistroCB = "0";
		qtdReg        = "0";
		nomArquivo    ="";
	}
	
	public void setCodControle(String codControle)  {
		this.codControle=codControle ;
		if (codControle==null) this.codControle="" ;
	}  
	
	public String getCodControle()  {
		return this.codControle;
	}
	
	public void setNumAutoInfracao(String numAutoInfracao)  {
		this.numAutoInfracao = numAutoInfracao;
		if (numAutoInfracao==null) this.numAutoInfracao="" ;	
	}  
	
	public String getNumAutoInfracao()  {
		return this.numAutoInfracao;
	}
	
	public void setNumNotificacao(String numNotificacao)  {
		this.numNotificacao=numNotificacao ;
		if (numNotificacao==null) this.numNotificacao="" ;
	}  
	
	public String getNumNotificacao()  {
		return this.numNotificacao;
	}
	
	public void setNumPlaca(String numPlaca)  {
		this.numPlaca=numPlaca ;
		if (numPlaca==null) this.numPlaca="" ;
	}  
	
	public String getNumPlaca()  {
		return this.numPlaca;
	}
	
	public void setCodStatusAuto(String codStatusAuto)  {
		this.codStatusAuto=codStatusAuto ;
		if (codStatusAuto==null) this.codStatusAuto="" ;
	}  
	
	public String getCodStatusAuto()  {
		return this.codStatusAuto;
	}
	
	public void setDatEnvio(Date datEnvio){
		this.datEnvio = datEnvio;
	}
	
	public Date getDatEnvio()  {
		return this.datEnvio;
	}
	
	public void setCodArquivoEnvio(String codArquivoEnvio){
		this.codArquivoEnvio=codArquivoEnvio ;
		if (codArquivoEnvio==null)
			this.codArquivoEnvio = codArquivoEnvio ;
	}  
	
	public String getCodArquivoEnvio()  {
		return this.codArquivoEnvio;
	}
	
	public void setNomUsuarioEnvio(String nomUsuarioEnvio)  {
		this.nomUsuarioEnvio=nomUsuarioEnvio ;
		if (nomUsuarioEnvio==null) this.nomUsuarioEnvio="" ;
	}  
	
	public String getNomUsuarioEnvio()  {
		return this.nomUsuarioEnvio;
	}
	
	public void setCodOrgaoEnvio(String codOrgaoEnvio)  {
		this.codOrgaoEnvio=codOrgaoEnvio;
		if (codOrgaoEnvio==null) this.codOrgaoEnvio="" ;
	}  
	
	public String getCodOrgaoEnvio()  {
		return this.codOrgaoEnvio;
	}	
	
	public void setDatReceb(Date datReceb)  {
		this.datReceb=datReceb ;
	}  
	
	public Date getDatReceb()  {
		return this.datReceb;
	}
	
	public void setNomUsuarioReceb(String nomUsuarioReceb)  {
		this.nomUsuarioReceb = nomUsuarioReceb;
		if (nomUsuarioReceb==null) this.nomUsuarioReceb="" ;  
	}  
	
	public String getNomUsuarioReceb()  {
		return this.nomUsuarioReceb;
	}
	
	public void setCodOrgaoReceb(String codOrgaoReceb)  {
		this.codOrgaoReceb = codOrgaoReceb;
		if (codOrgaoReceb==null) this.codOrgaoReceb="" ;  
	}  
	
	public String getCodOrgaoReceb()  {
		return this.codOrgaoReceb;
	}
	
	public void setDatRetorno(Date datRetorno)  {
		this.datRetorno = datRetorno;
	}  
	
	public Date getDatRetorno()  {
		return this.datRetorno;
	}
	
	public void setCodArquivoRetorno(String codArquivoRetorno)  {
		this.codArquivoRetorno = codArquivoRetorno;
		if (codArquivoRetorno==null) this.codArquivoRetorno="" ;  
	}  
	
	public String getCodArquivoRetorno()  {
		return this.codArquivoRetorno;
	}
	
	public void setNomUsuarioRetorno(String nomUsuarioRetorno)  {
		this.nomUsuarioRetorno = nomUsuarioRetorno;
		if (nomUsuarioRetorno==null) this.nomUsuarioRetorno="" ;  
	}  
	
	public String getNomUsuarioRetorno()  {
		return this.nomUsuarioRetorno;
	}
	
	public void setCodOrgaoRetorno(String codOrgaoRetorno)  {
		this.codOrgaoRetorno = codOrgaoRetorno;
		if (codOrgaoRetorno==null) this.codOrgaoRetorno="" ;  
	}  
	
	public String getCodOrgaoRetorno()  {
		return this.codOrgaoRetorno;
	}
	
	public void setCodStatusNotif(String codStatusNotif)  {
		this.codStatusNotif = codStatusNotif;
		if (codStatusNotif==null) this.codStatusNotif="" ;  
	}  
	
	public String getCodStatusNotif()  {
		return this.codStatusNotif;
	}
	
	public void setDatEnvioOrig(Date datEnvioOrig)  {
		this.datEnvioOrig = datEnvioOrig;
	}  
	
	public Date getDatEnvioOrig()  {
		return this.datEnvioOrig;
	}
	
	public void setCodArquivoEnvioOrig(String codArquivoEnvioOrig)  {
		this.codArquivoEnvioOrig = codArquivoEnvioOrig;
		if (codArquivoEnvioOrig==null) this.codArquivoEnvioOrig="" ;  
	}  
	
	public String getCodArquivoEnvioOrig()  {
		return this.codArquivoEnvioOrig;
	}
	
	public void setNomUsuarioEnvioOrig(String nomUsuarioEnvioOrig)  {
		this.nomUsuarioEnvioOrig = nomUsuarioEnvioOrig;
		if (nomUsuarioEnvioOrig==null) this.nomUsuarioEnvioOrig="" ;  
	}  
	
	public String getNomUsuarioEnvioOrig()  {
		return this.nomUsuarioEnvioOrig;
	}
	
	public void setCodOrgaoEnvioOrig(String codOrgaoEnvioOrig)  {
		this.codOrgaoEnvioOrig = codOrgaoEnvioOrig;
		if (codOrgaoEnvioOrig==null) this.codOrgaoEnvioOrig="" ;  
	}  
	
	public String getCodOrgaoEnvioOrig()  {
		return this.codOrgaoEnvioOrig;
	}
	
	public void setDatRetornoOrig(Date datRetornoOrig)  {
		this.datRetornoOrig = datRetornoOrig;
	}  
	
	public Date getDatRetornoOrig()  {
		return this.datRetornoOrig;
	}
	
	public void setNomUsuarioRetornoOrig(String nomUsuarioRetornoOrig)  {
		this.nomUsuarioRetornoOrig = nomUsuarioRetornoOrig;
		if (nomUsuarioRetornoOrig==null) this.nomUsuarioRetornoOrig="" ;  
	}  
	
	public String getNomUsuarioRetornoOrig()  {
		return this.nomUsuarioRetornoOrig;
	}
	
	public void setCodOrgaoRetornoOrig(String codOrgaoRetornoOrig)  {
		this.codOrgaoRetornoOrig = codOrgaoRetornoOrig;
		if (codOrgaoRetornoOrig==null) this.codOrgaoRetornoOrig="" ;  
	}  
	
	public String getCodOrgaoRetornoOrig()  {
		return this.codOrgaoRetornoOrig;
	}
	
	public void setCodLinhaArquivoRetorno(String codLinhaArquivoRetorno)  {
		this.codLinhaArquivoRetorno=codLinhaArquivoRetorno ;
		if (codLinhaArquivoRetorno==null) this.codLinhaArquivoRetorno="" ;
	}  
	
	public String getCodLinhaArquivoRetorno()  {
		return this.codLinhaArquivoRetorno;
	}
	
	public void setCodLinhaArquivoEnvio(String codLinhaArquivoEnvio)  {
		this.codLinhaArquivoEnvio=codLinhaArquivoEnvio ;
		if (codLinhaArquivoEnvio==null) this.codLinhaArquivoEnvio="" ;
	}  
	
	public String getCodLinhaArquivoEnvio()  {
		return this.codLinhaArquivoEnvio;
	}
	
	public void setCodOrgaoAutuacao(String codOrgaoAutuacao)  {
		this.codOrgaoAutuacao=codOrgaoAutuacao ;
		if (codOrgaoAutuacao==null) this.codOrgaoAutuacao="" ;
	}  
	
	public String getCodOrgaoAutuacao()  {
		return this.codOrgaoAutuacao;
	}	
	
	public Date getDatEmissao() {
		return datEmissao;
	}
	
	public void setDatEmissao(Date datEmissao) {
		this.datEmissao = datEmissao;
	}
	
	public Date getDatEntrega() {
		return datEntrega;
	}
	
	public void setDatEntrega(Date datEntrega) {
		this.datEntrega = datEntrega;
	}
	
	public Date getDatDigitalizacao() {
		return datDigitalizacao;
	}
	
	public void setDatDigitalizacao(Date datDigitalizacao) {
		this.datDigitalizacao = datDigitalizacao;
	}
	
	
	public void setSitDigitaliza(String sitDigitaliza) {
		this.sitDigitaliza = sitDigitaliza;
		if (sitDigitaliza==null) this.sitDigitaliza="" ;
	}
	
	public String getSitDigitaliza() {
		return sitDigitaliza;
	}
	
	



	/**
	 * M�todo criado para implementar para a funcionalidade de controle de notifica��es
	 * o retorno do nome do arquivo recebido do codArquivo passado por par�metro.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @param codArquivo
	 * @return nome de um arquivo recebido.
	 * @author Sergio Roberto Junior
	 * @since 03/02/2005
	 * @throws Exception
	 * @version 1.0
	 */
	public String getNomArquivo(String codArquivo) throws Exception{
		if(codArquivo == null)
			return "";
		try{
			ArquivoRecebidoBean arquivoRec = new ArquivoRecebidoBean();
			arquivoRec.setCodArquivo(codArquivo);
			Dao dao = Dao.getInstance();
			dao.CarregaArqRecebido(arquivoRec,"","");
			return arquivoRec.getNomArquivo();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para implementar para a funcionalidade de controle de notifica��es
	 * o retorno da quantidade de linhas com erros, existentes no arquivo recebido 
	 * do codArquivo passado por par�metro.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @param codArquivo
	 * @return quantidade de linhas com erro.
	 * @author Sergio Roberto Junior
	 * @since 03/02/2005
	 * @throws Exception
	 * @version 1.0
	 */
	public String getQtdErros(String codArquivo) throws Exception{
		if(codArquivo == null)
			return "";
		try{
			ArquivoRecebidoBean arquivoRec = new ArquivoRecebidoBean();
			arquivoRec.setCodArquivo(codArquivo);
			Dao dao = Dao.getInstance();
			dao.CarregaArqRecebido(arquivoRec,"","");
			return arquivoRec.getQtdLinhaErro();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para implementar para a funcionalidade de controle de notifica��es
	 * o retorno da quantidade de linhas com pend�ncias, existentes no arquivo recebido 
	 * do codArquivo passado por par�metro.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @param codArquivo
	 * @return quantidade de linhas pendentes.
	 * @author Sergio Roberto Junior
	 * @since 03/02/2005
	 * @throws Exception
	 * @version 1.0
	 */
	public String getQtdPendencias(String codArquivo) throws Exception{
		if(codArquivo == null)
			return "";
		try{
			ArquivoRecebidoBean arquivoRec = new ArquivoRecebidoBean();
			arquivoRec.setCodArquivo(codArquivo);
			Dao dao = Dao.getInstance();
			dao.CarregaArqRecebido(arquivoRec,"","");
			return arquivoRec.getQtdLinhaPendente();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para implementar para a funcionalidade de controle de notifica��es
	 * o retorno do sigOrgao de um determinado �rg�o para posterior apresenta��o.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @param codOrgao
	 * @return sigOrgao do �rg�o referenciado por par�metro.
	 * @author Sergio Roberto Junior
	 * @since 03/02/2005
	 * @throws Exception
	 * @version 1.0
	 */
	public String getSigCodOrgao(String codOrgao) throws Exception{
		if(codOrgao == null)
			return "";
		try{
			OrgaoBean orgaoId = new OrgaoBean();
			orgaoId.Le_Orgao(codOrgao,0);
			return orgaoId.getSigOrgao();
		}
		catch(Exception e){
			throw new Exception(e.getMessage());	
		}
	}
	
	/**
	 * M�todo criado para implementar a funcionalidade de controle de notifica��es,
	 * retornando o nome do status do auto de um determinado auto.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @return nome do status do auto do �rg�o referenciado por par�metro.
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws Exception
	 * @version 1.0
	 */
	public String getNomStatusAuto() throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			return dao.getDescStatusAuto(codStatusAuto);
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para implementar a funcionalidade de controle de notifica��es,
	 * retornando o nome do status da notifica��o de um determinado auto.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @return status da notifica��o referenciado por par�metro.
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws Exception
	 * @version 1.0
	 */	
	public String getNomStatusNotificacao() throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			return dao.getStatusNotificacao(codStatusNotif);
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para implementar a funcionalidade de controle de notifica��es,
	 * retornando o nome do status da notifica��o de um determinado auto.
	 * Esse m�todo deve ser utilizado para todos os tipos de arquivo pertencentes ao
	 * controle de notifica��es, sejam eles de envio ou retorno.
	 * @return status da notifica��o referenciado por par�metro.
	 * @author Sergio Roberto Junior
	 * @since 14/02/2005
	 * @throws Exception
	 * @version 1.0
	 */	
	public String getNomErro(String tipo) throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
			List listas = new ArrayList();
			LinhaArquivoRec linhaRec = new LinhaArquivoRec();
			if(tipo.equalsIgnoreCase("envio"))
				linhaRec.setCodLinhaArqRec(codLinhaArquivoEnvio);
			else
				linhaRec.setCodLinhaArqRec(codLinhaArquivoRetorno);
			listas.add(linhaRec);
			arqRecebido.setLinhaArquivoRec(listas);
			dao.buscarArqLinha(arqRecebido);
			return dao.getDescLinhaErro(linhaRec.getCodRetorno());
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	public List getListaNotifs() {
		return listaNotif;
	}
	public void setListaNotifs(List listaNotif) {
		this.listaNotif = listaNotif;
	}	
	public NotifControleBean getListaNotif(int i) { 
		return ( NotifControleBean )this.listaNotif.get(i); 
	}	
	
	/**
	 * M�todo criado para atualizar os campos relativos a DIGITALIZACAO da notificacao (controle de notifica��es)
	 * Para isso, os seguintes campos devem estar PREVIAMENTE setados na Bean: 
	 *    NUM_NOTIFICACAO e DAT_DIGITALIZACAO.
	 * @author Jos� Fernando
	 * @since 21/03/2006
	 */	
	
	public void atualizaDigitalizacao() throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			dao.atualizaDigitalizacao(this);
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}

    public String getDatRegistroCBString() {
        
        String datString = "";
        if (datRegistroCB != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            datString = df.format(this.datRegistroCB);
        }
        return datString;
    }
	
	public Date getDatRegistroCB() {
		return datRegistroCB;
	}

	public void setDatRegistroCB(Date datRegistroCB) {
		this.datRegistroCB = datRegistroCB;
	}
	
    public String getParametro(ACSS.ParamSistemaBean param) {        
        String parametro = "DIR_CODIGO_BARRA";
        /*Novo Path*/
        parametro+="_"+getDatRegistroCBString().substring(6,10);
        return parametro;
    }
    
    public String getArquivo(ACSS.ParamSistemaBean param,int iSeq) {
        String pathArquivo;
        try {
            pathArquivo = param.getParamSist(this.getParametro(param))+"/"+this.getImagem()+
			              "/CodigoBarra/"+iSeq+"/"+this.numNotificacao + ".jpg";				
				
        } catch (Exception e) {
            pathArquivo = "";
        }
        return pathArquivo;
    }    
	
	public String getImagem() {
		String pathImagem = "";
        pathImagem =getDatRegistroCBString().substring(6,10)+ 
        getDatRegistroCBString().substring(3,5) +
		"/" + getDatRegistroCBString().substring(0,2); 
        return pathImagem;	
    }

	public String getSeqRegistroCB() {
		return seqRegistroCB;
	}

	public void setSeqRegistroCB(String seqRegistroCB) {
		this.seqRegistroCB = seqRegistroCB;
	}  			
	
	public String getQtdReg() {
		return qtdReg;
	}

	public void setQtdReg(String qtdReg) {
		this.qtdReg = qtdReg;
	}  		
	
	public String getNomArquivo() {
		return nomArquivo;
	}
	public void setNomArquivo(String nomArquivo) {
		this.nomArquivo = nomArquivo;
	}	
	
}