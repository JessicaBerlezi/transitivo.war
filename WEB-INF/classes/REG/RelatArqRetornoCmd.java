package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

public class RelatArqRetornoCmd extends sys.Command 
{
	private static final String jspPadrao = "/REG/RelatArqRetorno.jsp";
	public RelatArqRetornoCmd() { 
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException 
	{
		String nextRetorno = jspPadrao;
		String datIniRel ="";
		String datFimRel ="";
		
		try {
			RelatArqRetornoBean RelArqDownlBeanId = (RelatArqRetornoBean) req.getSession().getAttribute("RelArqDownlBeanId");
			if( RelArqDownlBeanId == null ) RelArqDownlBeanId = new RelatArqRetornoBean();
			
			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if ( acao.equals("") ) {
				
				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
				
				RelArqDownlBeanId.setDatIni( datIniRel );
				RelArqDownlBeanId.setDatFim( datFimRel );
				req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
			}
			else if(acao.equals("ImprimeRelatorio")) 
			{
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");
				
				if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
				{ 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
				}  
				if( RelArqDownlBeanId.ConsultaRelDiario( datIniRel, datFimRel,vErro ) )				{  
					String tituloConsulta = "RELATÓRIO -ARQUIVOS DE RETORNO : "+ datIniRel +" a "+ datFimRel;
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/REG/RelatArqRetornoImp.jsp";
				}
				else {
					RelArqDownlBeanId.setDatIni( datIniRel );
					RelArqDownlBeanId.setDatFim( datFimRel );
					nextRetorno = "/REG/RelatArqRetorno.jsp";
				}
				
				req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);					
			}
		} catch (Exception e) {	
			throw new sys.CommandException("RelatArqRetornoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
	
}