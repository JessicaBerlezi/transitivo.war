package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Categoria<br>
* <b>Description:</b>  Comando para Consultar Categorias<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class CategoriaCmd extends sys.Command {

	private static final String jspPadrao = "/REG/CategoriaConsulta.jsp";

	private String next;

	public CategoriaCmd() {
		next = jspPadrao;
	}

	public CategoriaCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();

		try {
			TAB.CategoriaBeanCol Catbc = new TAB.CategoriaBeanCol();
			Catbc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("CategoriaBeanColId", Catbc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
