package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class acompPendEmitevexCmd extends sys.Command 
{
	private static final String jspPadrao = "/REG/acompPendEmitevex.jsp";
	private String statusInteracao = null;
	public acompPendEmitevexCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException 
	{
		String nextRetorno = jspPadrao;
		String datIniRel ="";
		String datFimRel ="";
		
		try {
			HttpSession session   = req.getSession() ;					
			acompPendEmitevexBean acompPendEmitevexBeanId = (acompPendEmitevexBean) req.getSession().getAttribute("acompPendEmitevexBeanId");
			if( acompPendEmitevexBeanId == null ) acompPendEmitevexBeanId = new acompPendEmitevexBean();
			
			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			

			// Variav�l usada para controlar intera��o o usu�rio com o sistema.
			statusInteracao = req.getParameter("statusInteracao");
			if( statusInteracao == null ) statusInteracao = "";
			
			if ( acao.equals("") ) {
				
				statusInteracao = "1";
				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
				
				acompPendEmitevexBeanId.setDatIni( datIniRel );
				acompPendEmitevexBeanId.setDatFim( datFimRel );
				req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
			}
			else if(acao.equals("ImprimeRelatorio")) 
			{
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");
				
				if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
				{ 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano; 					
					
				}  
				if( acompPendEmitevexBeanId.ConsultaacompPendEmitevex( datIniRel, datFimRel,vErro ) )				{ 
					
					String tituloConsulta = "RELAT�RIO -ARQUIVOS DE NOTIFICA��O : "+ datIniRel +" a "+ datFimRel;
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/REG/acompPendEmitevexImp.jsp";
					req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
				}
				else {
					acompPendEmitevexBeanId.setDatIni( datIniRel );
					acompPendEmitevexBeanId.setDatFim( datFimRel );
					nextRetorno = "/REG/acompPendEmitevex.jsp";
				}
				
			}else if(acao.equals("VisualizaRelatorio")) 
				{
					Vector vErro = new Vector();
					
					datIniRel = req.getParameter("dataIni");
					datFimRel = req.getParameter("dataFim");
					
					if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
					{ 
						datFimRel = sys.Util.formatedToday().substring(0,10); 
						Calendar cal = new GregorianCalendar();
						cal.setTime( new Date() );
						cal.add(Calendar.MONTH,0);
						Date date = cal.getTime();
						cal.setTime(date);
						String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
						String mes = cal.get(Calendar.MONTH)+ 1 + "";
						if(mes.length() < 2) mes = "0" + mes;  
						String ano = cal.get(Calendar.YEAR) + "";
						datIniRel = dia +"/"+ mes +"/"+ ano;     
					}  
					if( acompPendEmitevexBeanId.ConsultaacompPendEmitevex( datIniRel, datFimRel,vErro ) )				{ 
						
						String tituloConsulta = "RELAT�RIO - ACOMPANHAMENTO DE PEND�NCIAS - EMITEVEX : "+ datIniRel +" a "+ datFimRel;
						req.setAttribute("tituloConsulta", tituloConsulta);
						req.setAttribute("dataIni", datIniRel);
						req.setAttribute("dataFim", datFimRel);	
						req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
						nextRetorno = "/REG/acompPendEmitevexVis.jsp";
					}
					else {
						acompPendEmitevexBeanId.setDatIni( datIniRel );
						acompPendEmitevexBeanId.setDatFim( datFimRel );
						nextRetorno = "/REG/acompPendEmitevex.jsp";
					}
					
				}	else if(acao.equalsIgnoreCase("detalhe")){
						
					    String codArquivo = req.getParameter("codigoArquivo");	
						String detalhe = req.getParameter("detalhe");						
						String sentido = req.getParameter("sentido");
						String codOrgao = req.getParameter("codOrgao");
						if (codOrgao == null) codOrgao = "0";
						String todosOrgaos = req.getParameter("todosOrgaos");
						String prazoDigitado = req.getParameter("prazoDigitado");
						if (prazoDigitado == null) prazoDigitado = "0";
						String statusAuto = req.getParameter("codStatusAuto");
						if (statusAuto == null) statusAuto = req.getParameter("statusAuto");
						
						String totalMesAno = req.getParameter("totalMesAno");
						
						String primeiroRegistro = req.getParameter("primeiroRegistro");
						if (primeiroRegistro == null) primeiroRegistro = "";
						
						String primeiroAnterior = req.getParameter("primeiroAnterior");
						if (primeiroAnterior == null) primeiroAnterior = "";
						
						String ultimoRegistro = req.getParameter("ultimoRegistro");
						if (ultimoRegistro == null) ultimoRegistro = "";
						
						String numPag = req.getParameter("numPag");
						if (numPag == null) numPag = "0";
						
						String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
						int prazo = Integer.parseInt(prazoDigitado);
						
						String seq = req.getParameter("seq");
						
						String[] orgaos = null;
						// Verifica se a consulta ser� feita para todos os �rg�os						
						
						try{			
							Dao dao = Dao.getInstance();
							NotifControleBean notifControle = (NotifControleBean) req.getSession().getAttribute("NotifControleId");
							if( notifControle == null ) notifControle = new NotifControleBean();							
							
							ParamSistemaBean parametro = (ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");
							String rowNum = parametro.getParamSist("LIMITE_REL_NOTIFICACOES");
							String qtdReg = rowNum;
							String rowNumImp = "";
							
							if (sentido == null) 
								sentido = "Prox";
							
							HashMap proxAnt = new HashMap();
							if(sentido.equals("Prox"))
							{
								proxAnt.put("primeiroRegistro", primeiroRegistro);
								proxAnt.put("ultimoRegistro", ultimoRegistro);
								numPag = Integer.toString(Integer.parseInt(numPag)+1);
								if (seq == null) 
									seq = "1";
								else
							        seq = Integer.toString(Integer.parseInt(seq)+Integer.parseInt(qtdReg));
							}
							else
							{
								proxAnt.put("primeiroRegistro", ""); 
								proxAnt.put("ultimoRegistro", "");
								numPag = Integer.toString(Integer.parseInt(numPag)-1);
								seq = Integer.toString(Integer.parseInt(seq)-Integer.parseInt(qtdReg));
							}
							proxAnt.put("sentido", sentido);
							proxAnt.put("primeiroAnterior", primeiroAnterior);
							proxAnt.put("sentido", sentido);
							
							
							if(codOrgao.equals(""))
								statusInteracao = "3";
							else
								statusInteracao = "2";
							
							
							
							/*15/08/2007*/
							dao.getacompPendEmitevex(notifControle, rowNum, proxAnt, rowNumImp,codArquivo);							
							notifControle.setCodArquivoEnvioOrig(codArquivo);
							req.setAttribute("codigodoArquivo",codArquivo);
							req.setAttribute("numPag",numPag);
							req.setAttribute("seq",seq);
							req.setAttribute("opcao",detalhe);
							req.setAttribute("statusAuto",statusAuto);
							req.setAttribute("codOrgao",codOrgao);
							req.setAttribute("prazo", String.valueOf(prazo));
							req.setAttribute("todosOrgaos",todosOrgaos);
							req.setAttribute("todosOrgaosCod",todosOrgaosCod);
							req.setAttribute("totalMesAno",totalMesAno);
							req.setAttribute("prazoDigitado",prazoDigitado);
							req.setAttribute("NotifControleId",notifControle);							
							req.setAttribute("primeiroAnterior",primeiroRegistro);
							req.setAttribute("statusInteracao",statusInteracao);
							nextRetorno = "/REG/NotifDetalhePendEmitevex.jsp";
							
							if (sentido.equals("Imprimir"))
							{									
								nextRetorno = "/REG/NotifDetalhePendEmitevexImp.jsp";
								String tituloConsulta = "RELAT�RIO - ACOMPANHAMENTO DE PEND�NCIAS - EMITEVEX  ";
								rowNumImp = "";	
								dao.getacompPendEmitevex(notifControle, rowNum, proxAnt, rowNumImp,codArquivo);
								req.setAttribute("tituloConsulta", tituloConsulta);
							}
						}
						catch(Exception e){
							throw new sys.CommandException(e.getMessage());	
						}				
				
			}
		} catch (Exception e) {
			throw new sys.CommandException("acompPendEmitevexCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
	
}