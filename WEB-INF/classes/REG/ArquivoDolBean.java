package REG;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * <b>Title:</b>        	SMIT - Bean de Arquivo Recebido via Upload<br>
 * <b>Description:</b>  	Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
 * @author  				Luiz Medronho
 * @version 				1.0
 */

public class ArquivoDolBean extends sys.HtmlPopupBean  { 
	
	private String codArquivo;	
	private String tipHeader;
	private String datRecebimento;
	private String datProcessamentoDetran;
	
	private String nomArquivo;	
	private String codIdentArquivo;
	private String numControleArq;
	private String responsavelArq;
	
	private String numAnoExercicio;
	private String proximoArquivo;
	private String numProtocolo;
	private String numVersao;
	
	private String codOrgao;
	private String nomUsername;
	private String sigOrgaoLotacao;
	private String codOrgaoLotacao;
	
	private String codStatus;  // 0 - Recebido, 9-Rejeitado
	private String qtdLote;
	private String qtdReg;
	private String qtdLoteErro;
	
	private List lote;
	private Vector erroArquivo;
	private Connection conn;
	
	private String indPrioridade;
	private String datDownload;
	private String dscPortaria59;
	
	public ArquivoDolBean()  throws sys.BeanException {
		
		super();
		setTabela("TSMI_ARQUIVO_DOL");	
		setPopupWidth(35);
		
		codArquivo	           = "0";		
		tipHeader              = "9900";
		datRecebimento         = sys.Util.formatedToday().substring(0,10);
		datProcessamentoDetran = "";
		
		nomArquivo             = "";
		codIdentArquivo        = "DE01";
		numControleArq         = "0";
		responsavelArq         = "";
		
		numAnoExercicio        = "2004";
		proximoArquivo         = "";
		numProtocolo           = "00000";
		numVersao           = "00000";
		codOrgao               = "";
		nomUsername            = "";
		sigOrgaoLotacao        = "";
		codOrgaoLotacao        = "";
		
		codStatus              = "9";
		qtdLote                = "0";
		qtdReg                 = "0";
		qtdLoteErro			   = "0";
		
		indPrioridade          = "";
		datDownload            = "";
		
		lote				   = new ArrayList();
		erroArquivo 		   = new Vector();		
		
		dscPortaria59          = "";
	}
	
//	--------------------------------------------------------------------------  
	public void setCodArquivo(String codArquivo)  {
		this.codArquivo=codArquivo ;
		if (codArquivo==null) this.codArquivo="" ;
	}  
	public String getCodArquivo()  {
		return this.codArquivo;
	}
//	--------------------------------------------------------------------------  
	public void setTipHeader(String tipHeader)  {
		this.tipHeader=tipHeader ;
		if (tipHeader==null) this.tipHeader="9" ;
	}  
	public String getTipHeader()  {
		return this.tipHeader;
	}
	
//	--------------------------------------------------------------------------  
	public void setDatRecebimento(String datRecebimento)	{
		if (datRecebimento==null) datRecebimento = sys.Util.formatedToday().substring(0,10) ;
		this.datRecebimento = datRecebimento;
	}
	public String getDatRecebimento()	{
		return this.datRecebimento;
	}
//	--------------------------------------------------------------------------  
	public void setDatProcessamentoDetran (String datProcessamentoDetran) {
		if (datProcessamentoDetran==null) datProcessamentoDetran = "";
		this.datProcessamentoDetran = datProcessamentoDetran;
	}
	public String getDatProcessamentoDetran()	{
		return this.datProcessamentoDetran;
	}
//	---------------------------------------------------------------------------
	public void setNomArquivo (String nomArquivo)	{
		this.nomArquivo = nomArquivo;
		if (nomArquivo==null) this.nomArquivo="" ;	
	}
	public String getNomArquivo() {
		return this.nomArquivo;
	}
//	--------------------------------------------------------------------------  
	public void setCodIdentArquivo (String codIdentArquivo)	{
		this.codIdentArquivo = codIdentArquivo;
		if (codIdentArquivo==null) this.codIdentArquivo="" ;	
	}
	public String getCodIdentArquivo() {
		return this.codIdentArquivo;
	}
	public String getTipArqRetorno() {
		return "7"; //Fixo
	}
//	--------------------------------------------------------------------------  
	public void setNumControleArq (String numControleArq) {
		this.numControleArq = numControleArq;
		if (numControleArq==null) this.numControleArq="00000" ;
		if (this.numControleArq.length()<5) {
			this.numControleArq=("00000"+this.numControleArq);
			this.numControleArq=this.numControleArq.substring(this.numControleArq.length()-5,this.numControleArq.length());
		}
	}
	public String getNumControleArq() {
		return this.numControleArq;
	}
//	--------------------------------------------------------------------------  
	public void setResponsavelArq (String responsavelArq) {
		this.responsavelArq = responsavelArq;
		if (responsavelArq==null) this.responsavelArq="00000" ;
	}
	public String getResponsavelArq() {
		return this.responsavelArq;
	}
//	--------------------------------------------------------------------------  
	public void setNumAnoExercicio (String numAnoExercicio)	{
		this.numAnoExercicio = numAnoExercicio;
		if (numAnoExercicio==null) this.numAnoExercicio="2004" ;
		if (this.numAnoExercicio.length()<4) {
			this.numAnoExercicio=("0000"+this.numAnoExercicio);
			this.numAnoExercicio=this.numAnoExercicio.substring(this.numAnoExercicio.length()-4,this.numAnoExercicio.length());
		}
	}
	public String getNumAnoExercicio() {
		return this.numAnoExercicio;
	}
//	--------------------------------------------------------------------------  
	public void setProximoArquivo (String proximoArquivo)	{
		this.proximoArquivo = proximoArquivo;
		if (proximoArquivo==null) this.proximoArquivo="" ;
	}
	public String getProximoArquivo() {
		return this.proximoArquivo;
	}
//	--------------------------------------------------------------------------
	public void setLote(List lote){
		this.lote = lote;
		if (lote==null) this.lote= new ArrayList() ;
	}
	
	public List getLote(){
		return lote;
	}
	
//	--------------------------------------------------------------------------  
	public void setNumProtocolo (String numProtocolo)	{
		this.numProtocolo = numProtocolo;
		if (numProtocolo==null) this.numProtocolo="00000" ;
	}
	public String getNumProtocolo() {
		return this.numProtocolo;
	}
//	--------------------------------------------------------------------------  
	public void setNumVersao (String numVersao)	{
		this.numVersao = numVersao;
		if (numVersao==null) this.numVersao="" ;
	}
	public String getNumVersao() {
		return this.numVersao;
	}
//	--------------------------------------------------------------------------  
	public void setCodOrgao (String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao==null) this.codOrgao="000000" ;
		if (this.codOrgao.length()<6)  {
			this.codOrgao=("000000"+this.codOrgao);
			this.codOrgao=this.codOrgao.substring(this.codOrgao.length()-6,this.codOrgao.length());
		}
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
//	--------------------------------------------------------------------------  
	public void setNomUsername (String nomUsername) {
		this.nomUsername = nomUsername;
		if (nomUsername==null) this.nomUsername="" ;
	}
	public String getNomUsername() {
		return this.nomUsername;
	}	
//	--------------------------------------------------------------------------
	public void setIndPrioridade(String indPrioridade){
		this.indPrioridade = indPrioridade;
		if(indPrioridade == null) indPrioridade = "";
	}
	public String getIndPrioridade(){
		return this.indPrioridade;	
	}  	
//	--------------------------------------------------------------------------  
	public void setSigOrgaoLotacao (String sigOrgaoLotacao) {
		this.sigOrgaoLotacao = sigOrgaoLotacao;
		if (sigOrgaoLotacao==null) this.sigOrgaoLotacao="" ;
	}
	public String getSigOrgaoLotacao() {
		return this.sigOrgaoLotacao;
	}
//	--------------------------------------------------------------------------  
	public void setCodOrgaoLotacao (String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if (codOrgaoLotacao==null) this.codOrgaoLotacao="" ;
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	
//	--------------------------------------------------------------------------  
	public void setCodStatus(String codStatus) 	{
		this.codStatus = codStatus;
		if (codStatus==null) this.codStatus="9" ;	
	}
	public String getCodStatus() {
		return this.codStatus;
	}
//	--------------------------------------------------------------------------  
	public void setQtdLote (String qtdLote){
		this.qtdLote = qtdLote;
		if (qtdLote==null) this.qtdLote="0" ;
	}
	public String getQtdLote() 	{
		return this.qtdLote;
	}
//	--------------------------------------------------------------------------
	public void setQtdLoteErro(String qtdLoteErro){
		this.qtdLoteErro = qtdLoteErro;
		if (qtdLoteErro==null) this.qtdLoteErro="0" ;			
	}
	public String getQtdLoteErro(){
		return qtdLoteErro;
	}
//	--------------------------------------------------------------------------  
	public void setErroArquivo(Vector erroArquivo) {
		this.erroArquivo = erroArquivo;
		if (this.erroArquivo==null) this.erroArquivo = new Vector();
	}
	public Vector getErroArquivo() 	{
		return erroArquivo;
	}
//	--------------------------------------------------------------------------  
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public Connection getConn() 	{
		return conn;
	}
//	--------------------------------------------------------------------------  
	/**
	 * M�todo para inserir Arquivo na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	/**	anterior � 06-03-2006	
	 public void isInsert(REC.ParamOrgBean ParamOrgaoId, int seqLote) {
	 try {
	 if (isOK() == false) {
	 setCodStatus("9") ;
	 }
	 Dao dao = Dao.getInstance();
	 dao.ArquivoInsertDol(this,ParamOrgaoId, seqLote) ;
	 }
	 catch (Exception e) { 
	 getErroArquivo().addElement("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n") ;
	 getErroArquivo().addElement("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
	 }
	 return ;  
	 }
	 */
//	Modificado por Wellem 06-03-2006	
	public boolean isInsert(REC.ParamOrgBean ParamOrgaoId, int seqLote,String Origem) {
		boolean ok=true;
		try {
			
			boolean bkp=false;
			
			if(!"BKP".equals(Origem)){
				if (isOK() == false) {
					setCodStatus("9") ;
				}
			}else{
				if (isbkpOK() == false){
					setCodStatus("9") ;
					ok=false;
				}
				else
					bkp=true;
			}
			Dao dao = Dao.getInstance();
			if(!"BKP".equals(Origem))				
				dao.ArquivoInsertDol(this,ParamOrgaoId, seqLote,"") ;			
			else{
				if(bkp)
					dao.ArquivoInsertDol(this,ParamOrgaoId, seqLote,"BKP") ;
			}
		}
		catch (Exception e) { 
			getErroArquivo().addElement("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n") ;
			getErroArquivo().addElement("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
		}
		return ok;  
	}
	
	
//	--------------------------------------------------------------------------  
	
	/**
	 * M�todo para Alterar Arquivo na base de dados
	 * O �nico campo que pode ser alterado � o status.
	 */
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.ArquivoUpdateDol(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Arquivo n�o atualizado: " + this.getNomArquivo() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false ;
		}
		this.setMsgErro(vErro) ;	
		return bOk;  
	}
//	--------------------------------------------------------------------------  
	public boolean isOK()  throws sys.BeanException {
		boolean oK = true;
		if ("DE01".equals(getCodIdentArquivo())==false) {
			this.getErroArquivo().addElement("00000" + "|" + "400#Tipo de Arquivo inv�lido: " + this.getCodIdentArquivo() + "" );
			oK = false;
		}
		else {
			try {
				Dao dao = Dao.getInstance();
				if (dao.ArquivoExisteDol(this)) {
					this.getErroArquivo().addElement("00000" + "|" + "999#Arquivo "+ this.getNomArquivo() +" j� RECEBIDO. "  );
					oK = false;
				}
			}
			catch (Exception e) { 
				throw new sys.BeanException(e.getMessage());
			}
		}		
		
		return oK;
	}
	
	
	public boolean isbkpOK()  throws sys.BeanException {
		boolean bkpoK = true;
		if ("DE01".equals(getCodIdentArquivo())==false) {
			this.getErroArquivo().addElement("00000" + "|" + "400#Tipo de Arquivo inv�lido: " + this.getCodIdentArquivo() + "" );
			bkpoK = false;
		}
		else {
			try {
				Dao dao = Dao.getInstance();
				if (dao.ArquivoExisteDolbkp(this)) {
					this.getErroArquivo().addElement("00000" + "|" + "999#Arquivo "+ this.getNomArquivo() +" j� RECEBIDO. "  );
					bkpoK = false;
				}
			}
			catch (Exception e) { 
				throw new sys.BeanException(e.getMessage());
			}
		}		
		
		return bkpoK;
	}
	
	
	
	
//	--------------------------------------------------------------------------  
	/**
	 * M�todo para Criticar Arquivo 
	 * Gera vetor com mensagens de erro.
	 */
	public void critica(REC.ParamOrgBean paramOrgaoId, String codOrgao) throws Exception{
		Vector vetCrit = new Vector();
		boolean codOrgaoOK = false;
		//Critica do Header
		if ("9900".compareTo(tipHeader) != 0) {				
			vetCrit.addElement(numControleArq + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		
		// Critica do Ano do Exerc�cio
		if ((this.getNumAnoExercicio().trim().equals(null) == true)) {
			vetCrit.addElement(numControleArq + "|" + "002#Exerc�cio do Movimento n�o preenchido no Header: '" + this.getNumAnoExercicio() + "'");
		}
		else{	
			if ( paramOrgaoId.getParamOrgao("PROCESSO_ANO","2004","2").equals(numAnoExercicio) == false) {
				vetCrit.addElement(numControleArq + "|" + "005#Exerc�cio do Movimento Inv�lido: '" + this.getNumAnoExercicio() + "'");
			}
		}
		
		//Critica o �rg�o Autuador
		if ((this.getCodOrgao().trim().equals(null) == true)) 
			vetCrit.addElement(numControleArq + "|" + "006#�rg�o Autuador n�o preenchido: '" + this.getCodOrgao() + "'");
		
		else if (sys.Util.isNumber(this.getCodOrgao()) == false)
			vetCrit.addElement(numControleArq + "|" + "007#�rg�o Autuador Inv�lido: '" + this.getCodOrgao() + "'");
		
		else{
			if (this.codOrgao.equals(codOrgao) == false)
				vetCrit.addElement(numControleArq + "|" + "008#�rg�o Autuador Inv�lido: '" + this.getCodOrgao() + "'");			
		}
		
		//Critica o N�mero de Controle
		if ((this.getNumControleArq().trim().equals(null) == true))
			vetCrit.addElement(numControleArq + "|" + "009#N�mero de Controle do Arquivo n�o preenchido: '" + this.getNumControleArq() + "'");
		
		else if (sys.Util.isNumber(this.getNumControleArq()) == false)
			vetCrit.addElement(numControleArq + "|" + "010#N�mero de Controle do Arquivo Inv�lido: '" + this.getNumControleArq() + "'");
		
		else{
			if( paramOrgaoId.getParamOrgao("PROXIMO_DE01","00001","10").equals(numControleArq) == false )
				vetCrit.addElement(numControleArq + "|" + "011#N�mero de Controle do Arquivo Inv�lido: '" + this.getNumControleArq() + "'");
		}
		
		//Critica a Quantidade de Registros
		if ((this.getQtdLote().trim().equals(null) == true)) 
			vetCrit.addElement(numControleArq + "|" + "012#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdLote() + "'");
		
		else{
			if (sys.Util.isNumber(this.getQtdLote()) == false) 
				vetCrit.addElement(numControleArq + "|" + "013#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdLote() + "'");
		}
		
		setErroArquivo(vetCrit);
		return ;
	} 
	public String getNomArquivoDir() {
		
		String nomArquivoDir = this.nomArquivo;
		nomArquivoDir = "DE01/" + nomArquivoDir;
		return nomArquivoDir;
	}   
	
	public String getNomArquivoBkp() {
		
		String nomArquivoBkp = "BK"+ this.nomArquivo.substring(2,this.nomArquivo.length());
		nomArquivoBkp = "DE01/" + nomArquivoBkp;
		return nomArquivoBkp;
	}   
	
	
	public String getDatDownload() {
		return datDownload;
	}
	
	public void setDatDownload(String datDownload) {
		this.datDownload = datDownload;
	}
	
	public String getQtdReg() {
		return qtdReg;
	}
	
	public void setQtdReg(String qtdReg) {
		this.qtdReg = qtdReg;
	}
	
	public String getDscPortaria59() {
		return dscPortaria59;
	}
	public void setDscPortaria59(String dscPortaria59) {
		if (dscPortaria59==null) dscPortaria59="";
		this.dscPortaria59 = dscPortaria59;
	}
	
	
}
