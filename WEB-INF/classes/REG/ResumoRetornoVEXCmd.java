package REG;


import java.math.BigDecimal;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import REC.CodigoRetornoBean;

import sys.BeanException;
import sys.Util;

/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */
public class ResumoRetornoVEXCmd  extends sys.Command {
	private static final String jspPadrao="/REG/ResumoRetorno.jsp";    
	private String next;
	
	public ResumoRetornoVEXCmd() {
		next = jspPadrao;
	}
	
	public ResumoRetornoVEXCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
			String datRetornoVex      = parSis.getParamSist("DAT_RETORNO_VEX");
			String datReferenciaVex   = parSis.getParamSist("MESANO_RET_VEX");			
			
			ResumoRetornoBean ResumoRetornoBeanId = (ResumoRetornoBean)session.getAttribute("ResumoRetornoBeanId");
			if (ResumoRetornoBeanId==null)  ResumoRetornoBeanId = new ResumoRetornoBean();
			ResumoRetornoBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if  (acao.equals("consultarAnoVex"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);

				String sNumAnoAnt=String.valueOf(Integer.parseInt(sNumAno)-1);				
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				String statusAuto = req.getParameter("statusAuto");
				if (statusAuto==null) statusAuto="";

				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setAnoAnt(sNumAnoAnt);				
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.setDatContrato(datReferenciaVex);
				ResumoRetornoBeanId.setCodIdentArquivo("EMITEVEX");
				
				if(statusAuto.equalsIgnoreCase("autuacao")) ResumoRetornoBeanId.setIndFase("001");
				else if(statusAuto.equalsIgnoreCase("penalidade")) ResumoRetornoBeanId.setIndFase("011");
                
				/*Preparar Estrutura do Relat�rio*/
				montarEstruturaRelatorio(ResumoRetornoBeanId);
				//ResumoRetornoBeanId.LeRetornosAnt();
				ResumoRetornoBeanId.LeRetornos();
				montarPercentuais(ResumoRetornoBeanId);
				/*Apresenta��o*/
				String tituloConsulta = null;
				/*
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+datRetornoVex+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				*/
				tituloConsulta = "VEX - RESUMO P/ DATA DE RETORNO - ANO : "+ ResumoRetornoBeanId.getAno()+" - EMISS�ES � PARTIR DE : "+
						ResumoRetornoBeanId.getDatContrato();
				
				nextRetorno = "/REG/ResumoRetornoAnoRetImp.jsp";
				req.setAttribute("tituloConsulta", tituloConsulta);
			}
			if  (acao.equals("consultarAnoPerrone"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);

				String sNumAnoAnt=String.valueOf(Integer.parseInt(sNumAno)-1);				
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				String statusAuto = req.getParameter("statusAuto");
				if (statusAuto==null) statusAuto="";

				ResumoRetornoBeanId = new ResumoRetornoBean();
				ResumoRetornoBeanId.setAno(sNumAno);
				ResumoRetornoBeanId.setAnoAnt(sNumAnoAnt);				
				ResumoRetornoBeanId.setDatInicio("01/01/"+sNumAno);
				ResumoRetornoBeanId.setDatFim("31/12/"+sNumAno);
				ResumoRetornoBeanId.setDatReferencia(sDtReferencia);
				ResumoRetornoBeanId.setDatContrato(datReferenciaVex);
				ResumoRetornoBeanId.setCodIdentArquivo("EMITEPER");
				
				if(statusAuto.equalsIgnoreCase("autuacao")) ResumoRetornoBeanId.setIndFase("001");
				else if(statusAuto.equalsIgnoreCase("penalidade")) ResumoRetornoBeanId.setIndFase("011");
                
				//Preparar Estrutura do Relat�rio
				montarEstruturaRelatorio(ResumoRetornoBeanId);
				//ResumoRetornoBeanId.LeRetornosAnt();
				ResumoRetornoBeanId.LeRetornos();
				montarPercentuais(ResumoRetornoBeanId);
				/*Apresenta��o*/
				String tituloConsulta = null;
				/*
				tituloConsulta = "RESUMO P/ DATA DE RETORNO - ANO : "+
				ResumoRetornoBeanId.getAno()+" - POSI��O EM : "+datRetornoVex+
				" - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				*/
				tituloConsulta = "PERRONE - RESUMO P/ DATA DE RETORNO - ANO : "+
						ResumoRetornoBeanId.getAno()+ " - EMISS�ES � PARTIR DE : "+ResumoRetornoBeanId.getDatContrato();
				
				nextRetorno = "/REG/ResumoRetornoAnoRetImp.jsp";
				req.setAttribute("tituloConsulta", tituloConsulta);
			}
			session.setAttribute("ResumoRetornoBeanId",ResumoRetornoBeanId) ;  
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("ResumoRetornoCmd: " + ue.getMessage());
		}
	}
	
	
	public void montarEstruturaRelatorio(ResumoRetornoBean ResumoRetornoBeanId)throws BeanException
	{
		/*Montar Totais Zerados de Enviados/N�o Retornados/Retornados*/
		ArrayList <ResumoRetornoBean>myListResumo = new ArrayList<ResumoRetornoBean>();
		for (int i=0;i<12;i++)
		{
			ResumoRetornoBean  myResumo = new ResumoRetornoBean();
			myResumo.setMes(Util.lPad(String.valueOf(i+1),"0",2));
			myListResumo.add(myResumo);
		}
		ResumoRetornoBeanId.setListRetornoMes(myListResumo);
		/*Montar Lista de C�digo de Retornos*/
		montarListaCodigoRetorno(ResumoRetornoBeanId,true);  /*Montar lista de c�digos de entrega="S"*/
		montarListaCodigoRetorno(ResumoRetornoBeanId,false); /*Montar lista de c�digos de entrega="N"*/
	}
	
	public void montarListaCodigoRetorno(ResumoRetornoBean ResumoRetornoBeanId, boolean bTipEntregue)throws BeanException
	{
		CodigoRetornoBean myRetorno= new CodigoRetornoBean();
		for (int i=0;i<myRetorno.getRetorno(0,0).size();i++) {
			if (myRetorno.getRetorno(i).getNumRetorno().trim().equals("09")) continue; /*Retirar Michel*/
			
			ResumoRetornoBean  myCodigoResumo = new ResumoRetornoBean();
			myCodigoResumo.getRetorno().setNumRetorno(myRetorno.getRetorno(i).getNumRetorno());
			myCodigoResumo.getRetorno().setDescRetorno(myRetorno.getRetorno(i).getDescRetorno());
			myCodigoResumo.getRetorno().setIndEntregue(bTipEntregue);			
			
			ArrayList <ResumoRetornoBean>myListCodigoResumo = new ArrayList<ResumoRetornoBean>();
			for (int z=0;z<12;z++)
			{
				ResumoRetornoBean  myCodResumo = new ResumoRetornoBean();
				myCodResumo.setMes(Util.lPad(String.valueOf(z+1),"0",2));
				myListCodigoResumo.add(myCodResumo);
			}
			myCodigoResumo.setListRetornoMes(myListCodigoResumo);				
			ResumoRetornoBeanId.getListCodigoRetornoMes().add(myCodigoResumo);
		}
	}
	
	public void montarPercentuais(ResumoRetornoBean ResumoRetornoBeanId)throws BeanException
	{
		BigDecimal bPercentual = new BigDecimal (0);
		/*Calcular Percentuais-C�digo de Retorno - Mensal*/
		for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) 
		{    
			for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++)
			{
				if (ResumoRetornoBeanId.getRetornos(j).getQtdTotalRetornada().equals("0"))
				  continue;
					
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(j).getQtdTotalRetornada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).setPercRetornada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-C�digo de Retorno - Geral*/			
			if (!ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getQtdTotalRetornada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getCodigoRetornos(i).setPercRetornada(String.valueOf(bPercentual));
			}
		}
		/*Calcular Percentuais-Enviados/Retornados/N�o Retornados-Geral*/
		for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
			/*Calcular Percentuais-Enviados-Geral*/
			if (!ResumoRetornoBeanId.getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercEnviada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-Retornados-C�digo de Retorno - Geral*/
			if (!ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercRetornada(String.valueOf(bPercentual));
			}
			/*Calcular Percentuais-N�o Retornados-Geral*/
			if (!ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada().equals("0"))
			{
				bPercentual = new BigDecimal(ResumoRetornoBeanId.getRetornos(i).getQtdTotalnaoRetornada());
				bPercentual=bPercentual.divide(new BigDecimal (ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada()), 100, BigDecimal.ROUND_HALF_EVEN);
				bPercentual=bPercentual.multiply(new BigDecimal (100));
				bPercentual=bPercentual.setScale(2,BigDecimal.ROUND_HALF_DOWN);
				ResumoRetornoBeanId.getRetornos(i).setPercnaoRetornada(String.valueOf(bPercentual));
			}
		}
	}
}