package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Marca_Modelo<br>
* <b>Description:</b>  Comando para Consultar Marcas<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class MarcaModeloCmd extends sys.Command {

	private static final String jspPadrao = "/REG/MarcaModeloConsulta.jsp";

	private String next;

	public MarcaModeloCmd() {
		next = jspPadrao;
	}

	public MarcaModeloCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();

		try {
			TAB.MarcaModeloBeanCol Marcabc = new TAB.MarcaModeloBeanCol();
			Marcabc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("MarcaModeloBeanColId", Marcabc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
