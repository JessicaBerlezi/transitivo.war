package REG;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Classe para promover a intera��o de ajuste das linhas dos arquivos Emitevex que apresentaram
 * pend�ncias de envio para a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */

public class NotifPendenciaRetCmd  extends sys.Command {
	
	private static final String jspPadrao = "/REG/NotifPendencia.jsp";
	public NotifPendenciaRetCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			
			String statusInteracao = req.getParameter("statusInteracao");
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
			if(OrgaoId == null)
				OrgaoId = new ACSS.OrgaoBean() ;	 
			ArquivoRecebidoBean arquivoRecId = new ArquivoRecebidoBean();					
			String acao = req.getParameter("acao");  
			String codOrgao = req.getParameter("codOrgao");
			arquivoRecId.setNomArquivo(req.getParameter("nomArquivo"));
			
			if(acao == null)
				acao = "";
			if(acao.equals("")){
				acao =" "; 
				statusInteracao = "1";
			}			
			
			/*
			 * A��o realizada ap�s a sele��o do �rg�o pelo usu�rio.
			 */ 
			else if(acao.equalsIgnoreCase("selecaoOrgao")){
				arquivoRecId.setCodOrgao(codOrgao);  
				arquivoRecId.setCodIdentArquivo("CODRET");
				
				OrgaoId.Le_Orgao(req.getParameter("codOrgao"),0);
				req.setAttribute("arquivos",arquivoRecId.getArquivoPendenteRetorno());	
				req.setAttribute("OrgaoId",OrgaoId);
				statusInteracao = "2";
			}
			
			/* 
			 * A��o realizada ap�s a sele��o do arquivo erro para detalhamento
			 * das linhas que possuem notifica��es pendentes.
			 */			
			else if(acao.equalsIgnoreCase("selecaoArquivo")){
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
				arquivoRecId.setCodStatus("9");
				arquivoRecId.getArquivoLinhaRetorno();
				req.setAttribute("arquivoRecId",arquivoRecId);
				statusInteracao = "3";
				nextRetorno = "/REG/NotifLinha.jsp";							
			}
			
			/*
			 * A��o realizada ap�s a sele��o de uma das linhas com pend�ncia de
			 * notifica��o, dentro do arquivo selecionado no passo anterior.
			 */
			else if(acao.equalsIgnoreCase("selecaoLinha")){
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));		
				arquivoRecId.setCodStatus("9");
				arquivoRecId.getArquivoLinhaRetorno();
				LinhaArquivoRec linhaRec = (LinhaArquivoRec)arquivoRecId.getLinhaArquivoRec().get(Integer.parseInt(req.getParameter("posicaoLinha")));
				linhaRec.setCodArquivo(arquivoRecId.getCodArquivo());
				req.setAttribute("linhaRecId",linhaRec);
				req.setAttribute("codLinha",linhaRec.getCodLinhaArqReq());
				statusInteracao = "4";
				nextRetorno = "/REG/NotifAcertoNotificacao.jsp";
			}
			
			// A��o realizada quando o usu�rio deseja cancelar uma ou mais linhas.
			else if(acao.equalsIgnoreCase("cancelarLinha")){
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
				arquivoRecId.setCodStatus("9");
				if(req.getParameterValues("linhas")!= null){
					linhaRec.cancelarLinha(req.getParameterValues("linhas"),req.getParameter("j_sigFuncao"));
					arquivoRecId.setMsgErro("Linha(s) cancelada(s) com sucesso");
				}
				else
					arquivoRecId.setMsgErro("Nenhuma linha selecionada!");
				arquivoRecId.getArquivoLinhaRetorno();
				req.setAttribute("arquivoRecId",arquivoRecId);
				statusInteracao = "3";
				nextRetorno = "/REG/NotifLinha.jsp";
			}
			
			/*
			 * A��o realizada quando o usu�rio altera as informa��es contidas dentro
			 * da linha, selecionado na passo anterior, acertando a notifica��o a
			 * ser enviada.
			 */
			else if(acao.equalsIgnoreCase("atualizar")){
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodLinhaArqRec(req.getParameter("codLinha"));
				
				/*
				 * Carrega toda a linha que est� sofrendo ajuste pelo usu�rio
				 * a partir do registro presente na base.
				 * Essa carga � feita para poder realizar a verifica��o de pend�ncias
				 * de envio para a linha.
				 */
				linhaRec.carregarLinha();
				
				/*
				 *  Recebe os par�metros da notifica��o da tela de ajuste e 
				 *  referencia esses valores na descri��o da linha.
				 *  Dependendo da a��o (Envio ou Retorno) as informa��es coletadas
				 *  ir�o diferir.
				 */
				StringBuffer stringBuf = new StringBuffer(linhaRec.getDscLinhaArqRec());				
				stringBuf.replace(9,18,req.getParameter("numNotificacao"));
				stringBuf.replace(37,45,req.getParameter("dataRecebimento"));
				stringBuf.replace(45,47,req.getParameter("codBaixa"));
				stringBuf.replace(47,51,req.getParameter("codCaixa"));
				stringBuf.replace(51,55,req.getParameter("codLote"));
				stringBuf.replace(95,107,req.getParameter("identRecebedor"));
				stringBuf.replace(55,95,req.getParameter("nomRecebedor"));
				linhaRec.setDscLinhaArqRec(stringBuf.toString());
				
				/*
				 * Caso verdadeiro a linha foi alterada com sucesso, caso contr�rio 
				 * � apresentado uma mensagem de erro com a referente pend�ncia.
				 * Dependendo da a��o (Envio ou Retorno) as informa��es coletadas
				 * ir�o diferir.
				 */ 					
				if(linhaRec.ajustaNotif(UsrLogado,req.getParameter("j_sigFuncao"))){
					arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
					arquivoRecId.setCodStatus("9");
					arquivoRecId.getArquivoLinhaRetorno();
					statusInteracao = "5";
					arquivoRecId.setMsgErro("Notifica��o atualizada com sucesso");
					req.setAttribute("arquivoRecId",arquivoRecId);
					nextRetorno = "/REG/NotifLinha.jsp";
				}
				else{
					statusInteracao = "4";
					req.setAttribute("codLinha",linhaRec.getCodLinhaArqReq());
					nextRetorno = "/REG/NotifAcertoNotificacao.jsp";
					req.setAttribute("linhaRecId",linhaRec);
				}
			}
			
			// Retorna a vari�vel statusIntera��o que ir� controlar a intera��o do usu�rio.
			req.setAttribute("statusInteracao",statusInteracao);
			req.setAttribute("codOrgao",codOrgao);
			req.setAttribute("nomArquivo",arquivoRecId.getNomArquivo());
		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return nextRetorno;
	}
	
}