package REG;

public class ControleVexBean extends sys.EmailBean{

	private String codArquivo;
	private String nomArquivo;
	private String datEnvio;
	private String mesDatEnvio;
	private String anoDataEnvio;
	private String numAutoInfracao;
	private String numPlaca;
	private String numNotificacao;
	private String tipNotificacao;
	private String codUF;
	private String numCEP;
	private String nomMunic;
	private String seqNotificacao;
	private String datRetorno;
	private String codArqRetorno;
	private String nomArqRetorno;
	private String mesDatRetorno;
	private String anoDatRetorno;
	private String codRetorno;
	private String indEntregue;
	private String sitCanc;
	private String pkCodControleVex;
	private String indFaturado;
	private String datProxRecuperacao;
	private String indContador;
	
	public ControleVexBean(){

		codArquivo = "";	
		nomArquivo = "";
		datEnvio = "";
		mesDatEnvio = "";
		anoDataEnvio = "";
		numAutoInfracao = "";
		numPlaca = "";
		numNotificacao = "";
		tipNotificacao = "";
		codUF = "";
		numCEP = "";
		nomMunic = "";
		seqNotificacao = "";
		datRetorno = "";
		codArqRetorno = "";
		nomArqRetorno = "";
		mesDatRetorno = "";
		anoDatRetorno = "";
		codRetorno = "";
		indEntregue = "";
		sitCanc = "";
		pkCodControleVex = "";
		indFaturado = "";
		datProxRecuperacao = "";
		indContador = "";

	}
	
	public void setNomArquivo(String nomArquivo){
		this.nomArquivo = nomArquivo;
		if (nomArquivo==null) this.nomArquivo="" ;
	}
	public String getNomArquivo(){
		return this.nomArquivo;
	}
	
	public void setCodArquivo(String codArquivo){
		this.codArquivo = codArquivo;
	}
	public String getCodArquivo(){
		return codArquivo;
	}
	
	public void setDatEnvio(String datEnvio){
		this.datEnvio = datEnvio;
		if (datEnvio==null) this.datEnvio="" ;
	}
	public String getDatEnvio(){
		return this.datEnvio;
	}

	public void setMesDatEnvio(String mesDatEnvio){
		this.mesDatEnvio = mesDatEnvio ; 
	}
	public String getMesDatEnvio (){
		return mesDatEnvio;
	}
	
	public void setAnoDataEnvio(String anoDataEnvio){
		this.anoDataEnvio = anoDataEnvio ; 
	}
	public String getAnoDataEnvio (){
		return anoDataEnvio;
	}

	public void setNumAutoInfracao(String numAutoInfracao){
		if(numAutoInfracao == null) numAutoInfracao = "";
		this.numAutoInfracao = numAutoInfracao ; 
	}
	public String getNumAutoInfracao (){
		return numAutoInfracao;
	}

	public void setNumPlaca(String numPlaca){
		this.numPlaca = numPlaca ; 
	}
	public String getNumPlaca (){
		return numPlaca;
	}

	public void setNumNotificacao(String numNotificacao){
		if(numNotificacao == null) numNotificacao = "";
		this.numNotificacao = numNotificacao ; 
	}
	public String getNumNotificacao (){
		return numNotificacao;
	}

	public void setTipNotificacao(String tipNotificacao){
		if(tipNotificacao == null) tipNotificacao = "";
		this.tipNotificacao = tipNotificacao ; 
	}
	public String getTipNotificacao (){
		return tipNotificacao;
	}

	public void setCodUF(String codUF){
		this.codUF = codUF ; 
	}
	public String getCodUF (){
		return codUF;
	}

	public void setNumCEP(String numCEP){
		this.numCEP = numCEP ; 
	}
	public String getNumCEP (){
		return numCEP;
	}

	public void setNomMunic(String nomMunic){
		this.nomMunic = nomMunic ; 
	}
	public String getNomMunic (){
		return nomMunic;
	}

	public void setSeqNotificacao(String seqNotificacao){
		if(seqNotificacao == null) seqNotificacao = "";
		this.seqNotificacao = seqNotificacao ; 
	}
	public String getSeqNotificacao (){
		return seqNotificacao;
	}

	public void setDatRetorno(String datRetorno){
		this.datRetorno = datRetorno ; 
	}
	public String getDatRetorno (){
		return datRetorno;
	}

	public void setCodArqRetorno(String codArqRetorno){
		this.codArqRetorno = codArqRetorno ; 
	}
	public String getCodArqRetorno (){
		return codArqRetorno;
	}

	public void setNomArqRetorno(String nomArqRetorno){
		this.nomArqRetorno = nomArqRetorno ; 
	}
	public String getNomArqRetorno (){
		return nomArqRetorno;
	}

	public void setMesDatRetorno(String mesDatRetorno){
		this.mesDatRetorno = mesDatRetorno ; 
	}
	public String getMesDatRetorno (){
		return mesDatRetorno;
	}

	public void setAnoDatRetorno(String anoDatRetorno){
		this.anoDatRetorno = anoDatRetorno ; 
	}
	public String getAnoDatRetorno (){
		return anoDatRetorno;
	}

	public void setCodRetorno(String codRetorno){
		this.codRetorno = codRetorno ; 
	}
	public String getCodRetorno (){
		return codRetorno;
	}


	public void setIndEntregue(String indEntregue){
		this.indEntregue = indEntregue ; 
	}
	public String getIndEntregue (){
		return indEntregue;
	}

	public void setSitCanc(String sitCanc){
		this.sitCanc = sitCanc ; 
	}
	public String getSitCanc (){
		return sitCanc;
	}

	public void setPkCodControleVex(String pkCodControleVex){
		this.pkCodControleVex = pkCodControleVex ; 
	}
	public String getPkCodControleVex (){
		return pkCodControleVex;
	}

	public void setIndFaturado(String indFaturado){
		this.indFaturado = indFaturado ; 
	}
	public String getIndFaturado (){
		return indFaturado;
	}

	public void setDatProxRecuperacao(String datProxRecuperacao){
		this.datProxRecuperacao = datProxRecuperacao ; 
	}
	public String getDatProxRecuperacao (){
		return datProxRecuperacao;
	}
	
	public String getDscCodRetorno()
	throws sys.BeanException {
		String dscCodRet = "";
	try {
		Dao dao = Dao.getInstance();
		dscCodRet = dao.BuscaDscCodRetorno(this.codRetorno);
	} 
	catch (Exception e) {
		throw new sys.BeanException(e.getMessage());
	} 
	return dscCodRet;
}
	
	
	public boolean getTemARDigitalizado()
	throws sys.BeanException {
		boolean arDig = false;
	try {
		Dao dao = Dao.getInstance();
		arDig = dao.getTemARDigitalizado(this);
	} 
	catch (Exception e) {
		arDig = false;
		throw new sys.BeanException(e.getMessage());
	} 
	return arDig;
}
	
	
	public void setIndContador(String indContador){
		this.indContador = indContador ; 
	}
	public String getIndContador (){
		return indContador;
	}
	
	
	
}
