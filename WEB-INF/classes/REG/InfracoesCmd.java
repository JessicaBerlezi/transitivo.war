package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Infracao<br>
* <b>Description:</b>  Comando para Consultar Infrac�es<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class InfracoesCmd extends sys.Command {

	private static final String jspPadrao = "/REG/InfracaoConsulta.jsp";

	private String next;

	public InfracoesCmd() {
		next = jspPadrao;
	}

	public InfracoesCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();
		Vector vEqua = new Vector();
		Vector vVal = new Vector();
		Vector vNum = new Vector();
		Vector vComp = new Vector();
		Vector vIndCond = new Vector();
		Vector vIndSusp = new Vector();
		Vector vDscGrav = new Vector();
		Vector vValDesc= new Vector();
				

		try {
			TAB.InfracaoBeanCol Infracaobc = new TAB.InfracaoBeanCol();
			Infracaobc.carregarCol("TODOS",vCod,vNom,vEqua,vVal,vNum,vComp,
			                       vIndCond,vIndSusp,vDscGrav,vValDesc );
			req.setAttribute("InfracaoBeanColId", Infracaobc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}
		return nextRetorno;
	}

}
