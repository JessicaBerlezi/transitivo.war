package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

/**
* <b>Title:</b>        Download de arquivos GRV e REJ<br>
* <b>Description:</b>  Comando para Download de arquivos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Elisson Dias	
* @version 1.0
*/

public class ConsultaDownloadCmd extends sys.Command {

  private static final String jspPadrao="/REG/consultaDownload.jsp";    
  private String next;

  public ConsultaDownloadCmd() {
		next = jspPadrao;
  }

	public ConsultaDownloadCmd(String next) {
		this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
  	
		String nextRetorno  = jspPadrao ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();

			//cria os Beans de Usuario e Perfil, se n�o existir			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();

			//cria o Bean para guardar os parametros da consulta			
			ConsultaDownloadBean ConsultaDownloadBeanId = (ConsultaDownloadBean) req.getAttribute("ConsultaDownloadBeanId");
			if (ConsultaDownloadBeanId == null) ConsultaDownloadBeanId = new ConsultaDownloadBean();
//			============================== WELLEM 04-01-2004======================
			//cria o Bean para guardar os Dados dos Arquivos Recebidos			
					  ArquivoRecebidoBean ArquivoRecebidoBeanId = (ArquivoRecebidoBean) req.getAttribute("ArquivoRecebidoBeanId");
					  if (ArquivoRecebidoBeanId == null) ArquivoRecebidoBeanId = new ArquivoRecebidoBean();
//			=========================================================================
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null)	usrLogado = new ACSS.UsuarioBean();
			
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) req.getSession().getAttribute
				 ("ParamSistemaBeanId");
						
			
			String datArqRecebidoBKP = param.getParamSist("DAT_ARQUIVO_RECEBIDO_BKP");
			
			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
				
			String sigFuncao = usrFunc.getJ_sigFuncao();
			Vector vetArquivos 	= new Vector();
			String acao = req.getParameter("acao");  
			if(acao == null) acao = " ";
			
			String arrIdentArquivos[][] = {
			        {"MR01","Autua��o"},
			        {"MX01","Defesa Pr�via"},
			        {"ME01","Penalidade"},
			        {"RECJARI","Rec./Result./1�Inst."},
			        {"MZ01","Troca Real Infrator"},
			        {"MC01","Cancelamento"},			        
			        {"CODRET","Retorno de Entrega"},
			        {"TVEP","Marca/Modelo"},
			        {"EMITEVEX","Emite VEX"},
			        {"MF01","Movimento Banc�rio"},
			        {"MA01","Estorno de TRI"},
				    {"LOG01","Log de Transa��es"},			        
                    {"FOTOVEX","Fotos VEX"}
			    };

			boolean bArqRecebidoBKP=false;
			if (Util.DifereDias(datArqRecebidoBKP,req.getParameter("De"))<0) bArqRecebidoBKP=true;
			if (acao.equals("buscaDownloads")) {
				ConsultaDownloadBeanId.setCodOrgao(req.getParameter("codOrgao"));				
				ConsultaDownloadBeanId.setCodIdentArquivo(req.getParameter("codIdentArquivo"));				
				ConsultaDownloadBeanId.setDataIni(req.getParameter("De"));
				ConsultaDownloadBeanId.setDataFim(req.getParameter("Ate"));
				ConsultaDownloadBeanId.LeConsulta(OrgaoBeanId,usrLogado,bArqRecebidoBKP);
				
			}
			
			else if (acao.equals("DetalheArquivo") || acao.equals("ImprimeDetalheArq")) {				  
				
				LinhaArquivoRec LinhaArquivoRecId = new LinhaArquivoRec();
				String Arq="";			 //			Guarda o codArquivo
				String nmArquivo="";			 //			Guarda o nome do arquivo
				String codOrgao="";		 //Guarda o Org�o de Atua��o
				
				
				Arq=req.getParameter("codArquivoRec");
				nmArquivo=req.getParameter("nmArquivo");
				codOrgao = req.getParameter("codOrgao");
				
				if (codOrgao.equals("-1"))
				{
					codOrgao = usrLogado.getCodOrgaoAtuacao();
				}
				
				ArquivoRecebidoBeanId.carregaArquivoDetalhe(LinhaArquivoRecId, Arq, codOrgao,bArqRecebidoBKP);
				
				if (acao.equals("DetalheArquivo")) {
					nextRetorno = "/REG/DetalheArqDownload.jsp";
				}
				else {
					nextRetorno = "/REG/DetalheArqDownloadImp.jsp";
					String tituloConsulta = "RELAT�RIO - ARQUIVO DE DOWNLOAD : " + nmArquivo;
					req.setAttribute("tituloConsulta", tituloConsulta);
				}
				
				req.setAttribute("nmArquivo", nmArquivo);
				req.setAttribute("codOrgao", codOrgao);
				req.setAttribute("codArquivoRec", Arq);
				req.setAttribute("sigOrgaoAtu", usrLogado.getSigOrgaoAtuacao());
				req.setAttribute("LinhaArquivoRecId", LinhaArquivoRecId);
			}
			//============================== WELLEM 04-01-2004======================
			else if (acao.equals("ReimprimirProtocolo")) {				  
				String OrgAtu="";		 //Guarda o Org�o de Atua��o				
				String SigOrgAtu=""; //Guarda a sigla do Org�o de Atua��o
				String Arq="";			 //			Guarda o codArquivo
				
				OrgAtu= usrLogado.getCodOrgaoAtuacao();
				Arq=req.getParameter("guardaArquivoRec");
				SigOrgAtu= usrLogado.getSigOrgaoAtuacao();
				ArquivoRecebidoBeanId.carregaArqRecebido(Arq,OrgAtu,SigOrgAtu);
				
				nextRetorno = "/REG/ProtocoloRecebimentoArq.jsp";		
				req.setAttribute("ArquivoRecebidoBeanId", ArquivoRecebidoBeanId);
			}
			//=========================================================================
			else if (acao.equals("ImprimeDownload")) {				
							ConsultaDownloadBeanId.setCodOrgao(req.getParameter("codOrgao"));				
							ConsultaDownloadBeanId.setCodIdentArquivo(req.getParameter("codIdentArquivo"));				
							ConsultaDownloadBeanId.setDataIni(req.getParameter("De"));
							ConsultaDownloadBeanId.setDataFim(req.getParameter("Ate"));
							ConsultaDownloadBeanId.LeConsulta(OrgaoBeanId,usrLogado,bArqRecebidoBKP);
							nextRetorno="/REG/consultaDownloadImp.jsp";
				
						}
		
				req.setAttribute("ConsultaDownloadBeanId", ConsultaDownloadBeanId);
				req.setAttribute("OrgaoBeanId", OrgaoBeanId);						
				req.setAttribute("arrIdentArquivos", arrIdentArquivos);
			 
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}