package REG;

import java.util.Vector;

/**
* <b>Title:</b>        	SMIT - Bean de Consulta para os Arquivos Recebidos e Download<br>
* <b>Description:</b>  	<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Mind Informatica<br>
* @author  				Elisson Dias
* @version 				1.0
*/

public class ConsultaDownloadBean extends sys.HtmlPopupBean {

	private String codOrgao;
	private String codIdentArquivo;
	private String dataIni;
	private String dataFim;
	private String msgErro;
	private Vector vetArquivos;

	public ConsultaDownloadBean() throws sys.BeanException {

		super();

		codOrgao = "-1";
		codIdentArquivo = "0";
		dataIni = "01" + sys.Util.formatedToday().substring(2, 10);
		dataFim = sys.Util.formatedToday().substring(0, 10);
		msgErro = "";
		vetArquivos = new Vector();
	}

	/**
	 * @return
	 */
	public String getCodIdentArquivo() {
		return codIdentArquivo;
	}

	/**
	 * @return
	 */
	public String getCodOrgao() {
		return codOrgao;
	}

	/**
	 * @return
	 */
	public String getDataFim() {
		return dataFim;
	}

	/**
	 * @return
	 */
	public String getDataIni() {
		return dataIni;
	}

	/**
	 * @param string
	 */
	public void setCodIdentArquivo(String string) {
		codIdentArquivo = string;
	}

	/**
	 * @param string
	 */
	public void setCodOrgao(String string) {
		codOrgao = string;
	}

	/**
	 * @param string
	 */
	public void setDataFim(String string) {
		dataFim = string;
	}

	/**
	 * @param string
	 */
	public void setDataIni(String string) {
		dataIni = string;
	}

	/**
	 * @return
	 */
	public String getMsgErro() {
		return msgErro;
	}

	/**
	 * @param string
	 */
	public void setMsgErro(String string) {
		msgErro = string;
	}

	public void LeConsulta(ACSS.OrgaoBean OrgaoBeanId, ACSS.UsuarioBean usrLogado,boolean bArqRecebidoBKP)
		throws sys.BeanException {
		String strCodOrgao = "";
		String strOrg = OrgaoBeanId.getOrgaos(usrLogado);
		String codOrgao = this.getCodOrgao();
		if (this.getCodOrgao().equals("-1")) {
			String arrOrgao[] = strOrg.split(","); //codigos e siglas
			String arrCodOrgao[] = new String[arrOrgao.length / 2];

			for (int i = 0; i < arrOrgao.length / 2; i++) {
				arrCodOrgao[i] = arrOrgao[i * 2 + 0]; //Somente os c�digos
				if (i < arrCodOrgao.length - 1) {
					strCodOrgao += arrCodOrgao[i] + ","; //Somente os c�digos - String
				} else {
					strCodOrgao += arrCodOrgao[i]; //Somente os c�digos - String
				}

			}
			//Necessario retirar as "" de todos os orgaos para fazer a consulta SQL
			strCodOrgao = strCodOrgao.replaceAll("\"", "");

		} else
			strCodOrgao = codOrgao.replaceAll("\"", "");

		String codIdent = this.getCodIdentArquivo();
		if (this.getCodIdentArquivo().equals("0")) {
			//Se o codigo for 0 todos os arquivos serao retornados
			codIdent = "' or '1'='1";
		}

		String dataIni = this.getDataIni();
		String dataFim = this.getDataFim();

		try {
			Dao dao = Dao.getInstance();
			this.setVetArquivos(
				dao.BuscaArquivosProcessadosConsulta(
					usrLogado,
					codIdent,
					strCodOrgao,
					dataIni,
					dataFim,
					bArqRecebidoBKP));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}

	}

	/**
	 * @return
	 */
	public Vector getVetArquivos() {
		return vetArquivos;
	}

	/**
	 * @param vector
	 */
	public void setVetArquivos(Vector vector) {
		vetArquivos = vector;
	}

}
