package REG;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class acompPendEmitevexXls {
	
	public void write(NotifControleBean notifControle, String titulo, String arquivo) throws IOException, WriteException
	{
	
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio Pend�ncia Emitevex", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 9/2;			
			
			Label label = new Label(celulaTitulo, 1, titulo, formatoTitulo);
			sheet.addCell(label);
			
			label = new Label(0, 2, "SEQ", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(0, 5);
			
			label = new Label(1, 2, "NOTIFICA��O", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(1, 15);
			
			label = new Label(2, 2, "AUTO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 15);
			
			label = new Label(3, 2, "PLACA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 15);
			
			label = new Label(4, 2, "EMISS�O", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 15);
			
			label = new Label(5, 2, "ARQUIVO RECEBIDO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 30);			
			
			
			Iterator it = notifControle.getListaNotifs().iterator();
			NotifControleBean relatorioImp  = new NotifControleBean();
			String datEmisao="";
			int contador =0;
			if( notifControle.getListaNotifs().size()>0 ) { 
				while (it.hasNext()) {
					contador++;
				
					relatorioImp = (NotifControleBean)it.next() ;
					   
					   SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						if (relatorioImp.getDatEmissao() != null)
							datEmisao=df.format(relatorioImp.getDatEmissao());
						else
							datEmisao=(df.format(new Date()));
					   
					   
					   label = new Label(0, cont, String.valueOf(seq)); 
					   sheet.addCell(label);
					   
					   
					   label = new Label(1, cont, relatorioImp.getNumNotificacao()); 
					   sheet.addCell(label);
					   
					   label = new Label(2, cont, relatorioImp.getNumAutoInfracao()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, cont, relatorioImp.getNumPlaca()); 
					   sheet.addCell(label);
					   
					   label = new Label(4, cont, datEmisao); 
					   sheet.addCell(label);
					   
					   label = new Label(5, cont, relatorioImp.getNomArquivo()); 
					   sheet.addCell(label);					   
					   
					   
					   if( cont >= 30000 ) { 				
							
							label = new Label(celulaTitulo,cont + 3, "O n�mero m�ximo de registros(30000) foi atingido", formatoTitulo);
							sheet.addCell(label);	
						break;	
						}
					   
					   
					   
					   cont++;
					   seq++;
				}
			}
			
            
			
			workbook.write(); 
			workbook.close();
			
			//System.out.println("ARQUIVO GERADO COM SUCESSO");
			
		}catch (Exception e) {	
			throw new IOException("GeraXls: " + e.getMessage());	}
	
	}

}
