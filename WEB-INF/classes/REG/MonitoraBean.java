package REG;

import java.util.ArrayList;
import java.util.List;

/**
* <b>Title:</b>        Consulta Monitor - Bean<br>
* <b>Description:</b>  Permite o monitoramento dos Robot<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
 */
public class MonitoraBean extends sys.MonitorLog{
	
	public final String COR_MSG_OK = "#0033FF";
	public final String COR_MSG_ERRO = "#FF0000";
	
	private List dados;
	private String datIni;
	private String datFim;
	private String codTipoMensagem;
	private String codMonitor;
	private String corMsg;
	private String ordem;
	
	public MonitoraBean(){
		super(); 
		dados = new ArrayList();
		datIni = "";
		datFim = "";
		codTipoMensagem = "";
		codMonitor = "0";
		corMsg = "";
		ordem = "Data";
	}
	public void setCodMonitor(String codMonitor){
		this.codMonitor = codMonitor;
	}
	public String getCodMonitor(){
		return this.codMonitor;
	}
	
	public void setCodTipoMensagem(String codTipoMensagem){
	  this.codTipoMensagem = codTipoMensagem;
	}
	public String getCodTipoMensagem(){
	  return this.codTipoMensagem;
	}
	
	public void setOrdem(String ordem){
	  this.ordem = ordem;
	}
	public String getOrdem(){
	  return this.ordem;
	}
	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public void setDatIni(String dataIni){
		this.datIni = dataIni;
	}
	public String getDatIni() { 
		return this.datIni; 
	}
	
	public void setDatFim(String dataFim){
		this.datFim = dataFim;
	}
	public String getDatFim(){ 
		return this.datFim; 
	}
	
	public void setCorMsg(String corMsg){
		this.corMsg = corMsg;
	}
	public String getCorMsg(){
		return this.corMsg;
	}
	
	
	public boolean consultaMonitor(MonitoraBean monitor, String ordem)
		throws sys.BeanException {
		boolean ok = false;
		try {
			Dao dao = Dao.getInstance();
			if(dao.ConsultaDadosMonitor(monitor,ordem)== true){
				ok = true;
				if (ordem.equals("O")) this.setOrdem("Org�o");
				if (ordem.equals("A")) this.setOrdem("Arquivo");
				if (ordem.equals("R")) this.setOrdem("Robot");
			}
			 
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
		return ok;
	}
}   
