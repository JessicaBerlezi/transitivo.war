package REG;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Classe para promover a intera��o de ajuste das linhas dos arquivos Emitevex que apresentaram
 * pend�ncias de envio para a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */

public class NotifPendenciaEnvCmd  extends sys.Command {
	
	private static final String jspPadrao = "/REG/NotifPendencia.jsp";    	
	public NotifPendenciaEnvCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			
			String statusInteracao = req.getParameter("statusInteracao");
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
			if(OrgaoId == null)
				OrgaoId = new ACSS.OrgaoBean() ;	 
			ArquivoRecebidoBean arquivoRecId = new ArquivoRecebidoBean();					
			String acao = req.getParameter("acao");  
			String codOrgao = req.getParameter("codOrgao");
			String todosOrgaos = req.getParameter("todosOrgaos");
			String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
			arquivoRecId.setNomArquivo(req.getParameter("nomArquivo"));
			
			String[] orgaos = null;
			// Verifica se a consulta ser� feita para todos os �rg�os
			if (todosOrgaos != null) {
				if (todosOrgaos.equalsIgnoreCase("null")) {
					todosOrgaos = null;
					orgaos = new String[] {codOrgao};
				} else
					orgaos = todosOrgaosCod;
			} else
				orgaos = new String[] {codOrgao};
			
			if(acao == null)
				acao = "";
			if(acao.equals("")){
				acao =" "; 
				statusInteracao = "1";
			}
			
			/*
			 * A��o realizada ap�s a sele��o do �rg�o pelo usu�rio.
			 */ 
			else if(acao.equalsIgnoreCase("selecaoOrgao")){
				// Verifica se a consulta ser� feita para todos os �rg�os
				if (todosOrgaos != null) {
					if (todosOrgaos.equalsIgnoreCase("null")) {
						OrgaoId.Le_Orgao(codOrgao,0);
					} else
						OrgaoId.setSigOrgao("TODOS");
				} else
					OrgaoId.Le_Orgao(codOrgao,0);
				
				arquivoRecId.setCodIdentArquivo("EMITEVEX");
				List arquivos = arquivoRecId.getArquivoPendenteEnvio(orgaos);
				req.setAttribute("arquivos",arquivos);	
				req.setAttribute("OrgaoId",OrgaoId);
				statusInteracao = "2";
			}
			
			/* 
			 * A��o realizada ap�s a sele��o do arquivo erro para detalhamento
			 * das linhas que possuem notifica��es pendentes.
			 */			
			else if(acao.equalsIgnoreCase("selecaoArquivo")){
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
				arquivoRecId.setCodStatus("9");
				arquivoRecId.getArquivoLinhaEnvio(orgaos);
				req.setAttribute("arquivoRecId",arquivoRecId);
				req.setAttribute("todosOrgaosCod", todosOrgaosCod);
				statusInteracao = "3";
				nextRetorno = "/REG/NotifLinha.jsp";							
			}
			
			/*
			 * A��o realizada ap�s a sele��o de uma das linhas com pend�ncia de
			 * notifica��o, dentro do arquivo selecionado no passo anterior.
			 */
			else if(acao.equalsIgnoreCase("selecaoLinha")){				
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));		
				arquivoRecId.setCodStatus("9");
				arquivoRecId.getArquivoLinhaEnvio(orgaos);
				LinhaArquivoRec linhaRec = (LinhaArquivoRec)arquivoRecId.getLinhaArquivoRec().get(Integer.parseInt(req.getParameter("posicaoLinha")));
				linhaRec.setCodArquivo(arquivoRecId.getCodArquivo());				
				req.setAttribute("linhaRecId",linhaRec);
				req.setAttribute("codLinha",linhaRec.getCodLinhaArqReq());
				req.setAttribute("todosOrgaosCod", todosOrgaosCod);
				statusInteracao = "4";
				nextRetorno = "/REG/NotifAcertoNotificacao.jsp";
			}
			
			// A��o realizada quando o usu�rio deseja cancelar uma ou mais linhas.
			else if(acao.equalsIgnoreCase("cancelarLinha")){				
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
				arquivoRecId.setCodStatus("9");
				if(req.getParameterValues("linhas")!= null){
					linhaRec.cancelarLinha(req.getParameterValues("linhas"),req.getParameter("j_sigFuncao"));
					arquivoRecId.setMsgErro("Linha(s) cancelada(s) com sucesso");
				}
				else
					arquivoRecId.setMsgErro("Nenhuma linha selecionada!");
				arquivoRecId.getArquivoLinhaEnvio(orgaos);
				req.setAttribute("arquivoRecId",arquivoRecId);
				req.setAttribute("todosOrgaosCod", todosOrgaosCod);
				statusInteracao = "3";
				nextRetorno = "/REG/NotifLinha.jsp";
			}
			
			/*
			 * A��o realizada quando o usu�rio altera as informa��es contidas dentro
			 * da linha, selecionado na passo anterior, acertando a notifica��o a
			 * ser enviada.
			 */
			else if(acao.equalsIgnoreCase("atualizar")){
				LinhaArquivoRec linhaRec = new LinhaArquivoRec();
				linhaRec.setCodLinhaArqRec(req.getParameter("codLinha"));
				
				/*
				 * Carrega toda a linha que est� sofrendo ajuste pelo usu�rio
				 * a partir do registro presente na base.
				 * Essa carga � feita para poder realizar a verifica��o de pend�ncias
				 * de envio para a linha.
				 */
				linhaRec.carregarLinha();
				
				/*
				 *  Recebe os par�metros da notifica��o da tela de ajuste e 
				 *  referencia esses valores na descri��o da linha.
				 *  Dependendo da a��o (Envio ou Retorno) as informa��es coletadas
				 *  ir�o diferir.
				 */
				StringBuffer stringBuf = new StringBuffer(linhaRec.getDscLinhaArqRec());
				stringBuf.replace(6,15,req.getParameter("numNotificacao"));
				stringBuf.replace(280,292,req.getParameter("numAuto"));
				stringBuf.replace(487,493,req.getParameter("orgAtuador"));
				stringBuf.replace(175,212,req.getParameter("nomMunicipio"));
				stringBuf.replace(162,170,req.getParameter("dataInfracao"));
				stringBuf.replace(170,175,req.getParameter("horaInfracao"));
				stringBuf.replace(475,476,req.getParameter("tipoReemissao"));
				stringBuf.replace(478,479,req.getParameter("indSuspensao"));
				stringBuf.replace(479,480,req.getParameter("tipoNotificacao"));
				stringBuf.replace(70,78,req.getParameter("vencimentoMulta"));
				stringBuf.replace(78,85,req.getParameter("valorMulta"));
				stringBuf.replace(110,117,req.getParameter("numPlaca"));
				stringBuf.replace(631,641,req.getParameter("especie"));
				stringBuf.replace(85,110,req.getParameter("marcaModelo"));
				stringBuf.replace(641,653,req.getParameter("categoriaVeiculo"));
				stringBuf.replace(653,671,req.getParameter("tipoVeiculo"));
				stringBuf.replace(671,679,req.getParameter("corVeiculo"));
				linhaRec.setDscLinhaArqRec(stringBuf.toString());
				
				/*
				 * Caso verdadeiro a linha foi alterada com sucesso, caso contr�rio 
				 * � apresentado uma mensagem de erro com a referente pend�ncia.
				 * Dependendo da a��o (Envio ou Retorno) as informa��es coletadas
				 * ir�o diferir.
				 */ 					
				if(linhaRec.ajustaNotif(UsrLogado,req.getParameter("j_sigFuncao"))){
					arquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
					arquivoRecId.setCodStatus("9");
					arquivoRecId.getArquivoLinhaEnvio(orgaos);
					statusInteracao = "5";
					arquivoRecId.setMsgErro("Notifica��o atualizada com sucesso");
					req.setAttribute("arquivoRecId",arquivoRecId);
					nextRetorno = "/REG/NotifLinha.jsp";
				}
				else{
					statusInteracao = "4";
					req.setAttribute("codLinha",linhaRec.getCodLinhaArqReq());
					nextRetorno = "/REG/NotifAcertoNotificacao.jsp";
					req.setAttribute("linhaRecId",linhaRec);
				}
				req.setAttribute("todosOrgaosCod", todosOrgaosCod);
			}
			
			// Retorna a vari�vel statusIntera��o que ir� controlar a intera��o do usu�rio.
			req.setAttribute("statusInteracao",statusInteracao);
			req.setAttribute("codOrgao",codOrgao);
			req.setAttribute("todosOrgaos",todosOrgaos);
			req.setAttribute("nomArquivo",arquivoRecId.getNomArquivo());
		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return nextRetorno;
	}
	
}