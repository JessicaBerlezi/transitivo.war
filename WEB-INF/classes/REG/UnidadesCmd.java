package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Unidade<br>
* <b>Description:</b>  Comando para Consultar Unidades<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class UnidadesCmd extends sys.Command {

	private static final String jspPadrao = "/REG/UnidadeConsulta.jsp";

	private String next;

	public UnidadesCmd() {
		next = jspPadrao;
	}

	public UnidadesCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();
		Vector vCodOrgao = new Vector();

		try {

			TAB.UnidadeBeanCol Unidadebc = new TAB.UnidadeBeanCol();
			Unidadebc.carregarCol("TODOS", vCod, vNom, vCodOrgao);
			req.setAttribute("UnidadeBeanColId", Unidadebc);

		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
