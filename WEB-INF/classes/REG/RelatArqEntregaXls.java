package REG;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelatArqEntregaXls {
	
	public void write(RelatArqEntregaBean RelArqDownlBeanId, String titulo, String arquivo) throws IOException, WriteException
	{
	
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio de Entregas", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTotal = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 6/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
			
			label = new Label(0, 2, "SEQ", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(0, 5);
			
			label = new Label(1, 2, "DATA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(1, 20);
			
			label = new Label(2, 2, "REGISTRADOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 20);
			
			label = new Label(3, 2, "EMITIDOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 10);
			
			label = new Label(4, 2, "RETORNADOS", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 20);
			
			label = new Label(5, 2, "PENDENTES", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 20);
			
			label = new Label(6, 2, "TEMPO M�DIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(6, 20);
			
			
			Iterator it = RelArqDownlBeanId.getBeans().iterator();
			RelatArqEntregaBean relatorioImp  = new RelatArqEntregaBean();
			if( RelArqDownlBeanId.getBeans().size()>0 ) { 
				while (it.hasNext()) {
					   relatorioImp = (RelatArqEntregaBean)it.next() ;
					   
					   label = new Label(0, cont, String.valueOf(seq)); 
					   sheet.addCell(label);
					   
					   label = new Label(1, cont, df.format(relatorioImp.getDatProc())); 
					   sheet.addCell(label);
					   
					   label = new Label(2, cont, relatorioImp.getQtdRegistrado().toString()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, cont, relatorioImp.getQtdRecebido().toString()); 
					   sheet.addCell(label);
					   
					   label = new Label(4, cont, relatorioImp.getQtdEntregue().toString()); 
					   sheet.addCell(label);
					   
					   label = new Label(5, cont, relatorioImp.getQtdPendente().toString()); 
					   sheet.addCell(label);
					   
					   label = new Label(6, cont, relatorioImp.getTempoMedio().toString()); 
					   sheet.addCell(label);
					   
					   
					   cont++;
					   seq++;
				}
			}
			
			label = new Label(0, cont, "Total:", formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(2, cont, RelArqDownlBeanId.getTotalQtdRegistrado().toString(), formatoTotal); 
			sheet.addCell(label);
			
			
			label = new Label(3, cont, RelArqDownlBeanId.getTotalQtdRecebido().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(4, cont, RelArqDownlBeanId.getTotalQtdEntregue().toString(), formatoTotal); 
			sheet.addCell(label);
			
			label = new Label(6, cont, RelArqDownlBeanId.getTotalTempoMedio().toString(), formatoTotal); 
			sheet.addCell(label);
			
			workbook.write(); 
			workbook.close();
			
			
		}catch (Exception e) {	
			throw new IOException("GeraXls: " + e.getMessage());	}
	
	}

}
