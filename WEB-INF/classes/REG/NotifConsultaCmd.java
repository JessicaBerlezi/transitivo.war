package REG;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */
public class NotifConsultaCmd  extends sys.Command {
	private static final String jspPadrao="/REG/NotifConsulta.jsp";    
	private String next;
	private String statusInteracao = null;
	
	public NotifConsultaCmd() {
		next = jspPadrao;
	}
	
	public NotifConsultaCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  				
			String acao = req.getParameter("acao");  
			
			if(acao == null)
				acao = "";
			if(acao.equals("")){
				acao =" "; 
				statusInteracao = "1";
			}			
			
			/*
			 * A��o realizada ap�s o usu�rio preencher o auto de infra��o e/ou a notifica��o de seu interesse.
			 */ 
			else if(acao.equalsIgnoreCase("consulta")){
				NotifControleBean notificacao = new NotifControleBean();
				notificacao.setNumAutoInfracao(req.getParameter("auto"));
				notificacao.setNumNotificacao(req.getParameter("notificacao"));	
				List notificacoes = null;
				Dao dao = Dao.getInstance();
				/*
				 * Bloco de IFs para realizar a consulta sobre as notifica��es de 
				 * acordo com as cl�usulas de filtros determinadas, podendo ser pelo
				 * auto de infra��o ou pelo n�mero da notifica��o.
				 */
				if( (!notificacao.getNumAutoInfracao().equals("")) && (!notificacao.getNumNotificacao().equals("")))
					notificacoes = dao.getNotificacoes(notificacao);				
				else if(!notificacao.getNumAutoInfracao().equals(""))
					notificacoes = dao.getNotificacoes(notificacao);
				else if(!notificacao.getNumNotificacao().equals(""))
					notificacoes = dao.getNotificacoes(notificacao);
				if(notificacoes.size()==0){
					notificacao.setMsgErro("Nenhuma Notifica��o Encontrada !");
					req.setAttribute("NotifControleId",notificacao);
					statusInteracao = "1";
				}
				else{
					req.setAttribute("notificacoes",notificacoes);
					statusInteracao = "2";
				}
			}
			req.setAttribute("statusInteracao",statusInteracao);
		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
