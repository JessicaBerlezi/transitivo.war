package REG;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class GeraXlsARDigitPendencia extends HttpServlet {

	private static final long serialVersionUID = 1L;
	String jspPadrao = "/REG/ARDigitPendenciaImp.jsp";
	String JSPDIR   = "/WEB-INF/jsp/";

	protected void service(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException {
		{
			String codOrgao = "";

			try {
				// cria os Beans de sessao, se n�o existir
				HttpSession session = req.getSession();
				ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean) req
						.getAttribute("OrgaoId");
				if (OrgaoId == null)
					OrgaoId = new ACSS.OrgaoBean();

				Map meses = new HashMap();
				meses.put("01", "JANEIRO");
				meses.put("02", "FEVEREIRO");
				meses.put("03", "MAR�O");
				meses.put("04", "ABRIL");
				meses.put("05", "MAIO");
				meses.put("06", "JUNHO");
				meses.put("07", "JULHO");
				meses.put("08", "AGOSTO");
				meses.put("09", "SETEMBRO");
				meses.put("10", "OUTUBRO");
				meses.put("11", "NOVEMBRO");
				meses.put("12", "DEZEMBRO");


				String acao = req.getParameter("acao");
				if (acao == null)
					acao = "";
				if (acao.equals("")) 
				{
					acao = " ";
				}

				String contexto = req.getParameter("contexto");
				if (contexto == null)
					contexto = "";

				String nomArquivo = req.getParameter("nomArquivo");
				if (nomArquivo == null)
					nomArquivo = "";


				if (acao.equals("GeraExcel")) 
				{
					String detalhe = req.getParameter("detalhe");
					
					String statusAuto = req.getParameter("codStatusAuto");
					if (statusAuto == null) 
						statusAuto = req.getParameter("statusAuto");
					
					String sitEntrega = req.getParameter("sitEntrega");
					
					String totalMesAno = req.getParameter("totalMesAno");
					
					codOrgao = req.getParameter("codOrgao");
					
					String todosOrgaos = req.getParameter("todosOrgaos");
					
					String sentido = req.getParameter("sentido");
					
					String prazoDigitado = req.getParameter("prazoDigitado");
					
					String primeiroRegistro = req.getParameter("primeiroRegistro");
					if (primeiroRegistro == null) primeiroRegistro = "";
					
					String primeiroAnterior = req.getParameter("primeiroAnterior");
					if (primeiroAnterior == null) primeiroAnterior = "";
					
					String ultimoRegistro = req.getParameter("ultimoRegistro");
					if (ultimoRegistro == null) ultimoRegistro = "";
					
					String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
					int prazo = Integer.parseInt(prazoDigitado);
					
					String[] orgaos = null;
					// Verifica se a consulta ser� feita para todos os �rg�os
					if (todosOrgaos != null) 
					{
						if (todosOrgaos.equalsIgnoreCase("null")) 
						{
							OrgaoId.Le_Orgao(codOrgao,0);
							todosOrgaos = null;
							orgaos = new String[] {codOrgao};
						} 
						else 
						{
							OrgaoId.setSigOrgao("TODOS");
							orgaos = todosOrgaosCod;
						}
					} 
					else 
					{
						OrgaoId.Le_Orgao(codOrgao,0);
						orgaos = new String[] {codOrgao};
					}				
					
					try{			
						Dao dao = Dao.getInstance();
						NotifControleBean notifControle = (NotifControleBean) req.getSession().getAttribute("NotifControleId");
						if( notifControle == null ) 
							notifControle = new NotifControleBean();
						
						if(statusAuto.equalsIgnoreCase("autuacao"))
							notifControle.setCodStatusAuto("001");
						else if(statusAuto.equalsIgnoreCase("penalidade"))
							notifControle.setCodStatusAuto("011");
						
						notifControle.setCodOrgaoAutuacao(codOrgao);
						
						ParamSistemaBean parametro = (ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");
						String rowNum = parametro.getParamSist("LIMITE_REL_PENDENCIAS_AR");
						String rowNumImp = "";
						
						if (sentido == null) 
							sentido = "Prox";
						
						// Alterado em 29.06.2006 por Marlon Falzetta.
						// � desejado que a impressao seja para todos os dados e nao
						// somente para os visualizados na tela.
						HashMap proxAnt = new HashMap();
						if(sentido.equals("Prox")){
							proxAnt.put("primeiroRegistro", primeiroRegistro);
							proxAnt.put("ultimoRegistro", ultimoRegistro);
						}
						else{
							proxAnt.put("primeiroRegistro", ""); 
							proxAnt.put("ultimoRegistro", "");
							
						}
							
						proxAnt.put("primeiroAnterior", primeiroAnterior);
						
						proxAnt.put("sentido", sentido);
						
						dao.consultaARDigitPendenciaDet(notifControle, prazo, totalMesAno, rowNum, proxAnt, orgaos, sitEntrega, rowNumImp);
						
						GregorianCalendar calendario = new GregorianCalendar();
						calendario.setTime(new Date());

						if (notifControle.getListaNotifs().size() > 0) 
						{
							ARDigitPendenciaXls geraxls = new ARDigitPendenciaXls();
							
							String tituloConsulta = "RELAT�RIO - PEND�NCIAS DE DIGITALIZA��O DE AR - " + 
							meses.get(totalMesAno.substring(0, 2)) + "/" + 
							String.valueOf(calendario.get(GregorianCalendar.YEAR));
							rowNumImp = "-1";
							dao.consultaARDigitPendenciaDet(notifControle, prazo, totalMesAno, rowNum, proxAnt, orgaos, sitEntrega, rowNumImp);
							ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
							paramSys.setCodSistema("24"); // M�dulo GER
							paramSys.PreparaParam();
							String arquivo = paramSys.getParamSist(contexto)+ "/" + nomArquivo;
							geraxls.write(notifControle, tituloConsulta,arquivo); 
							
							req.getSession().setAttribute("ParamSistemaBeanId",paramSys);
							RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
							rd.forward(req, resp);
						} 
						else 
						{
							req.setAttribute("NotifControleId", notifControle);
							RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + jspPadrao);
							rd.forward(req, resp);
						}

					} 
					catch (Exception e) 
					{
						throw new sys.CommandException(e.getMessage());
					}
				}
			} 
			catch (Exception e) 
			{
				throw new ServletException(e.getMessage());
			}
		}
	}
}
