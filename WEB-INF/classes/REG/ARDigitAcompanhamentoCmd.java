package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author Pedro N�brega
 *
 */

public class ARDigitAcompanhamentoCmd extends sys.Command{
	private static final String jspPadrao = "/REG/ARDigitAcompanhamento.jsp";
	public ARDigitAcompanhamentoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		HttpSession session = req.getSession();
		ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId") ;
		
		String datIniRel = "";
		String datFimRel = "";
		try {
			ARDigitAcompanhamentoBean ARDigitBeanId = new ARDigitAcompanhamentoBean();
			
			String acao = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if ( acao.equals("") ) {				
				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
				
				ARDigitBeanId.setDatInicial(datIniRel);
				ARDigitBeanId.setDatFinal(datFimRel);
				req.setAttribute("ARDigitBeanId", ARDigitBeanId);
			
			} else if(acao.equals("ImprimeRelatorio")) {
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");
				
				String todosOrgaos = req.getParameter("todosOrgaos");				
				String codOrgao = req.getParameter("codOrgao");
				
				ACSS.OrgaoBean orgao = new ACSS.OrgaoBean();
				// Verifica se a consulta ser� feita para todos os �rg�os
				if (todosOrgaos != null) {										
					orgao.setSigOrgao("TODOS");
					ARDigitBeanId.getSistema().setCodSistema(UsuarioFuncBeanId.getCodSistema());
					ARDigitBeanId.getUsuario().setCodUsuario(UsuarioFuncBeanId.getCodUsuario());
				
				} else {
					orgao.Le_Orgao(codOrgao,0);
					ARDigitBeanId.setOrgao(orgao);
				}
				
				if ( (datIniRel==null || datFimRel == null) || (datIniRel.equals("")||datFimRel.equals("")) ) { 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
				}
				ARDigitBeanId.setDatInicial(datIniRel);
				ARDigitBeanId.setDatFinal(datFimRel);				
				
				if( ARDigitBeanId.consultaARDigitAcompanhamento(vErro)) {
					String tituloConsulta = "ACOMPANHAMENTO DE DIGITALIZA��O DE AR: "+datIniRel+" a "+datFimRel+"<br>  ORG�O: "
						+ orgao.getSigOrgao();
					req.setAttribute("tituloConsulta", tituloConsulta);
					nextRetorno = "/REG/ARDigitAcompanhamentoImp.jsp";
				} else {
					ARDigitBeanId.setDatInicial(datIniRel);
					ARDigitBeanId.setDatFinal(datFimRel);
					nextRetorno = "/REG/ARDigitAcompanhamento.jsp";
				}
				
				req.setAttribute("ARDigitBeanId", ARDigitBeanId);					
			}
		} catch (Exception e) {
			throw new sys.CommandException("ARDigitAcompanhamentoCmd: " + e.getMessage());	
		}		
		return nextRetorno;
	}
}
