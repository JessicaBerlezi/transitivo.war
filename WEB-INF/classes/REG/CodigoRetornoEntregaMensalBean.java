package REG;

import sys.BeanException;
import REC.CodigoRetornoBean;

public class CodigoRetornoEntregaMensalBean extends sys.HtmlPopupBean {
	
	private REC.CodigoRetornoBean codigoRetorno;
	private String dscResumida;	
	private String qtd;
	private String perc;
	
	public CodigoRetornoEntregaMensalBean() throws sys.BeanException {
		super();
		codigoRetorno   = new REC.CodigoRetornoBean();
		qtd  = "0";
		perc = "0";
	}

	public REC.CodigoRetornoBean getCodigoRetorno() {
		return codigoRetorno;
	}

	public void setCodigoRetorno(REC.CodigoRetornoBean codigoRetorno) throws BeanException {
		if (codigoRetorno==null) codigoRetorno = new REC.CodigoRetornoBean();
		this.codigoRetorno = codigoRetorno;
	}

	public String getDscResumida() {
		return dscResumida;
	}

	public void setDscResumida(String dscResumida) {
		if (dscResumida==null) dscResumida="";
		this.dscResumida = dscResumida;
	}

	public String getPerc() {
		return perc;
	}

	public void setPerc(String perc) {
		this.perc = perc;
	}

	public String getQtd() {
		return qtd;
	}

	public void setQtd(String qtd) {
		this.qtd = qtd;
	}

	
}