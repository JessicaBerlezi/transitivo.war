 package REG;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
 * <b>Title:</b>        	SMIT - Bean de Arquivo Recebido via Upload<br>
 * <b>Description:</b>  	Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
 * @author  				Luiz Medronho
 * @version 				1.0
 */

/*
 * Importante !
 * Para retirar a trava do arquivo TVEP � s� remover o codigo que tiver a express�o
 * "Bacalhau do Bahia" nesta classe e na classe REG.Dao
 */

public class ArquivoRecebidoBean extends sys.HtmlPopupBean  
{
	private String arqFuncao;
	private String codArquivo;
	private String tipHeader;
	private String header;
	private String datRecebimento;
	private String datProcessamentoDetran;
	private String nomArquivo;
	private String codIdentArquivo;
	private String tipArqRetorno;
	private String sufixoArquivo;
	private String numTransBroker;
	private String codIdentRetornoGrv;
	private String codIdentRetornoRej;
	private String sufixoRetornoRej;
	private String sufixoRetornoGrv;
	private int tamNumControle;
	private boolean indProcessamento;
	private boolean pulaVerif;
	private String numControleArq;
	private String numSubPrefeitura;
	private String responsavelArq;
	private String numAnoExercicio;
	private String proximoArquivo;
	private String proximoControle;
	private String numProtocolo;
	private String indEmissaoNotifDetran;
	private String identMovimento;
	private String codOrgao;
	private String nomUsername;
	private String codOrgaoAtuacao;
	private String sigOrgaoAtuacao;
	private String sigOrgaoLotacao;
	private String codOrgaoLotacao;
	private String datDownload;
	private String codStatus;
	private String qtdReg;
	private int    qtdRegCalculada;
	private String qtdLinhaErro;
	private String qtdLinhaPendente;
	private String qtdLinhaCancelada;
	private String datDownloadGravado;
	private String datDownloadErro;
	private List linhaArquivoRec;
	private Vector erroArquivo;
	private Connection conn;
	private String indPrioridade;
	private int ultLinha;
	private int qtdEnviada;
	private int qtdFoto;
	private String indicadorEnvioDataPostagem;
	
	private String dscPortaria59;

	public  ArquivoRecebidoBean()  throws sys.BeanException {

		super();
		setTabela("TSMI_ARQUIVO_RECEBIDO");
		setPopupWidth(35);

		arqFuncao	           = "MR01";
		codArquivo	           = "0";
		tipHeader              = "9";
		header                 = "9";
		datRecebimento         = sys.Util.formatedToday().substring(0,10);
		datProcessamentoDetran = "";

		nomArquivo             = "";
		codIdentArquivo        = "MR01";
		tipArqRetorno          = "1";
		sufixoArquivo          = "";
		numTransBroker         = "400";
		codIdentRetornoGrv     = "";
		codIdentRetornoRej     = "";
		sufixoRetornoRej       = "";
		sufixoRetornoGrv       = "";
		tamNumControle         = 5;
		indProcessamento       = true;
		pulaVerif       	   = false;
		numControleArq         = "0";
		numSubPrefeitura       = "000000";
		responsavelArq         = "";
		numAnoExercicio        = "2005";
		proximoArquivo         = "";
		proximoControle        = "";
		numProtocolo           = "00000";
		indEmissaoNotifDetran  = "";
		codOrgao               = "";
		nomUsername            = "";
		codOrgaoAtuacao        = "";
		sigOrgaoAtuacao        = "";
		sigOrgaoLotacao        = "";
		codOrgaoLotacao        = "";

		datDownload            = "";
		codStatus              = "9";
		qtdReg                 = "0";
		qtdRegCalculada        = 0;
		qtdLinhaErro           = "0";
		qtdLinhaPendente	   = "0";
		qtdLinhaCancelada 	   = "0";
		datDownloadGravado	   = "";
		datDownloadErro		   = "";

		indPrioridade          = "";
		identMovimento         = "";

		linhaArquivoRec        = new ArrayList();
		erroArquivo            = new Vector();

		ultLinha               = 0;
		qtdEnviada             = 0;
		qtdFoto                = 0;
		dscPortaria59          = "";
		indicadorEnvioDataPostagem = "";
	}
	//--------------------------------------------------------------------------
	
	public void set_ArqFuncao(String j_sigFuncao)  {
		this.arqFuncao=j_sigFuncao ;
		if (j_sigFuncao==null) this.arqFuncao="" ;
	}

	
	public void setArqFuncao(String j_sigFuncao) {

		if (j_sigFuncao==null) j_sigFuncao="" ;
		this.arqFuncao="MR01" ;

		//Fun��es de Upload
		if ("REG2012".equals(j_sigFuncao))  this.arqFuncao="MX01" ;
		if ("REG2013".equals(j_sigFuncao))  this.arqFuncao="ME01" ;
		if ("REG2014".equals(j_sigFuncao))  this.arqFuncao="RECJARI" ;
		if ("REG2015".equals(j_sigFuncao))  this.arqFuncao="MZ01" ;
		if ("REG2016".equals(j_sigFuncao))  this.arqFuncao="MC01" ;
		if ("REG2017".equals(j_sigFuncao))  this.arqFuncao="CODRET" ;
		if ("REG2018".equals(j_sigFuncao))  this.arqFuncao="TVEP" ;
		if ("REG2019".equals(j_sigFuncao))  this.arqFuncao="EMITEVEX" ;
		//Atualiza��o para o Contrato Perrone 09/05/2012 jefferson.trindade EMITEPER
		if ("REG2110".equals(j_sigFuncao))  this.arqFuncao="EMITEPER" ;
		if ("REG2051".equals(j_sigFuncao))  this.arqFuncao="MF01" ;
		if ("REG2052".equals(j_sigFuncao))  this.arqFuncao="MA01" ;
		if ("REG2053".equals(j_sigFuncao))  this.arqFuncao="SDOL" ;
		if ("REG2054".equals(j_sigFuncao))  this.arqFuncao="PAGOS/VEX";
		if ("REG2057".equals(j_sigFuncao))  this.arqFuncao="JULG/DET";
	
		
		if ("REG2055".equals(j_sigFuncao))  this.arqFuncao="PE01";
		if ("REG2056".equals(j_sigFuncao))  this.arqFuncao="RECEBER/VEX";

		//Fun��es de Download
		if ("REG2022".equals(j_sigFuncao))  this.arqFuncao="MX01" ;
		if ("REG2023".equals(j_sigFuncao))  this.arqFuncao="ME01" ;
		if ("REG2024".equals(j_sigFuncao))  this.arqFuncao="RECJARI" ;
		if ("REG2025".equals(j_sigFuncao))  this.arqFuncao="MZ01" ;
		if ("REG2026".equals(j_sigFuncao))  this.arqFuncao="MC01" ;
		if ("REG2027".equals(j_sigFuncao))  this.arqFuncao="CODRET" ;
		if ("REG2028".equals(j_sigFuncao))  this.arqFuncao="TVEP" ;
		if ("REG2029".equals(j_sigFuncao))  this.arqFuncao="EMITEVEX" ;
		//Atualiza��o para o Contrato Perrone 09/05/2012 jefferson.trindade EMITEPER
		if ("REG2120".equals(j_sigFuncao))  this.arqFuncao="EMITEPER" ;
		if ("REG2061".equals(j_sigFuncao))  this.arqFuncao="MF01" ;
		if ("REG2062".equals(j_sigFuncao))  this.arqFuncao="MA01" ;
		if ("REG2063".equals(j_sigFuncao))  this.arqFuncao="FOTOVEX" ;
		if ("REG2064".equals(j_sigFuncao))  this.arqFuncao="SDOL" ;
		if ("REG2065".equals(j_sigFuncao))  this.arqFuncao="LOG01";
		if ("REG2067".equals(j_sigFuncao))  this.arqFuncao="EMITEPONTO";		
		//Atualiza��o para o Contrato Perrone 19/07/2012 jefferson.trindade EMITEPONTOPER
		if ("REG2068".equals(j_sigFuncao))  this.arqFuncao="EMITEPONTOPER";
		//Atualiza��o para a libera��o de download EMITEPER/EMITEPONTOPER 11/12/2012 rodrigo.fonseca
		if ("REG3010".equals(j_sigFuncao))  this.arqFuncao="EMITEPER";
		if ("REG3020".equals(j_sigFuncao))  this.arqFuncao="EMITEPONTOPER";
		if ("REG3030".equals(j_sigFuncao))  this.arqFuncao="EMITEVEX";

		//Envio de Arquivo
		if ("MX".equals(j_sigFuncao))           this.arqFuncao="MX01" ;
		if ("ME".equals(j_sigFuncao))  			this.arqFuncao="ME01" ;
		if ("RECJARI".equals(j_sigFuncao))  	this.arqFuncao="RECJARI" ;
		if ("MZ".equals(j_sigFuncao))		    this.arqFuncao="MZ01" ;
		if ("MC".equals(j_sigFuncao))  			this.arqFuncao="MC01" ;
		if ("CODRET".equals(j_sigFuncao))  		this.arqFuncao="CODRET" ;
		if ("TVEP".equals(j_sigFuncao))  		this.arqFuncao="TVEP" ;
		if ("EMITEVEX".equals(j_sigFuncao))		this.arqFuncao="EMITEVEX" ;
		//Atualiza��o para o Contrato Perrone 09/05/2012 jefferson.trindade EMITEPER
		if ("EMITEPER".equals(j_sigFuncao))		this.arqFuncao="EMITEPER" ;
		if ("MF".equals(j_sigFuncao))  			this.arqFuncao="MF01" ;
		if ("MA".equals(j_sigFuncao))  			this.arqFuncao="MA01" ;
		if ("FOTOVEX".equals(j_sigFuncao))  	this.arqFuncao="FOTOVEX" ;
		if ("SDOL".equals(j_sigFuncao))  		this.arqFuncao="SDOL" ;
		this.setCodIdentArquivo(this.arqFuncao);
	}
	public String getArqFuncao()  {
		return this.arqFuncao;
	}
	//--------------------------------------------------------------------------
	public void setCodArquivo(String codArquivo)  {
		this.codArquivo=codArquivo ;
		if (codArquivo==null) this.codArquivo="" ;
	}
	public String getCodArquivo()  {
		return this.codArquivo;
	}
	//--------------------------------------------------------------------------
	public void setTipHeader(String tipHeader)  {
		this.tipHeader=tipHeader ;
		if (tipHeader==null) this.tipHeader="9" ;
	}
	public String getTipHeader()  {
		return this.tipHeader;
	}

	//--------------------------------------------------------------------------
	public void setHeader(String Header)  {
		this.header=Header ;
		if (Header==null) this.header="" ;
	}
	public String getHeader()  {
		return this.header;
	}
	//--------------------------------------------------------------------------
	public void setDatRecebimento(String datRecebimento)	{
		if (datRecebimento==null) datRecebimento = sys.Util.formatedToday().substring(0,10) ;
		this.datRecebimento = datRecebimento;
	}
	public String getDatRecebimento()	{
		return this.datRecebimento;
	}
	//--------------------------------------------------------------------------
	public void setDatProcessamentoDetran (String datProcessamentoDetran) {
		if (datProcessamentoDetran==null) datProcessamentoDetran = "";
		this.datProcessamentoDetran = datProcessamentoDetran;
	}
	public String getDatProcessamentoDetran()	{
		return this.datProcessamentoDetran;
	}
	//--------------------------------------------------------------------------
	public void setNomArquivo (String nomArquivo)	{
		this.nomArquivo = nomArquivo;
		if (nomArquivo==null) this.nomArquivo="" ;
	}
	public String getNomArquivo() {
		return this.nomArquivo;

	}
	//--------------------------------------------------------------------------
	public void setCodIdentArquivo (String codIdentArquivo)	{
		this.codIdentArquivo = codIdentArquivo;
		if (codIdentArquivo==null) this.codIdentArquivo="" ;
		this.tipArqRetorno      = "1";
		this.sufixoArquivo      = "";
		this.numTransBroker     = "400";
		this.codIdentRetornoGrv = "MD01";
		this.codIdentRetornoRej = "MD01";   
		this.sufixoRetornoRej   = "REJ";
		this.sufixoRetornoGrv   = "GRV";
		this.tamNumControle = 5;
		this.indProcessamento = true;
		if (this.codIdentArquivo.equals("MX01")) {
			this.tipArqRetorno      = "3";
			this.numTransBroker     = "401";
			this.codIdentRetornoGrv = "MY01";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "RET";
		} else if (this.codIdentArquivo.equals("ME01")) {
			this.tipArqRetorno      = "2";
			this.numTransBroker     = "402";
			this.codIdentRetornoGrv = "MS01";
			this.codIdentRetornoRej = "MS01";
			this.sufixoRetornoRej   = "REJ";
			this.sufixoRetornoGrv   = "GRV";
		} else if (this.codIdentArquivo.equals("LOG01")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "LOG01";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.indProcessamento = false;
			this.indEmissaoNotifDetran = "";
		} else if (this.codIdentArquivo.equals("RECJARI")) {
			this.tipArqRetorno      = "";
			this.numTransBroker     = "403";
			this.codIdentRetornoGrv = "RECCERTOS";
			this.codIdentRetornoRej = "RECERROS";
			this.sufixoRetornoRej   = "TXT";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle = 0;
		} else if (this.codIdentArquivo.equals("MZ01")) {
			this.tipArqRetorno      = "4";
			this.numTransBroker     = "404";
			this.codIdentRetornoGrv = "MW01"; 
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "RET";
		} else if (this.codIdentArquivo.equals("MC01")) {
			this.tipArqRetorno      = "5";
			this.numTransBroker     = "405";
			this.codIdentRetornoGrv = "MK01";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "RET";
		} else if (this.codIdentArquivo.equals("CODRET")) {
			this.tipArqRetorno      = " ";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "406";
			this.codIdentRetornoGrv = "";
			this.codIdentRetornoRej = "RETERROS";
			this.sufixoRetornoRej   = "TXT";
			this.sufixoRetornoGrv   = "";
			this.tamNumControle = 9;
		} else if (this.codIdentArquivo.equals("TVEP")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "UPL";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "TVEP";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "UPL";
			//Bacalhau do Bahia
			//this.tamNumControle = 5;
			this.tamNumControle = 0;
			this.indProcessamento = false;
		} else if (this.codIdentArquivo.equals("EMITEVEX")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "EMITEVEX";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle = 9;
			this.indProcessamento = false;
			this.indEmissaoNotifDetran = "";
		} //Atualiza��o para o Contrato Perrone 09/05/2012 jefferson.trindade EMITEPER
		else if (this.codIdentArquivo.equals("EMITEPER")) {
				this.tipArqRetorno      = "";
				this.sufixoArquivo      = "TXT";
				this.numTransBroker     = "";
				this.codIdentRetornoGrv = "EMITEPER";
				this.codIdentRetornoRej = "";
				this.sufixoRetornoRej   = "";
				this.sufixoRetornoGrv   = "TXT";
				this.tamNumControle = 9;
				this.indProcessamento = false;
				this.indEmissaoNotifDetran = "";
		} else if (this.codIdentArquivo.equals("EMITEPONTO")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "EMITEPONTO";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle = 0;
			this.indProcessamento = false;
			this.indEmissaoNotifDetran = "";
			//Atualiza��o para o Contrato Perrone 19/07/2012 jefferson.trindade EMITEPONTOPER
		} else if (this.codIdentArquivo.equals("EMITEPONTOPER")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "EMITEPONTOPER";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle = 0;
			this.indProcessamento = false;
			this.indEmissaoNotifDetran = "";
			
		} else if (this.codIdentArquivo.equals("MF01")) {
			this.tipArqRetorno      = "8";
			this.numTransBroker     = "407";
			this.codIdentRetornoGrv = "MG01";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "RET";
		} else if (this.codIdentArquivo.equals("MA01")) {
			this.tipArqRetorno      = "6";
			this.numTransBroker     = "409";
			this.codIdentRetornoGrv = "MB01";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "RET";
		} else if (this.codIdentArquivo.equals("FOTOVEX")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "ZIP";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "FOTOVEX";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "ZIP";
			this.tamNumControle = 0;
			this.indProcessamento = false;
		} else if (this.codIdentArquivo.equals("SDOL")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "SDOL";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			//Bacalhau do Bahia
			//this.tamNumControle = 5;
			this.tamNumControle = 0;
			this.indProcessamento = false;
		}else if (this.codIdentArquivo.equals("PAGOS/VEX")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "PAGOS/VEX";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle     = 5;
			this.indProcessamento   = true;

		}else if (this.codIdentArquivo.equals("JULG/DET")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "JULG/DET";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle     = 5;
			this.indProcessamento   = true;
		}else if (this.codIdentArquivo.equals("PE01")) {
			this.tipArqRetorno      = "1";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "400";
			this.codIdentRetornoGrv = "PD01";
			this.codIdentRetornoRej = "PD01";
			this.sufixoRetornoRej   = "TXT";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle     = 5;
			this.indProcessamento   = true;
		}else if (this.codIdentArquivo.equals("RECEBER/VEX")) {
			this.tipArqRetorno      = "";
			this.sufixoArquivo      = "TXT";
			this.numTransBroker     = "";
			this.codIdentRetornoGrv = "RECEBER/VEX";
			this.codIdentRetornoRej = "";
			this.sufixoRetornoRej   = "";
			this.sufixoRetornoGrv   = "TXT";
			this.tamNumControle     = 5;
			this.indProcessamento   = true;
		}
		
	}
	public String getTipArqRetorno() {
		return this.tipArqRetorno;
	}
	public String getSufixoArquivo() {
		return this.sufixoArquivo;
	}
	public void setNumTransBroker(String sNumTransacao) {
		if (sNumTransacao==null) sNumTransacao="";
		this.numTransBroker=sNumTransacao;
	}
	public String getNumTransBroker() {
		return this.numTransBroker;
	}
	public String getCodIdentRetornoGrv() {
		return this.codIdentRetornoGrv;
	}
	public String getCodIdentRetornoRej() {
		return this.codIdentRetornoRej;
	}
	public String getSufixoRetornoRej() {
		return this.sufixoRetornoRej;
	}
	public String getSufixoRetornoGrv() {
		return this.sufixoRetornoGrv;
	}
	public String getCodIdentArquivo() {
		return this.codIdentArquivo;
	}
	public int getTamNumControle() {
		return this.tamNumControle;
	}
	public boolean isIndProcessamento() {
		return this.indProcessamento;
	}
	//--------------------------------------------------------------------------
	public void setPulaVerif (boolean  pulaVerif) {
		this.pulaVerif = pulaVerif;
	}
	public boolean getPulaVerif()	{
		return this.pulaVerif;
	}
	//--------------------------------------------------------------------------
	public void setNumControleArq (String numControleArq) {
		this.numControleArq = numControleArq;
		if (numControleArq==null) this.numControleArq="00000" ;
		if (this.numControleArq.length()<5) {
			this.numControleArq=("00000"+this.numControleArq);
			this.numControleArq=this.numControleArq.substring(this.numControleArq.length()-5,this.numControleArq.length());
		}
	}
	public String getNumControleArq() {
		return this.numControleArq;
	}
	//--------------------------------------------------------------------------
	
	
	public String getNumSubPrefeitura() {
		return this.numSubPrefeitura;
	}

	public void setNumSubPrefeitura (String numSubPrefeitura) {
		if (numSubPrefeitura==null) this.numSubPrefeitura="000000" ;
		if (numSubPrefeitura.equals("      ")) this.numSubPrefeitura="000000" ;
		
		this.numSubPrefeitura = numSubPrefeitura;
	}
	
	//--------------------------------------------------------------------------
	
	public void setResponsavelArq (String responsavelArq) {
		this.responsavelArq = responsavelArq;
		if (responsavelArq==null) this.responsavelArq="" ;
	}
	public String getResponsavelArq() {
		return this.responsavelArq;
	}
	//--------------------------------------------------------------------------
	public void setNumAnoExercicio (String numAnoExercicio)	{
		this.numAnoExercicio = numAnoExercicio;
		if (numAnoExercicio==null) this.numAnoExercicio="2005" ;
		if (this.numAnoExercicio.length()<4) {
			this.numAnoExercicio=("0000"+this.numAnoExercicio);
			this.numAnoExercicio=this.numAnoExercicio.substring(this.numAnoExercicio.length()-4,this.numAnoExercicio.length());
		}
	}
	public String getNumAnoExercicio() {
		return this.numAnoExercicio;
	}
	//--------------------------------------------------------------------------
	public void setProximoArquivo (String proximoArquivo)	{
		this.proximoArquivo = proximoArquivo;
		if (proximoArquivo==null) this.proximoArquivo="" ;
	}
	public String getProximoArquivo() {
		try { if (this.proximoArquivo.equals("")) calcProximoArquivo(null, null); }
		catch (sys.BeanException e) { };

		return this.proximoArquivo;
	}

	public void calcProximoArquivo(REC.ParamOrgBean myParam, String codOrgaoAtuacao) throws sys.BeanException {

		try {
			String proxArq = "";
			String proxControle = "";

			if (this.getArqFuncao().equals("CODRET"))
			{
				proxArq = this.getArqFuncao() + sys.Util.lPad(myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10"), "0", 9)
				+ "." + this.getSufixoArquivo();
			}
			else if (this.getArqFuncao().equals("EMITEVEX"))
			{
				proxArq      = this.getArqFuncao() + "_" + this.getNumAnoExercicio() + "." + this.getSufixoArquivo();
				this.proximoControle = sys.Util.lPad(myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10"), "0", 9);
			}
			// Altera��o para o contrato Perrone em 09/05/2012 jefferson.trindade
			else if (this.getArqFuncao().equals("EMITEPER"))
			{
				proxArq      = this.getArqFuncao() + "_" + this.getNumAnoExercicio() + "." + this.getSufixoArquivo();
				this.proximoControle = sys.Util.lPad(myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10"), "0", 9);
			}
			else if (this.getArqFuncao().equals("TVEP"))
			{
				proxArq = this.getArqFuncao() + myParam.getParamOrgao("PROCESSO_ANO","2005","2") + sys.Util.lPad(codOrgaoAtuacao, "0", 6)
				+ myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10") + "." + this.getSufixoArquivo();
			//			else if (this.getArqFuncao().equals("SDOL"))
			//				proxArq = this.getArqFuncao() + myParam.getParamOrgao("PROCESSO_ANO","2004","2") + sys.Util.lPad(codOrgaoAtuacao, "0", 6)
			//				+ myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10") + "." + this.getSufixoArquivo();
			}
			else if(this.getArqFuncao().equals("PAGOS/VEX"))
			{
				proxArq = "AUTOSPAGOS"+ myParam.getParamOrgao("PROXIMO_PROCESSAMENTO_"+ this.getArqFuncao(),sys.Util.getDataHoje().substring(3,10).replace("-",""),"10");
			}
			else if(this.getArqFuncao().equals("JULG/DET"))
			{
				proxArq = "JULGAMENTO"+ myParam.getParamOrgao("PROXIMO_PROCESSAMENTO_"+ this.getArqFuncao(),sys.Util.getDataHoje().substring(3,10).replace("-",""),"10");
			}
			else if(this.getArqFuncao().equals("RECEBER/VEX"))
			{
				proxArq = "AUTOSRECEBER"+ myParam.getParamOrgao("PROXIMO_PROCESSAMENTO_"+ this.getArqFuncao(),sys.Util.getDataHoje().substring(3,10).replace("-",""),"10");
			}
			else if(this.getArqFuncao().equals("PE01"))
			{
				proxArq = this.getArqFuncao() + myParam.getParamOrgao("PROCESSO_ANO","2005","2") + sys.Util.lPad(codOrgaoAtuacao, "0", 6)
				+ myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10")+ "." + this.getSufixoArquivo();;
			}
			else
			{
				proxArq = this.getArqFuncao() + myParam.getParamOrgao("PROCESSO_ANO","2005","2") + sys.Util.lPad(codOrgaoAtuacao, "0", 6)
				+ myParam.getParamOrgao("PROXIMO_" + this.getArqFuncao(),"00001","10");
			}

			this.setProximoArquivo(proxArq.toUpperCase());
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public void setNumProtocolo (String numProtocolo)	{
		this.numProtocolo = numProtocolo;
		if (numProtocolo==null) this.numProtocolo="00000" ;
	}
	public void setNumProtocolo ()  throws sys.BeanException	{
		try {
			Dao dao = Dao.getInstance();
			dao.buscaProtocolo(this) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return;
	}
	public String getNumProtocolo() {
		return this.numProtocolo;
	}
	//--------------------------------------------------------------------------
	public void setIndEmissaoNotifDetran (String indEmissaoNotifDetran) {
		this.indEmissaoNotifDetran = indEmissaoNotifDetran;
		if (indEmissaoNotifDetran==null) this.indEmissaoNotifDetran="E" ;
	}
	public String getIndEmissaoNotifDetran() {
		return this.indEmissaoNotifDetran;
	}
	//--------------------------------------------------------------------------
	public void setIdentMovimento(String identMovimento) {
		this.identMovimento = identMovimento;
		if (identMovimento==null) this.identMovimento="";
	}
	public String getIdentMovimento() {
		return this.identMovimento;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgao (String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao==null) this.codOrgao="000000" ;
		if (this.codOrgao.length()<6)  {
			this.codOrgao=("000000"+this.codOrgao);
			this.codOrgao=this.codOrgao.substring(this.codOrgao.length()-6,this.codOrgao.length());
		}
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	//--------------------------------------------------------------------------
	public void setNomUsername (String nomUsername) {
		this.nomUsername = nomUsername;
		if (nomUsername==null) this.nomUsername="" ;
	}
	public String getNomUsername() {
		return this.nomUsername;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoAtuacao (String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
		if (codOrgaoAtuacao==null) this.codOrgaoAtuacao="000000" ;
		if (this.codOrgaoAtuacao.length()<6) {
			this.codOrgaoAtuacao=("000000"+this.codOrgaoAtuacao);
			this.codOrgaoAtuacao=this.codOrgaoAtuacao.substring(this.codOrgaoAtuacao.length()-6,this.codOrgaoAtuacao.length());
		}
	}
	public String getCodOrgaoAtuacao() {
		return this.codOrgaoAtuacao;
	}
	//--------------------------------------------------------------------------
	public void setIndPrioridade(String indPrioridade){
		this.indPrioridade = indPrioridade;
		if(indPrioridade == null) indPrioridade = "";
	}
	public String getIndPrioridade(){
		return this.indPrioridade;
	}
	//--------------------------------------------------------------------------
	public void setSigOrgaoAtuacao (String sigOrgaoAtuacao) {
		this.sigOrgaoAtuacao = sigOrgaoAtuacao;
		if (sigOrgaoAtuacao==null) this.sigOrgaoAtuacao="" ;
	}
	public String getSigOrgaoAtuacao() {
		return this.sigOrgaoAtuacao;
	}
	//--------------------------------------------------------------------------
	public void setSigOrgaoLotacao (String sigOrgaoLotacao) {
		this.sigOrgaoLotacao = sigOrgaoLotacao;
		if (sigOrgaoLotacao==null) this.sigOrgaoLotacao="" ;
	}
	public String getSigOrgaoLotacao() {
		return this.sigOrgaoLotacao;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoLotacao (String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if (codOrgaoLotacao==null) this.codOrgaoLotacao="" ;
	}
	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}
	//--------------------------------------------------------------------------
	public void setDatDownload (String datDownload) {
		this.datDownload = datDownload;
		if (datDownload==null) this.datDownload="" ;
	}
	public String getDatDownload() 	{
		return this.datDownload;
	}
	public String getDatDownloadErro() {
		return datDownloadErro;
	}

	public String getDatDownloadGravado() {
		return datDownloadGravado;
	}

	public void setDatDownloadErro(String string) {
		datDownloadErro = string;
	}

	public void setDatDownloadGravado(String string) {
		datDownloadGravado = string;
	}
	//--------------------------------------------------------------------------
	public void setCodStatus(String codStatus) 	{
		this.codStatus = codStatus;
		if (codStatus==null) this.codStatus="9" ;
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	//--------------------------------------------------------------------------
	public void setQtdReg (String qtdReg){
		this.qtdReg = qtdReg;
		if (qtdReg==null) this.qtdReg="0" ;
	}
	public String getQtdReg() 	{
		return this.qtdReg;
	}
	
	public void setQtdRegCalculada (int qtdRegCalculada){
		this.qtdRegCalculada = qtdRegCalculada;
	}
	public int getQtdRegCalculada() 	{
		return this.qtdRegCalculada;
	}

	public String getQtdRegRecebido() 	{
		int retorno = Integer.valueOf(this.qtdReg).intValue() - Integer.valueOf(this.qtdLinhaPendente).intValue() -
		Integer.valueOf(this.qtdLinhaCancelada).intValue();
		return String.valueOf(retorno);
	}
	//--------------------------------------------------------------------------
	public void setQtdLinhaErro (String qtdLinhaErro){
		this.qtdLinhaErro = qtdLinhaErro;
		if (qtdLinhaErro==null) this.qtdLinhaErro="0" ;
	}
	public String getQtdLinhaErro() 	{
		return this.qtdLinhaErro;
	}
	//--------------------------------------------------------------------------
	public void setQtdLinhaPendente (String qtdLinhaPendente){
		this.qtdLinhaPendente = qtdLinhaPendente;
		if (qtdLinhaPendente==null) this.qtdLinhaPendente="0" ;
	}
	public String getQtdLinhaPendente() 	{
		return this.qtdLinhaPendente;
	}
	//--------------------------------------------------------------------------
	public void setQtdLinhaCancelada (String qtdLinhaCancelada){
		this.qtdLinhaCancelada = qtdLinhaCancelada;
		if (qtdLinhaCancelada==null) this.qtdLinhaCancelada="0" ;
	}
	public String getQtdLinhaCancelada() 	{
		return this.qtdLinhaCancelada;
	}
	//--------------------------------------------------------------------------
	
	public String getIndicadorEnvioDataPostagem() {
		return indicadorEnvioDataPostagem;
	}

	public void setIndicadorEnvioDataPostagem(String indicadorEnvioDataPostagem) {
		this.indicadorEnvioDataPostagem = indicadorEnvioDataPostagem;
		if (indicadorEnvioDataPostagem==null) this.indicadorEnvioDataPostagem=" " ;
		
	}
	//--------------------------------------------------------------------------	
	
	public void setLinhaArquivoRec (List linhaArquivoRec) {
		this.linhaArquivoRec = linhaArquivoRec;
		if (linhaArquivoRec==null) this.linhaArquivoRec= new Vector() ;
	}
	
	public List getLinhaArquivoRec() 	{
		return this.linhaArquivoRec;
	}
	//--------------------------------------------------------------------------
	public void setErroArquivo(Vector erroArquivo) {
		this.erroArquivo = erroArquivo;
		if (this.erroArquivo==null) this.erroArquivo = new Vector();
	}
	public Vector getErroArquivo() 	{
		return erroArquivo;
	}
	//--------------------------------------------------------------------------
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	public Connection getConn() 	{
		return conn;
	}

	/**
	 * M�todo para inserir Arquivo na base de dados
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isInsert(REC.ParamOrgBean ParamOrgaoId) {
		boolean retorno = true;
		try {
			if (isOK(false) == false) {
				setCodStatus("9");
				retorno = false;
			}

			Dao dao = Dao.getInstance();
			dao.ArquivoInsert(this,ParamOrgaoId) ;
		}
		catch (Exception e) {
			retorno = false;
			getErroArquivo().addElement("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n") ;
			getErroArquivo().addElement("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
		}
		return retorno;
	}
	public boolean isInsert(REC.ParamOrgBean ParamOrgaoId, Connection conn) {
		boolean retorno = true;
		try {
			if (isOK() == false) {
				setCodStatus("9");
				retorno = false;
			}

			Dao dao = Dao.getInstance();
			dao.ArquivoInsert(this,ParamOrgaoId,conn) ;
		}
		catch (Exception e) {
			retorno = false;
			getErroArquivo().addElement("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n") ;
			getErroArquivo().addElement("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
		}
		return retorno;
	}

	/**
	 * M�todo para move o Arquivo da tabela Temp para a definitiva
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public void moveTemp(REC.ParamOrgBean paramOrgaoId) throws sys.BeanException {
		try {
			Dao.getInstance().ArquivoMoveTemp(this, paramOrgaoId);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	/**
	 * M�todo para move o Arquivo da tabela Temp para a definitiva
	 * Valida primeiro se os dados est�o preenchidos.
	 */
	public boolean isDeleteTemp() {
		boolean retorno = false;
		try {
			Dao dao = Dao.getInstance();
			retorno = dao.ArquivoDeleteTemp(Integer.parseInt(this.codArquivo));
		}
		catch (Exception e) {
			getErroArquivo().addElement("Erro na exclus�o: "+e.getMessage());
		}
		return retorno;
	}

	/**
	 * M�todo para inserir Arquivo na base de dados
	 */
	public boolean isInsertTemp() {
		boolean retorno = false;
		try {
			if (isOK() == false) {
				retorno = false;
			} else {
				Dao dao = Dao.getInstance();
				retorno = dao.ArquivoInsertTemp(this);
			}
		}
		catch (Exception e) {
			getErroArquivo().addElement("Erro na inclus�o: "+e.getMessage());
		}
		return retorno;
	}

	/**
	 * M�todo para carregar as linhas do arquivo
	 */
	public boolean carregaLinhas()  {
		boolean ok = false;
		try {
			Dao dao = Dao.getInstance();
			if (dao.CarregarLinhasArquivo(this))
				ok=true;
		}
		catch (Exception e) {
			this.getErroArquivo().add(e.getMessage());
			ok=false;
		}
		return ok;
	}

	/**
	 * M�todo para carregar as linhas do arquivo temporario
	 */
	public boolean carregaLinhasTemp()  {
		boolean ok = false;
		try {
			Dao dao = Dao.getInstance();
			if (dao.CarregarLinhasArquivoTemp(this))
				ok=true;
		}
		catch (Exception e) {
			this.getErroArquivo().add(e.getMessage());
			ok=false;
		}
		return ok;
	}


	/**
	 * M�todo para inserir Linhas do Arquivo na base de dados
	 */
	public void isLinhaInsertTemp(String linha, int numLinha) {
		try {
			Dao dao = Dao.getInstance();
			dao.LinhaInsertTemp(this,linha,numLinha);
		}
		catch (Exception e) {
			this.getErroArquivo().add("Linha n�o inclu�da: " + e.getMessage());
		}
	}

	/**
	 * M�todo para Alterar Arquivo na base de dados
	 * O �nico campo que pode ser alterado � o status.
	 */
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.ArquivoUpdate(this) ;
		}
		catch (Exception e) {
			vErro.addElement("Arquivo n�o atualizado: " + this.getNomArquivo() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false ;
		}
		this.setMsgErro(vErro) ;
		return bOk;
	}
	
	
	public boolean isUpdateArqDetran() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.ArquivoUpdateArqDetran(this) ;
		}
		catch (Exception e) {
			vErro.addElement("Arquivo n�o atualizado: " + this.getNomArquivo() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false ;
		}
		this.setMsgErro(vErro) ;
		return bOk;
	}

	/**
	 * M�todo para validar o arquivo.
	 */
	public boolean isOK(boolean criticaRegistros)  throws sys.BeanException {
		boolean oK = true;
		if (getArqFuncao().equals(getCodIdentArquivo())==false) {
			this.getErroArquivo().addElement("00000" + "|" + "400#Tipo de Arquivo inv�lido: " + "("+getArqFuncao()+") difere de ("+this.getCodIdentArquivo() + ") .");
			oK = false;
		}
		else {
			try {
				Dao dao = Dao.getInstance();
				if (dao.ArquivoExiste(this)) {
					this.getErroArquivo().addElement("00000" + "|" + "999#Arquivo "+ this.getNomArquivo() +" j� RECEBIDO. "  );
					oK = false;
				}
				else {
					if ("RECJARI".equals(getArqFuncao())) this.criticaRECJARI();
					else if ("CODRET".equals(getArqFuncao())) this.criticaCODRET();
					else if ("TVEP".equals(getArqFuncao())) this.criticaTVEP();
					else if ("EMITEVEX".equals(getArqFuncao())) this.criticaEMITEVEX(criticaRegistros);
					else if ("EMITEPER".equals(getArqFuncao())) this.criticaEMITEVEX(criticaRegistros);
					else if ("SDOL".equals(getArqFuncao())) this.criticaSDOL();
					
					
					else if (  ("PAGOS/VEX".equals(getArqFuncao())) || ("RECEBER/VEX".equals(getArqFuncao())) ||  ("JULG/DET".equals(getArqFuncao())) )
							 this.criticaPAGOSVEX();
					
					
					else if ("PE01".equals(getArqFuncao())) this.criticaPE01();
					else this.critica(dao.ArquivoExisteP59(this));
					
					if (this.getErroArquivo().size() > 0) oK = false;
				}
			}
			catch (Exception e) {
				throw new sys.BeanException(e.getMessage());
			}
		}
		return oK;
	}
	public boolean isOK()  throws sys.BeanException {
		return isOK(true);
	}
	
	/**
	 * M�todo para Criticar Arquivo
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaPE01() {
		Vector vetCrit = new Vector();
		String strLinha = "00000";

		// Critica do Nome do Arquivo
		if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
			vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo()  );
		}
		
		// Critica do Ano do Exerc�cio
		if ((this.getNumAnoExercicio().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "004#Exerc�cio do Movimento n�o preenchido no Header: '" + this.getNumAnoExercicio() + "'");
		}
		else{
			if (sys.Util.isNumber(this.getNumAnoExercicio()) == false) {
				vetCrit.addElement(strLinha + "|" + "005#Exerc�cio do Movimento Inv�lido: '" + this.getNumAnoExercicio() + "'");
			}
		}
		
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		}
		if (sys.Util.isNumber(this.getQtdReg()) == false) {
			vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		}
		else {
			if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size()) {
				vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
			}
		}
		setErroArquivo(vetCrit);
	}


	/**
	 * M�todo para Criticar Arquivo
	 * Gera vetor com mensagens de erro.
	 */
	private void critica(boolean bExisteP59) {
		Vector vetCrit        = new Vector();
		boolean codOrgaoOK = false;
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("9")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		// Critica do Nome do Arquivo
		if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
			vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo()  );
		}
		String na = this.getCodIdentArquivo()+this.getNumAnoExercicio()+this.getCodOrgao()+this.getNumControleArq();
		if ((this.getNomArquivo().toUpperCase().equals(na.toUpperCase())) == false) {
			vetCrit.addElement(strLinha + "|" + "003#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o Header " + na  );
		}
		// Critica do Ano do Exerc�cio
		if ((this.getNumAnoExercicio().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "004#Exerc�cio do Movimento n�o preenchido no Header: '" + this.getNumAnoExercicio() + "'");
		}
		else{
			if (sys.Util.isNumber(this.getNumAnoExercicio()) == false) {
				vetCrit.addElement(strLinha + "|" + "005#Exerc�cio do Movimento Inv�lido: '" + this.getNumAnoExercicio() + "'");
			}
		}
		//Critica o �rg�o Autuador
		if ((this.getCodOrgao().trim().equals(null) == true))
		{
			vetCrit.addElement(strLinha + "|" + "006#�rg�o Autuador n�o preenchido: '" + this.getCodOrgao() + "'");
		}
		if (sys.Util.isNumber(this.getCodOrgao()) == false) {
			vetCrit.addElement(strLinha + "|" + "007#�rg�o Autuador Inv�lido: '" + this.getCodOrgao() + "'");
		}
		else codOrgaoOK = true;
		if ( (codOrgaoOK == true) &&
				(Integer.parseInt(this.getCodOrgao().trim()) != Integer.parseInt(this.getCodOrgaoAtuacao().trim())) ) 	{
			vetCrit.addElement(strLinha + "|" + "008#Usu�rio sem permiss�o para este �rg�o Autuador: '" + this.getCodOrgao() + "'");
		}
		//Critica o N�mero de Controle
		if ((this.getNumControleArq().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "009#N�mero de Controle do Arquivo n�o preenchido: '" + this.getNumControleArq() + "'");
		}
		if (sys.Util.isNumber(this.getNumControleArq()) == false) {
			vetCrit.addElement(strLinha + "|" + "010#N�mero de Controle do Arquivo Inv�lido: '" + this.getNumControleArq() + "'");
		}
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		}
		if (sys.Util.isNumber(this.getQtdReg()) == false) {
			vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		}
		else {
			if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size()) {
				vetCrit.addElement(strLinha + "|" + "013#Qtdeuantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
			}
		}
		// MX01 nao tem Envio pelo Detran
		if ("MX01,MZ01,MC01".indexOf(getArqFuncao())<0) {
			//Critica o Indicador de Emiss�o de Notifica��o pelo Detran
			if ((this.getIndEmissaoNotifDetran().trim().equals(null) == true)) {
				vetCrit.addElement(strLinha + "|" + "015#Indicador de Emiss�o de Notifica��o pelo Detran n�o preenchido: '" + this.getIndEmissaoNotifDetran() + "'");
			}
			if ("EN".indexOf(this.getIndEmissaoNotifDetran())<0) {
				vetCrit.addElement(strLinha + "|" + "016#Indicador de Emiss�o de Notifica��o pelo Detran Inv�lido: '" + this.getIndEmissaoNotifDetran() + "'");
			}
		}
		//Valida o layout do arquivo de Niter�i
		if ( (this.codOrgao.equals("258650")) && (this.codIdentArquivo.equals("MR")) ) 
		{
			for (int i = 0; i < this.getLinhaArquivoRec().size(); i++) {
				LinhaArquivoRec linha = (LinhaArquivoRec) this.getLinhaArquivoRec().get(i);
				if (linha.getDscLinhaArqRec().substring(396, 405).trim().length() < 9) {
					vetCrit.addElement(strLinha + "|" + "017#Identifica��o do Org�o Inv�lida: " + i);
					break;
				}
			}
		}
		
		if ( (this.getDscPortaria59().equals("P59")) && bExisteP59) {
			/*
			vetCrit.addElement(strLinha + "|" + "018#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
			*/
		}
		else
		{
			if (this.getDscPortaria59().trim().length()>0) {
				if (!this.getDscPortaria59().equals("P59"))  { 
					vetCrit.addElement(strLinha + "|" + "018#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
				}
			}
		}
		
		setErroArquivo(vetCrit);
	}

	/**
	 * M�todo para Criticar Arquivo RECJARI
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaRECJARI() {
		Vector vetCrit = new Vector();
		boolean codOrgaoOK = false;
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("01")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		//Critica o �rg�o Autuador
		if ((this.getCodOrgao().trim().equals(null) == true))
		{
			vetCrit.addElement(strLinha + "|" + "006#�rg�o Autuador n�o preenchido: '" + this.getCodOrgao() + "'");
		}
		if (sys.Util.isNumber(this.getCodOrgao()) == false) {
			vetCrit.addElement(strLinha + "|" + "007#�rg�o Autuador Inv�lido: '" + this.getCodOrgao() + "'");
		}
		else codOrgaoOK = true;
		if ( (codOrgaoOK == true) &&
				(Integer.parseInt(this.getCodOrgao().trim()) != Integer.parseInt(this.getCodOrgaoAtuacao().trim())) ) 	{
			vetCrit.addElement(strLinha + "|" + "008#Usu�rio sem permiss�o para este �rg�o Autuador: '" + this.getCodOrgao() + "'");
		}
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		}
		if (sys.Util.isNumber(this.getQtdReg()) == false) {
			vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		}
		else {
			if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size()) {
				vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
			}
		}
		if ((this.getDscPortaria59().equals("P59"))==false) {
			vetCrit.addElement(strLinha + "|" + "014#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		else
		{
			if (this.getDscPortaria59().trim().length()>0) {
				if (!this.getDscPortaria59().equals("P59"))  { 
					vetCrit.addElement(strLinha + "|" + "014#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
				}
			}
		}
		
		setErroArquivo(vetCrit);
	}

	/**
	 * M�todo para Criticar Arquivo CODRET
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaCODRET() {
		Vector vetCrit = new Vector();
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("3")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		// Critica do Nome do Arquivo
		if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
			vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo()  );
		}
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		}
		if (sys.Util.isNumber(this.getQtdReg()) == false) {
			vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		}
		else {
			if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size())
				vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
		}
		setErroArquivo(vetCrit);
	}

	/**
	 * M�todo para Criticar Arquivo SDOL
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaSDOL() {
		Vector vetCrit = new Vector();
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("SDOL3384")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		//Bacalhau do Bahia
		/*// Critica do Nome do Arquivo
		 if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
		 vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo()  );
		 }
		 //Critica a Quantidade de Registros
		  if ((this.getQtdReg().trim().equals(null) == true)) {
		  vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		  }
		  if (sys.Util.isNumber(this.getQtdReg()) == false) {
		  vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		  }
		  else {
		  if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size())
		  vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
		  }*/
		setErroArquivo(vetCrit);
	}

	/**
	 * M�todo para Criticar Arquivo TVEP
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaTVEP() {
		Vector vetCrit = new Vector();
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("TVEP8037")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		//Bacalhau do Bahia
		/*// Critica do Nome do Arquivo
		 if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
		 vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo()  );
		 }
		 //Critica a Quantidade de Registros
		  if ((this.getQtdReg().trim().equals(null) == true)) {
		  vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		  }
		  if (sys.Util.isNumber(this.getQtdReg()) == false) {
		  vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		  }
		  else {
		  if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size())
		  vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
		  }*/
		setErroArquivo(vetCrit);
	}

	/**
	 * M�todo para Criticar Arquivo EMITEVEX
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaEMITEVEX(boolean criticaRegistros) {
		Vector vetCrit = new Vector();
		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("20")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		// Critica do Nome do Arquivo
		/*
		if ((this.getNomArquivo().toUpperCase().equals(this.getProximoArquivo().toUpperCase())) == false) {
			vetCrit.addElement(strLinha + "|" + "002#Nome de Arquivo " + this.getNomArquivo() + " incompat�vel com o esperado " + this.getProximoArquivo());
		}
		*/
		//Critica a Quantidade de Registros
		if ((this.getQtdReg().trim().equals(null) == true)) {
			vetCrit.addElement(strLinha + "|" + "011#Quantidade de Registros do Arquivo n�o preenchida: '" + this.getQtdReg() + "'");
		}
		if (sys.Util.isNumber(this.getQtdReg()) == false) {
			vetCrit.addElement(strLinha + "|" + "012#Quantidade de Registros do Arquivo Inv�lida: '" + this.getQtdReg() + "'");
		}
		else if (criticaRegistros) {
			
			if (this.getLinhaArquivoRec().size()==0)
			{
				if(Integer.parseInt(this.getQtdReg()) != (this.getQtdRegCalculada()-1))
					vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
			}
			else
			{
				if(Integer.parseInt(this.getQtdReg()) != this.getLinhaArquivoRec().size())
					vetCrit.addElement(strLinha + "|" + "013#Quantidades de Registros incorreta: recebidos no Arquivo - " + String.valueOf(this.getLinhaArquivoRec().size())  );
			}
		}
		
		if ((this.getDscPortaria59().equals("P59"))==false) {
			vetCrit.addElement(strLinha + "|" + "014#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		else
		{
			if (this.getDscPortaria59().trim().length()>0) {
				if (!this.getDscPortaria59().equals("P59"))  { 
					vetCrit.addElement(strLinha + "|" + "014#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
				}
			}
		}
		
		/*Criticar Numero de Controle*/
		/*
		if (Integer.parseInt(this.getNumControleArq()) != Integer.parseInt((this.getProximoControle())) ) 
				vetCrit.addElement(strLinha + "|" + "015#N�mero de controle incorreto -  Pr�ximo n� de controle: "+this.getProximoControle());
		*/
		
		
		setErroArquivo(vetCrit);
	}
	
	/**
	 * M�todo para Criticar Arquivo RECJARI
	 * Gera vetor com mensagens de erro.
	 */
	private void criticaPAGOSVEX() {
		Vector vetCrit = new Vector();

		String strLinha = "00000";
		//Critica do Header
		if ((this.getTipHeader().equals("1")) == false) {
			vetCrit.addElement(strLinha + "|" + "001#Tipo de Header de Arquivo inv�lido: '" + this.getTipHeader() + "'" );
		}
		//Critica o �rg�o Autuador
		if ((this.getCodOrgao().trim().equals(null) == true))
		{
			vetCrit.addElement(strLinha + "|" + "006#�rg�o Autuador n�o preenchido: '" + this.getCodOrgao() + "'");
		}
		if (sys.Util.isNumber(this.getCodOrgao()) == false) {
			vetCrit.addElement(strLinha + "|" + "007#�rg�o Autuador Inv�lido: '" + this.getCodOrgao() + "'");
		}
	    setErroArquivo(vetCrit);
	}

	public String getHeaderGrv() {

		String headerGrv = "";

		if (this.getCodIdentArquivo().equals("RECJARI")) {
			String tipoReg  = "01";
			String indArq   = this.getCodIdentRetornoGrv();
			String dataProc = sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10));
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String dataMov  = this.getNumAnoExercicio();
			String nomResp  = sys.Util.rPad(this.getResponsavelArq(), " ", 8);
			String vago     = sys.Util.lPad("", " ", 141);
			headerGrv       = tipoReg+indArq+dataProc+orgAutu+dataMov+nomResp+vago;

		} else if (this.getCodIdentArquivo().equals("TVEP")) {
			String tipoReg  = "TVEP8037";
			String txtArq   = "CADASTRO DETRAN/RJ";
			String dataMov  = this.getNumAnoExercicio();
			String dataProc = this.getDatProcessamentoDetran().substring(0,2) + this.getDatProcessamentoDetran().
			substring(3,5) + this.getDatProcessamentoDetran().substring(6,10);
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 5);
			String qtdReg   = sys.Util.lPad(this.getQtdReg(), "0", 20);
			String indMov   = this.getIdentMovimento();
			String vago     = sys.Util.lPad("", " ", 55);
			headerGrv       = tipoReg+txtArq+dataProc+dataMov+numCtrl+qtdReg+indMov+vago;

		} else if (this.getCodIdentArquivo().equals("EMITEVEX")) {
			String tipoReg  = "20";
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 9);
			String dataProc = this.getNumAnoExercicio();
			String qtdReg   = sys.Util.lPad(this.getQtdReg(), "0", 6);
			String indMov   = this.getIdentMovimento();
			String txtArq   = "EMISSAO VEX";
			String dscP59   = "P59";
			headerGrv       = tipoReg+numCtrl+dataProc+qtdReg+indMov+txtArq+dscP59;
		} 
		//Altera��o realizada para o contrato Perrone jefferson.trindade 
		else if (this.getCodIdentArquivo().equals("EMITEPER")) {
			String tipoReg  = "20";
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 9);
			String dataProc = this.getNumAnoExercicio();
			String qtdReg   = sys.Util.lPad(this.getQtdReg(), "0", 6);
			String indMov   = this.getIdentMovimento();
			String txtArq   = "EMISSAO PERRONE";
			String dscP59   = "P59";
			headerGrv       = tipoReg+numCtrl+dataProc+qtdReg+indMov+txtArq+dscP59;

		} else if (this.getCodIdentArquivo().equals("SDOL")) {
			String tipoReg  = "SDOL3384";
			String txtArq   = "ACESSO DETRAN";
			String dataMov  = this.getNumAnoExercicio();
			String dataProc = this.getDatProcessamentoDetran().substring(0,2) + this.getDatProcessamentoDetran().
			substring(3,5) + this.getDatProcessamentoDetran().substring(6,10);
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 5);
			String qtdReg   = sys.Util.lPad(this.getQtdReg(), "0", 20);
			String indMov   = this.getIdentMovimento();
			String vago     = sys.Util.lPad("", " ", 55);
			headerGrv       = tipoReg+txtArq+dataProc+dataMov+numCtrl+qtdReg+indMov+vago;
		} else if (this.getCodIdentArquivo().equals("LOG01")) {
			String tipoReg  = "9";
			String indArq   = this.getCodIdentRetornoGrv();
			String excMov   = this.getNumAnoExercicio();
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 5);
			String qtdReg   = sys.Util.lPad(this.getQtdReg(), "0", 5);
			String vago     = sys.Util.lPad("", " ", 1390);
			if(this.getNomArquivo().indexOf("ME")>=0)
			{
				indArq          = "ME01";
				vago     = sys.Util.lPad("", " ", 398);				
			    headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+qtdReg+" "+vago;				
			}
			if(this.getNomArquivo().indexOf("MR")>=0)
			{
				indArq          = "MR01";
				vago     = sys.Util.lPad("", " ", 398);				
			    headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+qtdReg+" "+vago;				
			}
			if(this.getNomArquivo().indexOf("MX")>=0)
			{
				indArq          = "MX01";
				vago            = sys.Util.lPad("", " ", 399);				
			    headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+qtdReg+vago;
			}
			if(this.getNomArquivo().indexOf("MZ")>=0)
			{
				indArq          = "MZ01";
				vago            = sys.Util.lPad("", " ", 399);				
			    headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+qtdReg+vago;
			}
			if(this.getNomArquivo().indexOf("MF")>=0)
			{
				indArq          = "MF01";
				vago            = sys.Util.lPad("", " ", 399);				
			    headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+qtdReg+vago;
			}
			if(this.getNomArquivo().indexOf("RECJARI")>=0)
			{
				tipoReg         = "01";	
				indArq          = "RECJARI";
				String datProc  = sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10));
				String nomResp  = sys.Util.rPad(this.getResponsavelArq(), " ", 8);				
				vago            = sys.Util.lPad("", " ", 145);				
			    headerGrv       = tipoReg+indArq+datProc+qtdReg+orgAutu+nomResp+vago;
			}
		} else {
			String tipoReg  = "9";
			String indArq   = this.getCodIdentRetornoGrv();
			String excMov   = this.getNumAnoExercicio();
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 5);
			String dataProc = this.getDatProcessamentoDetran().substring(0,2) + this.getDatProcessamentoDetran().
			substring(3,5) + this.getDatProcessamentoDetran().substring(6,10);
			String vago     = sys.Util.lPad("", " ", 45);
			headerGrv       = tipoReg+indArq+excMov+orgAutu+numCtrl+dataProc+vago;
		}

		return headerGrv;
	}

	public String getHeaderRej() {

		String headerRej = "";

		if (this.getCodIdentArquivo().equals("RECJARI")) {
			String tipoReg  = "01";
			String indArq   = sys.Util.rPad(this.getCodIdentRetornoRej(), " ", 9);
			String dataProc = sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10));
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String dataMov  = this.getNumAnoExercicio();
			String nomResp  = sys.Util.rPad(this.getResponsavelArq(), " ", 8);
			String vago     = sys.Util.lPad("", " ", 141);
			headerRej       = tipoReg+indArq+dataProc+orgAutu+dataMov+nomResp+vago;

		} else if (this.getCodIdentArquivo().equals("CODRET")) {
			String tipoReg  = "3";
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 9);
			String qtdReg	= sys.Util.lPad(this.getQtdReg(), "0", 6);
			String qtdErro	= sys.Util.lPad(this.getQtdLinhaErro(), "0", 6);
			String dataMov  = this.getNumAnoExercicio();
			String dataProc = this.getDatProcessamentoDetran().substring(0,2) + this.getDatProcessamentoDetran().
			substring(3,5) + this.getDatProcessamentoDetran().substring(6,10);
			headerRej       = tipoReg+numCtrl+qtdReg+qtdErro+dataMov+dataProc;
		} else if (this.getCodIdentArquivo().equals("LOG01")) {
			String tipoReg  = "01";
			String indArq   = sys.Util.rPad(this.getCodIdentRetornoRej(), " ", 9);
			String dataProc = sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10));
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String dataMov  = this.getNumAnoExercicio();
			String nomResp  = sys.Util.rPad(this.getResponsavelArq(), " ", 8);
			String vago     = sys.Util.lPad("", " ", 141);
			headerRej       = tipoReg+indArq+dataProc+orgAutu+dataMov+nomResp+vago;
			
		} else {
			String tipoReg  = "9";
			String indArq   = this.getCodIdentRetornoRej();
			String excMov   = this.getNumAnoExercicio();
			String orgAutu  = sys.Util.lPad(this.getCodOrgao(), "0", 6);
			String numCtrl  = sys.Util.lPad(this.getNumControleArq(), "0", 5);
			String dataProc = this.getDatProcessamentoDetran().substring(0,2) + this.getDatProcessamentoDetran().
			substring(3,5) + this.getDatProcessamentoDetran().substring(6,10);
			String vago     = sys.Util.lPad("", " ", 45);
			headerRej       = tipoReg+indArq+excMov+orgAutu+numCtrl+dataProc+vago;
		}

		return headerRej;
	}

	public String getNomArquivoDir() {

		String nomArquivoDir = this.nomArquivo;
		int pos = nomArquivoDir.lastIndexOf(".")+1;
		String nomArquivoDirBkp=nomArquivoDir.substring(pos,nomArquivoDir.length());
		if("bkp".equals(nomArquivoDirBkp))
			nomArquivoDir = "DE01/" + nomArquivoDir;
		else
			nomArquivoDir = this.getCodIdentArquivo() + "/" + nomArquivoDir;
		return nomArquivoDir;
	}

	public String getNomArquivoGrv() {

		String nomArquivoGrv = "";

		if (this.getCodIdentArquivo().equals("RECJARI")) {
			if (this.getNomArquivo().indexOf(".") >= 0)
				nomArquivoGrv = this.getCodIdentRetornoGrv() + this.getNomArquivo().substring(7, this.getNomArquivo()
						.lastIndexOf(".")) + "." + this.getSufixoRetornoGrv();
			else
				nomArquivoGrv = this.getCodIdentRetornoGrv() + this.getNomArquivo().substring(7) + "." +
				this.getSufixoRetornoGrv();

		} 
		else if (this.getCodIdentArquivo().equals("EMITEVEX"))
			nomArquivoGrv = this.getNomArquivo();
		
		else if (this.getCodIdentArquivo().equals("EMITEPER"))
			nomArquivoGrv = this.getNomArquivo();

		else if (this.getCodIdentArquivo().equals("EMITEPONTO"))
			nomArquivoGrv = this.getNomArquivo();
		
		else if (this.getCodIdentArquivo().equals("EMITEPONTOPER"))
			nomArquivoGrv = this.getNomArquivo();
		
		else if (this.getCodIdentArquivo().equals("FOTOVEX"))
			nomArquivoGrv = this.getNomArquivo();

		else if (this.getCodIdentArquivo().equals("TVEP"))
			nomArquivoGrv = this.getCodIdentRetornoGrv() + this.getNomArquivo().substring(4, this.getNomArquivo()
					.lastIndexOf(".")) + "." + this.getSufixoRetornoGrv();

		else if (this.getCodIdentArquivo().equals("SDOL"))
			nomArquivoGrv = this.getNomArquivo();

		else if (this.getCodIdentArquivo().equals("LOG01"))
			nomArquivoGrv = this.getNomArquivo();
		else
			nomArquivoGrv = this.getCodIdentRetornoGrv() + this.getNomArquivo().substring(4) + "." + this.
			getSufixoRetornoGrv();

		nomArquivoGrv = this.getCodIdentArquivo() + "/" + nomArquivoGrv;

		return nomArquivoGrv;
	}

	public String getNomArquivoRej() {

		String nomArquivoRej = "";

		if (this.getCodIdentArquivo().equals("RECJARI")) {
			if (this.getNomArquivo().indexOf(".") >= 0)
				nomArquivoRej = this.getCodIdentRetornoRej() + this.getNomArquivo().substring(7, this.getNomArquivo().
						lastIndexOf(".")) + "." + this.getSufixoRetornoRej();
			else
				nomArquivoRej = this.getCodIdentRetornoRej() + this.getNomArquivo().substring(7) + "." +
				this.getSufixoRetornoRej();

		} else if (this.getCodIdentArquivo().equals("CODRET"))
			nomArquivoRej = this.getCodIdentRetornoRej() + this.getNomArquivo().substring(6,this.getNomArquivo().
					lastIndexOf(".")) + "." + this.getSufixoRetornoRej();
		else
			nomArquivoRej = this.getCodIdentRetornoRej() + this.getNomArquivo().substring(4)
			+ "." + this.getSufixoRetornoRej();

		nomArquivoRej = this.getCodIdentArquivo() + "/" + nomArquivoRej;

		return nomArquivoRej;
	}

	/**
	 * M�todo para caregar campos do Header do Arquivo
	 */
	public void carregarHeader(String linhaHeader, ACSS.UsuarioBean usuario) {

		if ("RECJARI".equals(this.getArqFuncao())) {
			this.setTipHeader(linhaHeader.substring(0, 2));
			this.setCodIdentArquivo(linhaHeader.substring(2, 9));
			this.setNumAnoExercicio(linhaHeader.substring(9, 17));
			this.setQtdReg(linhaHeader.substring(17, 23));
			this.setCodOrgao(linhaHeader.substring(23, 29));
			// Responsavel pela geracao do arquivo
			if (linhaHeader.length()<37) this.setResponsavelArq(linhaHeader.substring(29));
			else this.setResponsavelArq(linhaHeader.substring(29, 37));
			
			if (linhaHeader.length()>37)
  			  this.setDscPortaria59(linhaHeader.substring(37, 40));
			
		} else if ("CODRET".equals(this.getArqFuncao())) {
			this.setTipHeader(linhaHeader.substring(0, 1));
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setNumAnoExercicio(linhaHeader.substring(60, 68));
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setNumControleArq(linhaHeader.substring(5, 14));
			this.setQtdReg(linhaHeader.substring(14, 20));

		} else if ("TVEP".equals(this.getArqFuncao())) {
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setTipHeader(linhaHeader.substring(0, 8));
			this.setDatProcessamentoDetran(linhaHeader.substring(26,28)+"/"+linhaHeader.substring(28,30)
					+"/"+linhaHeader.substring(30,34));
			//Bacalhau do Bahia
			//this.setNumAnoExercicio(linhaHeader.substring(34, 38));
			//this.setNumControleArq(linhaHeader.substring(38, 43));
			//this.setQtdReg(linhaHeader.substring(43, 63));
			//this.setIdentMovimento(linhaHeader.substring(63,64));
			this.setNumAnoExercicio(linhaHeader.substring(30,34));
			this.setIdentMovimento("A");

		} else if ("EMITEVEX".equals(this.getArqFuncao())) {
			this.setTipHeader(linhaHeader.substring(0, 2));
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setNumAnoExercicio(linhaHeader.substring(11, 19));
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setNumControleArq(linhaHeader.substring(2, 11));
			this.setQtdReg(linhaHeader.substring(19, 25));
			this.setIdentMovimento(linhaHeader.substring(25,26));
			if (linhaHeader.length()>37)
	  			  this.setDscPortaria59(linhaHeader.substring(37, 40));
			
			// Altera��o realizada para o contrato Perrone 10/05/2012
		}else if ("EMITEPER".equals(this.getArqFuncao())) {
			this.setTipHeader(linhaHeader.substring(0, 2));
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setNumAnoExercicio(linhaHeader.substring(11, 19));
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setNumControleArq(linhaHeader.substring(2, 11));
			this.setQtdReg(linhaHeader.substring(19, 25));
			this.setIdentMovimento(linhaHeader.substring(25,26));
			if (linhaHeader.length()>37)
	  			  this.setDscPortaria59(linhaHeader.substring(37, 40));

		} else if ("SDOL".equals(this.getArqFuncao())) {
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setTipHeader(linhaHeader.substring(0, 8));
			this.setDatProcessamentoDetran(linhaHeader.substring(26,28)+"/"+linhaHeader.substring(28,30)
					+"/"+linhaHeader.substring(30,34));
			//Bacalhau do Bahia
			//this.setNumAnoExercicio(linhaHeader.substring(34, 38));
			//this.setNumControleArq(linhaHeader.substring(38, 43));
			//this.setQtdReg(linhaHeader.substring(43, 63));
			//this.setIdentMovimento(linhaHeader.substring(63,64));
			this.setNumAnoExercicio(linhaHeader.substring(30,34));
			this.setIdentMovimento("A");

		}
		else if ("PAGOS/VEX".equals(this.getArqFuncao())) {
			String sCodIdentArquivo=linhaHeader.substring(1, 10).trim();
			this.setTipHeader(linhaHeader.substring(0, 1));
			this.setCodIdentArquivo(sCodIdentArquivo);
			this.setNumAnoExercicio(linhaHeader.substring(10, 14));
			
		}
		
		
		else if ("JULG/DET".equals(this.getArqFuncao())) {
			String sCodIdentArquivo=linhaHeader.substring(1, 9).trim();
			this.setTipHeader(linhaHeader.substring(0, 1));
			this.setCodIdentArquivo(sCodIdentArquivo);
			this.setNumAnoExercicio(linhaHeader.substring(12, 16));
		} 
		
		
		
		else if ("PE01".equals(this.getArqFuncao())) {
			this.setCodIdentArquivo(this.getArqFuncao());
			this.setQtdReg(linhaHeader.substring(11,15));
			this.setNumAnoExercicio(linhaHeader.substring(19, 23));
			this.setResponsavelArq(linhaHeader.substring(23, 43).trim());
			this.setCodOrgao(usuario.getCodOrgaoAtuacao());
			this.setNumControleArq(this.getNomArquivo().substring(14,19));
		} 
		else if ("RECEBER/VEX".equals(this.getArqFuncao())) {
			this.setTipHeader(linhaHeader.substring(0, 1));
			this.setCodIdentArquivo(linhaHeader.substring(1, 12));
			this.setNumAnoExercicio(linhaHeader.substring(12,16));
			
		} 
		else {
			this.setTipHeader(linhaHeader.substring(0, 1));
			this.setCodIdentArquivo(linhaHeader.substring(1, 5));
			this.setNumAnoExercicio(linhaHeader.substring(5, 9));
			this.setCodOrgao(linhaHeader.substring(9, 15));
			this.setNumControleArq(linhaHeader.substring(15, 20));
			this.setQtdReg(linhaHeader.substring(20, 25));
			if ("MR01,ME01".indexOf(this.getArqFuncao())>=0) {
				this.setIndEmissaoNotifDetran(linhaHeader.substring(25, 26));
		  		this.setDscPortaria59(sys.Util.rPad(linhaHeader, " ", 29).substring(26, 29));
			}
			if ("MC01,MZ01,MX01".indexOf(this.getArqFuncao())>=0)
		  		this.setDscPortaria59(sys.Util.rPad(linhaHeader, " ", 28).substring(25, 28));
			
			/*
			this.setNumSubPrefeitura(linhaHeader.substring(26, 32));
			*/
			
			
			
			
		}
	}

	public void carregaArqRecebido(String codArq,String codOrg,String sigOrg)  throws sys.BeanException    {
		this.codArquivo = codArq;
		try {
			Dao dao = Dao.getInstance();
			dao.CarregaArqRecebido(this,codOrg,sigOrg) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return;
	}

	public void carregaArquivoDetalhe(LinhaArquivoRec LinhaArquivoRecId, String codArq, String codOrgao, boolean bArqRecebidoBKP)  throws sys.BeanException    {
		try {
			Dao dao = Dao.getInstance();
			dao.CarregaArquivoDetalhe(LinhaArquivoRecId, codArq, codOrgao, bArqRecebidoBKP) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}		
		return;
	}


	public void carregaArqRecebidoTemp(String codArq)  throws sys.BeanException    {
		this.codArquivo = codArq;
		try {
			Dao dao = Dao.getInstance();
			dao.CarregaArqRecebidoTemp(this);
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return;
	}

	/**
	 * M�todo usado para recuperar todos os arquivos com status pendentes que sejam
	 * do org�o lota��o informado atrav�s do codOrgao.
	 */
	public List getArquivoPendenteEnvio(String[] orgaos) throws Exception{
		try{
			Dao dao = Dao.getInstance();
			return dao.getArquivoPendenteEnvio(this, orgaos);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	public List getArquivoPendenteRetorno() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			return dao.getArquivoPendenteRetorno(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * M�todo usado para recuperar todos as linhas presente em um arquivo
	 * que possui o cosStatus passado por par�metro.
	 */
	public void getArquivoLinhaEnvio(String[] orgaos) throws Exception{
		try{
			Dao dao = Dao.getInstance();
			dao.getArquivoLinhaEnvio(this, orgaos);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	public void getArquivoLinhaRetorno() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			dao.getArquivoLinhaRetorno(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * M�todo criado para gerar um registro para um novo arquivo de envio das linhas,
	 * que deixaram de estar presentes por cr�ticas de envio para a VEX.
	 */
	public boolean isAtualizado(Connection conn){
		String auxCodArquivo = null;
		try {
			Dao dao = Dao.getInstance();
			qtdLinhaErro = "0";
			qtdReg = String.valueOf(Integer.parseInt(qtdReg) - linhaArquivoRec.size());
			dao.atualizaAQtdReg(this, conn);
			qtdReg = String.valueOf(linhaArquivoRec.size());
			dao.insertArquivoRec(this, conn);
			for(int i=0;i<linhaArquivoRec.size();i++){
				LinhaArquivoRec linha = (LinhaArquivoRec) linhaArquivoRec.get(i);
				auxCodArquivo = linha.getCodArquivo();
				if (linha.getCodStatus().equals("9"))
					dao.atualizaErroLinha(auxCodArquivo,1,conn);
				linha.setCodArquivo(codArquivo);
				linha.setCodStatus("1");
				linha.setCodRetorno("");
				linha.setConn(conn);
				linha.isUpdate();
				linha.setCodStatus("0");
				linha.atualizaArqEnvio(this,auxCodArquivo);
			}
		}
		catch (Exception e) {
			this.setMsgErro(e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * M�todo de consulta sobre as informa��es de um arquivo recebido presente
	 * na base de dados do SMIT.
	 * A consulta � feita de acordo com o c�digo do arquivo de objeto.
	 */
	public void getArquivoRecebido() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			dao.buscaArquivoRecebido(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	
	/**
	 * M�todo para enviar data de postagem para o M97
	 */
	public void enviaDataPostagemBrokerMulta() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			Iterator ite = this.getLinhaArquivoRec().iterator();
			
			while(ite.hasNext()){
				LinhaArquivoRec linha = (LinhaArquivoRec) (REG.LinhaArquivoRec) ite.next();				
				String tipoNotificacao = linha.getDscLinhaArqRec().substring(571, 572); 
				String codigoEvento = "0";
				if ("1".equals(tipoNotificacao)){
					codigoEvento = "911";
				}else if ("2".equals(tipoNotificacao)){
					codigoEvento = "912";
				}
				String numeroAuto = linha.getDscLinhaArqRec().substring(360, 372).trim();
				String placaVeiculo = linha.getDscLinhaArqRec().substring(125, 132).trim();
				String dataPostagem =  sys.Util.somaDias(5,this.datDownload);
				
				String codigoRetorno = dao.informaDataPostagemBroker(codigoEvento, numeroAuto, placaVeiculo, dataPostagem);


				dao.atualizaCodRetornoPostagemLinhaArquivo(linha, codigoRetorno.trim());
			}
			
			this.indicadorEnvioDataPostagem = "S";
			dao.atualizaIndicadorDataPostagemArquivo(this);
			
		}catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * M�todo de consulta do �rg�o de Atua��o dos Usu�rios do Envio de Arquivo
	 * via WebService
	 */
	public boolean pegaOrgaoAtuacao(String codigo) {
		try {
			Dao dao = Dao.getInstance();
			if (dao.PegarOrgaoAutuacao(codigo,this))
				return true;
			else
				return false;
		}
		catch(Exception e){
			return false;
		}
	}

	/**
	 * M�todo para Criticar a quantidade de Linhas enviadas pelo WebService.
	 */
	public boolean criticaQtdLinhas(int qtdLinhas) {
		boolean retorno = true;
		Vector erros = new Vector();
		if (Integer.parseInt(this.getQtdReg()) != qtdLinhas) {
			erros.addElement("014#Qtde de Regs enviadas Incorreta: enviadas(" + qtdLinhas
					+ ") Total Registro ("+this.getQtdReg()+") arquivo(" + this.getQtdReg() + ")");
			retorno = false;
		}
		this.setErroArquivo(erros);
		return retorno;
	}

	/**
	 * M�todo de consulta dos arquivos na tabela tempor�ria
	 */
	public List buscaArquivosTemp() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			return dao.BuscaArquivosTemp(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}

	public int getUltLinha() {
		return ultLinha;
	}
	public void setUltLinha(int ultLinha) {
		this.ultLinha = ultLinha;
	}

	public int getQtdEnviada() {
		return qtdEnviada;
	}
	public void setQtdEnviada(int qtdEnviada) {
		this.qtdEnviada = qtdEnviada;
	}

	public int getQtdFoto() {
		return qtdFoto;
	}
	public void setQtdFoto(int qtdFoto) {
		this.qtdFoto = qtdFoto;
	}	
	
	public String getNomArquivoBkp() {
		String nomArquivoBkp = "BK"+ this.nomArquivo.substring(2,this.nomArquivo.length());
		nomArquivoBkp = "DE01/" + nomArquivoBkp;
		return nomArquivoBkp;
	}   

	public ws.ArquivoBean[] buscarArquivosRecebidos(String dataInicial, String dataFinal) 
	throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			return dao.buscarArquivosRecebidos(this, dataInicial, dataFinal);
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}
	
	public void listarLinhasComFoto() throws DaoException{
		try{
			Dao dao = Dao.getInstance();
			dao.listarLinhasComFoto(this);
		}
		catch(Exception e){
			throw new DaoException(e.getMessage());
		}
	}

	public String getSetor(ACSS.UsuarioBean myUsr, String nomArq){
	    String mySetor = "" ; 
	    try    {
	       Dao dao = Dao.getInstance();
		   mySetor = dao.getSetor(myUsr,nomArq) ;
		}
		catch(Exception e){	}	
		return mySetor ;
	}
	
	public String buscaProximoArquivo(REC.ParamOrgBean myParam, String codOrgaoAtuacao) throws sys.BeanException {
		String proxArq = "";

		try {
			proxArq = this.getArqFuncao() + myParam.getParamOrgao("PROCESSO_ANO","2007","2") + sys.Util.lPad(codOrgaoAtuacao, "0", 6);

			this.setProximoArquivo(proxArq.toUpperCase());
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return proxArq; 
	}

	
	public Boolean validaNumeracaoRecebida(String codOrgaoAtuacao) throws sys.BeanException {
		Boolean valido = false;

		try {
		       Dao dao = Dao.getInstance();
		       valido = dao.validaNumeracaoRecebida(this,codOrgaoAtuacao) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return valido; 
	}
	
	public boolean isInsertSubPrefeitura(REC.ParamOrgBean ParamOrgaoId) {
		boolean retorno = true;
		try {
			if (isOK(false) == false) {
				setCodStatus("9");
				retorno = false;
			}

			Dao dao = Dao.getInstance();
			dao.ArquivoInsertSubPrefeitura(this,ParamOrgaoId) ;
		}
		catch (Exception e) {
			retorno = false;
			getErroArquivo().addElement("00000|999#Arquivo n�o inclu�do: " + this.getNomArquivo() + " \n") ;
			getErroArquivo().addElement("00000|999#Erro na inclus�o: "+e.getMessage()+" \n");
		}
		return retorno;
	}
	
	public String getDscPortaria59() {
		return dscPortaria59;
	}
	public void setDscPortaria59(String dscPortaria59) {
		if (dscPortaria59==null) dscPortaria59="";
		this.dscPortaria59 = dscPortaria59;
	}
	public String getProximoControle() {
		return proximoControle;
	}
	public void setProximoControle(String proximoControle) {
		if (proximoControle==null) proximoControle="";
		this.proximoControle = proximoControle;
	}
	
	
	
	public String geraStyleEEventoOnClickGrv(String funcao){
		String retorno = "style=\"cursor: hand;\" onClick=\"javascript:registra(document.frmDownload,'" + this.codArquivo + "','" + this.getNomArquivoGrv() + "','GRV','" + funcao +"');\"";
		
		if ("EMITEPER,EMITEVEX".indexOf(this.getCodIdentArquivo()) >= 0){
			if (this.datDownload != ""){
				retorno = "";
			}
		}
		
		return retorno;
	}
	
	public String geraStyleEEventoOnClickRej(String funcao){
		String retorno = "style=\"cursor: hand;\" onClick=\"javascript:registra(document.frmDownload,'" + this.codArquivo + "','" + this.getNomArquivoRej() + "','REJ','" + funcao +"');\"";
		if ("EMITEPER,EMITEVEX".indexOf(this.getCodIdentArquivo()) >= 0){
			if (this.datDownload != ""){
				retorno = "";
			}
		}
		return retorno;
	}

}