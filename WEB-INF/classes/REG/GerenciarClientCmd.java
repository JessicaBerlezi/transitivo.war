package REG;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Para Client - Command<br>
* <b>Description:</b>  Permite o parar o processamento dos Clients, bloqueando-os<br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class GerenciarClientCmd extends sys.Command {
	private static final String jspPadrao = "/REG/GerenciarClient.jsp";
	private String next;

	public GerenciarClientCmd() {
		next = jspPadrao;
	}

	public GerenciarClientCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String  nextRetorno = next;
		GerenciarClientBean GerenciarClientBean = (GerenciarClientBean) req.getAttribute("GerenciarClientBean");
		if (GerenciarClientBean == null) GerenciarClientBean = new GerenciarClientBean();
		
		
		//Obter e validar os paramentros recebidos
		String acao = req.getParameter("acao");
		if (acao == null) acao = ""; 
		
		//Recebe parametros 
		String valClient = req.getParameter("valClient");
		if (valClient == null) valClient = "";
		GerenciarClientBean.setValClient(valClient);

		try {
			if(acao.equals("busca")) 
			{
				GerenciarClientBean.getSituacaoExecucao();
				nextRetorno = "/REG/GerenciarClientResp.jsp";
			} 
			
			if(acao.equals("liberar"))
			{
				GerenciarClientBean.controlaClient(acao, GerenciarClientBean);
				GerenciarClientBean.getSituacaoExecucao();
				nextRetorno = "/REG/GerenciarClientResp.jsp";
			}
			
			if(acao.equals("parar"))
			{
				GerenciarClientBean.controlaClient(acao, GerenciarClientBean);
				GerenciarClientBean.getSituacaoExecucao();
				nextRetorno = "/REG/GerenciarClientResp.jsp";
			}
			
			
		} catch (Exception e) {
			throw new sys.CommandException("GerenciarClientCmd 001: " + e.getMessage());
		}   
		req.setAttribute("GerenciarClientBean", GerenciarClientBean);

		return nextRetorno;
		
	}
	
	
}
