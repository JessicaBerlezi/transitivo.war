package REG;


import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import sys.BeanException;
import sys.Util;


/**
 * Classe para promover a visualiza��o de informa��es operacionais sobre o andamento das 
 * notifica��es que est�o sendo trafegadas entre o DETRAN e a VEX.
 * PADR�O COMMAND
 * 
 * @author Sergio Roberto Junior
 * @since 17/12/2004
 */
public class BalancoPendenciaARCmd  extends sys.Command {
	private static final String jspPadrao="/REG/BalancoPendenciaAR.jsp";    
	private String next;
	
	public BalancoPendenciaARCmd() {
		next = jspPadrao;
	}
	
	public BalancoPendenciaARCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new REC.ParamOrgBean() ;
			
			ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
			
			String datRetornoVex      = parSis.getParamSist("DAT_RETORNO_VEX");
			String datReferenciaVex   = parSis.getParamSist("MESANO_RET_VEX");			
			
			BalancoPendenciaARBean BalancoPendenciaARBeanId = (BalancoPendenciaARBean)session.getAttribute("BalancoPendenciaARBeanId");
			if (BalancoPendenciaARBeanId==null)  BalancoPendenciaARBeanId = new BalancoPendenciaARBean();
			BalancoPendenciaARBeanId.setMsgErro("");
			
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="";
			
			String acao    = req.getParameter("acao");
			if( acao == null ) acao = "";
			
			if  (acao.equals("consultarAno"))  {
				String sNumAno=req.getParameter("numAno");
				if (sNumAno==null) sNumAno=sys.Util.formatedToday().substring(6,10);
				
				String sDtReferencia = req.getParameter("dtReferencia");
				if (sDtReferencia==null) sDtReferencia=sys.Util.formatedToday().substring(0,10);
				
				String statusAuto = req.getParameter("statusAuto");
				if (statusAuto==null) statusAuto="";

				BalancoPendenciaARBeanId = new BalancoPendenciaARBean();
				BalancoPendenciaARBeanId.setAno(sNumAno);
				BalancoPendenciaARBeanId.setDatInicio("01/01/"+sNumAno);
				BalancoPendenciaARBeanId.setDatFim("31/12/"+sNumAno);
				BalancoPendenciaARBeanId.setDatReferencia(sDtReferencia);
				BalancoPendenciaARBeanId.setDatContrato(datReferenciaVex);
				
                
				/*Preparar Estrutura do Relat�rio*/
				montarEstruturaRelatorio(BalancoPendenciaARBeanId);
				BalancoPendenciaARBeanId.LeBanlancoPendenciaAR();
			    /*Apresenta��o*/
				String tituloConsulta = null;
				tituloConsulta = "BALAN�O DE PEND�NCIA DE ARS - ANO : "+
				BalancoPendenciaARBeanId.getAno()+" - POSI��O EM : "+datRetornoVex+
				" - EMISS�ES � PARTIR DE : "+BalancoPendenciaARBeanId.getDatContrato();
				nextRetorno = "/REG/RelatBalancoPendencias.jsp";
				req.setAttribute("tituloConsulta", tituloConsulta);
			}

			session.setAttribute("BalancoPendenciaARBeanId",BalancoPendenciaARBeanId) ; 
		
			return nextRetorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("ResumoRetornoCmd: " + ue.getMessage());
		}
	}
	

	public void montarEstruturaRelatorio(BalancoPendenciaARBean BalancoPendenciaARBeanId)throws BeanException
	{
		/*Montar Totais Zerados de Enviados/N�o Retornados/Retornados*/
		ArrayList <BalancoPendenciaARBean>myListBalanco = new ArrayList<BalancoPendenciaARBean>();
		for (int i=0;i<12;i++)
		{
			BalancoPendenciaARBean  myBalanco = new BalancoPendenciaARBean();
			myBalanco.setMes(Util.lPad(String.valueOf(i+1),"0",2));
			myListBalanco.add(myBalanco);
		}
		BalancoPendenciaARBeanId.setListRetornoMes(myListBalanco);
	}
}






