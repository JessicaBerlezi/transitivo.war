package REG;

import ROB.RobotException;





/**
* <b>Title:</b>        Para Robo - Bean<br>
* <b>Description:</b>  Permite parar o processamento do Robo<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
 */
public class GerenciarRobotBean extends sys.MonitorLog{
	
	public final String STATUS_LIBERADO = "0";
	public final String STATUS_PARADO = "1";
	public final String COD_TIPO_REGISTRO = "1";
	
    private String codTipoRegistro;
	private String indContinua;
	private String situacaoRobo;
	private String situacaoRoboBloqueio;
	private String codMonitor;
	private String datUltimaExec;

	
	public GerenciarRobotBean(){
		super(); 
		codTipoRegistro = "";
		indContinua = "";
		situacaoRobo = "";
		situacaoRoboBloqueio="";
		codMonitor = "";
		datUltimaExec = "";
	}
	
	public void setCodMonitor(String codMonitor){
		if (codMonitor == null) codMonitor ="";
		else this.codMonitor = codMonitor;
	}
	public String getCodMonitor(){
		return this.codMonitor;
	}
	
	public void setCodTipoRegistro(String codTipoRegistro){
		if(codTipoRegistro == null) codTipoRegistro = "";
		else this.codTipoRegistro = codTipoRegistro;
	}
	public String getCodTipoRegistro(){
		return this.codTipoRegistro;
	}
	
	public void setIndContinua(String indContinua){
		if(indContinua == null) indContinua = "";
		else this.indContinua = indContinua;
	}
	public String getIndContinua(){
	  return this.indContinua;
	}
	
	public void setSituacaoRobo(String situacaoRobo){
		if(situacaoRobo == null) situacaoRobo = "";
		else this.situacaoRobo = situacaoRobo;
	}
	public String getSituacaoRobo(){
	  return this.situacaoRobo;
	}
	
	public void setSituacaoBloqueio(String situacaoRoboBloqueio){
		if(situacaoRoboBloqueio == null) situacaoRoboBloqueio = "";
		else this.situacaoRoboBloqueio = situacaoRoboBloqueio;
	}
	public String getSituacaoBloqueio() {		
		return this.situacaoRoboBloqueio;
	}
	
	public void setDatUltimaExec(String datUltimaExec) {
		if(datUltimaExec == null) datUltimaExec = "";
		else this.datUltimaExec = datUltimaExec;
	}
	public String getDatUltimaExec() {
		return datUltimaExec;
	}

	
	public String getSituacaoExecucao() throws ROB.RobotException{
		
		String situacao = "";		
		try {			
			ROB.Dao dao = ROB.Dao.getInstance();
			if (dao.verificarExecucao(this.getNomProcesso()))
				situacao = "Execu��o...";
			else
				situacao = "N�o executando";
		}
		catch (Exception e) {
			throw new RobotException(e.getMessage());
		}
				
		return situacao;
	}
	
	
	//Consulta o Status do Robot
	public boolean consultaStatusRobot(String nomRobot, GerenciarRobotBean robot)
		throws sys.BeanException {
		boolean ok = false;
		try {

			Dao dao = Dao.getInstance();
			if(dao.consultaStatusRobot(nomRobot, this)== true){
		        String situacao = "";
				String teste = this.getSituacaoRobo();
				if (this.getSituacaoRobo().equals("1")) 
				  situacao = "Robo Parado";
				else situacao = "Robo Liberado";
			  
			    this.setSituacaoBloqueio(situacao);
			    ok =true;
			}
			 
		
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
		return ok;
	}
	
	// Libera o Robo
	public boolean liberarRobot(String nomRobot)
		throws sys.BeanException {
		boolean ok = false;
		try {
			
			Dao dao = Dao.getInstance();
			if(dao.liberaRobot(nomRobot)== true){
				if (this.getSituacaoRobo().equals("1")) this.setSituacaoRobo(this.STATUS_PARADO);
				else this.setSituacaoRobo(this.STATUS_LIBERADO);
				ok = true;
				this.setMsgErro("Robot liberado com sucesso! \n");
			}
			else {
				ok = false;
				this.setMsgErro("Erro ao executar a opera��o de libera��o do Robot. \n");
			}
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
		return ok;
	}
	
	//Bloquea o Robo
	public boolean pararRobot(String nomRobot)
		throws sys.BeanException {
		boolean ok = false;
		try {
			
			Dao dao = Dao.getInstance();
			if(dao.paraRobot(nomRobot)== true){
				ok = true; 
				this.setMsgErro("Robot bloqueado com sucesso! \n");
			}
			else {
				ok = false;
				this.setMsgErro("Erro ao executar a opera��o de bloqueio do Robot. \n");
			}  	
		} 
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} 
		return ok;
	}

}   
