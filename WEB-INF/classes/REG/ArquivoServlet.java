package REG;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sys.GerenciadorException;


/**
 * @author sjunior
 */

public class ArquivoServlet extends HttpServlet{
	
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		try {
			HttpSession session = request.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
			
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) request.getSession().getAttribute("ParamSistemaBeanId");
			if (param == null) {
				request.setAttribute("javax.servlet.jsp.jspException", null);
				GerenciadorException.registraException(null, request);
				return;
				/*throw new ServletException("Sess�o de Trabalho inativa ! ("+usrLogado.getNomUserName()+")");*/	
			}
				
			
			String codArquivo=request.getParameter("codArquivo");
			if (codArquivo==null) codArquivo="";
			
			String extensao = request.getParameter("extensao");
			if (extensao==null) extensao="";
			if (extensao.length()>3) extensao=extensao.substring(0,3);
			
			String funcao = request.getParameter("funcao");
			if (funcao==null) funcao="";			

			
			Dao dao = Dao.getInstance();
			
			ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
			arqRecebido.setCodArquivo(codArquivo);
			
			/*
			 * Teste
			 
			String dataPostagem =  sys.Util.somaDias(5);
			System.out.println(dao.informaDataPostagemBroker("912", "LOR000002", "LOR0142", dataPostagem));
			*/
			
			/*
			 * Restando implementar para Pontua��o
			 */
			if ("REG2067".equals(funcao)) {
				dao.insereDownloadArquivoPontuacao(usrLogado,codArquivo,extensao);
				
			}
			else
			{
				dao.insereDownloadArquivo(usrLogado,codArquivo,extensao);
				
				/**
				 * 
				 BLOQUEIADO TEMPORARIAMENTE PARA FAZER UM ROBO DE PROCESSAMENTO
				if((arqRecebido.getCodIdentArquivo().equals("EMITEVEX")) || (arqRecebido.getCodIdentArquivo().equals("EMITEPER"))){
					arqRecebido.enviaDataPostagemBrokerMulta();
				}
				*/
				
				/*
				 * Caso o arquivo seja de envio para VEX executa o m�todo para 
				 * controle da notifica��o.
				 */  
				if("1".equals(param.getParamSist("HABILITA_CONTROLE_NOTIFICACAO"))){
					if(arqRecebido.getCodIdentArquivo().equals("EMITEVEX")){
						// Carrega o registro do arquivo junto com suas linhas
						arqRecebido.getArquivoRecebido(); /*27/07/2007*/
						dao.recebeDownloadNotif(arqRecebido,usrLogado);
						/*dao.GravaDataDownload(arqRecebido,usrLogado); 06/08/2009*/
					}
				}
			}
			
			dao.informaDataDownloadArquivo(arqRecebido.getCodArquivo());
			
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
			rd.forward(request, response);
			
		}
		catch (Exception e) {
			if (!e.getClass().getName().endsWith("ClientAbortException")) {
				throw new ServletException(e.getMessage());   
			}
		}               
	}
}
