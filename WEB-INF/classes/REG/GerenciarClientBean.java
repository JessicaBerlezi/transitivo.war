package REG;

import sys.BeanException;

/**
* <b>Title:</b>        Para Client - Bean<br>
* <b>Description:</b>  Permite parar o processamento do Client<br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class GerenciarClientBean extends sys.MonitorLog{
	
	public final String STATUS_LIBERADO = "0";
	public final String STATUS_PARADO = "1";
		
    private String codTipoRegistro;
	private String indContinua;
	private String situacaoClient;
	private String nomClient;
	private String valClient;
	private String statusClient;

	
	public GerenciarClientBean(){
		super(); 
		codTipoRegistro = "";
		indContinua = "";
		situacaoClient = "";
		nomClient = "";
		valClient = "";
		statusClient = "";
	}
	
	
	public void setCodTipoRegistro(String codTipoRegistro){
		if(codTipoRegistro == null) codTipoRegistro = "";
		else this.codTipoRegistro = codTipoRegistro;
	}
	
	public String getCodTipoRegistro(){
		return this.codTipoRegistro;
	}
	
	public void setIndContinua(String indContinua){
		if(indContinua == null) indContinua = "";
		else this.indContinua = indContinua;
	}
	
	public String getIndContinua(){
	  return this.indContinua;
	}
	
	public void setSituacaoClient(String situacaoClient){
		if(situacaoClient == null) situacaoClient = "";
		else this.situacaoClient = situacaoClient;
	}
	
	public String getSituacaoClient(){
	  return this.situacaoClient;
	}
	
	public void setNomClient(String nomClient) {
		if(nomClient == null) this.nomClient = "";
		else this.nomClient = nomClient;
	}
	   
	public String getNomClient() {
		return nomClient;
	}
	
	public void setStatusClient(String statusClient) {
		if(statusClient == null)this.statusClient = "";
		else this.statusClient = statusClient;
	}
	
	public String getStatusClient() {
		return statusClient;
	}
	
	public void setValClient(String valClient) {
		if(valClient == null) this.valClient = "";
		this.valClient = valClient;
	}

	public String getValClient() {
		return valClient;
	}


	public void getSituacaoExecucao() throws BeanException {

		String situacao = "";		
		String valParam = "";
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			
			valParam = param.getParamSist("PARAR_SMIT_DIG");

			if (valParam.equals("N"))
			{
				situacao = "Em execu��o...";
				this.setStatusClient(this.STATUS_LIBERADO);
			}
			else if (valParam.equals("S"))
			{
				situacao = "Bloqueado";
				this.setStatusClient(this.STATUS_PARADO);
			}
			this.setSituacaoClient(situacao);
			this.setNomClient(this.getValClient().substring(6,getValClient().length()));
			
		} catch (BeanException e) {
			throw new BeanException(e.getMessage());
		}
		
	}
	
	public void controlaClient(String acao, GerenciarClientBean GerenciarClientBean)
		throws sys.BeanException {

		//Pega os parametros de Sistema
		ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
		param.setCodSistema("45"); //M�dulo Recurso 
		param.setNomParam(GerenciarClientBean.getValClient());
		param.Le_ParamSist();
		
		if (acao.equals("liberar"))
		{
			param.setValParametro("N");
		}
		else if (acao.equals("parar"))
		{
			param.setValParametro("S");
		}
		param.atualizaParamSit(param);

	}
	

}   
