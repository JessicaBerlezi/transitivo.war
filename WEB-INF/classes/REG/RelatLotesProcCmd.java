package REG;

/**
 * <b>Title:</b>        Gerencial - Relatorio de Lotes Processos <br>
 * <b>Description:</b>  Informa qtd de autos processados, cancelados e rejeitados. <br>
 * <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luciana Rocha
 * @version 1.0
 */

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RelatLotesProcCmd extends sys.Command {
    private static final String jspPadrao = "/REG/RelatLotesProc.jsp";

    private String next;

    public RelatLotesProcCmd() {
        next = jspPadrao;
    }

    public RelatLotesProcCmd(String next) {
        this.next = next;
    }

    public String execute(HttpServletRequest req) throws sys.CommandException 
    { 
        String nextRetorno = next;
        try {
            HttpSession session = req.getSession();
            RelatLotesProcBean RelatLotesProcBean = (RelatLotesProcBean) session
                    .getAttribute("RelatLotesProcBean");
            if (RelatLotesProcBean == null)
                RelatLotesProcBean = new RelatLotesProcBean();

            ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req
                    .getAttribute("OrgaoBeanId");
            if (OrgaoBeanId == null)
                OrgaoBeanId = new ACSS.OrgaoBean();

            // Carrego os dados da Tela
            String datInicio = req.getParameter("datInicio");
            if (datInicio == null)
                datInicio = "";

            String datFim = req.getParameter("datFim");
            if (datFim == null)
                datFim = "";
            
            String data = req.getParameter("data");
            if (data == null)
                data = "";

            String codOrgao = req.getParameter("codOrgao");
            if (codOrgao == null)
                codOrgao = "";

            String acao = req.getParameter("acao");
            if (acao == null)
                acao = "";

            if (acao.equals("")) {
                RelatLotesProcBean = new RelatLotesProcBean();
                session.setAttribute("RelatLotesProcBean", RelatLotesProcBean);
                OrgaoBeanId = new ACSS.OrgaoBean();
                session.setAttribute("OrgaoBeanId", OrgaoBeanId);
                
            } else if (acao.equals("geraRelatorio")) {
                RelatLotesProcBean.setDataIni(datInicio);
                RelatLotesProcBean.setDataFim(datFim);
                RelatLotesProcBean.setCodOrgao(codOrgao);
                OrgaoBeanId.Le_Orgao(codOrgao, 0);

                // Carrega os relatores
                RelatLotesProcBean.getLotesProcessados();

                String msg = "";
                if (RelatLotesProcBean.getDados().size() == 0)
                    msg = "N�O EXISTEM DADOS A SEREM EXIBIDOS NESTE PER�ODO.";
                RelatLotesProcBean.setMsgErro(msg);
                nextRetorno = "/REG/RelatLotesProcResult.jsp";
                
            } else if (acao.equals("imprimirRelatorio")) {
                String tituloConsulta = "RELAT�RIO DE AUTOS PROCESSADOS - "
                        + RelatLotesProcBean.getDataIni() + " A "
                        + RelatLotesProcBean.getDataFim();
                req.setAttribute("tituloConsulta", tituloConsulta);
                nextRetorno = "/REG/RelatLotesProcResultImp.jsp";
                
            } else if (acao.equals("PROCESSADOS")) {
                OrgaoBeanId.Le_Orgao(RelatLotesProcBean.getCodOrgao(), 0);
                RelatLotesProcBean.setTipoConsulta(acao);
                RelatLotesProcBean.setDatLote(data);
                RelatLotesProcBean.mostraDetalhe();
                nextRetorno = "/REG/RelatLotesProcDetalhe.jsp";
                
            } else if (acao.equals("GRAVADOS")) {
                OrgaoBeanId.Le_Orgao(RelatLotesProcBean.getCodOrgao(), 0);
                RelatLotesProcBean.setTipoConsulta(acao);
                RelatLotesProcBean.setDatLote(data);
                RelatLotesProcBean.mostraDetalhe();
                nextRetorno = "/REG/RelatLotesProcDetalhe.jsp";
                
            } else if (acao.equals("REJEITADOS")) {
                OrgaoBeanId.Le_Orgao(RelatLotesProcBean.getCodOrgao(), 0);
                RelatLotesProcBean.setTipoConsulta(acao);
                RelatLotesProcBean.setDatLote(data);
                RelatLotesProcBean.mostraDetalhe();
                nextRetorno = "/REG/RelatLotesProcDetalhe.jsp";
                
            } else if (acao.equals("CANCELADOS")) {
                OrgaoBeanId.Le_Orgao(RelatLotesProcBean.getCodOrgao(), 0);
                RelatLotesProcBean.setTipoConsulta(acao);
                RelatLotesProcBean.setDatLote(data);
                RelatLotesProcBean.mostraDetalhe();
                nextRetorno = "/REG/RelatLotesProcDetalhe.jsp";
                
            }
            else if(acao.equals("imprimirRelDetalhe")){
                String tituloConsulta = "RELAT�RIO DE AUTOS PROCESSADOS - "
                    + RelatLotesProcBean.getDataIni() + " A "
                    + RelatLotesProcBean.getDataFim()+ "<BR> AUTOS "+RelatLotesProcBean.getTipoConsulta();
                OrgaoBeanId.Le_Orgao(RelatLotesProcBean.getCodOrgao(), 0);
                req.setAttribute("tituloConsulta", tituloConsulta);
                nextRetorno = "/REG/RelatLotesProcDetalheImp.jsp";
            }
                
            session.setAttribute("OrgaoBeanId", OrgaoBeanId);
            session.setAttribute("RelatLotesProcBean", RelatLotesProcBean);

        } catch (Exception e) {
            throw new sys.CommandException("RelatLotesProcCmd: "
                    + e.getMessage());
        }
        return nextRetorno;
    }
}