package REG;


import java.util.ArrayList;
import java.util.List;
import REC.EventoBean;


/**
* <b>Title:</b>        Controle de Acesso - Bean de Funcionalidades/Operacoes<br>
* <b>Description:</b>  Bean dados dos Usu�rios - Tabela de Hist�rico de Usu�rios<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Alexandre Bahia
* @version 1.0
* @Updates
*/

public class RelatArDigitTMPBean_Cont  extends sys.HtmlPopupBean{

	private String codRelArDig;
	private String datEnvio;
	private String qtdRetornados;
	private String qtdApropriado;
	private String qtdDesprezados;
	private String qtdNaoProcessados ;
	private String qtdAvisos ;
	

	private List dados;
	
	
	public RelatArDigitTMPBean_Cont() throws sys.BeanException {
		
		
		dados = new ArrayList();

		codRelArDig     = "";
		datEnvio        = "";
		qtdRetornados   = "0";
		qtdApropriado   = "0";
		qtdDesprezados   = "0";
		qtdNaoProcessados = "0";
		qtdAvisos = "0";

	}

	public void setCodRelArDig(String codRelArDig) {
		if(codRelArDig == null) codRelArDig = "";
		else this.codRelArDig = codRelArDig;
	}
	public String getCodRelArDig() {
		return codRelArDig;
	}

	public void setDatEnvio(String datEnvio) {
		if(datEnvio == null) datEnvio = "";
		else this.datEnvio = datEnvio;
	}
	public String getDatEnvio() {
		return datEnvio;
	}

	public void setQtdRetornados(String qtdRetornados) {
		if(qtdRetornados == null) qtdRetornados = "0";
		else this.qtdRetornados = qtdRetornados;
	}
	public String getQtdRetornados() {
		return qtdRetornados;
	}
	
	public void setQtdApropriado(String qtdApropriado) {
		if(qtdApropriado == null) qtdApropriado = "0";
		else this.qtdApropriado = qtdApropriado;
	}
	public String getQtdApropriado() {
		return qtdApropriado;
	}
	
	public void setQtdDesprezados(String qtdDesprezados) {
		if(qtdDesprezados == null) qtdDesprezados = "0";
		else this.qtdDesprezados = qtdDesprezados;
	}
	public String getQtdDesprezados() {
		return qtdDesprezados;
	}

	public void setQtdNaoProcessados(String qtdNaoProcessados) {
		if(qtdNaoProcessados == null) qtdNaoProcessados = "0";
		else this.qtdNaoProcessados = qtdNaoProcessados;
	}
	public String getQtdNaoProcessados() {
		return qtdNaoProcessados;
	}
	
	public void setQtdAvisos(String qtdAvisos) {
		if(qtdAvisos == null) qtdAvisos = "0";
		else this.qtdAvisos = qtdAvisos;
	}
	public String getQtdAvisos() {
		return qtdAvisos;
	}
	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}

	public RelatArDigitTMPBean_Cont getEventos(int i){
		   return (RelatArDigitTMPBean_Cont)this.dados.get(i);
	}
	
	

}
	
	
	
