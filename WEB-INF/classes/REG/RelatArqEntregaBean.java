package REG;

import java.math.BigInteger;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import sys.BeanException;

public class RelatArqEntregaBean extends sys.HtmlPopupBean {
	
	private ACSS.OrgaoBean orgao;
	private ACSS.UsuarioBean usuario;
	private ACSS.SistemaBean sistema;
	private Date datInicial;
	private Date datFinal;
	
	private Date datProc;
	private Integer qtdRegistrado;
	private Integer totalQtdRegistrado;
	private Integer qtdRecebido;
	private Integer totalQtdRecebido;	
	private Integer qtdEntregue;
	private Integer totalQtdEntregue;
	private Integer qtdPendente;
	private Integer qtdTempo;
	private Integer totalQtdTempo;
	
	private List beans;
	
	public RelatArqEntregaBean() throws sys.BeanException {
		super();
		orgao = new ACSS.OrgaoBean();
		usuario = new ACSS.UsuarioBean();
		sistema = new ACSS.SistemaBean();
		datInicial = null;
		datFinal = null;
		
		
		datProc = null;;
		qtdRegistrado = new Integer(0);
		totalQtdRegistrado = new Integer(0);
		qtdRecebido = new Integer(0);
		totalQtdRecebido = new Integer(0);	
		qtdEntregue = new Integer(0);
		totalQtdEntregue = new Integer(0);
		qtdPendente = new Integer(0);
		qtdTempo = new Integer(0);
		totalQtdTempo = new Integer(0);
		
		beans = new ArrayList(); 
	}
	
	public ACSS.OrgaoBean getOrgao() {
		return orgao;
	}
	public void setOrgao(ACSS.OrgaoBean orgao) {
		this.orgao = orgao;
	}
	
	public ACSS.SistemaBean getSistema() {
		return sistema;
	}
	public void setSistema(ACSS.SistemaBean sistema) {
		this.sistema = sistema;
	}
	
	public ACSS.UsuarioBean getUsuario() {
		return usuario;
	}
	public void setUsuario(ACSS.UsuarioBean usuario) {
		this.usuario = usuario;
	}	
	
	public Date getDatFinal() {
		return datFinal;
	}
	public void setDatFinal(Date datFinal) {
		this.datFinal = datFinal;
	}
	public void setDatFinal(String datFinal) throws BeanException {
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			this.datFinal = df.parse(datFinal);
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
	public Date getDatInicial() {
		return datInicial;
	}
	public void setDatInicial(Date datInicial) {
		this.datInicial = datInicial;
	}
	public void setDatInicial(String datInicial) throws BeanException {
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			this.datInicial = df.parse(datInicial);
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	
	
	public Date getDatProc() {
		return datProc;
	}
	public void setDatProc(Date datProc) {
		this.datProc = datProc;
	}
	public void setDatProc(String datProc) throws BeanException {
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			this.datProc = df.parse(datProc);
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
	public Integer getQtdEntregue() {
		return qtdEntregue;
	}
	public void setQtdEntregue(Integer qtdEntregue) {
		this.qtdEntregue = qtdEntregue;
	}
	
	public Integer getQtdPendente() {
		return qtdPendente;
	}
	public void setQtdPendente(Integer qtdPendente) {
		this.qtdPendente = qtdPendente;
	}
	
	public Integer getQtdRecebido() {
		return qtdRecebido;
	}
	public void setQtdRecebido(Integer qtdRecebido) {
		this.qtdRecebido = qtdRecebido;
	}
	
	public Integer getQtdRegistrado() {
		return qtdRegistrado;
	}
	public void setQtdRegistrado(Integer qtdRegistrado) {
		this.qtdRegistrado = qtdRegistrado;
	}
	
	public Integer getTotalQtdEntregue() {
		return totalQtdEntregue;
	}
	public void setTotalQtdEntregue(Integer totalQtdEntregue) {
		this.totalQtdEntregue = totalQtdEntregue;
	}
	
	public Integer getTotalQtdRecebido() {
		return totalQtdRecebido;
	}
	public void setTotalQtdRecebido(Integer totalQtdRecebido) {
		this.totalQtdRecebido = totalQtdRecebido;
	}
	
	public Integer getTotalQtdRegistrado() {
		return totalQtdRegistrado;
	}
	public void setTotalQtdRegistrado(Integer totalQtdRegistrado) {
		this.totalQtdRegistrado = totalQtdRegistrado;
	}
	
	public Integer getQtdTempo() {
		return qtdTempo;
	}
	public void setQtdTempo(Integer qtdTempo) {
		this.qtdTempo = qtdTempo;
	}
	public Integer getTempoMedio() {
		try {
			BigInteger divisao = new BigInteger(qtdTempo.toString());
			BigInteger resultado = divisao.divide(new BigInteger(qtdEntregue.toString()));
			return new Integer(resultado.intValue());
		} catch (Exception e) {
			return new Integer(0);
		}
	}	
	
	public Integer getTotalQtdTempo() {
		return totalQtdTempo;
	}
	public void setTotalQtdTempo(Integer totalQtdTempo) {
		this.totalQtdTempo = totalQtdTempo;
	}
	public Integer getTotalTempoMedio() {
		try {
			BigInteger divisao = new BigInteger(totalQtdTempo.toString());
			BigInteger resultado = divisao.divide(new BigInteger(totalQtdEntregue.toString()));
			return new Integer(resultado.intValue());
		} catch (Exception e) {
			return new Integer(0);
		}		
	}	

	public List getBeans() {
		return beans;
	}
	public void setBeans(List beans) {
		this.beans = beans;
	}
	public RelatArqEntregaBean getBean(int i) {
		return (RelatArqEntregaBean) beans.get(i);
	}	
	
	public boolean consultaRelEntrega(Vector vErro) throws  BeanException {
		boolean existe = false;		
		try {
			Dao dao = Dao.getInstance();
			if (dao.ConsultaRelEntrega(this)) 
				existe = true;
			else
				vErro.add("N�o existe(m) registro(s) entre as datas informadas para forma��o do relat�rio! \n");
		} catch (Exception e) { 
			throw new sys.BeanException(e.getMessage()); 
		}		
		setMsgErro(vErro);
		return existe;	
	}
	
	public void grava(Connection conn) throws  BeanException {
		try {
			Dao dao = Dao.getInstance();
			dao.GravaRelEntrega(this, conn);
		} catch (Exception e) { 
			throw new sys.BeanException(e.getMessage()); 
		}
	}
	
	public boolean totaliza() throws  BeanException {		
		boolean retorno = false;
		try {
			Dao dao = Dao.getInstance();
			retorno = dao.TotalizaEntrega(this);
		} catch (Exception e) { 
			throw new sys.BeanException(e.getMessage()); 
		}
		return retorno;
	}
	
}