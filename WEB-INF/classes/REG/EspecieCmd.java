package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Especie<br>
* <b>Description:</b>  Comando para Consultar Especies<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class EspecieCmd extends sys.Command {

	private static final String jspPadrao = "/REG/EspecieConsulta.jsp";

	private String next;

	public EspecieCmd() {
		next = jspPadrao;
	}

	public EspecieCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();

		try {

			TAB.EspecieBeanCol Especiebc = new TAB.EspecieBeanCol();
			Especiebc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("EspecieBeanColId", Especiebc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}
		return nextRetorno;
	}

}
