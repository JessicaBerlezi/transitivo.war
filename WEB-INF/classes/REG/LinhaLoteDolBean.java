package REG;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import sys.BeanException;
import ACSS.UsuarioBean;
import REC.AutoInfracaoBean;

/**
 * <b>Title:</b>        SMIT - Bean de Arquivo Recebido via Upload<br>
 * <b>Description:</b>  Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      MIND Informatica<br>
 * @author  			Luiz Medronho
 * @version 			1.0
 */

public class LinhaLoteDolBean extends sys.HtmlPopupBean {
	
	private String codLinhaLote;
	private String dscLinhaLote;
	private String dscLinhaLoteNIT;
	private String codArquivo;
	private String codLote;
	private String numSeqLinha;
	private String codStatus;
	private Vector erroLinhaLote;
	private String codRetorno;
	private String dscLinhaLoteRetorno;
	private String dscErro;
	
	//Atributos do Arquivo Autuacao DOL	
	private String identArquivo;
	private String tipoRegistro;
	private String username;
	private String codOrgaoLotacao;
	private String tipoTransacao;
	private String codOrgao;
	private String numAuto;
	private String numPlaca;
	private String codMarca;
	private String localInfracao;
	private String dataInfracao;
	private String horaInfracao;
	private String codMunicipio;
	private String codInfracao;
	private String valorMulta;
	private String dataVencimento;
	private String codAgente;
	private String matriculaAgente;
	private String velocidadePermitida;
	private String velocidadeAferida;
	private String velocidadeConsiderada;
	private String tipoDispositivo;
	private String identAparelho;
	private String numInmetro;
	private String dataUltimaAfericao;
	private String numCertificado;
	private String localAparelho;
	private String especieVeiculo;
	private String categoriaVeiculo;
	private String tipoVeiculo;
	private String corVeiculo;
	private String nomeCondutor;
	private String numCpfCondutor;
	private String numCnhCondutor;
	private String uFCnhCondutor;
	private String enderecoCondutor;
	private String numEnderecoCondutor;
	private String complementoEnderecoCondutor;
	private String cepEndereco;
	private String municipioCondutor;
	private String observacao;
	private String complObservacao;
	private String identPRF;
	private String desInfracaoRes;
	private String marcaModApar;
	private String localInfrDet;
	private String desMotCanc;
	private String ind_P59;
	
	private Connection conn;
	
	public LinhaLoteDolBean() throws sys.BeanException {
		
		super();
		setTabela("TSMI_LINHA_LOTE");	
		setPopupWidth(35);
		
		erroLinhaLote = new Vector();
		dscErro = "";
		
		identArquivo = "";
		tipoRegistro = "";
		username = "";
		codOrgaoLotacao = "";
		tipoTransacao = "";
		codOrgao = "";
		numAuto = "";
		numPlaca = "";
		codMarca = "";
		localInfracao = "";
		dataInfracao = "";
		horaInfracao = "";
		codMunicipio = "";
		codInfracao = "";
		valorMulta = "";
		dataVencimento = "";
		codAgente = "";
		matriculaAgente = "";
		velocidadePermitida = "";
		velocidadeAferida = "";
		velocidadeConsiderada = "";
		tipoDispositivo = "";
		identAparelho = "";
		numInmetro = "";
		dataUltimaAfericao = "";
		numCertificado = "";
		localAparelho = "";
		especieVeiculo = "";
		categoriaVeiculo = "";
		tipoVeiculo = "";
		corVeiculo = "";
		nomeCondutor = "";
		numCpfCondutor = "";
		numCnhCondutor = "";
		uFCnhCondutor = "";
		enderecoCondutor = "";
		numEnderecoCondutor = "";
		complementoEnderecoCondutor = "";
		cepEndereco = "";
		municipioCondutor = "";
		observacao = "";
		complObservacao = "";
		identPRF = "";
		desInfracaoRes = "";
		marcaModApar = "";
		localInfrDet = "";
		desMotCanc = "";
		ind_P59 = "";
		
	}
	
	public void setCodLinhaLote(String codLinhaLote) {
		this.codLinhaLote = codLinhaLote;
		if (codLinhaLote == null)
			this.codLinhaLote = "";
	}
	
	public String getCodLinhaLote() {
		return this.codLinhaLote;
	}

	public void setDscLinhaLote(String dscLinhaLote) {
		this.dscLinhaLote = dscLinhaLote;
		if (dscLinhaLote == null)
			this.dscLinhaLote = "";
	}
	
	public void setDscLinhaLoteNIT(String dscLinhaLoteNIT) {
		this.dscLinhaLoteNIT = dscLinhaLoteNIT;
		if (dscLinhaLoteNIT == null)
			this.dscLinhaLoteNIT = "";
	}

	public void setDscLinhaLote() {
		identArquivo = sys.Util.rPad(identArquivo, " ", 4);
		tipoRegistro = sys.Util.rPad(tipoRegistro, " ", 4);
		username = sys.Util.rPad(username, " ", 20);
		codOrgaoLotacao = sys.Util.lPad(codOrgaoLotacao, "0", 6);
		tipoTransacao = sys.Util.lPad(tipoTransacao, "0", 1);
		codOrgao = sys.Util.lPad(codOrgao, "0", 6);
		numAuto = sys.Util.rPad(numAuto, " ", 12);
		numPlaca = sys.Util.rPad(numPlaca, " ", 7);
		codMarca = sys.Util.lPad(codMarca, "0", 7);
		localInfracao = sys.Util.rPad(localInfracao, " ", 45);
		dataInfracao = sys.Util.rPad(dataInfracao, " ", 8);
		horaInfracao = sys.Util.rPad(horaInfracao, " ", 4);
		codMunicipio = sys.Util.lPad(codMunicipio, "0", 4);
		codInfracao = sys.Util.lPad(codInfracao, "0", 3) + "0";
		valorMulta = sys.Util.lPad(valorMulta, "0", 7);
		dataVencimento = sys.Util.rPad(dataVencimento, " ", 8);
		codAgente = sys.Util.lPad(codAgente, "0", 2);
		matriculaAgente = sys.Util.rPad(matriculaAgente, " ", 10);
		velocidadePermitida = sys.Util.lPad(velocidadePermitida, "0", 3);
		velocidadeAferida = sys.Util.rPad(velocidadeAferida, " ", 4);
		velocidadeConsiderada = sys.Util.lPad(velocidadeConsiderada, "0", 4);
		tipoDispositivo = sys.Util.rPad(tipoDispositivo, " ", 1);
		identAparelho = sys.Util.rPad(identAparelho, " ", 15);
		numInmetro = sys.Util.rPad(numInmetro, " ", 9);
		dataUltimaAfericao = sys.Util.rPad(dataUltimaAfericao, " ", 8);		
		numCertificado = sys.Util.rPad(numCertificado, " ", 10);
		localAparelho = sys.Util.rPad(localAparelho, " ", 40);
		especieVeiculo = sys.Util.rPad(especieVeiculo, " ", 2);
		categoriaVeiculo = sys.Util.rPad(categoriaVeiculo, " ", 2);
		tipoVeiculo = sys.Util.lPad(tipoVeiculo, "0", 3);
		corVeiculo = sys.Util.lPad(corVeiculo, "0", 3);
		nomeCondutor = sys.Util.rPad(nomeCondutor, " ", 45);
		numCpfCondutor = sys.Util.lPad(numCpfCondutor, "0", 11);
		numCnhCondutor = sys.Util.lPad(numCnhCondutor, "0", 11);
		uFCnhCondutor = sys.Util.rPad(uFCnhCondutor, " ", 2);
		enderecoCondutor = sys.Util.rPad(enderecoCondutor, " ", 25);
		numEnderecoCondutor = sys.Util.rPad(numEnderecoCondutor, " ", 5);
		complementoEnderecoCondutor = sys.Util.rPad(complementoEnderecoCondutor, " ", 11);
		cepEndereco = sys.Util.lPad(cepEndereco, "0", 8);
		municipioCondutor = sys.Util.lPad(municipioCondutor, "0", 4);
		observacao = sys.Util.rPad(observacao, " ", 51);
		identPRF = sys.Util.rPad(identPRF, " ", 9);
		desMotCanc = sys.Util.rPad(desMotCanc, " ", 70);
		
		dscLinhaLote = 
			identArquivo +
			tipoRegistro +
			username +
			codOrgaoLotacao +
			tipoTransacao +
			codOrgao +
			numAuto +
			numPlaca +
			codMarca +
			localInfracao +
			dataInfracao +
			horaInfracao +
			codMunicipio +
			codInfracao.substring(0,3)  +
			dscLinhaLote.substring(131,132) +
			valorMulta +
			dataVencimento +
			codAgente +
			matriculaAgente +
			velocidadePermitida +
			velocidadeAferida +
			velocidadeConsiderada +
			tipoDispositivo +
			identAparelho +
			numInmetro +
			dataUltimaAfericao +
			numCertificado +
			localAparelho +
			especieVeiculo +
			categoriaVeiculo +
			tipoVeiculo +
			corVeiculo +
			nomeCondutor +
			numCpfCondutor +
			numCnhCondutor +
			uFCnhCondutor +
			enderecoCondutor +
			numEnderecoCondutor +
			complementoEnderecoCondutor +
			cepEndereco +
			municipioCondutor +
			observacao +
			identPRF +
		    desMotCanc;
		
	}
	
	
	public void setDscLinhaLoteNIT() {
		identArquivo = sys.Util.rPad(identArquivo, " ", 4);
		tipoRegistro = sys.Util.rPad(tipoRegistro, " ", 4);
		username = sys.Util.rPad(username, " ", 20);
		codOrgaoLotacao = sys.Util.lPad(codOrgaoLotacao, "0", 6);
		tipoTransacao = sys.Util.lPad(tipoTransacao, "0", 1);
		codOrgao = sys.Util.lPad(codOrgao, "0", 6);
		numAuto = sys.Util.rPad(numAuto, " ", 12);
		numPlaca = sys.Util.rPad(numPlaca, " ", 7);
		codMarca = sys.Util.lPad(codMarca, "0", 7);
		localInfracao = sys.Util.rPad(localInfracao, " ", 45);
		dataInfracao = sys.Util.rPad(dataInfracao, " ", 8);
		horaInfracao = sys.Util.rPad(horaInfracao, " ", 4);
		codMunicipio = sys.Util.lPad(codMunicipio, "0", 4);
		codInfracao = sys.Util.lPad(codInfracao, "0", 3) + "0";
		valorMulta = sys.Util.lPad(valorMulta, "0", 7);
		dataVencimento = sys.Util.rPad(dataVencimento, " ", 8);
		codAgente = sys.Util.lPad(codAgente, "0", 2);
		matriculaAgente = sys.Util.rPad(matriculaAgente, " ", 10);
		velocidadePermitida = sys.Util.lPad(velocidadePermitida, "0", 3);
		velocidadeAferida = sys.Util.rPad(velocidadeAferida, " ", 4);
		velocidadeConsiderada = sys.Util.lPad(velocidadeConsiderada, "0", 4);
		tipoDispositivo = sys.Util.rPad(tipoDispositivo, " ", 1);
		identAparelho = sys.Util.rPad(identAparelho, " ", 15);
		numInmetro = sys.Util.rPad(numInmetro, " ", 9);
		dataUltimaAfericao = sys.Util.rPad(dataUltimaAfericao, " ", 8);		
		numCertificado = sys.Util.rPad(numCertificado, " ", 10);
		localAparelho = sys.Util.rPad(localAparelho, " ", 40);
		especieVeiculo = sys.Util.rPad(especieVeiculo, " ", 2);
		categoriaVeiculo = sys.Util.rPad(categoriaVeiculo, " ", 2);
		tipoVeiculo = sys.Util.lPad(tipoVeiculo, "0", 3);
		corVeiculo = sys.Util.lPad(corVeiculo, "0", 3);
		nomeCondutor = sys.Util.rPad(nomeCondutor, " ", 45);
		numCpfCondutor = sys.Util.lPad(numCpfCondutor, "0", 11);
		numCnhCondutor = sys.Util.lPad(numCnhCondutor, "0", 11);
		uFCnhCondutor = sys.Util.rPad(uFCnhCondutor, " ", 2);
		enderecoCondutor = sys.Util.rPad(enderecoCondutor, " ", 25);
		numEnderecoCondutor = sys.Util.rPad(numEnderecoCondutor, " ", 5);
		complementoEnderecoCondutor = sys.Util.rPad(complementoEnderecoCondutor, " ", 11);
		cepEndereco = sys.Util.lPad(cepEndereco, "0", 8);
		municipioCondutor = sys.Util.lPad(municipioCondutor, "0", 4);
		observacao = sys.Util.rPad(observacao, " ", 51);
		identPRF = sys.Util.rPad(identPRF, " ", 9);
		desInfracaoRes = sys.Util.rPad(desInfracaoRes, " ", 150);
		complObservacao = sys.Util.rPad(complObservacao, " ", 100);
		marcaModApar = sys.Util.rPad(marcaModApar, " ", 120);
		localInfrDet = sys.Util.rPad(localInfrDet, " ", 100);
		desMotCanc = sys.Util.rPad(desMotCanc, " ", 70);
		
		dscLinhaLoteNIT = 
			identArquivo +
			tipoRegistro +
			username +
			codOrgaoLotacao +
			tipoTransacao +
			codOrgao +
			numAuto +
			numPlaca +
			codMarca +
			localInfracao +
			dataInfracao +
			horaInfracao +
			codMunicipio +
			codInfracao.substring(0,3)  +
			dscLinhaLote.substring(131,132) +
			valorMulta +
			dataVencimento +
			codAgente +
			matriculaAgente +
			velocidadePermitida +
			velocidadeAferida +
			velocidadeConsiderada +
			tipoDispositivo +
			identAparelho +
			numInmetro +
			dataUltimaAfericao +
			numCertificado +
			localAparelho +
			especieVeiculo +
			categoriaVeiculo +
			tipoVeiculo +
			corVeiculo +
			nomeCondutor +
			numCpfCondutor +
			numCnhCondutor +
			uFCnhCondutor +
			enderecoCondutor +
			numEnderecoCondutor +
			complementoEnderecoCondutor +
			cepEndereco +
			municipioCondutor +
			observacao +
			identPRF +
		    desInfracaoRes +
		    complObservacao +
		    marcaModApar +
		    localInfrDet + 
		    desMotCanc;
		
	}


	public void setDscLinhaLoteP59() {
		identArquivo = sys.Util.rPad(identArquivo, " ", 4);
		tipoRegistro = sys.Util.rPad(tipoRegistro, " ", 4);
		username = sys.Util.rPad(username, " ", 20);
		codOrgaoLotacao = sys.Util.lPad(codOrgaoLotacao, "0", 6);
		tipoTransacao = sys.Util.lPad(tipoTransacao, "0", 1);
		codOrgao = sys.Util.lPad(codOrgao, "0", 6);
		numAuto = sys.Util.rPad(numAuto, " ", 12);
		numPlaca = sys.Util.rPad(numPlaca, " ", 7);
		codMarca = sys.Util.lPad(codMarca, "0", 7);
		localInfracao = sys.Util.rPad(localInfracao, " ", 80);
		dataInfracao = sys.Util.rPad(dataInfracao, " ", 8);
		horaInfracao = sys.Util.rPad(horaInfracao, " ", 4);
		codMunicipio = sys.Util.lPad(codMunicipio, "0", 5);
		codInfracao = sys.Util.rPad(codInfracao, "0", 5);
		valorMulta = sys.Util.lPad(valorMulta, "0", 7);
		dataVencimento = sys.Util.rPad(dataVencimento, " ", 8);
		codAgente = sys.Util.lPad(codAgente, "0", 2);
		matriculaAgente = sys.Util.rPad(matriculaAgente, " ", 15);
		velocidadePermitida = sys.Util.lPad(velocidadePermitida, "0", 9);
		velocidadeAferida = sys.Util.rPad(velocidadeAferida, " ", 9);
		velocidadeConsiderada = sys.Util.lPad(velocidadeConsiderada, "0", 9);
		tipoDispositivo = sys.Util.rPad(tipoDispositivo, " ", 1);
		identAparelho = sys.Util.rPad(identAparelho, " ", 30);
		numInmetro = sys.Util.rPad(numInmetro, " ", 9);
		dataUltimaAfericao = sys.Util.rPad(dataUltimaAfericao, " ", 8);		
		numCertificado = sys.Util.rPad(numCertificado, " ", 30);
		localAparelho = sys.Util.rPad(localAparelho, " ", 40);
		especieVeiculo = sys.Util.rPad(especieVeiculo, " ", 2);
		categoriaVeiculo = sys.Util.rPad(categoriaVeiculo, " ", 2);
		tipoVeiculo = sys.Util.lPad(tipoVeiculo, "0", 3);
		corVeiculo = sys.Util.lPad(corVeiculo, "0", 3);
		nomeCondutor = sys.Util.rPad(nomeCondutor, " ", 60);
		numCpfCondutor = sys.Util.lPad(numCpfCondutor, "0", 11);
		numCnhCondutor = sys.Util.lPad(numCnhCondutor, "0", 11);
		uFCnhCondutor = sys.Util.rPad(uFCnhCondutor, " ", 2);
		enderecoCondutor = sys.Util.rPad(enderecoCondutor, " ", 25);
		numEnderecoCondutor = sys.Util.rPad(numEnderecoCondutor, " ", 5);
		complementoEnderecoCondutor = sys.Util.rPad(complementoEnderecoCondutor, " ", 11);
		cepEndereco = sys.Util.lPad(cepEndereco, "0", 8);
		municipioCondutor = sys.Util.lPad(municipioCondutor, "0", 5);
		observacao = sys.Util.rPad(observacao, " ", 51);
		identPRF = sys.Util.rPad(identPRF, " ", 9);
		desMotCanc = sys.Util.rPad(desMotCanc, " ", 70);
		
		dscLinhaLote = 
			identArquivo +
			tipoRegistro +
			username +
			codOrgaoLotacao +
			tipoTransacao +
			codOrgao +
			numAuto +
			numPlaca +
			codMarca +
			localInfracao +
			dataInfracao +
			horaInfracao +
			codMunicipio +
			codInfracao  +
			valorMulta +
			dataVencimento +
			codAgente +
			matriculaAgente +
			velocidadePermitida +
			velocidadeAferida +
			velocidadeConsiderada +
			tipoDispositivo +
			identAparelho +
			numInmetro +
			dataUltimaAfericao +
			numCertificado +
			localAparelho +
			especieVeiculo +
			categoriaVeiculo +
			tipoVeiculo +
			corVeiculo +
			nomeCondutor +
			numCpfCondutor +
			numCnhCondutor +
			uFCnhCondutor +
			enderecoCondutor +
			numEnderecoCondutor +
			complementoEnderecoCondutor +
			cepEndereco +
			municipioCondutor +
			observacao +
			identPRF +
			sys.Util.rPad("", " ", 470) +
		    desMotCanc;
		
	}
	
	
	public void setDscLinhaLoteNITP59() {
		identArquivo = sys.Util.rPad(identArquivo, " ", 4);
		tipoRegistro = sys.Util.rPad(tipoRegistro, " ", 4);
		username = sys.Util.rPad(username, " ", 20);
		codOrgaoLotacao = sys.Util.lPad(codOrgaoLotacao, "0", 6);
		tipoTransacao = sys.Util.lPad(tipoTransacao, "0", 1);
		codOrgao = sys.Util.lPad(codOrgao, "0", 6);
		numAuto = sys.Util.rPad(numAuto, " ", 12);
		numPlaca = sys.Util.rPad(numPlaca, " ", 7);
		codMarca = sys.Util.lPad(codMarca, "0", 7);
		localInfracao = sys.Util.rPad(localInfracao, " ", 80);
		dataInfracao = sys.Util.rPad(dataInfracao, " ", 8);
		horaInfracao = sys.Util.rPad(horaInfracao, " ", 4);
		codMunicipio = sys.Util.lPad(codMunicipio, "0", 5);
		codInfracao = sys.Util.rPad(codInfracao, "0", 5);
		valorMulta = sys.Util.lPad(valorMulta, "0", 7);
		dataVencimento = sys.Util.rPad(dataVencimento, " ", 8);
		codAgente = sys.Util.lPad(codAgente, "0", 2);
		matriculaAgente = sys.Util.rPad(matriculaAgente, " ", 15);
		velocidadePermitida = sys.Util.lPad(velocidadePermitida, "0", 9);
		velocidadeAferida = sys.Util.rPad(velocidadeAferida, " ", 9);
		velocidadeConsiderada = sys.Util.lPad(velocidadeConsiderada, "0", 9);
		tipoDispositivo = sys.Util.rPad(tipoDispositivo, " ", 1);
		identAparelho = sys.Util.rPad(identAparelho, " ", 30);
		numInmetro = sys.Util.rPad(numInmetro, " ", 9);
		dataUltimaAfericao = sys.Util.rPad(dataUltimaAfericao, " ", 8);		
		numCertificado = sys.Util.rPad(numCertificado, " ", 30);
		localAparelho = sys.Util.rPad(localAparelho, " ", 40);
		especieVeiculo = sys.Util.rPad(especieVeiculo, " ", 2);
		categoriaVeiculo = sys.Util.rPad(categoriaVeiculo, " ", 2);
		tipoVeiculo = sys.Util.lPad(tipoVeiculo, "0", 3);
		corVeiculo = sys.Util.lPad(corVeiculo, "0", 3);
		nomeCondutor = sys.Util.rPad(nomeCondutor, " ", 60);
		numCpfCondutor = sys.Util.lPad(numCpfCondutor, "0", 11);
		numCnhCondutor = sys.Util.lPad(numCnhCondutor, "0", 11);
		uFCnhCondutor = sys.Util.rPad(uFCnhCondutor, " ", 2);
		enderecoCondutor = sys.Util.rPad(enderecoCondutor, " ", 25);
		numEnderecoCondutor = sys.Util.rPad(numEnderecoCondutor, " ", 5);
		complementoEnderecoCondutor = sys.Util.rPad(complementoEnderecoCondutor, " ", 11);
		cepEndereco = sys.Util.lPad(cepEndereco, "0", 8);
		municipioCondutor = sys.Util.lPad(municipioCondutor, "0", 5);
		observacao = sys.Util.rPad(observacao, " ", 51);
		identPRF = sys.Util.rPad(identPRF, " ", 9);
		desInfracaoRes = sys.Util.rPad(desInfracaoRes, " ", 150);
		complObservacao = sys.Util.rPad(complObservacao, " ", 100);
		marcaModApar = sys.Util.rPad(marcaModApar, " ", 120);
		localInfrDet = sys.Util.rPad(localInfrDet, " ", 100);
		desMotCanc = sys.Util.rPad(desMotCanc, " ", 70);
		
		dscLinhaLoteNIT = 
			identArquivo +
			tipoRegistro +
			username +
			codOrgaoLotacao +
			tipoTransacao +
			codOrgao +
			numAuto +
			numPlaca +
			codMarca +
			localInfracao +
			dataInfracao +
			horaInfracao +
			codMunicipio +
			codInfracao  +
			valorMulta +
			dataVencimento +
			codAgente +
			matriculaAgente +
			velocidadePermitida +
			velocidadeAferida +
			velocidadeConsiderada +
			tipoDispositivo +
			identAparelho +
			numInmetro +
			dataUltimaAfericao +
			numCertificado +
			localAparelho +
			especieVeiculo +
			categoriaVeiculo +
			tipoVeiculo +
			corVeiculo +
			nomeCondutor +
			numCpfCondutor +
			numCnhCondutor +
			uFCnhCondutor +
			enderecoCondutor +
			numEnderecoCondutor +
			complementoEnderecoCondutor +
			cepEndereco +
			municipioCondutor +
			observacao +
			identPRF +
		    desInfracaoRes +
		    complObservacao +
		    marcaModApar +
		    localInfrDet + 
		    desMotCanc;
		
	}
	
	
	public String getDscLinhaLoteP59() {
		return this.dscLinhaLote;
	}
	
	public String getDscLinhaLoteNITP59() {
		return this.dscLinhaLoteNIT;
	}
	
	
	public String getDscLinhaLote() {
		return this.dscLinhaLote;
	}
	
	public String getDscLinhaLoteNIT() {
		return this.dscLinhaLoteNIT;
	}

	
	public void setCodArquivo(String codArquivo) {
		this.codArquivo = codArquivo;
		if (codArquivo == null)
			this.codArquivo = "";
	}
	
	public String getCodArquivo() {
		return this.codArquivo;
	}
	
	public void setCodLote(String codLote) {
		this.codLote = codLote;
		if (codLote == null)
			this.codLote = "";
	}
	
	public String getCodLote() {
		return this.codLote;
	}
	
	public void setNumSeqLinha(String numSeqLinha) {
		this.numSeqLinha = numSeqLinha;
		if (numSeqLinha == null)
			this.numSeqLinha = "";
	}
	
	public String getNumSeqLinha() {
		return this.numSeqLinha;
	}
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setErroLinhaLote(Vector erroLinhaLote) {
		this.erroLinhaLote = erroLinhaLote;
	}
	
	public Vector getErroLinhaLote() {
		return this.erroLinhaLote;
	}
	
	public void setCodRetorno(String codRetorno) {
		this.codRetorno = codRetorno;
		if (codRetorno == null)
			this.codRetorno = "";
	}
	
	public String getCodRetorno() {
		return this.codRetorno;
	}
	
	public void setDscLinhaLoteRetorno(String dscLinhaLoteRetorno) {
		this.dscLinhaLoteRetorno = dscLinhaLoteRetorno;
		if (dscLinhaLoteRetorno == null)
			this.dscLinhaLoteRetorno = "";
	}
	
	public String getDscLinhaLoteRetorno() {
		return this.dscLinhaLoteRetorno;
	}
	
	public String getLinhaRej(ArquivoDolBean arqRecebido, int seqLinha, Connection conn)
	throws sys.BeanException {
		
		String linhaRej = "";
		try {
			String seq = sys.Util.lPad(String.valueOf(seqLinha), "0", 6);
			String numAutoInf = this.getDscLinhaLote().substring(1, 13);
			String codErro = sys.Util.lPad(this.getCodRetorno(), "0", 3);
			String dscErro =
				sys.Util.rPad(
						ROB.DaoBroker.getInstance().buscarErroBroker(conn, this.getCodRetorno()),
						" ",
						50);
			linhaRej = seq + " " + numAutoInf + " " + codErro + " " + dscErro;
			
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return linhaRej;
	}
	
	
	public String buscarCodMunicipioVelho(Connection conn, String dscMunicipio) {
		
		String codMunicipio = "";
		try {
			codMunicipio = Dao.getInstance().buscarCodMunicipioVelho(conn, dscMunicipio);
			return codMunicipio;
		} catch (Exception e) {
			return codMunicipio;
		}
	}
	
	public String getIdentArquivo() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(0, 4);
		else
			return "";
	}
	
	public String getTipoRegistro() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(4, 8);
		else
			return "";
	}
	
	public String getUsername() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(8, 28);
		else
			return "";
	}
	
	public String getCodOrgaoLotacao() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(28, 34);
		else
			return "";
	}
	
	public String getTipoTransacao() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(34, 35);
		else
			return "";
	}
	
	public String getCodOrgao() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(35, 41);
		else
			return "";
	}
	
	public String getNumAuto() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(41, 53);
		else
			return "";
	}
	
	public String getNumPlaca() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(53, 60);
		else
			return "";
	}
	
	public String getCodMarca() {
		if (!dscLinhaLote.equals(""))
			return dscLinhaLote.substring(60, 67);
		else
			return "";
	}
	
	public String getLocalInfracao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(67, 147);
			else
			    return dscLinhaLote.substring(67, 112);
				
		}else
			return "";
	}
	
	
	public String getDataInfracao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
				return dscLinhaLote.substring(147, 149)
				+ "/"
				+ dscLinhaLote.substring(149, 151)
				+ "/"
				+ dscLinhaLote.substring(151, 155);
			else
				return dscLinhaLote.substring(112, 114)
				+ "/"
				+ dscLinhaLote.substring(114, 116)
				+ "/"
				+ dscLinhaLote.substring(116, 120);
		}else
			return "";
	}
	
	public String getMotivoCancelamento() {
		if (!dscLinhaLote.equals(""))
		{
			String sMotivoCancelamento=dscLinhaLote;
			if (this.ind_P59.equals("P59"))
				sMotivoCancelamento=sMotivoCancelamento.substring(1024);
			else
				sMotivoCancelamento=sMotivoCancelamento.substring(915);
				
			return sMotivoCancelamento;
		}
		else
			return "";
	}
	
	public String getHoraInfracao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
    			return dscLinhaLote.substring(155, 157) + ":" + dscLinhaLote.substring(157, 159);
			else
    			return dscLinhaLote.substring(120, 122) + ":" + dscLinhaLote.substring(122, 124);
		}else
			return "";
	}
	
	public String getCodMunicipio() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59")){
				int icodMunic = Integer.parseInt(dscLinhaLote.substring(159, 164).trim()); 
				String scodMunic = String.valueOf(icodMunic);
				if (scodMunic.equals("0")) scodMunic = "00000";
			    return scodMunic;
			}else
				return dscLinhaLote.substring(124, 128);
		}else
			return "";
	}	
	
	
	
	public String getCodInfracao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(164,169);
			else
				return dscLinhaLote.substring(128, 131);
		}else
			return "";
	}
	
	
	public String getValorMulta() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(169,176);
			else
				return dscLinhaLote.substring(132, 139);
		}else
			return "";
	}
	
	public String getDataVencimento() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(176,184);
			else
				return dscLinhaLote.substring(139, 147);
		}else
			return "";
	}
	
	
	public String getCodAgente() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(184,186);
			else
				return dscLinhaLote.substring(147, 149);
		}else
			return "";
	}
	
	public String getMatriculaAgente() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(186,201);
			else
				return dscLinhaLote.substring(149, 159);
		}else
			return "";
	}
	
	public String getVelocidadePermitida() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(201,210);
			else
				return dscLinhaLote.substring(159, 162);
		}else
			return "";
	}
	
	public String getVelocidadeAferida() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(210,219);
			else
				return dscLinhaLote.substring(162, 166);
	    }else
			return "";
	}
	
	public String getVelocidadeConsiderada() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(219,228);
			else
				return dscLinhaLote.substring(166, 170);
	    }else
			return "";
	}
	
	public String getTipoDispositivo() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(228,229);
			else
				return dscLinhaLote.substring(170, 171);
		}else
			return "";
	}
	
	public String getIdentAparelho() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(229,259);
			else
				return dscLinhaLote.substring(171, 186);
		}else
			return "";
	}
	
	public String getNumInmetro() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(259,268);
			else
				return dscLinhaLote.substring(186, 195);
		}else
			return "";
	}
	
	public String getDataUltimaAfericao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(268,276);
			else
				return dscLinhaLote.substring(195, 203);
		}else
			return "";
	}
	
//	
	public String getNumCertificado() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(276,306);
			else
				return dscLinhaLote.substring(203, 213);
		}else
			return "";
	}
	
	public String getLocalAparelho() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(306,346);
			else
				return dscLinhaLote.substring(213, 253);
		}else
			return "";
	}
	
	public String getEspecieVeiculo() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(346,348);
			else
				return dscLinhaLote.substring(253, 255);
		}else
			return "";
	}
	
	public String getCategoriaVeiculo() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(348,350);
			else
				return dscLinhaLote.substring(255, 257);
		}else
			return "";
	}
	
	public String getTipoVeiculo() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(350,353);
			else
				return dscLinhaLote.substring(257, 260);
		}else
			return "";
	}
	
	public String getCorVeiculo() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(353,356);
			else
				return dscLinhaLote.substring(260, 263);
		}else
			return "";
	}
	
	public String getNomeCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(356,416);
			else
				return dscLinhaLote.substring(263, 308);
		}else
			return "";
	}
	
	public String getNumCpfCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(416,427);
			else
				return dscLinhaLote.substring(308, 319);
		}else
			return "";
	}
	
	public String getNumCnhCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(427,438);
			else
				return dscLinhaLote.substring(319, 330);
		}else
			return "";
	}
	
	public String getUFCnhCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(438,440);
			else
				return dscLinhaLote.substring(330, 332);
		}else
			return "";
	}
	
	public String getEnderecoCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(440,465);
			else
				return dscLinhaLote.substring(332, 357);
		}else
			return "";
	}
	
	public String getNumEnderecoCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(465,470);
			else
				return dscLinhaLote.substring(357, 362);
		}else
			return "";
	}
	
	public String getComplementoEnderecoCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(470,481);
			else
				return dscLinhaLote.substring(362, 373);
		}else
			return "";
	}
	
	public String getCepEndereco() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(481,489);
			else
				return dscLinhaLote.substring(373, 381);
		}else
			return "";
	}
	
    public String getMunicipioCondutor() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59")){
				int icodMunic = Integer.parseInt(dscLinhaLote.substring(489,494).trim()); 
				String scodMunic = String.valueOf(icodMunic);
				if (scodMunic.equals("0")) scodMunic = "00000";
			    return scodMunic;
			}else
				return dscLinhaLote.substring(381, 385);
		}else
			return "";
	}
	
	public String getObservacao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(494,545);
			else
				return dscLinhaLote.substring(385, 436);
		}else
			return "";
	}
	
	public String getIdentPRF() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(545,554);
			else
				return dscLinhaLote.substring(436, 445);
		}else
			return "";
	}
	
	public String getIdentPRFEdt()  {
		String identPRF = ""; 

		if (this.ind_P59.equals("P59"))
			identPRF = dscLinhaLote.substring(545,554);
		else
			identPRF = dscLinhaLote.substring(436, 445);
		
		
		if (identPRF.length() < 9) 
			return "";
		
		String identPRFEdt = "";
		
		if (identPRF.substring(0,1).equals("T"))
			identPRFEdt = "NIT" + identPRF.substring(1,2) + sys.Util.lPad(identPRF.substring(2, 9), "0", 8);
		else if (identPRF.substring(0,1).equals("E"))
			identPRFEdt = "NE" + identPRF.substring(1,2) + "-" + sys.Util.lPad(identPRF.substring(2, 9), "0", 7);
		
		return identPRFEdt;
	}	
	
	public String getDesInfracaoRes() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(554,704);
			else
				return dscLinhaLote.substring(445, 595);
		}else
			return "";
	}

	public String getComplObservacao() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(704,804);
			else
				return dscLinhaLote.substring(595, 695);
		}else
			return "";
	}
	
	
	public String getMarcaModApar() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(804,924);
			else
				return dscLinhaLote.substring(695, 815);
		}else
			return "";
	}

	public String getLocalInfrDet() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(924,1024);
			else
				return dscLinhaLote.substring(815, 915);
		}else
			return "";
	}

	public String getDesMotCanc() {
		if (!dscLinhaLote.equals("")){
			if (this.ind_P59.equals("P59"))
			    return dscLinhaLote.substring(1024,1094);
			else
			    return dscLinhaLote.substring(915, 985);
		}else
			return "";
	}
	
	public String getDscErro() {
		if (dscErro == null)
			return "";
		if (dscErro.equals("ERR"))
			return "Timeout";
		return dscErro;
	}
	
	public void setDscErro(String string) {
		dscErro = string;
	}
	
	public void setCategoriaVeiculo(String string) {
		categoriaVeiculo = string;
	}
	
	public void setCepEndereco(String string) {
		cepEndereco = string;
	}
	
	public void setCodAgente(String string) {
		codAgente = string;
	}
	
	public void setCodInfracao(String string) {
		codInfracao = string;
	}
	
	public void setCodMunicipio(String string) {
		String scodMunic = "";
		if (this.ind_P59.equals("P59"))
			scodMunic = sys.Util.lPad(string,"0",5);
		else
			scodMunic = sys.Util.lPad(string,"0",4);
		codMunicipio = scodMunic;
	}
	
	public void setCodOrgao(String string) {
		codOrgao = string;
	}
	
	public void setCodOrgaoLotacao(String string) {
		codOrgaoLotacao = string;
	}
	
	public void setComplementoEnderecoCondutor(String string) {
		complementoEnderecoCondutor = string;
	}
	
	public void setCorVeiculo(String string) {
		corVeiculo = string;
	}
	
	public void setDataInfracao(String string) {
		dataInfracao = string;
	}
	
	public void setEnderecoCondutor(String string) {
		enderecoCondutor = string;
	}
	
	public void setEspecieVeiculo(String string) {
		especieVeiculo = string;
	}
	
	public void setHoraInfracao(String string) {
		horaInfracao = string;
	}
	
	public void setIdentArquivo(String string) {
		identArquivo = string;
	}
	
	public void setLocalInfracao(String string) {
		localInfracao = string;
	}
	
	public void setMunicipioCondutor(String string) {
		String scodMunic = "";
		if (this.ind_P59.equals("P59"))
			scodMunic = sys.Util.lPad(string,"0",5);
		else
			scodMunic = sys.Util.lPad(string,"0",4);
		municipioCondutor = scodMunic;
	}
	
	public void setNomeCondutor(String string) {
		nomeCondutor = string;
	}
	
	public void setNumAuto(String string) {
		numAuto = string;
	}
	
	public void setNumCnhCondutor(String string) {
		numCnhCondutor = string;
	}
	
	public void setNumCpfCondutor(String string) {
		numCpfCondutor = string;
	}
	
	public void setNumEnderecoCondutor(String string) {
		numEnderecoCondutor = string;
	}
	
	public void setNumPlaca(String string) {
		numPlaca = string;
	}
	
	public void setObservacao(String string) {
		observacao = string;
	}
	
	public void setComplObservacao(String string) {
		complObservacao = string;
	}

	public void setTipoRegistro(String string) {
		tipoRegistro = string;
	}
	
	public void setTipoTransacao(String string) {
		tipoTransacao = string;
	}
	
	public void setTipoVeiculo(String string) {
		tipoVeiculo = string;
	}
	
	public void setUFCnhCondutor(String string) {
		uFCnhCondutor = string;
	}
	
	public void setUsername(String string) {
		username = string;
	}
	
	public void setCodMarca(String string) {
		codMarca = string;
	}
	
	public void setDataVencimento(String string) {
		dataVencimento = string;
	}
	
	public void setIdentAparelho(String string) {
		identAparelho = string;
	}
	
	public void setIdentPRF(String string) {
		identPRF = string;
	}

	public void setDesInfracaoRes(String string) {
		desInfracaoRes = string;
	}
	
	public void setMarcaModApar(String string) {
		marcaModApar = string;
	}

	public void setLocalInfrDet(String string) {
		localInfrDet = string;
	}

	public void setDesMotCanc(String string) {
		desMotCanc = string;
	}
	
	public void setMatriculaAgente(String string) {
		matriculaAgente = string;
	}
	
	public void setNumCertificado(String string) {
		numCertificado = string;
	}
	
	public void setNumInmetro(String string) {
		numInmetro = string;
	}
	
	public void setTipoDispositivo(String string) {
		tipoDispositivo = string;
	}
	
	public void setValorMulta(String string) {
		valorMulta = string;
	}
	
	public void setVelocidadeAferida(String string) {
		velocidadeAferida = string;
	}
	
	public void setVelocidadeConsiderada(String string) {
		velocidadeConsiderada = string;
	}
	
	public void setVelocidadePermitida(String string) {
		velocidadePermitida = string;
	}
	
	public void setLocalAparelho(String string) {
		localAparelho = string;
	}
	
	public void setDataUltimaAfericao(String string) {
		dataUltimaAfericao = string;
	}
	
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	public Connection getConn() 	{
		return conn;
	}
	
	
	public void setInd_P59(String ind_P59) {
		this.ind_P59 = ind_P59;
		if (ind_P59 == null)
			this.ind_P59 = "";
	}
	
	public String getInd_P59() {
		return this.ind_P59;
	}
	
	
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LinhaLoteUpdateDol(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o atualizada: " + this.getCodLinhaLote() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}
	
	public boolean logar() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LogarLinhaLoteDol(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o logada: " + this.getCodLinhaLote() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}
	
	public void gravarAutoInfracao(ArquivoDolBean arquivo) throws BeanException {
		try {			
			AutoInfracaoBean autoBean = new AutoInfracaoBean();
			UsuarioBean usuario = new UsuarioBean();
			usuario.setNomUserName(arquivo.getNomUsername());
			usuario.getOrgao().setCodOrgao(arquivo.getCodOrgaoLotacao());
			usuario.setCodOrgaoAtuacao("999999");
			String numAutoInfracao = this.dscLinhaLote.substring(41, 53).trim();
			autoBean.setNumAutoInfracao(numAutoInfracao);
			String numPlaca = this.dscLinhaLote.substring(53, 60);
			autoBean.setNumPlaca(numPlaca);
			autoBean.LeAutoInfracao("auto", usuario);
			
            /*Data de Registro*/
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			autoBean.setDatRegistro(df.format(new Date()));
			
			/*Dados de Niteroi - DOL*/
			if (arquivo.getCodOrgao().equals("258650")) { //Layout de Niteroi
				String linha = "";
				String identOrgao = "";  
				String resumoInfracao = ""; 
				String marcaModeloAparelho = ""; 
				String localInfracaoNotificacao = "";
				
				if (arquivo.getDscPortaria59().equals("P59")){
					linha = sys.Util.rPad(this.dscLinhaLoteNIT, " ", 1094);
					identOrgao = linha.substring(545,554);  
					resumoInfracao = linha.substring(554,704); 
					marcaModeloAparelho = linha.substring(804,924); 
					localInfracaoNotificacao = linha.substring(924,1024);
				}else{
					linha = sys.Util.rPad(this.dscLinhaLoteNIT, " ", 1000);
					identOrgao = linha.substring(436, 445);  
					resumoInfracao = linha.substring(445, 595); 
					marcaModeloAparelho = linha.substring(695, 815); 
					localInfracaoNotificacao = linha.substring(815, 915);
					
				}
				
				autoBean.setNumAutoInfracao(numAutoInfracao.trim());
				autoBean.setIdentOrgao(identOrgao);
				autoBean.setDscResumoInfracao(resumoInfracao.trim());
				autoBean.setDscMarcaModeloAparelho(marcaModeloAparelho.trim());
				autoBean.setDscLocalInfracaoNotificacao(localInfracaoNotificacao.trim());
			}				
			autoBean.GravaAutoInfracaoLocal();
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}	

	public boolean isUpdate(String Origem) {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LinhaLoteUpdateDol(this,Origem) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o atualizada: " + this.getCodLinhaLote() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}	
	
}