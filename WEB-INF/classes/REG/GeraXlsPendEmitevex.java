package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class GeraXlsPendEmitevex extends HttpServlet {
	
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
	{
		String datIniRel ="";
		String datFimRel ="";
		String nextRetorno = "/REG/acompPendEmitevex.jsp";
		String JSPDIR   = "/WEB-INF/jsp/";
		HttpSession session   = req.getSession() ;		
				        
		try {
             ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) req.getSession().getAttribute("UsuarioBeanId");
             //ParamSistemaBean paramSys = (ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId");
			 acompPendEmitevexBean acompPendEmitevexBeanId = (acompPendEmitevexBean) req.getSession().getAttribute("acompPendEmitevexBeanId");
			if( acompPendEmitevexBeanId == null ) acompPendEmitevexBeanId = new acompPendEmitevexBean();
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             String contexto = req.getParameter("contexto");
             if( contexto == null ) contexto = "";
             
             String nomArquivo = req.getParameter("nomArquivo");
             if( nomArquivo == null ) nomArquivo = "";
            
             if ( acao.equals("") ) {

				datFimRel = sys.Util.formatedToday().substring(0,10); 
				Calendar cal = new GregorianCalendar();
				cal.setTime( new Date() );
				cal.add(Calendar.MONTH,0);
				Date date = cal.getTime();
				cal.setTime(date);
				String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
				String mes = cal.get(Calendar.MONTH)+ 1 + "";
				if(mes.length() < 2) mes = "0" + mes;  
				String ano = cal.get(Calendar.YEAR) + "";
				datIniRel = dia +"/"+ mes +"/"+ ano;     
            	
				acompPendEmitevexBeanId.setDatIni( datIniRel );
            	acompPendEmitevexBeanId.setDatFim( datFimRel );
                req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
            }
            else if(acao.equals("GeraExcel")) 
			{
            	String codArquivo = req.getParameter("codigoArquivo");	
				String detalhe = req.getParameter("detalhe");						
				String sentido = req.getParameter("sentido");
				String codOrgao = req.getParameter("codOrgao");
				if (codOrgao == null) codOrgao = "0";
				String todosOrgaos = req.getParameter("todosOrgaos");
				String prazoDigitado = req.getParameter("prazoDigitado");
				if (prazoDigitado == null) prazoDigitado = "0";
				String statusAuto = req.getParameter("codStatusAuto");
				if (statusAuto == null) statusAuto = req.getParameter("statusAuto");
				
				String totalMesAno = req.getParameter("totalMesAno");
				
				String primeiroRegistro = req.getParameter("primeiroRegistro");
				if (primeiroRegistro == null) primeiroRegistro = "";
				
				String primeiroAnterior = req.getParameter("primeiroAnterior");
				if (primeiroAnterior == null) primeiroAnterior = "";
				
				String ultimoRegistro = req.getParameter("ultimoRegistro");
				if (ultimoRegistro == null) ultimoRegistro = "";
				
				String numPag = req.getParameter("numPag");
				if (numPag == null) numPag = "0";
				
				String[] todosOrgaosCod = req.getParameterValues("todosOrgaosCod");
				int prazo = Integer.parseInt(prazoDigitado);
				
				String seq = req.getParameter("seq");
			
				// Verifica se a consulta ser� feita para todos os �rg�os						
				
				
				try{			
					Dao dao = Dao.getInstance();
					NotifControleBean notifControle = (NotifControleBean) req.getSession().getAttribute("NotifControleId");
					if( notifControle == null ) notifControle = new NotifControleBean();							
					
					ParamSistemaBean parametro = (ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");
					String rowNum = parametro.getParamSist("LIMITE_REL_NOTIFICACOES");
					String qtdReg = rowNum;
					String rowNumImp = "";
					
					if (sentido == null) 
						sentido = "Prox";
					
					HashMap proxAnt = new HashMap();
					if(sentido.equals("Prox"))
					{
						proxAnt.put("primeiroRegistro", primeiroRegistro);
						proxAnt.put("ultimoRegistro", ultimoRegistro);
						numPag = Integer.toString(Integer.parseInt(numPag)+1);
						if (seq == null) 
							seq = "1";
						else
					        seq = Integer.toString(Integer.parseInt(seq)+Integer.parseInt(qtdReg));
					}
					else
					{
						proxAnt.put("primeiroRegistro", ""); 
						proxAnt.put("ultimoRegistro", "");
						numPag = Integer.toString(Integer.parseInt(numPag)-1);
						seq = Integer.toString(Integer.parseInt(seq)-Integer.parseInt(qtdReg));
					}
					proxAnt.put("sentido", sentido);
					proxAnt.put("primeiroAnterior", primeiroAnterior);
					proxAnt.put("sentido", sentido);
					
					/*15/08/2007*/
					if(!"".equals(codArquivo)){
						if(dao.getacompPendEmitevexXls(notifControle, rowNum, proxAnt, rowNumImp,codArquivo).size()>0){;							
						notifControle.setCodArquivoEnvioOrig(codArquivo);
						
						acompPendEmitevexBeanId.setDatIni( datIniRel );
						acompPendEmitevexBeanId.setDatFim( datFimRel );
						acompPendEmitevexXls geraxls = new acompPendEmitevexXls();
		            	String tituloConsulta = "RELAT�RIO -PEND�NCIAS DO EMITEVEX ";
		            	ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
		            	paramSys.setCodSistema("40"); // M�dulo GER
		            	paramSys.PreparaParam();
		            	String arquivo =  "c:"+ "/" + nomArquivo;// paramSys.getParamSist(contexto) + "/" + nomArquivo;
						geraxls.write(notifControle, tituloConsulta, arquivo);
						
						req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
						rd.forward(req, resp);	
						}
						else {
							  acompPendEmitevexBeanId.setDatIni( datIniRel );
							  acompPendEmitevexBeanId.setDatFim( datFimRel );
							  req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
							  RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + nextRetorno);
							  rd.forward(req, resp);
						}							
					}else{
						if(dao.getacompPendEmitevexXlsTodos(notifControle).size()>0){;				
						
						acompPendEmitevexBeanId.setDatIni( datIniRel );
						acompPendEmitevexBeanId.setDatFim( datFimRel );
						acompPendEmitevexXls geraxls = new acompPendEmitevexXls();
		            	String tituloConsulta = "RELAT�RIO -PEND�NCIAS DO EMITEVEX ";
		            	ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
		            	paramSys.setCodSistema("40"); // M�dulo GER
		            	paramSys.PreparaParam();
		            	String arquivo = "c:"+ "/" + nomArquivo;// paramSys.getParamSist(contexto) + "/" + nomArquivo;
						geraxls.write(notifControle, tituloConsulta, arquivo);
						
						req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
						RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
						rd.forward(req, resp);	
						}
						else {
							  acompPendEmitevexBeanId.setDatIni( datIniRel );
							  acompPendEmitevexBeanId.setDatFim( datFimRel );
							  req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
							  RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + nextRetorno);
							  rd.forward(req, resp);
						}							
						
					}
						
						
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";//cal.get(Calendar.DAY_OF_MONTH) + "";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano;     
					
					acompPendEmitevexBeanId.setDatIni( datIniRel );
					acompPendEmitevexBeanId.setDatFim( datFimRel );
					nextRetorno = "/REG/acompPendEmitevex.jsp";
					req.setAttribute("acompPendEmitevexBeanId", acompPendEmitevexBeanId);
					
					if (sentido.equals("Imprimir"))
					{
						nextRetorno = "/REG/NotifDetalheLinhaImp.jsp";
						String tituloConsulta = "RELAT�RIO - ACOMPANHAMENTO DE PEND�NCIAS - EMITEVEX  ";
						rowNumImp = "-1";	
						dao.getacompPendEmitevex(notifControle, rowNum, proxAnt, rowNumImp,codArquivo);
						req.setAttribute("tituloConsulta", tituloConsulta);
					}
				
					
		
			}catch(Exception e){
				throw new sys.CommandException(e.getMessage());	
			}			
				
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());            
		} 
	}
  }
}
