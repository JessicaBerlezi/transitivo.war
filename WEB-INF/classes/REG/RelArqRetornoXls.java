package REG;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Iterator;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.format.UnderlineStyle;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class RelArqRetornoXls {
	
	public void write(RelatArqRetornoBean RelArqDownlBeanId, String titulo, String arquivo) throws IOException, WriteException
	{
	
		int cont = 3;
		int seq = 1;
		int celulaTitulo = 0;
		String valorFormatado1="",valorFormatado2="";
		String valor1="",valor2="";
		
		try
		{
			WritableWorkbook workbook = Workbook.createWorkbook(new File(arquivo));
			
			WritableSheet sheet = workbook.createSheet("Relat�rio de Retornos", 0);
			
			WritableFont fonteTitulo = new WritableFont(WritableFont.ARIAL, 
                    WritableFont.DEFAULT_POINT_SIZE,
                    WritableFont.BOLD,
                    false,
                    UnderlineStyle.NO_UNDERLINE,
                    Colour.BLACK);
			
			WritableCellFormat formatoTituloCampos = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTitulo = new WritableCellFormat(fonteTitulo);
			WritableCellFormat formatoTotal = new WritableCellFormat(fonteTitulo);
			formatoTituloCampos.setAlignment(Alignment.CENTRE);
			formatoTituloCampos.setBackground(Colour.GRAY_25);
			formatoTitulo.setAlignment(Alignment.CENTRE);
			
			celulaTitulo = 6/2;
			
			Label label = new Label(celulaTitulo, 0, titulo, formatoTitulo);
			sheet.addCell(label);
			
			label = new Label(0, 2, "SEQ", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(0, 5);
			
			label = new Label(1, 2, "ARQUIVO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(1, 30);
			
			label = new Label(2, 2, "DATA ENVIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(2, 20);
			
			label = new Label(3, 2, "QTD ENV.", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(3, 10);
			
			label = new Label(4, 2, "USU�RIO ENVIO", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(4, 20);
			
			label = new Label(5, 2, "QTD ATUALIZADA", formatoTituloCampos); 
			sheet.addCell(label);
			sheet.setColumnView(5, 20);
			
			
			Iterator it = RelArqDownlBeanId.getListaArqs().iterator();
			RelatArqRetornoBean relatorioImp  = new RelatArqRetornoBean();
			if( RelArqDownlBeanId.getListaArqs().size()>0 ) { 
				while (it.hasNext()) {
					   relatorioImp = (RelatArqRetornoBean)it.next() ;
					   
					   label = new Label(0, cont, String.valueOf(seq)); 
					   sheet.addCell(label);
					   
					   label = new Label(1, cont, relatorioImp.getNomArquivo()); 
					   sheet.addCell(label);
					   
					   label = new Label(2, cont, relatorioImp.getDatRecebimento()); 
					   sheet.addCell(label);
					   
					   label = new Label(3, cont, relatorioImp.getQtdReg()); 
					   sheet.addCell(label);
					   
					   label = new Label(4, cont, relatorioImp.getNomUsername()); 
					   sheet.addCell(label);
					   
					   label = new Label(5, cont, relatorioImp.getQtdRecebido()); 
					   sheet.addCell(label);
					   
					   
					   cont++;
					   seq++;
				}
			}
			
			label = new Label(0, cont, "Total:", formatoTotal); 
			sheet.addCell(label);
			
			valor1 = RelArqDownlBeanId.getTotalQtdReg();
			if( (valor1 != null)||(valor1.equals("")) )  {
				DecimalFormat formato = new DecimalFormat(",##0") ;
				valorFormatado1  = formato.format(Integer.parseInt(valor1)); 
			}else valorFormatado1 = "0";
			
			label = new Label(3, cont, valorFormatado1, formatoTotal); 
			sheet.addCell(label);
			
			valor2 = RelArqDownlBeanId.getTotalQtdRecebido();
			if( (valor2 != null)||(valor2.equals("")) )  {
				DecimalFormat formato = new DecimalFormat(",##0") ;
				valorFormatado2  = formato.format(Integer.parseInt(valor2)); 
			}else valorFormatado2 = "0";
			
			label = new Label(5, cont, valorFormatado2, formatoTotal); 
			sheet.addCell(label);
			
			workbook.write(); 
			workbook.close();
			
			//System.out.println("ARQUIVO GERADO COM SUCESSO");
			
		}catch (Exception e) {	
			throw new IOException("GeraXls: " + e.getMessage());	}
	
	}

}
