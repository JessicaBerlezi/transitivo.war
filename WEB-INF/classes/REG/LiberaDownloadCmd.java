package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Libera o Download EMITE PERRONE e EMITEPONTO PERRONE<br>
 * <b>Description:</b>  Comando para Liberar Download de arquivos EMITE PERRONE e EMITEPONTO PERRONE<br>
 * <b>Copyright:</b>    Copyright (c) 2012<br>
 * <b>Company: </b> 	Facility Staff <br>
 * @author 			   	Rodrigo Cunha	
 * @version 1.0
 */

public class LiberaDownloadCmd extends sys.Command {
	
	private static final String jspPadrao="/REG/liberaDownload.jsp";    
	private String next;
	
	public LiberaDownloadCmd() {
		next = jspPadrao;
	}
	
	public LiberaDownloadCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		
		String nextRetorno = jspPadrao ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
						
			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
			
			//cria o Bean para guardar os parametros da consulta			
			ConsultaDownloadBean ConsultaDownloadBeanId = (ConsultaDownloadBean) req.getAttribute("ConsultaDownloadBeanId");
			if (ConsultaDownloadBeanId == null) ConsultaDownloadBeanId = new ConsultaDownloadBean();
			
			String sigFuncao = usrFunc.getJ_sigFuncao();
			Vector vetArquivos 	= new Vector();
			String Origem ="";
			String acao = req.getParameter("acao");  
			if(acao==null) acao = "";
						
			if (acao.equals("") || acao.equals("buscaDownloads")) {
				if (req.getParameter("De")!=null)
					ConsultaDownloadBeanId.setDataIni(req.getParameter("De"));
				if(req.getParameter("Ate")!=null)
					ConsultaDownloadBeanId.setDataFim(req.getParameter("Ate"));					
				
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setArqFuncao(sigFuncao);
				
				if("REG3020".equals(sigFuncao)){//EMITEPONTO PERRONE
						Dao dao = Dao.getInstance();
						vetArquivos=dao.BuscaArquivosProcessadosPontuacao(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
								getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
						
				}else{
					Dao dao = Dao.getInstance();
					vetArquivos=dao.BuscaArquivosProcessados(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
							getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
					
				}
			}
			if (acao.equals("liberaDownloads")){
				
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setArqFuncao(sigFuncao);
				
				String[] codigoArquivosSelecionados = req.getParameterValues("codArquivos");
				for (String codArquivo : codigoArquivosSelecionados){
					
					Dao dao = Dao.getInstance();
					boolean liberou = dao.liberaDownloadArquivoReg(codArquivo);
					if (liberou)
						ConsultaDownloadBeanId.setMsgErro("Arquivos liberados com sucesso!");
					else
						ConsultaDownloadBeanId.setMsgErro("Arquivos n�o liberados! Favor tente novamente, caso o erro persista entre em contato com o suporte.");
					
					vetArquivos=dao.BuscaArquivosProcessados(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
							getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
					
				}
				
			}
			req.setAttribute("ConsultaDownloadBeanId",ConsultaDownloadBeanId);
			req.setAttribute("msgErro",msgErro);						
			req.setAttribute("vetArquivos",vetArquivos);
			req.setAttribute("sigFuncao",sigFuncao);	
			
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}
