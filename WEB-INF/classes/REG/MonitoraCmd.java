package REG;

import javax.servlet.http.HttpServletRequest;

import sys.BeanException;

/**
* <b>Title:</b>        Consulta Monitor - Command<br>
* <b>Description:</b>  Permite o monitoramento dos Robot<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/
public class MonitoraCmd extends sys.Command {
	private static final String jspPadrao = "/REG/Monitora.jsp";
	private String next;

	public MonitoraCmd() {
		next = jspPadrao;
	}

	public MonitoraCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {	
			//Cria  Bean, se n�o existir
			ACSS.OrgaoBean orgaoId = (ACSS.OrgaoBean)req.getAttribute("orgaoId");
			if (orgaoId == null) orgaoId = new ACSS.OrgaoBean();

			MonitoraBean monitorId = (MonitoraBean) req.getAttribute("monitorId");
			if (monitorId == null) monitorId = new MonitoraBean();
			
			//Obter e validar os paramentros recebidos
			String acao = req.getParameter("acao");
			if (acao == null) acao = ""; 
			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)atualizarDependente = "N";
			
			//Recebe parametros das combos
			String codMonitor = req.getParameter("codMonitor");
			if((codMonitor == null)||(codMonitor.equals(""))) codMonitor = "0";
			String codOrgao = req.getParameter("codOrgao");
			if((codOrgao == null)||(codOrgao.equals(""))) codOrgao = "0";
			String robot = req.getParameter("robot");
			if (robot == null) robot = "";
			
			//Recebe parametros de datas
			String dataDe = req.getParameter("edtDe");
			if (dataDe == null) dataDe = "";
			String dataAte = req.getParameter("edtAte");
			if (dataAte == null) dataAte = "";
			
			//Recebe parametros do Robot e arquivo0
			String nomProcesso = req.getParameter("nomProcesso");
			if (nomProcesso == null) nomProcesso = "";
			
			
			//Recebe parametro para ordena��o 
			String ordem = req.getParameter("ordem");
			if (ordem == null) ordem = "";	

				
			//Valores atribuidos para as inputs disable	
			monitorId.setCodMonitor(codMonitor);
			monitorId.setCodOrgao(codOrgao);
			monitorId.setNomProcesso(nomProcesso);
			monitorId.setDatIni(dataDe);
			monitorId.setDatFim(dataAte);
			
			if(acao.equals("busca")){
				orgaoId.Le_Orgao(codOrgao,0);
				if (!codOrgao.equals("0")) monitorId.setSigOrgao(orgaoId.getSigOrgao()); 
				
				if((monitorId.consultaMonitor(monitorId, ordem))==false)
				    monitorId.setMsgErro("N�o existe registros gravados neste per�odo.");
			}
			req.setAttribute("monitorId", monitorId);	
			req.setAttribute("orgaoId", orgaoId);	
			req.setAttribute("atualizarDependente", atualizarDependente);
		}
		catch (BeanException e) {
			throw new sys.CommandException("MonitoraCmd 001: " + e.getMessage());
		}
		return nextRetorno;
		
	}
}
