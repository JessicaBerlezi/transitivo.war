package REG;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class GeraXlsRelatArqEntrega extends HttpServlet {
	
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException { 
	{
		String datIniRel ="";
		String datFimRel ="";
		String jspPadrao = "/REG/RelatArqEntrega.jsp";
		String JSPDIR   = "/WEB-INF/jsp/";
				        
		try {
			RelatArqEntregaBean RelArqDownlBeanId = new RelatArqEntregaBean();
			
			HttpSession session = req.getSession();
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId") ;
			 
             String acao = req.getParameter("acao");
             if( acao == null ) acao = "";
             
             String contexto = req.getParameter("contexto");
             if( contexto == null ) contexto = "";
             
             String nomArquivo = req.getParameter("nomArquivo");
             if( nomArquivo == null ) nomArquivo = "";
            
             if ( acao.equals("") ) {

            	 datFimRel = sys.Util.formatedToday().substring(0,10); 
 				Calendar cal = new GregorianCalendar();
 				cal.setTime( new Date() );
 				cal.add(Calendar.MONTH,0);
 				Date date = cal.getTime();
 				cal.setTime(date);
 				String dia = "01";
 				String mes = cal.get(Calendar.MONTH)+ 1 + "";
 				if(mes.length() < 2) mes = "0" + mes;  
 				String ano = cal.get(Calendar.YEAR) + "";
 				datIniRel = dia +"/"+ mes +"/"+ ano;     
 				
 				RelArqDownlBeanId.setDatInicial(datIniRel);
 				RelArqDownlBeanId.setDatFinal(datFimRel);
 				req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
            }
            else if(acao.equals("GeraExcel")) 
			{
				Vector vErro = new Vector();
				
				datIniRel = req.getParameter("dataIni");
				datFimRel = req.getParameter("dataFim");
				
				String todosOrgaos = req.getParameter("todosOrgaos");				
				String codOrgao = req.getParameter("codOrgao");
				
				ACSS.OrgaoBean orgao = new ACSS.OrgaoBean();
				// Verifica se a consulta ser� feita para todos os �rg�os
				if (todosOrgaos != null) {										
					orgao.setSigOrgao("TODOS");
					RelArqDownlBeanId.getSistema().setCodSistema(UsuarioFuncBeanId.getCodSistema());
					RelArqDownlBeanId.getUsuario().setCodUsuario(UsuarioFuncBeanId.getCodUsuario());
				
				} else {
					orgao.Le_Orgao(codOrgao,0);
					RelArqDownlBeanId.setOrgao(orgao);
				}

				if( (datIniRel==null || datFimRel == null)||(datIniRel.equals("")||datFimRel.equals("")) )
				{ 
					datFimRel = sys.Util.formatedToday().substring(0,10); 
					Calendar cal = new GregorianCalendar();
					cal.setTime( new Date() );
					cal.add(Calendar.MONTH,0);
					Date date = cal.getTime();
					cal.setTime(date);
					String dia = "01";
					String mes = cal.get(Calendar.MONTH)+ 1 + "";
					if(mes.length() < 2) mes = "0" + mes;  
					String ano = cal.get(Calendar.YEAR) + "";
					datIniRel = dia +"/"+ mes +"/"+ ano; 
				} 
				
				RelArqDownlBeanId.setDatInicial(datIniRel);
				RelArqDownlBeanId.setDatFinal(datFimRel);				
				
				if( RelArqDownlBeanId.consultaRelEntrega(vErro)) {
					String tituloConsulta = "RELAT�RIO DE ENTREGA : "+datIniRel+" a "+datFimRel+"  -  ORG�O: "
						+ orgao.getSigOrgao();
					req.setAttribute("tituloConsulta", tituloConsulta);
					
					RelatArqEntregaXls geraxls = new RelatArqEntregaXls();
					
					ACSS.ParamSistemaBean paramSys = new ACSS.ParamSistemaBean();
	            	paramSys.setCodSistema("24"); // M�dulo GER
	            	paramSys.PreparaParam();
	            	String arquivo = paramSys.getParamSist(contexto) + "/" + nomArquivo;
					geraxls.write(RelArqDownlBeanId, tituloConsulta, arquivo);
					
					req.getSession().setAttribute("ParamSistemaBeanId", paramSys);
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/download");
					rd.forward(req, resp);
					
				} else {
					RelArqDownlBeanId.setDatInicial(datIniRel);
					RelArqDownlBeanId.setDatFinal(datFimRel);
					req.setAttribute("RelArqDownlBeanId", RelArqDownlBeanId);
					RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + jspPadrao);
					rd.forward(req, resp);
				}
		
			}
		} catch (Exception e) {
			throw new ServletException(e.getMessage());            
		} 
	}
  }
}
