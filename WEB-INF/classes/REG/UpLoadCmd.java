package REG;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;

/**
 * <b>Title:</b>        SMIT - Upload de Arquivos de Multas<br>
 * <b>Description:</b>  Comando para Upload de Arquivos de Multas de Tr�nsito<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luiz Medronho
 * @version 1.0
 */

public class UpLoadCmd extends sys.Command {
	
	private static final String jspPadrao = "/REG/uploadBase.jsp";	
	public UpLoadCmd()  {
		super();
	}
	
	public String execute(HttpServletRequest req, sys.UploadFile upload)
	throws sys.CommandException {
		
 		String nextRetorno = jspPadrao;
		try {
			nextRetorno = jspPadrao;
			String acao = upload.getParameter("acao");
			if (acao == null) acao = "";
			
			HttpSession session = req.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();					
			
			ACSS.ParamSistemaBean parSis = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ACSS.ParamSistemaBean();
			
			String codOrgaoAtuacao = sys.Util.lPad(usrLogado.getCodOrgaoAtuacao(), "0", 6);
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId == null)  ParamOrgaoId = new REC.ParamOrgBean() ;	  			

			
			String orgContSetor = ParamOrgaoId.getParamOrgao("ORG_CONTROLA_SETOR","N","10");

			
			REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			arqRec.setArqFuncao(upload.getParameter("j_sigFuncao"));
			
			/*Caso �rgao Atua��o igual PMN, n�o associar 'PROXIMO ARQUIVO'*/
			boolean bCalcProximoArquivo=true;
			/*
			if ( (codOrgaoAtuacao.equals("258650")) && (arqRec.getArqFuncao().equals("MR")) )
			{
				File file = upload.getFile("arqUpload");				
                String sNome=file.getName().toUpperCase();
                if (sNome.trim().length()>0)
                {
                	String sPrefixo=sNome.substring(0,2);
                	if ("'MR','PE'".indexOf(sPrefixo)>=0)
                	{
                	  arqRec.set_ArqFuncao(sPrefixo);
					  arqRec.calcProximoArquivo(ParamOrgaoId,codOrgaoAtuacao);
                	}
                }
				bCalcProximoArquivo=false;
			}
			*/
			
			if (orgContSetor.equals("S")){
				arqRec.setProximoArquivo(upload.getParameter("proximoArquivo").toUpperCase());
			}else{
				if (bCalcProximoArquivo)
				{
					if (arqRec.getTamNumControle()>0) {					
						arqRec.calcProximoArquivo(ParamOrgaoId,codOrgaoAtuacao);
						if ("".equals(acao.toUpperCase())) {
							req.setAttribute("proximoArquivo",arqRec.getProximoArquivo());
						}
					}
				}
			}

			if ("UPLOAD".equals(acao.toUpperCase())) {
				BufferedReader br = null;				
				nextRetorno = "/REG/ProtocoloRecebimentoArq.jsp";
				//Criticar os arquivos enviados
				try {
					File file = upload.getFile(req,"arqUpload");
					br = new BufferedReader(new FileReader(file));
					String auxLinha ;
					List lstLinhas = new ArrayList();
					int prim = 0;
					while ((auxLinha = br.readLine()) != null) {
						auxLinha = auxLinha.replaceAll("'", " ");
						if (prim == 0) {
							prim = 1;                            
							
							arqRec.setNomArquivo(file.getName().toUpperCase().replace(".TXT", "")); 
							arqRec.setProximoArquivo(arqRec.getNomArquivo().replace(".TXT", ""));
							if (arqRec.isIndProcessamento())
								arqRec.setCodStatus("0"); //Ser� processado
							else
								arqRec.setCodStatus("1"); //N�o ser� processado
							
							arqRec.setCodOrgaoLotacao(usrLogado.getOrgao().getCodOrgao());
							arqRec.setSigOrgaoLotacao(usrLogado.getOrgao().getSigOrgao());
							arqRec.setCodOrgaoAtuacao(usrLogado.getCodOrgaoAtuacao());
							arqRec.setCodOrgaoAtuacao(usrLogado.getCodOrgaoAtuacao());
							arqRec.setSigOrgaoAtuacao(usrLogado.getSigOrgaoAtuacao());
							arqRec.setNomUsername(usrLogado.getNomUserName());
							arqRec.setDatRecebimento(sys.Util.formatedToday().substring(0,10));                            
							arqRec.carregarHeader(auxLinha, usrLogado);
							arqRec.setNumProtocolo();
							
							if (orgContSetor.equals("S")){
								if (!arqRec.validaNumeracaoRecebida(codOrgaoAtuacao)){
									nextRetorno = "/REG/uploadBaseSetor.jsp";
									arqRec.setMsgErro("Numero de controle fora da faixa estabelecida ou �rg�o inv�lido, verifique o arquivo enviado.");
									req.setAttribute("ArquivoRecebidoBeanId",arqRec);
									return nextRetorno;
								}
							}							
							
							
						} else {
							LinhaArquivoRec linha = new LinhaArquivoRec();
							linha.setDscLinhaArqRec(auxLinha);
							lstLinhas.add(linha);
						}												
					}
					arqRec.setLinhaArquivoRec(lstLinhas);
					
					boolean retorno = true;
					if (orgContSetor.equals("S"))
						retorno = arqRec.isInsertSubPrefeitura(ParamOrgaoId);
					else
						retorno = arqRec.isInsert(ParamOrgaoId);
					
					req.setAttribute("ArquivoRecebidoBeanId", arqRec);
					String NomeArquivo=arqRec.getNomArquivoDir();
					if (NomeArquivo.lastIndexOf(".")== - 1)
						NomeArquivo+=".txt";
					else
						NomeArquivo = NomeArquivo.substring(0,NomeArquivo.lastIndexOf(".")) + ".txt";
					
					if ( arqRec.getArqFuncao().equals("EMITEVEX") || arqRec.getArqFuncao().equals("EMITEPER") ){
						if (retorno){
							String arqOrigem = file.getPath();
							String arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
							FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));
						}
					}else{
						String arqOrigem = file.getPath();
						String arqDestino = parSis.getParamSist("DOWN_ARQUIVO_RECEBIDO") + "/" + NomeArquivo;
						FileUtils.copyFile(new File(arqOrigem), new File(arqDestino));
					}					
				} catch (IOException e) {
					arqRec.setMsgErro("UpLoad ==> Erro ao ler arquivo: " + e);
				}
				finally
				{
					if (br!=null) br.close();
				}
			}
		} catch (Exception ue) {
			throw new sys.CommandException("UploadCommand 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		
		String nextRetorno = jspPadrao;
		try {
			HttpSession session = req.getSession();
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
			
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";
			
			String codOrgaoAtuacao = sys.Util.lPad(usrLogado.getCodOrgaoAtuacao(), "0", 6);
			
			REC.ParamOrgBean ParamOrgaoId = (REC.ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId == null)  ParamOrgaoId = new REC.ParamOrgBean() ;
	

			REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			arqRec.setArqFuncao(req.getParameter("j_sigFuncao"));
			
			
			String orgContSetor = ParamOrgaoId.getParamOrgao("ORG_CONTROLA_SETOR","N","10");
			
			if (orgContSetor.equals("S")){
				nextRetorno = "/REG/uploadBaseSetor.jsp";
				session.setAttribute("nomArq",arqRec.buscaProximoArquivo(ParamOrgaoId,codOrgaoAtuacao));
				req.setAttribute("ArquivoRecebidoBeanId",arqRec);
			}else{
				
				/*Caso �rgao Atua��o igual PMN, n�o associar 'PROXIMO ARQUIVO'*/
				boolean bCalcProximoArquivo=true;
				/*
				if ( (codOrgaoAtuacao.equals("258650")) && (arqRec.getArqFuncao().equals("MR")) )
					bCalcProximoArquivo=false;
				*/
				
				if (bCalcProximoArquivo)
				{
					if (arqRec.getTamNumControle()>0) {					
						arqRec.calcProximoArquivo(ParamOrgaoId,codOrgaoAtuacao);
						if ("".equals(acao.toUpperCase())) {
							req.setAttribute("proximoArquivo",arqRec.getProximoArquivo());
						}
					}
					else
					{
						req.setAttribute("ArquivoRecebidoBeanId",arqRec);
					}
					/*Michel*/
					req.setAttribute("codIdentArquivo",arqRec.getCodIdentArquivo());					
					req.setAttribute("proxControle",arqRec.getProximoControle());
				}

				
				
			}
		} catch (Exception ue) {
			throw new sys.CommandException("UploadCommand 000: " + ue.getMessage());
		}		
		return nextRetorno;
	}
	
}
