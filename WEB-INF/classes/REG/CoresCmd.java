package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Consulta - Consultas Cor<br>
* <b>Description:</b>  Comando para Consultar Cores<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class CoresCmd extends sys.Command {

	private static final String jspPadrao = "/REG/CorConsulta.jsp";

	private String next;

	public CoresCmd() {
		next = jspPadrao;
	}

	public CoresCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();

		try {

			TAB.CorBeanCol Corbc = new TAB.CorBeanCol();
			Corbc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("CorBeanColId", Corbc);
		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}
		return nextRetorno;
	}

}
