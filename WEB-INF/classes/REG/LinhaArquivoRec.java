package REG;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.Util;
import ACSS.OrgaoBean;
import ACSS.UsuarioBean;
import REC.AutoInfracaoBean;
import REC.ParamOrgBean;
import ROB.DaoException;

/**
 * <b>Title:</b>        	SMIT - Bean de Arquivo Recebido via Upload<br>
 * <b>Description:</b>  	Bean dados de Arquivo - Tabela de Arquivos Recebidos<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      	MIND Informatica<br>
 * @author  				Luiz Medronho
 * @version 				1.0
 */

public class LinhaArquivoRec extends sys.HtmlPopupBean { 
	
	private String codLinhaArqRec;
	private String dscLinhaArqRec;
	private String codArquivo;
	private String datRecebimento;
	private String numSeqLinha;
	private String codStatus; // 0 - Recebida, 1 - Processada, 5 - Ignorada, 7 - Cancelada, 8 - Corrigida, 9 - Com Erro
	private Vector erroLinhaArq; 
	private REC.ResponsavelBean proprietario;
	private String codRetorno;
	private String codRetornoBatch;
	private String dscLinhaArqRetorno;
	private String numAutoInfracao;
	private String numNotificacao;
	private String codOrgao;
	private String numPlaca;
	private String dataInfracao;
	private String horaInfracao;
	private String codMunicipio;
	private String dscMunicipio; 
	private String codInfracao;
	
	private String datRecebimentoVEX;
	private String codigoRetornoVEX;
	
	
	private Connection conn;
	
	private String codStatusCorrecao;
	private boolean indReemissao;
	private List listaArquivo; 
	
	public LinhaArquivoRec() throws sys.BeanException {
		
		super();
		setTabela("TSMI_ARQUIVO_REC");	
		setPopupWidth(35);
		
		codLinhaArqRec = "";
		dscLinhaArqRec = "";
		codArquivo = "";
		datRecebimento = "";
		numSeqLinha = "";
		codStatus = "";
		codRetorno = "";
		codRetornoBatch = "";
		dscLinhaArqRetorno = "";
		numAutoInfracao = "";
		numNotificacao = "";
		codOrgao = "";
		codStatusCorrecao = "";
		indReemissao = false;
		dataInfracao = "";
		horaInfracao = "";
		codMunicipio = "";
		codInfracao = "";
		numPlaca   = "";
		
		erroLinhaArq = new Vector();
		proprietario = new REC.ResponsavelBean();
		listaArquivo = new ArrayList();
	}
	
	public void setCodLinhaArqRec(String codLinhaArqRec)  {
		this.codLinhaArqRec=codLinhaArqRec ;
		if (codLinhaArqRec==null) this.codLinhaArqRec="" ;
	}  
	
	public String getCodLinhaArqReq()  {
		return this.codLinhaArqRec;
	}
	
	public void setDscLinhaArqRec(String dscLinhaArqRec)  {
		this.dscLinhaArqRec = dscLinhaArqRec;
		if (dscLinhaArqRec==null) this.dscLinhaArqRec="" ;	
	}  
	
	public String getDscLinhaArqRec()  {
		return this.dscLinhaArqRec;
	}
	
	public void setCodArquivo(String codArquivo)  {
		this.codArquivo=codArquivo ;
		if (codArquivo==null) this.codArquivo="" ;
	}  
	
	public String getCodArquivo()  {
		return this.codArquivo;
	}
	
	public void setNumSeqLinha(String numSeqLinha)  {
		this.numSeqLinha=numSeqLinha ;
		if (numSeqLinha==null) this.numSeqLinha="" ;
	}  
	
	public String getNumSeqLinha()  {
		return this.numSeqLinha;
	}
	
	public void setCodStatus(String codStatus)  {
		this.codStatus=codStatus ;
		if (codStatus==null) this.codStatus="" ;
	}  
	
	public String getCodStatus()  {
		return this.codStatus;
	}
	
	public void setErroLinhaArq(Vector erroLinhaArq) 
	{
		this.erroLinhaArq = erroLinhaArq;
	}
	
	public Vector getErroLinhaArq()  {
		return this.erroLinhaArq;
	}
	
	public void setProprietario(REC.ResponsavelBean proprietario) throws sys.BeanException {
		this.proprietario=proprietario ;
		if (proprietario==null)
			this.proprietario = new REC.ResponsavelBean() ;
	}  
	
	public REC.ResponsavelBean getProprietario()  {
		return this.proprietario;
	}
	
	public void setCodRetorno(String codRetorno)  {
		this.codRetorno=codRetorno ;
		if (codRetorno==null) this.codRetorno="" ;
	}  
	
	public String getCodRetorno()  {
		return this.codRetorno;
	}
	
	public void setCodRetornoBatch(String codRetornoBatch)  {
		this.codRetornoBatch=codRetornoBatch;
		if (codRetornoBatch==null) this.codRetornoBatch="" ;
	}  
	
	public String getCodRetornoBatch()  {
		return this.codRetornoBatch;
	}	
	
	public void setDscLinhaArqRetorno(String dscLinhaArqRetorno)  {
		this.dscLinhaArqRetorno=dscLinhaArqRetorno ;
		if (dscLinhaArqRetorno==null) this.dscLinhaArqRetorno="" ;
	}  
	
	public String getDscLinhaArqRetorno()  {
		return this.dscLinhaArqRetorno;
	}
	
	public void setNumAutoInfracao(String numAutoInfracao)  {
		this.numAutoInfracao = numAutoInfracao;
		if (numAutoInfracao==null) this.numAutoInfracao="" ;  
	}  
	
	public String getNumAutoInfracao()  {
		return this.numAutoInfracao;
	}
	
	public void setNumNotificacao(String numNotificacao)  {
		this.numNotificacao = numNotificacao;
		if (numNotificacao==null) this.numNotificacao="" ;  
	}  
	
	public String getNumNotificacao()  {
		return this.numNotificacao;
	}
	
	public String getCodStatusCorrecao(){
		return codStatusCorrecao;
	}	
	public void setCodStatusCorrecao(String codStatusCorrecao){
		this.codStatusCorrecao = codStatusCorrecao;
	}
	
	public boolean isIndReemissao(){
		return indReemissao;
	}	
	public void setIndReemissao(boolean indReemissao){
		this.indReemissao = indReemissao;
	}
	
	public String getCodOrgao() {
		return codOrgao;
	}
	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
	}	
	
	/**
	 * @author Sergio Roberto Junior
	 * @return nome do erro na notifica��o
	 * @throws DaoException 
	 * @since 20/12/2004
	 */
	public String getNomStatusErro(String sigFuncao) throws DaoException{
		String retorno = "";
		try{
			Dao dao = Dao.getInstance();
			if( (codRetorno == null) || codRetorno.equals("") )
				return retorno;
			if("REG0510".equals(sigFuncao))
				retorno = dao.getDescLinhaErro(codRetorno);
			else if("REG0520".equals(sigFuncao))
				retorno = dao.verificaTabEvento(codRetorno);
		}
		catch(DaoException e){
			retorno = "ERRO DE ACESSO AO BANCO DE DADOS !";
		}
		return retorno;
	}
	
	public String getLinhaRej(ArquivoRecebidoBean arqRecebido, Connection conn)
	throws sys.BeanException {
		
		String linhaRej = "";
		String linha = sys.Util.rPad(this.getDscLinhaArqRec(), " ", 500);
		
		try {
			if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
				linhaRej = linha.substring(0, 177) + sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
			
			else if (arqRecebido.getCodIdentArquivo().equals("CODRET")) {
				String numAutoInf	= sys.Util.rPad(linha.substring(127).trim(), " ", 12);
				String numNotif		= sys.Util.rPad(linha.substring(9, 18), " ", 9);
				String codErro 		= sys.Util.lPad(this.getCodRetorno(), "0", 3);
				/*Tratamento de C�digo de Retorno*/
				try
				{
				  int iCodErro=Integer.parseInt(codErro);	
				}
				catch (Exception e)
				{
					codErro="999";
				}
				String dscErro 		= sys.Util.rPad(ROB.DaoBroker.getInstance().buscarErroBroker(conn,
						this.getCodRetorno()), " ", 50);
				linhaRej = numAutoInf + " " + numNotif + " " + codErro + " " + dscErro;
				
			} else {
				String seq 			= sys.Util.lPad(this.getNumSeqLinha(), "0", 6);        
				String numAutoInf 	= linha.substring(1, 13);
				String codErro 		= sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
				String dscErro 		= sys.Util.rPad(ROB.DaoBroker.getInstance().buscarErroBroker(conn,
						this.getCodRetorno()), " ", 50);           
				linhaRej = seq + " " + numAutoInf + " " + codErro + " " + dscErro;
			}
			
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}        
		return linhaRej;       
	}
	
	public String getLinhaGrv(ArquivoRecebidoBean arqRecebido, Connection conn) throws sys.BeanException {
		
		String linhaGrv = "";
		String linha = sys.Util.rPad(this.getDscLinhaArqRec(), " ", 1500);
		String linhaRetorno = sys.Util.rPad(this.getDscLinhaArqRetorno(), " ", 1500);
		
		try 
		{
			if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
				linhaGrv = linha.substring(0, 177) + sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
			
			else if (arqRecebido.getCodIdentArquivo().equals("LOG01")) {
				linhaGrv = sys.Util.rPad(this.getDscLinhaArqRec(), " ", 1400);				
			}
			else if (arqRecebido.getCodIdentArquivo().equals("EMITEVEX") || arqRecebido.getCodIdentArquivo().equals("EMITEPER")) {
				
				// Le o auto de infra��o para pegar os campos complementares
				UsuarioBean usuario = new UsuarioBean();
				usuario.setNomUserName(arqRecebido.getNomUsername());
				usuario.getOrgao().setCodOrgao(arqRecebido.getCodOrgaoLotacao());
				AutoInfracaoBean autoBean = new AutoInfracaoBean();
				autoBean.setNumAutoInfracao(this.numAutoInfracao.trim());
				
				if (!autoBean.LeAutoInfracaoLocal("auto")) {
					usuario.setCodOrgaoAtuacao("999999");
					String numPlaca = linha.substring(125,132);									
					autoBean.setNumPlaca(numPlaca);
					autoBean.LeAutoInfracao("auto", usuario);
					autoBean.GravaAutoInfracaoLocal();
				}
				/*
				usuario.setCodOrgaoAtuacao("999999");
				String numPlaca = linha.substring(125,132);									
				autoBean.setNumPlaca(numPlaca);
				autoBean.LeAutoInfracao("auto", usuario);
				autoBean.GravaAutoInfracaoLocal();
				*/
				
				String identOrgao    = Util.rPad(autoBean.getIdentOrgaoEdt(), " ", 12);
				/*DPWEB*/
				String codEletronico = Util.lPad(linha.substring(820,824).trim(), "0", 4);
				/*
				 * Em 13/12/2011, foi adicionada as 60 posi��es do Bradesco referente ao arquivo EMITEVEX.
				 */
				String posicoesBradesco = Util.rPad(linha.substring(824,884).trim(), " ", 60);

				
				String cpfCnpj = Util.lPad(autoBean.getProprietario().getNumCpfCnpj(), "0", 14);
				String numProcesso = Util.rPad("", " ", 20);
				
				if (linha.substring(571,572).equals("3") && autoBean.temDefesaPrevia(usuario))
					numProcesso = Util.rPad(autoBean.getNumProcesso(), " ", 20);
				
				String resumoInfracao = Util.rPad(autoBean.getDscResumoInfracao(), " ", 250);
				String marcaModeloAparelho = Util.rPad(autoBean.getDscMarcaModeloAparelho(), " ", 120);
				String codAgente = Util.lPad(autoBean.getCodAgente(), "0", 2);
				String localInfracaoNotificacao = Util.rPad(autoBean.getDscLocalInfracaoNotificacao(), " ", 100);
				String tipResponsaval = autoBean.getProprietario().getIndCpfCnpj();
				
				// Le o parametro de orgao para ler o prazo para recurso
				ParamOrgBean paramOrgao = new ParamOrgBean();
				String codOrgao = linha.substring(579,585);				
				usuario.setCodOrgaoAtuacao(codOrgao);				
				paramOrgao.PreparaParam(usuario);
				String prazo = "";
				try {
					prazo = Util.lPad(paramOrgao.getParamOrgao("DIAS_DEF_PENALIDADE"), "0", 2);
				} catch (Exception e) { 
					prazo = "00"; 
				}
				/*DPWEB*/
				linhaGrv = linha.substring(0, 811) + identOrgao + cpfCnpj + numProcesso + prazo + resumoInfracao + 
				marcaModeloAparelho + codAgente + localInfracaoNotificacao + tipResponsaval+codEletronico+posicoesBradesco;
				
				linhaGrv=linhaGrv.toUpperCase();
				
			} else if (arqRecebido.getCodIdentArquivo().equals("TVEP")) {
				linhaGrv = linha.substring(0, 119);
				
			} else if (arqRecebido.getCodIdentArquivo().equals("SDOL")) {
				linhaGrv = linha.substring(0, 250);
				
			} else if ("MR01,ME01".indexOf(arqRecebido.getCodIdentArquivo()) >= 0) {
				String numAutoInfracao  =   linha.substring(1,13);
				String numPlaca         =   linha.substring(13,20);
				String codRenavam       =   linhaRetorno.substring(24,33);
				String dscMarca         =   linhaRetorno.substring(129,154);
				String codMarca         =   sys.Util.lPad(buscarCodMarcaModelo(conn,dscMarca),"0",7);
				String local            =   linha.substring(27,72);            
				String datInfracao      =   linha.substring(76,80) + linha.substring(74,76) + linha.
				substring(72,74);
				String horInfracao      =   linha.substring(80,84);
				String codMunicipio     =   linha.substring(84,88);
				String codInfracao      =   sys.Util.lPad(linha.substring(88,92),"0",5);
				String valMulta         =   sys.Util.lPad(linha.substring(93,99),"0",9);
				String datVencimento    =   linhaRetorno.substring(233,237) + linhaRetorno.substring(231,233)
				+ linhaRetorno.substring(229,231);
				String codCancelamento  =   linhaRetorno.substring(0,3);
				String matAgente        =   linha.substring(109,119);
				String velPermitida     =   linha.substring(119,122);
				String velAferida       =   linha.substring(122,126);
				String orgAutu          =   sys.Util.lPad(arqRecebido.getCodOrgao(),"0",6);
				String codAgente        =   linha.substring(107,109);
				String nomPropietario   =   sys.Util.rPad(linhaRetorno.substring(189,229), " ", 45);
				
				String numCPF           =   sys.Util.lPad("","0",11);
				String numCGC           =   sys.Util.lPad("","0",14);
				String CPFouCGC         =   linhaRetorno.substring(188,189);            
				if (CPFouCGC.equals("1"))
					numCPF              =   linhaRetorno.substring(177,188);
				else if (CPFouCGC.equals("2"))
					numCGC             =   linhaRetorno.substring(174,188);
				
				String logEndereco      =   linhaRetorno.substring(33,58);
				String numEndereco      =   linhaRetorno.substring(73,78);
				String compEndereco     =   linhaRetorno.substring(103,114);
				
				String dscMunEndereco   =   linhaRetorno.substring(78,99);
				String sEspecie         =   linhaRetorno.substring(99,101);
				
				String ufEndereco		= 	linhaRetorno.substring(101,103).trim();				
				
				OrgaoBean orgaoBean = new OrgaoBean();
				orgaoBean.Le_Orgao(arqRecebido.getCodOrgao(),0);
				if (ufEndereco.equals("")) 	ufEndereco = orgaoBean.getEndereco().getCodUF();
				String codMunEndereco   =   sys.Util.lPad(buscarCodMunicipio(conn,dscMunEndereco,ufEndereco),"0",4);
				
				String cepEndereco      =   linhaRetorno.substring(121,129);
				String datCorreio       =   sys.Util.todayYYYYMMDD();
				String datBanco         =   sys.Util.todayYYYYMMDD();
				String statusDoc        =   "1";
				String datInclusao      =   sys.Util.todayYYYYMMDD();
				String vago             =   sys.Util.lPad("", " ", 8);

				/*DER (28/03/2007) Email-23/03*/
				//String sEspecie         =   linhaRetorno.substring(99,101); Pegar dentro do Munic�pio
				String sCategoria	    =   linhaRetorno.substring(237,239);
				String sTipo		    =   linhaRetorno.substring(240,242);
				String sCor             =   linhaRetorno.substring(243,245);
				vago                    =   sys.Util.rPad(sEspecie+sCategoria+sTipo+sCor," ",8);
				
				String respInfracao     =   "P";
				
				linhaGrv = numAutoInfracao+numPlaca+codRenavam+codMarca+local+datInfracao+horInfracao    
				+codMunicipio+codInfracao+valMulta+datVencimento+codCancelamento+matAgente      
				+velPermitida+velAferida+orgAutu+codAgente+nomPropietario+numCPF+numCGC+logEndereco    
				+numEndereco+compEndereco+codMunEndereco+cepEndereco+datCorreio+datBanco+statusDoc      
				+datInclusao+vago+respInfracao;   
				
			} else {
				String seq 			= sys.Util.lPad(this.getNumSeqLinha(), "0", 6);        
				String numAutoInf 	= linha.substring(1, 13);
				String codErro 		= sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
				String dscErro 		= sys.Util.rPad(ROB.DaoBroker.getInstance().buscarErroBroker(conn,
						this.getCodRetorno()), " ", 50);           
				linhaGrv = seq + " " + numAutoInf + " " + codErro + " " + dscErro;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			throw new sys.BeanException(e.getMessage());
		}  
		
		return linhaGrv;
	}
	

	public String getLinhaGrvP59(ArquivoRecebidoBean arqRecebido, Connection conn) throws sys.BeanException {
		
		String linhaGrv = "";
		String linha = sys.Util.rPad(this.getDscLinhaArqRec(), " ", 900);
		String linhaRetorno = sys.Util.rPad(this.getDscLinhaArqRetorno(), " ", 900);
		
		try 
		{
			if (arqRecebido.getCodIdentArquivo().equals("RECJARI"))
				linhaGrv = linha.substring(0, 197) + sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
			
			else if (arqRecebido.getCodIdentArquivo().equals("LOG01")) {
				linhaGrv = sys.Util.rPad(this.getDscLinhaArqRec(), " ", 1400);				
			}
			else if (arqRecebido.getCodIdentArquivo().equals("EMITEVEX")) {
				
				// Le o auto de infra��o para pegar os campos complementares
				UsuarioBean usuario = new UsuarioBean();
				usuario.setNomUserName(arqRecebido.getNomUsername());
				usuario.getOrgao().setCodOrgao(arqRecebido.getCodOrgaoLotacao());
				AutoInfracaoBean autoBean = new AutoInfracaoBean();
				autoBean.setNumAutoInfracao(this.numAutoInfracao.trim());
				
				if (!autoBean.LeAutoInfracaoLocal("auto")) {
					usuario.setCodOrgaoAtuacao("999999");
					String numPlaca = linha.substring(125,132);									
					autoBean.setNumPlaca(numPlaca);
					autoBean.LeAutoInfracao("auto", usuario);
					autoBean.GravaAutoInfracaoLocal();
				}
				
				String identOrgao    = Util.rPad(autoBean.getIdentOrgaoEdt(), " ", 12);
				/*DPWEB*/
				String codEletronico = Util.lPad(linha.substring(810,814).trim(), "0", 4);

				
				String cpfCnpj = Util.lPad(autoBean.getProprietario().getNumCpfCnpj(), "0", 14);
				String numProcesso = Util.rPad("", " ", 20);
				
				if (linha.substring(561,562).equals("3") && autoBean.temDefesaPrevia(usuario))
					numProcesso = Util.rPad(autoBean.getNumProcesso(), " ", 20);
				
				String resumoInfracao = Util.rPad(autoBean.getDscResumoInfracao(), " ", 250);
				String marcaModeloAparelho = Util.rPad(autoBean.getDscMarcaModeloAparelho(), " ", 120);
				String codAgente = Util.lPad(autoBean.getCodAgente(), "0", 2);
				String localInfracaoNotificacao = Util.rPad(autoBean.getDscLocalInfracaoNotificacao(), " ", 100);
				String tipResponsaval = autoBean.getProprietario().getIndCpfCnpj();
				
				// Le o parametro de orgao para ler o prazo para recurso
				ParamOrgBean paramOrgao = new ParamOrgBean();
				String codOrgao = linha.substring(569,575);
				usuario.setCodOrgaoAtuacao(codOrgao);				
				paramOrgao.PreparaParam(usuario);
				String prazo = "";
				try {
					prazo = Util.lPad(paramOrgao.getParamOrgao("DIAS_DEF_PENALIDADE"), "0", 2);
				} catch (Exception e) { 
					prazo = "00"; 
				}
				/*DPWEB*/
				linhaGrv = linha.substring(0, 801) + identOrgao + cpfCnpj + numProcesso + prazo + resumoInfracao + 
				marcaModeloAparelho + codAgente + localInfracaoNotificacao + tipResponsaval+codEletronico;
				
			} else if (arqRecebido.getCodIdentArquivo().equals("TVEP")) {
				linhaGrv = linha.substring(0, 119);
				
			} else if (arqRecebido.getCodIdentArquivo().equals("SDOL")) {
				linhaGrv = linha.substring(0, 250);
				
			} else if ("MR01,ME01".indexOf(arqRecebido.getCodIdentArquivo()) >= 0) {
				String numAutoInfracao  =   linha.substring(1,13);
				String numPlaca         =   linha.substring(13,20);
				String codRenavam       =   linhaRetorno.substring(22,33);
				String dscMarca         =   linhaRetorno.substring(154,179);
				String codMarca         =   sys.Util.lPad(buscarCodMarcaModelo(conn,dscMarca),"0",7);
//				String local            =   linha.substring(27,72);            
				String local            =   linha.substring(27,107);            
//				String datInfracao      =   linha.substring(76,80) + linha.substring(74,76) + linha.
//				substring(72,74);
				String datInfracao      =   linha.substring(111,115) + linha.substring(109,111) + linha.
				substring(107,109);
//				String horInfracao      =   linha.substring(80,84);
				String horInfracao      =   linha.substring(115,119);
//				String codMunicipio     =   linha.substring(84,88);
				String codMunicipio     =   linha.substring(119,124);
//				String codInfracao      =   sys.Util.lPad(linha.substring(88,92),"0",5);
				String codInfracao      =   sys.Util.lPad(linha.substring(124,129),"0",5);
//				String valMulta         =   sys.Util.lPad(linha.substring(93,99),"0",9);
				String valMulta         =   sys.Util.lPad(linha.substring(129,136),"0",9);
				String datVencimento    =   linhaRetorno.substring(278,282) + linhaRetorno.substring(276,278)
				+ linhaRetorno.substring(274,276);
				
				
				String codCancelamento  =   linhaRetorno.substring(0,3);
//				String matAgente        =   linha.substring(109,119);
				String matAgente        =   linha.substring(146,161);
//				String velPermitida     =   linha.substring(119,122);
				String velPermitida     =   linha.substring(161,170);
//				String velAferida       =   linha.substring(122,126);
				String velAferida       =   linha.substring(170,179);
				String orgAutu          =   sys.Util.lPad(arqRecebido.getCodOrgao(),"0",6);
//				String codAgente        =   linha.substring(107,109);
				String codAgente        =   linha.substring(144,146);
				String nomPropietario   =   sys.Util.rPad(linhaRetorno.substring(214,274), " ", 60);
				
				String numCPF           =   sys.Util.lPad("","0",11);
				String numCGC           =   sys.Util.lPad("","0",14);
				
				String CPFouCGC         =   linhaRetorno.substring(213,214);            
				if (CPFouCGC.equals("1"))
					numCPF              =   linhaRetorno.substring(202,213);
				else if (CPFouCGC.equals("2"))
					numCGC             =   linhaRetorno.substring(199,213);
				
				String logEndereco      =   linhaRetorno.substring(33,73);
				String numEndereco      =   linhaRetorno.substring(73,78);
				String compEndereco     =   linhaRetorno.substring(128,146);
				
				String dscMunEndereco   =   linhaRetorno.substring(78,99);
				String sEspecie         =   linhaRetorno.substring(99,101);
				
				String ufEndereco		= 	linhaRetorno.substring(101,103).trim();				
				
				
				OrgaoBean orgaoBean = new OrgaoBean();
				orgaoBean.Le_Orgao(arqRecebido.getCodOrgao(),0);
				if (ufEndereco.equals("")) 	ufEndereco = orgaoBean.getEndereco().getCodUF();
//				String codMunEndereco   =   sys.Util.lPad(buscarCodMunicipio(conn,dscMunEndereco,ufEndereco),"0",4);
				String codMunEndereco   =   sys.Util.lPad(buscarCodMunicipio(conn,dscMunEndereco,ufEndereco),"0",5);
				
				String cepEndereco      =   linhaRetorno.substring(146,154);
				String datCorreio       =   sys.Util.todayYYYYMMDD();
				String datBanco         =   sys.Util.todayYYYYMMDD();
				String statusDoc        =   "1";
				String datInclusao      =   sys.Util.todayYYYYMMDD();
				String vago             =   sys.Util.lPad("", " ", 8);

				/*DER (28/03/2007) Email-23/03*/
				//String sEspecie         =   linhaRetorno.substring(99,101); Pegar dentro do Munic�pio
//				String sCategoria	    =   linhaRetorno.substring(237,239);
				String sCategoria	    =   linhaRetorno.substring(282,284);
//				String sTipo		    =   linhaRetorno.substring(240,242);
				String sTipo		    =   linhaRetorno.substring(284,287);
//				String sCor             =   linhaRetorno.substring(243,245);
				String sCor             =   linhaRetorno.substring(287,290);
				vago                    =   sys.Util.rPad(sEspecie+sCategoria+sTipo+sCor," ",8);
				
				String respInfracao     =   "P";
				
				linhaGrv = numAutoInfracao+numPlaca+codRenavam+codMarca+local+datInfracao+horInfracao    
				+codMunicipio+codInfracao+valMulta+datVencimento+codCancelamento+matAgente      
				+velPermitida+velAferida+orgAutu+codAgente+nomPropietario+numCPF+numCGC+logEndereco    
				+numEndereco+compEndereco+codMunEndereco+cepEndereco+datCorreio+datBanco+statusDoc      
				+datInclusao+vago+respInfracao;   
				
			} else {
				String seq 			= sys.Util.lPad(this.getNumSeqLinha(), "0", 6);        
				String numAutoInf 	= linha.substring(1, 13);
				String codErro 		= sys.Util.lPad(this.getCodRetornoBatch(), "0", 2);
				String dscErro 		= sys.Util.rPad(ROB.DaoBroker.getInstance().buscarErroBroker(conn,
						this.getCodRetorno()), " ", 50);           
				linhaGrv = seq + " " + numAutoInf + " " + codErro + " " + dscErro;
			}
			
		} catch (Exception e) {
			System.out.println(e);
			throw new sys.BeanException(e.getMessage());
		}  
		
		return linhaGrv;
	}
	
	
	
	public String buscarCodMunicipio(Connection conn, String dscMunicipio, String UF) {
		
		String codMunicipio = "";
		try {
			codMunicipio = Dao.getInstance().buscarCodMunicipio(conn, dscMunicipio, UF);
			return codMunicipio;
		} catch (Exception e) {
			return codMunicipio;
		}                    
	}
	
	public String buscarCodMarcaModelo(Connection conn, String dscMarcaModelo) {
		
		String codMarcaModelo = "";
		try {
			codMarcaModelo = Dao.getInstance().buscarCodMarcaModelo(conn, dscMarcaModelo);
			return codMarcaModelo;
		} catch (Exception e) {
			return codMarcaModelo;
		}                    
	}
	
	
	public boolean buscarFoto(Connection conn, String numAutoInfracao) {
		
		boolean bFoto=true;
		try {
			bFoto = Dao.getInstance().buscarFoto(conn, numAutoInfracao);
			return bFoto;
		} catch (Exception e) {
			return bFoto;
		}                    
	}	
	
	
	
	
	public void setConn(Connection conn) {
		this.conn = conn;
	}
	
	public Connection getConn() 	{
		return conn;
	}
	
	public boolean isUpdate() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LinhaArquivoUpdate(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o atualizada: " + this.getCodLinhaArqReq() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}
	
	public boolean logar(Date iniLinha, Date iniBroker, Date fimBroker, Date fimLinha) {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.LogarLinhaArquivo(this, iniLinha, iniBroker, fimBroker, fimLinha);
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o logada: " + this.getCodLinhaArqReq() + " \n") ;
			vErro.addElement("Erro na atualiza��o: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}

	public void carregarCampos(ArquivoRecebidoBean arquivo) throws sys.BeanException {		
		try {
			if (arquivo.getCodIdentArquivo().equals("EMITEVEX")) { 
				String linha = this.getDscLinhaArqRec();
				String numAutoInfracao = linha.substring(360,372).trim();				
				String numNotificacao = linha.substring(6,15);
				String codOrgao = linha.substring(579,585);
				this.setNumAutoInfracao(numAutoInfracao);
				this.setNumNotificacao(numNotificacao);
				this.setCodOrgao(codOrgao);
				//Altera��o da Perrone 23/05/2012 Rodrigo Fonseca
			}else if (arquivo.getCodIdentArquivo().equals("EMITEPER")) { 
				String linha = this.getDscLinhaArqRec();
				String numAutoInfracao = linha.substring(360,372).trim();				
				String numNotificacao = linha.substring(6,15);
				String codOrgao = linha.substring(579,585);
				this.setNumAutoInfracao(numAutoInfracao);
				this.setNumNotificacao(numNotificacao);
				this.setCodOrgao(codOrgao);
				
			} else if (arquivo.getCodIdentArquivo().equals("CODRET")) { 
				String linha = this.getDscLinhaArqRec();
				String numAutoInfracao = linha.substring(127,139).trim();
				String numNotificacao = linha.substring(9,18);
				this.setNumAutoInfracao(numAutoInfracao);
				this.setNumNotificacao(numNotificacao);				
				if (linha.length() >= 147) {
					String codOrgao = linha.substring(140,146);
					this.setCodOrgao(codOrgao);
				} 
			}else if (arquivo.getCodIdentArquivo().equals("MR01")) { 
				String linha = this.getDscLinhaArqRec();
				String numAutoInfracao = linha.substring(1,13).trim();					
				String numPlaca = linha.substring(13,20);
				this.setNumAutoInfracao(numAutoInfracao);
				this.setNumPlaca(numPlaca);
				this.setCodOrgao(arquivo.getCodOrgao());
				
			}else if (arquivo.getCodIdentArquivo().equals("PE01")) { 
				String linha = this.getDscLinhaArqRec();
				String numAutoInfracao = linha.substring(47,59).trim();					
				String numPlaca = linha.substring(59,66);
				this.setNumAutoInfracao(numAutoInfracao);
				this.setNumPlaca(numPlaca);
				this.setCodOrgao(arquivo.getCodOrgao());
			}
			
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}	
	
	/**
	 * M�todo usado para ajustar as notifica��es pendentes de envio para 
	 * a VEX, atrav�s dos arquivos Emitevex.
	 * Caso a linha esteja livre para inclus�o em um novo arquivo Emitevex o m�todo 
	 * ira retornar true, caso contr�rio o m�todo ira retornar false com o erro referenciado
	 * pelo atributo codRetorno, presente no objeto da linha.
	 * @author Sergio Roberto Junior 
	 * @since 22/12/2004
	 * @return boolean
	 * @throws Exception
	 * @version 1.0
	 */
	public boolean ajustaNotif(UsuarioBean UsrLogado, String j_sigFuncao) throws Exception{
		try{
//			Executa o c�digo para funcionalidade de envio das notifica��es dos arquivos recebidos
			if("REG0510".equals(j_sigFuncao)){
				Dao dao = Dao.getInstance();
				ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
				List linha = new ArrayList();
				linha.add(this);
				arqRecebido.setLinhaArquivoRec(linha);
				dao.verificaNotificacoes(arqRecebido);
				if(!codStatusCorrecao.equals("0")){
					if(codRetorno.equals("N01")){
						dao.LinhaArquivoUpdate(this);
						setMsgErro("ATUALIZA��O N�O REALIZADA \nNOTIFICA��O J� ENVIADA ANTERIORMENTE");
					}
					else if(codRetorno.equals("N05")){
						dao.LinhaArquivoUpdate(this);
						dao.buscarArqLinha(arqRecebido);
						setMsgErro("ATUALIZA��O N�O REALIZADA \nNOTIFICA��O J� PRESENTE NO ARQUIVO "+arqRecebido.getNomArquivo());
					}   				
					else if(codRetorno.equals("N02")){
						dao.LinhaArquivoUpdate(this);
						setMsgErro("ATUALIZA��O N�O REALIZADA \nNOTIFICA��O PENDENTE DE FOTO");
					}
					else if(codRetorno.equals("N03")){
						dao.LinhaArquivoUpdate(this);
						setMsgErro("ATUALIZA��O N�O REALIZADA \n�RG�O AUTUADOR INV�LIDO");
					}
					return false;
				}
				codStatus = "8";
				codRetorno = "";
				numAutoInfracao = dscLinhaArqRec.substring(280,289);
				numNotificacao = dscLinhaArqRec.substring(6,15);
				dao.atualizaErroLinha(codArquivo,1);
				dao.LinhaArquivoUpdate(this);
				dao.linhaUpdateReenvio(this,UsrLogado);
			}
			
			// Executa o c�digo para funcionalidade de retorno das notifica��es dos arquivos recebidos
			else if("REG0520".equals(j_sigFuncao)){
				ROB.DaoBroker dao = ROB.DaoBroker.getInstance();
				String resultado = dao.TransacaoRetorno(UsrLogado.getNomUserName(),UsrLogado.getCodOrgaoAtuacao(),UsrLogado.getOrgao().getCodOrgao(),this);   						
				codRetorno = resultado.substring(0,3);
				if("000,632,633,634".indexOf(codRetorno)>=0){
					// Bloco de implementa��o usado para atualizar os dados da linha.
					codRetornoBatch = resultado.substring(3,5);
					codStatus = "1";
					isUpdate();
					
					// Bloco usado para atualizar o status do arquivo. 
					ArquivoRecebidoBean arqRecebido = new ArquivoRecebidoBean();
					arqRecebido.setCodArquivo(codArquivo);
					arqRecebido.getArquivoRecebido();
					arqRecebido.setQtdLinhaPendente(String.valueOf((Integer.parseInt(arqRecebido.getQtdLinhaPendente()))-1));
					if("632,633,634".indexOf(codRetorno)>=0)
						arqRecebido.setQtdLinhaErro(String.valueOf((Integer.parseInt(arqRecebido.getQtdLinhaErro()))+1));
					arqRecebido.setCodStatus("1");
					arqRecebido.isUpdate();
					codStatusCorrecao ="9";
					dao.atualizaContrNotif(UsrLogado.getNomUserName(),UsrLogado.getCodOrgaoAtuacao(),this);
				}
				else{
					if(resultado.indexOf("ERR")>=0)
						setMsgErro(" A NOTIFICA��O N�O FOI ATUALIZADA !\nERRO: ERRO DE ACESSO AO BROKER ");
					else
						setMsgErro(" A NOTIFICA��O N�O FOI ATUALIZADA !\nERRO: "+getNomStatusErro("REG0520"));
					return false;
				}
			}    		
			return true;
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	public void carregarLinha() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			dao.buscarLinha(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo usado para cancelar todas as linhas pendentes de envio, presentes no arquivo
	 * Emitevex, que n�o seram mais ajustadas.
	 * @param linhas
	 * @author Sergio Roberto Junior
	 * @since 29/12/2004
	 * @throws Exception
	 */
	public void cancelarLinha(String linhas[], String j_sigFuncao) throws Exception{
		try{
			/*
			 * Estrutura de condicionais criada para passar a DAO a correta cl�usula de 
			 * filtro que ser� usada nas instru��es SQL, correlacionadas a notifica��es 
			 * de envio ou retorno.
			 * N�o � passado o j_sigFuncao da opera��o para desacoplar ao m�ximo implement�-
			 * ��es espec�ficas do programa, a classe de acesso a base de dados do SMIT.
			 */
			if("REG0510".equals(j_sigFuncao))	
				j_sigFuncao = "ENVIO";
			else if("REG0520".equals(j_sigFuncao))
				j_sigFuncao = "RETORNO";
			
			Dao dao = Dao.getInstance();
			dao.cancelarLinha(linhas,j_sigFuncao);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo criado para atualizar o status da notifica��o como enviada com sucesso
	 * pelo Detran na tabela TSMI_CONTROLE_NOTIFICACOES
	 * @author Sergio Roberto Junior
	 * @since 11/05/2004
	 * @version 1.0
	 */
	public void atualizaStatusNotif() throws Exception{
		try{
			Dao dao = Dao.getInstance();
			codStatus = "0";
			dao.atualizaStatusNotif(this);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	/**
	 * M�todo para atualizar o c�digo do novo arquivo de envio de uma linha que possui
	 * seu status como pendente e foi ajustada.
	 * @param linhaRec
	 * @param codArquivoOrig
	 * @author Sergio Roberto Junior
	 * @since 11/05/2005
	 * @throws DaoException
	 * @version 1.0
	 */
	public void atualizaArqEnvio(ArquivoRecebidoBean arqRecebido, String codArquivoOrig) throws Exception{
		try{
			Dao dao = Dao.getInstance();
			dao.atualizaArqEnvio(this,arqRecebido,codArquivoOrig);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}	
	
	public void gravarAutoInfracao(ArquivoRecebidoBean arquivo) throws BeanException {
		try {
			if (arquivo.getCodIdentArquivo().equals("MR01")) {
				
				AutoInfracaoBean autoBean = new AutoInfracaoBean();
				UsuarioBean usuario = new UsuarioBean();
				usuario.setNomUserName(arquivo.getNomUsername());
				usuario.getOrgao().setCodOrgao(arquivo.getCodOrgaoLotacao());
				usuario.setCodOrgaoAtuacao("999999");
				String numAutoInfracao = this.dscLinhaArqRec.substring(1, 13).trim();
				autoBean.setNumAutoInfracao(numAutoInfracao);
				String numPlaca = this.dscLinhaArqRec.substring(13, 20);
				autoBean.setNumPlaca(numPlaca);
				autoBean.LeAutoInfracao("auto", usuario);
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				autoBean.setDatRegistro(df.format(new Date()));
				
				if (arquivo.getCodOrgao().equals("258650")) { //Layout de Niteroi
					String linha                    = sys.Util.rPad(this.dscLinhaArqRec, " ", 1000);
					String identOrgao               = "";
					String resumoInfracao           = "";
					String marcaModeloAparelho      = "";
					String localInfracaoNotificacao = "";
					if (arquivo.getDscPortaria59().equals("P59")){
						identOrgao = linha.substring(505, 514);
						resumoInfracao = linha.substring(514, 764);
						marcaModeloAparelho = linha.substring(764, 884);
						if (linha.length() >= 876)
							localInfracaoNotificacao = linha.substring(884, 984);
						else
							localInfracaoNotificacao = linha.substring(884, 984);
					}else{
						identOrgao = linha.substring(396, 405);
						resumoInfracao = linha.substring(405, 655);
						marcaModeloAparelho = linha.substring(655, 775);
						if (linha.length() >= 876)
							localInfracaoNotificacao = linha.substring(775, 875);
						else
							localInfracaoNotificacao = linha.substring(775, 875);
					}
					autoBean.setNumAutoInfracao(numAutoInfracao.trim());
					autoBean.setIdentOrgao(identOrgao);
					autoBean.setDscResumoInfracao(resumoInfracao.trim());
					autoBean.setDscMarcaModeloAparelho(marcaModeloAparelho.trim());
					autoBean.setDscLocalInfracaoNotificacao(localInfracaoNotificacao.trim());
				}				
				autoBean.GravaAutoInfracaoLocal();
			}
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
	public List getListaArquivos() {
		return listaArquivo;
	}
	
	public void setListaArquivos(List listaArquivo) {
		this.listaArquivo = listaArquivo;
	}
	
	public LinhaArquivoRec getListaArquivo(int i) { 
		return ( LinhaArquivoRec )this.listaArquivo.get(i); 
	}
	
	public String getCodInfracao() {
		return codInfracao;
	}
	
	public void setCodInfracao(String codInfracao) {
		if (codInfracao == null || codInfracao.equals("null") || codInfracao.equals(""))
			this.codInfracao = "-";
		else
			this.codInfracao = codInfracao;
	}
	
	public String getCodMunicipio() {
		return codMunicipio;
	}
	
	public void setCodMunicipio(String codMunicipio) {
		if (codMunicipio == null || codMunicipio.equals("null") || codMunicipio.equals(""))
			this.codMunicipio = "-";
		else
			this.codMunicipio = codMunicipio;
	}
	
	
	
	public String getHoraInfracao() {
		if (this.horaInfracao.equals("-"))
			return horaInfracao;
		else
			return this.horaInfracao.substring(0, 2) + ":" + this.horaInfracao.substring(2, 4);
	}
	
	public void setHoraInfracao(String horaInfracao) {
		if (horaInfracao == null || horaInfracao.equals("null") || horaInfracao.equals(""))
			this.horaInfracao = "-";
		else
			this.horaInfracao = horaInfracao;
	}
	
	public String getNumPlaca() {
		return numPlaca;
	}
	
	public void setNumPlaca(String numPlaca) {
		this.numPlaca = numPlaca;
	}
	
	public String getDataInfracao() {
		return dataInfracao;
	}
	
	public void setDataInfracao(String dataInfracao) {
		if (dataInfracao == null || dataInfracao.equals("null") || dataInfracao.equals(""))
			this.dataInfracao = "-";
		else
			this.dataInfracao = dataInfracao;
	}

	public boolean carregaEquipEletr() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.carregaEquipEletr(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o atualizada: " + this.getCodLinhaArqReq() + " \n") ;
			vErro.addElement("Erro na carregaEquipEletr: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}
	
	/*Bahia 23/09/2008*/
	public boolean processaRetornoArDigTMP() {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.processaRetornoArDigTMP(this) ;
		}
		catch (Exception e) { 
			vErro.addElement("Retorno n�o atualizado: " + this.getCodLinhaArqReq() + " \n") ;
			vErro.addElement("Erro na processaRetornoArDigTMP: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}	
	

	/*Retirar ap�s processsamento*/
	public boolean processaArDigTMP(ArquivoRecebidoBean arqRecebido) {
		boolean bOk = true ;
		Vector vErro = new Vector() ;
		try {
			Dao dao = Dao.getInstance();
			dao.processaArDigTMP(this,arqRecebido) ;
		}
		catch (Exception e) { 
			vErro.addElement("Linha n�o atualizada: " + this.getCodLinhaArqReq() + " \n") ;
			vErro.addElement("Erro na carregaEquipEletr: "+e.getMessage()+" \n");
			bOk = false;
		}
		this.setMsgErro(vErro);
		return bOk;  
	}

	public String getDatRecebimento() {
		return datRecebimento;
	}

	public void setDatRecebimento(String datRecebimento) {
		if (datRecebimento==null) datRecebimento="";
		this.datRecebimento = datRecebimento;
	}	

	
	
	
}