package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Tipo<br>
* <b>Description:</b>  Comando para Consultar Tipos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class TipoCmd extends sys.Command {

	private static final String jspPadrao = "/REG/TipoConsulta.jsp";

	private String next;

	public TipoCmd() {
		next = jspPadrao;
	}

	public TipoCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();
	
		try {
			TAB.TipoBeanCol Tipobc = new TAB.TipoBeanCol();
			Tipobc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("TipoBeanColId", Tipobc);

		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
