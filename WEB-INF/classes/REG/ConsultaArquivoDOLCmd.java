package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

/**
* <b>Title:</b>        Download de arquivos GRV e REJ<br>
* <b>Description:</b>  Comando para Download de arquivos<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Elisson Dias	
* @version 1.0
*/

public class ConsultaArquivoDOLCmd extends sys.Command {

  private static final String jspPadrao="/REG/consultaArquivoDOL.jsp";    
  private String next;

  public ConsultaArquivoDOLCmd() {
		next = jspPadrao;
  }

	public ConsultaArquivoDOLCmd(String next) {
		this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
  	
		String nextRetorno  = jspPadrao ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();

			//cria o Bean para guardar os parametros da consulta			
			ConsultaArquivoDOLBean ConsultaArquivoDOLBeanId = (ConsultaArquivoDOLBean) req.getAttribute("ConsultaArquivoDOLBeanId");
			if (ConsultaArquivoDOLBeanId == null) ConsultaArquivoDOLBeanId = new ConsultaArquivoDOLBean();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null)	usrLogado = new ACSS.UsuarioBean();
			
			//Carrega os parametros de sistema da sessao
			ACSS.ParamSistemaBean param = (ACSS.ParamSistemaBean) req.getSession().getAttribute
			("ParamSistemaBeanId");
			String datArqRecebidoBKP = param.getParamSist("DAT_ARQUIVO_RECEBIDO_DOL_BKP");						
						

			Vector vetArquivos 	= new Vector();
			
			String acao = req.getParameter("acao");
			
			String sNomConsulta=req.getParameter("nomConsulta");
			if (sNomConsulta==null) sNomConsulta="";
			
			String sNomArquivoSel=req.getParameter("nomArquivoSel");
			if (sNomArquivoSel==null) sNomArquivoSel="";

			String sDatRecebimento=req.getParameter("datRecebimento");
			if (sDatRecebimento==null) sDatRecebimento="";

			String sStatusArquivo=req.getParameter("statusArquivo");
			if (sStatusArquivo==null) sStatusArquivo="";
			
			ConsultaArquivoDOLBeanId.setNomConsulta(sNomConsulta);			
			ConsultaArquivoDOLBeanId.setNomArquivo(sNomArquivoSel);
			ConsultaArquivoDOLBeanId.setDatRecebimento(sDatRecebimento);
			ConsultaArquivoDOLBeanId.setStatusArquivo(sStatusArquivo);
			
			
			if(acao==null)   acao = "";
			
			boolean consulta=false;
			if (Util.DifereDias(req.getParameter("De"),Util.formatedToday().substring(0,10))>90) consulta=true;

			if (acao.equals("")) {
				ConsultaArquivoDOLBeanId.carregaListaArquivos(usrLogado,consulta);
			}
			else if (acao.equals("consulta")) {
				ConsultaArquivoDOLBeanId.setNomArquivo(req.getParameter("nomArquivo"));
				ConsultaArquivoDOLBeanId.setDataIni(req.getParameter("De"));
				ConsultaArquivoDOLBeanId.setDataFim(req.getParameter("Ate"));				
				ConsultaArquivoDOLBeanId.carregaListaArquivos(usrLogado,consulta);
				ConsultaArquivoDOLBeanId.LeConsulta(usrLogado,consulta);
			}
			else if (acao.equals("Imprime")) {
				ConsultaArquivoDOLBeanId.setNomArquivo(req.getParameter("nomArquivo"));
				ConsultaArquivoDOLBeanId.setDataIni(req.getParameter("De"));
				ConsultaArquivoDOLBeanId.setDataFim(req.getParameter("Ate"));				
				ConsultaArquivoDOLBeanId.carregaListaArquivos(usrLogado,consulta);
				ConsultaArquivoDOLBeanId.LeConsulta(usrLogado,consulta);
				nextRetorno="/REG/consultaArquivoDOLImp.jsp";
			}
			else if (acao.equals("ImprimeLote")) {
				String[] campos={"COD_LOTE"};
				String[] valores={req.getParameter("codLote")};
				String codLote=req.getParameter("codLote");				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
				
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/mostraLinhaLoteDOLImp.jsp";
			}
//			wellem 06-03-2006
			else if (acao.equals("ImprimeDetalhe")) {
				Vector lotes = new Vector();
				Vector Dados= new Vector();
				Vector consultas= new Vector();
				ConsultaArquivoDOLBeanId.setNomArquivo(req.getParameter("nomArquivo"));
				lotes=ConsultaArquivoDOLBeanId.VerificaLotes();
				nextRetorno="/REG/RelatDetalhadoDolImp.jsp";			
			}
//			=====================================
			
			else if (acao.equals("ENVIADOS")) {
				ConsultaArquivoDOLBeanId.setNomConsulta(acao);
				String[] campos={"COD_LOTE"};
				String[] valores={req.getParameter("codLote")};
				String codLote=req.getParameter("codLote");				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
			}
			else if (acao.equals("GRAVADOS")) {
				ConsultaArquivoDOLBeanId.setNomConsulta(acao);
				String[] campos={"COD_LOTE","COD_RETORNO"};
				String[] valores={req.getParameter("codLote"),"000"};
				String condicao= " AND SIT_CANC_DOL IS NULL";
				String codLote=req.getParameter("codLote");				

				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
				
				
				
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,condicao,ConsultaArquivoDOLBeanId.getNomConsulta());
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
			}
			else if (acao.equals("REJEITADOS")) {
				ConsultaArquivoDOLBeanId.setNomConsulta(acao);				
				String[] campos={"COD_LOTE"};
				String[] valores={req.getParameter("codLote")};
				String condicao = " AND COD_RETORNO <> 000" ;
				String codLote=req.getParameter("codLote");				
				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,condicao,ConsultaArquivoDOLBeanId.getNomConsulta());
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
			}		
//			wellem 06-03-2006
			else if (acao.equals("CANCELADOS")) {
				ConsultaArquivoDOLBeanId.setNomConsulta(acao);
				
				String[] campos={"COD_LOTE"};
				String[] valores={req.getParameter("codLote")};
				String condicao = " AND SIT_CANC_DOL = 'S'";				
				String codLote=req.getParameter("codLote");				
				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,condicao,ConsultaArquivoDOLBeanId.getNomConsulta());
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
			}			

//			=====================================			
			
			else if (acao.equals("mostraAuto")) {				
				String codLinhaLote=req.getParameter("codLinhaLote");
				String[] campos={"COD_LINHA_LOTE"};
				String[] valores={codLinhaLote};
				String codLote=req.getParameter("codLote");				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
				
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
				ConsultaArquivoDOLBeanId.CarregaComboUF();
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/acertarAuto.jsp";				
			}
			else if (acao.equals("atualizaMarca")) {
				String numPlaca=req.getParameter("numPlaca");
				//Operacaoes Broker
				ConsultaArquivoDOLBeanId.AtualizaMarcaBroker(numPlaca);
				
				//Mostra auto novamente
				String[] campos={"COD_LINHA_LOTE"};
				String[] valores={req.getParameter("codLinhaLote")};
				String codLote=req.getParameter("codLote");				
				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
				
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());				
				ConsultaArquivoDOLBeanId.CarregaComboUF();				
				ConsultaArquivoDOLBeanId.setNomConsulta(req.getParameter("nomConsulta"));							
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/acertarAuto.jsp";
			}
			else if (acao.equals("acertarAuto")) {
				String codLinhaLote=req.getParameter("codLinhaLote");
				String[] campos={"COD_LINHA_LOTE"};
				String[] valores={codLinhaLote};
				String codLote=req.getParameter("codLote");				
				//Consulta Lote
				LoteDolBean LoteDolBeanId = new LoteDolBean();
				LoteDolBeanId.setCodLote(codLote);
				ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
				//Consulta Arquivo
				ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
				ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
				ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
				
				//LoteDolBeanId e ArquivoDolBeanId OK				
				
				LinhaLoteDolBean LinhaLoteBeanId = new LinhaLoteDolBean();
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
				LinhaLoteBeanId=(LinhaLoteDolBean)ConsultaArquivoDOLBeanId.getVetConsulta().get(0);
				LinhaLoteBeanId.setInd_P59(ArquivoDolBeanId.getDscPortaria59());
				LinhaLoteBeanId.setCodLinhaLote(codLinhaLote);
				LinhaLoteBeanId.setIdentArquivo(LinhaLoteBeanId.getIdentArquivo());
				LinhaLoteBeanId.setTipoRegistro(LinhaLoteBeanId.getTipoRegistro());
				LinhaLoteBeanId.setUsername(LinhaLoteBeanId.getUsername());
				LinhaLoteBeanId.setCodOrgaoLotacao(LinhaLoteBeanId.getCodOrgaoLotacao());
				LinhaLoteBeanId.setTipoTransacao(LinhaLoteBeanId.getTipoTransacao());
				LinhaLoteBeanId.setCodOrgao(LinhaLoteBeanId.getCodOrgao());
				LinhaLoteBeanId.setNumAuto(req.getParameter("numAuto"));
				LinhaLoteBeanId.setNumPlaca(req.getParameter("numPlaca"));
				LinhaLoteBeanId.setCodMarca(req.getParameter("codMarca"));
				LinhaLoteBeanId.setLocalInfracao(req.getParameter("localInfracao"));
				LinhaLoteBeanId.setDataInfracao(req.getParameter("dataInfracao").replaceAll("/",""));
				LinhaLoteBeanId.setHoraInfracao(req.getParameter("horaInfracao").replaceAll(":",""));
				LinhaLoteBeanId.setCodMunicipio(req.getParameter("codMunicipio"));
				LinhaLoteBeanId.setCodInfracao(req.getParameter("codInfracao"));
				LinhaLoteBeanId.setValorMulta(LinhaLoteBeanId.getValorMulta());
				LinhaLoteBeanId.setDataVencimento(LinhaLoteBeanId.getDataVencimento());
				LinhaLoteBeanId.setCodAgente(req.getParameter("codAgente"));
				LinhaLoteBeanId.setMatriculaAgente(LinhaLoteBeanId.getMatriculaAgente());
				LinhaLoteBeanId.setVelocidadePermitida(LinhaLoteBeanId.getVelocidadePermitida());
				LinhaLoteBeanId.setVelocidadeAferida(LinhaLoteBeanId.getVelocidadeAferida());
				LinhaLoteBeanId.setVelocidadeConsiderada(LinhaLoteBeanId.getVelocidadeConsiderada());
				LinhaLoteBeanId.setTipoDispositivo(LinhaLoteBeanId.getTipoDispositivo());
				LinhaLoteBeanId.setIdentAparelho(LinhaLoteBeanId.getIdentAparelho());
				LinhaLoteBeanId.setNumInmetro(LinhaLoteBeanId.getNumInmetro());
				LinhaLoteBeanId.setDataUltimaAfericao(LinhaLoteBeanId.getDataUltimaAfericao());
				LinhaLoteBeanId.setNumCertificado(LinhaLoteBeanId.getNumCertificado());
				LinhaLoteBeanId.setLocalAparelho(LinhaLoteBeanId.getLocalAparelho());
				LinhaLoteBeanId.setEspecieVeiculo(req.getParameter("especieVeiculo"));
				LinhaLoteBeanId.setCategoriaVeiculo(req.getParameter("categoriaVeiculo"));
				LinhaLoteBeanId.setTipoVeiculo(req.getParameter("tipoVeiculo"));
				LinhaLoteBeanId.setCorVeiculo(req.getParameter("corVeiculo"));
				LinhaLoteBeanId.setNomeCondutor(req.getParameter("nomeCondutor"));
				LinhaLoteBeanId.setNumCpfCondutor(req.getParameter("numCpfCondutor"));
				LinhaLoteBeanId.setNumCnhCondutor(req.getParameter("numCnhCondutor"));
				LinhaLoteBeanId.setUFCnhCondutor(req.getParameter("uFCnhCondutor"));
				LinhaLoteBeanId.setEnderecoCondutor(req.getParameter("enderecoCondutor"));
				LinhaLoteBeanId.setNumEnderecoCondutor(req.getParameter("numEnderecoCondutor"));
				LinhaLoteBeanId.setComplementoEnderecoCondutor(req.getParameter("complementoEnderecoCondutor"));
				LinhaLoteBeanId.setMunicipioCondutor(req.getParameter("municipioCondutor"));
				LinhaLoteBeanId.setCepEndereco(req.getParameter("cepEndereco"));
				String txtObs = Util.rPad(req.getParameter("observacao")," ",151);
				LinhaLoteBeanId.setObservacao(txtObs.substring(0,51));
				LinhaLoteBeanId.setDesInfracaoRes(req.getParameter("desInfracaoRes"));
				LinhaLoteBeanId.setComplObservacao(txtObs.substring(51,151));
				
				LinhaLoteBeanId.setLocalInfrDet(req.getParameter("localInfracaoDet"));
				LinhaLoteBeanId.setMarcaModApar(LinhaLoteBeanId.getMarcaModApar());
				LinhaLoteBeanId.setDesMotCanc(LinhaLoteBeanId.getDesMotCanc());
				LinhaLoteBeanId.setIdentPRF(LinhaLoteBeanId.getIdentPRF());
				
				if (ArquivoDolBeanId.getDscPortaria59().equals("P59")){
					LinhaLoteBeanId.setDscLinhaLoteP59();
					LinhaLoteBeanId.setDscLinhaLoteNITP59();
				}else{
					LinhaLoteBeanId.setDscLinhaLote();
					LinhaLoteBeanId.setDscLinhaLoteNIT();
				}
				
				
				ConsultaArquivoDOLBeanId.AcertaAutoDol(LinhaLoteBeanId);
			
				//Operacaoes Broker
				ConsultaArquivoDOLBeanId.AcertaAutoDolBroker(usrLogado, ArquivoDolBeanId, LoteDolBeanId, LinhaLoteBeanId);
				if (!LoteDolBeanId.isUpdate(""))
					throw new DaoException("Erro ao atualizar CodLoteId: " + LoteDolBeanId.getCodLote() + " !");
				
				/*ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());*/
				ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"","");				
				ConsultaArquivoDOLBeanId.CarregaComboUF();
				ConsultaArquivoDOLBeanId.setMsgErro("Auto de Infra��o atualizado com sucesso!");
				req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
				nextRetorno="/REG/acertarAuto.jsp";		
			}
			else if (acao.equals("R")) {
				 if ((req.getParameter("nomConsulta")).equals("ENVIADOS")){
					String[] campos={"COD_LOTE"};
					String[] valores={req.getParameter("codLote")};
					String codLote=req.getParameter("codLote");				
					//Consulta Lote
					LoteDolBean LoteDolBeanId = new LoteDolBean();
					LoteDolBeanId.setCodLote(codLote);
					ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
					//Consulta Arquivo
					ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
					ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
					ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

					ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
					req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
					nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
				 }else if ((req.getParameter("nomConsulta")).equals("GRAVADOS")){
					String[] campos={"COD_LOTE","COD_RETORNO"};
					String[] valores={req.getParameter("codLote"),"000"};
					String codLote=req.getParameter("codLote");				
					//Consulta Lote
					LoteDolBean LoteDolBeanId = new LoteDolBean();
					LoteDolBeanId.setCodLote(codLote);
					ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
					//Consulta Arquivo
					ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
					ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
					ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

					ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,"",ConsultaArquivoDOLBeanId.getNomConsulta());
					req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
					nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
				 }else if ((req.getParameter("nomConsulta")).equals("REJEITADOS")){
					String[] campos={"COD_LOTE"};
					String[] valores={req.getParameter("codLote")};
					String condicao = " AND COD_RETORNO <> 000";
					String codLote=req.getParameter("codLote");				
					//Consulta Lote
					LoteDolBean LoteDolBeanId = new LoteDolBean();
					LoteDolBeanId.setCodLote(codLote);
					ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
					//Consulta Arquivo
					ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
					ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
					ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
					
					
					ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,condicao,ConsultaArquivoDOLBeanId.getNomConsulta());
					req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
					nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
//					wellem 06-03-2006	
				 }else if ((req.getParameter("nomConsulta")).equals("CANCELADOS")){
					    String[] campos={"COD_LOTE"};
						String[] valores={req.getParameter("codLote")};
						String condicao = " AND SIT_CANC_DOL = 'S'";						
						String codLote=req.getParameter("codLote");				
						//Consulta Lote
						LoteDolBean LoteDolBeanId = new LoteDolBean();
						LoteDolBeanId.setCodLote(codLote);
						ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
						//Consulta Arquivo
						ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
						ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
						ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);
					
						ConsultaArquivoDOLBeanId.LeConsulta(campos,valores,condicao,ConsultaArquivoDOLBeanId.getNomConsulta());
					 
						req.setAttribute("ArquivoDolBeanId", ArquivoDolBeanId);
						nextRetorno="/REG/mostraLinhaLoteDOL.jsp";
					
				 }
//===============================================				 
			}
			else if (acao.equals("Voltar")) 
			{
					String codLote=req.getParameter("codLote");				
					//Consulta Lote
					LoteDolBean LoteDolBeanId = new LoteDolBean();
					LoteDolBeanId.setCodLote(codLote);
					ConsultaArquivoDOLBeanId.ConsultaLote(LoteDolBeanId);
					//Consulta Arquivo
					ArquivoDolBean ArquivoDolBeanId = new ArquivoDolBean();
					ArquivoDolBeanId.setCodArquivo(LoteDolBeanId.getCodArquivo());
					ConsultaArquivoDOLBeanId.ConsultaArquivo(ArquivoDolBeanId);

					ConsultaArquivoDOLBeanId.setNomConsulta("consulta");
					ConsultaArquivoDOLBeanId.carregaListaArquivos(usrLogado,consulta);
					ConsultaArquivoDOLBeanId.LeConsulta(usrLogado,consulta);
		   }
		   req.setAttribute("ConsultaArquivoDOLBeanId", ConsultaArquivoDOLBeanId);
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}
