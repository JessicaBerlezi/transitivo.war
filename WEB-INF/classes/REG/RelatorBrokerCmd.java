package REG;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import REC.JuntaBean;
import REC.RelatorBean;
/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Relator<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class RelatorBrokerCmd extends sys.Command {


  private static final String jspPadrao="/REG/RelatorBroker.jsp";    
  private String next;

  public RelatorBrokerCmd() {
    next = jspPadrao;
  }

  public RelatorBrokerCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {   
  
		String acao = req.getParameter("acao");  
 		if(acao == null)
		  acao =" ";   
 		
	    String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao == null) 
		  codOrgao =""; 
		
	    String codJunta = req.getParameter("codJunta"); 		
		if(codJunta == null) 
		  codJunta =""; 
    	
		// cria os Beans do org�o, se n�o existir
		ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null)
		  OrgaoId = new ACSS.OrgaoBean() ;	 
		
		// cria os Beans da Junta, se n�o existir
		JuntaBean JuntaId = (JuntaBean)req.getAttribute("JuntaId") ;
		if(JuntaId == null)
		  JuntaId = new JuntaBean() ;	

		RelatorBean RelatorId = (RelatorBean)req.getAttribute("RelatorId") ;
		if(RelatorId == null)
			RelatorId = new RelatorBean();	
							
		String atualizarDependente ="N";  
		
	    if("buscaRelator".equals(acao)) {
	      Vector errosPosicoes = new Vector();	    
	      RelatorId.setCodJunta(codJunta);
	      RelatorId.setCodOrgao(codOrgao);
	      List relatorSmit = RelatorId.getJuntaRelator();
 	      List relatorBroker = RelatorId.getRelatorBroker();
 	      if( !RelatorId.compareSmitBroker(relatorSmit,relatorBroker) )
 	      	atualizarDependente = "N";
 	      else{
 	      	atualizarDependente = "S";
 	      	OrgaoId.Le_Orgao(codOrgao,0);
 	      	JuntaId.Le_Junta(codJunta,0);
 	      	req.setAttribute("RelatorBrokerId",relatorBroker) ;
 	      	req.setAttribute("RelatorSmitId",relatorSmit) ;
 	      }
	    }
		Vector vErro = new Vector(); 
		 
		if(acao.compareTo("A") == 0){
			String[] nomRelator_aux 		= req.getParameterValues("nomRelator_aux");
			String[] nomRelator 			= new String[nomRelator_aux.length];			

			String[] numCpf_aux				= req.getParameterValues("numCpf_aux");
			String[] numCpf     			= new String[numCpf_aux.length];
	
			String[] codRelator 			= req.getParameterValues("codRelator");
			String[] codRelatorBroker 		= req.getParameterValues("codRelatorBroker");
			 
			String[] nomRelatorBroker_aux  	= req.getParameterValues("nomRelatorBroker_aux");
			String[] nomRelatorBroker      	= new String[nomRelatorBroker_aux.length];
			
			String[] numCpfBroker_aux 		= req.getParameterValues("numCpfBroker_aux");
			String[] numCpfBroker     		= new String[numCpfBroker_aux.length];
			
			for(int i=0; i< codRelator.length;i++){
				nomRelator[i] 	 	= req.getParameter("nomRelator"+i);
				numCpf[i]     		= req.getParameter("numCpf"+i);
				nomRelatorBroker[i] = req.getParameter("nomRelatorBroker"+i);
				numCpfBroker[i]   	= req.getParameter("numCpfBroker"+i);
			}
				
			List relatorSmit       = new ArrayList(); 
			List relatorSmit_aux   = new ArrayList();
			List relatorBroker     = new ArrayList();
			List relatorBroker_aux = new ArrayList();
			RelatorBean myRelator, myRelator_aux, myRelatorBroker, myRelatorBroker_aux;
			for (int i = 0; i < nomRelator.length;i++) {
				myRelator           = new RelatorBean() ;
				myRelator_aux       = new RelatorBean() ;
				myRelatorBroker     = new RelatorBean() ;
				myRelatorBroker_aux = new RelatorBean() ;
				myRelator.setCodOrgao(codOrgao);
				myRelator.setCodRelator(codRelator[i]);
				myRelator.setNomRelator(nomRelator[i]);
				myRelator_aux.setNomRelator(nomRelator_aux[i]);
				myRelatorBroker.setCodOrgao(codOrgao);
				myRelatorBroker.setCodRelator(codRelatorBroker[i]);
				myRelatorBroker.setNomRelator(nomRelatorBroker[i]);
				myRelatorBroker_aux.setCodOrgao(codOrgao);
				myRelatorBroker_aux.setNomRelator(nomRelatorBroker_aux[i]);
				myRelator.setNumCpfEdt(numCpf[i]);
				myRelator_aux.setNumCpfEdt(numCpf_aux[i]);
				myRelatorBroker.setNumCpfEdt(numCpfBroker[i]);
				myRelatorBroker_aux.setNumCpfEdt(numCpfBroker_aux[i]);
				myRelator.setCodJunta(codJunta);
				myRelatorBroker.setCodJunta(codJunta);
				myRelatorBroker_aux.setCodJunta(codJunta);
				
				relatorSmit.add(myRelator);
				relatorSmit_aux.add(myRelator_aux);
				relatorBroker.add(myRelatorBroker);
				relatorBroker_aux.add(myRelatorBroker_aux);
			}
			RelatorId = new RelatorBean(); 
			if( RelatorId.isInsertRelatorBroker(relatorSmit, relatorSmit_aux, relatorBroker, relatorBroker_aux) )
				RelatorId.setMsgErro("Dados Alterados com Sucesso");
			atualizarDependente = "S";
			OrgaoId.Le_Orgao(codOrgao,0);
			JuntaId.Le_Junta(codJunta,0);
			req.setAttribute("RelatorBrokerId",relatorBroker) ;
			req.setAttribute("RelatorSmitId",relatorSmit) ;	
		}
		req.setAttribute("atualizarDependente",atualizarDependente);	
		req.setAttribute("OrgaoId",OrgaoId);
		req.setAttribute("JuntaId",JuntaId);
		req.setAttribute("RelatorId",RelatorId);
    }
    catch (Exception ue) {
      throw new sys.CommandException("JuntaBrokerCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
}