package REG;

import java.util.Vector;

import sys.BeanException;
import ROB.DaoException;

public class RelatArqNotificacaoBean extends sys.HtmlPopupBean {

	private String codArquivo;
	private String nomArquivo;
	private String codStatus;
	private String qtdReg;
	private String totalQtdReg;	
	private String qtdLinhaPendente;
	private String totalQtdLinhaPendente;
	private String qtdLinhaCancelada;
	private String totalQtdLinhaCancelada;	
	private String qtdRecebido;
	private String totalQtdRecebido;
	private String numProtocolo;
	private String datRecebimento;
	private String tarNomUsername;
	private String tarCodOrgaoLotacao;
	private String nomUsername;
	private String codOrgaoLotacao;
	private String datDownload;
	private String indControleVEX;
	private String sitControleVEX;
	private String identificadorArquivo;

	private String datIni;
	private String datFim;

	private Vector listaArq; 
	private String msgErro;

	public RelatArqNotificacaoBean() throws sys.BeanException {
		totalQtdReg		 ="";
		totalQtdLinhaPendente ="";
		totalQtdLinhaCancelada ="";
		totalQtdRecebido ="";
		codArquivo       ="";
		nomArquivo       ="";
		codStatus        ="";
		qtdReg           ="";
		qtdLinhaPendente ="";
		qtdLinhaCancelada="";
		qtdRecebido      ="";
		numProtocolo     ="";
		datRecebimento   ="";
		nomUsername      ="";
		codOrgaoLotacao  ="";
		datDownload      ="";
		msgErro          ="";
		indControleVEX   ="";		
		sitControleVEX   ="";
		datIni           ="";
		datFim           ="";
		tarNomUsername   ="";
		tarCodOrgaoLotacao="";
		listaArq = new Vector(); 
		identificadorArquivo="EMITEVEX"; //EMITEVEX ou EMITEPER
	}
	public void setTotalQtdReg(String s) {
		this.totalQtdReg = s;
		if( s == null )	this.totalQtdReg = "";
	}
	public String getTotalQtdReg() { return this.totalQtdReg; }
	public void setTotalQtdLinhaPendente(String s) {
		this.totalQtdLinhaPendente = s;
		if( s == null )	this.totalQtdLinhaPendente = "";
	}
	public String getTotalQtdLinhaPendente() { return this.totalQtdLinhaPendente; }
	public void setTotalQtdLinhaCancelada(String s) {
		this.totalQtdLinhaCancelada = s;
		if( s == null )	this.totalQtdLinhaCancelada = "";
	}
	public String getTotalQtdLinhaCancelada() { return this.totalQtdLinhaCancelada; }	
	public void setTotalQtdRecebido(String s) {
		this.totalQtdRecebido = s;
		if( s == null )	this.totalQtdRecebido = "";
	}
	public String getTotalQtdRecebido() { return this.totalQtdRecebido; }
	public void setTarNomUsername(String s) {
		this.tarNomUsername = s;
		if( s == null )	this.tarNomUsername = "";
	}
	public String getTarNomUsername() { return this.tarNomUsername; }
	public void setTarCodOrgaoLotacao(String s) {
		this.tarCodOrgaoLotacao = s;
		if( s == null )	this.tarCodOrgaoLotacao = "";
	}
	public String getTarCodOrgaoLotacao() { return this.tarCodOrgaoLotacao; }
	public void setNumProtocolo(String s) {
		this.numProtocolo = s;
		if( s == null )	this.numProtocolo = "";			  
	}
	public String getNumProtocolo() { return sys.Util.lPad(this.numProtocolo, "0", 5); }
	public void setDatIni(String s) {
		this.datIni = s;
		if( s == null )	this.datIni = "";
	}
	public String getDatIni() { return this.datIni; }
	public void setDatFim(String s) {
		this.datFim = s;
		if( s == null )	this.datFim = "";
	}
	public String getDatFim() { return this.datFim; }
	public void setCodArquivo(String s) {
		this.codArquivo = s;
		if( s == null )	this.codArquivo = "";
	}
	public String getCodArquivo() { return this.codArquivo; }
	public void setNomArquivo(String s) {
		this.nomArquivo = s;
		if (s == null) this.nomArquivo = "";
	}
	public String getNomArquivo() { return this.nomArquivo; }
	public void setCodStatus(String s) {
		this.codStatus = s;
		if (s == null) this.codStatus = "";
	}
	public String getCodStatus() { return this.codStatus; }
	public void setQtdReg(String s) {
		this.qtdReg = s;
		if (s == null) this.qtdReg = "";
	}
	public String getQtdReg() { return this.qtdReg; }
	public void setQtdLinhaPendente(String s) {
		this.qtdLinhaPendente = s;
		if (s == null) this.qtdLinhaPendente = "";
	}
	public String getQtdLinhaPendente() { return this.qtdLinhaPendente;	}
	public void setQtdLinhaCancelada(String s) {
		this.qtdLinhaCancelada = s;
		if (s == null) this.qtdLinhaCancelada = "";
	}
	public String getQtdLinhaCancelada() { return this.qtdLinhaCancelada;	}
	public void setQtdRecebido(String s) {
		this.qtdRecebido = s;
		if (s == null) this.qtdRecebido = "";
	}
	public String getQtdRecebido() { return this.qtdRecebido; }
	public void setDatRecebimento(String s) {
		this.datRecebimento = s;
		if (s == null) this.datRecebimento = "";
	}
	public String getDatRecebimento() {	return this.datRecebimento;	}
	public void setNomUsername(String s) {
		this.nomUsername = s;
		if (s == null) this.nomUsername = "";
	}
	public String getNomUsername() { return this.nomUsername; }
	public void setCodOrgaoLotacao(String s) {
		this.codOrgaoLotacao = s;
		if(s == null) this.codOrgaoLotacao = "";
	}
	public String getCodOrgaoLotacao() { return this.codOrgaoLotacao; }
	public void setDatDownload(String s) {
		this.datDownload = s;
		if(s == null) this.datDownload = "";
	}
	public String getDatDownload() { return this.datDownload; }
	public void setMsgErro( Vector vErro ) {
	   for (int j=0; j<vErro.size(); j++)  this.msgErro += vErro.elementAt(j) ;
	}
	public void setMsgErro( String msgErro ) {
	   this.msgErro = msgErro;
	}
	public String getMsgErro() {	return this.msgErro; }
	public void setListaArqs(Vector listaArqs) { 
		this.listaArq = listaArqs; 
	}
	public Vector getListaArqs(){ return this.listaArq; }
	public RelatArqNotificacaoBean getListaArqs(int i) { 
	  return ( RelatArqNotificacaoBean )this.listaArq.elementAt(i); 
	}   
	public boolean ConsultaRelDiario(String datIni,String datFim, Vector vErro ) throws  BeanException, DaoException, DaoException
	{
		boolean existe = false;
		Dao dao = Dao.getInstance();
		try {
		 	 if(dao.ConsultaRelDiario(this,datIni,datFim, this.identificadorArquivo)== true) 
		 	    existe = true;
		 	 else
				 vErro.addElement("N�o existe(m) registro(s) entre as datas informadas para forma��o do relat�rio! \n");
			} 
			catch (Exception e) { throw new sys.BeanException( e.getMessage() ); } 
			
		setMsgErro(vErro);
		return existe;	
	}
	public String getIndControleVEX() {
		return indControleVEX;
	}
	public void setIndControleVEX(String indControleVEX) {
		if (indControleVEX==null) indControleVEX="";
		this.indControleVEX = indControleVEX;
	}
	public String getSitControleVEX() {
		return sitControleVEX;
	}
	public void setSitControleVEX(String sitControleVEX) {
		if (sitControleVEX==null) sitControleVEX="";
		this.sitControleVEX = sitControleVEX;
	}
	public String getIdentificadorArquivo() {
		return identificadorArquivo;
	}
	public void setIdentificadorArquivo(String identificadorArquivo) {
		this.identificadorArquivo = identificadorArquivo;
	}
	
}