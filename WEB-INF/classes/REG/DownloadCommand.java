package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Download de arquivos GRV e REJ<br>
 * <b>Description:</b>  Comando para Download de arquivos<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author 			   Elisson Dias	
 * @version 1.0
 */

public class DownloadCommand extends sys.Command {
	
	private static final String jspPadrao="/REG/downloadArquivo.jsp";    
	private String next;
	
	public DownloadCommand() {
		next = jspPadrao;
	}
	
	public DownloadCommand(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		
		String nextRetorno = jspPadrao ;
		String msgErro="";
		try {       										
			HttpSession session = req.getSession();
			
			ACSS.UsuarioBean usrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (usrLogado == null) usrLogado = new ACSS.UsuarioBean();
						
			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();
			
			//cria o Bean para guardar os parametros da consulta			
			ConsultaDownloadBean ConsultaDownloadBeanId = (ConsultaDownloadBean) req.getAttribute("ConsultaDownloadBeanId");
			if (ConsultaDownloadBeanId == null) ConsultaDownloadBeanId = new ConsultaDownloadBean();
			
			String sigFuncao = usrFunc.getJ_sigFuncao();
			Vector vetArquivos 	= new Vector();
			String Origem ="";
			String acao = req.getParameter("acao");  
			if(acao==null) acao = "";
			
			if("REG2066".equals(sigFuncao))
				 Origem = sigFuncao;
			
			if (acao.equals("") || acao.equals("buscaDownloads")) {
				if (req.getParameter("De")!=null)
					ConsultaDownloadBeanId.setDataIni(req.getParameter("De"));
				if(req.getParameter("Ate")!=null)
					ConsultaDownloadBeanId.setDataFim(req.getParameter("Ate"));					
				
				ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
				arqRec.setArqFuncao(sigFuncao);
				
				if("REG2067".equals(sigFuncao)){//EMITEPONTO VEX
					Dao dao = Dao.getInstance();
					vetArquivos=dao.BuscaArquivosProcessadosPontuacao(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
							getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
					
				}else if("REG2068".equals(sigFuncao)){//EMITEPONTO PERRONE
						Dao dao = Dao.getInstance();
						vetArquivos=dao.BuscaArquivosProcessadosPontuacao(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
								getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
						
				}else{
					Dao dao = Dao.getInstance();
					vetArquivos=dao.BuscaArquivosProcessados(usrLogado,arqRec.getArqFuncao(),ConsultaDownloadBeanId.
							getDataIni(),ConsultaDownloadBeanId.getDataFim(),Origem);
					
				}
			}
			req.setAttribute("ConsultaDownloadBeanId",ConsultaDownloadBeanId);
			req.setAttribute("msgErro",msgErro);						
			req.setAttribute("vetArquivos",vetArquivos);
			req.setAttribute("sigFuncao",sigFuncao);	
			
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	} 
}
