package REG;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
/**
 * Relat�rio de Estatistica de Digitaliza��o
 * @author loliveira
 */
public class EstatisticaDigitalizaCmd  extends sys.Command {
	private static final String jspPadrao="/REG/EstatisticaDigitaliza.jsp";    
	private String next;
	
	public EstatisticaDigitalizaCmd() {
		next = jspPadrao;
	}
	
	public EstatisticaDigitalizaCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
			
			// cria os Beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
 		    if(OrgaoId == null)	OrgaoId = new ACSS.OrgaoBean() ;	 		
 		    
 		    EstatisticaDigitalizaBean EstatisticaBean = (EstatisticaDigitalizaBean)req.getAttribute("EstatisticaBean") ;
		    if(EstatisticaBean == null) EstatisticaBean = new EstatisticaDigitalizaBean() ;	 		
			
			String acao = req.getParameter("acao");  
			if(acao == null)acao = "";
			if(acao.equals(""))	acao =" "; 
			
			String statusAuto = req.getParameter("statusAuto");
			if (statusAuto == null) statusAuto = "";
			EstatisticaBean.setTipoSolicitacao(statusAuto);
			
			String todosOrgaos = req.getParameter("todosOrgaos");
			if (todosOrgaos == null) todosOrgaos = "";
			EstatisticaBean.setTodosOrgaos(todosOrgaos);
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			EstatisticaBean.setCodOrgao(codOrgao);
			
			String mesAnoInicio = req.getParameter("mesAnoInicio");
			if (mesAnoInicio == null) mesAnoInicio = "";
			EstatisticaBean.setDatIncio(mesAnoInicio);
			
			String mesAnoFim = req.getParameter("mesAnoFim");
			if (mesAnoFim == null) mesAnoFim = "";
			EstatisticaBean.setDatFim(mesAnoFim);
			
			if (acao.equals("visualizacao"))
			{
				EstatisticaBean.getRegistrados(EstatisticaBean);
				EstatisticaBean.getEmitidos(EstatisticaBean);
				EstatisticaBean.getDigitalizados(EstatisticaBean);
				EstatisticaBean.getArEntregues(EstatisticaBean);
				EstatisticaBean.getArDigitalizados(EstatisticaBean);
				OrgaoId.Le_Orgao(codOrgao,0);
				if(EstatisticaBean.getTodosOrgaos().length()>=0)
					OrgaoId.setSigOrgao("TODOS");
				next = "/REG/EstatisticaDigitalizaResult.jsp";
			}
		   
			
			req.setAttribute("EstatisticaBean",EstatisticaBean);
			req.setAttribute("statusAuto",statusAuto);
			req.setAttribute("OrgaoId",OrgaoId);

		}
		catch (Exception ue) {
			throw new sys.CommandException(ue.getMessage());
		}
		return next;
	}
}
