package REG;

import java.text.DecimalFormat;
import java.util.ArrayList;

import sys.BeanException;
import ROB.DaoException;


public class  EstatisticaDigitalizaBean extends sys.HtmlPopupBean   {

    private String tipoSolicitacao;
    private String todosOrgaos;
	private String datIncio;
    private String datFim;
    private String flag;
    private String datRec;
    private String codOrgao;
    private String codSistema;
    private String sigOrgao;
    
    private int qtdMR01;
    private int qtdDOL;
    private int qtdEmitidos;
    private int qtdPendencia;
    private int qtdDigitalizados;
    private int qtdNaoDigitalizados;
    private int qtdArRecebido;
    private int qtdArNaoRecebido;
    private int qtdArDigitalizados;
    private int qtdArNaoDigitalizados;
    private int qtdTotalMR01;
    private int qtdTotalDOL;
    
    private ArrayList arquivos;
    
    
	public EstatisticaDigitalizaBean() throws sys.BeanException {

		super();
	    tipoSolicitacao = "";
	    codSistema = "";
	    todosOrgaos = "";
		datIncio = "";
	    datFim = "";
	    flag = "";
	    datRec = "";
	    
	    qtdMR01 = 0;
	    qtdDOL = 0;
	    qtdEmitidos = 0;
	    qtdPendencia = 0;
	    qtdDigitalizados = 0;
	    qtdNaoDigitalizados = 0;
	    qtdArRecebido = 0;
	    qtdArNaoRecebido = 0;
	    qtdArDigitalizados = 0;
	    qtdArNaoDigitalizados = 0;
	    qtdTotalMR01 = 0;
	    qtdTotalDOL = 0;

	    
	    arquivos = new ArrayList();
	}

//	---------------------------------------------------------------------------------------
	public void setArquivos(ArrayList arquivos) {
		if(arquivos == null) arquivos = new ArrayList();
		else this.arquivos = arquivos;
	}
	public ArrayList getArquivos() {
		return arquivos;
	}

//	---------------------------------------------------------------------------------------
    public void setDatFim(String datFim) {
        if (datFim == null) datFim = "";
        else this.datFim = datFim;
	}
	public String getDatFim() {
		return datFim;
	}

//	---------------------------------------------------------------------------------------
	public void setDatIncio(String datIncio) {
		if (datIncio == null) datIncio = "";
		else this.datIncio = datIncio;
	}
	public String getDatIncio() {
		return datIncio;
	}

//	---------------------------------------------------------------------------------------
	public void setTipoSolicitacao(String tipoSolicitacao) {
		if (tipoSolicitacao == null) tipoSolicitacao = "";
		else this.tipoSolicitacao = tipoSolicitacao;
	}
	public String getTipoSolicitacao() {
		return tipoSolicitacao;
	}

//	---------------------------------------------------------------------------------------
	public void setTodosOrgaos(String todosOrgaos) {
		if(todosOrgaos == null) todosOrgaos = "";
		else this.todosOrgaos = todosOrgaos;
	}
	public String getTodosOrgaos() {
		return todosOrgaos;
	}

//	---------------------------------------------------------------------------------------
	public void setFlag(String flag) {
		if (flag == null) flag = "";
		else this.flag = flag;
	}
	public String getFlag() {
		return flag;
	}

//	---------------------------------------------------------------------------------------	
	public void setQtdDOL(int qtdDOL) {
		this.qtdDOL = qtdDOL;
	}
	public int getQtdDOL() {
		return qtdDOL;
	}

//	---------------------------------------------------------------------------------------
	public void setQtdMR01(int qtdMR01) {
		this.qtdMR01 = qtdMR01;
	}
	public int getQtdMR01() {
		return qtdMR01;
	}
	
//------------------------------------------------------------------------------------------	
	public void setQtdPendencia(int qtdPendencia) {
		this.qtdPendencia = qtdPendencia;
	}
	public int getQtdPendencia() {
		return qtdPendencia;
	}
	
//------------------------------------------------------------------------------------------
	public void setQtdEmitidos(int qtdEmitidos) {
		this.qtdEmitidos = qtdEmitidos;
	}
	public int getQtdEmitidos() {
		return qtdEmitidos;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdDigitalizados(int qtdDigitalizados) {
		this.qtdDigitalizados = qtdDigitalizados;
	}
	public int getQtdDigitalizados() {
		return qtdDigitalizados;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdNaoDigitalizados(int qtdNaoDigitalizados) {
		this.qtdNaoDigitalizados = qtdNaoDigitalizados;
	}
	public int getQtdNaoDigitalizados() {
		return qtdNaoDigitalizados;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdArNaoRecebido(int qtdArNaoRecebido) {
		this.qtdArNaoRecebido = qtdArNaoRecebido;
	}
	public int getQtdArNaoRecebido() {
		return qtdArNaoRecebido;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdArRecebido(int qtdArRecebido) {
		this.qtdArRecebido = qtdArRecebido;
	}
	public int getQtdArRecebido() {
		return qtdArRecebido;
	}
	
//	------------------------------------------------------------------------------------------
	public void setQtdArDigitalizados(int qtdArDigitalizados) {
		this.qtdArDigitalizados = qtdArDigitalizados;
	}
	public int getQtdArDigitalizados() {
		return qtdArDigitalizados;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdArNaoDigitalizados(int qtdArNaoDigitalizados) {
		this.qtdArNaoDigitalizados = qtdArNaoDigitalizados;
	}
	public int getQtdArNaoDigitalizados() {
		return qtdArNaoDigitalizados;
	}
	
//	------------------------------------------------------------------------------------------
	public void setQtdTotalDOL(int qtdTotalDOL) {
		this.qtdTotalDOL = qtdTotalDOL;
	}
	public int getQtdTotalDOL() {
		return qtdTotalDOL;
	}

//	------------------------------------------------------------------------------------------
	public void setQtdTotalMR01(int qtdTotalMR01) {
		this.qtdTotalMR01 = qtdTotalMR01;
	}
	public int getQtdTotalMR01() {
		return qtdTotalMR01;
	}

//  -----------------------------------------------------------------------------------------	
	public void setDatRec(String datRec) {
		if (datRec == null) datRec ="";
		else this.datRec = datRec;
	}
	public String getDatRec() {
		return datRec;
	}
	
//  -----------------------------------------------------------------------------------------	
	public void setCodOrgao(String codOrgao) {
		if (codOrgao == null) codOrgao = "";
		else this.codOrgao = codOrgao;
	}
	public String getCodOrgao() {
		return codOrgao;
	}

//  -----------------------------------------------------------------------------------------
	public void setSigOrgao(String sigOrgao) {
		if (sigOrgao == null) sigOrgao = "";
		else this.sigOrgao = sigOrgao;
	}
	public String getSigOrgao() {
		return sigOrgao;
	}

//  -----------------------------------------------------------------------------------------	
	public void setCodSistema(String codSistema) {
		if (codSistema == null) codSistema = "";
		else this.codSistema = codSistema;
	}
	public String getCodSistema() {
		return codSistema;
	}
	
//  -----------------------------------------------------------------------------------------

    /**
     * Retorna listagem com a quantidade de autos registrados - MR01 e DOL
     * @param EstatisticaBean
     * @throws DaoException
     * @throws DaoException 
     */
	public void getRegistrados(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException, DaoException{
		try {
			Dao dao = Dao.getInstance();
			dao.getRegistrados(EstatisticaBean);
			
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * Retorna listagem com a quantidade de autos que foram emitidos com sucesso 
	 * e as pendÍncias - EMITVEX
	 * @param EstatisticaBean
	 * @throws DaoException
	 */
	public void getEmitidos(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException{
		try {
			Dao dao = Dao.getInstance();
			dao.getEmitidos(EstatisticaBean);
			
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
    /**
     * Retorna listagem com a quantidade de autos que foram digitalizados
     * com sucesso e as pendencias .
     * @param EstatisticaBean
     * @throws DaoException
     */
	public void getDigitalizados(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException{
		try {
			Dao dao = Dao.getInstance();
			dao.getDigitalizados(EstatisticaBean);
			
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * Retorna listagem com a quantidade de autos que tiveram o AR entregues 
	 * com sucesso e pendÍncias - CODRET
	 * @param EstatisticaBean
	 * @throws DaoException
	 */
	public void getArEntregues(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException{
		try {
			Dao dao = Dao.getInstance();
			dao.getArEntregues(EstatisticaBean);
			
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	/**
	 * Retorna listagem de autos com a quantidade de AR digitalizado 
	 * com sucesso e pendencias
	 * @param EstatisticaBean
	 * @throws DaoException
	 */
	public void getArDigitalizados(EstatisticaDigitalizaBean EstatisticaBean) throws DaoException{
		try {
			Dao dao = Dao.getInstance();
			dao.getArDigitalizados(EstatisticaBean);
			
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	

	public String getMesExtenso(String mes){
		String mesExtenso = "";
		String [] meses  = {"","JAN","FEV","MAR","ABR","MAI","JUN","JUL","AGO","SET","OUT","NOV","DEZ"};
		int i = Integer.parseInt(mes.substring(0,2));
		mesExtenso = meses[i];
		return mesExtenso;
	}


	public String formataIntero (int valor) throws  DaoException, BeanException{
		  String valorFormatado = "";
		  if (valor != 0){
			  DecimalFormat formato = new DecimalFormat(",#00") ;
			  valorFormatado = formato.format(valor); 
		  }
		  else valorFormatado = "0";
		  return valorFormatado;
	}
  

}
