package REG;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CancelarEnvioArquvioCmd  extends sys.Command {
	
	private static final String jspPadrao = "/REG/CancelarEnvioArquivo.jsp";
	private String next;
	private String statusInteracao = null;
	
	public CancelarEnvioArquvioCmd() {
		next = jspPadrao;
	}
	
	public CancelarEnvioArquvioCmd(String next) {
		this.next = jspPadrao;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		try {     
			//cria os beans de sessao, se n�o existir
			HttpSession session = req.getSession();
			
			ArquivoRecebidoBean ArquivoRecId = new  ArquivoRecebidoBean();
			
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado==null) UsrLogado = new ACSS.UsuarioBean();	  									
			
			//cria os beans do org�o, se n�o existir
			ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId");
			if (OrgaoId == null) OrgaoId = new ACSS.OrgaoBean();
						
			String identArquivos[][] = {{"EMITEVEX","Emite VEX"}};
			req.setAttribute("identArquivos",identArquivos);
			
			String acao = req.getParameter("acao");
			if (acao == null) acao = "";

			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = UsrLogado.getCodOrgaoAtuacao();									
			
			String codIdentArquivo = req.getParameter("codIdentArquivo");
			if (codIdentArquivo == null) codIdentArquivo = "EMITEVEX";			
			
			if (acao.equalsIgnoreCase("selecaoOrgao")) {				
				ArquivoRecId.setCodOrgao(codOrgao);
				ArquivoRecId.setCodIdentArquivo(codIdentArquivo);
				List arquivos = ArquivoRecId.buscaArquivosTemp();
				req.setAttribute("arquivos",arquivos);
			}
			
			else if(acao.equalsIgnoreCase("selecaoArquivo")){
				ArquivoRecId.setCodArquivo(req.getParameter("codArquivo"));
				if (ArquivoRecId.isDeleteTemp())
					ArquivoRecId.setMsgErro("Arquivo Cancelado com Sucesso !");	
				
				ArquivoRecId.setCodOrgao(codOrgao);
				ArquivoRecId.setCodIdentArquivo(codIdentArquivo);
				List arquivos = ArquivoRecId.buscaArquivosTemp();
				req.setAttribute("arquivos",arquivos);
			}
			
			req.setAttribute("OrgaoId",OrgaoId);
			req.setAttribute("ArquivoId",ArquivoRecId);
			req.setAttribute("codOrgao",codOrgao);
			req.setAttribute("codIdentArquivo",codIdentArquivo);
		}
		catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return next;
	}
}
