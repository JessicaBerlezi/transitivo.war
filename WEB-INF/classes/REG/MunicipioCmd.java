package REG;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
/**
* <b>Title:</b>        Consulta - Consultas Municipio<br>
* <b>Description:</b>  Comando para Consultar Municipios<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem 
* @version 1.0
*/

public class MunicipioCmd extends sys.Command {

	private static final String jspPadrao = "/REG/MunicipioConsulta.jsp";

	private String next;

	public MunicipioCmd() {
		next = jspPadrao;
	}

	public MunicipioCmd(String next) {
		this.next = next;

	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		Vector vCod = new Vector();
		Vector vNom = new Vector();

		try {

			TAB.MunicipioBeanCol Municipiobc = new TAB.MunicipioBeanCol();
			Municipiobc.carregarCol("TODOS", vCod, vNom);
			req.setAttribute("MunicipioBeanColId", Municipiobc);

		} catch (Exception se) {
			throw new sys.CommandException(se.getMessage());
		}

		return nextRetorno;
	}

}
