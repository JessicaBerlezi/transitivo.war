package sys;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Title: </b>sys.PersistenteBean<br>
 * <b>Description: </b>Bean com m�todos padr�es para persist�ncia no banco<br>
 * <b>Company: </b>MIND Informatica<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public abstract class PersistenteBean extends Bean{
	
	protected Integer id;
	protected List beans;	
	
	public PersistenteBean() {
		super();
		id = null;
		beans = new ArrayList();
	}
	
	public Integer getId() {
		return this.id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public List getBeans() {
		return this.beans;
	}
	public void setBeans(List beans) {
		this.beans = beans;
	}
	public PersistenteBean getBean(int i) {
		return (PersistenteBean) this.beans.get(i); 
	}	
	
	
}