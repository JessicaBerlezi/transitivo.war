package sys;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MonitorLog extends HtmlPopupBean{
	
  public  final String DES_BASE = "B";
  public  final String DES_ARQUIVO = "T";
  public  final String DES_AMBOS = "A";
  public  final String MSG_INFO = "INFO";
  public  final String MSG_ERRO = "ERRO";
  public  final String MSG_INICIO = "INI";
  public  final String MSG_FIM = "FIM";
  public  final String MSG_INTERRUPCAO = "PARAR";
  public  final String MSG_AVISO = "AVISO";
  		
  private String nomArquivo;
  private String nomProcesso;
  private String nomObjeto;
  private String datLog;
  private String txtMensagem;
  private String codOrgao;
  private String sigOrgao;
  
  private String erroBase;
  private String destino;
  Connection conn;
  
  public MonitorLog(){
  	super();  	
	nomArquivo = "";
	nomProcesso = "";
	nomObjeto = "";
	datLog = "";
	txtMensagem = "";
	erroBase = "";
	codOrgao = "";
	sigOrgao = "";
	destino = "";
	conn = null;
  }
  
  public MonitorLog(String nomArquivo) {
    this.nomArquivo = nomArquivo;
	nomProcesso = "";
	nomObjeto = "";
	datLog = "";
	txtMensagem = "";
	erroBase = "";
	codOrgao = "";
	destino = "";
	conn = null;
  }
  
  public void setConn(Connection conn)  {
	  this.conn=conn ;
  }  
  public Connection getConn()  {
	  return this.conn;
  }
   
  public void setCodOrgao(String codOrgao){
	 this.codOrgao = codOrgao;
  }
  public String getCodOrgao(){
	 return this.codOrgao;
  }
  
  public void setSigOrgao(String sigOrgao){
	 this.sigOrgao = sigOrgao;
  }
  public String getSigOrgao(){
	 return this.sigOrgao;
  }
  
  public void setNomProcesso(String nomProcesso){
  	this.nomProcesso = nomProcesso;
  }
  public String getNomProcesso(){
  	return this.nomProcesso.trim();
  }
  
  public void setNomObjeto(String nomObjeto){
  	this.nomObjeto = nomObjeto;
  }
  public String getNomObjeto(){
  	 return this.nomObjeto.trim() ;
  }
  
  public void setDatLog(String datLog){
  	this.datLog = datLog;
  }
  public String getDatLog(){
  	return this.datLog;
  }
  
  public void setTxtMensagem(String txtMensagem){
  	this.txtMensagem = txtMensagem;
  }
  public String getTxtMensagem(){
  	return this.txtMensagem;
  }
  
  public void setDestino(String destino){
	 this.destino = destino;
   }
   public String getDestino(){
	 return this.destino;
   }
  
  public void setErroBase(String erroBase){
  	this.erroBase = erroBase;
  }
  public String getErroBase(){
  	return this.erroBase;
  }
  
  public void iniciaMonitor(String iniMens) throws DaoException{	
	gravaMonitor(iniMens, this.MSG_INICIO);
  } 
  
  public void finalizaMonitor(String fimMens) throws DaoException{
	gravaMonitor(fimMens, this.MSG_FIM); 
  }
  
  public void gravaMonitor(String texto,String tipoMsg) throws DaoException{
	String gravaEm = getDestino();
  	// Parametros pr�-definidos para destino 
	if (gravaEm.equals(DES_ARQUIVO)) gravaMonitorArquivo(texto,tipoMsg);
	if (gravaEm.equals(DES_BASE)) gravaMonitorBase(texto, tipoMsg);
	if (gravaEm.equals(DES_AMBOS)) gravaMonitorAmbos(texto, tipoMsg);
 }
 
  public void gravaMonitorAmbos(String texto, String tipoMsg) throws DaoException{
  	//Direciona dados para serem gravados em arquivo e na base  	
	gravaMonitorArquivo(texto, tipoMsg);
	gravaMonitorBase(texto, tipoMsg);
  }
  
  public void gravaMonitorBase(String texto, String tipoMsg) throws DaoException{
  	 try {
  	 	//Carrega parametros a serem gravados
  	 	String processo = getNomProcesso();
  	 	String objeto = getNomObjeto();
  	 	String codOrgao = getCodOrgao();
  	 	Connection Conn = getConn();
  	 	
  	 	Dao dao = Dao.getInstance();
		//grava dados na base. Caso retorne falso, grava os dados em arquivo
		dao.gravaMonitorBase(processo,objeto,tipoMsg,texto,codOrgao,Conn);

	} catch (DaoException e) {
		setErroBase("Erro");			
		gravaMonitorArquivo(texto, tipoMsg);
		throw new DaoException ("Erro Monitor.gravaMonitorBase(): " + e.getMessage());
	}
  }
  
  public void gravaMonitorArquivo(String texto, String tipoMsg){
    // Geracao do nome do arquivo Carga_YYYYMMDD.LOG
    Calendar calendar = new GregorianCalendar();
    Date today = new Date();
    calendar.setTime(today);
    String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
    String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
    String ano = calendar.get(Calendar.YEAR) + "";
    String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
    String min = calendar.get(Calendar.MINUTE) + "";
    String seg = calendar.get(Calendar.SECOND) + "";
    
    if (dia.length()<2) {dia = "0" + dia;}
    if (mes.length()<2) {mes = "0" + mes;}
    if (hor.length()<2) {hor = "0" + hor;}
    if (min.length()<2) {min = "0" + min;}
    if (seg.length()<2) {seg = "0" + seg;}
    
	String path;
	try {
		Connection conn = getConn();
		ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
		param.setCodSistema("40"); //M�DULO REGISTRO	
		param.PreparaParam(conn);
		path = param.getParamSist("MONITOR_LOG");
	} catch (BeanException e) {
		path = "C:/SMIT/log";
	}
	    
    String nomeArq = path + "/" + nomArquivo + "_" + ano + mes + dia +".LOG";

    // criacao ou apende do arquivo de erro
    try {
      FileOutputStream fos = new FileOutputStream(nomeArq,true);
      OutputStreamWriter file = new OutputStreamWriter(fos);
      PrintWriter pw = new PrintWriter(file); // utilizado no StackTrace
      try {
      	//Grava mensagem de identifica��o de erro na base - inicio
      	String erroNaBase = getErroBase();
      	if(erroNaBase.equals("Erro")) file.write(hor+":"+min+":"+seg+" - "+"ERRO"+ "  " +getNomProcesso()
		    + "  " +getNomObjeto()+ "  >> Inicio - Erro na gravacao na Base: << \n");
      	  
        file.write(hor+":"+min+":"+seg+" - "+tipoMsg+ "  " +getNomProcesso()
		    + "  " +getNomObjeto()+ "  " +texto+"\n");
                
        //Grava mensagem de identifica��o de erro na base - fim
		if(erroNaBase.equals("Erro")) file.write(hor+":"+min+":"+seg+" - "+"ERRO"+ "  " +getNomProcesso()
		    + "  " +getNomObjeto()+ "  >> Fim - Erro na gravacao na Base: << \n");
		file.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }  
    }
    catch (FileNotFoundException e) {
      System.out.println("Arquivo nao encontrado e nao gerado");
      e.printStackTrace();
    }
  } // end of gravaMonitor

}