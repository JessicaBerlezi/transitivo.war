package sys;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * @author pvinicius
 */

public class CadSubstituicaoCmd extends sys.Command{
	  private static final String jspPadrao="sys/CadSubstituicao.jsp";
	  private String next;
	  
	  public CadSubstituicaoCmd() {
	    next = jspPadrao;
	  }

	  public CadSubstituicaoCmd(String next) {
	    this.next = jspPadrao;
	  }
	  /**
	   * 
	   * @author Pedro N�brega
	   * @since 02/03/2005
	   * @version 1.2
	   * @param req Requisi��o do JSP a ser tratada
	   * @return retorna a pr�xima p�gina JSP a ser exibida
	   * @exception sys.CommandException
	   */
	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = jspPadrao ;
		SubstituicaoBean substituicaoBean = new SubstituicaoBean();
		HashMap atualizar = new HashMap();
		HashMap excluir = new HashMap();
		int chave = 0;
	    try 
		{     
	    	String acao = req.getParameter("acao");
	    	if(acao == null)
	    		acao = "";
    		int cont = 0;
	        boolean mensagemErro = false;
    		List mensagens = new ArrayList();
	    	if(acao.equalsIgnoreCase("atualizar"))
	    	{	
	    		String[] id= req.getParameterValues("id");
	    		String[] desPalavra = req.getParameterValues("desPalavra");  
	    		String[] desPalavraAux = req.getParameterValues("desPalavraAux");
	    		String[] desSubstituicao = req.getParameterValues("desSubstituicao");
	    		String[] desSubstituicaoAux = req.getParameterValues("desSubstituicaoAux");
	    		for(int i=0;i<desPalavra.length;i++)
				{	
					if( desPalavra[i].equals(desPalavraAux[i]) && desSubstituicao[i].equals(desSubstituicaoAux[i]))
		  				continue;
					chave = i + 1;
					if(!id[i].equals(""))
						substituicaoBean.setId(Integer.valueOf(id[i]));
					else
						substituicaoBean.setId(null);
					if(desPalavra[i].equals(""))
						cont++;
					if(desSubstituicao[i].equals(""))
						cont++;
					if(cont == 2)
						excluir.put(String.valueOf(chave), substituicaoBean);
					if(cont == 0)
					{
						substituicaoBean.setDesPalavra(desPalavra[i]);
						substituicaoBean.setDesSubstituicao(desSubstituicao[i]);
						atualizar.put(String.valueOf(chave), substituicaoBean);
					}
					if(cont == 1)
					{
						if(!mensagemErro)
						{
							mensagens.add("Nenhuma opera��o pode ser realizada pelo(s) seguinte(s) motivo(s):\n");
							mensagemErro = true;
						}
						mensagens.add("O registro " + chave + " estava com 1 campo em branco\n");
					}
				    cont = 0;
				    substituicaoBean = null;
				    substituicaoBean = new SubstituicaoBean();
				}
	    		if(mensagemErro)
	    			substituicaoBean.setMensagem(mensagens);
	    		substituicaoBean.atualizarExcluir(atualizar, excluir, desPalavra.length, mensagemErro);
	    	}
	    	substituicaoBean.consultar();
	    	req.setAttribute("substituicaoBean",substituicaoBean);
	    }
	    catch (Exception ue) 
		{	      
	    	substituicaoBean.setMensagem(ue.getMessage());
	      try
		  {
	      	substituicaoBean.consultar();
		  }
	      catch (Exception e)
		  {
	      	throw new CommandException(e.getMessage());
		  }
	      req.setAttribute("substituicaoBean",substituicaoBean);
	    }
		return nextRetorno;
	  }

}
