package sys;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
* <b>Title:</b>        HtmlPopupBean<br>
* <b>Description:</b>  Monta menu popup em html<br>
* <b>Copyright:</b>    Copyright (c) 2001<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Carlos Andre Fernandes
* @version 1.0
*/
public class HtmlPopupBean extends Tabela {

  /** nome do popup */  
  private String popupNome;  
  /** string contendo o codigo html do popup */    
  private String popupString;
  /** Tamanho do popup */
  private int popupSize;
  /** Largura default do popup */
  private int popupWidth;
  /** string contendo a exten��o do input, aonde ser�o colocados os<br>
  *   comandos de controle, como onChange, onMouseOver e etc.
  */    
  private String popupExt;
  /** indica o item do popup checked (default="")
  */  
  private String checked;
  /** Coluna a ser atribuida ao value */
  private String colunaValue;
  private sys.ServiceLocator serviceloc ;  
  
  /** Construtor.
  */    
  public HtmlPopupBean()    {
    super();
	popupNome="popup1";	
//	popupSize=1;
	popupWidth=2;
	String aux="";
	for(int f=0;f<popupWidth;f++)
		aux=aux+"&nbsp;";
	popupString="<select name='"+popupNome+"' size='"+popupSize+"' "+popupExt+"><OPTION VALUE='0'>"+aux+"</OPTION></SELECT>";
//	popupExt="";
	checked="";
	colunaValue = "pkid" ;
	try {	
		serviceloc =  sys.ServiceLocator.getInstance();
	}
	catch (Exception e) { 	} 				
  }
  public sys.ServiceLocator getServiceloc(){
  	return serviceloc;
  }
  
  /** Atualiza o nome do popup.
  *   @param popupNome (String).
  */
  public void setPopupNome(String popupNome){
  	this.popupNome=popupNome;
  }

  /** Recupera o nome do popup.
  *   @return popupNome (String).
  */
  public String getPopupNome(){
  	return popupNome;
  }

  /** Atualiza checked
  *   @param checked (String).
  */
  public void setChecked(String checked){  
  	this.checked=checked;
  }

  /** Recupera checked
  *   @return checked (String).
  */
  public String getChecked(){
  	return checked;
  }
  
  /** Atualiza a coluna ser atrinbuida ao value
  *   @param colunaValue (String).
  */
  public void setColunaValue(String colunaValue){
  	this.colunaValue=colunaValue;
  }

  /** Recupera a coluna ser atrinbuida ao value
  *   @return  colunavalue (String).
  */
  public String getColunaValue(){
  	return colunaValue;
  }
  

  /** Atualiza a extens�o do comando input do popup.
  *   @param popupExt (String).
  *   <br>Ex: "onChange(javascript: document.pro_lanForm.submit();)"
  */
  public void setPopupExt(String popupExt){
  	this.popupExt=popupExt;
  }

  /** Recupera a extens�o do comando input do popup.
  *   @return popupExt (String).
  */
  public String getPopupExt(){
  	return popupExt;
  }

  /** Atualiza o tamanho do popup.
  *   @param popupSize (int).
  */
  public void setPopupSize(int popupSize){
  	this.popupSize=popupSize;
  }

  /** Recupera o tamanho do popup.
  *   @return popupSize (int).
  */
  public int getPopupSize(){
  	return popupSize;
  }
  
  /** Atualiza a largura default do popup.
  *   @param popupWidth (int).
  */
  public void setPopupWidth(int popupWidth){
  	this.popupWidth=popupWidth;
  }
  public int getPopupWidth(){
  	return this.popupWidth;
  }
  
  /** Atualiza o codigo html do popup.
  *   @param opcoes (String).  
  *   <p>Obs.: formato do parametro:
  *   <br>String contendo nome da coluna e a clausula where, separados por virgula.
  *   <br>Ex.: "Doc_Descricao,SELECT pro_doc.Doc_Pk_Id,pro_doc.Doc_Descricao FROM pro_dsass INNER JOIN pro_doc ON pro_dsass.Dsass_Fk_Doc = pro_doc.Doc_Pk_Id WHERE (pro_dsass.Dsass_Fk_Sass = 2)".
 * @throws Exception 
  */
  public void setPopupString(String opcoes) throws Exception {
	String sqlCmd = null;
  	String aux="";
    for(int f=0;f<popupWidth;f++)
    	aux=aux+"&nbsp;";
	popupString="<select style='border-style: outset;' name='"+popupNome+"' size='"+popupSize+"' "+popupExt+"><OPTION VALUE='0'>"+aux+"</OPTION></SELECT>";
	Connection connTabela =null ;
    try{
	  String nome_coluna;	  
	  int loc_virg = opcoes.indexOf(",");
	  if (loc_virg==-1){
	    nome_coluna=opcoes;
  	    sqlCmd = "select "+this.colunaValue+","+nome_coluna+" from "+getTabela();
	  }
	  else{
	  	nome_coluna = opcoes.substring(0,loc_virg);
		sqlCmd      = opcoes.substring(loc_virg+1,opcoes.length());
	  }
	  String valor_pk="0";
	  String valor_coluna;
	  StringBuffer popupBuffer = new StringBuffer();
  	  connTabela = serviceloc.getConnection(getJ_abrevSist()) ;
      Statement stmt = connTabela.createStatement();
      ResultSet rs   = stmt.executeQuery(sqlCmd);
      popupBuffer.append("<select style='border-style: outset;' name='"+popupNome+"' size='"+popupSize+"' "+popupExt+"> \n");	  
      popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");	    			  
	  boolean not_eof=rs.next();
	  int conta=0;
	  String seleciona="";
	  if(not_eof){
  	    while(not_eof){
//		  conta++;
//		  seleciona    = (conta==checked) ? "selected" : "";
		  seleciona    = (checked.equals(rs.getString(this.colunaValue))) ? "selected" : "";
	      popupBuffer.append("<OPTION VALUE='"+rs.getString(this.colunaValue)+"' "+seleciona+" >"+rs.getString(nome_coluna).trim()+"</OPTION> \n");
          not_eof=rs.next();		
	    }
	  }	  	  
      popupBuffer.append("</SELECT> \n");	  
	  rs.close();
	  stmt.close();
	  popupString=popupBuffer.toString();
	  
    }
    catch (Exception e) 
    {
    	System.err.println("Informa��es ==>"+this.getClass().getName()+ " "+this.getClass().getClassLoader()+" "+this.getClass().getInterfaces());
    	System.err.println("HtmlPopupBean 01 - m�todo setPopupString:"+e.getMessage()+" "+sqlCmd+" "+sys.Util.formatedToday()+" "+this.getClass().getName()+ " "+this.getClass().getClassLoader()+" "+this.getClass().getInterfaces());    
    	throw new Exception("HtmlPopupBean 01 " + e.getMessage());
    }
	finally {
		if (connTabela != null) {
		  try { 
		  connTabela.rollback();
		  serviceloc.setReleaseConnection(connTabela); }
		  catch (Exception ey) { }
		}
	}
    
  }

  /** Obtem o codigo html do popup.
  *   @return popupString (String).  
  */
  public String getPopupString(){
  	return popupString;
  }  
}