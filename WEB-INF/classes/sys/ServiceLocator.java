package sys;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * <b>Title:</b>        Fiscaliza��o - Modulo sys - Servicos disponiveis para Aplicacao<br>
 * <b>Description:</b>  Service Locator<br>
 * <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Michel Beniste
 * @version 1.0
 */

public class ServiceLocator 
{
    private static ServiceLocator instance; 
    private String j_sistAbrev  ;
    
    private String[] abrevSistConn ;  
    private String[] driver ;  
    private String[] dbURL ;  
    private String[] user ;  
    private String[] pass ; 
    private String qualBanco  ;
	private String qualBroker  ;		  
    
    private int quantConexoes;
    private int crescimento;
    
    private Vector listaConexoes;
    private Vector listaConexoesUsadas; 
    private int proxConn;
    private int hant; 
    private HashMap setPara ;
    private int posInicial;
    private int posFinal;
    
    // Retorna a instancia da classe ServiceLocator  
    public static ServiceLocator getInstance() throws ServiceLocatorException {
        if (instance == null) instance = new ServiceLocator();
        return instance;
    }
    
    private ServiceLocator() throws ServiceLocatorException  {
        abrevSistConn = new String[4];
        driver = new String[4];
        dbURL  = new String[4];
        user   = new String[4];
        pass   = new String[4];
        hant   = 0 ;
        posInicial = Util.Roleta(2)+5;
        posFinal   = Util.Roleta(9);
    }
    
    // Carrega os parametros do web.xml informado e inicializa o servicelocator
    public void initParams(String xml) throws ServiceLocatorException {
        Hashtable params;
        try {
            params = sys.XMLUtil.readInitParams(xml, "acessoTool");            
            this.setJ_sistAbrev((String) params.get("sistAbrev"));			
            this.setQualBanco((String) params.get("qualBanco"));
	        this.setQualBroker((String) params.get("qualBroker"));
			
            this.setQuantConexoes("1");
            this.setQuantCresc("1");
            // Preparar as conexoes
            int qBco = Integer.parseInt(this.getQualBanco());
            if (qBco < 0 || qBco > 3) qBco = 0;
            for (int i = 0; i < 4; i++) {
                this.setAbrevSistConn(i, (String) params.get("abrevSistConn_" + qBco + "_" + i));
                this.setDriver(i, (String) params.get("driver_" + qBco + "_" + i));
                this.setDbURL(i, (String) params.get("dbURL_" + qBco + "_" + i));
                this.setUser(i, (String) params.get("user_" + qBco + "_" + i));
                this.setPass(i, (String) params.get("pass_" + qBco + "_" + i));
            }			
            //this.getPoolConnection();
        }
        catch (Exception e) {
            throw new ServiceLocatorException(e.getMessage());
        } 
    }
    
    protected HashMap getSetPara()	{
        return this.setPara ;
    }
    protected void setSetPara(HashMap setPara)	{
        this.setPara=setPara ;
    }
    
    //--------------------------------------------------------------------------
    protected String getNomBanco()  {
        String bco="????";
        if ("0".equals(this.qualBanco)) bco = "LAP TOP";
        if ("1".equals(this.qualBanco)) bco = "DESENVOLVIMENTO";
        if ("2".equals(this.qualBanco)) bco = "HOMOLOGA��O";
        if ("3".equals(this.qualBanco)) bco = "(P)";
				
        				
        return bco;
    }  
//	--------------------------------------------------------------------------
	  protected String getNomBroker()  {
		  String brk="????";
		  if ("0".equals(this.qualBroker)) brk = "BROKER-ADABAS";
		  if ("1".equals(this.qualBroker)) brk = "BROKER-ORACLE";         				
		  return brk;
	  }  
    
    public String getQualBanco()  {
        return this.qualBanco;
    }  
    
    protected void setQualBanco(String qualBanco)  {
        if (qualBanco==null) qualBanco="?" ; 
        this.qualBanco=qualBanco;
    }  
    
	public String getQualBroker()  {
			return this.qualBroker;
		}  
    
		protected void setQualBroker(String qualBroker)  {
			if (qualBroker==null) qualBroker="?" ; 
			this.qualBroker=qualBroker;
		}  
    
    protected void setJ_sistAbrev(String j_sistAbrev)  {
        this.j_sistAbrev=j_sistAbrev ;  
        if (j_sistAbrev==null) this.j_sistAbrev="" ;  
    }  
    protected String getJ_sistAbrev()  {
        if (this.j_sistAbrev==null) this.j_sistAbrev="" ;  
        return this.j_sistAbrev;
    }
    protected void setAbrevSistConn(int i,String abrevSistConn)  {
        if (abrevSistConn==null) abrevSistConn="ACSS" ; 
        if ((i<0) || (i>6)) i=0;
        this.abrevSistConn[i]=abrevSistConn;		
    }  
    protected String getDriver(int i)  {
        return this.driver[i];
    }  
    
    protected void setDriver(int i,String driver)  {
        if (driver==null) driver="" ; 
        if ((i<0) || (i>6)) i=0;		
        this.driver[i]=driver;
    }  
    protected String getDbURL(int i)  {
        return this.dbURL[i];
    }  
    
    protected void setDbURL(int i,String dbURL)  {
        if (dbURL==null) dbURL="" ; 
        if ((i<0) || (i>6)) i=0;		
        this.dbURL[i]=dbURL;
    }  
    protected String getUser(int i)  {
        return this.user[i];
    }  
    protected void setUser(int i,String user)  {
        if (user==null) user="" ; 
        if ((i<0) || (i>6)) i=0;		
        this.user[i]=user;
    }  
    protected String getPass(int i)  {
        return this.pass[i];
    }  
    
    protected void setPass(int i,String pass)  {
        if (pass==null) pass="" ; 
        if ((i<0) || (i>6)) i=0;		
        this.pass[i]=pass;
    }  
    
    protected void setQuantConexoes(String quantConexoes){
        this.quantConexoes = Integer.parseInt(quantConexoes);
    }
    
    protected String getQuantConexoes(){
        return String.valueOf(quantConexoes);
    }
    
    protected void setQuantCresc(String crescimento){
        this.crescimento = Integer.parseInt(crescimento);
    }
    
    protected String getQuantCresc(){
        return String.valueOf(crescimento);
    }
    
    // In�cio do Pool
    protected  void getPoolConnection()  throws ServiceLocatorException 
    {
        
        this.proxConn        = 0;
        System.out.println("Inicializando Pool: "+sys.Util.formatedToday());		
        listaConexoes       = new Vector();
        listaConexoesUsadas = new Vector();
        try{
            Class.forName(driver[1]);
        }
        catch(ClassNotFoundException e){
            throw new ServiceLocatorException(e.getMessage());
        }
        
//        for(int i=0; i < quantConexoes; i++){
//            Connection conn     = null ;			
//            try {
//                conn = DriverManager.getConnection(dbURL[1],user[1],pass[1]);
//            }
//            catch (Exception e) {
//                conn=null ;	
//                throw new ServiceLocatorException(e.getMessage());								
//            }
//            listaConexoes.add(conn);
//            listaConexoesUsadas.add("N");
//            System.out.println("Abrindo conex�es:"+i);
//            
//            
//        }
        
        System.out.println("Pool inicializado Conexoes: " + listaConexoes.size()+" as "+sys.Util.formatedToday());
    }
    
    public synchronized Connection getConnection(String abrevSist)  throws ServiceLocatorException {
        Connection conn = null ;
        int connLivre   = 0 ;
        int tam         = listaConexoes.size();	
        int elem        = 0 ;
        boolean temConnLivre = false ;	
        try {
            conn = DriverManager.getConnection(dbURL[1],user[1],pass[1]);
        	
            // t - tentativas
//            for (int t=0; t<5; t++) {
//                for (int k=0; k < tam;k++) {   	
//                    elem = k + proxConn ;
//                    elem = (( elem>=tam) ? (elem-tam) : elem ) ;
//                    if ("N".equals((String)listaConexoesUsadas.get(elem))) 
//                    {					
//                        conn = (Connection) listaConexoes.get(elem);
//                        //						a conex�o j� foi encerrada pelo DBMS?
//                        if (conn.isClosed())
//                        	listaConexoesUsadas.set(elem,"F");		
//                        else {
//                            conn.setAutoCommit(true);							
//                            listaConexoesUsadas.set(elem,"S");
//                            temConnLivre=true;
//                            proxConn++;
//                            if (proxConn>=tam) proxConn = 0;							
//                            break;
//                        }					 
//                    }
//                }								
//                if (!temConnLivre) {
//                    getMoreConnection();
//                    tam = listaConexoes.size();
//                }
//                else
//                	break;
//            }
        } 
        catch(Exception e) {
            throw new ServiceLocatorException(e.getMessage());
        }   	
        int dfim = Integer.parseInt(sys.Util.formatedToday().substring(0,2));
        int mfim = Integer.parseInt(sys.Util.formatedToday().substring(3,5));		
        int hfim = Integer.parseInt(sys.Util.formatedToday().substring(13,15));
        int mifim = Integer.parseInt(sys.Util.formatedToday().substring(16,18));
        hfim = ((((mfim*31)+dfim)*24+hfim)*60)+mifim ;
        
        /*Modificar de 30 min para 60 min*/
        if ( hfim - this.hant > 60 ) {		
            int livre = 0 ;
            int ocp = 0;
            int fech = 0 ;			
            for (int k=0; k < tam;k++) {   	
                if ("N".equals((String)listaConexoesUsadas.get(k))) livre++;					
                else {
                    if ("F".equals((String)listaConexoesUsadas.get(k))) fech++; 
                    else ocp++;
                }
            }
            System.out.println("Pool "+sys.Util.formatedToday()+" Prox.: "+proxConn+"    Livre: " + livre +"   Ocop: "+ocp+"   Fech: "+fech);
            this.hant = hfim ; 
        }		
        return conn;
    }
    
    private synchronized void getMoreConnection() throws ClassNotFoundException{		
        try {
            Class.forName(driver[1]);
            for(int i=0; i < crescimento; i++){
                Connection conn = null ;
                conn = DriverManager.getConnection(dbURL[1],user[1],pass[1]);
                listaConexoes.add(conn);
                listaConexoesUsadas.add("N");			
            }
            System.out.println("Lista de Conexoes aumentada para: " + listaConexoesUsadas.size()+" as "+sys.Util.formatedToday());			
        }
        catch (Exception e) {
            throw new ClassNotFoundException(e.getMessage());			
        }	
        return ;
    }
    
    public synchronized void setReleaseConnection(Connection connection) throws ServiceLocatorException{
        try{ 	
            int i = listaConexoes.indexOf(connection) ; 	
            if (i>=0) 	listaConexoesUsadas.set(i,"N") ;
            else 		connection.close();
        } 			
        catch(Exception e){
            throw new ServiceLocatorException(e.getMessage());
        }		
    }
    
    protected void close() {
        Connection conn= null;
        while (listaConexoes.size() > 0) {
            conn = (Connection)listaConexoes.get(0);
            try{
                conn.close();   
            } catch(SQLException e){
                System.out.println(e);
            }       
            listaConexoes.remove(0);            
        }
    }
    // Fim do Pool
    
    // In�cio do Email
    private synchronized void sendMessage(EmailBean email, String login, String senha, int type) throws MessagingException{
        MimeMessage msg;
        
        Properties props = System.getProperties();
        /*props.put("mail.smtp.host", "smtp.mindinformatica.com.br"); Mudan�a 05/05/2008 */
        /*props.put("mail.smtp.host", "smtp.mindnet"); 05/04/2010*/        
        //props.put("mail.smtp.host", "10.200.187.2");
        props.put("mail.smtp.host", "200.155.29.26");
        
        Usuario authenticator = new Usuario(login,senha);
        Session session = Session.getInstance(props, authenticator);
        session.setDebug(false);
        
        try {
            msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(email.getRemetente()));
            InternetAddress[] address = {new InternetAddress(email.getDestinatario())};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(email.getAssunto());
        } 
        catch(MessagingException e){
            throw new MessagingException(e.getMessage());
        }
        
        MimeBodyPart body = new MimeBodyPart();
        if( type == 1)
            body.setText(email.getConteudoEmail());
        else{
            body.setText(email.getConteudoEmail(),"iso-8859-1");
            body.setContent(email.getConteudoEmail(), "text/html; charset=iso-8859-1");
        }
        
        Multipart mp = new MimeMultipart();
        
        if(email.getArquivos() != null){
            MimeBodyPart file;
            List files = email.getArquivos();
            for(int i = 0; i < files.getItemCount(); i++){
                file = new MimeBodyPart();
                FileDataSource fds = new FileDataSource(files.getItem(i));
                file.setDataHandler(new DataHandler(fds));
                file.setFileName(fds.getName());
                mp.addBodyPart(file);
            }
        }
        
        mp.addBodyPart(body);       
        msg.setContent(mp);
        msg.setSentDate(new Date());
        Transport.send(msg);
    }
    
    public synchronized void sendEmail(EmailBean email) throws MessagingException{
        sendMessage(email,null,null,1);
    }
    
    public synchronized void sendEmail(EmailBean email, String login, String senha) throws MessagingException{
        sendMessage(email, login, senha, 1);
    }
    
    public synchronized void sendEmailHtml(EmailBean email) throws MessagingException{
        sendMessage(email, null, null, 2);
    }
    
    public synchronized void sendEmailHtml(EmailBean email, String login, String senha) throws MessagingException{
        sendMessage(email, login, senha, 2);
    }
    
    private class Usuario extends Authenticator{
        String login;
        String senha;
        public Usuario(String login, String senha) {
            this.login = login;
            this.senha = senha;
        }
        
        protected  PasswordAuthentication getPasswordAuthentication(){
            PasswordAuthentication Password = new PasswordAuthentication(login,senha);
            return Password;
        }
    }	
    // Fim do Email

	public int getPosFinal() {
		return posFinal;
	}

	public void setPosFinal(int posFinal) {
		this.posFinal = posFinal;
	}

	public int getPosInicial() {
		return posInicial;
	}

	public void setPosInicial(int posInicial) {
		this.posInicial = posInicial;
	}
    
}


