package sys;

import java.util.Arrays;
import java.util.StringTokenizer;

public class Fonetizador {
	
	private static Fonetizador instance;
	
	private static final char lista_consoante[] = { 'B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z' };
	private static final char lista_vogal[] = { 'A', 'E', 'I', 'O', 'U' };
	
	private StringBuffer buffer = new StringBuffer();
	private StringBuffer buffer2 = new StringBuffer();
	
	private char str_char[];
	private char str_char2[];
	private int tam = 0;
	private int tam2 = 0;
	private int ind = 0;
	
	private SupressaoBean preposicao;
	private SubstituicaoBean substituicaoFonetica;
	
	// Construtor
	private Fonetizador() throws FonetizadorException {
		try {
			preposicao = new SupressaoBean();
			preposicao.consultar();
			substituicaoFonetica = new SubstituicaoBean();
			substituicaoFonetica.consultar();
		} catch (Exception ue) {
			throw new FonetizadorException(ue.getMessage());
		}
	}
	
	// Retorna a instancia da classe Fonetizador  
	public static Fonetizador getInstance() throws FonetizadorException {
		if (instance == null)
			instance = new Fonetizador();
		return instance;
	}
	
	public String getFonetizado(String palavra) throws FonetizadorException {
		return fonetizar(palavra.toUpperCase());
	}
	
	private synchronized String fonetizar(String str) {
		
		String resposta = "";
		StringTokenizer ST1 = new StringTokenizer(str, " ");
		String s1 = new String();
		String str_aux = new String();
		
		// Etapa para retirada dos acentos
		while (ST1.hasMoreTokens()) {
			s1 = ST1.nextToken();
			
			if (ST1.countTokens() != 0) {
				buffer.setLength(0);
				buffer2.setLength(0);
				str_aux = str_aux + retiraAscentos(s1) + " ";
			} else {
				buffer.setLength(0);
				buffer2.setLength(0);
				str_aux = str_aux + retiraAscentos(s1);
			}
		}
		// Etapa para retirada das preposi��es		
		StringTokenizer ST2 = new StringTokenizer(str_aux, " ");
		String s2 = new String();
		boolean achou;
		while (ST2.hasMoreTokens()) {
			s2 = ST2.nextToken();
			achou = preposicao.busca(s2);
			if (!achou) // token n�o � preposi��o 
			{
				buffer.setLength(0);
				buffer2.setLength(0);
				s2 = substituicaoFonetica.substituir(s2);
				if (ST2.countTokens() != 0) {
					resposta = resposta + tratador(s2) + " ";
				} else {
					resposta = resposta + tratador(s2);
				}
			}
		}
		return resposta;
	}
	
	private String retiraAscentos(String token) {
		StringBuffer buffer_acento = new StringBuffer();
		char str_char[];
		int tamanho;
		int i = 0;
		int e = 0;
		
		tamanho = token.length();
		str_char = new char[tamanho];
		token.getChars(0, token.length(), str_char, 0);
		
		while (i < tamanho) {
			e = (int) str_char[i];
			
			if (e <= 47) {
			}//Erro
			else if ((48 <= e) && (e <= 57))
				buffer_acento.append(str_char[i]);
			else if ((58 <= e) && (e <= 64)) {
			}//Erro		
			else if ((65 <= e) && (e <= 90))
				buffer_acento.append(str_char[i]);
			else if ((91 <= e) && (e <= 96)) {
			}//Erro
			else if ((97 <= e) && (e <= 122))
				buffer_acento.append(str_char[i]);
			else if ((123 <= e) && (e <= 191)) {
			}//Erro
			else if ((192 <= e) && (e <= 198))
				buffer_acento.append('A');
			else if (e == 199)
				buffer_acento.append('C');
			else if ((200 <= e) && (e <= 203))
				buffer_acento.append('E');
			else if ((204 <= e) && (e <= 207))
				buffer_acento.append('I');
			else if (e == 209)
				buffer_acento.append('N');
			else if ((210 <= e) && (e <= 214))
				buffer_acento.append('O');
			else if ((217 <= e) && (e <= 220))
				buffer_acento.append('U');
			else if (e == 221)
				buffer_acento.append('I');
			i++;
		}
		return buffer_acento.toString();
	}
	
	private String tratador(String str_trat) {
		tam = str_trat.length();
		str_char = new char[tam];
		str_trat.getChars(0, tam, str_char, 0);
		ind = 0;
		while (ind < tam) {
			int a;
			a = Arrays.binarySearch(lista_vogal, str_char[ind]);
			if (a >= 0) // 1L � vogal
			{
				trataVogal(a);
			} else // 1L � consoante
			{
				int b = 0;
				b = Arrays.binarySearch(lista_consoante, str_char[ind]);
				if (b >= 0) {
					trataConsoante(b);
				} else {
					buffer.append(str_char[ind]);
					ind++;
				}
			}
			
		}
		
		/******  In�cio da Etapa dos Casos Especiais  ******/
		trataCasosEspeciais();
		return buffer2.toString();
	}
	
	/**
	 *-----------------------------------------------------------
	 * Fun��es de Casos Especiais
	 *-----------------------------------------------------------
	 */
	
	private void trataCasosEspeciais() {
		String str = buffer.toString();
		tam2 = str.length();
		str_char2 = new char[tam2];
		str.getChars(0, tam2, str_char2, 0);
		ind = 0;
		while (ind < tam2) {
			int a;
			a = Arrays.binarySearch(lista_vogal, str_char2[ind]);
			if (a >= 0) // 1L � vogal
			{
				trataCasosVogal();
			} else // 1L � consoante
			{
				int b;
				b = Arrays.binarySearch(lista_consoante, str_char2[ind]);
				if (b >= 0) {
					trataCasosConsoante();
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			}
		}
	}
	
	private void trataVogaisConsecutivas(int a, int b) {
		char c = lista_vogal[a];
		char d = lista_vogal[b];
		if (a == b) // 1L e 2L s�o iguais
		{
			ind++;
		} else // 1L e 2L n�o s�o iguais
		{
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataLetraS() {
		int g;
		if (ind + 2 == tam2) {
			buffer2.append(str_char2[ind]);
			buffer2.append('Z');
			ind = ind + 2;
		} else {
			g = Arrays.binarySearch(lista_consoante, str_char2[ind + 2]);
			if (g < 0) {
				buffer2.append(str_char2[ind]);
				buffer2.append('Z');
				ind = ind + 2;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		}
	}
	
	private void trataVogalSeguidoPorConsoanteEspecial() {
		int e;
		e = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if(e >= 0)
		{
			char f = lista_consoante[e];
			if ((f == 'B') || (f == 'D') || (f == 'F') || (f == 'P') || (f == 'T')) {
				if (ind + 2 == tam2) {
					buffer2.append(str_char2[ind]);
					buffer2.append(str_char2[ind + 1]);
					buffer2.append('I');
					ind = tam2;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			} else {
				if (f == 'S')
					trataLetraS();
				else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			}
		}
		else{
			buffer2.append(str_char2[ind]);
			ind++;
		}
			
	}
	
	private void trataVogalEspecial(int a) {		
		char b = lista_vogal[a];
		if ((b == 'A') || (b == 'O') || (b == 'U')) {
			buffer2.append('K');
			ind++;
		} else {
			if ((b == 'E') || (b == 'I')) {
				buffer2.append('S');
				ind++;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		}
	}
	
	private void trataConsoantesSeguidas(int a, int b) {
		char c = lista_consoante[a];
		char d = lista_consoante[b];
		if (a == b) // 1L e 2L s�o iguais
		{
			ind++;
		} else // 1L e 2L n�o s�o iguais
		{
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataUltima(int e) {
		int f;
		f = Arrays.binarySearch(lista_vogal, str_char[ind + 2]);
		if (f >= 0) {
			char g = lista_vogal[e];
			char h = lista_vogal[f];
			if ((g == 'E') && (h == 'A')) {
				buffer.append(str_char[ind]);
				buffer.append(str_char[ind + 1]);
				buffer.append('I');
				buffer.append(str_char[ind + 2]);
				ind = tam;
			} else {
				buffer.append(str_char[ind]);
				ind++;
			}
		} else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataPenultima() {
		int e;
		e = Arrays.binarySearch(lista_vogal, str_char[ind + 1]);
		if (e >= 0)
			trataUltima(e);
		else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataConsoanteSeguidaPorVogal() {
		if ((ind + 3) == tam)
			trataPenultima();
		else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataConsoante(int a) {
		if (ind < tam - 1) //caso n�o seja a �ltima letra
		{
			int b;
			b = Arrays.binarySearch(lista_consoante, str_char[ind + 1]);
			if (b >= 0) // 1L e 2L s�o consoantes
				trataConsoantesSeguidas(a, b);
			else
				// 1L � consoante e 2L � vogal
				trataConsoanteSeguidaPorVogal();
		} else //caso seja a �ltima letra
		{
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataLetraH(int e) {
		char f = lista_consoante[e];
		if (f != 'H') {
			buffer2.append(str_char2[ind]);
			buffer2.append('U');
			ind = ind + 2;
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataLetraL() {
		// 1L � 'I', 2L � 'L' e tamb�m �ltima letra
		if (ind + 2 == tam2) {
			buffer2.append(str_char2[ind]);
			buffer2.append('U');
			ind = ind + 2;
		}
		// 1L � 'I', 2L � 'L', 3L � consoante
		else {
			int e = Arrays.binarySearch(lista_consoante, str_char2[ind + 2]);
			if (e >= 0)
				trataLetraH(e);
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		}
	}
	
	private void trataLetraG() {
		// 1L � 'I', 2L � 'N', 3L � 'G' e tamb�m �ltima
		if (ind + 3 == tam2) {
			buffer2.append(str_char2[ind]);
			buffer2.append('M');
			ind = tam2;
		} else {
			int h = Arrays.binarySearch(lista_consoante, str_char2[ind + 3]);
			
			// 1L � 'I', 2L � 'N', 3L � 'G', 4L � consoante
			if ((h >= 0)) {
				buffer2.append(str_char2[ind]);
				buffer2.append('M');
				ind = ind + 3;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		}
	}
	
	private void trataLetraG(int f) {
		char g = lista_consoante[f];
		if (g == 'G')
			trataLetraG();
		else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataProximaLetra() {
		int f = Arrays.binarySearch(lista_consoante, str_char2[ind + 2]);
		if (f >= 0)
			trataLetraG(f);
		else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataLetraN() {
		if (str_char2[ind + 1] == 'N') // 2L � N
			trataProximaLetra();
		else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataLetraI() {
		if (ind + 1 < tam2) {
			if (str_char2[ind + 1] == 'L') // 2L � 'L']
				trataLetraL();
			else
				trataLetraN();
		}// ind+1<tam-1
		else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataLetraL(int l) {
		if (l == 'L') {
			buffer.append('O');
			ind++;
		} else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataLetraE(int i, int j) {
		if ((i >= 0) && (j >= 0)) {
			char k = lista_vogal[i];
			char l = lista_consoante[j];
			if (k == 'E')
				trataLetraL(l);
			else {
				buffer.append(str_char[ind]);
				ind++;
			}
		} else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataVogalSeguidoPorConsoante() {
		if (ind + 2 < tam - 1) {
			int i = Arrays.binarySearch(lista_vogal, str_char[ind + 1]);
			int j = Arrays.binarySearch(lista_consoante, str_char[ind + 2]);
			trataLetraE(i, j);
		} else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataLetraU() {
		if (str_char2[ind] == 'U') // 1L � U
			trataVogalSeguidoPorConsoanteEspecial();
		else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataCasosVogal() {
		if (ind < tam2 - 1) //caso n�o seja a �ltima letra
		{
			int a;
			a = Arrays.binarySearch(lista_vogal, str_char2[ind]);
			if (str_char2[ind] == 'I') // 1L � I
				trataLetraI();
			else
				trataLetraU();
		} else // caso seja a �ltima letra
		{
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataRseguidoporL() {
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if (a >= 0) // � consoante
		{
			char b = lista_consoante[a];
			if ((b != 'R') && (b != 'L')) {
				buffer2.append(str_char2[ind]);
				buffer2.append('I');
				ind++;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataVogal(int a) {
		if (ind < tam - 1) //caso n�o seja a �ltima letra
		{
			int b;
			b = Arrays.binarySearch(lista_vogal, str_char[ind + 1]);
			if (b >= 0) // 1L e 2L s�o vogais
				trataVogaisConsecutivas(a, b);
			else
				// 1L � vogal e 2L � consoante
				trataVogalSeguidoPorConsoante();
		}//caso seja a �ltima letra
		else {
			buffer.append(str_char[ind]);
			ind++;
		}
	}
	
	private void trataConsoante() {
		int c = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		char d = lista_consoante[c];
		if (d != 'H') {
			buffer2.append('K');
			ind++;
		} else {
			if (ind + 2 < tam) {
				int e = Arrays.binarySearch(lista_vogal, str_char2[ind + 2]);
				if (e >= 0) {
					buffer2.append('X');
					ind = ind + 2;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			}
		}
	}
	
	private void trataROuLEspecial() {
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if (a >= 0) {
			if ((str_char2[ind + 1] == 'R') || (str_char2[ind] == 'L')) {
				buffer2.append(str_char2[ind]);
				ind++;
			} else {
				buffer2.append(str_char2[ind]);
				buffer2.append('I');
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataUSeguidoPorEOuI(int b) {
		if (b == 'U') {
			if (ind + 2 < tam2) {
				char c = str_char2[ind + 2];
				if ((b == 'E') || (b == 'I')) {
					buffer2.append(str_char2[ind]);
					ind = ind + 2;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataEOuI() {
		int a = Arrays.binarySearch(lista_vogal, str_char2[ind + 1]);
		if (a >= 0) {
			char b = lista_vogal[a];
			if ((b == 'E') || (b == 'I')) {
				buffer2.append('J');
				ind++;
			} else
				trataUSeguidoPorEOuI(b);
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataConsoanteOuVogal() {
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		int b = Arrays.binarySearch(lista_vogal, str_char2[ind + 2]);
		if ((a >= 0) || (b >= 0)) {
			if (str_char2[ind + 1] == 'H') {
				buffer2.append(str_char2[ind]);
				buffer2.append('I');
				ind = ind + 2;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataHOuRSeguidoPorRL() {
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if (a >= 0) {
			char b = lista_consoante[a];
			if (b == 'H') {
				buffer2.append('F');
				ind = ind + 2;
			} else {
				if ((b != 'R') && (b != 'L')) {
					buffer2.append(str_char2[ind]);
					buffer2.append('I');
					ind++;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataEouI(int d) {
		if ((d == 'E') || (d == 'I')) {
			buffer2.append('K');
			ind = ind + 2;
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataAOuO() {
		int c = Arrays.binarySearch(lista_vogal, str_char2[ind + 2]);
		if (c >= 0) {
			char d = lista_vogal[c];
			if ((d == 'A') || (d == 'O')) {
				buffer2.append('K');
				buffer2.append(str_char2[ind + 1]);
				ind = ind + 2;
			} else
				trataEouI(d);
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataUSeguidoPorAOuO() {
		int a = Arrays.binarySearch(lista_vogal, str_char2[ind + 1]);
		if (a >= 0) {
			char b = lista_vogal[a];
			if (b == 'U')
				trataAOuO();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataHNaoEUltima() {
		if (ind + 3 < tam2) {
			int f = Arrays.binarySearch(lista_vogal, str_char2[ind + 3]);
			if (f >= 0) {
				buffer2.append('X');
				ind = ind + 3;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataLetraC() {
		int d = Arrays.binarySearch(lista_consoante, str_char2[ind + 2]);
		if (d >= 0) {
			char e = lista_consoante[d];
			if (e == 'H') {
				if (ind + 3 == tam2) {
					buffer2.append('X');
					ind = tam2;
				} else
					trataHNaoEUltima();
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataCOuH() {
		
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if (a >= 0) {
			char b = lista_consoante[a];
			if ((b == 'H') && (ind + 2 < tam2)) {
				int c = Arrays.binarySearch(lista_vogal, str_char2[ind + 2]);
				if (c >= 0) {
					buffer2.append('X');
					ind = ind + 2;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			} else if ((b == 'C') && (ind + 2 < tam2))
				trataLetraC();
			else if (ind == 0) {
				buffer2.append('E');
				buffer2.append(str_char2[ind]);
				ind++;
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataNemRNemL(int b) {
		if (b == 'H') {
			if (ind + 2 < tam2) {
				int c = Arrays.binarySearch(lista_vogal, str_char2[ind + 2]);
				if (c >= 0) {
					buffer2.append(str_char2[ind]);
					ind = ind + 2;
				} else {
					buffer2.append(str_char2[ind]);
					buffer2.append('I');
					ind++;
				}
			} else {
				buffer2.append(str_char2[ind]);
				buffer2.append('I');
				ind++;
			}
		} else {
			buffer2.append(str_char2[ind]);
			buffer2.append('I');
			ind++;
		}
	}
	
	private void trataROuL() {
		int a = Arrays.binarySearch(lista_consoante, str_char2[ind + 1]);
		if (a >= 0) {
			char b = lista_consoante[a];
			if ((b == 'R') || (b == 'L')) {
				buffer2.append(str_char[ind]);
				ind++;
			} else
				trataNemRNemL(b);
		} else {
			buffer2.append(str_char2[ind]);
			ind++;
		}
	}
	
	private void trataCasosConsoante() {
		switch (str_char2[ind]) {
		case 'B':
			if (ind + 1 < tam2)
				trataRseguidoporL();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'C':
			if (ind + 1 < tam2) {
				int a = Arrays.binarySearch(lista_vogal, str_char2[ind + 1]);
				if (a >= 0)
					trataVogalEspecial(a);
				else
					// 2L � consoante
					trataConsoante();
			} else // �ltima letra
			{
				buffer2.append('K');
				ind++;
			}
			break;
			
		case 'D':
			if (ind + 1 < tam2)
				trataROuL();
			else {
				buffer2.append(str_char[ind]);
				ind++;
			}
			break;
			
		case 'F':
			if (ind + 1 < tam2)
				trataRseguidoporL();
			else {
				buffer2.append(str_char[ind]);
				ind++;
			}
			break;
			
		case 'G':
			if (ind + 1 < tam2)
				trataEOuI();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'H':
			ind++;
			break;
			
		case 'L':
			if (ind + 2 < tam2)
				trataConsoanteOuVogal();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'N':
			if (ind + 1 == tam2) {
				buffer2.append('M');
				ind = tam;
			} else {
				int a = Arrays
				.binarySearch(lista_consoante, str_char2[ind + 1]);
				if (a >= 0) {
					char b = lista_consoante[a];
					if ((b == 'M') && (ind + 2 == tam2)) {
						buffer2.append('M');
						ind = tam2;
					} else {
						buffer2.append('M');
						ind++;
					}
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			}
			
			break;
			
		case 'P':
			if (ind + 1 < tam2)
				trataHOuRSeguidoPorRL();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'Q':
			if (ind + 2 < tam2)
				trataUSeguidoPorAOuO();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'R':
			if (ind + 2 == tam2) {
				if (str_char2[ind + 1] == 'D') {
					buffer2.append(str_char2[ind]);
					ind = tam2;
				} else {
					buffer2.append(str_char2[ind]);
					ind++;
				}
			} else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			break;
			
		case 'S':
			if (ind + 1 < tam2)
				trataCOuH();
			else {
				buffer2.append(str_char2[ind]);
				ind++;
			}
			
			break;
			
		case 'T':
			if (ind + 1 < tam2)
				trataROuLEspecial();
			else {
				buffer2.append(str_char[ind]);
				ind++;
			}
			break;
			
		case 'W':
			if (str_char2[ind] == 'W') {
				buffer2.append('V');
				ind++;
			}
			break;
			
		case 'Y':
			buffer2.append('I');
			ind++;
			
			break;
		default:
			buffer2.append(str_char2[ind]);
		ind++;
		}		
	}
	
}