package sys;

import java.util.HashMap;

/**
 * @author pvinicius
 */

public class SubstituicaoBean extends PersistenteBean{
	
	String desPalavra;
	String desSubstituicao;

	public SubstituicaoBean() {
		super();
		desPalavra = "";
		desSubstituicao = "";
	}
	
	public String getDesPalavra() {
		return desPalavra;
	}

	public void setDesPalavra(String desPalavra) {
		this.desPalavra = desPalavra;
	}

	public String getDesSubstituicao() {
		return desSubstituicao;
	}

	public void setDesSubstituicao(String desSubstituicao) {
		this.desSubstituicao = desSubstituicao;
	}
	public void consultar() throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			setBeans(dao.consultarSubstituicao());
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public void consultar(String desSubstituicaoFonetica) throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			setBeans(dao.consultarSubstituicao(desSubstituicaoFonetica));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void consultar(SubstituicaoBean substituicaoFoneticaBean) throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			setBeans(dao.consultarSubstituicao(substituicaoFoneticaBean));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public String substituir(String token)
	{
		String palavra;
		for(int i = 0; i < beans.size(); i++)
		{
			palavra = ((SubstituicaoBean)beans.get(i)).getDesPalavra();
			if(palavra.equalsIgnoreCase(token))
				return ((SubstituicaoBean)beans.get(i)).getDesSubstituicao();
		}
		return token;
	}
	
	public void atualizarExcluir(HashMap atualizar, HashMap excluir, int qtdElementos, boolean existeErro) throws sys.BeanException
	{
		try {
			Dao dao = Dao.getInstance();
			this.setMensagem(dao.atualizarExcluirSubstituicao(atualizar, excluir, qtdElementos, existeErro));
		}
		catch(Exception e) {
			throw new sys.BeanException(e.getMessage());
		}		
	}
}
