package sys;

public class BeanException extends Exception
{
	public BeanException()
	{
		super();
	}
	
	public BeanException(String msg)
	{
		super(msg);
	}
}
