package sys;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TypeUtil {

	/**
	 * M�todo para formatar um objeto Date em uma data em String.
	 * @param data Um objeto Date
	 * @param formato O formato da data a ser formada
	 * @return A data formata em String
	 */
	public static String formatData(java.util.Date data, String formato) {
		try {		
			if ((formato == null) || (formato.equals("")))
				formato = "dd/MM/yyyy";			
			SimpleDateFormat df = new SimpleDateFormat(formato);
			return df.format(data);
		} catch (Exception e) {
			return "";
		}	    
	}
	/**
	 * M�todo para formatar um objeto Date em uma data em String. 
	 * � assumido o formato de Data dd/mm/yyyy.
	 * @param data Um objeto Date
	 * @return A data formata em String
	 */	
	public static String formatData(java.util.Date data) {
		return formatData(data, null);
	}	
	
	/**
	 * M�todo para transformar uma data em String em um objeto Date.
	 * @param data Um data em String
	 * @param formato O formato da data a ser transformada
	 * @return O objeto Date transformado
	 */
	public static java.util.Date parseData(String data, String formato) { 
		try {
			if ((formato == null) || (formato.equals("")))
				formato = "dd/MM/yyyy";			
			SimpleDateFormat df = new SimpleDateFormat(formato);
			return df.parse(data);
		} catch (Exception e) {
			return null;
		}
	}
	/**	
	 * M�todo para transformar uma data em String em um objeto Date.
	 � assumido o formato de Data dd/mm/yyyy
	 * @param data Um data em String	 
	 * @return O objeto Date transformado
	 */
	public static java.util.Date parseData(String data) {
		try {
			return parseData(data, null);
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * M�todo para transformar uma data do pacote java.util para um data do pacote java.sql.
	 * Este convers�o � necess�ria pois o JDBC trabalha com a data do pacote java.sql.
	 * @param data Um data do pacote java.sql
	 * @return O objeto Date do pacote java.sql
	 */
	public static java.sql.Date converteDataToSql(Date data) { 
		try {
			return new java.sql.Date(data.getTime());
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * M�todo para transformar um objeto Date em um Timestamp.
	 * @param data Um objeto Date
	 * @return O objeto Timestamp convertido
	 */
	public static Timestamp converteDataToTime(Date data) { 
		try {
			return new Timestamp(data.getTime());
		} catch (Exception e) {
			return null;
		}
	}	
	
	/**
	 * M�todo para converte um atributo boolean e uma String
	 * @param campo Um atributo do tipo boolean
	 * @return O objeto boolean convertido para String, com os valores true = 'S' e false = 'N'
	 */
	public static String converteBoolToString(boolean campo) { 
		if (campo) return "S";
		else return "N";
	}
	
	/**
	 * M�todo para converte um atributo String e um valor boolean
	 * @param campo Um atributo do tipo String
	 * @return O objeto String convertido para boolean, com os valores 'S' = true e 'N' = false
	 */
	public static boolean converteStringToBool(String campo) { 
		if ("S".equals(campo)) return true;
		else return false;
	}		
	
	/**
	 * M�todo para formatar um objeto Double em uma numero em String.
	 * @param numero Um objeto Double
	 * @param formato O formato do numero a ser formatado
	 * @return O numero formatado em String
	 */
	public static String formatNumero(Double numero, String formato) {
		try {		
			if ((formato == null) || (formato.equals("")))
				formato = "0.00";			
			DecimalFormat df = new DecimalFormat(formato);
			return df.format(numero);
		} catch (Exception e) {
			return "";
		}	    
	}

	/**	
	 * M�todo para formatar um objeto Double em uma numero em String.
	 * � assumido o formato de Numero 0.00.
	 * @param numero Um objeto Double
	 * @return O numero formatado em String
	 */		
	public static String formatNumero(Double numero) {
		return formatNumero(numero, null);
	}		
	
}