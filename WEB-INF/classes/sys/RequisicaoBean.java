package sys;

import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class RequisicaoBean {

	private String id;
	private String abrevSist;
	private String codPerfil;
	private String jspOrigem;
	private String sigFuncao;
	private String cmdFuncao;
	private String token;
	private String acao;
	public final static String CMDDEFAULT = "login";


	public RequisicaoBean() {

		id = "";
		abrevSist = "";
		codPerfil="0";
		jspOrigem = "/sys/login.jsp";
		sigFuncao = "";
		cmdFuncao = CMDDEFAULT;
		token = "";
		acao = "";
	}

	public String getAbrevSist() {
		return abrevSist;
	}

	public String getCmdFuncao() {
		return cmdFuncao;
	}

	public String getJspOrigem() {
		return jspOrigem;
	}

	public String getSigFuncao() {
		return sigFuncao;
	}

	public String getToken() {
		return token;
	}

	public void setAbrevSist(String abrevSist) {
		if (abrevSist == null) abrevSist = "ACSS";		
		this.abrevSist = abrevSist;
	}

	public void setCmdFuncao(String cmdFuncao) {
		if (cmdFuncao == null) cmdFuncao = CMDDEFAULT;
		this.cmdFuncao = cmdFuncao;
	}

	public void setJspOrigem(String jspOrigem) {
		if (jspOrigem == null) jspOrigem = "/sys/login.jsp";	
		this.jspOrigem = jspOrigem;
	}

	public void setSigFuncao(String sigFuncao) {
		if (sigFuncao==null) sigFuncao="";
		this.sigFuncao = sigFuncao;
	}

	public void setToken(String token) {
		if (token==null) token="";
		this.token = token;
	}

	public String getId() {
		return id;
	}

	
	public void setCodPerfil(String codPerfil) {
		if (codPerfil==null) codPerfil="0";
		if (codPerfil.length()==0) codPerfil="0";
		
		this.codPerfil = codPerfil;
	}

	public String getCodPerfil() {
		return codPerfil;
	}

	
	
	public void setId(String id) {
		if (id==null) id="";
		this.id = id;
	}
	
	public String getAcao() {
		return acao;
	}

	public void setAcao(String acao) {
		if (acao == null) acao = "";
		this.acao = acao;
	}
	
	
	
	
	
	
	public boolean isFuncao(RequisicaoBean RequisicaoBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException, ServiceLocatorException, BeanException {
		ServiceLocator serviceloc =  sys.ServiceLocator.getInstance();
		int iPosInicial = serviceloc.getPosInicial();
		int iPosFinal   = serviceloc.getPosFinal();
		int iPosSigFuncao=Integer.parseInt(RequisicaoBeanId.getSigFuncao().substring(iPosInicial-1,iPosInicial));
		return (iPosSigFuncao==iPosFinal);
		
		
	}

	

}