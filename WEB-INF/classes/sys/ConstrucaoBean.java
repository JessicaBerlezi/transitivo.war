package sys;
import java.util.Vector;

/*
* <b>Title:</b>        Construcao<br>
* <b>Description:</b>  Bean para mensagem de comandos em construcao<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ConstrucaoBean  {

  private String jspOrigem;

  private String msgErro ;    
  
  public ConstrucaoBean(){

    jspOrigem     	  = "/sys/login.jsp";	    	
    msgErro           = "" ;	
  }
  
  public void setJspOrigem(String jspOrigem)  {
	if (jspOrigem == null) jspOrigem = "/sys/login.jsp";	  
  	this.jspOrigem= jspOrigem;
  }
  public String getJspOrigem()  {
  	return this.jspOrigem;
  }
   
  public void setMsgErro(Vector vErro)   {
    for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
     }
   }
public void setMsgErro(String msgErro)   {
	if (msgErro == null) msgErro = "";	
	this.msgErro = msgErro ;
   }   
   public String getMsgErro()   {
   	return this.msgErro;
   }   

  
}