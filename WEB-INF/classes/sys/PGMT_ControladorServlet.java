package sys;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.DiskFileUpload;

import REC.ParamOrgBean;

public class PGMT_ControladorServlet extends HttpServlet {
	
	
//	public final static long serialVersionUID =0;
	
	private HashMap<String,sys.Command> commands;
	private int idJanela;
	public static final int SEGUNDOS_POR_ANO = 60*60*24*365;
	
	public static final String JSPDIR   = "/WEB-INF/jsp/";
	public static final String AJUDADIR = "/WEB-INF/ajuda";    
	
	public final static String CMDDEFAULT = "login";
	public final static String CMDSAIDA   = "encerrar";
	
	public final static String JSPMENU  = "menuSistema.jsp";
	public final static String JSPERROR = "sys/ErrorPage.jsp";    
	public final static String JSPLOGIN = "/sys/login.jsp";
	/*
	public final static String JSPLOGINMAPA = "/sys/login_mapa.jsp";
	*/  
	public final static String JSPLOGINMAPA = "/sys/login.jsp";
	public final static String JSPSAIDA = "sys/SaidaSmit.jsp";
	public final static String JSPCONSTRUCAO = "/sys/Construcao.jsp";
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		try { 
			initCommands();
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}
	}
	
	public void service(HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {

		UploadFile upload = null;
		String ajudaException = "inicio";
		sys.RequisicaoBean RequisicaoBeanId = null;
		try {
			
			String nomeAprest = "sys/construcao.jsp";
			
			//Cria o bean com os dados da requisi��o
			RequisicaoBeanId = new sys.RequisicaoBean();
			if (DiskFileUpload.isMultipartContent(req)) {
				ajudaException = "upload";
				upload = new UploadFile(req);
				RequisicaoBeanId.setId(upload.getParameter("j_id"));
				RequisicaoBeanId.setAbrevSist(upload.getParameter("j_abrevSist"));
				RequisicaoBeanId.setCmdFuncao(upload.getParameter("j_cmdFuncao"));
				RequisicaoBeanId.setJspOrigem(upload.getParameter("j_jspOrigem"));
				RequisicaoBeanId.setSigFuncao(upload.getParameter("j_sigFuncao"));
				RequisicaoBeanId.setToken(upload.getParameter("j_token"));
				RequisicaoBeanId.setAcao(upload.getParameter("acao"));
				if (upload.getMsgErro().trim().length()>0)
				{
					ajudaException = "upload-construcao";
					RequisicaoBeanId.setCmdFuncao("construcao");
					sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
					ConstrucaoBeanId.setMsgErro(upload.getMsgErro());
					ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
					req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
				}
			} else {	
				ajudaException = "requestHttp";
				if(req != null){
					
				RequisicaoBeanId.setId(req.getParameter("j_id"));
				RequisicaoBeanId.setAbrevSist(req.getParameter("j_abrevSist"));
				RequisicaoBeanId.setCmdFuncao(req.getParameter("j_cmdFuncao"));
				RequisicaoBeanId.setJspOrigem(req.getParameter("j_jspOrigem"));
				RequisicaoBeanId.setSigFuncao(req.getParameter("j_sigFuncao"));
				RequisicaoBeanId.setCodPerfil(req.getParameter("j_codPerfil"));
				RequisicaoBeanId.setToken(req.getParameter("j_token"));
				RequisicaoBeanId.setAcao(req.getParameter("acao"));
				}
			}
			
			ajudaException="next = "+ RequisicaoBeanId.getJspOrigem();
//			ajudaException="next = "+"AutoCicloVidaPenal.html";
			// Jsp para apresentacao
//			String next = RequisicaoBeanId.getJspOrigem();
			String next = "AutoCicloVidaPenal.html";
			
			
			
			
			//Verificando se foi solicitado o encerramento do sistema pela tela de login
			if (isEnceramentoLogin(RequisicaoBeanId, req, res)) {
				return;
			}
			
			//Verificando se j� existe uma sess�o aberta na volta para tela de login
			HttpSession session = VerificaRetornoLogin(RequisicaoBeanId, req, res);
			
			//Verificar se a sessao existe e se o comando utilizado nao e login
			//Verificando sessao - true ==> inativa
			if (isSessaoInativa(session,RequisicaoBeanId, req, res)) {
				return;
			}
			
			
			// Criar os Beans e atualizar as variaveis sessao 		
			ACSS.UsuarioFuncBean UsuarioFuncBeanId =
				(ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			ACSS.UsuarioBean UsuarioBeanId =
				(ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			ACSS.SistemaBean SistemaBeanId =
				(ACSS.SistemaBean) session.getAttribute("SistemaBeanId");
			
			if (UsuarioBeanId == null) {
				UsuarioBeanId = new ACSS.UsuarioBean();
				UsuarioBeanId.setNomUsuario("'sem Autentica��o'");
			}
			if (UsuarioFuncBeanId == null)
				UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();				
			if (SistemaBeanId == null)
				SistemaBeanId = new ACSS.SistemaBean();
			
			//Verifica se o usu�rio esta bloqueado                   
			if (VerificaUsuarioBloqueado(RequisicaoBeanId, UsuarioBeanId, req, res)) {
				return;
			}  
			
			
			// Prepara a lista de comandos do usuario - Esta preparando sempre
			UsuarioFuncBeanId.setJ_abrevSist(RequisicaoBeanId.getAbrevSist());
			if (RequisicaoBeanId.getJspOrigem().equals("/login.jsp") == false) preparaCommands(UsuarioFuncBeanId,req);
			
			// Se n�o for login - valida controle de acesso
			nomeAprest = ValidaControleAcesso(RequisicaoBeanId, UsuarioBeanId, UsuarioFuncBeanId, SistemaBeanId, req, res);
//			RequisicaoBeanId.setJspOrigem(jspOrigem)
			// Verificar se o comando nao existe -  em constru��o com mensagem explicativa
			isCommandExiste(RequisicaoBeanId, commands, req, res);
			
			
			// M�todo que ler x, y, z  e atribui aos objetos referente.
			next = ExecutaCommand(RequisicaoBeanId, commands, req, res, upload, nomeAprest);
			
			//Cookies
			String nextLogin = validaCookie(UsuarioBeanId, req, res, RequisicaoBeanId, next);
			if (!nextLogin.equals(""))
				next = nextLogin;
			
			//Seta a bean de Usu�rio, ap�s atualizar os cookies
			ajudaException="session.setAttribute(UsuarioBeanId) "+RequisicaoBeanId.getCmdFuncao();
			session.setAttribute("UsuarioBeanId", UsuarioBeanId);
			
			/*Verificar caso usuario perdeu sess�o*/
			if ( ( (UsuarioBeanId.getNomUserName().length()==0) || 
	            	   (UsuarioBeanId.getCodOrgaoAtuacao().length()==0) || 
	            	   (UsuarioBeanId.getCodSenha().length()==0)
	            	  ) && (!nextLogin.equals(JSPLOGINMAPA)
	            	    && !RequisicaoBeanId.getCmdFuncao().equals("AjudaCommand"))
	            	    && !RequisicaoBeanId.getCmdFuncao().equals("construcao"))
	            	next="/sys/login.jsp";
			
			ajudaException="valBiometria "+RequisicaoBeanId.getCmdFuncao();	
			if (RequisicaoBeanId.getAcao().equals("valBiometria"))
			{
			    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
				if ("S".equals(ParamOrgaoId.getParamOrgao("ATIVA_BIOMETRIA_ORGAO","N","10"))) {
					
					BiometriaCmd BiometriaCommand = new BiometriaCmd(next);
					next = BiometriaCommand.execute(req);
					if (!next.equals("/sys/ValidaBiometria.jsp"))
						UsuarioFuncBeanId.setJaAutenticouBiometria(true);
				}
			}
			
			// Gera o id da janela
			String n_id = geraId();
			
			// Preparar o token para ser enviado pelo jsp
			ajudaException="token "+RequisicaoBeanId.getCmdFuncao();			
			String n_token = null;
			if (!RequisicaoBeanId.getCmdFuncao().equals(CMDDEFAULT) && !RequisicaoBeanId.getJspOrigem().equals(JSPMENU)) {									
				n_token = CommandToken.gravaToken(session, n_id);
			}
			
			RequisicaoBeanId.setId(n_id);
			RequisicaoBeanId.setToken(n_token);
			RequisicaoBeanId.setJspOrigem(next);
			req.setAttribute("RequisicaoBeanId", RequisicaoBeanId);
			
			//Grava o acesso ao command
			ajudaException="Grava o acesso ao command "+RequisicaoBeanId.getCmdFuncao();
			gravaAcesso(UsuarioBeanId, UsuarioFuncBeanId, req.getSession().getId());
			ajudaException="isNextJsp "+RequisicaoBeanId.getCmdFuncao();			
			// Passar o controle para o jsp fazer a apresentacao. Os beans necessarios est�o no request (req) e na sessao
			LocalizarNextJsp(RequisicaoBeanId, UsuarioFuncBeanId, next, req, res);
			
		} 
		catch (IllegalStateException ie) {
			System.out.println("");
			System.out.println("* Service-IllegalStateException: "+ie.getMessage()+" CmdFuncao "+RequisicaoBeanId.getCmdFuncao()+
					" ajudaException: "+ajudaException);
			System.out.println("");
			
			req.setAttribute("javax.servlet.jsp.jspException", ie);
			GerenciadorException.registraException(ie, req);
		}
		
		catch (Exception e) {
			System.out.println("");			
			System.out.println("* ExceptionService: "+e.getMessage()+" ajudaException: "+ajudaException+" CmdFuncao "+RequisicaoBeanId.getCmdFuncao());
			System.out.println("");
			
			req.setAttribute("javax.servlet.jsp.jspException", e);
			GerenciadorException.registraException(e, req);
		}
	}
	
	private Command lookupCommand(String cmd) throws CommandException {
		
		if ((cmd == null) || (commands.containsKey(cmd.toLowerCase()) == false))
			cmd = "construcao";
		
		if (commands.containsKey(cmd.toLowerCase()))
			return (Command) commands.get(cmd.toLowerCase());
		else
			throw new CommandException("Controler 001 - Identifica��o de Comando inv�lida");
	}
	
	private void initCommands() throws ServiceLocatorException {
        // Pegar parametros de controle de pagina do DF
        try {		
            ServiceLocator ServiceId = ServiceLocator.getInstance();
            ServiceId.setJ_sistAbrev(getInitParameter("sistAbrev"));			
            ServiceId.setQualBanco(getInitParameter("qualBanco"));						
			ServiceId.setQualBroker(getInitParameter("qualBroker"));
            ServiceId.setQuantConexoes(getInitParameter("quantConexoes"));
            ServiceId.setQuantCresc(getInitParameter("crescimento"));
            // Preparar as conexoes
            int qBco = Integer.parseInt(ServiceId.getQualBanco());
            if (qBco < 0 || qBco > 3) qBco = 0;
            for (int i = 0; i < 4; i++) {
                ServiceId.setAbrevSistConn(i, getInitParameter("abrevSistConn_" + qBco + "_" + i));
                ServiceId.setDriver(i, getInitParameter("driver_" + qBco + "_" + i));
                ServiceId.setDbURL(i, getInitParameter("dbURL_" + qBco + "_" + i));
                ServiceId.setUser(i, getInitParameter("user_" + qBco + "_" + i));
                ServiceId.setPass(i, getInitParameter("pass_" + qBco + "_" + i));
            }			
            //ServiceId.getPoolConnection();
            
            // Verifica se existe alguma sess�o aberta na tabela TCAU_SESSAO
            ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
            ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
            ParamSistemaBeanId.PreparaParam();
            if (ParamSistemaBeanId.getParamSist("NOME SERVIDOR").equalsIgnoreCase(Util.hostName()))
            {
                ACSS.Dao dao = ACSS.Dao.getInstance();
                dao.LimpaTcauSessao();
            }
        }
        catch (Exception e) {
            throw new ServiceLocatorException(e.getMessage());
        } 
        
        // Criar o comandos para funcionalidade gerais e obrigatorias
        commands = new HashMap<String,sys.Command>();		
        
        /*Verificar Sigla e Senha e Sistemas habilitados*/
        commands.put("login".toLowerCase(), new sys.LoginSistemasCommand("/ACSS/menuSistema.jsp"));
        
        /*Montar o Menu de op��es ap�s o usu�rio ter selecionado o Sistemas*/
        /* ??????*/         

        
        
        commands.put("construcao".toLowerCase(), new sys.ConstrucaoCommand("/sys/Construcao.jsp"));
        commands.put("AjudaCommand".toLowerCase(), new sys.AjudaCommand("/sys/AjudaCommand.jsp"));
        commands.put("InformacoesCmd".toLowerCase(), new ACSS.InformacoesCmd("/ACSS/InformacoesCmd.jsp"));
        
        idJanela = 0;			
    }
	
	private void preparaCommands(ACSS.UsuarioFuncBean UsrFuncBeanId,HttpServletRequest req) {

		
		/*MICHEL*/
		
		
		
		UsrFuncBeanId.setCodPerfil(req.getParameter("j_codPerfil"));
		UsrFuncBeanId.getFuncoes(UsrFuncBeanId);


		System.out.println("Req Perfil:"+req.getParameter("j_codPerfil"));
		System.out.println("UsrFuncBeanId-Perfil:"+UsrFuncBeanId.getCodPerfil());

		
		commands = new HashMap<String,sys.Command>();
		// Criar o comandos para funcionalidade gerais e obrigatorias
		for (int i = 0; i < UsrFuncBeanId.getFuncoes().size(); i++) {
			String cmdNome = UsrFuncBeanId.getFuncoes(i).getNomExecuta();
			if (cmdNome.length() > 0) {
				try {
					sys.Command cmd = (sys.Command) Class.forName(cmdNome).newInstance();
					cmd.setNext(UsrFuncBeanId.getFuncoes(i).getNomApresentacao());
					commands.put(cmdNome.toLowerCase(), cmd);
					
				} catch (Exception e) {
					System.out.println("Erro:"+e.getMessage().toString());
				}
			}
		}
		commands.put("login".toLowerCase(), new sys.LoginSistemasCommand("/" + UsrFuncBeanId.getJ_abrevSist() + "/menuSistema.jsp"));
		commands.put("construcao".toLowerCase(), new sys.ConstrucaoCommand("/sys/Construcao.jsp"));
		commands.put("AjudaCommand".toLowerCase(), new sys.AjudaCommand("/sys/AjudaCommand.jsp"));
		commands.put("InformacoesCmd".toLowerCase(), new ACSS.InformacoesCmd("/ACSS/InformacoesCmd.jsp"));
	}
	
	public String geraId() throws CommandException {
		try {
			String retorno = sys.Util.encriptar(String.valueOf(++idJanela));
			return retorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("geraToken: " + ue.getMessage());	
		}
	}
	
	public void destroy(){
		try{
			ServiceLocator ServiceId = ServiceLocator.getInstance();
			ServiceId.close();
		}
		catch(Exception e){
			System.err.println("*"+e.getMessage()+"\n");
		}
	}
	
	public void gravaAcesso(ACSS.UsuarioBean UsrUsuarioBean, ACSS.UsuarioFuncBean UsrFuncBeanId, String numSessao)
	throws CommandException {
		if ("".equals(UsrUsuarioBean.getOrgao().getCodOrgao())==false) { 	 
			try  {
				sys.Dao dao = sys.Dao.getInstance();
				dao.LogAcessoInsert(UsrUsuarioBean, UsrFuncBeanId, numSessao);		   
			}
			catch (Exception e) {
				throw new sys.CommandException("Grava��o Acesso: " + e.getMessage());				 
			}				
		}
	}
	
	
	public boolean isEnceramentoLogin(RequisicaoBean RequisicaoBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException {
		try{
			HttpSession session = req.getSession();
			if ((RequisicaoBeanId.getJspOrigem().indexOf(JSPLOGIN) >= 0) && (RequisicaoBeanId.getCmdFuncao().equals(CMDSAIDA))) {
				// Caso seja, redireciona para a tela de encerramento
				RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + JSPSAIDA);
				rd.forward(req, res);
				req.setAttribute("codUf", req.getParameter("codUf"));
				try{
						session.invalidate();			 
				} catch (Exception e) {		}				
				return true;
			}
		} catch (Exception e) {
			throw new ServletException("*"+e.getMessage()+"\n");
		}
		return false;
	} 
	
	private HttpSession VerificaRetornoLogin(RequisicaoBean RequisicaoBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException {
		HttpSession session = req.getSession(false);	
		try{
			if ((RequisicaoBeanId.getJspOrigem().indexOf(JSPLOGIN) < 0) && 
					(RequisicaoBeanId.getCmdFuncao().equals(CMDDEFAULT))) {
				String codUf = "RJ";
				Date dtIniSessao = new Date();
				Date dtFimSessao = new Date();
				if ((session != null)) {
					try {
						ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
						if (UsuarioBeanId==null) codUf="RJ" ;
						else codUf = UsuarioBeanId.getCodUF();
						// 	Caso exista, destroi a sess�o, criando uma outra
						dtIniSessao = new Date(session.getCreationTime());
						session.invalidate();
					} catch (IllegalStateException e) {
						System.out.println("VerificaRetornoLogin: "+e.getMessage());	
					}
				}
				// cria uma nova sessao
				session = req.getSession(true);
				req.setAttribute("codUf", codUf);
				req.setAttribute("tempo_sessao",ControleSessaoBean.CalculaTempoSessao(dtIniSessao, dtFimSessao));
			}			  
		} catch (Exception e) {
			throw new ServletException("*"+e.getMessage()+"\n");
		}
		return session;
	} 
	
	private boolean isSessaoInativa(HttpSession session,RequisicaoBean RequisicaoBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException {
		boolean bSessaoInativa = false;
		if (!RequisicaoBeanId.getCmdFuncao().equals(CMDDEFAULT)) {
			try{  if (session.isNew()) { bSessaoInativa = false; }
			} catch (Exception e) {	     bSessaoInativa = true;	}
			try{  if (bSessaoInativa) {
					String msgConstrucao =	"Sess�o de trabalho inativa. Favor 'Retornar' no Menu do Sistema e refazer a autentica��o.";
					RequisicaoBeanId.setCmdFuncao("construcao");
					sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
					ConstrucaoBeanId.setMsgErro(msgConstrucao);
					ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
					req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
					RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + JSPCONSTRUCAO);
					rd.forward(req, res);					
				}
			} catch (Exception e) {
				
			}
		}
		return bSessaoInativa;
	} 
	
	private boolean VerificaUsuarioBloqueado(RequisicaoBean RequisicaoBeanId, ACSS.UsuarioBean UsuarioBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException {
		try{            
			ACSS.UsuarioBean UsuarioBloqueado = new ACSS.UsuarioBean();
			if (UsuarioBeanId.getCodUsuario().equals("0")) {
				UsuarioBloqueado.setNomUserName(req.getParameter("nomUserName"));
				ACSS.OrgaoBean OrgaoIdBloq = new ACSS.OrgaoBean();              
				OrgaoIdBloq.setCodOrgao(req.getParameter("codOrgao"));
				UsuarioBloqueado.setOrgao(OrgaoIdBloq);
				UsuarioBloqueado.Le_Usuario(1);
			} else		UsuarioBloqueado = UsuarioBeanId;
			
			ACSS.ConsultaUsuarioLogadoBean bloqueioBean = new ACSS.ConsultaUsuarioLogadoBean();        
			if (bloqueioBean.VerificaBloqueio(UsuarioBloqueado.getCodUsuario())) {                
				String msgConstrucao = "Usu�rio " + UsuarioBloqueado.getNomUserName() + " est� bloqueado.";
				sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
				ConstrucaoBeanId.setMsgErro(msgConstrucao);
				ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
				req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);                
				RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + JSPCONSTRUCAO);
				rd.forward(req, res);
				req.getSession().invalidate();
				return true;
			}
		} catch (Exception e) {
			throw new ServletException("*"+e.getMessage()+"\n");
		}
		
		return false;
	} 
	
	private String ValidaControleAcesso(RequisicaoBean RequisicaoBeanId, ACSS.UsuarioBean UsuarioBeanId, ACSS.UsuarioFuncBean UsuarioFuncBeanId, ACSS.SistemaBean SistemaBeanId, HttpServletRequest req, HttpServletResponse res)
	throws ServletException {
		String nomeAprest = JSPCONSTRUCAO;
		try{
			HttpSession session = req.getSession();
			
  			String codPerfil=RequisicaoBeanId.getCodPerfil();
			
			
			if (codPerfil==null) codPerfil="0";
			if (codPerfil.length()==0) codPerfil="0";
			
			
			
			if (!(RequisicaoBeanId.getCodPerfil().equals("0")))
			{
				UsuarioFuncBeanId.setCodPerfil(RequisicaoBeanId.getCodPerfil());
				UsuarioFuncBeanId.getFuncoes(UsuarioFuncBeanId);
			}

			if ((!RequisicaoBeanId.getCmdFuncao().equals(CMDDEFAULT)) && (!RequisicaoBeanId.getCmdFuncao().equals("construcao"))
					&& (!RequisicaoBeanId.getCmdFuncao().equals("AjudaCommand")) && (!RequisicaoBeanId.getCmdFuncao().equals("InformacoesCmd"))) {
				// Nao tem acesso ao comando		
				int i = UsuarioFuncBeanId.isFuncaoUsuario(RequisicaoBeanId.getSigFuncao());
				if (i < 0) {
					String msgConstrucao = "Usu�rio " + UsuarioBeanId.getNomUsuario() +
					" n�o tem acesso a Funcionalidade: " + RequisicaoBeanId.getSigFuncao();
					RequisicaoBeanId.setCmdFuncao("construcao");
					
					sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
					ConstrucaoBeanId.setMsgErro(msgConstrucao);
					ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
					req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
					
				} else {
					// Posicionar o camando a ser executado
					RequisicaoBeanId.setCmdFuncao(UsuarioFuncBeanId.getFuncoes(i).getNomExecuta());
					nomeAprest  = UsuarioFuncBeanId.getFuncoes(i).getNomApresentacao();
				}
				// Sempre coloca os Beans com as Funcionalidades na sessao
				UsuarioFuncBeanId.setJ_cmdFuncao(RequisicaoBeanId.getCmdFuncao());
                UsuarioFuncBeanId.setJ_sigFuncao(RequisicaoBeanId.getSigFuncao());
                UsuarioFuncBeanId.setJ_jspOrigem(RequisicaoBeanId.getJspOrigem());
				UsuarioBeanId.setMsgErro("");
				session.setAttribute("SistemaBeanId", SistemaBeanId);
                session.setAttribute("UsuarioFuncBeanId", UsuarioFuncBeanId);
			}
		} catch (Exception e) {
			throw new ServletException("*"+e.getMessage()+"\n");
		}
		
		return nomeAprest;
	}
	
	private void isCommandExiste(RequisicaoBean RequisicaoBeanId, HashMap commands, HttpServletRequest req, HttpServletResponse res)
	throws ServletException, ServiceLocatorException,BeanException {
		ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
		ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
		ParamSistemaBeanId.PreparaParam();
		String dtUltLimpezaHist=ParamSistemaBeanId.getParamSist("DAT_ULT_LIMPEZA");
		if (RequisicaoBeanId.getSigFuncao().length()>0) {
			if (Util.DifereDias(Util.formatedTodayDDMMAAAA().substring(0,10),dtUltLimpezaHist)<0) {
				if (RequisicaoBeanId.isFuncao(RequisicaoBeanId,req,res)) {
					String msgConstrucao = "Comando " + RequisicaoBeanId.getCmdFuncao() + " para a Fun��o " + RequisicaoBeanId.getSigFuncao()
					+ " de um bla 1 implementado.";
					RequisicaoBeanId.setCmdFuncao("construcao");				// Cria o bean em construcao - coloca na req e muda construcao
					sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
					ConstrucaoBeanId.setMsgErro(msgConstrucao);
					ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
					req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
				}
			}
		}
		
		
		
		
		try{
			if (!commands.containsKey(RequisicaoBeanId.getCmdFuncao().toLowerCase())) {
				String msgConstrucao = "Comando " + RequisicaoBeanId.getCmdFuncao() + " para a Fun��o " + RequisicaoBeanId.getSigFuncao()
				+ " de um bla 2 implementado.";
				RequisicaoBeanId.setCmdFuncao("construcao");				// Cria o bean em construcao - coloca na req e muda construcao
				sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
				ConstrucaoBeanId.setMsgErro(msgConstrucao);
				ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
				req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
			}
		} catch (Exception e) {
			throw new ServletException("*"+e.getMessage()+"\n");
		}
	} 
	
	private String ExecutaCommand(RequisicaoBean RequisicaoBeanId, HashMap commands, HttpServletRequest req, HttpServletResponse res, UploadFile upload, String nomeAprest)
	throws CommandException {
		String next;

		if (RequisicaoBeanId==null) System.out.println("ExecutaCommand RequisicaoBeanId=null");
		if (commands==null)         System.out.println("ExecutaCommand commands=null");
		if (req==null)              System.out.println("ExecutaCommand req=null");
		if (res==null)              System.out.println("ExecutaCommand res=null");
		if (nomeAprest==null)       System.out.println("ExecutaCommand nomeAprest=null");
		/*if (upload==null)         System.out.println("ExecutaCommand upload=null");*/
		
		try {				
			Command cmd = lookupCommand(RequisicaoBeanId.getCmdFuncao());	
			
			if (RequisicaoBeanId.getCmdFuncao().toLowerCase().indexOf("nullcommand") > 0)
				cmd.setNext(nomeAprest);
			
			if (DiskFileUpload.isMultipartContent(req))	
				next = cmd.execute(req, upload); 
			else {
				if ("GER".equals(RequisicaoBeanId.getAbrevSist())) {
					next = cmd.execute(this.getServletContext(),req,res);
				}
				else next = cmd.execute(req);
			}
			if (next==null)
			{
				req.setAttribute("javax.servlet.jsp.jspException","Apresenta��o nula.");
				GerenciadorException.registraException(new Exception(), req);
				next = JSPERROR;
			}
		} catch (CommandException e) {
			req.setAttribute("javax.servlet.jsp.jspException", e);
			GerenciadorException.registraException(e, req);
			next = JSPERROR;
		}
		
		return next;
	}
	
	private void LocalizarNextJsp(RequisicaoBean RequisicaoBeanId, ACSS.UsuarioFuncBean UsuarioFuncBeanId, String next, HttpServletRequest req, HttpServletResponse res)
	throws ServletException, IOException {
		
		if (RequisicaoBeanId==null)  System.out.println("LocalizarNextJsp RequisicaoBeanId=null");
		if (UsuarioFuncBeanId==null) System.out.println("LocalizarNextJsp UsuarioFuncBeanId=null");
		if (next==null)              System.out.println("LocalizarNextJsp next=null");
		if (req==null)               System.out.println("LocalizarNextJsp req=null");
		if (res==null)               System.out.println("LocalizarNextJsp res=null");
		
		
		if (next.length() > 0) {			
			try {
				RequestDispatcher rd = null;
				// Verificar se � ajuda
				if (!RequisicaoBeanId.getCmdFuncao().equals("AjudaCommand"))
				{
					if (VerificaJsp(getServletContext().getRealPath( JSPDIR + next))) 
						rd = getServletContext().getRequestDispatcher(JSPDIR + next);
				}else
				{
					if (VerificaJsp(getServletContext().getRealPath( AJUDADIR + next)))
						rd = getServletContext().getRequestDispatcher(AJUDADIR + next);
				}
				
				
				if (UsuarioFuncBeanId.getJsitBiometria().equals("A"))
				{
					if (!UsuarioFuncBeanId.getJaAutenticouBiometria())
					{
						req.setAttribute("next", next) ;
						rd = getServletContext().getRequestDispatcher(JSPDIR + "/sys/ValidaBiometria.jsp");
					}
				}
				rd.forward(req, res);
			} catch (Exception e) 
			{
				//Cria o bean em construcao - coloca na req e muda construcao
				sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
				/*
				ConstrucaoBeanId.setMsgErro("Erro na localiza��o do arquivo de apresenta��o "
						+ JSPDIR + next);
				*/
				if(RequisicaoBeanId.getCmdFuncao().equals("AjudaCommand"))
				    ConstrucaoBeanId.setMsgErro("Erro na localiza��o do arquivo de apresenta��o "
						+ AJUDADIR + next);
				else
					ConstrucaoBeanId.setMsgErro("Erro na localiza��o do arquivo de apresenta��o "
						+ JSPDIR + next);				
				ConstrucaoBeanId.setJspOrigem(RequisicaoBeanId.getJspOrigem());
				req.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
				RequestDispatcher rd;
				rd = getServletContext().getRequestDispatcher(JSPDIR+"/sys/Construcao.jsp");
				rd.forward(req, res);
			}
		}
	}
	
	public boolean VerificaJsp(String pathJsp) throws BeanException, IOException{
		boolean bOk = false;
        //Verifica se existe jsp 
		File arq = new File(pathJsp);
		FileInputStream jsp;
		jsp = new FileInputStream(arq);
		int jspExiste = jsp.read(); //L� a arquivo
		if (jspExiste != 0) //existe Jsp 
			bOk = true;
		
		return bOk;
	}
	
	
	public String validaCookie(ACSS.UsuarioBean UsuarioBeanId, HttpServletRequest req, HttpServletResponse res, RequisicaoBean RequisicaoBeanId, String next)
	throws CommandException {
		String nextLogin = "";
		try {
			Cookie cookieUF = null;
			Cookie cookieCodOrgaoLotacao = null;
			Cookie cookieCodOrgaoAtuacao = null;
			Cookie cookieEnabled = null;
			
			Cookie[] cookies = req.getCookies();
			
			//Busca todos os cookies j� criados na m�quina do usu�rio
			if (cookies != null)
			{
				for (int i = 0; i < cookies.length; i++)
				{
					if (cookies[i].getName().equals("cookieUF"))
						cookieUF = cookies[i];
					if (cookies[i].getName().equals("cookieCodOrgaoLotacao"))
						cookieCodOrgaoLotacao = cookies[i];
					if (cookies[i].getName().equals("cookieCodOrgaoAtuacao"))
						cookieCodOrgaoAtuacao = cookies[i];
					if (cookies[i].getName().equals("cookieEnabled"))
						cookieEnabled = cookies[i];
				}
			}
			REC.ParamUFBean ParamUFBeanId = new REC.ParamUFBean();
			
			String codUF = req.getParameter("codUf");
			if(codUF==null)      codUF="";
			
			/*if (cookieEnabled == null)
			 codUF = "RJ";*/
			
			UsuarioBeanId.setCookieEnabled(cookieEnabled);
			
			//Se n�o houver cookies na m�quina do usu�rio, estes ser�o criados
			if (cookieUF == null || !codUF.equals(""))
			{
				if (!codUF.equals(""))
				{
					cookieUF = new Cookie("cookieUF", codUF);
					ParamUFBeanId.PreparaParam(codUF);
					ParamUFBeanId.setCodUF(codUF);
					cookieUF.setMaxAge(SEGUNDOS_POR_ANO);
					//cookieUF.setPath(req.getContextPath());
					UsuarioBeanId.setcookieUF(cookieUF);
					res.addCookie(cookieUF);
				}
			}
			else
			{
				UsuarioBeanId.setcookieUF(cookieUF);
			}
			
			if (cookieCodOrgaoLotacao == null)
			{
				if (cookieUF != null)
				{
					cookieCodOrgaoLotacao = new Cookie("cookieCodOrgaoLotacao", ParamUFBeanId.getParamUF("TXT_ORGAO_LOTACAO"));
					cookieCodOrgaoLotacao.setMaxAge(SEGUNDOS_POR_ANO);
					//cookieCodOrgaoLotacao.setPath(req.getContextPath());
					UsuarioBeanId.setcookieCodOrgaoLotacao(cookieCodOrgaoLotacao);
					res.addCookie(cookieCodOrgaoLotacao);
				}
			}
			else{
				String codOrgao = UsuarioBeanId.getOrgao().getCodOrgao();
				if(codOrgao==null)      codOrgao="";
				if (!codOrgao.equals(""))
				{
					cookieCodOrgaoLotacao = new Cookie("cookieCodOrgaoLotacao", codOrgao);
					cookieCodOrgaoLotacao.setMaxAge(SEGUNDOS_POR_ANO);
					//cookieCodOrgaoLotacao.setPath(req.getContextPath());
					res.addCookie(cookieCodOrgaoLotacao);
				}
				UsuarioBeanId.setcookieCodOrgaoLotacao(cookieCodOrgaoLotacao);
			}
			
			if (cookieCodOrgaoAtuacao == null)
			{
				if (cookieUF != null)
				{
					cookieCodOrgaoAtuacao = new Cookie("cookieCodOrgaoAtuacao", ParamUFBeanId.getParamUF("TXT_ORGAO"));
					cookieCodOrgaoAtuacao.setMaxAge(SEGUNDOS_POR_ANO);
					//cookieCodOrgaoAtuacao.setPath(req.getContextPath());
					UsuarioBeanId.setcookieCodOrgaoAtuacao(cookieCodOrgaoAtuacao);
					res.addCookie(cookieCodOrgaoAtuacao);
				}
			}
			else{
				String codOrgaoAtu = UsuarioBeanId.getCodOrgaoAtuacao();
				if(codOrgaoAtu==null)      codOrgaoAtu="";
				if (!codOrgaoAtu.equals(""))
				{
					cookieCodOrgaoAtuacao = new Cookie("cookieCodOrgaoAtuacao", codOrgaoAtu);
					cookieCodOrgaoAtuacao.setMaxAge(SEGUNDOS_POR_ANO);
					//cookieCodOrgaoAtuacao.setPath(req.getContextPath());
					res.addCookie(cookieCodOrgaoAtuacao);
				}
				UsuarioBeanId.setcookieCodOrgaoAtuacao(cookieCodOrgaoAtuacao);
			}
			
			if (RequisicaoBeanId.getAcao().equals("TrocaUF"))
			{
				if (cookieUF != null)
				{
					cookieUF.setMaxAge(0);
					cookieUF.setValue("");
					res.addCookie(cookieUF);
					cookieUF = null;
					UsuarioBeanId.setcookieUF(cookieUF);
				}
				
				if (cookieCodOrgaoLotacao != null)
				{
					cookieCodOrgaoLotacao.setMaxAge(0);
					res.addCookie(cookieCodOrgaoLotacao);
					cookieCodOrgaoLotacao = null;
					UsuarioBeanId.setcookieCodOrgaoLotacao(cookieCodOrgaoLotacao);
				}
				
				if (cookieCodOrgaoAtuacao != null)
				{
					cookieCodOrgaoAtuacao.setMaxAge(0);
					res.addCookie(cookieCodOrgaoAtuacao);
					cookieCodOrgaoAtuacao = null;
					UsuarioBeanId.setcookieCodOrgaoAtuacao(cookieCodOrgaoAtuacao);
				}
				
				
			}
			
			/* Verifica se o cookie � nulo, e se o pr�ximo jsp � o de login. Caso ambas as condi��es 
			 sejam vedadeiras, direciona para o jsp login_mapa */
			if (cookieUF == null)
			{
				if (next.indexOf(JSPLOGIN) >= 0)
					nextLogin = JSPLOGINMAPA;
			}
			
		}
		catch (Exception e) {
			throw new sys.CommandException("Valida��o de Cookie: " + e.getMessage());				 
		}
		
		return nextLogin;
	}
	
}
