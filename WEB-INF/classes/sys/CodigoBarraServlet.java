package sys;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CodigoBarraServlet extends HttpServlet {
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String sTipo=null;
        try {
            response.setContentType("image/jpeg");
            response.setHeader ("Pragma", "no-cache");
            response.setHeader ("Cache-Control", "no-cache");
            response.setDateHeader ("Expires",0);        
            
            boolean retorno;
            String barras;
            String tipo;
            String altura;
            String flag;
            
            barras = request.getParameter("codigo");
            if (barras==null) barras="";

            tipo = request.getParameter("tipo");
            altura = request.getParameter("altura");
            flag = request.getParameter("flag");
            
            /*
             Altura padrao=70
             Tipo padrao=1   1-->     Code128
             outro--> Febraban (somente string numerico com comprimento par)  
             */
            int intAltura = 70;
            int intTipo = 1;
            int intFlag = 1;
            
            if (tipo == null)
                intTipo = 1;
            else {
                try {
                    intTipo = Integer.parseInt(tipo);
                }
                catch (Exception e) {
                    intTipo = 1;
                }
            }

            if(intTipo==0)
            {
    		    if (barras.length()>=48)
    		    {
    		      String sCBPedaco1=barras.substring(0,8);
    		      String sCBPedaco2=barras.substring(8,16);
    		      String sCBPedaco3=barras.substring(16,24);
    		      String sCBPedaco4=barras.substring(24,32);
    		      String sCBPedaco5=barras.substring(32,40);
    		      String sCBPedaco6=barras.substring(40,48);		      
    		      int iBarras1,iBarras2,iBarras3,iBarras4,iBarras5,iBarras6;
              	  try {
              		iBarras1 = Integer.parseInt(sCBPedaco1);
              		iBarras2 = Integer.parseInt(sCBPedaco2);
              		iBarras3 = Integer.parseInt(sCBPedaco3);
              		iBarras4 = Integer.parseInt(sCBPedaco4);
              		iBarras5 = Integer.parseInt(sCBPedaco5);
              		iBarras6 = Integer.parseInt(sCBPedaco6);              		
              	  }
              	  catch (Exception e) {
              		System.out.println("\nErro na gera��o do c�digo de barras (SMIT):"+barras+"\n"); 
              	  }
    		    }
    		    else
    		    	System.out.println("\nC�digo de barras inferior a 48 posi��es (SMIT):"+barras+"\n"); 
            }
            
            if (altura == null)
                intAltura = 70;
            else {
                try {
                    intAltura = Integer.parseInt(altura);
                }
                catch (Exception e) {
                    intAltura = 70;
                }
            }
            
            if (flag == null)
                intFlag = 1;
            else {
                try {
                    intFlag = Integer.parseInt(flag);
                }
                catch (Exception e) {
                    intFlag = 1;
                }
            }
            sTipo="drawingBarcodeDirectToGraphics "+barras+" "+intAltura;
            if (intTipo == 1)
                CodigoBarra.drawingBarcodeDirectToGraphics(barras,response,intAltura,intFlag);
            else {
                /*
                 Se o tipo nao for 1, o comprimento precisa ser par. Zero adicionado a esquerda.
                 */
            	sTipo="criaImagem barras: ("+barras+") altura: ("+intAltura+")";
                if (!(barras.length()%2==0))
                    barras = "0" + barras;
                CodigoBarra.criaImagem(barras,response,intAltura);
            }
            
        } catch (Exception e) {
            throw new ServletException("(SMIT) : "+sTipo+" "+e.getMessage());
        }               
    }
}
