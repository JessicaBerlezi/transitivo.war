package sys;

import java.net.InetAddress;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;

import REG.NotifControleBean;

public class Util {
	
	public static String today(){
		
		java.util.Date data = new java.util.Date();
		return data.toString();
		
	} // Fim today
	
	/*Gerar lista de sequencia no tamanho informado*/
	public static ArrayList<Integer> ListaSequencial(int iTamanho)
	{
		ArrayList <Integer>ListSequencial = new ArrayList<Integer>();
		for (int i=0;i<iTamanho;i++)
		{
			ListSequencial.add(i);
		}
		return ListSequencial;
	}

	/*Gerar lista de sequencia sorteado no tamanho informado*/
	public static ArrayList<Integer> ListaSequencialSort(int iTamanho)
	{
		ArrayList <Integer>ListaTMP = ListaSequencial(iTamanho);
		ArrayList <Integer>LSequencialProcSort = new ArrayList<Integer>();		
		for (int i=0;i<iTamanho-1;i++)
		{
			/*Gerar n�mero sorteado �nico*/
			int	iSorteado=Util.Roleta(ListaTMP.size());
			LSequencialProcSort.add(ListaTMP.get(iSorteado));
			/*Retirar o n�mero sorteado da posicao efetiva*/
			ListaTMP.remove(iSorteado);
		}
		LSequencialProcSort.add((Integer)ListaTMP.get(0));
		return LSequencialProcSort;
	}
	
	/*Gerar um n�mero rand�mico de acordo com no intervalo de 0 ao tamanho informado*/
	public static int Roleta(int iTamanho)
	{
		int iRoleta=0;
		Random generator = new Random();
		int rand = generator.nextInt();
		iRoleta  = Math.abs(rand % iTamanho);
		return iRoleta;
	}
	
	
	
	public static String formatedToday(){
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length()<2) {dia = "0" + dia;}
		if (mes.length()<2) {mes = "0" + mes;}
		if (hor.length()<2) {hor = "0" + hor;}
		if (min.length()<2) {min = "0" + min;}
		if (seg.length()<2) {seg = "0" + seg;}
		// dd/mm/yyyy, hh24:mi:ss
		return dia+"/"+mes+"/"+ano+" , "+hor+":"+min+":"+seg;
		
	}  // Fim formatedToday
	

	public static String formatedTodayDDMMAAAA(){
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length()<2) {dia = "0" + dia;}
		if (mes.length()<2) {mes = "0" + mes;}
		if (hor.length()<2) {hor = "0" + hor;}
		if (min.length()<2) {min = "0" + min;}
		if (seg.length()<2) {seg = "0" + seg;}
		// dd/mm/yyyy, hh24:mi:ss
		return dia+"/"+mes+"/"+ano;
		
	}  // Fim formatedToday
	
	
	/**
	 * @author Pedro N�brega
	 */
	public static String formatedLast2Month() {
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -60);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;
		
	}
	
	public static String ValidaData(String spDt){
		String  sWork = "" ;
		if (spDt.length() == 0) {
			return sWork ;
		}
		char ch ;
		int iNumBar = 0 ;
		int iAno,iMes,iDia ;
		boolean bDterro = false ;
		String sDia = "00";
		String sMes = "00";
		String sAno = "" ;
		for (int i=0; i<spDt.length(); i++) {
			ch = spDt.charAt(i);
			if (( ch < '0' || ch > '9') && ch != ' ' && ch != '/') {
				bDterro = true ;
				break ;
			}
			if (ch == '/') { iNumBar += 1 ;  }
			else {if (iNumBar == 0) { sDia = sDia +  ch; }
			else { if (iNumBar == 1) { sMes = sMes + ch; }
			else { sAno = sAno + ch; }
			}
			}
		}
		try {
			iAno = Integer.parseInt(sAno) ;
			if (sAno.length() == 2) { if (iAno < 10)  sAno = "20"+sAno; 
			else            sAno = "19"+sAno; 
			}
			sAno = "0000" + sAno ;
			sDia = sDia.substring((sDia.length()-2),sDia.length()) ;
			sMes = sMes.substring((sMes.length()-2),sMes.length()) ;
			sAno = sAno.substring((sAno.length()-4),sAno.length()) ;
			iAno = Integer.parseInt(sAno) ;
			iMes = Integer.parseInt(sMes) ;
			iDia = Integer.parseInt(sDia) ;
		}
		catch (Exception e) {
			iAno = 0 ;
			iMes = 0 ;
			iDia = 0 ;
		}
		// validar datas
		if ((iAno > 2099) || (iAno < 1900)) bDterro = true; 
		if ((iMes < 1)    || (iMes > 12))   bDterro = true;
		
		if ((iAno%4==0) && (iMes == 2) && ((iDia < 1) || (iDia > 29))){bDterro = true; }
		if ((iAno%4!=0) && (iMes == 2) && ((iDia < 1) || (iDia > 28))){bDterro = true; } 		
		
		if (((iMes == 4) || (iMes == 6) || (iMes == 9) || (iMes == 11)) && ((iDia < 1) || (iDia > 30))){bDterro = true; }
		if (iDia > 31) {bDterro = true; }
		if (bDterro == false) { sWork = sDia+"/"+sMes+"/"+sAno ; }
		return  sWork ;
	}
	public static String FormataNumero(String numero, String pat) {
		String numFormatado="";
		try {
			double l = Double.parseDouble(numero)/100;
			NumberFormat dec = NumberFormat.getInstance( );
			DecimalFormat decform = (DecimalFormat)dec;
			//		decform.applyPattern(pat);
			numFormatado = decform.format(l);
		}
		catch (Exception e) {
			System.err.println("Exception: "+e);
			
		}
		return numFormatado;
	}
	
	public static String ComEdt(String spCelula) {
		String sWork = "" ;
		char ch ;
		int l ;
		if (spCelula.length() == 0) sWork = "0,00" ;
		if (spCelula.length() == 1) sWork = "0,0"+spCelula ;
		if (spCelula.length() == 2) sWork = "0,"+spCelula ;
		if (spCelula.length() >= 3) {
			for (int k=0; k<spCelula.length(); k++) {
				ch = spCelula.charAt(k);
				l  = spCelula.length() - k ;
				if (l == 3) sWork = sWork + ch + "," ;
				else { if ((l == 6) || (l == 9) || (l == 12) || (l == 15)) sWork = sWork + ch + "." ;
				else                                                sWork = sWork + ch ;
				}
			}
		}
		sWork = sWork.trim() ;
		if (sWork.substring(0,2).equals("-.")) sWork = "-"+sWork.substring(2,sWork.length()) ;
		for (int k=sWork.length(); k<18 ; k++) {
			sWork = " " + sWork ;
		}
		return sWork ;
	}
	public static String ComEdt(String spCelula,int tam,int nCasas) {
		String sWork = "" ;
		char ch ;
		if (nCasas==0)  {
			sWork = spCelula;
		}
		else  {
			if (spCelula.length() == nCasas) sWork = "0,"+spCelula ;  
			for (int i=0;i<nCasas;i++)  {
				if (spCelula.length() == i) sWork = "0,"+"0000000000".substring(0,nCasas-i) ;
			}
			if (spCelula.length() > nCasas) {
				int l ;
				for (int k=0; k<spCelula.length(); k++) {
					ch = spCelula.charAt(k);
					l  = spCelula.length() - k ;
					if (l == (nCasas+1)) sWork = sWork + ch + "," ;
					else { if ((l == (nCasas+4)) || (l == (nCasas+7)) || (l == (nCasas+10)) || (l == (nCasas+13))) sWork = sWork + ch + "." ;
					else                                                sWork = sWork + ch ;
					}
				}
			}
		}
		sWork = sWork.trim() ;
		if (sWork.substring(0,nCasas).equals("-.")) sWork = "-"+sWork.substring(nCasas,sWork.length()) ;
		for (int k=sWork.length(); k<tam ; k++) {
			sWork = " " + sWork ;
		}
		return sWork ;
	} // fim ComEdt
	public static String SemZeros(String spCelula) {
		String sWork = "0" 	;
		int k = 0;
		for (k=0; k < spCelula.length(); k++) {
			if ((spCelula.substring(k,k + 1).equals("0")))  {
				continue ;
			}
			else break;
		}
		if (k < spCelula.length()) sWork = spCelula.substring(k);
		return sWork ;
	} // fim SemEdt
	
	public static String SemEdt(String spCelula,char decPoint) {
		String sWork = "" 	;
		char ch ;
		boolean btemVirg = false ;
		for (int k=0; k < spCelula.length(); k++) {
			ch = spCelula.charAt(k);
			if (ch == decPoint)        btemVirg = true ;
			if ((ch < '0' || ch > '9') && (ch!='-'))  continue ;
			sWork = sWork + ch ;
		}
		if (btemVirg == false)   sWork += "00" ;
		if (sWork.length() == 0) sWork  = "0" ;
		return sWork ;
	} // fim SemEdt
	
	
	public static String Sem_Edt(String spCelula,char decPoint) {
		String sWork = "" 	;
		char ch ;
		//boolean btemVirg = false ;
		for (int k=0; k < spCelula.length(); k++) {
			ch = spCelula.charAt(k);
			//if (ch == decPoint)        btemVirg = true ;
			if ((ch < '0' || ch > '9') && (ch!='-'))  continue ;
			sWork = sWork + ch ;
		}
		/*
		 if (btemVirg == false)   sWork += "00" ;
		 if (sWork.length() == 0) sWork  = "0" ;
		 */
		return sWork ;
	} // fim SemEdt
	
	
	public static String fmtNumBD(String numEdt) {
		String sWork = "" 	;
		char ch ;
		for (int k=0; k < numEdt.length(); k++) {
			ch = numEdt.charAt(k);
			if (ch == ',')   {
				sWork +="." ;
				continue ;
			}
			if ((ch < '0' || ch > '9') && (ch!='-'))  continue ;
			sWork += ch ;
		}
		return sWork ;
		
	} // fim fmtNumBD
	
	
	public static int CriticarDigitoIE	(String Inscenv) {
		/**
		 retorno = 0 - digito correto
		 retorno = 1 - digito invalido
		 retorno = 2 - tamanho difere de 8 posicoes
		 retorno = 3 - numero igual 00000000
		 retorno = 9 - erro no processamento
		 */
		try {
			int i, j, k, lenNum, posNum, resto, dv ;
			int acc    = 0;
			String    numero = "";
			for (i=0;i<Inscenv.length();i++) {
				char ch = Inscenv.charAt(i);
				if ((ch < '0') || (ch >'9')) continue ;
				numero += ch ;
			}
			lenNum = numero.length();
			if (lenNum != 8) { return 2; }
			if (numero.equals("00000000")) {return 3; }
			j = lenNum - 2;
			for (i=0;i<(lenNum - 1);i++) {
				posNum=Integer.parseInt(numero.substring(j,j+1));
				if (i > 5) { k=posNum * (i-4); }
				else { k=posNum * (i+2); }
				acc += k;
				j--;
			} //Fim for
			resto = acc % 11;
			if (resto <= 1) { dv = 0; }
			else { dv = 11 - resto; }
			posNum = Integer.parseInt(numero.substring(7));
			if (dv != posNum) { return 1; }
			return 0;
		}	// Fim try
		catch (Exception e) {
			return 9;
		}
		
	} // Fim CriticarDigitoIE
	
	
	public static boolean CriticarDigitoCgc	(String cgc) {
		/**
		 * cgc  -> campo caracter de 15 posicoes com o numero do CGC
		 *  retorno = true  - digito correto
		 *  retorno = false - digito invalido
		 */
		boolean bOk = false ;
		try {
			String    numero = "";
			for (int i=0;i<cgc.length();i++) {
				char ch = cgc.charAt(i);
				if ((ch < '0') || (ch >'9')) continue ;
				numero += ch ;
			}
			if (numero.length() > 15)  numero=numero.substring(0,15) ;
			if (numero.length() < 15)  {
				numero=("000000000000000"+numero) ;
				numero=numero.substring(numero.length()-15,numero.length()) ;
			}
			String dv = CalcularDigitoCgc(numero.substring(0,13)) ;
			bOk = numero.substring(13,15).equals(dv) ;
		}
		catch (Exception e) {   }
		return bOk ;
	} // Fim CriticarDigitoCGC
	
	public static String CalcularDigitoCgc	(String cgc) {
		/**
		 * cgc  -> campo caracter de 13 posicoes com o numero do CGC
		 *  retorno = String de duas posi��es com os digitos calculados do cgc
		 *          = "NN" quando ocorre algum erro
		 */
		try {
			int soma,dv1,dv2,n ;
			String    numero = "";
			for ( int i=0;i<cgc.length();i++) {
				char ch = cgc.charAt(i);
				if ((ch < '0') || (ch >'9')) continue ;
				numero += ch ;
			}
			if (numero.length() > 13)  numero=numero.substring(0,13) ;
			if (numero.length() < 13)   {
				numero=("0000000000000"+numero) ;
				numero=numero.substring(numero.length()-13,numero.length()) ;
			}
			/* Calculo do primeiro digito */
			soma = 0 ;
			for (int i=0; i<13; i++) {
				n=0;
				try { n=Integer.parseInt(numero.substring(i,i+1)); }
				catch (NumberFormatException e) {  }
				if (i<=4) soma += (13-7-i)*n ;
				else      soma += (13-7+8-i)*n ;
			}
			dv1 = 11 - (soma%11) ;
			if (dv1 > 9) dv1 = 0 ;
			numero += String.valueOf(dv1) ;
			/* Calculo do segundo digito */
			soma = 0 ;
			for (int i=0; i<14; i++) {
				n=0;
				try { n=Integer.parseInt(numero.substring(i,i+1)); }
				catch (NumberFormatException e) {  }
				if (i<=5) soma += (14-7-i)*n ;
				else      soma += (14-7+8-i)*n;
			}
			dv2 = 11 - (soma%11) ;
			if (dv2 > 9) dv2 = 0 ;
			return  String.valueOf(dv1)+String.valueOf(dv2) ;
		}	// Fim try
		catch (Exception e) {
			return "NN";
		}
	} // Fim CalcularDigitoCGC
	
	
	public static boolean CriticarDigitoCpf	(String cpf) {
		/**
		 * cpf  -> campo caracter de 11 posicoes com o numero do CPF
		 *  retorno = true  - digito correto
		 *  retorno = false - digito invalido
		 */
		boolean bOk = false ;
		try {
			String    numero = "";
			for (int i=0;i<cpf.length();i++) {
				char ch = cpf.charAt(i);
				if ((ch < '0') || (ch >'9')) continue ;
				numero += ch ;
			}
			if (numero.length() > 11)  numero=numero.substring(0,11) ;
			if (numero.length() < 11)  {
				numero=("00000000000"+numero) ;
				numero=numero.substring(numero.length()-11,numero.length()) ;
			}
			String dv = CalcularDigitoCpf(numero.substring(0,9)) ;
			bOk = numero.substring(9,11).equals(dv) ;
		}
		catch (Exception e) {   }
		return bOk ;
	} // Fim CriticarDigitoCPF
	
	public static String CalcularDigitoCpf	(String cpf) {
		/**
		 * cpf  -> campo caracter de 9 posicoes com o numero do CPF
		 *  retorno = String de duas posi��es com os digitos calculados do cgc
		 *          = "NN" quando ocorre algum erro
		 */
		try {
			int soma,dv1,dv2,n ;
			String    numero = "";
			for (int i=0;i<cpf.length();i++) {
				char ch = cpf.charAt(i);
				if ((ch < '0') || (ch >'9')) continue ;
				numero += ch ;
			}
			if (numero.length() > 9)  numero=numero.substring(0,9) ;
			if (numero.length() < 9)  {
				numero=("000000000"+numero) ;
				numero=numero.substring(numero.length()-9,numero.length()) ;
			}
			/* Calculo do primeiro digito */
			soma = 0 ;
			for (int i=0; i<9; i++) {
				n=0;
				try { n=Integer.parseInt(numero.substring(i,i+1)); }
				catch (NumberFormatException e) {  }
				soma += (10-i)*n ;
			}
			dv1 = 11 - (soma%11) ;
			if (dv1 > 9) dv1 = 0 ;
			numero += String.valueOf(dv1) ;
			/* Calculo do segundo digito */
			soma = 0 ;
			for (int i=0; i<10; i++) {
				n=0;
				try { n=Integer.parseInt(numero.substring(i,i+1)); }
				catch (NumberFormatException e) {  }
				soma += (11-i)*n ;
			}
			dv2 = 11 - (soma%11) ;
			if (dv2 > 9) dv2 = 0 ;
			return  String.valueOf(dv1)+String.valueOf(dv2) ;
		}	// Fim try
		catch (Exception e) {
			return "NN";
		}
	} // Fim CalcularDigitoCPF
	
	
	public static boolean CriticarCep	(String cep) {
		return true ;
	} // Fim CriticarDigitoCEP
	
	
	public static String InscricaoSemEdt (String pInscricao) {
		String sWork = "" 	;
		char ch ;
		for (int i=0; i < pInscricao.length(); i++) {
			ch = pInscricao.charAt(i);
			if (ch < '0' || ch > '9') continue ;
			sWork = sWork + ch ;
		}
		return sWork ;
	}
	
	/** Obtem a data atual.
	 *   @return m_data (String).
	 */
	public static String getData(){
		String m_data=null;
		java.util.Date nd = null;
		try {
			nd=new java.util.Date();
			Calendar c = new GregorianCalendar (TimeZone.getTimeZone ("EST"), Locale.US);
			c.setTime (nd);
			int diatrb = c.get(Calendar.DAY_OF_MONTH);
			String sdia=String.valueOf(diatrb+100);
			sdia=sdia.substring(1,3);
			int mestrb = c.get(Calendar.MONTH);
			String smes=String.valueOf(mestrb+101);
			smes=smes.substring(1,3);
			int anotrb = c.get(Calendar.YEAR);
			String sano=String.valueOf(anotrb);
			m_data=sano+"-"+smes+"-"+sdia;
		} catch (IllegalArgumentException e){
			m_data="1900-01-01";
		}
		return m_data;
	} //fim do m�todo
	
	/** Obtem a hora atual.
	 *   @return m_hora (String).
	 */
	public static String getHora(){
		java.util.Date nd = null;
		String m_hora=null;
		try {
			nd=new java.util.Date();
			Calendar c = new GregorianCalendar (TimeZone.getTimeZone ("America/Sao_Paulo"), new Locale("pt", "BR"));
			//c.setTime (nd);
			int hortrb = c.get(Calendar.HOUR_OF_DAY);
			int mintrb = c.get(Calendar.MINUTE);
			int sectrb = c.get(Calendar.SECOND);
			m_hora=hortrb+":"+mintrb+":"+sectrb;
			
		} catch (IllegalArgumentException e){
			m_hora="12:00:00";
		}
		return m_hora;
	} 
	
	
	/** M�todo para formatar Datas (java.sql.Date)
	 * @param dt_data (java.sql.Date)
	 * @return s_return (String)
	 */
	public static String fmtData(java.util.Date dt_data){
		
		/** String de retorno */
		String s_return ="";
		
		/** Preparo a formata��o da Data */
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		/** Jogo a String resultado da formatacao para a de retorno */
		s_return = sdf.format(dt_data);
		
		/** retorno o valor formatado */
		return s_return;
		
	}
	
	/** M�todo para formatar Datas (String)
	 * @param dt_data (String - formato americano)
	 * @return s_return (String - formato britanico)
	 */
	public static String fmtData(String dt_data){
		String s_return = dt_data.substring(8,10)+"-"+dt_data.substring(5,7)+"-"+dt_data.substring(0,4);
		return s_return;
	}
	
	/** M�todo para retornar se a data est� vazia
	 * @param sData (String) data a ser validada
	 * @result flag_result (boolean) resultado. Verdadeiro/Falso
	 */
	public static boolean isEmptyData(String sData) {
		
		boolean flag_result = (sData.compareTo("1899-12-30")==0) ;
		
		sData = sData.trim();
		
		if (sData.compareTo("")==0) flag_result = true ;
		
		return flag_result ;
		
	}
	
	/** m�todo para retornar a quantidade de dias entre duas datas
	 * @param dt1 (Date)
	 * @param dt2 (Date)
	 * @return r_result (int)
	 */
	public static int DifereDias(String dt1, String dt2) {	
		int i_result = -1;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dat1=df.parse(dt1);
			java.util.Date dat2=df.parse(dt2);
			Long mili = new Long(24*60*60000); 
			
			Long dif  = new Long(dat2.getTime()-dat1.getTime());
			Long l    = new Long((dif.longValue())/mili.longValue());
			i_result = l.intValue();
		}
		catch (ParseException e)	{ i_result = -1;	}
		catch (Exception e)			{ i_result = -1;	}
		return i_result;
	}
	/** M�todo para retornar a data somada a um n�mero de dias
	 * @param iVal (int) n�mero de dias
	 * @return dtData (Date)
	 */
	public static String addData(String dtDataIni, int iVal)  throws sys.CommandException {
		String dtData = null ;
		
		if (dtDataIni==null) dtDataIni=sys.Util.formatedToday().substring(0,10) ;		
		int dia = Integer.parseInt(dtDataIni.substring(0,2));
		int mes = Integer.parseInt(dtDataIni.substring(3,5))-1;
		int ano = Integer.parseInt(dtDataIni.substring(6,10));
		/* pego a data ini e jogo num calendar para poder somar os dias*/
		
		Calendar clnd = Calendar.getInstance();
		clnd.set(ano, mes, dia) ;
		clnd.add(Calendar.DAY_OF_MONTH,iVal);
		
		String sDia=Integer.toString(clnd.get(Calendar.DAY_OF_MONTH));
		if (sDia.length()==1) sDia="0"+sDia;		
		
		String sMes=Integer.toString(clnd.get(Calendar.MONTH)+1);
		if (sMes.length()==1) sMes="0"+sMes;
		
		dtData = sDia+"/"+sMes+"/"+Integer.toString(clnd.get(Calendar.YEAR));
		return dtData;
	}
	
	/** M�todo para retroagir datas.
	 *   @param dt (Date)
	 * @author Wellem
	 */
	
	public static String DiminuiDias(int dias) {
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, - dias);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;
		
	}
	
	/** M�todo para somar dias a uma data
	 *   @param String dias
	 * @author Rodrigo
	 */
	
	public static String somaDias(int dias) {
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// YYYYMMDD
		return ano+mes+dia;
		
	}
	
	
	/** M�todo para somar dias a uma data - SOBRECARGA
	 *   @param String dias, String Data
	 * @author Rodrigo
	 */
	
	public static String somaDias(int dias, String data) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = new GregorianCalendar();

		try {
			calendar.setTime(format.parse(data));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// YYYYMMDD
		return ano+mes+dia;
		
	}
	
	/** M�todo para tirar o ano de uma data.
	 *   @param dt (Date)
	 */
	public static int ano(Date dt) {
		
		int iAno = 0;
		
		if ((dt!=null) && (!dt.toString().equals("1899-12-30")))
			iAno = Integer.parseInt(dt.toString().substring(0,4));
		
		return iAno ;
	}
	
	
	/** M�todo para completar a string com o caracter escolhido
	 * @param s (String) - string a ser preenchida
	 * @param c (String) - caracter a completar a string
	 */
	public static String pad(String s, int i, String c) {
		for (int y=s.length()+1;y<=i;y++){
			s=c+s;
		}
		return s ;
	}
	
	/** M�todo para retornar a data atual
	 * @return sDtHoje (String)
	 */
	public static String getDataHoje(){
		String sDtHoje="01-01-1900";
		try {
			java.util.Date nd = new java.util.Date();
			Calendar c = new GregorianCalendar (TimeZone.getTimeZone ("EST"), Locale.US);
			c.setTime (nd);
			int diatrb = c.get(Calendar.DAY_OF_MONTH);
			String sDia=String.valueOf(diatrb+100);
			sDia=sDia.substring(1,3);
			int mestrb = c.get(Calendar.MONTH);
			String sMes=String.valueOf(mestrb+101);
			sMes=sMes.substring(1,3);
			int anotrb = c.get(Calendar.YEAR);
			String sAno=String.valueOf(anotrb);
			sDtHoje=sDia + "-" + sMes + "-" + sAno;
		} catch (IllegalArgumentException e){
			sDtHoje="01-01-1900";
		}
		return sDtHoje;
	}
	
	/** M�todo para retornar a data atual no formato americano
	 * @return sDtHoje (String)
	 */
	public static String getDataUsa(){
		String sDtHoje="19000101";
		try {
			java.util.Date nd = new java.util.Date();
			Calendar c = new GregorianCalendar (TimeZone.getTimeZone ("EST"), Locale.US);
			c.setTime (nd);
			int diatrb = c.get(Calendar.DAY_OF_MONTH);
			String sDia=String.valueOf(diatrb+100);
			sDia=sDia.substring(1,3);
			int mestrb = c.get(Calendar.MONTH);
			String sMes=String.valueOf(mestrb+101);
			sMes=sMes.substring(1,3);
			int anotrb = c.get(Calendar.YEAR);
			String sAno=String.valueOf(anotrb);
			sDtHoje=sAno + sMes + sDia;
		} catch (IllegalArgumentException e){
			sDtHoje="19000101";
		}
		return sDtHoje;
	}
	public static String edtCmp(String cmp,int tam,String nomCmp){
		int i = 99 ;
		if (nomCmp.compareTo("CGC")==0)                 i = 0 ;
		if (nomCmp.compareTo("CPF")==0)                 i = 1 ;
		if (nomCmp.compareTo("PROCESSO")==0)            i = 2 ;
		if (nomCmp.compareTo("CONTA CONTABIL")==0)      i = 3 ;
		if (nomCmp.compareTo("INSCRICAO ESTADUAL")==0)  i = 4 ;
		if (nomCmp.compareTo("INSCRICAO MUNICIPAL")==0) i = 5 ;
		if (nomCmp.compareTo("CEP")==0)                 i = 6 ;
		if (nomCmp.compareTo("INSCRICAO IPTU")==0)      i = 7 ;
		if (nomCmp.compareTo("ORGAO")==0)               i = 8 ;
		if (nomCmp.compareTo("VALOR")==0)               i = 9 ;
		return edtCmp(cmp,tam,i) ;
	}
	public static String edtCmp(String cmp,int tam,int numCmp){
		String cmpEdt = "" ;
		switch (numCmp) {
		case 0:
			// CGC
			cmpEdt=Tiraedt(cmp,15)  ;     
			cmpEdt=cmpEdt.substring(0,3)+"."+cmpEdt.substring(3,6)+"."+cmpEdt.substring(6,9)+"/"+cmpEdt.substring(9,13)+"-"+cmpEdt.substring(13,15)      ;
			break ;
		case 1:
			// CPF
			cmpEdt=Tiraedt(cmp,11)  ;     
			cmpEdt=cmpEdt.substring(0,3)+"."+cmpEdt.substring(3,6)+"."+cmpEdt.substring(6,9)+"-"+cmpEdt.substring(9,11) 
			;
			break ;
		case 2:
			// Processo
			cmpEdt=Tiraedt(cmp,11)  ;     
			cmpEdt=cmpEdt.substring(0,3)+"/"+cmpEdt.substring(3,9)+"/"+cmpEdt.substring(9,11);
			break ;
		case 3:
			// Conta Cont�bil
			cmpEdt=Tiraedt(cmp,8)  ;     
			cmpEdt=cmpEdt.substring(0,1)+"."+cmpEdt.substring(1,2)+"."+cmpEdt.substring(2,3)+"."+cmpEdt.substring(3,4)+"."+cmpEdt.substring(4,6)+"."+cmpEdt.substring(6,8);
			break ;
		case 4:
			// Inscri��o Estadual
			cmpEdt=Tiraedt(cmp,9)  ;     
			cmpEdt=cmpEdt.substring(0,3)+"."+cmpEdt.substring(3,6)+"."+cmpEdt.substring(6,9);
			break ;
		case 5:
			// Inscri��o Municipal
			cmpEdt=Tiraedt(cmp,9)  ;   
			cmpEdt=cmpEdt.substring(0,3)+"."+cmpEdt.substring(3,6)+"."+cmpEdt.substring(6,9);
			break ;
		case 6:
			// CEP
			cmpEdt=Tiraedt(cmp,8)  ;
			cmpEdt=cmpEdt.substring(0,5)+"-"+cmpEdt.substring(5,8);
			break ;
		case 7:
			// Inscri��o de IPTU
			cmpEdt=Tiraedt(cmp,6)  ;
			cmpEdt=cmpEdt.substring(0,5)+"-"+cmpEdt.substring(5,6);
			break ;
		case 8:
			// Org�o e Unidade
			cmpEdt=Tiraedt(cmp,4)  ;
			cmpEdt=cmpEdt.substring(0,2)+"."+cmpEdt.substring(2,4);
			break ;
		case 9:
			// Valores
			cmpEdt=ComEdt(SemEdt(cmp,','))  ;
			break ;
		default :
			// Outros
			cmpEdt=Tiraedt(cmp,tam)  ;
		break ;
		}
		return cmpEdt ;
	} // fim edtCmp
	
	public static String Tiraedt(String cmp,int tam){
		String sWork = "" 	;
		char ch ;
		for (int k=0; k < cmp.length(); k++) {
			ch = cmp.charAt(k);
			if ((ch >= '0') && (ch <= '9')) sWork = sWork + ch ;
		}
		if (tam!=0)   {
			if (sWork.length() < tam) sWork = "0" ; {
				for (int k=sWork.length(); k<tam; k++) 	{
					sWork = "0"+sWork ;
				}
			}
			if (sWork.length() > tam) sWork = sWork.substring(0,tam) ;
		}
		return sWork ;
	} // fim TiraEdt
	public static String nomCmp(int numCmp){
		String nomcmp = "" ;
		switch (numCmp) {
		case 0:
			nomcmp = "CGC" ;
			break ;
		case 1:
			nomcmp = "CPF" ;
			break ;
		case 2:
			nomcmp = "Processo" ;
			break ;
		case 3:
			nomcmp = "Conta Cont�bil" ;
			break ;
		case 4:
			nomcmp = "Inscri��o Estadual" ;
			break ;
		case 5:
			nomcmp = "Inscri��o Municipal" ;
			break ;
		case 6:
			nomcmp = "CEP" ;
			break ;
		case 7:
			nomcmp = "Inscri��o de IPTU" ;
			break ;
		case 8:
			nomcmp = "Org�o" ;
			break ;
		case 9:
			nomcmp = "Valor" ;
			break ;
		default :
			nomcmp = "Outros" ;
		break ;
		}
		return nomcmp ;
	} // fim nomCmp
	
	public static String getParam(HttpServletRequest req, String param, String 
			strret) {
		if (req.getParameter(param) == null)  return strret;
		else                                  return req.getParameter(param) ;
	} // Fim getParam
	
	public static String isChecked(String param, String val) {
		if (param.equals(val))  return "checked" ;
		else                   return "" ;
	} // Fim getParam
	public static String isSelected(String param, String val) {
		if (param.equals(val))  return "selected" ;
		else                   return "" ;
	} // Fim getParam
	public static String isSelectedMult(String param, String val) {
		if (param.indexOf(val)>=0)  return "selected" ;
		else                   return "" ;
	} // Fim getParam
	
	public static synchronized String encriptar(String string) throws NoSuchAlgorithmException{
		StringBuffer hexString = new StringBuffer();
		try	{
			byte buffer[] = string.getBytes();
			MessageDigest algorithm = MessageDigest.getInstance ("MD5");
			algorithm.reset();
			algorithm.update(buffer);
			byte digest[] = algorithm.digest();
			for (int i=0;i<digest.length;i++) {
				hexString.append (Integer.toHexString(0xFF & digest[i]));
				hexString.append (" ");
			}
		}
		catch(NoSuchAlgorithmException e) {
			//			throw new sys.NoSuchAlgorithmException("encriptar: " + e.getMessage());	
		}
		return hexString.toString();
	}
	
	public static String montarWHERE(String[] campos, String[] valores) {	
		String where = " WHERE ";
		for (int i = 0; i < campos.length; i++) {
			where += campos[i] + " = " + valores[i];
			if (i != (campos.length - 1))
				where += " AND ";
		}
		return where;
	}
	
	public static String todayYYYYMMDD(){
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length()<2) {dia = "0" + dia;}
		if (mes.length()<2) {mes = "0" + mes;}
		if (hor.length()<2) {hor = "0" + hor;}
		if (min.length()<2) {min = "0" + min;}
		if (seg.length()<2) {seg = "0" + seg;}
		// dd/mm/yyyy, hh24:mi:ss
		return ano + mes + dia;
		
	}  // Fim formatedToday
	
	public static String formataDataDDMMYYYY(String dataYYYYMMDD){
		String novaData="";
		
		if ("00000000".equals(dataYYYYMMDD)) dataYYYYMMDD="";	
		if ("".equals(dataYYYYMMDD.trim()))  novaData="";
		else { 
			if (dataYYYYMMDD.length() < 8) dataYYYYMMDD = rPad(dataYYYYMMDD," ",8);
			novaData = dataYYYYMMDD.substring(6,8) + "/" + dataYYYYMMDD.substring(4,6) + "/" +
			dataYYYYMMDD.substring(0,4);
		}		
		return novaData;
	}
	public static String formataDataYYYYMMDD(String dt){
		if ("".equals(dt.trim())) dt="0000000000";	
		if (dt.length() < 10) dt = rPad(dt," ",10);
		return dt.substring(6,10) + dt.substring(3,5) + dt.substring(0,2);
	}
	
	public static boolean isNumber(String auxNum)	
	{
		boolean OK = true ;
		String auxPar = auxNum;
		if (auxPar.equals(null)) auxPar = "0";
		if(auxPar.trim().equals("")) auxPar = "0";
		try {
			new Double(auxPar.trim());
		}
		catch(Exception e) 	{
			OK = false;
		}
		return OK;  
		
	}
	
	public static boolean isDate(String parData, int parMascara)	
	{
		boolean OK;
		String strData;
		if ("12".indexOf(Integer.toString(parMascara)) < 0) return false;
		
		if (parData == null) strData = "00000000";
		else
			strData = parData;
		
		if (parMascara == 0) parMascara = 1;
		//	Calendar auxData = null;
		
		OK = true;
		
		if (parData.length() < 8) 
		{
			strData = lPad(strData,"0",8);
		}
		try 
		{
			if (parMascara == 1)  // YYYYMMDD
			{
				new GregorianCalendar(Integer.parseInt(strData.substring(0,4)),Integer.parseInt(strData.substring(4,6)),Integer.parseInt(strData.substring(6,8)));			
			}
			if (parMascara == 2)  // DDMMYYYY
			{
				new GregorianCalendar(Integer.parseInt(strData.substring(4,8)),Integer.parseInt(strData.substring(2,4)),Integer.parseInt(strData.substring(0,2)));			
			}
			
		}
		catch(Exception e) 
		{
			OK = false;
		}
		return OK;
	}
	
	public static boolean isHora(String parHora)	
	{
		boolean OK;
		String strHora;
		
		if (parHora == null) strHora = "0000";
		else
			strHora = parHora;
		
		//	Calendar auxData = Calendar.getInstance();
		
		OK = true;
		
		if (parHora.length() < 4) 
		{
			strHora = lPad(strHora,"0",4);
		}
		
		try 
		{
			new GregorianCalendar(1900,01,01,Integer.parseInt(strHora.substring(0,2)),Integer.parseInt(strHora.substring(2,4)));
		}
		catch(Exception e) 
		{
			OK = false;
		}
		return OK;
	}
	public static String lTrim(String spCelula) {
		String sWork = "" 	;
		int k = 0;
		for (k=0; k < spCelula.length(); k++) {
			if ((spCelula.substring(k,k + 1).equals(" ")))  {
				continue ;
			}
			else break;
		}
		if (k < spCelula.length()) 
			sWork = spCelula.substring(k);
		return sWork ;
	} // fim SemEdt
	
	public static String lPad(String parString,String parFill, int parLength)
	{
		String  retString;
		int 	auxLength;
		
		if (parFill.length()==0) parFill="0";
		if (parString.length() ==0) retString = "";
		else 
			retString = parString;
		if (parLength < retString.length()) auxLength = retString.length();
		else 
			auxLength = parLength;
		while(retString.length() < auxLength )
		{
			retString = parFill + retString;
		}
		if (retString.trim().length()>parLength)
			retString=retString.substring(0,parLength);
		
		return retString;
	}
	
	public static String rPad(String parString,String parFill, int parLength)
	{
		String  retString;
		int 	auxLength;
		if (parFill.length()==0) parFill=" ";        
		if (parString.length() ==0) retString = "";
		else 
			retString = parString;
		
		if (parLength < retString.length()) auxLength = retString.length();
		else 
			auxLength = parLength;
		
		while(retString.length() < auxLength )
		{
			retString = retString + parFill;
		}
		if (retString.trim().length()>parLength)
			retString=retString.substring(0,parLength);
		
		
		
		
		return retString;	
	}
	public static String CodVerif(String CodBarra)  {
		String cod = ""; 		 
		int DigCodBar = 0;
		int i =0;
		for (i=0; i < 9; i++) {
			if (i==8){
				DigCodBar = DigCodBar + (Integer.parseInt(CodBarra.substring(i,i+1)) * (9));
			}					
			else 
			{					
				DigCodBar = DigCodBar + (Integer.parseInt(CodBarra.substring(i,i+1)) * (9-i));					       
			}
		}
		DigCodBar = DigCodBar % 10; 
		if (DigCodBar > 0){
			DigCodBar = 10 - DigCodBar;
		}					
		cod= DigCodBar + cod;					
		return cod;
	}     
	/*
	 * Metodo que monta uma String separada por virgula de uma lista de campos 
	 */
	public static String MontaStringCampos(List valor)    {                   
		Iterator itx =  valor.iterator();
		String stringao="";
		while (itx.hasNext()){
			stringao += (String) itx.next();
			stringao += ",";
		}
		if (stringao.length() > 0)
			stringao= stringao.substring(0,stringao.length()-1);
		
		return stringao;
	}
	
	/** Verifica se � Sabado ou Domingo.
	 @return m_data (String).
	 */
	public static int isFimdeSemana(String mydata)
	{
		int iDias=0;			
		String sDsemana = "";
		int dia = Integer.parseInt(mydata.substring(0,2));
		int mes = Integer.parseInt(mydata.substring(3,5))-1;
		int ano = Integer.parseInt(mydata.substring(6,10));
		Calendar clnd = Calendar.getInstance();
		clnd.set(ano,mes,dia);
		sDsemana = Integer.toString(clnd.get(Calendar.DAY_OF_WEEK));
		if ("1".indexOf(sDsemana)>=0)
			iDias=1;
		if ("7".indexOf(sDsemana)>=0)
			iDias=2;	
		
		return iDias;
	} //fim do m�todo
	
	/**
	 *  Retorna o nome da m�quina local 
	 */    
	public static String hostName() { 
		try { 
			return InetAddress.getLocalHost().getHostName(); 
		} 
		catch(Exception e) { 
			return new String("");    
		} 
	}
	
	public static String convertPonto(String numEdt) {
		//M�todo substitui "." por "," 	
		String sWork = "" 	;
		char ch ;
		for (int k=0; k < numEdt.length(); k++) {
			ch = numEdt.charAt(k);
			if (ch == '.')   {
				sWork +="," ;
				continue ;
			}
			if ((ch < '0' || ch > '9') && (ch!='-'))  continue ;
			sWork += ch ;
		}
		return sWork ;
		
	} // fim fmtNumBD

	public static String converteData(java.sql.Date data) {
		String Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = df.format(data);
		} catch (Exception e) {
			Retorno = "";
		}
		return Retorno;
	}

	
	public static String formataIntero (String valor) throws  DaoException, BeanException{
		String valorFormatado = "";
		if (valor == null || valor.equals("0")||valor.equals("")) valorFormatado = "0";
		else
		{
			DecimalFormat formato = new DecimalFormat(",#00") ;
			valorFormatado = formato.format(Integer.parseInt(valor)); 
		}
		return valorFormatado;
	}
	
	public static String mesPorExtenso(String mes) {	
		String mesExtenso="";
		HashMap <String,String> meses = new HashMap <String,String> ();
		meses.put("01", "JANEIRO");
		meses.put("02", "FEVEREIRO");
		meses.put("03", "MAR�O");
		meses.put("04", "ABRIL");
		meses.put("05", "MAIO");
		meses.put("06", "JUNHO");
		meses.put("07", "JULHO");
		meses.put("08", "AGOSTO");
		meses.put("09", "SETEMBRO");
		meses.put("10", "OUTUBRO");
		meses.put("11", "NOVEMBRO");
		meses.put("12", "DEZEMBRO");			
		mesExtenso = meses.get(mes);	
		return mesExtenso;
	}
	
	public static String VerificaMaiorData(String dt1, String dt2) {	
		
		String retorno = "";
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			java.util.Date dat1=df.parse(dt1);
			java.util.Date dat2=df.parse(dt2);
			
            if(dat1.getTime()>dat2.getTime()) retorno = dt1;
            else retorno = dt2;
			
		}
		catch (ParseException e)	{ }
		catch (Exception e)			{ }
		return retorno;
	}	
	
	public static String dataPorExtenso (){
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length()<2) {dia = "0" + dia;}
		if (mes.length()<2) {mes = "0" + mes;}
		if (hor.length()<2) {hor = "0" + hor;}
		if (min.length()<2) {min = "0" + min;}
		if (seg.length()<2) {seg = "0" + seg;}
		
		return "Rio de Janeiro, "+dia + " de "+ mesPorExtenso(mes) + " de "+ano;
		
	}
	
	
} // Fim da Classe.
