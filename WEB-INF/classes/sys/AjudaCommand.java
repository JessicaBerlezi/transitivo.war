package sys;

import javax.servlet.http.HttpServletRequest;

public class AjudaCommand extends Command {

	private String next;
	private static final String jspPadrao = "/ACSS/login.jsp";  	

	public AjudaCommand() {
		next = jspPadrao;
	}

	public AjudaCommand(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		  String next = jspPadrao; 
			
			try {                        					
				next = DevolveNextHelpJsp(req.getParameter("j_sigFuncao"),req.getParameter("j_jspOrigem"));		
			 			 
			}
			catch (Exception e) {
				throw new sys.CommandException(e.getMessage());
			}
			return next;
		}	
		
	public String execute(HttpServletRequest req, UploadFile upload) throws sys.CommandException {
		  			
			String nextRetorno = jspPadrao;
			String jspOrigem = "";		
				try{
					 jspOrigem = DevolveNextHelpJsp(upload.getParameter("j_sigFuncao"),upload.getParameter("j_jspOrigem"));
				}
			  catch (Exception e) {
							throw new sys.CommandException(e.getMessage());
			  }								
				return jspOrigem;				
	}
		
	  public String DevolveNextHelpJsp(String Funcao, String jspOrigem)
		throws BeanException {		
      if (!"".equals(Funcao)){
   
      if     ("REC0211,REC0235,REC0237,REC0236,REC0238,REC0342,REC0346,REC0347,REC0345,REC0341".indexOf(Funcao) >= 0){		
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "1_H.jsp";
      }		
		else if("REC0221,REC0239,REC0338,REC0336,REC0432,REC0441,REC0442,REC0445,REC0446,REC0447,REC0460".indexOf(Funcao) >= 0){ 	   			
			next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "2_H.jsp";
		}
		else if("REC0240,REC0244,REC0438,REC0436".indexOf(Funcao) >= 0){ 	   			
			next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "3_H.jsp";
		}
		else if("REC0281,REC0331".indexOf(Funcao) >= 0){ 	   			
			next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "4_H.jsp";
		}
		else if("REC0282,REC0344".indexOf(Funcao) >= 0){ 	   			
			next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "5_H.jsp";
		}		
	  else if("REC0337,REC0435".indexOf(Funcao) >= 0){ 	   			
	    next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "6_H.jsp";
		}
		else if("REC0339,REC0444".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "7_H.jsp";
		}				
		else if("REC0340,REC0810".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "8_H.jsp";
		}
		else if("REC0381,REC0811".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "9_H.jsp";
		}
		else if("REC0382,REC0820".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "10_H.jsp";
		}
		else if("REC0383,REC0830".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "11_H.jsp";
		}
		else if("REC0437,REC0840".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "12_H.jsp";
		}
		else if("REC0439".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "13_H.jsp";
		}
		else if("REC0440".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "14_H.jsp";
		}
		else if("REC0481".indexOf(Funcao) >= 0){
			next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "15_H.jsp";
		}
		else if("REC0482".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "16_H.jsp";
		}
		else if("REC0483".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "17_H.jsp";
		}
		else if("REC0852".indexOf(Funcao) >= 0){
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "18_H.jsp";
		}	
		else if("REG0857".indexOf(Funcao) >= 0){
			next = "/REG/ParamOrgao_H.jsp";
		}
		else if("REG0858".indexOf(Funcao) >= 0){
			next = "/REG/ParamOrgao_H.jsp";
		}
		else if("ACSS100".indexOf(Funcao) >= 0){
			next = "/ACSS/Orgao_H.jsp";
		}
		else if("ACSS200".indexOf(Funcao) >= 0){
			next = "/ACSS/Usuario_H.jsp";
		}
		else if("REG0110".indexOf(Funcao) >= 0){
			next = "/REG/EnviarLote_H.jsp";
		}
		else if("REG0120".indexOf(Funcao) >= 0){
			next = "/REG/EnviarLoteBkp_H.jsp";
		}
		else if("REG2014".indexOf(Funcao) >= 0){
			next = "/REG/uploadBaseSimples_H.jsp";
		}
		else if("REG2018".indexOf(Funcao) >= 0){
			next = "/REG/uploadBaseSimples_H.jsp";
		}
		else if("REG2019".indexOf(Funcao) >= 0){
			next = "/REG/uploadBaseSimples_H.jsp";
		}
		else if("REG2053".indexOf(Funcao) >= 0){
			next = "/REG/uploadBaseSimples_H.jsp";
		}
		else if("REG9999".indexOf(Funcao) >= 0){
			next = "/REG/trocaSenha_H.jsp";
		}
		else{
		if (jspOrigem.indexOf(".jsp") >= 0)
		  next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "_H.jsp";
				else
			next = jspOrigem;				
		}
		
		if (next.indexOf("/") > 0)
					next = "/" + next;
	
   }
   else{
		   if (jspOrigem.indexOf(".jsp") >= 0)
			 next = jspOrigem.substring(0, jspOrigem.indexOf(".jsp")) + "_H.jsp";
				   else
			   next = jspOrigem;				
		   }
		   if (next.indexOf("/") > 0)
					   next = "/" + next;
		return next;
	  }

}
