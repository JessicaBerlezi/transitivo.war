package sys;

import java.text.SimpleDateFormat;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.logging.LogFactory;
import org.apache.log4j.LogManager;

/**
 * Classe usada para gerenciar os eventos que possam ocorrer na cria��o 
 * e destrui��o do contexto do SMIT
 * 
 * @author Glaucio Jannotti
 */

public class ContextListener implements ServletContextListener {

    public void contextInitialized(ServletContextEvent sce) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
//        System.out.println(df.format(new Date()) + " Inicializando SMIT...");
    }
    
    public void contextDestroyed(ServletContextEvent sce) {
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");        
//System.out.println(df.format(new Date()) + " Parando SMIT...");
        
        LogManager.shutdown();
        java.beans.Introspector.flushCaches();
        LogFactory.releaseAll();
    }

    
}
