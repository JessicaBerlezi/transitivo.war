package sys ;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
public abstract class Command {

  public String execute(HttpServletRequest req) throws CommandException {return null;}
  public String execute(ServletContext contexto,HttpServletRequest req,HttpServletResponse res) throws CommandException {return null;}  
  public String execute(HttpServletRequest req, UploadFile upload) throws CommandException {return null;}
  public void setNext(String next) {return;}
}
