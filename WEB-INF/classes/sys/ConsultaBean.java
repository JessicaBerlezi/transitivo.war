package sys;


/** 
* <b>Title:</b>        Consulta<br>
* <b>Description:</b>  Classe para Paginar uma Tabela<br>
* <b>Copyright:</b>    Copyright (c) 2001<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Carlos Andre Fernandes
* @version 1.0
*/
public class ConsultaBean {
  /** Nome do jsp de Origem */
//  private String origem ;  
  /** Nome da Consulta em processamento */
  private String nomConsulta ;    
  /** N� max de registros p/ pagina */
  private int maxReg;
  /** Colunas visiveis na pagina��o */
  private String[] colunas;
  /** Numero de Colunas visiveis na pagina��o */
  private int nCol;  
  /** Cabe�alho das colunas visiveis na pagina��o 
  * posi��o correspondente da propriedade colunas, s� ser�
  * visivel a coluna que tenha cabe�alho (cabecalho.length()>0)
  */
  private String[] cabecalho;
  /** Percetual para cada coluna que ser� apresentada - utilizado no width das tables */
  private String[] colPerct;    
  /** Comando select para montar o conjunto de resultado */
  private String cmdSelect;  
  /** Expr. da Coluna da Ordem  */
  private String exprOrdem;  
  /** Expr. da Coluna da Ordem (expressao) */
  private String exprpkOrdem;  
  
  /** Parametros utilizados pela consulta */
  private String param0;    
  /** Parametros utilizados pela consulta */
  private String param1;    
  /** Parametros utilizados pela consulta (troca de % por }) */
  private String paramSub0;    
  /** Parametros utilizados pela consulta (troca de % por })*/
  private String paramSub1;    
  /** Direcao solicitada - Proximo ou Anterior */
  private String direcao;
  /** Tabela com o resultado parcial da paginacao */
  private String[][] tabRes; 
  /** Posicao (top) da Tela de apresentacao  */
  private String telaConsTop; 
  /** Posicao (Left) da Tela de apresentacao  */
  private String telaConsLeft; 
  /** Comando que originou a consulta  */
  private String cmdOrigem; 
  private String abrevSist; 
  
  /** Construtor. 
  */
  public ConsultaBean() {
	nomConsulta="";  		
	maxReg     = 18; //default.				
    colunas    = new String[1];	
    colunas[0] = "Indefinido" ;
	cabecalho  = new String[1];	
	cabecalho[0]= "Indefinido" ;				
    colPerct   = new String[1];	
    colPerct[0]= "100" ;	
	cmdSelect  = "";
	exprOrdem  = "";
	exprpkOrdem  = "";
	param0     = "" ;
	param1     = "" ;
    direcao    = "" ;
	tabRes     = new String[0][0];
	telaConsTop 	= "100px" ;	
	telaConsLeft	= "0px" ;
	abrevSist	= "USCAU" ;	
  }
  
  public String getAbrevSist(){
    return this.abrevSist;
  }	
  public void setAbrevSist(String abrevSist){
    this.abrevSist=abrevSist;
  }
  
  
  /** M�todo para obter o nome da Consulta.
  *   @return nomConsulta (String).
  */
  public String getNomConsulta(){
    return this.nomConsulta;
  }	
  /** M�todo para atualizar o nome da Consulta.
  *   @param nomConsulta (String).
  */
  public void setNomConsulta(String nomConsulta){
    this.nomConsulta=nomConsulta;
  }	
  /** M�todo para atualizar o n� de max. de regs. na pagina de consulta.
  *   @param maxReg (Int).
  */
  public void setMaxReg(int maxReg){
    this.maxReg=maxReg;
  }	
  /** M�todo para obter o n� de max. de regs. na pagina de consulta.
  *   @return maxReg (Int).
  */
  public int getMaxReg(){
    return this.maxReg;
  }	
  /** M�todo para atualizar as colunas visiveis na pagina��o.
  *   @param colunas (String[]).
  */
  public void setColunas(String[] colunas){
    int nCol  = colunas.length ;	
    if (nCol == 0)     {
       colunas    = new String[1] ;
       colunas[0] = "Indefinido" ;
    }  
	this.nCol = nCol ;
    this.colunas=colunas;
  }	
  /** M�todo para obter o numero de colunas
  *   @return nCol (String).
  */  
  public int getNCol(){
    return this.nCol;
  }	
  
  /** M�todo para obter as colunas visiveis na pagina��o.
  *   @return colunas (String[]).
  */
  public String[] getColunas(){
    return this.colunas;
  }	
  /** M�todo para atualizar o cabe�alho das colunas visiveis na pagina��o.
  *   Tamanho deve ser maior ou igual ao das colunas. 
  *   Valores nulo significa que n�o � coluna visivel.  
  *   @param cabecalho (String[]).
  */
  public void setCabecalho(String[] cabecalho){
	  if (this.nCol == 0)     {
    	 cabecalho    = new String[1] ;
	     cabecalho[0] = "Indefinido" ;
	  }        
	  this.cabecalho=cabecalho;
  }	
  /** M�todo para obter o cabe�alho das colunas visiveis na pagina��o.
  *   @return cabecalho (String[]).
  */
  public String[] getCabecalho(){
    return this.cabecalho;
  }	
  /** M�todo para obter o cabe�alho de uma coluna
  *   @return cabecalho (String).
  */
  public String getCabecalho(int col){
    return this.cabecalho[col];
  }	
  
  /** M�todo para atualizar os Percentuais a serem utilizados na apresenta��o das colunas visiveis na pagina��o.
  *   @param colPerct (String[]).
  */
public void setColPerct(String[] colPerct){
	 if (colPerct.length==0)	{
	     colPerct    = new String[1] ;
	     colPerct[0] = "100" ;
     }
	 int s = 0;
	 int p = 0;
     for (int i=0;i<colPerct.length;i++) {
	 	 p = 0;	 
	     try { p=Integer.parseInt(colPerct[i]); }
		 catch (NumberFormatException e) {  }  		           
	     s += p;
     }
     for (int i=0;i<colPerct.length;i++) {
	 	 p = 0;
	     try { p=Integer.parseInt(colPerct[i]); 
	         colPerct[i] = Integer.toString(p*100/s);		 
		 }
    	 catch (NumberFormatException e) {  }  		           	 
     }	 
    this.colPerct=colPerct;
  }	 
  /** M�todo para obter os Percentuais a serem utilizados na apresenta��o das colunas visiveis na pagina��o.
  *   @return colPerct (String[]).
  */
  public String[] getColPerct(){
    return this.colPerct;
  }	
  /** M�todo para obter o Percentual de uma coluna
  *   @return colPerct (String).
  */
  public String getColPerct(int col){
    return this.colPerct[col];
  }	
  
  /** M�todo para obter os parametros utilizados pela Consulta.
  *   @return param0 (String).
  */
  public String getParam0(){
    return this.param0;
  }	
  /** M�todo para obter os parametros sub (sem %)utilizados pela Consulta.
  *   @return paramSub0 (String).
  */
  public String getParamSub0(){
    return this.paramSub0;
  }	
  
  /** M�todo para atualizar  os parametros utilizados pela Consulta.
  *   @param param0 (String).
  */
public void setParam0(String param0){
  String parametrosSub  = "" ;
  String parametros   = "" ;	
  String letra        = "" ;
  for (int i=0; i<param0.length();i++) {
  	letra = param0.substring(i,i+1) ;
  	if (letra.compareTo("%")==0) parametrosSub += "{" ;
  	else parametrosSub += letra ;
  	if (letra.compareTo("{")==0) parametros += "%" ;
  	else parametros += letra ;						
  }
	this.param0    = parametros ;
	this.paramSub0 = parametrosSub ;
  }	
  /** M�todo para obter os parametros utilizados pela Consulta.
  *   @return param1 (String).
  */
  public String getParam1(){
    return this.param1;
  }	
  /** M�todo para obter os parametros Sub (sem %) utilizados pela Consulta.
  *   @return param1 (String).
  */
  public String getParamSub1(){
    return this.paramSub1;
  }	  
  /** M�todo para atualizar  os parametros utilizados pela Consulta.
  *   @param param1 (String).
  */
  public void setParam1(String param1){
	  String parametrosSub  = "" ;
	  String parametros   = "" ;	
	  String letra        = "" ;
	  for (int i=0; i<param1.length();i++) {
	  	letra = param1.substring(i,i+1) ;
	  	if (letra.compareTo("%")==0) parametrosSub += "{" ;
	  	else parametrosSub += letra ;
	  	if (letra.compareTo("{")==0) parametros += "%" ;
	  	else parametros += letra ;						
	  }
    this.param1    = parametros ;
    this.paramSub1 = parametrosSub ;
  }	
  /** M�todo para obter o comando a ser executado no Banco de Dados na forma��o da consulta.
  *   @return cmdSelect (String).
  */
  public String getCmdSelect(){
    return this.cmdSelect;
  }	
  /** M�todo para posicionar o comando a ser executado no Banco de Dados na forma��o da consulta.
  *   @param cmdSelect (String).
  */
  public void setCmdSelect(String cmdSelect){
    this.cmdSelect = cmdSelect;
  }	
  /** M�todo para obter a expressao que comp�e a Ordem
  *   @return exprOrdem (String).
  */
  public String getExprOrdem(){
    return this.exprOrdem;
  }	
  /** M�todo para posicionar a expressao que comp�e a Ordem
  *   @param exprOrdem (String).
  */
  public void setExprOrdem(String exprOrdem){
    this.exprOrdem = exprOrdem;
  }	
  
  /** M�todo para para obter a dire��o da consulta: Proximos ou Anteriores
  *   @return direcao (String).
  */
  public String getDirecao(){
    return this.direcao;
  }	
  /** M�todo para atualizar a dire��o da consulta: Proximos ou Anteriores
  *   @param direcao (String).
  */
  public void setDirecao(String direcao){
    this.direcao=direcao.trim();
  }	
  /** M�todo para obter a posicao Top da Tela de apresentacao
  *   @return telaConsTop (String).
  */
  public String getTelaConsTop(){
    return this.telaConsTop;
  }	
  /** M�todo para atualizar a posicao Top da Tela de apresentacao
  *   @param telaConsTop (String).
  */
  public void setTelaConsTop(String telaConsTop){
    this.telaConsTop=telaConsTop;
  }	  
  /** M�todo para obter a posicao Left da Tela de apresentacao
  *   @return telaConsLeft (String).
  */
  public String getTelaConsLeft(){
    return this.telaConsLeft;
  }	
  /** M�todo para atualizar a posicao Left da Tela de apresentacao
  *   @param telaConsLeft (String).
  */
  public void setTelaConsLeft(String telaConsLeft){
    this.telaConsLeft=telaConsLeft;
  }	  
  /** M�todo para obter o comando de Origem
  *   @return cmdOrigem (String).
  */
  public String getCmdOrigem(){
    return this.cmdOrigem;
  }	
  /** M�todo para atualizar o comando de Origem
  *   @param cmdOrigem (String).
  */
  public void setCmdOrigem(String cmdOrigem){
    this.cmdOrigem=cmdOrigem;
  }	

  /** M�todo de inicializa��o da tabela com o resultado parcial da pagina��o.
  */
  public void setTabRes(){  
	  try {
		  if (this.cmdSelect.length()!=0)	{
		      Dao dao = Dao.getInstance();		
	    	  this.tabRes = dao.ConsultaBuscaResultado(this) ;
		  }
		  /* validar que o cabecalho e colPerct tenham o mesmo tamanho de colunas */			
		  this.cabecalho = valTamanho(this.cabecalho);
		  this.colPerct  = valTamanho(this.colPerct);
	  } // fim do try
	  catch (Exception e) { 
	  }	// fim do catch 
  }	  
  /** M�todo para obter a tabela com o resultado parcial da pagina��o.
  *   @return tabRes (String[][]).
  */
  public String[][] getTabRes(){
      return this.tabRes;
  }
  
  /** M�todo para obter uma linha da tabela com o resultado parcial da pagina��o.
  *   @return tabRes (String[]).
  */
  public String[] getTabRes(int lin){
      return this.tabRes[lin];
  }
  /** M�todo para obter uma coluna de uma linha da tabela com o resultado parcial da pagina��o.
  *   @return tabRes (String).
  */
  public String getTabRes(int lin,int col){
      return this.tabRes[lin][col];
  }
  /** M�todo para obter o numero de linhas da tabela com o resultado parcial da pagina��o.
  *   @return tabResTamanho ().
  */
  public int getTabResTamanho(){
      return this.tabRes.length;
  }
  
  public String[] valTamanho(String[] aValidar){
  /* validar tamanho de cabecalho e colPert com colunas */
   int nc = this.colunas.length ;
   String aTemp[] = new String[nc] ;
   for (int i=0; i<nc ; i++)   {
		if (i<aValidar.length) aTemp[i]=aValidar[i] ;
		else aTemp[i]="" ;
   }    
   return aTemp ;
  }
}


