package sys;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Dao {
	private static Dao instance;  
	private ServiceLocator serviceloc ;  
	
	public static Dao getInstance() throws DaoException {
		if (instance == null)  instance = new Dao();
		return instance;
	}
	
	//**************************************************************************************
	private Dao() throws DaoException {
		try {	
			serviceloc = sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}  
	//************************************************************************************** 
	
	public String[][] ConsultaBuscaResultado(ConsultaBean myBean)
	throws DaoException {
		String cmdSel = myBean.getCmdSelect() ;
		String[][] tabRes = myBean.getTabRes() ;
		Connection conn =null ;					
		try {	
			conn = serviceloc.getConnection(myBean.getAbrevSist()) ;					
			/* preparar o select em funcao da direcao */					
			if (((myBean.getDirecao().compareTo("Ant")==0) || (myBean.getDirecao().compareTo("Prox")==0)) && (tabRes.length>0)) {
				String cmdPos = "" ;		
				if (myBean.getDirecao().compareTo("Ant")==0) cmdPos = "(("+myBean.getExprOrdem()+")<='"+tabRes[0][0]+"')" ;			
				else                                         cmdPos = "(("+myBean.getExprOrdem()+")>='"+tabRes[tabRes.length-1][0]+"')" ;															
				/* atualizar o select */
				if (cmdPos.length()>0) {
					int            a = cmdSel.indexOf(" WHERE ") ;		   							   																		
					if (a<0)       a = cmdSel.indexOf(" where ") ;
					if (a>=0) cmdSel = cmdSel.substring(0,a+7)+cmdPos+" and "+cmdSel.substring(a+7,cmdSel.length()) ;	   
					else {
						a = cmdSel.indexOf(" ORDER BY ") ;
						if (a>=0) cmdSel = cmdSel.substring(0,a)+" WHERE "+cmdPos+cmdSel.substring(a,cmdSel.length()) ;
						else 	  cmdSel = cmdSel.substring(0,cmdSel.length())+" WHERE "+cmdPos	 ;     
					}
				}  
			}
			int totReg      = 0 ;
			int pos         = 0 ;   	
			int numCol      = myBean.getNCol() ;
			String Aux      = "";
			String[] colunas  = myBean.getColunas() ;						
			/* instaciar o String para resultado */				
			//synchronized	
			Statement stmt = conn.createStatement();
			
			
			ResultSet rs   = stmt.executeQuery(cmdSel);
			/* calcula o numero de registro a ser retornado */
			while(rs.next() && totReg<=myBean.getMaxReg()){
				totReg++;
			}
			tabRes = new String[totReg][numCol+2]  ;
			/* carregar o resultado no String  */				   
			rs   = stmt.executeQuery(cmdSel);
			totReg = 0 ;
			while(rs.next() && totReg<=myBean.getMaxReg()){
				/* pegar conteudo da ordem e colocar em tabRes[][0] */
				Aux = rs.getString(1);
				if (Aux==null) Aux="";
				/* colocar na tabela de resultado */
				tabRes[totReg][0] = Aux;		   			   	       
				/* pegar conteudo da Pk e colocar em tabRes[][1] */
				Aux = rs.getString(2);
				if (Aux==null) Aux="";
				/* colocar na tabela de resultado */
				tabRes[totReg][1] = Aux;		   			   	       
				/* varrer as colunas e se est� no result set */
				for(int c=0;c<numCol;c++) {
					if (colunas[c].length() == 0) pos = 0 ;
					else                          pos = rs.findColumn(colunas[c]);
					if (pos>0)  Aux = rs.getString(pos);
					else        Aux = "" ;
					if (Aux==null) Aux = "" ;
					/* colocar na tabela de resultado */
					tabRes[totReg][c+2] = Aux;		   			   
				}
				totReg++;			 
			}
			rs.close();
			stmt.close();
			conn.close();
			return tabRes ;
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) { 
			throw new DaoException(ex.getMessage());		  	
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());		  
				}
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * DAO relativo � EnderecoBean
	 *-----------------------------------------------------------
	 */
	
	public boolean EnderecoInsert(EnderecoBean auxClasse, Connection conn)
	throws DaoException {
		boolean existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "select seq_tsmi_Endereco.nextval Seq_Endereco from dual";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setSeqEndereco(rs.getString("Seq_Endereco"));
				auxClasse.setPkid(auxClasse.getSeqEndereco());
			}
			rs.close();
			sCmd  = "INSERT INTO TSMI_Endereco (txt_Endereco,seq_endereco,num_Endereco,txt_Complemento, nom_Bairro, cod_municipio, num_cep, cod_uf,txt_email)";
			sCmd += "VALUES ('"+ auxClasse.getTxtEndereco() + "'," ;
			sCmd += " "+ auxClasse.getSeqEndereco()         + "," ;
			sCmd += "'"+ auxClasse.getNumEndereco()         +"',";
			sCmd += "'"+ auxClasse.getTxtComplemento()      +"'," ;
			sCmd += "'"+ auxClasse.getNomBairro()           +"'," ;
			sCmd += "'"+ auxClasse.getCodCidade()           +"'," ;
			sCmd += "'"+ auxClasse.getNumCEP()              +"'," ;
			sCmd += "'"+ auxClasse.getCodUF()               +"'," ;
			sCmd += "'"+ auxClasse.getTxtEmail()            +"')" ;
			stmt.execute(sCmd);
			stmt.close();
			existe = true ;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		
		return existe;
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Update em Endereco
	 *-----------------------------------------------------------
	 */
	public boolean EnderecoUpdate(EnderecoBean auxClasse, Connection conn) throws DaoException {
		boolean existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TSMI_Endereco set ";
			sCmd += "txt_endereco ='"    + auxClasse.getTxtEndereco()    + "',";
			sCmd += "num_endereco ='"    + auxClasse.getNumEndereco()    + "',";
			sCmd += "txt_complemento ='" + auxClasse.getTxtComplemento() + "',";
			sCmd += "nom_bairro ='"      + auxClasse.getNomBairro()      + "',";
			sCmd += "cod_municipio ='"   + auxClasse.getCodCidade()      + "',";
			sCmd += "num_cep ='"         + auxClasse.getNumCEP()         + "',";
			sCmd += "cod_UF ='"          + auxClasse.getCodUF()          + "',";
			sCmd += "txt_email ='"       + auxClasse.getTxtEmail()       + "'";			
			sCmd += " where seq_Endereco ='" + auxClasse.getSeqEndereco()+"'";
			stmt.execute(sCmd);
			stmt.close();
			existe = true ;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Delete em Endereco
	 *-----------------------------------------------------------
	 */
	public boolean EnderecoDelete(EnderecoBean auxClasse, Connection conn) throws DaoException {
		boolean 	existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "DELETE TSMI_ENDERECO where seq_Endereco ='" + auxClasse.getSeqEndereco()+"'";
			stmt.execute(sCmd);
			stmt.close();
			existe = true ;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	
	
	public boolean EnderecoLeBean(EnderecoBean myEnd,Connection conn) throws DaoException {
		//synchronized
		boolean bOk = true ;
		try {
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT Txt_Endereco,Seq_Endereco,Txt_Email,num_Endereco,cod_UF,Num_Cep,Txt_Complemento,cod_municipio,Nom_Bairro from TSMI_ENDERECO " ;
			sCmd += " WHERE Seq_Endereco = "+myEnd.getSeqEndereco();
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myEnd.setTxtEndereco(rs.getString("Txt_Endereco"));
				myEnd.setSeqEndereco(rs.getString("Seq_Endereco"));
				myEnd.setTxtEmail(rs.getString("Txt_Email"));
				myEnd.setNumEndereco(rs.getString("Num_Endereco")) ;
				myEnd.setCodUF(rs.getString("cod_UF"));
				myEnd.setNumCEP(rs.getString("Num_Cep"));
				myEnd.setTxtComplemento(rs.getString("Txt_Complemento"));
				myEnd.setCodCidade(rs.getString("cod_municipio"));				 
				myEnd.setNomBairro(rs.getString("Nom_Bairro"));
			}
			rs.close();
			stmt.close();
			//conn.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myEnd.setSeqEndereco("") ;
			myEnd.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myEnd.setSeqEndereco("") ;	
			myEnd.setPkid("0") ;	
			throw new DaoException(ex.getMessage());
		}
		return bOk;
	}
	
	
	/**
	 * @param auxClasse
	 * @param conn
	 * @return
	 * @throws DaoException
	 */
	/**
	 *-----------------------------------------------------------
	 * DAO relativo � TelefoneBean
	 *-----------------------------------------------------------
	 */
	public boolean TelefoneInsert(TelefoneBean auxClasse,Connection conn) throws DaoException {
		boolean existe = false ;
		try {
			
			Statement stmt  = conn.createStatement();
			String sCmd  = "select seq_tsmi_Telefone.nextval Seq_Telefone from dual";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				auxClasse.setSeqTelefone(rs.getString("Seq_Telefone"));
				auxClasse.setPkid(auxClasse.getSeqTelefone());
			}
			rs.close();
			sCmd  = "INSERT INTO TSMI_TELEFONE (seq_telefone,cod_DDD,num_Tel_comercial,num_Tel_residencial,num_Tel_fax,num_tel_movel)";
			sCmd += " VALUES ("+ auxClasse.getSeqTelefone() + "," ;
			sCmd += "'"  + auxClasse.getCodDDD()            + "'," ;
			sCmd += "'"  + auxClasse.getNumTelComercial()   + "'," ;
			sCmd += "'"  + auxClasse.getNumTelResidencial() + "'," ;
			sCmd += "'"  + auxClasse.getNumTelFax()         +"',";
			sCmd += "'"  + auxClasse.getNumTelMovel()       +"') " ;
			stmt.execute(sCmd);
			stmt.close();
			existe = true;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
			
		}
		return existe;
	}
	/**
	 *-----------------------------------------------------------
	 * Update em Telefone
	 *-----------------------------------------------------------
	 */
	public boolean TelefoneUpdate(TelefoneBean auxClasse, Connection conn) throws DaoException {
		boolean existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "UPDATE TSMI_Telefone set ";
			sCmd += "cod_DDD ='"             + auxClasse.getCodDDD()            + "',";
			sCmd += "num_tel_comercial ='"   + auxClasse.getNumTelComercial()   + "',";
			sCmd += "num_tel_residencial ='" + auxClasse.getNumTelResidencial() + "',";
			sCmd += "num_tel_fax ='"         + auxClasse.getNumTelFax()         + "',";
			sCmd += "num_tel_movel ='"       + auxClasse.getNumTelMovel()       + "' ";		  	
			sCmd += " where seq_telefone ="  + auxClasse.getSeqTelefone();
			stmt.execute(sCmd);
			stmt.close();
			existe = true;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	/**
	 *-----------------------------------------------------------
	 * Delete em Telefone
	 *-----------------------------------------------------------
	 */
	public boolean TelefoneDelete(TelefoneBean auxClasse, Connection conn) throws DaoException {
		boolean 	existe = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "DELETE TSMI_TELEFONE where seq_Telefone = '" + auxClasse.getSeqTelefone()+"'";
			stmt.execute(sCmd);
			stmt.close();
			existe = true;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
		}
		return existe;
	}
	
	//********
	public boolean TelefoneLeBean(TelefoneBean myTel,Connection conn) throws DaoException {
		//synchronized
		boolean bOk = true ;
		try {
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT Seq_Telefone,Cod_DDD,num_Tel_Residencial,Num_Tel_Comercial,Num_Tel_Fax,Num_Tel_Movel from TSMI_TELEFONE " ;
			sCmd       += " WHERE Seq_Telefone = " + myTel.getSeqTelefone() ;
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myTel.setNumTelResidencial(rs.getString("Num_Tel_Residencial"));
				myTel.setSeqTelefone(rs.getString("Seq_Telefone"));
				myTel.setNumTelComercial(rs.getString("Num_Tel_Comercial")) ;
				myTel.setNumTelFax(rs.getString("Num_Tel_Fax"));
				myTel.setNumTelMovel(rs.getString("Num_Tel_Movel"));			
				myTel.setCodDDD(rs.getString("Cod_DDD"));				 
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myTel.setSeqTelefone("") ;
			myTel.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myTel.setSeqTelefone("") ;	
			myTel.setPkid("0") ;	
			throw new DaoException(ex.getMessage());
		}
		return bOk;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert em TCAU_LOG_ACESSO
	 *-----------------------------------------------------------
	 */
	public void LogAcessoInsert(ACSS.UsuarioBean UsrUsuarioBean, ACSS.UsuarioFuncBean UsrFuncBeanId, 
			String num_sessao) throws DaoException {
		
		Connection conn = null;
		try {             
			conn = serviceloc. getConnection("ACSS") ;                          
			Statement stmt  = conn.createStatement();
			
			String      dataHoje = "";
			String      sCmd = " SELECT TO_CHAR(SYSDATE, 'dd/mm/yyyy:HH24:MI:SS') dataHoje FROM DUAL";
			ResultSet   rs = stmt.executeQuery(sCmd);
			
			if (rs.next())
				dataHoje = rs.getString("dataHoje");
			
			sCmd    = " INSERT " +
			" INTO TCAU_LOG_ACESSO( COD_ACESSO, " +
			"   COD_ORGAO_LOTACAO,  " +
			"   COD_ORGAO_ATUACAO,  " +
			"   NOM_USERNAME    ,   " +
			"   NOM_ABREV, " +
			"   NOM_OPERACAO    ,   " +
			"   SIG_FUNCAO      ,   " +
			"   NUM_SESSAO      ,   " +
			"   DAT_OPERACAO) " +
			" VALUES( SEQ_TCAU_LOG_ACESSO.NEXTVAL , " +                                 
			"'" + UsrUsuarioBean.getOrgao().getCodOrgao() + "'," +
			"'" + UsrUsuarioBean.getCodOrgaoAtuacao() + "'," +         
			"'" + UsrUsuarioBean.getNomUserName() + "',"   +
			"'" + UsrFuncBeanId.getJ_abrevSist() + "',"    +
			"'" + UsrFuncBeanId.getJ_nomFuncao() + "',"    +
			"'" + UsrFuncBeanId.getJ_sigFuncao() + "',"    +
			"'" + num_sessao + "',"    +
			" to_date('" +  dataHoje + "', 'dd/mm/yyyy:hh24:mi:ss'))";                                                
			stmt.execute(sCmd);
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {            
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) try {
				serviceloc.setReleaseConnection(conn);
			} 
			catch (ServiceLocatorException e) {
				throw new sys.DaoException (e.getMessage());
			}                                 
		}       
	}
	
	// E-MAIL
	
	/**--------------------------------------
	 * Relativo ao ConteudoEmailBean
	 ---------------------------------------*/
	
	//Consulta conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public ArrayList consultaConteudoEmail(String[] campos, String[] valores) throws sys.DaoException{
		ConteudoEmailBean emailBean = null;
		ArrayList listEmail = new ArrayList();  
		Connection conn = null;
		try {
			String sql = "SELECT COD_TIPO, COD_ORGAO, DSC_TIPO, TXT_EMAIL," +
			" NOM_REMETENTE, TXT_ASSUNTO FROM TSMI_EMAIL_EVENTO";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection("ACSS");
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				emailBean = new ConteudoEmailBean();
				emailBean.setCodOrgao(rs.getString("COD_ORGAO"));
				emailBean.setCodTipo(rs.getString("COD_TIPO"));
				emailBean.setDscTipo(rs.getString("DSC_TIPO"));
				emailBean.setTxtAssunto(rs.getString("TXT_ASSUNTO"));
				emailBean.setNomRemetente(rs.getString("NOM_REMETENTE"));
				emailBean.setConteudoEmail(rs.getString("TXT_EMAIL"));
				listEmail.add(emailBean);
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: consultaConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return listEmail;
	}
	
	// Altera conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public void alteraConteudoEmail(String[] campos, String[] valores, String conteudo,
			String nomRemetente, String txtAssunto) throws sys.DaoException{
		Connection conn = null;
		try {
			String sql = "UPDATE TSMI_EMAIL_EVENTO SET " +
			"TXT_EMAIL = '" + conteudo + "', " +
			"NOM_REMETENTE = '" + nomRemetente + "', "+
			"TXT_ASSUNTO = '" + txtAssunto + "' ";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection("ACSS");
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	// Insere conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public void insereConteudoEmail(String[] valores) throws sys.DaoException{
		ConteudoEmailBean emailBean = null;
		Connection conn = null;
		try {
			String sql = "INSERT INTO TSMI_EMAIL_EVENTO (COD_ORGAO,COD_TIPO," +
			"TXT_EMAIL,NOM_REMETENTE,TXT_ASSUNTO) ";
			sql += "VALUES (" + valores[0] + ", " + valores[1] + ", " + valores[2] + ", "+
			valores[3] + ", " +  valores[4] + ")";
			conn = serviceloc.getConnection("ACSS");
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			sql = "SELECT DSC_TIPO FROM TCAU_EMAIL_EVENTO ";
			sql += "WHERE COD_TIPO ='"+ valores[1] +"'";
			
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				emailBean = new ConteudoEmailBean ();
				emailBean.setDscTipo(rs.getString("dsc_tipo"));
			}
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	//Ler tipo
	public  boolean TipoLeBean (ConteudoEmailBean emailBean,int tpUsr) throws DaoException {
		
		boolean bOk = true ;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection("ACSS") ;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_tipo,dsc_tipo from TSMI_EMAIL_EVENTO" ;
			if (tpUsr==1)  sCmd += " WHERE dsc_tipo = '"+ emailBean.getDscTipo ()+"'" ;
			else           sCmd += " WHERE cod_tipo = '"+emailBean.getCodTipo()+"'" ;
			
			//Limpo o conte�do dos Beans de Endereco e Telefone
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			emailBean.setMsgErro("") ;
			while (rs.next()) {
				//Carrega os beans com os dados vindos da pesquisa de Orgao
				emailBean.setDscTipo(rs.getString("dsc_tipo"));
				emailBean.setCodTipo(rs.getString("cod_tipo"));
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	// FIM E-MAIL
	
	/**------------------------------------
	 *  DAO Relativo ao Monitor
	 --------------------------------------*/
	
	public boolean gravaMonitorBase(String processo, String objeto,
			String tipoMensagem, String texto,String codOrgao, Connection conn) 
	throws DaoException {
		boolean ok = false ;
		try {
			Statement stmt  = conn.createStatement();
			String sCmd  = "INSERT INTO TSMI_MONITOR (cod_monitor,nom_processo," +
			"nom_objeto,dat_log,txt_mensagem,cod_tipo_mensagem, cod_orgao)";
			sCmd += " VALUES ( SEQ_TSMI_MONITOR.NEXTVAL," ;
			sCmd += "'" + processo + "'," ;
			sCmd += "'" + objeto + "'," ;
			sCmd += "sysdate,";
			sCmd += "'" + texto + "'," ;
			sCmd += "'" + tipoMensagem + "'," ;
			sCmd += "'" + codOrgao + "')" ;
			stmt.execute(sCmd);
			stmt.close();
			ok = true ;
			conn.close();
		}
		catch (SQLException e) {
			ok = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			ok = false ;
			throw new DaoException(ex.getMessage());
			
		}
		return ok;
	}
	
	private  void ReleaseConnection(Connection conn) {
		if (conn != null) {
			try { serviceloc.setReleaseConnection(conn); }
			catch (Exception ey) { }		  
		}
	}
	
	
	/*-----------------------------------------------------------
	 * DAO relativo � TCAU_USUARIO
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public String ValidaBiometria(ACSS.UsuarioBean UserID) throws DaoException 
	{
		String biometria = null;
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT BIOMETRIA FROM TCAU_USUARIO" ;
			sCmd       += " WHERE NOM_USERNAME = " + "'" + UserID.getNomUserName() + "'";
			sCmd       += " AND NOM_USUARIO = " + "'" + UserID.getNomUsuario() + "'";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			if (rs.next()) {
				biometria = rs.getString("BIOMETRIA");
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return biometria;
	}
	
	/*-----------------------------------------------------------
	 * DAO relativo � TCAU_USUARIO
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public boolean CadastraBiometria(ACSS.UsuarioBean UserID, String password) throws DaoException {
		boolean existe = false ;
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt  = conn.createStatement();
			
			String sCmd  = "UPDATE TCAU_USUARIO SET BIOMETRIA = ";
			sCmd += "'" + password + "'" ;
			sCmd += " WHERE NOM_USERNAME = " ;
			sCmd += "'" + UserID.getNomUserName() + "'" ;
			sCmd += " AND NOM_USUARIO = " ;
			sCmd += "'" + UserID.getNomUsuario() + "'" ;
			
			stmt.execute(sCmd);
			stmt.close();
			existe = true ;
			conn.close();
		}
		catch (SQLException e) {
			existe = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			existe = false ;
			throw new DaoException(ex.getMessage());
			
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	} 
	
	/*-----------------------------------------------------------
	 * DAO relativo � TCAU_SUBSTITUICAO_FONETICA
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public List consultarSubstituicao() throws DaoException 
	{
		List desPalavras = new ArrayList();
		SubstituicaoBean substituicaoBean = new SubstituicaoBean();
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT ID_SUBSTITUICAO, DES_PALAVRA, DES_SUBSTITUICAO FROM TSMI_SUBSTITUICAO_FONETICA ORDER BY DES_PALAVRA ASC " ;
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				String id = rs.getString("ID_SUBSTITUICAO");
				substituicaoBean.setId(Integer.decode(id));
				substituicaoBean.setDesPalavra(rs.getString("DES_PALAVRA"));
				substituicaoBean.setDesSubstituicao(rs.getString("DES_SUBSTITUICAO"));
				desPalavras.add(substituicaoBean);
				substituicaoBean = new SubstituicaoBean();
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return desPalavras;
	}   
	
	/*-----------------------------------------------------------
	 * DAO relativo � TSMI_SUBSTITUICAO_FONETICA
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public List consultarSubstituicao(String desSubstituicao) throws DaoException 
	{
		List desPalavras = new ArrayList();
		SubstituicaoBean substituicaoBean = new SubstituicaoBean();
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT ID_SUBSTITUICAO, DES_PALAVRA, DES_SUBSTITUICAO FROM TSMI_SUBSTITUICAO_FONETICA WHERE UPPER(DES_PALAVRA) = '";
			sCmd        = sCmd + desSubstituicao.toUpperCase()	+ "'" + " ORDER BY DES_PALAVRA ASC";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				String id = rs.getString("ID_SUBSTITUICAO");
				substituicaoBean.setId(Integer.decode(id));
				substituicaoBean.setDesPalavra(rs.getString("DES_PALAVRA"));
				substituicaoBean.setDesSubstituicao(rs.getString("DES_SUBSTITUICAO"));
				desPalavras.add(substituicaoBean);
				substituicaoBean = new SubstituicaoBean();
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return desPalavras;
	}   
	
	/*-----------------------------------------------------------
	 * DAO relativo � TSMI_SUBSTITUICAO_FONETICA
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public List consultarSubstituicao(SubstituicaoBean substituicaoBean) throws DaoException 
	{
		List desPalavras = new ArrayList();
		SubstituicaoBean substituicaoBean2 = new SubstituicaoBean();
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT ID_SUBSTITUICAO, DES_PALAVRA, DES_SUBSTITUICAO FROM TSMI_SUBSTITUICAO_FONETICA WHERE UPPER(DES_PALAVRA) = '";
			sCmd        = sCmd + substituicaoBean.getDesPalavra().toUpperCase()	+ "'" + " AND ";
			sCmd = sCmd + "UPPER(DES_SUBSTITUICAO) = '";
			sCmd        = sCmd + substituicaoBean.getDesSubstituicao().toUpperCase()	+ "'";
			sCmd = sCmd + " ORDER BY DES_PALAVRA ASC ";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				String id = rs.getString("ID_SUBSTITUICAO");
				substituicaoBean2.setId(Integer.decode(id));
				substituicaoBean2.setDesPalavra(rs.getString("DES_PALAVRA"));
				substituicaoBean2.setDesSubstituicao(rs.getString("DES_SUBSTITUICAO"));
				desPalavras.add(substituicaoBean2);
				substituicaoBean2 = new SubstituicaoBean();
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return desPalavras;
	} 
	
	/*-----------------------------------------------------------
	 * DAO relativo � TSMI_SUPRESSAO_FONETICA
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public List consultarSupressao() throws DaoException 
	{
		List desSupressoes = new ArrayList();
		SupressaoBean supressaoBean = new SupressaoBean();
		int cont=0;
		String id = "";
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT ID_SUPRESSAO, DES_SUPRESSAO FROM TSMI_SUPRESSAO_FONETICA ORDER BY DES_SUPRESSAO ASC " ;
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				id = rs.getString("ID_SUPRESSAO");
				supressaoBean.setId(Integer.decode(id));
				supressaoBean.setDesSupressao(rs.getString("DES_SUPRESSAO"));
				desSupressoes.add(supressaoBean);
				supressaoBean = new SupressaoBean();
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return desSupressoes;
	} 
	
	/*-----------------------------------------------------------
	 * DAO relativo � TSMI_SUPRESSAO_FONETICA
	 * @author dgeraldes
	 *-----------------------------------------------------------
	 */
	public List consultarSupressao(String desSupressao) throws DaoException 
	{
		List desSupressoes = new ArrayList();
		SupressaoBean supressaoBean = new SupressaoBean();
		Connection conn = null;
		
		try {
			conn = conn = serviceloc.getConnection("ACSS");
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT ID_SUPRESSAO, DES_SUPRESSAO FROM TSMI_SUPRESSAO_FONETICA WHERE UPPER(DES_SUPRESSAO) = '";
			sCmd        = sCmd + desSupressao.toUpperCase()	+ "'" + " ORDER BY DES_SUPRESSAO ASC";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				String id = rs.getString("ID_SUPRESSAO");
				supressaoBean.setId(Integer.decode(id));
				supressaoBean.setDesSupressao(rs.getString("DES_SUPRESSAO"));
				desSupressoes.add(supressaoBean);
				supressaoBean = new SupressaoBean();
			}
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return desSupressoes;
	}
	
	public List atualizarExcluirSubstituicao(HashMap atualizar, HashMap excluir, int qtdElementos, boolean existeErro) throws sys.DaoException
	{
		SubstituicaoBean substituicaoBean = new SubstituicaoBean();
		List mensagens = new ArrayList();
		boolean mensagemAtualizacao = false;
		boolean mensagemExclusao = false;
		boolean mensagemErro = false;
		Connection conn = null;
		String sCmd;
		int chave = 1;
		try {
			for(; chave <= qtdElementos; chave++)
			{
				if(atualizar.containsKey(String.valueOf(chave)))
				{
					substituicaoBean = (SubstituicaoBean)atualizar.get(String.valueOf(chave));
					if(substituicaoBean.getId() != null)
						substituicaoBean.consultar(substituicaoBean);
					else
						substituicaoBean.consultar(substituicaoBean.getDesPalavra());
					if(substituicaoBean.getBeans().size() == 0)
					{
						if(substituicaoBean.getId() != null)
						{
							sCmd  = "UPDATE TSMI_SUBSTITUICAO_FONETICA SET DES_PALAVRA = ";
							sCmd += "'" + substituicaoBean.getDesPalavra() + "'" ;
							sCmd += ", DES_SUBSTITUICAO = " ;
							sCmd += "'" + substituicaoBean.getDesSubstituicao() + "'" ;
							sCmd += " WHERE ID_SUBSTITUICAO = " ;
							sCmd += substituicaoBean.getId() ;
						}
						else
						{
							sCmd  = "INSERT INTO TSMI_SUBSTITUICAO_FONETICA (ID_SUBSTITUICAO,DES_PALAVRA,DES_SUBSTITUICAO) VALUES(SEQ_TSMI_SUBSTITUICAO_FONETICA.NEXTVAL,";
							sCmd += "'" + substituicaoBean.getDesPalavra() + "', " ;
							sCmd += "'" + substituicaoBean.getDesSubstituicao() + "')" ;
						}
						
						conn = serviceloc.getConnection("ACSS");
						Statement stmt = conn.createStatement();
						stmt.executeUpdate(sCmd);
						stmt.close();
						
						if(!mensagemAtualizacao && !mensagemErro && !existeErro)
						{
							mensagens.add("Atualiza��o de substitui��o fon�tica realizada com sucesso!\n");
							mensagemAtualizacao = true;
						}
					}
					else
					{
						if(!mensagemErro && !existeErro)
						{
							mensagens = null;
							mensagens = new ArrayList();
							mensagens.add("Nenhuma opera��o pode ser realizada pelo(s) seguinte(s) motivo(s):\n");
							mensagemErro = true;
						}
						mensagens.add("O registro " + chave + " - " + substituicaoBean.getDesPalavra() + " j� existe no cadastro\n");
					} 
				}
				if(excluir.containsKey(String.valueOf(chave)))
				{
					substituicaoBean = (SubstituicaoBean)excluir.get(String.valueOf(chave));
					try
					{
						sCmd  = "DELETE FROM TSMI_SUBSTITUICAO_FONETICA";
						sCmd += " WHERE ID_SUBSTITUICAO = " ;
						sCmd += substituicaoBean.getId() ;
						
						conn = serviceloc.getConnection("ACSS");
						Statement stmt = conn.createStatement();
						stmt.execute(sCmd);
						stmt.close();
					}
					catch(Exception e) 
					{
						if(e.getMessage().substring(0,9).equalsIgnoreCase("ORA-02292"))
						{
							if(!mensagemErro && !existeErro)
							{
								mensagens = null;
								mensagens = new ArrayList();
								mensagens.add("Nenhuma opera��o pode ser realizada pelo(s) seguinte(s) motivo(s):\n");
								mensagemErro = true;
							}
							mensagens.add("Registro " + chave + ": j� existe um cadastro utilizando este registro\n");
							if(chave == qtdElementos)
							{
								substituicaoBean.setMensagem(mensagens);
								throw new sys.DaoException(substituicaoBean.getMensagem());
							}
						}
						else
							throw new sys.DaoException(e.getMessage());	
					}  
					if(!mensagemExclusao && !mensagemErro && !existeErro)
					{
						mensagens.add("Exclus�o de substitui��o fon�tica realizada com sucesso!\n");
						mensagemExclusao = true;
					}
				}
			}
			conn.close();
		}
		catch(Exception e ) { 
			throw new sys.DaoException(e.getMessage()); 
		}
		finally 
		{
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return mensagens;
	}  
	
	public List atualizarExcluirSupressao(HashMap atualizar, HashMap excluir, int qtdElementos) throws sys.DaoException
	{
		SupressaoBean supressaoBean = new SupressaoBean();
		List mensagens = new ArrayList();
		boolean mensagemAtualizacao = false;
		boolean mensagemExclusao = false;
		boolean mensagemErro = false;
		int chave = 1;
		Connection conn = null;
		String sCmd;
		try {
			for(; chave <= qtdElementos; chave++)
			{
				if(atualizar.containsKey(String.valueOf(chave)))
				{
					supressaoBean = (SupressaoBean)atualizar.get(String.valueOf(chave));
					supressaoBean.consultar(supressaoBean.getDesSupressao());
					if(supressaoBean.getBeans().size() == 0)
					{
						if(supressaoBean.getId() != null)
						{
							sCmd  = "UPDATE TSMI_SUPRESSAO_FONETICA SET DES_SUPRESSAO = ";
							sCmd += "'" + supressaoBean.getDesSupressao() + "'" ;
							sCmd += " WHERE ID_SUPRESSAO = " ;
							sCmd += supressaoBean.getId() ;
						}
						else
						{
							sCmd  = "INSERT INTO TSMI_SUPRESSAO_FONETICA (ID_SUPRESSAO,DES_SUPRESSAO) VALUES(SEQ_TSMI_SUPRESSAO_FONETICA.NEXTVAL,";
							sCmd += "'" + supressaoBean.getDesSupressao() + "') " ;
						}
						
						conn = serviceloc.getConnection("ACSS");
						Statement stmt = conn.createStatement();
						stmt.executeUpdate(sCmd);
						stmt.close();
						
						if(!mensagemAtualizacao && !mensagemErro)
						{
							mensagens.add("Atualiza��o de supress�o realizada com sucesso!\n");
							mensagemAtualizacao = true;
						}
					}
					else
					{
						if(!mensagemErro)
						{
							mensagens = null;
							mensagens = new ArrayList();
							mensagens.add("Nenhuma opera��o pode ser realizada pelo(s) seguinte(s) motivo(s):\n");
							mensagemErro = true;
						}
						mensagens.add("O registro " + chave + " - " + supressaoBean.getDesSupressao() + " j� existe no cadastro\n");
					} 
				}
				if(excluir.containsKey(String.valueOf(chave)))
				{
					supressaoBean = (SupressaoBean)excluir.get(String.valueOf(chave));
					try
					{
						sCmd  = "DELETE FROM TSMI_SUPRESSAO_FONETICA";
						sCmd += " WHERE ID_SUPRESSAO = " ;
						sCmd += supressaoBean.getId() ;
						
						conn = serviceloc.getConnection("ACSS");
						Statement stmt = conn.createStatement();
						stmt.execute(sCmd);
						stmt.close();
					}
					catch(Exception e) 
					{
						if(e.getMessage().substring(0,9).equalsIgnoreCase("ORA-02292"))
						{
							if(!mensagemErro)
							{
								mensagens = null;
								mensagens = new ArrayList();
								mensagens.add("Nenhuma opera��o pode ser realizada pelo(s) seguinte(s) motivo(s):\n");
								mensagemErro = true;
							}
							mensagens.add("Registro " + chave + ": j� existe um cadastro utilizando este registro\n");
							if(chave == qtdElementos)
							{
								supressaoBean.setMensagem(mensagens);
								throw new sys.DaoException(supressaoBean.getMensagem());
							}
						}
						else
							throw new sys.DaoException(e.getMessage());	
					}  
					if(!mensagemExclusao && !mensagemErro)
					{
						mensagens.add("Exclus�o de supress�o realizada com sucesso!\n");
						mensagemExclusao = true;
					}
				}
			}
			conn.close();
		}
		catch(Exception e ) { 
			throw new sys.DaoException(e.getMessage()); 
		}
		finally 
		{
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return mensagens;
	}  
	
	public void InsertLogException(GerenciadorException exception) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc. getConnection("ACSS") ;
			Statement stmt  = conn.createStatement();
			
			String      sCmd = " SELECT SEQ_TCAU_LOG_EXCEPTION.NEXTVAL AS ID_LOG_EXCEPTION FROM DUAL";
			ResultSet   rs = stmt.executeQuery(sCmd);
			
			if (rs.next())
				exception.setIdLogException(new Integer(rs.getInt("ID_LOG_EXCEPTION")));
			
			sCmd    = " INSERT " +
			" INTO TCAU_LOG_EXCEPTION( ID_LOG_EXCEPTION, " +
			"   COD_SISTEMA,  " +
			"   COD_ORGAO_ATUACAO,  " +
			"   NOM_EXCEPTION    ,   " +
			"   TXT_EXCEPTION, " +
			"   TXT_CAMINHO, " +
			"   NOM_USERNAME    ,   " +
			"   NOM_PROGRAMA      ,   " +
			"   EML_DESTINO      ,   " +
			"   DAT_EXCEPTION) " +
			" VALUES( :1,:2,:3,:4,:5,:6,:7,:8,:9,:10)";
			
			PreparedStatement pstmt  = conn.prepareStatement(sCmd);
			pstmt.setInt(1, exception.getIdLogException().intValue());
			pstmt.setString(2, exception.getCodSistema());
			pstmt.setString(3, exception.getCodOrgaoAtuacao());
			pstmt.setString(4, exception.getNomException());
			pstmt.setString(5, exception.getTxtException());
			pstmt.setString(6, exception.getTxtCaminho());
			pstmt.setString(7, exception.getNomUsername());
			pstmt.setString(8, exception.getNomPrograma());
			pstmt.setString(9, exception.getEmlDestino());
			pstmt.setTimestamp(10, new Timestamp(exception.getDatException().getTime()));
			pstmt.executeUpdate();
			pstmt.close();
			
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) try {
				serviceloc.setReleaseConnection(conn);
			}
			catch (ServiceLocatorException e) {
				throw new sys.DaoException (e.getMessage());
			}
		}
	}
	
}


