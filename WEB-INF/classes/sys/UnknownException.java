package sys;

public class UnknownException extends Exception {
  
  public UnknownException() {
    super();
  }

  public UnknownException(String msg) {
    super(msg);
  }
}
