package sys;

public class ServiceLocatorException extends Exception {
  
  public ServiceLocatorException() {
    super();
  }

  public ServiceLocatorException(String msg) {  
    super(msg);
  }
}
