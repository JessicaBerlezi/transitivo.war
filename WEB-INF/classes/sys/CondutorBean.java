package sys;



/**
* <b>Title:</b>        	SMIT-REC - Bean de Condutor <br>
* <b>Description:</b>  	Bean dados das Condutor - Tabela de Condutor<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Wellem Mello de Lyra
* @version 				1.0
*/

public class CondutorBean extends sys.PessoaFisicaBean { 

	private String numCNH;
	private String uFCNH;
	private String numPermissao;
	      
	

	public void setNumCNH(String numCNH)  {
		this.numCNH=numCNH ;
		if (numCNH==null) this.numCNH="" ;
	}  

	public String getNumCNH()  {
		return this.numCNH;
	}

	public void setUFCNH(String uFCNH)  {
		this.uFCNH=uFCNH ;
		if (uFCNH==null) this.uFCNH="" ;
	}  

	public String getUFCNH()  {
		return this.uFCNH;
	}


	public void setNumPermissao(String numPermissao)  {
		this.numPermissao=numPermissao ;
		if (numPermissao==null) this.numPermissao="" ;
	}  

	public String getNumPermissao()  {
		return this.numPermissao;
	}

		   
}
