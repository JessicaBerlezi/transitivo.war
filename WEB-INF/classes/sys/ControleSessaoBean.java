package sys;

import java.util.Date;

/**
 * @author dgeraldes
 */

public class ControleSessaoBean {
    
    private String codSistema;
    private String   codOrgaoLotacao; 
    private String   codOrgaoAtuacao; 
    private String nomUserName ;
    private String datBloqueio ;
    private String ip ;
    private String num_sessao ;
    
    public ControleSessaoBean()   throws sys.BeanException  {
        super();
        codSistema   = ""; ;	
        codOrgaoLotacao   = "";
        codOrgaoAtuacao   = "";
        nomUserName	  = "";
        datBloqueio	 = "";
        ip	= "";
        num_sessao = "";
    }
    
    public void setcodSistema(String codSistema)  {
        this.codSistema    = codSistema;
        if (codSistema==null) this.codSistema="" ;
    }
    
    public String getcodSistema()  {
        return this.codSistema;
    }
    
    //------------------------------------------
    
    public void setcodOrgaoLotacao(String codOrgaoLotacao)  {
        this.codOrgaoLotacao    = codOrgaoLotacao;
        if (codOrgaoLotacao==null) this.codOrgaoLotacao="" ;
    }
    
    public String getcodOrgaoLotacao()  {
        return this.codOrgaoLotacao;
    }
    
    //	------------------------------------------
    
    public void setcodOrgaoAtuacao(String codOrgaoAtuacao)  {
        this.codOrgaoAtuacao    = codOrgaoAtuacao;
        if (codOrgaoAtuacao==null) this.codOrgaoAtuacao="" ;
    }
    
    public String getcodOrgaoAtuacao()  {
        return this.codOrgaoAtuacao;
    }
    
    //------------------------------------------
    
    public void setnomUserName(String nomUserName)  {
        this.nomUserName    = nomUserName;
        if (nomUserName==null) this.nomUserName="" ;
    }
    
    public String getnomUserName()  {
        return this.nomUserName;
    }
    
    //------------------------------------------
    
    public void setdatBloqueio(String datBloqueio)  {
        this.datBloqueio    = datBloqueio;
        if (datBloqueio==null) this.datBloqueio= "00/00/0000" ;
    }
    
    public String getdatBloqueio()  {
        return this.datBloqueio;
    }
    
    //------------------------------------------
    
    public void setIp(String ip)  {
        this.ip    = ip;
        if (ip==null) this.ip="" ;
    }
    
    public String getIp()  {
        return this.ip;
    }
    
    //	------------------------------------------
    
    public void setNumSessao(String num_sessao)  {
        this.num_sessao    = num_sessao;
        if (num_sessao==null) this.num_sessao="" ;
    }
    
    public String getNumSessao()  {
        return this.num_sessao;
    }
    
    /*
     * Compara a data de inicio e de encerramento da sess�o, retornando o tempo em seg, min ou horas 
     */
    public static String CalculaTempoSessao(Date dtIniSessao, Date dtFimSessao)  {                   
        String tempoSessao = "";
        long difHora, difMinuto, difSegundo;
        
        
        long dtDif = (dtFimSessao.getTime() - dtIniSessao.getTime()) / 1000;                    
        difHora = dtDif / 3600; 
        difMinuto = (dtDif / 60) - (difHora*60);
        difSegundo = dtDif % 60;
        
        if(difHora > 0)
        {
        	if(difHora < 10)
                tempoSessao = "0" + Long.toString(difHora) + ":";
            else
                tempoSessao = Long.toString(difHora) + ":";
        }
        else
        {
        	tempoSessao = "00" + ":";
        }
        
        if(difMinuto > 0) {
            if(difHora > 0) {
                if(difMinuto < 10)
                    tempoSessao = tempoSessao + "0" + Long.toString(difMinuto) + ":";
                else
                    tempoSessao = tempoSessao + Long.toString(difMinuto) + ":";
            } 
            else
            {
            	if(difMinuto < 10)
                    tempoSessao = tempoSessao + "0" + Long.toString(difMinuto) + ":";
                else
                    tempoSessao = tempoSessao + Long.toString(difMinuto) + ":";
            }
        } else
            tempoSessao = tempoSessao + "00" + ":";
        
        if(difSegundo > 0) {
           if(difSegundo < 10)
              tempoSessao = tempoSessao + "0" + Long.toString(difSegundo);
           else
              tempoSessao = tempoSessao + Long.toString(difSegundo);
        } else
             tempoSessao = tempoSessao + "00";
            
        return tempoSessao;
    }
    
}