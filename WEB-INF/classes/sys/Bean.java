package sys;

import java.util.List;

/**
 * <b>Title: </b>sys.Bean<br>
 * <b>Description: </b>Bean com m�todos padr�es<br>
 * <b>Company: </b>MIND Informatica<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public abstract class Bean {
	
	protected String mensagem;
	
	public Bean() {
		mensagem = "";
	}		 
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
    public void setMensagem(List mensagens) {
        for (int i=0; i<mensagens.size(); i++)
       		   this.mensagem += mensagens.get(i);
     }
	
}
