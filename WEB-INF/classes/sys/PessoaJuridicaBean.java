package sys;



/**
* <b>Title:</b>        	SMIT-REC - Bean de PESSOAJURIDICA <br>
* <b>Description:</b>  	Bean dados das PessoasJuridicas - Tabela de PessoaJuridica<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Wellem Mello de Lyra
* @version 				1.0
*/

public class PessoaJuridicaBean extends sys.PessoaBean { 

	private String numCNPJ;
	private String nomContato;
	private String numTel1;
	private String numTel2;
	private String numTel3;
	private String numFAX;      
	

	public void setNumCNPJ(String numCNPJ)  {
		this.numCNPJ=numCNPJ ;
		if (numCNPJ==null) this.numCNPJ="" ;
	}  

	public String getNumCNPJ()  {
		return this.numCNPJ;
	}

	public void setNomContato(String nomContato)  {
		this.nomContato=nomContato ;
		if (nomContato==null) this.nomContato="" ;
	}  

	public String getNomContato()  {
		return this.nomContato;
	}


	public void setNumTel1(String numTel1)  {
		this.numTel1=numTel1 ;
		if (numTel1==null) this.numTel1="" ;
	}  

	public String getNumTel1()  {
		return this.numTel1;
	}

	public void setNumTel2(String numTel2)  {
		this.numTel2 = numTel2;
	if (numTel2==null) this.numTel2="" ;	
	}  

	public String getNumTel2()  {
		return this.numTel2;
	}
	
	public void setNumTel3(String numTel3)  {
		this.numTel3 = numTel3;
	if (numTel3==null) this.numTel3="" ;	
	}  

	public String getNumTel3()  {
		return this.numTel3;
	}

	   
}
