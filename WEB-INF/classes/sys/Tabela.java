package sys;

import java.util.Vector;
/**
* <b>Title:</b>        Tabela<br>
* <b>Description:</b>  Classe de controle de tabelas<br>
* <b>Copyright:</b>    Copyright (c) 2001<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @version 1.0
* @author Carlos Andre Fernandes
*
*/
public class Tabela {
  /** Nome da tabela */
  
  private String tabela;
  private String pkid;
  private String msgErro ;  
  private String j_abrevSist;    
  /** Construtor.
  */
  public Tabela() {
  	tabela    = "";
	pkid      = "0";
	msgErro   = "" ;		
	j_abrevSist="ACSS";    	
  }
  /** Atualiza o nome da tabela.
  *   @param tabela (String).
  */
  public void setTabela(String tabela){
  	if (tabela==null) tabela="";
   	this.tabela=tabela;
  }

  /** Obtem o nome da tabela.
  *   @return tabela (String).
  */
  public String getTabela(){
   	return tabela;
  }
  public void setJ_abrevSist(String j_abrevSist)  {
    if (j_abrevSist==null) j_abrevSist="ACSS" ;    
  this.j_abrevSist=j_abrevSist ;  
  }  
  public String getJ_abrevSist()  {
  	return this.j_abrevSist;
  }
  
  public void setPkid(String pkid)  {
    if (pkid==null) pkid="" ;    
	this.pkid=pkid ;  
  }  
  public String getPkid()  {
  	return this.pkid;
  }
  public void setMsgErro(Vector vErro)   {
   	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
	   }
   }
  public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
   }   
  public String getMsgErro()   {
   	return this.msgErro;
   }     

  /** Retorna o objeto como um String */
  public String toString(){
  	return this.tabela;
  } //deve ser sobreposta pela classe filha.

}