package sys;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class CommandToken {
	
	private static TokenBean TokenBeanId = new TokenBean();
	private static boolean bNovaHash = false;

	private static String geraToken(HttpSession session) throws CommandException {
		try {
			String retorno = sys.Util.encriptar(session.getId() + System.currentTimeMillis());
			return retorno;
		}
		catch (Exception ue) {
			throw new sys.CommandException("geraToken: " + ue.getMessage());	
		}

	}

	public static String gravaToken(HttpSession session, String id) throws CommandException {

		HashMap tokens = (HashMap) session.getAttribute("tokens");
		if (tokens == null)
			tokens = new HashMap();

		String token = geraToken(session);

		tokens.put(id, token);
		session.setAttribute("tokens", tokens);
		return token;
	}

	public static String getToken(HttpSession session, String id) {

		HashMap tokens = (HashMap) session.getAttribute("tokens");
		if (tokens == null)
			return null;

		return (String) tokens.get(id);
	}
	
	public static HashMap gravaRequisicao(HttpSession session){

		HashMap requisicoes = (HashMap) session.getAttribute("requisicoes");
		bNovaHash = false;
		if (requisicoes == null)
		{
			requisicoes = new HashMap();
			session.setAttribute("requisicoes", requisicoes);
			bNovaHash = true;
		}

		return requisicoes;
	}
	
	public static boolean verificaIndMulti(HttpSession session, HttpServletRequest req, String next){

		String reqId = (String) req.getParameter("j_id");
		if (reqId != null)
		{
			HashMap requisicoes = gravaRequisicao(session);
			HashMap temp = new HashMap();
			String temp_atributo = "";
			if (bNovaHash)
				return false;
			TokenBeanId = (TokenBean) requisicoes.get(reqId);
			if (TokenBeanId == null)
				return false;
			if (TokenBeanId.getIndMulti())
			{
				TokenBeanId.setIndContinua(true);
				TokenBeanId.setNext(next);
				Enumeration atributos = req.getAttributeNames();
				Vector vetor_atributos = new Vector();
				Vector vetor_valores = new Vector();
				while (atributos.hasMoreElements())
				{
					temp_atributo = atributos.nextElement().toString();
					vetor_atributos.addElement(temp_atributo);
					vetor_valores.addElement(req.getAttribute(temp_atributo));
				}
				temp.put("atributos", vetor_atributos);
				temp.put("valores", vetor_valores);
				TokenBeanId.setReq(temp);
				requisicoes.put(reqId, TokenBeanId);
				session.setAttribute("requisicoes", requisicoes);
				return true;
			}
		}
		return false;

	}
	
	public static TokenBean getTokenBean(){
		
		return TokenBeanId;
	}

	// Verifica se o token enviado pela requisicao e igual ao token da sessao
	private static boolean validaToken(HttpSession session, String reqId, String reqToken) {

		HashMap tokens = (HashMap) session.getAttribute("tokens");
		HashMap requisicoes = gravaRequisicao(session);
		if (tokens == null)
			return false;

		if (reqId == null)
			return false;

		if (reqToken == null)
			return false;

		String sesToken = (String) tokens.get(reqId);
		if (sesToken == null)
		{
			TokenBeanId = (TokenBean) requisicoes.get(reqId);
			if (TokenBeanId.getIndContinua() == false)
			{
				TokenBeanId.setIndMulti(true);
				requisicoes.put(reqId, TokenBeanId);
				session.setAttribute("requisicoes", requisicoes);
			}
			else
			{
				requisicoes.remove(reqId);
				return true;
			}
			return false;
		}

		if (sesToken.equals(reqToken)) {
			tokens.remove(reqId);
			session.setAttribute("tokens", tokens);
			//
			TokenBeanId = new TokenBean();
			TokenBeanId.setIndContinua(false);
			TokenBeanId.setIndMulti(false);
			requisicoes.put(reqId, TokenBeanId);
			session.setAttribute("requisicoes", requisicoes);
			return true;
		} else
			return false;
	}
	
	public static boolean validaToken(HttpServletRequest req) {

		String reqId = (String) req.getParameter("j_id");
		String reqToken = (String) req.getParameter("j_token");

		return validaToken(req.getSession(), reqId, reqToken);
	}

	public static boolean validaToken(HttpServletRequest req, UploadFile upload) {
		
		String reqId = (String) upload.getParameter("j_id");
		String reqToken = (String) upload.getParameter("j_token");

		return validaToken(req.getSession(), reqId, reqToken);
	}
	

}
