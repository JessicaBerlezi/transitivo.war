package sys;

import java.util.HashMap;

/**
 * @author pvinicius
 */

public class SupressaoBean extends PersistenteBean{
	
	String desSupressao;

	public SupressaoBean() {
		super();
		desSupressao = "";
	}
	
	public String getDesSupressao() {
		return desSupressao;
	}

	public void setDesSupressao(String desSupressao) {
		this.desSupressao = desSupressao;
	}
	
	public void consultar() throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			setBeans(dao.consultarSupressao());
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public void consultar(String desPreposicaoFonetica) throws sys.BeanException {
		try {
			Dao dao = Dao.getInstance();
			setBeans(dao.consultarSupressao(desPreposicaoFonetica));
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public boolean busca(String token)
	{
		for(int i = 0; i < beans.size(); i++)
			if(((SupressaoBean)beans.get(i)).getDesSupressao().equalsIgnoreCase(token))
				return true;
		return false;
	}
	
	public void atualizarExcluir(HashMap atualizar, HashMap excluir, int qtdElementos) throws sys.BeanException
	{
		try {
			Dao dao = Dao.getInstance();
			this.setMensagem(dao.atualizarExcluirSupressao(atualizar, excluir, qtdElementos));
		}
		catch(Exception e) {
			throw new sys.BeanException(e.getMessage());
		}		
	}
}
