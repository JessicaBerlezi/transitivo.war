package sys;



/**
* <b>Title:</b>        	SMIT - Bean de Telefone<br>
* <b>Description:</b>  	Bean dados de Telefone - Tabela de Telefone<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Wellem Lyra
* @version 				1.0
*/

public class TelefoneBean extends sys.HtmlPopupBean { 

	private String seqTelefone;
	private String codDDD;
	private String numTelComercial;
	private String numTelResidencial;
	private String numTelFax;
	private String numTelMovel;
	private String numTel;
	
	public TelefoneBean() {

	  	super();
	  	setTabela("TSMI_TELEFONE");	
	  	setPopupWidth(20);
		seqTelefone       = "";
		codDDD	          = "";	
	  	numTelComercial	  = "";
		numTelResidencial = "";
		numTelFax	      = "";
		numTelMovel	      = "";
		numTel	          = "";
	}
		
	public void setSeqTelefone(String seqTelefone)  {
		this.seqTelefone=seqTelefone ;
		if (seqTelefone==null) this.seqTelefone="" ;
	}  
	public String getSeqTelefone()  {
		return this.seqTelefone;
	}
	
	public void setCodDDD(String codDDD)  {
		this.codDDD=codDDD ;
		if (codDDD==null) this.codDDD="" ;
	}  
	public String getCodDDD()  {
		return this.codDDD;
	}
		
	
	public void setNumTelComercial(String numTelComercial)  {
		this.numTelComercial=numTelComercial  ;
		if (numTelComercial==null) this.numTelComercial="" ;
	}  
	public String getNumTelComercial()  {
		return this.numTelComercial;
	}

	public void setNumTelResidencial(String numTelResidencial)  {
		this.numTelResidencial = numTelResidencial;
	if (numTelResidencial==null) this.numTelResidencial="" ;	
	}  
	public String getNumTelResidencial()  {
		return this.numTelResidencial;
	}
	
	public void setNumTelFax(String numTelFax)  {
		this.numTelFax = numTelFax;
	if (numTelFax==null) this.numTelFax="" ;	
	}  
	public String getNumTelFax()  {
		return this.numTelFax;
	}
	
	public void setNumTelMovel(String numTelMovel ) {
		this.numTelMovel = numTelMovel;
	if (numTelMovel==null) this.numTelMovel="" ;	
	}  
	public String getNumTelMovel()  {
		return this.numTelMovel;
	}
	
	public void setNumTel(String numTel ) {
		this.numTel = numTel;
	if (numTel==null) this.numTel="" ;	
	}  
	public String getNumTel()  {
		return this.numTel;
	}   
      
}
