package sys;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b> LoginSistemas<br>
 * <b>Description:</b> Identifica o Usuario e a escolha do Sistema<br>
 * <b>Copyright:</b> Copyright (c) 2001<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class LoginSistemasCommand extends Command {

	private String next;
	public final static String CMDDEFAULT = "login";

	public LoginSistemasCommand() {
		next = "/sys/login.jsp";

	}

	public LoginSistemasCommand(String next) {
		this.next = next;
	}

	public String execute(ServletContext contexto, HttpServletRequest req, HttpServletResponse res)
			throws sys.CommandException {
		String nextRetorno = next;
		try {
			nextRetorno = execute(req);
		} catch (Exception ue) {
			throw new sys.CommandException("LoginSistemasCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

	public String execute(HttpServletRequest req) throws CommandException {
		try {
			// verifica se o Bean do Usuario ja existe - se nao cria
			HttpSession session = req.getSession();
			// cria os Beans do Usuario, Sistema e Funcionalidade
			ACSS.UsuarioBean UsuarioBeanId = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsuarioBeanId == null)
				UsuarioBeanId = new ACSS.UsuarioBean();
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();
			ACSS.SistemaBean SistemaBeanId = new ACSS.SistemaBean();
			REC.ParamOrgBean ParamOrgBeanId = new REC.ParamOrgBean();
			REC.ParamUFBean ParamUFBeanId = new REC.ParamUFBean();
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();

			// obtem e valida os parametros recebidos
			String j_abrevSist = req.getParameter("j_abrevSist");
			if (j_abrevSist == null)
				j_abrevSist = "ACSS";
			String nextRetorno = "/" + j_abrevSist + "/menuSistema.jsp";

			String j_jspOrigem = req.getParameter("j_jspOrigem");
			if (j_jspOrigem == null)
				j_jspOrigem = "";
			if (j_jspOrigem.equals("/sys/login.jsp") == false) {
				nextRetorno = "/sys/login.jsp";
				return nextRetorno;
			}
			String acao = req.getParameter("acao");
			if (acao == null)
				acao = " ";
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null)
				codOrgao = "";
			String codOrgaoAtuacao = req.getParameter("codOrgaoAtuacao");
			if (codOrgaoAtuacao == null)
				codOrgaoAtuacao = codOrgao;
			String sigOrgaoAtuacao = req.getParameter("sigOrgaoAtuacao");
			if (sigOrgaoAtuacao == null)
				sigOrgaoAtuacao = codOrgaoAtuacao;
			String codUf = req.getParameter("codUf");
			if (codUf == null)
				codUf = "";
			String nomUserName = req.getParameter("nomUserName");
			if (nomUserName == null)
				nomUserName = "";
			String codSenha = req.getParameter("codSenha");
			if (codSenha == null)
				codSenha = "";

			/* & */
			/*
			 * if ((nomUserName==null) || (codOrgao==null) || (codSenha==null))
			 * {
			 */
			if ((nomUserName.length() == 0) || (codOrgao.length() == 0) || (codSenha.length() == 0)) {
				nextRetorno = "/sys/login.jsp";
				return nextRetorno;
			}
			// le o Usuario e valida a senha recebida
			UsuarioBeanId.getOrgao().setCodOrgao(codOrgao);
			UsuarioBeanId.setCodOrgaoAtuacao(codOrgaoAtuacao);
			UsuarioBeanId.setSigOrgaoAtuacao(sigOrgaoAtuacao);
			UsuarioBeanId.setNomUserName(nomUserName);
			UsuarioBeanId.setCodUsuario("0");

			/* NOVA FRIBURGO (1) */

			UsuarioBeanId.Le_Usuario(3);
			UsuarioFuncBeanId.setCodOrgaoAtuacao(codOrgaoAtuacao);
			boolean semErro = true;

			if (UsuarioBeanId.getcookieUF() == null) {
				/*
				 * String UF = req.getParameter("codUfCNHTR");
				 */
				String UF = req.getParameter("codUf");
				if (UF == null)
					UF = "";
				if (UF.equals("")) {
					UsuarioBeanId.setMsgErro("UF n�o informada");
					nextRetorno = "/sys/login.jsp";
					semErro = false;
				}
			}

			if (acao.equals("TrocaUF")) {
				nextRetorno = "/sys/login.jsp";
				return nextRetorno;
			}

			if (UsuarioBeanId.getcookieUF() == null) {
				/*
				 * String UF = req.getParameter("codUfCNHTR");
				 */
				String UF = req.getParameter("codUf");
				if (UF == null)
					UF = "";
				if (UF.equals("")) {
					UsuarioBeanId.setMsgErro("UF n�o informada");
					nextRetorno = "/sys/login.jsp";
					semErro = false;
				}
			}

			if (UsuarioBeanId.getCodUsuario().equals("0")) {
				UsuarioBeanId.setMsgErro("Usu�rio n�o cadastrado no �rg�o " + UsuarioBeanId.getOrgao().getSigOrgao());
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}
			if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0, 10), UsuarioBeanId.getDatValidade()) < 0) {
				UsuarioBeanId.setMsgErro("Usu�rio com data de Validade expirada: " + UsuarioBeanId.getDatValidade());
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}
			// se for esqueci senha - ler perg e resp e retornar para o login
			if ((semErro) && (acao.equals("Esqueci"))) {
				UsuarioBeanId.Le_PergResp(j_abrevSist);
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}
			// se for esqueciOk digitou a resposta
			boolean senhaOk = false;
			if ((semErro) && (acao.equals("EsqueciOk"))) {
				UsuarioBeanId.Le_PergResp(j_abrevSist);
				String txt_resp = req.getParameter("txt_resp");
				if (txt_resp == null)
					txt_resp = " ";
				if (UsuarioBeanId.getNomResp().equals(sys.Util.encriptar(txt_resp)))
					senhaOk = true;
				else {
					UsuarioBeanId.setMsgErro("Resposta inv�lida");
					nextRetorno = "/sys/login.jsp";
					semErro = false;
				}
				UsuarioBeanId.setNomPerg("");
			}
			// Verifica a Senha
			if ((semErro) && (senhaOk == false) && (UsuarioBeanId.isSenhaValid(codSenha) == false)) {
				nextRetorno = "/sys/login.jsp";
				semErro = false;
				// Verifica quantidade de erro - 3 erros bloqueia
				String quantErro = req.getParameter("quantErro");
				if (quantErro == null)
					quantErro = "0";
				if (quantErro.equals("null"))
					quantErro = "0";
				int erro = Integer.parseInt(quantErro);
				String loginBloq = req.getParameter("loginBloq");
				if (loginBloq == null)
					quantErro = "0";
				if (loginBloq.equals("null"))
					loginBloq = "";
				if (UsuarioBeanId.verificaSenhaBloqueada() == false) {
					if (("".equals(loginBloq)) || (!loginBloq.equals(UsuarioBeanId.getNomUserName()))) {
						erro = 0;
						loginBloq = UsuarioBeanId.getNomUserName();
					}
					erro++;
					quantErro = Integer.toString(erro);
					if ((erro > 1) && (erro < UsuarioBeanId.MAX_TENTATIVAS)) {
						UsuarioBeanId.setMsgErro("Senha inv�lida! Bloqueio em " + (UsuarioBeanId.MAX_TENTATIVAS - erro)
								+ " tentativa(s).");
					}
					if (erro >= UsuarioBeanId.MAX_TENTATIVAS) {
						UsuarioBeanId.bloqueiaSenha();
						UsuarioBeanId.setMsgErro("Senha Bloqueada! Consulte o administrador do Sistema.");
						quantErro = Integer.toString(0);
						loginBloq = "";
					}
				}
				req.setAttribute("quantErro", quantErro);
				req.setAttribute("loginBloq", loginBloq);
			}
			// verifica se o Sistema solicitado existe
			SistemaBeanId.setNomAbrev(j_abrevSist);
			SistemaBeanId.Le_Sistema(j_abrevSist, 1);
			SistemaBeanId.setCodUF(codUf);

			if ((semErro) && (SistemaBeanId.getCodSistema().equals("0") == true)) {
				UsuarioBeanId.setMsgErro(SistemaBeanId.getMsgErro());
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}
			// verifica se o Usuario tem acesso (perfil) neste sistema
			UsuarioFuncBeanId.Le_PerfilUsuario(UsuarioBeanId, j_abrevSist);
			if ((semErro) && (UsuarioFuncBeanId.getCodPerfil().equals("0"))) {
				UsuarioBeanId.setMsgErro(UsuarioFuncBeanId.getMsgErro());
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}
			// verifica se o perfil do Usuario tem alguma funcionalidade
			UsuarioFuncBeanId.getFuncoes(UsuarioFuncBeanId);
			if ((semErro) && (UsuarioFuncBeanId.getCodPerfil().equals("0"))) {
				UsuarioBeanId.setMsgErro(UsuarioFuncBeanId.getMsgErro());
				nextRetorno = "/sys/login.jsp";
				semErro = false;
			}

			UsuarioBeanId.cdNatureza = Integer.parseInt(req.getParameter("codNatureza"));

			// Grava o usuario e o orgao na tabela TCAU_SESSAO
			ControleSessaoBean SessaoId = new ControleSessaoBean();
			SessaoId.setNumSessao(req.getSession().getId());
			SessaoId.setnomUserName(nomUserName);
			SessaoId.setcodOrgaoLotacao(codOrgao);
			SessaoId.setIp(req.getRemoteAddr());
			SessaoId.setcodOrgaoAtuacao(codOrgaoAtuacao);
			SessaoId.setcodSistema(SistemaBeanId.getCodSistema());
			ACSS.Dao.getInstance().AtualizaSessao(SessaoId);

			// sempre coloca os Beans com as Funcionalidades na sessao
			session.setAttribute("UsuarioBeanId", UsuarioBeanId);
			session.setAttribute("SistemaBeanId", SistemaBeanId);
			session.setAttribute("UsuarioFuncBeanId", UsuarioFuncBeanId);

			// colocar os parametors na sessao
			ParamOrgBeanId.PreparaParam(UsuarioBeanId);
			ParamUFBeanId.PreparaParam(codUf);

			session.setAttribute("ParamOrgBeanId", ParamOrgBeanId);
			session.setAttribute("ParamUFBeanId", ParamUFBeanId);

			// preparando os parametros do sistema
			ParamSistemaBeanId.setCodSistema(SistemaBeanId.getCodSistema());
			ParamSistemaBeanId.PreparaParam();
			session.setAttribute("ParamSistemaBeanId", ParamSistemaBeanId);
			ServiceLocator servicelocator = ServiceLocator.getInstance();
			session.setAttribute("j_qualBanco", servicelocator.getNomBanco());

			session.setAttribute("codNatureza", req.getParameter("codNatureza"));

			return nextRetorno;
		} catch (Exception ue) {
			throw new CommandException("LoginSistemas 001: " + ue.getMessage());
		}
	}
}