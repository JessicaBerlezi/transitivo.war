package sys;



/**
* <b>Title:</b>        	SMIT - Bean de Endereco<br>
* <b>Description:</b>  	Bean dados de Endereco - Tabela de Endereco<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Wellem Lyra
* @version 				1.0
*/

public class EnderecoBean extends sys.HtmlPopupBean { 

	private String seqEndereco;
	private String txtEndereco;
	private String numEndereco;
	private String txtComplemento;
	private String nomBairro;
	private String nomCidade;
	private String codCidade;
	
	private String numCEP;
	private String codUF;
	private String txtEmail;
       
	public EnderecoBean()	{
	
	  	super();
	  	setTabela("TSMI_ENDERECO");	
	  	setPopupWidth(35);
	  	seqEndereco = "";
		txtEndereco	= "";	
	  	numEndereco	= "";
		txtComplemento	= "";
		nomBairro   = "";
		nomCidade   = "";
		codCidade   = "";
		
		numCEP      = "";
		codUF       = "";
		txtEmail    = "";
	}
    
	public void setSeqEndereco(String seqEndereco)  {
		this.seqEndereco=seqEndereco ;
		if (seqEndereco==null) this.seqEndereco="" ;
	}  
	public String getSeqEndereco()  {
		return this.seqEndereco;
	}
	
	public void setNumEndereco(String numEndereco)  {
		this.numEndereco=numEndereco ;
		if (numEndereco==null) this.numEndereco="" ;
	}  
	public String getNumEndereco()  {
		return this.numEndereco;
	}

	public void setTxtEndereco(String txtEndereco)  {
		this.txtEndereco=txtEndereco ;
		if (txtEndereco==null) this.txtEndereco="" ;
	}  
	public String getTxtEndereco()  {
		return this.txtEndereco;
	}

	public void setTxtComplemento(String txtComplemento)  {
		this.txtComplemento = txtComplemento;
	if (txtComplemento==null) this.txtComplemento="" ;	
	}  
	public String getTxtComplemento()  {
		return this.txtComplemento;
	}

	public void setNomBairro(String nomBairro)  {
		this.nomBairro=nomBairro ;
		if (nomBairro==null) this.nomBairro="" ;
	}  
	public String getNomBairro()  {
		return this.nomBairro;
	}

	public void setNomCidade(String nomCidade)  {
		this.nomCidade=nomCidade ;
		if (nomCidade==null) this.nomCidade="" ;
	}  
	public String getNomCidade()  {
		return this.nomCidade;
	}
	public void setCodCidade(String codCidade)  {
		this.codCidade=codCidade ;
		if (codCidade==null) this.codCidade="" ;
	}  
	public String getCodCidade()  {
		return this.codCidade;
	}

	public void setNumCEP(String numCEP)  {
		this.numCEP=numCEP ;
		if (numCEP==null) this.numCEP="" ;
	}  
	public String getNumCEP()  {
		return this.numCEP;
	}
	public void setNumCEPEdt(String numCEP)  {
	  if (numCEP==null)      numCEP = "";
	  if (numCEP.length()<9) numCEP += "          ";
	  this.numCEP=numCEP.substring(0,5)+numCEP.substring(6,9) ;
	}  
	public String getNumCEPEdt()  {
		if ("".equals(this.numCEP)) return this.numCEP ;
		if (this.numCEP.length()<8) this.numCEP+="         ";
		return this.numCEP.substring(0,5)+"-"+this.numCEP.substring(5,8);
	}
	public void setCodUF(String codUF)  {
		this.codUF=codUF ;
		if (codUF==null) this.codUF="" ;
	}  
	public String getCodUF()  {
		return this.codUF;
	}
	
	public void setTxtEmail(String txtEmail)  {
		this.txtEmail=txtEmail ;
		if (txtEmail==null) this.txtEmail="" ;
	}  
	public String getTxtEmail()  {
		return this.txtEmail;
	}
}
