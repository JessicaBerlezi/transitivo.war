package sys;



/**
* <b>Title:</b>        	SMIT-REC - Bean de PESSOA <br>
* <b>Description:</b>  	Bean dados das Pessoas - Tabela de PessoaFisica<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Wellem Mello de Lyra
* @version 				1.0
*/

public class PessoaBean { 

	private String nomePessoa;
	private String dtnascPessoa;
	private String endPessoa;      
	

	public void setNomePessoa(String nomePessoa)  {
		this.nomePessoa=nomePessoa ;
		if (nomePessoa==null) this.nomePessoa="" ;
	}  

	public String getNomePessoa()  {
		return this.nomePessoa;
	}

	public void setDtnascPessoa(String dtnascPessoa)  {
		this.dtnascPessoa=dtnascPessoa ;
		if (dtnascPessoa==null) this.dtnascPessoa="" ;
	}  

	public String getDtnascPessoa()  {
		return this.dtnascPessoa;
	}

	public void setEndPessoa(String endPessoa)  {
		this.endPessoa = endPessoa;
	if (endPessoa==null) this.endPessoa="" ;	
	}  

	public String getEndPessoa()  {
		return this.endPessoa;
	}

	   
}
