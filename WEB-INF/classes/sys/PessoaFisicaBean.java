package sys;



/**
* <b>Title:</b>        	SMIT-REC - Bean de PESSOAFISICA <br>
* <b>Description:</b>  	Bean dados das PessoasFisicas - Tabela de PessoaFisica<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
* <b>Company:</b>      	Itc - Informatica Tecnologica e Cientifica<br>
* @author  				Wellem Mello de Lyra
* @version 				1.0
*/

public class PessoaFisicaBean extends sys.PessoaBean { 

	private String numCPF;
	private String numTel;
	private String numCel;      
	

	public void setNumCPF(String numCPF)  {
		this.numCPF=numCPF ;
		if (numCPF==null) this.numCPF="" ;
	}  

	public String getNumCPF()  {
		return this.numCPF;
	}

	public void setNumTel(String numTel)  {
		this.numTel=numTel ;
		if (numTel==null) this.numTel="" ;
	}  

	public String getNumTel()  {
		return this.numTel;
	}

	public void setNumCel(String numCel)  {
		this.numCel = numCel;
	if (numCel==null) this.numCel="" ;	
	}  

	public String getNumCel()  {
		return this.numCel;
	}

	   
}
