package sys;

public class FonetizadorException extends Exception {
	
	public FonetizadorException() {
		super();
	}
	
	public FonetizadorException(String msg) {  
		super(msg);
	}
}
