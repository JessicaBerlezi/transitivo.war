/*
 * Created on 21/01/2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package sys;

import java.util.HashMap;

/**
 * @author dgeraldes
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class TokenBean {
	private String next;
	private boolean ind_continua;
	private boolean ind_multi;
	private HashMap requi;
	
	public TokenBean() {
		requi = new HashMap();
		next       = "";
		ind_continua	= false;	
		ind_multi	  = false;
	}
		
	public void setIndContinua(boolean ind_continua)  {
		this.ind_continua=ind_continua ;
	}  
	public boolean getIndContinua()  {
		return this.ind_continua;
	}
	
	public void setIndMulti(boolean ind_multi)  {
		this.ind_multi=ind_multi ;
	}  
	public boolean getIndMulti()  {
		return this.ind_multi;
	}
	
	public void setNext(String next)  {
		this.next=next ;
		if (next==null) this.next="" ;
	}  
	public String getNext()  {
		return this.next;
	}
		
	public void setReq(HashMap requi)  {
		this.requi = requi  ;
	}  
	public HashMap getReq()  {
		return this.requi;
	}

}
