package sys;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Monitor {
  private String path;
  private String nomArquivo;
  
  public Monitor(String pathParametro, String nomArquivo) {
    path = pathParametro; 
    path = "c:\\smit\\log\\";   
    this.nomArquivo = nomArquivo;
  }
  
  public void iniciaMonitor(String iniMens){
    gravaMonitor("Ini: "+iniMens);
  } // end of iniciaMonitor
  
  public void finalizaMonitor(String fimMens){
    gravaMonitor("Fim: "+fimMens);
  }  // end of finalizaMonitor

  public void gravaMonitor(String texto){
    // Geracao do nome do arquivo Carga_YYYYMMDD.LOG
    Calendar calendar = new GregorianCalendar();
    Date today = new Date();
    calendar.setTime(today);
    String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
    String mes = calendar.get(Calendar.MONTH)+ 1 + ""; // deve somar um mesmo !!
    String ano = calendar.get(Calendar.YEAR) + "";
    String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
    String min = calendar.get(Calendar.MINUTE) + "";
    String seg = calendar.get(Calendar.SECOND) + "";
    
    if (dia.length()<2) {dia = "0" + dia;}
    if (mes.length()<2) {mes = "0" + mes;}
    if (hor.length()<2) {hor = "0" + hor;}
    if (min.length()<2) {min = "0" + min;}
    if (seg.length()<2) {seg = "0" + seg;}

    String nomeArq = path + nomArquivo + "_" + ano + mes + dia +".LOG";

    // criacao ou apende do arquivo de erro
    try {
      FileOutputStream fos = new FileOutputStream(nomeArq,true);
      OutputStreamWriter file = new OutputStreamWriter(fos);
      PrintWriter pw = new PrintWriter(file); // utilizado no StackTrace
      try {
        file.write(hor+":"+min+":"+seg+" - "+texto+"\n");
        file.close();
      }
      catch (IOException e) {
        e.printStackTrace();
      }  
    }
    catch (FileNotFoundException e) {
      System.out.println("Arquivo nao encontrado e nao gerado");
      e.printStackTrace();
    }
  } // end of gravaMonitor

}