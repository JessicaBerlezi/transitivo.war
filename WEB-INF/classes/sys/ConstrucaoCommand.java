package sys;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;

import REG.ArquivoRecebidoBean;
import REG.LinhaArquivoRec;

public class ConstrucaoCommand extends Command {
  private String next;

  public ConstrucaoCommand() {
    next             = "/sys/Construcao.jsp" ;
  }

  public ConstrucaoCommand(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req)
    throws CommandException {
//    String j_jspDestino = req.getParameter("j_jspDestino");
//    if( j_jspDestino == null) j_jspDestino =" ";
//	if (j_jspDestino.indexOf(".jsp")>0) next = j_jspDestino ;	
    return next;
  }
  
  
  public String execute(HttpServletRequest req, sys.UploadFile upload)
  throws sys.CommandException {
	  
	  return next;
  }
  
  
  
  
  public void setNext(String next){ 
  	  this.next= next ;
	  return; 
  }
}
