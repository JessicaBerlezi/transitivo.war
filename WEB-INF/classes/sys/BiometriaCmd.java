/*
 * Created on 16/02/2005
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package sys;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @author dgeraldes
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class BiometriaCmd extends sys.Command{
	private String next;
	
	private static final String jspPadrao = "/sys/ValidaBiometria.jsp";

	  public BiometriaCmd() {
		next = jspPadrao;
	  }

	  public BiometriaCmd(String next) {
		this.next = next;
	  }

	  
	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {  
			// cria os Beans, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
			
			//cria os Beans do Usuario, se n�o existir
			ACSS.UsuarioBean UsrBeanId = (ACSS.UsuarioBean)req.getAttribute("UsrBeanId") ;
			if (UsrBeanId==null)        UsrBeanId = new ACSS.UsuarioBean() ;			
							
			// obtem e valida os parametros recebidos					
			String acao           = req.getParameter("acao");  
			if(acao==null)       acao =" ";
			String biometria    = req.getParameter("biometria");
			if(biometria==null)   biometria="";
			
			String pkid = req.getParameter("pkid");   
			if(pkid==null)          pkid  ="0";  		  	  	    	  
			String codOrgao       = req.getParameter("codOrgao");	     
			if(codOrgao==null)      codOrgao=""; 
			String nomUsuario     = req.getParameter("nomUsuario");	     
			if(nomUsuario==null)    nomUsuario=" "; 
			String numCpf         = req.getParameter("numCpf");
			if(numCpf==null)        numCpf="";
			String nomUserName    = req.getParameter("nomUserName");	     
			if(nomUserName==null)   nomUserName=""; 
			String codSenha       = req.getParameter("codSenha");
			if(codSenha==null)      codSenha="";  
			String datValidade    = req.getParameter("datValidade");
			if(datValidade==null)   datValidade="";  
			String horaValidade	  = req.getParameter("horaValidade");
			if(horaValidade==null)  horaValidade="";
			String codUsrResp     = req.getParameter("codUsrResp");
			if(codUsrResp==null)    codUsrResp="";  
			String codUsrRespAlt  = req.getParameter("codUsrRespAlt");
			if(codUsrRespAlt==null) codUsrRespAlt="";  
			String email  = req.getParameter("email");
			if(email==null) email="";
			String localTrabalho  = req.getParameter("localTrabalho");
			if(localTrabalho==null) localTrabalho="";
			
			//Preencher o Bean
			UsrBeanId.setPkid(pkid) ;
			UsrBeanId.setCodUsuario(pkid) ;			
			UsrBeanId.getOrgao().setCodOrgao(codOrgao);				
			UsrBeanId.setNomUsuario(nomUsuario);		
			UsrBeanId.setNumCpfEdt(numCpf);			
			UsrBeanId.setNomUserName(nomUserName);					
			UsrBeanId.setCodSenha(codSenha);		
			UsrBeanId.setDatValidade(datValidade);	
			UsrBeanId.setHoraValidade(horaValidade);
			UsrBeanId.setCodUsrResp(codUsrResp);		
			UsrBeanId.setCodUsrRespAlt(codUsrRespAlt);	
			UsrBeanId.setEmail(email);
			UsrBeanId.setLocalTrabalho(localTrabalho);
				 
			Vector vErro = new Vector();
			sys.GerenciadorBiometrico BiometriaId = sys.GerenciadorBiometrico.getInstance();
			
			if (acao.equals("cadBiometria")) {
				BiometriaId.CadastraBiometria(UsrBeanId,biometria);
				vErro.clear();
				vErro.addElement("Biometria cadastrada com sucesso");
				UsrBeanId.setMsgErro(vErro);
				nextRetorno="/sys/CadastraBiometria.jsp";
			}
			
			if (acao.equals("valBiometria")) {
				boolean autentica = false;
				autentica = BiometriaId.ValidaBiometria(UsrLogado,biometria);
				
				if(autentica)
				{
					vErro.clear();
					vErro.addElement("Autentica��o de biometria aprovada");
					UsrBeanId.setMsgErro(vErro);
					nextRetorno=next;
				}
				else
				{
					vErro.clear();
					vErro.addElement("Autentica��o de biometria n�o foi aprovada");
					UsrBeanId.setMsgErro(vErro);
					nextRetorno="/sys/ValidaBiometria.jsp";
				}
			}
			
			req.setAttribute("UsrBeanId",UsrBeanId) ;
	

		}
		catch (Exception ue) {
	  		throw new sys.CommandException("BiometriaCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
}
