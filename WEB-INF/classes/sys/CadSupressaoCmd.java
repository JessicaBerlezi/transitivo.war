package sys;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

/**
 * @author pvinicius
 */

public class CadSupressaoCmd extends sys.Command{
	  private static final String jspPadrao="sys/CadSupressao.jsp";
	  private String next;
	  
	  public CadSupressaoCmd() {
	    next = jspPadrao;
	  }

	  public CadSupressaoCmd(String next) {
	    this.next = jspPadrao;
	  }
	  /**
	   * 
	   * @author Pedro N�brega
	   * @since 01/03/2005
	   * @version 1.2
	   * @param req Requisi��o do JSP a ser tratada
	   * @return retorna a pr�xima p�gina JSP a ser exibida
	   * @exception sys.CommandException
	   */
	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = jspPadrao ;
		SupressaoBean supressaoBean = new SupressaoBean();
		HashMap atualizar = new HashMap();
		HashMap excluir = new HashMap();
		int chave = 0;
	    try 
		{     
	    	String acao = req.getParameter("acao");
	    	if(acao == null)
	    		acao = "";
			int cont = 0;
	    	boolean mensagemAtualizacao = false;
	        boolean mensagemExclusao = false;
	    	if(acao.equalsIgnoreCase("atualizar"))
	    	{	
	    		String[] id= req.getParameterValues("id");
	    		String[] descricao = req.getParameterValues("descricao");  
	    		String[] descricao_aux = req.getParameterValues("descricao_aux");
	    		//id[0] = "";
	    		for(int i=0; i<descricao.length ;i++)
				{	
					if( descricao[i].equals(descricao_aux[i]) )
		  				continue;
					chave = i + 1;
					if(!id[i].equals(""))
						supressaoBean.setId(Integer.valueOf(id[i]));
					else
						supressaoBean.setId(null);
					supressaoBean.setDesSupressao(descricao[i]);
					if(!descricao[i].equals(""))
						atualizar.put(String.valueOf(chave), supressaoBean);
					else
						excluir.put(String.valueOf(chave), supressaoBean);
					supressaoBean = null;
					supressaoBean = new SupressaoBean();
				}
	    		supressaoBean.atualizarExcluir(atualizar, excluir, descricao.length);
			}
	    	supressaoBean.consultar();
	    	req.setAttribute("supressaoBean",supressaoBean);	
	    }
	    catch (Exception ue) 
		{	      
	    	supressaoBean.setMensagem(ue.getMessage());
	      try
		  {
	      	supressaoBean.consultar();
		  }
	      catch (Exception e)
		  {
	      	throw new CommandException(e.getMessage());
		  }
	      req.setAttribute("supressaoBean",supressaoBean);
	    }
		return nextRetorno;
	  }
}
