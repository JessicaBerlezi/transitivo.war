package sys;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class IOUtil {
    
    private static final int SIZE = 100000;
    private static final byte[] BUFFER = new byte[SIZE];
    
    public static void compactarZip(String[] arquivos, File arquivoZip, boolean dirInfo) throws IOException {
        
        ZipOutputStream out = null;
        HashMap lista = new HashMap();
        try {
            out = new ZipOutputStream(new FileOutputStream(arquivoZip));
            
            for (int i = 0; i < arquivos.length; i++) {
                FileInputStream in = new FileInputStream(arquivos[i]);
                
                //Monta o nome do arquivo no zip, com ou sem informa��o de diret�rio (dirInfo)
                String nomArquivo = "";
                if (dirInfo) nomArquivo = arquivos[i];
                else         nomArquivo = new File(arquivos[i]).getName();
                
                //Verifica se o arquivo j� existe no zip
                if (lista.containsKey(nomArquivo)) {
                    in.close();
                    continue;
                } else
                    lista.put(nomArquivo, nomArquivo);                
                    
                out.putNextEntry(new ZipEntry(nomArquivo));
                
                int len;
                while ((len = in.read(BUFFER)) > 0)                  
                    out.write(BUFFER, 0, len);             
                
                out.closeEntry();               
                in.close();         
            }                               
            out.close();        
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        } finally {
            try {
                if (out != null) out.close();
            } catch (IOException e) {
                System.out.println(e);
            }    
        }
    }
    
    public static void descompactarZip(File arquivo, String destino) throws IOException {
                
        ZipInputStream input = null;
        BufferedOutputStream output = null;
        ZipEntry item;    
        
        try {       
            FileInputStream fis = new FileInputStream(arquivo);
            input = new ZipInputStream(fis);                        
            
            while ((item = input.getNextEntry()) != null )
            {
                if (item.isDirectory()) {
                    //Cria o diretorio do destino
                    File tempDir = new File(destino + "/" + item.getName());
                    tempDir.mkdirs();                                   
                } else {
                    try {
                        // Cria o diretorio do destino
                        File tempDir = new File(destino + "/" + item.getName().substring(0, item.
                                getName().lastIndexOf("/")));
                        tempDir.mkdirs();
                        
                        //Cria o arquivo de destino vazio
                        File tempArquivo = new File(destino + "/" + item.getName());                    
                        tempArquivo.createNewFile();
                        
                        FileOutputStream fos = new FileOutputStream(tempArquivo);
                        output = new BufferedOutputStream(fos);
                        
                        while (true) {
                            int qtdBytes = input.read(BUFFER);
                            if (qtdBytes == -1)
                                break;
                            output.write(BUFFER, 0, qtdBytes);
                        }
                        output.close();
                        
                    } catch (Exception e) {
                        throw new IOException(e.getMessage());
                    }                                           
                }
            }
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        } finally {
            try {
                if (input != null) input.close();
            } catch (IOException e) {
                System.out.println(e);
            }    
        }
    }           
    
}
