package sys;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;

/**
 * @author Glaucio Jannotti
 */

public class UploadFile {
    
    List items;
    String sMsgErro;
    
    public UploadFile(HttpServletRequest req) throws FileUploadException {
        
        try {
        	items = new ArrayList<String>();
        	this.sMsgErro="";
            DiskFileUpload upload = new DiskFileUpload();
            upload.setSizeMax(-1);
            items = upload.parseRequest(req);
            if (this.items==null) items = new ArrayList<String>();
            
        } catch (FileUploadException e) {   
        	setMsgErro("Erro ao enviar o arquivo :"+e.getMessage());
            /*throw new FileUploadException("UploadFile: "+e.getMessage());*/
        }
    }
    
    public String getParameter(String name) {
    	if (this.items==null) items = new ArrayList<String>();
        String value = "";
        Iterator iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            if (item.isFormField() && item.getFieldName().equals(name)) {
                value = item.getString();
                break;
            }
        }
        return value;
    }
    
    public String[] getParameterValues(String name) {
        if (this.items==null) items = new ArrayList<String>();
        String values[] = new String[0];
        ArrayList<String> vetValues = new ArrayList<String>();
        Iterator iter = items.iterator();
        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();
            if (item.isFormField() && item.getFieldName().equals(name))
                vetValues.add(item.getString());
        }
        values = new String[vetValues.size()];
        vetValues.toArray(values);
        return values;
    }
    
    public File getFile(HttpServletRequest req,String name) throws IOException  {
        
        File arquivo = null;
        try {            
            Iterator iter = items.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField() && item.getFieldName().equals(name)) {
                    
                    String nomArquivo = (new File(item.getName())).getName();
                    String tmpDir = System.getProperty("java.io.tmpdir");                                        
                    arquivo = new File(tmpDir, nomArquivo);
                    arquivo.createNewFile();
                    item.write(arquivo);
                    
                    item.delete();
                    break;
                }
            }
        } catch (Exception e) {
        	setMsgErro("Erro ao buscar o arquivo :"+e.getMessage());        	
        	/*throw new IOException ("UploadFile: "+e.getMessage());*/
        }
        return arquivo;
    }

	public String getMsgErro() {
		return this.sMsgErro;
	}

	public void setMsgErro(String sErro) {
		if (sErro==null) sErro="";
		this.sMsgErro = sErro;
	}
    
    
}