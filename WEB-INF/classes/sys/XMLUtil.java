package sys;

import java.util.Hashtable;
import java.util.Iterator;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

public class XMLUtil {
    
    public static Hashtable readInitParams(String xmlFile, String servletName) {
        
        Hashtable params = null;
        try {
            params = new Hashtable();                                                

            SAXBuilder builder = new SAXBuilder();
            Document doc = builder.build(xmlFile);
            
            Element webApp = doc.getRootElement();
            Namespace ns = Namespace.getNamespace("http://java.sun.com/xml/ns/j2ee");
            Element servlet = webApp.getChild("servlet", ns);
            
            Iterator i = servlet.getChildren("servlet-name", ns).iterator();
            while (i.hasNext()) {
                Element e1 = (Element) i.next();
                if (e1.getText().equals(servletName)) {
                    Iterator j = servlet.getChildren("init-param", ns).iterator();
                    while (j.hasNext()) {
                    
                        Element e2 = (Element) j.next();
                        String paramName = e2.getChildText("param-name", ns);
                        String paramValue = e2.getChildText("param-value", ns);
                        params.put(paramName, paramValue);
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return params;
    }
    
    public static String readInitParam(String xmlFile, String servletName, String paramName) {
        Hashtable params = readInitParams(xmlFile, servletName);
        return (String) params.get(paramName);
    }

}