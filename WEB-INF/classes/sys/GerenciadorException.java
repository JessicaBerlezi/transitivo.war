package sys;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GerenciadorException {

	private Integer idLogException;
	private String codSistema;
	private String nomException;
	private String nomPrograma;
	private String nomSistema;
	private String nomOrgao;
	private String dataExceptionFormatada;
	private String txtException;
	private String txtCaminho;
	private String nomUsername;
	private String codOrgaoAtuacao;
	private String emlDestino;
	private Date datException;

	public GerenciadorException() {
		idLogException  = 0 ;
		codSistema      = "";
		nomException    = "";
		nomPrograma     = "";
		nomSistema      = "";
		nomOrgao        = "";
		datException    = new Date();
		dataExceptionFormatada = Util.formatedTodayDDMMAAAA().substring(0,10);
		txtException    = "";
		txtCaminho      = "";
		nomUsername     = "";
		codOrgaoAtuacao = "";
		emlDestino      = "";
	}

	public String getCodOrgaoAtuacao() {
		return codOrgaoAtuacao;
	}
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao) {
		this.codOrgaoAtuacao = codOrgaoAtuacao;
	}

	public String getCodSistema() {
		return codSistema;
	}
	public void setCodSistema(String codSistema) {
		this.codSistema = codSistema;
	}

	public Date getDatException() {
		return datException;
	}
	public void setDatException(Date datException) {
		this.datException = datException;
	}

	public String getDataExceptionFormatada() {
		return dataExceptionFormatada;
	}
	public void setDataExceptionFormatada(String dataExceptionFormatada) {
		this.dataExceptionFormatada = dataExceptionFormatada;
	}

	public String getEmlDestino() {
		return emlDestino;
	}
	public void setEmlDestino(String emlDestino) {
		this.emlDestino = emlDestino;
	}

	public String getNomException() {
		return nomException;
	}
	public void setNomException(String nomException) {
		this.nomException = nomException;
	}

	public String getNomUsername() {
		return nomUsername;
	}
	public void setNomUsername(String nomUsername) {
		this.nomUsername = nomUsername;
	}

	public String getTxtCaminho() {
		return txtCaminho;
	}
	public void setTxtCaminho(String txtCaminho) {
		this.txtCaminho = txtCaminho;
	}

	public String getTxtException() {
		return txtException;
	}
	public void setTxtException(String txtException) {
		this.txtException = txtException;
	}

	public String getNomOrgao() {
		return nomOrgao;
	}
	public void setNomOrgao(String nomOrgao) {
		this.nomOrgao = nomOrgao;
	}

	public String getNomPrograma() {
		return nomPrograma;
	}
	public void setNomPrograma(String nomPrograma) {
		this.nomPrograma = nomPrograma;
	}

	public String getNomSistema() {
		return nomSistema;
	}
	public void setNomSistema(String nomSistema) {
		this.nomSistema = nomSistema;
	}

	public Integer getIdLogException() {
		return idLogException;
	}
	public void setIdLogException(Integer idLogException) {
		this.idLogException = idLogException;
	}

	public static sys.GerenciadorException registraException(Exception exception, HttpServletRequest req) {
		GerenciadorException command = new GerenciadorException();
		try{
			//cria os Beans, se n�o existir
			HttpSession session   = req.getSession() ;

			ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId");
			if (UsrLogado==null) UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.SistemaBean SistemaBeanId     = (ACSS.SistemaBean)session.getAttribute("SistemaBeanId") ;
			if (SistemaBeanId==null) SistemaBeanId = new ACSS.SistemaBean();
			
			
			
			ACSS.OrgaoBean OrgaoBeanId = new ACSS.OrgaoBean();
			
			//obtem e valida os parametros recebidos
			String sigFuncao           = req.getParameter("j_sigFuncao");
			if(sigFuncao==null)       sigFuncao =" ";

			String usuario           = UsrLogado.getNomUserName();
			String codSistema		 = SistemaBeanId.getCodSistema();
			String codOrgaoAtuacao		 = UsrLogado.getCodOrgaoAtuacao();
			SistemaBeanId.setCodSistema(codSistema);
			String sistema = SistemaBeanId.getNomAbrev();
			OrgaoBeanId.Le_Orgao(codOrgaoAtuacao, 0);
			OrgaoBeanId.setCodOrgao(codOrgaoAtuacao);
			String orgaoAtuacao = OrgaoBeanId.getSigOrgao();
			Throwable causa = exception;
			String nomException = "";
			String emlDestino = "";
			String emlRemetente = "";
			String assuntoException = "";
			String txtEmailException = "";
			String txtException = "";
			StringBuffer caminhoException = new StringBuffer();
			StringBuffer conteudoEmail = new StringBuffer();
			String formato = "dd/MM/yyyy HH:mm:ss";
			SimpleDateFormat df = new SimpleDateFormat(formato);
			Vector<String> caminho = new Vector<String> ();
			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			emlDestino = ParamSistemaBeanId.getParamSist("EMAIL_ADMINISTRADOR_EXCEPTION");
			emlRemetente = ParamSistemaBeanId.getParamSist("REMETENTE_EMAIL_EXCEPTION");
			assuntoException = ParamSistemaBeanId.getParamSist("ASSUNTO_EXCEPTION");
			txtEmailException = ParamSistemaBeanId.getParamSist("TEXTO_EMAIL_EXCEPTION");
			txtException = causa.getLocalizedMessage();
			while(null != causa) {
				String txtCausaName = causa.getClass().getName();
				if (txtCausaName==null) txtCausaName="";
				caminho.add(txtCausaName + ": ");
				nomException = txtCausaName;
				causa = causa.getCause();
			}
			for (int i=caminho.size()-1; i>=0; i--)
				caminhoException.append(caminho.get(i));

			Date dataHoje = new Date();
			String dataException = df.format(dataHoje);
			
			String txtCaminho = caminhoException.toString().substring(0, caminhoException.toString().length()-2);

			conteudoEmail.append(txtEmailException + "<br><br>");
			conteudoEmail.append("Sistema: " + sistema + "<br>");
			conteudoEmail.append("Fun&ccedil�o: " + sigFuncao + "<br>");
			conteudoEmail.append("Usu�rio: " + usuario + " / " + orgaoAtuacao + "<br>");
			conteudoEmail.append("Data Hora: " + dataException + "<br>");
			conteudoEmail.append("Exception: " + nomException + "<br>");
			conteudoEmail.append("Mensagem: " + txtException + "<br><br>");
			conteudoEmail.append("Caminho: " + txtCaminho + "<br>");
			command.setCodSistema(codSistema);
			command.setNomException(nomException);
			command.setNomUsername(usuario);
			command.setCodOrgaoAtuacao(codOrgaoAtuacao);
			command.setEmlDestino(emlDestino);
			command.setTxtException(txtException);
			command.setTxtCaminho(txtCaminho);
			command.setNomPrograma(sigFuncao);
			command.setDatException(dataHoje);
			command.setNomSistema(sistema);
			command.setNomOrgao(orgaoAtuacao);
			command.setDataExceptionFormatada(dataException);
			sys.Dao dao = sys.Dao.getInstance();
			dao.InsertLogException(command);
			conteudoEmail.append("Ocorr�ncia n�: " + command.getIdLogException());

			//Envia e-mail comunicando exception
			if (!emlDestino.equals(""))
			{
				EmailBean email = new EmailBean();
				email.setRemetente(emlRemetente);
				email.setDestinatario(emlDestino);
				email.setAssunto(assuntoException);
				email.setConteudoEmail(conteudoEmail.toString());
				email.setIndPrioridade(EmailBean.IndPrioridade.ALTA);
				email.setTipoEmail(EmailBean.TipoEmail.HTML);
				ServiceLocator.getInstance().sendEmail(email);
			}
		}
		catch (Exception e) {
		}
		req.setAttribute("GerenciadorExceptionId", command);
		return command;
	}

	public static void registraExceptionRobo(Exception exception, String nomPrograma, String nomSistema) {
		try{
			GerenciadorException command = new GerenciadorException();

			//cria os Beans, se n�o existir
			ACSS.SistemaBean SistemaBeanId     = new ACSS.SistemaBean();

			//obtem e valida os parametros recebidos
			SistemaBeanId.setNomAbrev(nomSistema);
			SistemaBeanId.Le_Sistema("", 1);
			String sistema = SistemaBeanId.getNomAbrev();
			String codSistema = SistemaBeanId.getCodSistema();

			Throwable causa = exception;
			String nomException = "";
			String emlDestino = "";
			String emlRemetente = "";
			String assuntoException = "";
			String txtEmailException = "";
			String txtException = "";
			StringBuffer caminhoException = new StringBuffer();
			StringBuffer conteudoEmail = new StringBuffer();
			String formato = "dd/MM/yyyy HH:mm:ss";
			SimpleDateFormat df = new SimpleDateFormat(formato);
			Vector<String>  caminho = new Vector<String> ();

			ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			ParamSistemaBeanId.PreparaParam();
			emlDestino = ParamSistemaBeanId.getParamSist("EMAIL_ADMINISTRADOR_EXCEPTION");
			emlRemetente = ParamSistemaBeanId.getParamSist("REMETENTE_EMAIL_EXCEPTION");
			assuntoException = ParamSistemaBeanId.getParamSist("ASSUNTO_EXCEPTION");
			txtEmailException = ParamSistemaBeanId.getParamSist("TEXTO_EMAIL_EXCEPTION");

			txtException = causa.getLocalizedMessage();

			while(null != causa) {
				caminho.add(causa.getClass().getName() + ": ");
				nomException = causa.getClass().getName();
				causa = causa.getCause();
			}

			for (int i=caminho.size()-1; i>=0; i--)
				caminhoException.append(caminho.get(i));

			Date dataHoje = new Date();
			String dataEmail = df.format(dataHoje);

			String txtCaminho = caminhoException.toString().substring(0, caminhoException.toString().length()-2);

			conteudoEmail.append(txtEmailException + "<br><br>");
			conteudoEmail.append("Sistema: " + sistema + "<br>");
			conteudoEmail.append("Fun&ccedil�o: " + nomPrograma + "<br>");
			conteudoEmail.append("Data Hora: " + dataEmail + "<br>");
			conteudoEmail.append("Exception: " + nomException + "<br>");
			conteudoEmail.append("Mensagem: " + txtException + "<br><br>");
			conteudoEmail.append("Caminho: " + txtCaminho);

			command.setCodSistema(codSistema);
			command.setNomException(nomException);
			command.setEmlDestino(emlDestino);
			command.setTxtException(txtException);
			command.setTxtCaminho(txtCaminho);
			command.setNomPrograma(nomPrograma);
			command.setDatException(dataHoje);

			sys.Dao dao = sys.Dao.getInstance();
			dao.InsertLogException(command);

			//Envia e-mail comunicando exception
			EmailBean email = new EmailBean();
			email.setRemetente(emlRemetente);
			email.setDestinatario(emlDestino);
			email.setAssunto(assuntoException);
			email.setConteudoEmail(conteudoEmail.toString());
			email.setIndPrioridade(EmailBean.IndPrioridade.ALTA);
			email.setTipoEmail(EmailBean.TipoEmail.HTML);
			ServiceLocator.getInstance().sendEmail(email);
		}
		catch (Exception e) {
		}
	}

}