package sys;

import javax.servlet.http.HttpServletRequest;

public class NullCommand extends Command {
  private String next;

  public NullCommand() {
    next             = "" ;
  }

  public NullCommand(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req)
    throws CommandException {
    String j_jspDestino = req.getParameter("j_jspDestino");
    if( j_jspDestino == null) j_jspDestino =" ";	
	if (j_jspDestino.indexOf(".jsp")>0) next = j_jspDestino ;	
    return next;
  }
  public void setNext(String next){ 
  	  this.next= next ;
	  return; 
  }
}
