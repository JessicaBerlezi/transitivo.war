package sys;

import java.util.ArrayList;
import java.util.Vector;

/**
* <b>Title:</b>      Recurso - Manuten��o Tabela<br>
* <b>Description:</b>Bean do Contudo dos E-mail<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    Itc - Informatica Tecnologica e Cientifica<br>
* @author Sergio Junior
* @version 1.0
*/

public class ConteudoEmailBean extends HtmlPopupBean{
	
	private String conteudoEmail;
	private String codOrgao;
	private String codTipo;
	private String dscTipo;
	private String txtAssunto;
	private String nomRemetente;
	

	public ConteudoEmailBean()  {
		super();
		setTabela("TSMI_EMAIL_EVENTO");	
		setPopupWidth(35);
		conteudoEmail = "";
		codOrgao = "0";	
		codTipo = "0";
		txtAssunto = "";
		nomRemetente = "";	
	}
	
	public void setCodTipo (String codTipo){
		this.codTipo = codTipo;
		if (codTipo==null) this.codTipo="0" ;
	}
	public String getCodTipo (){
		return this.codTipo;
	}
	
	public void setTxtAssunto (String txtAssunto){
		this.txtAssunto = txtAssunto;
		if (txtAssunto==null) this.txtAssunto="" ;
	}
	public String getTxtAssunto (){
		return this.txtAssunto;
	}
	
	public void setDscTipo(String dscTipo){
		this.dscTipo = dscTipo;
		if (dscTipo==null) this.dscTipo="" ;
	}
	public String getDscTipo(){
		return this.dscTipo;
	}
	
	public void setNomRemetente (String nomRemetente){
		this.nomRemetente = nomRemetente;
		if (nomRemetente==null) this.nomRemetente="" ;
	}
	public String getNomRemetente (){
		return this.nomRemetente;
	}
	
	public void setConteudoEmail(String conteudoEmail){
		this.conteudoEmail = conteudoEmail;
	}
	public String getConteudoEmail(){
		return conteudoEmail;
	}

	public void setCodOrgao(String codOrgao){
		this.codOrgao = codOrgao;
	}
	public String getCodOrgao(){
		return codOrgao;
	}
	
	public void setMessage(String mensagem){
		Vector message = new Vector();
		message.add(mensagem);
		setMsgErro(message);
	}
	

	public ConteudoEmailBean consultaConteudo(){
		ArrayList listEmail = new ArrayList();
		try { 
			Dao dao = Dao.getInstance();
			listEmail = dao.consultaConteudoEmail(
			                new String[] {"COD_ORGAO", "COD_TIPO"}, 
			                new String[] {codOrgao,codTipo});
		}
		catch(Exception e){
			System.err.println(e.getMessage());
		}
		if (listEmail.size()== 0)
		   return null;
		   
		return (ConteudoEmailBean)listEmail.remove(0);
	}
	
	public boolean alteraConteudo(String conteudoEmail, String nomRemetente, String txtAssunto){
		boolean retorno = true;
		try { 
			Dao dao = Dao.getInstance();
			dao.alteraConteudoEmail(
			    new String[] {"COD_ORGAO", "COD_TIPO"},
			    new String[] {codOrgao,codTipo}, conteudoEmail,nomRemetente,txtAssunto);
		}
		catch(Exception e){
			Vector vErro = new Vector();
			vErro.add("Conteudo n�o inclu�do. Procure o Administrador do Sistema");
			vErro.add("Erro: " + e.getMessage());
			setMsgErro(vErro);
			retorno = false;
		}
		this.conteudoEmail = conteudoEmail;
		this.nomRemetente = nomRemetente;
		this.txtAssunto = txtAssunto;
		return retorno;
	}
	
	public boolean insereConteudo(String conteudoEmail,String nomRemetente, String txtAssunto){
		boolean retorno = true;
		try { 
			Dao dao = Dao.getInstance();
			dao.insereConteudoEmail(new String[] {codOrgao,codTipo,conteudoEmail, 
				 nomRemetente, txtAssunto });
		}
		catch(Exception e){
			Vector vErro = new Vector();
			vErro.add("Conteudo n�o inclu�do. Procure o Administrador do Sistema");
			vErro.add("Erro: " + e.getMessage());
			ConteudoEmailBean bean = new ConteudoEmailBean();
			setMsgErro(vErro);
			retorno = false;
		}
		this.conteudoEmail = conteudoEmail;
		this.nomRemetente = nomRemetente;
		this.txtAssunto = txtAssunto;
		return retorno;
	}
	
	public void Le_Tipo(String tpOrg, int pos)  {
		 try { 
			 Dao dao = Dao.getInstance();
			 if (pos==0) this.codTipo=tpOrg;		 	
			 else 	    this.dscTipo=tpOrg;	
		     if ( dao.TipoLeBean(this, pos) == false) {
				 this.codTipo	  = "";
				 setPkid("0") ;		
			 }
		 }
		 catch (Exception e) {
				this.codOrgao = "" ;
				setPkid("0") ;		        			
				System.err.println("ConteudoEmail001: " + e.getMessage());
		 }
	  }
}
