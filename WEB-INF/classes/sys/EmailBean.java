/*
 * Created on 04/08/2004
 *
 */
package sys;

import java.awt.List;

/**
 *
 */
public class EmailBean {
	
	public enum IndPrioridade { ALTA, NORMAL }
	private IndPrioridade indPrioridade;
	
	public enum TipoEmail { TEXTO, HTML }
	private TipoEmail tipoEmail;
	
	protected String conteudoEmail;
	private String assunto;
	private String remetente;
	private String destinatario;
	private List arquivos;
	
	public EmailBean(){
		conteudoEmail = "";
		assunto = "Sem Assunto";
		remetente = "";	
		arquivos = null;
	}

	
	public void setAssunto (String assunto){
		this.assunto = assunto;
		if (assunto==null) this.assunto="" ;
	}
	public String getAssunto (){
		return this.assunto;
	}
	
	public void setRemetente (String remetente){
		this.remetente = remetente;
		if (remetente==null) this.remetente="" ;
	}
	public String getRemetente(){
		return this.remetente;
	}
	
	public void setConteudoEmail(String conteudoEmail){
		this.conteudoEmail = conteudoEmail;
	}
	public String getConteudoEmail(){
		return conteudoEmail;
	}

	
	public void setDestinatario(String destinatario){
		this.destinatario = destinatario;
	}
	
	public String getDestinatario(){
		return destinatario;
	}
	
	public void addArquivo(String caminho){
		if( arquivos == null)
			arquivos = new List();
		arquivos.add(caminho);
	}
	
	public void setArquivos(List arquivos){
		this.arquivos = arquivos;
	}
	
	public List getArquivos(){
		return arquivos;
	}
	
	protected boolean verificaEmail(){
		if(destinatario.compareTo("") == 0)
			return false;
		if( conteudoEmail.compareTo("") == 0)
		   	return false; 
		return true;
	}
	
	public void sendEmail() throws Exception{
		ServiceLocator Serviceloc = ServiceLocator.getInstance();
		Serviceloc.sendEmail(this);
	}
	
	public void sendEmail(String login, String senha) throws Exception{
		ServiceLocator Serviceloc = ServiceLocator.getInstance();
		Serviceloc.sendEmail(this,login,senha);		
	}	
	
	public void sendEmailHtml() throws Exception{
		ServiceLocator Serviceloc = ServiceLocator.getInstance();
		Serviceloc.sendEmailHtml(this);
	}

	public void sendEmailHtml(String login, String senha) throws Exception{
		ServiceLocator Serviceloc = ServiceLocator.getInstance();
		Serviceloc.sendEmailHtml(this,login, senha);
	}
	public TipoEmail getTipoEmail() {
		return tipoEmail;
	}
	public void setTipoEmail(TipoEmail tipoEmail) {
		this.tipoEmail = tipoEmail;
	}
	
	public void setIndPrioridade(IndPrioridade indPrioridade) {
		this.indPrioridade = indPrioridade;
	}
	public IndPrioridade getIndPrioridade() {
		return indPrioridade;
	}
}
