package sys;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import REC.AutoInfracaoBean;
import REC.InfracaoBean;
import REC.ResponsavelBean;
import REC.VeiculoBean;

import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import ACSS.UsuarioFuncBean;

/**
 * @author sjunior
 */

public class DownloadServlet extends HttpServlet{
	private HashMap <String, String> tipos = null;
	public static final String JSPDIR   = "/WEB-INF/jsp/";
	public final static String JSPCONSTRUCAO = "/sys/Construcao.jsp";
	
	private String nomArquivo;
	private String sTexto="";
	private String contexto;
	private String tpDownload;
	private ParamSistemaBean paramSys;
	private String tipoDocumento;
	private File file;
	private UsuarioBean usrLogado;
	byte[] arquivo = null;
	
	public void init() throws ServletException {
		super.init();
		loadTipos();
	}
	
	public void loadTipos() { 
		//Lista os tipos de arquivo suportados
		tipos = new HashMap<String, String>();
		//Tipos espec�ficos do SMIT
		tipos.put(".grv",         "text/plain");
		tipos.put(".rej",         "text/plain");
		tipos.put(".ret",         "text/plain");
		tipos.put(".upl",         "text/plain"); 
		tipos.put(".bkp",         "text/plain");
		//Tipos padr�o W3C
		tipos.put(".txt",         "text/plain");      
		tipos.put(".html",        "text/html");        
		tipos.put(".htm",         "text/html");        
		tipos.put(".shtm",        "text/html");        
		tipos.put(".shtml",       "text/html");        
		tipos.put(".rtf",         "application/rtf");      
		tipos.put(".pdf",         "application/pdf");      
		tipos.put(".zip",         "application/zip");      
		tipos.put(".tar",         "application/x-tar");        
		tipos.put(".tgz",         "application/x-tar");        
		tipos.put(".gz",          "application/x-gzip");       
		tipos.put(".gtar",        "application/x-gtar");       
		tipos.put(".sh",          "application/x-sh");         
		tipos.put(".csh",         "application/x-csh");        
		tipos.put(".bin",         "application/octet-stream");         
		tipos.put(".class",       "application/octet-stream");         
		tipos.put(".exe",         "application/octet-stream");         
		tipos.put(".sea",         "application/octet-stream");         
		tipos.put(".hqx",         "application/mac-binhex40");         
		tipos.put(".doc",         "application/msword");
		tipos.put(".xls",         "application/x-excel");
		tipos.put(".dot",         "application/msword");       
		tipos.put(".pot",         "application/mspowerpoint");         
		tipos.put(".pps",         "application/mspowerpoint");         
		tipos.put(".ppt",         "application/mspowerpoint");         
		tipos.put(".ppz",         "application/mspowerpoint");         
		tipos.put(".ai",          "application/postscript");       
		tipos.put(".eps",         "application/postscript");       
		tipos.put(".ps",          "application/postscript");       
		tipos.put(".js",          "application/x-javascript");         
		tipos.put(".swf",         "application/x-shockwave-flash");        
		tipos.put(".sit",         "application/x-stuffit");        
		tipos.put(".tcl",         "application/x-tcl");        
		tipos.put(".bmp",         "image/bmp");        
		tipos.put(".gif",         "image/gif");        
		tipos.put(".jpe",         "image/jpeg");       
		tipos.put(".jpeg",        "image/jpeg");       
		tipos.put(".jpg",         "image/jpeg");       
		tipos.put(".png",         "image/png");        
		tipos.put(".tif",         "image/tiff");       
		tipos.put(".tiff",        "image/tiff");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		execute(request, response);
	}
	
	protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			getParametros(request);
			
			if ((nomArquivo.trim().length()==0) || (contexto.trim().length()==0)){
				return;
			}
			sTexto+="nomArquivo:"+nomArquivo+" contexto:"+contexto+" tpDownload:"+tpDownload;
			
			verificaTpDownload(response);
			response.setDateHeader("Expires",0);
			sTexto+="DownloadServlet Etapa (1)\n";
			
			if (paramSys == null) {
				setResponseError(request, response, "Par�metros por Sistema inexistente na sess�o !");
				return;
			}
			sTexto+=" paramSys.getParamSist(contexto): "+paramSys.getParamSist(contexto);
			
			//Altera��o realizada para Download dos arquivos de Defesa Previa via Web
			if (nomArquivo.equals("DOC_WEB")){
				
				AutoInfracaoBean autoInfracaoBeanId = (AutoInfracaoBean)request.getAttribute("AutoInfracaoBeanId");
				if (autoInfracaoBeanId == null){
					autoInfracaoBeanId = new AutoInfracaoBean();
			  		autoInfracaoBeanBuilder(request, autoInfracaoBeanId);
				}
				file = new File (autoInfracaoBeanId.caminhoArquivoDocumento(this.paramSys, tipoDocumento));
				
				if (!file.exists()) {
					setResponseError(request, response, "Houve um problema na localiza��o do Arquivo: "+file.getPath());
					return;
				}
				
				try {
		            arquivo = fileToByte(file);  
		        } catch (Exception e) {  
		            throw new ServletException(e);  
		        }
				
		        response.setContentType("application/pdf");  
		        response.setContentLength(arquivo.length);  
		        response.getOutputStream().write(arquivo, 0, arquivo.length);  
		        response.getOutputStream().flush();  
		        response.getOutputStream().close();
				
			}else{
				file = new File(paramSys.getParamSist(contexto) + "/" + nomArquivo);
				
				if (!file.exists()) {
					setResponseError(request, response, "Houve um problema na localiza��o do Arquivo: "+file.getPath());
					return;
				}
				response.setHeader("Content-Disposition", "attachment;filename=\"" + file.getName() + "\"");
				sTexto+="DownloadServlet Etapa (2)\n";
				sTexto+="  file.getName():"+file.getName();
				
				String tipo = getContentType(file.getName());
				sTexto+=" tipo: "+tipo;			
				
				if (tipo.equals("")){
					throw new ServletException(sTexto+"  Tipo de arquivo n�o reconhecido pelo SMIT !!!");
				}
				response.setContentType(tipo);
				sTexto+="DownloadServlet Etapa (3)\n";
				
				int resp;
				byte[] conteudo = new byte[1024];
				FileInputStream input = new FileInputStream(file);
				sTexto+="DownloadServlet Etapa (4)\n";			
				
				while((resp = input.read(conteudo, 0, conteudo.length)) != -1){
					response.getOutputStream().write(conteudo, 0, resp);
				}
				input.close();
				sTexto+="DownloadServlet Etapa (5)\n";			

				response.getOutputStream().flush();
				sTexto+="DownloadServlet Etapa (6)\n";
				
				response.getOutputStream().close();
				sTexto+="DownloadServlet Etapa (7-Conclu�do)\n";
			}
		}
		catch (IllegalStateException e) 
		{
			request.setAttribute("javax.servlet.jsp.jspException", null);
			GerenciadorException.registraException(null, request);
			return;
			/*
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
			sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
			ConstrucaoBeanId.setMsgErro("Sess�o de Trabalho inativa !");
			ConstrucaoBeanId.setJspOrigem("DownloadServlet");
			request.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
			RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + JSPCONSTRUCAO);
			rd.forward(request, response);
			return;
			*/
	    }
		catch (Exception e) {
			if (!e.getClass().getName().endsWith("ClientAbortException")) {
				throw new ServletException(sTexto+" Erro de leitura do arquivo (SMIT): "+e.getMessage());
			}
		}               
	}

	private void autoInfracaoBeanBuilder(HttpServletRequest request, AutoInfracaoBean autoInfracaoBeanId) throws BeanException {
		if  (autoInfracaoBeanId.getProprietario()==null) autoInfracaoBeanId.setProprietario(new ResponsavelBean());
		if  (autoInfracaoBeanId.getCondutor()==null) autoInfracaoBeanId.setCondutor(new ResponsavelBean());
		if  (autoInfracaoBeanId.getVeiculo()==null) autoInfracaoBeanId.setVeiculo(new VeiculoBean());
		if  (autoInfracaoBeanId.getInfracao()==null) autoInfracaoBeanId.setInfracao(new InfracaoBean());
		
		autoInfracaoBeanId.setNumAutoInfracao(request.getParameter("numAutoInfracao"));  
		autoInfracaoBeanId.setNumPlaca(request.getParameter("numPlaca"));  
		autoInfracaoBeanId.setNumProcesso(request.getParameter("numProcesso"));
		
		String numRenavam = request.getParameter("numRenavam");
		if (numRenavam==null) numRenavam="";
		autoInfracaoBeanId.setNumRenavam(numRenavam);
		String codAcessoWeb = request.getParameter("numAcesso");
		if (codAcessoWeb==null) codAcessoWeb="";
		autoInfracaoBeanId.setCodAcessoWeb(codAcessoWeb);
		
		autoInfracaoBeanId.LeAutoInfracao(usrLogado);
		autoInfracaoBeanId.LeRequerimento(usrLogado);
	}

	private void setResponseError(HttpServletRequest request, HttpServletResponse response, String msgErro) throws ServletException, IOException {
		
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		sys.ConstrucaoBean ConstrucaoBeanId = new sys.ConstrucaoBean();
//		ConstrucaoBeanId.setMsgErro("Par�metros por Sistema inexistente na sess�o !");
		ConstrucaoBeanId.setMsgErro(msgErro);
		ConstrucaoBeanId.setJspOrigem("DownloadServlet");
		request.setAttribute("ConstrucaoBeanId", ConstrucaoBeanId);
		RequestDispatcher rd = getServletContext().getRequestDispatcher(JSPDIR + JSPCONSTRUCAO);
		rd.forward(request, response);
		
	}
	
	private void getParametros(HttpServletRequest request) throws BeanException {
		
		nomArquivo = request.getParameter("nomArquivo");
		if (nomArquivo == null) nomArquivo="";

		contexto = request.getParameter("contexto");
		if (contexto == null) contexto="";

		tpDownload = request.getParameter("header");
		
		tipoDocumento = request.getParameter("tipoDocumento");
		
		paramSys = (ParamSistemaBean)request.getSession().getAttribute("ParamSistemaBeanId");
		
		if (usrLogado == null){
			usrLogado = new UsuarioBean() ;
		}
		usrLogado = (UsuarioBean)request.getSession().getAttribute("UsuarioBeanId") ;
		
	}

	private void verificaTpDownload(HttpServletResponse response) {
		if (tpDownload != null){
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache");
		}
	}
	
	private String getContentType(String filename) {
		String retorno = "";
		String extensao = filename.substring(filename.lastIndexOf(".")).toLowerCase();
		
		if (tipos == null){
			loadTipos();
		}
		
		if (tipos.containsKey(extensao)){
			retorno = (String) tipos.get(extensao);
		}
		
		return retorno;
	}
	
	public static InputStream byteToInputStream(byte[] bytes) throws Exception {  
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes);  
        return bais;  
    }  
  
    public static byte[] fileToByte(File imagem) throws Exception {  
        FileInputStream fis = new FileInputStream(imagem);  
        ByteArrayOutputStream baos = new ByteArrayOutputStream();  
        byte[] buffer = new byte[8192];  
        int bytesRead = 0;  
        while ((bytesRead = fis.read(buffer, 0, 8192)) != -1) {  
            baos.write(buffer, 0, bytesRead);  
        }  
        return baos.toByteArray();  
    }  
}
