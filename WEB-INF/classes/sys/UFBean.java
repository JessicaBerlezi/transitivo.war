package sys;



/**
* <b>Title:</b>        	SMIT - Bean de UF<br>
* <b>Description:</b>  	Bean dados de Endereco - Tabela de UF<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Wellem Lyra
* @version 				1.0
*/

public class UFBean extends sys.HtmlPopupBean { 

	private String nomUF;
	private String codUF;
       
	public UFBean()	{
	
	  	super();
	  	setTabela("TSMI_ENDERECO");	
	  	setPopupWidth(5);
		nomUF   = "";
		codUF       = "";
	}
    
	public void setNomUF(String nomUF)  {
		this.nomUF=nomUF ;
		if (nomUF==null) this.nomUF="" ;
	}  
	public String getNomUF()  {
		return this.nomUF;
	}

	public void setCodUF(String codUF)  {
		this.codUF=codUF ;
		if (codUF==null) this.codUF="" ;
	}  
	public String getCodUF()  {
		return this.codUF;
	}
}
