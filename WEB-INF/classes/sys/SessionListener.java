package sys;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Classe usada para gerenciar os eventos que possam ocorrer na cria��o 
 * e destrui��o das sess�es do SMIT
 * 
 * @author Glaucio Jannotti
 */

public class SessionListener extends HttpServlet implements HttpSessionListener {
    
    public void sessionCreated(HttpSessionEvent se) {
        try  {
			ACSS.Dao.getInstance().TcauAbreSessaoInsert(se.getSession().getId());
		}
		catch (Exception e) {
		   new DaoException("SessionListener: " + e.getMessage());
		}

    }
    
    public void sessionDestroyed(HttpSessionEvent se) {
    	try{
    		ACSS.Dao.getInstance().TcauFechaSessaoUpdate(se.getSession().getId());
    	}
    	catch (Exception e) {
 		   new DaoException("SessionListener: " + e.getMessage());
 		}
    	
    }

}