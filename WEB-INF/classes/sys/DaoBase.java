package sys;

public class DaoBase {
  
	public int posIni;
	
//**************************************************************************************
  public DaoBase() throws DaoException {
	posIni = 0;
  }  
	public void setPosIni(int posIni) {
		this.posIni=posIni;
	}
	public int getPosIni() {
		return this.posIni ;
	}
	
	public String getPedaco(String origem,int tam,String dirpad,String carpad) {
				
		if (carpad==null) carpad=" ";
		if (dirpad==null) dirpad="R";
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		if ((result.length()<tam) && (" ".equals(dirpad)==false)) {
			if (dirpad.equals("R")) result=sys.Util.rPad(result,carpad,tam);
			else result=sys.Util.lPad(result,carpad,tam);
		} 
		this.posIni+=tam;		
		return result ;
	}
	public String getPedacoNum(String origem,int tam,int dec,String ptdec) {
				
		if (dec>tam)  		   dec = 0;
		if (ptdec==null)  		ptdec=".";		
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		result = sys.Util.SemZeros(result) ;
		if (sys.Util.isNumber(result)) 	{
			if (result.length()<(dec+1))	{
				if (result.length()<(dec)) result=sys.Util.rPad(result,"0",dec) ;
				if (dec>0)		result="0"+result;
			}
			// colocar ponto decimal
			if (dec>0) result=result.substring(0,result.length()-dec)+ptdec+result.substring(result.length()-dec,result.length());
		}
		else result = null;	
		this.posIni+=tam;
		return result ;
	}
	public String getPedacoDt(String origem,int tam) {			
		
		String result = "" ;
		if (origem.length()>=(posIni+tam)) 	{
			result = origem.substring(posIni,(posIni+tam));
		}
		result=sys.Util.lTrim(result.trim());
		if ((result.length()<8) && (result.length() > 0)) {
			result=sys.Util.rPad(result," ",8); 
			result = result.substring(6,8)+"/"+ result.substring(4,6)+"/"+result.substring(0,4);
		}
		this.posIni+=tam;				
		return result ;
	}
	
}
    
  
