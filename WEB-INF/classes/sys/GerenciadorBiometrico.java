package sys;

public class GerenciadorBiometrico {

  private static GerenciadorBiometrico instance = null;

  public static GerenciadorBiometrico getInstance() {
	if (instance == null)
		instance = new GerenciadorBiometrico();    
	return instance;
  }

  private native boolean verifyMatch(String password, String biometria);

  private GerenciadorBiometrico() {
	System.loadLibrary("SecuJNI");
  }

  private boolean verificaAutenticidade(String password, String biometria) {
    boolean res = false;
    try {
      res = verifyMatch(password, biometria);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    return res;
  }
  
  public boolean ValidaBiometria(ACSS.UsuarioBean UserID, String password)
  throws sys.BeanException 
  {
	  try {
			  Dao dao = Dao.getInstance();
			  String biometria = dao.ValidaBiometria(UserID);
			  if (biometria != null) {
				  if (this.verificaAutenticidade(password, biometria)) {
					  //Usuario autenticado!
					  return true;
				  }
				  else {
					  //"Usuario nao autenticado!
					  return false;
				  }
			  }
			  else {
				  //Usuario nao cadastrado!
				  return false;
			  }
		  } 
		  catch (Exception e) {
			  throw new sys.BeanException(e.getMessage());
		  } 
  }
	
  public void CadastraBiometria(ACSS.UsuarioBean UserID, String password)
  throws sys.BeanException 
  {
	  try {
		  Dao dao = Dao.getInstance();
		  dao.CadastraBiometria(UserID, password);
	  }
	  catch (Exception e) {
		  throw new sys.BeanException(e.getMessage());
	  } 		
  }
  
}