package sys;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;

import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

public class CodigoBarra {
    
    public static boolean criaImagem(String texto,HttpServletResponse response,int h) 
        throws ServletException,IOException {
        
    	ServletOutputStream out = null;
    	try
    	{
        int i, j, tam;
        //Variavel que contem a representacao em Codigo de Barras de cada um dos numeros.
        //Nessa representacao:
        // 0 significa Barra Fina (NARROW);
        // 1 significa Barra Grossa (WIDE).
        String[] barras = 
                {"00110", //0
                "10001", //1
                "01001", //2
                "11000", //3
                "00101", //4
                "10100", //5
                "01100", //6
                "00011", //7
                "10010", //8
        "01010"};//9
        //O Codigo de barras e formado sempre por pares intercalados.
        //Por exemplo 12:
        //Pegando-se a representacao do 1 e do 2 na variavel acima teriamos o seguinte: 
        //1001000011.
        //Com isso, o primeiro numero representa as Barras Pretas (Fina ou Grossa)
        // e o Segundo numero representa as Barras Brancas, ou espacos, (Fina ou Grossa).
        int preto,branco;
        int w = 9 + (9 * texto.length());
        out = response.getOutputStream();
        BufferedImage image = new BufferedImage(w,h,BufferedImage.TYPE_INT_RGB);
        Graphics g = image.getGraphics();
        tam = 0;
        g.setColor(Color.white);
        g.fillRect(0, 0, w, h); //come�o do Codigo de Barras = 0 = 00 / 1 = 00
        g.setColor(Color.black);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        g.setColor(Color.white);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        g.setColor(Color.black);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        g.setColor(Color.white);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        //Conteudo do Codigo de Barras
        for(i = 0; i <= texto.length()-1; i++) {
            preto = Integer.parseInt(String.valueOf(texto.charAt(i)));
            branco = Integer.parseInt(String.valueOf(texto.charAt(i+1)));
            i++;
            for(j = 0; j < 5; j++) {
                g.setColor(Color.black);
                if(String.valueOf(barras[preto].charAt(j)).equals("0")) {
                    g.fillRect(tam, 0, tam + 1, h);
                    tam++;
                }
                else {
                    g.fillRect(tam, 0, tam + 3, h);
                    tam+=3;
                }   
                g.setColor(Color.white);
                if(String.valueOf(barras[branco].charAt(j)).equals("0")) { 
                    g.fillRect(tam, 0, tam + 1, h);
                    tam++; 
                }
                else { 
                    g.fillRect(tam, 0, tam + 3, h); 
                    tam += 3; 
                }
            }
        }        
        //fim da barra = 0 = 10 / 1 = 0
        g.setColor(Color.black);
        g.fillRect(tam, 0, tam+3, h);
        tam+=3;
        g.setColor(Color.white);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        g.setColor(Color.black);
        g.fillRect(tam, 0, tam+1, h);
        tam++;
        JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
        encoder.encode(image);
    	}
    	catch (Exception e) 
    	{
    		System.out.println("criaImagem texto (SMIT):("+texto+") altura: ("+h+") "+e.getMessage());
    	}
    	finally
    	{
            out.close();    		
    	}
        return true; 
    }
    
    public static BufferedImage getImage(Barcode barcode) {
        
        BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
        Graphics2D g = bi.createGraphics();
        barcode.draw(g, 0, 0);
        bi.flush();
        return bi;
    }
    
    public static boolean drawingBarcodeDirectToGraphics(String texto,HttpServletResponse response,int h,int flag)
    throws BarcodeException,ServletException,IOException {
    	int etapa=0;
    	ServletOutputStream out = null;
    	try
    	{
    		out = response.getOutputStream();
    		
    		etapa=1;
    		
    		Barcode barcode = BarcodeFactory.createCode128B(texto);
    		
    		etapa=2;
    		
    		for (int i=0;i<1000;i++) {}    		
    		
    		barcode.setBarHeight((double)h);
    		
    		etapa=3;
    		
    		boolean bFlag=true;
    		if (flag==0)
    			bFlag=false;		
    		barcode.setDrawingText(bFlag);
    		
    		etapa=4;
    		
    		BufferedImage image = getImage(barcode);
    		
    		for (int i=0;i<1000;i++) {}
    		
    		if (barcode==null) return true;
    		if (image==null)   return true;
    		
    		etapa=5;
    		
    		JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(out);
    		
    		etapa=6;
    		
    		for (int i=0;i<1000;i++) {}
    		
    		encoder.encode(image);
    		
    		etapa=7;
    		
    	}
    	catch (Exception e) 
    	{
    		System.out.println("drawingBarcode etapa (SMIT): "+etapa+" texto :("+texto+") altura: ("+h+") "+e.getMessage());
    	}    	
    	finally
    	{
            out.close();    		
    	}
    	
    	
    	
    	return true;
    }
}