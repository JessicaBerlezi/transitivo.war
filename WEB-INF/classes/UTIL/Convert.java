package UTIL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public abstract class Convert {
	public static JSONArray arrayListToJsonArray(List<HashMap<String, String>> arrayList){
		JSONArray jsonList = new JSONArray();
		
		try{
			for(HashMap<String, String> item : arrayList){
				JSONObject obj = new JSONObject();
				
				for(Entry<String, String> ent : item.entrySet()){					
					obj.put(ent.getKey(), ent.getValue());
				}
				
				jsonList.add(obj);
			}
		}
		catch(Exception ex){
			System.out.println("ERRO CLASSE Convert: " + ex.getMessage());
		}
		
		return jsonList;
	}
	
	public static ArrayList<HashMap<String, String>> jsonStringToArrayList(String jsonString){
		ArrayList<HashMap<String, String>> arrayList = new ArrayList<HashMap<String,String>>();
		
		try{
			JSONArray aaa = (JSONArray)new JSONParser().parse(jsonString);
			
			arrayList.addAll(aaa);
		}
		catch(Exception ex){
			System.out.println("ERRO CLASSE Convert: " + ex.getMessage());
		}
		
		return arrayList;
	}
}