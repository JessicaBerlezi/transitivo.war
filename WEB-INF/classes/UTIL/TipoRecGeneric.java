package UTIL;

/**
*<b>Resumo:</b><br>Lista todas os tipos possíveis de requisições do servlet generic ControladorRecGeneric
*@author victor
*@version 1.0
 */
public final class TipoRecGeneric
{
	public static final int MONTAR_RELATOR = 1;
	public static final int RELATORIO_ATA = 2;
	public static final int LISTA_PROCESSOS = 3;
	public static final int MONTAR_RELATOR_ABRIR_PROCESSO = 4;
	public static final int VALIDA_ABRIR_PROCESSO_REQUERIMENTO = 5;
	public static final int ATA_PUBLICACAO = 6;
	public static final int RELATORIO_ATA_PUBLICACAO = 7;
	public static final int CONSULTAR_PROCESSOS_SEM_ATA = 8;
	public static final int CONSULTAR_PROCESSOS_COM_ATA = 9;
	public static final int CONSULTAR_ATA = 10;
	public static final int ALTERAR_ATA_PROCESSO = 11;
	public static final int LISTA_AUTO_LOTE = 12;
	public static final int PROCESSA_AUTO_LOTE = 13;
	public static final int REQUERIMENTO_POR_AUTO = 14;
	public static final int GERENCIAR_ATA_CONSULTAR_ATAS = 15;
	public static final int ABRIR_PROCESSO_CONSULTAR_AUTOS = 16;
	public static final int ABRIR_PROCESSO_ABRIR_REQUERIMENTO = 17; 
	public static final int ALTERAR_DATA_REQUERIMENTO = 18;
	public static final int DELETAR_REQUERIMENTO = 19;
	public static final int INFORMAR_RESULTADO = 20;
}