package UTIL;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Classe assistente para gera��o de arquivo de Log
 *
 */
public class LogDeRecuperacao {
	
	private PrintWriter log;
	
		
	private String caminhoLogRecuperacao = "c:/logs/";
	
	public LogDeRecuperacao() {
		Date dataHoje = new Date();
		String nomeArquivo = DataUtils.getAnoString(dataHoje) + DataUtils.getMesString(dataHoje) + DataUtils.getDiaString(dataHoje);
		
		try {
			String arquivoLog = caminhoLogRecuperacao.concat(nomeArquivo + ".log");
			if(!new File(arquivoLog).exists()) {
				new File(caminhoLogRecuperacao).mkdir();
				log = new PrintWriter(new FileWriter(arquivoLog, false), true);
			}
			else{
				log = new PrintWriter(new FileWriter(arquivoLog, true), true);
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}

	/**
	 * Grava mensagem em nova linha
	 * @param mensagem
	 */
	public void gravaMensagem(String mensagem) {
		log.println(mensagem);
		log.flush();
	}
	
	/**
	 * Grava mensagem em nova linha, setando linha(s) a ser(em) pulada(s) antes e depois
	 * @param linhasAntes
	 * @param linhasDepois
	 * @param mensagem
	 */
	public void gravaMensagem(int linhasAntes, int linhasDepois, String mensagem){
		if (linhasAntes != 0){
			for (int i = 0; i < linhasAntes; i++){
				log.println("");
			}
		}
		log.println(mensagem);
		if(linhasDepois !=0){
			for(int i = 0; i < linhasDepois; i++){
				log.println("");
			}
		}
		log.flush();
	}
	
	/**
	 * Grava mensagem em nova linha, setando linha(s) depois
	 * @param linhasDepois
	 * @param mensagem
	 */
	public void gravaMensagem(int linhasDepois, String mensagem){
		gravaMensagem(0,linhasDepois,mensagem);
		log.flush();
	}
	
	/**
	 * Finaliza arquivo de log e grava ">>"
	 */
	public void finalizaEFechaArquivo(){
		log.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		log.close();
	}
	
	/**
	 * Fecha o arquivo de log
	 */
	public void close() {
	    if (log != null) {
	    	log.close();
	    }
	}

}
