package UTIL;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logs {
	public static void gravarLog(String text){
		try{
			String txtName = "LOG - " + new SimpleDateFormat("dd-MM-yyyy")
			.format(new Date(System.currentTimeMillis()));

			File diretorio = new File("/opt/tomcat/logs/");
			//File diretorio = new File("c:/");

			if(!diretorio.exists())
				diretorio.mkdir();

			File arquivo = new File(diretorio.toString() + "/" + txtName + ".txt");

			FileWriter fileWriter = null;

			if(arquivo.exists())
				fileWriter = new FileWriter(arquivo, true);
			else{
				arquivo.createNewFile();

				fileWriter = new FileWriter(arquivo);
			}

			BufferedWriter buffer = new BufferedWriter(fileWriter);

			buffer.write("Data: " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(System.currentTimeMillis())));
			buffer.newLine();
			buffer.write(text);
			buffer.newLine();
			buffer.write("---------------------------------------");
			buffer.newLine();

			buffer.close();
			fileWriter.close();
		}catch(Exception ex) { }
	}
}
