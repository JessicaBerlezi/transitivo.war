package UTIL;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

/**
 * Utilitario para conversão de datas para objetos String formatados
 *
 */
public class DataUtils {
	
	public enum IndiceDatas {
		Dia, Mes, Ano, Completa
	}
	
	final static Logger logger = Logger.getLogger(DataUtils.class);
	static DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	final static String dataString = null;

	/**
	 * Obtem o dia da data
	 * @param data
	 * @return
	 */
	public static String getDiaString(Date data) {
		return getDataPorIndice(data, IndiceDatas.Dia);
	}

	/**
	 * Obtem o mes da data
	 * @param data
	 * @return
	 */
	public static String getMesString(Date data) {
		return getDataPorIndice(data, IndiceDatas.Mes);
	}
	
	/**
	 * Obtem o ano da data
	 * @param data
	 * @return
	 */
	public static String getAnoString(Date data) {
		return getDataPorIndice(data, IndiceDatas.Ano);
	}
	
	/**
	 * Obtem a data completa no formato ("dd/MM/yyyy")
	 * @param data
	 * @return
	 */
	public static String getDataString(Date data) {
		return getDataPorIndice(data, IndiceDatas.Completa);
	}

	private static String getDataPorIndice(Date data, IndiceDatas indice) {

		try {
			String dataFormatada = format.format(data);
			
			if (indice == IndiceDatas.Completa){
				return dataFormatada;
			}
			
			String arrayData[] = dataFormatada.split("/");

			return arrayData[indice.ordinal()];

		} catch (Exception e) {
			logger.error("Erro ao converter data " + e.getMessage());
			
			return null;
		}
	}

	public static String dateToDateBr(String data) throws ParseException{
		if(data != null && data != ""){
			SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date dt = new Date(dtFormat.parse(data).getTime());
			SimpleDateFormat dtFinal = new SimpleDateFormat("dd/MM/yyyy");
			return dtFinal.format(dt);
		}
		else{
			return "--/--/----";
		}
	}
	
	public static String getDateBr() throws Exception {
		return new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
	}
	
	public static String getDateTimeBr() throws Exception {
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
	}
	
	public static boolean validarDataBR(String data) {
		if (data != null && data != "") {
			try {
				SimpleDateFormat dtFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date dt = new Date(dtFormat.parse(data).getTime());
				SimpleDateFormat dtFinal = new SimpleDateFormat("dd/MM/yyyy");
				
				return dtFinal.format(dt).equals(data);
			} catch (Exception ex) {
				return false;
			}
		} else {
			return false;
		}
	}
}
