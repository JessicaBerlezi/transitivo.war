package REC;

import java.util.ArrayList;
import java.util.List;


/**
 * <b>Title:</b>        SMIT - Entrada de Recurso - Parecer Juridico Bean<br>
 * <b>Description:</b>  Parecer Juridico Bean <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Elisson Dias
 * @version 1.0
 */

public class EtiquetaAutoBean extends sys.HtmlPopupBean{
	
	private String numlote; 
	private String numCaixa; 
	private String numAutoInfracao;
	private int codEtqAutoInfracao;
	
	private String ordem;
	private List dados;
	
	public EtiquetaAutoBean()  throws sys.BeanException {
		numlote = "";
		numCaixa = "";
		numAutoInfracao = "";
		codEtqAutoInfracao = 0;
		
		ordem = "";
		dados = new ArrayList();
	}
	
	public void setNumLote(String numlote){
		this.numlote = numlote;
		if (this.numlote == null) this.numlote = "";
	}
	public String getNumLote(){
		return this.numlote;
	}
	
	public void setNumCaixa(String numCaixa){
		this.numCaixa = numCaixa;
		if (this.numCaixa == null) this.numCaixa = "";
	}
	public String getNumCaixa(){
		return this.numCaixa;
	}
	
	
	public void setOrdem(String ordem){
		this.ordem = ordem;
		if(this.ordem == null) this.ordem = "";
	}
	public String getOrdem(){
		return this.ordem;
	}
	
	public void setDados(List dados) {
		this.dados = dados;
	}
	public List getDados() {
		return this.dados;
	}
	
	public int getCodEtqAutoInfracao() {
		return codEtqAutoInfracao;
	}
	
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	
	public void setCodEtqAutoInfracao(int cod) {
		codEtqAutoInfracao = cod;
	}
	
	public void setNumAutoInfracao(String numAutoInfracao) {
		this.numAutoInfracao = numAutoInfracao;
		if(this.numAutoInfracao == null) this.numAutoInfracao = "";
	}
	
	//Executa a consulta dos Autos de infracao
	public void consultaAutoInfracao(String codOrgao, String numLote,String numCaixa, String numAuto) 
	throws DaoException{
		String parametro = "";
		try {
			DaoDigit dao = DaoDigit.getInstance();
			if(dao.ConsultaAutoInfracao(codOrgao, numLote,numCaixa,numAuto, this)== false){
				//Montagem da mensagem de retorno
				if(!numCaixa.equals(""))parametro += " - Caixa: "+ numCaixa;
				if(!numLote.equals("")) parametro += " - Lote: "+ numLote;
				if(!numAuto.equals("")) parametro += " - Auto: "+ numAuto;
				this.setMsgErro("N�o existe informa��es cadastradas para " + parametro);
			}
			
		} 
		catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void consultaLotes(String codOrgao, String numCaixa) throws DaoException{
		String parametro = "";
		try {
			DaoDigit dao = DaoDigit.getInstance();
			if(dao.ConsultaLoteImp(codOrgao, numCaixa,this)== false){
				//Montagem da mensagem de retorno			 
				if(!numCaixa.equals("")) parametro += " - Caixa: "+ numCaixa;			  
				this.setMsgErro("N�o existe informa��es cadastradas para " + parametro);
			}
			
		} 
		catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	
}
