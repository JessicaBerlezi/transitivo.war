package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Title:</b>        SMIT - MODULO RECURSO<br>
 * <b>Description:</b>  Bean de Auto de Infra��o Digitalizado<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class AIDigitalizadoRobBean extends sys.HtmlPopupBean {
    
    private int codAIDigitalizado;
    private String numAutoInfracao;
    private String numLote;
    private CaixAIDigitalizadoBean caixa;
    private Date datDigitalizacao;
    
    public AIDigitalizadoRobBean() {
        
        super();
        codAIDigitalizado = 0;
        numAutoInfracao = "";
        numLote = "";
        caixa = new CaixAIDigitalizadoBean();
        datDigitalizacao = null;
    }
    
    public AIDigitalizadoRobBean(String numAutoInfracao, String numLote, CaixAIDigitalizadoBean caixa,
            Date datDigitalizacao) {
        
        this();
        this.numAutoInfracao = numAutoInfracao;
        this.numLote = numLote;
        this.caixa = caixa;
        this.datDigitalizacao = datDigitalizacao;
    }
    
    public String getNumAutoInfracao() {
        return numAutoInfracao;
    }
    
    public CaixAIDigitalizadoBean getCaixa() {
        return caixa;
    }
    
    public String getNumLote() {
        return numLote;
    }
    
    public void setNumAutoInfracao(String string) {
        numAutoInfracao = string;
    }
    
    public void setCaixa(CaixAIDigitalizadoBean caixa) {
        this.caixa = caixa;
    }
    
    public void setNumLote(String string) {
        numLote = string;
    }
     
    public String getImagem() {
		String pathImagem = "";
        pathImagem =getDataDigitalizacaoString().substring(6,10)+ 
		getDataDigitalizacaoString().substring(3,5) +
		"/" + getDataDigitalizacaoString().substring(0,2); 
        return pathImagem;	
    }    

    public String getParametro(ACSS.ParamSistemaBean param) {        
        String parametro = "DIR_AI_DIGITALIZADO";
        /*
        parametro+="_"+getDataDigitalizacaoString().substring(6,10);
        */
        return parametro;
    }
    
    public String getArquivo(ACSS.ParamSistemaBean param) {
        String pathArquivo;
        try {
            pathArquivo = "E:/PROD-smit/digitalizado/"+this.getImagem();
			if (this.numAutoInfracao.length() >= 6)
				pathArquivo +=  "/Auto/" + this.numAutoInfracao + ".gif";
				
        } catch (Exception e) {
            pathArquivo = "";
        }
        return pathArquivo;
    }    
    
    public Date getDatDigitalizacao() {
        return this.datDigitalizacao;
    }
    
    public void setDatDigitalizacao(Date date) {
        this.datDigitalizacao = date;
    }
    
    public String getDataDigitalizacaoString() {
        
        String datString = "";
        if (datDigitalizacao != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            datString = df.format(this.datDigitalizacao);
        }
        return datString;
    }
    
    public int getCodAIDigitalizado() {
        return codAIDigitalizado;
    }
    
    public void setCodAIDigitalizado(int i) {
        codAIDigitalizado = i;
    }
    
    public String grava() throws DaoException {
        return DaoRobDigit.getInstance().GravaAIDigitalizado(this);
    }
    
    public static AIDigitalizadoRobBean consultaAI(String[] campos, String[] valores) throws sys.BeanException {
        
        AIDigitalizadoRobBean AI = null;
        try {
            AI = DaoRobDigit.getInstance().ConsultaAIDigitalizado(campos, valores);
            if (AI == null) {
                AI = new AIDigitalizadoRobBean();
                AI.setMsgErro("AUTO DE INFRA��O DIGITALIZADO N�O ENCONTRADO !");
            }
        } catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
        return AI;
    }
    
    public static AIDigitalizadoRobBean[] consultaAIs(String[] campos, String[] valores) throws sys.BeanException {
        
        try {
            return DaoRobDigit.getInstance().ConsultaAIDigitalizados(campos, valores);
        } 
        catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
    }
	public String getNomFoto(){
				return getImagem() + "/Auto/"+this.numAutoInfracao + ".jpg";
		}
    
}