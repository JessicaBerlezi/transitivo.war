package REC;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GuiaInformarResultCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaReemite.jsp" ;  
   
   public GuiaInformarResultCmd() {
      next = jspPadrao;
   }

   public GuiaInformarResultCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;
          
          ACSS.UsuarioFuncBean UsuarioFuncBeanId  = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
          
		  ParamOrgBean ParamOrgaoId  = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId  = new ParamOrgBean() ;	 
		  
      	  GuiaDistribuicaoBean GuiaDistribuicaoId  = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;
      	  
      	  //Carrego os Beans		
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		  GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
     	  
		  String dataIni = req.getParameter("De");
		  if (dataIni == null) dataIni = "";
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  if (dataFim == null) dataFim = "";
		  GuiaDistribuicaoId.setDataFim(dataFim);
		  
		  
		  String acao = req.getParameter("acao");   
		  if (acao == null) acao = " ";	
		  if ( (acao.equals(" ")) || (acao.equals("Novo")) )
		  {		  
			GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");	
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	acao = " ";
		  }
		  else 
		  {
			if ("LeGuia,InformaResult,Classifica,MostraAuto,imprimirInf,ImprimirGuia".indexOf(acao)<0)	{
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		  }
		  
		  if (acao.equals("LeGuia")) 
		  {
		    GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
		    GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
		    GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		    GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());						
			GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
			GuiaDistribuicaoId.LeGuia();
		    if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
			if (GuiaDistribuicaoId.getDatRS().length()==0)GuiaDistribuicaoId.setDatRS(sys.Util.formatedToday().substring(0,10));		    
			nextRetorno = "/REC/GuiaInformaResult.jsp";
		  }	        
		  
          if (acao.equals("Classifica")) 
          {
             GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;       
			 nextRetorno = "/REC/GuiaInformaResult.jsp";
      	  }
          
		  if  (acao.equals("ImprimirGuia"))  {	      	  
		  	if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS NA GUIA");				  	    
		  	nextRetorno = "/REC/GuiaPreparaImp.jsp" ;      	  
		  }
		  
      	  if (acao.equals("MostraAuto"))  
      	  {
        	AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
			if (acao.equals("MostraAuto")) 
			{ 
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			}
			else 
			{
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
				nextRetorno = "/REC/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
			AutoInfracaoBeanId.LeRequerimento(UsrLogado);
			UsrLogado.setCodOrgaoAtuacao(temp);	
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId);
      	  }	
      	  
		  if (acao.equals("InformaResult"))
		  {
		  	Vector vErro = new Vector(); 
			GuiaDistribuicaoId.setDatRS(req.getParameter("datRS"));
			if (GuiaDistribuicaoId.getDatRS().length()==0) vErro.addElement("Data do Resultado n�o preenchida. \n") ;
			else {
				if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),GuiaDistribuicaoId.getDatRS())>0) vErro.addElement("Data do Resultado n�o pode ser superior a hoje. \n") ;
				else {			
//					if (sys.Util.DifereDias(GuiaDistribuicaoId.getDatRS(),GuiaDistribuicaoId.getDatProc())>0) vErro.addElement("Data n�o pode ser anterior a Guia ("+GuiaDistribuicaoId.getDatProc()+"). \n") ;
				}
			}
			
			if (vErro.size()==0) 
			{
		    	int err = VerificaInformaResult(req,GuiaDistribuicaoId,UsrLogado) ;
		    	if (err==0) { 
					GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
					String datSessao = req.getParameter("datSessao");
					if (datSessao == null) datSessao = "";
					GuiaDistribuicaoId.setDatSessao(datSessao);
					    
					int inc = dao.GuiaGravaResult(GuiaDistribuicaoId) ; 
			  		GuiaDistribuicaoId.setMsgErro("Incluidos "+inc+" Resultados para Guia "+GuiaDistribuicaoId.getNumGuia()); 	
					GuiaDistribuicaoId.setMsgOk("S");
			  		nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp" ;
		    	}
		    	else {
			  		GuiaDistribuicaoId.setMsgErro(GuiaDistribuicaoId.getMsgErro()+" \n Nenhum Resultado incluido ");
					nextRetorno = "/REC/GuiaInformaResult.jsp"; 
		    	}
			}		
			else { 		
				GuiaDistribuicaoId.setMsgErro(vErro);
				nextRetorno = "/REC/GuiaInformaResult.jsp";
		    } 
		  }
		  
          // processamento de saida dos formularios
          if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
      	  else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaInformaResultCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }

   public int VerificaInformaResult(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
	   int err = 0 ;
	   try {	
			GuiaDistribuicaoId.setMsgErro("") ;				
			// buscar os resultados
			if (GuiaDistribuicaoId.getAutos().size()>0) 
			{
				int seq = 0;
		 		Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;
		 		while (it.hasNext()) 
		 		{
					REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		 			
			 		myAuto = (REC.AutoInfracaoBean)it.next() ;				
					myAuto.getRequerimentos(0).setCodResultRS(req.getParameter("codResultRS"+seq))	;					
					myAuto.getRequerimentos(0).setTxtMotivoRS(req.getParameter("txtMotivoRS"+seq))	;
                    myAuto.getRequerimentos(0).setCodMotivoDef(req.getParameter("codMotivo"+seq)) ;
					if (myAuto.getRequerimentos(0).getCodResultRS().length()==0) {
						myAuto.setMsgErro("Resultado n�o informado.");
						err++;
		 			}
					seq++;			
				}
				if (err>0) GuiaDistribuicaoId.setMsgErro(err+" Requerimento(s) sem resultado.") ;			
			}
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaInformaResultCmd: " + ue.getMessage());
	   }
	   return err;
   } 	

   
}
