package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar PerfisSistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class JuntaCmd extends sys.Command {

  private static final String jspPadrao="/REC/Junta.jsp";    
  private String next;

  public JuntaCmd() {
    next = jspPadrao;
  }

  public JuntaCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do Usuario, se n�o existir
		ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null)
		  OrgaoId = new ACSS.OrgaoBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)
		  acao =" ";   
		
	    String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao == null) 
		  codOrgao =""; 
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null || atualizarDependente.equals(""))
		  atualizarDependente ="N";  
		JuntaBean JuntaId = new JuntaBean();
		
	    if("buscaOrgao".indexOf(acao)>=0) {
	    	JuntaId.setCodOrgao(codOrgao);
	    	OrgaoId.Le_Orgao(codOrgao,0);
	    	atualizarDependente = "S";	
	    	req.setAttribute("myJuntas",JuntaId.getJunta(20,5));			  	
	    }
			 
		Vector vErro = new Vector(); 
					 
	    if(acao.compareTo("A") == 0){
			String[] sigJunta_aux 	= req.getParameterValues("sigJunta_aux");
			String[] sigJunta 		= req.getParameterValues("sigJunta");
			
			String[] nomJunta_aux	= req.getParameterValues("nomJunta_aux");   
			String[] nomJunta  		= req.getParameterValues("nomJunta");
			
			String[] codJunta 		= req.getParameterValues("codJunta");
       
			String[] indTipo_aux 	= req.getParameterValues("indTipo_aux"); 
			String[] indTipo 		= req.getParameterValues("indTipo");
			
			vErro = isParametros(sigJunta,nomJunta,codJunta) ;
			OrgaoId.Le_Orgao(codOrgao,0) ;
						 
            if (vErro.size()==0) {
				List junta 		= new ArrayList(); 
				List junta_aux	= new ArrayList();
				for (int i = 0; i < sigJunta.length;i++) {
					JuntaBean myJunta 	  = new JuntaBean();	
					JuntaBean myJunta_aux = new JuntaBean();
					myJunta.setSigJunta(sigJunta[i]);
					myJunta_aux.setSigJunta(sigJunta_aux[i]);
					myJunta.setNomJunta(nomJunta[i]);	
					myJunta_aux.setNomJunta(nomJunta_aux[i]);
				    myJunta.setCodOrgao(codOrgao);	
				    myJunta.setCodJunta(codJunta[i]);
				    myJunta.setIndTipo(indTipo[i]);											
				    myJunta_aux.setIndTipo(indTipo_aux[i]);										
					junta.add(myJunta); 	
					junta_aux.add(myJunta_aux);
				}
				if( JuntaId.isInsertJunta(junta,junta_aux) )
					JuntaId.setMsgErro("Dados Alterados com Sucesso");
				req.setAttribute("myJuntas",junta);
		    	atualizarDependente = "S";
			}
			else JuntaId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
			JuntaId.setCodOrgao(codOrgao);
			OrgaoId.Le_Orgao(codOrgao,0);
			req.setAttribute("myJuntas",JuntaId.getJunta(0,0));
			nextRetorno = "/REC/JuntaImp.jsp";
	    }
	    
	    req.setAttribute("atualizarDependente",atualizarDependente);
	    req.setAttribute("JuntaId",JuntaId);
		req.setAttribute("OrgaoId",OrgaoId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("JuntaCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  private Vector isParametros(String[] sigJunta,String[] nomJunta,String[] codJunta) {
		Vector vErro = new Vector() ;		 
		if ((sigJunta.length!=nomJunta.length) || (sigJunta.length!=codJunta.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 
}


