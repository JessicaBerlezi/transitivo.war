package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RemessaIncluiProcCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/REC/RemessaIncluirProc.jsp";

	public RemessaIncluiProcCmd() {
		next = jspPadrao;
	}
 
	public RemessaIncluiProcCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {
			sys.Command cmd = (sys.Command) Class.forName("REC.ProcessaAutoCmd").newInstance();
			cmd.setNext(this.next);
			nextRetorno = cmd.execute(req);
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)
				UsrLogado = new ACSS.UsuarioBean();

			ACSS.UsuarioFuncBean usrFunc =(ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null)
				usrFunc = new ACSS.UsuarioFuncBean();

			ParamOrgBean ParamOrgaoId = (ParamOrgBean) session.getAttribute("ParamOrgBeanId");
			if (ParamOrgaoId == null)
				ParamOrgaoId = new ParamOrgBean();
			
			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId =(AutoInfracaoBean) req.getAttribute("AutoInfracaoBeanId");
			RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
			if (RequerimentoId == null)
				RequerimentoId = new RequerimentoBean();
			
			RemessaIncluirProcBean RemessaIncluirProcBeanId =	(RemessaIncluirProcBean) req.getAttribute("RemessaIncluirProcBeanId");
			if (RemessaIncluirProcBeanId == null)
				RemessaIncluirProcBeanId = new RemessaIncluirProcBean();
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			session.setAttribute("codReq", "");
			if (acao == null)
				acao = " ";
			String sigFuncao = usrFunc.getJ_sigFuncao();

			if ((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length() == 0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				//Bahia
				RemessaBean RemessaBeanId = new RemessaBean();
				RemessaBeanId.filtaAutosRemessa(AutoInfracaoBeanId);

				if (AutoInfracaoBeanId.getRequerimentos().size()<=0)
				{
					AutoInfracaoBeanId.setMsgErro("Auto com Requerimento(s) j� vinculado(s) em Guia de Remessa ou Distribui��o.");
					AutoInfracaoBeanId.setMsgOk("S") ;
					RemessaIncluirProcBeanId.setEventoOK("N");
				}
				else
				{
					RemessaIncluirProcBeanId.setEventoOK("S");
					session.setAttribute("codReq", RemessaIncluirProcBeanId.getCodReq());
				}
			}
			
			if (acao.equals("IncluirProcesso")) {
				String codRemessa = req.getParameter("codRemessa");
				String codReq = req.getParameter("codReq");
				String existeGuia = "";
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				//Bahia
				  session.setAttribute("codReq", codReq);
				  RemessaIncluirProcBeanId.setEventoOK(AutoInfracaoBeanId,sigFuncao,codReq,UsrLogado);
				  RemessaIncluirProcBeanId.incluirProcessoRemessa(AutoInfracaoBeanId,codRemessa,codReq,UsrLogado,existeGuia);
				  if (AutoInfracaoBeanId.getMsgErro().length()==0){
					AutoInfracaoBeanId.setMsgErro("Requerimento inclu�do na guia " + codRemessa + " com sucesso.");
					AutoInfracaoBeanId.setMsgOk("S") ;
				  }
			}	

			//Bahia
			if (acao.equals("LeReq")) {
				String codReq = req.getParameter("codReq");
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				RemessaIncluirProcBeanId.setEventoOK(AutoInfracaoBeanId,sigFuncao,codReq,UsrLogado);
				session.setAttribute("codReq", codReq);
			}	

			req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			req.setAttribute("RemessaIncluirProcBeanId", RemessaIncluirProcBeanId);
			session.setAttribute("UsuarioFuncBeanId", usrFunc);
		} catch (Exception ue) {
			throw new sys.CommandException("RemessaIncluirProcessoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

}
