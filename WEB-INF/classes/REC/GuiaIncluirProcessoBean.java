package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sys.BeanException;
import ACSS.UsuarioBean;
/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Parecer Juridico Bean<br>
* <b>Description:</b>  Parecer Juridico Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Elisson Dias
* @version 1.0
*/

public class GuiaIncluirProcessoBean extends EventoBean {

	private String eventoOK;
	private String msg;
	private String comboReqs;
	private String comboGuias;
	private List guias;
    private String codEvento ;
	private String codReq ;
	private String tipoJunta;	

	public GuiaIncluirProcessoBean() throws sys.BeanException {
		super();
		eventoOK = "N";
		msg = "";
		comboReqs = "";
		comboGuias = "";
		codEvento = "208";
		guias = new ArrayList();

	}
	
	/*Metodo Criado - EventoBean*/
	public void setJ_sigFuncao(String j_sigFuncao,String obrParJur)   {
		if (j_sigFuncao==null) j_sigFuncao = "" ;
		setCFuncao(j_sigFuncao) ;
		// Defesa Previa - REC0230
		setStatusValido("5") ;
		setStatusTPCVValido("0,1,2,3,4") ;		
		setNFuncao("DEFINIR/TROCAR RELATOR DE DEF. PR�VIA") ;
		setTipoReqValidos("DP,DC") ;
		// Testar parametro de Orgao - para verificar se � obrigat�rio Parecer Juridico.
		if (obrParJur==null) obrParJur = "N";
		if ("S".equals(obrParJur)) setStatusReqValido("0,1,2") ;
		else setStatusReqValido("0,1,2") ;	
		setOrigemEvento("100") ;
		setCodEvento("208") ;	
		this.tipoJunta="0";
		// 1a Instancia 
		if ("REC0330".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("") ;		
			setNFuncao("DEFINIR/TROCAR RELATOR DE 1a. INST�NCIA") ;
			setTipoReqValidos("1P,1C") ;	
			setStatusReqValido("0,1,2") ;	
			setOrigemEvento("100") ;
			setCodEvento("328") ;		
			this.tipoJunta="1";
		}
		if ("REC0430".equals(j_sigFuncao)) {
			setStatusValido("35") ;
			setStatusTPCVValido("") ;		
			setNFuncao("DEFINIR/TROCAR RELATOR DE 2a. INST�NCIA") ;
			setTipoReqValidos("2P,2C") ;	
			setStatusReqValido("0,1,2") ;	
			setOrigemEvento("100") ;
			setCodEvento("335") ;	
			this.tipoJunta="2";
		}
		return ;	
	}   
	
	

	//--------------------------------------------------------------------------  
	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "N";
		this.eventoOK = eventoOK;
	}

	public void setCodReq(String CodReq) {
		this.codReq = CodReq;
	}


	public void setEventoOK(GuiaDistribuicaoBean GuiaDistribuicaoId ,REC.AutoInfracaoBean myAuto, String sigFuncao, String codReq,UsuarioBean UsrLogado)
		throws sys.BeanException {
		this.msg = "";
		this.eventoOK = "N";
		if ("".equals(myAuto.getNomStatus()))
			return;
		this.msg = myAuto.getNomStatus() + " (" + myAuto.getDatStatus() + ")";
		

		boolean existe = false;
		boolean temGuia = false;
		String existeGuia = "";
		if ( (sigFuncao.equals("REC0238")) || (sigFuncao.equals("REC0236")) || (sigFuncao.equals("REC0250")) ) {
            this.codEvento="208";
            /*Critica modificada para ser identica ao EventoBean
            if (!myAuto.getCodStatus().equals("5")) {
            	this.msg += " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";
            	return;
            }
            */
  		  boolean  ok = (getStatusValido().equals(myAuto.getCodStatus()));
//		  if (!ok) ok = ( ("".equals(getStatusTPCVValido())==false) && ("1".equals(myAuto.getIndTPCV())) && (getStatusTPCVValido().indexOf(myAuto.getCodStatus())>=0)) ;
		  if (!ok) ok = ( (getStatusValido().length()==myAuto.getCodStatus().length()) && (getStatusTPCVValido().indexOf(myAuto.getCodStatus())>=0)) ;
		  if ( (!ok) && ("15,35".indexOf(getStatusValido())>=0) ) ok = ("15,16,35,36".indexOf(myAuto.getCodStatus())>=0);
	   	  if (!ok) {
          	this.msg += " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";
        	return;
	   	  }
            
            
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();

				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();
					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					
					//if (("".equals(codReq)) || (codReq.equals(req.getCodRequerimento()))){
				
					if (codReq.equals(req.getCodRequerimento())){

						existeGuia = ConsultaReqGuia(myAuto,req.getCodRequerimento());
						
						this.codReq = req.getCodRequerimento();	
						
						
						if (existeGuia.length()>0) {	
							this.msg = "Requerimento incluido na Guia " + existeGuia;
						}
						
							if (req.getCodStatusRequerimento().equals("0")
								|| req.getCodStatusRequerimento().equals("1")
								|| req.getCodStatusRequerimento().equals("2")) {
								existe = true;
								consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"2", "0", "", existeGuia,UsrLogado);
								if (!guias.isEmpty()) {
									temGuia = true;
								}
								
								break;
							}
							if (req.getCodStatusRequerimento().equals("3")) {
								existe = true;
								consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"3", "0", req.getCodRelatorJU(), existeGuia,UsrLogado);
								if (!guias.isEmpty()) {
									temGuia = true;
								}
								break;
							
							}
							if (req.getCodStatusRequerimento().equals("4")) {
								existe = true;
								consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"4", "0",req.getCodRelatorJU(), existeGuia,UsrLogado);
								if (!guias.isEmpty()) {
										temGuia = true;
								}
								break;
							}
					}
				
				}
				

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		if ( (sigFuncao.equals("REC0338")) || (sigFuncao.equals("REC0336")) || (sigFuncao.equals("REC0350")) ) {
			this.codEvento="328" ;
			if (!myAuto.getCodStatus().equals("15") && !myAuto.getCodStatus().equals("16")) {

				this.msg += " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();

				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();

					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					
					//if (("".equals(codReq)) || (codReq.equals(req.getCodRequerimento()))){
				
					if (codReq.equals(req.getCodRequerimento())){

						if (req.getCodStatusRequerimento().equals("0")
							|| req.getCodStatusRequerimento().equals("1")
							|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"2", "1", "", existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}								
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"3", "1", req.getCodRelatorJU(), existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
							
						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"4", "1",req.getCodRelatorJU(), existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
									temGuia = true;
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		if ( (sigFuncao.equals("REC0438")) || (sigFuncao.equals("REC0436")) || (sigFuncao.equals("REC0450"))) {
			this.codEvento="335" ;
			if (!myAuto.getCodStatus().equals("35") && !myAuto.getCodStatus().equals("36")) {

				this.msg += " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();

				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();
					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					
					//if (("".equals(codReq)) || (codReq.equals(req.getCodRequerimento()))){
				
					if (codReq.equals(req.getCodRequerimento())){

						if (req.getCodStatusRequerimento().equals("0")
							|| req.getCodStatusRequerimento().equals("1")
							|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"2", "2", "", existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
								
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"3", "2", req.getCodRelatorJU(), existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
							
						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaGuiaIncluirProcesso(GuiaDistribuicaoId,"4", "2",req.getCodRelatorJU(), existeGuia,UsrLogado);
							if (!guias.isEmpty()) {
									temGuia = true;
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		
		if (existe && temGuia)
			this.eventoOK = "S";
		else if (existe && !temGuia)
			this.msg += " - N�o existe guia dispon�vel para incluir novos processos.";
		else
			this.msg
				+= " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";
		
		setMsg(this.msg);
	}
	public String getEventoOK() {
		return this.eventoOK;
	}
	//--------------------------------------------------------------------------
	public void setMsg(String msg) {
		if(msg == null) msg = "";
		else this.msg = msg;
	}
	public String getMsg() {
		return msg;
	}

	//--------------------------------------------------------------------------  
	public String getCodReq() {
		return codReq;
	}

	//--------------------------------------------------------------------------  
	public void setComboGuias(REC.AutoInfracaoBean myAuto) throws sys.BeanException {
		this.comboGuias = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append(
			"<select style='border-style: outset;' name='codLoteRelator' size='1'> \n");	    			  
		try {
			Iterator it = guias.iterator();

			String nom = "";
			String cod = "";
			while (it.hasNext()) {
				
				StatusBean status = (StatusBean) it.next();
				cod =  status.getCodStatus();
				nom = status.getNomStatus();
				
				popupBuffer.append("<OPTION VALUE='" + cod + "'" + ">" + nom + "</OPTION> \n");
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboGuias = popupBuffer.toString();
	}
	public String getComboGuias() {
		return (("S".equals(this.eventoOK)) ? this.comboGuias : this.msg);
	}

	public void setComboReqs(REC.AutoInfracaoBean myAuto,String codReq) throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append(
                                                                    	  //Bahia
            "<select style='border-style: outset;' name='codReq' size='1' onChange='javascript: valida(\"LeReq\",this.form)'> \n");	    			  
		try {
			Iterator it = myAuto.getRequerimentos().iterator();
			REC.RequerimentoBean req = new REC.RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {				
				req =(REC.RequerimentoBean)it.next() ;
			    if (req.getCodStatusRequerimento().equals("0") ||
				    req.getCodStatusRequerimento().equals("1") ||				
				    req.getCodStatusRequerimento().equals("2") ||				
				    req.getCodStatusRequerimento().equals("3") ||				
				    req.getCodStatusRequerimento().equals("4")) {				
					nom=req.getCodTipoSolic() + " " + req.getNumRequerimento()+" ("+req.getDatRequerimento()+")";
					if (req.getCodRequerimento().equals(codReq))
      					popupBuffer.append("<OPTION selected VALUE='" + req.getCodRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                else
    					popupBuffer.append("<OPTION VALUE='" + req.getCodRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                
			   }			
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}

	public void setComboReqsManual(REC.AutoInfracaoBean myAuto,String codReq) throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append(
                                                                    	  //Bahia
            "<select style='border-style: outset;' name='codReq' size='1' onChange='javascript: valida(\"LeReq\",this.form)'> \n");	    			  
		try {
			Iterator it = myAuto.getRequerimentos().iterator();
			REC.RequerimentoBean req = new REC.RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {				
				req =(REC.RequerimentoBean)it.next() ;
				    if (req.getCodStatusRequerimento().equals("0")) {				
					nom=req.getCodTipoSolic() + " " + req.getNumRequerimento()+" ("+req.getDatRequerimento()+")";
					if (req.getCodRequerimento().equals(codReq))
      					popupBuffer.append("<OPTION selected VALUE='" + req.getCodRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                else
    					popupBuffer.append("<OPTION VALUE='" + req.getCodRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                
				}			
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}

	public String getComboReqs() {
		return (("S".equals(this.eventoOK)) ? this.comboReqs : "&nbsp;");
	}


	public void consultaGuiaIncluirProcesso(GuiaDistribuicaoBean GuiaDistribuicaoId,String statusGuia, String tipoJunta, String numCPF, String existeGuia,ACSS.UsuarioBean UsrLogado)
		throws BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			this.guias = dao.consultaGuiaIncluirProcesso(GuiaDistribuicaoId,statusGuia, tipoJunta, numCPF, existeGuia,UsrLogado);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	public void incluirProcessoGuia(AutoInfracaoBean myAuto, 
	String codLoteRelator, String codReq,ACSS.UsuarioBean UsrLogado, String existeGuia)
		throws BeanException  {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.incluirProcessoGuia(myAuto,
			codLoteRelator, codReq,codEvento,UsrLogado, existeGuia);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}


	public String ConsultaReqGuia(AutoInfracaoBean myAuto, 
	String codReq)
		throws BeanException  {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.ConsultaReqGuia(myAuto);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	public void setTipoJunta(String tipoJunta)  {
		this.tipoJunta=tipoJunta ;
		if (tipoJunta==null) this.tipoJunta= "";
	}  
	public String getTipoJunta()  {
		return this.tipoJunta;
	}  



	//--------------------------------------------------------------------------  
}
