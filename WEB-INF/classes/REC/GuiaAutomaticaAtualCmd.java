package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;
import sys.BeanException;
import sys.Util;

public class GuiaAutomaticaAtualCmd extends sys.Command {
	private String next;   
	private static final String jspPadrao="/REC/GuiaAutomatica.jsp" ;
	
	
	public GuiaAutomaticaAtualCmd() {
		next = jspPadrao;
	}
	
	public GuiaAutomaticaAtualCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;      	
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null) UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
			
			ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			
			RelatorBean RelatorBeanId = (RelatorBean)session.getAttribute("RelatorBeanId") ;
			if (RelatorBeanId==null)  RelatorBeanId = new RelatorBean() ;
			
			String acao    = req.getParameter("acao");
			if (acao==null)	{
				session.removeAttribute("GuiaDistribuicaoId") ;
				acao = " ";
			}		  
			
			GuiaDistribuicaoBean GuiaDistribuicaoId = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId");
			if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
			
			//Carrego os Beans
			String sSigFuncao=req.getParameter("j_sigFuncao");
			if (sSigFuncao==null) sSigFuncao="REC0250";
			GuiaDistribuicaoId.setJ_sigFuncao(sSigFuncao,ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		  
			
			if ( (acao.equals(" ")) || (acao.equals("Retornar")) ){
				nextRetorno="/REC/GuiaAutomatica.jsp";			  			 
			}	  
			
			if  (acao.equals("EnviarProcesso"))  
			{
				String numProcesso[] = req.getParameterValues("numProcesso");
				if (numProcesso==null) numProcesso = new String[0];
				nextRetorno=EnviarProcesso(numProcesso,GuiaDistribuicaoId,UsrLogado,UsuarioFuncBeanId);
				RelatorBeanId.getRelatoresAtivos(UsrLogado,GuiaDistribuicaoId.getTipoJunta());				
			}	  			
			
			if (acao.equals("Distribuir"))  
			{
				try {	
					GerarGuias(req,GuiaDistribuicaoId,UsrLogado,ParamOrgaoId,sSigFuncao);
					
				}
				catch (Exception ue) {
					throw new sys.CommandException("GuiaAutomaticaCmd: " + ue.getMessage());
				}				
				nextRetorno = "/REC/GuiaDistribuicaoMostraAutomatica.jsp"; 
			}

			if  (acao.equals("ImprimirGuia"))  {	   
				nextRetorno="/REC/GuiaPreparaImpAutomatica.jsp" ;				
			}
			
			// processamento de saida dos formularios
			if (acao.equals("R")){
				session.removeAttribute("GuiaDistribuicaoId") ;
				session.removeAttribute("RelatorBeanId") ;
			}
			else {
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
				session.setAttribute("RelatorBeanId",RelatorBeanId) ;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaAutomaticaCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public String EnviarProcesso(String[] numProcesso,GuiaDistribuicaoBean GuiaDistribuicaoId,ACSS.UsuarioBean UsrLogado,ACSS.UsuarioFuncBean UsuarioFuncBeanId)throws BeanException
	{
		UsuarioBean  UsuarioLogado = new ACSS.UsuarioBean();  
		UsuarioLogado.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		String nextRetorno="/REC/GuiaAutomaticaProcesso.jsp";
		int iTotalRequerimentos=0,iToTalProcessosErro=0,iToTalProcessos=0;;
		List <AutoInfracaoBean>Processos = new ArrayList<AutoInfracaoBean>();
		for (int j=0; j < numProcesso.length; j++) 
		{
			String sNumProcesso  = numProcesso[j];
			if (sNumProcesso.trim().length()==0) continue;
			iToTalProcessos++; 
			AutoInfracaoBean myAuto = new AutoInfracaoBean();
			myAuto.setNumProcesso(sNumProcesso);
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsuarioLogado.setCodOrgaoAtuacao("999999"); 
			myAuto.LeAutoInfracao(UsuarioLogado);
			UsuarioLogado.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			
			if (myAuto.getNumAutoInfracao().trim().length()!=0) {
				myAuto.setMsgErro("Processo sem Requerimento");
				myAuto.LeRequerimento(UsrLogado);
				/*Adicionei os Requerimentos*/
				/*
				for (int k=0;k<myAuto.getRequerimentos().size();k++)
				{
					REC.RequerimentoBean reqProcesso = new REC.RequerimentoBean();
					reqProcesso = (REC.RequerimentoBean)myAuto.getRequerimentos().get(k);
					if ( (reqProcesso.getCodStatusRequerimento().equals("0")) &&
							(GuiaDistribuicaoId.getTipoReqValidos().indexOf(reqProcesso.getCodTipoSolic())>=0) )	{
					  myAuto.getAutos().add(myAuto);						
					}
				}
				*/
				
				int iRequerimentoAuto = 0;
				try {
					Iterator it = myAuto.getRequerimentos().iterator();
					REC.RequerimentoBean reqProcesso = new REC.RequerimentoBean();
					while (it.hasNext()) {				
						reqProcesso =(REC.RequerimentoBean)it.next();
						if ( (reqProcesso.getCodStatusRequerimento().equals("0")) &&
								(GuiaDistribuicaoId.getTipoReqValidos().indexOf(reqProcesso.getCodTipoSolic())>=0) )	{
							iTotalRequerimentos++;
							iRequerimentoAuto++;
							myAuto.setMsgErro("");
							myAuto.setNumRequerimento(reqProcesso.getNumRequerimento());
							myAuto.setTpRequerimento(reqProcesso.getCodTipoSolic());
							String sExiste=GuiaDaoBroker.getInstance().ConsultaReqGuia(myAuto);
							if (sExiste.length()>0) myAuto.setMsgErro("Processo/Requerimento j� Cadastrado em outra Guia.");
							if (iRequerimentoAuto>1) myAuto.setCodArquivo("999");
							Processos.add(myAuto);						
						}
					}					  
					
				} catch (Exception ex) {
					throw new sys.BeanException("GuiaAutomaticaCmd-EnviarProcesso"+ex.getMessage());
				}
			}
			else {
				Processos.add(myAuto);
			}
			if (myAuto.getMsgErro().length()>0)  iToTalProcessosErro++; 
			/*Adicionar processos sem requerimento*/
			if (myAuto.getMsgErro().equals("Processo sem Requerimento")) Processos.add(myAuto);
		}
		GuiaDistribuicaoId.setAutos(Processos);
		GuiaDistribuicaoId.setTotalRequerimentos(iTotalRequerimentos);
		GuiaDistribuicaoId.setToTalProcessosErro(iToTalProcessosErro);
		GuiaDistribuicaoId.setTotalProcessos(iToTalProcessos);
		if (iToTalProcessos==0) {
			GuiaDistribuicaoId.setMsgErro("Nenhum processo selecionado."); 
			nextRetorno="/REC/GuiaAutomatica.jsp";			  
		}
		return nextRetorno;
	}
	
	
	public void GerarGuias(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
			ACSS.UsuarioBean UsrLogado,ParamOrgBean ParamOrgaoId,String sSigFuncao) throws Exception {
		
		/*Lista dos Processos Atual*/
		ArrayList ListProcAtual = (ArrayList)GuiaDistribuicaoId.getAutos();
				
	      /*Lista dos Relatores Atuais*/		
		String relatSelec[] = req.getParameterValues("relatores");
            if (relatSelec==null) relatSelec= new String[0];
		ArrayList<RelatorBean>ListRelatores = new ArrayList<RelatorBean>();
		for (int j=0; j < relatSelec.length; j++) 
		{
			RelatorBean myRelator = new RelatorBean();
			myRelator.setCodJunta(relatSelec[j].substring(0,6));
			myRelator.setNumCpf(relatSelec[j].substring(6,17));
			myRelator.Le_Relator("CPF");
			ListRelatores.add(myRelator);
		}
		/*Calcular Processos por Relator*/
		CalcularProcessosPorRelator(ListRelatores,ListProcAtual.size());
	
		/*Obter uma lista Sorteada do tamanho de Processos Atual*/
		ArrayList <Integer>ListSequencialProcSort = Util.ListaSequencialSort(ListProcAtual.size());
	
		/*Gerar Guias por Relator*/
		GerarGuiasPorRelator(GuiaDistribuicaoId,ListRelatores,ListProcAtual,ListSequencialProcSort,req,ParamOrgaoId,UsrLogado,sSigFuncao);
	}
		
	public void CalcularProcessosPorRelator(ArrayList <RelatorBean>ListRelatores,int iTamanho)
	{
		int iNumProcRelator=iTamanho/ListRelatores.size();
		int iRestoProcRelator=iTamanho%ListRelatores.size();
		for (int i=0;i<ListRelatores.size();i++)
		{
			if (i<iRestoProcRelator)
				ListRelatores.get(i).setProcessoPorGuia(iNumProcRelator+1);
			else
				ListRelatores.get(i).setProcessoPorGuia(iNumProcRelator);
		}
	}
	
	public void GerarGuiasPorRelator(GuiaDistribuicaoBean myGuias,ArrayList <RelatorBean>ListRelatores,ArrayList ListProcAtual,ArrayList ListSequencialProcSort,HttpServletRequest req,ParamOrgBean paramOrgao,UsuarioBean myUsuario,String sSigFuncao) throws Exception
	{
		List <GuiaDistribuicaoBean> myListGuia = new ArrayList <GuiaDistribuicaoBean>();
	        int iSequenciaProcUsados=0;
		for (int i=0;i<ListRelatores.size();i++)
		{
			GuiaDistribuicaoBean myGuia = new GuiaDistribuicaoBean();
			myGuia.setJ_sigFuncao(sSigFuncao,paramOrgao.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7")); 
			myGuia.setNomUserName(myUsuario.getNomUserName());
			myGuia.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			myGuia.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			myGuia.setTipoGuia(myGuia.GUIA_AUTOMATICA);
			myGuia.setCodJunta(ListRelatores.get(i).getCodJunta());
			myGuia.setSigJunta(ListRelatores.get(i).getSigJunta());			
			myGuia.setNumCPFRelator(ListRelatores.get(i).getNumCpf());
			myGuia.setNomRelator(ListRelatores.get(i).getNomRelator());			
			myGuia.setCodRelator(ListRelatores.get(i).getCodRelator());
			ArrayList <AutoInfracaoBean>ListProcessos = new ArrayList<AutoInfracaoBean>();			
			for (int j=0;j<ListRelatores.get(i).getProcessoPorGuia();j++)
			{
				ListProcessos.add((AutoInfracaoBean)ListProcAtual.get((Integer)ListSequencialProcSort.get(j+iSequenciaProcUsados)));

				/*Adicionar Requerimentos Existentes no Processo para o mesmo Relator*/
				/*
				AutoInfracaoBean myAuto = new AutoInfracaoBean();
				myAuto = (AutoInfracaoBean)ListProcAtual.get((Integer)ListSequencialProcSort.get(j+iSequenciaProcUsados));
				if (myAuto.getCodArquivo().equals("999"))
					continue;
				
				for (int k=0;k<myAuto.getAutos().size();k++) {
					ListProcessos.add(myAuto.getAutos().get(i));
				}
				*/
			}
			myGuia.setAutos(ListProcessos);
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.GuiaGrava(myGuia,myUsuario);
			myListGuia.add(myGuia);
			/*Adicionar a Lista*/
			iSequenciaProcUsados+=ListRelatores.get(i).getProcessoPorGuia();
		}
		myGuias.setRelatGuias(myListGuia);
	}
}

