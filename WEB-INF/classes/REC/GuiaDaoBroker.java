package REC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;
import ACSS.UsuarioBean;

public class GuiaDaoBroker extends sys.DaoBase{
	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "REC";
	private static GuiaDaoBroker instance;
	public static GuiaDaoBroker getInstance()
	throws sys.DaoException {
		if (instance == null)
			instance = new GuiaDaoBroker();
		return instance;
	}
	
	private GuiaDaoBroker() throws sys.DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new sys.DaoException(e.getMessage());
		}
	}
	
	
	private String converteData(java.sql.Date data) {
		String Retorno = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			Retorno = df.format(data);
		} catch (Exception e) {
			Retorno = "";
		}
		return Retorno;
	}	 
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPrepara (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog,String datInicioRemessa,String numMaxProc) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();			
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String parteVar    = "";
			String tpJunta     = "1";	
			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
			else {
				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
				else tpJunta="3" ;				
			}
			if (myGuia.getCFuncao().equals("REC0251")) tpJunta="4";
			
			String indContinua = sys.Util.rPad(" "," ",27);
			List autos = new ArrayList();
			int reg=0;			
			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
				indContinua = sys.Util.rPad(" "," ",27);
				continua   = true;				  
				while (continua)
				{
					/*
					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					tpJunta ;
					*/
					
					/*DPWEB*/
					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					tpJunta;

					resultado = chamaTransBRK("049",parteVar,indContinua);
					AutoInfracaoBean myAt = new AutoInfracaoBean();							
					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
						myGuia.setMsgErro(myAt.getMsgErro());
						return false;					
					}
					String linha = "";
					int i = 0;
					for (i=0; i < 26; i++) {				
						/*linha = resultado.substring((i*112)+3,(i+1)*112+3);*/
						
						/*DPWEB*/
						linha = resultado.substring((i*115)+3,(i+1)*115+3);
						
						AutoInfracaoBean myAuto = new AutoInfracaoBean();
						RequerimentoBean myReq = new RequerimentoBean();
						
						myAuto.setInfracao(new InfracaoBean());	
						myAuto.setProprietario(new ResponsavelBean());
						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
						setPosIni(0);											
						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
						if (myAuto.getNumAutoInfracao().trim().length()>0) {
							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
							/*Retirar*/
							VerificaAutoOracle(myAuto,conn);								
							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
							myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
							// Buscar Resultado
							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
							
							/*DPWEB*/
							String sIndReqWEB=getPedaco(linha,1," "," ");
							
							
							//								myAuto.setEventoOK("1");
							
							myReq.setNumRequerimento(myAuto.getNumRequerimento());
							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());

							/*dao.LeRequerimentoLocal(myReq,conn); 02/04/2008*/
							UsuarioBean myUsrLogado = new UsuarioBean();				
							dao.LeRequerimento(myAuto,myUsrLogado);				
							int seq = 0;
							Iterator it = myAuto.getRequerimentos().iterator();
							REC.RequerimentoBean reqProcesso = new REC.RequerimentoBean();
							while (it.hasNext()) {				
								reqProcesso =(REC.RequerimentoBean)it.next();
								if (myReq.getNumRequerimento().equals(myAuto.getRequerimentos(seq).getNumRequerimento()))	{						
									myReq.setCodRequerimento(reqProcesso.getCodRequerimento());
									myReq.setCodTipoSolic(reqProcesso.getCodTipoSolic());
									myReq.setDatRequerimento(reqProcesso.getDatRequerimento());
									myReq.setCodStatusRequerimento(reqProcesso.getCodStatusRequerimento());
									myReq.setNomUserNameDP(reqProcesso.getNomUserNameDP());
									myReq.setCodOrgaoLotacaoDP(reqProcesso.getCodOrgaoLotacaoDP());
									myReq.setNomResponsavelDP(reqProcesso.getNomResponsavelDP());			
									myReq.setDatProcDP(reqProcesso.getDatProcDP());
									myReq.setDatPJ(reqProcesso.getDatPJ());                
									myReq.setCodParecerPJ(reqProcesso.getCodParecerPJ());
									myReq.setCodRespPJ(reqProcesso.getCodRespPJ());
									myReq.setTxtMotivoPJ(reqProcesso.getTxtMotivoPJ());
									myReq.setNomUserNamePJ(reqProcesso.getNomUserNamePJ());
									myReq.setCodOrgaoLotacaoPJ(reqProcesso.getCodOrgaoLotacaoPJ());
									myReq.setDatProcPJ(reqProcesso.getDatProcPJ());
									myReq.setDatJU(reqProcesso.getDatJU());
									myReq.setCodJuntaJU(reqProcesso.getCodJuntaJU());
									myReq.setCodRelatorJU(reqProcesso.getCodRelatorJU());
									myReq.setNomUserNameJU(reqProcesso.getNomUserNameJU());
									myReq.setCodOrgaoLotacaoJU(reqProcesso.getCodOrgaoLotacaoJU());
									myReq.setDatProcJU(reqProcesso.getDatProcJU());
									myReq.setDatRS(reqProcesso.getDatRS());
									myReq.setNomUserNameRS(reqProcesso.getNomUserNameRS());
									myReq.setCodOrgaoLotacaoRS(reqProcesso.getCodOrgaoLotacaoRS());
									myReq.setDatProcRS(reqProcesso.getDatProcRS());
									myReq.setNomCondutorTR(reqProcesso.getNomCondutorTR());
									myReq.setIndTipoCNHTR(reqProcesso.getIndTipoCNHTR());
									myReq.setNumCNHTR(reqProcesso.getNumCNHTR());
									myReq.setCodUfCNHTR(reqProcesso.getCodUfCNHTR());
									myReq.setNumCPFTR(reqProcesso.getNumCPFTR());	
									myReq.setDatAtuTR(reqProcesso.getDatAtuTR());
									myReq.setNomUserNameTR(reqProcesso.getNomUserNameTR());
									myReq.setCodOrgaoLotacaoTR(reqProcesso.getCodOrgaoLotacaoTR());
									myReq.setSitProtocolo(reqProcesso.getSitProtocolo());
									myReq.setDatRecebimentoGuia(reqProcesso.getDatRecebimentoGuia());
									myReq.setDatEnvioGuia(reqProcesso.getDatEnvioGuia());
									myReq.setNomUserNameRecebimento(reqProcesso.getNomUserNameRecebimento());
									myReq.setNomUserNameEnvio(reqProcesso.getNomUserNameEnvio());
									myReq.setCodRemessa(reqProcesso.getCodRemessa());	
									myReq.setDatEnvioDO(reqProcesso.getDatEnvioDO());
									myReq.setCodEnvioDO(reqProcesso.getCodEnvioDO());
									myReq.setDatPublPDO(reqProcesso.getDatPublPDO());
									myReq.setCodMotivoDef(reqProcesso.getCodMotivoDef());
									sCmd =  "SELECT * FROM TSMI_REQUERIMENTO_WEB WHERE "+
									"NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
									rs = stmt.executeQuery(sCmd) ;			 
									if (rs.next())
									{
										myReq.setNomRequerente(rs.getString("NOM_REQUERENTE"));
										myReq.setTxtJustificativa(rs.getString("TXT_MOTIVO"));
										myReq.setTxtEmail(rs.getString("TXT_EMAIL"));
									}
									break;
								}
								seq++;
							}
							
							
							/*DPWEB*/
							myReq.setIndReqWEB(sIndReqWEB);		
							
							
							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");							
							Date dat_InicioRemessa = new Date();
							Date dat_Requerimento  = new Date();
							boolean bProtocolo=true;
							
							if(datInicioRemessa.length()>0)
							{
								/*dao.LeRequerimentoLocal(myReq,conn);*/
								dat_InicioRemessa = df.parse(datInicioRemessa);								
								if (myReq.getDatRequerimento().length()==0)
									myReq.setDatRequerimento("26/07/2004");
								
								dat_Requerimento = df.parse(myReq.getDatRequerimento());
								if (dat_Requerimento.after(dat_InicioRemessa))
								{
									if ( (myReq.getSitProtocolo().length()==0) || (myReq.getSitProtocolo().equals("0")) )
										bProtocolo=false;
								}
							}
							
//							Verifica se tem AI Digitalizado
							sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
							" WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
							rs    = stmt.executeQuery(sCmd) ;
							while (rs.next()) {
								myAuto.setTemAIDigitalizado(true);			
							}				
							myAuto.setEventoOK("0");
							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
							{
								if (bProtocolo) {
									myAuto.getRequerimentos().add(myReq);
									autos.add(myAuto) ;
								}
							}
						}										
					}
					/*DPWEB*/
					//setPosIni(3+(28*115)); antigo
					setPosIni(3+26*115); 
					
					if (reg>Integer.parseInt(numMaxProc)) break;					
					
					indContinua = getPedaco(resultado,27," "," ");
					if (indContinua.trim().length()<=0) continua=false;
					reg++;
				}			    
			}
			myGuia.setAutos(autos);	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaDetran (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog,String datInicioRemessa) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();			
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String parteVar    = "";
			String tpJunta     = "1";	
			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1" ;				
			else {
				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
				else tpJunta="3" ;				
			}
			String indContinua = sys.Util.rPad(" "," ",27);
			List autos = new ArrayList();
			int reg=0;			
			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
				indContinua = sys.Util.rPad(" "," ",27);
				continua   = true;				  
				while (continua)
				{
					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					tpJunta ;
					resultado = chamaTransBRK("049",parteVar,indContinua);
					AutoInfracaoBean myAt = new AutoInfracaoBean();							
					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
						myGuia.setMsgErro(myAt.getMsgErro());
						return false;					
					}
					String linha = "";
					int i = 0;
					for (i=0; i < 26; i++) {				
						linha = resultado.substring((i*115)+3,(i+1)*115+3);					
						AutoInfracaoBean myAuto = new AutoInfracaoBean();
						RequerimentoBean myReq = new RequerimentoBean();
						
						myAuto.setInfracao(new InfracaoBean());	
						myAuto.setProprietario(new ResponsavelBean());
						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
						setPosIni(0);											
						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
						if (myAuto.getNumAutoInfracao().trim().length()>0) {
							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
							/*Retirar*/
							VerificaAutoOracle(myAuto,conn);								
							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
							myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
							// Buscar Resultado
							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
							//								myAuto.setEventoOK("1");
							
							myReq.setNumRequerimento(myAuto.getNumRequerimento());
							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
							/*dao.LeRequerimentoLocal(myReq,conn); 08/02/2007*/
							
							SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");							
							Date dat_InicioRemessa = new Date();
							Date dat_Requerimento  = new Date();
							boolean bProtocolo=true;
							
							if(datInicioRemessa.length()>0)
							{
								dao.LeRequerimentoLocal(myReq,conn);
								dat_InicioRemessa = df.parse(datInicioRemessa);								
								if (myReq.getDatRequerimento().length()==0)
									myReq.setDatRequerimento("26/07/2004");
								
								dat_Requerimento = df.parse(myReq.getDatRequerimento());
								if (dat_Requerimento.after(dat_InicioRemessa))
								{
									if ( (myReq.getSitProtocolo().length()==0) || (myReq.getSitProtocolo().equals("0")) )
										bProtocolo=false;
								}
							}
							
							/*Retirar*/
//							if (myAuto.getCodStatus().equals("99"))
//								bProtocolo=false;
							int ianoProc = Integer.parseInt(myAuto.getDatProcesso().substring(6,10));
							if (ianoProc < 2006)
								bProtocolo=false;
							
							
							
							
//							Verifica se tem AI Digitalizado
							sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
							" WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
							rs    = stmt.executeQuery(sCmd) ;
							while (rs.next()) {
								myAuto.setTemAIDigitalizado(true);			
							}				
							myAuto.setEventoOK("0");
							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
							{
								if (bProtocolo)
									autos.add(myAuto) ;	
							}
						}										
					}
					//setPosIni(3+(28*112));
					setPosIni(3+26*115); 
					
					indContinua = getPedaco(resultado,27," "," ") ;				
					if (indContinua.trim().length()<=0) continua=false;
					reg++;
				}			    
			}
			myGuia.setAutos(autos);	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaAgravo (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt  = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;
			ResultSet rs2      = null;			
			boolean continua   = true;
			String resultado   = "";
			String parteVar    = "";
			String tpJunta     = "1";	
			int iContador = 0;
			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
			else {
				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
				else tpJunta="3" ;				
			}
			
			List autos = new ArrayList();			
			sCmd="SELECT * FROM TSMI_GUIA ORDER BY NUM_PROCESSO,to_date(dat_processo,'dd/mm/yyyy')";
			rs2=stmt2.executeQuery(sCmd);
			while (rs2.next())
			{
				
				AutoInfracaoBean myAuto = new AutoInfracaoBean();
				myAuto.setInfracao(new InfracaoBean());
				
				myAuto.setProprietario(new ResponsavelBean());
				myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
				myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
				myAuto.setNumAutoInfracao(rs2.getString("NUM_AUTO"));
				myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
				myAuto.getInfracao().setCodAutoInfracao(rs2.getString("COD_AUTO_INFRACAO"));
				myAuto.getInfracao().setCodInfracao(rs2.getString("COD_INFRACAO"));
				String sEnquadramento=rs2.getString("DSC_ENQUADRAMENTO");
				myAuto.setDatInfracao(rs2.getString("DAT_INFRACAO"));
				myAuto.setNumProcesso(rs2.getString("NUM_PROCESSO"));
				myAuto.setDatProcesso(rs2.getString("DAT_PROCESSO"));
				myAuto.setNumRequerimento(rs2.getString("NUM_REQUERIMENTO"));
				myAuto.setTpRequerimento(rs2.getString("TIP_REQUERIMENTO"));
				myAuto.setCodResultPubDO(rs2.getString("COD_RESULTADO"));
				myAuto.setCodVinculada(rs2.getString("COD_VINCULADA"));
				myAuto.getInfracao().setDscEnquadramento(sEnquadramento);
				myAuto.setEventoOK("0");
				if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) 
					autos.add(myAuto);
			}
			myGuia.setAutos(autos);	
			if (rs != null) rs.close();
			if (rs2!= null) rs2.close();			
			stmt.close();
			stmt2.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean GuiaPreparaAgravoInsert (GuiaDistribuicaoBean myGuia,ACSS.UsuarioBean myUsrLog) throws sys.DaoException {
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String parteVar    = "";
			String tpJunta     = "1";	
			int iContador = 0;
			if ("0".equals(myGuia.getTipoJunta())) tpJunta="1";				
			else {
				if ("1".equals(myGuia.getTipoJunta())) 	tpJunta="2" ;							
				else tpJunta="3" ;				
			}
			String indContinua = sys.Util.rPad(" "," ",27);
			List autos = new ArrayList();
			int reg=0;
			for (int k=0; k<myGuia.getStatusReqValido().length();k++) {
				indContinua = sys.Util.rPad(" "," ",27);
				continua   = true;				  
				while (continua) {
					parteVar = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6)+ 
					sys.Util.lPad(myGuia.getStatusReqValido().substring(k,k+1),"0",3)+
					tpJunta ;
					resultado = chamaTransBRK("049",parteVar,indContinua);
					
					String sResultadoSlv=resultado;
					AutoInfracaoBean myAt = new AutoInfracaoBean();							
					if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
						myGuia.setMsgErro(myAt.getMsgErro());
						return false;					
					}
					String linha = "";
					int i = 0;
					for (i=0; i < 26; i++) {				
						linha = resultado.substring((i*115)+3,(i+1)*115+3);					
						AutoInfracaoBean myAuto = new AutoInfracaoBean();
						myAuto.setInfracao(new InfracaoBean());	
						myAuto.setProprietario(new ResponsavelBean());
						myAuto.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());	
						myAuto.setSigOrgao(myUsrLog.getSigOrgaoAtuacao());
						setPosIni(0);											
						myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));	
						if (myAuto.getNumAutoInfracao().trim().length()>0) {
							myAuto.setNumPlaca(getPedaco(linha,7," "," "));
							VerificaAutoOracle(myAuto,conn);								
							myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
							myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," "));			
							myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
							myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
							myAuto.setNumProcesso(getPedaco(linha,20," "," "));
							myAuto.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));							
							//								myAuto.getProprietario().setNomResponsavel(getPedaco(linha,40," "," "));
							myAuto.setNumRequerimento(getPedaco(linha,30," "," "));
							myAuto.setTpRequerimento(getPedaco(linha,2," "," "));								
							// Buscar Resultado
							myAuto.setCodResultPubDO(getPedaco(linha,1," "," "));
							myAuto.setCodVinculada(getPedaco(linha,1," "," "));
							//								myAuto.setEventoOK("1");
							myAuto.setEventoOK("0");
							
							/*Chamar 041*/
							resultado = chamaTransBRK("041",
									sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
									sys.Util.rPad(myAuto.getNumPlaca()," ",7)+
									sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
									sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
									"         ","");
							
							setPosIni(581);
							String sCodStatus=getPedaco(resultado,3,"0",".");
							resultado=sResultadoSlv;
							if (myGuia.getTipoReqValidos().indexOf(myAuto.getTpRequerimento())>=0) {
								sCmd =  "insert into tsmi_guia (COD_AUTO_INFRACAO,NUM_AUTO,NUM_PLACA,COD_INFRACAO,DSC_ENQUADRAMENTO,"+
								"NUM_PROCESSO,DAT_INFRACAO,DAT_PROCESSO,NUM_REQUERIMENTO,TIP_REQUERIMENTO,COD_RESULTADO,"+
								"COD_VINCULADA,COD_STATUS) VALUES ("+
								"'"+myAuto.getInfracao().getCodAutoInfracao()+"',"+
								"'"+myAuto.getNumAutoInfracao()+"',"+
								"'"+myAuto.getNumPlaca()+"',"+
								"'"+myAuto.getInfracao().getCodInfracao()+"',"+			
								"'"+myAuto.getInfracao().getDscEnquadramento()+"',"+
								"'"+myAuto.getNumProcesso()+"',"+								
								"'"+myAuto.getDatInfracao()+"',"+
								"'"+myAuto.getDatProcesso()+"',"+							
								"'"+myAuto.getNumRequerimento()+"',"+
								"'"+myAuto.getTpRequerimento()+"',"+								
								"'"+myAuto.getCodResultPubDO()+"',"+
								"'"+myAuto.getCodVinculada()+"',"+
								"'"+sCodStatus+"')";								
								stmt.execute(sCmd);
								autos.add(myAuto);
								iContador++;									
							}
						}										
					}
					//setPosIni(3+(28*112));
					setPosIni(3+26*115); 
					indContinua = getPedaco(resultado,27," "," ") ;				
					if (indContinua.trim().length()<=0) continua=false;
					reg++;
				}
			}
			myGuia.setAutos(autos);	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/******************************************
	 ConsultaAuto com Requerimento em 0 ou 2 - Transa��es 049
	 ******************************************/
	public  boolean PreparaRemessa (RemessaBean myRemessa,ACSS.UsuarioBean myUsrLog) throws sys.DaoException 
	{
		//		  synchronized
		boolean bOk = true ;
		Connection conn =null ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt      = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;
			DaoBroker dao = DaoBrokerFactory.getInstance();
			List autos = new ArrayList();
			
			//Guia de Remessa / CEDOC
			String sTipoSolic="";
			if (myRemessa.getIndRecurso().equals("5") || myRemessa.getIndRecurso().equals("98"))
				sTipoSolic="'DP','DC'";
			else if (myRemessa.getIndRecurso().equals("15") || myRemessa.getIndRecurso().equals("96"))
				sTipoSolic="'1P','1C'";
			else if (myRemessa.getIndRecurso().equals("35") || myRemessa.getIndRecurso().equals("97"))
				sTipoSolic="'2P','2C'";
			
			//Indica o sit_xx de acordo com o tipo de remessa
			String sIndSit = "";
			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
					myRemessa.getIndRecurso().equals("97"))
				sIndSit = "SIT_CEDOC";
			else 
				sIndSit = "SIT_PROTOCOLO";
			
			
			String sStatus=myRemessa.getIndRecurso();
			if (sStatus.equals("15"))
				sStatus="'15','16'";					
			
			sCmd="SELECT /*+rule*/ NUM_AUTO_INFRACAO,NUM_PLACA,NUM_REQUERIMENTO FROM TSMI_AUTO_INFRACAO T1,TSMI_REQUERIMENTO T2 WHERE "+ 
			"T1.COD_AUTO_INFRACAO=T2.COD_AUTO_INFRACAO AND "+sIndSit+" IS NULL AND T1.COD_STATUS in ("+sStatus+") "+
			"AND T2.COD_TIPO_SOLIC IN ("+sTipoSolic+") "+
			"AND T1.COD_ORGAO='"+myRemessa.getCodOrgao()+"' " +
			"AND T2.COD_STATUS_REQUERIMENTO='"+myRemessa.getStatusReqValido()+"' ";
			if (myRemessa.getDatInicio().length()>0)
				sCmd+="AND T2.DAT_REQUERIMENTO>=TO_DATE('"+myRemessa.getDatInicio()+"','DD/MM/YYYY') ";
			sCmd+="ORDER BY NUM_PROCESSO";
			rs=stmt.executeQuery(sCmd);			
			while (rs.next())
			{
				AutoInfracaoBean myAuto      = new AutoInfracaoBean();
				if  (myAuto.getProprietario()==null) myAuto.setProprietario(new REC.ResponsavelBean());		
				if  (myAuto.getCondutor()==null)     myAuto.setCondutor(new REC.ResponsavelBean());		
				if  (myAuto.getVeiculo()==null)      myAuto.setVeiculo(new REC.VeiculoBean());		
				if  (myAuto.getInfracao()==null)     myAuto.setInfracao(new REC.InfracaoBean());		
				myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
				String sNumRequerimento=rs.getString("NUM_REQUERIMENTO");
				myAuto.setCodOrgao(myRemessa.getCodOrgao());
				dao.LeAutoInfracaoLocal(myAuto,"auto");
				RequerimentoBean myReq=new RequerimentoBean();
				myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
				myReq.setNumRequerimento(sNumRequerimento);
				dao.LeRequerimentoLocal(myReq,conn);
				myAuto.setNumRequerimento(myReq.getNumRequerimento());							
				myAuto.setCodRequerimento(myReq.getCodRequerimento());
				List requerimentos = new ArrayList();
				requerimentos.add(myReq);
				myAuto.setRequerimentos(requerimentos);
				autos.add(myAuto) ;	
			}			
			myRemessa.setAutos(autos);	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Remessa
	 *-----------------------------------------------------------
	 */
	public int RemessaGrava(RemessaBean myRemessa) throws sys.DaoException 
	{
		Connection conn =null ;
		List AutosProc = new ArrayList() ;		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			conn.setAutoCommit(false);
			ResultSet rs   = null;
			String sCmd    = "";
			
			//Indica o sit_xx de acordo com o tipo de remessa
			String sIndSit = "";
			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
					myRemessa.getIndRecurso().equals("97"))
				sIndSit = "SIT_CEDOC";
			else 
				sIndSit = "SIT_PROTOCOLO";
			
			if (myRemessa.getCodRemessa().length()>0)
			{
				// fazer iterator e pegar todos os processos
				Iterator it = myRemessa.getAutos().iterator();
				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
				Vector vErro = new Vector();
				while (it.hasNext()) {
					myAuto = (REC.AutoInfracaoBean)it.next() ;
					myAuto.setMsgErro("");				
					sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
					"DAT_RECEB_GUIA = '"+myRemessa.getDatProcRecebimento()+"',"+
					"NOM_USERNAME_RECEB_GUIA='"+myRemessa.getUsernameRecebimento()+"'"+
					" WHERE COD_REQUERIMENTO = '" +myAuto.getCodRequerimento()+"'";					       					
					stmt.execute(sCmd);
					AutosProc.add(myAuto);
				}
				if (myRemessa.getAutos().size()==AutosProc.size())
				{
					sCmd = "UPDATE TSMI_REMESSA SET "+
					"IND_STATUS_REMESSA='"+myRemessa.getStatusRemessa()+"',"+
					"USERNAME_RECEBIMENTO='"+myRemessa.getUsernameRecebimento()+"',"+
					"DAT_PROC_RECEBIMENTO=to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),"+
					"COD_ORGAO_LOT_RECEBIMENTO='"+myRemessa.getCodOrgaoLotacaoRecebimento()+"'"+				
					" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
					stmt.execute(sCmd);
				}
			}
			else
			{
				String codRemessa = "";	
				sCmd    = "SELECT SEQ_TSMI_REMESSA.NEXTVAL COD_REMESSA from dual" ;
				rs      = stmt.executeQuery(sCmd);
				while (rs.next())
				{	
					codRemessa = rs.getString("COD_REMESSA");
				}
				sCmd = "INSERT INTO TSMI_REMESSA (COD_REMESSA,IND_RECURSO, " +
				"COD_ORGAO,IND_STATUS_REMESSA,USERNAME,COD_ORGAO_LOTACAO, "+
				"DAT_PROC,USERNAME_RECEBIMENTO,DAT_PROC_RECEBIMENTO,COD_ORGAO_LOT_RECEBIMENTO) "+				
				" VALUES ("+codRemessa+"," +
				"'"+myRemessa.getIndRecurso()+"','" +myRemessa.getCodOrgao()+"','"+ myRemessa.getStatusRemessa()+ "'," +
				"'"+myRemessa.getUsername()+"','" +myRemessa.getCodOrgaoLotacao()+"',"+ 
				"to_date('"+sys.Util.formatedToday().substring(0,10)+"','DD/MM/YYYY'),'"+myRemessa.getUsernameRecebimento()+"',to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),'"+ myRemessa.getCodOrgaoLotacaoRecebimento()+"')";
				stmt.execute(sCmd);
				// fazer iterator e pegar todos os processos
				Iterator it = myRemessa.getAutos().iterator();
				myRemessa.setDatProc(sys.Util.formatedToday().substring(0,10));
				myRemessa.setCodRemessa(codRemessa);					
				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
				
				while (it.hasNext()) {
					myAuto = (REC.AutoInfracaoBean)it.next() ;
					myAuto.setMsgErro("");				
					sCmd = "INSERT INTO TSMI_REMESSA_ITEM (COD_REMESSA,COD_REMESSA_ITEM," +
					"COD_AUTO_INFRACAO,COD_REQUERIMENTO)" +					
					" VALUES("+codRemessa+",SEQ_TSMI_REMESSA_ITEM.NEXTVAL," +
					"'"+myAuto.getCodAutoInfracao()+"','"+myAuto.getCodRequerimento()+"')";					
					stmt.execute(sCmd) ;					
					sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
					"DAT_ENVIO_GUIA=to_date('"+sys.Util.formatedToday().substring(0,10)+"','DD/MM/YYYY')," +
					"NOM_USERNAME_ENVIO_GUIA = '"+myRemessa.getUsername()+"',COD_REMESSA='" +codRemessa+"'" +
					" WHERE COD_REQUERIMENTO = '" +myAuto.getCodRequerimento()+"'";					       					
					stmt.execute(sCmd);
					AutosProc.add(myAuto);
				}
				rs.close();
			}
			conn.commit();	
			myRemessa.setAutos(AutosProc);
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return AutosProc.size() ;
	}
	
	
	/**
	 *------------------------------------------------------------------------
	 * Insere novo processo na guia de Remessa
	 *------------------------------------------------------------------------
	 */
	public void incluirProcessoRemessa(AutoInfracaoBean myAuto,
			String codRemessa,String cRq,String codEvento,ACSS.UsuarioBean UsrLogado, String existeGuia)
	throws DaoException {	
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST); 
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeRequerimentoLocal(myAuto,conn);
			RemessaBean myRemessa=new RemessaBean();
			myRemessa.setCodRemessa(codRemessa);
			RemessaLe(myRemessa,conn);
			
			String sCodreq="";
			Iterator it = myAuto.getRequerimentos().iterator();		
			RequerimentoBean myReq=new RequerimentoBean();		
			while (it.hasNext()) {
				myReq = (REC.RequerimentoBean)it.next() ;
				if (myReq.getNumRequerimento().equals(cRq))
				{
					sCodreq=myReq.getCodRequerimento();
					break;
				}
			}
			
			//Indica o sit_xx de acordo com o tipo de remessa
			String sIndSit = "";
			if (myRemessa.getIndRecurso().equals("98") || myRemessa.getIndRecurso().equals("96") ||
					myRemessa.getIndRecurso().equals("97"))
				sIndSit = "SIT_CEDOC";
			else 
				sIndSit = "SIT_PROTOCOLO";
			
			String sCmd = "UPDATE TSMI_REQUERIMENTO SET "+sIndSit+" = '"+myRemessa.getStatusRemessa()+"'," +
			"DAT_ENVIO_GUIA = to_date('"+myRemessa.getDatProc()+"','DD/MM/YYYY'),"+
			"NOM_USERNAME_ENVIO_GUIA='"+myRemessa.getUsername()+"',"+
			"DAT_RECEB_GUIA = to_date('"+myRemessa.getDatProcRecebimento()+"','DD/MM/YYYY'),"+
			"NOM_USERNAME_RECEB_GUIA='"+myRemessa.getUsernameRecebimento()+"',"+
			"COD_REMESSA='"+codRemessa+"'"+
			" WHERE COD_REQUERIMENTO = '"+sCodreq+"'";
			stmt.execute(sCmd);
			
			sCmd =	"INSERT INTO TSMI_REMESSA_ITEM "+
			"(COD_REMESSA,COD_REMESSA_ITEM,COD_AUTO_INFRACAO,COD_REQUERIMENTO)" +
			" VALUES (" + codRemessa + ",SEQ_TSMI_REMESSA_ITEM.NEXTVAL,'" + myAuto.getCodAutoInfracao() + "','"+
			sCodreq+"')";
			
			stmt.execute(sCmd);
			conn.commit();
			stmt.close();		
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Consulta Guia de Remessa
	 *-----------------------------------------------------------
	 */	
	public String ConsultaReqRemessa(REC.AutoInfracaoBean myAuto, String codReq) throws sys.DaoException{
		
		Connection conn = null;
		ResultSet rs = null;
		String existe = "";
		String sql = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sql = "Select cod_requerimento from tsmi_requerimento where " +
			" num_requerimento = '"+codReq+"' ";
			rs = stmt.executeQuery(sql);
			String codRequeriemento = "";
			while(rs.next()){
				codRequeriemento = rs.getString("cod_requerimento");
			}
			
			
			sql = "select r.cod_remessa "+
			" FROM tsmi_remessa r, tsmi_remessa_item i " +
			" WHERE i.cod_auto_infracao = '"+ myAuto.getCodAutoInfracao()+"'" ;
			if(!codRequeriemento.equals(""))
				sql += " and i.cod_requerimento = '" +codRequeriemento+"' ";
			sql += " and r.cod_remessa = i.cod_remessa " +
			" and rownum = 1" ;
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				existe = rs.getString("cod_remessa");
			}
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Consulta Guias de Remessa de acordo com Status da guia e tipoJunta
	 *------------------------------------------------------------------------
	 */
	public List consultaRemessaIncluirProcesso(String tipoSolic)throws DaoException {
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_REMESSA WHERE ";    
			if  ("DP,DC".indexOf(tipoSolic)>=0)
				sCmd+= " IND_RECURSO = 5";
			
			if  ("1P,1C".indexOf(tipoSolic)>=0) 
				sCmd+= " IND_RECURSO = 15 ";
			
			if  ("2P,2C".indexOf(tipoSolic)>=0)
				sCmd+= " IND_RECURSO = 35 ";
			
			sCmd += " AND IND_STATUS_REMESSA = 0 ORDER BY COD_REMESSA DESC";
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				StatusBean mystat = new StatusBean();
				mystat.setCodStatus(rs.getString("COD_REMESSA"));
				mystat.setNomStatus(rs.getString("IND_STATUS_REMESSA"));
				vList.add(mystat) ;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Consulta Guia de Remessa
	 *-----------------------------------------------------------
	 */	
	public void RemessaLe(RemessaBean myRemessa,Connection conn) throws sys.DaoException{
		
		ResultSet rs = null;
		String sCmd  = "";
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT * FROM TSMI_REMESSA WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next())
			{
				myRemessa.setCodOrgao(rs.getString("COD_ORGAO"));
				myRemessa.setIndRecurso(rs.getString("IND_RECURSO"));
				myRemessa.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myRemessa.setDatProc(converteData(rs.getDate("DAT_PROC")));
				myRemessa.setDatProcRecebimento(converteData(rs.getDate("DAT_PROC_RECEBIMENTO")));
				myRemessa.setUsername(rs.getString("USERNAME"));
				myRemessa.setUsernameRecebimento(rs.getString("USERNAME_RECEBIMENTO"));
				myRemessa.setCodOrgaoLotacaoRecebimento(rs.getString("COD_ORGAO_LOT_RECEBIMENTO"));
				myRemessa.setStatusRemessa(rs.getString("IND_STATUS_REMESSA"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Ler Guia de Remessa
	 *-----------------------------------------------------------
	 */
	public void RemessaLe(RemessaBean myRemessa,ACSS.UsuarioBean myUsrLog,ACSS.UsuarioFuncBean myUsrFunc) throws sys.DaoException {
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select IND_RECURSO,USERNAME,COD_ORGAO," +
			"COD_ORGAO_LOTACAO,TO_CHAR(DAT_PROC,'DD/MM/YYYY') DAT_PROC,"+
			"TO_CHAR(DAT_PROC_RECEBIMENTO,'DD/MM/YYYY') DAT_PROC_RECEBIMENTO,"+
			"USERNAME_RECEBIMENTO,COD_ORGAO_LOT_RECEBIMENTO,IND_STATUS_REMESSA " +
			"FROM TSMI_REMESSA "+
			"WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"' and "+
			"COD_ORGAO='"+myRemessa.getCodOrgao()+"'";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myRemessa.setCodOrgao(rs.getString("COD_ORGAO"));
				myRemessa.setIndRecurso(rs.getString("IND_RECURSO"));
				myRemessa.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myRemessa.setDatProc(rs.getString("DAT_PROC"));
				myRemessa.setDatProcRecebimento(rs.getString("DAT_PROC_RECEBIMENTO"));
				myRemessa.setUsername(rs.getString("USERNAME"));
				myRemessa.setUsernameRecebimento(rs.getString("USERNAME_RECEBIMENTO"));
				myRemessa.setCodOrgaoLotacaoRecebimento(rs.getString("COD_ORGAO_LOT_RECEBIMENTO"));
				myRemessa.setStatusRemessa(rs.getString("IND_STATUS_REMESSA"));
				
			}	
			String sigOrgao="";
			String nomOrgao="";
			sCmd = "Select O.SIG_ORGAO,O.NOM_ORGAO FROM TSMI_ORGAO O "+
			"WHERE O.COD_ORGAO='"+myRemessa.getCodOrgao()+"' ";			  		              
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				sigOrgao = rs.getString("SIG_ORGAO");	
				nomOrgao = rs.getString("NOM_ORGAO");				 
			}
			
			ArrayList Autos = new ArrayList();
			sCmd="SELECT T1.COD_REMESSA_ITEM, "+
			"T1.COD_AUTO_INFRACAO,T1.COD_REQUERIMENTO,T2.NUM_REQUERIMENTO,T3.NUM_AUTO_INFRACAO,T3.NUM_PLACA "+
			"FROM SMIT.TSMI_REMESSA_ITEM T1, TSMI_REQUERIMENTO T2,TSMI_AUTO_INFRACAO T3 "+
			"WHERE T1.COD_REMESSA='"+myRemessa.getCodRemessa()+"' AND T1.COD_REQUERIMENTO=T2.COD_REQUERIMENTO AND "+
			"T1.COD_AUTO_INFRACAO=T3.COD_AUTO_INFRACAO ";	   
			if(myRemessa.getGeraGuia().equals("D"))
				sCmd += " AND T1.SIT_GUIA_DISTRIBUICAO IS NULL";
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {   
				DaoBroker dao = DaoBrokerFactory.getInstance();
				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
				myAuto.setInfracao(new InfracaoBean());	
				myAuto.setProprietario(new ResponsavelBean());
				
				String sCodAutoInfracao=rs.getString("COD_AUTO_INFRACAO");
				String sCodRequerimento=rs.getString("COD_REQUERIMENTO");
				String sNumRequerimento=rs.getString("NUM_REQUERIMENTO");
				String sNumAutoInfracao=rs.getString("NUM_AUTO_INFRACAO");
				String sNumPlaca=rs.getString("NUM_PLACA");
				
				myAuto.setNumAutoInfracao(sNumAutoInfracao);
				myAuto.setNumPlaca(sNumPlaca);
				myAuto.setNumRequerimento(sNumRequerimento);
				myAuto.setCodRequerimento(sCodRequerimento);				
				myAuto.setCodOrgao(myRemessa.getCodOrgao());	
				dao.LeAutoInfracaoLocal(myAuto,"auto");
				
				List reqs = new ArrayList();
				REC.RequerimentoBean myReq = new REC.RequerimentoBean();
				myReq.setCodAutoInfracao(sCodAutoInfracao);
				myReq.setNumRequerimento(sNumRequerimento);
				dao.LeRequerimentoLocal(myReq,conn);
				reqs.add(myReq);
				myAuto.setRequerimentos(reqs);
				
				Autos.add(myAuto);
			}
			myRemessa.setAutos(Autos);
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Ler Guia de Distribuicao
	 *-----------------------------------------------------------
	 */
	public void GuiaLe(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt  = conn.createStatement();
			Statement stmt1 = conn.createStatement();
			ResultSet rs    = null;			
			ResultSet rs1   = null;			
			DaoBroker dao = DaoBrokerFactory.getInstance();
			String codItemRelator="0";
			
			String sCmd = "Select L.COD_STATUS,L.COD_LOTE_RELATOR,L.NOM_USERNAME, " +
			"L.COD_ORGAO_LOTACAO,to_char(L.DT_PROC,'dd/mm/yyyy') DT_PROC,"+
			"to_char(L.DAT_RS,'dd/mm/yyyy') DAT_RS,"+
			"L.COD_ORGAO,L.COD_JUNTA,J.SIG_JUNTA,J.IND_TIPO," +
			"L.NUM_CPF,R.COD_RELATOR,R.NOM_RELATOR,L.COD_ENVIO_PUBDO," +
			"to_char(L.DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO, L.TIPO_GUIA," +
			"to_char(L.DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO, L.NUM_SESSAO,to_char(L.DAT_SESSAO,'DD/MM/YYYY') DAT_SESSAO " +
			"FROM TSMI_LOTE_RELATOR L,TSMI_RELATOR R,TSMI_JUNTA J "+
			"WHERE L.COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' and "+
			"L.COD_ORGAO='"+myGuia.getCodOrgaoAtuacao()+"' and " +
			"L.COD_JUNTA=J.COD_JUNTA AND L.COD_ORGAO=J.COD_ORGAO AND "+
			"L.NUM_CPF=R.NUM_CPF AND L.COD_JUNTA=R.COD_JUNTA " ;		              
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myGuia.setCodStatus(rs.getString("COD_STATUS"));	
				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));	
				
				myGuia.setDatProc(rs.getString("DT_PROC"));	
				myGuia.setDatRS(rs.getString("DAT_RS"));	
				myGuia.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				
				myGuia.setCodJunta(rs.getString("COD_JUNTA"));
				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
				myGuia.setTipoJunta(rs.getString("IND_TIPO"));
				
				myGuia.setNumCPFRelator(rs.getString("NUM_CPF"));
				myGuia.setCodRelator(rs.getString("COD_RELATOR"));
				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				myGuia.setNumSessao(rs.getString("NUM_SESSAO"));
				myGuia.setDatSessao(rs.getString("DAT_SESSAO"));
				myGuia.setTipoGuia(rs.getString("TIPO_GUIA"));
			}
			
			if(myGuia.getNumSessao().length()>0)
			{
				sCmd = "SELECT to_char(DAT_SESSAO,'dd/mm/yyyy') DAT_SESSAO FROM TSMI_LOTE_RELATOR WHERE "+
				" NUM_SESSAO ='"+myGuia.getNumSessao()+"' ";                               
				rs   = stmt.executeQuery(sCmd) ;
				while (rs.next()) 
					myGuia.setDatSessao(rs.getString("DAT_SESSAO"));              
			}
			
			String sigOrgao="";
			String nomOrgao="";
			sCmd = "Select O.SIG_ORGAO,O.NOM_ORGAO FROM TSMI_ORGAO O "+
			"WHERE O.COD_ORGAO='"+myGuia.getCodOrgaoAtuacao()+"' ";			  		              
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				sigOrgao = rs.getString("SIG_ORGAO");	
				nomOrgao = rs.getString("NOM_ORGAO");				 
			}
			
			ArrayList Autos = new ArrayList() ;
			sCmd = "Select COD_ITEM_LOTE_RELATOR," +
			"NUM_AUTO_INFRACAO,NUM_PROCESSO,to_char(DAT_PROCESSO,'dd/mm/yyyy') DAT_PROCESSO, " +
			"NUM_PLACA,to_char(DAT_INFRACAO,'dd/mm/yyyy') DAT_INFRACAO, " +
			"DSC_INFRACAO,DSC_LOCAL_INFRACAO," +
			"NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO," +
			"NOM_PROPRIETARIO,DSC_ENQUADRAMENTO,MSG_ERRO, " +
			"COD_RESULT_RS,TXT_MOTIVO_RS "+
			"FROM TSMI_ITEM_LOTE_RELATOR "+
			"WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'"+
			" order by NUM_PROCESSO ASC";
			rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
				myAuto.setInfracao(new InfracaoBean());	
				myAuto.setProprietario(new ResponsavelBean());
				List reqs = new ArrayList();
				REC.RequerimentoBean req = new REC.RequerimentoBean();
				myAuto.setCodOrgao(myGuia.getCodOrgaoAtuacao());
				myAuto.setSigOrgao(sigOrgao);
				myAuto.setNomOrgao(nomOrgao);
				String codItemLoteRelator=rs.getString("COD_ITEM_LOTE_RELATOR");
				myAuto.setCodAutoInfracao(codItemLoteRelator);				
				myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));			
				myAuto.setNumProcesso(rs.getString("NUM_PROCESSO"));	
				myAuto.setDatProcesso(rs.getString("DAT_PROCESSO"));	 
				myAuto.setNumPlaca(rs.getString("NUM_PLACA"));	
				myAuto.setDatInfracao(rs.getString("DAT_INFRACAO"));
				myAuto.getInfracao().setDscInfracao(rs.getString("DSC_INFRACAO"));	
				myAuto.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));
				myAuto.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				myAuto.setTpRequerimento(rs.getString("IND_TIPO_REQUERIMENTO"));
				myAuto.getProprietario().setNomResponsavel(rs.getString("NOM_PROPRIETARIO"));			
				myAuto.getInfracao().setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));			
				myAuto.setMsgErro(rs.getString("MSG_ERRO"));
				req.setCodResultRS(rs.getString("COD_RESULT_RS"));
				req.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				req.setNumRequerimento(myAuto.getNumRequerimento());
				UsuarioBean myUsrLogado = new UsuarioBean();				
				dao.LeRequerimento(myAuto,myUsrLogado);				
				int seq = 0;
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean reqProcesso = new REC.RequerimentoBean();
				while (it.hasNext()) {				
					reqProcesso =(REC.RequerimentoBean)it.next();
					if (myAuto.getNumRequerimento().equals(myAuto.getRequerimentos(seq).getNumRequerimento()))	{						
						req.setCodRequerimento(reqProcesso.getCodRequerimento());
						req.setCodTipoSolic(reqProcesso.getCodTipoSolic());
						req.setDatRequerimento(reqProcesso.getDatRequerimento());
						req.setCodStatusRequerimento(reqProcesso.getCodStatusRequerimento());
						req.setNomUserNameDP(reqProcesso.getNomUserNameDP());
						req.setCodOrgaoLotacaoDP(reqProcesso.getCodOrgaoLotacaoDP());
						req.setNomResponsavelDP(reqProcesso.getNomResponsavelDP());			
						req.setDatProcDP(reqProcesso.getDatProcDP());
						req.setDatPJ(reqProcesso.getDatPJ());                
						req.setCodParecerPJ(reqProcesso.getCodParecerPJ());
						req.setCodRespPJ(reqProcesso.getCodRespPJ());
						req.setTxtMotivoPJ(reqProcesso.getTxtMotivoPJ());
						req.setNomUserNamePJ(reqProcesso.getNomUserNamePJ());
						req.setCodOrgaoLotacaoPJ(reqProcesso.getCodOrgaoLotacaoPJ());
						req.setDatProcPJ(reqProcesso.getDatProcPJ());
						req.setDatJU(reqProcesso.getDatJU());
						req.setCodJuntaJU(reqProcesso.getCodJuntaJU());
					    req.setCodRelatorJU(reqProcesso.getCodRelatorJU());
						req.setNomUserNameJU(reqProcesso.getNomUserNameJU());
						req.setCodOrgaoLotacaoJU(reqProcesso.getCodOrgaoLotacaoJU());
						req.setDatProcJU(reqProcesso.getDatProcJU());
						req.setDatRS(reqProcesso.getDatRS());
						req.setNomUserNameRS(reqProcesso.getNomUserNameRS());
						req.setCodOrgaoLotacaoRS(reqProcesso.getCodOrgaoLotacaoRS());
						req.setDatProcRS(reqProcesso.getDatProcRS());
						req.setNomCondutorTR(reqProcesso.getNomCondutorTR());
						req.setIndTipoCNHTR(reqProcesso.getIndTipoCNHTR());
						req.setNumCNHTR(reqProcesso.getNumCNHTR());
						req.setCodUfCNHTR(reqProcesso.getCodUfCNHTR());
						req.setNumCPFTR(reqProcesso.getNumCPFTR());	
						req.setDatAtuTR(reqProcesso.getDatAtuTR());
						req.setNomUserNameTR(reqProcesso.getNomUserNameTR());
						req.setCodOrgaoLotacaoTR(reqProcesso.getCodOrgaoLotacaoTR());
						req.setSitProtocolo(reqProcesso.getSitProtocolo());
						req.setDatRecebimentoGuia(reqProcesso.getDatRecebimentoGuia());
						req.setDatEnvioGuia(reqProcesso.getDatEnvioGuia());
						req.setNomUserNameRecebimento(reqProcesso.getNomUserNameRecebimento());
						req.setNomUserNameEnvio(reqProcesso.getNomUserNameEnvio());
						req.setCodRemessa(reqProcesso.getCodRemessa());	
		                req.setDatEnvioDO(reqProcesso.getDatEnvioDO());
		                req.setCodEnvioDO(reqProcesso.getCodEnvioDO());
		                req.setDatPublPDO(reqProcesso.getDatPublPDO());
		                req.setCodMotivoDef(reqProcesso.getCodMotivoDef());
						sCmd =  "SELECT * FROM TSMI_REQUERIMENTO_WEB WHERE "+
						"NUM_REQUERIMENTO='"+req.getNumRequerimento()+"'";
						rs1 = stmt1.executeQuery(sCmd);			 
						if (rs1.next())
						{
							req.setNomRequerente(rs1.getString("NOM_REQUERENTE"));
							req.setTxtJustificativa(rs1.getString("TXT_MOTIVO"));
							req.setTxtEmail(rs1.getString("TXT_EMAIL"));
						}
		                /*Gravar Auto Infra��o*/
		                dao.GravaAutoInfracaoLocal(myAuto);
		                dao.LeAutoInfracaoLocal(myAuto,"auto");
		                req.setCodAutoInfracao(myAuto.getCodAutoInfracao());		                
		                myAuto.setCodAutoInfracao(codItemLoteRelator);		                
		                
		                
		                /*Gravar Requerimento
		                dao.GravarRequerimento(req,conn);
		                dao.LeRequerimentoLocal(req,conn);		   
		                req.setCodRequerimento(req.getCodRequerimento());*/
						break;
					}
					seq++;
				}					
				// Verifica se tem AI Digitalizado
				sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
				" WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
				rs1    = stmt1.executeQuery(sCmd) ;
				while (rs1.next()) {
					myAuto.setTemAIDigitalizado(true);			
				}				
				
				reqs.add(req);			
				myAuto.setRequerimentos(reqs);
				Autos.add(myAuto);
			}
			myGuia.setAutos(Autos);
			rs.close();
			if (rs1!=null) rs1.close();
			stmt.close();
			stmt1.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao
	 *-----------------------------------------------------------
	 */
	
	public int GuiaGrava(GuiaDistribuicaoBean myGuia, UsuarioBean UsrLogado) throws sys.DaoException {
		Connection conn =null ;
		String sCmd = "";
		int inc = 0;
		try {
			if (myGuia.getAutos().size()==0) return 0;
			
			conn = serviceloc.getConnection(MYABREVSIST);		
			Statement stmt = conn.createStatement();			
			
			conn.setAutoCommit(false);	
			
			sCmd    = "Select SEQ_TSMI_LOTE_RELATOR.NEXTVAL codLote from dual" ;
			String codLote = "0";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				codLote = rs.getString("codLote");	 
			}
			myGuia.setNumGuia(codLote);
			
			sCmd = "INSERT INTO TSMI_LOTE_RELATOR (COD_LOTE_RELATOR,COD_STATUS, " +
			"NOM_USERNAME, COD_ORGAO_LOTACAO, DT_PROC,COD_ORGAO, "+
			"COD_JUNTA, NUM_CPF, DSC_TIPOS_REQUERIMENTO,NUM_SESSAO,DAT_SESSAO,TIPO_GUIA) "+				
			" VALUES ("+codLote+",'2'," +
			"'"+myGuia.getNomUserName()+"','" + myGuia.getCodOrgaoLotacao()+ "'," +
			"to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),'"+myGuia.getCodOrgaoAtuacao()+"',"+
			"'"+myGuia.getCodJunta()+"','" + myGuia.getNumCPFRelator()+ "','"+myGuia.getTipoReqValidos()+"','"+myGuia.getNumSessao()+"'" +
			",to_date('"+myGuia.getDatSessao()+"','dd/mm/yyyy'),'"+myGuia.getTipoGuia()+"')" ;
			stmt.execute(sCmd) ;
			
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator();
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int s = 0;    
			List AutosProc = new ArrayList() ;
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next();
				/*Gravar dados do Auto de Infra��o*/
				VerificaAutoOracle(myAuto,conn);
				
				myAuto.setMsgErro("");
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
				sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);			
				
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				
				if("000".equals(resultado.substring(0,3))){
					int qtdAtualProc=0;					
					sCmd = "SELECT QTD_PROC_DISTRIB FROM TSMI_RELATOR WHERE COD_RELATOR ='"+myGuia.getCodRelator()+"'";
					rs   = stmt.executeQuery(sCmd) ;
					if(rs.next())
						qtdAtualProc = rs.getInt("QTD_PROC_DISTRIB")+1;		
						sCmd = "UPDATE TSMI_RELATOR  SET QTD_PROC_DISTRIB = "+qtdAtualProc+" WHERE COD_RELATOR ='"+myGuia.getCodRelator()+"'";				
						stmt.execute(sCmd);
				}
				
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Definir/Troca de Relator.")==false) {
					vErro.add(myAuto.getMsgErro());
				}
				else {
					inc++;					
					sCmd = "INSERT INTO TSMI_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR," +
					"NUM_AUTO_INFRACAO,NUM_PROCESSO,DAT_PROCESSO,NUM_PLACA,"+
					"NOM_PROPRIETARIO,DSC_ENQUADRAMENTO,"+					
					"DAT_INFRACAO,DSC_INFRACAO,DSC_LOCAL_INFRACAO,NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO,"+
					"MSG_ERRO) " +					
					" VALUES("+codLote+",SEQ_TSMI_ITEM_LOTE_RELATOR.NEXTVAL," +
					"'"+myAuto.getNumAutoInfracao()+"','"+myAuto.getNumProcesso()+"',"+
					"to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),"+
					"'"+myAuto.getNumPlaca()+"'," +
					"'"+myAuto.getProprietario().getNomResponsavel()+"','"+myAuto.getInfracao().getDscEnquadramento()+"'," +					
					"to_date('"+myAuto.getDatInfracao()+"','dd/mm/yyyy'),"+						
					"'"+myAuto.getInfracao().getDscInfracao()+"'," +
					"'"+myAuto.getDscLocalInfracao()+"'," +
					"'"+myAuto.getNumRequerimento()+"','" +myAuto.getTpRequerimento()+"',"+
					"'"+myAuto.getMsgErro()+"')" ;					
					stmt.execute(sCmd) ;

					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");	

					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento03(myGuia.getCodJunta());	
					myHis.setTxtComplemento04(myGuia.getNumCPFRelator());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
					myReq.setNumRequerimento(myAuto.getNumRequerimento());
					dao.LeRequerimentoLocal(myReq,conn);							
					myAuto.LeRequerimento(UsrLogado);
					myReq.setNumRequerimento(myAuto.getNumRequerimento());
					
					Iterator itReq = myAuto.getRequerimentos().iterator();
					RequerimentoBean myRequerimento = new RequerimentoBean();
					while (it.hasNext())
					{
						myRequerimento = (REC.RequerimentoBean)itReq.next();
						if (myRequerimento.getNumRequerimento().equals(myReq.getNumRequerimento()))
						{
							myReq.setCodTipoSolic(myRequerimento.getCodTipoSolic());
							myReq.setDatRequerimento(myRequerimento.getDatRequerimento());
							myReq.setNomUserNameDP(myRequerimento.getNomUserNameDP());
							myReq.setCodOrgaoLotacaoDP(myRequerimento.getCodOrgaoLotacaoDP());
							myReq.setDatProcDP(myRequerimento.getDatProcDP());
							break;
						}
					}
					myReq.setNomUserNameJU(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("2");
					myReq.setCodOrgaoLotacaoJU(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcJU(myGuia.getDatProc());
					myReq.setDatJU(myGuia.getDatProc());	
					myReq.setCodJuntaJU(myGuia.getCodJunta());	
					myReq.setCodRelatorJU(myGuia.getNumCPFRelator());

					/*Atualizar Dados de Remessa*/
					if(myGuia.getTipoGuia().equals("R"))
					{
						RemessaBean myRemessa = new RemessaBean();
						sCmd = "UPDATE TSMI_REMESSA_ITEM  SET SIT_GUIA_DISTRIBUICAO = '" +myRemessa.SIT_GUIA_DISTRIBUICAO+"'"+
						"WHERE COD_REMESSA ='"+myGuia.getNumGuia()+"' AND COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao()+
						" AND COD_REQUERIMENTO = "+myReq.getCodRequerimento();
						stmt.execute(sCmd);
					}
					
                    /*Gravar Auto de Infracao*/					
					dao.GravaAutoInfracaoLocal(myAuto, conn);
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
				}
				AutosProc.add(myAuto);
			}	
			if (inc==0) conn.rollback();	
			else 		conn.commit();
			myGuia.setMsgErro(vErro);				
			myGuia.setAutos(AutosProc);
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			System.out.println("* SQL Inv�lido :"+sCmd);			
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc ;
	}
	
	public int AtualizarGuia(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			
			conn.setAutoCommit(false);	
			
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int s = 0;    
			List AutosProc = new ArrayList() ;
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
				sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11);			
				
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Definir/Troca de Relator.")==false) {
					vErro.add(myAuto.getMsgErro());
				}
				else {
					inc++;					
					String sCmd = "INSERT INTO TSMI_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR," +
					"NUM_AUTO_INFRACAO,NUM_PROCESSO,DAT_PROCESSO,NUM_PLACA,"+
					"NOM_PROPRIETARIO,DSC_ENQUADRAMENTO,"+					
					"DAT_INFRACAO,DSC_INFRACAO,DSC_LOCAL_INFRACAO,NUM_REQUERIMENTO,IND_TIPO_REQUERIMENTO,"+
					"MSG_ERRO) " +					
					" VALUES("+myGuia.getNumGuia()+",SEQ_TSMI_ITEM_LOTE_RELATOR.NEXTVAL," +
					"'"+myAuto.getNumAutoInfracao()+"','"+myAuto.getNumProcesso()+"',"+
					"to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),"+
					"'"+myAuto.getNumPlaca()+"'," +
					"'"+myAuto.getProprietario().getNomResponsavel()+"','"+myAuto.getInfracao().getDscEnquadramento()+"'," +					
					"to_date('"+myAuto.getDatInfracao()+"','dd/mm/yyyy'),"+						
					"'"+myAuto.getInfracao().getDscInfracao()+"'," +
					"'"+myAuto.getDscLocalInfracao()+"'," +
					"'"+myAuto.getNumRequerimento()+"','" +myAuto.getTpRequerimento()+"',"+
					"'"+myAuto.getMsgErro()+"')" ;	
					stmt.execute(sCmd) ;
					
					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");					
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento03(myGuia.getCodJunta());	
					myHis.setTxtComplemento04(myGuia.getNumCPFRelator());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
					myReq.setNumRequerimento(myAuto.getNumRequerimento());	
					dao.LeRequerimentoLocal(myReq,conn);
					myReq.setNomUserNameJU(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("2");
					myReq.setCodOrgaoLotacaoJU(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcJU(myGuia.getDatProc());
					myReq.setDatJU(myGuia.getDatProc());	
					myReq.setCodJuntaJU(myGuia.getCodJunta());	
					myReq.setCodRelatorJU(myGuia.getNumCPFRelator());
					
					dao.GravaAutoInfracaoLocal(myAuto, conn);
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
				}
				AutosProc.add(myAuto);
			}	
			if (inc==0) conn.rollback();	
			else 		conn.commit();
			myGuia.setMsgErro(vErro);				
			myGuia.setAutos(AutosProc);
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc ;
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Resultado
	 *-----------------------------------------------------------
	 */
	public int GuiaGravaResult(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='3', " +
			" NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO='"+myGuia.getCodOrgaoLotacao()+"',"+
			" DAT_RS= to_date('"+myGuia.getDatRS()+"','dd/mm/yyyy'), "+
			" DAT_SESSAO = to_date('"+myGuia.getDatSessao()+"','dd/mm/yyyy') "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			//System.out.println("GuiaDaoBroker: "+sCmd);
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;		
			REC.AutoInfracaoBean myAuto        = new REC.AutoInfracaoBean();		
			REC.AutoInfracaoBean myAutoBroker  = new REC.AutoInfracaoBean();			
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				// Gera transacao 209/329/336 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatRS()) +							
				sys.Util.rPad(myAuto.getRequerimentos(0).getCodResultRS()," ",1) +
				sys.Util.rPad(myAuto.getRequerimentos(0).getTxtMotivoRS()," ",1000);
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o do Resultado.")==false) {
					vErro.add(myAuto.getMsgErro());
				}
				else {
					inc++;					
					sCmd = "UPDATE TSMI_ITEM_LOTE_RELATOR set " +
					"COD_RESULT_RS='"+myAuto.getRequerimentos(0).getCodResultRS()+"'," +
					"TXT_MOTIVO_RS='"+myAuto.getRequerimentos(0).getTxtMotivoRS()+"' "+
					"WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' and " + 
					"COD_ITEM_LOTE_RELATOR='"+myAuto.getCodAutoInfracao()+"'" ;
					//System.out.println("GuiaDaoBroker: "+sCmd);
					stmt.execute(sCmd);
					
					
					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");	
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatRS());	
					myHis.setTxtComplemento03(myAuto.getRequerimentos(0).getCodResultRS());	
					myHis.setTxtComplemento04(myAuto.getRequerimentos(0).getTxtMotivoRS());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setNumRequerimento(myAuto.getNumRequerimento());	
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
					dao.LeRequerimentoLocal(myReq,conn);
					myReq.setNomUserNameRS(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("3");
					myReq.setCodOrgaoLotacaoRS(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcRS(myGuia.getDatProc());
					myReq.setNumRequerimento(myAuto.getNumRequerimento());	
					myReq.setDatRS(myGuia.getDatRS());
					myReq.setCodResultRS(myAuto.getRequerimentos(0).getCodResultRS());	
					myReq.setTxtMotivoRS(myAuto.getRequerimentos(0).getTxtMotivoRS());
					if(myAuto.getRequerimentos(0).getCodResultRS().equals("D"))
					   myReq.setCodMotivoDef(myAuto.getRequerimentos(0).getCodMotivoDef());
					
					/*Gravar Dados do Auto de Infra��o*/
					myAutoBroker.setNumAutoInfracao(myAuto.getNumAutoInfracao());
					myAutoBroker.setNumPlaca(myAuto.getNumPlaca());
					myAutoBroker.setCodOrgao(myAuto.getCodOrgao());
					UsuarioBean myUsuario=new UsuarioBean();
					myUsuario.setCodOrgaoAtuacao(myAutoBroker.getCodOrgao());
					myUsuario.getOrgao().setCodOrgao(myAutoBroker.getCodOrgao());
					dao.LeAutoInfracao(myAutoBroker,"auto",myUsuario);
					dao.GravaAutoInfracaoLocal(myAutoBroker, conn);
					if (myAutoBroker.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
				}
			}	
			if (inc==0) conn.rollback();	
			else 		conn.commit();	
			myGuia.setMsgErro(vErro);
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc ;
	}

	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Alterar Relator
	 *-----------------------------------------------------------
	 */
	public int GuiaGravaTrocaRelator(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='"+myGuia.getCodStatus()+"', " +
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
			" COD_JUNTA         = '"+myGuia.getCodJunta()+"',"+
			" NUM_CPF           = '"+myGuia.getNumCPFRelator()+"' "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.lPad(myGuia.getCodJunta(),"0",6) +
				sys.Util.lPad(myGuia.getNumCPFRelator(),"0",11) ;			 			
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Troca de Junta/Relator.")==false) {
					vErro.add(myAuto.getMsgErro());
				}
				else
				{
					inc++;
					
					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");					
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento03(myGuia.getCodJunta());	
					myHis.setTxtComplemento04(myGuia.getNumCPFRelator());
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
					myReq.setNumRequerimento(myAuto.getNumRequerimento());	
					dao.LeRequerimentoLocal(myReq,conn);
					myReq.setNomUserNameJU(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento("2");
					myReq.setCodOrgaoLotacaoJU(myGuia.getCodOrgaoLotacao());
					myReq.setDatProcJU(myGuia.getDatProc());
					myReq.setDatJU(myGuia.getDatProc());	
					myReq.setCodJuntaJU(myGuia.getCodJunta());	
					myReq.setCodRelatorJU(myGuia.getNumCPFRelator());
					
					dao.GravaAutoInfracaoLocal(myAuto, conn);
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	
	
	
	public void RemessaEstorno(RemessaBean myRemessa) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			conn.setAutoCommit(false);
			
			String sCmd = "UPDATE TSMI_REQUERIMENTO set COD_REMESSA=null,SIT_PROTOCOLO=null "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			sCmd = "DELETE FROM TSMI_REMESSA_ITEM "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			sCmd = "DELETE FROM TSMI_REMESSA "+
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			
			conn.commit();		
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	
	
	
	public void RemessaEstornoRecebimento(RemessaBean myRemessa) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_REMESSA set IND_STATUS_REMESSA='0'," +
			"USERNAME_RECEBIMENTO=null,"+                  
			"DAT_PROC_RECEBIMENTO=null,"+                   
			"COD_ORGAO_LOT_RECEBIMENTO=null "+             
			" WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			conn.commit();		
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	
	
	
	
	
	
	
	public int GuiaEstornoTrocaRelator(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='8', " +
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
			" COD_JUNTA         = '"+myGuia.getCodJunta()+"',"+
			" NUM_CPF           = '"+myGuia.getNumCPFRelator()+"' "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad("215","0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.rPad(" "," ",50) +
				sys.Util.rPad("ESTORNO DE JUNTA/RELATOR"," ",11) ;			 			
				resultado = chamaTransBRK("215",parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Troca de Junta/Relator.")==false) {
					vErro.add(myAuto.getMsgErro());
				}
				else {	inc++;	}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	/**
	 *------------------------------------------------------------------------
	 * Consulta Guias de acordo com Status da guia e tipoJunta
	 * @author Elisson Dias
	 * Date: 10/09/2004 - 10:20
	 *------------------------------------------------------------------------
	 */
	public List consultaGuiaIncluirProcesso(GuiaDistribuicaoBean GuiaDistribuicaoId, String statusGuia, String tipoJunta, String numCPF, String existeGuia,UsuarioBean UsrLogado)
	throws DaoException {
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String filtro = " AND L.COD_STATUS=" + statusGuia + " AND J.IND_TIPO=" + tipoJunta;
			
			String sCmd =
				"SELECT COD_LOTE_RELATOR,"
				+ "' Guia: '||L.cod_lote_relator||'  '||rpad(j.sig_junta,10,' ')||'-'||r.nom_relator||"
				+ "' Status: '||decode(L.cod_status,2,'Def. Junta/Relator',3,'Resultado',4,'Enviado p/ Publica��o',8,'Estornado','Publicacado')  NUM_GUIA "
				+ " FROM TSMI_LOTE_RELATOR L, TSMI_RELATOR R, TSMI_JUNTA J";
			if (GuiaDistribuicaoId.getControleGuia().equals("S"))
				sCmd +=  ",TSMI_CONTROLE_RELATOR C ";
			sCmd += " WHERE L.COD_JUNTA=J.COD_JUNTA AND l.COD_ORGAO='"+UsrLogado.getCodOrgaoAtuacao() +"' "
			+ " AND L.NUM_CPF=R.NUM_CPF"
			+ " AND L.COD_JUNTA=R.COD_JUNTA AND R.IND_ATIVO = 'S' ";
			if (GuiaDistribuicaoId.getControleGuia().equals("S"))
			{
				sCmd += " AND C.COD_RELATOR = R.COD_RELATOR" +
				" AND C.COD_USUARIO = "+UsrLogado.getCodUsuario();
			}
			
			if (!numCPF.equals("")){ 
				sCmd = sCmd + " AND L.NUM_CPF='"+numCPF+"'";
			}
			if (!existeGuia.equals("")){ 
				sCmd = sCmd + " AND L.cod_lote_relator not in ("+existeGuia+")";
			}
			sCmd = sCmd + filtro;
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				StatusBean mystat = new StatusBean();
				mystat.setCodStatus(rs.getString("COD_LOTE_RELATOR"));
				mystat.setNomStatus(rs.getString("NUM_GUIA"));
				vList.add(mystat) ;
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	/**
	 *------------------------------------------------------------------------
	 * Insere novo processo na guia
	 * @author Elisson Dias
	 * Date: 13/09/2004 - 11:38
	 *------------------------------------------------------------------------
	 */
	
	public void incluirProcessoGuia(AutoInfracaoBean myAuto,
			String codLoteRelator,String cRq,String codEvento,ACSS.UsuarioBean UsrLogado, String existeGuia)
	throws DaoException {	
		Connection conn = null;
		boolean Ok = true;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST); 
			String sCmd ="SELECT COD_JUNTA,NUM_CPF FROM TSMI_LOTE_RELATOR" +
			" WHERE COD_LOTE_RELATOR='" + codLoteRelator + "'" ;
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			String cjunta="";
			String ccpf = "";
			while (rs.next()) {
				cjunta = rs.getString("COD_JUNTA");
				if (cjunta==null) cjunta="";
				ccpf = rs.getString("NUM_CPF");
				if (ccpf==null) ccpf="";
			}
			rs.close();		
			
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int s = 0;    
			myAuto.setMsgErro("");
			
			if (myAuto.getRequerimentos(cRq).getCodStatusRequerimento().equals("0") ||		
					myAuto.getRequerimentos(cRq).getCodStatusRequerimento().equals("1") ||		
					myAuto.getRequerimentos(cRq).getCodStatusRequerimento().equals("2")){		
				
				//Gera transacao 208/328/335 para cada Auto			
				
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)) +
				sys.Util.lPad(codEvento,"0",3) +																																			  
				sys.Util.lPad("100","0",3) +
				sys.Util.rPad(UsrLogado.getNomUserName()," ",20) +	
				sys.Util.lPad(UsrLogado.getOrgao().getCodOrgao(),"0",6) +	
				sys.Util.rPad(myAuto.getRequerimentos(cRq).getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)) +							
				sys.Util.lPad(cjunta,"0",6) +
				sys.Util.lPad(ccpf,"0",11) ;			  			
				resultado = chamaTransBRK(codEvento,parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Incluir Processo na Guia.")==false) {
					vErro.add(myAuto.getMsgErro());
					Ok = false;
				}
			}
			if (existeGuia.length()>0) {
				//			System.out.println("Existe Guia e para Deletar");
				sCmd =	"DELETE FROM TSMI_ITEM_LOTE_RELATOR WHERE COD_LOTE_RELATOR = "+existeGuia+" AND NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'"+ 
				" AND NUM_REQUERIMENTO = '"+myAuto.getRequerimentos(cRq).getNumRequerimento()+"'";
				stmt.execute(sCmd) ;
			}
			sCmd =	"INSERT INTO TSMI_ITEM_LOTE_RELATOR (COD_LOTE_RELATOR,COD_ITEM_LOTE_RELATOR,NUM_AUTO_INFRACAO,NUM_PROCESSO,DAT_PROCESSO,NUM_PLACA,DAT_INFRACAO,DSC_INFRACAO,DSC_LOCAL_INFRACAO,NUM_REQUERIMENTO,DSC_ENQUADRAMENTO)" +
			" VALUES (" + codLoteRelator + ",SEQ_TSMI_ITEM_LOTE_RELATOR.NEXTVAL,'" + myAuto.getNumAutoInfracao() + "','" + myAuto.getNumProcesso() + "',to_date('" + myAuto.getDatProcesso() +  "','dd/mm/yyyy'),'" + myAuto.getNumPlaca() + "',to_date('" + myAuto.getDatInfracao() +  "','dd/mm/yyyy'),'" + myAuto.getInfracao().getDscInfracao() + "','" +
			myAuto.getDscLocalInfracao() + "','" + myAuto.getRequerimentos(cRq).getNumRequerimento() + "','" + myAuto.getInfracao().getDscEnquadramento() +  "')"; 
			stmt.execute(sCmd) ;
			
			
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.LeAutoInfracaoLocal(myAuto,"auto");					  	    	
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(myAuto.getNumProcesso());
			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(sys.Util.formatedToday().substring(0,10));
			myHis.setCodEvento(codEvento);
			myHis.setCodOrigemEvento("100");		
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			myHis.setTxtComplemento01(myAuto.getRequerimentos(cRq).getNumRequerimento());	
			myHis.setTxtComplemento02(sys.Util.formatedToday().substring(0,10));	
			myHis.setTxtComplemento03(cjunta);	
			myHis.setTxtComplemento04(ccpf);
			
			//preparar Bean de Requerimento
			RequerimentoBean myReq = new RequerimentoBean();
			myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
			myReq.setNumRequerimento(myAuto.getRequerimentos(cRq).getNumRequerimento());
			dao.LeRequerimentoLocal(myReq,conn);
			myReq.setNomUserNameJU(UsrLogado.getNomUserName());
			myReq.setCodStatusRequerimento("2");
			myReq.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcJU(sys.Util.formatedToday().substring(0,10));
			myReq.setDatJU(sys.Util.formatedToday().substring(0,10));	
			myReq.setCodJuntaJU(cjunta);	
			myReq.setCodRelatorJU(ccpf);
			dao.GravaAutoInfracaoLocal(myAuto, conn);
			if (myAuto.getCodAutoInfracao().length()>0)
			{
				/*Gravar Requerimento*/
				dao.GravarRequerimento(myReq,conn);
				/*Gravar Historico*/
				dao.GravarHistorico(myHis,conn); 
			}
			
			
			// fechar a transa��o - commit ou rollback
			if (Ok) conn.commit();
			else 	conn.rollback();
			stmt.close();		
			
		} catch (SQLException e) {
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - Enviar DO
	 *-----------------------------------------------------------
	 */
	public int GuiaEnviaDO(GuiaDistribuicaoBean GuiaDistribuicaoId) throws sys.DaoException 
	{
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			conn.setAutoCommit(false);	
			GuiaDistribuicaoBean myGuia = null;
			List listAutos = null;
			for(int x=0;x<GuiaDistribuicaoId.getAutos().size();x++){
				myGuia = (GuiaDistribuicaoBean)GuiaDistribuicaoId.getAutos().get(x);
				if(myGuia.getAutos().size()>0){
					String sCmd = "UPDATE TSMI_LOTE_RELATOR SET COD_STATUS='4'," +
					"NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
					"COD_ORGAO_LOTACAO='"+ myGuia.getCodOrgaoLotacao()+ "'," +
					" DT_PROC=to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),"+				
					"COD_ENVIO_PUBDO='"+ myGuia.getCodEnvioDO()+ "'," +
					"DAT_ENVIO_PUBDO=to_date('"+myGuia.getDatEnvioDO()+"','dd/mm/yyyy') " +
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'" ;
					stmt.execute(sCmd) ;			
					// fazer iterator e pegar todos os processos
					Iterator it = myGuia.getAutos().iterator() ;				
					myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
					REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
					REC.RequerimentoBean myReq   = new REC.RequerimentoBean();
					String parteVar  = "" ;
					String resultado = "" ;
					String parteDo   =sys.Util.formataDataYYYYMMDD(myGuia.getDatEnvioDO()) + 
					sys.Util.rPad(myGuia.getCodEnvioDO()," ",20) +	
					sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
					sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6);  			
//					int nauto = 0;
					listAutos = new ArrayList();
					while (it.hasNext()) {
						myAuto = (REC.AutoInfracaoBean)it.next() ;
						listAutos.add(myAuto);
						myReq = myAuto.getRequerimentos(0) ;					
						myAuto.setMsgErro("");
						// Gera transacao 261/381/388 para cada Auto			
						parteVar += sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
						sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
						sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +					
						sys.Util.lPad(myReq.getCodResultRS(),"0",1) +
						sys.Util.rPad(myAuto.getNumRequerimento()," ",30) ;
						
						/*				C�digo comentado para n�o enviar mais um grupo de autos de uma vez para o 
						 * 				Broker. Essa altera��o ir� permitir que uma prov�vel mensagem de erro
						 * 				fique contida somente no auto que tenha tido o problema no envio.
						 * 				
						 * C�DIGO------------------------------------------------------------------------------
						 * 						nauto++;
						 *							if (nauto<56) continue ;
						 *-------------------------------------------------------------------------------------
						 */
						
						resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
						if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro no Envio para Publica��o."))
						{
							inc ++/*=nauto*/;
							DaoBroker dao = DaoBrokerFactory.getInstance();
							dao.VerificaAutoOracle(myAuto,conn);
							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
							myReq.setNumRequerimento(myAuto.getNumRequerimento());
							if (myReq.getCodAutoInfracao().length()>0)
							{
								/*Gravar Requerimento*/
								dao.LeRequerimentoLocal(myReq,conn);
								myReq.setCodEnvioDO(myGuia.getCodEnvioDO());
								myReq.setDatEnvioDO(myGuia.getDatEnvioDO());
								dao.GravarRequerimento(myReq,conn);
							}
						}
						else marcarAutos(conn,myAuto,myGuia,listAutos) ; 
						listAutos = new ArrayList();
						/*					
						 * 				  O coment�trio � realizado pelo mesmo motivo ao de cima.
						 * 
						 * C�DIGO------------------------------------------------------------------------------
						 *						nauto=0;
						 *-------------------------------------------------------------------------------------
						 */
						parteVar="";
					}	
					/*					
					 * 				  O coment�trio � realizado pelo mesmo motivo ao de cima.
					 * 
					 * C�DIGO------------------------------------------------------------------------------
					 * 					if (nauto>0) {				
					 *						resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
					 *						if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro no Envio para Publica��o.")) inc+=nauto;
					 *						else marcarAutos(conn,myAuto,myGuia,listAutos);
					 *					}	
					 *-------------------------------------------------------------------------------------
					 */
				}			
			}  // fim do for
			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if (inc==0) 
						conn.rollback();
					else
						conn.commit();	
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		GuiaDistribuicaoId.setMsgErro(inc + " Requerimento(s) Enviado(s) para Publica��o");
		removeEmptyGuias(GuiaDistribuicaoId);
		return inc ;
	}
	
	private void marcarAutos(Connection conn,AutoInfracaoBean myAuto,GuiaDistribuicaoBean myGuia, List listAutos) throws sys.DaoException {
		try {	
			Statement stmt = conn.createStatement();				
			AutoInfracaoBean auto = null;
			for(int x=0;x<listAutos.size();x++){
				auto = (AutoInfracaoBean)listAutos.get(x);
				String sCmd = "UPDATE TSMI_ITEM_LOTE_RELATOR SET MSG_ERRO='"+myAuto.getMsgErro()+"' "+
				" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' AND " +
				" NUM_REQUERIMENTO='"+auto.getNumRequerimento()+"'";
				stmt.execute(sCmd) ;
				auto.setMsgErro(myAuto.getMsgErro());
			}  // fim do for
			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	private void removeEmptyGuias(GuiaDistribuicaoBean GuiaDistribuicaoId){
		for(int x=0;x<GuiaDistribuicaoId.getAutos().size();x++){
			if( ((GuiaDistribuicaoBean)GuiaDistribuicaoId.getAutos().get(x)).getAutos().size() == 0 ){
				GuiaDistribuicaoId.getAutos().remove(x);
				x--;
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Guia de Distribuicao - InformaPublicacao DO
	 *-----------------------------------------------------------
	 */
	
	public int GuiaInformaPublicacaoDO(GuiaDistribuicaoBean GuiaDistribuicaoId) throws sys.DaoException {
		Connection conn =null ;
		int inc = 0;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();			
			conn.setAutoCommit(false);	
			GuiaDistribuicaoBean myGuia = null;
			List listAutos = null;
			for(int x=0;x<GuiaDistribuicaoId.getAutos().size();x++){
				myGuia = (GuiaDistribuicaoBean)GuiaDistribuicaoId.getAutos().get(x);
				if(myGuia.getAutos().size()>0){
					String sCmd = "UPDATE TSMI_LOTE_RELATOR SET COD_STATUS='9'," +
					" NOM_USERNAME='"+myGuia.getNomUserName()+"',"+
					" COD_ORGAO_LOTACAO='"+ myGuia.getCodOrgaoLotacao()+ "'," +
					" DT_PROC=to_date('"+sys.Util.formatedToday().substring(0,10)+"','dd/mm/yyyy'),"+				
					" DAT_PUBDO=to_date('"+myGuia.getDatPubliPDO()+"','dd/mm/yyyy') " +			
					" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'" ;
					stmt.execute(sCmd) ;			
					// fazer iterator e pegar todos os processos
					Iterator it = myGuia.getAutos().iterator() ;
					myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
					REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
					String parteVar  = "" ;
					String resultado = "" ;
					String parteDo   =sys.Util.formataDataYYYYMMDD(myGuia.getDatPubliPDO()) + 	
					sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
					sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6);  			
					int nauto = 0;
					listAutos = new ArrayList();				
					while (it.hasNext()) {
						myAuto = (REC.AutoInfracaoBean)it.next() ;
						listAutos.add(myAuto);
						myAuto.setMsgErro("");
						// Gera transacao 263/383/390 para cada Auto			
						parteVar += sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
						sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
						sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
						sys.Util.rPad(myAuto.getNumRequerimento()," ",30) ;
						nauto++;
						if (nauto<58) continue ;
						
						resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
						if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na Publica��o.")) 
						{
							inc+=nauto;
						}
						else marcarAutos(conn,myAuto,myGuia,listAutos);
						listAutos = new ArrayList();
						nauto=0;
						parteVar="";
					}	
					if (nauto>0) {				
						resultado = chamaTransBRK(GuiaDistribuicaoId.getCodEvento(),parteDo+parteVar,"");
						if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro no Envio para Publica��o."))
						{
							inc+=nauto;
							RequerimentoBean myReq = new RequerimentoBean();
							DaoBroker dao = DaoBrokerFactory.getInstance();  
							dao.VerificaAutoOracle(myAuto,conn);
							myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
							myReq.setNumRequerimento(myAuto.getNumRequerimento());
							
							if (myReq.getCodAutoInfracao().length()>0)
							{
								/*Gravar Requerimento*/
								dao.LeRequerimentoLocal(myReq,conn);
								myReq.setDatPublPDO(myGuia.getDatPubliPDO());
								dao.GravarRequerimento(myReq,conn);
							}
						}
						else marcarAutos(conn,myAuto,myGuia,listAutos);
					}								 
				}			
			}  // fim do for
			stmt.close();
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if (inc==0) 
						conn.rollback();
					else
						conn.commit();	
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		GuiaDistribuicaoId.setMsgErro(inc + " Requerimentos Publicados");
		removeEmptyGuias(GuiaDistribuicaoId);
		return inc ;
	}
	
	public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua) 
	throws sys.DaoException {
		String resultado = "";
		BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		if (opcEscolha.equals(null)) 
			resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		else {
			transBRK.setParteFixa(codTransacao);				
			transBRK.setParteVariavel(opcEscolha + indContinua);
			//System.out.println("parte Var ("+codTransacao+"): " + opcEscolha + indContinua);
			resultado = transBRK.getResultado();				  
		}
		return resultado;
	}
	
	public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg,Statement stmt) throws sys.DaoException {
		boolean bOk = false ;
		try {
			if ((resultado.substring(0,3).equals("000")==false) &&
					(resultado.substring(0,3).equals("037")==false)) {
				if ( (resultado.substring(0,3).equals("ERR")) || (sys.Util.isNumber(resultado.substring(0,3))==false) ) {
					myInf.setMsgErro(msg+" Erro: "+resultado) ;		
				}
				else {
					myInf.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+resultado.substring(0,3)+"'" ;
					ResultSet rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myInf.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
					}
					rs.close();
				}
			}
			else   bOk = true ;
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;  
	}
	public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg) throws sys.DaoException {
		Connection conn = null ;
		boolean bOK = false ;	
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			bOK = verErro(myInf,resultado,msg,stmt);
			stmt.close();		
		}
		catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOK;
	}
	
	public void acertaComplemento (HistoricoBean myHis) throws sys.DaoException {
		if ("206,326,334, 211,331,338".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento03(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento03()));
		}
		if ("200, 207,352, 208,328,335, 209,329,336, 210,330,337, 226,350,351,352 227, 406,410,411".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento02(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento02()));
		}
		if ("204,324,214".indexOf(myHis.getCodEvento())>=0) {
			myHis.setTxtComplemento01(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento01()));	
		}
	}
	/******************************************
	 Verifica se o Auto ja esta no Oracle
	 ******************************************/
	public void VerificaAutoOracle (AutoInfracaoBean myAuto,Connection conn) throws sys.DaoException { 
		try {	
			boolean existeAuto = false;	  
			String sCmd  = "SELECT cod_auto_infracao,cod_status from TSMI_AUTO_INFRACAO "+
			"WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
			Statement stmt = conn.createStatement();
			ResultSet rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				existeAuto = true;	  				  	  				  
				myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	
				myAuto.setCodStatus(rs.getString("cod_status"));
			}
			if (existeAuto == false) {
				 sCmd =  "insert into tsmi_auto_infracao (cod_auto_infracao, num_auto_infracao, num_placa, dat_infracao) "+
				 " values(seq_tsmi_auto_infracao.nextval,"+
				 " '"+myAuto.getNumAutoInfracao()+ "','"+myAuto.getNumPlaca() + "','"+myAuto.getDatInfracao()+"')";
				stmt.execute(sCmd) ;
				sCmd  = "SELECT cod_auto_infracao from TSMI_AUTO_INFRACAO WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
				rs    = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	  			 
				}
			}
			rs.close();
			stmt.close();
		}			
		catch (Exception ex) {
			throw new sys.DaoException("GuiaDaoBroker - VerificaAutoOracle: "+ex.getMessage());
		}
		return;
	}
	//------------------------------------------------------------------
	//------------------------------------------------------------------
	public String[] AtaGuias(String codEnvioDO, String codStatus, String codOrgao) throws sys.DaoException{
		Connection conn =null ;
		int numGuias=0,numReqs=0;
		String[] totalGuia = new String[] {"0","0"};
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();	
            //Verifica se o n� da Ata j� existe
		    boolean existeAta = false;
            String sCmd = "SELECT COD_LOTE_RELATOR FROM TSMI_LOTE_RELATOR " +
            "WHERE COD_ENVIO_PUBDO = '"+ codEnvioDO + "' AND COD_STATUS = '" + codStatus + "'"+
            " AND COD_ORGAO = "+codOrgao;
            ResultSet rs   = stmt.executeQuery(sCmd) ;
            while(rs.next()){
            	existeAta = true;
            }
            if(existeAta) totalGuia = new String[] {"-2","-2"};
            else 
            {
                sCmd = "SELECT COD_LOTE_RELATOR FROM TSMI_LOTE_RELATOR " +
    			"WHERE COD_ENVIO_PUBDO = '"+ codEnvioDO + "' AND COD_STATUS = '" + codStatus + "'"+
    			" AND COD_ORGAO = "+codOrgao;
    			rs   = stmt.executeQuery(sCmd) ;
    			boolean flag = true;
    			while(rs.next()){
    				flag = false;
    			}
    			if(!flag) totalGuia = new String[] {"-1","-1"};
    			else {		
    				sCmd = "SELECT I.COD_LOTE_RELATOR,COUNT(I.COD_LOTE_RELATOR) as numReq FROM TSMI_ITEM_LOTE_RELATOR I, TSMI_LOTE_RELATOR R "+
    				"WHERE I.COD_LOTE_RELATOR=R.COD_LOTE_RELATOR AND COD_ENVIO_PUBDO='"+ codEnvioDO + "' " +
    				" AND COD_STATUS=4 GROUP BY I.COD_LOTE_RELATOR" ;		              
    				rs   = stmt.executeQuery(sCmd) ;
    				while (rs.next()) {
    					numGuias++;
    					numReqs += rs.getInt("numReq");
    				}
    				totalGuia[0]=String.valueOf(numGuias);
    				totalGuia[1]=String.valueOf(numReqs);
    			}
            }
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return totalGuia;
	}
	
	//------------------------------------------------------
	public List consultaGuia(GuiaDistribuicaoBean myG,ACSS.UsuarioBean UsrLogado) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_LOTE_RELATOR,DT_PROC,COD_STATUS,DSC_TIPOS_REQUERIMENTO,NOM_USERNAME,"
				+ "COD_ORGAO_LOTACAO,L.COD_ORGAO,L.COD_JUNTA,L.NUM_CPF,DAT_RS,COD_ENVIO_PUBDO,DAT_ENVIO_PUBDO,DAT_PUBDO, "
				+ "SIG_JUNTA, IND_TIPO, COD_RELATOR, NOM_RELATOR "
				+ " FROM " ;
			
			if (myG.getControleGuia().equals("S"))
				sCmd +=  " TSMI_CONTROLE_RELATOR C JOIN ";
			
			sCmd += " TSMI_JUNTA J JOIN TSMI_RELATOR R JOIN TSMI_LOTE_RELATOR L"
				+ " ON L.NUM_CPF = R.NUM_CPF ON L.COD_JUNTA = J.COD_JUNTA"  ;
			
			
			if (myG.getControleGuia().equals("S"))
				sCmd += " ON C.COD_RELATOR = R.COD_RELATOR ";
			
			sCmd += " where IND_TIPO = '"+myG.getTipoJunta() +"' and COD_STATUS='"+myG.getCodStatus()+"' "
			+ " and L.COD_ORGAO='"+myG.getCodOrgaoAtuacao()+"' " 
			+ " and L.COD_JUNTA = R.COD_JUNTA and R.IND_ATIVO = 'S' ";
			
			
			if (myG.getControleGuia().equals("S"))
			{
				sCmd += " AND C.COD_RELATOR = R.COD_RELATOR" +
				" AND C.COD_USUARIO = "+UsrLogado.getCodUsuario();
			}      
			
			
			sCmd += " ORDER BY COD_LOTE_RELATOR";
			ResultSet rs = stmt.executeQuery(sCmd);
			GuiaDistribuicaoBean myGuia = null;
			while (rs.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));
				myGuia.setDatProc(rs.getString("DT_PROC"));
				myGuia.setCodStatus(rs.getString("COD_STATUS"));
				myGuia.setTipoReqValidos(rs.getString("DSC_TIPOS_REQUERIMENTO"));
				myGuia.setNomUserName(rs.getString("NOM_USERNAME"));
				myGuia.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myGuia.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				myGuia.setCodJunta(rs.getString("COD_JUNTA"));
				myGuia.setNumCPFRelator(rs.getString("NUM_CPF"));
				myGuia.setDatRS(rs.getString("DAT_RS"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				myGuia.setTipoJunta(rs.getString("IND_TIPO"));
				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
				myGuia.setCodRelator(rs.getString("COD_RELATOR"));
				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
				vList.add(myGuia);
			}
			
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}
	
	public List consultaAtasPublicar(GuiaDistribuicaoBean myG,ACSS.UsuarioBean UsrLogado) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT distinct COD_STATUS,COD_ENVIO_PUBDO,"+
			"to_char(DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO,to_char(DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO " +			
			" FROM" ;
			
			if (myG.getControleGuia().equals("S"))
				sCmd +=  " TSMI_CONTROLE_RELATOR C , ";
			
			sCmd +=" TSMI_LOTE_RELATOR L , TSMI_JUNTA J, TSMI_RELATOR R " +
			" WHERE IND_TIPO = '"+myG.getTipoJunta() +"' and COD_STATUS='"+myG.getCodStatus()+"' "+
			" AND L.COD_JUNTA = J.COD_JUNTA " +
			" AND L.COD_ORGAO='"+myG.getCodOrgaoAtuacao()+"' " +
			" AND R.IND_ATIVO = 'S' " +
			" AND L.NUM_CPF = R.NUM_CPF  ";
			
			if (myG.getControleGuia().equals("S"))
			{
				sCmd += " AND C.COD_RELATOR = R.COD_RELATOR" +
				" AND C.COD_USUARIO = "+UsrLogado.getCodUsuario();
			}	
			
			sCmd +=" ORDER BY DAT_ENVIO_PUBDO desc";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			GuiaDistribuicaoBean myGuia = null;
			while (rs.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setCodStatus(rs.getString("COD_STATUS"));
				myGuia.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myGuia.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				vList.add(myGuia);
			}
			
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return vList;
	}//------------------------------------------------------
	
	
	public Vector LeGuiasAta(GuiaDistribuicaoBean myG) throws DaoException{
		Vector guias = new Vector();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_LOTE_RELATOR,COD_STATUS "+
			" FROM TSMI_LOTE_RELATOR L, TSMI_JUNTA J" +			
			" where J.IND_TIPO = '"+myG.getTipoJunta() +"' " +
			" and L.COD_ENVIO_PUBDO='"+myG.getCodEnvioDO()+"' "+
			" and L.COD_ORGAO='"+myG.getCodOrgaoAtuacao()+"' "+
			" and L.COD_JUNTA=J.COD_JUNTA " +
			" ORDER BY COD_LOTE_RELATOR";
			
			//System.out.println("LeGuiasAta Guia: "+sCmd);
			
			ResultSet rs = stmt.executeQuery(sCmd);
			String  mg = "";
			while (rs.next()) {
				mg = rs.getString("COD_LOTE_RELATOR");
				if (mg!=null) {
					guias.addElement(mg);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		//System.out.println("LeGuiasAta Guia FIM: "+guias.size());	
		return guias;
	}
	
	/*-----------------------------------------
	 * DAO Relativo ao verifica se o Requerimento j� foi incluido na guia
	 * @author Alexandre Bahia
	 ------------------------------------------ */  
	
	public String ConsultaReqGuia(REC.AutoInfracaoBean myAuto) throws sys.DaoException{
		
		Connection conn = null;
		ResultSet rs = null;
		String existe = "";
		String sql = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sql = "select l.cod_lote_relator "+
			" FROM tsmi_lote_relator l, tsmi_item_lote_relator i " +
			" WHERE i.num_auto_infracao = '"+ myAuto.getNumAutoInfracao()+"' " +
			" and l.cod_status<>9 " +
			" and l.cod_lote_relator = i.cod_lote_relator " +
			" and i.num_requerimento = '"+myAuto.getNumRequerimento()+"' and rownum = 1" ;
			rs = stmt.executeQuery(sql);
			while(rs.next()){
				existe = rs.getString("cod_lote_relator");
			}
			rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return existe;
	}
	
	//------------------------------------------------------
	public void consultaAta(GuiaDistribuicaoBean guia, int ordem) throws DaoException{
		List vList = new ArrayList();
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtGuia = conn.createStatement();
			String sCmd = "SELECT COD_LOTE_RELATOR,DT_PROC,COD_STATUS,DSC_TIPOS_REQUERIMENTO,NOM_USERNAME,"
				+ "COD_ORGAO_LOTACAO,L.COD_ORGAO,L.COD_JUNTA,L.NUM_CPF,TO_CHAR(DAT_RS,'dd/mm/yyyy') AS DAT_RESULT,COD_ENVIO_PUBDO,DAT_ENVIO_PUBDO,DAT_PUBDO, "
				+ "SIG_JUNTA, IND_TIPO, COD_RELATOR, NOM_RELATOR, NOM_TITULO, L.NUM_SESSAO,TO_CHAR(L.DAT_SESSAO,'dd/mm/yyyy') DAT_SESSAO "
				+ " FROM TSMI_JUNTA J JOIN TSMI_RELATOR R JOIN TSMI_LOTE_RELATOR L"
				+ " ON L.NUM_CPF = R.NUM_CPF ON L.COD_JUNTA = J.COD_JUNTA"			
				+ " where IND_TIPO = '"+guia.getTipoJunta() + "' " 
				+ " AND L.COD_ORGAO='"+guia.getCodOrgaoAtuacao()+"' " 
				+ " AND COD_ENVIO_PUBDO='"+guia.getCodEnvioDO()+"' "
				+ " AND R.COD_JUNTA = J.COD_JUNTA";
			if(ordem == 1)
				sCmd += " ORDER BY COD_LOTE_RELATOR";
			else if(ordem == 2)
				sCmd += " ORDER BY COD_ENVIO_PUBDO ASC, COD_JUNTA ASC, DAT_RS ASC, NOM_TITULO ASC";
			else if(ordem == 3)
				sCmd += " ORDER BY NOM_RELATOR";
			else if(ordem == 4)
				sCmd += " ORDER BY COD_ENVIO_PUBDO ASC, COD_JUNTA ASC, NOM_TITULO ASC";
			
			
			ResultSet rsGuia = stmtGuia.executeQuery(sCmd);
			GuiaDistribuicaoBean myGuia = null;
			List guias = new ArrayList();
			List autos = null;
			ResultSet rs = null;
			SimpleDateFormat dat = new SimpleDateFormat("dd/MM/yyyy");
			while (rsGuia.next()) {
				myGuia = new GuiaDistribuicaoBean();
				myGuia.setNumGuia(rsGuia.getString("COD_LOTE_RELATOR"));
				myGuia.setDatProc(rsGuia.getString("DT_PROC"));
				myGuia.setCodStatus(rsGuia.getString("COD_STATUS"));
				myGuia.setNumSessao(rsGuia.getString("NUM_SESSAO"));
				myGuia.setDatSessao(rsGuia.getString("DAT_SESSAO"));
				myGuia.setTipoReqValidos(rsGuia.getString("DSC_TIPOS_REQUERIMENTO"));
				myGuia.setNomUserName(rsGuia.getString("NOM_USERNAME"));
				myGuia.setCodOrgaoLotacao(rsGuia.getString("COD_ORGAO_LOTACAO"));
				myGuia.setCodOrgaoAtuacao(rsGuia.getString("COD_ORGAO"));
				myGuia.setCodJunta(rsGuia.getString("COD_JUNTA"));
				myGuia.setNumCPFRelator(rsGuia.getString("NUM_CPF"));
				myGuia.setDatRS(rsGuia.getString("DAT_RESULT"));
				myGuia.setCodEnvioDO(rsGuia.getString("COD_ENVIO_PUBDO"));
				myGuia.setDatEnvioDO(dat.format(rsGuia.getDate("DAT_ENVIO_PUBDO")));
				myGuia.setDatPubliPDO(rsGuia.getString("DAT_PUBDO"));
				myGuia.setTipoJunta(rsGuia.getString("IND_TIPO"));
				myGuia.setSigJunta(rsGuia.getString("SIG_JUNTA"));
				myGuia.setCodRelator(rsGuia.getString("COD_RELATOR"));
				myGuia.setNomRelator(rsGuia.getString("NOM_RELATOR"));
				myGuia.setNomTituloRelator(rsGuia.getString("NOM_TITULO"));			
				sCmd = "Select I.COD_ITEM_LOTE_RELATOR," +
				"I.NUM_AUTO_INFRACAO,I.NUM_PROCESSO,to_char(I.DAT_PROCESSO,'dd/mm/yyyy') DAT_PROCESSO, " +
				"I.NUM_PLACA,to_char(I.DAT_INFRACAO,'dd/mm/yyyy') DAT_INFRACAO, " +
				"I.DSC_INFRACAO,I.DSC_LOCAL_INFRACAO," +
				"I.NUM_REQUERIMENTO,I.IND_TIPO_REQUERIMENTO," +
				"I.NOM_PROPRIETARIO,I.DSC_ENQUADRAMENTO,I.MSG_ERRO, " +
				"I.COD_RESULT_RS,I.TXT_MOTIVO_RS "+
				"FROM TSMI_ITEM_LOTE_RELATOR I "+
				"WHERE I.COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' " ;
				
				rs   = stmt.executeQuery(sCmd) ;
				autos = new ArrayList();
				while (rs.next()) {
					REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
					myAuto.setInfracao(new InfracaoBean());	
					myAuto.setProprietario(new ResponsavelBean());
					List reqs = new ArrayList();
					REC.RequerimentoBean req = new REC.RequerimentoBean();
					myAuto.setCodOrgao(myGuia.getCodOrgaoAtuacao());
					myAuto.setCodAutoInfracao(rs.getString("COD_ITEM_LOTE_RELATOR"));
					myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));			
					myAuto.setNumProcesso(rs.getString("NUM_PROCESSO"));	
					myAuto.setDatProcesso(rs.getString("DAT_PROCESSO"));	 
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));	
					myAuto.setDatInfracao(rs.getString("DAT_INFRACAO"));
					myAuto.getInfracao().setDscInfracao(rs.getString("DSC_INFRACAO"));	
					myAuto.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));
					myAuto.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
					myAuto.setTpRequerimento(rs.getString("IND_TIPO_REQUERIMENTO"));
					myAuto.getProprietario().setNomResponsavel(rs.getString("NOM_PROPRIETARIO"));			
					myAuto.getInfracao().setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));			
					myAuto.setMsgErro(rs.getString("MSG_ERRO"));
					req.setCodResultRS(rs.getString("COD_RESULT_RS"));
					req.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
					reqs.add(req);			
					myAuto.setRequerimentos(reqs);
					autos.add(myAuto);
				}
				myGuia.setAutos(autos);
				guias.add(myGuia);	
				rs.close();
			}
			rsGuia.close();
			stmt.close();
			stmtGuia.close();
			guia.setAutos(guias);
			guia.setDatEnvioDO(myGuia.getDatEnvioDO());
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
	}
	
	//----------------------------------------------------------------------------------
	/*
	public void consultaAta(GuiaDistribuicaoBean guia) throws DaoException{
		consultaAta(guia,1);
	}
	*/
	
	//----------------------------------------------------------------------------------
	public boolean gravaArquivo(GuiaDistribuicaoBean GuiaDistribuicaoId, String quantReq) throws Exception{
		Connection conn = null;
		boolean existeArq = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = null;
			if(!GuiaDistribuicaoId.getCodStatus().equals("-1")){
				sCmd = "SELECT COD_ENVIO_PUBDO FROM TSMI_ARQUIVO_ATA WHERE COD_ENVIO_PUBDO = '" + GuiaDistribuicaoId.getCodEnvioDO() + "'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					existeArq = true;
				}
				if(existeArq)
					return false;
			}
			else{
				sCmd = "UPDATE TSMI_ARQUIVO_ATA SET COD_STATUS = 2 WHERE COD_ENVIO_PUBDO = '" + GuiaDistribuicaoId.getCodEnvioDO() + "'";
				stmt.executeUpdate(sCmd);
			}
			sCmd = "INSERT INTO TSMI_ARQUIVO_ATA (COD_ARQUIVO_ATA, COD_ENVIO_PUBDO, DAT_CRIACAO, QTD_REG, NOM_USERNAME, COD_ORGAO_LOTACAO , NOM_ARQUIVO_ATA, COD_STATUS)"+
			" VALUES (SEQ_TSMI_ARQUIVO_ATA.NEXTVAL, '" + GuiaDistribuicaoId.getCodEnvioDO() + "',TO_DATE('" + sys.Util.formatedToday().substring(0,10) + "','DD/MM/YYYY'),'" +	
			quantReq + "','" + GuiaDistribuicaoId.getNomUserName() + "','" + GuiaDistribuicaoId.getCodOrgaoLotacao() + "','" + GuiaDistribuicaoId.getNomArquivoATA() + "', 1)";
			stmt.execute(sCmd);
			stmt.close();
		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}
		return true;
	}
	//----------------------------------------------------------------------------------
	/**
	 *------------------------------------------------------------------------
	 * Busca todos os Arquivos Recebidos de acordo com o Tipo e estado passado
	 *------------------------------------------------------------------------
	 */
	
	public Vector buscaArquivosAta(GuiaDistribuicaoBean myArq, String dataIni, String dataFim) throws DaoException {
		GuiaDistribuicaoBean relClasse;
		Vector vRetClasse   = new Vector();
		
		Connection conn = null;
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			String sCmd ="SELECT DISTINCT COD_ARQUIVO_ATA, A.COD_ENVIO_PUBDO, NOM_ARQUIVO_ATA, DAT_PUBDO, L.COD_STATUS AS STATUS_ATA" +
			" FROM TSMI_ARQUIVO_ATA A JOIN TSMI_LOTE_RELATOR L JOIN TSMI_JUNTA J" +
			" ON L.COD_JUNTA = J.COD_JUNTA" +
			" ON A.COD_ENVIO_PUBDO = L.COD_ENVIO_PUBDO" +
			" WHERE L.COD_ORGAO=" + myArq.getCodOrgaoAtuacao() +
			" AND IND_TIPO = '" + myArq.getTipoJunta() + "'" + 
			" AND DAT_ENVIO_PUBDO BETWEEN to_date('" + dataIni + "','dd/mm/yyyy') AND to_date('" + dataFim + "','dd/mm/yyyy')" +
			" AND A.COD_STATUS = 1" +
			" ORDER BY COD_ENVIO_PUBDO DESC";         
			ResultSet rs = stmt.executeQuery(sCmd);
			ResultSet rs2 = null;
			while (rs.next())   {
				relClasse = new GuiaDistribuicaoBean();
				relClasse.setNumGuia(rs.getString("COD_ARQUIVO_ATA"));
				relClasse.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO")); 
				relClasse.setNomArquivoATA(rs.getString("NOM_ARQUIVO_ATA"));
				if(rs.getDate("DAT_PUBDO") != null)
					relClasse.setDatPubliPDO(df.format(rs.getDate("DAT_PUBDO")));   
				else
					relClasse.setDatPubliPDO("");
				relClasse.setCodStatus(rs.getString("STATUS_ATA"));
				sCmd = "SELECT COD_ARQUIVO_ATA FROM TSMI_DOWNLOAD_ATA WHERE COD_ARQUIVO_ATA = '"+relClasse.getNumGuia()+"'";
				rs2 = stmt2.executeQuery(sCmd);
				if(rs2.next())
					relClasse.setCodEvento("1");
				else
					relClasse.setCodEvento("0");
				
				vRetClasse.addElement(relClasse);           
			}           
			rs.close();
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
		return vRetClasse;
	}
	//-------------------------------------------------------------------------------------------
	public void downloadArquivoAta(GuiaDistribuicaoBean myArq, String codArquivoAta) throws DaoException {
		GuiaDistribuicaoBean relClasse;
		Vector vRetClasse   = new Vector();
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			SimpleDateFormat dat_download = new SimpleDateFormat("dd/MM/yyyy");
			String sCmd ="INSERT INTO TSMI_DOWNLOAD_ATA (COD_DOWNLOAD_ATA, DAT_DOWNLOAD, NOM_USERNAME," +
			"COD_ARQUIVO_ATA,COD_ORGAO_LOTACAO) VALUES(SEQ_TSMI_DOWNLOAD_ATA.NEXTVAL, TO_DATE('"+
			dat_download.format(new Date()) + "','DD/MM/YYYY'),'" +myArq.getNomUserName() + "','" 
			+ codArquivoAta + "','" + myArq.getCodOrgaoLotacao() + "')";
			stmt.executeQuery(sCmd);
			stmt.close();
		}       
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {          
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {
					throw new DaoException(ey.getMessage());
				}
			}
		}       
	}
	
	//-------------------------------------------------------------------------------------------
	public int guiaEstornoResultado(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List autos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy')," +
			" DAT_SESSAO = NULL," +
			" DAT_RS = NULL  "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			sCmd = "UPDATE TSMI_ITEM_LOTE_RELATOR SET COD_RESULT_RS = NULL "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			
			
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				autos.add(myAuto);
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.rPad(" "," ",50) +
				sys.Util.rPad("ESTORNO " + myGuia.getCodStatus() + " INSTANCIA"," ",1000) ;			 			
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Estornar Resultado.")==false) {
					vErro.add(myAuto.getMsgErro());
					marcarAutos(conn,myAuto,myGuia,autos);
				}
				else 
				{	
					inc++;	
					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");	
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento04("ESTORNO " + myGuia.getCodStatus() + " INSTANCIA");
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
					myReq.setNumRequerimento(myAuto.getNumRequerimento());
					dao.LeRequerimentoLocal(myReq,conn);
					myReq.setNomUserNameEST(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento(myGuia.getCodStatus());
					myReq.setDatEST(myGuia.getDatProc());
					myReq.setTxtMotivoEST("ESTORNO " + myGuia.getCodStatus() + " INSTANCIA");	
					dao.GravaAutoInfracaoLocal(myAuto, conn);
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	
	//----------------------------------------------------------------------------------------
	public int guiaEstornoJuntaRelator(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();		
			List autos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "DELETE FROM TSMI_ITEM_LOTE_RELATOR "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"' ";
			stmt.execute(sCmd) ;
			
			sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy') "+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				autos.add(myAuto);
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +
				sys.Util.lPad(myGuia.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myGuia.getOrigemEvento(),"0",3) +
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30) +							
				sys.Util.formataDataYYYYMMDD(myGuia.getDatProc()) +							
				sys.Util.rPad(" "," ",50) +
				sys.Util.rPad("ESTORNO DA GUIA DE DISTRIBUI��O"," ",1000) ;			 			
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Estornar Guia de Distribui��o.")==false) {
					vErro.add(myAuto.getMsgErro());
					marcarAutos(conn,myAuto,myGuia,autos);
				}
				else 
				{
					inc++;
					DaoBroker dao = DaoBrokerFactory.getInstance();
					dao.LeAutoInfracaoLocal(myAuto,"auto");	
					HistoricoBean myHis = new HistoricoBean() ;
					myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
					myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
					myHis.setNumProcesso(myAuto.getNumProcesso());
					myHis.setCodStatus(myAuto.getCodStatus());  
					myHis.setDatStatus(myGuia.getDatProc());
					myHis.setCodEvento(myGuia.getCodEvento());
					myHis.setCodOrigemEvento(myGuia.getOrigemEvento());		
					myHis.setNomUserName(myGuia.getNomUserName());
					myHis.setCodOrgaoLotacao(myGuia.getCodOrgaoLotacao());
					myHis.setDatProc(myGuia.getDatProc());
					myHis.setTxtComplemento01(myAuto.getNumRequerimento());	
					myHis.setTxtComplemento02(myGuia.getDatProc());	
					myHis.setTxtComplemento04("ESTORNO DA GUIA DE DISTRIBUI��O");
					
					//preparar Bean de Requerimento
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setNumRequerimento(myAuto.getNumRequerimento());
					myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
					dao.LeRequerimentoLocal(myReq, conn);
					myReq.setNomUserNameEST(myGuia.getNomUserName());
					myReq.setCodStatusRequerimento(myGuia.getCodStatus());
					myReq.setDatEST(myGuia.getDatProc());
					myReq.setTxtMotivoEST("ESTORNO DA GUIA DE DISTRIBUI��O");
					
					dao.GravaAutoInfracaoLocal(myAuto, conn);
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						dao.GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						dao.GravarHistorico(myHis,conn); 
					}
					//Se auto pertencer a uma guia de remessa, retorna o auto a mesma 
					// para ser redistribuido
					RequerimentoBean req = new RequerimentoBean();
					req.setNumRequerimento(myAuto.getNumRequerimento());
					req.setCodAutoInfracao(myAuto.getCodAutoInfracao());
					dao.LeRequerimentoLocal(req,conn);
					sCmd = "UPDATE TSMI_REMESSA_ITEM SET SIT_GUIA_DISTRIBUICAO = NULL" +
					" WHERE COD_REQUERIMENTO = " + req.getCodRequerimento();
					stmt.execute(sCmd) ;
					if(!req.getCodRemessa().equals("")){
						sCmd = "UPDATE TSMI_REMESSA SET SIT_GUIA_DISTRIBUICAO = NULL" +
						" WHERE COD_REMESSA = " + req.getCodRemessa();
						stmt.execute(sCmd) ;
					}
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	
	//----------------------------------------------------------------------------------------
	public int guiaEstornoDO(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		int inc =  0 ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();	
			List autos = new ArrayList();
			
			conn.setAutoCommit(false);	
			
			String sCmd = "UPDATE TSMI_LOTE_RELATOR set COD_STATUS='" + myGuia.getCodStatus() + "',"+
			" NOM_USERNAME      = '"+myGuia.getNomUserName()+"',"+
			" COD_ORGAO_LOTACAO = '"+myGuia.getCodOrgaoLotacao()+"',"+
			" DT_PROC           = to_date('"+myGuia.getDatProc()+"','dd/mm/yyyy'), "+
			" COD_ENVIO_PUBDO = NULL , DAT_ENVIO_PUBDO = NULL, NOM_RESP_ATA = NULL, TXT_TITULO_ATA = NULL, DAT_PUBDO =NULL"+
			" WHERE COD_LOTE_RELATOR='"+myGuia.getNumGuia()+"'";
			stmt.execute(sCmd) ;
			
			
			
			
			// fazer iterator e pegar todos os processos
			Iterator it = myGuia.getAutos().iterator() ;
			myGuia.setDatProc(sys.Util.formatedToday().substring(0,10));		
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
			String parteVar  = "" ;
			String resultado = "";
			Vector vErro = new Vector();
			int nauto = 0;
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				autos.add(myAuto);
				// Gera transacao 208/328/335 para cada Auto			
				parteVar = sys.Util.formataDataYYYYMMDD(myGuia.getDatEnvioDO()) + 
				sys.Util.rPad(myGuia.getNomUserName()," ",20) +	
				sys.Util.lPad(myGuia.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumRequerimento()," ",30);	
				nauto++;
				if (nauto<1) continue ;
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Estornar Envio p/publica��o.")==false) {
					vErro.add(myAuto.getMsgErro());
					marcarAutos(conn,myAuto,myGuia,autos);
				}
				else {	 
					inc+=nauto;
				}
				nauto=0;
			}
			if (nauto>0) {	
				resultado = chamaTransBRK(myGuia.getCodEvento(),parteVar,"");
				if (verErro(myAuto,resultado,"Auto: "+myAuto.getNumAutoInfracao()+" Erro na grava��o de Estornar Envio p/publica��o.")==false) {
					vErro.add(myAuto.getMsgErro());
					marcarAutos(conn,myAuto,myGuia,autos);
				}
				else {	 
					inc+=nauto;
				}
			}
			if (inc==0) conn.rollback();	
			else 		conn.commit();		
			myGuia.setMsgErro(vErro);
			stmt.close();	
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return inc;
	}
	//----------------------------------------------------------------------------------------
	public void removeAutoGuia(AutoInfracaoBean myAuto,RequerimentoBean myReq) throws DaoException{
		Connection conn =null ;
		boolean retorno = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();	
			conn.setAutoCommit(false);	
			
			String sCmd = "SELECT L.COD_LOTE_RELATOR as COD_LOTE_RELATOR from TSMI_ITEM_LOTE_RELATOR I JOIN TSMI_LOTE_RELATOR L"+
			" ON I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR"+
			" WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'"+
			" AND num_requerimento = '"+myReq.getNumRequerimento()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			String codLoteRelator="";
			while(rs.next()){
				codLoteRelator = rs.getString("COD_LOTE_RELATOR");
			}
			myAuto.setCodEnvioDO(codLoteRelator);
			sCmd = "DELETE tsmi_item_lote_relator i" +
			" WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao() +"'"+
			" AND num_requerimento = '"+myReq.getNumRequerimento()+"'"+
			" AND COD_LOTE_RELATOR = '"+codLoteRelator+"'";
			stmt.execute(sCmd);
			
			sCmd = "SELECT COUNT(COD_ITEM_LOTE_RELATOR) AS TOTAL from TSMI_ITEM_LOTE_RELATOR I JOIN TSMI_LOTE_RELATOR L"+
			" ON I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR"+
			" WHERE L.COD_LOTE_RELATOR = '"+codLoteRelator+"'";
			ResultSet rs2 = stmt.executeQuery(sCmd);
			rs2.next();
			if(rs2.getInt("TOTAL") == 0){
				sCmd = "UPDATE TSMI_LOTE_RELATOR SET COD_STATUS = 8,COD_ENVIO_PUBDO = '', DAT_ENVIO_PUBDO ='', NOM_RESP_ATA = '', TXT_TITULO_ATA = '', DAT_PUBDO ='' WHERE COD_LOTE_RELATOR = '"+codLoteRelator+"'";
				stmt.executeUpdate(sCmd);
			}
			stmt.close();
			rs.close();
			rs2.close();
			conn.commit();	
		}
		catch (Exception e) {	
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if(retorno)
						conn.commit();
					else
						conn.rollback();
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}				
	}
	
	/*****************************************************************
	 Retorna guias de um determinado mes/ano 
	 *****************************************************************/
	public  boolean getGuias (GER.ResultadoAnualBean ResultadoAnualBeanId,String tipoResultado,
			String datInicio,String datFim,GuiaDistribuicaoBean guiaBean) throws sys.DaoException {
		boolean bOk = true ;
		Connection conn =null ;		
		List guias = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			String sCmd = "";
			ResultSet rs = null;
			ResultSet rs2 = null;
			
			sCmd = " SELECT * FROM TSMI_LOTE_RELATOR LR left outer join tsmi_relator R on  LR.num_cpf = R.num_cpf and  LR.cod_junta=R.cod_junta " +
			" JOIN tsmi_junta J on LR.cod_junta=J.cod_junta "+
			" WHERE LR.DAT_RS >= to_date('"+datInicio+"','dd/mm/yyyy') AND " +
			" LR.DAT_RS <= to_date('"+datFim+"','dd/mm/yyyy') AND LR.DSC_TIPOS_REQUERIMENTO = '"+ResultadoAnualBeanId.getTpreq()+"'";
			if (!ResultadoAnualBeanId.getOrgao().equals("0"))
				sCmd += " AND LR.COD_ORGAO = " + ResultadoAnualBeanId.getOrgao()+ " AND LR.COD_ORGAO_LOTACAO = "+ guiaBean.getCodOrgaoLotacao();
			rs = stmt.executeQuery(sCmd);
			while(rs.next())
			{
				GuiaDistribuicaoBean myGuia = new GuiaDistribuicaoBean();
				List autos = new ArrayList(); 
				sCmd = " SELECT * FROM TSMI_item_LOTE_RELATOR t1,tsmi_auto_infracao t2,tsmi_lote_relator t3, tsmi_orgao t4 "+
				" WHERE t3.cod_lote_relator = t1.cod_lote_relator AND "+ 
				" t4.cod_orgao = t2.cod_orgao AND "+
				" t1.num_auto_infracao = t2.num_auto_infracao AND "+ 
				" t3.DAT_RS >= to_date('"+datInicio+"','dd/mm/yyyy') AND t3.DAT_RS <= to_date('"+datFim+"','dd/mm/yyyy')"+
				" AND t1.ind_tipo_requerimento in ('"+ResultadoAnualBeanId.getTpreq().substring(0,2)+"','"+ResultadoAnualBeanId.getTpreq().substring(3,5)+"') AND t1.cod_lote_relator = "+rs.getString("COD_LOTE_RELATOR");
				if (!tipoResultado.substring(0,1).equals("T"))  
					sCmd += " AND t1.COD_RESULT_RS = '"+tipoResultado.substring(0,1)+"'";
				if (!ResultadoAnualBeanId.getOrgao().equals("0"))  
					sCmd += " AND t2.COD_ORGAO = " + ResultadoAnualBeanId.getOrgao();
				rs2 = stmt2.executeQuery(sCmd);
				while(rs2.next())
				{
					AutoInfracaoBean myAuto = new AutoInfracaoBean(); 
					RequerimentoBean myReq = new RequerimentoBean();
					myAuto.setNumAutoInfracao(rs2.getString("NUM_AUTO_INFRACAO"));
					myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));
					myAuto.setSigOrgao(rs2.getString("SIG_ORGAO"));
					myAuto.setNumProcesso(rs2.getString("NUM_PROCESSO"));
					myAuto.setDatProcesso(sys.Util.fmtData(rs2.getString("DAT_PROCESSO")).replaceAll("-","/"));							
					myAuto.setNumRequerimento(rs2.getString("NUM_REQUERIMENTO"));
					myAuto.setTpRequerimento(rs2.getString("IND_TIPO_REQUERIMENTO"));
					myAuto.getInfracao().setDscEnquadramento(rs2.getString("DSC_ENQUADRAMENTO"));
					myReq.setCodResultRS(rs2.getString("COD_RESULT_RS"));
					myAuto.getRequerimentos().add(myReq);
					myAuto.setDatInfracao(sys.Util.fmtData(rs2.getString("DAT_INFRACAO")).replaceAll("-","/"));
					autos.add(myAuto);
				}
				myGuia.setAutos(autos);
				myGuia.setNumGuia(rs.getString("COD_LOTE_RELATOR"));
				myGuia.setNomRelator(rs.getString("NOM_RELATOR"));
				myGuia.setSigJunta(rs.getString("SIG_JUNTA"));
				myGuia.setDatProc(sys.Util.fmtData(rs.getString("DT_PROC")).replaceAll("-","/"));
				if(myGuia.getAutos().size()!=0) guias.add(myGuia);
			}
			guiaBean.setRelatGuias(guias);
			if (rs2 != null) rs2.close();
			if (rs != null) rs.close();
			stmt.close();
			stmt2.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new sys.DaoException("GuiaPreparaDaoBroker: "+ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}	
	
	/**---------------------------------------------------------------------------------------
	 * Informa que guia de remessa j� foi usada para gerar uma guia de distribui��o.
	 * Impossibilitando ser reutilizada.
	 * Campo: SIT_GUIA_DISTRIBUICAO 
	 * Tabela: TSMI_REMESSA
	 * @param myRemessa
	 * @throws sys.DaoException
	 */
	public void gravaSitGuiaDistribuicao(RemessaBean myRemessa) throws sys.DaoException{
		String sCmd  = "";
		Connection conn = null;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "UPDATE TSMI_REMESSA  SET SIT_GUIA_DISTRIBUICAO = '" +myRemessa.SIT_GUIA_DISTRIBUICAO+"'"+
			"WHERE COD_REMESSA='"+myRemessa.getCodRemessa()+"'";
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		return ;
	}
	
	/**-------------------------------------------------------------------------
	 * Informa quantidade de autos dentro de uma guia de Remessa que ainda n�o 
	 * est�o em uma guia de distribui��o.
	 * @param myRemessa
	 * @throws sys.DaoException
	 */
	public boolean getQTDAutosRemessa(RemessaBean myRemessa) throws sys.DaoException{
		String sCmd  = "";
		boolean bOk = false;
		Connection conn = null;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = " SELECT SIT_GUIA_DISTRIBUICAO FROM TSMI_REMESSA_ITEM" +
			" WHERE COD_REMESSA =" + myRemessa.getCodRemessa()+
			" AND SIT_GUIA_DISTRIBUICAO IS NULL "; 	
			ResultSet rs = stmt.executeQuery(sCmd);
			if(rs.next()){
				bOk = true;
			}
			
			stmt.close();
		}
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}
		return bOk ;
	}
	
	
	
	public boolean verifGerouGuia (RemessaBean remessaBean) throws DaoException{
		Connection conn =null ;
		boolean bOk = false;
		String  sitGuiaDistribuicao = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT SIT_GUIA_DISTRIBUICAO FROM TSMI_REMESSA " +
			" WHERE COD_REMESSA = "+remessaBean.getCodRemessa();
			
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				sitGuiaDistribuicao = rs.getString("SIT_GUIA_DISTRIBUICAO");
				if (sitGuiaDistribuicao == null) sitGuiaDistribuicao = "";
				if(sitGuiaDistribuicao.equals("S"))
					bOk = true;
			}
			
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return bOk;
	}
	
	
	public boolean existeRemessa(RemessaBean remessaBean, AutoInfracaoBean myAuto) throws DaoException{
		Connection conn =null ;
		boolean bOk = false;
		ArrayList auto = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT I.COD_REMESSA, I.COD_REQUERIMENTO, RE.NUM_REQUERIMENTO " +
			" FROM TSMI_REMESSA_ITEM I, TSMI_REMESSA R, TSMI_REQUERIMENTO RE WHERE " +
			" I.COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao()+
			" AND I.COD_REMESSA = R.COD_REMESSA " +
			" AND RE.COD_REQUERIMENTO = I.COD_REQUERIMENTO";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				RemessaBean myRemessaBean = new RemessaBean();
				myRemessaBean.setCodRemessa(rs.getString("COD_REMESSA"));
				myRemessaBean.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				myRemessaBean.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				bOk = true;
				auto.add(myRemessaBean);
			}
			remessaBean.setAutos(auto);
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return bOk;
	}
	
	
	public boolean getCodRemessa (RemessaBean remessaBean, AutoInfracaoBean myAuto) throws DaoException{
		Connection conn =null ;
		boolean bOk = false;
		ArrayList auto = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT I.COD_REMESSA, I.COD_REQUERIMENTO, RE.NUM_REQUERIMENTO " +
			" FROM TSMI_REMESSA_ITEM I, TSMI_REMESSA R, TSMI_REQUERIMENTO RE WHERE " +
			" I.COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao()+
			" AND I.COD_REMESSA = R.COD_REMESSA " +
			" AND (" +
			"RE.COD_TIPO_SOLIC='DP' OR RE.COD_TIPO_SOLIC='DC' OR "+			
			"RE.COD_TIPO_SOLIC='1P' OR RE.COD_TIPO_SOLIC='1C' OR "+
			"RE.COD_TIPO_SOLIC='2P' OR RE.COD_TIPO_SOLIC='2C'" +
			") AND RE.COD_REQUERIMENTO = I.COD_REQUERIMENTO";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				RemessaBean myRemessaBean = new RemessaBean();
				myRemessaBean.setCodRemessa(rs.getString("COD_REMESSA"));
				myRemessaBean.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				myRemessaBean.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				bOk = true;
				auto.add(myRemessaBean);
			}
			remessaBean.setAutos(auto);
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return bOk;
	}
	
	public boolean getCodRemessaEnviada(RemessaBean remessaBean, AutoInfracaoBean myAuto) throws DaoException{
		Connection conn =null ;
		boolean bOk = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT I.COD_REMESSA, I.COD_REQUERIMENTO, RE.NUM_REQUERIMENTO " +
			" FROM TSMI_REMESSA_ITEM I, TSMI_REMESSA R, TSMI_REQUERIMENTO RE WHERE " +
			" I.COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao()+
			" AND R.IND_STATUS_REMESSA = 1 AND I.COD_REMESSA = R.COD_REMESSA " +
			" AND (RE.COD_TIPO_SOLIC='DP' OR RE.COD_TIPO_SOLIC='1P' OR RE.COD_TIPO_SOLIC='2P') "+
			" AND RE.COD_REQUERIMENTO = I.COD_REQUERIMENTO AND I.SIT_GUIA_DISTRIBUICAO IS NOT NULL";
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			if (rs.next()) bOk = true;
			rs.close();
			stmt.close();					
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return bOk;
	}
	
	
	
	public boolean getCodRemessa (RequerimentoBean myReq) throws DaoException{
		Connection conn =null ;
		boolean bExisteCodRemessa = false;
		ResultSet rs = null;
		String sCmd = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			sCmd = " SELECT COD_REQUERIMENTO FROM TSMI_REQUERIMENTO " +
			" WHERE NUM_REQUERIMENTO = '"+myReq.getNumRequerimento()+"' " +
			" AND COD_STATUS_REQUERIMENTO = 0 ";
			rs   = stmt.executeQuery(sCmd) ;
			
			String codReq = "";
			while (rs.next()) {
				codReq = rs.getString("COD_REQUERIMENTO");
			}
			if(!codReq.equals("")){
				sCmd = "SELECT I.COD_REMESSA, I.COD_REQUERIMENTO, RE.NUM_REQUERIMENTO " +
				" FROM TSMI_REMESSA_ITEM I, TSMI_REMESSA R, TSMI_REQUERIMENTO RE WHERE " +
				" I.COD_REQUERIMENTO = "+codReq+
				" AND I.COD_REMESSA = R.COD_REMESSA " +
				" AND RE.COD_REQUERIMENTO = I.COD_REQUERIMENTO";
				rs   = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					bExisteCodRemessa = true;
				}
			}
			
			if(rs!= null) rs.close();
			stmt.close();		
		}	
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return bExisteCodRemessa;
	}
	
	public void excluirProc(RemessaBean myRemessa, AutoInfracaoBean myAuto,
			String codReq) throws sys.DaoException {
		Connection conn =null ;
		boolean bOk = false;
		String sCmd = "";
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;		
			Statement stmt = conn.createStatement();
			
			bOk = getCodRemessa(myRemessa,myAuto);
			
			if(bOk)   
			{
				sCmd = "UPDATE TSMI_REQUERIMENTO SET SIT_PROTOCOLO = NULL, COD_REMESSA = NULL" +
				" WHERE COD_REMESSA = "+myRemessa.getCodRemessa()+
				" AND COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao();
				if(!codReq.equals(""))
					sCmd += " AND NUM_REQUERIMENTO ='"+codReq+"'";
				
				stmt.execute(sCmd);
				DaoBroker dao = DaoBrokerFactory.getInstance();
				RequerimentoBean myReq = new RequerimentoBean();
				myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
				myReq.setNumRequerimento(codReq);
				dao.LeRequerimentoLocal(myReq,conn);
				
				sCmd = "DELETE FROM TSMI_REMESSA_ITEM WHERE " +
				" COD_REMESSA = "+myRemessa.getCodRemessa()+
				" AND COD_AUTO_INFRACAO = "+myAuto.getCodAutoInfracao();	
				if(!codReq.equals(""))
					sCmd += " AND COD_REQUERIMENTO ="+myReq.getCodRequerimento();
				stmt.execute(sCmd);
				myAuto.setMsgErro("Opera��o realizada com sucesso.");
			}
			
			//verifica se a guia de remessa ficou vazia
			sCmd = "SELECT COUNT(*)AS COD_REMESSA FROM TSMI_REMESSA_ITEM "+
			" WHERE COD_REMESSA = "+myRemessa.getCodRemessa();
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			String codRemessa = "";
			while (rs.next()) {
				codRemessa = rs.getString("COD_REMESSA");
			}
			
			if (codRemessa.equals("0"))//Guia de remessa vazia
			{
				sCmd = "DELETE FROM TSMI_REMESSA WHERE " +
				" COD_REMESSA = "+myRemessa.getCodRemessa();
				stmt.execute(sCmd);
			}
			
			stmt.close();					
		}	
		catch (SQLException sqle) {
			myAuto.setMsgErro("Erro ao excluir Processo: "+sqle.getMessage());
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {	
			myAuto.setMsgErro("Erro ao excluir Processo: "+e.getMessage());
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
		return;
	}
	
	public void filtaAutosRemessa (AutoInfracaoBean myAuto) throws DaoException{
		Connection conn =null ;
		String sCmd = "";
		ResultSet rs = null;
		String sitGuiaDistribuicao = "";
		boolean bOk = false;
		List requerimentos = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			Iterator it = myAuto.getRequerimentos().iterator();
			RequerimentoBean myReq = new RequerimentoBean();
			while(it.hasNext()){
				myReq = (RequerimentoBean)it.next();
				sCmd = " SELECT COD_REQUERIMENTO FROM TSMI_REQUERIMENTO " +
				" WHERE NUM_REQUERIMENTO = '"+myReq.getNumRequerimento()+"' " +
				" AND COD_STATUS_REQUERIMENTO = 0 ";
				rs   = stmt.executeQuery(sCmd) ;
				
				String codReq = "";
				while (rs.next()) 
				{
					codReq = rs.getString("COD_REQUERIMENTO");
				}
				if(!codReq.equals("")){
					//verifico se requerimento est� em uma guia de distribui��o
					sCmd = " SELECT SIT_GUIA_DISTRIBUICAO, COD_REMESSA FROM TSMI_REMESSA_ITEM " +
					" WHERE COD_REQUERIMENTO = "+codReq;
					rs   = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						sitGuiaDistribuicao = rs.getString("SIT_GUIA_DISTRIBUICAO");
						if (sitGuiaDistribuicao == null) sitGuiaDistribuicao = "";
						if(sitGuiaDistribuicao.equals("S"))
							bOk = true;
					}
					if(!bOk){
						//verifico se requerimento est� em uma guia de remessa
						sCmd = " SELECT I.COD_REMESSA FROM TSMI_REMESSA_ITEM I, TSMI_REMESSA R  " +
						" WHERE I.COD_REQUERIMENTO = "+codReq +" AND I.COD_REMESSA = R.COD_REMESSA ";
						rs   = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							bOk = true;
						}
					}
					if(!bOk)
						requerimentos.add(myReq);
					bOk = false;
					
				}
			}
			if(rs!= null) rs.close();
			myAuto.setRequerimentos(requerimentos);
		}	
		catch (Exception e) {	
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}			
	}
	
	public ArrayList GuiaLeNit(GuiaDistribuicaoBean myGuia) throws sys.DaoException {
		Connection conn =null ;
		ArrayList guias = new ArrayList();
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;      
			Statement stmt = conn.createStatement();
			Statement stmt2 = conn.createStatement();
			DaoBroker dao = DaoBrokerFactory.getInstance();
			ResultSet rs1= null; 
			ResultSet rs2= null;
			
			String sCmd = "Select L.COD_STATUS,L.COD_LOTE_RELATOR,L.NOM_USERNAME, " +
			"L.COD_ORGAO_LOTACAO,to_char(L.DT_PROC,'dd/mm/yyyy') DT_PROC,"+
			"to_char(L.DAT_RS,'dd/mm/yyyy') DAT_RS," +
			"O.SIG_ORGAO,O.NOM_ORGAO, "+
			"L.COD_ORGAO,L.COD_JUNTA,J.SIG_JUNTA,J.IND_TIPO," +
			"L.NUM_CPF,R.COD_RELATOR,R.NOM_RELATOR,L.COD_ENVIO_PUBDO," +
			"to_char(L.DAT_ENVIO_PUBDO,'dd/mm/yyyy') DAT_ENVIO_PUBDO, L.TIPO_GUIA," +
			"to_char(L.DAT_PUBDO,'dd/mm/yyyy') DAT_PUBDO, L.NUM_SESSAO,to_char(L.DAT_SESSAO,'dd/mm/yyyy') DAT_SESSAO " +
			"FROM TSMI_LOTE_RELATOR L,TSMI_RELATOR R,TSMI_JUNTA J, TSMI_ORGAO O "+
			"WHERE L.NUM_SESSAO ='"+myGuia.getNumSessao()+"' and "+
			"L.COD_ORGAO='"+myGuia.getCodOrgaoAtuacao()+"' and " +
			"L.COD_JUNTA=J.COD_JUNTA AND L.COD_ORGAO=J.COD_ORGAO AND "+
			"L.NUM_CPF=R.NUM_CPF AND L.COD_JUNTA=R.COD_JUNTA AND " +
			"L.COD_STATUS = 3 AND J.IND_TIPO = 0 AND O.COD_ORGAO = L.COD_ORGAO " +
			"ORDER BY R.NOM_RELATOR, L.COD_LOTE_RELATOR" ;                      
			ResultSet rs   = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				GuiaDistribuicaoBean guiaBean = new GuiaDistribuicaoBean();
				guiaBean.setCodStatus(rs.getString("COD_STATUS"));    
				guiaBean.setNumGuia(rs.getString("COD_LOTE_RELATOR"));    
				guiaBean.setDatProc(rs.getString("DT_PROC")); 
				guiaBean.setDatRS(rs.getString("DAT_RS"));    
				guiaBean.setCodOrgaoAtuacao(rs.getString("COD_ORGAO"));
				guiaBean.setCodJunta(rs.getString("COD_JUNTA"));
				guiaBean.setSigJunta(rs.getString("SIG_JUNTA"));
				guiaBean.setTipoJunta(rs.getString("IND_TIPO"));
				guiaBean.setNumCPFRelator(rs.getString("NUM_CPF"));
				guiaBean.setCodRelator(rs.getString("COD_RELATOR"));
				guiaBean.setNomRelator(rs.getString("NOM_RELATOR"));
				guiaBean.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				guiaBean.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				guiaBean.setDatPubliPDO(rs.getString("DAT_PUBDO"));
				guiaBean.setNumSessao(rs.getString("NUM_SESSAO"));
				guiaBean.setDatSessao(rs.getString("DAT_SESSAO"));
				guiaBean.setTipoGuia(rs.getString("TIPO_GUIA"));
				String sigOrgao = rs.getString("SIG_ORGAO");   
				String nomOrgao = rs.getString("NOM_ORGAO");                
				
				ArrayList Autos = new ArrayList() ;
				sCmd = "Select A.COD_AUTO_INFRACAO,I.COD_ITEM_LOTE_RELATOR," +
				"I.NUM_AUTO_INFRACAO,I.NUM_PROCESSO,TO_CHAR(I.DAT_PROCESSO,'dd/mm/yyyy') DAT_PROCESSO, " +
				"I.NUM_PLACA,TO_CHAR(I.DAT_INFRACAO,'dd/mm/yyyy') DAT_INFRACAO, " +
				"I.DSC_INFRACAO,I.DSC_LOCAL_INFRACAO," +
				"I.NUM_REQUERIMENTO,I.IND_TIPO_REQUERIMENTO," +
				"I.NOM_PROPRIETARIO,I.DSC_ENQUADRAMENTO,I.MSG_ERRO, " +
				"I.COD_RESULT_RS,I.TXT_MOTIVO_RS "+
				"FROM TSMI_ITEM_LOTE_RELATOR I, TSMI_AUTO_INFRACAO A "+
				"WHERE I.COD_LOTE_RELATOR='"+guiaBean.getNumGuia()+"' AND "+
				"I.NUM_AUTO_INFRACAO=A.NUM_AUTO_INFRACAO order by I.NUM_PROCESSO ASC";
				rs2   = stmt2.executeQuery(sCmd) ;
				while (rs2.next()) {
					REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
					myAuto.setInfracao(new InfracaoBean()); 
					myAuto.setProprietario(new ResponsavelBean());
					List reqs = new ArrayList();
					REC.RequerimentoBean req = new REC.RequerimentoBean();
					myAuto.setCodOrgao(guiaBean.getCodOrgaoAtuacao());
					myAuto.setSigOrgao(sigOrgao);
					myAuto.setNomOrgao(nomOrgao);
					String sCodAutoInfracao=rs2.getString("COD_AUTO_INFRACAO");
					myAuto.setCodAutoInfracao(rs2.getString("COD_ITEM_LOTE_RELATOR"));
					myAuto.setNumAutoInfracao(rs2.getString("NUM_AUTO_INFRACAO"));           
					myAuto.setNumProcesso(rs2.getString("NUM_PROCESSO"));    
					myAuto.setDatProcesso(rs2.getString("DAT_PROCESSO"));     
					myAuto.setNumPlaca(rs2.getString("NUM_PLACA"));  
					myAuto.setDatInfracao(rs2.getString("DAT_INFRACAO"));
					myAuto.getInfracao().setDscInfracao(rs2.getString("DSC_INFRACAO"));  
					myAuto.setDscLocalInfracao(rs2.getString("DSC_LOCAL_INFRACAO"));
					myAuto.setNumRequerimento(rs2.getString("NUM_REQUERIMENTO"));
					myAuto.setTpRequerimento(rs2.getString("IND_TIPO_REQUERIMENTO"));
					myAuto.getProprietario().setNomResponsavel(rs2.getString("NOM_PROPRIETARIO"));           
					myAuto.getInfracao().setDscEnquadramento(rs2.getString("DSC_ENQUADRAMENTO"));            
					myAuto.setMsgErro(rs2.getString("MSG_ERRO"));
					req.setCodResultRS(rs2.getString("COD_RESULT_RS"));
					req.setTxtMotivoRS(rs2.getString("TXT_MOTIVO_RS"));
					
					
					RequerimentoBean myReq = new RequerimentoBean();
					myReq.setNumRequerimento(myAuto.getNumRequerimento());
					myReq.setCodAutoInfracao(sCodAutoInfracao);
					dao.LeRequerimentoLocal(myReq,conn);
					req.setCodRemessa(myReq.getCodRemessa());
					req.setDatEnvioGuia(myReq.getDatEnvioGuia());   
					
					reqs.add(req);          
					myAuto.setRequerimentos(reqs);
					Autos.add(myAuto);
					guiaBean.setAutos(Autos);
				}
				if(guiaBean.getAutos().size()>0)
					guias.add(guiaBean);
				
			}
			rs.close();
			if(rs1 != null) rs1.close();
			stmt.close();                   
		}   
		catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		}
		catch (Exception e) {   
			throw new sys.DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}           
		return guias;
	}
	
	
	
	
}


