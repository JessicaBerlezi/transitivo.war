package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BeanException;
import sys.DaoException;

public class RemessaReceberCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/REC/RemessaReemite.jsp" ;  
	
	public RemessaReceberCmd() {
		next = jspPadrao;
	}
	
	public RemessaReceberCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
			ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
			RemessaBean RemessaId         = (RemessaBean)session.getAttribute("RemessaId") ;
			if (RemessaId==null)  RemessaId= new RemessaBean() ;	  	
			//Carrego os Beans		
			RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
			RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			RemessaId.setUsername(UsrLogado.getNomUserName());	
			String indRecurso= UsuarioFuncBeanId.getJ_sigFuncao();
			
			String dataIni = req.getParameter("De");
			if (dataIni == null) dataIni = "";
			RemessaId.setDataIni(dataIni);
			
			String dataFim = req.getParameter("Ate");
			if (dataFim == null) dataFim = "";
			RemessaId.setDataFim(dataFim);
			
			
			
			
			String acao    = req.getParameter("acao");   
			if (acao==null) acao = " ";	
			if ( (acao.equals(" ")) || (acao.equals("Novo")) ){		  
				RemessaId         = new RemessaBean() ;
				RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
				RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
				RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				RemessaId.setUsername(UsrLogado.getNomUserName());
				RemessaId.setStatusRemessa("1");	
				RemessaId.setMsgErro("");						  				  	
				session.removeAttribute("RemessaId") ;
				acao = " ";
			}
			else {
				if ("LeRemessa,InformaResult,Classifica,MostraAuto,imprimirInf,ImprimirRemessa".indexOf(acao)<0)	{
					sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
					cmd.setNext(this.next) ;
					nextRetorno = cmd.execute(req);
				}		
			}
			if (acao.equals("LeRemessa")) {
				RemessaId         = new RemessaBean() ;		
				RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
				RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
				RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				RemessaId.setCodOrgaoLotacaoRecebimento(UsrLogado.getCodOrgaoAtuacao());
				RemessaId.setUsernameRecebimento(UsrLogado.getNomUserName());
				RemessaId.setStatusRemessa("1");							
				RemessaId.setCodRemessa(req.getParameter("numRemessa"));
				RemessaId.LeRemessa(UsrLogado,UsuarioFuncBeanId);
				if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
				if (RemessaId.getDatProc().length()==0)RemessaId.setDatProc(sys.Util.formatedToday().substring(0,10));		    
				nextRetorno = "/REC/RemessaReceber.jsp";
			}	        
			if (acao.equals("Classifica")) {
				RemessaId.Classifica(req.getParameter("ordem"))	;       
				nextRetorno = "/REC/RemessaReceber.jsp";
			}
			if  (acao.equals("ImprimirRemessa"))  {	      	  
				if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
				nextRetorno = "/REC/RemessaPreparaImp.jsp" ;      	  
			}
			if (acao.equals("AtualizaRecebimento")) {
				RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));	  		  	
				RemessaId.setCodOrgaoLotacaoRecebimento(UsrLogado.getOrgao().getCodOrgao());
				RemessaId.setDatProcRecebimento(sys.Util.formatedToday().substring(0,10));
				RemessaId.setUsernameRecebimento(UsrLogado.getNomUserName());
				RemessaId.setStatusRemessa("1");							
				
				int inc = AtualizaRecebimento(req,RemessaId,UsrLogado) ;
				if (inc>0) {
					RemessaId.setMsgErro("Guia de Remessa "+RemessaId.getCodRemessa()+" recebida com sucesso para "+inc+" Processos/Requerimentos."); 			
					nextRetorno = "/REC/RemessaMostra.jsp" ;
				}
				else {
					RemessaId.setMsgErro(RemessaId.getMsgErro()+" \n Nenhum Processos/Requerimentos incluido ");
					nextRetorno = "/REC/RemessaMostra.jsp" ; 
				}				
			}
			if  (acao.equals("ImprimirRemessa"))  {	      	  
				if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
				nextRetorno = "/REC/RemessaPreparaImp.jsp" ;      	  
			}
			
			if (acao.equals("MostraAuto"))  {
				AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
				if (acao.equals("MostraAuto")) { 
					AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
					AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
					AutoInfracaoBeanId.LeRequerimento(UsrLogado);
					nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
				}
				else {
					AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
					AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
					nextRetorno = "/REC/AutoImp.jsp" ;
				}					 
				// Verifica se o Usuario logado ve Todos os Orgaos
				String temp = UsrLogado.getCodOrgaoAtuacao();
				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
				AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
				UsrLogado.setCodOrgaoAtuacao(temp);	
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			}	
			
			// processamento de saida dos formularios
			if (acao.equals("R")) session.removeAttribute("RemessaId") ;
			else 				    session.setAttribute("RemessaId",RemessaId) ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("RemessaReceberCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public int AtualizaRecebimento(HttpServletRequest req,RemessaBean RemessaId,
			ACSS.UsuarioBean UsrLogado) throws sys.CommandException, DaoException, BeanException {
		int inc = 0 ;
		Vector vErro = new Vector() ;
		if ((vErro.size()==0) && (RemessaId.getAutos().size()==0)) vErro.addElement("Nenhum processo selecionado.") ;
		if (vErro.size()>0) RemessaId.setMsgErro(vErro);
		else{	 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			inc = dao.RemessaGrava(RemessaId) ; 
		}	
		return inc;   
	}
	
}
