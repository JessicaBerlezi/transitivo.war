package REC;


import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Status<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Status<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class StatusCmd extends sys.Command {

  private static final String jspPadrao="/REC/Status.jsp";    
  private String next;

  public StatusCmd() {
    next = jspPadrao;
  }

  public StatusCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do Usuario, se n�o existir
		TAB.StatusBean StatusId = (TAB.StatusBean)req.getAttribute("StatusId") ;
		if(StatusId == null) StatusId = new TAB.StatusBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null) acao =" ";   
		  
	   
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null || atualizarDependente.equals(""))
		  atualizarDependente ="N";  

		StatusId.setAtualizarDependente(atualizarDependente);
		
		StatusId.getStatus(20,5) ;

		Vector vErro = new Vector(); 
         
		 
	    if(acao.compareTo("A") == 0){
			String[] codStatus= req.getParameterValues("codStatus");
			if(codStatus == null) codStatus = new String[0];  
			  
			String[] nomStatus = req.getParameterValues("nomStatus");
			if(nomStatus == null)  nomStatus = new String[0];         
    
			  
			vErro = isParametros(codStatus,nomStatus) ;
						 
            if (vErro.size()==0) {
				Vector status = new Vector(); 
				for (int i = 0; i < nomStatus.length;i++) {
					TAB.StatusBean myStatus = new TAB.StatusBean() ;		
					myStatus.setNomStatus(nomStatus[i]);
					myStatus.setCodStatus(codStatus[i]);	  
					status.addElement(myStatus) ; 					
				}
				StatusId.setStatus(status) ;
				StatusId.isInsertStatus() ;
			    StatusId.getStatus(20,5) ;						
			}
			else StatusId.setMsgErro(vErro) ;  	
	    }
	    
	    if(acao.compareTo("I") == 0){
	    	req.setAttribute("Status",StatusId.getStatus(0,0));
	    	nextRetorno = "/REC/StatusImp.jsp";
	    }
	    
		req.setAttribute("StatusId",StatusId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("StatusCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  private Vector isParametros(String[] codStatus,String[] nomStatus) {
		Vector vErro = new Vector() ;		 
		if (nomStatus.length!=codStatus.length) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 
}


