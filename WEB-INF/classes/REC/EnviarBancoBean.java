package REC;

/**
* <b>Title:</b>        SMIT - Enviar para Publicacao - <br>
* <b>Description:</b>  Enviar para Publicacao <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/
public class EnviarBancoBean extends EventoBean  {   
  
  private String indMovimento;
  private String datEnvioBanco;  
  
  public EnviarBancoBean()  throws sys.BeanException {

  super() ;
  indMovimento = "1";
  datEnvioBanco = "";  
  }
//-------------------------------------------------------------------------- 	  	  
public void setIndMovimento(String indMovimento)   {
	if (indMovimento==null) indMovimento="1";
	this.indMovimento = indMovimento;  	
}
public String getIndMovimento()   {
	return this.indMovimento ;  	
}
//-------------------------------------------------------------------------- 	  	  
public void setDatEnvioBanco(String datEnvioBanco)   {
	if (datEnvioBanco==null) datEnvioBanco="";
	this.datEnvioBanco = datEnvioBanco;  	
}
public String getDatEnvioBanco()   {
	return this.datEnvioBanco ;  	
}

//	-------------------------------------------------------------------------- 	  	  
  public void setJ_sigFuncao(String j_sigFuncao)   
  {  	
	if (j_sigFuncao==null) j_sigFuncao = "" ;
	setCFuncao(j_sigFuncao) ;
	// Enviar Movimento para Banco	
		setNFuncao("Enviar Movimento para Banco") ;	
		setOrigemEvento("100") ;
		setCodEvento("407") ;									
    return ;	
  } 
//--------------------------------------------------------------------------  
   public void setEventoOK(REC.AutoInfracaoBean myAuto,String dtRef) throws sys.BeanException   {
	setMsg("");
	setEventoOK("N");	
	if ("".equals(myAuto.getNomStatus())) return ;
	setMsg(myAuto.getNomStatus()+" ("+myAuto.getDatStatus()+")") ;
    try {
    	
		  /*if (myAuto.getCodStatus().length()>=2)  {
			  if ("1".equals(myAuto.getIndPago())) setEventoOK("S");
			  else 	setMsg(getMsg() + " - Auto PAGO n�o permite "+getNFuncao());
			  */
			setEventoOK("S");
		  /*}
		  else 	setMsg(getMsg() + " - Status do Auto n�o permite "+getNFuncao());
		  */				
	   }
	   catch (Exception ue) {
		 throw new sys.BeanException("EnviarBancoBean: " + ue.getMessage());
	   }			   
	  return ;	
  } 
}
