package REC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import ACSS.Ata;
import ACSS.Dao;
import ACSS.ParamSistemaBean;
import ACSS.UsuarioBean;
import sys.BeanException;
import sys.EnderecoBean;
import sys.Util;

/**
 * <b>Title:</b> SMIT - Entrada de Recurso <br>
 * <b>Description:</b> Auto Infracao Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class AutoInfracaoBean {
	public static final String STATUS_REGISTRADO = "0";
	public static final String STATUS_DP = "5";
	public static final String STATUS_PENALIDADE_REGISTRADA = "10";
	public static final String STATUS_RECURSO_1P = "15";
	public static final String STATUS_AGUARDANDO_2P = "30";
	public static final String STATUS_RECURSO_2P = "35";
	public static final String STATUS_DEFERIMENTO_DP = "98";
	public static final String STATUS_DEFERIMENTO_1P = "96";
	public static final String STATUS_DEFERIMENTO_2P = "97";
	public static final String STATUS_TRANSITADO_JULGADO = "99";
	public static final String STATUS_PAE = "1000";

	private String msgErro;
	private String msgOk;
	private String codAutoInfracao;
	private String numAutoInfracao;
	private String numNotificacao;
	private String datNotificacao;
	private String nomMotivoNotificacao;
	private String datInfracao;
	private String valHorInfracao;
	private String dscLocalInfracao;
	private String numPlaca;
	private String numRenavam;
	private String numProcesso;
	private String datProcesso;
	private String datExpAut;
	private String datExpPen;

	private InfracaoBean infracao;
	private String codAgente;
	private String nomAgente;
	private String codOrgao;
	private String sigOrgao;
	private String nomOrgao;
	private ACSS.OrgaoBean orgao;

	private String dscLocalAparelho;
	private String numCertAferAparelho;
	private String datUltAferAparelho;
	private String numIdentAparelho;
	private String numInmetroAparelho;
	private String codTipDispRegistrador;

	private String datVencimento;

	private String codMunicipio;
	private String nomMunicipio;

	private String valVelocPermitida;
	private String valVelocAferida;
	private String valVelocConsiderada;

	private String datAlteracao;
	private String datInclusao;
	private String datRegistro;
	private String dscSituacao;

	private VeiculoBean veiculo;
	private String indCondutorIdentificado; // 0 - Nao 1 - Sim
	private ResponsavelBean condutor;
	private ResponsavelBean proprietario;

	private String codLote;
	private String codArquivo;
	private String nomUserName;

	private String indTPCV;

	private String codStatus;
	private String datStatus;
	private String nomStatus;

	private String datLimiteRecurso;
	private String datLimiteRecursoFolga;
	private String datEnvioDO;
	private String codEnvioDO;
	private String codResultPubDO;
	private String nomUsernameDO;
	private String codOrgaoLotacaoDO;
	private String datProcDO;

	private String datPublPDO;
	private String nomUsernamePDO;
	private String codOrgaoLotacaoPDO;
	private String datProcPDO;
	private String codBarra;

	private String indFase;
	private String indSituacao;
	private String indPago;
	private String datPag;
	private String autoAntigo;

	private String numNotifAut;
	private String datNotifAut;
	private String codNotifAut;
	private String nomMotivoNotifAut;
	private String numARNotifAut;

	private String numNotifPen;
	private String datNotifPen;
	private String codNotifPen;
	private String nomMotivoNotifPen;
	private String numARNotifPen;

	private String numAutoOrigem;
	private String numInfracaoOrigem;

	private String identAutoOrigem;
	private String numInfracaoRenainf;

	private String identOrgao;
	private String dscResumoInfracao;
	private String dscMarcaModeloAparelho;
	private String dscLocalInfracaoNotificacao;
	private String codVinculada;
	private String txtEmail;

	private List<RequerimentoBean> requerimentos;
	private List<HistoricoBean> historicos;

	private String eventoOK;
	private String numRequerimento;
	private String codRequerimento;
	private String tpRequerimento;
	private String chaveSort;

	private String dscAgente;
	private String dscUnidade;
	private String dscLote;

	private boolean temAIDigitalizado;
	private boolean temARDigitalizado;
	private boolean temFotoDigitalizada;

	private AIDigitalizadoBean AIDigitalizado;
	private ARDigitalizadoBean[] ARDigitalizado;
	private FotoDigitalizadaBean FotoDigitalizada;

	private Date datDigitalizacao;
	private ArrayList<AutoInfracaoBean> autos;
	private String sit_digitaliza;

	private String sDtGerCBProcesso;
	private String sDtGerCBNotificacaoAut;
	private String sDtGerCBNotificacaoPen;
	private int iSeqGeracaoCBNotificacaoAut;
	private int iSeqGeracaoCBNotificacaoPen;

	/* DPWEB */
	private String codAcessoWeb;
	private String obsCondutor;

	private String indPontuacao;
	private String indParcelamento;

	private String codAta;
	private String dsAta;

	private int cdNatureza;

	public AutoInfracaoBean() throws sys.BeanException {

		msgErro = "";
		msgOk = "";
		codAutoInfracao = "";
		numAutoInfracao = "";
		numNotificacao = "";
		datNotificacao = "";
		nomMotivoNotificacao = "";
		datInfracao = "";
		valHorInfracao = "";
		dscLocalInfracao = "";
		numPlaca = "";
		numRenavam = "";
		numProcesso = "";
		datProcesso = "";
		datExpAut = "";
		datExpPen = "";
		infracao = new InfracaoBean();
		codAgente = ""; // instituicao
		nomAgente = "";
		codOrgao = "";
		sigOrgao = "";
		nomOrgao = "";
		orgao = new ACSS.OrgaoBean();
		dscLocalAparelho = "";
		numCertAferAparelho = "";
		datUltAferAparelho = "";
		numIdentAparelho = "";
		numInmetroAparelho = "";
		codTipDispRegistrador = "";
		datVencimento = "";
		codMunicipio = "";
		nomMunicipio = "";
		valVelocPermitida = "";
		valVelocAferida = "";
		valVelocConsiderada = "";
		datAlteracao = "";
		datInclusao = "";
		datRegistro = "";
		dscSituacao = "";
		veiculo = new REC.VeiculoBean();
		condutor = new REC.ResponsavelBean();
		proprietario = new REC.ResponsavelBean();
		indCondutorIdentificado = "0"; // 0 - Nao 1 - Sim
		codLote = "";
		codArquivo = "";
		nomUserName = "";
		indTPCV = "0";
		codStatus = "";
		datStatus = "";
		nomStatus = "";
		datLimiteRecurso = "";
		datLimiteRecursoFolga = "";
		datEnvioDO = "";
		codEnvioDO = "";
		codResultPubDO = "";
		nomUsernameDO = "";
		codOrgaoLotacaoDO = "";
		datProcDO = "";
		datPublPDO = "";
		nomUsernamePDO = "";
		codOrgaoLotacaoPDO = "";
		datProcPDO = "";
		codBarra = "";
		indFase = "";
		indSituacao = "";
		indPago = "";
		datPag = "";
		autoAntigo = "0";
		numNotifAut = "";
		datNotifAut = "";
		codNotifAut = "";
		nomMotivoNotifAut = "";
		numARNotifAut = "";
		numNotifPen = "";
		datNotifPen = "";
		codNotifPen = "";
		nomMotivoNotifPen = "";
		numARNotifPen = "";
		requerimentos = new ArrayList<RequerimentoBean>();
		historicos = new ArrayList<HistoricoBean>();
		eventoOK = "";
		numRequerimento = "";
		codRequerimento = "";
		tpRequerimento = "";
		chaveSort = "";
		dscAgente = "";
		dscUnidade = "";
		dscLote = "";
		numAutoOrigem = "";
		numInfracaoOrigem = "";
		identAutoOrigem = "";
		numInfracaoRenainf = "";
		identOrgao = "";
		dscResumoInfracao = "";
		dscMarcaModeloAparelho = "";
		dscLocalInfracaoNotificacao = "";
		codVinculada = "";
		txtEmail = "";
		temAIDigitalizado = false;
		temARDigitalizado = false;
		temFotoDigitalizada = false;

		datDigitalizacao = null;
		autos = new ArrayList<AutoInfracaoBean>();
		sit_digitaliza = "N";

		sDtGerCBProcesso = "";
		sDtGerCBNotificacaoAut = "";
		sDtGerCBNotificacaoPen = "";
		iSeqGeracaoCBNotificacaoAut = 0;
		iSeqGeracaoCBNotificacaoPen = 0;
		/* DPWEB */
		codAcessoWeb = "";
		obsCondutor = "";
		indPontuacao = "";
		indParcelamento = "";

		datDigitalizacao = new Date();
	}

	public static AutoInfracaoBean consultarAutoPor(int cod) throws Exception {
		HashMap<String, Object> hashAuto = Dao.getInstance().consultarAutoPor(cod);

		return mapHashMapToEntity(hashAuto);
	}

	public static AutoInfracaoBean consultarAutoPor(String numAuto, int cdNatureza) throws Exception {
		HashMap<String, Object> hashAuto = Dao.getInstance().consultarAutoPor(numAuto, cdNatureza);

		return mapHashMapToEntity(hashAuto);
	}

	public static AutoInfracaoBean consultarAutoPorNumProcesso(String numProcesso, int cdNatureza) throws Exception {
		HashMap<String, Object> hashAuto = Dao.getInstance().consultarAutoPorNumProcesso(numProcesso, cdNatureza);

		return mapHashMapToEntity(hashAuto);
	}

	public static List<AutoInfracaoBean> consultarAutosPor(String placa, String numAuto, String numProcesso,
			String orgao, String codNatureza, String dataInicial, String dataFinal) throws Exception {
		List<AutoInfracaoBean> lista = Dao.getInstance().consultarAutosPor(placa, numAuto, numProcesso, orgao,
				codNatureza, dataInicial, dataFinal);

		return lista;
	}

	public static HashMap<String, Object> mapResultSetToHashMap(ResultSet result) {
		HashMap<String, Object> auto = null;

		try {
			if (result != null) {
				auto = new HashMap<String, Object>();

				auto.put("COD_AUTO_INFRACAO", result.getInt("COD_AUTO_INFRACAO"));
				auto.put("NUM_AUTO_INFRACAO", result.getString("NUM_AUTO_INFRACAO"));
				auto.put("DAT_INFRACAO", result.getString("DAT_INFRACAO"));
				auto.put("VAL_HOR_INFRACAO", result.getString("VAL_HOR_INFRACAO"));
				auto.put("DSC_LOCAL_INFRACAO", result.getString("DSC_LOCAL_INFRACAO"));
				auto.put("NUM_PLACA", result.getString("NUM_PLACA"));
				auto.put("NUM_PROCESSO", result.getString("NUM_PROCESSO"));
				auto.put("DAT_PROCESSO", result.getString("DAT_PROCESSO"));
				auto.put("COD_AGENTE", result.getString("COD_AGENTE"));
				auto.put("COD_ORGAO", result.getInt("COD_ORGAO"));
				auto.put("DSC_LOCAL_APARELHO", result.getString("DSC_LOCAL_APARELHO"));
				auto.put("NUM_CERTIF_AFER_APARELHO", result.getString("NUM_CERTIF_AFER_APARELHO"));
				auto.put("DAT_ULT_AFER_APARELHO", result.getString("DAT_ULT_AFER_APARELHO"));
				auto.put("NUM_IDENT_APARELHO", result.getString("NUM_IDENT_APARELHO"));
				auto.put("NUM_INMETRO_APARELHO", result.getString("NUM_INMETRO_APARELHO"));
				auto.put("COD_TIPO_DISP_REGISTRADOR", result.getString("COD_TIPO_DISP_REGISTRADOR"));
				auto.put("DAT_VENCIMENTO", result.getString("DAT_VENCIMENTO"));
				auto.put("COD_MUNICIPIO", result.getInt("COD_MUNICIPIO"));
				auto.put("VAL_VELOC_PERMITIDAA", result.getInt("VAL_VELOC_PERMITIDAA"));
				auto.put("VAL_VELOC_AFERIDA", result.getInt("VAL_VELOC_AFERIDA"));
				auto.put("VAL_VELOC_CONSIDERADA", result.getInt("VAL_VELOC_CONSIDERADA"));
				auto.put("DSC_SITUACAO", result.getString("DSC_SITUACAO"));
				auto.put("IND_CONDUTOR_IDENTIFICADO", result.getString("IND_CONDUTOR_IDENTIFICADO"));
				auto.put("NUM_LOTE", result.getString("NUM_LOTE"));
				auto.put("COD_STATUS", result.getInt("COD_STATUS"));
				auto.put("DAT_STATUS", result.getString("DAT_STATUS"));
				auto.put("DAT_PUBDO", result.getString("DAT_PUBDO"));
				auto.put("COD_ENVIO_PUBDO", result.getString("COD_ENVIO_PUBDO"));
				auto.put("DAT_ENVDO", result.getString("DAT_ENVDO"));
				auto.put("IND_TP_CV", result.getString("IND_TP_CV"));
				auto.put("COD_RESULT_PUBDO", result.getString("COD_RESULT_PUBDO"));
				auto.put("COD_BARRA", result.getString("COD_BARRA"));
				auto.put("IND_SITUACAO", result.getString("IND_SITUACAO"));
				auto.put("IND_FASE", result.getString("IND_FASE"));
				auto.put("IND_PAGO", result.getString("IND_PAGO"));
				auto.put("NUM_IDENT_ORGAO", result.getString("NUM_IDENT_ORGAO"));
				auto.put("DSC_RESUMO_INFRACAO", result.getString("DSC_RESUMO_INFRACAO"));
				auto.put("DSC_MARCA_MODELO_APARELHO", result.getString("DSC_MARCA_MODELO_APARELHO"));
				auto.put("DSC_LOCAL_INFRACAO_NOTIF", result.getString("DSC_LOCAL_INFRACAO_NOTIF"));
				auto.put("COD_INFRACAO", result.getString("COD_INFRACAO"));
				auto.put("DSC_INFRACAO", result.getString("DSC_INFRACAO"));
				auto.put("DSC_ENQUADRAMENTO", result.getString("DSC_ENQUADRAMENTO"));
				auto.put("VAL_REAL", result.getFloat("VAL_REAL"));
				auto.put("NUM_PONTO", result.getString("NUM_PONTO"));
				auto.put("DSC_COMPETENCIA", result.getString("DSC_COMPETENCIA"));
				auto.put("IND_COND_PROP", result.getString("IND_COND_PROP"));
				auto.put("IND_SUSPENSAO", result.getString("IND_SUSPENSAO"));
				auto.put("VAL_REAL_DESCONTO", result.getFloat("VAL_REAL_DESCONTO"));
				auto.put("COD_MARCA_MODELO", result.getInt("COD_MARCA_MODELO"));
				auto.put("COD_ESPECIE", result.getInt("COD_ESPECIE"));
				auto.put("COD_CATEGORIA", result.getInt("COD_CATEGORIA"));
				auto.put("COD_TIPO", result.getInt("COD_TIPO"));
				auto.put("COD_COR", result.getInt("COD_COR"));
				auto.put("NOM_PROPRIETARIO", result.getString("NOM_PROPRIETARIO"));
				auto.put("TIP_DOC_PROPRIETARIO", result.getString("TIP_DOC_PROPRIETARIO"));
				auto.put("NUM_DOC_PROPRIETARIO", result.getString("NUM_DOC_PROPRIETARIO"));
				auto.put("TIP_CNH_PROPRIETARIO", result.getString("TIP_CNH_PROPRIETARIO"));
				auto.put("NUM_CNH_PROPRIETARIO", result.getString("NUM_CNH_PROPRIETARIO"));
				auto.put("DSC_END_PROPRIETARIO", result.getString("DSC_END_PROPRIETARIO"));
				auto.put("NUM_END_PROPRIETARIO", result.getString("NUM_END_PROPRIETARIO"));
				auto.put("DSC_COMP_PROPRIETARIO", result.getString("DSC_COMP_PROPRIETARIO"));
				auto.put("COD_MUN_PROPRIETARIO", result.getInt("COD_MUN_PROPRIETARIO"));
				auto.put("NUM_CEP_PROPRIETARIO", result.getString("NUM_CEP_PROPRIETARIO"));
				auto.put("UF_PROPRIETARIO", result.getString("UF_PROPRIETARIO"));
				auto.put("NOM_CONDUTOR", result.getString("NOM_CONDUTOR"));
				auto.put("TIP_DOC_CONDUTOR", result.getString("TIP_DOC_CONDUTOR"));
				auto.put("NUM_DOC_CONDUTOR", result.getString("NUM_DOC_CONDUTOR"));
				auto.put("TIP_CNH_CONDUTOR", result.getString("TIP_CNH_CONDUTOR"));
				auto.put("NUM_CNH_CONDUTOR", result.getString("NUM_CNH_CONDUTOR"));
				auto.put("DSC_END_CONDUTOR", result.getString("DSC_END_CONDUTOR"));
				auto.put("NUM_END_CONDUTOR", result.getString("NUM_END_CONDUTOR"));
				auto.put("DSC_COMP_CONDUTOR", result.getString("DSC_COMP_CONDUTOR"));
				auto.put("COD_MUN_CONDUTOR", result.getInt("COD_MUN_CONDUTOR"));
				auto.put("NUM_CEP_CONDUTOR", result.getString("NUM_CEP_CONDUTOR"));
				auto.put("UF_CONDUTOR", result.getString("UF_CONDUTOR"));
				auto.put("NUM_NOTIF_AUTUACAO", result.getInt("NUM_NOTIF_AUTUACAO"));
				auto.put("DAT_NOTIF_AUTUACAO", result.getString("DAT_NOTIF_AUTUACAO"));
				auto.put("COD_ENT_AUTUACAO", result.getString("COD_ENT_AUTUACAO"));
				auto.put("NUM_LOTE_AUTUACAO", result.getString("NUM_LOTE_AUTUACAO"));
				auto.put("NUM_NOTIF_PENALIDADE", result.getInt("NUM_NOTIF_PENALIDADE"));
				auto.put("DAT_NOTIF_PENALIDADE", result.getString("DAT_NOTIF_PENALIDADE"));
				auto.put("COD_ENT_PENALIDADE", result.getString("COD_ENT_PENALIDADE"));
				auto.put("NUM_LOTE_PENALIDADE", result.getString("NUM_LOTE_PENALIDADE"));
				auto.put("DSC_AGENTE", result.getString("DSC_AGENTE"));
				auto.put("DSC_UNIDADE", result.getString("DSC_UNIDADE"));
				auto.put("DAT_PAGAMENTO", result.getString("DAT_PAGAMENTO"));
				auto.put("NUM_AUTO_ORIGEM", result.getString("NUM_AUTO_ORIGEM"));
				auto.put("COD_INFRACAO_ORIGEM", result.getInt("COD_INFRACAO_ORIGEM"));
				auto.put("IND_TIPO_AUTO", result.getString("IND_TIPO_AUTO"));
				auto.put("NUM_AUTO_RENAINF", result.getString("NUM_AUTO_RENAINF"));
				auto.put("TXT_EMAIL", result.getString("TXT_EMAIL"));
				auto.put("DAT_INCLUSAO", result.getString("DAT_INCLUSAO"));
				auto.put("DAT_ALTERACAO", result.getString("DAT_ALTERACAO"));
				auto.put("SIT_AI_DIGITALIZADO", result.getString("SIT_AI_DIGITALIZADO"));
				auto.put("SIT_AR_DIGITALIZADO", result.getString("SIT_AR_DIGITALIZADO"));
				auto.put("DAT_DIGITALIZACAO", result.getString("DAT_DIGITALIZACAO"));
				auto.put("DAT_REGISTRO", result.getString("DAT_REGISTRO"));
				auto.put("SIT_DIGITALIZACAO", result.getString("SIT_DIGITALIZACAO"));
				auto.put("VAL_VELOC_PERMITIDA", result.getFloat("VAL_VELOC_PERMITIDA"));
				auto.put("NUM_RENAVAM", result.getString("NUM_RENAVAM"));
				auto.put("IDENT_ORGAO", result.getString("IDENT_ORGAO"));
				auto.put("COD_ACESSO_WEB", result.getString("COD_ACESSO_WEB"));
				auto.put("OBS_CONDUTOR", result.getString("OBS_CONDUTOR"));
				auto.put("IND_PONTUACAO", result.getString("IND_PONTUACAO"));
				auto.put("IND_PARCELAMENTO", result.getString("IND_PARCELAMENTO"));
				auto.put("COD_ATA", result.getInt("COD_ATA"));
				auto.put("DS_ATA", result.getString("DS_ATA"));
				auto.put("cd_natureza", result.getInt("cd_natureza"));
			}
		} catch (Exception ex) {
		}

		return auto;
	}

	public static AutoInfracaoBean mapHashMapToEntity(HashMap<String, Object> hashAuto) throws BeanException {
		AutoInfracaoBean auto = new AutoInfracaoBean();

		if (hashAuto == null)
			return null;

		for (String key : hashAuto.keySet()) {
			if (key == "NUM_PONTO")
				hashAuto.put(key, "0");
			else if (hashAuto.get(key) == null)
				hashAuto.put(key, "");
		}

		auto.codAutoInfracao = hashAuto.get("COD_AUTO_INFRACAO").toString();
		auto.numAutoInfracao = hashAuto.get("NUM_AUTO_INFRACAO").toString();
		auto.datInfracao = hashAuto.get("DAT_INFRACAO").toString();
		auto.valHorInfracao = hashAuto.get("VAL_HOR_INFRACAO").toString();
		auto.dscLocalInfracao = hashAuto.get("DSC_LOCAL_INFRACAO").toString();
		auto.numPlaca = hashAuto.get("NUM_PLACA").toString();
		auto.numProcesso = hashAuto.get("NUM_PROCESSO").toString();
		auto.datProcesso = hashAuto.get("DAT_PROCESSO").toString();
		auto.codAgente = hashAuto.get("COD_AGENTE").toString();
		auto.codOrgao = hashAuto.get("COD_ORGAO").toString();
		auto.dscLocalAparelho = hashAuto.get("DSC_LOCAL_APARELHO").toString();
		auto.numCertAferAparelho = hashAuto.get("NUM_CERTIF_AFER_APARELHO").toString();
		auto.datUltAferAparelho = hashAuto.get("DAT_ULT_AFER_APARELHO").toString();
		auto.numIdentAparelho = hashAuto.get("NUM_IDENT_APARELHO").toString();
		auto.numInmetroAparelho = hashAuto.get("NUM_INMETRO_APARELHO").toString();
		auto.codTipDispRegistrador = hashAuto.get("COD_TIPO_DISP_REGISTRADOR").toString();
		auto.datVencimento = hashAuto.get("DAT_VENCIMENTO").toString();
		auto.codMunicipio = hashAuto.get("COD_MUNICIPIO").toString();
		auto.valVelocPermitida = hashAuto.get("VAL_VELOC_PERMITIDAA").toString();
		auto.valVelocAferida = hashAuto.get("VAL_VELOC_AFERIDA").toString();
		auto.valVelocConsiderada = hashAuto.get("VAL_VELOC_CONSIDERADA").toString();
		auto.dscSituacao = hashAuto.get("DSC_SITUACAO").toString();
		auto.indCondutorIdentificado = hashAuto.get("IND_CONDUTOR_IDENTIFICADO").toString();
		auto.codLote = hashAuto.get("NUM_LOTE").toString();
		auto.codStatus = hashAuto.get("COD_STATUS").toString();
		auto.datStatus = hashAuto.get("DAT_STATUS").toString();
		auto.datPublPDO = hashAuto.get("DAT_PUBDO").toString();
		auto.codEnvioDO = hashAuto.get("COD_ENVIO_PUBDO").toString();
		auto.datEnvioDO = hashAuto.get("DAT_ENVDO").toString();
		auto.indTPCV = hashAuto.get("IND_TP_CV").toString();
		auto.codResultPubDO = hashAuto.get("COD_RESULT_PUBDO").toString();
		auto.codBarra = hashAuto.get("COD_BARRA").toString();
		auto.indSituacao = hashAuto.get("IND_SITUACAO").toString();
		auto.indFase = hashAuto.get("IND_FASE").toString();
		auto.indPago = hashAuto.get("IND_PAGO").toString();
		auto.identOrgao = hashAuto.get("NUM_IDENT_ORGAO").toString();
		auto.dscResumoInfracao = hashAuto.get("DSC_RESUMO_INFRACAO").toString();
		auto.dscMarcaModeloAparelho = hashAuto.get("DSC_MARCA_MODELO_APARELHO").toString();
		auto.dscLocalInfracaoNotificacao = hashAuto.get("DSC_LOCAL_INFRACAO_NOTIF").toString();

		auto.infracao.setCodInfracao(hashAuto.get("COD_INFRACAO").toString());
		auto.infracao.setDscInfracao(hashAuto.get("DSC_INFRACAO").toString());
		auto.infracao.setDscEnquadramento(hashAuto.get("DSC_ENQUADRAMENTO").toString());
		auto.infracao.setValReal(hashAuto.get("VAL_REAL").toString());
		auto.infracao.setNumPonto(hashAuto.get("NUM_PONTO").toString());
		auto.infracao.setDscCompetencia(hashAuto.get("DSC_COMPETENCIA").toString());
		auto.infracao.setIndTipo(hashAuto.get("IND_COND_PROP").toString());
		auto.infracao.setIndSDD(hashAuto.get("IND_SUSPENSAO").toString());
		auto.infracao.setValRealDesconto(hashAuto.get("VAL_REAL_DESCONTO").toString());

		auto.veiculo.setCodMarcaModelo(hashAuto.get("COD_MARCA_MODELO").toString());
		auto.veiculo.setCodEspecie(hashAuto.get("COD_ESPECIE").toString());
		auto.veiculo.setCodCategoria(hashAuto.get("COD_CATEGORIA").toString());
		auto.veiculo.setCodTipo(hashAuto.get("COD_TIPO").toString());
		auto.veiculo.setCodCor(hashAuto.get("COD_COR").toString());

		auto.proprietario.setNomResponsavel(hashAuto.get("NOM_PROPRIETARIO").toString());
		auto.proprietario.setIndCpfCnpj(hashAuto.get("TIP_DOC_PROPRIETARIO").toString());
		auto.proprietario.setNumCpfCnpj(hashAuto.get("NUM_DOC_PROPRIETARIO").toString());
		auto.proprietario.setIndTipoCnh(hashAuto.get("TIP_CNH_PROPRIETARIO").toString());
		auto.proprietario.setNumCnh(hashAuto.get("NUM_CNH_PROPRIETARIO").toString());

		EnderecoBean enderecoProprietario = new EnderecoBean();
		enderecoProprietario.setTxtEndereco(hashAuto.get("DSC_END_PROPRIETARIO").toString());
		enderecoProprietario.setNumEndereco(hashAuto.get("NUM_END_PROPRIETARIO").toString());
		enderecoProprietario.setTxtComplemento(hashAuto.get("DSC_COMP_PROPRIETARIO").toString());
		enderecoProprietario.setCodCidade(hashAuto.get("COD_MUN_PROPRIETARIO").toString());
		enderecoProprietario.setCodUF(hashAuto.get("UF_PROPRIETARIO").toString());
		enderecoProprietario.setNumCEP(hashAuto.get("NUM_CEP_PROPRIETARIO").toString());

		auto.proprietario.setEndereco(enderecoProprietario);

		auto.condutor.setNomResponsavel(hashAuto.get("NOM_CONDUTOR").toString());
		auto.condutor.setIndCpfCnpj(hashAuto.get("TIP_DOC_CONDUTOR").toString());
		auto.condutor.setNumCpfCnpj(hashAuto.get("NUM_DOC_CONDUTOR").toString());
		auto.condutor.setIndTipoCnh(hashAuto.get("TIP_CNH_CONDUTOR").toString());
		auto.condutor.setNumCnh(hashAuto.get("NUM_CNH_CONDUTOR").toString());

		EnderecoBean enderecoCondutor = new EnderecoBean();
		enderecoCondutor.setTxtEndereco(hashAuto.get("DSC_END_CONDUTOR").toString());
		enderecoCondutor.setNumEndereco(hashAuto.get("NUM_END_CONDUTOR").toString());
		enderecoCondutor.setTxtComplemento(hashAuto.get("DSC_COMP_CONDUTOR").toString());
		enderecoCondutor.setCodCidade(hashAuto.get("COD_MUN_CONDUTOR").toString());
		enderecoCondutor.setCodUF(hashAuto.get("UF_CONDUTOR").toString());
		enderecoCondutor.setNumCEP(hashAuto.get("NUM_CEP_CONDUTOR").toString());

		auto.condutor.setEndereco(enderecoCondutor);

		auto.numNotifAut = hashAuto.get("NUM_NOTIF_AUTUACAO").toString();
		auto.datNotifAut = hashAuto.get("DAT_NOTIF_AUTUACAO").toString();
		auto.codNotifAut = hashAuto.get("COD_ENT_AUTUACAO").toString();
		auto.numARNotifAut = hashAuto.get("NUM_LOTE_AUTUACAO").toString();
		auto.numNotifPen = hashAuto.get("NUM_NOTIF_PENALIDADE").toString();
		auto.datNotifPen = hashAuto.get("DAT_NOTIF_PENALIDADE").toString();
		auto.codNotifPen = hashAuto.get("COD_ENT_PENALIDADE").toString();
		auto.numARNotifPen = hashAuto.get("NUM_LOTE_PENALIDADE").toString();
		auto.dscAgente = hashAuto.get("DSC_AGENTE").toString();
		auto.dscUnidade = hashAuto.get("DSC_UNIDADE").toString();
		auto.datPag = hashAuto.get("DAT_PAGAMENTO").toString();
		auto.numAutoOrigem = hashAuto.get("NUM_AUTO_ORIGEM").toString();
		auto.numInfracaoOrigem = hashAuto.get("COD_INFRACAO_ORIGEM").toString();
		auto.identAutoOrigem = hashAuto.get("IND_TIPO_AUTO").toString();
		auto.numInfracaoRenainf = hashAuto.get("NUM_AUTO_RENAINF").toString();
		auto.txtEmail = hashAuto.get("TXT_EMAIL").toString();
		auto.datInclusao = hashAuto.get("DAT_INCLUSAO").toString();
		auto.datAlteracao = hashAuto.get("DAT_ALTERACAO").toString();
		// hashAuto.get("SIT_AI_DIGITALIZADO").toString();
		// hashAuto.get("SIT_AR_DIGITALIZADO").toString();
		auto.datAlteracao = hashAuto.get("DAT_DIGITALIZACAO").toString();
		auto.datRegistro = hashAuto.get("DAT_REGISTRO").toString();
		auto.sit_digitaliza = hashAuto.get("SIT_DIGITALIZACAO").toString();
		auto.valVelocPermitida = hashAuto.get("VAL_VELOC_PERMITIDA").toString();
		auto.numRenavam = hashAuto.get("NUM_RENAVAM").toString();
		auto.identOrgao = hashAuto.get("IDENT_ORGAO").toString();
		auto.codAcessoWeb = hashAuto.get("COD_ACESSO_WEB").toString();
		auto.obsCondutor = hashAuto.get("OBS_CONDUTOR").toString();
		auto.indPontuacao = hashAuto.get("IND_PONTUACAO").toString();
		auto.indParcelamento = hashAuto.get("IND_PARCELAMENTO").toString();
		auto.codAta = hashAuto.get("COD_ATA").toString();
		auto.dsAta = hashAuto.get("DS_ATA").toString();
		auto.cdNatureza = Integer.parseInt(hashAuto.get("cd_natureza").toString());

		return auto;
	}

	public String getCodAta() {
		return codAta;
	}

	public void setCodAta(String codAta) {
		this.codAta = codAta;
	}

	public String getDsAta() {
		return dsAta;
	}

	public int getCdNatureza() {
		return this.cdNatureza;
	}

	public void setCdNatureza(int cdNatureza) {
		this.cdNatureza = cdNatureza;
	}

	public void setDsAta(String dsAta) {
		this.dsAta = dsAta;
	}

	public void setDtGerCBNotificacaoAut(String sDtGerCBNotificacaoAut) {
		this.sDtGerCBNotificacaoAut = sDtGerCBNotificacaoAut;
		if (sDtGerCBNotificacaoAut == null)
			this.sDtGerCBNotificacaoAut = "";
	}

	public String getDtGerCBNotificacaoAut() {
		return this.sDtGerCBNotificacaoAut;
	}

	public void setSeqGeracaoCBNotificacaoAut(int iSeqGeracaoCBNotificacaoAut) {
		this.iSeqGeracaoCBNotificacaoAut = iSeqGeracaoCBNotificacaoAut;
	}

	public int getSeqGeracaoCBNotificacaoAut() {
		return this.iSeqGeracaoCBNotificacaoAut;
	}

	/*------------------------------------------------------------------*/
	public void setIndParcelamento(String indParcelamento) {
		this.indParcelamento = indParcelamento;
	}

	public String getIndParcelamento() {
		if ("1".equals(indPago)) {
			if (this.indParcelamento.equals("S"))
				this.indParcelamento = "Parcelado";
			if (this.indParcelamento.equals("R"))
				this.indParcelamento = "Parcelamento rompido (res�duo a pagar)";
			if (this.indParcelamento.equals("T"))
				this.indParcelamento = "Parcelamento rompido AI reativado";
		} else
			this.indParcelamento = "";
		return this.indParcelamento;
	}
	/*------------------------------------------------------------------*/

	public void setDtGerCBNotificacaoPen(String sDtGerCBNotificacaoPen) {
		this.sDtGerCBNotificacaoPen = sDtGerCBNotificacaoPen;
		if (sDtGerCBNotificacaoPen == null)
			this.sDtGerCBNotificacaoPen = "";
	}

	public String getDtGerCBNotificacaoPen() {
		return this.sDtGerCBNotificacaoPen;
	}

	public void setSeqGeracaoCBNotificacaoPen(int iSeqGeracaoCBNotificacaoPen) {
		this.iSeqGeracaoCBNotificacaoPen = iSeqGeracaoCBNotificacaoPen;
	}

	public int getSeqGeracaoCBNotificacaoPen() {
		return this.iSeqGeracaoCBNotificacaoPen;
	}

	public void setDtGerCBProcesso(String sDtGerCBProcesso) {
		this.sDtGerCBProcesso = sDtGerCBProcesso;
		if (sDtGerCBProcesso == null)
			this.sDtGerCBProcesso = "";
	}

	public String getDtGerCBProcesso() {
		return this.sDtGerCBProcesso;
	}

	public void setMsgErro(Vector<?> vErro) {
		for (int j = 0; j < vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j);
		}
	}

	public void setMsgErro(String msgErro) {
		if (msgErro == null)
			msgErro = "";
		this.msgErro = msgErro;
	}

	public String getMsgErro() {
		return this.msgErro;
	}

	public void setMsgOk(String msgOk) {
		if (msgOk == null)
			msgOk = "";
		this.msgOk = msgOk;
	}

	public String getMsgOk() {
		return this.msgOk;
	}

	public void setCodAutoInfracao(String codAutoInfracao) {
		this.codAutoInfracao = codAutoInfracao;
		if (codAutoInfracao == null)
			this.codAutoInfracao = "";
	}

	public String getCodAutoInfracao() {
		return this.codAutoInfracao;
	}

	public void setNumAutoInfracao(String numAutoInfracao) {
		this.numAutoInfracao = numAutoInfracao;
		if (numAutoInfracao == null)
			this.numAutoInfracao = "";
	}

	public String getNumAutoInfracao() {
		return this.numAutoInfracao;
	}

	public void setNumAutoInfracaoEdt(String numAutoInfracao) {
		if (numAutoInfracao == null)
			numAutoInfracao = "";
		if (numAutoInfracao.length() < 15)
			numAutoInfracao += "               ";
		this.numAutoInfracao = numAutoInfracao.substring(0, 1) + numAutoInfracao.substring(4, 15);
	}

	public String getNumAutoInfracaoEdt() {
		if ("".equals(this.numAutoInfracao))
			return this.numAutoInfracao;
		this.numAutoInfracao = Util.rPad(this.numAutoInfracao, " ", 12);
		return this.numAutoInfracao.substring(0, 1) + " - " + this.numAutoInfracao.substring(1, 12);
	}

	public void setNumNotificacao(String numNotificacao) {
		this.numNotificacao = numNotificacao;
		if (numNotificacao == null)
			this.numNotificacao = "";
	}

	public String getNumNotificacao() {
		return this.numNotificacao;
	}

	public void setDatNotificacao(String datNotificacao) {
		if (datNotificacao == null)
			datNotificacao = "";
		this.datNotificacao = datNotificacao;
	}

	public String getDatNotificacao() {
		return this.datNotificacao;
	}

	public void setDatExpAut(String datExpAut) {
		if (datExpAut == null)
			datExpAut = "";
		this.datExpAut = datExpAut;
	}

	public String getDatExpAut() {
		return this.datExpAut;
	}

	public void setDatExpPen(String datExpPen) {
		if (datExpPen == null)
			datExpPen = "";
		this.datExpPen = datExpPen;
	}

	public String getDatExpPen() {
		return this.datExpPen;
	}

	// --------------------------------------------------------------------------
	public void setNomMotivoNotificacao(String nomMotivoNotificacao) {
		if (nomMotivoNotificacao == null)
			nomMotivoNotificacao = "";
		this.nomMotivoNotificacao = nomMotivoNotificacao;
	}

	public String getNomMotivoNotificacao() {
		return this.nomMotivoNotificacao;
	}

	// --------------------------------------------------------------------------
	public void setDatInfracao(String datInfracao) {
		this.datInfracao = datInfracao;
		if (datInfracao == null)
			this.datInfracao = "";
		if (datInfracao.equals("00000000"))
			this.datInfracao = "";
	}

	public String getDatInfracao() {
		return this.datInfracao;
	}

	public void setValHorInfracao(String valHorInfracao) {
		this.valHorInfracao = valHorInfracao;
		if (valHorInfracao == null)
			this.valHorInfracao = "";
	}

	public String getValHorInfracao() {
		return this.valHorInfracao;
	}

	public String getValHorInfracaoEdt() {
		if ("".equals(this.valHorInfracao))
			return this.valHorInfracao;
		String h = "    " + this.valHorInfracao;
		return h.substring(h.length() - 4, h.length() - 2) + ":" + h.substring(h.length() - 2, h.length());
	}

	public void setDscLocalInfracao(String dscLocalInfracao) {
		this.dscLocalInfracao = dscLocalInfracao;
		if (dscLocalInfracao == null)
			this.dscLocalInfracao = "";
	}

	public String getDscLocalInfracao() {
		return this.dscLocalInfracao;
	}

	public void setNumPlaca(String numPlaca) {
		this.numPlaca = numPlaca;
		if (numPlaca == null)
			this.numPlaca = "";
	}

	public String getNumPlaca() {
		return this.numPlaca;
	}

	public void setNumPlacaEdt(String numPlaca) {
		if (numPlaca == null)
			numPlaca = "";
		if (numPlaca.length() < 8)
			numPlaca += "        ";
		this.numPlaca = numPlaca.substring(0, 3) + numPlaca.substring(4, 8);
	}

	public String getNumPlacaEdt() {
		if ("".equals(this.numPlaca))
			return this.numPlaca;
		return this.numPlaca.substring(0, 3) + " " + this.numPlaca.substring(3, 7);
	}

	public void setNumRenavam(String numRenavam) {
		this.numRenavam = numRenavam;
		if (numRenavam == null)
			this.numRenavam = "";
	}

	public String getNumRenavam() {
		return this.numRenavam;
	}

	public void setNumProcesso(String numProcesso) {
		this.numProcesso = numProcesso;
		if (numProcesso == null)
			this.numProcesso = "";
	}

	public String getNumProcesso() {
		return this.numProcesso;
	}

	public void setDatProcesso(String datProcesso) {
		this.datProcesso = datProcesso;
		if (datProcesso == null)
			this.datProcesso = "";
	}

	public String getDatProcesso() {
		return this.datProcesso;
	}

	public void setInfracao(InfracaoBean infracao) throws sys.BeanException {
		this.infracao = infracao;
		if (infracao == null)
			this.infracao = new InfracaoBean();
	}

	public InfracaoBean getInfracao() {
		return this.infracao;
	}

	public void setCodAgente(String codAgente) {
		this.codAgente = codAgente;
		if (codAgente == null)
			this.codAgente = "";
	}

	public String getCodAgente() {
		return this.codAgente;
	}

	public void setNomAgente(String nomAgente) {
		this.nomAgente = nomAgente;
		if (nomAgente == null)
			this.nomAgente = "";
	}

	public String getNomAgente() {
		return this.nomAgente;
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}

	public String getCodOrgao() {
		return this.acertaVisualizacaoCodOrgaoPMERJ(this.codOrgao);
	}

	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}

	public String getSigOrgao() {
		return this.acertaVisualizacaoSiglaOrgaoPMERJ(this.sigOrgao);
	}

	public void setNomOrgao(String nomOrgao) {
		this.nomOrgao = nomOrgao;
		if (nomOrgao == null)
			this.nomOrgao = "";
	}

	public String getNomOrgao() {
		return this.nomOrgao;
	}

	public void setOrgao(ACSS.OrgaoBean orgao) {
		this.orgao = orgao;
	}

	public ACSS.OrgaoBean getOrgao() {
		return this.orgao;
	}

	public void setDscLocalAparelho(String dscLocalAparelho) {
		this.dscLocalAparelho = dscLocalAparelho;
		if (dscLocalAparelho == null)
			this.dscLocalAparelho = "";
	}

	public String getDscLocalAparelho() {
		return this.dscLocalAparelho;
	}

	public void setNumCertAferAparelho(String numCertAferAparelho) {
		this.numCertAferAparelho = numCertAferAparelho;
		if (numCertAferAparelho == null)
			this.numCertAferAparelho = "";
	}

	public String getNumCertAferAparelho() {
		return this.numCertAferAparelho;
	}

	public void setDatUltAferAparelho(String datUltAferAparelho) {
		this.datUltAferAparelho = datUltAferAparelho;
		if (datUltAferAparelho == null)
			this.datUltAferAparelho = "";
		if (datUltAferAparelho.equals("00000000"))
			this.datUltAferAparelho = "";
	}

	public String getDatUltAferAparelho() {
		return this.datUltAferAparelho;
	}

	public void setNumIdentAparelho(String numIdentAparelho) {
		this.numIdentAparelho = numIdentAparelho;
		if (numIdentAparelho == null)
			this.numIdentAparelho = "";
	}

	public String getNumIdentAparelho() {
		return this.numIdentAparelho;
	}

	public void setNumInmetroAparelho(String numInmetroAparelho) {
		this.numInmetroAparelho = numInmetroAparelho;
		if (numInmetroAparelho == null)
			this.numInmetroAparelho = "";
	}

	public String getNumInmetroAparelho() {
		return this.numInmetroAparelho;
	}

	public void setCodTipDispRegistrador(String codTipDispRegistrador) {
		this.codTipDispRegistrador = codTipDispRegistrador;
		if (codTipDispRegistrador == null)
			this.codTipDispRegistrador = "";
	}

	public String getCodTipDispRegistrador() {
		return this.codTipDispRegistrador;
	}

	public void setDatVencimento(String datVencimento) {
		if (datVencimento == null)
			datVencimento = "";
		this.datVencimento = datVencimento;
	}

	public String getDatVencimento() {
		return this.datVencimento;
	}

	public void setCodMunicipio(String codMunicipio) {
		this.codMunicipio = codMunicipio;
		if (codMunicipio == null)
			this.codMunicipio = "";
	}

	public String getCodMunicipio() {
		return this.codMunicipio;
	}

	public void setNomMunicipio(String nomMunicipio) {
		this.nomMunicipio = nomMunicipio;
		if (nomMunicipio == null)
			this.nomMunicipio = "";
	}

	public String getNomMunicipio() {
		return this.nomMunicipio;
	}

	public void setValVelocPermitida(String valVelocPermitida) {
		this.valVelocPermitida = valVelocPermitida;
		if (valVelocPermitida == null)
			this.valVelocPermitida = "";
	}

	public String getValVelocPermitida() {
		return this.valVelocPermitida;
	}

	public void setValVelocAferida(String valVelocAferida) {
		this.valVelocAferida = valVelocAferida;
		if (valVelocAferida == null)
			this.valVelocAferida = "";
	}

	public String getValVelocAferida() {
		return this.valVelocAferida;
	}

	public void setValVelocConsiderada(String valVelocConsiderada) {
		this.valVelocConsiderada = valVelocConsiderada;
		if (valVelocConsiderada == null)
			this.valVelocConsiderada = "";
	}

	public String getValVelocConsiderada() {
		return this.valVelocConsiderada;
	}

	public void setDatAlteracao(String datAlteracao) {
		this.datAlteracao = datAlteracao;
		if (datAlteracao == null)
			this.datAlteracao = "";
	}

	public String getDatAlteracao() {
		return this.datAlteracao;
	}

	public void setDatInclusao(String datInclusao) {
		this.datInclusao = datInclusao;
		if (datInclusao == null)
			this.datInclusao = "";
	}

	public String getDatInclusao() {
		return this.datInclusao;
	}

	public void setDatRegistro(String datRegistro) {
		this.datRegistro = datRegistro;
		if (datRegistro == null)
			this.datRegistro = "";
	}

	public String getDatRegistro() {
		return this.datRegistro;
	}

	public void setDscSituacao(String dscSituacao) {
		this.dscSituacao = dscSituacao;
		if (dscSituacao == null)
			this.dscSituacao = "";
	}

	public String getDscSituacao() {
		return this.dscSituacao;
	}

	public void setVeiculo(VeiculoBean veiculo) throws sys.BeanException {
		this.veiculo = veiculo;
		if (veiculo == null)
			this.veiculo = new VeiculoBean();
	}

	public VeiculoBean getVeiculo() {
		return this.veiculo;
	}

	public void setIndCondutorIdentificado(String indCondutorIdentificado) {
		this.indCondutorIdentificado = indCondutorIdentificado;
		if (indCondutorIdentificado == null)
			this.indCondutorIdentificado = "";
	}

	public String getIndCondutorIdentificado() {
		return this.indCondutorIdentificado;
	}

	public void setCondutor(ResponsavelBean condutor) throws sys.BeanException {
		this.condutor = condutor;
		if (condutor == null)
			this.condutor = new ResponsavelBean();
	}

	public ResponsavelBean getCondutor() {
		return this.condutor;
	}

	public void setProprietario(ResponsavelBean proprietario) throws sys.BeanException {
		this.proprietario = proprietario;
		if (proprietario == null)
			this.proprietario = new ResponsavelBean();
	}

	public ResponsavelBean getProprietario() {
		return this.proprietario;
	}

	public void setCodLote(String codLote) {
		this.codLote = codLote;
		if (codLote == null)
			this.codLote = "";
	}

	public String getCodLote() {
		return this.codLote;
	}

	public void setCodArquivo(String codArquivo) {
		this.codArquivo = codArquivo;
		if (codArquivo == null)
			this.codArquivo = "";
	}

	public String getCodArquivo() {
		return this.codArquivo;
	}

	public void setNomUserName(String nomUserName) {
		this.nomUserName = nomUserName;
		if (nomUserName == null)
			this.nomUserName = "";
	}

	public String getNomUserName() {
		return this.nomUserName;
	}

	public void setIndTPCV(String indTPCV) {
		this.indTPCV = indTPCV;
		if (indTPCV == null)
			this.indTPCV = "0";
	}

	public String getIndTPCV() {
		return this.indTPCV;
	}

	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}

	public String getCodStatus() {
		return this.codStatus;
	}

	public void setDatStatus(String datStatus) {
		this.datStatus = datStatus;
		if (datStatus == null)
			this.datStatus = "";
		if (datStatus.equals("00000000"))
			this.datStatus = "";
	}

	public String getDatStatus() {
		return this.datStatus;
	}

	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}

	public String getNomStatus() {
		return nomStatus;
	}

	public void setDatLimiteRecurso(String datLimiteRecurso) {
		this.datLimiteRecurso = datLimiteRecurso;
		if (datLimiteRecurso == null)
			this.datLimiteRecurso = "";
		this.datLimiteRecursoFolga = this.datLimiteRecurso;
	}

	public void setSit_digitaliza(String sit_digitaliza) {
		this.sit_digitaliza = sit_digitaliza;
		if (sit_digitaliza == null)
			this.sit_digitaliza = "";
	}

	public String getSit_digitaliza() {
		return sit_digitaliza;
	}

	private String getDtn() throws sys.BeanException {
		String sDtn = this.getDatNotificacao();

		if (sDtn == null)
			sDtn = sys.Util.formatedToday().substring(0, 10);
		if (sDtn.length() == 0)
			sDtn = sys.Util.formatedToday().substring(0, 10);
		if ((this.getCodStatus().equals("30")) || (this.getCodStatus().equals("35"))) {
			sDtn = this.getDatStatus();
			HistoricoBean his = new HistoricoBean();
			Iterator itx = this.getRequerimentos().iterator();
			while (itx.hasNext()) {
				his = (HistoricoBean) itx.next();
				if (his.getCodStatus().equals("30")) {
					sDtn = his.getDatStatus();
					break;
				}
			}
		}
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			int iDias = daoTabela.VerificaFeriado(this, sDtn);
			while (iDias > 0) {
				sDtn = sys.Util.addData(sDtn, iDias);
				iDias = daoTabela.VerificaFeriado(this, sDtn);
			}
		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return sDtn;
	}

	public String verificaFeriado(String data) throws BeanException {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			int iDias = daoTabela.VerificaFeriado(this, data);
			while (iDias > 0) {
				data = sys.Util.addData(data, iDias);
				iDias = daoTabela.VerificaFeriado(this, data);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return data;
	}

	public void setDatLimiteRecurso(ParamOrgBean ParamOrgaoId) throws sys.BeanException {
		String dtn = this.getDtn();
		if (dtn.length() == 0)
			dtn = sys.Util.formatedToday().substring(0, 10);
		this.datLimiteRecurso = dtn;
		this.datLimiteRecursoFolga = dtn;

		if (ParamOrgaoId != null) {
			int nd = 0;
			int nf = 0;
			try {
				if (this.getCodStatus().length() == 1) {
					nd = Integer.parseInt(ParamOrgaoId.getParamOrgao("DIAS_DEF_PREVIA", "15", "1"));
					// nf =
					// Integer.parseInt(ParamOrgaoId.getParamOrgao("DIAS_FOLGA_DEFPREVIA","10","1"))
					// ;
					// FOLGA NAO CONSIDERA MAIS PARA ACEITACAO DO RECURSO
					// FOLGA SERA CONSIDERADA APENAS PARA VIRAR PENALIDADE NO
					// FECHAMENTO DIARIO
					nf = 0;
				}
				if (this.getCodStatus().length() == 2) {

					nd = Integer.parseInt(ParamOrgaoId.getParamOrgao("DIAS_DEF_PENALIDADE", "30", "3"));
					// nf =
					// Integer.parseInt(ParamOrgaoId.getParamOrgao("DIAS_FOLGA_DEFPENALIDADE","10","3"))
					// ;
					nf = 0;
				}
				this.datLimiteRecurso = sys.Util.addData(dtn, nd);
				this.datLimiteRecursoFolga = sys.Util.addData(dtn, (nd + nf));
				// verificar Sabado Domingo e Feriados
				this.datLimiteRecurso = verificaFeriado(datLimiteRecurso);
				this.datLimiteRecursoFolga = verificaFeriado(datLimiteRecursoFolga);
			} catch (Exception e) {
			}
		}
	}

	public String getDatLimiteRecurso() {
		return datLimiteRecurso;
	}

	public String getDatLimiteRecursoFolga() {
		return datLimiteRecursoFolga;
	}

	public void setDatEnvioDO(String datEnvioDO) {
		this.datEnvioDO = datEnvioDO;
		if (datEnvioDO == null)
			this.datEnvioDO = "";
	}

	public String getDatEnvioDO() {
		return datEnvioDO;
	}

	public void setCodEnvioDO(String codEnvioDO) {
		this.codEnvioDO = codEnvioDO;
		if (codEnvioDO == null)
			this.codEnvioDO = "";
	}

	public String getCodEnvioDO() {
		return codEnvioDO;
	}

	public void setCodResultPubDO(String codResultPubDO) {
		this.codResultPubDO = codResultPubDO;
		if (codResultPubDO == null)
			this.codResultPubDO = "";
	}

	public String getCodResultPubDO() {
		return this.codResultPubDO;
	}

	public String getNomResultPubDO() {
		String n = "";
		if ("D".equals(this.codResultPubDO))
			n = "Deferido";
		if ("I".equals(this.codResultPubDO))
			n = "Indeferido";
		return n;
	}

	public void setNomUsernameDO(String nomUsernameDO) {
		this.nomUsernameDO = nomUsernameDO;
		if (nomUsernameDO == null)
			this.nomUsernameDO = "";
	}

	public String getNomUsernameDO() {
		return nomUsernameDO;
	}

	public void setCodOrgaoLotacaoDO(String codOrgaoLotacaoDO) {
		this.codOrgaoLotacaoDO = codOrgaoLotacaoDO;
		if (codOrgaoLotacaoDO == null)
			this.codOrgaoLotacaoDO = "";
	}

	public String getCodOrgaoLotacaoDO() {
		return codOrgaoLotacaoDO;
	}

	public void setDatProcDO(String datProcDO) {
		this.datProcDO = datProcDO;
		if (datProcDO == null)
			this.datProcDO = "";
	}

	public String getDatProcDO() {
		return datProcDO;
	}

	public void setDatPublPDO(String datPublPDO) {
		if (datPublPDO == null)
			datPublPDO = "";
		this.datPublPDO = datPublPDO;
	}

	public String getDatPublPDO() {
		return datPublPDO;
	}

	public void setNomUsernamePDO(String nomUsernamePDO) {
		this.nomUsernamePDO = nomUsernamePDO;
		if (nomUsernamePDO == null)
			this.nomUsernameDO = "";
	}

	public String getNomUsernamePDO() {
		return nomUsernamePDO;
	}

	public void setCodOrgaoLotacaoPDO(String codOrgaoLotacaoPDO) {
		this.codOrgaoLotacaoPDO = codOrgaoLotacaoPDO;
		if (codOrgaoLotacaoPDO == null)
			this.codOrgaoLotacaoPDO = "";
	}

	public String getCodOrgaoLotacaoPDO() {
		return codOrgaoLotacaoPDO;
	}

	public void setDatProcPDO(String datProcPDO) {
		this.datProcPDO = datProcPDO;
		if (datProcPDO == null)
			this.datProcPDO = "";
		if (datProcPDO.equals("00000000"))
			this.datProcPDO = "";
	}

	public String getDatProcPDO() {
		return datProcPDO;
	}

	public void setCodBarra(String codBarra) {
		this.codBarra = codBarra;
		if (codBarra == null)
			this.codBarra = "";
	}

	public String getCodBarra() {
		return this.codBarra;
	}

	// --------------------------------------------------------------------------
	public void setIndFase(String indFase) {
		if ((indFase == null) || (indFase.length() == 0))
			indFase = "";
		if ("123".indexOf(indFase) < 0)
			indFase = "";
		this.indFase = indFase;
	}

	public String getIndFase() {
		return this.indFase;
	}

	// --------------------------------------------------------------------------
	public void setIndSituacao(String indSituacao) {
		// if ((indSituacao == null) || (indSituacao.length() == 0))
		// indSituacao = "";
		// if ("12345678AB".indexOf(indSituacao) < 0)
		// indSituacao = "";
		this.indSituacao = indSituacao;
	}

	public String getIndSituacao() {
		return this.indSituacao;
	}

	public String getSituacao() {
		String sit = "";
		if ("3".equals(indFase))
			sit = "1a. INST�NCIA";
		else {
			if ("2".equals(indFase))
				sit = "PENALIDADE";
			else {
				if ("1".equals(indFase))
					sit = "AUTUA��O";
			}
		}
		/*
		 * else if ("5".equals(indSituacao)) sit+=
		 * " Suspenso-(Agravo Inst. 22.253/2003)";
		 */

		if ("2".equals(indSituacao))
			sit += " (EM RECURSO)";
		else if ("3".equals(indSituacao))
			sit += " (SUSPENSA)";
		else if ("4".equals(indSituacao))
			sit += " (CANCELADA)";
		else if ("5".equals(indSituacao))
			sit += " Cancelado Decis�o Judicial";
		else if ("6".equals(indSituacao))
			sit += " Suspenso-Proc.2004.024.002355-6(Itagua�)";
		else if ("7".equals(indSituacao))
			sit += " Pontua��o Suspensa";
		else if ("8".equals(indSituacao))
			sit += " Desvincula��o/Leil�o";
		else if ("A".equals(indSituacao))
			sit += " (Desvincula��o/Pena de perdimento)";
		else if ("B".equals(indSituacao))
			sit += " (Desvincula��o/Decis�o Judicial)";

		if ("2".equals(indPago))
			sit += " - PAGA";
		else if ("1".equals(indPago))
			sit += " - N�O PAGA";
		if (datPag.length() > 0)
			sit += " Pagto:" + datPag;
		return sit;
	}

	// --------------------------------------------------------------------------
	public void setIndPago(String indPago) {
		if ((indPago == null) || (indPago.length() == 0))
			indPago = "";
		if ("12".indexOf(indPago) < 0)
			indPago = "";
		this.indPago = indPago;
	}

	public String getIndPago() {
		return this.indPago;
	}

	// --------------------------------------------------------------------------
	public void setDatPag(String datPag) {
		if (datPag == null)
			datPag = "";
		this.datPag = datPag;
	}

	public String getDatPag() {
		return this.datPag;
	}

	// --------------------------------------------------------------------------
	public void setAutoAntigo(String autoAntigo) {
		if (autoAntigo == null)
			autoAntigo = "0";
		this.autoAntigo = autoAntigo;
	}

	public String getAutoAntigo() {
		return this.autoAntigo;
	}

	// --------------------------------------------------------------------------
	public void setDatNotifAut(String datNotifAut) {
		if (datNotifAut == null)
			datNotifAut = "";
		this.datNotifAut = datNotifAut;
	}

	public String getDatNotifAut() {
		return this.datNotifAut;
	}

	// --------------------------------------------------------------------------
	public void setNomMotivoNotifAut(String nomMotivoNotifAut) {
		if (nomMotivoNotifAut == null)
			nomMotivoNotifAut = "";
		this.nomMotivoNotifAut = nomMotivoNotifAut;
	}

	public String getNomMotivoNotifAut() {
		return this.nomMotivoNotifAut;
	}

	// --------------------------------------------------------------------------
	public void setNumNotifAut(String numNotifAut) {
		if (numNotifAut == null)
			numNotifAut = "";
		this.numNotifAut = numNotifAut;
	}

	public String getNumNotifAut() {
		return this.numNotifAut;
	}

	// --------------------------------------------------------------------------
	public void setCodNotifAut(String codNotifAut) {
		if (codNotifAut == null)
			codNotifAut = "";
		this.codNotifAut = codNotifAut;
	}

	public String getCodNotifAut() {
		return this.codNotifAut;
	}

	// --------------------------------------------------------------------------
	public void setNumARNotifAut(String numARNotifAut) {
		if (numARNotifAut == null)
			numARNotifAut = "";
		this.numARNotifAut = numARNotifAut;
	}

	public String getNumARNotifAut() {
		return this.numARNotifAut;
	}

	// --------------------------------------------------------------------------
	public void setDatNotifPen(String datNotifPen) {
		if (datNotifPen == null)
			datNotifPen = "";
		this.datNotifPen = datNotifPen;
	}

	public String getDatNotifPen() {
		return this.datNotifPen;
	}

	// --------------------------------------------------------------------------
	public void setNomMotivoNotifPen(String nomMotivoNotifPen) {
		if (nomMotivoNotifPen == null)
			nomMotivoNotifPen = "";
		this.nomMotivoNotifPen = nomMotivoNotifPen;
	}

	public String getNomMotivoNotifPen() {
		return this.nomMotivoNotifPen;
	}

	// --------------------------------------------------------------------------
	public void setNumNotifPen(String numNotifPen) {
		if (numNotifPen == null)
			numNotifPen = "";
		this.numNotifPen = numNotifPen;
	}

	public String getNumNotifPen() {
		return this.numNotifPen;
	}

	// --------------------------------------------------------------------------
	public void setCodNotifPen(String codNotifPen) {
		if (codNotifPen == null)
			codNotifPen = "";
		this.codNotifPen = codNotifPen;
	}

	public String getCodNotifPen() {
		return this.codNotifPen;
	}

	// --------------------------------------------------------------------------
	public void setNumARNotifPen(String numARNotifPen) {
		if (numARNotifPen == null)
			numARNotifPen = "";
		this.numARNotifPen = numARNotifPen;
	}

	public String getNumARNotifPen() {
		return this.numARNotifPen;
	}

	// --------------------------------------------------------------------------
	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "";
		this.eventoOK = eventoOK;
	}

	public String getEventoOK() {
		return this.eventoOK;
	}

	// --------------------------------------------------------------------------
	public void setNumRequerimento(String numRequerimento) {
		if (numRequerimento == null)
			numRequerimento = "";
		this.numRequerimento = numRequerimento;
	}

	public String getNumRequerimento() {
		return this.numRequerimento;
	}

	// --------------------------------------------------------------------------
	public void setCodRequerimento(String codRequerimento) {
		if (codRequerimento == null)
			codRequerimento = "";
		this.codRequerimento = codRequerimento;
	}

	public String getCodRequerimento() {
		return this.codRequerimento;
	}

	// --------------------------------------------------------------------------
	public void setTpRequerimento(String tpRequerimento) {
		if (tpRequerimento == null)
			tpRequerimento = "";
		this.tpRequerimento = tpRequerimento;
	}

	public String getTpRequerimento() {
		return this.tpRequerimento;
	}

	// --------------------------------------------------------------------------
	public void setChaveSort(String chaveSort) {
		this.chaveSort = chaveSort;
	}

	public String getChaveSort() {
		return this.chaveSort;
	}

	// --------------------------------------------------------------------------
	public void setDscAgente(String dscAgente) {
		if (dscAgente == null)
			dscAgente = "";
		this.dscAgente = dscAgente;
	}

	public String getDscAgente() {
		return this.dscAgente;
	}

	// --------------------------------------------------------------------------
	public void setDscUnidade(String dscUnidade) {
		if (dscUnidade == null)
			dscUnidade = "";
		this.dscUnidade = dscUnidade;
	}

	public String getDscUnidade() {
		return this.dscUnidade;
	}

	// --------------------------------------------------------------------------
	public void setDscLote(String dscLote) {
		if (dscLote == null)
			dscLote = "";
		this.dscLote = dscLote;
	}

	public String getDscLote() {
		return this.dscLote;
	}

	public void setNumAutoOrigem(String numAutoOrigem) {
		this.numAutoOrigem = numAutoOrigem;
		if (numAutoOrigem == null)
			this.numAutoOrigem = "";
	}

	public String getNumAutoOrigem() {
		return this.numAutoOrigem;
	}

	// --------------------------------------------------------------------------
	public void setNumInfracaoOrigem(String numInfracaoOrigem) {
		this.numInfracaoOrigem = numInfracaoOrigem;
		if (numInfracaoOrigem == null)
			this.numInfracaoOrigem = "";
	}

	public String getNumInfracaoOrigem() {
		return this.numInfracaoOrigem;
	}

	public void setIdentAutoOrigem(String identAutoOrigem) {
		this.identAutoOrigem = identAutoOrigem;
		if (identAutoOrigem == null)
			this.identAutoOrigem = "";
	}

	public String getIdentAutoOrigem() {
		return this.identAutoOrigem;
	}

	public String getDescIdentAutoOrigem() {
		String sDesc = "";
		if (getIdentAutoOrigem().equals("1"))
			sDesc = "Ve�culo de outro estado autuado no RJ";
		else if (getIdentAutoOrigem().equals("2"))
			sDesc = "Ve�culo do RJ autuado em outro estado";
		else if (getIdentAutoOrigem().equals("3"))
			sDesc = "Ve�culo de outro estado autuado num outro estado diferente do primeiro";
		return sDesc;
	}

	public void setNumInfracaoRenainf(String numInfracaoRenainf) {
		this.numInfracaoRenainf = numInfracaoRenainf;
		if (numInfracaoRenainf == null)
			this.numInfracaoRenainf = "";
	}

	public String getNumInfracaoRenainf() {
		return this.numInfracaoRenainf;
	}

	// --------------------------------------------------------------------------
	public void setIdentOrgao(String identOrgao) {
		this.identOrgao = identOrgao;
		if (identOrgao == null)
			this.identOrgao = "";
	}

	public String getIdentOrgao() {
		return this.identOrgao;
	}

	public void setIdentOrgaoEdt(String identOrgaoEdt) {
		if ((identOrgaoEdt.length() != 12) && (identOrgaoEdt.length() != 11)) {
			this.identOrgao = "";
			return;
		}
		String identOrgao = "";
		if (identOrgaoEdt.substring(0, 3).equals("NIT"))
			identOrgao = "T" + identOrgaoEdt.substring(3, 4) + sys.Util.lPad(identOrgaoEdt.substring(5, 12), "0", 7);
		else if (identOrgaoEdt.substring(0, 2).equals("NE"))
			identOrgao = "E" + identOrgaoEdt.substring(2, 3) + sys.Util.lPad(identOrgaoEdt.substring(4, 11), "0", 7);
		this.identOrgao = identOrgao;
	}

	public String getIdentOrgaoEdt() {
		if (this.identOrgao.length() < 9)
			return "";
		String identOrgaoEdt = "";
		if (this.identOrgao.substring(0, 1).equals("T"))
			identOrgaoEdt = "NIT" + this.identOrgao.substring(1, 2)
					+ sys.Util.lPad(this.identOrgao.substring(2, 9), "0", 8);
		else if (this.identOrgao.substring(0, 1).equals("E"))
			identOrgaoEdt = "NE" + this.identOrgao.substring(1, 2) + "-"
					+ sys.Util.lPad(this.identOrgao.substring(2, 9), "0", 7);
		return identOrgaoEdt;
	}

	public void setIdentOrgaoFoto(String identOrgaoFoto) {
		if ((identOrgaoFoto.length() != 12) && (identOrgaoFoto.length() != 10)) {
			this.identOrgao = "";
			return;
		}
		String identOrgao = "";
		if (identOrgaoFoto.substring(0, 3).equals("NIT"))
			identOrgao = "T" + identOrgaoFoto.substring(3, 4) + sys.Util.lPad(identOrgaoFoto.substring(5, 12), "0", 7);
		else if (identOrgaoFoto.substring(0, 2).equals("NE"))
			identOrgao = "E" + identOrgaoFoto.substring(2, 3) + sys.Util.lPad(identOrgaoFoto.substring(3, 10), "0", 7);
		this.identOrgao = identOrgao;
	}

	// --------------------------------------------------------------------------
	public void setDscResumoInfracao(String dscResumoInfracao) {
		this.dscResumoInfracao = dscResumoInfracao;
		if (dscResumoInfracao == null)
			this.dscResumoInfracao = "";
	}

	public String getDscResumoInfracao() {
		return (this.dscResumoInfracao.length() > 0 ? this.dscResumoInfracao : this.getInfracao().getDscInfracao());
	}

	// --------------------------------------------------------------------------
	public void setDscMarcaModeloAparelho(String dscMarcaModeloAparelho) {
		this.dscMarcaModeloAparelho = dscMarcaModeloAparelho;
		if (dscMarcaModeloAparelho == null)
			this.dscMarcaModeloAparelho = "";
	}

	public String getDscMarcaModeloAparelho() {
		return this.dscMarcaModeloAparelho;
	}

	// --------------------------------------------------------------------------
	public void setDscLocalInfracaoNotificacao(String dscLocalInfracaoNotificacao) {
		this.dscLocalInfracaoNotificacao = dscLocalInfracaoNotificacao;
		if (dscLocalInfracaoNotificacao == null)
			this.dscLocalInfracaoNotificacao = "";
	}

	public String getDscLocalInfracaoNotificacao() {
		return (this.dscLocalInfracaoNotificacao.length() > 0 ? this.dscLocalInfracaoNotificacao
				: this.getDscLocalInfracao());

	}

	// --------------------------------------------------------------------------
	public void setCodVinculada(String codVinculada) {
		if (codVinculada == null)
			codVinculada = "";
		this.codVinculada = codVinculada;
		if (("1".equals(this.codVinculada)) || ("2".equals(this.codVinculada))) {
			String autoVin = "";
			if (this.numAutoInfracao.length() > 8) {
				int n = Integer.parseInt(this.numAutoInfracao.substring(1, 9));
				if ("1".equals(this.codVinculada))
					n--;
				else
					n++;
				autoVin = this.numAutoInfracao.substring(0, 1) + Integer.toString(n);
			}
			getInfracao().setDscEnquadramento(getInfracao().getDscEnquadramento() + " - Auto Vinculado: " + autoVin);
		}
	}

	public String getCodVinculada() {
		return this.codVinculada;
	}

	// --------------------------------------------------------------------------
	public void setTxtEmail(String txtEmail) {
		if (txtEmail == null)
			txtEmail = "";
		this.txtEmail = txtEmail;
	}

	public String getTxtEmail() {
		return this.txtEmail;
	}

	// --------------------------------------------------------------------------
	public void LeAutoInfracao(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {
			LeAutoInfracao("codigo", myUsrLog);
		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public void LeAutoInfracao(String tpchave, ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {

			DaoBroker dao = DaoBrokerFactory.getInstance();

			dao.LeAutoInfracao(this, tpchave, myUsrLog);

		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	// --------------------------------------------------------------------------
	public void setRequerimentos(List<RequerimentoBean> requerimentos) {
		this.requerimentos = requerimentos;
	}

	public List<RequerimentoBean> getRequerimentos() {
		return this.requerimentos;
	}

	public RequerimentoBean getRequerimentos(int seq) throws sys.BeanException {
		RequerimentoBean re = new RequerimentoBean();
		try {
			if ((this.requerimentos != null) && (this.requerimentos.size() > seq)) {
				re = (RequerimentoBean) this.requerimentos.get(seq);
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return re;
	}

	public RequerimentoBean getRequerimentos(String myCodRequerimento) throws sys.BeanException {
		try {
			if (myCodRequerimento == null)
				myCodRequerimento = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getCodRequerimento().equals(myCodRequerimento)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentos(String myNumRequerimento, String campo) throws sys.BeanException {
		try {
			if (myNumRequerimento == null)
				myNumRequerimento = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getNumRequerimento().equals(myNumRequerimento)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public RequerimentoBean getRequerimentoTipo(String myTipo) throws sys.BeanException {
		try {
			if (myTipo == null)
				myTipo = "";
			RequerimentoBean re = new RequerimentoBean();
			Iterator<RequerimentoBean> itx = getRequerimentos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				re = (RequerimentoBean) itx.next();
				if (re.getCodTipoSolic().equals(myTipo)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				re = new RequerimentoBean();
			return re;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void LeRequerimento(UsuarioBean myUsuario) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			if (dao.LeRequerimento(this, myUsuario) == false) {
			}
		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public void setHistoricos(List<HistoricoBean> historicos) {
		this.historicos = historicos;
	}

	public List<HistoricoBean> getHistoricos() {
		return this.historicos;
	}

	public HistoricoBean getHistoricos(String myCodHistoricoAuto) throws sys.BeanException {
		try {
			HistoricoBean his = new HistoricoBean();
			Iterator<HistoricoBean> itx = getHistoricos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				his = (HistoricoBean) itx.next();
				if (his.getCodHistoricoAuto().equals(myCodHistoricoAuto)) {
					achou = true;
					break;
				}
			}
			if (achou == false)
				his = new HistoricoBean();
			return his;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public void LeHistorico(UsuarioBean myUsuario) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			if (dao.LeHistorico(this, myUsuario) == false) {
			}
		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public String getCodNotificacao(ParamOrgBean myParam) throws sys.BeanException {
		String codNotificacao = "1";

		if ((this.getCodStatus().length() > 1) && (this.getCodStatus().length() < 3)
				&& ("98".equals(this.getCodStatus()) == false)) {
			codNotificacao = "3";
		}
		try {
			// Status 0,1,2,4,5 - NOTIFICACAO DE AUTUACAO
			if (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA", "0,1,2,4,5", "1").indexOf(getCodStatus()) >= 0
					&& this.getCodStatus().length() < 2) {
				// Proprietario � igual ao condutor
				if (this.getProprietario().getCodResponsavel().equals(this.getCondutor().getCodResponsavel())) {
					codNotificacao = "1";
				} else {
					if (this.getIndCondutorIdentificado().equals("1")) {
						/* codNotificacao="2" ; 2� Via sempre notifica��o */
						codNotificacao = "1";
					} else
						codNotificacao = "1";
				}

				// Verifica se � AUTUACAO Administrativa
				if (myParam.getParamOrgao("INSTR_NOTIF_ADM", "51,52", "7")
						.indexOf(this.getInfracao().getCodInfracao()) >= 0)
					codNotificacao = "10";
			}
			// Status 10,11,12,14,15,16 - NOTIFICACAO DE PENALIDADE
			if (myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE", "10,11,12,14,15,16,82", "3")
					.indexOf(getCodStatus()) >= 0 && this.getCodStatus().length() >= 2) {
				// Proprietario � igual ao condutor
				if (this.getProprietario().getCodResponsavel().equals(this.getCondutor().getCodResponsavel())) {
					codNotificacao = "3";
				} else {
					if (this.getIndCondutorIdentificado().equals("1")) {
						/* codNotificacao="4" ; 2� Via sempre notifica��o */
						codNotificacao = "3";
					} else
						codNotificacao = "3";
				}

				// Verifica se � Penalidade Administrativa
				if (myParam.getParamOrgao("INSTR_NOTIF_ADM", "51,52", "7")
						.indexOf(this.getInfracao().getCodInfracao()) >= 0)
					codNotificacao = "11";
			}
			// Status 30,35,36
			if (myParam.getParamOrgao("STATUS_ABRIR_2INSTANCIA", "30,35,36", "4").indexOf(getCodStatus()) >= 0
					&& this.getCodStatus().length() >= 2) {
				// Proprietario � igual ao condutor
				if (this.getProprietario().getCodResponsavel().equals(this.getCondutor().getCodResponsavel())) {
					codNotificacao = "3";
				} else {
					if (this.getIndCondutorIdentificado().equals("1")) {
						/* codNotificacao="4" ; 2� Via sempre notifica��o */
						codNotificacao = "3";

					} else
						codNotificacao = "3";
				}
				// Verifica se � Penalidade Administrativa
				if (myParam.getParamOrgao("INSTR_NOTIF_ADM", "51,52", "7")
						.indexOf(this.getInfracao().getCodInfracao()) >= 0)
					codNotificacao = "11";
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return codNotificacao;
	}

	// --------------------------------------------------------------------------
	public boolean getTemAIDigitalizado() throws sys.BeanException {
		return temAIDigitalizado;
	}

	public void setTemAIDigitalizado(boolean temAIDigitalizado) throws sys.BeanException {
		this.temAIDigitalizado = temAIDigitalizado;
		return;
	}

	// --------------------------------------------------------------------------
	public boolean getTemARDigitalizado() throws sys.BeanException {
		return temARDigitalizado;
	}

	public void setTemARDigitalizado(boolean temARDigitalizado) throws sys.BeanException {
		this.temARDigitalizado = temARDigitalizado;
		return;
	}

	// --------------------------------------------------------------------------
	public boolean getTemFotoDigitalizada() throws sys.BeanException {
		return temFotoDigitalizada;
	}

	public void setTemFotoDigitalizada(boolean temFotoDigitalizada) throws sys.BeanException {
		this.temFotoDigitalizada = temFotoDigitalizada;
		return;
	}

	// --------------------------------------------------------------------------
	public void LeAIDigitalizado() throws sys.BeanException {
		try {
			this.AIDigitalizado = AIDigitalizadoBean.consultaAI(new String[] { "NUM_AUTO_INFRACAO" },
					new String[] { "'" + getNumAutoInfracao() + "'" });

		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void setAIDigitalizado(AIDigitalizadoBean AIDigitalizado) {
		this.AIDigitalizado = AIDigitalizado;
	}

	public AIDigitalizadoBean getAIDigitalizado() {
		return this.AIDigitalizado;
	}

	// --------------------------------------------------------------------------
	public ARDigitalizadoBean[] getARDigitalizado() {
		return ARDigitalizado;
	}

	public ARDigitalizadoBean getARDigitalizado(int i) {
		return (ARDigitalizadoBean) this.ARDigitalizado[i];
	}

	public void setARDigitalizado(ARDigitalizadoBean[] beans) {
		ARDigitalizado = beans;
	}

	public void LeARDigitalizado() throws sys.BeanException {
		try {
			String[] campos = new String[0];
			String[] valores = new String[0];

			if (this.getNumNotificacao().equals("")) {
				campos = new String[] { "NUM_AUTO_INFRACAO" };
				valores = new String[] { "'" + getNumAutoInfracao() + "'" };

			} else if (this.getNumAutoInfracao().equals("")) {
				campos = new String[] { "NUM_NOTIFICACAO" };
				valores = new String[] { "'" + getNumNotificacao() + "'" };

			} else {
				campos = new String[] { "NUM_AUTO_INFRACAO", "NUM_NOTIFICACAO" };
				valores = new String[] { "'" + getNumAutoInfracao() + "'", "'" + getNumNotificacao() + "'" };
			}

			this.ARDigitalizado = ARDigitalizadoBean.consultaARs(campos, valores);
		} catch (Exception ex) {
			this.msgErro = "Erro: " + ex.getMessage();
			throw new sys.BeanException(ex.getMessage());
		}
		return;
	}

	// --------------------------------------------------------------------------
	public void LeFotoDigitalizada() throws sys.BeanException {
		try {
			this.FotoDigitalizada = FotoDigitalizadaBean.consultaFoto(new String[] { "NUM_AUTO_INFRACAO" },
					new String[] { "'" + getNumAutoInfracao() + "'" });

		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void setFotoDigitalizada(FotoDigitalizadaBean FotoDigitalizada) {
		this.FotoDigitalizada = FotoDigitalizada;
	}

	public FotoDigitalizadaBean getFotoDigitalizada() {
		return this.FotoDigitalizada;
	}

	// --------------------------------------------------------------------------
	public Date getDatDigitalizacao() {
		return datDigitalizacao;
	}

	public void setDatDigitalizacao(Date datDigitalizacao) {
		this.datDigitalizacao = datDigitalizacao;
	}

	public void setAutos(ArrayList<AutoInfracaoBean> autos) {
		this.autos = autos;
	}

	public ArrayList<AutoInfracaoBean> getAutos() {
		return this.autos;
	}

	/**
	 * M�todo criado para atualizar os campos relativos a DIGITALIZACAO de
	 * autos.
	 * 
	 * @author Jos� Fernando
	 * @since 30/03/2006
	 * @throws Exception
	 * @version 1.0
	 */
	public void atualizaDigitalizacao() throws DaoException {
		try {
			REC.Dao dao = REC.Dao.getInstance();
			dao.atualizaDigitalizacao(this);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}

	// --------------------------------------------------------------------------
	public List<HistoricoBean> classificaHist(List<HistoricoBean> myHist) throws sys.BeanException {
		List<HistoricoBean> result = myHist;
		boolean troca = false;
		String dt1 = "";
		String dt2 = "";
		try {
			int tam = myHist.size();
			HistoricoBean tmpi = new HistoricoBean();
			HistoricoBean tmpj = new HistoricoBean();
			String d1, d2;
			int j = 0;
			for (int i = 0; i < tam; i++) {
				troca = false;
				tmpi = (HistoricoBean) result.get(i);
				dt1 = tmpi.getDatStatus();
				if (dt1.length() < 10)
					dt1 = dt1 + "            ";
				d1 = dt1.substring(6, 10) + dt1.substring(3, 5) + dt1.substring(0, 2);
				for (j = i + 1; j < tam; j++) {
					tmpj = (HistoricoBean) result.get(j);
					troca = false;
					// "Data Status" ;
					dt2 = tmpj.getDatStatus();
					if (dt2.length() < 10)
						dt2 = dt2 + "            ";
					d2 = dt2.substring(6, 10) + dt2.substring(3, 5) + dt2.substring(0, 2);
					if (d1.compareTo(d2) > 0) {
						troca = true;
						break;
					}
				}
				if (troca) {
					result.set(i, tmpj);
					result.set(j, tmpi);
				}
			}
		} catch (Exception ue) {
			throw new sys.BeanException("AutoInfracaoBean-classificaHist: " + ue.getMessage());
		}
		return result;
	}

	public HistoricoBean getHist(RequerimentoBean myReq, String evento) throws sys.BeanException {
		try {
			HistoricoBean his = new HistoricoBean();
			Iterator<HistoricoBean> itx = getHistoricos().iterator();
			boolean achou = false;
			while (itx.hasNext()) {
				his = (HistoricoBean) itx.next();
				if (evento.indexOf(his.getCodEvento()) >= 0) {
					if (("214".equals(his.getCodEvento()))
							&& (his.getTxtComplemento11().equals(myReq.getNumRequerimento()))) {
						achou = true;
						break;
					}
					// 206,326,340
					else {
						if (his.getTxtComplemento01().equals(myReq.getNumRequerimento())) {
							achou = true;
							break;
						}
					}

				}
			}
			if (achou == false)
				his = new HistoricoBean();
			return his;
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	// --------------------------------------------------------------------------
	public void GravaAutoInfracaoLocal() throws sys.BeanException {
		try {
			REC.DaoBrokerFactory.getInstance().GravaAutoInfracaoLocal(this);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public void GravaAutoInfracaoLocal(Connection conn) throws sys.BeanException {
		try {
			REC.DaoBrokerFactory.getInstance().GravaAutoInfracaoLocal(this, conn);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}

	public boolean LeAutoInfracaoLocal(String tipo) throws sys.BeanException {
		boolean retorno = false;
		try {
			retorno = REC.DaoBrokerFactory.getInstance().LeAutoInfracaoLocal(this, tipo);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return retorno;
	}

	// --------------------------------------------------------------------------
	public boolean temDefesaPrevia(UsuarioBean myUsuario) throws sys.BeanException {
		boolean retorno = false;
		try {
			if (this.getNumProcesso().equals(""))
				return false;

			if (this.getRequerimentos().size() == 0)
				this.LeRequerimento(myUsuario);

			RequerimentoBean requerimento = null;
			if (this.getInfracao().getIndTipo().equals("P")) // Propriet�rio
				requerimento = this.getRequerimentoTipo("DP");
			else
				// Condutor
				requerimento = this.getRequerimentoTipo("DC");

			if (!requerimento.getNumRequerimento().equals(""))
				retorno = true;
			else
				retorno = false;

			/* Tem Somente TRI, n�o mostrar Processo */
			if ((this.getRequerimentos().size() == 1) && (requerimento.getCodTipoSolic().equals("TR")))
				return false;

		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return retorno;
	}

	/**
	 * Faz o filtro dos eventos de acordo com os eventos informados no parametro
	 * LISTAGEM_DE_EVENTOS.
	 * 
	 * @param myAuto
	 * @param ParamOrgaoId
	 * @throws sys.BeanException
	 */
	public void filtraHistorico(AutoInfracaoBean myAuto, String codEventoFiltro) throws sys.BeanException {

		ArrayList<HistoricoBean> historico = new ArrayList<HistoricoBean>();
		Iterator<HistoricoBean> it = myAuto.getHistoricos().iterator();
		HistoricoBean myHist = new HistoricoBean();
		while (it.hasNext()) {
			myHist = (HistoricoBean) it.next();
			if (codEventoFiltro.indexOf(myHist.getCodEvento()) >= 0)
				historico.add(myHist);
		}
		myAuto.setHistoricos(historico);
	}

	public String getParametro(String sDiretorio) {
		String sParametro = sDiretorio;
		String sData = "";
		if (sDiretorio.equals("DIR_CODIGO_BARRA_PROCESSO"))
			sData = getDtGerCBProcesso();
		else
			sData = ((Integer.parseInt(getCodStatus()) < 10) ? getDtGerCBNotificacaoAut() : getDtGerCBNotificacaoPen());
		if (sData.length() < 10)
			sData = "01/01/2007";
		/* Novo Path */
		sParametro += "_" + sData.substring(6, 10);
		return sParametro;
	}

	public String getArquivo(ACSS.ParamSistemaBean param, String sDiretorio) {
		String pathArquivo;
		try {
			if (sDiretorio.equals("DIR_CODIGO_BARRA_PROCESSO")) {
				REC.DaoBrokerFactory.getInstance().BuscarDataCodigoBarraProcesso(this);
				pathArquivo = this.getImagem(this.getDtGerCBProcesso()) + "/CodigoBarraProcesso/"
						+ getNumProcesso().replaceAll("/", "") + ".jpg";
			} else {
				REC.DaoBrokerFactory.getInstance().BuscarDataCodigoBarraNotificacao(this);
				pathArquivo = this
						.getImagem(((Integer.parseInt(getCodStatus()) < 10) ? getDtGerCBNotificacaoAut()
								: getDtGerCBNotificacaoPen()))
						+ "/CodigoBarra/"
						+ ((Integer.parseInt(getCodStatus()) < 10) ? getSeqGeracaoCBNotificacaoAut()
								: getSeqGeracaoCBNotificacaoPen())
						+ "/" + ((Integer.parseInt(getCodStatus()) < 10) ? getNumNotifAut() : getNumNotifPen())
						+ ".jpg";
			}
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}

	public String getImagem(String sData) {
		if (sData.length() < 10)
			sData = "01/01/2007";
		String pathImagem = sData.substring(6, 10) + sData.substring(3, 5) + "/" + sData.substring(0, 2);
		return pathImagem;
	}

	public String getCodAcessoWeb() {
		return codAcessoWeb;
	}

	public void setCodAcessoWeb(String codAcessoWeb) {
		if (codAcessoWeb == null)
			codAcessoWeb = "";
		codAcessoWeb = Util.lPad(codAcessoWeb, "0", 4);
		if (codAcessoWeb.equals("0000"))
			codAcessoWeb = "";
		this.codAcessoWeb = codAcessoWeb;
	}

	public String getObsCondutor() {
		return obsCondutor;
	}

	public void setObsCondutor(String obsCondutor) {
		if (obsCondutor == null)
			obsCondutor = "";
		this.obsCondutor = obsCondutor;
	}

	public String getIndPontuacao() {
		return indPontuacao;
	}

	public void setIndPontuacao(String indPontuacao) {
		if (indPontuacao == null)
			indPontuacao = "";
		this.indPontuacao = indPontuacao;
	}

	/**
	 * M�todo para acerto do problema em rela��o ao Org�o PMERJ
	 */
	public String acertaVisualizacaoCodOrgaoPMERJ(String codigoOrgao) {
		if (this.isNumAutoEPmerj()) {
			codigoOrgao = "119100";
		}
		return codigoOrgao;
	}

	public String acertaVisualizacaoSiglaOrgaoPMERJ(String siglaOrgao) {
		if (this.isNumAutoEPmerj()) {
			siglaOrgao = "PMERJ";
		}
		return siglaOrgao;
	}

	public Boolean isNumAutoEPmerj() {
		if (this.numAutoInfracao != null) {
			if (this.numAutoInfracao.length() > 0) {
				if (("I".equals(this.numAutoInfracao.substring(0, 1)))
						|| ("C".equals(this.numAutoInfracao.substring(0, 1)))
						|| ("E".equals(this.numAutoInfracao.substring(0, 1)))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se o Requerimento possui o arquivo de defesa pr�via WEB do tipo
	 * documento (Tipo 1)
	 * 
	 * @return
	 * @throws sys.BeanException
	 */
	public boolean possuiDocumentoRequerimentoWeb() throws sys.BeanException {

		if (!this.getRequerimentos().isEmpty()) {
			for (RequerimentoBean requerimento : this.getRequerimentos()) {
				if (requerimento.possuiDocumentoDPWeb()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se o Requerimento possui o arquivo de defesa pr�via WEB do tipo
	 * CRVL (Tipo 2)
	 * 
	 * @return
	 * @throws sys.BeanException
	 */
	public boolean possuiCRLVRequerimentoWeb() throws sys.BeanException {

		if (!this.getRequerimentos().isEmpty()) {
			for (RequerimentoBean requerimento : this.getRequerimentos()) {
				if (requerimento.possuiCRLVDPWeb()) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Verifica se o Requerimento possui o arquivo de defesa pr�via WEB do tipo
	 * procura��o (Tipo 3)
	 * 
	 * @return
	 * @throws sys.BeanException
	 */
	public boolean possuiProcuracaoRequerimentoWeb() throws sys.BeanException {

		if (!this.getRequerimentos().isEmpty()) {
			for (RequerimentoBean requerimento : this.getRequerimentos()) {
				if (requerimento.possuiProcuracaoDPWeb()) {
					return true;
				}
			}
		}

		return false;
	}

	public String caminhoArquivoDocumento(ParamSistemaBean parSis, String tipoDocumento) {

		StringBuilder caminhoCompleto = new StringBuilder();

		try {
			RequerimentoBean reqBean = this.getRequerimentoTipo("DP");
			String dataProc = reqBean.getDatProcDP();
			String caminhoParametro = parSis.getParamSist("DIR_ARQUIVOS_DIGITALIZADO_" + dataProc.substring(6, 10));

			caminhoCompleto.append(caminhoParametro).append("/").append(dataProc.substring(6, 10))
					.append(dataProc.substring(3, 5)).append("/").append(dataProc.substring(0, 2)).append("/")
					.append(reqBean.getNomeArquivoPor(tipoDocumento));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return caminhoCompleto.toString();
	}

	public ArrayList<HashMap<String, String>> getProcessos(ArrayList<Ata> atas, String codOrgao) {
		ArrayList<HashMap<String, String>> listProcessos = new ArrayList<HashMap<String, String>>();

		try {
			Dao dao = Dao.getInstance();
			listProcessos = dao.getProcessos(atas, codOrgao);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE AUTOINFRACAOBEAN: AO CONSULTAR OS PROCESSOS PARA O RELAT�RIO!");
		}

		return listProcessos;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosSemAta(String codOrgao, String tipoAta,
			int cdNatureza) {
		ArrayList<HashMap<String, String>> processos = new ArrayList<HashMap<String, String>>();

		try {
			Dao dao = Dao.getInstance();
			processos = dao.consultarProcessosSemAta(codOrgao, tipoAta, cdNatureza);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE AUTOINFRACAOBEAN M�TODO: consultarProcessosSemAta().");
		}

		return processos;
	}

	public ArrayList<HashMap<String, String>> consultarProcessosComAta(String codOrgao, String codAta) {
		ArrayList<HashMap<String, String>> processos = new ArrayList<HashMap<String, String>>();

		try {
			Dao dao = Dao.getInstance();
			processos = dao.consultarProcessosComAta(codOrgao, codAta);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE AUTOINFRACAOBEAN M�TODO: consultarProcessosComAta().");
		}

		return processos;
	}

	public void atribuirAta(ArrayList<HashMap<String, String>> processos, String codAta, String desAta, String codOrgao,
			String nomeUsuario) {
		try {
			Dao.getInstance().atribuirAta(processos, codAta, desAta, codOrgao, nomeUsuario);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE AUTOINFRACAOBEAN M�TODO: atribuirAta().");
		}
	}

	public void removerAta(ArrayList<HashMap<String, String>> processos, String codOrgao, String desAta,
			String nomeUsuario, String codAta) {
		try {
			Dao.getInstance().removerAta(processos, codOrgao, desAta, nomeUsuario, codAta);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE AUTOINFRACAOBEAN M�TODO: atribuirAta().");
		}
	}

	public static String getDescricaoStatusPorCodStatus(String codStatus) {
		String nomeStatus = "";

		switch (codStatus) {
		case "0":
			nomeStatus = "REGISTRADO";
			break;
		case "1":
			nomeStatus = "EM NOTIFICA��O DE AUTUA��O";
			break;
		case "2":
			nomeStatus = "NOTIF. SEM SUCESSO - AUTUA��O";
			break;
		case "4":
			nomeStatus = "NOTIFICADO DE AUTUA��O";
			break;
		case "5":
			nomeStatus = "DEFESA PR�VIA";
			break;
		case "8":
			nomeStatus = "D.O. DE AUTUA��O";
			break;
		case "10":
			nomeStatus = "PENALIDADE REGISTRADA";
			break;
		case "11":
			nomeStatus = "EM NOTIFICA��O DE PENALIDADE";
			break;
		case "12":
			nomeStatus = "NOTIF. SEM SUCESSO - PENALIDADE";
			break;
		case "14":
			nomeStatus = "NOTIFICADO DE PENALIDADE";
			break;
		case "15":
			nomeStatus = "RECURSO DE 1A. INST�NCIA";
			break;
		case "16":
			nomeStatus = "EFEITO SUSPENSIVO";
			break;
		case "18":
			nomeStatus = "D.O. DE 1A. INST�NCIA";
			break;
		case "30":
			nomeStatus = "AGUARDANDO 2A. INST�NCIA";
			break;
		case "35":
			nomeStatus = "RECURSO DE 2A. INST�NCIA";
			break;
		case "38":
			nomeStatus = "D.O. DE 2A. INST�NCIA";
			break;
		case "82":
			nomeStatus = "PENALIDADE REGISTRADA - ANT";
			break;
		case "90":
			nomeStatus = "CANCELADO";
			break;
		case "91":
			nomeStatus = "INTEMPESTIVO";
			break;
		case "95":
			nomeStatus = "SUSPENSO";
			break;
		case "96":
			nomeStatus = "DEFERIMENTO - 1A. INST�NCIA";
			break;
		case "97":
			nomeStatus = "DEFERIMENTO - 2A. INST�NCIA";
			break;
		case "98":
			nomeStatus = "DEFERIMENTO - DEFESA PR�VIA";
			break;
		case "99":
			nomeStatus = "TRANSITADO EM JULGADO";
			break;
		case "100":
			nomeStatus = "PENALIDADE PAGA";
			break;
		case "200":
			nomeStatus = "DIVIDA ATIVA";
			break;
		case "1000":
			nomeStatus = "EM PENALIDADE DE ADVERTENCIA POR ESCRITO";
			break;
		case "9000":
			nomeStatus = "CANCELADO PELA SECRETARIA";
			break;
		case "9001":
			nomeStatus = "CANCELADO AI SUBSTITUIDO";
			break;
		case "9002":
			nomeStatus = "CANCELADO PELO FISCAL";
			break;
		case "9003":
			nomeStatus = "CANCELADO MARCA/MODELO";
			break;
		case "9004":
			nomeStatus = "CANCELADO N�O LOCALIZADO";
			break;
		case "9005":
			nomeStatus = "CANCELADO FORA DO PRAZO";
			break;
		}

		return nomeStatus;
	}

	public boolean permitirAbrirRequerimento() {
		if (codStatus.equals("90") || codStatus.equals("96") || codStatus.equals("97") || codStatus.equals("98")
				|| codStatus.equals("99") || codStatus.equals("9000") || codStatus.equals("9001") || codStatus.equals("9002")
				|| codStatus.equals("9003") || codStatus.equals("9004")) {
			return false;
		}

		return true;
	}

	public static boolean verificarNumeroProcesso(String numeroProcesso, String numeroAuto) throws Exception {
		return Dao.getInstance().verificarNumeroProcesso(numeroProcesso, numeroAuto);
	}

	public static void atualizar(AutoInfracaoBean auto) throws Exception {
		Dao.getInstance().atualizarAutoInfracao(auto);
	}
}