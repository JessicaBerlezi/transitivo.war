package REC;



/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
* <b>Description:</b>  Abrir Recurso Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class AbrirRecursoDPBean    extends AbrirRecBean     { 


  public AbrirRecursoDPBean()  throws sys.BeanException {

  super() ;	
  
  }
  
//--------------------------------------------------------------------------  
  public void setEventoOK(ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)  throws sys.BeanException  {
	if ("".equals(myAuto.getNomStatus())) {
		setMsg("Status: "+myAuto.getCodStatus()+" n�o cadastrado na Tabela.");
		setEventoOK("N");	
	}
	else {
  	  setMsg(myAuto.getNomStatus()+" (Data do Status: "+myAuto.getDatStatus()+")");
	  String complMsg = "" ;
	  try {
		  	setEventoOK("S");	  
		  // Status 0,1,2,4,5
		  if ( (myAuto.getCodStatus().length()==1) && (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA","0,1,2,4,5","1").indexOf(myAuto.getCodStatus())<0) )  
			{
		  	setEventoOK("N");
		  	complMsg = " - N�o pode entrar com Defesa Pr�via";
		  }
		  // Status 0,1,2,4,5
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA","0,1,2,4,5","1").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()<2) )	{
		  	complMsg = " - Defesa Pr�via";
		  	setNFuncao("DEFESA PR�VIA") ;
		  	/*Michel
			setTpSolDP(getDP("DP",myParam,myAuto));
			setTpSolDC(getDC("DC",myParam,myAuto));
			*/
			setTpSolDP(getDP("DP",myParam,myAuto));
			if ((!myAuto.getProprietario().getNomResponsavel().trim().equals(myAuto.getCondutor().getNomResponsavel().trim())) && (myAuto.getInfracao().getIndTipo().equals("C")) )	
			  setTpSolDC(getDC("DC",myParam,myAuto));			
		  }
		  if ( ("".equals(getTpSolDP())) && ("".equals(getTpSolDC())) ) {
			  setEventoOK("N");
			  complMsg = " - Nenhuma solicita��o efetuada.";
		  }
		  if (myAuto.getOrgao().getIntegrada().equals("N"))	  {
		      setEventoOK("N");
		      complMsg = " - Org�o isolado. N�o pode entrar com Defesa Pr�via";
		  }
		  
		  setMsg(getMsg() + complMsg) ;
	 }
	 catch (Exception ue) {
       throw new sys.BeanException("AbrirRecursoDPBean: " + ue.getMessage());
	 }
	 
   }
 }   




}
