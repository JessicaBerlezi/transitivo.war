package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

 

/**
* <b>Title:</b>      Recurso - Ajuste Manual<br>
* <b>Description:</b>Transacao 413:Ajuste Manual do Respons�vewl pelos pontos<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/
public class AjusteManualRespPontosCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteManualRespPontos.jsp";

	public AjusteManualRespPontosCmd() {
		next = jspPadrao;
	}

	public AjusteManualRespPontosCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
		    nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;

			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
			
            RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;
            
            HistoricoBean myHist = new HistoricoBean();
            
	
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";

			
			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AutoInfracaoBeanId.setMsgOk("S");
					
			}else if (acao.equals("ConfirmaAjuste")) { 	
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
    				  AutoInfracaoBeanId.getCondutor().setNomResponsavel(req.getParameter("nomResp"));
    				  AutoInfracaoBeanId.getCondutor().setNumCnh(req.getParameter("numCNH"));
    				  AutoInfracaoBeanId.getCondutor().setCodUfCnh(req.getParameter("codUfCNHTR"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
    				  AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
    				  AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNH"));
    				  if ("0".equals(AutoInfracaoBeanId.getCondutor().getIndTipoCnh())) AutoInfracaoBeanId.getCondutor().setIndTipoCnh("1");
    				  else  AutoInfracaoBeanId.getCondutor().setIndTipoCnh("2");
    				  AutoInfracaoBeanId.getCondutor().setNumCpfCnpj(req.getParameter("cpfpnthidden"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setNomBairro(req.getParameter("nomBairro"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setNomCidade(req.getParameter("nomCidade"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setNumCEPEdt(req.getParameter("numCEPTR"));
    				  AutoInfracaoBeanId.getCondutor().getEndereco().setCodUF(req.getParameter("codUf"));
    				  RequerimentoId.setDatRequerimento(req.getParameter("dataReq"));
    				  
    				  
    				  HistoricoBean myHis = new HistoricoBean();
    				  myHis.setCodAutoInfracao(AutoInfracaoBeanId.getCodAutoInfracao());   
    				  myHis.setNumAutoInfracao(AutoInfracaoBeanId.getNumAutoInfracao()); 
    				  myHis.setNumProcesso(AutoInfracaoBeanId.getNumProcesso());
    				  myHis.setCodStatus(AutoInfracaoBeanId.getCodStatus());  
    				  myHis.setDatStatus(AutoInfracaoBeanId.getDatStatus());
    				  myHis.setCodEvento("413");
    				  myHis.setCodOrigemEvento("100");		
    				  myHis.setNomUserName(UsrLogado.getNomUserName());
    				  myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
    				  myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
    				  myHis.setTxtComplemento01(req.getParameter("dataReq"));
    				  
    				  String numCPF=""; /*Vari�vel criada, pois b�o existe no JSP*/
    				  
    				  myHis.setTxtComplemento02(numCPF);			
    				  myHis.setTxtComplemento03(req.getParameter("nomResp"));
    				  myHis.setTxtComplemento04(req.getParameter("txtEndereco")+" "+req.getParameter("txtNumEndereco")+" "+req.getParameter("txtComplemento"));
    				  myHis.setTxtComplemento05(req.getParameter("nomBairro"));
    				  myHis.setTxtComplemento06(req.getParameter("nomCidade"));
    				  myHis.setTxtComplemento07(req.getParameter("codUf"));
    				  myHis.setTxtComplemento08(req.getParameter("numCEPTR"));
    				  myHis.setTxtComplemento09(req.getParameter("numCNH"));
    				  myHis.setTxtComplemento10(req.getParameter("codUfCNHTR"));
                      String decisaoJur = req.getParameter("decisaoJur");
                      if(decisaoJur == null) decisaoJur = "0";
                      myHist.setTxtComplemento11(decisaoJur);
                      
    				  DaoBroker dao = DaoBrokerFactory.getInstance();
    				  dao.ajustarManual(AutoInfracaoBeanId,UsrLogado,RequerimentoId,myHist);
    				  
    				  if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
    					   AutoInfracaoBeanId.setMsgErro("Ajuste Manual do Respons�vel pelos pontos realizado com sucesso.") ;
    					   nextRetorno	= "/REC/AjusteManualRespPontosImp.jsp" ;
    				  }
				}
			}

			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
            req.setAttribute("myHist", myHist);
			req.setAttribute("RequerimentoId",RequerimentoId) ;
			req.setAttribute("UsrLogado",UsrLogado) ;
		}
		catch (Exception ue) {
			  throw new sys.CommandException("AjusteManualRespPontosCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	  }

}