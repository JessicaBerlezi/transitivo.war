package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Relator<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class RelatorCmd extends sys.Command {


  private static final String jspPadrao="/REC/Relator.jsp";    
  private String next;

  public RelatorCmd() {
    next = jspPadrao;
  }

  public RelatorCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do org�o, se n�o existir
		ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null)
		  OrgaoId = new ACSS.OrgaoBean() ;	 
		// cria os Beans da Junta, se n�o existir
		JuntaBean JuntaId = (JuntaBean)req.getAttribute("JuntaId") ;
		if(JuntaId == null)
		  JuntaId = new JuntaBean() ;	 
		
		RelatorBean RelatorId = (RelatorBean)req.getAttribute("RelatorId") ;
		if(RelatorId == null)
		  RelatorId = new RelatorBean() ;	
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)  acao =" ";   
		  
	    String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao == null) codOrgao =""; 
		
	    String codJunta = req.getParameter("codJunta"); 		
		if(codJunta == null)  codJunta ="";   
		
		String atualizarDependente = "N";
		
	    OrgaoId.Le_Orgao(codOrgao,0) ;
		JuntaId.Le_Junta(codJunta,0) ;
		RelatorId.setCodOrgao(codOrgao);
		RelatorId.setCodJunta(codJunta);
			 
	    if(acao.compareTo("buscaRelator")==0) {
		  List relatores = RelatorId.getJuntaRelator();
		  if(relatores.size()<10){
		  	for(int i=relatores.size();i<10;i++)
		  		relatores.add(new RelatorBean());
		  }
		  else{
		  	for(int i=0;i<5;i++)
		  		relatores.add(new RelatorBean());
		  }
		  if( relatores != null ){
			  atualizarDependente ="S"; 	
			  req.setAttribute("relatores",relatores);
		  }
	    }
			 
		Vector vErro = new Vector(); 
					 
	    if(acao.compareTo("A") == 0){
			String[] codRelator= req.getParameterValues("codRelator");
			  
			String[] nomRelator = req.getParameterValues("nomRelator");  
			String[] nomRelator_aux = req.getParameterValues("nomRelator_aux"); 
			String[] numCpf = req.getParameterValues("numCpf");
			String[] numCpf_aux = req.getParameterValues("numCpf_aux");   
			String[] nomTitulo = req.getParameterValues("nomTitulo");
			String[] nomTitulo_aux = req.getParameterValues("nomTitulo_aux");
            String[] indAtivo = req.getParameterValues("indAtivo");		
			String[] indAtivo_aux = indAtivo;
            vErro = isParametros(codRelator, nomRelator, numCpf, indAtivo);
						 
            if (vErro.size()==0) {
				List relator 	 = new ArrayList(); 
				List relator_aux = new ArrayList();
				RelatorBean myRelator; 
				RelatorBean myRelator_aux; 
				for (int i = 0; i < nomRelator.length;i++) {
					myRelator 	  = new RelatorBean();	
					myRelator_aux = new RelatorBean();		
					myRelator.setCodRelator(codRelator[i]);
					myRelator_aux.setCodRelator(codRelator[i]);
					myRelator.setNomRelator(nomRelator[i]);	
					myRelator_aux.setNomRelator(nomRelator_aux[i]);	  
				    myRelator.setCodOrgao(codOrgao);
				    myRelator_aux.setCodOrgao(codOrgao);
					myRelator.setCodJunta(codJunta);
				    myRelator_aux.setCodJunta(codJunta);
				    myRelator.setNumCpfEdt(numCpf[i]);		
				    myRelator_aux.setNumCpfEdt(numCpf_aux[i]);	
				    myRelator.setNomTitulo(nomTitulo[i]);		
				    myRelator_aux.setNomTitulo(nomTitulo_aux[i]);
                    myRelator.setIndAtivo(indAtivo[i]);
                    myRelator_aux.setIndAtivo(indAtivo_aux[i]);
					relator.add(myRelator); 	
					relator_aux.add(myRelator_aux);
				}
				if( RelatorId.isInsertRelator(relator, relator_aux) )
					RelatorId.setMsgErro("Dados Alterados com Sucesso");
				atualizarDependente ="S"; 
				req.setAttribute("relatores",relator);
			}
			else RelatorId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
			List relatores = RelatorId.getJuntaRelator();
			/*if(relatores.size()<10){
				for(int i=0;i<5;i++)
					relatores.add(new RelatorBean());
			}	*/
			req.setAttribute("relatores",relatores);
			nextRetorno = "/REC/RelatorImp.jsp";
	    }
	    
	    req.setAttribute("atualizarDependente",atualizarDependente);
		req.setAttribute("OrgaoId",OrgaoId) ;		 
		req.setAttribute("JuntaId",JuntaId) ;
		req.setAttribute("RelatorId",RelatorId) ;
    }
    catch (Exception ue) {
      throw new sys.CommandException("RelatorCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
  private Vector isParametros(String[] codRelator, String[] nomRelator,
          String[] numCpf, String[] indAtivo) {
      Vector vErro = new Vector();
      if ((codRelator.length != nomRelator.length)
              || (nomRelator.length != numCpf.length)
              || (indAtivo.length != numCpf.length))
          vErro.addElement("Par�metros inconsistentes");
      return vErro;
  }

}