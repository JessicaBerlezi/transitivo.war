package REC;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;


/**
 * <b>Title:</b>        SMIT - Prepara Guia de Distribuicao<br>
 * <b>Description:</b>  Prepara Guia de Distribuicao <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Wellem Mello
 * @version 1.0
 */

public class RemessaBean extends EventoBean  { 
	
	private String msgErro ; 
	private String msgOk ; 
	
	private String codRemessa;
	private String statusRemessa;  
	private String indRecurso;
	private String codOrgao;  
	private String username; 	
	private String codOrgaoLotacao;
	private String codOrgaoLotacaoRecebimento;
	private String datProc;
	private String usernameRecebimento;
	
	private String datProcRecebimento;
	private String datInicio;

	
	private List Autos;

	private String ordClass ;
	private String ordem ;
	public final String SIT_GUIA_DISTRIBUICAO = "S"; 
	private String geraGuia;
	private String sitGuiaDistribuicao;
	
	private String codRequerimento;
	private String numRequerimento;
	private String codAutoInfracao;
	private String dataIni ;
	private String dataFim;
	
	public RemessaBean()  throws sys.BeanException {
		super() ;
		msgErro                    = "" ;
		msgOk                      = "" ;		
		codRemessa                 = "" ;	
		statusRemessa              = "" ; 
		indRecurso                 = "" ;		
		codOrgao                   = "" ;  
		username                   = "" ;		
		codOrgaoLotacao            = "" ;
		codOrgaoLotacaoRecebimento = "" ;
		datProc                    = "" ;
		datInicio                  = "" ;
		usernameRecebimento        = "" ;		
		datProcRecebimento         = "" ;	
		ordClass        		   = "" ;		
		ordem                      = "" ;	
		Autos                      = new ArrayList() ;	
		geraGuia                   = "";
		sitGuiaDistribuicao        = "";
		codRequerimento            = "";
		numRequerimento            = "";
		dataIni = formatedLast2Month().substring(0, 10);
		dataFim = sys.Util.formatedToday().substring(0, 10);
		
	}
	
//-------------------------------------------------------------------------------------	
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j)+"\n" ;
		}
	}
	public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
	}   
	
	public String getMsgErro()   {
		return this.msgErro;
	}    
	public void setMsgOk(String msgOk)   {
		this.msgOk = msgOk ;
	}   
	public String getMsgOk()   {
		return this.msgOk;
	} 
	//--------------------------------------------------------------------------   
	public void setAutos(List Autos)  {
		this.Autos=Autos ;	
	}
	public List getAutos()  {
		return this.Autos;
	}
	public AutoInfracaoBean getAutos(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.Autos.size()) ) return (new AutoInfracaoBean()) ;
		}
		catch (Exception e) { 			
			throw new sys.BeanException(e.getMessage());
		}
		return (AutoInfracaoBean)this.Autos.get(i);
	}	
	//--------------------------------------------------------------------------
	public void setCodOrgaoLotacaoRecebimento(String codOrgaoLotacaoRecebimento)  {
		this.codOrgaoLotacaoRecebimento=codOrgaoLotacaoRecebimento ;
		if (codOrgaoLotacaoRecebimento==null) this.codOrgaoLotacaoRecebimento= "";
	}  
	public String getCodOrgaoLotacaoRecebimento()  {
		return this.codOrgaoLotacaoRecebimento;
	}	
	//--------------------------------------------------------------------------
	public void setCodRemessa(String codRemessa)  {
		this.codRemessa=codRemessa ;
		if (codRemessa==null) this.codRemessa= "";
	}  
	public String getCodRemessa()  {
		return this.codRemessa;
	}  
	//--------------------------------------------------------------------------
	public void setCodOrgao(String codOrgao)  {
		this.codOrgao=codOrgao ;
		if (codOrgao==null) this.codOrgao= "";
	}  
	public String getCodOrgao()  {
		return this.codOrgao;
	}  
	//--------------------------------------------------------------------------
	public void setIndRecurso(String indRecurso)  {
		this.indRecurso=indRecurso ;
		if (indRecurso==null) this.indRecurso= "";
	}  
	public String getIndRecurso()  {
		return this.indRecurso;
	}    
	//--------------------------------------------------------------------------
	public void setStatusRemessa(String statusRemessa)  {
		this.statusRemessa=statusRemessa ;
		if (statusRemessa==null) this.statusRemessa= "";
	}  
	public String getStatusRemessa()  {
		return this.statusRemessa;
	}
	  
	//--------------------------------------------------------------------------
	public void setUsername(String username)  {
		this.username=username ;
		if (username==null) this.username= "";
	}  
	public String getUsername()  {
		return this.username;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
		this.codOrgaoLotacao=codOrgaoLotacao ;
		if (codOrgaoLotacao==null) this.codOrgaoLotacao= "";
	}  
	public String getCodOrgaoLotacao()  {
		return this.codOrgaoLotacao;
	}
	//--------------------------------------------------------------------------
	public void setDatProc(String datProc)  {
		this.datProc=datProc ;
		if (datProc==null) this.datProc= "";
	}  
	public String getDatProc()  {
		return this.datProc;
	}
	//--------------------------------------------------------------------------	
	public void setDatInicio(String datInicio)  {
		this.datInicio=datInicio ;
		if (datInicio==null) this.datInicio= "";
	}  
	public String getDatInicio()  {
		return this.datInicio;
	}
	
	//--------------------------------------------------------------------------
	public void setUsernameRecebimento(String usernameRecebimento)  {
		this.usernameRecebimento=usernameRecebimento ;
		if (usernameRecebimento==null) this.usernameRecebimento= "";
	}  
	public String getUsernameRecebimento()  {
		return this.usernameRecebimento;
	}
//	--------------------------------------------------------------------------
	public void setDatProcRecebimento(String datProcRecebimento)  {
		this.datProcRecebimento=datProcRecebimento ;
		if (this.datProcRecebimento==null) this.datProcRecebimento= "";
	}  
	public String getDatProcRecebimento()  {
		return this.datProcRecebimento;
	}
	
//	--------------------------------------------------------------------------
	public void setGeraGuia(String geraGuia) {
		if (geraGuia == null) geraGuia = "";
		else this.geraGuia = geraGuia;
	}
	public String getGeraGuia() {
		return geraGuia;
	}
	
//-----------------------------------------------------------------------------
	public void setCodRequerimento(String codRequerimento) {
		if(codRequerimento == null) codRequerimento = "";
		else this.codRequerimento = codRequerimento;
	}
	public String getCodRequerimento() {
		return codRequerimento;
	}

//-----------------------------------------------------------------------------
	public void setNumRequerimento(String numRequerimento) {
		if(numRequerimento == null) numRequerimento = "";
		else this.numRequerimento = numRequerimento;
	}
	public String getNumRequerimento() {
		return numRequerimento;
	}
//-----------------------------------------------------------------------------
	public void setCodAutoInfracao(String codAutoInfracao) {
		if (codAutoInfracao == null)codAutoInfracao = "";
		else this.codAutoInfracao = codAutoInfracao;
	}
	public String getCodAutoInfracao() {
		return codAutoInfracao;
	}

//--------------------------------------------------------------------------
	public void setDataIni(String dataIni) {
		if (dataIni == null) dataIni = "";
		else this.dataIni = dataIni;
	}
	public String getDataIni() {
		return dataIni;
	}
//--------------------------------------------------------------------------
	public void setDataFim(String dataFim) {
		if (dataFim == null) dataFim = "";
		else this.dataFim = dataFim;
	}
	public String getDataFim() {
		return dataFim;
	}
//-----------------------------------------------------------------------------
	public void PreparaRemessa(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try { 
			Autos  		     = new ArrayList() ;	
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());		
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			if (dao.PreparaRemessa(this,myUsrLog)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());			
				
				/*Classifica("Enquadramento");*/
				Classifica("DatProcesso");			
				
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());			
			}	   		   
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	} 
	//	--------------------------------------------------------------------------
	public void setOrdClass(String ordClass)  {
		this.ordClass=ordClass ;
		if (ordClass==null) this.ordClass= "ascendente";
	} 
	public String getOrdClass()  {
		return this.ordClass;
	}
	//	--------------------------------------------------------------------------
	public void setOrdem(String ordem)  {
		this.ordem=ordem ;
		if (ordem==null) this.ordem= "Data";
	}  
	public String getOrdem()  {
		return this.ordem;
	}
	public String getNomOrdem()  {
		String nomOrdem = "Data Infra��o, Org�o e Num Auto" ;
		if (this.ordem.equals("Orgao"))       nomOrdem = "Org�o e Num Auto" ;
		if (this.ordem.equals("Placa"))       nomOrdem = "Placa e Data Infra��o" ;   
		if (this.ordem.equals("Processo"))    nomOrdem = "Processo" ;   
		if (this.ordem.equals("Auto"))        nomOrdem = "Auto de Infra��o" ;   
		if (this.ordem.equals("Responsavel")) nomOrdem = "Respons�vel e Placa" ;  
		if (this.ordem.equals("Status"))      nomOrdem = "Status e Data Infra��o" ;  
		if (this.ordem.equals("DatProcesso")) nomOrdem = "Data e Processo"  ;  
		if (this.ordem.equals("Enquadramento")) nomOrdem = "Enquadramento, Data e Processo"  ; 	
		return nomOrdem+ " ("+getOrdClass()+")" ;
	}
	//	--------------------------------------------------------------------------
	public void Classifica(String ordemSol) throws sys.BeanException {
		int ord = 0;
		if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
		if (ordemSol.equals("Orgao"))               ord = 1 ;
		if (ordemSol.equals("Placa"))               ord = 2 ;   
		if (ordemSol.equals("Processo"))            ord = 3 ;  
		if (ordemSol.equals("Responsavel"))         ord = 4 ; 
		if (ordemSol.equals("Auto"))                ord = 5 ;  
		if (ordemSol.equals("Status"))              ord = 6 ;  
		if (ordemSol.equals("DatProcesso"))         ord = 7 ;  
		if (ordemSol.equals("Enquadramento"))       ord = 8 ;		   		  
		if (ordemSol.equals(getOrdem()))   {
			if ( getOrdClass().equals("ascendente")) setOrdClass("descendente"); 
			else 									 setOrdClass("ascendente"); 
		}
		else setOrdClass("ascendente");
		setOrdem(ordemSol) ;
		int tam = getAutos().size() ;
		AutoInfracaoBean tmp = new AutoInfracaoBean();
		String d1,chave ;  
		for (int i=0; i<tam; i++)  {  
			switch (ord) {
			// "Data Infra��o, Org�o e Num Auto" ;
			case 0:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()) + 
				sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) +
				sys.Util.rPad(getAutos(i).getNumAutoInfracao()," ",12);
				break;
				// "Org�o e Num Auto" ;
			case 1:
				chave = sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) ;
				break;
				// "Placa e Data Infra��o" ;   
			case 2:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao());  			 
				break;
				// "Processo" ;   
			case 3:
				chave = sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
				break;
				// "Respons�vel e Placa" ; 
			case 4:
				chave = sys.Util.rPad(getAutos(i).getProprietario().getNomResponsavel(),"",40) + 		  	
				sys.Util.rPad(getAutos(i).getNumPlaca(),"",7) ;
				break;
				// "Num Auto" ;
			case 5:
				chave = sys.Util.rPad(getAutos(i).getNumAutoInfracao(),"",12) ; 		  	
				break;
				// Status e Data da Infracao
			case 6:	
				chave = sys.Util.lPad(getAutos(i).getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()) ; 
				break;
				// "Data Processo, Processo" ;
			case 7:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatProcesso()) + 
				sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
				break;
				// "Enquadramento, Processo" ;
			default:		
				chave = d1 = sys.Util.rPad(getAutos(i).getInfracao().getDscEnquadramento()," ",50) +  
				sys.Util.formataDataYYYYMMDD(getAutos(i).getDatProcesso()) +
				sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
			}			  
			tmp = getAutos(i) ;
			tmp.setChaveSort(chave) ;
			getAutos().set(i,tmp);			  
		}
		QSort(getAutos(),getOrdClass()) ;
	}	
	
	//	--------------------------------------------------------------------------
	public static void QSort(List lista,int lo0, int hi0,String ordem)
	throws sys.BeanException {
		int lo = lo0;
		int hi = hi0;
		AutoInfracaoBean tmp ;
		if (lo >= hi) return;
		else if( lo == hi - 1 ) {
			/*
			 *  sort a two element list by swapping if necessary 
			 */
			if (QSortCompara((AutoInfracaoBean)lista.get(lo),(AutoInfracaoBean)lista.get(hi),ordem))   {  // a[lo] > a[hi]) 
				tmp = (AutoInfracaoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(AutoInfracaoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}
			return;
		}
		/*
		 *  Pick a pivot and move it out of the way
		 */
		AutoInfracaoBean pivot = (AutoInfracaoBean)lista.get((lo + hi) / 2);
		lista.set( ((lo + hi)/2),lista.get(hi));
		lista.set(hi,pivot) ;
		
		while( lo < hi ) {
			/*
			 *  Search forward from a[lo] until an element is found that
			 *  is greater than the pivot or lo >= hi 
			 */
			while ( !(QSortCompara((AutoInfracaoBean)lista.get(lo),pivot,ordem)) && (lo < hi) )       {    //(a[lo] <= pivot) && (lo < hi)) 
				lo++;
			}
			/*
			 *  Search backward from a[hi] until element is found that
			 *  is less than the pivot, or lo &gt;= hi
			 */
			while ( !(QSortCompara(pivot,(AutoInfracaoBean)lista.get(hi),ordem)) && (lo < hi) ) {    // (pivot <= a[hi])
				hi--;
			}
			/*
			 *  Swap elements a[lo] and a[hi]
			 */
			if( lo < hi ) {
				tmp = (AutoInfracaoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(AutoInfracaoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}	    
		}
		/*
		 *  Put the median in the "center" of the list
		 */
		lista.set(hi0,(AutoInfracaoBean)lista.get(hi)) ;         // a[hi0] = a[hi];
		lista.set(hi,pivot) ;                     // a[hi] = pivot;
		/*
		 *  Recursive calls, elements a[lo0] to a[lo-1] are less than or
		 *  equal to pivot, elements a[hi+1] to a[hi0] are greater than
		 *  pivot.
		 */
		QSort(lista, lo0, lo-1,ordem);
		QSort(lista, hi+1, hi0,ordem);	  		  	
	}
	
	public static void QSort(List lista,String ordem) throws sys.BeanException {
		QSort(lista, 0, lista.size()-1,ordem);
	}
	public static boolean QSortCompara(AutoInfracaoBean a0,AutoInfracaoBean a1,String ordem)
	throws sys.BeanException {
		boolean b = false ;
		if (ordem.equals("descendente")) b = (a1.getChaveSort()).compareTo(a0.getChaveSort())>0 ;
		else							 b = (a0.getChaveSort()).compareTo(a1.getChaveSort())>0 ;	 	
		return b ;
	}
	
	//-----------------------------------------------------------------------------------------

	   public void estornarRemessa() throws sys.CommandException {
	   	try {	

   			if(statusRemessa.equals("2")){
   				statusRemessa = "8";
   		    //	dao.RemessaEstornoJuntaRelator(this);
   			}
   			else if(statusRemessa.equals("3")){
   				statusRemessa = "2";
   			//	dao.RemessaEstornoResultado(this);	
   			}
   			else if(statusRemessa.equals("4")){
   				statusRemessa = "3";
   			//	dao.RemessaEstornoDO(this);
   			}
	   	}
	   	catch (Exception ue) {
	   		throw new sys.CommandException("RemessaEstornoCmd: " + ue.getMessage());
	   	}
	   	if(this.getMsgErro().length() == 0)
	   		this.setMsgErro("Estorno da Remessa: " + codRemessa + " efetuado com sucesso");
	   	return ;
	   }
	   
    //---------------------------------------------------------------  
		
		public void setJ_sigFuncao(String j_sigFuncao,String obrParJur)   {
			if (j_sigFuncao==null) j_sigFuncao = "" ;
			setCFuncao(j_sigFuncao) ;
			// Defesa Previa 
			setStatusValido("5") ;
			setStatusTPCVValido("0,1,2,3,4");
			setTipoReqValidos("DP,DC");
			
			//Guia de Remessa
			if (  ("REC0127".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - DEFESA DA AUTUA��O") ;
			else if (  ("REC0128".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - RECURSO DE MULTA - 1� INST") ;
			else if (  ("REC0129".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - RECURSO DE MULTA - 2� INST") ;
			else if (  ("REC0131".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - REEMITIR GUIA") ;
			else if (  ("REC0203".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - DEFESA DA AUTUA��O");
			else if (  ("REC0312".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - RECURSO DE MULTA - 1� INST") ;
			else if (  ("REC0403".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA  - RECURSO DE MULTA - 2� INST") ;
			else if ( ("REC0135".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC  - DEFESA DA AUTUA��O") ;
			else if ( ("REC0136".equals(j_sigFuncao.trim())))
				setNFuncao("GUIA DE REMESSA CEDOC - RECURSO DE MULTA - 1� INST") ;
			else if ( ("REC0137".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC - RECURSO DE MULTA - 2� INST") ;
			else if ( ("REC0138".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC - REEMITIR GUIA") ;
			else if ( ("REC0206".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC - DEFESA DA AUTUA��O");
			else if ( ("REC0314".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC - RECURSO DE MULTA - 1� INST") ;
			else if ( ("REC0405".equals(j_sigFuncao.trim())) )
				setNFuncao("GUIA DE REMESSA CEDOC - RECURSO DE MULTA - 2� INST") ;
			else
				setNFuncao("GUIA DE REMESSA") ;

	
			// Testar parametro de Orgao - para verificar se � obrigat�rio Parecer Juridico.
			if (obrParJur==null) obrParJur = "N";
			if ("S".equals(obrParJur)) setStatusReqValido("0") ;
			else setStatusReqValido("0") ;	
			
			return ;	
		}   
 //--------------------------------------------------------------------------  
		public void LeRemessa(ACSS.UsuarioBean myUsrLog,ACSS.UsuarioFuncBean myUsrFunc) throws sys.BeanException {
			try { 
				Autos  		     = new ArrayList() ;		
				GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
				dao.RemessaLe(this,myUsrLog,myUsrFunc) ;		
				Classifica("Processo") ;
			}
			catch (Exception ex) {
				throw new sys.BeanException(ex.getMessage());
			}
		}
		
		
	public void estornarGuia() throws sys.CommandException {
	 try {	
		 GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
		 if(statusRemessa.equals("1")){
			 statusRemessa="0";
			 dao.RemessaEstornoRecebimento(this);
		 }
		 else if(statusRemessa.equals("0")){
			 dao.RemessaEstorno(this);	
		 }
	 }
	 catch (Exception ue) {
		 throw new sys.CommandException("RemessaEstornoCmd: " + ue.getMessage());
	 }
	 if(this.getMsgErro().length() == 0)
		 this.setMsgErro("Estorno da Remessa: " + codRemessa + " efetuado com sucesso");
	 return ;
	}
	
	public void gravaSitGuiaDistribuicao(RemessaBean myRemessa) throws sys.BeanException {
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			dao.gravaSitGuiaDistribuicao(myRemessa) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}	
	
	public boolean getQTDAutosRemessa(RemessaBean myRemessa) throws sys.BeanException {
		boolean bQTDAutosRemessa = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bQTDAutosRemessa = dao.getQTDAutosRemessa(myRemessa) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bQTDAutosRemessa;
	}

    //--------------------------------------------------------------------------  
	public void excluirProc(RemessaBean remessaBean, AutoInfracaoBean myAuto,
			String codReq) throws sys.BeanException {
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			dao.excluirProc(remessaBean,myAuto,codReq) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

//----------------------------------------------------------------------------
	public boolean getCodRemessa(RemessaBean remessaBean, AutoInfracaoBean myAuto) throws sys.BeanException {
		boolean bOk = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bOk = dao.getCodRemessa(remessaBean,myAuto) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bOk;
	}

	public boolean getCodRemessaEnviada(RemessaBean remessaBean, AutoInfracaoBean myAuto) throws sys.BeanException {
		boolean bOk = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bOk = dao.getCodRemessaEnviada(remessaBean,myAuto) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bOk;
	}
	
	
	public boolean getCodRemessa(RequerimentoBean myReq) throws sys.BeanException {
		boolean bOk = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bOk = dao.getCodRemessa(myReq) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bOk;
	}
	
	public boolean existeRemessa(RemessaBean remessaBean, AutoInfracaoBean myAuto) throws sys.BeanException {
		boolean bOk = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bOk = dao.existeRemessa(remessaBean,myAuto) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bOk;
	}
	
//-------------------------------------------------------------------------------------
	public void setSitGuiaDistribuicao(String sitGuiaDistribuicao) {
		if (sitGuiaDistribuicao == null) sitGuiaDistribuicao = "";
		else this.sitGuiaDistribuicao = sitGuiaDistribuicao;
	}
	public String getSitGuiaDistribuicao() {
		return sitGuiaDistribuicao;
	}
	
//------------------------------------------------------------------------------------	
	public boolean verifGerouGuia(RemessaBean remessaBean) throws sys.BeanException {
		boolean bOk = false;
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			bOk = dao.verifGerouGuia(remessaBean) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return bOk;
	}

//-------------------------------------------------------------------------------------
	/* Filtra os autos, deixando apenas os que n�o est�o vinculados 
	 * em uma guia de distribuicao */

	public void filtaAutosRemessa(AutoInfracaoBean myAuto) throws sys.BeanException {
		try { 
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();	      
			dao.filtaAutosRemessa(myAuto) ;		
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	//---------------------------------------------------------------------------------------------
	public static String formatedLast2Month() {

		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -60);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;

	} // Fim formatedToday

}

