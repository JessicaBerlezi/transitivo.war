package REC;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GuiaPreparaRemessaManualCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaPreparaRemessaManual.jsp" ;  
   
   public GuiaPreparaRemessaManualCmd() {
      next = jspPadrao;
   }

   public GuiaPreparaRemessaManualCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	  String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
          
		  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
		  
      	  GuiaDistribuicaoBean GuiaDistribuicaoId = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;	  	
      	  
      	  //Carrego os Beans
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
		  GuiaDistribuicaoId.setMsgErro("");
		  
		  AutoInfracaoBean AutoInfracaoManualBeanId = (AutoInfracaoBean) req.getAttribute("AutoInfracaoManualBeanId");
		  if (AutoInfracaoManualBeanId == null) AutoInfracaoManualBeanId = new AutoInfracaoBean();

		  RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
		  if (RequerimentoId == null) RequerimentoId = new RequerimentoBean();
		  
		  GuiaIncluirProcessoBean GuiaIncluirProcessoBeanId = (GuiaIncluirProcessoBean) req.getAttribute("GuiaIncluirProcessoBeanId");
		  if (GuiaIncluirProcessoBeanId == null) GuiaIncluirProcessoBeanId = new GuiaIncluirProcessoBean();

		  RemessaBean RemessaId = (RemessaBean) session.getAttribute("RemessaId");
		  if (RemessaId == null) RemessaId = new RemessaBean();
		  
		  //Carrega Parametros
		  String sNumProcesso = req.getParameter("numProcesso");
		  if (sNumProcesso==null) sNumProcesso="";
		  
		  String sNumSessao = req.getParameter("numSessao");
		  if (sNumSessao == null) sNumSessao = "";
		  session.setAttribute("codReq", "");

		  String sigFuncao = UsuarioFuncBeanId.getJ_sigFuncao();
		  if (sigFuncao == null) sigFuncao = "";

		  String acao = req.getParameter("acao");
		  if (acao==null)	{
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	session.removeAttribute("RemessaId");
		  	acao = " ";
		  }
		  else {
			if ("Classifica,Voltar,MostraAuto,AtualizaGuia,ImprimirGuia,MostraAI".indexOf(acao)<0)	{
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		  }
		  
		  if (acao.equals("LeAI")) {
				AutoInfracaoManualBeanId.setNumProcesso(sNumProcesso);
				GuiaDistribuicaoId.setNumSessao(sNumSessao);
				String temp = UsrLogado.getCodOrgaoAtuacao();
				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
				UsrLogado.setCodOrgaoAtuacao(temp);	
				//Bahia
				GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,AutoInfracaoManualBeanId, sigFuncao,"",UsrLogado);
				GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
				GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
			
				//Prepara a Guia de Distribui��o a partir da remessa
				RemessaId.setStatusRemessa("0");
				RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
			    RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			    RemessaId.setUsername(UsrLogado.getNomUserName());						
				RemessaId.setCodRemessa(req.getParameter("numRemessa"));
				RemessaId.setGeraGuia("D");// "D" = Guia de Distribui��o
				RemessaId.LeRemessa(UsrLogado,UsuarioFuncBeanId);
				GuiaDistribuicaoId.setAutos(RemessaId.getAutos());
				
				session.setAttribute("codReq", GuiaIncluirProcessoBeanId.getCodReq());
				req.setAttribute("AutoInfracaoManualBeanId", AutoInfracaoManualBeanId);	
				session.setAttribute("RemessaId", RemessaId);
	      }
		  
		  
		  if ( (acao.equals(" ")) || (acao.equals("Novo")) ){
				GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
				GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
				GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
				GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
				GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
				RemessaId = new RemessaBean();
		  }		
		  
          if (acao.equals("Classifica"))    
        	  GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;
          
      	  if (acao.equals("AtualizaGuia")) {
      	  	String numSessao = req.getParameter("numSessao");
      	  	if (numSessao == null) numSessao = "";
      	    GuiaDistribuicaoId.setNumSessao(numSessao);
      	    
      	    GuiaDistribuicaoId.setTipoGuia("R");//"R" = Remessa
      	    GuiaDistribuicaoId.setNumGuia(RemessaId.getCodRemessa());//N� guia de Remessa
			int inc = atualizaGuia(req,GuiaDistribuicaoId,UsrLogado) ;
			if (inc>0) {
		  		GuiaDistribuicaoId.setMsgErro("Incluidos "+inc+" Processos/Requerimentos para Guia "+GuiaDistribuicaoId.getNumGuia());
	  		    boolean existeAutosRemessa = RemessaId.getQTDAutosRemessa(RemessaId);
	  		    if (!existeAutosRemessa)   
	  		      RemessaId.gravaSitGuiaDistribuicao(RemessaId);

		  		nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp" ;
      	  	}
			else {
				GuiaDistribuicaoId.setMsgErro(GuiaDistribuicaoId.getMsgErro()+" \n Nenhum Processos/Requerimentos incluido ");
				nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp" ; 
			}	
		  	
      	  }
      	  
		  if  (acao.equals("ImprimirGuia"))  {	      	  
		  	if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
		  	nextRetorno = "/REC/GuiaPreparaImp.jsp" ;      	  
		  }
		  

		  if (acao.equals("MostraAuto"))  {
			  AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
			  if (acao.equals("MostraAuto")) { 
				  AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				  AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				  nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			  }
			  else {
				  AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				  AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
				  nextRetorno = "/REC/AutoImp.jsp" ;
			  }		
		
			  // Verifica se o Usuario logado ve Todos os Orgaos
			  String temp = UsrLogado.getCodOrgaoAtuacao();
			  if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			  AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
			  AutoInfracaoBeanId.LeRequerimento(UsrLogado);
			  UsrLogado.setCodOrgaoAtuacao(temp);	
			  req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		}		
      	  
	    if (acao.equals("MostraAI"))  {			
		  AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	  			  			
		  myAuto.setNumPlaca(req.getParameter("mostraplaca")); 
		  myAuto.setNumAutoInfracao(req.getParameter("mostranumauto"));
			
		  myAuto.LeAIDigitalizado();
			
		  req.setAttribute("AutoInfracaoBeanId",myAuto) ;
		  nextRetorno = "/REC/VisAIDig.jsp";
		}

		req.setAttribute("GuiaIncluirProcessoBeanId", GuiaIncluirProcessoBeanId);
	    // processamento de saida dos formularios
        if (acao.equals("R")) {
        	session.removeAttribute("GuiaDistribuicaoId") ;
        	session.removeAttribute("RemessaId");
        	GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
        	GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
        	GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
        	session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
        }
        else session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaPreparaManualCmd: " + ue.getMessage());
      }
     
      return nextRetorno;
   }

   public int atualizaGuia(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
	   int inc = 0 ;
	   try {	
	   	
			GuiaDistribuicaoId.setMsgErro("") ;
			Vector vErro = new Vector() ;
			GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
			JuntaBean myJunta = new JuntaBean();
			myJunta.Le_Junta(GuiaDistribuicaoId.getCodJunta(),0);
			GuiaDistribuicaoId.setSigJunta(myJunta.getSigJunta());			

			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
			RelatorBean myRelator = new RelatorBean();
			myRelator.setNumCpf(GuiaDistribuicaoId.getNumCPFRelator());
			myRelator.setCodJunta(GuiaDistribuicaoId.getCodJunta());
			myRelator.Le_Relator("CPF");			
			GuiaDistribuicaoId.setNomRelator(myRelator.getNomRelator());			
			GuiaDistribuicaoId.setCodRelator(myRelator.getCodRelator());
		
			if ( (GuiaDistribuicaoId.getCodJunta().length()==0) ||
				 (GuiaDistribuicaoId.getNumCPFRelator().length()==0)) vErro.addElement("Junta/Relator n�o selecionado. \n") ;
			// buscar os autos selecionados
			String procSelec[] = req.getParameterValues("Selecionado")	;
			int n =0 ;				
			ArrayList autosSelec = new ArrayList();			
			if (procSelec!=null)	{
				for (int i=0;  i<procSelec.length-1; i++) {					
					n = Integer.parseInt(procSelec[i]) ;	
					if ((n>=0) && (n<GuiaDistribuicaoId.getAutos().size())) { 
						autosSelec.add((AutoInfracaoBean)GuiaDistribuicaoId.getAutos().get(n));
					}
				}
				
				GuiaDistribuicaoId.setAutos(autosSelec);
			}
			else vErro.addElement("Nenhum processo selecionado.") ;
			
			if ((vErro.size()==0) && (GuiaDistribuicaoId.getAutos().size()==0)) vErro.addElement("Nenhum processo selecionado.") ;
			
			if (vErro.size()>0) GuiaDistribuicaoId.setMsgErro(vErro);
			else{	 
				GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
				inc = dao.GuiaGrava(GuiaDistribuicaoId,UsrLogado) ; 
			}		
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaPreparaManualCmd: " + ue.getMessage());
	   }
	   return inc;
   } 	
}
