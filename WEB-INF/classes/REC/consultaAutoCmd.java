package REC;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class consultaAutoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/consultaAuto.jsp" ;  
   
  public consultaAutoCmd() {
    next             =  jspPadrao;
  }

  public consultaAutoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {

  	String nextRetorno  = next ;
  	String sMensagem    = "";
    try {  
		
	    // cria os Beans de sessao, se n�o existir
    	HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
	    ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
    	if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  				
		ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
	    ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (parSis == null)	parSis = new ParamSistemaBean();
		consultaAutoBean consultaAutoId                 = (consultaAutoBean)session.getAttribute("consultaAutoId") ;
		if (consultaAutoId==null)  	consultaAutoId       = new consultaAutoBean() ;	  		    
		consultaAutoId.setAbrevSist(UsuarioFuncBeanId.getAbrevSistema());	
		consultaAutoId.setSigFuncao(req.getParameter("j_sigFuncao"));
		
	    String acao = req.getParameter("acao");  
	    if (acao==null)	{
	      session.removeAttribute("consultaAutoId") ;
		  consultaAutoId       = new consultaAutoBean() ;	  		      
	      acao = " ";
			
			if(UsuarioFuncBeanId.getCodOrgaoAtuacao().equals("258470")){
				nextRetorno = "/REC/ConsultarRequerimentoPorAuto.jsp";
				
				return nextRetorno;
			}
	    }
		else {
			if ("Classifica,ConsultaAuto,MostraAuto,imprimirInf,ImprimeAuto,RecebeNotif,ImprimeNots".indexOf(acao)<0)	{
				
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		}

		//Carrego os Beans  
		consultaAutoId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));		
		consultaAutoId.setNumCPFEdt(req.getParameter("numCpf"));
		consultaAutoId.setNumPlaca(req.getParameter("numPlaca"));
		consultaAutoId.setNumRenavam(req.getParameter("numRenavam"));
		consultaAutoId.setNumProcesso(req.getParameter("numProcesso"));
		consultaAutoId.setDatInfracaoDe(req.getParameter("De"));
		consultaAutoId.setDatInfracaoAte(req.getParameter("Ate"));
		consultaAutoId.setIndFase(req.getParameter("indFase"));
		String Sit9 = req.getParameter("Sit9");
		
		if (Sit9==null) Sit9="";
		if ("9".equals(Sit9)) consultaAutoId.setIndSituacao("9");
		else {
			String Sit1 = req.getParameter("Sit1");
			if (Sit1==null) Sit1="";
			String Sit2 = req.getParameter("Sit2");
			if (Sit2==null) Sit2="";
			String Sit3 = req.getParameter("Sit3");
			if (Sit3==null) Sit3="";
			String Sit4 = req.getParameter("Sit4");
			if (Sit4==null) Sit4="";
			consultaAutoId.setIndSituacao(Sit1+Sit2+Sit3+Sit4);
		}
		
		/*Agravo*/
		if (consultaAutoId.getSigFuncao().equals("REC0610")) consultaAutoId.setIndSituacao("5"); 
		
		consultaAutoId.setIndPago(req.getParameter("indPago"));
		consultaAutoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		if (acao.equals("Classifica")) 
			consultaAutoId.Classifica(req.getParameter("ordem"));
		if (acao.equals("ConsultaAuto")) 
		{ 
			String numProcessoOriginal=consultaAutoId.getNumProcesso();
			if (numProcessoOriginal==null)  numProcessoOriginal = "";
			
			if(!"".equals(numProcessoOriginal)){ 				
				String numprocessoA="";
				int pos =numProcessoOriginal.indexOf("A/");
				if (pos>=0)
					consultaAutoId.ConsultaAutos(UsrLogado);				
				else{
					pos=numProcessoOriginal.lastIndexOf("/");
					if (pos<0)
						numprocessoA=numProcessoOriginal+"A";
					else
    					numprocessoA=numProcessoOriginal.substring(0,pos)+"A"+numProcessoOriginal.substring((pos),numProcessoOriginal.length());
					consultaAutoId.setNumProcesso(numprocessoA);
					consultaAutoId.ConsultaAutos(UsrLogado);
					if (consultaAutoId.getAutos().size()>0) 
						req.setAttribute("processoA","EXISTE PROCESSO COM MESMO N�MERO + 'A'.");					
					
					consultaAutoId.setNumProcesso(numProcessoOriginal);
					consultaAutoId.ConsultaAutos(UsrLogado);
				}
			}else
				consultaAutoId.ConsultaAutos(UsrLogado);			
			
			if ("1".equals(UsuarioFuncBeanId.getIndRecebNot())==true && consultaAutoId.getSigFuncao().equals("REC0600"))
			{		
				consultaAutoId.ConsultaAutosPostos(UsrLogado);		 
			}
			if (consultaAutoId.getAutos().size()==0) { 
				if ("REC0680".equals(consultaAutoId.getSigFuncao())) req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
				else req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");
			}
		}		 
		
		if (acao.equals("Novo"))  		  consultaAutoId = new consultaAutoBean() ;
		if ((acao.equals("MostraAuto")) || (acao.equals("imprimirInf")) )  {
			
			AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;
			
			
			if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
			if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
			if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
			if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
			if (acao.equals("MostraAuto")) { 
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca"));				
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));		
				AutoInfracaoBeanId.setAutoAntigo(req.getParameter("mostraAutoAntigo"));
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			}
			else {
				
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));
				AutoInfracaoBeanId.LeFotoDigitalizada() ; 
				nextRetorno = "/REC/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999");
			AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
			String codInfracaoAdm = ParamOrgaoId.getParamOrgao("COD_INFRACAO_ADM","69200,99800","10");
			if(codInfracaoAdm.indexOf(AutoInfracaoBeanId.getInfracao().getCodInfracao())>=0)
				AutoInfracaoBeanId.setTemAIDigitalizado(true);
  			String codAgente=parSis.getParamSist("CODIGO_AGENTE_PALM");
  			if  ( (codAgente.indexOf(AutoInfracaoBeanId.getCodAgente())>=0) &&
  				  (AutoInfracaoBeanId.getCodAgente().length()==2)
  				)
  				AutoInfracaoBeanId.setTemAIDigitalizado(true);  				
			if (temp.equals("258650"))
			{
				if (AutoInfracaoBeanId.getIdentOrgao().trim().length()>0)
				{
					/*Palm - Radar*/
					if ( (AutoInfracaoBeanId.getIdentOrgao().substring(1,2).equals("1")) 
					   )
						AutoInfracaoBeanId.setTemAIDigitalizado(true);
				}
			}			
			UsrLogado.setCodOrgaoAtuacao(temp);	
			AutoInfracaoBeanId.LeRequerimento(UsrLogado);  
			//AutoInfracaoBeanId.LeHistorico(UsrLogado);     
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		}
				
		if  (acao.equals("ImprimeAuto"))  {		
			if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");
			nextRetorno = "/REC/consultaAutoImp.jsp" ;
		}	
		
		if  (acao.equals("ImprimeNots"))  {
			if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
			else {
				processaImprimeNotif(consultaAutoId,ParamOrgaoId,UsrLogado);
				nextRetorno = "/REC/ImprimeNotificacoes.jsp" ;
			}			
		}		
			
		if  (acao.equals("RecebNotif"))  {				
			if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
			else {
				consultaAutoId.setMsgErro("");
				processaRecebeNotif(consultaAutoId,ParamOrgaoId,UsrLogado);
				if (consultaAutoId.getMsgErro().length()==0) { 
					consultaAutoId.setMsgErro("RECEBIDOS "+consultaAutoId.getAutos().size()+" AUTOS.");
					consultaAutoId.setMsgOk("S");
				}				
			}			
		}			
							
		if (acao.equals("R"))	{
			nextRetorno ="" ;
			consultaAutoId       = new consultaAutoBean() ;	  				
		    session.removeAttribute("consultaAutoId");
		}
		else
			session.setAttribute("consultaAutoId",consultaAutoId) ;
    }
    catch (Exception ue) {
	      throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
    }
	return nextRetorno;
  }  
  
	public void processaImprimeNotif(consultaAutoBean consultaAutoId,ParamOrgBean ParamOrgaoId,
		ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
		String sMensagem="";
		try {
  			consultaAutoId.setMsgErro("IMPRESSOS "+consultaAutoId.getAutos().size()+" AUTOS.");	
  			//Seta o c�digo da Notifica��o para passar para o Notificacao.jsp
			Iterator it  = consultaAutoId.getAutos().iterator() ;
			List notifs  = new ArrayList() ;		
			int posNot = 0 ;

			while (it.hasNext()) {
				REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();				
				not   = (REC.AutoInfracaoBean)it.next() ;	
				// temos que ler todos os dados da notificacao e atualizar o objetio na lista
				/*Org�o de Autua��o-Armazenando Org�o*/
				String temp = UsrLogado.getCodOrgaoAtuacao();				
				UsrLogado.setCodOrgaoAtuacao("999999");
				not.LeAutoInfracao(UsrLogado);
				consultaAutoId.getAutos().set(posNot,not);
		  		InstrNotBean NotifBeanId = new InstrNotBean();
				ACSS.UsuarioBean UsrLogadoRec       = new ACSS.UsuarioBean() ;	
			    ParamOrgBean ParamOrgaoRecId        = new ParamOrgBean() ;
		  		UsrLogadoRec.setCodOrgaoAtuacao(not.getCodOrgao());
				ParamOrgaoRecId.PreparaParam(UsrLogadoRec);			  		
			    
		  		not.LeHistorico(UsrLogado);				
 				NotifBeanId.setCodNotificacao(not.getCodNotificacao(ParamOrgaoId));  	
				NotifBeanId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
				NotifBeanId.setTxtEnderecoRecurso(ParamOrgaoRecId.getParamOrgao("ENDERECO_RECURSO"));				
 				NotifBeanId.LeNotificacoes();
				not.LeFotoDigitalizada(); 
				notifs.add(NotifBeanId);
				UsrLogado.setCodOrgaoAtuacao(temp);				
				posNot++;
			}
			consultaAutoId.setNotifs(notifs);
		}
		catch (Exception ue) {
			  throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
		}
	} 	
	public void processaRecebeNotif(consultaAutoBean consultaAutoId,ParamOrgBean ParamOrgaoId,
		ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
		String sMensagem="";		
		try {	
			//Seta o c�digo da Notifica��o para passar para o Notificacao.jsp
			Iterator it  = consultaAutoId.getAutos().iterator() ;	
			DaoBroker dao = DaoBrokerFactory.getInstance();
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
			HistoricoBean myHis = new HistoricoBean() ;	
			String proxStatus   = "";
			String codEvento    = "204";
			Vector vErro = new Vector();
			
			while (it.hasNext()) {
				myAuto   = (REC.AutoInfracaoBean)it.next() ;

				if (myAuto.getDatStatus().length()==0) myAuto.setDatStatus(sys.Util.formatedToday().substring(0,10)); 				

				myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
				myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
				myHis.setNumProcesso(myAuto.getNumProcesso());
				if (myAuto.getCodStatus().length()==1) {
					proxStatus = "4";	
					codEvento    = "204";				
				}
				else {
					proxStatus = "14";
					codEvento    = "324";
				}
				myHis.setCodStatus(proxStatus);  
				myHis.setDatStatus(sys.Util.formatedToday().substring(0,10));
				myHis.setCodEvento(codEvento);
				myHis.setCodOrigemEvento("100");
				myHis.setNomUserName(UsrLogado.getNomUserName());
				myHis.setCodOrgaoLotacao(UsrLogado.getCodOrgaoAtuacao());
				myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
				myHis.setTxtComplemento01(sys.Util.formatedToday().substring(0,10));	
				myHis.setTxtComplemento02("34");
				dao.atualizarAuto204(myAuto,myHis,UsrLogado) ; 
				if (myAuto.getMsgErro().length()>0) 
					vErro.addElement("Auto: "+myAuto.getNumAutoInfracao()+" - "+myAuto.getMsgErro()+" /n");				
			}
			if (vErro.size()>0) consultaAutoId.setMsgErro(vErro);
		}
		catch (Exception ue) {
			  throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
		}
	} 	

	
}
