package REC;


import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sys.ServiceLocator;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ParamUFBean extends sys.HtmlPopupBean {
	private static final String MYABREVSIST = "REC";
	private String codUF;
	private String codParametroUF;
	
	private String nomParam;
	private String nomDescricao;
	private String valParametro;

	private List nomParamUF;
	private List valParamUF;


	public ParamUFBean() {
		super();
		setTabela("TSMI_PARAM_UF");
		setPopupWidth(35);
		codUF = "";
		codParametroUF = "";
		nomParam = "";
		nomDescricao = "";
		valParametro = "";

		nomParamUF = new ArrayList();
		valParamUF = new ArrayList();
	}

	public void setCodUF(String codUF) {
		this.codUF = codUF;
		if (codUF == null)
			this.codUF = "";
	}
	public String getCodUF() {
		return this.codUF;
	}

	public void setCodParametroUF(String codParametroUF) {
		this.codParametroUF = codParametroUF;
		if (codParametroUF == null)
			this.codParametroUF = "";
	}
	public String getCodParametroUF() {
		return this.codParametroUF;
	}

	public void setNomParam(String nomParam) {
		this.nomParam = nomParam;
		if (nomParam == null)
			this.nomParam = "";
	}
	public String getNomParam() {
		return this.nomParam;
	}
	

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setValParametro(String valParametro) {
		this.valParametro = valParametro;
		if (valParametro == null)
			this.valParametro = "";
	}
	public String getValParametro() {
		return this.valParametro;
	}

	public void Le_ParamUF() {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if (daoTabela.ParamUFLeBean(this) == false) {
				this.codParametroUF = "0";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codParametroUF = "0";
			setPkid("0");
			setMsgErro(
				"Erro na leitura de Parametros da UF: " + e.getMessage());
		} // fim do catch 
	}

	//--------------------------------------------------------------------------
	public void setNomParamUF(List nomParamUF) {
		this.nomParamUF = nomParamUF;
	}

	public List getNomParamUF() {
		return this.nomParamUF;
	}
	public String getParamUF(String myNomParam) throws sys.BeanException {
		try {
			 return getParamUF(myNomParam,"","7") ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public String getParamUF(String myNomParam,String Padrao,String grupo) throws sys.BeanException {		
		Connection conn = null;				
		try {					
			String myVal = "";
			String re = "";
			Iterator itx = getNomParamUF().iterator();
			int i = 0;		
			boolean achou = false ;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomParam)) {						
					myVal = getValParamUF(i);
					achou = true ;
					break;
				}
				i++;
			}
			if (!achou){				     	     
				conn = ServiceLocator.getInstance().getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();					       
				this.setCodParametroUF("0");
				this.setNomParam(myNomParam);
				this.setNomDescricao(myNomParam);									
				this.setValParametro(Padrao);
				this.setCodUF(this.getCodUF());
				REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();	
				daoTabela.ParamInsertUF(this,stmt) ;	
				stmt.close();
				conn.commit();
				this.getNomParamUF().add(myNomParam);
				this.getValParamUF().add(Padrao);							   
			}				
			return myVal;					
		}		
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					ServiceLocator.getInstance().setReleaseConnection(conn);
				}
				catch (Exception ey) {		}
			}
		}
	}
//--------------------------------------------------------------------------
	public void setValParamUF(List valParamUF) {
		this.valParamUF = valParamUF;
	}
	public void setValParamUF(String myNomParam,String myValParam) {
		Iterator itx = getNomParamUF().iterator();
		int i = 0;
		String re = "";
		while (itx.hasNext()) {
			re = (String) itx.next();
			if (re.equals(myNomParam)) {
				getValParamUF().set(i,myValParam);
				break;
			}
			i++;
		}
	}

	public List getValParamUF() {
		return this.valParamUF;
	}

	public String getValParamUF(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValParamUF().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}

	//--------------------------------------------------------------------------
	public void PreparaParam(String CodUF)
		throws sys.BeanException {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.PreparaParam(this, CodUF);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}

}