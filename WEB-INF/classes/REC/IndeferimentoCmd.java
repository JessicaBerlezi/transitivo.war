package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class IndeferimentoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/Indeferimento.jsp" ;  
   
  public IndeferimentoCmd() {
    next             =  jspPadrao;
  }

  public IndeferimentoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {  
	    AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;

    	if (AutoInfracaoBeanId==null)  	{
	    	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
    		if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
	    	if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
    		if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
    		if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
	    	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
    	}
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		
			//cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
		String sigFuncao = 	req.getParameter("j_sigFuncao");
		if(sigFuncao==null)   sigFuncao = " ";

	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {

	    }
	    if  (acao.equals("CadastrarIndeferimento"))  {
			AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;				
			nextRetorno = processaIndeferimento(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado, sigFuncao);
	    }
	    
		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	 		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("IndeferimentoCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }


private String processaIndeferimento(HttpServletRequest req,ParamOrgBean ParamOrgaoId,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado, String sigFuncao) throws sys.CommandException { 
	  String nextRetorno=jspPadrao;		
			
	  try {
		  myAuto.setMsgErro("") ;
		  Vector vErro = new Vector() ;
		  String datIndeferimento = req.getParameter("datIndeferimento");
		  if (datIndeferimento==null) datIndeferimento = "";	 			
		  //validar data de entrada
		  if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),datIndeferimento)>0)
			   vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;	  
/*			
			String txtMotivo = req.getParameter("txtMotivo");
				  if (txtMotivo==null) txtMotivo = "";					 	
			  
		  if (txtMotivo.length()==0) vErro.addElement("Motivo n�o preenchido.\n") ;
			  
*/
		  String indOper = "DI";
		  if (myAuto.getCodStatus().length()>1) {
		  	 if ("30,31,35,36,38".indexOf(myAuto.getCodStatus())>=0) indOper = "2I";
		  	 else indOper = "1I";	
		  } 			 
		  if (vErro.size()>0) {
			   myAuto.setMsgErro(vErro) ;
			   nextRetorno = this.next ;
		  }
		  else {
			  REC.RequerimentoBean myReq = new REC.RequerimentoBean() ;						
			  myAuto.setMsgErro(atualizarAutoIndeferimento(req,myAuto,ParamOrgaoId,UsrLogado,myReq,datIndeferimento,indOper)) ;	
			  if (myAuto.getMsgErro().length()==0) {					
				  if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) { 	
					  myAuto.setMsgErro("Indeferimento interposto com Requerimento:"+myReq.getNumRequerimento()) ;
					  myAuto.setMsgOk("S") ;
				  }
				  else { 		
  					if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","R","2").indexOf("R")>=0)
  					    nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
  				    else
  						nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;	
  				}			
			  }								
		  }
	  }
	  catch (Exception e){
		  throw new sys.CommandException("AbrirRecursoDPCmdb: " + e.getMessage());
	  }
	  return nextRetorno  ;
  }


private Vector atualizarAutoIndeferimento(HttpServletRequest req,AutoInfracaoBean myAuto,ParamOrgBean myParam,ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,String datIndeferimento,String indOper) throws sys.CommandException { 
	Vector vErro = new Vector() ;
	try { 	
		// preparar o Bean myHist (sem processo e num requerimento
			
		myAuto.setDatStatus(datIndeferimento);		
		myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
		myReq.setCodTipoSolic(indOper);
		
		myReq.setDatRequerimento(datIndeferimento);
		// preparar a data do Processo - se nao existir
		if (myAuto.getDatProcesso().length()==0) myAuto.setDatProcesso(myReq.getDatRequerimento());
		myReq.setCodStatusRequerimento("0");
		myReq.setNomUserNameDP(myUsrLog.getNomUserName());
		myReq.setCodOrgaoLotacaoDP(myUsrLog.getOrgao().getCodOrgao());
		myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
		// preparar Bean de Historico
		HistoricoBean myHis = new HistoricoBean() ;
		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
		myHis.setNumProcesso(myAuto.getNumProcesso());
		myHis.setCodOrgao(myAuto.getCodOrgao());
		myHis.setCodStatus(myAuto.getCodStatus());  
		myHis.setDatStatus(myAuto.getDatStatus());
			
		myHis.setCodOrigemEvento("100");
		if (myReq.getCodTipoSolic().equals("DI")) myHis.setCodEvento("236");
		if (myReq.getCodTipoSolic().equals("1I")) myHis.setCodEvento("356");
		if (myReq.getCodTipoSolic().equals("2I")) myHis.setCodEvento("364");
					
		myHis.setNomUserName(myUsrLog.getNomUserName());
		myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
		myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
//		myHis.setTxtComplemento01(myReq.getNumRequerimento());
		// complemento 01 - Num Requerimento no DAO
		myHis.setTxtComplemento02(myReq.getCodTipoSolic());
			
		myHis.setTxtComplemento03(myReq.getDatRequerimento());
		myHis.setTxtComplemento04(myAuto.getProprietario().getNomResponsavel());
		myHis.setTxtComplemento05(myAuto.getProprietario().getIndCpfCnpj());				
		myHis.setTxtComplemento06(myAuto.getProprietario().getNumCpfCnpj());
		myHis.setTxtComplemento08("0");
		
	   DaoBroker dao = DaoBrokerFactory.getInstance();
	   req.setAttribute("HistoricoBeanId",myHis) ;
	   req.setAttribute("ExisteProcesso",("".equals(myAuto.getNumProcesso()) ? "S" : "N")) ;		    
	   dao.atualizarAuto206(myAuto,myParam,myReq,myHis,myUsrLog) ;		    

	}// fim do try
	catch (Exception ex) {
		throw new sys.CommandException(ex.getMessage());
	}
	return vErro ;
}

  
}