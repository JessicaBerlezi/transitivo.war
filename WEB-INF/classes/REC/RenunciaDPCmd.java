package REC;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
public class RenunciaDPCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/RenunciaDP.jsp" ;  
   
  public RenunciaDPCmd() {
    next = jspPadrao;
  }

  public RenunciaDPCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
    	   
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    // cria os Beans, se n�o existir

		ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

	    AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";	    
		if  (acao.equals("RenunciaDP"))  {
			REC.RequerimentoBean myReq = new REC.RequerimentoBean() ;						
			RenunciaDP(req,AutoInfracaoBeanId,ParamOrgaoId,UsrLogado,myReq) ;	
			if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
				if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) { 	
					AutoInfracaoBeanId.setMsgErro("Ren�ncia de Defesa Pr�via interposta com Requerimento:"+myReq.getNumRequerimento()) ;
					AutoInfracaoBeanId.setMsgOk("S") ;
				}
				else { 		
  					if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","R","2").indexOf("R")>=0){
  						AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
						String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setNomeImagem(nomeImagem);

                        String alturaImageGra = "80";
                        AbrirRecBeanId.setAlturaImageGra(alturaImageGra);
                        
                        String larguraImageGra = "175";
                        AbrirRecBeanId.setLarguraImageGra(larguraImageGra);
                        
                        String alturaImagePeq = "80";
                        AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);
                        
                        String larguraImagePeq = "175";
                        AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);
                        
                        String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setObservacao(obs);
                        
                        String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setSigProtocolo(siglaProt);

   
					    nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
					    req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	
			
  					}
  				    else
  						nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;	
  				}				
			}			    	
		}				

	    
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;	    		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("RenunciaDPCmd1: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
  private void RenunciaDP(HttpServletRequest req,AutoInfracaoBean myAuto,ParamOrgBean myParam,
	  ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq) throws sys.CommandException { 
	  try {
		  myAuto.setMsgErro("") ;
		  myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao());
		  myReq.setCodResponsavelDP(myAuto.getProprietario().getCodResponsavel());
		  myReq.setCodTipoSolic("RD");
		  myReq.setDatRequerimento(sys.Util.formatedToday().substring(0,10));
		  myReq.setCodStatusRequerimento("0");
		  myReq.setNomUserNameDP(myUsrLog.getNomUserName());
		  myReq.setCodOrgaoLotacaoDP(myUsrLog.getOrgao().getCodOrgao());
		  myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
		
		  // preparar Bean de Historico
		  HistoricoBean myHis = new HistoricoBean() ;
		  myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
		  myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
		  myHis.setNumProcesso(myAuto.getNumProcesso());
		  myHis.setCodOrgao(myAuto.getCodOrgao());
		  myHis.setCodStatus(myAuto.getCodStatus());  
		  myHis.setDatStatus(myAuto.getDatStatus());
		  myHis.setCodEvento("238");
		  myHis.setCodOrigemEvento("100");		
		  myHis.setNomUserName(myUsrLog.getNomUserName());
		  myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
		  myHis.setDatProc(sys.Util.formatedToday().substring(0,10));	  		
		  // complemento 01 - Num Requerimento no DAO
		  myHis.setTxtComplemento02(myReq.getCodTipoSolic());
		  myHis.setTxtComplemento03(myReq.getDatRequerimento());
		  myHis.setTxtComplemento04(myAuto.getProprietario().getNomResponsavel());									
		  myHis.setTxtComplemento05(myAuto.getProprietario().getIndCpfCnpj());									
		  myHis.setTxtComplemento06(myAuto.getProprietario().getNumCpfCnpj());
		  myHis.setTxtComplemento07(req.getParameter("txtMotivo"));
	      /*DPWEB*/
		  myHis.setTxtComplemento08("0");		   
		  DaoBroker dao = DaoBrokerFactory.getInstance();
		  req.setAttribute("HistoricoBeanId",myHis) ;
		  req.setAttribute("ExisteProcesso",("".equals(myAuto.getNumProcesso()) ? "S" : "N")) ;
		  dao.atualizarAuto206(myAuto,myParam,myReq,myHis,myUsrLog) ;		    
	  }
	  catch (Exception e){
		  throw new sys.CommandException("RenunciaDPCmd: " + e.getMessage());
	  }
	  return  ;
	}
  
}
