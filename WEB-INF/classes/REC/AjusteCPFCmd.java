package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

 

/**
* <b>Title:</b>      Recurso - Ajuste CPF<br>
* <b>Description:</b>Transacao 413:Ajuste CPF<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/
public class AjusteCPFCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteCPF.jsp";

	public AjusteCPFCmd() {
		next = jspPadrao;
	}

	public AjusteCPFCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
		  nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;

			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
	
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";

			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.setMsgOk("S");					
			}
			
			if (acao.equals("ConfirmaAjuste")) { 	
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {		
				  String indResp = req.getParameter("indRespPontos") ;
				  if (indResp==null) indResp="1";
				  if ("123".indexOf(indResp)<0) indResp="1";			  
				  AutoInfracaoBeanId.getProprietario().setIndCpfCnpj(req.getParameter("IndCPF"));
				  
				  if ("1".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()))
				  		AutoInfracaoBeanId.getProprietario().setNumCpfCnpjEdt(req.getParameter("numCPF"));
				  else AutoInfracaoBeanId.getProprietario().setNumCpfCnpjEdt(req.getParameter("numCNPJ"));
				  DaoBroker dao = DaoBrokerFactory.getInstance();
				  dao.ajustarCPF(AutoInfracaoBeanId,UsrLogado,indResp);				  
				  if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
					   AutoInfracaoBeanId.setMsgErro("Ajuste do CPF realizado com sucesso.") ;
					}
				}
			}
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId);
			req.setAttribute("UsrLogado",UsrLogado) ;

		}
		catch (Exception ue) {
			  throw new sys.CommandException("AjusteCPFCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	  }

}