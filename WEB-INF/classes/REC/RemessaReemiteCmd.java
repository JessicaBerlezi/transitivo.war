package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RemessaReemiteCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/RemessaReemite.jsp" ;  
   
   public RemessaReemiteCmd() {
      next = jspPadrao;
   }

   public RemessaReemiteCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado  = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  
          	UsrLogado = new ACSS.UsuarioBean() ;
          
          ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null) 
          	UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
          
		  ParamOrgBean ParamOrgaoId= (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null) 
		  	ParamOrgaoId = new ParamOrgBean() ;
		  
      	  RemessaBean RemessaId = (RemessaBean)session.getAttribute("RemessaId") ;
      	  if (RemessaId==null) 
      	  	RemessaId = new RemessaBean() ;
		  RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
      		
		  //Carrego os Beans		
      	  RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
		  RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  RemessaId.setUsername(UsrLogado.getNomUserName());	
		  String indRecurso= UsuarioFuncBeanId.getJ_sigFuncao();
		  
		  String dataIni = req.getParameter("De");
		  if (dataIni == null) dataIni = "";
		  RemessaId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  if (dataFim == null) dataFim = "";
		  RemessaId.setDataFim(dataFim);

		  /*Carrega a propriedade que indica o Status da Remessa*/
		  //Guia de Remessa
		  if ( ("REC0127".equals(indRecurso)) || ("REC0128".equals(indRecurso)) )
		    RemessaId.setStatusRemessa("0");//**> Remessa Enviada <**
		  else if ( ("REC0202".equals(indRecurso)) || ("REC0312".equals(indRecurso)) )
		    RemessaId.setStatusRemessa("1");//**> Remessa Recebida <**

		  //Guia de Remessa CEDOC
		  if ( ("REC0135".equals(indRecurso)) || ("REC0136".equals(indRecurso)) )
		    RemessaId.setStatusRemessa("0");//**> Remessa Enviada <**
		  else if ( ("REC0206".equals(indRecurso)) || ("REC0314".equals(indRecurso)) )
		    RemessaId.setStatusRemessa("1");//**> Remessa Recebida <**

		  
		  String acao = req.getParameter("acao");
		  if (acao==null)	
		  {
			RemessaId  = new RemessaBean() ;
			RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
			RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			RemessaId.setUsername(UsrLogado.getNomUserName());		
			RemessaId.setMsgErro("");	
			
			/*Carrega a propriedade que indica o Status da Remessa*/
			//Guia de Remessa
			if ( ("REC0127".equals(indRecurso)) || ("REC0128".equals(indRecurso)) )
			  RemessaId.setStatusRemessa("0");//**> Remessa Enviada <**
			else if ( ("REC0202".equals(indRecurso)) || ("REC0312".equals(indRecurso)) )
			  RemessaId.setStatusRemessa("1");//**> Remessa Recebida <**
				  
			//Guia de Remessa CEDOC
			if ( ("REC0135".equals(indRecurso)) || ("REC0136".equals(indRecurso)) )
			    RemessaId.setStatusRemessa("0");//**> Remessa Enviada <**
			else if ( ("REC0206".equals(indRecurso)) || ("REC0314".equals(indRecurso)) )
			    RemessaId.setStatusRemessa("1");//**> Remessa Recebida <**

		  	session.removeAttribute("RemessaId") ;
		  	acao = " ";
		  }
		  else 
		  {
			if ("LeRemessa,Classifica,MostraAuto,imprimirInf,ImprimirRemessa".indexOf(acao)<0)	{
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		  }
		  

		  if (acao.equals("LeRemessa")) 
		  {
		    RemessaId = new RemessaBean() ;		
			RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));		           	  
		    RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
		    RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    RemessaId.setUsername(UsrLogado.getNomUserName());						
			RemessaId.setCodRemessa(req.getParameter("numRemessa"));
			RemessaId.LeRemessa(UsrLogado,UsuarioFuncBeanId);
		    if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO PARA ENVIAR AOS RELATORES") ;
			nextRetorno = "/REC/RemessaMostra.jsp";
		  }	  
		  
          if (acao.equals("Classifica")) 
          { 
          	RemessaId.Classifica(req.getParameter("ordem"))	;
          	nextRetorno = "/REC/RemessaMostra.jsp";
      	  }

		  if  (acao.equals("ImprimirRemessa"))  
		  {
			RemessaId.setJ_sigFuncao(UsuarioFuncBeanId.getJ_sigFuncao(),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		  	if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");
		  	if("S".equals(req.getParameter("todos")))
				req.setAttribute("imprimeAI", "S");
			else
				req.setAttribute("imprimeAI", "N");
		  	
            //Fazer iterator e pegar todos os processos
			Iterator it = RemessaId.getAutos().iterator() ;					
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
			List AIDigs = new ArrayList() ;
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				myAuto.setMsgErro("");
				myAuto.LeAIDigitalizado();
				AIDigs.add(myAuto);
			}
			RemessaId.setAutos(AIDigs);
			session.setAttribute("RemessaId",RemessaId);
			req.setAttribute("AutoInfracaoBeanId",myAuto) ;
		  	nextRetorno = "/REC/RemessaPreparaImp.jsp" ;      	  
		  }
      	  if (acao.equals("MostraAuto"))  
      	  {
        	AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
			if (acao.equals("MostraAuto")) { 
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			}
			else {
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
				nextRetorno = "/REC/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
			UsrLogado.setCodOrgaoAtuacao(temp);	
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
      	  }		
          // processamento de saida dos formularios
          if (acao.equals("R")) session.removeAttribute("RemessaId") ;
      	  else session.setAttribute("RemessaId",RemessaId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("RemessaReemiteCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }
}
