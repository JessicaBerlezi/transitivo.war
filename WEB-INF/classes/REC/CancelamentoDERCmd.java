package REC;

import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BiometriaCmd;
import sys.Command;
import sys.Util;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - CancelaAutoCmd<br>
* <b>Description:</b>  Cancela Autos de Infra��o <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello
* @version 1.0
*/

  
public class CancelamentoDERCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/CancelamentoDER.jsp" ;  
   
  public CancelamentoDERCmd() {
    next = jspPadrao;
  }

  public CancelamentoDERCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    HttpSession session   = req.getSession() ;
	    
        //cria os Beans, se n�o existir
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			

	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	    	  			
	    AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)   	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  				   

		CancelamentoBean CancelamentoId   = (CancelamentoBean)req.getAttribute("CancelamentoId") ;
		if (CancelamentoId==null)   	CancelamentoId = new CancelamentoBean() ;
		CancelamentoId.setSigFuncao(req.getParameter("j_sigFuncao"));
		
		ACSS.ParamSistemaBean ParamSistemaBeanId = (ACSS.ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");	  			  				   
		
	    String acao = req.getParameter("acao");  
	    if(acao==null){
	    	acao = " ";	    
	    }
	    
		nextRetorno="/REC/CancelamentoDER.jsp" ;

		if  (acao.equals("CancelaAuto"))  {
 			    String txtMotivo = req.getParameter("txtMotivo");
				nextRetorno = processaAjuste(req,AutoInfracaoBeanId,UsrLogado,CancelamentoId,txtMotivo);
		}		
	  req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		req.setAttribute("CancelamentoId",CancelamentoId) ;
	}
	catch (Exception ue) {
		  throw new sys.CommandException("CancelaAutoCmd: " + ue.getMessage());
	}
	return nextRetorno;
  }
  private String processaAjuste(HttpServletRequest req,AutoInfracaoBean myAuto,
  	ACSS.UsuarioBean UsrLogado,CancelamentoBean CancelamentoId, String txtMotivo) throws sys.CommandException { 
	String nextRetorno = next ;
	try {		
		myAuto.setMsgErro("") ;
		Vector vErro = new Vector() ;	
		//Valida o C�digo do Cancelamento
		String codMotivo = req.getParameter("codMotivo");
		if (codMotivo==null) codMotivo="";
		if( codMotivo.compareTo("") == 0)
			vErro.addElement("C�digo do Motivo n�o Informado. \n") ;
		String data = req.getParameter("Data");
		if (data==null) data = "" ;
		
		String indOper = req.getParameter("IndicOper");
				if (indOper==null) indOper = "" ;
		
		String numProcesso = req.getParameter("ProcInv");
		if (numProcesso==null) numProcesso = "";
		
		//validar data de Notificacao		
		if (data.length()==0) vErro.addElement("Data do Cancelamento n�o preenchida. \n") ;
		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),data)>0) vErro.addElement("Data da Notifica��o n�o pode ser superior a hoje. \n") ;
		if (vErro.size()==0) {
			HistoricoBean myHis = new HistoricoBean();
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(numProcesso);
			myHis.setCodStatus("7");  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodEvento("418");
			myHis.setCodOrigemEvento("100");		
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			myHis.setTxtComplemento01("7");
			myHis.setTxtComplemento02(data);			
			myHis.setTxtComplemento03(txtMotivo);
			myHis.setTxtComplemento04(" ");
			myHis.setTxtComplemento06(myAuto.getCodStatus());
			
			
			String linhaArq="";
			String Usuario = "";
			Usuario= sys.Util.rPad(UsrLogado.getNomUserName()," ",20) +"AA";			
			
			linhaArq= sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+ sys.Util.rPad(myAuto.getNumPlaca()," ",7) + "0000000"+Util.lPad(codMotivo,"0",3) +
			          sys.Util.rPad(numProcesso," ",20) + Usuario.substring(0,8) + indOper + sys.Util.rPad(" "," ",640)+ sys.Util.rPad(txtMotivo," ",51) + sys.Util.rPad(" "," ",9) ;			
			
			ROB.DaoBroker dao = ROB.DaoBroker.getInstance();			
			String resultado = "";			
			BRK.TransacaoBRKR transBRKDOL = new BRK.TransacaoBRKR();						
			resultado = dao.TransacaoIsoladas(Usuario.substring(0,20),sys.Util.lPad(UsrLogado.getOrgao().getCodOrgao(),"0",6),"418","5",sys.Util.lPad(UsrLogado.getCodOrgaoAtuacao(),"0",6),linhaArq,null,transBRKDOL,"","P59");
			
			if (resultado.substring(0,3).equals("000")){			  			
				myAuto.setMsgOk("S") ;										
				myAuto.setMsgErro("Cancelamento/Suspens�o efetuado(a) para o Auto: "+myAuto.getNumAutoInfracao()+" em "+myAuto.getDatStatus() + "\nStatus: " + myAuto.getCodStatus() + " - "+ myAuto.getNomStatus()) ;
				myAuto.setDatStatus(data);  
				myAuto.setCodStatus("7");	
				/*Gravar Historico*/
				myHis.gravarHistorico(myHis);
			}
			else{
				REG.Dao daoreg = REG.Dao.getInstance();
				String Erro="";
				Erro= daoreg.verificaTabEvento(resultado.substring(0,3));
				myAuto.setMsgOk("S") ;										
				vErro.addElement("Erro n�. "+ resultado.substring(0,3) +" : " +  Erro) ;
				myAuto.setMsgErro(vErro);	
			}
		}
		  
		  else{myAuto.setMsgErro(vErro);}
	}
	catch (Exception e){
		throw new sys.CommandException("CancelaAutoCmd: " + e.getMessage());
	}
	return nextRetorno  ;
  }
}
