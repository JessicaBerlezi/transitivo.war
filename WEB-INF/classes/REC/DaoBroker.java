package REC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ACSS.Ata;
import ACSS.UsuarioBean;

import sys.DaoException;
import sys.ServiceLocatorException;

/**
 * @author glaucio
 *
 *  To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface DaoBroker {
        
    /*****************************************
     Transa��es de acesso ao Broker
     *****************************************/
    public abstract boolean LeAutoInfracao(AutoInfracaoBean myInf, String tpchave,
            ACSS.UsuarioBean myUsrLog) throws DaoException;

    /**
     *-----------------------------------------------------------
     * Le Historico - Transa��es 044
     *-----------------------------------------------------------
     */
    public abstract boolean LeHistorico(AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException;

    /**
     *-----------------------------------------------------------
     * Le Requerimento - Transa��es 045
     *-----------------------------------------------------------
     */
    public abstract boolean LeRequerimento(AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException;
    
	public abstract void GravarRequerimento(RequerimentoBean myReq, Connection conn) throws DaoException;
	public abstract void GravarHistorico(HistoricoBean myHist, Connection conn) throws DaoException, ServiceLocatorException, SQLException ;
	public abstract void GravarHistorico(AutoInfracaoBean myInf) throws DaoException, ServiceLocatorException, SQLException ;
	public abstract void gravarHistorico(HistoricoBean myHist) throws DaoException, ServiceLocatorException, SQLException ;
	
    /******************************************
     ConsultaAuto e carrega no Bean - Transa��es 048
     ******************************************/
    public abstract boolean ConsultaAutos(consultaAutoBean myConsult, UsuarioBean myUsuario) throws DaoException;
    public abstract String ConsultaCodRequerimento(AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException;
    public abstract boolean StatusAuto(consultaAutoBean myConsult, UsuarioBean myUsuario) throws DaoException;    
    

    /**
     /******************************************
     LeResponsavel - Transa��es 051
     ******************************************/
    public abstract boolean LeResponsavel(ResponsavelBean myResp, String tpPesq)
            throws DaoException;
    
    
    /******************************************
     Transa��es 204, 324
     ******************************************/
    public abstract void atualizarAuto204(AutoInfracaoBean myAuto, HistoricoBean myHis,UsuarioBean myUsuario)
            throws DaoException;

    /**
     *-----------------------------------------------------------
     * Abrir Defesa Previa - 206,326,340
     *-----------------------------------------------------------
     */
    public abstract void atualizarAuto206(AutoInfracaoBean myAuto, ParamOrgBean myParam,
            RequerimentoBean myReq, HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException;
    
    public abstract void atualizarAutoProcessos(AutoInfracaoBean myAuto,
            RequerimentoBean myReq, HistoricoBean myHis,  Ata ata) throws DaoException;
    
    
    public abstract void atualizarAuto206(AutoInfracaoBean myAuto, ParamOrgBean myParam,
            RequerimentoBean myReq, HistoricoBean myHis,UsuarioBean myUsuario, Ata ata) throws DaoException;
    
    public abstract void atualizarProcesso(AutoInfracaoBean myAuto, ParamOrgBean myParam,
            RequerimentoBean myReq, HistoricoBean myHis,UsuarioBean myUsuario, Ata ata) throws DaoException;
    
    
    public abstract void processaPenalidadeDER(AutoInfracaoBean myAuto,ACSS.UsuarioBean myUsrLog) throws DaoException;
    
    
    /**
     *-----------------------------------------------------------
     * Atualiza Requerimento com Parecer Juridico e grava Historico - Transa��es 207 
     *-----------------------------------------------------------
     */
    public abstract void atualizarAuto207(AutoInfracaoBean myAuto, RequerimentoBean myReq,
            HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException;

    /**
     *-----------------------------------------------------------
     * Atualiza Junta e Relator e grava Historico - Transa��es 208, 328, 335 
     *-----------------------------------------------------------
     */
    public abstract void atualizarAuto208(AutoInfracaoBean myAuto, RequerimentoBean myReq,
            HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException;

    /**
     *---------------------------------------
     * Atualiza Resultado e grava Historico - 209, 329, 336
     *---------------------------------------
     */
    public abstract void atualizarAuto209(RequerimentoBean myReq, HistoricoBean myHis,
            AutoInfracaoBean myAuto,UsuarioBean myUsuario, Ata ata) throws DaoException;

    /**
     *---------------------------------------
     * Atualiza data de Envio para DO e Numero da CI e grava Historico
     *     Transa��es 211, 331, 344
     *  *---------------------------------------
     */
    public abstract void atualizarAuto211(HistoricoBean myHis, AutoInfracaoBean myAuto,UsuarioBean myUsuario)
            throws DaoException;

    /**
     *---------------------------------------
     * Atualiza Data de envio para DO e grava Historico
     * Transa��es 213, 333, 346
     *---------------------------------------
     */
    public abstract void atualizarAuto213(HistoricoBean myHis, AutoInfracaoBean myAuto,UsuarioBean myUsuario)
            throws DaoException;

    /**
     *-----------------------------------------------------------
     * executar Troca de Real Infrator - 214
     *-----------------------------------------------------------
     */
    public abstract void atualizarAuto214(RequerimentoBean myReq, HistoricoBean myHis,
            AutoInfracaoBean myAuto, ParamOrgBean myParam,UsuarioBean myUsuario) throws DaoException;
    
    
    
    
    

    /**
     *---------------------------------------
     * Estorno DP, 1A e 2A - 226 - 350 - 351 - 352 - 210 - 330 - 337
     *---------------------------------------
     */
    /*Estorno*/
    
    public abstract void atualizarAuto226(RequerimentoBean myReq, HistoricoBean myHis,
            AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException;

    public abstract void atualizarAuto264(RequerimentoBean myReq, HistoricoBean myHis,
            AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException;
    
    
    /**-----------------------------------------------------------
     * Transa��o 250 - Atualiza Data de Vencimento
     ------------------------------------------------------------- */
    public abstract void atualizaDataVenc(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,
            String novadata) throws DaoException;
    
    
    /**
     *---------------------------------------
     * Atualiza data de Envio para Publica��o por Requerimento
     *     Transa��es 261, 381, 388
     *  *---------------------------------------
     */
    public abstract void atualizarAuto261(RequerimentoBean myReq,HistoricoBean myHis, AutoInfracaoBean myAuto,UsuarioBean myUsuario)
            throws DaoException;

    /**
     *---------------------------------------
     * Atualiza data de Envio para Publica��o por Requerimento
     *     Transa��es 263, 383, 390
     *  *---------------------------------------
     */
    public abstract void atualizarAuto263(RequerimentoBean myReq,HistoricoBean myHis, AutoInfracaoBean myAuto,UsuarioBean myUsuario)
            throws DaoException;

    /**
     *---------------------------------------
     * Realiza o estorno dos requerimentos com status 4 (Enviados para publica��o).
     * @param myReq. Objeto referente ao requerimento, possui todas as informa��es do mesmo
     * @param myHis. Objeto hist�rico do processamento realizado. Somente � utilizado para informar o usu�rio e o codEvento a ser realizado.
     * @param myAuto. Objeto referente ao auto de infra��o, possui todas as informa��es do mesmo.
     * @author Sergio Roberto Junior
     * @since 09/12/2004
     * @throws DaoException
     * @version 1.0
     *---------------------------------------
     */
    /*Estorno*/
    public abstract void atualizarAuto382(RequerimentoBean myReq, HistoricoBean myHis,
            AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException;

    /**
     *-----------------------------------------------------------
     * Movimento para Banco - Transa��es 407 
     *-----------------------------------------------------------
     */
    public abstract void atualizarAuto407(AutoInfracaoBean myAuto, HistoricoBean myHis,UsuarioBean myUsuario)
            throws DaoException;

    /*-----------------------------------------------------------
     * Atualiza Numero do Processo e grava Historico - 410 
     *-----------------------------------------------------------
     */public abstract void atualizarAuto410(AutoInfracaoBean myAuto, HistoricoBean myHis,UsuarioBean myUsuario)
            throws DaoException;

    /*-----------------------------------------------------------
     * Atualiza Status e grava Historico - 411 
     *-----------------------------------------------------------
     */public abstract void atualizarAuto411(AutoInfracaoBean myAuto, HistoricoBean myHis,UsuarioBean myUsuario)
            throws DaoException;

    /*---------------------------------------------
     * Ajuste Automatico de CR da Venda - 412 
     *-----------------------------------------------
     */public abstract ArrayList ajustarCrVenda412(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog)
            throws DaoException;

    /**-----------------------------------------------------------
     * Transa��o 413 - Ajuste manual do Responsavel pelos Pontos
     ------------------------------------------------------------- */
    public abstract void ajustarManual(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,
            RequerimentoBean myReq,HistoricoBean myHist) throws DaoException;

    /**-----------------------------------------------------------
     * Transa��o 414 - Ajuste CPF
     ------------------------------------------------------------- */
    public abstract void ajustarCPF(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,
            String indResp) throws DaoException;

    /**-----------------------------------------------------------
     * Transa��o 415 - Ajuste manual do Responsavel pela Pec�nia
     ------------------------------------------------------------- */
    public abstract void ajustarManualRespPecunia(AutoInfracaoBean myAuto,
            ACSS.UsuarioBean myUsrLog, RequerimentoBean myReq,HistoricoBean myHis) throws DaoException;

    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
     * @param conn Conex�o com a base de dados 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    public abstract String Transacao086(String codOrgao, String codJunta, String codAcao,
            String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd,
            String numEnd, String txtCompl, String nomBairro, String numCep, String codMun,
            String numDddTel, String numTel, String numFax, Connection conn)
            throws DaoException;

    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    public abstract String Transacao086(String codOrgao, String codJunta, String codAcao,
            String indContinua, String nomJunta, String sigJunta, String tpJunta, String txtEnd,
            String numEnd, String txtCompl, String nomBairro, String numCep, String codMun,
            String numDddTel, String numTel, String numFax) throws DaoException;

    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codJunta C�digo da Junta. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    public abstract String Transacao087(String codOrgao, String codJunta, String numCPF,
            String codAcao, String indContinua, String nomRelator) throws DaoException;

    /**************************************
     * 
     * @param codOrgao C�digo do Orgao. 999999 = Todos
     * @param codAcao C�digo da a��o desejada
     * @param indContinua C�digo do Orgao/Respons�vel pelo parecer Jur�dico a partir do qual continuar� a consulta geral 
     * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
     */
    public abstract String Transacao088(String codOrgao, String numCPF, String codAcao,
            String indContinua, String nomResponsavel) throws DaoException;

    public abstract String chamaTransBRK(String codTransacao, String opcEscolha, String indContinua)
            throws DaoException;

    public abstract String chamaTransBRK(String codTransacao, String opcEscolha, String indContinua, UsuarioBean myUsuario)
    throws DaoException;
    
    
    

    public abstract boolean verErro(AutoInfracaoBean myInf, String resultado, String msg,
            Statement stmt) throws DaoException;

    public abstract boolean verErro(AutoInfracaoBean myInf, String resultado, String msg)
            throws DaoException;

    public abstract void acertaComplemento(HistoricoBean myHis) throws DaoException;
    
	public abstract boolean LeAutoInfracaoLocal(AutoInfracaoBean myAuto, String tipo) throws DaoException;
	
	public abstract boolean LeRequerimentoLocal (AutoInfracaoBean myInf, Connection conn) throws sys.DaoException;
	public abstract boolean LeRequerimentoLocal (RequerimentoBean myReq, Connection conn) throws sys.DaoException;	
	
	
	public abstract void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto) throws DaoException;
	public abstract void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto, Connection conn) throws DaoException,SQLException;		
	  
    /******************************************
     Verifica se o Auto ja esta no Oracle
     ******************************************/
    public abstract void VerificaAutoOracle(AutoInfracaoBean myAuto, Connection conn)
            throws DaoException;	
//	 **************************************************************************************	
	   public void setPosIni(int posIni); 
	   public int getPosIni(); 
	   public String getPedaco(String origem,int tam,String dirpad,String carpad); 
	   public String getPedacoNum(String origem,int tam,int dec,String ptdec);
		
	   public String getPedacoDt(String origem,int tam) ;
	   
	   public abstract boolean consultaAutoSemAr (ConsultaAutosSemDigiBean myAutosSemDig,ACSS.UsuarioBean myUsrLog) throws DaoException;
   
	   public List getAutosPendentes(String Placa,String Auto )throws DaoException;
	   public List getAutosPendentesDol(String Placa,String Auto )throws DaoException;
       public void LeInfracao(InfracaoBean bean, String tpchave)throws DaoException;
   	   public void AtualizaCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException;
   	   public void BuscarDataCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException;
   	   public void BuscarDataCodigoBarraNotificacao(REC.AutoInfracaoBean myAuto) throws DaoException;
}