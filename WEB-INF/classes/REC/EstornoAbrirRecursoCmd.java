package REC;

import java.util.Iterator;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class EstornoAbrirRecursoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/REC/EstornoAbrirRecurso.jsp" ;  
	public EstornoAbrirRecursoCmd() {
		next = jspPadrao;
	}

	public EstornoAbrirRecursoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if(UsrLogado==null)UsrLogado = new ACSS.UsuarioBean() ;

			// cria os Beans, se n�o existir
			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if(RequerimentoId==null) RequerimentoId = new RequerimentoBean() ;	  			

			EstornoAbrirRecursoBean EstornoAbrirRecursoId = (EstornoAbrirRecursoBean)req.getAttribute("EstornoAbrirRecursoId") ;
			if(EstornoAbrirRecursoId==null) EstornoAbrirRecursoId = new EstornoAbrirRecursoBean() ;	  			
			EstornoAbrirRecursoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));

			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";

			if( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
				// SE INDEFERIMENTO DE PLANO - 237 VERIFICAR STATUS DO AUTO E MUDAR PARA DI/1I/2I
				if ("DI,1I,2I".indexOf(EstornoAbrirRecursoId.getTipoReqValidos())>=0) 
					acertaTransacao(EstornoAbrirRecursoId,AutoInfracaoBeanId) ;
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				EstornoAbrirRecursoId.setEventoOK(AutoInfracaoBeanId) ;

			}

			if(acao.equals("LeReq"))  {
				if ("DI,1I,2I".indexOf(EstornoAbrirRecursoId.getTipoReqValidos())>=0) 
					acertaTransacao(EstornoAbrirRecursoId,AutoInfracaoBeanId) ;
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				EstornoAbrirRecursoId.setEventoOK(AutoInfracaoBeanId) ;
				RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
				if (RequerimentoId.getCodRequerimento().length()!=0) {
					RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
					if (RequerimentoId.getDatEST().length()==0) RequerimentoId.setDatEST(sys.Util.formatedToday().substring(0,10)) ;			
				}
			}

			if(acao.equals("EstornaRecurso"))  {
				if ("DI,1I,2I".indexOf(EstornoAbrirRecursoId.getTipoReqValidos())>=0) 
					acertaTransacao(EstornoAbrirRecursoId,AutoInfracaoBeanId) ;
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				EstornoAbrirRecursoId.setEventoOK(AutoInfracaoBeanId) ;
				//Verifica se possui Guia de Remessa - Valido para estorno de Recurso
				if(req.getParameter("j_sigFuncao").equals("REC0810")||
						req.getParameter("j_sigFuncao").equals("REC0820")||
						req.getParameter("j_sigFuncao").equals("REC0830")){
					RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
					Iterator it = AutoInfracaoBeanId.getRequerimentos().iterator() ;
					REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
					while (it.hasNext()) {
						reqIt =(REC.RequerimentoBean)it.next() ;
						if ( (EstornoAbrirRecursoId.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
								(EstornoAbrirRecursoId.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
								(reqIt.getCodRequerimento().equals(RequerimentoId.getCodRequerimento())) ) { 
							RequerimentoId.copiaRequerimento(reqIt) ;					   
							break ;
						}
					}
					RemessaBean myRemessa = new RemessaBean();
					boolean bExisteCodRemessa = myRemessa.getCodRemessa(RequerimentoId);
					if(!bExisteCodRemessa)
						nextRetorno = processaResult(EstornoAbrirRecursoId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
					else {
						AutoInfracaoBeanId.setMsgErro("Requerimento est� vinculado a uma Guia de Remessa n� "+RequerimentoId.getCodRemessa());
						EstornoAbrirRecursoId.setEventoOK("N") ;
					}
				}else
					nextRetorno = processaResult(EstornoAbrirRecursoId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);

			}
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			req.setAttribute("RequerimentoId",RequerimentoId) ;		
			req.setAttribute("EstornoAbrirRecursoId",EstornoAbrirRecursoId) ;		
		}
		catch (Exception ue) {
			throw new sys.CommandException("EstornoAbrirRecursoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

	private String processaResult(EstornoAbrirRecursoBean EstornoAbrirRecursoId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		String nextRetorno = next ;
		try {
			myAuto.setMsgErro("") ;   		
			Vector vErro = new Vector() ;

			// validar se status permite recurso ou defesa previa : 5,25,35
			if ("S".equals(EstornoAbrirRecursoId.getEventoOK())==false) vErro.addElement(EstornoAbrirRecursoId.getMsg()+" \n") ;
			myReq.setCodRequerimento(req.getParameter("codRequerimento"));
			if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado.\n") ;

			// localizar o requerimento e validar status
			boolean existe = false ;		
			Iterator it = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
			while (it.hasNext()) {
				reqIt =(REC.RequerimentoBean)it.next() ;
				if ( (EstornoAbrirRecursoId.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
						(EstornoAbrirRecursoId.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
						(reqIt.getCodRequerimento().equals(myReq.getCodRequerimento())) ) { 
					myReq.copiaRequerimento(reqIt) ;					   
					existe = true ;
					break ;
				}
			}
			if (existe==false) vErro.addElement("Requerimento n�o localizado no Auto de Infra��o. \n") ;
			String datEST = req.getParameter("datEST");		
			if (datEST==null) datEST = "";
			if (sys.Util.DifereDias( datEST,myReq.getDatRequerimento())>0) vErro.addElement("Data n�o pode ser anterior ao Requerimento ("+myReq.getDatRequerimento()+"). \n") ;
			String sta = "8" ;

			// Estorno de Recurso	
			//validar data de entrada
			if (datEST.length()==0) vErro.addElement("Data do Estorno n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),datEST)>0) vErro.addElement("Data do Estorno n�o pode ser superior a hoje. \n") ;
			String txtMotivoEST = req.getParameter("txtMotivoEST");
			if (txtMotivoEST==null) txtMotivoEST = "";
			if (txtMotivoEST.length()==0) vErro.addElement("Preenchimento do Motivo � obrigat�rio. \n") ;		
			myReq.setCodStatusRequerimento(sta);		
			myReq.setDatEST(datEST);
			myReq.setTxtMotivoEST(txtMotivoEST);

			if (vErro.size()==0) {
				myReq.setNomUserNameEST(UsrLogado.getNomUserName());
				myReq.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
				myReq.setDatProcEST(sys.Util.formatedToday().substring(0,10));		
				atualizarReq(EstornoAbrirRecursoId,myAuto,myReq,UsrLogado) ;	
				if (myAuto.getMsgErro().length()==0) {					
					myAuto.setMsgErro("Estorno efetuado para o Requerimento:"+myReq.getNumRequerimento()) ;	
					myAuto.setMsgOk("S") ;												
				}
			}
			else myAuto.setMsgErro(vErro) ;
		}
		catch (Exception e){
			throw new sys.CommandException("EstornoAbrirRecursoCmd: " + e.getMessage());
		}
		return nextRetorno  ;
	}

	private void atualizarReq(EstornoAbrirRecursoBean EstornoAbrirRecursoId,AutoInfracaoBean myAuto,RequerimentoBean myReq,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		try { 	
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(myAuto.getNumProcesso());
			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodEvento(EstornoAbrirRecursoId.getCodEvento());
			myHis.setCodOrigemEvento(EstornoAbrirRecursoId.getOrigemEvento());
			myHis.setNomUserName(myReq.getNomUserNameEST());
			myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoEST());
			myHis.setDatProc(myReq.getDatProcEST());
			myHis.setTxtComplemento01(myReq.getNumRequerimento());	
			myHis.setTxtComplemento02(myReq.getDatEST());	
			myHis.setTxtComplemento04(myReq.getTxtMotivoEST());	
			DaoBroker dao = DaoBrokerFactory.getInstance();
			if( (EstornoAbrirRecursoId.getCFuncao().equals("REC0348")) || (EstornoAbrirRecursoId.getCFuncao().equals("REC0448"))){
				myHis.setTxtComplemento01(EstornoAbrirRecursoId.getNFuncao());
				dao.atualizarAuto382(myReq,myHis,myAuto,UsrLogado);
			}
			else
				dao.atualizarAuto226(myReq,myHis,myAuto,UsrLogado) ; 
			EstornoAbrirRecursoId.removeAutoGuia(myAuto,myReq);
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
	}	

	private void acertaTransacao(EstornoAbrirRecursoBean EstornoAbrirRecursoId,AutoInfracaoBean myAuto) throws sys.CommandException { 
		try {
			//Defesa Pr�via
			if ("0,2,4,5".indexOf(myAuto.getCodStatus())>=0) {
				EstornoAbrirRecursoId.setStatusValido(myAuto.getCodStatus());
				EstornoAbrirRecursoId.setTipoReqValidos("DI") ;
				EstornoAbrirRecursoId.setCodEvento("237") ;
			}

			if(myAuto.getCodStatus().length()>1) {
				//1a. int�ncia
				if ("10,11,14,15,16,82".indexOf(myAuto.getCodStatus())>=0) {
					EstornoAbrirRecursoId.setStatusValido(myAuto.getCodStatus());
					EstornoAbrirRecursoId.setTipoReqValidos("1I") ;
					EstornoAbrirRecursoId.setCodEvento("357") ;	
				}

				//2a. instancia
				if ("30,35".indexOf(myAuto.getCodStatus())>=0) {
					EstornoAbrirRecursoId.setStatusValido(myAuto.getCodStatus());
					EstornoAbrirRecursoId.setTipoReqValidos("2I") ;
					EstornoAbrirRecursoId.setCodEvento("365");   	 	
				}
			} 			 	    	
		}
		catch (Exception e){
			throw new sys.CommandException("EstornoAbrirRecursoCmd: " + e.getMessage());
		}
	}
}