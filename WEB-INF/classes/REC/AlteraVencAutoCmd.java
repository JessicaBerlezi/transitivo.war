package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

public class AlteraVencAutoCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AlteraVencAuto.jsp";
	private Logger log = Logger.getLogger(this.getClass().getName());

	public AlteraVencAutoCmd() {
		next = jspPadrao;
	}

	public AlteraVencAutoCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try { 
			
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			
			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
			
			ACSS.ParamSistemaBean parAltVenc = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parAltVenc == null)	parAltVenc = new ACSS.ParamSistemaBean();
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			String alerta="";
			
			if(alerta==null){
				alerta= "";
			}
			if(acao==null){ 
				acao = " ";
				
			}
			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.setMsgOk("S");					
			
			}
			
			if (acao.equals("AlteraData")) { 
		
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {		
					String novaData = req.getParameter("novaData") ;				  
					if(novaData==null) novaData= "";
				
					//Verifica se a data de altera��o do Auto � V�lida
					String DatMaxAntes=(parAltVenc.getParamSist("N_DIAS_MAX_ANTECIPACAO"));	
					
					String DatMaxDepois=(parAltVenc.getParamSist("N_DIAS_MAX_PRORROGACAO"));
					
					if (sys.Util.DifereDias(novaData,AutoInfracaoBeanId.getDatVencimento())>Integer.parseInt(DatMaxDepois))						
						AutoInfracaoBeanId.setMsgErro("Data superior ao prazo m�ximo permitido !") ;
						
						else if (sys.Util.DifereDias(novaData,AutoInfracaoBeanId.getDatVencimento())>Integer.parseInt(DatMaxAntes))
						AutoInfracaoBeanId.setMsgErro("Data Inferior ao prazo m�nimo permitido !") ;
					else{
						if(!"".equals(novaData)){
							
							DaoBroker dao = DaoBrokerFactory.getInstance();
							dao.atualizaDataVenc(AutoInfracaoBeanId,UsrLogado,novaData);				  
							if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
								AutoInfracaoBeanId.setMsgErro("Altera��o da Data efetuada com sucesso.") ;
							}
						}
					}
				}
			}
			if (acao.equals(" ")) 
				AutoInfracaoBeanId = new AutoInfracaoBean();
			
		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		req.setAttribute("UsrLogado",UsrLogado) ;
		req.setAttribute("alerta",alerta) ;		
		
	}
	catch (Exception ue) {
		throw new sys.CommandException("AlteraVencAutoCmd: " + ue.getMessage());
	}		
	return nextRetorno;
}

}