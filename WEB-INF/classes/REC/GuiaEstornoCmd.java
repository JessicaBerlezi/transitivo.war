package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GuiaEstornoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/REC/GuiaReemite.jsp" ;  
	
	public GuiaEstornoCmd() {
		next = jspPadrao;
	}
	
	public GuiaEstornoCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try {     
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
			ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
			GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
			if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
			
			//Carrego os Beans		
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");
			String acao    = req.getParameter("acao");   	 
			String statusInteracao = req.getParameter("statusInteracao");
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			if (acao==null)	{				  	
				acao = " ";
				statusInteracao = "1";
			}else{
				String dataIni = req.getParameter("De");
			    if (dataIni == null) dataIni = "";
			    GuiaDistribuicaoId.setDataIni(dataIni);
			  
			    String dataFim = req.getParameter("Ate");
			    if (dataFim == null) dataFim = "";
			    GuiaDistribuicaoId.setDataFim(dataFim);
			}
			
			if (acao.equals("LeGuia")) {				
				GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
				GuiaDistribuicaoId.LeGuia();
				if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
				nextRetorno = "/REC/GuiaEstorno.jsp";
			}	        
			
			if  (acao.equals("ImprimirGuia"))  {	      	  
				if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
				nextRetorno = "/REC/GuiaPreparaImp.jsp" ;      	  
			}
			
			if (acao.equals("Classifica")) {  
				GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;
				nextRetorno = "/REC/GuiaEstorno.jsp";
			}
			
			if (acao.equals("estonarGuia")) {  
				GuiaDistribuicaoId.estornarGuia();
				nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp" ; 				
			}
			
			// processamento de saida dos formularios
			if (acao.equals("R")){
				session.removeAttribute("GuiaDistribuicaoId") ;
				if( statusInteracao!= null){
					if(statusInteracao.equals("apresentacao"))
						session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
				}
				String dataIni = GuiaDistribuicaoBean.formatedLast2Month().substring(0, 10);
			    String dataFim = sys.Util.formatedToday().substring(0, 10);
 		        GuiaDistribuicaoId.setDataIni(dataIni);
				GuiaDistribuicaoId.setDataFim(dataFim);
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
			}
			else{
				session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
			}	
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEstornarJuntaRelatorCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}  
}
