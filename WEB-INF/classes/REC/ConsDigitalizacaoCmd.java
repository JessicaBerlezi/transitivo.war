package REC;

import javax.servlet.http.HttpServletRequest;

public class ConsDigitalizacaoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/REC/ConsDigitalizacao.jsp";

	public ConsDigitalizacaoCmd() {
		next = jspPadrao;
	}

	public ConsDigitalizacaoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;

		try {
			String acao = req.getParameter("acao");
			if (acao == null)
				acao = "";

			if (!acao.equals("")) {
				
				String numAutoInfracao = req.getParameter("numAutoInfracao");
				if (numAutoInfracao == null) numAutoInfracao = "";
                
                String numNotificacao = req.getParameter("numNotificacao");
                if (numNotificacao == null) numNotificacao = "";
				
				AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean();
				AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());
				AutoInfracaoBeanId.setNumAutoInfracao(numAutoInfracao);
                AutoInfracaoBeanId.setNumNotificacao(numNotificacao);
				
				//Auto Digitalizado
                if (acao.equals("AIDigitalizado")) {					
					AutoInfracaoBeanId.LeAIDigitalizado();
					req.setAttribute("mostrarLocal", "S");
					nextRetorno = "/REC/VisAIDig.jsp";
						
				} else if (acao.equals("chamaImpressao")) {					
					AutoInfracaoBeanId.LeAIDigitalizado();
					nextRetorno = "/REC/VisAIDigImpressao.jsp" ;
					
				} if (acao.equals("chamaImagem")) {					
					AutoInfracaoBeanId.LeAIDigitalizado();
					nextRetorno = "/REC/VisAIDigImagem.jsp" ;
					
                //AR Digitalizado                    
				} else if (acao.equals("ARDigitalizado")) {					
					AutoInfracaoBeanId.LeARDigitalizado();
					nextRetorno = "/REC/VisARDig.jsp";
											
				} else if  (acao.equals("VisARDigImagem"))  {										
					AutoInfracaoBeanId.LeARDigitalizado();	
					nextRetorno = "/REC/VisARDigImagem.jsp" ;
				
                //Foto Digitalizada                    
                } else if (acao.equals("FotoDigitalizada")) {                    
                    AutoInfracaoBeanId.LeFotoDigitalizada();
                    nextRetorno = "/REC/VisFotoDig.jsp";
                                            
                } else if  (acao.equals("VisFotoDigImagem"))  {                                        
                    AutoInfracaoBeanId.LeFotoDigitalizada();  
                    nextRetorno = "/REC/VisFotoDigImagem.jsp" ;
                }
				
				req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);						
			}

		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	}

}
