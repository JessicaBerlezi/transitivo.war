package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class PublicacaoAtaCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/PublicacaoAta.jsp" ;  
  public static  String cdOrgao;
  public PublicacaoAtaCmd() {
    next  =  jspPadrao;
  }

  public PublicacaoAtaCmd(String next) {
    this.next = next;
  }
  public void setNext(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
  	String sMensagem    = "";
    try {     
	    // cria os Beans de sessao, se n�o existir //
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado          = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado     = new ACSS.UsuarioBean() ;
		ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
	    ParamOrgBean ParamOrgaoId           = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
	    ParamSistemaBean parSis             = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (parSis == null)	parSis          = new ParamSistemaBean();
	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  	{
	  		AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  		if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
	  		if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
	  		if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
	  		if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
	  		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
	  	}
	  	
		// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
		String numAta           = req.getParameter("numAta");
		cdOrgao = ParamOrgaoId.getCodOrgao();
	  	if(acao==null)   acao = " ";
	  	if ( ("Novo".equals(acao)) || ("valBiometria".equals(acao))  ){
		  	req.setAttribute("LeAI","S");
			acao =" ";
		  	AutoInfracaoBeanId     = new AutoInfracaoBean() ;
	  	}	
		else {	
		  	if (" ".equals(acao)) req.setAttribute("LeAI","S");
			else {	
				req.setAttribute("LeAI","N");				
	  			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));  
		  		AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));  
	  			AutoInfracaoBeanId.setNumProcesso(req.getParameter("numProcesso"));
	  			String numRenavam = req.getParameter("numRenavam");
	  			if (numRenavam==null) numRenavam="";
	  			AutoInfracaoBeanId.setNumRenavam(numRenavam);
	  			String codAcessoWeb = req.getParameter("numAcesso");
	  			if (codAcessoWeb==null) codAcessoWeb="";
	  			AutoInfracaoBeanId.setCodAcessoWeb(codAcessoWeb);
				String temp = UsrLogado.getCodOrgaoAtuacao();
				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999");
				
				Ata ata = new Ata();
				if(acao.equals("publicacaoAta")){
					/*
					 * Verifica se status da ata esta fechada return false
					 */
					if(!ata.validaStatusAta(numAta)){
						nextRetorno = "/REC/AtaPublicacaoImp.jsp" ;
					}else{
						//Exibir alert...
						nextRetorno = "/REC/PublicacaoAta.jsp" ;
					}
				}
			
  				if (AutoInfracaoBeanId.getMsgErro().length()!=0) req.setAttribute("LeAI","S");
				
			  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;			
			}	
		}
    }
    catch (Exception ue) {
	      throw new sys.CommandException("ProcessaAutoCmd: " + ue.getMessage()+" "+sMensagem);
    }
    
    if(cdOrgao.equals("258470")){
		nextRetorno = "/REC/InfoPublicacaoProcesso.jsp";
	}else{
		nextRetorno = "/REC/AtaDistribuicaoImp.jsp";
	}
    
	return nextRetorno ;
  }
  
}
