package REC;

import java.util.Vector;

/**
* <b>Title:</b>Motivo de Cancelamento<br>
* <b>Description:</b>Cadastra os motivos do cancelamento dos autos<br>
* <b>Copyright:</b>Copyright (c) 2005<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha	
* @version 1.0
*/

public class MotivoCancelBean extends sys.HtmlPopupBean { 

	private int codMotivoCancel;
	private String numMotivoCancel;
	private String descMotivoCancel;
	private String sNumMotivoCancel;
	private String atualizarDependente;
	private Vector vMotivoCancel;

	public MotivoCancelBean() throws sys.BeanException{
	  	super();
	  	setTabela("TSMI_MOTIVO_CANCELAMENTO");	
	  	setPopupWidth(35);
	  	numMotivoCancel = "";   	
	  	descMotivoCancel = "";
	  	codMotivoCancel = 0;
	  	vMotivoCancel = new Vector();
		atualizarDependente = "N";
	}
	

	public void setCodMotivoCancel(int codMotivoCancel) {
		if(codMotivoCancel == 0) codMotivoCancel = 0;
		else this.codMotivoCancel = codMotivoCancel;
	}
	public int getCodMotivoCancel() {
		return codMotivoCancel;
	}
	   
	
	public void setNumMotivoCancel(String numMotivoCancel) {
		if (numMotivoCancel == null) numMotivoCancel = "";
		else
		{ 
			if(!numMotivoCancel.equals(""))
			  this.numMotivoCancel = sys.Util.lPad(numMotivoCancel,"0",3);
			else this.numMotivoCancel = numMotivoCancel;
		}
	}
	public String getNumMotivoCancel() {
		return this.numMotivoCancel;
	}
	public String getNumMotivoCancelEdit()  {
		sNumMotivoCancel = sys.Util.lPad(getNumMotivoCancel(),"0",3);
		return this.sNumMotivoCancel;
	}
	
	
	public void setAtualizarDependente(String atualizarDependente) {
		if(atualizarDependente == null) atualizarDependente = "";
		else this.atualizarDependente = atualizarDependente;
	}
    public String getAtualizarDependente() {
	   return atualizarDependente;
	}
	
    
	public void setDescMotivoCancel(String descMotivoCancel) {
		if (descMotivoCancel == null) descMotivoCancel = "";
		else this.descMotivoCancel = descMotivoCancel;
	}
	public String getDescMotivoCancel() {
		return descMotivoCancel;
	}
	

    public void setMotivoCancel(Vector vMotivoCancel)  {
  	  this.vMotivoCancel = vMotivoCancel;
    }  
  	public MotivoCancelBean getMotivoCancel(int i)  { 
  		 return (MotivoCancelBean)this.vMotivoCancel.elementAt(i);
  	}
    public Vector getMotivoCancel()  {
   	  return this.vMotivoCancel;
    }   
    public Vector getMotivoCancel(int min,int livre)  {
	   try  { 
		  DaoDigit dao = DaoDigit.getInstance();
		  vMotivoCancel = dao.ConsultaMotivoCancel(null, null);
  
		  if (this.vMotivoCancel.size()<min-livre) 
			  livre = min-vMotivoCancel.size() ;	  	
		  for (int i=0;i<livre; i++) 	{
		      this.vMotivoCancel.addElement(new MotivoCancelBean()) ;
		  }
	   }
	   catch (Exception e) {
		  setMsgErro("Retorno 007 - Leitura: " + e.getMessage());
	   }
	   return this.vMotivoCancel;
   }


  public boolean isAlterMotivoCancel()   {
	  boolean retorno = true;
	  Vector vErro = new Vector() ;
		
	  // Verificada a qual ser� a a��o a ser realizada sobre os dados. Se inclus�o, altera��o ou exclus�o
	  try  {
		  DaoDigit dao = DaoDigit.getInstance();		   
		  if(dao.AlterMotivoCancel(this,vErro))  vErro.add("Opera��o realizada com sucesso!");
	  }
	  catch (Exception e) {
	  	vErro.addElement("Motivo Cancelamento: "+String.valueOf(getCodMotivoCancel())+" n�o atualizados:"+ " \n") ;
	  	vErro.addElement("Erro : "+ e.getMessage() +"\n");
	    retorno = false;
	  }
      setMsgErro(vErro);
      return retorno;
  }

}