package REC;



import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        SMIT - Consulta Etiqueta Auto Cmd <br>
* <b>Description:</b>  Consulta Etiquetas por caixa, lote ou auto <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Lucian Rocha
* @version 1.0
*/

public class ConsultaEtiquetaAutoCmd extends sys.Command {

  private static final String jspPadrao="/REC/ConsultaEtiquetaAuto.jsp";    
  private String next;

  public ConsultaEtiquetaAutoCmd() {
		next = jspPadrao;
  }

	public ConsultaEtiquetaAutoCmd(String next) {
		this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
  	
		String nextRetorno  = jspPadrao ;
		
		try {    
			  // Cria o bean
			 EtiquetaAutoBean bean = (EtiquetaAutoBean)req.getAttribute("bean");
			 if(bean == null) bean = new EtiquetaAutoBean();
				 
			 String verTable = req.getParameter("verTable");
			 if (verTable == null)verTable = "N";
			 
			 String acao = req.getParameter("acao");  
			 if (acao == null)acao = "";
										
			String numLote = req.getParameter("numLote");
			if (numLote == null) numLote = "";
			else numLote=numLote.trim();
	
			String numCaixa = req.getParameter("numCaixa");			
			if (numCaixa == null)numCaixa = "";
			else numCaixa=numCaixa.trim();
	
			 String numAuto = req.getParameter("numAuto");			
			 if (numAuto == null) numAuto = "";
			 else numAuto = numAuto.trim();
				
			 String ordem = req.getParameter("ordem");			
			 if (ordem == null) ordem = "";
			 else ordem = ordem.trim();
					
			 //atribuindo valores
			 bean.setNumCaixa(numCaixa);
			 bean.setNumLote(numLote);
			 bean.setNumAutoInfracao(numAuto);
			 bean.setOrdem(ordem);	
	
			 //Execucao dos metodos
			 
			 if(acao.equals("consultaAutoEtiq")){
				verTable = "N";
		  	 }
		  	 
			 if (acao.equals("retorna")) {
				 bean.setNumCaixa("");
				 bean.setNumLote("");
				 bean.setNumAutoInfracao("");
			 }
				
			 if (acao.equals("buscaAutoEtiq")) {
				 verTable = "S";
				// bean.consultaAutoInfracao(numLote,numCaixa,numAuto);
			 }
				 
			 //Chama o jsp EtiquetaAuto
			 ArrayList listAuto = new ArrayList();
			 //String msgErro="";
			 if(acao.equals("chamaAutoEtiq")){
				 // Parametros selecionadas da linha
				 String numLoteSel = req.getParameter("numLoteSel");
				 if (numLoteSel == null) numLoteSel = "";
		
				 String numCaixaSel = req.getParameter("numCaixaSel");			
				 if (numCaixaSel == null) numCaixaSel = "";
		
				 String numAutoSel = req.getParameter("numAutoSel");			
				 if (numAutoSel == null) numAutoSel = "";
		
				 try {
					 DaoDigit dao = DaoDigit.getInstance();			
					 //dao.ConsultaEtiquetaAutoInfracao(numLoteSel,numCaixaSel,listAuto);
				 } 
				 catch (Exception e) {					
					 throw new sys.CommandException(e.getMessage());
				 }
				 nextRetorno = "/REC/EtiquetaAuto.jsp" ;
				 numCaixa = numCaixaSel;
				 numLote = numLoteSel;
		 }
			
		 req.setAttribute("bean",bean);
		 req.setAttribute("numCaixa",numCaixa);
		 req.setAttribute("numLote",numLote);
		 req.setAttribute("listAuto",listAuto);
		 req.setAttribute("numAuto",numAuto);
		 req.setAttribute("verTable",verTable);
				 
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
			
		return nextRetorno;
	} 
}
