package REC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import TAB.EventoBean;

import sys.DaoException;
import sys.Util;

/**
 * <b>Title:</b>        SMIT - MODULO RECURSO<br>
 * <b>Description:</b>  DaoTabelas - Acesso a Base de Dados das Tabelas do M�dulo<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class DaoTabelas {
	
	private static final String MYABREVSIST = "REC";
	private static DaoTabelas instance;
	private sys.ServiceLocator serviceloc ;  
	
	public static DaoTabelas getInstance()
	throws DaoException {
		if (instance == null)
			instance = new DaoTabelas();
		return instance;
	}
//	**************************************************************************************
	private DaoTabelas() throws DaoException {
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	/*
	 /**
	  *-----------------------------------------------------------
	  * Le Instrucao de Notificacao e carrega no Bean
	  *-----------------------------------------------------------
	  */
	public  boolean InstrNotLeBean (InstrNotBean myInstr,int tpUsr) throws DaoException {
		
//		synchronized
		boolean bOk = true ;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			
			Statement stmt     = conn.createStatement();
			String sCmd        = "SELECT n.cod_instrucao_not,n.num_linha_not,n.txt_linha_not,n.cod_orgao,n.cod_tipo_linha_not,n.cod_notificacao,o.sig_orgao,d.dsc_notificacao";
			sCmd += " from TSMI_INSTRUCAO_NOT n, TSMI_ORGAO o, tsmi_notificacao d" ;
			sCmd += " WHERE n.cod_orgao = o.cod_orgao";
			sCmd += " AND d.cod_notificacao ='"+myInstr.getCodNotificacao()+"'" ;
			sCmd += " AND n.cod_notificacao ='"+myInstr.getCodNotificacao()+"'" ;
			sCmd += " and n.cod_orgao = '"+myInstr.getCodOrgao()+"'" ;;
			sCmd += " ORDER BY n.NUM_LINHA_NOT"	;
			
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			myInstr.setMsgErro("") ;
			while (rs.next()) {
				myInstr.setNumLinhaNot(rs.getString("num_linha_not"));
				myInstr.setTxtLinhaNot(rs.getString("txt_linha_not"));
				myInstr.setCodOrgao(rs.getString("cod_orgao"));
				myInstr.setCodNotificacao(rs.getString("cod_notificacao"));
				myInstr.getOrgao().setSigOrgao(rs.getString("sig_orgao"));
				myInstr.setCodTipoLinhaNot(rs.getString("cod_tipo_linha_not"));
				myInstr.setDscNotificacao(rs.getString("dsc_notificacao"));
				myInstr.setPkid(myInstr.getCodInstrucaoNot()) ;
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myInstr.setCodInstrucaoNot("0") ;
			myInstr.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myInstr.setCodInstrucaoNot("0") ;
			myInstr.setPkid("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	public String getNotificacoes(String paraSQL)  throws DaoException {
		String myNotificacao ="";
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			
			
			String	sCmd     = "Select * from TSMI_NOTIFICACAO ";
			sCmd    += " order by COD_NOTIFICACAO";
			ResultSet rs = stmt.executeQuery(sCmd) ;
			
			while (rs.next()){
				if ("SQL".equals(paraSQL)) {
					myNotificacao += ","+rs.getString("cod_notificacao") +"";
				}
				else {
					myNotificacao += ",\""+rs.getString("cod_notificacao") +"\"";
					myNotificacao += ",\""+rs.getString("dsc_notificacao") +"\"";
				}
			}
			myNotificacao = myNotificacao.substring(1,myNotificacao.length());
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myNotificacao;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le Instrucoes
	 *-----------------------------------------------------------
	 */
	public void InstrNotGetParam(InstrNotBean myInstr)  throws DaoException  {
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_instrucao_not,num_linha_not,txt_linha_not,cod_orgao,cod_tipo_linha_not,cod_notificacao ";
			sCmd += " from TSMI_INSTRUCAO_NOT";
			sCmd += " WHERE cod_orgao = '"+ myInstr.getCodOrgao() + "'";
			sCmd += " and cod_notificacao =  '"+ myInstr.getCodNotificacao() + "'";
			sCmd += " order by num_linha_not";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector param = new Vector();
			while( rs.next() ) {
				InstrNotBean myParam = new InstrNotBean() ;
				myParam.setNumLinhaNot((rs.getString("num_linha_not")));
				myParam.setCodTipoLinhaNot((rs.getString("cod_tipo_linha_not")));
				myParam.setTxtLinhaNot((rs.getString("txt_linha_not")));
				myParam.setCodInstrucaoNot((rs.getString("cod_instrucao_not")));
				param.addElement(myParam) ;
			}
			myInstr.setParametros(param) ;
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) {
			myInstr.setMsgErro("Orgao008p - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myInstr.setCodOrgao("0") ;
		}
		catch (Exception e) {
			myInstr.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			myInstr.setCodOrgao("0") ;
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insere  de Intrucao de Preenchimento
	 *-----------------------------------------------------------
	 */
	public boolean InstrNotInsert(InstrNotBean myInstr, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			Vector paramAtual = myInstr.getParametros() ;
			// iniciar transa��o
			conn.setAutoCommit(false);
			
			String sCmd  = "DELETE TSMI_INSTRUCAO_NOT where cod_orgao ="+myInstr.getCodOrgao()+"";
			sCmd  += " and  cod_notificacao ="+myInstr.getCodNotificacao()+"";
			stmt.execute(sCmd);
			
			for (int i = 0 ; i<paramAtual.size(); i++) {
				
				InstrNotBean myParam = (InstrNotBean)paramAtual.elementAt(i) ;
				if ("".equals(myParam.getTxtLinhaNot().trim())==false ) 	{
					InstrNotParamInsert(myParam,stmt) ;
				}
				else {
					if ("".equals(myParam.getCodTipoLinhaNot())==false) InstrNotDelete(myParam,stmt) ;
					
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else 		 conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myInstr.setMsgErro(vErro);
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert em Instrucao
	 *-----------------------------------------------------------
	 */
	public void InstrNotParamInsert(InstrNotBean myParam,Statement stmt) throws DaoException {
		try {
			String codParam  = myParam.getCodInstrucaoNot();
			String sCmd    = "";
			ResultSet rs   ;
			
			if ((myParam.getTxtLinhaNot()!="") || (myParam.getTxtLinhaNot()!= " "))  {
				sCmd  = "INSERT INTO TSMI_INSTRUCAO_NOT (cod_instrucao_not,";
				sCmd += "num_linha_not,txt_linha_not,cod_tipo_linha_not,cod_notificacao,cod_Orgao) " ;
				sCmd += "VALUES (seq_tsmi_instrucao.nextval," ;
				sCmd += ""+myParam.getNumLinhaNot()+"," ;
				sCmd += "'"+myParam.getTxtLinhaNot()+"'," ;
				sCmd += "'"+myParam.getCodTipoLinhaNot()+"'," ;
				sCmd += ""+myParam.getCodNotificacao()+"," ;
				sCmd += ""+myParam.getCodOrgao()+") " ;
				
				stmt.execute(sCmd);
			}
			else   {
				
				
				sCmd  = "UPDATE TSMI_INSTRUCAO_NOT set ";
				sCmd += "num_linha_not   ='"+myParam.getNumLinhaNot()+"'," ;
				sCmd += "txt_linha_not   ='"+myParam.getTxtLinhaNot()+"'," ;
				sCmd += "cod_tipo_linha_not   ='"+myParam.getCodTipoLinhaNot()+"' " ;
				sCmd += " where cod_Instrucao_not = "+myParam.getCodInstrucaoNot()+" " ;
				
				stmt.execute(sCmd);
			}
			
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
	}
	/**
	 *-----------------------------------------------------------
	 * Delete em Instrucao
	 *-----------------------------------------------------------
	 */
	public void InstrNotDelete(InstrNotBean myParam,Statement stmt) throws DaoException {
		try {
			String sCmd  = "DELETE TSMI_INSTRUCAO_NOT where COD_INSTRUCAO_NOT = "+ myParam.getCodInstrucaoNot() ;
			stmt.execute(sCmd);
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
	}
	
	public void CopiaInstrucoes(InstrNotBean myInstr) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtCp = conn.createStatement();
			Vector vErro = new Vector();
			
			String sCmd = "SELECT num_linha_not,txt_linha_not,cod_tipo_linha_not FROM TSMI_INSTRUCAO_NOT "+
			"WHERE cod_orgao ='" + myInstr.getCodOrgao()+"' AND COD_NOTIFICACAO='" + myInstr.getCodNotificacao() + "' ";
			ResultSet rs    = stmt.executeQuery(sCmd);
			String numlinhanot = "";
			String txtlinhanot = "";
			String codtipolinhanot  = "";
			
			int nInstrOrig  = 0;
			int nInstrCopia = 0;
			int nInstrAtualiza= 0;			 			
			boolean existe  = false;
			while (rs.next()) {
				numlinhanot = rs.getString("num_linha_not") ;
				if (numlinhanot==null) numlinhanot="";
				txtlinhanot = rs.getString("txt_linha_not") ;
				if (txtlinhanot==null) txtlinhanot="";
				codtipolinhanot = rs.getString("cod_tipo_linha_not") ;
				if (codtipolinhanot==null) codtipolinhanot="D";
				nInstrOrig++;
				sCmd = "SELECT num_linha_not FROM TSMI_INSTRUCAO_NOT WHERE cod_orgao = "
					+ myInstr.getCodOrgaoCp() + " and num_linha_not = '" + numlinhanot + "'" +" and COD_NOTIFICACAO = '" + myInstr.getCodNotificacao() + "' ";
				ResultSet rsCp = stmtCp.executeQuery(sCmd);
				existe = false;
				while (rsCp.next()) {
					existe = true;
					vErro.addElement("Instru��o "+numlinhanot+" j� existente no Org�o "+myInstr.getCodOrgaoCp()+". \n");
				}
				if (existe == false){ 
					sCmd = "INSERT INTO TSMI_INSTRUCAO_NOT (cod_orgao,cod_instrucao_not,txt_linha_not,"+
					"num_linha_not, cod_tipo_linha_not,cod_notificacao)"+
					"VALUES (" + myInstr.getCodOrgaoCp() + ",seq_tsmi_instrucao.nextval,"+
					"'"+ txtlinhanot+ "','" + numlinhanot +"',"+
					"'"+ codtipolinhanot + "','" + myInstr.getCodNotificacao() +"')";
					stmtCp.execute(sCmd);
					nInstrCopia++;					
				}
				else {
					if (myInstr.getSobrescreve()== true){																				
						sCmd = "UPDATE TSMI_INSTRUCAO_NOT set ";												
						sCmd += "txt_linha_not ='" + txtlinhanot + "', ";											
						sCmd += "cod_tipo_linha_not ='" + codtipolinhanot + "' ";							        
						sCmd += "WHERE cod_notificacao = '" + myInstr.getCodNotificacao() + "' ";
						sCmd += " And cod_orgao = '" + myInstr.getCodOrgaoCp() + "' ";
						sCmd += " And num_linha_not = '" + numlinhanot + "' ";
						stmtCp.execute(sCmd);	
						nInstrAtualiza++;									
					}
				}			
			}
			stmtCp.close();
			stmt.close();
			vErro.addElement("Total de Instru��es na origem "+nInstrOrig+". Copiadas: "+nInstrCopia+". Atualizadas: "+ nInstrAtualiza + " \n");
			myInstr.setMsgErro(vErro);
			
		}		
		catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {				}
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le as Instrucoes de Notificacoes
	 *-----------------------------------------------------------
	 */
	public  boolean LeNotificacoes (InstrNotBean mynotificacoes) throws DaoException {
//		synchronized
		boolean bOk = true ;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt     = conn.createStatement();
			String sCmd  =  "SELECT * from TSMI_instrucao_not " ;
			sCmd += " WHERE cod_notificacao = '"+mynotificacoes.getCodNotificacao()+"' " +			"and cod_orgao='"+ mynotificacoes.getCodOrgao()+"' order by cod_instrucao_not";
			ResultSet rs       = stmt.executeQuery(sCmd) ;
			
			List notificacoes = new ArrayList();
			while (rs.next()) {
				InstrNotBean myHis = new InstrNotBean();
				myHis.setCodInstrucaoNot(rs.getString("cod_Instrucao_Not"));
				myHis.setCodTipoLinhaNot(rs.getString("cod_tipo_linha_not"));
				myHis.setNumLinhaNot(rs.getString("num_linha_not"));
				myHis.setCodOrgao(rs.getString("cod_orgao"));	  				  	  				 
				myHis.setTxtLinhaNot(rs.getString("txt_linha_not"));
				myHis.setCodNotificacao(rs.getString("cod_notificacao"));
				notificacoes.add(myHis) ;
			}
			mynotificacoes.setNotificacoes(notificacoes);
			sCmd  =  "SELECT dsc_notificacao from TSMI_NOTIFICACAO " ;
			sCmd += " WHERE cod_notificacao = '"+mynotificacoes.getCodNotificacao()+"'" ;
			rs       = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				mynotificacoes.setDscNotificacao(rs.getString("dsc_notificacao"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	/**
	 *-----------------------------------------------------------
	 * Codigo Relativo a Junta - Insert em Junta
	 *-----------------------------------------------------------
	 */
	public boolean JuntaInsert(JuntaBean myJunta, Statement stmt) {
		try {	
			String sCmd ="";
			String indTipo;
			if(myJunta.getIndTipo().equals("0"))
				indTipo = "1";
			else if(myJunta.getIndTipo().equals("1"))
				indTipo = "2";
			else
				indTipo = "3";
			if (("".equals(myJunta.getCodJunta())) || ("0".equals(myJunta.getCodJunta())))  {
				sCmd = "SELECT seq_tsmi_junta.nextval as codJunta FROM dual";
				ResultSet rs = stmt.executeQuery(sCmd);
				rs.next();
				myJunta.setCodJunta(rs.getString("codJunta"));
				rs.close();
				sCmd  = "INSERT INTO TSMI_JUNTA (cod_junta,"+
				"nom_junta,cod_orgao,sig_junta,ind_tipo) " +
				"VALUES (" +
				"'"+myJunta.getCodJunta()+"'," +
				"'"+myJunta.getNomJunta()+"'," +
				"'"+myJunta.getCodOrgao()+"',"+
				"'"+myJunta.getSigJunta()+"',"+
				"'"+myJunta.getIndTipo()+"')";
				stmt.execute(sCmd);
//				DaoBroker broker = DaoBrokerFactory.getInstance();
//				sCmd = broker.Transacao086(Util.lPad(myJunta.getCodOrgao(),"0",6),Util.lPad(myJunta.getCodJunta(),"0",6),"1",Util.lPad("","0",6),Util.rPad(myJunta.getNomJunta()," ",20),Util.rPad(myJunta.getSigJunta()," ",10),indTipo,"","","","","","","","","");
//				if( !sCmd.substring(0,3).equals("000") )
//					return false;
			}
			else   {
				sCmd  = "UPDATE TSMI_JUNTA set "+
				"sig_junta ='"+myJunta.getSigJunta()+"'," +
				"nom_junta ='"+myJunta.getNomJunta()+"', " +
				"ind_tipo  ='"+myJunta.getIndTipo()+"' " +
				" where cod_junta = '"+myJunta.getCodJunta()+"' " ;
				stmt.execute(sCmd);
//				DaoBroker broker = DaoBrokerFactory.getInstance();
//				sCmd = broker.Transacao086(Util.lPad(myJunta.getCodOrgao(),"0",6),Util.lPad(myJunta.getCodJunta(),"0",6),"2",Util.lPad("","0",6),Util.rPad(myJunta.getNomJunta()," ",20),Util.rPad(myJunta.getSigJunta()," ",10),indTipo,"","","","","","","","","");
//				if( !sCmd.substring(0,3).equals("000") )
//					return false;
			} 
		}
		catch (Exception ex)   {
			return false;
		}
		return true;
	}
	/**
	 *-----------------------------------------------------------
	 * Delete em Junta
	 *-----------------------------------------------------------
	 */
	public boolean JuntaDelete(JuntaBean myJunta,Statement stmt) {
		try {
			String sCmd  = "DELETE TSMI_JUNTA where cod_junta = '"+myJunta.getCodJunta()+"'";
			stmt.execute(sCmd);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			sCmd = broker.Transacao086(Util.lPad(myJunta.getCodOrgao(),"0",6),Util.lPad(myJunta.getCodJunta(),"0",6),"3",Util.lPad("","0",6),Util.rPad(myJunta.getNomJunta()," ",20),Util.rPad(myJunta.getSigJunta()," ",10),"","","","","","","","","","");
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			return false;
		}
		return true;
	}
	
	
	/**-----------------------------------------------------------
	 * Le Junta
	 *-----------------------------------------------------------
	 */ 
	public String getJunta() throws DaoException {
		String myJunta ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_junta,cod_orgao,nom_junta, sig_junta from TSMI_JUNTA";
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myJunta += ",\""+rs.getString("cod_junta") +"\"";
				myJunta += ",\""+rs.getString("cod_orgao") +"\"";
				myJunta += ",\""+rs.getString("nom_junta") +"\"";
				myJunta += ",\""+rs.getString("sig_junta") +"\"";
			}
			myJunta = myJunta.substring(1,myJunta.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myJunta;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le Junta e carrega no Bean
	 *-----------------------------------------------------------
	 */
	public boolean JuntaLeBean(JuntaBean myJunta,int tpJun)
	throws DaoException {
		//synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT nom_junta,sig_junta,cod_orgao,ind_tipo from TSMI_JUNTA " ;
			sCmd += "WHERE cod_junta = '"+myJunta.getCodJunta()+"'" ;
			ResultSet rs = stmt.executeQuery(sCmd) ;
			myJunta.setMsgErro("") ;
			while (rs.next()) {
				myJunta.setNomJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setCodOrgao(rs.getString("cod_orgao"));
				myJunta.setIndTipo(rs.getString("Ind_Tipo"));
				myJunta.setPkid(myJunta.getCodJunta()) ;
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myJunta.setCodJunta("0") ;
			myJunta.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myJunta.setCodJunta("0") ;
			myJunta.setPkid("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	public Vector OrgaoGetJunta(String codOrgao) throws DaoException {
		Connection conn =null ;	
		Vector junta = new Vector();
		boolean retorno = true;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT cod_junta,nom_junta,sig_junta,ind_tipo"+
			" from TSMI_JUNTA "+
			" WHERE cod_orgao = '"+ codOrgao + "'"+
			" order by nom_junta";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while( rs.next() ) {
				JuntaBean myJunta = new JuntaBean() ;
				myJunta.setCodJunta(rs.getString("cod_junta"));
				myJunta.setNomJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setIndTipo(rs.getString("ind_tipo"));
				junta.addElement(myJunta) ;
			}    
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			retorno = false;
		}//fim do catch
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn); 
					if(!retorno)
						return null;
				}
				catch (Exception ey) { }
			}
		}
		return junta;
	}
	
	public void OrgaoGetJunta(ACSS.OrgaoBean myOrg)  
	throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT cod_junta,nom_junta,sig_junta,ind_tipo"+
			" from TSMI_JUNTA "+
			" WHERE cod_orgao = '"+ myOrg.getCodOrgao() + "'"+
			" order by nom_junta";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector junta = new Vector();
			while( rs.next() ) {
				JuntaBean myJunta = new JuntaBean() ;
				myJunta.setCodJunta(rs.getString("cod_junta"));
				myJunta.setNomJunta(rs.getString("nom_junta"));
				myJunta.setSigJunta(rs.getString("sig_junta"));
				myJunta.setIndTipo(rs.getString("ind_tipo"));
				
				junta.addElement(myJunta) ;
			}    
			myOrg.setJunta(junta) ;		
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) { 
			myOrg.setMsgErro("Orgao008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myOrg.setCodOrgao("0") ;
		}
		catch (Exception e) {
			myOrg.setMsgErro("Orgao008 - Leitura: " + e.getMessage());
			myOrg.setCodOrgao("0") ;
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public boolean OrgaoInsertJunta(ACSS.OrgaoBean myOrg, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			Vector JuntaAtual = myOrg.getJunta() ;
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< JuntaAtual.size(); i++) {
				JuntaBean myJunta = (JuntaBean)JuntaAtual.elementAt(i) ;
				if ("".equals(myJunta.getNomJunta().trim()) == false) 	{
					JuntaInsert(myJunta, stmt) ;
				}
				else {
					if ("".equals(myJunta.getCodJunta()) == false)
						JuntaDelete(myJunta, stmt) ;
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();		  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myOrg.setMsgErro(vErro);
		return retorno;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * C�digo Relativo a Relator
	 *-----------------------------------------------------------
	 */
	/**
	 *---------------------
	 * Insert em Relator
	 *---------------------
	 */
	public boolean RelatorInsert(RelatorBean myRelator, Statement stmt) {
		Connection conn = null ;		
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt_Rel = conn.createStatement();
			
			
			if (("".equals(myRelator.getCodRelator())) || ("0".equals(myRelator.getCodRelator())))  {
				String sCmd = "SELECT seq_tsmi_relator.nextval as codRelator FROM dual";
				ResultSet rs = stmt_Rel.executeQuery(sCmd);
				rs.next();
				myRelator.setCodRelator(rs.getString("codRelator"));
				rs.close();
				
				sCmd  = "INSERT INTO TSMI_RELATOR (cod_relator,";
				sCmd += "nom_relator,cod_junta,num_cpf,nom_titulo, ind_ativo,qtd_proc_distrib, num_ordem) ";
				sCmd += "VALUES (";
				sCmd += "'"+myRelator.getCodRelator()+"',";
				sCmd += "'"+myRelator.getNomRelator()+"',";
				sCmd += "'"+myRelator.getCodJunta()+"',";
				sCmd += "'"+myRelator.getNumCpf()+"',";
				sCmd += "'"+myRelator.getNomTitulo()+"'," ;
				sCmd += "'"+myRelator.getIndAtivo()+"'," ;
				sCmd += myRelator.getQtdProcDistrib()+"," ;
				//Inclui o Presidente no topo da lista
				if(myRelator.getNomTitulo().indexOf("PRESIDENTE")>=0)
					sCmd += "'"+myRelator.PRESIDENTE+"')";
				else
					sCmd += "'"+myRelator.RELATOR+"')";
				stmt_Rel.execute(sCmd);
//				DaoBroker broker = DaoBrokerFactory.getInstance();
//				sCmd = broker.Transacao087(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelator.getNomRelator()," ",40));
				stmt_Rel.close();				
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
		}
		catch (Exception ex){
			return false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return true;
	}	
	
	
	/**
	 *--------------------
	 * Delete em Relator
	 *--------------------
	 */
	public void RelatorDelete(RelatorBean myRelator,Statement stmt) throws DaoException {
		String sCmd = "";
		String numGuia = "";
		try {
			if(!myRelator.getNumCpf().equals(""))
			{
				sCmd = "SELECT COD_LOTE_RELATOR FROM TSMI_LOTE_RELATOR " +
				" WHERE NUM_CPF = '"+myRelator.getNumCpf()+"'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					numGuia = rs.getString("COD_LOTE_RELATOR");
				}
			}
			if(numGuia.equals("")&&!myRelator.getIndAtivo().equals("N"))
			{
				sCmd  = "DELETE TSMI_Relator where cod_relator = '"+myRelator.getCodRelator()+"'";
				stmt.execute(sCmd);
			}
			else {
                if(!numGuia.equals(""))
                    myRelator.setMsgErro("Relator vinculado em uma Guia.");
                else if(!myRelator.getIndAtivo().equals("N"))
                    myRelator.setMsgErro("Relator inativo.");
			}
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());	
		}
		catch (Exception ex){
			throw new DaoException(ex.getMessage());
		}
	}
	
	public boolean RelatorDelete(String codOrgao, RelatorBean myRelator,Statement stmt) throws DaoException {
		String sCmd = "";
		String numGuia = "";
		try {
			if(!myRelator.getNumCpf().equals(""))
			{
				sCmd = "SELECT COD_LOTE_RELATOR FROM TSMI_LOTE_RELATOR " +
				" WHERE NUM_CPF = '"+myRelator.getNumCpf()+"'";
				ResultSet rs = stmt.executeQuery(sCmd);
				while (rs.next())
				{
					numGuia = rs.getString("COD_LOTE_RELATOR");
				}
			}
			
			if(numGuia.equals("")&&!myRelator.getIndAtivo().equals("N"))
			{
				sCmd  = "DELETE TSMI_Relator where cod_relator = '"+myRelator.getCodRelator()+"'";
				stmt.execute(sCmd);
				DaoBroker broker = DaoBrokerFactory.getInstance();
				sCmd = broker.Transacao087(Util.lPad(codOrgao,"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"3",Util.lPad("","0",11),Util.rPad(myRelator.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )
					return false;
			}
			else {
				if(!numGuia.equals(""))
					myRelator.setMsgErro("Relator vinculado em uma Guia.");
				else if(!myRelator.getIndAtivo().equals("N"))
					myRelator.setMsgErro("Relator inativo.");
				return false;
			}
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	/**-------------
	 * Ler Relator
	 *---------------
	 */ 
	public String getRelator() throws DaoException {
		String myRelator = "";
		Connection conn = null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_relator,cod_junta," +
			"ind_ativo, nom_relator,num_cpf from TSMI_RELATOR";
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			
			while (rs.next()){
				myRelator += ",\""+rs.getString("cod_relator") +"\"";
				myRelator += ",\""+rs.getString("cod_junta") +"\"";
				myRelator += ",\""+rs.getString("nom_relator") +"\"";
				myRelator += ",\""+rs.getString("num_cpf") +"\"";
				myRelator += ",\""+rs.getString("ind_ativo") +"\"";
			}
			myRelator = myRelator.substring(1,myRelator.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myRelator;
	}
	
	public List getRelatores(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorId) throws DaoException {
		Connection conn =null ;	
		List relator = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo "+
			"FROM TSMI_RELATOR r,TSMI_JUNTA j "+
			"where r.cod_junta=j.cod_junta and j.ind_tipo='"+TipoJunta+"' AND "+
			"j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' "+
			" order by nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				relator.add(myRelator) ;
			}    	
			RelatorId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	
	
	

	public List getRelatoresAtivos(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorBeanId) throws DaoException {
		Connection conn =null ;	
		List relator = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo "+
			"FROM TSMI_RELATOR r,TSMI_JUNTA j "+
			"where r.cod_junta=j.cod_junta and j.ind_tipo='"+TipoJunta+"' AND "+
			"j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' AND "+
			"r.ind_ativo='S' "+
			" order by nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				relator.add(myRelator);
			}    	
			RelatorBeanId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
	
	
	
	public String getRelatores(JuntaBean myJunta)  throws DaoException {
		String myRelator ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			// verificar se � admnistrador do Controle de acesso
			
			String	sCmd     = "select cod_relator,nom_relator from TSMI_RELATOR" +								   	
			" WHERE cod_Junta="+myJunta.getCodJunta()+								  
			" order by NOM_RELATOR";							
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()){
				
				myRelator += ",\""+rs.getString("cod_relator") +"\"";
				myRelator += ",\""+rs.getString("nom_relator") +"\"";							
			}
			myRelator = myRelator.substring(1,myRelator.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myRelator;
	}  
	
	
	
	public List getRelatoresAutomatica(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorId) throws DaoException {
		Connection conn =null ;	
		List relator = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT LPAD(trim(j.cod_junta),6,0)cod_junta, lpad(trim(r.num_cpf),11,'0') num_cpf,"+
			"j.sig_junta,r.nom_relator, r.cod_relator, r.ind_Ativo,r.qtd_proc_distrib "+
			"FROM TSMI_RELATOR r,TSMI_JUNTA j "+
			"where r.cod_junta=j.cod_junta and j.ind_tipo='"+TipoJunta+"' AND "+
			"j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' AND "+
			"r.ind_ativo='S' "+
			" order by qtd_proc_distrib,nom_relator";
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setSigJunta(rs.getString("sig_junta"));
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				myRelator.setQtdProcDistrib(rs.getInt("qtd_proc_distrib"));
				relator.add(myRelator) ;
			}    	
			RelatorId.setRelatores(relator);
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return relator;
	}
		
	
	
	public boolean JuntaInsertRelator(JuntaBean myJunta, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			Vector RelatorAtual = myJunta.getRelator() ;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< RelatorAtual.size(); i++) {
				RelatorBean myRelator = (RelatorBean)RelatorAtual.elementAt(i) ;
				if ("".equals(myRelator.getNomRelator().trim()) == false) 	{
					RelatorInsert(myRelator, stmt) ;
				}
				else {
					if ("".equals(myRelator.getCodRelator()) == false) RelatorDelete(myRelator, stmt) ;
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else	  	 conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myJunta.setMsgErro(vErro);
		return retorno;
	}
	
	public void JuntaGetRelator(JuntaBean myJunta)  
	throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT cod_relator,nom_relator,lpad(num_cpf,11,'0') num_cpf, ind_ativo";
			sCmd += " from TSMI_RELATOR ";
			sCmd += " WHERE cod_junta = '"+ myJunta.getCodJunta() + "'";
			sCmd += " order by nom_relator";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			Vector relator = new Vector();
			while( rs.next() ) {
				RelatorBean myRelator = new RelatorBean() ;
				myRelator.setCodRelator(rs.getString("cod_relator"));
				myRelator.setNomRelator(rs.getString("nom_relator"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
				relator.addElement(myRelator) ;
			}    
			myJunta.setRelator(relator) ;		
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) { 
			myJunta.setMsgErro("Junta008 - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myJunta.setCodOrgao("0") ;
		}
		catch (Exception e) {
			myJunta.setMsgErro("Junta008 - Leitura: " + e.getMessage());
			myJunta.setCodOrgao("0") ;
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		
	}
	
	public boolean RelatorExiste(RelatorBean auxClasse) throws sys.DaoException {
		
		boolean existe = false;
		boolean eConnLocal = false;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String where = "WHERE cod_relator ='" + auxClasse.getCodRelator() + "' and cod_junta = '"+ auxClasse.getCodJunta()+"'" ;
			String sCmd = "SELECT COD_RELATOR from TSMI_RELATOR " + where;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe = true;
			}
			rs.close();
			stmt.close();
			stmt = null;
			
		} catch (SQLException e) {
			existe = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			existe = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (eConnLocal) {
						conn.rollback();
						serviceloc.setReleaseConnection(conn);
					}
				} catch (Exception ey) {
				}
			}
		}
		return existe;
	}
	/**
	 *-----------------------------------------------------------
	 * Le Junta e carrega no Bean
	 *-----------------------------------------------------------
	 */
	public boolean RelatorLeBean(RelatorBean myRelator,String chave)
	throws DaoException {
		//synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT nom_relator,cod_junta,num_cpf,cod_relator, ind_ativo from TSMI_RELATOR " ;
			
			if ("CPF".equals(chave)) sCmd +=" where cod_junta = '"+myRelator.getCodJunta()+"' and "+ 
			" num_cpf = '"+myRelator.getNumCpf()+"'" ;
			else sCmd += " where cod_relator = '"+myRelator.getCodRelator()+"'" ;
			ResultSet rs = stmt.executeQuery(sCmd) ;
			myRelator.setMsgErro("") ;
			while (rs.next()) {
				myRelator.setNomRelator(rs.getString("nom_Relator"));
				myRelator.setCodJunta(rs.getString("cod_junta"));
				myRelator.setNumCpf(rs.getString("num_cpf"));
				myRelator.setCodRelator(rs.getString("cod_Relator"));
				myRelator.setPkid(myRelator.getCodRelator()) ;
				myRelator.setIndAtivo(rs.getString("ind_ativo"));
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myRelator.setCodJunta("0") ;
			myRelator.setCodRelator("0") ;			
			myRelator.setPkid("0") ;
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myRelator.setCodJunta("0") ;
			myRelator.setCodRelator("0") ;				
			myRelator.setPkid("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/**
	 *-----------------------------------------------------------
	 * C�digo Relativo a Responsavel Parecer Juridico
	 *-----------------------------------------------------------
	 */
	
	
	/**
	 *----------------------------------------
	 * Insert em Responsavel Parecer Juridico
	 *----------------------------------------
	 */
	
	public boolean RespParecerInsert(RespJuridicoBean myRespParecer, Statement stmt) throws DaoException {
		try {
			String sCmd = "SELECT seq_tsmi_resp_parecer.nextval as codRespJuridico FROM dual";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs.next();
			myRespParecer.setCodRespParecer(rs.getString("codRespJuridico"));
			rs.close();
			sCmd  = "INSERT INTO TSMI_RESP_PARECER (cod_resp_parecer,";
			sCmd += "nom_resp_parecer,cod_orgao,num_cpf) " ;
			sCmd += "VALUES ( " ;
			sCmd += "'"+ myRespParecer.getCodRespParecer()+"'," ;
			sCmd += "'"+ myRespParecer.getNomRespParecer()+"'," ;
			sCmd += "'"+myRespParecer.getCodOrgao()+"',";
			sCmd += "'"+myRespParecer.getNumCpf()+"')";
			stmt.execute(sCmd);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			sCmd = broker.Transacao088(Util.lPad(myRespParecer.getCodOrgao(),"0",6),Util.lPad(myRespParecer.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespParecer.getNomRespParecer()," ",43));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	public boolean RespParecerUpdate(RespJuridicoBean myRespParecer, Statement stmt){
		try{
			String sCmd  = "UPDATE TSMI_RESP_PARECER set ";
			sCmd += "nom_resp_parecer ='"+myRespParecer.getNomRespParecer()+"'," ;
			sCmd += "cod_orgao ='"+myRespParecer.getCodOrgao()+"', " ;
			sCmd += "num_cpf ='"+myRespParecer.getNumCpf()+"' " ;
			sCmd += " where cod_resp_parecer = '"+myRespParecer.getCodRespParecer()+"' " ;
			stmt.execute(sCmd);
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	public boolean insertRespParecer(Vector vErro,List respJuridico,List respJuridico_aux) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			RespJuridicoBean myRespJuridico,myRespJuridico_aux;
			for (int i = 0 ; i< respJuridico.size(); i++) {
				myRespJuridico = (RespJuridicoBean)respJuridico.get(i);
				myRespJuridico_aux = (RespJuridicoBean)respJuridico_aux.get(i);
				if( (myRespJuridico.getNomRespParecer().equals(myRespJuridico_aux.getNomRespParecer())) && (myRespJuridico.getNumCpf().equals(myRespJuridico_aux.getNumCpf())) )
					continue;
				
				if ("".equals(myRespJuridico.getNomRespParecer().trim()) == false){
					if(myRespJuridico.getCodRespParecer().equals("")){	
						if( RespParecerInsert(myRespJuridico, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro de inser��o do Respons�vel Jur�dico: " + myRespJuridico.getNomRespParecer() + "\n");
							retorno = false;
						}
					}
					else{
						DaoBroker broker = DaoBrokerFactory.getInstance();
						String sCmd = "";
						if( !myRespJuridico.getNumCpf().equals(myRespJuridico_aux.getNumCpf()) ){
							sCmd = broker.Transacao088(Util.lPad(myRespJuridico_aux.getCodOrgao(),"0",6),Util.lPad(myRespJuridico_aux.getNumCpf(),"0",11),"3",Util.lPad("","0",17),Util.rPad(myRespJuridico_aux.getNomRespParecer()," ",43));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de exclus�o do Respons�vel Jur�dico Broker: " + myRespJuridico.getNomRespParecer() + "\n");	
								retorno = false;
								continue;
							}          		
							sCmd = broker.Transacao088(Util.lPad(myRespJuridico.getCodOrgao(),"0",6),Util.lPad(myRespJuridico.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespJuridico.getNomRespParecer()," ",43));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de inser��o do Respons�vel Jur�dico Broker: " + myRespJuridico.getNomRespParecer() + "\n");	
								sCmd = broker.Transacao088(Util.lPad(myRespJuridico_aux.getCodOrgao(),"0",6),Util.lPad(myRespJuridico_aux.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespJuridico_aux.getNomRespParecer()," ",43));
								retorno = false;
								continue;
							}
						}
						else{
							sCmd = broker.Transacao088(Util.lPad(myRespJuridico.getCodOrgao(),"0",6),Util.lPad(myRespJuridico.getNumCpf(),"0",11),"2",Util.lPad("","0",17),Util.rPad(myRespJuridico.getNomRespParecer()," ",43));
							if( !sCmd.substring(0,3).equals("000") ){
								vErro.add("Erro de atualiza��o do Respons�vel Jur�dico no Broker: " + myRespJuridico.getNomRespParecer() + "\n");	
								retorno = false;
								continue;
							}
						}
						if( RespParecerUpdate(myRespJuridico, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro de atualiza��o do Relator: " + myRespJuridico.getNomRespParecer() + "\n");
							retorno = false;
						}
					}
				}
				else {
					if ("".equals(myRespJuridico.getCodRespParecer()) == false) 
						if( RespParecerDelete(myRespJuridico_aux, stmt) ){
							conn.commit();
							respJuridico.remove(i);
							respJuridico_aux.remove(i);
							i--;
						}
						else{
							conn.rollback();
							vErro.add("Erro de exclus�o do Relator: " + myRespJuridico.getNomRespParecer());
							retorno = false;
						}
				}
			}
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if(!retorno)
						conn.rollback();  
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	
	
	/**
	 *---------------------------------------
	 * Delete em Responsavel Parecer Juridico
	 *---------------------------------------
	 */
	public boolean RespParecerDelete(RespJuridicoBean myRespParecer,Statement stmt){
		try {
			String sCmd  = "DELETE TSMI_RESP_PARECER where cod_resp_parecer = '"+myRespParecer.getCodRespParecer()+"'";
			stmt.execute(sCmd);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			sCmd = broker.Transacao088(Util.lPad(myRespParecer.getCodOrgao(),"0",6),Util.lPad(myRespParecer.getNumCpf(),"0",11),"3",Util.lPad("","0",17),Util.rPad(myRespParecer.getNomRespParecer()," ",43));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	
	/**--------------------------------
	 * Ler Responsavel Parecer Juridico
	 *----------------------------------
	 */ 
	public String getRespParecer() throws DaoException {
		String myRespParecer = "";
		Connection conn = null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_resp_parecer,cod_junta,nom_resp_parecer,num_cpf from TSMI_RESP_PARECER";
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			
			while (rs.next()){
				myRespParecer += ",\""+rs.getString("cod_resp_parecer") +"\"";
				myRespParecer += ",\""+rs.getString("cod_junta") +"\"";
				myRespParecer += ",\""+rs.getString("nom_resp_parecer") +"\"";
				myRespParecer += ",\""+rs.getString("num_cpf") +"\"";
			}
			myRespParecer = myRespParecer.substring(1,myRespParecer.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myRespParecer;
	}
	
	public boolean JuntaInsertRespParecer(JuntaBean myJunta, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			Vector RespParecerAtual = myJunta.getRespParecer() ;
			Vector RespParecerCorrespondente = myJunta.getRespParecerGravado();
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< RespParecerAtual.size(); i++) {
				RespJuridicoBean respParecer = (RespJuridicoBean)RespParecerAtual.elementAt(i) ;
				RespJuridicoBean respParecerCorresp = (RespJuridicoBean)RespParecerCorrespondente.elementAt(i) ;
				
				if ("".equals(respParecer.getNomRespParecer().trim()) == false) 	{
					RespParecerInsert(respParecer, stmt) ;
				}
				else {
					if ("".equals(respParecerCorresp.getCodRespParecer())==false){
						if("           ".equals(respParecer.getCodRespParecer())){
							RespParecerDelete(respParecerCorresp, stmt) ;
						}
					}
				}
			}
			// fechar a transa��o - commit ou rollback
			
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myJunta.setMsgErro(vErro);
		return retorno;
	}
	
	public List JuntaGetRespParecer(RespJuridicoBean myRespJuridico) throws DaoException {
		Connection conn =null ;					
		ArrayList respParecer = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT cod_resp_parecer,nom_resp_parecer,lpad(num_cpf,11,'0') num_cpf";
			sCmd += " from TSMI_RESP_PARECER ";
			sCmd += " WHERE cod_orgao = '"+ myRespJuridico.getCodOrgao() + "'";
			sCmd += " order by nom_resp_parecer";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			
			while( rs.next() ) {
				RespJuridicoBean myRespParecer = new RespJuridicoBean() ;
				myRespParecer.setCodRespParecer(rs.getString("cod_resp_parecer"));
				myRespParecer.setNomRespParecer(rs.getString("nom_resp_parecer"));
				myRespParecer.setNumCpf(rs.getString("num_cpf"));
				myRespParecer.setCodOrgao(myRespJuridico.getCodOrgao());
				respParecer.add(myRespParecer) ;
			}    		
			rs.close();
			stmt.close();
		}//fim do try
		catch (Exception e) {
			myRespJuridico.setMsgErro("RespParecer008 - Leitura: " + e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return respParecer;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le Parametros do Orgao e carrega no Bean
	 *-----------------------------------------------------------
	 */
	public boolean ParamOrgLeBean(REC.ParamOrgBean myParam)
	throws DaoException {
		//synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT nom_Descricao,nom_Parametro,val_parametro,cod_Orgao, num_ordem from TCAU_PARAM_ORGAO ";
			sCmd += "WHERE cod_Parametro = '" + myParam.getCodParametro() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myParam.setMsgErro("");
			while (rs.next()) {
				myParam.setNomDescricao(rs.getString("nom_Descricao"));
				myParam.setNomParam(rs.getString("nom_Parametro"));
				myParam.setValParametro(rs.getString("val_Parametro"));
				myParam.setCodOrgao(rs.getString("cod_Orgao"));
				myParam.setNumOrdem(rs.getString("num_Ordem"));
				myParam.setPkid(myParam.getCodParametro());
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false;
			myParam.setCodParametro("0");
			myParam.setPkid("0");
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false;
			myParam.setCodParametro("0");
			myParam.setPkid("0");
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		return bOk;
	}
	/**
	 *-----------------------------------------------------------
	 * Le OrgaoGetParam
	 *-----------------------------------------------------------
	 */
	public void OrgaoGetParam(ACSS.OrgaoBean myOrg) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT cod_parametro,nom_parametro,nom_descricao,val_parametro,num_ordem, cod_grupo ";
			sCmd += " from TCAU_PARAM_ORGAO";
			sCmd += " WHERE cod_orgao = '" + myOrg.getCodOrgao() + "'";
			
			if(myOrg.getCodGrupo().trim().equals("") == false) 
				sCmd += " and cod_grupo = " + myOrg.getCodGrupo();
			
			sCmd += " order by num_ordem";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector param = new Vector();
			while (rs.next()) {
				ParamOrgBean myParam = new ParamOrgBean();
				myParam.setCodParametro(rs.getString("cod_parametro"));
				myParam.setNomParam(rs.getString("nom_parametro"));
				myParam.setNomDescricao(rs.getString("nom_descricao"));
				myParam.setValParametro(rs.getString("val_parametro"));
				myParam.setNumOrdem(rs.getString("num_ordem"));
				myParam.setCodGrupo(rs.getString("cod_grupo"));
				param.addElement(myParam);
			}
			myOrg.setParametros(param);
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			myOrg.setMsgErro(
					"Orgao008p - Leitura do Banco de dados - SQL: "
					+ sqle.getMessage());
			myOrg.setCodOrgao("");
		}
		catch (Exception e) {
			myOrg.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			myOrg.setCodOrgao("");
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert evento
	 *-----------------------------------------------------------
	 */
	public boolean InsertEvento(TAB.EventoBean myEvento , Vector vErro) throws DaoException {

		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			for(int i=0; i < myEvento.getEvento().size(); i++){
				
				myEvento.getNomEvento();
				EventoBean evento = myEvento.getEvento(i);
			}
			
			
			//FAZER GETPARAMETROS
//			Vector eventoAtual = myEvento.getParametros();
//			// iniciar transa��o
//			conn.setAutoCommit(false);
//			for (int i = 0; i < eventoAtual.size(); i++) {
//				
//				EventoBean myParam = (EventoBean) eventoAtual.elementAt(i);
//				
//				if ("".equals(myParam.getNomEvento()) == false) {
//					ParamInsertEvento(myParam, stmt);
//				}
//				else {
//					if ("".equals(myParam.getCodEvento()) == false)
//						ParamInsertEvento(myParam, stmt);
//				}
//			}
//			// fechar a transa��o - commit ou rollback
//			if (retorno)
//				conn.commit();
//			else
//				conn.rollback();
//			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} //fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		myEvento.setMsgErro(vErro);
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert ParamInsert Evento
	 *-----------------------------------------------------------
	 */
	public void ParamInsertEvento(EventoBean myEvento , Statement stmt)
	throws DaoException {
		try {
			String codEvento = myEvento.getCodEvento();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codEvento)) || ("0".equals(codEvento))) {
				sCmd = "INSERT INTO TSMI_EVENTO (cod_evento,"+
				 "dsc_evento) "+
				"VALUES (seq_tcau_param_orgao.nextval,"+ "'" + myEvento.getNomEvento() + "'" + "," + myEvento.getCodEvento() + ")";		
				stmt.execute(sCmd);
			}else{
				sCmd = "UPDATE TSMI_EVENTO set "+
						 "dsc_evento ='" + myEvento.getNomEvento() + "',"+
						 " where cod_evento = '" + codEvento + "' ";
						stmt.execute(sCmd);	
			}
			
		sCmd = "SELECT cod_evento FROM TSMI_EVENTO "+
				"WHERE cod_evento ='"+ myEvento.getCodEvento()+ "' ";
				rs = stmt.executeQuery(sCmd);
		while (rs.next()) {
			myEvento.setCodEvento(rs.getString("cod_evento"));
		}
					
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert OrgaoInsertParam
	 *-----------------------------------------------------------
	 */
	public boolean OrgaoInsertParam(ACSS.OrgaoBean myOrg, Vector vErro)
	throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Vector paramAtual = myOrg.getParametros();
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < paramAtual.size(); i++) {
				ParamOrgBean myParam = (ParamOrgBean) paramAtual.elementAt(i);
				myParam.setCodGrupo(myOrg.getCodGrupo());
				
				if ("".equals(myParam.getNomDescricao()) == false) {
					ParamInsert(myParam, stmt);
				}
				else {
					if ("".equals(myParam.getCodParametro()) == false)
						ParamDelete(myParam, stmt);
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} //fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		myOrg.setMsgErro(vErro);
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert ParamInsert
	 *-----------------------------------------------------------
	 */
	public void ParamInsert(REC.ParamOrgBean myParam, Statement stmt)
	throws DaoException {
		try {
			String codParam = myParam.getCodParametro();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codParam)) || ("0".equals(codParam))) {
				sCmd = "INSERT INTO TCAU_PARAM_ORGAO (cod_Parametro,"+
				 "nom_Parametro,nom_Descricao,val_parametro,cod_Orgao," +
                 "num_ordem, nom_username_alt, cod_grupo) "+
				 "VALUES (seq_tcau_param_orgao.nextval,"+
				 "'" + myParam.getNomParam() + "',"+
				 "'" + myParam.getNomDescricao() + "',"+
				 "'" + myParam.getValParametro() + "',"+
				 "'" + myParam.getCodOrgao() + "',"+
				 "'" + myParam.getNumOrdem() + "', "+     
                 "'" + myParam.getNomUsrNameAlt() + "', "+
				 "'" + myParam.getCodGrupo() + "') ";
				stmt.execute(sCmd);
				if ("DIAS_EXPEDIR,DIAS_FOLGA_EXPEDIR,DIAS_FOLGA_DEFPREVIA,DIAS_FOLGA_DEFPENALIDADE,DIAS_FOLGA_DEF2AINSTANCIA,DIAS_DEF_PREVIA,DIAS_DEF_PENALIDADE,DIAS_DEF_2AINSTANCIA,COD_ASSUNTO_DEF_PREVIA,COD_ASSUNTO_PENALIDADE,COD_SECRETARIA".indexOf(myParam.getNomParam()) >= 0)
					this.ParamBroker(myParam,stmt,"1");				
			}
			else {
				sCmd = "UPDATE TCAU_PARAM_ORGAO set "+
				 "nom_Parametro ='" + myParam.getNomParam() + "',"+
				 "val_Parametro ='" + myParam.getValParametro() + "',"+
				 "nom_Descricao ='" + myParam.getNomDescricao() + "', "+
                 "nom_username_alt ='" + myParam.getNomUsrNameAlt() + "', "+
				 "num_ordem ='" + myParam.getNumOrdem() + "' "+
				 " where cod_Parametro = '" + codParam + "' ";
				stmt.execute(sCmd);
				if ("DIAS_EXPEDIR,DIAS_FOLGA_EXPEDIR,DIAS_FOLGA_DEFPREVIA,DIAS_FOLGA_DEFPENALIDADE,DIAS_FOLGA_DEF2AINSTANCIA,DIAS_DEF_PREVIA,DIAS_DEF_PENALIDADE,DIAS_DEF_2AINSTANCIA,COD_ASSUNTO_DEF_PREVIA,COD_ASSUNTO_PENALIDADE,COD_SECRETARIA".indexOf(myParam.getNomParam()) >= 0)
					this.ParamBroker(myParam,stmt,"2");
			}
			sCmd = "SELECT COD_PARAMETRO FROM TCAU_PARAM_ORGAO "+
			 "WHERE num_ordem ='"+ myParam.getNumOrdem()+
			 "' and cod_orgao='"+ myParam.getCodOrgao()+ "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myParam.setCodParametro(rs.getString("cod_Parametro"));
			}
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	/**
	 *-----------------------------------------------------------
	 * Delete em ParamDelete
	 *-----------------------------------------------------------
	 */
	public void ParamDelete(REC.ParamOrgBean myParam, Statement stmt)
	throws DaoException {
		try {
			String sCmd =
				"DELETE TCAU_PARAM_ORGAO where cod_parametro = '"
				+ myParam.getCodParametro()
				+ "'";
			stmt.execute(sCmd);
			if ("DIAS_EXPEDIR,DIAS_FOLGA_EXPEDIR,DIAS_FOLGA_DEFPREVIA,DIAS_FOLGA_DEFPENALIDADE,DIAS_FOLGA_DEF2AINSTANCIA,DIAS_DEF_PREVIA,DIAS_DEF_PENALIDADE,DIAS_DEF_2AINSTANCIA,COD_ASSUNTO_DEF_PREVIA,COD_ASSUNTO_PENALIDADE,COD_SECRETARIA".indexOf(myParam.getNomParam()) >= 0)
				this.ParamBroker(myParam,stmt,"3");
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Prepara Parametros para o Login 
	 *-----------------------------------------------------------
	 */
	public void PreparaParam(REC.ParamOrgBean myParam,
			ACSS.UsuarioBean myUsrLog)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT * FROM TCAU_PARAM_ORGAO where COD_ORGAO = '"
				+ myUsrLog.getCodOrgaoAtuacao()
				+ "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			List vPar = new ArrayList();
			List nPar = new ArrayList();
			String v = "";
			String n = "";
			while (rs.next()) {
				n = rs.getString("nom_parametro");
				if (n == null)
					n = "";
				v = rs.getString("val_parametro");
				if (v == null)
					v = "";
				vPar.add(v);
				nPar.add(n);
			}
			myParam.setCodOrgao(myUsrLog.getCodOrgaoAtuacao());			
			myParam.setNomParamOrgao(nPar);
			myParam.setValParamOrgao(vPar);
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		
	}
	
	public void CopiaParam(ACSS.OrgaoBean myOrg, ParamOrgBean myParam)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtCp = conn.createStatement();
			Vector vErro = new Vector();
			
			String sCmd = "SELECT nom_parametro,nom_descricao,val_parametro,num_ordem, cod_grupo "+
			"FROM TCAU_PARAM_ORGAO WHERE cod_orgao ='" + myOrg.getCodOrgao()+"'";
			
			if (myParam.getCodGrupo().trim().equals("") == false)
				sCmd += " and cod_grupo = " + myParam.getCodGrupo();
			
			ResultSet rs    = stmt.executeQuery(sCmd);
			String nomParam = "";
			String valParam = "";
			String nomDesc  = "";
			String numOrdem = "";
			String codGrupo = "";
			
			int nParamOrig  = 0;
			int nParamCopia = 0;			
			boolean existe  = false;
			while (rs.next()) {
				nomParam = rs.getString("nom_parametro") ;
				if (nomParam==null) nomParam="NULO";
				nomDesc = rs.getString("nom_descricao") ;
				if (nomDesc==null) nomDesc="";
				valParam = rs.getString("val_parametro") ;
				if (valParam==null) nomParam="";
				numOrdem = rs.getString("num_ordem") ;
				if (numOrdem==null) numOrdem="";
				codGrupo = rs.getString("cod_grupo") ;
				if (codGrupo==null) codGrupo="";
				
				nParamOrig++;
				sCmd = "SELECT nom_parametro FROM TCAU_PARAM_ORGAO WHERE cod_orgao = "
					+ myParam.getCodOrgaoCp() + " and nom_parametro = '" + nomParam+ "'";
				ResultSet rsCp = stmtCp.executeQuery(sCmd);
				existe = false;
				while (rsCp.next()) {
					existe = true;
					vErro.addElement("Par�metro "+nomParam+" j� existente no Org�o "+myParam.getCodOrgaoCp()+". \n");
				}
				if (existe == false){ 
					sCmd = "INSERT INTO TCAU_PARAM_ORGAO (cod_orgao,cod_parametro,nom_parametro,"+
					"nom_descricao, val_parametro, num_ordem, cod_grupo)"+
					"VALUES (" + myParam.getCodOrgaoCp() + ",seq_tcau_param_orgao.nextval,"+
					"'"+ nomParam+ "','" + nomDesc +"',"+
					"'"+ valParam+ "','"	+ numOrdem +"','" + codGrupo + "')";
					stmtCp.execute(sCmd);
					nParamCopia++;
				}
			}
			stmtCp.close();
			stmt.close();
			vErro.addElement("Total de Par�metros na origem "+nParamOrig+". Copiados: "+nParamCopia+". \n");
			myOrg.setMsgErro(vErro);
		}
		catch (SQLException sqle) {
			myOrg.setMsgErro(sqle.getMessage());
			myOrg.setCodOrgao("");
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			myOrg.setMsgErro(e.getMessage());
			myOrg.setCodOrgao("");
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {				}
			}
		}
	}
	
	public void ParamBroker(REC.ParamOrgBean myParam, Statement stmt, String acao) throws DaoException {
		
		String sCmd = "";
		try {
//			stmt.getConnection().setAutoCommit(false);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			if (acao.equals(("3"))) {
			}
			else {
				sCmd = "delete from tsmi_aux_param_orgao where cod_orgao = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "insert into tsmi_aux_param_orgao(cod_orgao) values('" + myParam.getCodOrgao() + "')";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_EXPDIR = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_EXPEDIR') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_FOLGA_EXPDIR = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_FOLGA_EXPEDIR') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_FOLGA_DEFPREVIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_FOLGA_DEFPREVIA') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_FOLGA_1AINSTANCIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_FOLGA_DEFPENALIDADE') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_FOLGA_2AINSTANCIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_FOLGA_DEF2AINSTANCIA') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_DEF_PREVIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_DEF_PREVIA') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_1INSTANCIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_DEF_PENALIDADE') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    DIAS_2INSTANCIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'DIAS_DEF_2AINSTANCIA') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    COD_ASSUNTO_DEF_PREVIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'COD_ASSUNTO_DEF_PREVIA') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    COD_ASSUNTO_PENALIDADE = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'COD_ASSUNTO_PENALIDADE') " +
				"WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				sCmd = "UPDATE TSMI_AUX_PARAM_ORGAO A " +
				"SET    COD_SECRETARIA = " +
				"(SELECT VAL_PARAMETRO	 FROM   TCAU_PARAM_ORGAO P " +
				" WHERE  P.COD_ORGAO = A.COD_ORGAO AND    NOM_PARAMETRO = 'COD_SECRETARIA') " +
				" WHERE COD_ORGAO = '" + myParam.getCodOrgao() + "'";
				stmt.execute(sCmd);		
				
				sCmd = "select cod_orgao, nvl(dias_expdir,1) dias_expdir, nvl(dias_folga_expdir,1) dias_folga_expdir, " +
				"nvl(dias_folga_defprevia,1) dias_folga_defprevia, nvl(dias_julg_1ainstancia,1) dias_julg_1ainstancia, " +
				"nvl(dias_folga_1ainstancia,1) dias_folga_1ainstancia, nvl(dias_folga_2ainstancia,1) dias_folga_2ainstancia, " +
				"nvl(dias_def_previa,1) dias_def_previa, nvl(dias_1instancia,1) dias_1instancia, " +
				"nvl(dias_2instancia,1) dias_2instancia, nvl(cod_secretaria,'XXX') cod_secretaria, " +
				"nvl(cod_assunto_def_previa,1) cod_assunto_def_previa, " +
				"nvl(cod_assunto_penalidade,1) cod_assunto_penalidade from tsmi_aux_param_orgao where cod_orgao = '" + myParam.getCodOrgao() + "'";
				ResultSet rsPar = stmt.executeQuery(sCmd);
				String sParteVar = "";
				while (rsPar.next()) {
					sParteVar = sys.Util.lPad(rsPar.getString("cod_orgao"),"0",6);
					
					String auxDias = rsPar.getString("dias_expdir");
					int nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_folga_expdir");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_folga_defprevia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_julg_1ainstancia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_folga_1ainstancia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_folga_2ainstancia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_def_previa");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_1instancia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					auxDias = rsPar.getString("dias_2instancia");
					nDias = Integer.parseInt(auxDias);
					if (nDias > 999) auxDias = "999";
					sParteVar += sys.Util.lPad(auxDias,"0",3);
					
					sParteVar += sys.Util.rPad(rsPar.getString("cod_secretaria")," ",3).substring(0,3); 
					
					auxDias = rsPar.getString("cod_assunto_def_previa");
					if (auxDias.trim().length()>0)
					{
					  nDias = Integer.parseInt(auxDias);
					  if (nDias > 9999) auxDias = "9999";
					}
					sParteVar += sys.Util.lPad(auxDias,"0",4);
					
					auxDias = rsPar.getString("cod_assunto_penalidade");
					if (auxDias.trim().length()>0)
					{
					  nDias = Integer.parseInt(auxDias);
					  if (nDias > 9999) auxDias = "9999";
					}
					sParteVar += sys.Util.lPad(auxDias,"0",4);
					
					sParteVar += acao;
				}
				String result = broker.chamaTransBRK("089",sParteVar,"                 ");
				if (! result.substring(0,3).equals("000"))
					throw new DaoException("Erro na atualiza��o da Base: " + result.substring(0,3));
			}
			
		}
		catch (Exception e){
			throw new DaoException(e.getMessage());			
		}
		
	}
	/**
	 *-----------------------------------------------------------
	 * Fim DAOTABELA de Parametros de Orgao
	 *-----------------------------------------------------------
	 */
	
	
	/**---------------------------------------------------------
	 * M�todos relacionados ao ParamUFBean
	 *-----------------------------------------------------------*/
	
	/**-----------------------------------------------------------
	 * Le Parametros do Orgao e carrega no Bean
	 *-----------------------------------------------------------*/
	public boolean ParamUFLeBean(REC.ParamUFBean myParam)
	throws DaoException {
		//synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT nom_Parametro,val_parametro,cod_uf from TCAU_PARAM_UF ";
			sCmd += "WHERE cod_param_uf = '" + myParam.getCodParametroUF() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myParam.setMsgErro("");
			while (rs.next()) {
				myParam.setNomParam(rs.getString("nom_Parametro"));
				myParam.setValParametro(rs.getString("val_Parametro"));
				myParam.setCodUF(rs.getString("cod_uf"));
				myParam.setPkid(myParam.getCodParametroUF());
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false;
			myParam.setCodParametroUF("0");
			myParam.setPkid("0");
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false;
			myParam.setCodParametroUF("0");
			myParam.setPkid("0");
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		return bOk;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insert ParamInsert
	 *-----------------------------------------------------------
	 */
	public void ParamInsertUF(REC.ParamUFBean myParam, Statement stmt)
	throws DaoException {
		try {
			String codParam = myParam.getCodParametroUF();
			String sCmd = "";
			ResultSet rs;
			if (("".equals(codParam)) || ("0".equals(codParam))) {
				sCmd = "INSERT INTO TCAU_PARAM_UF (cod_Param_uf,";
				sCmd += "nom_Parametro,val_parametro,cod_uf) ";
				sCmd += "VALUES (seq_tcau_param_uf.nextval,";
				sCmd += "'" + myParam.getNomParam() + "',";
				sCmd += "'" + myParam.getValParametro() + "',";
				sCmd += "'" + myParam.getCodUF() + "')";
				stmt.execute(sCmd);
			}
			else {
				sCmd = "UPDATE TCAU_PARAM_UF set ";
				sCmd += "nom_Parametro ='" + myParam.getNomParam() + "',";
				sCmd += "val_Parametro ='" + myParam.getValParametro() + "',";
				sCmd += "cod_uf ='" + myParam.getCodUF() + "', ";
				sCmd += " where cod_Param_uf = '" + codParam + "' ";
				stmt.execute(sCmd);
			}
			sCmd = "SELECT cod_param_uf FROM TCAU_PARAM_UF ";
			sCmd += "WHERE cod_uf ='"+ myParam.getCodUF()+ "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myParam.setCodParametroUF(rs.getString("cod_Param_uf"));
			}
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 *-----------------------------------------------------------
	 * Prepara Parametros para o Login 
	 *-----------------------------------------------------------
	 */
	public void PreparaParam(REC.ParamUFBean myParam,
			String codUF)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT * FROM TCAU_PARAM_UF where COD_UF = '"	+ codUF	+ "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			List vPar = new ArrayList();
			List nPar = new ArrayList();
			String v = "";
			String n = "";
			while (rs.next()) {
				n = rs.getString("nom_parametro");
				if (n == null)
					n = "";
				v = rs.getString("val_parametro");
				if (v == null)
					v = "";
				vPar.add(v);
				nPar.add(n);
			}
			myParam.setCodUF(codUF);			
			myParam.setNomParamUF(nPar);
			myParam.setValParamUF(vPar);
			
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		
	}
	
	
	
	/****
	 *-----------------------------------------------------------
	 * Le Rejeito e carrega no Bean
	 *-----------------------------------------------------------
	 */
	/**--------------------------------------
	 * Relativo ao ConteudoEmailBean
	 ---------------------------------------*/
	
	//Consulta conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public ArrayList consultaConteudoEmail(String[] campos, String[] valores) throws sys.DaoException{
		ConteudoEmailBean emailBean = null;
		ArrayList emailList = new ArrayList();
		Connection conn = null;
		try {
			String sql = "SELECT COD_EVENTO, COD_ORGAO, TXT_EMAIL," +
			" NOM_REMETENTE, TXT_ASSUNTO, NOM_LOGIN, TXT_SENHA" +
			" FROM TSMI_EMAIL_EVENTO";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()){
				emailBean = new ConteudoEmailBean();
				emailBean.setCodOrgao(rs.getString("COD_ORGAO"));
				emailBean.setCodEvento(rs.getString("COD_EVENTO"));
				emailBean.setAssunto(rs.getString("TXT_ASSUNTO"));
				emailBean.setRemetente(rs.getString("NOM_REMETENTE"));
				emailBean.setConteudoEmail(rs.getString("TXT_EMAIL"));
				emailBean.setLogin(rs.getString("NOM_LOGIN"));
				emailBean.setSenha(rs.getString("TXT_SENHA"));
				emailList.add(emailBean);
			}
			rs.close();
			stmt.close();
			
			
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: consultaConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return emailList;
	}
	
	// Altera conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public void alteraConteudoEmail(String[] campos, String[] valores, String conteudo,
			String nomRemetente, String txtAssunto, String login, String senha)
	throws sys.DaoException{
		
		Connection conn = null;
		try {
			String sql = "UPDATE TSMI_EMAIL_EVENTO SET " +
			"TXT_EMAIL = '" + conteudo + "', " +
			"NOM_REMETENTE = '" + nomRemetente + "', "+
			"TXT_ASSUNTO = '" + txtAssunto + "', " +
			"NOM_LOGIN = '" + login + "', " +
			"TXT_SENHA = '" + senha + "' " ;
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	// Insere conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public void insereConteudoEmail(String[] valores) throws sys.DaoException{
		ConteudoEmailBean emailBean = null;
		Connection conn = null;
		try {
			String sql = "INSERT INTO TSMI_EMAIL_EVENTO (COD_ORGAO,COD_EVENTO," +
			"TXT_EMAIL,NOM_REMETENTE,TXT_ASSUNTO, NOM_LOGIN, TXT_SENHA) ";
			sql += "VALUES ('" + valores[0] + "', '" + valores[1] + "', '" + valores[2] + "',"+
			"'"+ valores[3] + "', '" +  valores[4] + "','" + valores[5]+ "','" + valores[6]+"')";
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	//	Excluir conteudo de email na tabela TSMI_CONTEUDO_EMAIL
	public void excluiConteudoEmail(String[] campos, String[] valores) throws sys.DaoException{
		Connection conn = null;
		try {
			String sql = "DELETE FROM TSMI_EMAIL_EVENTO ";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
			stmt.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("REC.DaoTabelas: alteraConteudoEmail " + ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	
	
	/*--------------------------------
	 * DAO Relativo ao MotivoOrgBean
	 * @author Wellem 
	 ---------------------------------*/
	
	/**
	 * M�todo respons�vel por buscar o org�o
	 * relativo ao codigo Motivo informado.
	 * @param myOrg
	 * @throws DaoException
	 */
	public boolean MotivoLeBean(REC.MotivoBean myMotivo)
	throws DaoException {
		//synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT COD_MOTIVO,DSC_MOTIVO,COD_ORGAO FROM TSMI_MOTIVO ";
			sCmd += "WHERE COD_MOTIVO = '" + myMotivo.getCodMotivo() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			myMotivo.setMsgErro("");
			while (rs.next()) {
				myMotivo.setCodMotivo(rs.getString("COD_MOTIVO"));
				myMotivo.setDscMotivo(rs.getString("DSC_MOTIVO"));					
				myMotivo.setCodOrgao(rs.getString("cod_Orgao"));
				
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false;
			myMotivo.setCodMotivo("0");
			myMotivo.setPkid("0");
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false;
			myMotivo.setCodMotivo("0");
			myMotivo.setPkid("0");
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		return bOk;
	}
	
	/**
	 * M�todo respons�vel por buscar o codigo do Motivo
	 * relativo ao orgao informado.
	 * @param myOrg
	 * @throws DaoException
	 */
	public void GetMotivo(ACSS.OrgaoBean myOrg, MotivoBean motivoBean) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd ="SELECT COD_MOTIVO,DSC_MOTIVO,COD_ORGAO FROM TSMI_MOTIVO";				
			sCmd += " WHERE cod_orgao = '" + myOrg.getCodOrgao() + "' ";
			sCmd += " order by cod_motivo";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector motivo = new Vector();
			while (rs.next()) {
				MotivoBean myMotivo = new MotivoBean();
				myMotivo.setCodMotivo(rs.getString("COD_MOTIVO"));
				myMotivo.setDscMotivo(rs.getString("DSC_MOTIVO"));
				myMotivo.setCodOrgao(rs.getString("COD_ORGAO"));
				
				motivo.addElement(myMotivo);
			}
			motivoBean.setMotivo(motivo);
			motivoBean.setMotivoExcluido(motivo);
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			myOrg.setMsgErro(
					"Motivo008p - Leitura do Banco de dados - SQL: "
					+ sqle.getMessage());
			myOrg.setCodOrgao("");
		}
		catch (Exception e) {
			myOrg.setMsgErro("Motivo008p - Leitura: " + e.getMessage());
			myOrg.setCodOrgao("");
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
	}
	
	/**
	 * M�todo respons�vel por filtrar os dados
	 * encaminh�-los para inserir ou deletar.
	 * @param myOrg
	 * @param vErro
	 * @return
	 * @throws DaoException
	 */
	public boolean InsertMotivo(MotivoBean motivoBean,ACSS.OrgaoBean myOrg, Vector vErro)
	throws DaoException {
		boolean retorno = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Vector motivoAtual = motivoBean.getMotivo();
			Vector motivoExcluido = motivoBean.getMotivoExcluido();
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0; i < motivoAtual.size(); i++) {
				MotivoBean myMotivo = (MotivoBean) motivoAtual.elementAt(i);
				MotivoBean myMotivoExcluido = (MotivoBean) motivoExcluido.elementAt(i);
				if ("".equals(myMotivo.getDscMotivo()) == false) {
					MotivoInsert(myMotivo, stmt);
				}
				else {
					if ("".equals(myMotivoExcluido.getCodMotivo()) == false)
						if("".equals(myMotivo.getCodMotivo()))
							MotivoDelete(myMotivoExcluido, stmt);
				}
			}
			// fechar a transa��o - commit ou rollback
			if (retorno)
				conn.commit();
			else
				conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} //fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		myOrg.setMsgErro(vErro);
		return retorno;
	}
	
	/**
	 * Metodo respons�vel pela inclus�o ou atualizar  
	 * os dados na tabela TSMI_MOTIVO.
	 * @param myMotivo
	 * @param stmt
	 * @throws DaoException
	 */
	public void MotivoInsert(REC.MotivoBean myMotivo, Statement stmt)
	throws DaoException {
		try {
			String codMotivo = myMotivo.getCodMotivo();
			String sCmd = "";
			boolean existe = false;
			ResultSet rs;
			
			sCmd  = "SELECT COD_Motivo FROM TSMI_MOTIVO ";
			sCmd += " WHERE cod_Motivo ='" + myMotivo.getCodMotivo()+ "'";
			sCmd += " and cod_orgao='"+ myMotivo.getCodOrgao()+ "' ";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				existe=true;
			}
			
			if (existe==false){
				sCmd = "INSERT INTO TSMI_Motivo (seq_motivo,cod_Motivo,";
				sCmd += "dsc_Motivo,cod_Orgao) ";
				sCmd += "VALUES (seq_tsmi_motivo.nextval, '"+ myMotivo.getCodMotivo() + "',";					
				sCmd += "'" + myMotivo.getDscMotivo()+ "',";					
				sCmd += "'" + myMotivo.getCodOrgao() + "') ";				
				stmt.execute(sCmd);
				existe=false;
			}
			else {
				sCmd = "UPDATE TSMI_Motivo set ";
				sCmd += "cod_Motivo ='" + myMotivo.getCodMotivo() + "',";
				sCmd += " dsc_Motivo ='" + myMotivo.getDscMotivo() + "'";
				sCmd += " where cod_Motivo ='" + myMotivo.getCodMotivo() + "' ";
				stmt.execute(sCmd);
				existe=false;
			}
			
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 * M�todo Respons�vel por excluir os dados
	 * na tabela TSMI_MOTIVO.
	 * @param myMotivo
	 * @param stmt
	 * @throws DaoException
	 */
	public void MotivoDelete(REC.MotivoBean myMotivo, Statement stmt)
	throws DaoException {
		try {
			String sCmd =
				"DELETE TSMI_Motivo where cod_Motivo = '"
				+ myMotivo.getCodMotivo()
				+ "'";
			stmt.execute(sCmd);
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
	}
	
	/**
	 * M�todo respons�vel por consultar todos os
	 * dados contidos na tabela TSMI_MOTIVO.
	 * @param myMotivo
	 * @param myUsrLog
	 * @throws DaoException
	 */
	public void PreparaMotivo(REC.MotivoBean myMotivo,
			ACSS.UsuarioBean myUsrLog)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd =
				"SELECT * FROM TSMI_MOTIVO where COD_ORGAO = '"
				+ myUsrLog.getCodOrgaoAtuacao()
				+ "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			List vMot = new ArrayList();
			List nMot = new ArrayList();
			String v = "";
			String n = "";
			while (rs.next()) {
				n = rs.getString("DSC_MOTIVO");
				if (n == null)
					n = "";
				v = rs.getString("COD_MOTIVO");
				if (v == null)
					v = "";
				vMot.add(v);
				nMot.add(n);
			}
			myMotivo.setDscMotivoOrgao(nMot);
			myMotivo.setCodMotivoOrgao(vMot);
		}
		catch (SQLException e) {
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
		
	}
	
	/**
	 * M�todo respons�vel por copiar todos os motivos
	 * cadastrados em um org�o para outro.
	 * @param myOrg
	 * @param myMotivo
	 * @throws DaoException
	 */	
	public void CopiaMotivo(ACSS.OrgaoBean myOrg, MotivoBean myMotivo)
	throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			Statement stmtCp = conn.createStatement();
			Vector vErro = new Vector();
			
			String sCmd = "SELECT dsc_motivo,cod_motivo,cod_orgao "+
			"FROM TSMI_MOTIVO WHERE cod_orgao ='" + myOrg.getCodOrgao()+"'";
			ResultSet rs    = stmt.executeQuery(sCmd);
			String dscMotivo = "";
			String codMotivo = "";
			String codOrgao  = "";
			
			int nMotivoOrig  = 0;
			int nMotivoCopia = 0;			
			boolean existe  = false;
			while (rs.next()) {
				dscMotivo = rs.getString("dsc_motivo") ;
				if (dscMotivo==null) dscMotivo="NULO";
				codMotivo = rs.getString("cod_motivo") ;
				if (codMotivo==null) codMotivo="";
				codOrgao = rs.getString("cod_orgao") ;
				if (codOrgao==null) codOrgao="";					  
				nMotivoOrig++;
				sCmd = "SELECT dsc_motivo FROM TSMI_MOTIVO WHERE cod_orgao = "
					+ myMotivo.getCodOrgaoCp() + " and dsc_motivo = '" + dscMotivo+ "'";
				ResultSet rsCp = stmtCp.executeQuery(sCmd);
				existe = false;
				while (rsCp.next()) {
					existe = true;
					vErro.addElement("Motivo "+dscMotivo+" j� existente no �rg�o "+myMotivo.getCodOrgao()+". \n");
				}
				if (existe == false){ 
					sCmd = "INSERT INTO TSMI_MOTIVO (seq_motivo,cod_orgao,cod_motivo,dsc_motivo)"+							
					" VALUES (seq_tsmi_motivo.nextval, '" + myMotivo.getCodOrgaoCp() + "','" + codMotivo + "'," +
					"'"+ dscMotivo+ "')";
					
					stmtCp.execute(sCmd);
					nMotivoCopia++;
				}
			}
			stmtCp.close();
			stmt.close();
			vErro.addElement("Total de Motivos na origem "+nMotivoOrig+". Copiados: "+nMotivoCopia+". \n");
			myMotivo.setMsgErro(vErro);
		}
		catch (SQLException sqle) {
			myMotivo.setMsgErro(sqle.getMessage());
			myMotivo.setCodOrgao("");
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			myMotivo.setMsgErro(e.getMessage());
			myMotivo.setCodOrgao("");
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) {				}
			}
		}
	}
	
	/*--------------------------------
	 * FIM - DAO Relativo ao MotivoOrgBean
	 * @author Wellem 
	 ---------------------------------*/
	
	public List getRespJuridicoBroker(RespJuridicoBean RespJuridicoId) throws DaoException {
		List respJuridico = new ArrayList();
		try {
			String indContinuidade = Util.lPad("","0",17);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			RespJuridicoBean myRespJuridico;
			while(true){ 
				String sCmd = broker.Transacao088(Util.lPad(RespJuridicoId.getCodOrgao(),"0",6),Util.lPad("","9",11),"5",indContinuidade,Util.rPad(RespJuridicoId.getNomRespParecer()," ",43));
				if( !sCmd.substring(0,3).equals("000") && (!sCmd.substring(0,3).equals("017")) )
					return null;
				if( sCmd.substring(0,3).equals("017") )
					return respJuridico;	
				
				int ini, pos, fim;
				for(pos=0,ini=3,fim=9;(!sCmd.substring(ini,fim).equals(Util.lPad("","0",6))) && (pos<53);ini = fim, fim = ini+6,pos++){
					myRespJuridico = new RespJuridicoBean();
					myRespJuridico.setCodOrgao(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 11;
					myRespJuridico.setNumCpf(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 40;
					myRespJuridico.setNomRespParecer(sCmd.substring(ini,fim));
					respJuridico.add(myRespJuridico);
				}
				indContinuidade = sCmd.substring(ini,fim);
				if(pos!=53)
					break;
			}	
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		return respJuridico;
	}
	
	public boolean RespJuridicoUpdateBroker(RespJuridicoBean myRespJuridicoBroker, RespJuridicoBean myRespJuridicoBroker_aux){
		boolean retorno = true;
		String sCmd = "";
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			if( !myRespJuridicoBroker.getNumCpf().equals(myRespJuridicoBroker_aux.getNumCpf()) ){
				sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker_aux.getNumCpf(),"0",11),"3",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker_aux.getNomRespParecer()," ",43));
				if( !sCmd.substring(0,3).equals("000") )
					return false;
				
				sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker.getNomRespParecer()," ",43));
				if( !sCmd.substring(0,3).equals("000") ){	
					sCmd = broker.Transacao087(Util.lPad(myRespJuridicoBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker_aux.getCodRespParecer(),"0",6),Util.lPad(myRespJuridicoBroker_aux.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker_aux.getNomRespParecer()," ",43));
					return false;
				}
			}
			else{
				sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker.getNumCpf(),"0",11),"2",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker.getNomRespParecer()," ",43));
				if( !sCmd.substring(0,3).equals("000") )	
					return false;
			}
		}
		catch (Exception ex)   {
			return false;
		}
		return true;
	}
	
	public boolean RespJuridicoUpdate(RespJuridicoBean myRespParecer){
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd  = "UPDATE TSMI_RESP_PARECER set ";
			sCmd += "nom_resp_parecer ='"+myRespParecer.getNomRespParecer()+"'," ;
			sCmd += "cod_orgao ='"+myRespParecer.getCodOrgao()+"', " ;
			sCmd += "num_cpf ='"+myRespParecer.getNumCpf()+"' " ;
			sCmd += " where cod_resp_parecer = '"+myRespParecer.getCodRespParecer()+"' " ;
			stmt.execute(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean RespJuridicoUpdateBroker(RespJuridicoBean myRespJuridicoBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker.getNumCpf(),"0",11),"2",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker.getNomRespParecer()," ",43));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RespJuridicoDeleteBroker(RespJuridicoBean myRespJuridicoBroker){
		boolean retorno = true;
		try {
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker.getNumCpf(),"0",11),"3",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker.getNomRespParecer()," ",43));
			if( !sCmd.substring(0,3).equals("000") )
				return false;	
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RespJuridicoInsert(RespJuridicoBean myRespParecer) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT seq_tsmi_resp_parecer.nextval as codRespJuridico FROM dual";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs.next();
			myRespParecer.setCodRespParecer(rs.getString("codRespJuridico"));
			rs.close();
			sCmd  = "INSERT INTO TSMI_RESP_PARECER (cod_resp_parecer,";
			sCmd += "nom_resp_parecer,cod_orgao,num_cpf) " ;
			sCmd += "VALUES ( " ;
			sCmd += "'"+ myRespParecer.getCodRespParecer()+"'," ;
			sCmd += "'"+ myRespParecer.getNomRespParecer()+"'," ;
			sCmd += "'"+myRespParecer.getCodOrgao()+"',";
			sCmd += "'"+myRespParecer.getNumCpf()+"')";
			stmt.execute(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean RespJuridicoInsertBroker(RespJuridicoBean myRespJuridicoBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao088(Util.lPad(myRespJuridicoBroker.getCodOrgao(),"0",6),Util.lPad(myRespJuridicoBroker.getNumCpf(),"0",11),"1",Util.lPad("","0",17),Util.rPad(myRespJuridicoBroker.getNomRespParecer()," ",43));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RespJuridicoDelete(RespJuridicoBean myRespParecer) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd  = "DELETE TSMI_RESP_PARECER where cod_resp_parecer = '"+myRespParecer.getCodRespParecer()+"'";
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex){
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
    public List getRelator(String codJunta) throws DaoException {
      	Connection conn =null ;	
  		List relator = new ArrayList();
      	try {
      		conn = serviceloc.getConnection(MYABREVSIST) ;
      		Statement stmt = conn.createStatement();
      		
      		String sCmd = "SELECT cod_relator,nom_relator,lpad(num_cpf,11,'0') num_cpf, nom_titulo,ind_ativo ";
      		sCmd += " from TSMI_RELATOR ";
      		sCmd += " WHERE cod_junta = '"+ codJunta + "'";
      		sCmd += " order by nom_relator";
      		ResultSet rs = stmt.executeQuery(sCmd);     
      		
      		while( rs.next() ) {
      			RelatorBean myRelator = new RelatorBean() ;
      			myRelator.setCodJunta(codJunta);
      			myRelator.setCodRelator(rs.getString("cod_relator"));
      			myRelator.setNomRelator(rs.getString("nom_relator"));
      			myRelator.setNumCpf(rs.getString("num_cpf"));
      			myRelator.setNomTitulo(rs.getString("nom_titulo"));
                myRelator.setIndAtivo(rs.getString("ind_ativo"));
      			relator.add(myRelator) ;
      		}    	
      		rs.close();
      		stmt.close();
      	}//fim do try
      	catch (Exception e) {
      		throw new DaoException(e.getMessage());
      	}//fim do catch
      	finally {
      		if (conn != null) {
      			try { serviceloc.setReleaseConnection(conn); }
      			catch (Exception ey) { }
      		}
      	}
      	return relator;
      }
	
	public List getRelatorBroker(RelatorBean RelatorId) throws DaoException {
		List relator = new ArrayList();
		try {
			String indContinuidade = Util.lPad("","0",11);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			RelatorBean myRelator;
			while(true){ 
				String sCmd = broker.Transacao087(Util.lPad(RelatorId.getCodOrgao(),"0",6),Util.lPad(RelatorId.getCodJunta(),"0",6),Util.lPad(RelatorId.getNumCpf(),"9",11),"5",indContinuidade,Util.rPad(RelatorId.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") && (!sCmd.substring(0,3).equals("017")) )
					return null;
				if( sCmd.substring(0,3).equals("017") )
					return relator;	
				
				int ini, pos, fim;
				for(pos=0,ini=3,fim=9;(!sCmd.substring(ini,fim).equals(Util.lPad("","0",6))) && (pos<46);ini = fim, fim = ini+6,pos++){
					myRelator = new RelatorBean();
					myRelator.setCodOrgao(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim +6;
					myRelator.setCodJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 11;
					myRelator.setNumCpf(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 40;
					myRelator.setNomRelator(sCmd.substring(ini,fim));
					relator.add(myRelator);
				}
				indContinuidade = sCmd.substring(ini,fim);
				if(pos!=46)
					break;
			}	
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		return relator;
	}
	
	public boolean RelatorUpdateBroker(RelatorBean myRelatorBroker, RelatorBean myRelatorBroker_aux){
		boolean retorno = true;
		String sCmd = "";
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			if( !myRelatorBroker.getNumCpf().equals(myRelatorBroker_aux.getNumCpf()) ){
				sCmd = broker.Transacao087(Util.lPad(myRelatorBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker_aux.getCodJunta(),"0",6),Util.lPad(myRelatorBroker_aux.getNumCpf(),"0",11),"3",Util.lPad("","0",11),Util.rPad(myRelatorBroker_aux.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )
					return false;
				
				sCmd = broker.Transacao087(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") ){	
					sCmd = broker.Transacao087(Util.lPad(myRelatorBroker_aux.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker_aux.getCodJunta(),"0",6),Util.lPad(myRelatorBroker_aux.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelatorBroker_aux.getNomRelator()," ",40));
					return false;
				}
			}
			else{
				sCmd = broker.Transacao087(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"2",Util.lPad("","0",11),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
				if( !sCmd.substring(0,3).equals("000") )	
					return false;
			}
		}
		catch (Exception ex)   {
			return false;
		}
		return true;
	}
	
	public boolean RelatorUpdate(RelatorBean myRelator, Statement stmt) {    
		try{
			String sCmd  = "UPDATE TSMI_RELATOR set ";
			sCmd += "nom_relator ='"+myRelator.getNomRelator()+"',";
			sCmd += "cod_junta   ='"+myRelator.getCodJunta()+"', ";
			sCmd += "num_cpf     ='"+myRelator.getNumCpf()+"', ";
			sCmd += "nom_titulo  ='"+myRelator.getNomTitulo()+"',";
			sCmd += "ind_ativo   ='"+myRelator.getIndAtivo()+"',";
			//Manter o Presidente no topo da lista
			if(myRelator.getNomTitulo().indexOf("PRESIDENTE")>=0)
				sCmd += "num_ordem = '"+myRelator.PRESIDENTE+"'";
			else
				sCmd += "num_ordem = '"+myRelator.RELATOR+"'";
			sCmd += " where cod_relator = '"+myRelator.getCodRelator()+"' ";
			stmt.execute(sCmd);
		}
		catch (Exception ex){
			return false;
		}
		return true;
	}
	
	public boolean RelatorUpdate(RelatorBean myRelator){
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_RELATOR set "+
			"nom_relator ='"+myRelator.getNomRelator()+"'," +
			"num_cpf ='"+myRelator.getNumCpf()+"' " +
			" where cod_relator = '"+myRelator.getCodRelator()+"' " ;
			stmt.executeUpdate(sCmd);
			
			/*Michel*/
			stmt.close();
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean RelatorUpdateBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao087(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"2",Util.lPad("","0",11),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RelatorInsert(RelatorBean myJunta) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			sCmd = "SELECT seq_tsmi_relator.nextval as codRelator FROM dual";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs.next();
			myJunta.setCodRelator(rs.getString("codRelator"));
			rs.close();
			sCmd  = "INSERT INTO TSMI_RELATOR (cod_relator,"+
			"nom_relator,num_cpf,ind_ativo,cod_junta) " +
			"VALUES (" +
			"'"+myJunta.getCodRelator()+"'," +
			"'"+myJunta.getNomRelator()+"'," +
			"'"+myJunta.getNumCpf()+"',"+
			"'"+myJunta.getIndAtivo()+"',"+
			"'"+myJunta.getCodJunta()+"')";
			stmt.executeUpdate(sCmd);
			
			/*Michel*/
			stmt.close();
			
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean RelatorInsertBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {	
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao087(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RelatorDeleteBroker(RelatorBean myRelatorBroker){
		boolean retorno = true;
		try {
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao087(Util.lPad(myRelatorBroker.getCodOrgao(),"0",6),Util.lPad(myRelatorBroker.getCodJunta(),"0",6),Util.lPad(myRelatorBroker.getNumCpf(),"0",11),"3",Util.lPad("","0",11),Util.rPad(myRelatorBroker.getNomRelator()," ",40));
			if( !sCmd.substring(0,3).equals("000") )
				return false;	
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean RelatorDelete(RelatorBean myJunta) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd  = "DELETE TSMI_RELATOR where cod_relator = '"+myJunta.getCodRelator()+"'";
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex){
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean JuntaInsertRelator(Vector vErro, List relatores, List relatores_aux) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			RelatorBean myRelator,myRelator_aux;
			for (int i = 0 ; i< relatores.size(); i++) {
				myRelator = (RelatorBean)relatores.get(i);
				myRelator_aux = (RelatorBean)relatores_aux.get(i);
				String indAtivo = verificaSitRelator(myRelator);
				if( (myRelator.getNomRelator().equals(myRelator_aux.getNomRelator())) 
						&& (myRelator.getNumCpf().equals(myRelator_aux.getNumCpf())) 
						&& (myRelator.getNomTitulo().equals(myRelator_aux.getNomTitulo()))
						&& (indAtivo.equals(myRelator.getIndAtivo())))
					continue;
				
				if ("".equals(myRelator.getNomRelator().trim()) == false){
					if(myRelator.getCodRelator().equals("")){	
						if( RelatorInsert(myRelator, stmt) )
							conn.commit();
						else{
							conn.rollback();
							vErro.add("Erro de atualiza��o do Relator: " + myRelator.getNomRelator() + "\n");
							retorno = false;
						}
					}
//					else{
//						DaoBroker broker = DaoBrokerFactory.getInstance();
//						String sCmd = "";
//						if( !myRelator.getNumCpf().equals(myRelator_aux.getNumCpf()) ){
//							sCmd = broker.Transacao087(Util.lPad(myRelator_aux.getCodOrgao(),"0",6),Util.lPad(myRelator_aux.getCodJunta(),"0",6),Util.lPad(myRelator_aux.getNumCpf(),"0",11),"3",Util.lPad("","0",11),Util.rPad(myRelator_aux.getNomRelator()," ",40));
////							if( !sCmd.substring(0,3).equals("000") ){
////								vErro.add("Erro de exclus�o do Relator Broker: " + myRelator.getNomRelator() + "\n");	
////								retorno = false;
////								continue;
////							}          		
//							sCmd = broker.Transacao087(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelator.getNomRelator()," ",40));
////							if( !sCmd.substring(0,3).equals("000") ){
////								vErro.add("Erro de inser��o do Relator Broker: " + myRelator.getNomRelator() + "\n");	
////								sCmd = broker.Transacao087(Util.lPad(myRelator_aux.getCodOrgao(),"0",6),Util.lPad(myRelator_aux.getCodJunta(),"0",6),Util.lPad(myRelator_aux.getNumCpf(),"0",11),"1",Util.lPad("","0",11),Util.rPad(myRelator_aux.getNomRelator()," ",40));
////								retorno = false;
////								continue;
////							}
//						}
//						else{
//							sCmd = broker.Transacao087(Util.lPad(myRelator.getCodOrgao(),"0",6),Util.lPad(myRelator.getCodJunta(),"0",6),Util.lPad(myRelator.getNumCpf(),"0",11),"2",Util.lPad("","0",11),Util.rPad(myRelator.getNomRelator()," ",40));
//							if( !sCmd.substring(0,3).equals("000") ){
//								vErro.add("Erro de atualiza��o do Relator no Broker: " + myRelator.getNomRelator() + "\n");	
//								retorno = false;
//								continue;
//							}
//						}
//						if( RelatorUpdate(myRelator, stmt) )
//							conn.commit();
//						else{
//							conn.rollback();
//							vErro.add("Erro de atualiza��o do Relator: " + myRelator.getNomRelator() + "\n");
//							retorno = false;
//						}
//					}
				}
				else {
					if ("".equals(myRelator.getCodRelator()) == false) 
						if( RelatorDelete(myRelator_aux.getCodOrgao(), myRelator_aux, stmt) ){
							conn.commit();
							relatores.remove(i);
							relatores_aux.remove(i);
							i--;
						}
						else{
							conn.rollback();
							vErro.add("Erro de exclus�o do Relator: " + myRelator_aux.getNomRelator()+ " - " + myRelator_aux.getMsgErro());
							retorno = false;
						}
				}
			}
			/*Michel*/
			stmt.close();
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					if(!retorno)
						conn.rollback(); 
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	
	public List OrgaoGetJuntaBroker(String codOrgao) throws DaoException {
		List junta = new ArrayList();
		try {
			String indContinuidade = Util.rPad(""," ",6);
			DaoBroker broker = DaoBrokerFactory.getInstance();
			JuntaBean myJunta;
			while(true){ 
				String sCmd = broker.Transacao086(Util.lPad(codOrgao,"0",6),Util.lPad("","9",6),"5",indContinuidade,"","","","","","","","","","","","");
				if(!sCmd.substring(0,3).equals("000") && (!sCmd.substring(0,3).equals("017")))
					return null;
				if( sCmd.substring(0,3).equals("017") )
					return junta;	
				
				int ini, pos, fim;
				for(pos=0,ini=3,fim=9;(!sCmd.substring(ini,fim).equals(Util.rPad(""," ",6))) && (pos<21);ini = fim + 108, fim = ini+6,pos++){
					myJunta = new JuntaBean();
					myJunta.setCodOrgao(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim +6;
					myJunta.setCodJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 20;
					myJunta.setNomJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 10;
					myJunta.setSigJunta(sCmd.substring(ini,fim));
					ini = fim;
					fim = fim + 1; 
					if(sCmd.substring(ini,fim).equals("1"))
						myJunta.setIndTipo("0");
					else if(sCmd.substring(ini,fim).equals("2"))
						myJunta.setIndTipo("1");
					else
						myJunta.setIndTipo("2");
					junta.add(myJunta);
				}
				indContinuidade = sCmd.substring(ini,fim);
				if(pos!=21)
					break;
			}	
		}
		catch (Exception e) {
			return null;
		}//fim do catch
		return junta;
	}
	
	public boolean JuntaUpdateBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {	
			String indTipo;
			if(myJuntaBroker.getIndTipo().equals("0"))
				indTipo = "1";
			else if(myJuntaBroker.getIndTipo().equals("1"))
				indTipo = "2";
			else
				indTipo = "3";
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao086(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"2",Util.lPad("","0",6),Util.rPad(myJuntaBroker.getNomJunta()," ",20),Util.rPad(myJuntaBroker.getSigJunta()," ",10),indTipo,"","","","","","","","","");
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean JuntaUpdate(JuntaBean myJunta){
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "UPDATE TSMI_JUNTA set "+
			"sig_junta ='"+myJunta.getSigJunta()+"'," +
			"nom_junta ='"+myJunta.getNomJunta()+"', " +
			"ind_tipo  ='"+myJunta.getIndTipo()+"' " +
			" where cod_junta = '"+myJunta.getCodJunta()+"' " ;
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean JuntaInsert(JuntaBean myJunta, boolean flag) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			if(flag){
				sCmd = "SELECT seq_tsmi_junta.nextval as codJunta FROM dual";
				ResultSet rs = stmt.executeQuery(sCmd);
				rs.next();
				myJunta.setCodJunta(rs.getString("codJunta"));
				rs.close();
			}
			sCmd  = "INSERT INTO TSMI_JUNTA (cod_junta,"+
			"nom_junta,cod_orgao,sig_junta,ind_tipo) " +
			"VALUES (" +
			"'"+myJunta.getCodJunta()+"'," +
			"'"+myJunta.getNomJunta()+"'," +
			"'"+myJunta.getCodOrgao()+"',"+
			"'"+myJunta.getSigJunta()+"',"+
			"'"+myJunta.getIndTipo()+"')";
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex)   {
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean JuntaInsertBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {	
			String indTipo;
			if(myJuntaBroker.getIndTipo().equals("0"))
				indTipo = "1";
			else if(myJuntaBroker.getIndTipo().equals("1"))
				indTipo = "2";
			else
				indTipo = "3";
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao086(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"1",Util.lPad("","0",6),Util.rPad(myJuntaBroker.getNomJunta()," ",20),Util.rPad(myJuntaBroker.getSigJunta()," ",10),indTipo,"","","","","","","","","");
			if( !sCmd.substring(0,3).equals("000") )
				return false;
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean JuntaDeleteBroker(JuntaBean myJuntaBroker){
		boolean retorno = true;
		try {
			DaoBroker broker = DaoBrokerFactory.getInstance();
			String sCmd = broker.Transacao086(Util.lPad(myJuntaBroker.getCodOrgao(),"0",6),Util.lPad(myJuntaBroker.getCodJunta(),"0",6),"3",Util.lPad("","0",6),"","","","","","","","","","","","");
			if( !sCmd.substring(0,3).equals("000") )
				return false;	
		}
		catch (Exception ex)   {
			retorno = false;
		}
		return retorno;
	}
	
	public boolean JuntaDelete(JuntaBean myJunta) {
		Connection conn = null;
		boolean retorno = true;
		try {	
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd  = "DELETE TSMI_JUNTA where cod_junta = '"+myJunta.getCodJunta()+"'";
			stmt.executeUpdate(sCmd);
		}
		catch (Exception ex){
			retorno = false;
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	public boolean InsertJunta(Vector vErro, List myJuntas,List myJuntas_aux) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			for (int i = 0 ; i< myJuntas.size(); i++) {
				JuntaBean myJunta = (JuntaBean)myJuntas.get(i) ;
				JuntaBean myJunta_aux = (JuntaBean)myJuntas_aux.get(i);
				if( (myJunta.getNomJunta().equals(myJunta_aux.getNomJunta())) && (myJunta.getIndTipo().equals(myJunta_aux.getIndTipo())) )
					continue;
				if ("".equals(myJunta.getNomJunta().trim()) == false) 	{
					if( JuntaInsert(myJunta, stmt) )
						conn.commit();
					else{
						conn.rollback();
						vErro.add("Erro na atualiza��o da Junta:" + myJunta.getNomJunta() +"\n");
						retorno = false;
					}
				}
				else {
					if ("".equals(myJunta.getCodJunta()) == false)
						if( JuntaDelete(myJunta, stmt) ){
							conn.commit();
							myJuntas.remove(i);
							myJuntas_aux.remove(i);
							i--;
						}
						else{
							conn.rollback();
							vErro.add("Erro na exclus�o da Junta:" + myJunta_aux.getNomJunta() +"\n");
							retorno = false;
						}
				}
			}
			stmt.close();
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					if(!retorno)
						conn.rollback();		  
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le Feriados (MUNIC�PIO,Nacional e Estadual)
	 *-----------------------------------------------------------
	 */
	public void FeriadosGetFer(FeriadoBean myFer)  throws DaoException  {
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String    sCmd = "SELECT cod_municipio,cod_mun_feriado,to_char(dat_feriado,'dd/mm/yyyy')dt_feriado,txt_feriado,tipo_feriado";
			sCmd += " from TSMI_MUN_FERIADO";
			
			if ("M".equals(myFer.getTipoFeriado())){								
				sCmd += " WHERE cod_municipio = '"+ myFer.getCodMunicipio() + "'";
				sCmd += " AND TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";	 
			}else
				sCmd += " WHERE TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'"; 	
			
			
			sCmd += " order by dat_feriado";		
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			Vector Fer = new Vector();
			
			while( rs.next() ) {
				FeriadoBean myFeriado = new FeriadoBean() ;
				myFeriado.setCodMunicipio((rs.getString("cod_municipio")));
				myFeriado.setDatFeriado((rs.getString("dt_feriado")));
				myFeriado.setTxtFeriado((rs.getString("txt_feriado")));
				myFeriado.setTipoFeriado((rs.getString("tipo_feriado")));	
				myFeriado.setCodMunFeriado((rs.getString("cod_mun_feriado")));			  
				Fer.addElement(myFeriado) ;
			}      
			myFer.setFeriados(Fer) ;
			myFer.setCodMunicipio(myFer.getCodMunicipio());	
			myFer.setTipoFeriado(myFer.getTipoFeriado());		
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) {
			myFer.setMsgErro("Municipio008p - Leitura do Banco de dados - SQL: " + sqle.getMessage());
			myFer.setCodMunicipio("0") ;
		}
		catch (Exception e) {
			myFer.setMsgErro("Municipio008p - Leitura: " + e.getMessage());
			myFer.setCodMunicipio("0") ;
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	/*
	 /**
	  *-----------------------------------------------------------
	  * Le Feriados e carrega no Bean (MUNIC�PIO,Nacional e Estadual)
	  *-----------------------------------------------------------
	  */
	public  boolean FeriadosLeBean (FeriadoBean myFer,int tpUsr) throws DaoException {
		
//		synchronized
		
		boolean bOk = true;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt     = conn.createStatement();
			String sCmd = "SELECT cod_mun_feriado,cod_municipio,to_char(dat_feriado,'dd/mm/yyyy')dat_feriado,txt_feriado,tipo_feriado,cod_uf";
			sCmd += " from TSMI_MUN_FERIADO";
			
			if ("M".equals(myFer.getTipoFeriado())){								
				sCmd += " WHERE cod_municipio = '"+ myFer.getCodMunicipio() + "'";
				sCmd += " AND TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";	 
			}else{
				sCmd += " WHERE TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'"; 	
			}			     
			
			sCmd += " order by dat_feriado";
			
			ResultSet rs  = stmt.executeQuery(sCmd) ;
			myFer.setMsgErro("") ;		
			
			while (rs.next()) {
				myFer.setCodMunicipio((rs.getString("cod_municipio")));		 
				myFer.setDatFeriado(rs.getString("dat_feriado"));			  
				myFer.setTxtFeriado(rs.getString("txt_feriado"));
				myFer.setCodMunFeriado(rs.getString("cod_mun_feriado"));
				myFer.setTipoFeriado(rs.getString("tipo_feriado"));							  
			}
			rs.close();
			stmt.close();
		}
		catch (SQLException e) {
			bOk = false ;
			myFer.setCodMunicipio("0") ;			
			throw new DaoException(e.getMessage());
		}
		catch (Exception ex) {
			bOk = false ;
			myFer.setCodMunicipio("0") ;			
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Insere  Dados dos Feriados (MUNIC�PIO,Nacional e Estadual)
	 *-----------------------------------------------------------
	 */
	public boolean FeriadosPreparaInsert(FeriadoBean myFer, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn =null ;
		String sCmd  = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			Vector feriadoAtual = myFer.getFeriados() ;
			// iniciar transa��o
			conn.setAutoCommit(false);
			if ("M".equals(myFer.getTipoFeriado())){	
				sCmd  = "DELETE TSMI_MUN_FERIADO where cod_municipio ="+myFer.getCodMunicipio()+"";
				sCmd += " AND TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";
			}else{			
				sCmd  = "DELETE TSMI_MUN_FERIADO where TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";
			}
			stmt.execute(sCmd);
			
			for (int i = 0 ; i<feriadoAtual.size(); i++) {
				
				FeriadoBean myFeriado = (FeriadoBean)feriadoAtual.elementAt(i) ;
				if ("".equals(myFeriado.getDatFeriado().trim())==false ) 	{
					FeriadosInsert(myFeriado,stmt) ;
				}			  
			}
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else 		 conn.rollback();
			stmt.close();
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();	  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myFer.setMsgErro(vErro);
		return retorno;
	}
	/**
	 *-----------------------------------------------------------
	 * Insert em Feriado (MUNIC�PIO,Nacional e Estadual)
	 *-----------------------------------------------------------
	 */
	public void FeriadosInsert(FeriadoBean myFer,Statement stmt) throws DaoException {
		try {
			String codParam  = myFer.getCodMunicipio();
			String sCmd    = "";
			ResultSet rs   ;
			
			
			sCmd  = "INSERT INTO TSMI_MUN_FERIADO "; 
			sCmd += "VALUES (seq_tsmi_mun_feriado.nextval," ;
			
			if ("M".equals(myFer.getTipoFeriado())){
				sCmd += "'"+myFer.getCodMunicipio()+"'," ;
			}else{
				sCmd += "''," ;			
			}
			
			sCmd += "to_date('"+myFer.getDatFeriado()+"','dd/mm/yyyy')," ;
			sCmd += "'"+myFer.getTxtFeriado()+"'," ;
			sCmd += "'"+myFer.getTipoFeriado()+"',";
			if ("N".equals(myFer.getTipoFeriado())){
				sCmd += "'')" ;
			}else{
				sCmd += "'"+myFer.getCodUF()+"')";			
			}			
			stmt.execute(sCmd);			
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
	}
	/**
	 *-----------------------------------------------------------
	 * Delete em Feriado (MUNIC�PIO,Nacional e Estadual)
	 *-----------------------------------------------------------
	 */
	public void FeriadosDelete(FeriadoBean myFer,Statement stmt) throws DaoException {
		String sCmd ="";
		try {
			if ("M".equals(myFer.getTipoFeriado())){	
				sCmd  = "DELETE TSMI_MUN_FERIADO where COD_MUNICIPIO = "+ myFer.getCodMunicipio() ;
				sCmd += " AND TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";
			}else{
				sCmd  = "DELETE TSMI_MUN_FERIADO where TIPO_FERIADO = '"+myFer.getTipoFeriado()+"'";
			}
			
			stmt.execute(sCmd);
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
	}
	
	
	public int VerificaFeriado(AutoInfracaoBean myAuto, String myData ) throws DaoException {
		int iDias=0;
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;					
			// iniciar transa��o
			iDias = Util.isFimdeSemana(myData);
			if (iDias == 0) {
				Statement stmt  = conn.createStatement();
				String    sCmd  = "SELECT cod_municipio,to_char(dat_feriado,'dd/mm/yyyy') dt_feriado,txt_feriado,tipo_feriado,cod_uf";
				sCmd += " from TSMI_MUN_FERIADO";														
				sCmd += " WHERE (cod_municipio = '"+ myAuto.getCodMunicipio() + "' OR COD_MUNICIPIO IS NULL) ";
				sCmd += " AND (COD_UF = '"+myAuto.getOrgao().getEndereco().getCodUF()+"' OR COD_UF IS NULL) ";	
				sCmd += " AND DAT_FERIADO = to_date('"+myData+"','dd/mm/yyyy')";
				
				System.out.println(sCmd);
				
				ResultSet rs    = stmt.executeQuery(sCmd);
				if (rs.next())			
					iDias=1;				
				stmt.close();
			}		
		}
		catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally 
		{
			if (conn != null) 
			{
				try 
				{ 				  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return iDias;
	}
	/*Luciana*/
	
	public void GetNivelUsuarioRelator(NivelUsuarioRelatorBean mySist,JuntaBean myJunta,RelatorBean myRelator) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();							
			
			String sCmd =  "SELECT c.cod_controle_relator,r.nom_relator,J.COD_JUNTA ";
			sCmd += " from TSMI_CONTROLE_RELATOR C,TSMI_RELATOR R,TSMI_JUNTA J";
			sCmd += " WHERE C.COD_CONTROLE_RELATOR = R.COD_RELATOR" ;
			sCmd += " AND R.COD_JUNTA = J.COD_JUNTA";
			sCmd += " AND c.cod_CONTROLE_RELATOR = '" + myRelator.getCodRelator() + "'" ;					
			sCmd += " order by C.COD_CONTROLE_RELATOR";
			ResultSet rs = stmt.executeQuery(sCmd);
			Vector Nivelprocesso = new Vector();
			
			while (rs.next()) {
				NivelUsuarioRelatorBean myNivelUsuario = new NivelUsuarioRelatorBean();
				myNivelUsuario.setCodNivelUsuario(rs.getString("cod_controle_relator"));
				myNivelUsuario.setNomNivelUsuario(rs.getString("nom_relator"));						
				Nivelprocesso.addElement(myNivelUsuario);
			}
			mySist.setNivelUsuarioRelator(Nivelprocesso);
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
			mySist.setMsgErro("Orgao008p - Leitura do Banco de dados - SQL: "
					+ sqle.getMessage());
			mySist.setCodUsuario("");
		}
		catch (Exception e) {
			mySist.setMsgErro("Orgao008p - Leitura: " + e.getMessage());
			mySist.setCodUsuario("");
		}
		finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) {
				}
			}
		}
	}
	
	
	
	public String getNivelUsuario(String codUsuario) throws DaoException {
		String myNiveis ="";
		Connection conn =null ;					
		try {
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement();
			
			String sCmd = "SELECT * from TSMI_PROCESSO_SISTEMA_NIVEL";
			sCmd += " WHERE COD_PROCESSO_SISTEMA ='"+codUsuario+"'";				
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myNiveis += ",\""+rs.getString("COD_PROCESSO_SISTEMA_NIVEL") +"\"";
				myNiveis += ",\""+rs.getString("DSC_NIVEL_PROCESSO") +"\"";
			}
			myNiveis = myNiveis.substring(1,myNiveis.length());
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myNiveis;
	}
	
	public boolean NivelLeBean(NivelUsuarioRelatorBean myNivel,int tp) throws DaoException
	{
		boolean retorno = false;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			String sCmd = "";
			
			sCmd = "SELECT * from TSMI_CONTROLE_RELATOR ";
			if(tp==0) sCmd += "WHERE COD_CONTROLE_RELATOR='"+ myNivel.getCodNivelUsuario()+"'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while( rs.next() ) {
				retorno=true ;
				myNivel.setCodUsuario( rs.getString("COD_CONTROLE_RELATOR"));
			}
			
			rs.close();
			stmt.close();
		}//fim do try
		catch (SQLException sqle) {
			myNivel.setCodRelator("0");
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			myNivel.setCodRelator("0");
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return retorno;
	}
	
	
	
	
	public void Le_NivelUsuario(NivelUsuarioRelatorBean myNivelUsuario,ACSS.UsuarioBean myUsrLogado) throws DaoException 
	{
		String sCmd = "";
		Connection conn =null ;	
		ArrayList Usuarios = new ArrayList();
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			sCmd  = "SELECT C.COD_CONTROLE_RELATOR, C.COD_RELATOR, C.COD_Usuario,R.COD_RELATOR,R.NOM_RELATOR," +
			"U.NOM_USERNAME,U.COD_USUARIO,J.COD_JUNTA, U.NOM_USUARIO " +
			"FROM TSMI_CONTROLE_RELATOR C, TCAU_Usuario U,TSMI_JUNTA J,TSMI_RELATOR R  " +
			"WHERE C.COD_USUARIO = U.COD_USUARIO AND R.COD_JUNTA = J.COD_JUNTA " +
			"AND J.COD_ORGAO = '"+myUsrLogado.getCodOrgaoAtuacao()+"' " +
			"AND C.COD_RELATOR = "+ myNivelUsuario.getCodRelator()+"  AND C.COD_RELATOR = R.COD_RELATOR" ;
			PreparedStatement stmt = conn.prepareStatement(sCmd);					
			ResultSet rs = stmt.executeQuery();
			while (rs.next()){
				NivelUsuarioRelatorBean NivelUsuarioBean = new NivelUsuarioRelatorBean();
				NivelUsuarioBean.setCodNivelUsuario(rs.getString("COD_CONTROLE_RELATOR"));						
				NivelUsuarioBean.setCodUsuario(rs.getString("COD_USUARIO"));
				NivelUsuarioBean.setNomUsuario(rs.getString("NOM_USUARIO"));
				NivelUsuarioBean.setNomRelator(rs.getString("NOM_RELATOR"));
				Usuarios.add(NivelUsuarioBean);
			}
			myNivelUsuario.setListNivelDestino(Usuarios);
			rs.close();
		}
		catch (SQLException sqle) {
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();		  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
	}
	
	public boolean gravaNivelUsuarioRelator(NivelUsuarioRelatorBean myNivelUsuario, Vector vErro) throws DaoException{
		boolean retorno = true;
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			PreparedStatement stmt  = null;
			
			// iniciar transa��o
			conn.setAutoCommit(false);
			retorno=retorno && deleteNivelUsuario(myNivelUsuario,stmt,conn) ;
			for (int i = 0 ; i<myNivelUsuario.getNivelUsuario().size(); i++) {
				NivelUsuarioRelatorBean nivelUsuarioBean = (NivelUsuarioRelatorBean)myNivelUsuario.getNivelUsuario().elementAt(i) ;
				retorno=retorno && insertNivelUsuario(nivelUsuarioBean,stmt,conn) ;
			}
			
			// fechar a transa��o - commit ou rollback
			if (retorno) conn.commit();
			else 		 conn.rollback();
			
		}
		catch (SQLException sqle) {
			retorno = false;
			throw new DaoException(sqle.getMessage());
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}//fim do catch
		finally {
			if (conn != null) {
				try { 
					conn.rollback();		  
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myNivelUsuario.setMsgErro(vErro);
		return retorno;
	} 
	
	public List getUsuarios(NivelUsuarioRelatorBean myNvlUsrRelat, ACSS.UsuarioBean myUsrLogado) throws DaoException {
		Connection conn =null ;	
		List Usuario = new ArrayList();
		try {
			String sFuncao=
				"'REC0220','REC0236','REC0236','REC0237','REC0238'," +
				"'REC0239','REC0240','REC0243','REC0336','REC0337'," +
				"'REC0338','REC0339','REC0340','REC0341','REC0342'," +
				"'REC0346','REC0347','REC0351','REC0343','REC0345'," +
				"'REC0360','REC0436','REC0437','REC0438','REC0439'," +
				"'REC0440','REC0441','REC0442','REC0446','REC0447'," +
				"'REC0443','REC0445','REC0460'"; 
			
			
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT distinct U.nom_usuario,U.cod_usuario,U.nom_username,U.cod_orgao " +
			" from TCAU_USUARIO U, tcau_perfil_orgao p, tcau_acesso a, tcau_operacao o " +                
			" where p.cod_orgao_atuacao = '"+myUsrLogado.getCodOrgaoAtuacao()+"' " +
			" and u.cod_usuario = p.cod_usuario " +
			" and a.cod_perfil = p.cod_perfil " +
			" and a.cod_operacao = o.cod_operacao" +
			" and o.sig_funcao in("+sFuncao+")"; 
			
			ResultSet rs = stmt.executeQuery(sCmd);     
			
			while( rs.next() ) {
				NivelUsuarioRelatorBean myUsuario = new NivelUsuarioRelatorBean() ;					
				myUsuario.setCodUsuario(rs.getString("cod_usuario"));
				myUsuario.setNomUsuario(rs.getString("nom_usuario"));						
				Usuario.add(myUsuario) ;
			}    	
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return Usuario;
		
	}	
	
	
	public boolean deleteNivelUsuario(NivelUsuarioRelatorBean myNivelUsuario,
			PreparedStatement stmt, Connection conn) throws DaoException {
		boolean retorno = false ;
		String sCmd = "";
		
		try {
			sCmd  = "DELETE TSMI_CONTROLE_RELATOR WHERE COD_RELATOR = :1";
			stmt = conn.prepareStatement(sCmd);
			stmt.setInt(1,Integer.parseInt(myNivelUsuario.getCodRelator()));
			stmt.execute();
			stmt.close();
			retorno = true ;
		}
		catch (SQLException e) {
			retorno = false;
			throw new DaoException(e.getMessage());	
		}
		catch (Exception ex)   {
			retorno = false;
			throw new DaoException(ex.getMessage());
		}
		return retorno ;
	}
	
	public boolean insertNivelUsuario(NivelUsuarioRelatorBean myNivelUsuario,PreparedStatement stmt,
			Connection conn) throws DaoException {
		boolean retorno = false ;
		String sCmd = "";
		try {
			sCmd  = "INSERT INTO TSMI_CONTROLE_RELATOR (COD_CONTROLE_RELATOR," +
			"COD_USUARIO,COD_RELATOR) VALUES (SEQ_TSMI_PROC_SIST_NIV_DEST.NEXTVAL,:1,:2)";
			stmt = conn.prepareStatement(sCmd);
			stmt.setInt(1,Integer.parseInt(myNivelUsuario.getCodUsuario()));
			stmt.setInt(2,Integer.parseInt(myNivelUsuario.getCodRelator()));
			stmt.execute();
			stmt.close();
			retorno =true ;
		}
		catch (SQLException e) {throw new DaoException(e.getMessage());	}
		catch (Exception ex)   {throw new DaoException(ex.getMessage());}
		return retorno ;
	}	    
	
	public String getJunta(ACSS.UsuarioBean UsrLogado) throws DaoException {
		String myJunta ="";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt = conn.createStatement();
			
			String sCmd = "Select cod_junta,cod_orgao,nom_junta, sig_junta from TSMI_JUNTA" +
			" where cod_orgao = "+UsrLogado.getCodOrgaoAtuacao();
			
			ResultSet rs = stmt.executeQuery(sCmd) ;
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next()){
				myJunta += ",\""+rs.getString("cod_junta") +"\"";
				myJunta += ",\""+rs.getString("cod_orgao") +"\"";
				myJunta += ",\""+rs.getString("nom_junta") +"\"";
				myJunta += ",\""+rs.getString("sig_junta") +"\"";
			}
			myJunta = myJunta.substring(1,myJunta.length()) ;
			rs.close();
			stmt.close();
		}
		catch (SQLException sqle) {
		}//fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return myJunta;
	}	    
	
	public String verificaSitRelator(RelatorBean myRelator) throws DaoException{
		String indAtivo = "";
		Connection conn = null;
		try {   
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			if(!myRelator.getCodRelator().equals(""))
			{
				String sCmd = "SELECT IND_ATIVO FROM TSMI_RELATOR WHERE " +
				" COD_RELATOR = " + myRelator.getCodRelator();
				ResultSet rs = stmt.executeQuery(sCmd);
				while(rs.next()){
					indAtivo = rs.getString("IND_ATIVO");
					if(indAtivo == null) indAtivo = "";
				}
			}
		}
		catch (Exception ex)   {
			throw new DaoException(ex.getMessage());
		}
		return indAtivo;
	}
	
	
}	  



