package REC;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class RemessaExcluirProcCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/RemessaExcluirProc.jsp" ;  
  public RemessaExcluirProcCmd() {
	  next = jspPadrao;
   }

   public RemessaExcluirProcCmd(String next) {
	  this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
	   String nextRetorno = next ;
	  try {     
		  // cria os Beans de sessao, se n�o existir
		  sys.Command cmd = (sys.Command) Class.forName("REC.ProcessaAutoCmd").newInstance();
		  cmd.setNext(this.next);
		  nextRetorno = cmd.execute(req);
	
		  HttpSession session   = req.getSession() ;								
		  ACSS.UsuarioBean UsrLogado  = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		  if (UsrLogado==null)  
		  	UsrLogado = new ACSS.UsuarioBean() ;	
		  
		  ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		  if (UsuarioFuncBeanId==null)
		  	UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
		  
		  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)
		  	ParamOrgaoId = new ParamOrgBean() ;	
		  
		  RemessaBean RemessaId = (RemessaBean)session.getAttribute("RemessaId") ;
		  if (RemessaId==null) 
		  	RemessaId = new RemessaBean() ;	

		  AutoInfracaoBean AutoInfracaoBeanId =(AutoInfracaoBean) req.getAttribute("AutoInfracaoBeanId");
		  RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
		  if (RequerimentoId == null) 
			  RequerimentoId = new RequerimentoBean();

		  
		  //Carrega os Beans		
		  RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
		  RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  RemessaId.setUsername(UsrLogado.getNomUserName());	
   		  RemessaId.setStatusRemessa("0");
		  
		  String acao = req.getParameter("acao");
		  if (acao==null) acao = " ";
		  session.setAttribute("codReq", "");


		  if ((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length() == 0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
              
				//Verifica se auto est� em uma guia de remessa em status enviada
				boolean remessaExiste=RemessaId.existeRemessa(RemessaId,AutoInfracaoBeanId);
				if (!remessaExiste)
				{
					AutoInfracaoBeanId.setMsgErro("N�o existe guia de remess para este auto de infra��o");
				}
				else
				{
					boolean existeRemessaEnviada = RemessaId.getCodRemessaEnviada(RemessaId,AutoInfracaoBeanId);
					boolean existeRemessa = RemessaId.getCodRemessa(RemessaId,AutoInfracaoBeanId);
					if(existeRemessaEnviada)
						AutoInfracaoBeanId.setMsgErro("Auto "+AutoInfracaoBeanId.getNumAutoInfracao()+" est� em uma " +
						"Guia de Remessa em Status de Enviada.");
					else if(existeRemessa){
						RemessaId.setEventoOK("S");
					}
				}
		  }
		  
		  if (acao.equals("ExcluirProc")) 
		  {
			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));
			AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			String codReq = "";
			String codReqDP = req.getParameter("codReq1");
			if (codReqDP==null) codReqDP="";
			String codReqDC = req.getParameter("codReq2");
			if (codReqDC==null) codReqDC="";
			if (!codReqDP.equals(""))codReq = codReqDP;
			else if (!codReqDC.equals(""))codReq = codReqDC;
			
			Iterator it = RemessaId.getAutos().iterator();
	  	    REC.RemessaBean myRemessa = new REC.RemessaBean();
	  	    while(it.hasNext()){
	  		   myRemessa = (REC.RemessaBean)it.next();
	  		   if(!myRemessa.getCodRemessa().equals(""))
	  			 RemessaId.setCodRemessa(myRemessa.getCodRemessa());
	  		   break;
	  	    }
	  	    
	        RemessaId.excluirProc(RemessaId,AutoInfracaoBeanId,codReq);
			RemessaId.setEventoOK("N");
		  }	        

		  session.setAttribute("RemessaId",RemessaId) ;
		  req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;

	  }
	  catch (Exception ue) {
			throw new sys.CommandException("RemessaReemiteCmd: " + ue.getMessage());
	  }
	  return nextRetorno;
   }
}
