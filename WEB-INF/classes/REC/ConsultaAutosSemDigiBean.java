package REC;


import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import sys.DaoException;
import sys.ServiceLocatorException;
 
/**
* <b>Title:</b>        RECURSO - Relatorio de Controle de AR, AI e fotos Bean<br>
* <b>Description:</b>  Informa os autos ser foto ou AR. <br>
* <b>Copyright:</b>    Copyright (c) 2005<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class ConsultaAutosSemDigiBean extends sys.HtmlPopupBean {

	
	private ArrayList autos;
	private String codOrgao; 
	private String sigOrgao;
	private String dataIni;
	private String dataFim;
	
	private String opcao;
	private String codStatus;
	
	//lista de ordenacao
	private String ordClass ;
	private String ordem ;

	public ConsultaAutosSemDigiBean() throws sys.BeanException {
		autos = new ArrayList();
		codOrgao = "";
		sigOrgao = "";
		dataIni = "";
		dataFim = "";
		
		opcao = "";
		codStatus = "";
	}
	
	public void setOpcao(String opcao) {
			this.opcao = opcao;
			if (opcao == null)
				this.opcao = "";
	}
	public String getOpcao() {
		return this.opcao;
	}
	
	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}
	public String getCodStatus() {
		return this.codStatus;
	}
	
	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}
	
	public void setSigOrgao(String sigOrgao) {
		this.sigOrgao = sigOrgao;
		if (sigOrgao == null)
			this.sigOrgao = "";
	}
	public String getSigOrgao() {
		return this.sigOrgao;
	}
	
	public void setDataIni(String dataIni) {
		this.dataIni = dataIni;
		if (dataIni == null)
			this.dataIni = "";
	}
	public String getDataIni() {
		return this.dataIni;
	}
	
	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
		if (dataFim == null)
			this.dataFim = "";
	}
	public String getDataFim() {
		return this.dataFim;
	}
	
	public void setAutos(ArrayList autos) {
		this.autos = autos;
	}
	public List getAutos() {
		return this.autos;
	}
	
	
//	--------------------------  Metodos da Bean ----------------------------------

	public boolean consultaAutoSemAr (ConsultaAutosSemDigiBean consultaAutosSemDigitalizadosBean,ACSS.UsuarioBean myUsrLog) throws   DaoException, ServiceLocatorException{
			boolean existe = false;
			DaoBroker dao = DaoBrokerFactory.getInstance();
			try {
				if(dao.consultaAutoSemAr(consultaAutosSemDigitalizadosBean,myUsrLog)== true)
				  existe = true;
				Classifica("numAuto");				  
		} 
			catch (Exception e) {
				throw new DaoException(e.getMessage());
			} 
			return existe;	
		}

	public boolean verificaData(String dataini, String datafim,String databroker) throws ParseException{


		boolean ok = false;
		DateFormat df = DateFormat.getDateInstance();

	  Date datainicio = new Date();
	  datainicio = df.parse(dataini);

		Date datafinal = new Date();
		datafinal = df.parse(datafim);
		
		Date datbroker = new Date();
		if (!databroker.equals("")) datbroker = df.parse(databroker);
		
		if(datbroker.after(datainicio)&& datbroker.before(datafinal)||
		   datbroker.equals(datainicio)||datbroker.equals(datafinal)){
			ok = true;
		}		
		
		return ok;
	}
	
	/**--------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista 
	 *----------------------------------------------------------------------
	 */
	
	 public AutoInfracaoBean getAutos(int i)  throws sys.BeanException {
	   try {
		   if ((i<0) || (i>=this.getAutos().size()) ) return (new AutoInfracaoBean()) ;
	   }
	   catch (Exception e) { 			
		   throw new sys.BeanException(e.getMessage());
	   }
	   return (AutoInfracaoBean)this.getAutos().get(i);
	  }
//	  --------------------------------------------------------------------------
		public void setOrdClass(String ordClass)  {
			this.ordClass=ordClass ;
			if (ordClass==null) this.ordClass= "ascendente";
		} 
		public String getOrdClass()  {
			return this.ordClass;
		}
// --------------------------------------------------------------------------
		public void setOrdem(String ordem)  {
			this.ordem=ordem ;
			if (ordem==null) this.ordem= "numAuto";
		}  
		public String getOrdem()  {
			return this.ordem;
		}
// --------------------------------------------------------------------------
		public void Classifica(String ordemSol) throws sys.BeanException {
			int ord = 0;
			if (ordemSol.equals("numAuto")) ord = 1 ;
			if (ordemSol.equals("numPlaca"))  ord = 2 ;   
			if (ordemSol.equals("numProc")) ord = 3 ;  
			if (ordemSol.equals("datInf")) ord = 4 ; 
			if (ordemSol.equals("rec")) ord = 5 ;
			boolean troca    = false ;
			boolean trocaasc = true ;
			boolean trocades = false ;
			
			if (ordemSol.equals(getOrdem()))   {
					if ( getOrdClass().equals("ascendente")) {
						trocaasc = false ;
						trocades = true ;
						setOrdClass("descendente"); 
					} 
					else {
						trocaasc = true ;
						trocades = false;
						setOrdClass("ascendente"); 
					}
			}else setOrdClass("ascendente");
			
			setOrdem(ordemSol) ;
			int tam = getAutos().size() ;
			AutoInfracaoBean tmp = new AutoInfracaoBean();
			String auto1,auto2,placa1,placa2,proc1,proc2,dat1,dat2,rec1,rec2 ;  
			for (int i=0; i<tam; i++)  {  
				for (int j=i+1; j<tam; j++)  {
					troca=false;
					// compara os campos
					switch (ord) {
					case 0:
						auto1 = getAutos(i).getNumAutoInfracao();
						auto2 = getAutos(j).getNumAutoInfracao(); 
						placa1 = getAutos(i).getNumPlaca();
						placa2 = getAutos(j).getNumPlaca();
						proc1 = getAutos(i).getDatProcesso() ;
						proc2 = getAutos(j).getDatProcesso() ;
						dat1 = getAutos(i).getDatInfracao();
						dat2 = getAutos(j).getDatInfracao();
						rec1 =getAutos(i).getTpRequerimento();
						rec2 =getAutos(j).getTpRequerimento();
					
						if ((auto1+proc1+dat1+rec1).compareTo(auto2+proc2+dat2+rec2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "N� Infracao" ;
					case 1:
						if(getAutos(i).getNumAutoInfracao().compareTo(getAutos(j).getNumAutoInfracao())>0) troca=trocaasc;
							 else troca=trocades ;
						break;
          //"N�Placa" ;   
				  case 2:
					  if (getAutos(i).getNumPlaca().compareTo(getAutos(j).getNumPlaca())>0 ) troca=trocaasc;
					  else troca=trocades ;	
					  break;	
						
					// "N�Processo" ;   
					case 3:
						if (getAutos(i).getDatProcesso().compareTo(getAutos(j).getDatProcesso())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Data Infracao" ;   
					case 4:
						if (getAutos(i).getDatInfracao().compareTo(getAutos(j).getDatInfracao())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "Tipo Requerimento" ; 
					case 5:
					  if (getAutos(i).getTpRequerimento().compareTo(getAutos(j).getTpRequerimento())>0 ) troca=trocaasc;
						else troca=trocades ;	
						break;
					// "N� Auto" ;	
					default:
					if(getAutos(i).getNumAutoInfracao().compareTo(getAutos(j).getNumAutoInfracao())>0) troca=trocaasc;
					 else troca=trocades ;
				   break;
				}	
				if (troca) 	{		
				   tmp = (AutoInfracaoBean)getAutos(i);
				   getAutos().set(i,getAutos(j));
				   getAutos().set(j,tmp);
				}
			}
		}
	}	


}