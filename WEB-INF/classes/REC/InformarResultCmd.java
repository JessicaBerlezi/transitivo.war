package REC;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;
import ACSS.UsuarioFuncBean;

public class InformarResultCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/InformarResult.jsp" ;  
  Ata ata;
  public InformarResultCmd() {
    next             =  jspPadrao;
  }

  public InformarResultCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);

	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	  	// cria os Beans, se n�o existir
	  	RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
	  	if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
		
	  	InformarResultBean InformarResultId     = (InformarResultBean)req.getAttribute("InformarResultId") ;
	  	if (InformarResultId==null)  InformarResultId = new InformarResultBean() ;	  			
		InformarResultId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));

	    AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
		    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	    }
	    if  (acao.equals("LeReq"))  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	        InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	        RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
	        if (RequerimentoId.getCodRequerimento().length()!=0) {
				RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
				if (RequerimentoId.getDatRS().length()==0) RequerimentoId.setDatRS(sys.Util.formatedToday().substring(0,10)) ;			
	        }
	    }	
	    if  (acao.equals("InformaResult"))  {
	      	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	    	InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	    	nextRetorno = processaResult(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
	    }
	    if  (acao.equals("imprimirInf"))  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	    	InformarResultId.setEventoOK(AutoInfracaoBeanId) ;	    
			RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
	    	if (RequerimentoId.getCodRequerimento().length()!=0) {
	    		RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
	    		if (RequerimentoId.getDatRS().length()==0) RequerimentoId.setDatRS(sys.Util.formatedToday().substring(0,10)) ;			
   			}
			RequerimentoId.setTxtMotivoRS(req.getParameter("txtMotivoRS"));
			RequerimentoId.setCodResultRS(req.getParameter("codResultRSradio"));
   		    nextRetorno = "/REC/InformaResultImp.jsp" ;
   		}	
		
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	    req.setAttribute("RequerimentoId",RequerimentoId) ;		
	    req.setAttribute("InformarResultId",InformarResultId) ;		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("InformarResultCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }

  protected String processaResult(InformarResultBean InformarResultId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
   	String nextRetorno = next ;
   	try {
   		myAuto.setMsgErro("") ;
  		Vector vErro = new Vector() ;
  		// validar se status permite recurso ou defesa previa : 5,25,35
	 	if ("S".equals(InformarResultId.getEventoOK())==false) vErro.addElement(InformarResultId.getMsg()+" \n") ;
  		myReq.setCodRequerimento(req.getParameter("codRequerimento"));
        
        //Raj
  		if(myAuto.getCodStatus().equals("5")){
  			myAuto.setCodStatus("10");
  		}
  		
  		if(myAuto.getCodStatus().equals("15")){
  			myAuto.setCodStatus("30");
  		}
  		
  		//Para o Oficio Funcionar
  		if (myReq.getCodRequerimento().equals("")){
  			OficioBean OficioBean = new OficioBean();
	 		String solicDEF = req.getParameter("SolicDPBox");
			if (solicDEF==null) solicDEF="";
			if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
			if (solicDEF==null) solicDEF="";
			OficioBean.setTipoRecurso(solicDEF);
  			String codRequerimento = OficioBean.verificaCodReq(myAuto,myReq,OficioBean,UsrLogado);
  			myReq.setCodRequerimento(codRequerimento);
  		}
  		
  		if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado. \n") ;
  		// localizar o requerimento e validar status
  		boolean existe = false ;		
  		Iterator it = myAuto.getRequerimentos().iterator() ;
  		REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
  		while (it.hasNext()) {
  			reqIt =(REC.RequerimentoBean)it.next() ;
  			if ( (InformarResultId.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
  			     (InformarResultId.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
  				 (reqIt.getCodRequerimento().equals(myReq.getCodRequerimento())) ) { 
  				  myReq.copiaRequerimento(reqIt) ;					   
  				  existe = true ;
				  break ;
  			}
  		}
  		if (existe==false) vErro.addElement("Requerimento n�o localizado no Auto de Infra��o. \n") ;
  		String datRS = req.getParameter("datRS");		
   		if (datRS==null) datRS = "";
   		if (sys.Util.DifereDias( datRS,myReq.getDatRequerimento())>0) vErro.addElement("Data n�o pode ser anterior ao Requerimento ("+myReq.getDatRequerimento()+"). \n") ;
   		
		String sta = "3" ;
		String codResultRS = "";
		// Estorno de Resultado		
		if ("3".equals(myReq.getCodStatusRequerimento())) {
			//validar data de entrada
			sta ="2" ;
			codResultRS = "";
			if (datRS.length()==0) vErro.addElement("Data do Estorno do Resultado n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),datRS)>0) vErro.addElement("Data do Estorno n�o pode ser superior a hoje. \n") ;
			if (sys.Util.DifereDias( datRS,myReq.getDatRS())>0) vErro.addElement("Data do Estorno n�o pode ser anterior ao Resultado. \n") ;
		}
		// Ver se � Inclusao de Resultado
		else {
			//validar data de entrada
		  	myReq.setCodResultRS(codResultRS);								
			if (datRS.length()==0) vErro.addElement("Data do Resultado n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),datRS)>0) vErro.addElement("Data do Resultado n�o pode ser superior a hoje. \n") ;
			codResultRS = req.getParameter("codResultRS");
			if (codResultRS==null) codResultRS = "";
			if (codResultRS.length()==0) vErro.addElement("Resultado n�o selecionado. \n") ;		
		}
   		String txtMotivoRS = req.getParameter("txtMotivoRS");
   		
   		ata = new Ata();
   		ata = ata.getAtasInfResult(myAuto.getNumAutoInfracao());
   		
   		if (txtMotivoRS==null) txtMotivoRS = "";
//		if (txtMotivoRS.length()==0) vErro.addElement("Preenchimento do Motivo � obrigat�rio. \n") ;		
  		myReq.setCodStatusRequerimento(sta);		
  		myReq.setDatRS(datRS);
  		myReq.setCodResultRS(codResultRS);
  		myReq.setTxtMotivoRS(txtMotivoRS);
		if (vErro.size()==0) {
			myReq.setNomUserNameRS(UsrLogado.getNomUserName());
			myReq.setCodOrgaoLotacaoRS(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcRS(sys.Util.formatedToday().substring(0,10));
            if(codResultRS.equals("D")){
                String codMotivoDef = req.getParameter("codMotivo");
                if (codMotivoDef == null) codMotivoDef = "";
              //Raj
                
                if(InformarResultId.getCFuncao().equals("REC0243")){ //Deferimento - Defesa Previa
                	myAuto.setCodStatus("98"); 
                }
                
                if(InformarResultId.getCFuncao().equals("REC0343")){ //Deferimento - Defesa Previa 1 instancia
                	myAuto.setCodStatus("96"); 
                }
                
                if(InformarResultId.getCFuncao().equals("REC0443")){ //Deferimento - Defesa Previa 2 instancia
                	myAuto.setCodStatus("97"); 
                }
//                
                myReq.setCodMotivoDef(codMotivoDef);
            }
  			atualizarReq(InformarResultId,myAuto,myReq,UsrLogado, ata) ;	
			if (myAuto.getMsgErro().length()==0) {  		
    			myAuto.setMsgErro("Resultado "+("2".equals(sta) ? "estornado" : "inclu�do")+" para o Requerimento:"+myReq.getNumRequerimento()) ;	
    			myAuto.setMsgOk("S") ;
			}						
  		}
	  	else myAuto.setMsgErro(vErro) ;
   	}
    catch (Exception e){
  	    throw new sys.CommandException("InformarResultCmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }
  private void atualizarReq(InformarResultBean InformarResultId,AutoInfracaoBean myAuto,RequerimentoBean myReq,ACSS.UsuarioBean UsrLogado, Ata ata) throws sys.CommandException { 
  	try { 	
  		// preparar Bean de Historico
  		HistoricoBean myHis = new HistoricoBean() ;
  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
  		myHis.setCodStatus(myAuto.getCodStatus());
  		myHis.setDatStatus(myAuto.getDatStatus());
		myHis.setCodEvento(InformarResultId.getCodEvento());
		myHis.setCodOrigemEvento(InformarResultId.getOrigemEvento());
  		myHis.setNomUserName(myReq.getNomUserNameRS());
  		myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoRS());
  		myHis.setDatProc(myReq.getDatProcRS());
  		myHis.setTxtComplemento01(myReq.getNumRequerimento());	
  		myHis.setTxtComplemento02(myReq.getDatRS());	
  		myHis.setTxtComplemento03(myReq.getCodResultRS());	
  		myHis.setTxtComplemento04(myReq.getTxtMotivoRS());	
  	    DaoBroker dao = DaoBrokerFactory.getInstance();
  	    InformarResultId.removeAutoGuia(myAuto,myReq);
  	    dao.atualizarAuto209(myReq,myHis,myAuto,UsrLogado, ata) ; 
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException(ex.getMessage());
  	}
  	return  ;
  }	
  
}