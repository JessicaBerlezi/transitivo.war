package REC;

import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BiometriaCmd;
import sys.Command;
import sys.Util;



public class CancelaAutoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/REC/CancelaAuto.jsp" ;  

	public CancelaAutoCmd() {
		next = jspPadrao;
	}

	public CancelaAutoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		
			HttpSession session   = req.getSession() ;

			//cria os Beans, se n�o existir
			ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			

			ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

			AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null)   	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  				   

			CancelamentoBean CancelamentoId   = (CancelamentoBean)req.getAttribute("CancelamentoId") ;
			if (CancelamentoId==null)   	CancelamentoId = new CancelamentoBean() ;
			CancelamentoId.setSigFuncao(req.getParameter("j_sigFuncao"));

			ACSS.ParamSistemaBean ParamSistemaBeanId = (ACSS.ParamSistemaBean)session.getAttribute("ParamSistemaBeanId");	  			  				   

			String acao = req.getParameter("acao");  
			if(acao==null){
				acao = " ";	    
			}

			nextRetorno="/REC/CancelaAuto.jsp" ;

			if  (acao.equals("CancelaAuto"))  {
				String txtMotivo = req.getParameter("txtMotivo");
				nextRetorno = processaAjuste(req,AutoInfracaoBeanId,UsrLogado,CancelamentoId,txtMotivo);
			}		
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			req.setAttribute("CancelamentoId",CancelamentoId) ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("CancelaAutoCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private String processaAjuste(HttpServletRequest req,AutoInfracaoBean myAuto,
			ACSS.UsuarioBean UsrLogado,CancelamentoBean CancelamentoId, String txtMotivo) throws sys.CommandException { 
		String nextRetorno = next ;
		try {		
			myAuto.setMsgErro("") ;
			Vector vErro = new Vector() ;	
			//Valida o C�digo do Cancelamento
			String codMotivo = req.getParameter("codMotivo");
			if (codMotivo==null) codMotivo="";
			if( codMotivo.compareTo("") == 0)
				vErro.addElement("C�digo do Motivo n�o Informado. \n") ;
			String data = req.getParameter("Data");
			if (data==null) data = "" ;

			String indOper = req.getParameter("IndicOper");
			if (indOper==null) indOper = "" ;
			
			String codStatus="";
			if (indOper.equals("1")) codStatus="90";
			else
				                     codStatus="95";

			String numProcesso = req.getParameter("ProcInv");
			if (numProcesso==null) numProcesso = "";

			//validar data de Notificacao		
			if (data.length()==0) vErro.addElement("Data do Cancelamento n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),data)>0) vErro.addElement("Data da Notifica��o n�o pode ser superior a hoje. \n") ;
			if (vErro.size()==0) {
				HistoricoBean myHis = new HistoricoBean();
				myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
				myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
				myHis.setNumProcesso(numProcesso);
				myHis.setCodStatus(codStatus);  
				myHis.setDatStatus(myAuto.getDatStatus());
				myHis.setCodEvento("405");
				myHis.setCodOrigemEvento("100");		
				myHis.setNomUserName(UsrLogado.getNomUserName());
				myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
				myHis.setTxtComplemento01(codStatus);
				myHis.setTxtComplemento02(data);			
				myHis.setTxtComplemento03(txtMotivo);
				myHis.setTxtComplemento04(" ");
				myHis.setTxtComplemento06(myAuto.getCodStatus());
				myAuto.setDatStatus(data);  
				myAuto.setCodStatus(codStatus);	
				myAuto.GravaAutoInfracaoLocal();
				if (myAuto.getCodAutoInfracao().length() > 0) 
				{				
					myHis.gravarHistorico(myHis);
					if (myAuto.getMsgErro().length() < 1)
					{
						myAuto.setMsgOk("S") ;										
						myAuto.setMsgErro("Cancelamento/Suspens�o efetuado(a) para o Auto: "+myAuto.getNumAutoInfracao()+" em "+myAuto.getDatStatus() + "\nStatus: " + myAuto.getCodStatus() + " - "+ myAuto.getNomStatus()) ;
					}
				}
			}
			else{myAuto.setMsgErro(vErro);}
		}
		catch (Exception e){
			throw new sys.CommandException("CancelaAutoCmd: " + e.getMessage());
		}
		return nextRetorno  ;
	}
}
