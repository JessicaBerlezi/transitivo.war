package REC;
import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;

public class EnviarDOSimplesCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/EnviarDOSimples.jsp" ;  
   
  public EnviarDOSimplesCmd() {
    next             =  jspPadrao;
  }

  public EnviarDOSimplesCmd(String next) {
    this.next = next;
  }

public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		

	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
		ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

	  	// cria os Beans, se n�o existir	  					
		RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
		if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
		InformarResultBean InformarResultId     = (InformarResultBean)req.getAttribute("InformarResultId") ;
		if (InformarResultId==null)  InformarResultId = new InformarResultBean() ;	  			
		InformarResultId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));	
		
		AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;
	    
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
		    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	    }
	    if  (acao.equals("LeReq"))  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	        InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	        RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
	        if (RequerimentoId.getCodRequerimento().length()!=0) {
				RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
				if (AutoInfracaoBeanId.getDatEnvioDO().length()==0) AutoInfracaoBeanId.setDatEnvioDO(sys.Util.formatedToday().substring(0,10)) ;			
	        }
	    }	
	    if  (acao.equals("EnviarDO"))  {
	      	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	    	InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
	    	nextRetorno = processaEnviaDO(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
	    }
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	    req.setAttribute("RequerimentoId",RequerimentoId) ;		
	    req.setAttribute("InformarResultId",InformarResultId) ;		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("EnviarDOCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
protected String processaEnviaDO(InformarResultBean InformarResultId,HttpServletRequest req,
		RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	   	String nextRetorno = next ;   	
	   	try {
	   		myAuto.setMsgErro("") ;
	  		Vector vErro = new Vector() ;
	  		if ("S".equals(InformarResultId.getEventoOK())==false) vErro.addElement(InformarResultId.getMsg()+" \n") ;	   		
	  		myReq.setCodRequerimento(req.getParameter("codRequerimento"));
	  		
	  		//para o Oficio Funcionar
	  		if (myReq.getCodRequerimento().equals("")){
	  			OficioBean OficioBean = new OficioBean();
		 		String solicDEF = req.getParameter("SolicDPBox");
				if (solicDEF==null) solicDEF="";
				if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
				if (solicDEF==null) solicDEF="";
				OficioBean.setTipoRecurso(solicDEF);
	  			String codRequerimento = OficioBean.verificaCodReq(myAuto,myReq,OficioBean,UsrLogado);
	  			myReq.setCodRequerimento(codRequerimento);
	  		}
	  		
	  		if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado. \n") ;
	  		// localizar o requerimento e validar status
	  		boolean existe = false ;		
	  		Iterator it = myAuto.getRequerimentos().iterator() ;
	  		REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
	  		while (it.hasNext()) {
	  			reqIt =(REC.RequerimentoBean)it.next() ;
	  			if ( (InformarResultId.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
	  			     (InformarResultId.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
	  				 (reqIt.getCodRequerimento().equals(myReq.getCodRequerimento())) ) { 
	  				  myReq.copiaRequerimento(reqIt) ;					   
	  				  existe = true ;
	  			  break ;
	  			}
	  		}
	  		if (existe==false) vErro.addElement("Requerimento n�o localizado no Auto de Infra��o. \n") ;
	  		// Pegar os parametros digitados - data, numero da CI
	  		String datEnvioDO = req.getParameter("datEnvioDO");		
	   		if (datEnvioDO==null) datEnvioDO = "";
			if (datEnvioDO.length()==0) vErro.addElement("Data de Publica��o n�o preenchida. \n") ;
			if (sys.Util.DifereDias(datEnvioDO,myAuto.getDatInfracao())>0) 
				vErro.addElement("Data de Publica��o n�o pode ser anterior a Infra��o. \n") ;
			String codEnvioDO = req.getParameter("codEnvioDO");	
			if (codEnvioDO==null) codEnvioDO = "";
			if (codEnvioDO.length()==0) vErro.addElement("N�mero da ATA para Publica��o n�o preenchida. \n") ;	   		
			if (vErro.size()==0) {
				myReq.setCodStatusRequerimento("4");
				myAuto.setDatStatus(datEnvioDO);
				myAuto.setDatEnvioDO(datEnvioDO);
				myAuto.setCodEnvioDO(codEnvioDO);
				myAuto.setNomUsernameDO(UsrLogado.getNomUserName());
				myAuto.setCodOrgaoLotacaoDO(UsrLogado.getOrgao().getCodOrgao());			
				myAuto.setDatProcDO(sys.Util.formatedToday().substring(0,10));			
				atualizarDO(InformarResultId,myAuto,myReq,UsrLogado) ;
				if (myAuto.getMsgErro().length()==0) {
	    			myAuto.setMsgErro("Enviado para publica��o o Requerimento:"+myReq.getNumRequerimento()) ;	
	    			myAuto.setMsgOk("S") ;
				}										
	  		}
		  	else myAuto.setMsgErro(vErro) ;
	   	}
	    catch (Exception e){
	  	    throw new sys.CommandException("EnviarDOCmd: " + e.getMessage());
	    }
	   	return nextRetorno  ;
	  }
private void atualizarDO(InformarResultBean InformarResultId,AutoInfracaoBean myAuto,RequerimentoBean myReq,UsuarioBean myUsuario) throws sys.CommandException { 
  	try { 	
  		// preparar Bean de Historico
  		HistoricoBean myHis = new HistoricoBean() ;
  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
  		myHis.setCodStatus(myAuto.getCodStatus());  
  		myHis.setDatStatus(myAuto.getDatStatus());
		myHis.setCodEvento(InformarResultId.getCodEvento());
		myHis.setCodOrigemEvento(InformarResultId.getOrigemEvento());
  		myHis.setNomUserName(myAuto.getNomUsernameDO());
  		myHis.setCodOrgaoLotacao(myAuto.getCodOrgaoLotacaoDO());
  		myHis.setDatProc(myAuto.getDatProcDO());
  		myHis.setTxtComplemento01(myAuto.getDatEnvioDO());	
  		myHis.setTxtComplemento02(myAuto.getCodEnvioDO());	
  		myHis.setTxtComplemento03(myReq.getCodResultRS());
  		myHis.setTxtComplemento04(myReq.getNumRequerimento());  
  		InformarResultId.removeAutoGuia(myAuto,myReq);
  	    DaoBroker dao = DaoBrokerFactory.getInstance();
  	    dao.atualizarAuto261(RequerimentoBean myReq,myHis,myAuto,myUsuario) ; 
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException(ex.getMessage());
  	}
  	return  ;
  }	
}
