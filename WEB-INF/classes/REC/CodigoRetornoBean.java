package REC;

import java.util.Vector;

/**
 * <b>Title:</b>        	SMIT-REC - Bean de Relator <br>
 * <b>Description:</b>  	Bean dados das Juntas - Tabela de Retorno<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author  				Sergio Roberto Jr.
 * @version 				1.0
 */

public class CodigoRetornoBean extends sys.HtmlPopupBean { 
	
	private int codCodigoRetorno;
	private String numRetorno;
	private String descRetorno;
	private boolean indEntregue;
	private String indCSS;
	private String sNumRetorno;
	private String atualizarDependente;
	private Vector retorno;
	
	public CodigoRetornoBean() throws sys.BeanException{
		
		super();
		setTabela("TSMI_CODIGO_RETORNO");	
		setPopupWidth(35);
		numRetorno  = "";   	
		descRetorno	= "";
		atualizarDependente = "N";
		indCSS      = "";
	}
	
	public void setCodCodigoRetorno(int codCodigoRetorno)  {
		this.codCodigoRetorno = codCodigoRetorno ;
	}
	public int getCodCodigoRetorno()  {
		return this.codCodigoRetorno;
	}
	
	public void setNumRetorno(String numRetorno)  {
		if (numRetorno == null) 
			this.numRetorno ="" ;
		else 
		{ 
			if(!numRetorno.equals(""))
				this.numRetorno = sys.Util.lPad(numRetorno,"0",2);
			else this.numRetorno = numRetorno;
		}
	}
	public String getNumRetorno()  {
		return this.numRetorno;
	}
	public String getNumRetornoEdit()  {
		sNumRetorno = sys.Util.lPad(getNumRetorno(),"0",2);
		return this.sNumRetorno;
	}
	
	public void setDescRetorno(String descRetorno){
		this.descRetorno = descRetorno;
		if (descRetorno == null)
			this.descRetorno = ""; 
	}
	public String getDescRetorno()  {
		return this.descRetorno;
	}
	
	
	public void setIndEntregue(boolean indEntregue)  {
		this.indEntregue = indEntregue;
	}  
	
	public boolean getIndEntregue()  {
		return this.indEntregue;
	}
	
	/*
	 *  Consulta os c�digos de retorno vex-detran, na base de dados
	 */
	public Vector getRetorno(int min,int livre)  {
		
		try  { 
			DaoDigit dao = DaoDigit.getInstance();
			retorno = dao.ConsultaCodigoRetorno(null, null);
			
			if (this.retorno.size()<min-livre) 
				livre = min-retorno.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				this.retorno.addElement(new CodigoRetornoBean()) ;
			}
		}
		catch (Exception e) {
			setMsgErro("Retorno 007 - Leitura: " + e.getMessage());
		}
		return this.retorno;
	}
	
	public Vector getRetorno()  {
		return this.retorno;
	}   
	
	public String getAtualizarDependente() {
		return atualizarDependente;
	}
	
	public void setAtualizarDependente(String string) {
		atualizarDependente = string;
	}
	
	public CodigoRetornoBean getRetorno(int i)  { 
		return (CodigoRetornoBean)this.retorno.elementAt(i);
	}
	
	public void setRetorno(Vector retorno)  {
		this.retorno = retorno;
	}  
	
	
	public void LeRetorno()   
	{
		try  
		{
			DaoDigit dao = DaoDigit.getInstance();		   
			dao.LeRetorno(this);
		}
		catch (Exception e) {
		}
	}
	
	
	public boolean isAlterRetorno()   
	{
		boolean retorno = true;
		Vector vErro = new Vector() ;
		
		// Verificada a qual ser� a a��o a ser realizada sobre os dados. Se inclus�o, altera��o ou exclus�o
		try  {
			DaoDigit dao = DaoDigit.getInstance();		   
			dao.AlterRetorno(this,vErro);
		}
		catch (Exception e) {
			vErro.addElement("Retorno: "+String.valueOf(getCodCodigoRetorno())+" n�o atualizados:"+ " \n "+e.getMessage()) ;
			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
			retorno = false;
		}
		
		setMsgErro(vErro);
		return retorno;
	}
	
	public String getIndCSS() {
		return indCSS;
	}
	
	public void setIndCSS(String indCSS) {
		if (indCSS==null) indCSS="";
		this.indCSS = indCSS;
	}
	
	
	
}