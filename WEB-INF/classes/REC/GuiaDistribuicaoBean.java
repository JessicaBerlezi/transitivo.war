package REC;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;

import sys.Util;
import ACSS.ParamSistemaBean;


/**
 * <b>Title:</b>        SMIT - Prepara Guia de Distribuicao<br>
 * <b>Description:</b>  Prepara Guia de Distribuicao <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Wellem Mello
 * @version 1.0
 */

public class GuiaDistribuicaoBean extends EventoBean  {
	
	private String msgErro ;
	private String msgOk ;
	
	private String numGuia;
	private String codStatus;
	private String nomStatus;
	private String numCPFRelator;
	private String codRelator;
	private String nomRelator;
	
	private String sigJunta;
	private String codJunta;
	private String tipoJunta;
	private String nomTitulo;
	
	private String nomUserName;
	private String codOrgaoLotacao;
	private String codOrgaoAtuacao;
	private String datProc;
	private String datRS;
	
	private String codEnvioDO;
	private String datEnvioDO;
	private String datPubliPDO;
	private String nomArquivoATA;
	
	private List Autos;
	private List AutosNaoProc;
	private String ordClass ;
	private String ordem ;
	
	private int proximoRel;
	private int iTotalProc;
	private List AutosSobra;
	private List autosSelect;
	private String geraNovaGuia;
	private List relatGuias;
	
	private String numSessao;
	private String datSessao;
	private String indSessao;
	private String tipoGuia;
	public final String GUIA_AUTOMATICA = "S";
	private String controleGuia ;
	private String dataIni ;
	private String dataFim;
	
	private int iTotalProcessos;
	private int iTotalRequerimentos;
	private int iToTalProcessosErro;
	
	
	
	public GuiaDistribuicaoBean()  throws Exception {
		super() ;
		msgErro = "";
		msgOk = "" ;
		numGuia = "";
		codStatus = "";
		nomStatus = "";
		numCPFRelator = "";
		codRelator = "";
		nomRelator = "";
		nomTitulo  = "";
		
		sigJunta = "";
		codJunta = "";
		tipoJunta = "0";
		
		nomUserName = "";
		codOrgaoLotacao = "";
		codOrgaoAtuacao	= "";
		datProc = "";
		datRS = "";
		
		codEnvioDO = "";
		datEnvioDO = "";
		datPubliPDO = "";
		nomArquivoATA = "";
		
		ordClass  = "ascendente";
		ordem     = "Data";
		
		Autos = new ArrayList() ;
		autosSelect = new ArrayList() ;
		AutosNaoProc = new ArrayList() ;
		
		proximoRel   = 0;
		iTotalProc   = 0;
		AutosSobra   = new ArrayList() ;
		geraNovaGuia = "N";		
		relatGuias   = new ArrayList() ;
		
		numSessao = "";
		datSessao = "";
		indSessao = "";
		tipoGuia = "";
		controleGuia ="";
		dataIni = formatedLast2Month().substring(0, 10);
		dataFim = sys.Util.formatedToday().substring(0, 10);
		
		
		iTotalProcessos = 0;
		iTotalRequerimentos =0;
		iToTalProcessosErro = 0;
		
	}

	public void setTotalProcessos(int iTotalProcessos) {
	  this.iTotalProcessos = iTotalProcessos;
	}
	public int getTotalProcessos() {
		return iTotalProcessos;
	}

	public void setTotalRequerimentos(int iTotalRequerimentos) {
		this.iTotalRequerimentos = iTotalRequerimentos;
	}
	public int getTotalRequerimentos() {
		return iTotalRequerimentos;
	}
	
	public void setToTalProcessosErro(int iToTalProcessosErro) {
		this.iToTalProcessosErro = iToTalProcessosErro;
	}
	public int getToTalProcessosErro() {
		return iToTalProcessosErro;
	}
	
	public void setTipoGuia(String tipoGuia) {
		if(tipoGuia == null) tipoGuia = "";
		else this.tipoGuia = tipoGuia;
	}
	
	public String getTipoGuia() {
		return tipoGuia;
	}
	
	public void setNumSessao(String numSessao)   {
		if(numSessao == null) numSessao = "";
		else this.numSessao = numSessao ;
	}
	public String getNumSessao()   {
		return this.numSessao;
	}
	
	public void setDatSessao(String datSessao)   {
		if(datSessao == null) datSessao = "";
		else this.datSessao = datSessao ;
	}
	public String getDatSessao()   {
		return this.datSessao;
	}
	
	public void setIndSessao(String indSessao)   {
		if(indSessao == null) indSessao = "N";
		this.indSessao = indSessao ;
	}
	public String getIndSessao()   {
		return this.indSessao;
	}
	
	
	//---------------------------------------------------------------
	
	public void setJ_sigFuncao(String j_sigFuncao,String obrParJur)   {
		if (j_sigFuncao==null) j_sigFuncao = "" ;
		setCFuncao(j_sigFuncao) ;
		// Defesa Previa - REC0230
		setStatusValido("5") ;
		setStatusTPCVValido("0,1,2,3,4") ;
		setNFuncao("PREPARA GUIA DE DISTRIBUI��O - DEFESA PR�VIA") ;
		setTipoReqValidos("DP,DC") ;
		// Testar parametro de Orgao - para verificar se � obrigat�rio Parecer Juridico.
		if (obrParJur==null) obrParJur = "N";
		if ("S".equals(obrParJur)) setStatusReqValido("01"); 
		else setStatusReqValido("01") ; 
		setOrigemEvento("100");
		setCodEvento("208");
		this.tipoJunta="0";
		// 1a Instancia
		if (  ("REC0336".equals(j_sigFuncao))   || ("REC0337".equals(j_sigFuncao)) ||
			  ("REC0339".equals(j_sigFuncao))   || ("REC0349".equals(j_sigFuncao)) || 
			  ("REC0350".equals(j_sigFuncao)) )
		{
			setStatusValido("15") ;
			setStatusTPCVValido("10,11,12,14") ;
			setNFuncao("PREPARA GUIA DE DISTRIBUI��O - 1a. INST�NCIA") ;
			setTipoReqValidos("1P,1C") ;
			setStatusReqValido("0") ;
			setOrigemEvento("100") ;
			setCodEvento("328") ;
			this.tipoJunta="1";
			this.codStatus = "2";
		}
		else if (  ("REC0436".equals(j_sigFuncao)) || ("REC0437".equals(j_sigFuncao)) ||
				   ("REC0439".equals(j_sigFuncao)) || ("REC0450".equals(j_sigFuncao)) ){
			setStatusValido("35") ;
			setStatusTPCVValido("") ;
			setNFuncao("PREPARA GUIA DE DISTRIBUI��O - 2a. INST�NCIA") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("0") ;
			setOrigemEvento("100") ;
			setCodEvento("335") ;
			this.tipoJunta="2";
			this.codStatus = "2";
		}
		/*DPWEB*/
		else if ("REC0251".equals(j_sigFuncao)) /*Guia de Distribui��o - WEB*/
		{
			setStatusValido("5");
			setStatusTPCVValido("0,1,2,3,4") ;
			setNFuncao("PREPARA GUIA DE DISTRIBUI��O - DEFESA PR�VIA (WEB)") ;
			setTipoReqValidos("DP") ;
		    setStatusReqValido("01") ;
			setOrigemEvento("100");
			setCodEvento("208");
			this.tipoJunta="0";
		}
		else if ("REC0240".equals(j_sigFuncao)) {
			setStatusValido("5") ;
			setStatusTPCVValido("0,1,2,3,4") ;
			setNFuncao("RESULTADO DE DEFESA PR�VIA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("209") ;
			this.tipoJunta="0";
			this.codStatus = "2";
		}
		else if ("REC0340".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("10,11,12,14") ;
			setNFuncao("RESULTADO DE 1a. INST�NCIA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("329") ;
			this.tipoJunta="1";
			this.codStatus = "2";
		}
		else if ("REC0440".equals(j_sigFuncao)) {
			setStatusValido("35") ;
			setStatusTPCVValido("") ;
			setNFuncao("RESULTADO DE 2a. INST�NCIA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("336") ;
			this.tipoJunta="2";
			this.codStatus = "2";
		}
		else if ("REC0241".equals(j_sigFuncao)) {
			setStatusValido("5") ;
			setStatusTPCVValido("0,1,2,3,4") ;
			setNFuncao("ENVIAR PARA PUBLICA��O - GUIA DE DISTRIBUI��O");
			setTipoReqValidos("DP,DC") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("261") ;
			this.tipoJunta="0";
		}
		else if ("REC0341".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("10,11,12,14") ;
			setNFuncao("ENVIAR PARA PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("381") ;
			this.tipoJunta="1";
		}
		else if ("REC0441".equals(j_sigFuncao)) {
			setStatusValido("35") ;
			setStatusTPCVValido("") ;
			setNFuncao("ENVIAR PARA PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("388") ;
			this.tipoJunta="2";
		}
		else if ("REC0246".equals(j_sigFuncao)) {
			setStatusValido("5") ;
			setStatusTPCVValido("0,1,2,3,4") ;
			setNFuncao("INFORMAR PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("263") ;
			this.tipoJunta="0";
		}
		else if ("REC0346".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("10,11,12,14") ;
			setNFuncao("INFORMAR PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("383") ;
			this.tipoJunta="1";
		}
		else if ("REC0446".equals(j_sigFuncao)) {
			setStatusValido("35") ;
			setStatusTPCVValido("") ;
			setNFuncao("INFORMAR PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("2") ;
			setOrigemEvento("100") ;
			setCodEvento("390") ;
			this.tipoJunta="2";
		}
		
		else if ("REC0852".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("") ;
			setNFuncao("ESTORNO DE JUNTA/RELATOR - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("0") ;
			setOrigemEvento("100") ;
			setCodEvento("215") ;
			this.tipoJunta="1";
		}
		else if ("REC0347".equals(j_sigFuncao)) {
			setNFuncao("DOWNLOAD ATA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			this.tipoJunta="1";
		}
		else if ("REC0447".equals(j_sigFuncao)) {
			setNFuncao("DOWNLOAD ATA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			this.tipoJunta="2";
		}
		else if ("REC0342".equals(j_sigFuncao)) {
			setNFuncao("REEMITIR ATA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			this.tipoJunta="1";
		}
		else if ("REC0442".equals(j_sigFuncao)) {
			setNFuncao("REEMITIR ATA - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			this.tipoJunta="2";
		}
		else if ("REC0281".equals(j_sigFuncao)) {
			setNFuncao("ESTORNAR JUNTA/RELATOR - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			setCodEvento("215");
			setOrigemEvento("100");
			tipoJunta="0";
			codStatus="2";
		}
		else if ("REC0282".equals(j_sigFuncao)) {
			setNFuncao("ESTONAR RESULTADO - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			tipoJunta="0";
			codStatus="3";
			setCodEvento("210");
			setOrigemEvento("100");
		}
		else if ("REC0381".equals(j_sigFuncao)) {
			setNFuncao("ESTONAR JUNTA/RELATOR - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			tipoJunta="1";
			codStatus="2";
			setCodEvento("215");
			setOrigemEvento("100");
		}
		else if ("REC0382".equals(j_sigFuncao)) {
			setNFuncao("ESTONAR RESULTADO - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			tipoJunta="1";
			codStatus="3";
			setCodEvento("330");
			setOrigemEvento("100");
		}
		else if ("REC0383".equals(j_sigFuncao)) {
			setNFuncao("ESTONAR ENVIO P/PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("1P,1C") ;
			tipoJunta="1";
			codStatus="4";
			setCodEvento("382");
			setOrigemEvento("100");
		}
		
		else if ("REC0481".equals(j_sigFuncao)) {
			setNFuncao("ESTONAR JUNTA/RELATOR - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			tipoJunta="2";
			codStatus="2";
			setCodEvento("215");
			setOrigemEvento("100");
		}
		
		else if ("REC0482".equals(j_sigFuncao)) {
			setNFuncao("ESTORNAR RESULTADO - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			tipoJunta="2";
			codStatus="3";
			setCodEvento("337");
			setOrigemEvento("100");
		}
		
		else if ("REC0483".equals(j_sigFuncao)) {
			setNFuncao("ESTORNAR ENVIO P/ PUBLICA��O - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("2P,2C") ;
			tipoJunta="2";
			codStatus="4";
			setCodEvento("389");
			setOrigemEvento("100");
		}
		
		else if ("REC0237".equals(j_sigFuncao)) {
			setStatusValido("15") ;
			setStatusTPCVValido("10,11,12,14") ;
			setNFuncao("TROCAR RELATOR - GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			setStatusReqValido("0") ;
			setOrigemEvento("100") ;
			setCodEvento("208") ;
			this.tipoJunta="0";
			this.codStatus="2";
		}
		
		else if ("REC0239".equals(j_sigFuncao)) {
			setNFuncao("REEMITIR DP- GUIA DE DISTRIBUI��O") ;
			setTipoReqValidos("DP,DC") ;
			this.tipoJunta="0";
		}
		return ;
	}
	public void setMsgErro(Vector vErro)   {
		for (int j=0; j<vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j)+"\n" ;
		}
	}
	public void setMsgErro(String msgErro)   {
		this.msgErro = msgErro ;
	}
	
	public String getMsgErro()   {
		return this.msgErro;
	}
	public void setMsgOk(String msgOk)   {
		this.msgOk = msgOk ;
	}
	public String getMsgOk()   {
		return this.msgOk;
	}
	//--------------------------------------------------------------------------
	public void setAutos(List Autos)  {
		this.Autos=Autos ;
	}
	public List getAutos()  {
		return this.Autos;
	}
	public AutoInfracaoBean getAutos(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.Autos.size()) ) return (new AutoInfracaoBean()) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return (AutoInfracaoBean)this.Autos.get(i);
	}
	
	public AutoInfracaoBean getAutosSelect(int i)  throws sys.BeanException {
		try {
			if ((i<0) || (i>=this.autosSelect.size()) ) return (new AutoInfracaoBean()) ;
		}
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
		return (AutoInfracaoBean)this.autosSelect.get(i);
	}
	
	
	//--------------------------------------------------------------------------
	public void setAutosNaoProc(List AutosNaoProc)  {
		this.AutosNaoProc=AutosNaoProc ;
	}
	public List getAutosNaoProc()  {
		return this.AutosNaoProc;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoAtuacao(String codOrgaoAtuacao)  {
		this.codOrgaoAtuacao=codOrgaoAtuacao ;
		if (codOrgaoAtuacao==null) this.codOrgaoAtuacao= "";
	}
	public String getCodOrgaoAtuacao()  {
		return this.codOrgaoAtuacao;
	}
	//--------------------------------------------------------------------------
	public void setNomUserName(String nomUserName)  {
		this.nomUserName=nomUserName ;
		if (nomUserName==null) this.nomUserName= "";
	}
	public String getNomUserName()  {
		return this.nomUserName;
	}
	//--------------------------------------------------------------------------
	public void setNumGuia(String numGuia)  {
		this.numGuia=numGuia ;
		if (numGuia==null) this.numGuia= "";
	}
	public String getNumGuia()  {
		return this.numGuia;
	}
	//--------------------------------------------------------------------------
	public void setCodStatus(String codStatus)  {
		this.codStatus=codStatus ;
		if (codStatus==null) this.codStatus= "";
	}
	public String getCodStatus()  {
		return this.codStatus;
	}
	
	
	/*Luciana*/
	public void setProximoRelator(int proximoRel)   {
		this.proximoRel = proximoRel ;
	}
	
	public int getProximoRelator()   {
		return this.proximoRel;
	}
	
	public void setTotalProc(int iTotalProc)   {
		this.iTotalProc = iTotalProc ;
	}
	
	public int getTotalProc()   {
		return this.iTotalProc;
	}
	
	public void setGeraNovaGuia(String geraNovaGuia)   {
		this.geraNovaGuia = geraNovaGuia ;
		if (geraNovaGuia==null) this.geraNovaGuia= "N";		
	}
	
	public String getGeraNovaGuia()   {
		return this.geraNovaGuia;
	}
	
	public void setRelatGuias(List relatGuias)  {
		this.relatGuias=relatGuias ;
	}
	public List getRelatGuias()  {
		return this.relatGuias;
	}
	
	public void setAutosSelect(List autosSelect)  {
		this.autosSelect=autosSelect ;
	}
	public List getAutosSelect()  {
		return this.autosSelect;
	}
	
	public void setAutosSobra(List AutosSobra)  {
		this.AutosSobra=AutosSobra ;
	}
	public List getAutosSobra()  {
		return this.AutosSobra;
	}
	
	
	//--------------------------------------------------------------------------
	public void setNomStatus(String nomStatus)  {
		this.nomStatus=nomStatus ;
		if (nomStatus==null) this.nomStatus= "";
	}
	public String getNomStatus()  {
		if(codStatus.equals("2"))
			nomStatus = "Def. Junta/Relator";
		else if(codStatus.equals("3"))
			nomStatus = "Resultado";
		else if(codStatus.equals("4"))
			nomStatus = "Enviado p/ Publica��o";
		else if(codStatus.equals("8"))
			nomStatus = "Estornado";
		else if(codStatus.equals("9"))
			nomStatus = "Publicado";
		return nomStatus;
	}
	//--------------------------------------------------------------------------
	public void setNumCPFRelator(String numCPFRelator)  {
		this.numCPFRelator=numCPFRelator ;
		if (numCPFRelator==null) this.numCPFRelator= "";
	}
	public String getNumCPFRelator()  {
		return this.numCPFRelator;
	}
	public void setNumCPFRelatorEdt(String numCpf)  {
		if (numCpf==null)      numCpf = "";
		if (numCpf.length()<14) numCpf += "               ";
		this.numCPFRelator=numCpf.substring(0,3)
		+ numCpf.substring(4,7)
		+ numCpf.substring(8,11)
		+ numCpf.substring(12,14);
	}
	public String getNumCPFRelatorEdt()  {
		if ("".equals(this.numCPFRelator.trim())) return this.numCPFRelator.trim() ;
		if (this.numCPFRelator.length()<11) return this.numCPFRelator.trim() ;
		return numCPFRelator.substring(0,3)+ "." + numCPFRelator.substring(3,6)
		+ "." + numCPFRelator.substring(6,9) + "-" + numCPFRelator.substring(9,11) ;
	}
	
	//--------------------------------------------------------------------------
	public void setCodRelator(String codRelator)  {
		this.codRelator=codRelator ;
		if (codRelator==null) this.codRelator= "";
	}
	public String getCodRelator()  {
		return this.codRelator;
	}
	
	//--------------------------------------------------------------------------
	public void setNomRelator(String nomRelator)  {
		this.nomRelator=nomRelator ;
		if (nomRelator==null) this.nomRelator= "";
	}
	public String getNomRelator()  {
		return this.nomRelator;
	}
	//--------------------------------------------------------------------------
	public void setNomTituloRelator(String nomTitulo)  {
		this.nomTitulo=nomTitulo ;
		if (nomTitulo==null) this.nomTitulo= "";
	}
	public String getNomTituloRelator()  {
		return this.nomTitulo;
	}
	//--------------------------------------------------------------------------
	public void setSigJunta(String sigJunta)  {
		this.sigJunta=sigJunta ;
		if (sigJunta==null) this.sigJunta= "";
	}
	public String getSigJunta()  {
		return this.sigJunta;
	}
	//--------------------------------------------------------------------------
	public void setCodJunta(String codJunta)  {
		this.codJunta=codJunta ;
		if (codJunta==null) this.codJunta= "";
	}
	public String getCodJunta()  {
		return this.codJunta;
	}
	//--------------------------------------------------------------------------
	public void setTipoJunta(String tipoJunta)  {
		this.tipoJunta=tipoJunta ;
		if (tipoJunta==null) this.tipoJunta= "0";
	}
	public String getTipoJunta()  {
		return this.tipoJunta;
	}
	//--------------------------------------------------------------------------
	public void setCodOrgaoLotacao(String codOrgaoLotacao)  {
		this.codOrgaoLotacao=codOrgaoLotacao ;
		if (codOrgaoLotacao==null) this.codOrgaoLotacao= "";
	}
	public String getCodOrgaoLotacao()  {
		return this.codOrgaoLotacao;
	}
	//--------------------------------------------------------------------------
	public void setDatProc(String datProc)  {
		this.datProc=datProc ;
		if (datProc==null) this.datProc= "";
	}
	public String getDatProc()  {
		return this.datProc;
	}
	//--------------------------------------------------------------------------
	public void setDatRS(String datRS)  {
		this.datRS=datRS ;
		if (datRS==null) this.datRS= "";
	}
	public String getDatRS()  {
		return this.datRS;
	}
	//--------------------------------------------------------------------------
	public void setCodEnvioDO(String codEnvioDO)  {
		this.codEnvioDO=codEnvioDO ;
		if (codEnvioDO==null) this.codEnvioDO= "";
	}
	public String getCodEnvioDO()  {
		return this.codEnvioDO;
	}
	//--------------------------------------------------------------------------
	public void setDatEnvioDO(String datEnvioDO)  {
		this.datEnvioDO=datEnvioDO ;
		if (datEnvioDO==null) this.datEnvioDO= "";
	}
	public String getDatEnvioDO()  {
		return this.datEnvioDO;
	}
	//--------------------------------------------------------------------------
	public void setDatPubliPDO(String datPubliPDO)  {
		this.datPubliPDO=datPubliPDO ;
		if (datPubliPDO==null) this.datPubliPDO= "";
	}
	public String getDatPubliPDO()  {
		return this.datPubliPDO;
	}
	//--------------------------------------------------------------------------
	public void setNomArquivoATA(String nomArquivoATA)  {
		this.nomArquivoATA=nomArquivoATA ;
		if (nomArquivoATA==null) this.nomArquivoATA= "";
	}
	public String getNomArquivoATA()  {
		return nomArquivoATA;
	}
	
	//-------------------------------------------------------------------------
	public void setControleGuia(String controleGuia) {
		if(controleGuia == null) controleGuia = "";
		else this.controleGuia = controleGuia;
	}
	public String getControleGuia() {
		return controleGuia;
	}
	
	//--------------------------------------------------------------------------
	public void setDataIni(String dataIni) {
		if (dataIni == null) dataIni = "";
		else this.dataIni = dataIni;
	}
	public String getDataIni() {
		return dataIni;
	}
	//--------------------------------------------------------------------------
	public void setDataFim(String dataFim) {
		if (dataFim == null) dataFim = "";
		else this.dataFim = dataFim;
	}
	public String getDataFim() {
		return dataFim;
	}
	
	//--------------------------------------------------------------------------
	public void PreparaGuia(ACSS.UsuarioBean myUsrLog,String datInicioRemessa,String numMaxProc) throws sys.BeanException {
		try {
			Autos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPrepara(this,myUsrLog,datInicioRemessa,numMaxProc)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				Classifica("Processo");
				
				
				
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	//--------------------------------------------------------------------------
	public void PreparaGuiaDetran(ACSS.UsuarioBean myUsrLog,String datInicioRemessa) throws sys.BeanException {
		try {
			Autos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPreparaDetran(this,myUsrLog,datInicioRemessa)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				Classifica("Processo");
				
				
				
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	public void PreparaGuiaAgravoInsert(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {
			Autos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			
			if (dao.GuiaPreparaAgravoInsert(this,myUsrLog)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				/*Classifica("DatProcesso");*/
				Classifica("Processo");
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	
	//--------------------------------------------------------------------------
	public void PreparaGuiaAgravo(ACSS.UsuarioBean myUsrLog) throws sys.BeanException {
		try {
			Autos  		     = new ArrayList() ;
			//System.err.println("Inicio Prepara Guia: "+sys.Util.formatedToday());
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if (dao.GuiaPreparaAgravo(this,myUsrLog)) {
				//System.err.println("Inicio Classifica Prepara Guia: "+sys.Util.formatedToday());
				
				/*Classifica("Enquadramento");*/
				/*Classifica("DatProcesso");*/
				//Classifica("Processo");
				//System.err.println("Fim Classifica Prepara Guia: "+sys.Util.formatedToday());
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	
	
	//--------------------------------------------------------------------------
	public void LeGuia() throws sys.BeanException {
		try {
			Autos  		     = new ArrayList() ;
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.GuiaLe(this);
			/*Classifica("Processo");*/
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public Vector LeGuiasAta() throws sys.BeanException {
		Vector guias = new Vector();
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			guias=dao.LeGuiasAta(this) ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return guias ;
	}
	//	--------------------------------------------------------------------------
	
	public String[] AtaGuias() throws sys.BeanException{
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.AtaGuias(codEnvioDO, "9",codOrgaoAtuacao) ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public List consultaGuia(ACSS.UsuarioBean UsrLogado) throws DaoException{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.consultaGuia(this,UsrLogado);
		}
		catch (Exception ey) {
			throw new DaoException(ey.getMessage());
		}
	}
	//--------------------------------------------------------------------------
	public List consultaAtasPublicar(ACSS.UsuarioBean UsrLogado ) throws DaoException{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.consultaAtasPublicar(this,UsrLogado);
		}
		catch (Exception ey) {
			throw new DaoException(ey.getMessage());
		}
	}
	public void consultaAta(int ordem) throws Exception{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.consultaAta(this,ordem);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	
	//	--------------------------------------------------------------------------
	public void setOrdClass(String ordClass)  {
		this.ordClass=ordClass ;
		if (ordClass==null) this.ordClass= "ascendente";
	}
	public String getOrdClass()  {
		return this.ordClass;
	}
	//	--------------------------------------------------------------------------
	public void setOrdem(String ordem)  {
		this.ordem=ordem ;
		if (ordem==null) this.ordem= "Data";
	}
	public String getOrdem()  {
		return this.ordem;
	}
	public String getNomOrdem()  {
		String nomOrdem = "Data Infra��o, Org�o e Num Auto" ;
		if (this.ordem.equals("Orgao"))       nomOrdem = "Org�o e Num Auto" ;
		if (this.ordem.equals("Placa"))       nomOrdem = "Placa e Data Infra��o" ;
		if (this.ordem.equals("Processo"))    nomOrdem = "Processo" ;
		if (this.ordem.equals("Auto"))        nomOrdem = "Auto de Infra��o" ;
		if (this.ordem.equals("Responsavel")) nomOrdem = "Respons�vel e Placa" ;
		if (this.ordem.equals("Status"))      nomOrdem = "Status e Data Infra��o" ;
		if (this.ordem.equals("DatProcesso")) nomOrdem = "Data e Processo"  ;
		if (this.ordem.equals("Enquadramento")) nomOrdem = "Enquadramento, Data e Processo"  ;
		return nomOrdem+ " ("+getOrdClass()+")" ;
	}
	//	--------------------------------------------------------------------------
	public void Classifica(String ordemSol) throws sys.BeanException {
		int ord = 0;
		if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
		if (ordemSol.equals("Orgao"))               ord = 1 ;
		if (ordemSol.equals("Placa"))               ord = 2 ;
		if (ordemSol.equals("Processo"))            ord = 3 ;
		if (ordemSol.equals("Responsavel"))         ord = 4 ;
		if (ordemSol.equals("Auto"))                ord = 5 ;
		if (ordemSol.equals("Status"))              ord = 6 ;
		if (ordemSol.equals("DatProcesso"))         ord = 7 ;
		if (ordemSol.equals("Enquadramento"))       ord = 8 ;
		if (ordemSol.equals(getOrdem()))   {
			if ( getOrdClass().equals("ascendente")) setOrdClass("descendente");
			else 									 setOrdClass("ascendente");
		}
		else setOrdClass("ascendente");
		setOrdem(ordemSol) ;
		int tam = getAutos().size() ;
		AutoInfracaoBean tmp = new AutoInfracaoBean();
		String chave ;
		for (int i=0; i<tam; i++)  {
			switch (ord) {
			// "Data Infra��o, Org�o e Num Auto" ;
			case 0:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()) +
				sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) +
				sys.Util.rPad(getAutos(i).getNumAutoInfracao()," ",12);
				break;
				// "Org�o e Num Auto" ;
			case 1:
				chave = sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) ;
				break;
				// "Placa e Data Infra��o" ;
			case 2:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao());
				break;
				// "Processo" ;
			case 3:
				chave = sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
				break;
				// "Respons�vel e Placa" ;
			case 4:
				chave = sys.Util.rPad(getAutos(i).getProprietario().getNomResponsavel(),"",40) +
				sys.Util.rPad(getAutos(i).getNumPlaca(),"",7) ;
				break;
				// "Num Auto" ;
			case 5:
				chave = sys.Util.rPad(getAutos(i).getNumAutoInfracao(),"",12) ;
				break;
				// Status e Data da Infracao
			case 6:
				chave = sys.Util.lPad(getAutos(i).getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()) ;
				break;
				// "Data Processo, Processo" ;
			case 7:
				chave = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatProcesso()) +
				sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
				break;
				// "Enquadramento, Processo" ;
			default:
				chave = sys.Util.rPad(getAutos(i).getInfracao().getDscEnquadramento()," ",50) +
				sys.Util.formataDataYYYYMMDD(getAutos(i).getDatProcesso()) +
				sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
			}
			tmp = getAutos(i) ;
			tmp.setChaveSort(chave) ;
			getAutos().set(i,tmp);
		}
		QSort(getAutos(),getOrdClass()) ;
	}
	
	//	--------------------------------------------------------------------------
	public static void QSort(List lista,int lo0, int hi0,String ordem)
	throws sys.BeanException {
		int lo = lo0;
		int hi = hi0;
		AutoInfracaoBean tmp ;
		if (lo >= hi) return;
		else if( lo == hi - 1 ) {
			/*
			 *  sort a two element list by swapping if necessary
			 */
			if (QSortCompara((AutoInfracaoBean)lista.get(lo),(AutoInfracaoBean)lista.get(hi),ordem))   {  // a[lo] > a[hi])
				tmp = (AutoInfracaoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(AutoInfracaoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}
			return;
		}
		/*
		 *  Pick a pivot and move it out of the way
		 */
		AutoInfracaoBean pivot = (AutoInfracaoBean)lista.get((lo + hi) / 2);
		lista.set( ((lo + hi)/2),lista.get(hi));
		lista.set(hi,pivot) ;
		
		while( lo < hi ) {
			/*
			 *  Search forward from a[lo] until an element is found that
			 *  is greater than the pivot or lo >= hi
			 */
			while ( !(QSortCompara((AutoInfracaoBean)lista.get(lo),pivot,ordem)) && (lo < hi) )       {    //(a[lo] <= pivot) && (lo < hi))
				lo++;
			}
			/*
			 *  Search backward from a[hi] until element is found that
			 *  is less than the pivot, or lo &gt;= hi
			 */
			while ( !(QSortCompara(pivot,(AutoInfracaoBean)lista.get(hi),ordem)) && (lo < hi) ) {    // (pivot <= a[hi])
				hi--;
			}
			/*
			 *  Swap elements a[lo] and a[hi]
			 */
			if( lo < hi ) {
				tmp = (AutoInfracaoBean)lista.get(lo);             // a[lo]
				lista.set(lo,(AutoInfracaoBean)lista.get(hi)) ;         // a[lo] = a[hi]
				lista.set(hi,tmp) ;                     // a[hi] = T;
			}
		}
		/*
		 *  Put the median in the "center" of the list
		 */
		lista.set(hi0,(AutoInfracaoBean)lista.get(hi)) ;         // a[hi0] = a[hi];
		lista.set(hi,pivot) ;                     // a[hi] = pivot;
		/*
		 *  Recursive calls, elements a[lo0] to a[lo-1] are less than or
		 *  equal to pivot, elements a[hi+1] to a[hi0] are greater than
		 *  pivot.
		 */
		QSort(lista, lo0, lo-1,ordem);
		QSort(lista, hi+1, hi0,ordem);
	}
	
	public static void QSort(List lista,String ordem) throws sys.BeanException {
		QSort(lista, 0, lista.size()-1,ordem);
	}
	public static boolean QSortCompara(AutoInfracaoBean a0,AutoInfracaoBean a1,String ordem)
	throws sys.BeanException {
		boolean b = false ;
		if (ordem.equals("descendente")) b = (a1.getChaveSort()).compareTo(a0.getChaveSort())>0 ;
		else							 b = (a0.getChaveSort()).compareTo(a1.getChaveSort())>0 ;
		return b ;
	}
	
	//----------------------------------------------------------------------------------
	public boolean gravaArquivo(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId) throws Exception{
		try{
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = codEnvioDO;
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_" + nomAta+ ".rtf");
			if(!codStatus.equals("-1")){
				if(file.exists())
					return false;
			}
			file.createNewFile();
			if(!file.canWrite()){
				setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}
			nomArquivoATA = file.getName();
			int quantReq = 0;
			FileWriter fwrite = new FileWriter(file);
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_EMPRESA_ATA","SECRETARIA DE ESTADO DE SEGURAN�A","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_DEPARTAMENTO","DEPARTAMENTO DE TR�NSITO","3")+"\n");
			fwrite.write(""+ParamOrgaoId.getParamOrgao("NOME_JARI","JUNTA ADMINISTRATIVA DE RECURSOS DE INFRA��ES","3")+"\n");
			fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO);
			GuiaDistribuicaoBean myGuia = null;
			AutoInfracaoBean myAuto = null;
			String sigJunta = "", nomTitulo = "";
			for(int x=0; x<Autos.size() ;x++){
				myGuia = (GuiaDistribuicaoBean)Autos.get(x);
				if(!myGuia.getSigJunta().equals(sigJunta)){
					sigJunta = myGuia.getSigJunta();
					fwrite.write("\n\nAtas da: "+myGuia.getSigJunta()+"\n");
					fwrite.write("Ata julgada em " + datEnvioDO + ", atrav�s do(a) n�: "+ codEnvioDO+" \n");					
				}
				/*fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + " atrav�s da Guia " + myGuia.getNumGuia() +" \n");*/
				
				
				if(!myGuia.getNomTituloRelator().equals(nomTitulo)){
					nomTitulo = myGuia.getNomTituloRelator();
					fwrite.write("\n\nProcessos Distribu�dos a(o) Sr(a)." + myGuia.getNomTituloRelator() + ".\n");					
				}
				
				/*fwrite.write("Processos \t \t \t Requerimento \t \t \t Resultado \n");*/
				for(int y=0; y<myGuia.getAutos().size(); y++){
					myAuto = myGuia.getAutos(y);
					String myAutoInf=myAuto.getNumProcesso().replaceAll("/    /","/");
					fwrite.write(myAutoInf.trim()+", " + myAuto.getNumRequerimento().trim()+ " ("+myAuto.getRequerimentos(0).getNomResultRS() +")"+ (y%2==0?"\n":";"));
					quantReq++;
				}
			}
			fwrite.close();
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(!dao.gravaArquivo(this, String.valueOf(quantReq))){
				if(!file.delete()){
					setMsgErro("Erro F�sico no Arquivo");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		setMsgErro("Arquivo Gerado com Sucesso");
		return true;
	}
	//----------------------------------------------------------------------------------
	public List buscaArquivosAta(String dataIni, String dataFim) throws DaoException{
		List vetArquivos = null;
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			vetArquivos = dao.buscaArquivosAta(this,dataIni,dataFim);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		if(vetArquivos.size() == 0){
			msgErro = "Nenhum arquivo localizado para a data especificada !!!";
		}
		return vetArquivos;
	}
	//-----------------------------------------------------------------------------------------
	public void downloadArquivoAta() throws DaoException{
		
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.downloadArquivoAta(this,codEnvioDO);
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	//-----------------------------------------------------------------------------------------
	
	public void estornarGuia() throws sys.CommandException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(codStatus.equals("2")){
				codStatus = "8";
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				setCodStatus(codStatus);
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				
				dao.guiaEstornoJuntaRelator(this);
			}
			else if(codStatus.equals("3")){
				codStatus = "2";
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				setCodStatus(codStatus);
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				
				dao.guiaEstornoResultado(this);
			}
			else if(codStatus.equals("4")){
				codStatus = "3";
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				setCodStatus(codStatus);
				/*-------------------- C�digo para Carregar Tabela Orqacle ----------------------*/
				
				dao.guiaEstornoDO(this);
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEstornoCmd: " + ue.getMessage());
		}
		if(this.getMsgErro().length() == 0)
			this.setMsgErro("Estorno da Guia: " + numGuia + " efetuado com sucesso");
		return ;
	}
	//---------------------------------------------------------------------------------------------
	public void consultaAtaImprime(int ordem) throws Exception{
		try{
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.consultaAta(this,ordem);
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
	}
	
	//---------------------------------------------------------------------------------------------
	public static String formatedLast2Month() {
		
		Calendar calendar = new GregorianCalendar();
		java.util.Date today = new java.util.Date();
		calendar.setTime(today);
		calendar.add(Calendar.DAY_OF_MONTH, -60);
		String dia = calendar.get(Calendar.DAY_OF_MONTH) + "";
		String mes = calendar.get(Calendar.MONTH) + 1 + ""; // deve somar um mesmo !!
		String ano = calendar.get(Calendar.YEAR) + "";
		String hor = calendar.get(Calendar.HOUR_OF_DAY) + "";
		String min = calendar.get(Calendar.MINUTE) + "";
		String seg = calendar.get(Calendar.SECOND) + "";
		if (dia.length() < 2) {
			dia = "0" + dia;
		}
		if (mes.length() < 2) {
			mes = "0" + mes;
		}
		if (hor.length() < 2) {
			hor = "0" + hor;
		}
		if (min.length() < 2) {
			min = "0" + min;
		}
		if (seg.length() < 2) {
			seg = "0" + seg;
		}
		// dd/mm/yyyy, hh24:mi:ss
		return dia + "/" + mes + "/" + ano + " , " + hor + ":" + min + ":" + seg;
		
	} // Fim formatedToday
	
	public boolean gravaArquivoNit(ParamSistemaBean paramSys, ParamOrgBean ParamOrgaoId, GuiaDistribuicaoBean  GuiaDistribuicaoId) throws Exception{
		try{
			paramSys.PreparaParam();
			File file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA"));
			file.mkdirs();
			String nomAta = codEnvioDO;
			for(int i=0;i<nomAta.length();i++){
				switch(nomAta.charAt(i)){
				case '/': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '*': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '"': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '?': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case ':': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '>': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				case '<': nomAta = nomAta.replace(nomAta.charAt(i),'_'); break;
				}
			}
			file = new File(paramSys.getParamSist("DIR_ARQUIVO_ATA") + "//ATA_" + nomAta+ ".txt");
			if(!codStatus.equals("-1")){
				if(file.exists())
					return false;
			}
			file.createNewFile();
			if(!file.canWrite()){
				setMsgErro("Erro na Grava��o F�sica do Arquivo");
				return true;
			}
			nomArquivoATA = file.getName();
			int quantReq = 0;
			FileWriter fwrite = new FileWriter(file);
			
			REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();  
			REC.GuiaDistribuicaoBean myGuia = new REC.GuiaDistribuicaoBean();
			REC.RelatorBean myRel = new REC.RelatorBean();
			String presidente = "";
			String nomRel = "";
			List guias = GuiaDistribuicaoId.getAutos();
			String nomRelatorPres = "";
			String nomRelator = "";
			for(int x = 0;x < guias.size();x++){
				myGuia = (REC.GuiaDistribuicaoBean)guias.get(x);
				
				// Encontro o Presidente da Junta
				myRel.setCodJunta(myGuia.getCodJunta());
				List relatores = myRel.getJuntaRelator();
				for(int y = 0;y < relatores.size();y++)
				{    
					REC.RelatorBean nomPresidente = (REC.RelatorBean)relatores.get(y);
					if (nomPresidente.getNomTitulo().equals("PRESIDENTE") && nomRelatorPres.equals(nomPresidente.getNomRelator()))
						presidente = nomPresidente.getNomRelator();
					nomRelatorPres = myGuia.getNomRelator();
				}
				
				// Encontro os Relatores da Junta
				if(!nomRelator.equals(myGuia.getNomRelator())&&
						(nomRel.indexOf(myGuia.getNomRelator())< 0)){
					nomRel += myGuia.getNomRelator()+ ", ";
				}
				nomRelator = myGuia.getNomRelator();
			}  
			
			fwrite.write("Secretaria Municipal de Servi�os P�blicos, Tr�nsito e \nTransportes Junta Administrativa de Recursos de Infra��es - \nJARI 01 \n\n");
			fwrite.write(myGuia.getNumSessao()+ "� Sess�o Ordin�ria da JARI 01, realizada em " + myGuia.getDatSessao()+".\n\n");
			fwrite.write("Presid�ncia: "+ presidente +". Membros efetivos participantes: \n");
			if(!nomRel.equals(""))
			{ 
				fwrite.write(nomRel.substring(0,nomRel.length()-2)+"\n");
			}     
			fwrite.write("\n\n1.Instala��o, verifica��o de quorum e abertura da sess�o \npelo presidente da JARI 01. \n\n");
			fwrite.write("2.Julgamento de recursos destinados ao cancelamento de \nmultas por infra��es de tr�nsito. \n\n");
			fwrite.write("2.1.Vistos, relatados e discutidos, os recursos de multa, \nabaixo elencados, decidem os membros da JARI 01, em \nsess�o plen�ria:\n\n"); 
			List autos = null; 
			String nomRelat = "";
			for(int i = 0;i < guias.size();i++)
			{
				myGuia = (REC.GuiaDistribuicaoBean)guias.get(i);
				autos = myGuia.getAutos();
				if(!myGuia.getNomRelator().equals(nomRelat))
				{   
					fwrite.write("2.1."+(i+1)+ ".Relator: "+myGuia.getNomRelator()+"\n");
				} 
				nomRelat = myGuia.getNomRelator();
				fwrite.write("Recurso de multa n�\t \t \tDecis�o da JARI 01 \n");
				String codResultado = "";
				for(int z=0;z<autos.size();z++){
					myAuto = (REC.AutoInfracaoBean)autos.get(z);
					if(myAuto.getRequerimentos(0).getCodResultRS().equals("D"))
					{
						codResultado = "Deferido";
					}else if(myAuto.getRequerimentos(0).getCodResultRS().equals("I"))
					{
						codResultado = "Indeferido";   
					}
					fwrite.write(myAuto.getNumProcesso()+"\t \t \t"+ codResultado+"\n"); 
				} 
			} 
			fwrite.write("\nNada mais havendo, encerrada a sess�o e lavrada e \nassinada a devida ata."); 
			fwrite.close();
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(!dao.gravaArquivo(this, String.valueOf(quantReq)))
			{
				if(!file.delete()){
					setMsgErro("Erro F�sico no Arquivo.");
					return true;
				}
				return false;
			}
		}
		catch(Exception e){
			throw new Exception(e.getMessage());
		}
		setMsgErro("Arquivo Gerado com Sucesso.");
		return true;
	}
	
    public ArrayList LeGuiaNit() throws sys.BeanException {
        ArrayList guias = new ArrayList();
        try {
            GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
            guias = dao.GuiaLeNit(this);
            /*Classifica("Processo");*/
        }
        catch (Exception ex) {
            throw new sys.BeanException(ex.getMessage());
        }
        return guias;
    }	
	
}