package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class EtiquetasCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/Etiquetas.jsp" ;  
   
  public EtiquetasCmd() {
    next             =  jspPadrao;
  }

  public EtiquetasCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);	
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;
			ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	    // cria os Beans, se n�o existir
    	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
	    if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
	    EtiquetasBean EtiquetasId     = (EtiquetasBean)req.getAttribute("EtiquetasId") ;
	    if (EtiquetasId==null)  EtiquetasId = new EtiquetasBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    EtiquetasId.setEventoOK(AutoInfracaoBeanId) ;
	    }
	    if  (acao.equals("Etiquetas"))  {
	      	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			AutoInfracaoBeanId.LeHistorico(UsrLogado);	    			    		
			HistoricoBean myHis = new HistoricoBean();
			RequerimentoBean re = AutoInfracaoBeanId.getRequerimentos(req.getParameter("codRequerimentoAuto"));			
			myHis = AutoInfracaoBeanId.getHist(re,"206,326,334,214,238,236,356,364");
			String cProcesso = req.getParameter("chkProcesso");
			req.setAttribute("ExisteProcesso",("P".equals(cProcesso) ? "S" : "N")) ;
			req.setAttribute("HistoricoBeanId",myHis) ;					
			if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","R","2").indexOf("R")>=0){
				AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
				String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
				AbrirRecBeanId.setNomeImagem(nomeImagem);

                String alturaImageGra = "80";
                AbrirRecBeanId.setAlturaImageGra(alturaImageGra);
                
                String larguraImageGra = "175";
                AbrirRecBeanId.setLarguraImageGra(larguraImageGra);
                
                String alturaImagePeq = "80";
                AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);
                
                String larguraImagePeq = "175";
                AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);
                
                String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
                AbrirRecBeanId.setObservacao(obs);
                
                String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
                AbrirRecBeanId.setSigProtocolo(siglaProt);
                
                //Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
				String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			    if (!codEventoFiltro.equals("N")){
			    	AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
			    }
			    req.setAttribute("codEventoFiltro",codEventoFiltro);
			    
				nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
				req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	
			}
		    else
				nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;				
	    }
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
	    req.setAttribute("EtiquetasId",EtiquetasId) ;		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("EtiquetasCmd1: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
}
