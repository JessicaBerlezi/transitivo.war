package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>      Recurso - Ajuste Manual<br>
* <b>Description:</b>Transacao 415:AJUSTE MANUAL DO PROPRIET�RIO <br>
* <b>Copyright:</b>  Copyright (c) 2005<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/

public class AjusteManualProprietarioCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteManualProprietario.jsp";

	public AjusteManualProprietarioCmd() {
		next = jspPadrao;
	}

	public AjusteManualProprietarioCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
		  nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;

			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  	
	
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";

			
			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AutoInfracaoBeanId.setMsgOk("S");
					
			}
			
			if (acao.equals("ConfirmaAjuste")) { 	
				  AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
				  AutoInfracaoBeanId.getProprietario().setNomResponsavel(req.getParameter("nomResp"));
				  AutoInfracaoBeanId.getProprietario().setNumCnh(req.getParameter("numCNH"));
				  AutoInfracaoBeanId.getProprietario().setCodUfCnh(req.getParameter("codUfCNHTR"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
				  AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
				  AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNH"));
				  if ("0".equals(AutoInfracaoBeanId.getProprietario().getIndTipoCnh())) AutoInfracaoBeanId.getProprietario().setIndTipoCnh("1");
				  else  AutoInfracaoBeanId.getProprietario().setIndTipoCnh("2");
		 
				  String numCPF = req.getParameter("cpfhidden");
				  AutoInfracaoBeanId.getProprietario().setNumCpfCnpj(numCPF);
				  // Valida se � CPF ou CPNJ		
				  if (AutoInfracaoBeanId.getProprietario().getNumCpfCnpj().length() == 11) AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("1");
				  else  AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("2");
				  
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNomBairro(req.getParameter("nomBairro"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNomCidade(req.getParameter("nomCidade"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNumCEPEdt(req.getParameter("numCEPTR"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setCodUF(req.getParameter("codUf"));
				  RequerimentoId.setDatRequerimento(req.getParameter("dataReq"));	
				  
				  HistoricoBean myHis = new HistoricoBean();
				  myHis.setCodAutoInfracao(AutoInfracaoBeanId.getCodAutoInfracao());   
				  myHis.setNumAutoInfracao(AutoInfracaoBeanId.getNumAutoInfracao()); 
				  myHis.setNumProcesso(AutoInfracaoBeanId.getNumProcesso());
				  myHis.setCodStatus(AutoInfracaoBeanId.getCodStatus());  
				  myHis.setDatStatus(AutoInfracaoBeanId.getDatStatus());
				  myHis.setCodEvento("415");
				  myHis.setCodOrigemEvento("100");		
				  myHis.setNomUserName(UsrLogado.getNomUserName());
				  myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				  myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
				  myHis.setTxtComplemento01(req.getParameter("dataReq"));
				  myHis.setTxtComplemento02(numCPF);			
				  myHis.setTxtComplemento03(req.getParameter("nomResp"));
				  myHis.setTxtComplemento04(req.getParameter("txtEndereco")+" "+req.getParameter("txtNumEndereco")+" "+req.getParameter("txtComplemento"));
				  myHis.setTxtComplemento05(req.getParameter("nomBairro"));
				  myHis.setTxtComplemento06(req.getParameter("nomCidade"));
				  myHis.setTxtComplemento07(req.getParameter("codUf"));
				  myHis.setTxtComplemento08(req.getParameter("numCEPTR"));
				  
				  DaoBroker dao = DaoBrokerFactory.getInstance();				  
				  dao.ajustarManualRespPecunia(AutoInfracaoBeanId,UsrLogado,RequerimentoId,myHis);				  
  
				  
				  if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
					   AutoInfracaoBeanId.setMsgErro("Ajuste Manual do Propriet�rio realizado com sucesso.");
					}
				}
			}

			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			req.setAttribute("RequerimentoId",RequerimentoId) ;
			req.setAttribute("UsrLogado",UsrLogado) ;

		}
		catch (Exception ue) {
			  throw new sys.CommandException("AjusteManualProprietarioCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	  }

}