package REC;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

/**
 * <b>Title:</b> SMIT - Entrada de Recurso - Historico Bean<br>
 * <b>Description:</b> Historico Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class HistoricoBean {
	public static final String RESULTADO_DP = "209";
	public static final String RESULTADO_1P = "329";
	public static final String RESULTADO_2P = "336";
	public static final String ATRIBUIR_ATA = "700";
	public static final String REMOVER_ATA = "701";
	public static final String EVENTO_ATRIBUIR_RELATOR = "208";
	public static final String EVENTO_REMOVER_RELATOR = "702";
	public static final String EVENTO_ABRIR_DP = "206";
	public static final String EVENTO_ABRIR_RECURSO_1P = "326";
	public static final String EVENTO_ABRIR_RECURSO_2P = "334";
	public static final String EVENTO_ABRIR_RECURSO_PAE = "1100";
	public static final String EVENTO_RESULTADO_DO_RECURSO_PAE_ALTERADO = "1101";

	private String msgErro;

	private String codHistoricoAuto;
	private String codAutoInfracao;
	private String numAutoInfracao;
	private String numProcesso;
	private String codOrgao;

	private String codStatus;
	private String nomStatus;
	private String datStatus;
	private String codEvento;
	private String nomEvento;
	private String codOrigemEvento;
	private String nomOrigemEvento;

	private String txtComplemento01;
	private String txtComplemento02;
	private String txtComplemento03;
	private String txtComplemento04;
	private String txtComplemento05;
	private String txtComplemento06;
	private String txtComplemento07;
	private String txtComplemento08;
	private String txtComplemento09;
	private String txtComplemento10;
	private String txtComplemento11;
	private String txtComplemento12;
	private String nomUserName;
	private String codOrgaoLotacao;
	private String sigOrgaoLotacao;
	private String datProc;
	private String codAta;
	private String dsAta;

	public String getCodAta() {
		return codAta;
	}

	public void setCodAta(String codAta) {
		this.codAta = codAta;
	}

	public String getDsAta() {
		return dsAta;
	}

	public void setDsAta(String dsAta) {
		this.dsAta = dsAta;
	}

	public HistoricoBean() throws sys.BeanException {

		msgErro = "";

		codHistoricoAuto = "";
		codAutoInfracao = "";
		numAutoInfracao = "";
		numProcesso = "";
		codOrgao = "";

		codStatus = "";
		nomStatus = "";
		datStatus = "";
		codEvento = "";
		nomEvento = "";
		codOrigemEvento = "";
		nomOrigemEvento = "";

		codOrigemEvento = "";
		txtComplemento01 = "";
		txtComplemento02 = "";
		txtComplemento03 = "";
		txtComplemento04 = "";
		txtComplemento05 = "";
		txtComplemento06 = "";
		txtComplemento07 = "";
		txtComplemento08 = "";
		txtComplemento09 = "";
		txtComplemento10 = "";
		txtComplemento11 = "";
		txtComplemento12 = "";
		nomUserName = "";
		codOrgaoLotacao = "";
		sigOrgaoLotacao = "";
		datProc = "";

	}

	public void setMsgErro(Vector vErro) {
		for (int j = 0; j < vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j);
		}
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public String getMsgErro() {
		return this.msgErro;
	}

	public void setCodHistoricoAuto(String codHistoricoAuto) {
		this.codHistoricoAuto = codHistoricoAuto;
		if (codHistoricoAuto == null)
			this.codHistoricoAuto = "";
	}

	public String getCodHistoricoAuto() {
		return this.codHistoricoAuto;
	}

	public void setCodAutoInfracao(String codAutoInfracao) {
		this.codAutoInfracao = codAutoInfracao;
		if (codAutoInfracao == null)
			this.codAutoInfracao = "";
	}

	public String getCodAutoInfracao() {
		return this.codAutoInfracao;
	}

	public void setNumAutoInfracao(String numAutoInfracao) {
		this.numAutoInfracao = numAutoInfracao;
		if (numAutoInfracao == null)
			this.numAutoInfracao = "";
	}

	public String getNumAutoInfracao() {
		return this.numAutoInfracao;
	}

	public void setNumProcesso(String numProcesso) {
		this.numProcesso = numProcesso;
		if (numProcesso == null)
			this.numProcesso = "";
	}

	public String getNumProcesso() {
		return this.numProcesso;
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}

	public String getCodOrgao() {
		return this.codOrgao;
	}

	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
		if (codStatus == null)
			this.codStatus = "";
	}

	public String getCodStatus() {
		return this.codStatus;
	}

	public void setNomStatus(String nomStatus) {
		this.nomStatus = nomStatus;
		if (nomStatus == null)
			this.nomStatus = "";
	}

	public String getNomStatus() {
		return this.nomStatus;
	}

	public void setDatStatus(String datStatus) {
		this.datStatus = datStatus;
		if (datStatus == null)
			this.datStatus = "";
	}

	public String getDatStatus() {
		return this.datStatus;
	}

	public void setCodEvento(String codEvento) {
		this.codEvento = codEvento;
		if (codEvento == null)
			this.codEvento = "";
	}

	public String getCodEvento() {
		return this.codEvento;
	}

	public void setNomEvento(String nomEvento) {
		this.nomEvento = nomEvento;
		if (nomEvento == null)
			this.nomEvento = "";
	}

	public String getNomEvento() {
		return this.nomEvento;
	}

	public void setCodOrigemEvento(String codOrigemEvento) {
		this.codOrigemEvento = codOrigemEvento;
		if (codOrigemEvento == null)
			this.codOrigemEvento = "";
	}

	public String getCodOrigemEvento() {
		return this.codOrigemEvento;
	}

	public void setNomOrigemEvento(String nomOrigemEvento) {
		this.nomOrigemEvento = nomOrigemEvento;
		if (nomOrigemEvento == null)
			this.nomOrigemEvento = "";
	}

	public String getNomOrigemEvento() {
		return this.nomOrigemEvento;
	}

	public void setTxtComplemento01(String txtComplemento01) {
		this.txtComplemento01 = txtComplemento01;
		if (txtComplemento01 == null)
			this.txtComplemento01 = "";
	}

	public String getTxtComplemento01() {
		return this.txtComplemento01;
	}

	public void setTxtComplemento02(String txtComplemento02) {
		this.txtComplemento02 = txtComplemento02;
		if (txtComplemento02 == null)
			this.txtComplemento02 = "";
	}

	public String getTxtComplemento02() {
		return this.txtComplemento02;
	}

	public void setTxtComplemento03(String txtComplemento03) {
		this.txtComplemento03 = txtComplemento03;
		if (txtComplemento03 == null)
			this.txtComplemento03 = "";
	}

	public String getTxtComplemento03() {
		return this.txtComplemento03;
	}

	public void setTxtComplemento04(String txtComplemento04) {
		this.txtComplemento04 = txtComplemento04;
		if (txtComplemento04 == null)
			this.txtComplemento04 = "";
	}

	public String getTxtComplemento04() {
		return this.txtComplemento04;
	}

	public void setTxtComplemento05(String txtComplemento05) {
		this.txtComplemento05 = txtComplemento05;
		if (txtComplemento05 == null)
			this.txtComplemento05 = "";
	}

	public String getTxtComplemento05() {
		return this.txtComplemento05;
	}

	public void setTxtComplemento06(String txtComplemento06) {
		this.txtComplemento06 = txtComplemento06;
		if (txtComplemento06 == null)
			this.txtComplemento06 = "";
	}

	public String getTxtComplemento06() {
		return this.txtComplemento06;
	}

	public void setTxtComplemento07(String txtComplemento07) {
		this.txtComplemento07 = txtComplemento07;
		if (txtComplemento07 == null)
			this.txtComplemento07 = "";
	}

	public String getTxtComplemento07() {
		return this.txtComplemento07;
	}

	public void setTxtComplemento08(String txtComplemento08) {
		this.txtComplemento08 = txtComplemento08;
		if (txtComplemento08 == null)
			this.txtComplemento08 = "";
	}

	public String getTxtComplemento08() {
		return this.txtComplemento08;
	}

	public void setTxtComplemento09(String txtComplemento09) {
		this.txtComplemento09 = txtComplemento09;
		if (txtComplemento09 == null)
			this.txtComplemento09 = "";
	}

	public String getTxtComplemento09() {
		return this.txtComplemento09;
	}

	public void setTxtComplemento10(String txtComplemento10) {
		this.txtComplemento10 = txtComplemento10;
		if (txtComplemento10 == null)
			this.txtComplemento10 = "";
	}

	public String getTxtComplemento10() {
		return this.txtComplemento10;
	}

	public void setTxtComplemento11(String txtComplemento11) {
		this.txtComplemento11 = txtComplemento11;
		if (txtComplemento11 == null)
			this.txtComplemento11 = "";
	}

	public String getTxtComplemento11() {
		return this.txtComplemento11;
	}

	public void setTxtComplemento12(String txtComplemento12) {
		this.txtComplemento12 = txtComplemento12;
		if (txtComplemento12 == null)
			this.txtComplemento12 = "";
	}

	public String getTxtComplemento12() {
		return this.txtComplemento12;
	}

	public void setNomUserName(String nomUserName) {
		this.nomUserName = nomUserName;
		if (nomUserName == null)
			this.nomUserName = "";
	}

	public String getNomUserName() {
		return this.nomUserName;
	}

	public void setCodOrgaoLotacao(String codOrgaoLotacao) {
		this.codOrgaoLotacao = codOrgaoLotacao;
		if (codOrgaoLotacao == null)
			this.codOrgaoLotacao = "";
	}

	public String getCodOrgaoLotacao() {
		return this.codOrgaoLotacao;
	}

	public void setSigOrgaoLotacao(String sigOrgaoLotacao) {
		this.sigOrgaoLotacao = sigOrgaoLotacao;
		if (sigOrgaoLotacao == null)
			this.sigOrgaoLotacao = "";
	}

	public String getSigOrgaoLotacao() {
		return this.sigOrgaoLotacao;
	}

	public void setDatProc(String datProc) {
		this.datProc = datProc;
		if (datProc == null)
			this.datProc = "";
	}

	public String getDatProc() {
		return this.datProc;
	}

	public void gravarHistorico(HistoricoBean myHis) throws sys.BeanException {
		try {
			DaoBroker dao = DaoBrokerFactory.getInstance();
			dao.gravarHistorico(myHis);
		} // fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}

	public static void cadastrarHistorico(RequerimentoBean requerimento, String numProcesso, String codOrgao,
			String dsAta, String nomeUsuario, String codAta, String evento) {
		try {
			HistoricoBean novoHistorico = new HistoricoBean();
			novoHistorico.setCodAutoInfracao(requerimento.getCodAutoInfracao());
			novoHistorico.setNumProcesso(numProcesso);
			novoHistorico.setTxtComplemento01(requerimento.getNumRequerimento());
			novoHistorico.setTxtComplemento02(requerimento.getCodTipoSolic());
			novoHistorico.setTxtComplemento03(requerimento.getCodResultRS());
			novoHistorico.setTxtComplemento12(requerimento.getCodRelatorJU());
			novoHistorico.setNomUserName(nomeUsuario);
			novoHistorico.setCodOrgaoLotacao(codOrgao);
			novoHistorico.setCodAta(codAta);
			novoHistorico.setDsAta(dsAta);
			novoHistorico.setCodEvento(evento);

			if (requerimento.getCodTipoSolic().equals("DP"))
				novoHistorico.setCodStatus("5");
			else if (requerimento.getCodTipoSolic().equals("1P"))
				novoHistorico.setCodStatus("15");
			else if (requerimento.getCodTipoSolic().equals("2P"))
				novoHistorico.setCodStatus("30");
			else if (requerimento.getCodTipoSolic().equals("PAE"))
				novoHistorico.setCodStatus(HistoricoBean.EVENTO_ABRIR_RECURSO_PAE);

			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String dataAtual = sdf.format(c.getTime());

			novoHistorico.setDatStatus(dataAtual);

			Dao.getInstance().cadastrarHistorico(novoHistorico);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	public static void cadastrarHistorico(HistoricoBean historico) {
		try {
			Calendar c = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			String dataAtual = sdf.format(c.getTime());

			historico.setDatStatus(dataAtual);

			Dao.getInstance().cadastrarHistorico(historico);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
}
