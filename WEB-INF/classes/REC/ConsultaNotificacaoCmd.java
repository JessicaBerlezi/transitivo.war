package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ConsultaNotificacaoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/ConsultaNotificacao.jsp" ;  
   
  public ConsultaNotificacaoCmd() {
    next             =  jspPadrao;
  }

  public ConsultaNotificacaoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
		ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  				
	    ParamOrgBean ParamOrgaoId             = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  {
			AutoInfracaoBeanId = new AutoInfracaoBean() ;	
			if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
			if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
			if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
			if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());				  			
	  	}
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if (acao==null)   acao = " ";
		if (acao.equals("LeNotificacao"))  {
			AutoInfracaoBeanId.setNumNotificacao(req.getParameter("numNotificacao"));
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			AutoInfracaoBeanId.LeAutoInfracao("notificacao",UsrLogado);
			UsrLogado.setCodOrgaoAtuacao(temp);			
		}	
		else {
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);
		}		
  		// processamento de saida dos formularios
	  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
    }
    catch (Exception ue) {
	      throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }		    
}