package REC;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>      Recurso - Ajuste<br>
* <b>Description:</b>Transacao 412:Ajuste Automatico CR da Venda<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/
public class AjusteAutomaticoCrCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/REC/AjusteAutomaticoCr.jsp";

	public AjusteAutomaticoCrCmd() {
		next = jspPadrao;
	}

	public AjusteAutomaticoCrCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try { 
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null) UsrLogado = new ACSS.UsuarioBean() ;
			ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null) ParamOrgaoId = new ParamOrgBean() ;	  			
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
			consultaAutoBean consultaAutoId = (consultaAutoBean)session.getAttribute("consultaAutoId") ;
			if (consultaAutoId==null) consultaAutoId = new consultaAutoBean() ;	  		
			    
			//	obtem e valida os parametros recebidos
			String acao = req.getParameter("acao");
			if (acao == null)	acao = "";
			
			String numPlaca = req.getParameter("numPlaca");
			if (numPlaca == null) numPlaca = "";
			
			
			if (acao.equals("ajustaCrVenda")) {
				AutoInfracaoBeanId.setMsgErro("") ;
				if (numPlaca.length()==0) AutoInfracaoBeanId.setMsgErro("Placa n�o informada.") ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {				
					AutoInfracaoBeanId.setNumPlaca(numPlaca);
					DaoBroker dao = DaoBrokerFactory.getInstance();
					ArrayList resutTrans412 = dao.ajustarCrVenda412(AutoInfracaoBeanId,UsrLogado) ;	

					if ("S".equals(AutoInfracaoBeanId.getMsgOk())) {	
						req.setAttribute("resutTrans412",resutTrans412);
						req.setAttribute("numPlaca",numPlaca);
						nextRetorno="/REC/AjusteAutomaticoCrDetalhes.jsp";				
						AutoInfracaoBeanId.setMsgErro("Ajuste Automatico (CR) para placa "+AutoInfracaoBeanId.getNumPlaca()+" realizado.\n") ;
					}
				} 
			}  
			
			if (acao.equals("MostraAuto")) { 
				consultaAutoId.setNumPlaca(numPlaca);
				consultaAutoId.ConsultaAutos(UsrLogado)	;
			 	if (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
				
				
			}
				
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId);
		}
		catch (Exception ue) {
		   throw new sys.CommandException("AjusteAutoaticoCrCmd: " + ue.getMessage());
	  }
		return nextRetorno;
	}
}
