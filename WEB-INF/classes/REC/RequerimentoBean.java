package REC;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import ACSS.Ata;
import sys.BeanException;

/**
 * <b>Title:</b> SMIT - Entrada de Recurso - Requerimento Bean<br>
 * <b>Description:</b> Requerimento Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class RequerimentoBean {
	private String msgErro;
	private String codAta;
	private String codRequerimento;
	private String codAutoInfracao;
	private String codTipoSolic;
	private String numRequerimento;
	private String codStatusRequerimento;
	private String datRequerimento;
	private String nomUserNameDP;
	private String codOrgaoLotacaoDP;
	private String codResponsavelDP;
	private String nomResponsavelDP;
	private String datProcDP;
	private String datPJ;
	private String codParecerPJ; // D-deferir ou I- indeferir
	private String txtMotivoPJ;
	private String codRespPJ;
	private String nomRespPJ;
	private String nomUserNamePJ;
	private String codOrgaoLotacaoPJ;
	private String datProcPJ;
	private String datJU;
	private String codJuntaJU;
	private String sigJuntaJU;
	private String codRelatorJU;
	private String nomRelatorJU;
	private String nomUserNameJU;
	private String codOrgaoLotacaoJU;
	private String datProcJU;
	private String datRS;
	private String codResultRS; // D-deferido ou I- indeferido
	private String txtMotivoRS;
	private String nomUserNameRS;
	private String codOrgaoLotacaoRS;
	private String datProcRS;
	private String datAtuTR;
	private String nomCondutorTR;
	private String numCNHTR;
	private String indTipoCNHTR;
	private String codUfCNHTR;
	private String numCPFTR;
	private String txtEnderecoTR;
	private String numCEPTR;
	private String nomBairroTR;
	private String nomCidadeTR;
	private String codCidadeTR;
	private String codUfTR;
	private String txtCategoriaTR;
	private String datValidTR;
	private String nomUserNameTR;
	private String codOrgaoLotacaoTR;
	private String datProcTR;
	private String datEST;
	private String txtMotivoEST;
	private String nomUserNameEST;
	private String codOrgaoLotacaoEST;
	private String datProcEST;
	private String datEnvioDO;
	private String codEnvioDO;
	private String datPublPDO;
	private String numGuiaDistr;
	private String sitProtocolo;
	private String codMotivoDef; // 1 - Inconsistencia do AI; 2 - Notificacao; 3
									// - Alegacoes de Defesa; 4 - Outros

	// Propriedades para contemplar as informa��es da Guia de Remessa
	private String datEnvioGuia;
	private String datRecebimentoGuia;
	private String nomUserNameEnvio;
	private String nomUserNameRecebimento;
	private String codRemessa;

	/* DPWEB */
	private String txtEmail;
	private String nomRequerente;
	private String txtJustificativa;
	private String indReqWEB;

	// DPWEB - Arquivo Requerimento
	private List<ArquivoRequerimentoBean> arquivosRequerimentoBean;

	public RequerimentoBean() throws sys.BeanException {

		msgErro = "";

		codRequerimento = "";
		codAutoInfracao = "";

		codTipoSolic = "";
		numRequerimento = "";
		datRequerimento = "";
		codStatusRequerimento = "";
		nomUserNameDP = "";
		codOrgaoLotacaoDP = "";
		codResponsavelDP = "";
		nomResponsavelDP = "";
		datProcDP = "";

		datPJ = "";
		codParecerPJ = ""; // D-deferir ou I- indeferir
		txtMotivoPJ = "";
		codRespPJ = "";
		nomRespPJ = "";
		nomUserNamePJ = "";
		codOrgaoLotacaoPJ = "";
		datProcPJ = "";

		datJU = "";
		codJuntaJU = "";
		sigJuntaJU = "";
		codRelatorJU = "";
		nomRelatorJU = "";
		nomUserNameJU = "";
		codOrgaoLotacaoJU = "";
		datProcJU = "";

		datRS = "";
		codResultRS = ""; // D-deferido ou I- indeferido
		txtMotivoRS = "";
		nomUserNameRS = "";
		codOrgaoLotacaoRS = "";
		datProcRS = "";

		datAtuTR = "";
		nomCondutorTR = "";
		numCNHTR = "";
		indTipoCNHTR = "2";
		codUfCNHTR = "";
		numCPFTR = "";
		txtEnderecoTR = "";
		nomBairroTR = "";
		nomCidadeTR = "";
		codCidadeTR = "";
		codUfTR = "";
		numCEPTR = "";
		txtCategoriaTR = "";
		datValidTR = "";

		nomUserNameTR = "";
		codOrgaoLotacaoTR = "";
		datProcTR = "";

		numGuiaDistr = "";
		sitProtocolo = "";

		datEST = "";
		txtMotivoEST = "";
		nomUserNameEST = "";
		codOrgaoLotacaoEST = "";
		datProcEST = "";

		datProcEST = "";
		datEnvioDO = "";
		codEnvioDO = "";
		datPublPDO = "";

		datEnvioGuia = "";
		datRecebimentoGuia = "";
		nomUserNameEnvio = "";
		nomUserNameRecebimento = "";
		codRemessa = "";

		codMotivoDef = ""; // 1 - Inconsistencia do AI; 2 - Notificacao; 3 -
							// Alegacoes de Defesa; 4 - Outros

		// DPWEB
		txtEmail = "";
		nomRequerente = "";
		txtJustificativa = "";
		indReqWEB = "0";

		codAta = "";

		arquivosRequerimentoBean = new ArrayList<ArquivoRequerimentoBean>();
	}

	public String getCodAta() {
		return codAta;
	}

	public void setCodAta(String codAta) {
		this.codAta = codAta;
	}

	public void setMsgErro(Vector<?> vErro) {
		for (int j = 0; j < vErro.size(); j++) {
			this.msgErro += vErro.elementAt(j);
		}
	}

	public void setMsgErro(String msgErro) {
		this.msgErro = msgErro;
	}

	public String getMsgErro() {
		return this.msgErro;
	}

	public void setCodRequerimento(String codRequerimento) {
		this.codRequerimento = codRequerimento;
		if (codRequerimento == null)
			this.codRequerimento = "";
	}

	public String getCodRequerimento() {
		return this.codRequerimento;
	}

	public void setCodAutoInfracao(String codAutoInfracao) {
		this.codAutoInfracao = codAutoInfracao;
		if (codAutoInfracao == null)
			this.codAutoInfracao = "";
	}

	public String getCodAutoInfracao() {
		return this.codAutoInfracao;
	}

	// --------------------------------------------------------------------------
	public void setCodTipoSolic(String codTipoSolic) {
		this.codTipoSolic = codTipoSolic;
		if (codTipoSolic == null)
			this.codTipoSolic = "";
	}

	public String getCodTipoSolic() {
		return this.codTipoSolic;
	}

	public String getNomTipoSolic() {
		String ns = "";
		if ("TR".equals(this.codTipoSolic))
			ns = "TROCA DE REAL INFRATOR";
		if ("DP".equals(this.codTipoSolic))
			ns = "DEFESA PR�VIA - PROPRIET�RIO";
		if ("DC".equals(this.codTipoSolic))
			ns = "DEFESA PR�VIA - CONDUTOR";
		if ("1P".equals(this.codTipoSolic))
			ns = "REC. 1a INST�NCIA - PROPRIET�RIO";
		if ("1C".equals(this.codTipoSolic))
			ns = "REC. 1a INST�NCIA - CONDUTOR";
		if ("2P".equals(this.codTipoSolic))
			ns = "REC. 2a INST�NCIA - PROPRIET�RIO";
		if ("2C".equals(this.codTipoSolic))
			ns = "REC. 2a INST�NCIA - CONDUTOR";
		if ("RD".equals(this.codTipoSolic))
			ns = "REN�NCIA DE DEFESA PR�VIA";
		if ("DI".equals(this.codTipoSolic))
			ns = "INDEF. DE PLANO - DEF PR�VIA";
		if ("1I".equals(this.codTipoSolic))
			ns = "INDEF. DE PLANO - 1a INST�NCIA";
		if ("2I".equals(this.codTipoSolic))
			ns = "INDEF. DE PLANO - 2a INST�NCIA";

		return ns;
	}

	public void setNumRequerimento(String numRequerimento) {
		this.numRequerimento = numRequerimento;
		if (numRequerimento == null)
			this.numRequerimento = "";
	}

	public String getNumRequerimento() {
		return this.numRequerimento;
	}

	public void setDatRequerimento(String datRequerimento) {
		this.datRequerimento = datRequerimento;
		if (datRequerimento == null)
			this.datRequerimento = "";
	}

	public String getDatRequerimento() {
		return this.datRequerimento;
	}

	public void setCodStatusRequerimento(String codStatusRequerimento) {
		this.codStatusRequerimento = codStatusRequerimento;
		if (codStatusRequerimento == null)
			this.codStatusRequerimento = "";

		// Requerimento setado como conclu�do quando Indeferimento de Plano
		if ("DI,1I,2I".indexOf(this.codTipoSolic) >= 0)
			this.codStatusRequerimento = "0";
	}

	public String getCodStatusRequerimento() {
		return this.codStatusRequerimento;
	}

	public String getNomStatusRequerimento() {
		return getNomStatusRequerimento(this.codStatusRequerimento);
	}

	public String getNomStatusRequerimento(String codSta) {
		String ns = "";
		if ("0".equals(codSta))
			ns = "ABERTO ";
		if ("2".equals(codSta))
			ns = "JUNTA/RELATOR DEFINIDOS";
		if ("3".equals(codSta))
			ns = "RESULTADO INFORMADO";
		if ("4".equals(codSta))
			ns = "ENVIADO P/PUBLICA��O";
		if ("7".equals(codSta))
			ns = "CANCELADO";
		if ("8".equals(codSta))
			ns = "INV�LIDO";
		if ("9".equals(codSta))
			ns = "CONCLUIDO";
		return ns;
	}

	// --------------------------------------------------------------------------

	public void setNomUserNameDP(String nomUserNameDP) {
		this.nomUserNameDP = nomUserNameDP;
		if (nomUserNameDP == null)
			this.nomUserNameDP = "";
	}

	public String getNomUserNameDP() {
		return this.nomUserNameDP;
	}

	public void setCodOrgaoLotacaoDP(String codOrgaoLotacaoDP) {
		this.codOrgaoLotacaoDP = codOrgaoLotacaoDP;
		if (codOrgaoLotacaoDP == null)
			this.codOrgaoLotacaoDP = "";
	}

	public String getCodOrgaoLotacaoDP() {
		return this.codOrgaoLotacaoDP;
	}

	public void setDatProcDP(String datProcDP) {
		this.datProcDP = datProcDP;
		if (datProcDP == null)
			this.datProcDP = "";
	}

	public String getDatProcDP() {
		return datProcDP;
	}

	public void setCodResponsavelDP(String string) {
		codResponsavelDP = string;
		if (codResponsavelDP == null)
			this.codResponsavelDP = "";
	}

	public String getCodResponsavelDP() {
		return codResponsavelDP;
	}

	public void setNomResponsavelDP(String nomResponsavelDP) {
		this.nomResponsavelDP = nomResponsavelDP;
		if (nomResponsavelDP == null)
			this.nomResponsavelDP = "";
	}

	public String getNomResponsavelDP() {
		return this.nomResponsavelDP;
	}

	// --------------------------------------------------------------------------
	public void setDatPJ(String datPJ) {
		this.datPJ = datPJ;
		if (datPJ == null)
			this.datPJ = "";
	}

	public String getDatPJ() {
		return this.datPJ;
	}

	public void setCodParecerPJ(String codParecerPJ) {
		this.codParecerPJ = codParecerPJ;
		if (codParecerPJ == null)
			this.codParecerPJ = "";
	}

	public String getCodParecerPJ() {
		return this.codParecerPJ;
	}

	public String getNomResultPJ() {
		String n = "";
		if ("D".equals(this.codParecerPJ))
			n = "Deferido";
		if ("I".equals(this.codParecerPJ))
			n = "Indeferido";
		if ("E".equals(this.codParecerPJ))
			n = "Estornado";
		return n;
	}

	public void setTxtMotivoPJ(String txtMotivoPJ) {
		this.txtMotivoPJ = txtMotivoPJ;
		if (txtMotivoPJ == null)
			this.txtMotivoPJ = "";
	}

	public String getTxtMotivoPJ() {
		return this.txtMotivoPJ;
	}

	public void setCodRespPJ(String codRespPJ) {
		this.codRespPJ = codRespPJ;
		if (codRespPJ == null)
			this.codRespPJ = "";
	}

	public String getCodRespPJ() {
		return this.codRespPJ;
	}

	public void setNomRespPJ(String nomRespPJ) {
		this.nomRespPJ = nomRespPJ;
		if (nomRespPJ == null)
			this.nomRespPJ = "";
	}

	public String getNomRespPJ() {
		return this.nomRespPJ;
	}

	public void setNomUserNamePJ(String nomUserNamePJ) {
		this.nomUserNamePJ = nomUserNamePJ;
		if (nomUserNamePJ == null)
			this.nomUserNamePJ = "";
	}

	public String getNomUserNamePJ() {
		return this.nomUserNamePJ;
	}

	public void setCodOrgaoLotacaoPJ(String codOrgaoLotacaoPJ) {
		this.codOrgaoLotacaoPJ = codOrgaoLotacaoPJ;
		if (codOrgaoLotacaoPJ == null)
			this.codOrgaoLotacaoPJ = "";
	}

	public String getCodOrgaoLotacaoPJ() {
		return this.codOrgaoLotacaoPJ;
	}

	public void setDatProcPJ(String datProcPJ) {
		this.datProcPJ = datProcPJ;
		if (datProcPJ == null)
			this.datProcPJ = "";
	}

	public String getDatProcPJ() {
		return this.datProcPJ;
	}

	// --------------------------------------------------------------------------
	public void setDatJU(String datJU) {
		this.datJU = datJU;
		if (datJU == null)
			this.datJU = "";
	}

	public String getDatJU() {
		return this.datJU;
	}

	public void setCodJuntaJU(String codJuntaJU) {
		this.codJuntaJU = codJuntaJU;
		if (codJuntaJU == null)
			this.codJuntaJU = "";
	}

	public String getCodJuntaJU() {
		return this.codJuntaJU;
	}

	public void setSigJuntaJU(String sigJuntaJU) {
		this.sigJuntaJU = sigJuntaJU;
		if (sigJuntaJU == null)
			this.sigJuntaJU = "";
	}

	public String getSigJuntaJU() {
		return this.sigJuntaJU;
	}

	public void setCodRelatorJU(String codRelatorJU) {
		this.codRelatorJU = codRelatorJU;
		if (codRelatorJU == null)
			this.codRelatorJU = "";
	}

	public String getCodRelatorJU() {
		return this.codRelatorJU;
	}

	public void setNomRelatorJU(String nomRelatorJU) {
		this.nomRelatorJU = nomRelatorJU;
		if (nomRelatorJU == null)
			this.nomRelatorJU = "";
	}

	public String getNomRelatorJU() {
		return this.nomRelatorJU;
	}

	public void setNomUserNameJU(String nomUserNameJU) {
		this.nomUserNameJU = nomUserNameJU;
		if (nomUserNameJU == null)
			this.nomUserNameJU = "";
	}

	public String getNomUserNameJU() {
		return this.nomUserNameJU;
	}

	public void setCodOrgaoLotacaoJU(String codOrgaoLotacaoJU) {
		this.codOrgaoLotacaoJU = codOrgaoLotacaoJU;
		if (codOrgaoLotacaoJU == null)
			this.codOrgaoLotacaoJU = "";
	}

	public String getCodOrgaoLotacaoJU() {
		return this.codOrgaoLotacaoJU;
	}

	public void setDatProcJU(String datProcJU) {
		this.datProcJU = datProcJU;
		if (datProcJU == null)
			this.datProcJU = "";
	}

	public String getDatProcJU() {
		return this.datProcJU;
	}

	// --------------------------------------------------------------------------
	public void setDatRS(String datRS) {
		this.datRS = datRS;
		if (datRS == null)
			this.datRS = "";
	}

	public String getDatRS() {
		return this.datRS;
	}

	public void setCodResultRS(String codResultRS) {
		this.codResultRS = codResultRS;
		if (codResultRS == null)
			this.codResultRS = "";
	}

	public String getCodResultRS() {
		return this.codResultRS;
	}

	public String getNomResultRS() {
		String n = "";
		if ("D".equals(this.codResultRS))
			n = "Deferido";
		if ("I".equals(this.codResultRS))
			n = "Indeferido";
		if ("E".equals(this.codResultRS))
			n = "Estornado";
		return n;
	}

	public void setTxtMotivoRS(String txtMotivoRS) {

		if (txtMotivoRS == null)
			txtMotivoRS = "";
		if (txtMotivoRS.length() == 0)
			txtMotivoRS = ".";
		this.txtMotivoRS = txtMotivoRS;
	}

	public String getTxtMotivoRS() {
		return this.txtMotivoRS;
	}

	public void setNomUserNameRS(String nomUserNameRS) {
		this.nomUserNameRS = nomUserNameRS;
		if (nomUserNameRS == null)
			this.nomUserNameRS = "";
	}

	public String getNomUserNameRS() {
		return this.nomUserNameRS;
	}

	public void setCodOrgaoLotacaoRS(String codOrgaoLotacaoRS) {
		this.codOrgaoLotacaoRS = codOrgaoLotacaoRS;
		if (codOrgaoLotacaoRS == null)
			this.codOrgaoLotacaoRS = "";
	}

	public String getCodOrgaoLotacaoRS() {
		return this.codOrgaoLotacaoRS;
	}

	public void setDatProcRS(String datProcRS) {
		this.datProcRS = datProcRS;
		if (datProcRS == null)
			this.datProcRS = "";
	}

	public String getDatProcRS() {
		return this.datProcRS;
	}

	// --------------------------------------------------------------------------
	public void setDatAtuTR(String datAtuTR) {
		this.datAtuTR = datAtuTR;
		if (datAtuTR == null)
			this.datAtuTR = "";
	}

	public String getDatAtuTR() {
		return this.datAtuTR;
	}

	public void setNomCondutorTR(String nomCondutorTR) {
		this.nomCondutorTR = nomCondutorTR;
		if (nomCondutorTR == null)
			this.nomCondutorTR = "";
	}

	public String getNomCondutorTR() {
		return this.nomCondutorTR;
	}

	public void setNumCNHTR(String numCNHTR) {
		this.numCNHTR = numCNHTR;
		if (numCNHTR == null)
			this.numCNHTR = "";
	}

	public String getNumCNHTR() {
		return this.numCNHTR;
	}

	public void setIndTipoCNHTR(String indTipoCNHTR) {
		this.indTipoCNHTR = indTipoCNHTR;
		if (indTipoCNHTR == null)
			this.indTipoCNHTR = "2";
	}

	public String getIndTipoCNHTR() {
		return this.indTipoCNHTR;
	}

	public void setCodUfCNHTR(String codUfCNHTR) {
		this.codUfCNHTR = codUfCNHTR;
		if (codUfCNHTR == null)
			this.codUfCNHTR = "";
	}

	public String getCodUfCNHTR() {
		return this.codUfCNHTR;
	}

	public void setNumCPFTR(String numCPFTR) {
		this.numCPFTR = numCPFTR;
		if (numCPFTR == null)
			this.numCPFTR = "";
	}

	public String getNumCPFTR() {
		return this.numCPFTR;
	}

	public void setNumCPFTREdt(String numCpf) {
		if (numCpf == null)
			numCpf = "";
		if (numCpf.length() < 14)
			numCpf += "               ";
		this.numCPFTR = numCpf.substring(0, 3) + numCpf.substring(4, 7) + numCpf.substring(8, 11)
				+ numCpf.substring(12, 14);
	}

	public String getNumCPFTREdt() {
		if ("".equals(this.numCPFTR.trim()))
			return this.numCPFTR.trim();
		return this.numCPFTR = numCPFTR.substring(0, 3) + "." + numCPFTR.substring(3, 6) + "."
				+ numCPFTR.substring(6, 9) + "-" + numCPFTR.substring(9, 11);
	}

	public void setTxtEnderecoTR(String txtEnderecoTR) {
		this.txtEnderecoTR = txtEnderecoTR;
		if (txtEnderecoTR == null)
			this.txtEnderecoTR = "";
	}

	public String getTxtEnderecoTR() {
		return this.txtEnderecoTR;
	}

	public void setNomBairroTR(String nomBairroTR) {
		this.nomBairroTR = nomBairroTR;
		if (nomBairroTR == null)
			this.nomBairroTR = "";
	}

	public String getNomBairroTR() {
		return this.nomBairroTR;
	}

	public void setNomCidadeTR(String nomCidadeTR) {
		this.nomCidadeTR = nomCidadeTR;
		if (nomCidadeTR == null)
			this.nomCidadeTR = "";
	}

	public String getNomCidadeTR() {
		return this.nomCidadeTR;
	}

	public void setCodCidadeTR(String codCidadeTR) {
		this.codCidadeTR = codCidadeTR;
		if (codCidadeTR == null)
			this.codCidadeTR = "";
	}

	public String getCodCidadeTR() {
		return this.codCidadeTR;
	}

	public void setCodUfTR(String codUfTR) {
		this.codUfTR = codUfTR;
		if (codUfTR == null)
			this.codUfTR = "";
	}

	public String getCodUfTR() {
		return this.codUfTR;
	}

	public void setNumCEPTR(String numCEPTR) {
		this.numCEPTR = numCEPTR;
		if (numCEPTR == null)
			this.numCEPTR = "";
	}

	public String getNumCEPTR() {
		return this.numCEPTR;
	}

	public void setNumCEPTREdt(String numCEPTR) {
		if (numCEPTR == null)
			numCEPTR = "";
		if (numCEPTR.length() < 9)
			numCEPTR += "               ";
		this.numCEPTR = numCEPTR.substring(0, 5) + numCEPTR.substring(6, 9);
	}

	public String getNumCEPTREdt() {
		if ("".equals(this.numCEPTR.trim()))
			return this.numCEPTR.trim();
		if (this.numCEPTR.length() < 8)
			this.numCEPTR = this.numCEPTR + "          ";
		return this.numCEPTR.substring(0, 5) + "-" + this.numCEPTR.substring(5, 8);
	}

	public void setTxtCategoriaTR(String txtCategoriaTR) {
		this.txtCategoriaTR = txtCategoriaTR;
		if (txtCategoriaTR == null)
			this.txtCategoriaTR = "";
	}

	public String getTxtCategoriaTR() {
		return this.txtCategoriaTR;
	}

	public void setDatValidTR(String datValidTR) {
		this.datValidTR = datValidTR;
		if (datValidTR == null)
			this.datValidTR = "";
	}

	public String getDatValidTR() {
		return this.datValidTR;
	}

	public void setNomUserNameTR(String nomUserNameTR) {
		this.nomUserNameTR = nomUserNameTR;
		if (nomUserNameTR == null)
			this.nomUserNameTR = "";
	}

	public String getNomUserNameTR() {
		return this.nomUserNameTR;
	}

	public void setCodOrgaoLotacaoTR(String codOrgaoLotacaoTR) {
		this.codOrgaoLotacaoTR = codOrgaoLotacaoTR;
		if (codOrgaoLotacaoTR == null)
			this.codOrgaoLotacaoTR = "";
	}

	public String getCodOrgaoLotacaoTR() {
		return this.codOrgaoLotacaoTR;
	}

	public void setDatProcTR(String datProcTR) {
		this.datProcTR = datProcTR;
		if (datProcTR == null)
			this.datProcTR = "";
	}

	public String getDatProcTR() {
		return this.datProcTR;
	}

	// --------------------------------------------------------------------------
	public void setNumGuiaDistr(String numGuiaDistr) {
		this.numGuiaDistr = numGuiaDistr;
		if (numGuiaDistr == null)
			this.numGuiaDistr = "";
	}

	public String getNumGuiaDistr() {
		return this.numGuiaDistr;
	}

	public void setSitProtocolo(String sitProtocolo) {
		this.sitProtocolo = sitProtocolo;
		if (sitProtocolo == null)
			this.sitProtocolo = "";
	}

	public String getSitProtocolo() {
		return this.sitProtocolo;
	}

	// --------------------------------------------------------------------------
	public void setDatEST(String datEST) {
		this.datEST = datEST;
		if (datEST == null)
			this.datEST = "";
	}

	public String getDatEST() {
		return this.datEST;
	}

	public void setTxtMotivoEST(String txtMotivoEST) {
		this.txtMotivoEST = txtMotivoEST;
		if (txtMotivoEST == null)
			this.txtMotivoEST = "";
	}

	public String getTxtMotivoEST() {
		return this.txtMotivoEST;
	}

	public void setNomUserNameEST(String nomUserNameEST) {
		this.nomUserNameEST = nomUserNameEST;
		if (nomUserNameEST == null)
			this.nomUserNameEST = "";
	}

	public String getNomUserNameEST() {
		return this.nomUserNameEST;
	}

	public void setNomUserNameEnvio(String nomUserNameEnvio) {
		this.nomUserNameEnvio = nomUserNameEnvio;
		if (nomUserNameEnvio == null)
			this.nomUserNameEnvio = "";
	}

	public String getNomUserNameEnvio() {
		return this.nomUserNameEnvio;
	}

	public void setNomUserNameRecebimento(String nomUserNameRecebimento) {
		this.nomUserNameRecebimento = nomUserNameRecebimento;
		if (nomUserNameRecebimento == null)
			this.nomUserNameRecebimento = "";
	}

	public String getNomUserNameRecebimento() {
		return this.nomUserNameRecebimento;
	}

	public void setCodRemessa(String codRemessa) {
		this.codRemessa = codRemessa;
		if (codRemessa == null)
			this.codRemessa = "";
	}

	public String getCodRemessa() {
		return this.codRemessa;
	}

	public void setCodOrgaoLotacaoEST(String codOrgaoLotacaoEST) {
		this.codOrgaoLotacaoEST = codOrgaoLotacaoEST;
		if (codOrgaoLotacaoEST == null)
			this.codOrgaoLotacaoEST = "";
	}

	public String getCodOrgaoLotacaoEST() {
		return this.codOrgaoLotacaoEST;
	}

	public void setDatProcEST(String datProcEST) {
		this.datProcEST = datProcEST;
		if (datProcEST == null)
			this.datProcEST = "";
	}

	public String getDatProcEST() {
		return this.datProcEST;
	}

	public void setDatEnvioGuia(String datEnvioGuia) {
		this.datEnvioGuia = datEnvioGuia;
		if (datEnvioGuia == null)
			this.datEnvioGuia = "";
	}

	public String getDatEnvioGuia() {
		return this.datEnvioGuia;
	}

	public void setDatRecebimentoGuia(String datRecebimentoGuia) {
		this.datRecebimentoGuia = datRecebimentoGuia;
		if (datRecebimentoGuia == null)
			this.datRecebimentoGuia = "";
	}

	public String getDatRecebimentoGuia() {
		return this.datRecebimentoGuia;

	}

	public void setDatPublPDO(String datPublPDO) {
		if (datPublPDO == null)
			this.datPublPDO = "";
		else
			this.datPublPDO = datPublPDO;
	}

	public String getDatPublPDO() {
		return datPublPDO;
	}

	public void setDatEnvioDO(String datEnvioDO) {
		this.datEnvioDO = datEnvioDO;
		if (datEnvioDO == null)
			this.datEnvioDO = "";
	}

	public String getDatEnvioDO() {
		return datEnvioDO;
	}

	public void setCodEnvioDO(String codEnvioDO) {
		this.codEnvioDO = codEnvioDO;
		if (codEnvioDO == null)
			this.codEnvioDO = "";
	}

	public String getCodEnvioDO() {
		return codEnvioDO;
	}

	public void setCodMotivoDef(String codMotivoDef) {
		if (codMotivoDef == null)
			codMotivoDef = "";
		else
			this.codMotivoDef = codMotivoDef;
	}

	public String getCodMotivoDef() {
		return codMotivoDef;
	}

	public String getNomMotivoDef() {
		String nome = "";
		if (this.codMotivoDef.equals("1"))
			nome = "Inconsist�ncia do AI";
		else if (this.codMotivoDef.equals("2"))
			nome = "Notifica��o";
		else if (this.codMotivoDef.equals("3"))
			nome = "Alega��es de Defesa";
		else if (this.codMotivoDef.equals("4"))
			nome = "Outros";
		return nome;
	}

	// --------------------------------------------------------------------------

	public void copiaRequerimento(RequerimentoBean reqIt) throws DaoException {
		// synchronized
		try {
			setCodRequerimento(reqIt.getCodRequerimento());

			setCodAutoInfracao(reqIt.getCodAutoInfracao());
			setCodTipoSolic(reqIt.getCodTipoSolic());
			setNumRequerimento(reqIt.getNumRequerimento());
			setDatRequerimento(reqIt.getDatRequerimento());
			setCodStatusRequerimento(reqIt.getCodStatusRequerimento());

			setNomUserNameDP(reqIt.getNomUserNameDP());
			setCodOrgaoLotacaoDP(reqIt.getCodOrgaoLotacaoDP());
			setCodResponsavelDP(reqIt.getCodResponsavelDP());
			setDatProcDP(reqIt.getDatProcDP());

			setDatPJ(reqIt.getDatPJ());
			setCodParecerPJ(reqIt.getCodParecerPJ());
			setTxtMotivoPJ(reqIt.getTxtMotivoPJ());
			setCodRespPJ(reqIt.getCodRespPJ());
			setNomRespPJ(reqIt.getNomRespPJ());
			setNomUserNamePJ(reqIt.getNomUserNamePJ());
			setCodOrgaoLotacaoPJ(reqIt.getCodOrgaoLotacaoPJ());
			setDatProcPJ(reqIt.getDatProcPJ());

			setDatJU(reqIt.getDatJU());
			setCodJuntaJU(reqIt.getCodJuntaJU());
			setSigJuntaJU(reqIt.getSigJuntaJU());
			setCodRelatorJU(reqIt.getCodRelatorJU());
			setNomRelatorJU(reqIt.getNomRelatorJU());
			setNomUserNameJU(reqIt.getNomUserNameJU());
			setCodOrgaoLotacaoJU(reqIt.getCodOrgaoLotacaoJU());
			setDatProcJU(reqIt.getDatProcJU());

			setDatRS(reqIt.getDatRS());
			setCodResultRS(reqIt.getCodResultRS());
			setTxtMotivoRS(reqIt.getTxtMotivoRS());
			setNomUserNameRS(reqIt.getNomUserNameRS());
			setCodOrgaoLotacaoRS(reqIt.getCodOrgaoLotacaoRS());
			setDatProcRS(reqIt.getDatProcRS());

			setDatAtuTR(reqIt.getDatAtuTR());
			setNomCondutorTR(reqIt.getNomCondutorTR());
			setNumCNHTR(reqIt.getNumCNHTR());
			setCodUfCNHTR(reqIt.getCodUfCNHTR());
			setNumCPFTR(reqIt.getNumCPFTR());
			setTxtEnderecoTR(reqIt.getTxtEnderecoTR());
			setNumCEPTR(reqIt.getNumCEPTR());
			setNomBairroTR(reqIt.getNomBairroTR());
			setNomCidadeTR(reqIt.getNomCidadeTR());
			setCodUfTR(reqIt.getCodUfTR());
			setNomUserNameTR(reqIt.getNomUserNameTR());
			setCodOrgaoLotacaoTR(reqIt.getCodOrgaoLotacaoTR());
			setDatProcTR(reqIt.getDatProcTR());
			setDatEST(reqIt.getDatEST());
			setTxtMotivoEST(reqIt.getTxtMotivoEST());
			setNomUserNameEST(reqIt.getNomUserNameEST());
			setCodOrgaoLotacaoEST(reqIt.getCodOrgaoLotacaoEST());
			setDatProcEST(reqIt.getDatProcEST());
			setCodRemessa(reqIt.getCodRemessa());

		} catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return;
	}

	public String getIndReqWEB() {
		return indReqWEB;
	}

	public void setIndReqWEB(String indReqWEB) {
		if (indReqWEB == null)
			indReqWEB = "0";
		this.indReqWEB = indReqWEB;
	}

	public String getNomRequerente() {
		return nomRequerente;
	}

	public void setNomRequerente(String nomRequerente) {
		if (nomRequerente == null)
			nomRequerente = "";
		this.nomRequerente = nomRequerente;
	}

	public String getTxtJustificativa() {
		return txtJustificativa;
	}

	public void setTxtJustificativa(String txtJustificativa) {
		if (txtJustificativa == null)
			txtJustificativa = "";
		this.txtJustificativa = txtJustificativa;
	}

	public String getTxtEmail() {
		return txtEmail;
	}

	public void setTxtEmail(String txtEmail) {
		if (txtEmail == null)
			txtEmail = "";
		this.txtEmail = txtEmail;
	}

	public List<ArquivoRequerimentoBean> getArquivosRequerimentoBean() {
		return arquivosRequerimentoBean;
	}

	public void setArquivosRequerimentoBean(List<ArquivoRequerimentoBean> arquivosRequerimentoBean) {
		this.arquivosRequerimentoBean = arquivosRequerimentoBean;
	}

	/**
	 * Percorre a lista de ArquivosRequerimento pelo tipo 1 (Documento)
	 * 
	 * @return
	 */
	public Boolean possuiDocumentoDPWeb() {
		if (!this.getArquivosRequerimentoBean().isEmpty()) {
			for (ArquivoRequerimentoBean arquivo : this.getArquivosRequerimentoBean()) {
				if (arquivo.getTipoArquivo().equals("1")) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Percorre a lista de ArquivosRequerimento pelo tipo 2 (CRLV)
	 * 
	 * @return
	 */
	public Boolean possuiCRLVDPWeb() {
		if (!this.getArquivosRequerimentoBean().isEmpty()) {
			for (ArquivoRequerimentoBean arquivo : this.getArquivosRequerimentoBean()) {
				if (arquivo.getTipoArquivo().equals("2")) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Percorre a lista de ArquivosRequerimento pelo tipo 3 (Procuracao)
	 * 
	 * @return
	 */
	public Boolean possuiProcuracaoDPWeb() {
		if (!this.getArquivosRequerimentoBean().isEmpty()) {
			for (ArquivoRequerimentoBean arquivo : this.getArquivosRequerimentoBean()) {
				if (arquivo.getTipoArquivo().equals("3")) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Percorre a lista de ArquivosRequerimentoBean, busca o nome do arquivo que
	 * cont�m o tipoArquivo informado no par�metro
	 * 
	 * @param tipoArquivo
	 *            (1-Documento, 2-CRLV, 3-Procura��o)
	 * @return
	 */
	public String getNomeArquivoPor(String tipoArquivo) {
		String nomeArquivo = "";
		if (!this.getArquivosRequerimentoBean().isEmpty()) {
			for (ArquivoRequerimentoBean arquivo : this.getArquivosRequerimentoBean()) {
				if (arquivo.getTipoArquivo().equals(tipoArquivo)) {
					nomeArquivo = arquivo.getNomeArquivo();
				}
			}
		}
		return nomeArquivo;
	}

	public static RequerimentoBean consultarRequerimentoPor(String codRequerimento) throws Exception {
		RequerimentoBean requerimento = Dao.getInstance().consultarRequerimentoPor(codRequerimento);

		return requerimento;
	}

	public static List<HashMap<String, String>> consultarRequerimentoPorAta(String codAta, String codRelator,
			String codOrgao) throws Exception {
		List<HashMap<String, String>> requerimento = Dao.getInstance().consultarRequerimentoPorAta(codAta, codRelator,
				codOrgao);

		return requerimento;
	}

	public static List<HashMap<String, String>> consultarRequerimentosPorAuto(String codAuto) throws Exception {
		List<HashMap<String, String>> requerimento = Dao.getInstance().consultarRequerimentosPorAuto(codAuto);

		return requerimento;
	}

	public static void cadastrar(RequerimentoBean requerimento) throws Exception {
		Dao.getInstance().cadastarRequerimento(requerimento);
	}

	public void atualizarDataRequerimento() throws Exception {
		Dao.getInstance().atualizarDataRequerimento(this);
	}

	public static void informarResultado(String[] codRequerimentos, String resultado, String usuario) throws Exception {
		if (resultado.equals("INDEFERIDO"))
			resultado = "I";
		else if (resultado.equals("DEFERIDO"))
			resultado = "D";
		else if (resultado.equals("SUSPENSO"))
			resultado = "S";
		else if (resultado.equals("ARQUIVADO"))
			resultado = "A";
		else if (resultado.equals("SEM RESULTADO"))
			resultado = "";
		
		for (int i = 0; i < codRequerimentos.length; i++) {
			String codStatus = "";
			RequerimentoBean requerimento = RequerimentoBean.consultarRequerimentoPor(codRequerimentos[i]);
			Ata ata = Ata.consultarAtaPor(requerimento.codAta);
			String tipoAta = ata.getTpAta();
			AutoInfracaoBean auto = AutoInfracaoBean.consultarAutoPor(Integer.parseInt(requerimento.codAutoInfracao));
			HistoricoBean historico = new HistoricoBean();

			Dao.getInstance().informarResultadoRequerimento(codRequerimentos[i], resultado);

			if (tipoAta.equals("Def. Previa")) {
				codStatus = AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA;
				if (resultado.isEmpty())
					codStatus = AutoInfracaoBean.STATUS_DP;
				else if (resultado.equals("D"))
					codStatus = AutoInfracaoBean.STATUS_DEFERIMENTO_DP;
			} else if (tipoAta.equals("1� INST�NCIA")) {
				codStatus = AutoInfracaoBean.STATUS_AGUARDANDO_2P;
				historico.setCodEvento(HistoricoBean.RESULTADO_1P);

				if (resultado.isEmpty())
					codStatus = AutoInfracaoBean.STATUS_RECURSO_1P;
				else if (resultado.equals("D"))
					codStatus = AutoInfracaoBean.STATUS_DEFERIMENTO_1P;
			} else if (tipoAta.equals("2� INST�NCIA")) {
				codStatus = AutoInfracaoBean.STATUS_TRANSITADO_JULGADO;
				historico.setCodEvento(HistoricoBean.RESULTADO_2P);

				if (resultado.isEmpty())
					codStatus = AutoInfracaoBean.STATUS_RECURSO_2P;
				else if (resultado.equals("D"))
					codStatus = AutoInfracaoBean.STATUS_DEFERIMENTO_2P;
			} else if (tipoAta.equals("PAE"))
				historico.setCodEvento(HistoricoBean.EVENTO_RESULTADO_DO_RECURSO_PAE_ALTERADO);
			
			auto.setCodStatus(codStatus);
			
			historico.setCodStatus(codStatus);
			historico.setTxtComplemento03(resultado);
			historico.setCodAutoInfracao(auto.getCodAutoInfracao());
			historico.setNumAutoInfracao(auto.getNumAutoInfracao());
			historico.setNumProcesso(auto.getNumProcesso());
			historico.setCodOrgao(auto.getCodOrgao());
			historico.setCodStatus(auto.getCodStatus());
			historico.setDatStatus(auto.getDatStatus());
			historico.setTxtComplemento01(requerimento.getNumRequerimento());
			historico.setTxtComplemento02(requerimento.getCodTipoSolic());
			historico.setNomUserName(usuario);
			historico.setCodOrgaoLotacao(auto.getCodOrgao());
			historico.setTxtComplemento12(requerimento.getCodRelatorJU());
			historico.setCodAta(ata.getCodAta());			
			historico.setDsAta(ata.getDescAta());
			
			AutoInfracaoBean.atualizar(auto);
			HistoricoBean.cadastrarHistorico(historico);
		}
	}

	public void deletar() throws Exception {
		Dao.getInstance().deletarRequerimento(this.codRequerimento);
	}
}