package REC;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.Util;

public class EnviarBancoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/EnviarBanco.jsp" ;  
   
  public EnviarBancoCmd() {
    next             =  jspPadrao;
  }

  public EnviarBancoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
    	sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
    	cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		
    	
    	// cria os Beans de sessao, se n�o existir
    	HttpSession session   = req.getSession() ;								
    	ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
    	ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
    	if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
    	
    	// cria os Beans, se n�o existir	  					
    	EnviarBancoBean EnviarBancoId     = (EnviarBancoBean)req.getAttribute("EnviarBancoId") ;
    	if (EnviarBancoId==null)  EnviarBancoId = new EnviarBancoBean() ;	  			
    	EnviarBancoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));
    	EnviarBancoId.setDatEnvioBanco(sys.Util.formatedToday().substring(0,10));		
    	AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
    	if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
    	// obtem e valida os parametros recebidos					
    	String acao           = req.getParameter("acao");  
    	String verBanco = req.getParameter("verBanco");
    	if(verBanco == null) verBanco = "N";	    
    	if(acao==null)   acao = " ";
    	if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
    		EnviarBancoId.setEventoOK(AutoInfracaoBeanId) ;
    		verBanco = "S";
    	}
    	if  (acao.equals("EnviarBanco"))  {				
    		EnviarBancoId.setEventoOK(AutoInfracaoBeanId) ;
    		nextRetorno = processaEnviaBanco(EnviarBancoId,req,AutoInfracaoBeanId,UsrLogado);    				
    		String Erro = AutoInfracaoBeanId.getMsgErro();
    		EnviarBancoId= new EnviarBancoBean() ;
    		AutoInfracaoBeanId = new AutoInfracaoBean() ;
    		AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
	  		AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
	  		AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
	  		AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());	  		
	  		AutoInfracaoBeanId.setMsgErro(Erro);
	  		req.setAttribute("LeAI","S");
    	}
    	req.setAttribute("verBanco", verBanco);
    	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
    	req.setAttribute("EnviarBancoId",EnviarBancoId) ;		
    }
    catch (Exception ue) {
    	throw new sys.CommandException("EnviarBancoCmd: " + ue.getMessage());
    }
    return nextRetorno;
  }
  
  private String processaEnviaBanco(EnviarBancoBean EnviarBancoId,HttpServletRequest req,
  		AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
  	String nextRetorno = next ;
  	try {
  		myAuto.setMsgErro("") ;
  		Vector vErro = new Vector() ;
  		// validar se status permite recurso ou defesa previa : 5,25,35
  		if ("S".equals(EnviarBancoId.getEventoOK())==false) vErro.addElement(EnviarBancoId.getMsg()+" \n") ;
  		// Pegar os parametros digitados - data, numero da CI		
  		String datEnvioBanco = sys.Util.formatedToday().substring(0,10);
  		String codEnvioBanco = req.getParameter("indMovimento");		
  		if (codEnvioBanco==null) codEnvioBanco = "";
  		if (codEnvioBanco.length()==0) vErro.addElement("Tipo de Movimenta��o n�o preenchido. \n") ;
		String dtStatus=Util.ValidaData(myAuto.getDatStatus());
		if (dtStatus.length()==0) vErro.addElement("Data do Status Inv�lida. \n") ;

  		if (vErro.size()==0) {
  			atualizarBanco(EnviarBancoId,myAuto,codEnvioBanco,UsrLogado) ;
  			if (myAuto.getMsgErro().length()==0) {
  				myAuto.setMsgErro("Efetuada a atualiza��o para Envio ao Banco: "+myAuto.getNumAutoInfracao()) ;	
  				myAuto.setMsgOk("S") ;
  			}										
  		}
  		else myAuto.setMsgErro(vErro) ;
  	}
  	catch (Exception e){
  		throw new sys.CommandException("EnviarBancoCmd: " + e.getMessage());
  	}
  	return nextRetorno  ;
  }
  private void atualizarBanco(EnviarBancoBean EnviarBancoId,AutoInfracaoBean myAuto,
  		String codEnvioBanco,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
  	try { 	
  		// preparar Bean de Historico
  		HistoricoBean myHis = new HistoricoBean() ;
  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
  		myHis.setCodStatus(myAuto.getCodStatus());  
  		myHis.setDatStatus(myAuto.getDatStatus());
  		myHis.setCodEvento(EnviarBancoId.getCodEvento());
  		myHis.setCodOrigemEvento(EnviarBancoId.getOrigemEvento());
  		myHis.setNomUserName(UsrLogado.getNomUserName());
  		myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
  		myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
  		myHis.setTxtComplemento01(codEnvioBanco);  						
  		DaoBroker dao = DaoBrokerFactory.getInstance();
  		dao.atualizarAuto407(myAuto,myHis,UsrLogado) ; 
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException(ex.getMessage());
  	}
  	return  ;
  }	
  
}