package REC;


import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class AjustaStatusCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/AjustaStatus.jsp" ;  
   
  public AjustaStatusCmd() {
    next = jspPadrao;
  }

  public AjustaStatusCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    // cria os Beans, se n�o existir	  			
	    AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)   	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  				   

		StatusBean StatusId   = (StatusBean)req.getAttribute("StatusId") ;
		if (StatusId==null)   	StatusId = new StatusBean() ;	  			  				   
		
		StatusId.setSigFuncao(req.getParameter("j_sigFuncao"));
	    String acao = req.getParameter("acao");  
	    if(acao==null){
	    	acao = " ";
	    	req.setAttribute("statusIteracao",null);
	    }
		if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  
			req.setAttribute("statusIteracao","1");
		if  (acao.equals("AjustaStatus"))  {
			nextRetorno = processaAjuste(req,AutoInfracaoBeanId,UsrLogado,StatusId);
		}
		if(acao.equals("Novo"))	req.setAttribute("statusIteracao",null);
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		req.setAttribute("StatusId",StatusId) ;
	}
	catch (Exception ue) {
		  throw new sys.CommandException("AjustaStatusCmd: " + ue.getMessage());
	}
	return nextRetorno;
  }
  private String processaAjuste(HttpServletRequest req,AutoInfracaoBean myAuto,
  	ACSS.UsuarioBean UsrLogado,StatusBean StatusId) throws sys.CommandException { 
	String nextRetorno = next ;
	try {
		myAuto.setMsgErro("") ;
		Vector vErro = new Vector() ;	
		//Valida o C�digo do Status
		String codStatus = req.getParameter("codStatus");
		if (codStatus==null) codStatus="";
		if( codStatus.compareTo("") == 0)
			vErro.addElement("C�digo do Status n�o Selecionado. \n") ;
		if ( (("5".equals(codStatus)) || ("15".equals(codStatus))) && (myAuto.getNumProcesso().length()==0))
			vErro.addElement("Auto sem N�mero de Processo, n�o podemos passar para Status: "+codStatus+". \n") ;
		String data = req.getParameter("Data");
		if (data==null) data = "" ;
		StatusId.setTxtMotivo(req.getParameter("txtMotivo"));

		//validar data de Notificacao
		if (data.length()==0) vErro.addElement("Data para o Novo Status n�o preenchida. \n") ;
		
		if (myAuto.getCodStatus().equals("99")) vErro.addElement("Status n�o Autorizado. \n") ;
		
		
		
		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),data)>0) vErro.addElement("Data da Notifica��o n�o pode ser superior a hoje. \n") ;
		if (vErro.size()==0) {
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(myAuto.getNumProcesso());
			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodEvento("411");
			myHis.setCodOrigemEvento("100");		
			myHis.setNomUserName(UsrLogado.getNomUserName());
			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			myHis.setTxtComplemento01(codStatus);
			myHis.setTxtComplemento02(data);			
			myHis.setTxtComplemento03(" ");
			myHis.setTxtComplemento04(StatusId.getTxtMotivo());
			DaoBroker dao = DaoBrokerFactory.getInstance();			
			dao.atualizarAuto411(myAuto,myHis,UsrLogado) ;
			if (myAuto.getMsgErro().length()==0) {  			
				myAuto.setMsgOk("S") ;										
				myAuto.setMsgErro("Status alterado para o Auto: "+myAuto.getNumAutoInfracao()+" em "+myAuto.getDatStatus() + "\nStatus: " + myAuto.getCodStatus() + " - "+ myAuto.getNomStatus()) ;
				myAuto.setDatStatus(data);
				myAuto.setCodStatus(codStatus);			
			}
			req.setAttribute("statusIteracao",null);
		}
		else{
			myAuto.setMsgErro(vErro) ;
			req.setAttribute("statusIteracao","1");
		}
	}
	catch (Exception e){
		throw new sys.CommandException("AjustaStatusCmd: " + e.getMessage());
	}
	return nextRetorno  ;
  }
}
