 package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.BeanException;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class InstrNotBean extends sys.HtmlPopupBean { 

  private String codInstrucaoNot;
  private String codTipoLinhaNot;
  private String numLinhaNot; 
  private String txtLinhaNot;  
  private String codOrgao; 
  private String codNotificacao; 
  private ACSS.OrgaoBean orgao;   
  private String atualizarDependente;
  private Vector parametros; 
  private String dscNotificacao;
  private String codOrgaoCp;
  private boolean sobrescreve;
  private String txtEnderecoRecurso;
  
  private List notificacoes; 
  
      
  public InstrNotBean() throws sys.BeanException {

    super();
    setTabela("TSMI_INSTRUCAO_NOT");	
    setPopupWidth(35);	    
    codInstrucaoNot   = "";
	codTipoLinhaNot   = "";	
    numLinhaNot       = "";
	txtLinhaNot	      = "";
	codOrgao	      = "0";
	dscNotificacao    = "";
	orgao    = new ACSS.OrgaoBean() ;
 	codNotificacao	  = "0";
	parametros        = new Vector() ;
    dscNotificacao	  = "";
    notificacoes      = new ArrayList() ;
    codOrgaoCp        = "";
    sobrescreve 	  = false;
    txtEnderecoRecurso= "";
  }

  //--------------------------------------------------------------------------
public void setCodInstrucaoNot(String codInstrucaoNot) {
  	 if (codInstrucaoNot == null ) 	this.codInstrucaoNot = "0";
  	 else    					this.codInstrucaoNot = codInstrucaoNot ;
  }
public String getCodInstrucaoNot()  { return this.codInstrucaoNot; }
  
//--------------------------------------------------------------------------
public void setCodTipoLinhaNot(String codTipoLinhaNot)  {
  	if (codTipoLinhaNot == null)  this.codTipoLinhaNot = "";
  	else			  	 	this.codTipoLinhaNot = codTipoLinhaNot;
  }
public String getCodTipoLinhaNot()  {	return this.codTipoLinhaNot; }
  
//--------------------------------------------------------------------------
public void setNumLinhaNot(String numLinhaNot)  {
  	if (numLinhaNot == null) this.numLinhaNot = null;
  	else				  	this.numLinhaNot = numLinhaNot;
  }
public String getNumLinhaNot()  { return this.numLinhaNot; }
//--------------------------------------------------------------------------
public void  setOrgao(ACSS.OrgaoBean orgao)  {
  	if (orgao == null) this.orgao = null;
  	else				  	this.orgao = orgao;
  }
public ACSS.OrgaoBean getOrgao()  { return this.orgao; }

//--------------------------------------------------------------------------
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
    }
	public String getAtualizarDependente() {	return this.atualizarDependente; }

//--------------------------------------------------------------------------
public void setTxtLinhaNot(String txtLinhaNot)  {
  	if (txtLinhaNot == null)	this.txtLinhaNot =""; 
  	else				  	this.txtLinhaNot = txtLinhaNot;
  }
public String getTxtLinhaNot()  { return this.txtLinhaNot; }

//--------------------------------------------------------------------------
public void setCodOrgao(String codOrgao)  {
  	if (codOrgao == null)	this.codOrgao ="0"; 
  	else				  	this.codOrgao = codOrgao;
  }
public String getCodOrgao()  { return this.codOrgao; }
//--------------------------------------------------------------------------
public void setCodNotificacao(String codNotificacao)  {
  	if (codNotificacao == null)	this.codNotificacao ="1"; 
  	else				  	this.codNotificacao = codNotificacao;
  }
public String getCodNotificacao()  { return this.codNotificacao; }
//--------------------------------------------------------------------------
public void setDscNotificacao(String dscNotificacao)  {
  	if (dscNotificacao == null)	this.dscNotificacao =""; 
  	else				  	this.dscNotificacao = dscNotificacao;
  }
public String getDscNotificacao()  { return this.dscNotificacao; }

//--------------------------------------------------------------------------
  public void setNotificacoes(List notificacoes)  {
  	 this.notificacoes=notificacoes ;	
   }
  public List getNotificacoes()  {
  	return this.notificacoes;
  }


//-----------------------------------------------------------
public String getNotificacoes(ACSS.UsuarioBean myUsr){
    String myNotificacao = "" ; 
    try    {
       DaoTabelas daoTabela = DaoTabelas.getInstance();
	   myNotificacao = daoTabela.getNotificacoes("") ;
	}
	catch(Exception e){	}	
	return myNotificacao ;
}

//-----------------------------------------------------------

  public void setCodOrgaoCp(String codOrgaoCp) {
		  this.codOrgaoCp = codOrgaoCp;
		  if (codOrgaoCp == null)
			  this.codOrgaoCp = "";
	  }
	  public String getCodOrgaoCp() {
		  return this.codOrgaoCp;
	  }

//	-----------------------------------------------------------
//	--------------------------------------------------------------------------
  public void setSobrescreve(boolean sobrescreve)  {	   
	 		  	this.sobrescreve = sobrescreve;
	}
  public boolean getSobrescreve()  { return this.sobrescreve; }

//	--------------------------------------------------------------------------



public void Le_InstrNot(String tpsist, String tpnotif, int pos)  {     
	 try { 
	       DaoTabelas daoTabela = DaoTabelas.getInstance();
 				this.codOrgao=tpsist;		 	
		 	    this.codNotificacao=tpnotif;	
    	    if ( daoTabela.InstrNotLeBean(this, pos) == false)  {
			     this.codOrgao	  = "0";
			     setPkid("0") ;		
            }
     }// fim do try
     catch (Exception e) 	 {
          	this.codOrgao = "0" ;
			setPkid("0") ;		        			
     }
}

//--------------------------------------------------------------------------
public void setParametros(Vector parametros)  {
   	this.parametros = parametros ;
   }

public Vector getParametros(int min,int livre)  {
      try  {
		DaoTabelas daoTabela = DaoTabelas.getInstance();
		daoTabela.InstrNotGetParam(this);
	   	if (this.parametros.size()<min-livre) livre = min-parametros.size() ;		  
   		for (int i=0;i<livre; i++) 	{
   			this.parametros.addElement(new InstrNotBean()) ;
	   	}
      }
      catch (Exception e) {
           setMsgErro("Parametro007 - Leitura: " + e.getMessage());
      }//fim do catch
    return this.parametros ;
   }

public Vector getParametros()  {
      return this.parametros;
   }

public  InstrNotBean getParametros(int i)  { return (InstrNotBean)this.parametros.elementAt(i); }
 

public boolean isValidParametros(Vector vErro) {
	   if ("0".equals(this.codOrgao)) vErro.addElement("Parametro n�o selecionado. \n") ;
	   String sDesc   = ""  ;
	   for (int i = 0 ; i<this.parametros.size(); i++) {
		   	InstrNotBean myParam = (InstrNotBean)this.parametros.elementAt(i) ;
		    sDesc = myParam.getTxtLinhaNot();
		   	if ((sDesc.length()==0) ||("".equals(sDesc)))  continue ;
		   	// verificar duplicata 
		   	for (int m = 0 ; m<this.parametros.size(); m++) {
		   		InstrNotBean myParamOutra = (InstrNotBean)this.parametros.elementAt(m) ;		
		   	   if ((m!=i) && (myParamOutra.getTxtLinhaNot().trim().length()!=0) && (myParamOutra.getTxtLinhaNot().equals(sDesc))) {
		   		 vErro.addElement("Parametro em duplicata  !!!!: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
		   	   }
		   	}
	   }
	   return (vErro.size()==0) ;  
   }

public boolean isInsertParametros()   {
	boolean retorno = true;
	Vector vErro = new Vector() ;
    // Verificada a valida��o dos dados nos campos
       try  {
            DaoTabelas daoTabela = DaoTabelas.getInstance();		   
          if (daoTabela.InstrNotInsert(this,vErro))    {					   
               vErro.addElement("Dados da Instru��o Atualizados");
   	   }
   		   else    {			   		   
   		      vErro.addElement("Dados da Instru��o  N�o Atualizados");     	   
   		   }
       }
       catch (Exception e) {
            vErro.addElement("Dados da Instru��o  N�o Atualizados") ;
            vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
        	   retorno = false;
       }//fim do catch
   
   
    setMsgErro(vErro);
    return retorno;
  }

public InstrNotBean getNotificacoes(String myCodNotificacao) throws sys.BeanException {
	  try { 	 
	  InstrNotBean	his = new InstrNotBean();					
	  	Iterator itx  = getNotificacoes().iterator() ;	
	  	boolean achou = false;
	  	while (itx.hasNext()) {
	  		his =(InstrNotBean)itx.next() ;
  			if (his.getCodNotificacao().equals(myCodNotificacao)) {
  				achou = true;
  				break;
	  		}
  		}
	  	if (achou==false) his = new InstrNotBean();
  		return his;
	}		
	catch (Exception ex) {
		throw new sys.BeanException(ex.getMessage());
	}		
  }
  public void LeNotificacoes()  throws sys.BeanException {
  try { 	 
        DaoTabelas daoTabela = DaoTabelas.getInstance();
     if ( daoTabela.LeNotificacoes(this) == false) {
        }	   		   
  }// fim do try
  catch (Exception ex) {
  	throw new sys.BeanException(ex.getMessage());
  }
  return ;
  }
//--------------------------------------------------------------------------

  //Verifica Se Parametro Existe
  public void CopiaInstrucoes(InstrNotBean myInstr) throws BeanException {
	  Vector vErro = new Vector();
	  try {
		  DaoTabelas daoTabela = DaoTabelas.getInstance();
		  daoTabela.CopiaInstrucoes(myInstr);
	  }
	  catch (Exception e) {
		  throw new sys.BeanException(
				  "Erro ao efetuar verifica��o: " + e.getMessage());
	  }
	  setMsgErro(vErro);
  }
  
  public String getTxtEnderecoRecurso() {
	  return txtEnderecoRecurso;
  }
  
  public void setTxtEnderecoRecurso(String txtEnderecoRecurso) {
	  if (txtEnderecoRecurso==null) txtEnderecoRecurso="";
	  this.txtEnderecoRecurso = txtEnderecoRecurso;
  }
  
}

