package REC;

import java.util.List;
import java.util.Vector;

import sys.Util;


/**
* <b>Title:</b>        	SMIT-REC - Bean de Responsavel Parecer <br>
* <b>Description:</b>  	Bean dados das Juntas - Tabela de Responsavel Parecer<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Luciana Rocha
* @version 				1.0
*/

public class RespJuridicoBean extends sys.HtmlPopupBean { 

	private String codRespParecer;
	private String nomRespParecer;
	private String numCpf;
	private String codOrgao;


	public RespJuridicoBean(){

	  	super();
	  	setTabela("TSMI_RESP_PARECER");	
	  	setPopupWidth(35);
	  	codRespParecer	= "";	  	
		nomRespParecer	= "";
		numCpf	        = "";		
		codOrgao		= "";
				
	}


	public void setCodRespParecer(String codRespParecer)  {
		this.codRespParecer = codRespParecer ;
		if (codRespParecer == null) 
		 this.codRespParecer ="" ;
	}
	public String getCodRespParecer()  {
  		return this.codRespParecer;
  	}
	
	
	public void setNomRespParecer(String nomRespParecer){
		this.nomRespParecer = nomRespParecer;
		if (nomRespParecer == null)
		 this.nomRespParecer = "";
	}
	public String getNomRespParecer()  {
  		return this.nomRespParecer;
  	}
	
	
	public void setNumCpf(String numCpf)  {
		this.numCpf = numCpf ;
		if (numCpf == null)
		 this.numCpf="" ;
	}  
	public String getNumCpf()  {
  		return this.numCpf;
  	}
  	
	public void setNumCpfEdt(String numCpf)  {
		  if (numCpf==null)      numCpf = "";
		  if (numCpf.length()<14) numCpf += "               ";
		  this.numCpf=numCpf.substring(0,3)
			+ numCpf.substring(4,7) 
			+ numCpf.substring(8,11)
			+ numCpf.substring(12,14);
	}  
	public String getNumCpfEdt()  {
	if ("".equals(this.numCpf.trim())) return this.numCpf.trim() ;
	if (this.numCpf.length()<11) this.numCpf=sys.Util.rPad(this.numCpf,"0",11);  
	return this.numCpf.substring(0,3)+ "." + this.numCpf.substring(3,6) + "." + this.numCpf.substring(6,9) + "-" + this.numCpf.substring(9,11) ;	
	}
	
  	public void setCodRespParecerEdt(String codRespParecer)  {
  	  if (codRespParecer==null)      codRespParecer = "";
  	  if (codRespParecer.length()<14) codRespParecer += "               ";
  	  this.codRespParecer=codRespParecer.substring(0,3)
  	    + codRespParecer.substring(4,7) 
  		+ codRespParecer.substring(8,11)
  		+ codRespParecer.substring(12,14);
  	}  
  	public String getCodRespParecerEdt()  {
  	if ("".equals(this.codRespParecer.trim())) return this.codRespParecer.trim() ;
  		return this.codRespParecer=codRespParecer.substring(0,3)+ "." + codRespParecer.substring(3,6) 
  			+ "." + codRespParecer.substring(6,9) + "-" + codRespParecer.substring(9,11) ;
  	}
	
	public void setCodOrgao(String codOrgao)  {
		this.codOrgao = codOrgao ;
		if (codOrgao == null) 
		 this.codOrgao = "" ;
	}  
	public String getCodOrgao()  {
  		return codOrgao;
  	}
	
	public String getRespParecer(){
    	String myRespParecer = "" ; 
     	try    {
        	DaoTabelas daoTabela = DaoTabelas.getInstance();
        	myRespParecer = daoTabela.getRespParecer() ;
			
  		}
   		catch(Exception e){
   		}	
   		return myRespParecer ;
 	}
	
	public List getRespParecer(int min,int livre)  {
		List respParecer = null;
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			respParecer = daoTabela.JuntaGetRespParecer(this);
			if (respParecer.size()<min-livre) 
				livre = min-respParecer.size() ;	  	
			for (int i=0;i<livre; i++) 	{
				respParecer.add(new RespJuridicoBean()) ;
			}
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}
		return respParecer ;
	}
	
	public List getRespParecer(String codOrgao)  {
		List respParecer = null;
	    this.codOrgao = codOrgao;
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			respParecer = daoTabela.JuntaGetRespParecer(this);
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}
		return respParecer ;
	}
	
	public boolean isInsertRelator(List myRespJuridico, List myRespJuridico_aux)   {
		Vector vErro = new Vector() ;
		boolean retorno = true;
		// Verificada a valida��o dos dados nos campos
		try  {
			DaoTabelas daoTabela = REC.DaoTabelas.getInstance();
			retorno = daoTabela.insertRespParecer(vErro,myRespJuridico,myRespJuridico_aux);
		}
		catch (Exception e) {
			vErro.addElement("Erro de altera��o: "+ e.getMessage() +"\n");
		}
		setMsgErro(vErro);
		return retorno;
	}
	
	public List getRespJuridicoBroker()  {
		List respJuridico = null;
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if( (respJuridico = daoTabela.getRespJuridicoBroker(this)) == null )
				setMsgErro("Respons�vel Jur�dico009 - Erro de Leitura Detran: ");
		}
		catch (Exception e) {
			setMsgErro("Respons�vel Jur�dico007 - Leitura: " + e.getMessage());
		}//fim do catch
		return respJuridico;
	}
	
	public boolean compareSmitBroker(List respJuridicoSmit, List respJuridicoBroker){
		RespJuridicoBean myRespJuridico, myRespJuridico_aux;
		boolean flag = true;
		if( (respJuridicoSmit == null) || (respJuridicoBroker == null) )
			return false;
		
		for(int pos=0;pos<respJuridicoSmit.size();pos++,flag=true){
			myRespJuridico = (RespJuridicoBean)respJuridicoSmit.get(pos);
			for(int i=0;i<respJuridicoBroker.size();i++){
				myRespJuridico_aux = (RespJuridicoBean)respJuridicoBroker.get(i);
				if(Util.lPad(myRespJuridico.getNumCpf(),"0",11).equals(Util.lPad(myRespJuridico_aux.getNumCpf(),"0",11))){
					if(pos != i){
						myRespJuridico = (RespJuridicoBean)respJuridicoBroker.set(pos,myRespJuridico_aux);
						respJuridicoBroker.set(i,myRespJuridico);
					}
					flag=false;
					break;
				}
			}
			if(flag){
				if(pos>=respJuridicoBroker.size())
					continue;
				myRespJuridico = (RespJuridicoBean)respJuridicoBroker.set(pos,new RespJuridicoBean());
				respJuridicoBroker.add(myRespJuridico);
			}
		}
		if( respJuridicoSmit.size() > respJuridicoBroker.size() ){
			for(int pos = respJuridicoBroker.size(); pos < respJuridicoSmit.size(); pos++)
				respJuridicoBroker.add(new RespJuridicoBean());
		}
		if( respJuridicoSmit.size() < respJuridicoBroker.size() ){
			for(int pos = respJuridicoSmit.size(); pos < respJuridicoBroker.size(); pos++)
				respJuridicoSmit.add(new RespJuridicoBean());
		}
		if( respJuridicoSmit.size() < 10 ){
			for(int pos=respJuridicoSmit.size();pos < 10;pos++){
				respJuridicoSmit.add(new RespJuridicoBean());
				respJuridicoBroker.add(new RespJuridicoBean());
			}
		}
		else{
			for(int i=0;i<5;i++){
				respJuridicoSmit.add(new RespJuridicoBean());
				respJuridicoBroker.add(new RespJuridicoBean());
			}
		}
		return true;
	}

	  public boolean insertRespJuridicoBroker(List respJuridico, List respJuridico_aux, List respJuridicoBroker, List respJuridicoBroker_aux)   {
	  	boolean retorno = true;
	  	Vector vErro = new Vector() ;
	  	
	  	RespJuridicoBean myRespJuridico, myRespJuridico_aux, myRespJuridicoBroker, myRespJuridicoBroker_aux;
	  	// Verificada a valida��o dos dados nos campos
	  	try  {
	  		DaoTabelas daoTabela = DaoTabelas.getInstance();		   
	  		for(int pos=0; pos < respJuridico.size();pos++){
	  			myRespJuridico       = (RespJuridicoBean)respJuridico.get(pos);
	  			myRespJuridico_aux   = (RespJuridicoBean)respJuridico_aux.get(pos);
	  			myRespJuridicoBroker     = (RespJuridicoBean)respJuridicoBroker.get(pos);
	  			myRespJuridicoBroker_aux = (RespJuridicoBean)respJuridicoBroker_aux.get(pos);	  		
	  			if( (myRespJuridico.getNomRespParecer().equals(myRespJuridico_aux.getNomRespParecer())) && (myRespJuridico.getNumCpf().equals(myRespJuridico_aux.getNumCpf())) && (myRespJuridicoBroker.getNomRespParecer().equals(myRespJuridicoBroker_aux.getNomRespParecer())) && (myRespJuridicoBroker.getNumCpf().equals(myRespJuridicoBroker_aux.getNumCpf())) )
	  				continue;
	  			 
	  			if( (myRespJuridico.getCodRespParecer().equals("")) && (myRespJuridico.getNomRespParecer().equals("")) && ( (myRespJuridicoBroker.getCodRespParecer().equals("")) || (myRespJuridicoBroker.getCodRespParecer().equals(Util.lPad(""," ",11))) ) )
	  				continue;
	  			if( (!myRespJuridico.getCodRespParecer().equals("")) && (!myRespJuridico.getNomRespParecer().equals("")) && (!myRespJuridicoBroker.getCodRespParecer().equals("")) ){
	  				if( daoTabela.RespJuridicoUpdateBroker(myRespJuridicoBroker,myRespJuridicoBroker_aux) )
	  				{
	  					if( !daoTabela.RespJuridicoUpdate(myRespJuridico) ){
	  						vErro.add("Erro: 001 | Altera��o no Smit Relator: "+myRespJuridico.getNomRespParecer()+ "\n");
	  						retorno = false;
	  						daoTabela.RespJuridicoUpdateBroker(myRespJuridicoBroker_aux);
	  					}
	  				}
	  				else{
	  					vErro.add("Erro: 001 | Altera��o no Detran Relator: "+myRespJuridicoBroker.getNomRespParecer()+ "\n");
	  					retorno = false;
	  				}
	  			}
	  			if( (myRespJuridico.getCodRespParecer().equals("")) && (!myRespJuridico.getNomRespParecer().equals("")) && (!myRespJuridicoBroker.getCodRespParecer().equals("")) && (!myRespJuridicoBroker.getCodRespParecer().equals(Util.lPad(""," ",11))) ){
	  				if( daoTabela.RespJuridicoUpdateBroker(myRespJuridicoBroker,myRespJuridicoBroker_aux) )
	  				{
	  					if( !daoTabela.RespJuridicoInsert(myRespJuridico) ){
	  						vErro.add("Erro: 002 | Inclus�o no Smit Relator: "+myRespJuridico.getNomRespParecer()+ "\n");
	  						retorno = false;
	  						daoTabela.RespJuridicoUpdateBroker(myRespJuridicoBroker_aux);
	  					}
	  				}
	  				else{
	  					vErro.add("Erro: 002 | Altera��o no Detran Relator: "+myRespJuridicoBroker.getNomRespParecer()+myRespJuridicoBroker.getCodRespParecer() +"\n");
	  					retorno = false;
	  				}
	  			}
	  			if( (!myRespJuridico.getCodRespParecer().equals("")) && (!myRespJuridico.getNomRespParecer().equals("")) && ((myRespJuridicoBroker.getCodRespParecer().equals("")) || (myRespJuridicoBroker.getCodRespParecer().equals(Util.lPad(""," ",11)))) ){
	  				myRespJuridicoBroker.setCodRespParecer(myRespJuridico.getCodRespParecer());
	  				if( daoTabela.RespJuridicoInsertBroker(myRespJuridicoBroker) )
	  				{
	  					if( !daoTabela.RespJuridicoUpdate(myRespJuridico) ){
	  						vErro.add("Erro: 003 | Altera��o no Smit Relator: "+myRespJuridico.getNomRespParecer()+ "\n");
	  						retorno = false;
	  						daoTabela.RespJuridicoDeleteBroker(myRespJuridicoBroker);
	  					}
	  				}
	  				else{
	  					vErro.add("Erro: 003 | Inclus�o no Detran Relator: "+myRespJuridicoBroker.getNomRespParecer()+ "\n");
	  					retorno = false;
	  				}
	  			}
	  			if( (myRespJuridico.getCodRespParecer().equals("")) && (!myRespJuridico.getNomRespParecer().equals("")) && ((myRespJuridicoBroker.getCodRespParecer().equals("")) || (myRespJuridicoBroker.getCodRespParecer().equals(Util.lPad(""," ",11)))) ){
	  				if( daoTabela.RespJuridicoInsert(myRespJuridico) )
	  				{
	  					myRespJuridicoBroker.setCodRespParecer(myRespJuridico.getCodRespParecer());
	  					if( !daoTabela.RespJuridicoInsertBroker(myRespJuridicoBroker) ){
	  						vErro.add("Erro: 004 | Inclus�o no Detran Relator: "+myRespJuridicoBroker.getNomRespParecer()+ "\n");
	  						retorno = false;
	  						daoTabela.RespJuridicoDelete(myRespJuridico);
	  					}
	  				}
	  				else{
	  					vErro.add("Erro: 004 | Inclus�o no Smit Relator: "+myRespJuridicoBroker.getNomRespParecer()+ "\n");
	  					retorno = false;
	  				}
	  			}
	  			if( (myRespJuridico.getNomRespParecer().equals("")) && (myRespJuridicoBroker.getNomRespParecer().equals(""))){
	  				boolean sucesso = true;
	  				if( (!myRespJuridicoBroker.getCodRespParecer().equals("")) || (!myRespJuridicoBroker.getCodRespParecer().equals(Util.lPad(""," ",11))) ){
	  					if( !daoTabela.RespJuridicoDeleteBroker(myRespJuridicoBroker_aux) ){
	  						vErro.add("Erro: 005 | Exclus�o no Detran Relator: "+myRespJuridicoBroker_aux.getNomRespParecer()+ "\n");
	  						sucesso = false;
	  						retorno = false;
	  					}
	  				}
	  				if(sucesso){
	  					if( !myRespJuridico.getCodRespParecer().equals("") ){
	  						if( !daoTabela.RespJuridicoDelete(myRespJuridico) ){
	  							vErro.add("Erro: 005 | Exclus�o no Smit Relator: "+myRespJuridico_aux.getNomRespParecer()+ "\n");
	  							if( !myRespJuridicoBroker.getCodRespParecer().equals(""))
	  								daoTabela.RespJuridicoInsertBroker(myRespJuridicoBroker_aux);
	  							sucesso = false;
	  							retorno = false;
	  						}
	  					}
	  				}
	  				if(sucesso){
	  					respJuridico.remove(pos);
	  					respJuridico_aux.remove(pos);
	  					respJuridicoBroker.remove(pos);
	  					respJuridicoBroker_aux.remove(pos);
	  					pos--;
	  				}
	  			}
	  		}
	  	}
	  	catch (Exception e) {
	  		vErro.addElement("Erro na altera��o: "+ e.getMessage() +"\n");
	  		retorno = false;
	  	}//fim do catch
	  	setMsgErro(vErro);
	  	
	  	if(respJuridico.size()<10){
	  		for(int i=respJuridico.size(); i<10;i++){
	  			respJuridico.add(new RespJuridicoBean());
	  			respJuridicoBroker.add(new RespJuridicoBean());
	  		}
	  	}
	  	
	  	return retorno;
	   }

	
}

