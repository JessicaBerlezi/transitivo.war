package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class GuiaEnviaDOCmd extends sys.Command {
	private String next;
	private static final String jspPadrao="/REC/GuiaEnviarDO.jsp" ;  
	
	public GuiaEnviaDOCmd() {
		next = jspPadrao;
	}
	
	public GuiaEnviaDOCmd(String next) {
		this.next = next;
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next ;
		try { 
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			
			if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
			ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			
			if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
			GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
			
			if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
			//Carrego os Beans		
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");
			String acao    = req.getParameter("acao");   
			
			if (acao==null)	{
				GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
				GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
				GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
				GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
				GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
				GuiaDistribuicaoId.setMsgErro("");	
				GuiaDistribuicaoId.setCodStatus("3");
				/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
				GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
				req.setAttribute("guias",GuiaDistribuicaoId.consultaGuia(UsrLogado));
				acao = " ";
			}		  	 
			
			if (acao.equals("LeGuia")) {					
				
				int dias = Integer.parseInt(ParamOrgaoId.getParamOrgao("DIAS_PARA_INFORMAR_RESULTADO","120","10"));
				GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("codEnvioDO"));
				GuiaDistribuicaoId.setDatEnvioDO(req.getParameter("datEnvioDO"));	
				GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
				String[] guias = req.getParameterValues("guia");
				List lGuias = new ArrayList();
				GuiaDistribuicaoBean auxGuia = null;
				
				for(int i=0;i<guias.length;i++){
					auxGuia = new GuiaDistribuicaoBean();
					auxGuia.setNumGuia(guias[i]);
					auxGuia.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
					auxGuia.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					auxGuia.setNomUserName(UsrLogado.getNomUserName());
					auxGuia.LeGuia();
					auxGuia.setCodEnvioDO(GuiaDistribuicaoId.getCodEnvioDO());
					auxGuia.setDatEnvioDO(GuiaDistribuicaoId.getDatEnvioDO());	
					
					lGuias.add(auxGuia);
				}
				String numGuias[] = GuiaDistribuicaoId.AtaGuias();
				Vector vErro = new Vector();
				
				if(numGuias[0].equals("-1"))
					vErro.addElement("Ata possui Guia sem status de Enviada para Publica��o.");
                
                if(numGuias[0].equals("-2"))
                    vErro.addElement("N� Ata j� existe. Informe um outro n�mero.");
                    
				if (GuiaDistribuicaoId.getCodEnvioDO().length()==0) 
					vErro.addElement("N�mero da ATA n�o preenchido. \n") ;
				
				if (GuiaDistribuicaoId.getDatEnvioDO().length()==0) 
					vErro.addElement("Data de Envio n�o preenchida. \n") ;						
				String dtbase = sys.Util.addData(sys.Util.formatedToday().substring(0,10),dias) ;
				
				if (sys.Util.DifereDias(dtbase,GuiaDistribuicaoId.getDatEnvioDO())>0) 
					vErro.addElement("Data de Envio n�o pode ser superior a "+dtbase+". \n") ;			
				GuiaDistribuicaoId.setMsgErro(vErro);
				
				if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
				
				if (GuiaDistribuicaoId.getMsgErro().length()==0){
					req.setAttribute("numGuias",numGuias[0]);
					req.setAttribute("numReqs",numGuias[1]);
					GuiaDistribuicaoId.setAutos(lGuias);
					nextRetorno = "/REC/GuiaEnviarDOConfirma.jsp";
				}
				else
					req.setAttribute("guias",GuiaDistribuicaoId.consultaGuia(UsrLogado)); 
			}	
			
			if (acao.equals("EnviarDO")) {  
				if(EnviaDO(GuiaDistribuicaoId))   	req.setAttribute("flag","S");
				else   	req.setAttribute("flag","N");
				req.setAttribute("numGuias",req.getParameter("numGuias"));
				req.setAttribute("numReqs",req.getParameter("numReqs"));
				nextRetorno = "/REC/GuiaEnviarDOConfirma.jsp";				
			}
			if(acao.equals("Imprime")){
				nextRetorno = "/REC/GuiaPublicaAtaImp.jsp";	
			}
			
			// processamento de saida dos formularios
			if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
			else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEnviaDOCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	public boolean EnviaDO(GuiaDistribuicaoBean GuiaDistribuicaoId) throws sys.CommandException {
		GuiaDistribuicaoId.setMsgErro("");
		try {	
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			if(dao.GuiaEnviaDO(GuiaDistribuicaoId) == 0) {
				GuiaDistribuicaoId.setMsgErro("Nenhuma Guia enviada para Publica��o. \n"+
						GuiaDistribuicaoId.getMsgErro()) ;
				return false;
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("GuiaEnviaDOCmd: " + ue.getMessage());
		}
		return true;
	} 	
	
	
}
