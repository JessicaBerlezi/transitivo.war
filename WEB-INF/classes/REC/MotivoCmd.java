package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Orgao<br>
* <b>Description:</b>  Comando para Manter os Parametros por Orgao<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class MotivoCmd extends sys.Command {

	private static final String jspPadrao = "/REC/Motivo.jsp";
	private String next;

	public MotivoCmd() {
		next = jspPadrao;
	}

	public MotivoCmd(String next) {
		this.next = jspPadrao;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans do Usuario, se n�o existir
			ACSS.OrgaoBean OrgId = (ACSS.OrgaoBean) req.getAttribute("OrgId");
			if (OrgId == null)OrgId = new ACSS.OrgaoBean();
			
			MotivoBean MotivoId = (MotivoBean) req.getAttribute("MotivoId");
			if (MotivoId == null)MotivoId = new MotivoBean();
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null)acao = " ";

			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";

			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null) atualizarDependente = "N";

			OrgId.Le_Orgao(codOrgao, 0);
			OrgId.setAtualizarDependente(atualizarDependente);
			MotivoId.getMotivo(20,3,OrgId);

			if ("buscaMotivo".indexOf(acao) >= 0) {
				MotivoId.getMotivo(20,3,OrgId);
				OrgId.setAtualizarDependente("S");
			}

			if (acao.compareTo("Copiar") == 0) {
				String codOrgaoCp = req.getParameter("codOrgaoCp");
				
				MotivoId.setCodOrgao(codOrgao);
				MotivoId.setCodOrgaoCp(codOrgaoCp);
				MotivoId.CopiaMotivoOrgao(OrgId);
			}
   
			Vector vErro = new Vector();

			if (acao.compareTo("A") == 0) {
				String[] dscMotivo = req.getParameterValues("dscMotivo");
				if (dscMotivo == null)
				    dscMotivo = new String[0];
			
				String[] codMotivo = req.getParameterValues("codMotivo");
				if (codMotivo == null)
				    codMotivo = new String[0];				

				vErro =	isMotivos(dscMotivo,codMotivo);
				if (vErro.size() == 0) {
					Vector motivo = new Vector();
					for (int i = 0; i < dscMotivo.length; i++) {
						MotivoBean myMotivo = new MotivoBean();
						myMotivo.setDscMotivo(dscMotivo[i]);
						myMotivo.setCodMotivo(codMotivo[i]);
						myMotivo.setCodOrgao(codOrgao);
						
						motivo.addElement(myMotivo);
					}
					MotivoId.setMotivo(motivo);
					MotivoId.isInsertMotivo(OrgId);
					MotivoId.getMotivo(20, 3, OrgId);
				}
				else
				MotivoId.setMsgErro(vErro);
			}
			req.setAttribute("OrgId", OrgId);
			req.setAttribute("MotivoId", MotivoId);
		}
		catch (Exception ue) {
			throw new sys.CommandException(
				"MotivoOrgaoCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private Vector isMotivos(String[] dscMotivo, String[] codMotivo) {
		Vector vErro = new Vector();
				
		if ((codMotivo.length != dscMotivo.length))
			vErro.addElement("Motivos inconsistentes");
		return vErro;
	}
}