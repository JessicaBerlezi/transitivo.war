package REC;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class FormNotifCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/FormNotif.jsp" ;  
   
  public FormNotifCmd() {
    next             =  jspPadrao;
  }

  public FormNotifCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try { 
	   
	    // cria os Beans, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado     = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
	  	ParamOrgBean ParamOrgaoId     = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	  	if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

		InstrNotBean NotifBeanId = (InstrNotBean) req.getAttribute("NotifBeanId");
		if (NotifBeanId==null)  NotifBeanId = new InstrNotBean() ;	  			  
	  
//		cria os Beans, se n�o existir
		 AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
		 if (AutoInfracaoBeanId==null)  	{
		 	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
		 	if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
		 	if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
		 	if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
		 	if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
		 	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
		 }
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
		if  (acao.equals("FormNotifBranco"))  {	
			NotifBeanId.setCodNotificacao(req.getParameter("codnotif"));	  	
			NotifBeanId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
			NotifBeanId.LeNotificacoes();
			nextRetorno = "/REC/Notificacao.jsp" ;			   
		}
  		// processamento de saida dos formularios  	
		req.setAttribute("NotifBeanId",NotifBeanId) ;
		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
    }
    catch (Exception ue) {
	      throw new sys.CommandException("FormNotifCmd: " + ue.getMessage());
    }
	return nextRetorno;
}

}
