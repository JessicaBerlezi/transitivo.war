package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.DaoException;
import ACSS.UsuarioBean;



/** 
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Sistema<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello
* @version 1.0
* @Updates
*/

public class NivelUsuarioRelatorBean extends sys.HtmlPopupBean {

	private String codRelator;
	private String nomRelator;
	private String codNivelUsuario;
	private String codUsuario;
	private String nomUsuario;
	private String nomNivelUsuario;
	private String valNivelProcesso;
	private String tipoMsg;
	private List nomNivelProcessoSist;
	private List valNivelProcessoSist;
	private ArrayList listNivelDestino;
	private Vector nivelUsuario;
	private Vector nivelUsuarioRelator;


	public NivelUsuarioRelatorBean() {
		super();
		setTabela("TSMI_CONTROLE_RELATOR");
		setPopupWidth(35);
		codRelator = "";
		nomRelator = "";
		codNivelUsuario = "";
		codUsuario = "";
		nomUsuario = "";
		nomNivelUsuario = "";
		valNivelProcesso = "";
		tipoMsg = "";
		nomNivelProcessoSist = new ArrayList();
		valNivelProcessoSist = new ArrayList();
		listNivelDestino  = new ArrayList();
		nivelUsuario = new Vector();
		nivelUsuarioRelator = new Vector();
	}

	public void setCodRelator(String codRelator) {
		this.codRelator = codRelator;
		if (codRelator == null)
			this.codRelator = "";
	}
	public String getCodRelator() {
		return this.codRelator;
	}

	public void setCodNivelUsuario(String codNivelUsuario) {
		this.codNivelUsuario = codNivelUsuario;
		if (codNivelUsuario == null)
			this.codNivelUsuario = "";
	}
	public String getCodNivelUsuario() 
	{
		return this.codNivelUsuario;
	}

	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
		if (codUsuario == null)
			this.codUsuario = "";
	}
	public String getCodUsuario() 
	{
		return this.codUsuario;
	}
	
	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
		if (nomUsuario == null)
			this.nomUsuario = "";
	}
	public String getNomUsuario() 
	{
		return this.nomUsuario;
	}
	
	public void setNomNivelUsuario(String nomNivelUsuario) {
		this.nomNivelUsuario = nomNivelUsuario;
		if (nomNivelUsuario == null)
			this.nomNivelUsuario = "";
	}
	public String getNomNivelUsuario() {
		return this.nomNivelUsuario;
	}

	public void setValNivelProcesso(String valNivelProcesso) {
		this.valNivelProcesso = valNivelProcesso;
		if (valNivelProcesso == null)
			this.valNivelProcesso = "";
	}
	public String getValNivelProcesso() {
		return this.valNivelProcesso;
	}

	public void setTipoMsg(String tipoMsg) {
		if(tipoMsg == null) tipoMsg = "";
		else this.tipoMsg = tipoMsg;
	}
	public String getTipoMsg() {
		return tipoMsg;
	}
	
	public void setNomRelator(String nomRelator) {
		if(nomRelator == null) nomRelator = "";
		else this.nomRelator = nomRelator;
	}
	public String getNomRelator() {
		return nomRelator;
	}



	//--------------------------------------------------------------------------

	public void setNomNivelProcessoSist(List nomNivelProcessoSist) {
		this.nomNivelProcessoSist = nomNivelProcessoSist;
	}

	public List getNomNivelProcessoSist() {
		return this.nomNivelProcessoSist;
	}
	public String getNivelProcessoSist(String myNomNivelProcesso) throws sys.BeanException {
		try {
			String myVal = "";
			String re = "";
			Iterator itx = getNomNivelProcessoSist().iterator();
			int i = 0;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomNivelProcesso)) {
					myVal = getValNivelProcessoSist(i);
					break;
				}
				i++;
			}

			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
	//--------------------------------------------------------------------------
	public void setValNivelProcessoSist(List valNivelProcessoSist) {
		this.valNivelProcessoSist = valNivelProcessoSist;
	}

	public List getValNivelProcessoSist() {
		return this.valNivelProcessoSist;
	}

	
	public void setNivelUsuario(Vector nivelUsuario) {
		if(nivelUsuario == null) nivelUsuario = new Vector();
		else this.nivelUsuario = nivelUsuario;
	}
	public Vector getNivelUsuario() {
		return this.nivelUsuario;
	}
	
	
	
	public String getValNivelProcessoSist(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValNivelProcessoSist().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
	
	
	public void setNivelUsuarioRelator(Vector nivelUsuarioRelator) {
		this.nivelUsuarioRelator = nivelUsuarioRelator;
	}
	
	
	public Vector getNivelUsuarioRelator(int min, int livre,JuntaBean myJunta,RelatorBean myRelator) {
		try {
			DaoTabelas dao = DaoTabelas.getInstance();
			dao.GetNivelUsuarioRelator(this,myJunta,myRelator);
			if (this.nivelUsuarioRelator.size() < min - livre)
				livre = min - nivelUsuarioRelator.size();
			for (int i = 0; i < livre; i++) {
				this.nivelUsuarioRelator.addElement(new NivelUsuarioRelatorBean());
			}
		}
		catch (Exception e) {
			setMsgErro("ParametroNivelProcesso007 - Leitura: " + e.getMessage());
		} 
		return this.nivelUsuarioRelator;
	}
	
	
//---------------------------------------------------------------------------------------
	public String getNiveis(String codNivelUsuario) {
		String myNivel = "";
		try {
			DaoTabelas dao = DaoTabelas.getInstance();
			myNivel = dao.getNivelUsuario(codNivelUsuario);
		}
		catch (Exception e) {
		}
		return myNivel;
	}
	
//---------------------------------------------------------------------------------------
	public void Le_Niveis(String tpNivel, int pos,UsuarioBean myUsrlogado) {
		try {
			DaoTabelas dao = DaoTabelas.getInstance();
			if (pos == 0)
				this.codUsuario = tpNivel;
			else
				this.nomNivelUsuario = tpNivel;
			if (dao.NivelLeBean(this, pos) == false) {
				this.codUsuario = "0";
				this.nomNivelUsuario = "";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codUsuario = "0";
			setPkid("0");
			System.err.println("Sistema001: " + e.getMessage());
		}
	}

	public void setListNivelDestino(ArrayList listNivelDestino) {
		if(listNivelDestino == null) listNivelDestino = new ArrayList();
		else this.listNivelDestino = listNivelDestino;
	}
	public ArrayList getListNivelDestino() {
		return this.listNivelDestino;
	}
	public void Le_NivelUsuario(UsuarioBean myUsrLogado) throws DaoException {
		DaoTabelas dao;
		try {
			dao = DaoTabelas.getInstance();
			dao.Le_NivelUsuario(this,myUsrLogado);
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
	}
	public boolean isInsertNivelUsuario() throws DaoException {
		boolean retorno = true;
		Vector vErro = new Vector();
		// Verificada a valida��o dos dados nos campos
		DaoTabelas dao;
		try {
			dao = DaoTabelas.getInstance();
		} catch (DaoException e) {
			throw new DaoException(e.getMessage());
		}
		
		if (dao.gravaNivelUsuarioRelator(this, vErro)) {
			vErro.addElement("Usuarios selecionados para este n�vel atualizados com sucesso. \n");
		}
		setMsgErro(vErro);
		return retorno;
	}



}