package REC;

import java.util.Iterator;

/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Executar TRI Bean<br>
* <b>Description:</b>  Executar TRI Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ExecutarTRIBean  extends EventoBean { 
	private String txtMotivo;	
	
  public ExecutarTRIBean()  throws sys.BeanException {
  	
  super() ;
  txtMotivo  = "";
  }
  
  
//--------------------------------------------------------------------------  
  public void setTxtMotivo(String txtMotivo)   {
   if (txtMotivo==null) txtMotivo = "";
   this.txtMotivo = txtMotivo ;	
  }
  public String getTxtMotivo()   {
   return txtMotivo ;	
  } 
  
//--------------------------------------------------------------------------  
  public void setJ_sigFuncao(String j_sigFuncao)   {
		if (j_sigFuncao==null) j_sigFuncao = "" ;
		setCFuncao(j_sigFuncao) ;
		// Troca de Real Infrator
		setStatusValido("0,1,2,3,4,5") ;
		setStatusTPCVValido("0,1,2,3,4,5") ;		
		setNFuncao("Executar Troca de Real Infrator") ;
		setTipoReqValidos("TR") ;
		setStatusReqValido("0") ;	
		setOrigemEvento("100") ;
		setCodEvento("214");
		// Estorno de Troca de Real Infrator
		if ("REC0215".equals(j_sigFuncao)) {
		    setStatusValido("4,5") ;
	    	setStatusTPCVValido("0,1,2,3,4,5") ;		
	    	setNFuncao("Estorno de Troca de Real Infrator") ;
    		setTipoReqValidos("TR") ;	
		    setStatusReqValido("0,9") ;	
    		setOrigemEvento("100") ;
		    setCodEvento("352") ;			
		}
	}

	public void setEventoOK(REC.AutoInfracaoBean myAuto) throws sys.BeanException   {
	   setMsg("");
	   setEventoOK("N");	
	   if ("".equals(myAuto.getNomStatus())) return ;
	   setMsg(myAuto.getNomStatus()+" ("+myAuto.getDatStatus()+")") ;
	   if ( ("10,11,12,14,15,16,82".indexOf(myAuto.getCodStatus())>=0)  &&
	        (myAuto.getCodStatus().length()>1) )   {
	   		setEvento1a(myAuto);
			return;
	   }
	   
	  if (myAuto.getOrgao().getIntegrada().equals("N"))	  {
	      setEventoOK("N");
	      setMsg(getMsg()+ " \n Org�o isolado. N�o pode entrar com TRI.");
	      return;
	  }

	   try {
	   	  if ( (getStatusValido().indexOf(myAuto.getCodStatus())>=0) ||
	   	     (("".equals(getStatusTPCVValido())==false) && ("1".equals(myAuto.getIndTPCV())) && (getStatusTPCVValido().indexOf(myAuto.getCodStatus())>=0)) ) 
	   		{
				if (veInfracao(myAuto)) {
		      		Iterator it = myAuto.getRequerimentos().iterator() ;
	    	  		REC.RequerimentoBean req  = new REC.RequerimentoBean();
	      			boolean existe = false ;
    	  			while (it.hasNext()) {
   						req =(REC.RequerimentoBean)it.next() ;
	      				if (("DC,"+getTipoReqValidos()).indexOf(req.getCodTipoSolic())>=0 && !"7".equals(req.getCodStatusRequerimento()) && !"8".equals(req.getCodStatusRequerimento())) {
	      					existe = true ;
    	  					break ;
	    	  			}
	      			}
	    	  		if (existe) setMsg(getMsg() + " - J� existe requerimento de TRI ou DC para "+getNFuncao()+".");					   	
	      			else 		setEventoOK("S");
		   		}
		      }
	      else {		  
	   		setMsg(getMsg() + " - Status do Auto n�o permite "+getNFuncao());				
	      }
	   }
	   catch (Exception ue) {
	     throw new sys.BeanException("ExecutarTRIBean: " + ue.getMessage());
	   }
	  return ;	
	} 

	public void setEvento1a(REC.AutoInfracaoBean myAuto) throws sys.BeanException   {
	   try {
			if (sys.Util.DifereDias("16/07/2004",myAuto.getDatInfracao())>0) {
				setMsg(getMsg() + " - TRI em 1a. Inst�ncia somente para autos at� 15/07/2004. ");				
			}
			else {
				if (veInfracao(myAuto)) {
		      		Iterator it = myAuto.getRequerimentos().iterator() ;
	    	  		REC.RequerimentoBean req  = new REC.RequerimentoBean();
	      			boolean existe = false ;
    	  			while (it.hasNext()) {
   						req =(REC.RequerimentoBean)it.next() ;
	      				if ("1C,TR".indexOf(req.getCodTipoSolic())>=0) {
	      					existe = true ;
    	  					break ;
	    	  			}
	      			}
	    	  		if (existe) setMsg(getMsg() + " - J� existe requerimento de TRI ou 1C.");					   	
	      			else 		setEventoOK("S");
		   		}
	        }
	   }
	   catch (Exception ue) {
	     throw new sys.BeanException("ExecutarTRIBean: " + ue.getMessage());
	   }
	  return ;	
	} 



public boolean veInfracao(REC.AutoInfracaoBean myAuto) throws sys.BeanException   {
	boolean ok = false ;
	// N�o pode ser Condutor identificado nem prop diferente de condutor
	if ( "1".equals(myAuto.getIndCondutorIdentificado())==false ) {
		// INfracao de Proprietario
		if ("P".equals(myAuto.getInfracao().getIndTipo())) {
			// Pessoa Juridica
			if ("2".equals(myAuto.getProprietario().getIndCpfCnpj())) ok=true;
			// Pessoa Fisica	
			else setMsg(getMsg()+ " Infra��o de Proprietario pessoa Fisica - n�o pode ter TRI") ;						
		}
		// Infracao de Condutor
		else {
			// Proprietario  igual ao Condutor
			if (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())) 	ok=true;
			// Proprietario diferente do Condutor
//			else setMsg(getMsg()+ " Infra��o de Condutor com Proprietario diferente do Condutor - n�o pode ter TRI") ;
			else	ok=true;
		}
	}
	// Condutor Identificado
	else { 
		setMsg(getMsg()+" Condutor Identificado n�o pode ter TRI.");
	}
	return ok ;
 }
//--------------------------------------------------------------------------  

	public void buscaCondutor(RequerimentoBean myReq) throws sys.BeanException   {
    	try { 	 
			   ResponsavelBean myResp = new ResponsavelBean();
			   myResp.setNumCnh(myReq.getNumCNHTR());
			   myResp.setIndTipoCnh(myReq.getIndTipoCNHTR());
			   myResp.setCodUfCnh(myReq.getCodUfCNHTR());

			   DaoBroker dao = DaoBrokerFactory.getInstance();	 	       
    		   if (dao.LeResponsavel(myResp,"cnh" )) { 
				   if ("".equals(myResp.getCodResponsavel())) myReq.setMsgErro("Responsavel n�o localizado com CNH: "+("1".equals(myReq.getIndTipoCNHTR()) ? "Permission�rio-" : "")+myReq.getNumCNHTR()+"/"+myReq.getCodUfCNHTR()) ;
				   myReq.setCodResponsavelDP(myResp.getCodResponsavel());
    			   myReq.setNomCondutorTR(myResp.getNomResponsavel());
    			   myReq.setNumCPFTR(myResp.getNumCpfCnpj());
				   // garantindo os tamanhos
				   if (myResp.getEndereco().getTxtEndereco().length()<27)    myResp.getEndereco().setTxtEndereco(sys.Util.rPad(myResp.getEndereco().getTxtEndereco()," ",27)) ;
				   if (myResp.getEndereco().getNumEndereco().length()<5)     myResp.getEndereco().setNumEndereco(sys.Util.rPad(myResp.getEndereco().getNumEndereco()," ",5)) ;			   
				   if (myResp.getEndereco().getTxtComplemento().length()<11) myResp.getEndereco().setTxtComplemento(sys.Util.rPad(myResp.getEndereco().getTxtComplemento()," ",11)) ;
				   myResp.getEndereco().setTxtEndereco(myResp.getEndereco().getTxtEndereco().substring(0,27)) ;
				   myResp.getEndereco().setNumEndereco(myResp.getEndereco().getNumEndereco().substring(0,5)) ;
				   myResp.getEndereco().setTxtComplemento(myResp.getEndereco().getTxtComplemento().substring(0,11)) ;			   
    			   myReq.setTxtEnderecoTR(myResp.getEndereco().getTxtEndereco()+myResp.getEndereco().getNumEndereco()+myResp.getEndereco().getTxtComplemento());
	    		   myReq.setNomBairroTR(myResp.getEndereco().getNomBairro());
	    		   myReq.setTxtCategoriaTR(myResp.getCategoriaCnh());
	    		   myReq.setDatValidTR(myResp.getDatValidade());
    			   myReq.setNomCidadeTR(myResp.getEndereco().getNomCidade());
    			   myReq.setCodCidadeTR(myResp.getEndereco().getCodCidade());
    			   myReq.setCodUfTR(myResp.getEndereco().getCodUF());
	    		   myReq.setNumCEPTR(myResp.getEndereco().getNumCEP());
    		   }
			   else myReq.setMsgErro(myResp.getMsgErro());
     	}// fim do try
    	 catch (Exception ex) {
     		throw new sys.BeanException(ex.getMessage());
     	}
	  return ;	
	} 
	
	public String getNumProcesso (String nAuto){
		try{
			 Dao dao = Dao.getInstance();
			 return dao.LeNumProcessoAuto(nAuto);
		}catch (Exception e) {
		}
		return "numProcesso";
	}
	
}
