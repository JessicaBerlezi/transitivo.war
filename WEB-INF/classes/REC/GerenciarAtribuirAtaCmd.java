package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class GerenciarAtribuirAtaCmd extends  sys.Command{
	private String next;
	private static final String jspPadrao="/REC/AtribuirAtaProcesso.jsp" ;


	public GerenciarAtribuirAtaCmd() {
		next = jspPadrao;
	}
	public GerenciarAtribuirAtaCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado          = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado     = new ACSS.UsuarioBean() ;
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
			ParamOrgBean ParamOrgaoId           = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			ParamSistemaBean parSis             = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis          = new ParamSistemaBean();
			AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			// cria os Beans, se n�o existir
			AbrirRecursoDPBean AbrirRecursoBeanId     = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId") ;

			if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecursoDPBean() ;	  			

			if (AutoInfracaoBeanId==null)  	{
				AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
				if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
			}
			// obtem e valida os parametros recebidos					
			String acao           = req.getParameter("acao");  

			if(acao==null)   acao = " ";
		
		}
		catch (Exception ue) {
			throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
}
