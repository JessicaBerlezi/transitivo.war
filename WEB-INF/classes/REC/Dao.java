package REC;

import java.awt.Cursor;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import sys.BeanException;
import sys.DaoException;
import sys.ServiceLocatorException;

/**
 * <b>Title:</b> SMIT - MODULO RECURSO<br>
 * <b>Description:</b> Dao - Acesso a Base de Dados<br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b> DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class Dao extends sys.DaoBase {

	private sys.ServiceLocator serviceloc;
	private static final String MYABREVSIST = "REC";
	private static Dao instance;

	public static Dao getInstance() throws sys.DaoException {
		if (instance == null)
			instance = new Dao();
		return instance;
	}

	// **************************************************************************************
	private Dao() throws sys.DaoException {
		try {
			serviceloc = sys.ServiceLocator.getInstance();
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- DAO relativos
	 * a InfracaoBean
	 * -----------------------------------------------------------
	 */

	public void updateRequerimento(RequerimentoBean myReq, Connection conn) throws sys.DaoException {
		try {
			String sCmd = "";
			String codRelator = "";
			Statement stmt = conn.createStatement();
			sCmd = "SELECT cod_relator FROM TSMI_RELATOR WHERE num_cpf = '" + myReq.getCodRelatorJU() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);

			if (rs.next())
				codRelator = rs.getString("cod_relator");

			sCmd = "UPDATE TSMI_REQUERIMENTO set " + " Cod_Tipo_Solic='" + myReq.getCodTipoSolic() + "',"
					+ " Dat_Requerimento=to_date('" + myReq.getDatRequerimento() + "','dd/mm/yyyy'),"
					+ " Cod_Status_Requerimento='" + myReq.getCodStatusRequerimento() + "'," +

			" Nom_Username_DP='" + myReq.getNomUserNameDP() + "'," + " Cod_Orgao_Lotacao_DP='"
					+ myReq.getCodOrgaoLotacaoDP() + "'," + " Cod_Responsavel_DP='" + myReq.getCodResponsavelDP() + "',"
					+ " Dat_Proc_DP=to_date('" + myReq.getDatProcDP() + "','dd/mm/yyyy')," + " dat_PJ=to_date('"
					+ myReq.getDatPJ() + "','dd/mm/yyyy')," + " cod_Parecer_PJ='" + myReq.getCodParecerPJ() + "',"
					+ " txt_Motivo_PJ='" + myReq.getTxtMotivoPJ() + "'," +
					/*
					 * " cod_Resp_PJ='"+myReq.getCodRespPJ()+"',"+
					 */
					" nom_UserName_PJ='" + myReq.getNomUserNamePJ() + "'," + " cod_Orgao_Lotacao_PJ='"
					+ myReq.getCodOrgaoLotacaoPJ() + "'," + " dat_Proc_PJ=to_date('" + myReq.getDatProcPJ()
					+ "','dd/mm/yyyy'), " + " dat_JU=to_date('" + myReq.getDatJU() + "','dd/mm/yyyy')," +
					/*
					 * " cod_Junta_JU='"+myReq.getCodJuntaJU()+"',"+
					 */
					" cod_Relator_JU='" + codRelator + "'," + " nom_UserName_JU='" + myReq.getNomUserNameJU() + "',"
					+ " cod_Orgao_Lotacao_JU='" + myReq.getCodOrgaoLotacaoJU() + "'," + " dat_Proc_JU=to_date('"
					+ myReq.getDatProcJU() + "','dd/mm/yyyy')," + " dat_RS=to_date('" + myReq.getDatRS()
					+ "','dd/mm/yyyy')," + " cod_Result_RS='" + myReq.getCodResultRS() + "'," + " txt_Motivo_RS='"
					+ myReq.getTxtMotivoRS() + "'," + " nom_UserName_RS='" + myReq.getNomUserNameRS() + "',"
					+ " cod_Orgao_Lotacao_RS='" + myReq.getCodOrgaoLotacaoRS() + "'," + " dat_Proc_RS=to_date('"
					+ myReq.getDatProcRS() + "','dd/mm/yyyy')," + " dat_Atu_TR=to_date('" + myReq.getDatAtuTR()
					+ "','dd/mm/yyyy')," + " nom_Condutor_TR ='" + myReq.getNomCondutorTR() + "'," + " num_CNH_TR='"
					+ myReq.getNumCNHTR() + "'," + " ind_tipo_CNH_TR='" + myReq.getIndTipoCNHTR() + "',"
					+ " cod_Uf_CNH_TR='" + myReq.getCodUfCNHTR() + "'," + " num_CPF_TR='" + myReq.getNumCPFTR() + "',"
					+ " txt_Endereco_TR ='" + myReq.getTxtEnderecoTR() + "'," + " num_CEP_TR='" + myReq.getNumCEPTR()
					+ "'," + " nom_Bairro_TR ='" + myReq.getNomBairroTR() + "'," + " nom_Cidade_TR='"
					+ myReq.getNomCidadeTR() + "'," + " cod_Uf_TR='" + myReq.getCodUfTR() + "'," + " nom_UserName_TR='"
					+ myReq.getNomUserNameTR() + "'," + " cod_Orgao_Lotacao_TR="
					+ (myReq.getCodOrgaoLotacaoTR().equals("0") ? "null" : "'" + myReq.getCodOrgaoLotacaoTR() + "'")
					+ "," + " dat_Proc_TR=to_date('" + myReq.getDatProcTR() + "','dd/mm/yyyy'), " + " dat_EST=to_date('"
					+ myReq.getDatEST() + "','dd/mm/yyyy')," + " txt_Motivo_EST='" + myReq.getTxtMotivoEST() + "',"
					+ " nom_UserName_EST='" + myReq.getNomUserNameEST() + "'," + " cod_Orgao_Lotacao_EST='"
					+ myReq.getCodOrgaoLotacaoEST() + "'," + " dat_Proc_EST=to_date('" + myReq.getDatProcEST()
					+ "','dd/mm/yyyy') " + " where Cod_Auto_Infracao = '" + myReq.getCodAutoInfracao() + "' and"
					+ " Num_Requerimento='" + myReq.getNumRequerimento() + "'";
			stmt.execute(sCmd);
			stmt.close();
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Recebe
	 * Notificacao e grava Historico - transacao 204
	 * -----------------------------------------------------------
	 */
	public void atualizarAuto204(AutoInfracaoBean myAuto, HistoricoBean myHis) throws sys.DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			// atualizar NUMERO DE PROCESSO DO Auto de Infracao, se ja nao
			// houver
			String sCmd = "UPDATE TSMI_AUTO_INFRACAO set COD_STATUS='" + myAuto.getCodStatus()
					+ "',DAT_STATUS=to_date('" + myAuto.getDatStatus() + "','dd/mm/yyyy'), " + " NUM_PROCESSO='"
					+ myAuto.getNumProcesso() + "', Ind_TP_CV='" + myAuto.getIndTPCV() + "'"
					+ " where cod_auto_infracao='" + myAuto.getCodAutoInfracao() + "'";
			stmt.execute(sCmd);
			// gravar registro no historico
			GravarHistorico(myHis, conn);
			// fechar a transa��o - commit ou rollback
			conn.commit();
			stmt.close();
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Atualiza
	 * Requerimento com Parecer Juridico e grava Historico - transacao 207 -
	 * Parecer Juridico
	 * -----------------------------------------------------------
	 */
	public void atualizarAuto207(AutoInfracaoBean myAuto, RequerimentoBean myReq, HistoricoBean myHis)
			throws sys.DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			// iniciar transa��o
			conn.setAutoCommit(false);
			// buscar junta e relator - se nao existirem
			if ((myReq.getCodJuntaJU().length() == 0) || (myReq.getCodRelatorJU().length() == 0)) {
				ParamOrgBean myParam = new ParamOrgBean();
				buscarJunta(myReq, myHis, myParam, conn);
			}
			// atualizar requerimento
			updateRequerimento(myReq, conn);
			// gravar registro no historico
			GravarHistorico(myHis, conn);
			// fechar a transa��o - commit ou rollback
			conn.commit();
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Atualiza
	 * Requerimento com Resultado e grava Historico - transacao 209/329 -
	 * Resultado -----------------------------------------------------------
	 */
	public void atualizarAuto209(RequerimentoBean myReq, HistoricoBean myHis, AutoInfracaoBean myAuto)
			throws sys.DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			// atualizar requerimento
			updateRequerimento(myReq, conn);
			// gravar registro no historico
			GravarHistorico(myHis, conn);
			// fechar a transa��o - commit ou rollback
			conn.commit();
			stmt.close();
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- Atualiza
	 * Numero do Processo e grava Historico - transacao 410
	 * -----------------------------------------------------------
	 */
	public void atualizarAuto410(AutoInfracaoBean myAuto, HistoricoBean myHis) throws sys.DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// Inicia cr�tica para ver se processo j� existe
			String sCmd = "SELECT num_auto_infracao FROM  TSMI_AUTO_INFRACAO WHERE num_processo='"
					+ myAuto.getNumProcesso() + "'";
			String au = "";
			boolean tem = false;
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				au = rs.getString("num_auto_infracao");
				if (au == null)
					au = "";
				if (au.equals(myAuto.getNumAutoInfracao()) == false) {
					myAuto.setMsgErro("Opera��o n�o realizada - Processo " + myAuto.getNumProcesso()
							+ " existe para o Auto " + au);
					tem = true;
					break;
				}
			}
			if (!tem) {
				// iniciar transa��o
				conn.setAutoCommit(false);
				// atualizar DATA DO PROCESSO DO Auto de Infracao, se ja nao
				// houver
				sCmd = "UPDATE TSMI_AUTO_INFRACAO set DAT_PROCESSO=to_date('" + myAuto.getDatProcesso()
						+ "','dd/mm/yyyy'), " + " NUM_PROCESSO='" + myAuto.getNumProcesso() + "'"
						+ " where cod_auto_infracao='" + myAuto.getCodAutoInfracao() + "'";
				stmt.execute(sCmd);
				// gravar registro no historico
				GravarHistorico(myHis, conn);
				// fechar a transa��o - commit ou rollback
				conn.commit();
				stmt.close();
			}
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * --------------------------------------- Estorno DP, 1A e 2A - 226 - 350 -
	 * 351 - 352 - 210 - 330 - 337 ---------------------------------------
	 */
	public void atualizarAuto226(AutoInfracaoBean myAuto, RequerimentoBean myReq, HistoricoBean myHis)
			throws sys.DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			// iniciar transa��o
			conn.setAutoCommit(false);
			String sCmd = "";
			String codStatus = "";
			sCmd = " SELECT COD_STATUS_REQUERIMENTO  from TSMI_REQUERIMENTO ";
			sCmd += " WHERE NUM_REQUERIMENTO = '" + myHis.getTxtComplemento01() + "'";

			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				codStatus = rs.getString("COD_STATUS_REQUERIMENTO");
			}
			rs.close();

			if (!codStatus.equals("") && "2,3".indexOf(codStatus) >= 0)
				sCmd = " UPDATE TSMI_REQUERIMENTO  ";
			if (codStatus.equals("2")) {
				codStatus = "0";
				sCmd += " SET DAT_PROC_JU = null,";
				sCmd += " DAT_JU = null,";
				sCmd += " COD_JUNTA_JU = null,";
				sCmd += " COD_RELATOR_JU = null,";
				sCmd += " NOM_USERNAME_JU = null,";
				sCmd += " COD_ORGAO_LOTACAO_JU = null,";
				sCmd += " COD_STATUS_REQUERIMENTO = '" + codStatus + "'";
				sCmd += " WHERE NUM_REQUERIMENTO = '" + myHis.getTxtComplemento01() + "'";
			}

			if (codStatus.equals("3")) {
				codStatus = "2";
				sCmd += " SET DAT_RS = null,";
				sCmd += " COD_RESULT_RS = null,";
				sCmd += " TXT_MOTIVO_RS = null,";
				sCmd += " DAT_PROC_RS = null,";
				sCmd += " COD_STATUS_REQUERIMENTO = '" + codStatus + "'";
				sCmd += " WHERE NUM_REQUERIMENTO = '" + myHis.getTxtComplemento01() + "'";
			}

			stmt.executeUpdate(sCmd);
			stmt.close();

			// gravar registro no historico
			GravarHistorico(myHis, conn);
			// fechar a transa��o - commit ou rollback
			conn.commit();
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

	/**
	 * ----------------------------------------------------------- FIM DAS
	 * TRANSACOES - BROKER
	 * -----------------------------------------------------------
	 */

	/****
	 * ----------------------------------------------------------- Le Status e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean LeStatus(consultaAutoBean mycons) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = LeStatus(mycons, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public boolean LeStatus(consultaAutoBean mycons, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		try {

			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * from TSMI_STATUS_AUTO " + " order by Cod_Status desc";
			ResultSet rs = stmt.executeQuery(sCmd);
			List autos = new ArrayList();
			while (rs.next()) {
				StatusBean mystat = new StatusBean();
				mystat.setCodStatus(rs.getString("Cod_Status"));
				mystat.setNomStatus(rs.getString("Nom_Status"));
				autos.add(mystat);
			}
			mycons.setAutos(autos);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- ConsultaAuto
	 * e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean ConsultaAutos(consultaAutoBean myConsult) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = ConsultaAutos(myConsult, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- ConsultaAuto
	 * e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean ConsultaAutos(consultaAutoBean myConsult, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT a.cod_auto_infracao,a.num_auto_infracao,to_char(a.Dat_infracao,'dd/mm/yyyy') Dat_infracao,"
					+ "	a.num_Placa,a.num_processo,a.cod_orgao,o.sig_orgao,"
					+ "	r.cod_responsavel,r.nom_responsavel,a.cod_status,to_char(a.dat_status,'dd/mm/yyyy') dat_status,s.nom_status,"
					+ "	r.num_cpf_cnpj,r.num_cnh,r.nom_responsavel,to_char(a.Dat_Vencimento,'dd/mm/yyyy') Dat_vencimento,"
					+ "	a.ind_fase,a.ind_situacao,a.ind_pago,i.val_real_desconto "
					+ " from TSMI_AUTO_INFRACAO a left join TSMI_INFRACAO_AUTO i ON (a.cod_auto_infracao=i.cod_auto_infracao), TSMI_RESPONSAVEL r, TSMI_ORGAO o, TSMI_STATUS_AUTO s "
					+ " WHERE a.cod_proprietario= r.cod_responsavel and a.cod_orgao = o.cod_orgao and a.cod_status=s.cod_status  ";

			if (myConsult.getAbrevSist() != "GER") {
				sCmd += " and a.cod_orgao='" + myConsult.getCodOrgaoAtuacao() + "' ";
			} else {
				if (myConsult.getOrgaosEscolhidos().length() > 0)
					sCmd += "and lpad(a.cod_orgao,'6','0') in (" + myConsult.getOrgaosEscolhidos() + ") ";
			}
			if (myConsult.getStatusEscolhidos().length() > 0)
				sCmd += "and lpad(a.cod_status,'10','0') in (" + myConsult.getStatusEscolhidos() + ") ";
			if (myConsult.getNumAutoInfracao().length() > 0)
				sCmd += "and a.num_auto_infracao = '" + myConsult.getNumAutoInfracao() + "' ";
			if (myConsult.getNumPlaca().length() > 0)
				sCmd += "and a.num_placa = '" + myConsult.getNumPlaca() + "' ";
			if (myConsult.getNumProcesso().length() > 0)
				sCmd += "and a.num_processo = '" + myConsult.getNumProcesso() + "' ";
			if ("".equals(myConsult.getNumCPF().trim()) == false)
				sCmd += "and r.num_cpf_cnpj = '" + myConsult.getNumCPF() + "' ";
			if (myConsult.getNumCNH().length() > 0)
				sCmd += "and r.num_cnh = '" + myConsult.getNumCNH() + "' ";
			if ((myConsult.getDatInfracaoDe().length() > 0) && (myConsult.getDatInfracaoDe().length() > 0))
				sCmd += "and a.Dat_infracao between to_date('" + myConsult.getDatInfracaoDe()
						+ "','dd/mm/yyyy') and to_date('" + myConsult.getDatInfracaoAte() + "','dd/mm/yyyy') ";
			sCmd += "order by a.Dat_infracao , a.cod_orgao, a.num_auto_infracao";
			myConsult.setMsgErro("N�O H� DADOS PARA ESSA PESQUISA");
			ResultSet rs = stmt.executeQuery(sCmd);
			// System.err.println("consultaAutoCmd: "+sCmd );
			List autos = new ArrayList();
			while (rs.next()) {
				AutoInfracaoBean myinf = new AutoInfracaoBean();
				myinf.setProprietario(new ResponsavelBean());
				myinf.setCodAutoInfracao(rs.getString("cod_auto_infracao"));
				myinf.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				myinf.setDatInfracao(rs.getString("dat_infracao"));
				myinf.setNumProcesso(rs.getString("num_processo"));
				myinf.setCodOrgao(rs.getString("cod_orgao"));
				myinf.setSigOrgao(rs.getString("sig_orgao"));
				myinf.setNumPlaca(rs.getString("num_Placa"));
				myinf.getProprietario().setNomResponsavel(rs.getString("nom_responsavel"));
				myinf.setCodStatus(rs.getString("cod_status"));
				myinf.setDatStatus(rs.getString("dat_status"));
				myinf.setNomStatus(rs.getString("nom_status"));
				myinf.setDatVencimento(rs.getString("dat_vencimento"));
				myinf.setIndFase(rs.getString("ind_fase"));
				myinf.setIndSituacao(rs.getString("ind_situacao"));
				myinf.setIndPago(rs.getString("ind_pago"));
				myinf.getInfracao().setValRealDesconto(rs.getString("val_real_desconto"));

				myConsult.setMsgErro("");
				autos.add(myinf);
			}
			myConsult.setAutos(autos);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- Le Historico
	 * e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean LeHistorico(AutoInfracaoBean myInf) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = LeHistorico(myInf, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public boolean LeHistorico(AutoInfracaoBean myInf, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT h.cod_auto_infracao,h.cod_historico_auto,h.num_processo,h.cod_orgao,"
					+ " h.cod_status,s.nom_status,to_char(h.Dat_Status,'dd/mm/yyyy') Dat_Status,h.cod_evento,"
					+ " h.cod_origem_evento,h.txt_complemento_01,h.txt_complemento_02,h.txt_complemento_03,"
					+ " h.txt_complemento_04,h.txt_complemento_05,h.txt_complemento_06,h.txt_complemento_07,"
					+ " h.txt_complemento_08,h.txt_complemento_09,h.txt_complemento_10,h.txt_complemento_11,"
					+ " h.nom_username,h.cod_orgao_lotacao,to_char(h.Dat_Proc,'dd/mm/yyyy') Dat_Proc,e.dsc_evento,o.dsc_origem_evento"
					+ " from TSMI_HISTORICO_AUTO h,TSMI_EVENTO e,TSMI_ORIGEM_EVENTO o,TSMI_STATUS_AUTO s "
					+ " WHERE h.cod_status=s.cod_status and h.cod_evento=e.cod_evento and h.cod_origem_evento=o.cod_origem_evento and h.cod_auto_infracao = '"
					+ myInf.getCodAutoInfracao() + "' order by h.dat_status desc, h.cod_historico_auto desc";
			// System.err.println("LeHistorico: "+sCmd );

			ResultSet rs = stmt.executeQuery(sCmd);
			List historicos = new ArrayList();
			while (rs.next()) {
				HistoricoBean myHis = new HistoricoBean();
				myHis.setCodHistoricoAuto(rs.getString("Cod_Historico_Auto"));
				myHis.setCodAutoInfracao(rs.getString("Cod_Auto_Infracao"));
				// myHis.setNumAutoInfracao(rs.getString("Num_Auto_Infracao"));
				myHis.setNumProcesso(rs.getString("Num_Processo"));
				myHis.setCodOrgao(rs.getString("Cod_orgao"));
				myHis.setCodStatus(rs.getString("Cod_Status"));
				myHis.setNomStatus(rs.getString("Nom_Status"));
				myHis.setDatStatus(rs.getString("Dat_Status"));
				myHis.setCodEvento(rs.getString("Cod_Evento"));
				myHis.setNomEvento(rs.getString("dsc_evento"));
				myHis.setCodOrigemEvento(rs.getString("Cod_Origem_Evento"));
				myHis.setNomOrigemEvento(rs.getString("dsc_origem_evento"));
				myHis.setTxtComplemento01(rs.getString("txt_complemento_01"));
				myHis.setTxtComplemento02(rs.getString("txt_complemento_02"));
				myHis.setTxtComplemento03(rs.getString("txt_complemento_03"));
				myHis.setTxtComplemento04(rs.getString("txt_complemento_04"));
				myHis.setTxtComplemento05(rs.getString("txt_complemento_05"));
				myHis.setTxtComplemento06(rs.getString("txt_complemento_06"));
				myHis.setTxtComplemento07(rs.getString("txt_complemento_07"));
				myHis.setTxtComplemento08(rs.getString("txt_complemento_08"));
				myHis.setTxtComplemento09(rs.getString("txt_complemento_09"));
				myHis.setTxtComplemento10(rs.getString("txt_complemento_10"));
				myHis.setTxtComplemento11(rs.getString("txt_complemento_11"));
				myHis.setNomUserName(rs.getString("Nom_Username"));
				myHis.setCodOrgaoLotacao(rs.getString("Cod_Orgao_Lotacao"));
				myHis.setDatProc(rs.getString("Dat_Proc"));
				historicos.add(myHis);
			}
			myInf.setHistoricos(historicos);
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- Le Veiculo e
	 * carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean LeVeiculo(VeiculoBean myVei, String tpPesq) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = LeVeiculo(myVei, tpPesq, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public boolean LeVeiculo(VeiculoBean myVei, String tpPesq, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		try {
			Statement stmt = conn.createStatement();
			myVei.setMsgErro("");
			String sCmd = "SELECT cod_Veiculo,num_Placa,cod_Marca_Modelo,cod_Especie,cod_Categoria,cod_Tipo,cod_Cor ";
			sCmd += " from TSMI_VEICULO ";
			if ("codigo".equals(tpPesq))
				sCmd += " WHERE cod_Veiculo = '" + myVei.getCodVeiculo() + "'";
			if ("placa".equals(tpPesq))
				sCmd += " WHERE num_Placa   = '" + myVei.getNumPlaca() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setCodVeiculo(rs.getString("cod_Veiculo"));
				myVei.setNumPlaca(rs.getString("num_Placa"));
				myVei.setCodMarcaModelo(rs.getString("cod_Marca_Modelo"));
				myVei.setCodEspecie(rs.getString("cod_Especie"));
				myVei.setCodCategoria(rs.getString("cod_Categoria"));
				myVei.setCodTipo(rs.getString("cod_Tipo"));
				myVei.setCodCor(rs.getString("cod_Cor"));
			}
			sCmd = "SELECT dsc_especie from TSMI_ESPECIE WHERE cod_especie = '" + myVei.getCodEspecie() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setDscEspecie(rs.getString("dsc_especie"));
			}
			sCmd = "SELECT dsc_categoria from TSMI_CATEGORIA WHERE cod_categoria = '" + myVei.getCodCategoria() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setDscCategoria(rs.getString("dsc_categoria"));
			}
			sCmd = "SELECT dsc_tipo from TSMI_TIPO WHERE cod_TIPO = '" + myVei.getCodTipo() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setDscTipo(rs.getString("dsc_tipo"));
			}
			sCmd = "SELECT dsc_cor from TSMI_COR WHERE cod_cor = '" + myVei.getCodCor() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setDscCor(rs.getString("dsc_cor"));
			}
			sCmd = "SELECT dsc_marca_modelo from TSMI_MARCA_MODELO WHERE cod_marca_modelo = '"
					+ myVei.getCodMarcaModelo() + "'";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myVei.setDscMarcaModelo(rs.getString("dsc_marca_modelo"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			myVei.setCodVeiculo("0");
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myVei.setCodVeiculo("0");
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- Le
	 * Responsavel e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean LeResponsavel(ResponsavelBean myResp, String tpPesq) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = LeResponsavel(myResp, tpPesq, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public boolean LeResponsavel(ResponsavelBean myResp, String tpPesq, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = false;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT cod_responsavel,nom_responsavel,ind_cpf_cnpj,num_cpf_cnpj,num_cnh,ind_tipo_cnh,cod_uf_cnh,seq_endereco ";
			sCmd += " from TSMI_RESPONSAVEL ";
			if ("codigo".equals(tpPesq))
				sCmd += " WHERE cod_responsavel = " + myResp.getCodResponsavel();
			if ("cpfcnpf".equals(tpPesq))
				sCmd += " WHERE num_cpf_cnpj = " + myResp.getNumCpfCnpj();
			if ("cnh".equals(tpPesq))
				sCmd += " WHERE num_cnh = " + myResp.getNumCnh() + " and cod_uf_cnh='" + myResp.getCodUfCnh()
						+ "' and ind_tipo_cnh=" + myResp.getIndTipoCnh();
			myResp.setMsgErro("Responsavel n�o localizado.");
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myResp.setMsgErro("");
				myResp.setCodResponsavel(rs.getString("cod_responsavel"));
				myResp.setNomResponsavel(rs.getString("nom_responsavel"));
				myResp.setIndCpfCnpj(rs.getString("ind_cpf_cnpj"));
				myResp.setNumCpfCnpj(rs.getString("num_cpf_cnpj"));
				myResp.setNumCnh(rs.getString("num_cnh"));
				myResp.setIndTipoCnh(rs.getString("ind_tipo_cnh"));
				myResp.setCodUfCnh(rs.getString("cod_uf_cnh"));
				myResp.getEndereco().setSeqEndereco(rs.getString("seq_endereco"));
				bOk = true;
			}
			rs.close();
			stmt.close();

			if (bOk == false)
				myResp.setEndereco(new sys.EnderecoBean());
			else {
				sys.Dao sysDao = sys.Dao.getInstance();
				/* sysDao.EnderecoLeBean(myResp.getEndereco(),conn); */
			}

		} catch (SQLException e) {
			bOk = false;
			myResp.setCodResponsavel("0");
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myResp.setCodResponsavel("0");
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	/****
	 * ----------------------------------------------------------- Le Multas na
	 * Base e carrega no Bean
	 * -----------------------------------------------------------
	 */
	public boolean LeInfracao(InfracaoBean myResp, String tpPesq) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			bOk = LeInfracao(myResp, tpPesq, conn);
		} catch (Exception ex) {
			bOk = false;
			throw new sys.DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public boolean LeInfracao(InfracaoBean myInf, String tpPesq, Connection conn) throws sys.DaoException {
		// synchronized
		boolean bOk = true;
		try {
			Statement stmt = conn.createStatement();
			myInf.setMsgErro("");
			String sCmd = "SELECT cod_infracao,dsc_infracao,dsc_enquadramento,";
			sCmd += "dsc_Gravidade,val_real_desconto,val_real,";
			sCmd += " num_ponto,dsc_competencia,ind_cond_prop,ind_suspensao from TSMI_INFRACAO_AUTO ";
			if ("codigo".equals(tpPesq))
				sCmd += " WHERE cod_auto_infracao = '" + myInf.getCodAutoInfracao() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myInf.setCodInfracao(rs.getString("cod_infracao"));
				myInf.setDscInfracao(rs.getString("dsc_infracao"));
				myInf.setDscGravidade(rs.getString("dsc_Gravidade"));
				myInf.setValRealDesconto(rs.getString("val_real_desconto"));
				myInf.setValReal(rs.getString("val_real"));
				myInf.setDscEnquadramento(rs.getString("dsc_enquadramento"));
				myInf.setNumPonto(rs.getString("num_ponto"));
				myInf.setDscCompetencia(rs.getString("dsc_competencia"));
				myInf.setIndSDD(rs.getString("ind_suspensao"));
				myInf.setIndTipo(rs.getString("ind_cond_prop"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			bOk = false;
			myInf.setCodInfracao("0");
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bOk = false;
			myInf.setCodInfracao("0");
			throw new sys.DaoException(ex.getMessage());
		}
		return bOk;
	}

	public void GravarHistorico(HistoricoBean myHis, Connection conn) throws sys.DaoException {
		// synchronized
		try {
			String sCmd = "INSERT INTO TSMI_HISTORICO_AUTO " + "(Cod_historico_auto,Cod_Auto_Infracao,"
					+ " Num_processo,cod_orgao,Cod_Status,Dat_Status," + " cod_evento,cod_origem_evento,"
					+ " txt_complemento_01,txt_complemento_02,txt_complemento_03,"
					+ " txt_complemento_04,txt_complemento_05,txt_complemento_06,"
					+ " txt_complemento_07,txt_complemento_08,txt_complemento_09,"
					+ " txt_complemento_10,txt_complemento_11," + " Nom_Username,Cod_Orgao_Lotacao,Dat_Proc)"
					+ " VALUES (" + " seq_tsmi_historico_auto.nextval,'" + myHis.getCodAutoInfracao() + "', " + " '"
					+ myHis.getNumProcesso() + "','" + myHis.getCodOrgao() + "','" + myHis.getCodStatus()
					+ "',to_date('" + myHis.getDatStatus() + "','dd/mm/yyyy')," + " '" + myHis.getCodEvento() + "','"
					+ myHis.getCodOrigemEvento() + "', " + " '" + myHis.getTxtComplemento01() + "','"
					+ myHis.getTxtComplemento02() + "','" + myHis.getTxtComplemento03() + "', " + " '"
					+ myHis.getTxtComplemento04() + "','" + myHis.getTxtComplemento05() + "','"
					+ myHis.getTxtComplemento06() + "', " + " '" + myHis.getTxtComplemento07() + "','"
					+ myHis.getTxtComplemento08() + "','" + myHis.getTxtComplemento09() + "', " + " '"
					+ myHis.getTxtComplemento10() + "','" + myHis.getTxtComplemento11() + "'," + " '"
					+ myHis.getNomUserName() + "','" + myHis.getCodOrgaoLotacao() + "', " + " to_date('"
					+ myHis.getDatProc() + "','dd/mm/yyyy')) ";
			Statement stmt = conn.createStatement();
			// System.err.println("gravarHistorico: "+sCmd );
			stmt.execute(sCmd);
			stmt.close();
		} catch (SQLException e) {
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			throw new sys.DaoException(ex.getMessage());
		}
		return;
	}

	public void GravarHistorico(AutoInfracaoBean myInf) throws DaoException, ServiceLocatorException, SQLException {
		Connection conn = null;
		ResultSet rs = null;
		String sCmd = "";
		conn = serviceloc.getConnection(MYABREVSIST);
		Statement stmt = conn.createStatement();
		conn.setAutoCommit(false);
		try {
			boolean existeAuto = false;
			sCmd = "DELETE FROM TSMI_HISTORICO_AUTO WHERE Cod_Auto_Infracao='" + myInf.getCodAutoInfracao() + "'";
			stmt.executeQuery(sCmd);
			Iterator it = myInf.getHistoricos().iterator();
			REC.HistoricoBean myhist = new REC.HistoricoBean();
			while (it.hasNext()) {
				myhist = (REC.HistoricoBean) it.next();
				GravarHistorico(myhist, conn);
			}
			conn.commit();
			if (rs != null)
				rs.close();
			if (conn != null)
				stmt.close();
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					conn.rollback();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
	}

	public boolean VerificaRequerimento(RequerimentoBean myReq, Connection conn) throws sys.DaoException {
		boolean bVerifica = true;
		try {
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_REQUERIMENTO WHERE NUM_REQUERIMENTO='" + myReq.getNumRequerimento() + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				bVerifica = false;
			rs.close();
			stmt.close();
			return bVerifica;
		} catch (SQLException e) {
			bVerifica = false;
			throw new sys.DaoException(e.getMessage());
		} catch (Exception ex) {
			bVerifica = false;
			throw new sys.DaoException(ex.getMessage());
		}
	}

	/**
	 * ----------------------------------------------------------- Atualiza
	 * AUTO, grava requerimento e Historico - transacao 206 - Abrir Defesa
	 * Pr�via/TRI -----------------------------------------------------------
	 */
	public void buscarJunta(RequerimentoBean myReq, HistoricoBean myHis, ParamOrgBean myParam, Connection conn)
			throws sys.DaoException {
		try {
			int ultJunta = 0;
			int ultRelator = 0;
			// verificar tipo de Junta
			String tpJun = ("DP,DC".indexOf(myReq.getCodTipoSolic()) >= 0) ? "0" : "1";
			String myOrg = myHis.getCodOrgao();
			if ("2P,2C".indexOf(myReq.getCodTipoSolic()) >= 0) {
				myOrg = myParam.getParamOrgao("COD_ORGAO_2A_INSTANCIA", "900", "4");
				tpJun = "2";
			}
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT COD_JUNTA,cod_ult_junta FROM TSMI_JUNTA " + " where cod_orgao='" + myOrg
					+ "' and ind_tipo='" + tpJun
					+ "' and cod_junta in (Select cod_junta from tsmi_relator) order by cod_ult_junta desc FOR UPDATE";
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myReq.setCodJuntaJU(rs.getString("COD_JUNTA"));
				ultJunta = rs.getInt("cod_ult_junta");
				break;
			}
			sCmd = "SELECT COD_RELATOR,NUM_CPF,cod_ult_relator FROM TSMI_RELATOR " + " where COD_JUNTA='"
					+ myReq.getCodJuntaJU() + "' order by cod_ult_relator desc FOR UPDATE";
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				myReq.setCodRelatorJU(rs.getString("NUM_CPF"));
				ultRelator = rs.getInt("cod_ult_relator");
				break;
			}
			// atualizar
			ultRelator++;
			sCmd = "UPDATE TSMI_RELATOR SET cod_ult_relator='" + ultRelator + "' WHERE COD_RELATOR='"
					+ myReq.getCodRelatorJU() + "'";
			stmt.execute(sCmd);
			ultJunta++;
			sCmd = "UPDATE TSMI_JUNTA SET cod_ult_junta='" + ultJunta + "' WHERE COD_JUNTA='" + myReq.getCodJuntaJU()
					+ "'";
			stmt.execute(sCmd);
		} catch (SQLException sqle) {
			throw new sys.DaoException(sqle.getMessage());
		} catch (Exception e) {
			throw new sys.DaoException(e.getMessage());
		} // fim do catch
		return;
	}

	/**
	 * Atualiza a data de digitaliza��o do auto de infra��o. Esta data � usada
	 * para controlar as pend�ncias de digitaliza��o de autos
	 * 
	 * @author Jos� Fernando
	 * @since 30/03/2006
	 */
	public void atualizaDigitalizacao(AutoInfracaoBean bean) throws DaoException {
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = " UPDATE TSMI_AUTO_INFRACAO SET " + " DAT_DIGITALIZACAO = to_date('"
					+ df.format(bean.getDatDigitalizacao()) + "', 'dd/mm/yyyy') ," + " SIT_DIGITALIZACAO = '"
					+ bean.getSit_digitaliza() + "'" + " WHERE NUM_AUTO_INFRACAO = '" + bean.getNumAutoInfracao() + "'";

			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();

		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}

	public ArrayList getEventos() throws DaoException {
		Connection conn = null;
		ArrayList eventos = new ArrayList();
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "Select cod_evento,dsc_evento,cod_evento_ref from tsmi_evento "
					+ "where cod_evento_ref is not null order by dsc_evento";
			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {

				EventoBean myEvent = new EventoBean();

				myEvent.setCodEvento(rs.getString("cod_evento"));
				myEvent.setDscEvento(rs.getString("dsc_evento"));
				myEvent.setCodEventoRef(rs.getString("cod_evento_ref"));
				eventos.add(myEvent);
			}

			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return eventos;

	}

	public String buscaCodEventoRef(String codEvento) throws DaoException {
		Connection conn = null;
		String codEventoRef = "";
		try {

			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();

			String sCmd = "Select cod_evento_ref from tsmi_evento " + "where cod_evento = " + codEvento;

			ResultSet rs = stmt.executeQuery(sCmd);
			rs = stmt.executeQuery(sCmd);
			while (rs.next()) {
				codEventoRef = rs.getString("cod_evento_ref");
			}

			rs.close();
			stmt.close();
		} catch (SQLException sqle) {
		} // fim do catch
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return codEventoRef;

	}

	/**
	 * ----------------------------------------------------------- Le
	 * NumProcessoAuto
	 * -----------------------------------------------------------
	 */
	public String LeNumProcessoAuto(String nrAuto) {
		Connection conn = null;
		String sCmd = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			sCmd = "SELECT  NUM_PROCESSO FROM TSMI_AUTO_INFRACAO A WHERE A.NUM_AUTO_INFRACAO = '" + nrAuto + "'";
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next()) {
				if (rs.getString("NUM_PROCESSO") == null) {
					return "";
				} else {
					if (rs.getString("NUM_PROCESSO").length() > 0) {
						return rs.getString("NUM_PROCESSO");
					} else {
						return "";
					}
				}
			}
			if (stmt != null)
				stmt.close();
			rs.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return "";
		}
		return "";
	}

	public REC.RequerimentoBean consultarRequerimentoPor(String codRequerimento) throws Exception {
		REC.RequerimentoBean requerimento = new REC.RequerimentoBean();

		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select * from tsmi_requerimento req where req.cod_requerimento = " + codRequerimento;

		ResultSet result = stm.executeQuery(query);

		if (result.next()) {
			requerimento.setCodAutoInfracao(result.getString("COD_AUTO_INFRACAO"));
			requerimento.setCodRequerimento(result.getString("COD_REQUERIMENTO"));
			requerimento.setCodTipoSolic(result.getString("COD_TIPO_SOLIC"));
			requerimento.setNumRequerimento(result.getString("NUM_REQUERIMENTO"));
			requerimento.setDatRequerimento(result.getString("DAT_REQUERIMENTO"));
			requerimento.setCodStatusRequerimento(result.getString("COD_STATUS_REQUERIMENTO"));
			requerimento.setNomUserNameDP(result.getString("NOM_USERNAME_DP"));
			requerimento.setCodOrgaoLotacaoDP(result.getString("COD_ORGAO_LOTACAO_DP"));
			requerimento.setNomUserNameRS(result.getString("NOM_RESP_DP"));
			requerimento.setDatProcDP(result.getString("DAT_PROC_DP"));
			requerimento.setDatPJ(result.getString("DAT_PJ"));
			requerimento.setCodParecerPJ(result.getString("COD_PARECER_PJ"));
			requerimento.setTxtMotivoPJ(result.getString("TXT_MOTIVO_PJ"));
			requerimento.setCodRespPJ(result.getString("COD_RESP_PJ"));
			requerimento.setNomUserNamePJ(result.getString("NOM_USERNAME_PJ"));
			requerimento.setCodOrgaoLotacaoPJ(result.getString("COD_ORGAO_LOTACAO_PJ"));
			requerimento.setDatProcPJ(result.getString("DAT_PROC_PJ"));
			requerimento.setDatJU(result.getString("DAT_JU"));
			requerimento.setCodJuntaJU(result.getString("COD_JUNTA_JU"));
			requerimento.setCodRelatorJU(result.getString("COD_RELATOR_JU"));
			requerimento.setNomUserNameJU(result.getString("NOM_USERNAME_JU"));
			requerimento.setCodOrgaoLotacaoJU(result.getString("COD_ORGAO_LOTACAO_JU"));
			requerimento.setDatProcJU(result.getString("DAT_PROC_JU"));
			requerimento.setDatRS(result.getString("DAT_RS"));
			requerimento.setCodResultRS(result.getString("COD_RESULT_RS"));
			requerimento.setTxtMotivoRS(result.getString("TXT_MOTIVO_RS"));
			requerimento.setNomUserNameRS(result.getString("NOM_USERNAME_RS"));
			requerimento.setCodOrgaoLotacaoRS(result.getString("COD_ORGAO_LOTACAO_RS"));
			requerimento.setDatProcRS(result.getString("DAT_PROC_RS"));
			requerimento.setDatAtuTR(result.getString("DAT_ATU_TR"));
			requerimento.setNomCondutorTR(result.getString("NOM_CONDUTOR_TR"));
			requerimento.setNumCNHTR(result.getString("NUM_CNH_TR"));
			requerimento.setCodUfCNHTR(result.getString("COD_UF_CNH_TR"));
			requerimento.setNumCPFTR(result.getString("NUM_CPF_TR"));
			requerimento.setCodUfTR(result.getString("COD_UF_TR"));
			requerimento.setNomUserNameTR(result.getString("NOM_USERNAME_TR"));
			requerimento.setCodOrgaoLotacaoTR(result.getString("COD_ORGAO_LOTACAO_TR"));
			requerimento.setDatProcTR(result.getString("DAT_PROC_TR"));
			requerimento.setDatProcEST(result.getString("DAT_PROC_EST"));
			requerimento.setIndTipoCNHTR(result.getString("IND_TIPO_CNH_TR"));
			requerimento.setSitProtocolo(result.getString("SIT_PROTOCOLO"));
			requerimento.setDatEnvioGuia(result.getString("DAT_ENVIO_GUIA"));
			requerimento.setNomUserNameEnvio(result.getString("NOM_USERNAME_ENVIO_GUIA"));
			requerimento.setDatRecebimentoGuia(result.getString("DAT_RECEB_GUIA"));
			requerimento.setNomUserNameRecebimento(result.getString("NOM_USERNAME_RECEB_GUIA"));
			requerimento.setCodRemessa(result.getString("COD_REMESSA"));
			// requerimento.setsit.setSitCedoc(result.getString("SIT_CEDOC"));
			requerimento.setCodEnvioDO(result.getString("COD_ENVIO_PUBDO"));
			requerimento.setDatEnvioDO(result.getString("DAT_ENVIO_PUBDO"));
			requerimento.setDatPublPDO(result.getString("DAT_PUBDO"));
			requerimento.setCodMotivoDef(result.getString("COD_MOTIVO_DEFERIDO"));
			requerimento.setIndReqWEB(result.getString("IND_WEB"));
			requerimento.setCodAta(result.getString("COD_ATA"));
		}

		// Set<String> sets = auto.keySet();
		// for (String key : sets) {
		// if(key == "NUM_PONTO")
		// auto.put(key, "0");
		// else if(auto.get(key) == null)
		// auto.put(key, "");
		// }

		stm.close();
		conn.close();

		return requerimento;
	}

	public List<HashMap<String, String>> consultarRequerimentoPorAta(String codAta, String codRelator, String codOrgao)
			throws Exception {
		List<HashMap<String, String>> listaRequerimentos = new ArrayList<HashMap<String, String>>();

		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();
		String query = "select distinct" + " ata.des_ata," + " ata.dt_sessao," + " ai.num_auto_infracao,"
				+ " ai.num_processo," + " ai.cod_infracao," + " ai.nom_proprietario," + " rel.nom_relator,"
				+ " ai.num_placa," + " ai.dat_processo," + " req.cod_result_rs"
				+ " from tsmi_requerimento req, tsmi_auto_infracao ai, tsmi_ata ata, tsmi_relator rel"
				+ " where req.cod_auto_infracao = ai.cod_auto_infracao" + " and req.cod_ata = ata.cod_ata"
				+ " and req.cod_relator_ju = rel.cod_relator" + " and ata.cod_orgao = " + codOrgao
				+ " and ata.cod_ata = " + codAta;

		if (!codRelator.isEmpty() && !codRelator.equals("SELECIONE"))
			query += " and req.cod_relator_ju = " + codRelator;

		query += " order by ai.num_auto_infracao asc";

		ResultSet result = stm.executeQuery(query);

		while (result.next()) {
			HashMap<String, String> requerimento = new HashMap<String, String>();

			requerimento.put("ata", result.getString("des_ata"));
			requerimento.put("dataSessao", result.getString("dt_sessao"));
			requerimento.put("numAuto", result.getString("num_auto_infracao"));
			requerimento.put("numProcesso", result.getString("num_processo"));
			requerimento.put("nomRequerente", result.getString("nom_proprietario"));
			requerimento.put("codInfracao", result.getString("cod_infracao"));
			requerimento.put("nomRelator", result.getString("nom_relator"));
			requerimento.put("placa", result.getString("num_placa"));
			requerimento.put("dataProcesso", result.getString("dat_processo"));
			requerimento.put("resultado", result.getString("cod_result_rs"));

			listaRequerimentos.add(requerimento);
		}

		result.close();
		stm.close();
		conn.close();

		return listaRequerimentos;
	}

	public List<HashMap<String, String>> consultarRequerimentosPorAuto(String codAuto) throws Exception {
		List<HashMap<String, String>> lista = new ArrayList<HashMap<String, String>>();
		String query = "select distinct" + " req.cod_requerimento," + " ai.num_auto_infracao," + " ai.num_placa,"
				+ " req.cod_tipo_solic," + " ata.des_ata," + " ai.num_processo," + " ai.dat_infracao,"
				+ " ai.dsc_local_infracao," + " ai.val_real," + " ai.dsc_enquadramento," + " ai.nom_proprietario,"
				+ " ai.cod_infracao," + " req.cod_result_rs," + " rel.nom_relator," + " req.num_requerimento,"
				+ " req.dat_requerimento"
				+ " from tsmi_requerimento req, tsmi_auto_infracao ai, tsmi_ata ata, tsmi_relator rel"
				+ " where req.cod_auto_infracao = ai.cod_auto_infracao" + " and req.cod_ata = ata.cod_ata (+)"
				+ " and req.cod_relator_ju = rel.cod_relator (+)" + " and req.cod_auto_infracao = " + codAuto;

		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement stm = conn.createStatement();

		ResultSet result = stm.executeQuery(query);

		while (result.next()) {
			HashMap<String, String> item = new HashMap<String, String>();

			item.put("codRequerimento", result.getString("cod_requerimento"));
			item.put("numAuto", result.getString("num_auto_infracao"));
			item.put("placa", result.getString("num_placa"));
			item.put("tipoRequerimento", result.getString("cod_tipo_solic"));
			item.put("ata", result.getString("des_ata"));
			item.put("numProcesso", result.getString("num_processo"));
			item.put("dataInfracao", ControladorRecGeneric.dateToDateBr(result.getString("dat_infracao")));
			item.put("localInfracao", result.getString("dsc_local_infracao"));
			item.put("valor", result.getString("val_real"));
			item.put("enquadramento", result.getString("dsc_enquadramento"));
			item.put("nomProprietario", result.getString("nom_proprietario"));
			item.put("codInfracao", result.getString("cod_infracao"));
			item.put("resultado", result.getString("cod_result_rs"));
			item.put("relator", result.getString("nom_relator"));
			item.put("numRequerimento", result.getString("num_requerimento").replace("REQ_", "").replace("_00", "")
					.replace("_01", "").replace("_02", "").replace("_03", "").replace("REQ/", ""));
			item.put("dataRequerimento", ControladorRecGeneric.dateToDateBr(result.getString("dat_requerimento")));

			lista.add(item);
		}

		result.close();
		stm.close();
		conn.close();

		return lista;
	}

	public boolean cadastrarHistorico(HistoricoBean historico) {
		boolean result = false;
		Connection conn;
		Statement statement;

		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			statement = conn.createStatement();
			String query = "INSERT INTO tsmi_historico_auto"
					+ " (COD_AUTO_INFRACAO, COD_HISTORICO_AUTO, NUM_PROCESSO, COD_STATUS, DAT_STATUS, COD_EVENTO, COD_ORIGEM_EVENTO, TXT_COMPLEMENTO_01,"
					+ " TXT_COMPLEMENTO_02, TXT_COMPLEMENTO_03, TXT_COMPLEMENTO_04, TXT_COMPLEMENTO_05, TXT_COMPLEMENTO_06,"
					+ " TXT_COMPLEMENTO_07, TXT_COMPLEMENTO_08, TXT_COMPLEMENTO_09, TXT_COMPLEMENTO_10, TXT_COMPLEMENTO_11,"
					+ " NOM_USERNAME, COD_ORGAO_LOTACAO, DAT_PROC, TXT_COMPLEMENTO_12, COD_ATA, DS_ATA)" + " VALUES ("
					+ historico.getCodAutoInfracao() + ", SEQ_TSMI_HISTORICO_AUTO.Nextval, '"
					+ historico.getNumProcesso() + "', " + historico.getCodStatus() + "," + " '"
					+ historico.getDatStatus() + "', " + historico.getCodEvento() + ", '"
					+ historico.getCodOrigemEvento() + "'," + " '" + historico.getTxtComplemento01() + "', '"
					+ historico.getTxtComplemento02() + "', '" + historico.getTxtComplemento03() + "'," + " '"
					+ historico.getTxtComplemento04() + "', '" + historico.getTxtComplemento05() + "', '"
					+ historico.getTxtComplemento06() + "'," + " '" + historico.getTxtComplemento07() + "', '"
					+ historico.getTxtComplemento08() + "', '" + historico.getTxtComplemento09() + "'," + " '"
					+ historico.getTxtComplemento10() + "', '" + historico.getTxtComplemento11() + "', '"
					+ historico.getNomUserName() + "'," + " " + historico.getCodOrgaoLotacao() + ", '"
					+ historico.getDatProc() + "', '" + historico.getTxtComplemento12() + "'," + " '"
					+ historico.getCodAta() + "', '" + historico.getDsAta() + "')";

			result = statement.execute(query);

			statement.close();
			conn.close();
		} catch (Exception ex) {
			System.out.println("ERRO: class Dao, m�todo cadastrarHistorico - " + ex.getMessage());
		} finally {
		}

		return result;
	}

	public void cadastarRequerimento(RequerimentoBean requerimento) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement statement = conn.createStatement();
		String query = "INSERT INTO TSMI_REQUERIMENTO (COD_AUTO_INFRACAO, COD_REQUERIMENTO, COD_TIPO_SOLIC, "
				+ "NUM_REQUERIMENTO, DAT_REQUERIMENTO, COD_STATUS_REQUERIMENTO, NOM_USERNAME_DP, "
				+ "COD_ORGAO_LOTACAO_DP, NOM_RESP_DP, DAT_PROC_DP, DAT_PJ, COD_PARECER_PJ, TXT_MOTIVO_PJ, "
				+ "COD_RESP_PJ, NOM_USERNAME_PJ, COD_ORGAO_LOTACAO_PJ, DAT_PROC_PJ, DAT_JU, COD_JUNTA_JU, "
				+ "COD_RELATOR_JU, NOM_USERNAME_JU, COD_ORGAO_LOTACAO_JU, DAT_PROC_JU, DAT_RS, COD_RESULT_RS, "
				+ "TXT_MOTIVO_RS, NOM_USERNAME_RS, COD_ORGAO_LOTACAO_RS, DAT_PROC_RS, DAT_ATU_TR, NOM_CONDUTOR_TR, "
				+ "NUM_CNH_TR, COD_UF_CNH_TR, NUM_CPF_TR, COD_UF_TR, NOM_USERNAME_TR, COD_ORGAO_LOTACAO_TR, DAT_PROC_TR, "
				+ "DAT_PROC_EST, IND_TIPO_CNH_TR, SIT_PROTOCOLO, DAT_ENVIO_GUIA, NOM_USERNAME_ENVIO_GUIA, DAT_RECEB_GUIA, "
				+ "NOM_USERNAME_RECEB_GUIA, COD_REMESSA, SIT_CEDOC, COD_ENVIO_PUBDO, DAT_ENVIO_PUBDO, DAT_PUBDO, "
				+ "COD_MOTIVO_DEFERIDO, IND_WEB, COD_ATA) " + "VALUES " + "(" + requerimento.getCodAutoInfracao()
				+ ", SEQ_TSMI_REQUERIMENTO.NEXTVAL, '" + requerimento.getCodTipoSolic() + "', '"
				+ requerimento.getNumRequerimento() + "', '" + requerimento.getDatRequerimento() + "', " + "'"
				+ requerimento.getCodStatusRequerimento() + "', '" + requerimento.getNomUserNameDP() + "', '"
				+ requerimento.getCodOrgaoLotacaoDP() + "', '" + requerimento.getNomResponsavelDP() + "', '"
				+ requerimento.getDatProcDP() + "', " + "'" + requerimento.getDatPJ() + "', '"
				+ requerimento.getCodParecerPJ() + "', '" + requerimento.getTxtMotivoPJ() + "', '"
				+ requerimento.getCodRespPJ() + "', '" + requerimento.getNomUserNamePJ() + "', '"
				+ requerimento.getCodOrgaoLotacaoPJ() + "', " + "'" + requerimento.getDatProcPJ() + "', '"
				+ requerimento.getDatJU() + "', '" + requerimento.getCodJuntaJU() + "', '"
				+ requerimento.getCodRelatorJU() + "', '" + requerimento.getNomUserNameJU() + "', '"
				+ requerimento.getCodOrgaoLotacaoJU() + "', " + "'" + requerimento.getDatProcJU() + "', '"
				+ requerimento.getDatRS() + "', '" + requerimento.getCodResultRS() + "', '"
				+ requerimento.getTxtMotivoRS() + "', '" + requerimento.getNomUserNameRS() + "', '"
				+ requerimento.getCodOrgaoLotacaoRS() + "', " + "'" + requerimento.getDatProcRS() + "', '"
				+ requerimento.getDatAtuTR() + "', '" + requerimento.getNomCondutorTR() + "', '"
				+ requerimento.getNumCNHTR() + "', '" + requerimento.getCodUfCNHTR() + "', '"
				+ requerimento.getNumCPFTR() + "', '" + requerimento.getCodUfTR() + "', '"
				+ requerimento.getNomUserNameTR() + "', '" + requerimento.getCodOrgaoLotacaoTR() + "', '"
				+ requerimento.getDatProcTR() + "', '" + requerimento.getDatProcEST() + "', '"
				+ requerimento.getIndTipoCNHTR() + "', '" + requerimento.getSitProtocolo() + "', '"
				+ requerimento.getDatEnvioGuia() + "', '" + requerimento.getNomUserNameEnvio() + "', '"
				+ requerimento.getDatRecebimentoGuia() + "', '" + requerimento.getNomUserNameRecebimento() + "', '"
				+ requerimento.getCodRemessa() + "', '', '" + requerimento.getCodEnvioDO() + "', '"
				+ requerimento.getDatEnvioDO() + "', '" + requerimento.getDatPublPDO() + "', '"
				+ requerimento.getCodMotivoDef() + "', '" + requerimento.getIndReqWEB() + "', '"
				+ requerimento.getCodAta() + "')";

		statement.execute(query);

		statement.close();
		conn.close();
	}

	public void atualizarDataRequerimento(RequerimentoBean requerimento) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement statement = conn.createStatement();
		String query = "update tsmi_requerimento req" + " set req.dat_requerimento = '"
				+ requerimento.getDatRequerimento() + "'" + " where req.cod_requerimento = "
				+ requerimento.getCodRequerimento();

		statement.execute(query);

		statement.close();
		conn.close();
	}

	public void deletarRequerimento(String codRequerimento) throws Exception {
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement statement = conn.createStatement();
		String query = "delete tsmi_requerimento req where req.COD_REQUERIMENTO = " + codRequerimento;

		statement.execute(query);

		statement.close();
		conn.close();
	}

	public boolean informarResultadoRequerimento(String codRequerimento, String resultado) throws Exception {
		boolean retorno = true;
		Connection conn = serviceloc.getConnection(MYABREVSIST);
		Statement statement = conn.createStatement();
		String query = "update tsmi_requerimento r set r.cod_result_rs = '@resultado'";

		if (resultado.isEmpty())
			query += ", r.cod_status_requerimento = '0'";
		else
			query += ", r.cod_status_requerimento = '3'";

		query += " where r.cod_requerimento = @codRequerimento";
		
		query = query.replace("@resultado", resultado);
		query = query.replace("@codRequerimento", codRequerimento);

		retorno = statement.execute(query);
		
		statement.close();
		conn.close();

		return retorno;
	}
}