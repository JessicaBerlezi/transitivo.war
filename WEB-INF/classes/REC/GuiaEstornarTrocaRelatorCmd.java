package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class GuiaEstornarTrocaRelatorCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaReemite.jsp" ;  
   
   public GuiaEstornarTrocaRelatorCmd() {
      next = jspPadrao;
   }

   public GuiaEstornarTrocaRelatorCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
		  ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
      	  GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
      		//Carrego os Beans		
      		
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
		  GuiaDistribuicaoId.setMsgErro("");
		  String acao    = req.getParameter("acao");   	  
		  if (acao==null)	{
			GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");						  	
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  	acao = " ";
		  }
		  else {
			if ("LeGuia,TrocaRelator,Classifica,MostraAuto,imprimirInf,ImprimirGuia".indexOf(acao)<0)	{
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		  }
		  if (acao.equals("LeGuia")) {
		    GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;		
		    GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
		    GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		    GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());						
			GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
			GuiaDistribuicaoId.LeGuia();
		    if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
			nextRetorno = "/REC/GuiaTrocaRelator.jsp";
		  }	        
          if (acao.equals("Classifica"))    GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;       

		  if  (acao.equals("ImprimirGuia"))  {	      	  
		  	if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
		  	nextRetorno = "/REC/GuiaPreparaImp.jsp" ;      	  
		  }
      	  if (acao.equals("MostraAuto"))  {
        	AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
			if (acao.equals("MostraAuto")) { 
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			}
			else {
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
				nextRetorno = "/REC/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
			UsrLogado.setCodOrgaoAtuacao(temp);	
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
      	  }	
		  if (acao.equals("TrocaRelator")) {  
		    TrocaRelator(req,GuiaDistribuicaoId,UsrLogado) ;
			nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp" ; 				
		  }
          // processamento de saida dos formularios
          if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
      	  else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaTrocaRelatorCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }

   public void TrocaRelator(HttpServletRequest req,GuiaDistribuicaoBean GuiaDistribuicaoId,
	   ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
		try {	
	 		GuiaDistribuicaoId.setMsgErro("") ;
	 		Vector vErro = new Vector() ;
	 		GuiaDistribuicaoId.setCodJunta(req.getParameter("codJuntaJU"));
	 		JuntaBean myJunta = new JuntaBean();
	 		myJunta.Le_Junta(GuiaDistribuicaoId.getCodJunta(),0);
	 		GuiaDistribuicaoId.setSigJunta(myJunta.getSigJunta());			

			GuiaDistribuicaoId.setNumCPFRelator(req.getParameter("codRelatorJU"));
	 		RelatorBean myRelator = new RelatorBean();
	 		myRelator.setNumCpf(GuiaDistribuicaoId.getNumCPFRelator());
	 		myRelator.setCodJunta(GuiaDistribuicaoId.getCodJunta());
	 		myRelator.Le_Relator("CPF");			
	 		GuiaDistribuicaoId.setNomRelator(myRelator.getNomRelator());			
	 		GuiaDistribuicaoId.setCodRelator(myRelator.getCodRelator());
		
			 if ( (GuiaDistribuicaoId.getCodJunta().length()==0) ||
				  (GuiaDistribuicaoId.getNumCPFRelator().length()==0)) vErro.addElement("Junta/Relator n�o selecionado. \n") ;
			 if (vErro.size()>0) GuiaDistribuicaoId.setMsgErro(vErro);
			 else{	 
				 GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
				 dao.GuiaEstornoTrocaRelator(GuiaDistribuicaoId) ; 
	 		}		
	   }
	   catch (Exception ue) {
			 throw new sys.CommandException("GuiaTrocaRelatorCmd: " + ue.getMessage());
	   }
	   return ;
   } 	

   
}
