package REC;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.CommandException;

/**
* <b>Title:</b>        RECURSO - Relatorio de Controle de AR, AI e fotos Cmd<br>
* <b>Description:</b>  Informa os autos ser foto ou AR. <br>
* <b>Copyright:</b>    Copyright (c) 2005<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/


public class ConsultaAutosSemDigiCmd extends sys.Command {

	private static final String jspPadrao = "/REC/ConsultaAutosSemDig.jsp";
	private String next;

	public ConsultaAutosSemDigiCmd() {
		next = jspPadrao;
	}

	public ConsultaAutosSemDigiCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.OrgaoBean OrgaoBeanId = (ACSS.OrgaoBean) req.getAttribute("OrgaoBeanId");
			if (OrgaoBeanId == null) OrgaoBeanId = new ACSS.OrgaoBean();
			
			ConsultaAutosSemDigiBean consultaAutosSemDigiBean = (ConsultaAutosSemDigiBean) session.getAttribute("consultaAutosSemDigitalizadosBean");
			if (consultaAutosSemDigiBean == null) consultaAutosSemDigiBean = new ConsultaAutosSemDigiBean();
	
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null) acao = " ";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null) codOrgao = "";
			
			String codStatus = req.getParameter("codStatus");
			if (codStatus == null) codStatus = "";
			
			String dataIni = req.getParameter("dataIni");
			if (dataIni == null) dataIni = "";
			
			String dataFim = req.getParameter("dataFim");
			if (dataFim == null) dataFim = "";
			
			String opcao = req.getParameter("opcao");
			if (opcao == null) opcao = "";
			
			String quantAutosAlta = req.getParameter("quantAutosAlta");
			if (quantAutosAlta == null) quantAutosAlta = "N";
			
			String ordem = req.getParameter("ordem");
			if(ordem == null)ordem = "numAuto";
			
			String msg = "";
			consultaAutosSemDigiBean.setCodOrgao(codOrgao);
			consultaAutosSemDigiBean.setDataIni(dataIni);
			consultaAutosSemDigiBean.setDataFim(dataFim);
			consultaAutosSemDigiBean.setOpcao(opcao);
			consultaAutosSemDigiBean.setCodStatus(codStatus);
	
			if (acao.equals("MostraConsulta")) {
  				//Titulo do Relatorio 
					String tituloConsulta = "RELAT�RIO DE AUTOS SEM DIGITALIZADOS : "+dataIni+" at� "+dataFim;
					req.setAttribute("tituloConsulta", tituloConsulta);
					OrgaoBeanId.Le_Orgao(codOrgao,0);
					
					consultaAutosSemDigiBean.consultaAutoSemAr(consultaAutosSemDigiBean,UsrLogado);

					if (consultaAutosSemDigiBean.getAutos().size()==0) msg = "N�O EXISTEM REGISTROS NESTE PER�ODO DE DIAS." ;	
	        if (consultaAutosSemDigiBean.getAutos().size()>=1000) quantAutosAlta = "S";
	        
				 	else nextRetorno="/REC/ConsultaAutosSemDigitImp.jsp";
			} 
			  
			if (acao.equals("Cancela"))quantAutosAlta = "N";

			session.setAttribute("UsrLogado", UsrLogado);
			req.setAttribute("msg", msg);
			req.setAttribute("OrgaoBeanId", OrgaoBeanId);
			req.setAttribute("quantAutosAlta", quantAutosAlta);
		  session.setAttribute("consultaAutosSemDigiBean",consultaAutosSemDigiBean);
		}
		catch (Exception ue) {
			throw new sys.CommandException("ConsultaAutosSemDigiCmd: "+ ue.getMessage());
		}
		return nextRetorno;
	}

}