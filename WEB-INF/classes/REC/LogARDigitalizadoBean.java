package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Title:</b>        SMIT - MODULO RECURSO<br>
 * <b>Description:</b>  Bean de Aviso de Recebimento Digitalizado<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class LogARDigitalizadoBean extends sys.HtmlPopupBean {
    
    private Date datProcessamento;
    private String nomArquivo;
    private String numCaixa;
    private String numLote;
    private String codOperacao;
    private String dscRetorno;
    private String nomUsuario;
    
    private LogARDigitalizadoBean[] $Logs;

    public LogARDigitalizadoBean() {
        
        super();
        datProcessamento = new Date();
        nomArquivo = "";
        numCaixa = "";
        numLote = "";
        codOperacao = "";
        dscRetorno = "";
        nomUsuario = "";
    }
        
    public LogARDigitalizadoBean(String nomArquivo, String numCaixa, String numLote, String codOperacao,
            String dscRetorno, String nomUsuario) {
        
        this();        
        this.nomArquivo = nomArquivo;
        this.numCaixa = numCaixa;
        this.numLote = numLote;
        this.codOperacao = codOperacao;
        this.dscRetorno = dscRetorno;
        this.nomUsuario = nomUsuario;
    }
    
    public Date getDatProcessamento() {
        return datProcessamento;
    }
    public void setDatProcessamento(Date datProcessamento) {
        this.datProcessamento = datProcessamento;
    }
    public String getDatProcessamentoExt() {
        String extenso = "";
        if (datProcessamento != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            extenso = df.format(datProcessamento);
        }
        return extenso;
    }
    
    public String getDscRetorno() {
        return dscRetorno;
    }
    public void setDscRetorno(String dscRetorno) {
        this.dscRetorno = dscRetorno;
    }
    
    public String getNomArquivo() {
        return nomArquivo;
    }
    public void setNomArquivo(String nomArquivo) {
        this.nomArquivo = nomArquivo;
    }
    
    public String getNumCaixa() {
        return numCaixa;
    }
    public void setNumCaixa(String numCaixa) {
        this.numCaixa = numCaixa;
    }
    
    public String getNumLote() {
        return numLote;
    }
    public void setNumLote(String numLote) {
        this.numLote = numLote;
    }    
    
    public String getNomUsuario() {
        return nomUsuario;
    }
    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }
    
    public String getCodOperacao() {
        return codOperacao;
    }
    public void setCodOperacao(String codOperacao) {
        this.codOperacao = codOperacao;
    }
    public String getCodOperacaoExt() {        
        String extenso = ""; 
        if (codOperacao.equals("I"))
            extenso = "Inserido";
        else if (codOperacao.equals("S"))
            extenso = "Substituido";
        else if (codOperacao.equals("R"))
            extenso = "Recusado";            
        return extenso;
    }

    public void grava() throws DaoException {
        DaoDigit.getInstance().GravaLogARDigitalizado(this);
    }
    
    public void carrega(Date datInicial, Date datFinal) throws DaoException {        
        $Logs = DaoDigit.getInstance().ConsultaLogARDigitalizados(datInicial, datFinal);
    }
    public LogARDigitalizadoBean[] get$Logs() {
        return $Logs;
    }
    
}