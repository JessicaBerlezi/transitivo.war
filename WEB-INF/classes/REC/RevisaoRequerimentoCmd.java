package REC;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>      Recurso - Revis�o de Requerimento<br>
* <b>Description:</b>Permite Troca de Status do Requerimento 9<br>
* <b>Copyright:</b>  Copyright (c) 2005<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/
public class RevisaoRequerimentoCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/RevisaoRequerimento.jsp";

	public RevisaoRequerimentoCmd() {
		next = jspPadrao;
	}

	public RevisaoRequerimentoCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try { 
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		
			
			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			
			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
			
			RevisaoRequerimentoBean revisaoReqBean = (RevisaoRequerimentoBean) req.getAttribute("RevisaoRequerimentoBeanId");
			if (revisaoReqBean==null) revisaoReqBean = new RevisaoRequerimentoBean() ;
			revisaoReqBean.setJ_sigFuncao(req.getParameter("j_sigFuncao"));
			
			RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
			if(RequerimentoId==null)RequerimentoId = new RequerimentoBean();
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) 
				acao = " ";

			if(acao.equals("Novo")) 
				AutoInfracaoBeanId = new AutoInfracaoBean() ;
			
			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.setMsgOk("S");
			    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			    revisaoReqBean.setEventoOKRevisao(AutoInfracaoBeanId);
			}
			
		    if(acao.equals("LeReq"))  {
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    	revisaoReqBean.setEventoOKRevisao(AutoInfracaoBeanId) ;
		    	RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
		        if (RequerimentoId.getCodRequerimento().length()!=0) {
					RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
					if (RequerimentoId.getDatEST().length()==0) RequerimentoId.setDatEST(sys.Util.formatedToday().substring(0,10)) ;			
		        }
		    }
		

			if (acao.equals("alterarStatusReq")) { 
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
					
					String data = req.getParameter("data");				  
					if(data == null) data= "";
					
					String codRequerimento = req.getParameter("codRequerimento");				  
					if(codRequerimento == null) codRequerimento= "";
					
					String txtMotivo = req.getParameter("txtMotivo");				  
					if(txtMotivo == null) txtMotivo= "";
					
					//AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
					
					if(!"".equals(data)){
						AutoInfracaoBeanId.LeRequerimento(UsrLogado);
						Iterator it = AutoInfracaoBeanId.getRequerimentos().iterator();
				  		REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
				  		while (it.hasNext()) {
				  			reqIt =(REC.RequerimentoBean)it.next();
				  			if ( (revisaoReqBean.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
				  			     (revisaoReqBean.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
				  				 (reqIt.getCodRequerimento().equals(codRequerimento)) ) { 
				  				  RequerimentoId.copiaRequerimento(reqIt);					   
								  break;
								  
				  			}
				  		}
				  		
				  		RequerimentoId.setNomUserNameEST(UsrLogado.getNomUserName());
						RequerimentoId.setCodOrgaoLotacaoEST(UsrLogado.getOrgao().getCodOrgao());
						RequerimentoId.setDatProcEST(sys.Util.formatedToday().substring(0,10));
						RequerimentoId.setDatEST(data);
						RequerimentoId.setTxtMotivoEST(txtMotivo);
						AutoInfracaoBeanId.setCodStatus(revisaoReqBean.getStatusTPCVValido());
						
					    atualizarReq(revisaoReqBean,AutoInfracaoBeanId,RequerimentoId,UsrLogado);
					    
						if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
							AutoInfracaoBeanId.setMsgErro("Revis�o do Requerimento "+RequerimentoId.getNumRequerimento()+" realizada com sucesso.") ;
						}
						
					}
				}
			}
			
		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
		session.setAttribute("UsuarioBeanId",UsrLogado) ;
		session.setAttribute("UsuarioFuncBeanId",UsuarioFuncBeanId) ;
		req.setAttribute("RevisaoRequerimentoBeanId",revisaoReqBean) ;	
		req.setAttribute("RequerimentoId",RequerimentoId) ;
		
	}
	catch (Exception ue) {
		throw new sys.CommandException("AlteraVencAutoCmd: " + ue.getMessage());
	}		
	return nextRetorno;
}
	
	 private void atualizarReq(RevisaoRequerimentoBean revisaoReqBean,AutoInfracaoBean myAuto,RequerimentoBean myReq,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
	  	try { 	
	  		// preparar Bean de Historico
	  		HistoricoBean myHis = new HistoricoBean() ;
	  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
	  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
	  		myHis.setNumProcesso(myAuto.getNumProcesso());
	  		myAuto.setCodStatus(myAuto.getCodStatus());  
	  		myHis.setDatStatus(myAuto.getDatStatus());
		    myHis.setCodEvento(revisaoReqBean.getCodEvento());
			myHis.setCodOrigemEvento(revisaoReqBean.getOrigemEvento());
	  		myHis.setNomUserName(myReq.getNomUserNameEST());
	  		myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoEST());
	  		myHis.setDatProc(myReq.getDatProcEST());
	  		myHis.setTxtComplemento01(myReq.getNumRequerimento());	
	  		myHis.setTxtComplemento02(myReq.getCodTipoSolic());	
	  		myHis.setTxtComplemento03(myReq.getTxtMotivoEST());
	  	    DaoBroker dao = DaoBrokerFactory.getInstance();
	  	    dao.atualizarAuto264(myReq,myHis,myAuto,UsrLogado) ;
	  	    
	  	    // Exclui processo da guia, se o mesmo estiver vinculado a uma
	  	    EstornoAbrirRecursoBean EstornoAbrirRecursoId = new EstornoAbrirRecursoBean();
	  	    EstornoAbrirRecursoId.removeAutoGuia(myAuto,myReq);
	  	    
	  	}// fim do try
	  	catch (Exception ex) {
	  		throw new sys.CommandException(ex.getMessage());
	  	}
	  }	
	 
	 

}