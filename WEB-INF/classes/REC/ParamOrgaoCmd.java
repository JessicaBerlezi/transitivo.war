package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
 * <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Orgao<br>
 * <b>Description:</b>  Comando para Manter os Parametros por Orgao<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Sergio Monteiro
 * @version 1.0
 */

public class ParamOrgaoCmd extends sys.Command {
	
	private static final String jspPadrao = "/REC/ParamOrgao.jsp";
	
	public ParamOrgaoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		try {
			// cria os Beans do Usuario, se n�o existir
			ACSS.OrgaoBean OrgId = (ACSS.OrgaoBean) req.getAttribute("OrgId");
			if (OrgId == null)
				OrgId = new ACSS.OrgaoBean();
			
			// Pegar a sigla da fun��o para ver o grupo de par�metros
			String sigFuncao = req.getParameter("j_sigFuncao");
			if (sigFuncao == null)
				sigFuncao = " ";
			
			String codGrupo = "7";
			
			if (sigFuncao.equals("REC0961")) codGrupo = "1";  // DEFESA PR�VIA
			if (sigFuncao.equals("REC0962")) codGrupo = "2";  // PROCESSOS
			if (sigFuncao.equals("REC0963")) codGrupo = "3";  // 1A. INSTANCIA
			if (sigFuncao.equals("REC0964")) codGrupo = "4";  // 2A. INSTANCIA
			if (sigFuncao.equals("REC0965")) codGrupo = "7";  // GERAL
			if (sigFuncao.equals("REG0857")) codGrupo = "5";  // ARQUIVOS ISOLADAS
			if (sigFuncao.equals("REG0858")) codGrupo = "6";  // ARQUIVOS DOL
			
			
			/* ***********************************************************************
			 * ************************ PONTUACAO (PNT) ****************************** 
			 * ***********************************************************************/ 
			if(sigFuncao.equals("PNT0960"))	 codGrupo = "12";  // ARQUIVOS (PONTUACAO)
			if(sigFuncao.equals("PNT0970"))	 codGrupo = "14";  // RECURSOS (PONTUACAO)
			if(sigFuncao.equals("PNT0980"))	 codGrupo = "15";  // EDITAL   (PONTUACAO)			
			if(sigFuncao.equals("PNT0985"))	 codGrupo = "16";  // GERAL    (PONTUACAO)
			/* ***********************************************************************
			 * ************************* FIM PONTUACAO ******************************** *
			 * ***********************************************************************/ 
			
			OrgId.setCodGrupo(codGrupo);
			
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			if (acao == null)
				acao = " ";
			
			String codOrgao = req.getParameter("codOrgao");
			if (codOrgao == null)
				codOrgao = "";
			
			String atualizarDependente = req.getParameter("atualizarDependente");
			if (atualizarDependente == null)
				atualizarDependente = "N";
			
			OrgId.Le_Orgao(codOrgao, 0);
			OrgId.setAtualizarDependente(atualizarDependente);
			
			if ("buscaOrgao".indexOf(acao) >= 0) {
				OrgId.getParametros(20, 6);
				OrgId.setAtualizarDependente("S");
			}
			
			if (acao.compareTo("Copiar") == 0) {
				String codOrgaoCp = req.getParameter("codOrgaoCp");
				ParamOrgBean paramId = new ParamOrgBean();
				paramId.setCodOrgaoCp(codOrgaoCp);
				paramId.setCodGrupo(codGrupo);
				paramId.CopiaParamOrgao(OrgId, paramId);
			}
			
			Vector vErro = new Vector();
			
			if (acao.compareTo("A") == 0) {
				String[] nomParametro = req.getParameterValues("nomParametro");
				if (nomParametro == null)
					nomParametro = new String[0];
				
				String[] nomDescricao = req.getParameterValues("nomDescricao");
				if (nomDescricao == null)
					nomDescricao = new String[0];
				
				String[] codParametro = req.getParameterValues("codParametro");
				if (codParametro == null)
					codParametro = new String[0];
				
				String[] numOrdem = req.getParameterValues("numOrdem");
				if (numOrdem == null)
					numOrdem = new String[0];
				
				String[] valParametro = req.getParameterValues("valParametro");
				if (valParametro == null)
					valParametro = new String[0];
				
				vErro =	isParametros(nomParametro,nomDescricao,codParametro,valParametro,numOrdem);
				if (vErro.size() == 0) {
					Vector param = new Vector();
					for (int i = 0; i < nomParametro.length; i++) {
						ParamOrgBean myParam = new ParamOrgBean();
						myParam.setNomParam(nomParametro[i]);
						myParam.setNomDescricao(nomDescricao[i]);
						myParam.setCodOrgao(codOrgao);
						myParam.setCodParametro(codParametro[i]);
						myParam.setValParametro(valParametro[i]);
						myParam.setNumOrdem(numOrdem[i]);
						param.addElement(myParam);
					}
					OrgId.setParametros(param);
					OrgId.isInsertParametros();
					OrgId.getParametros(20, 6);
				}
				else
					OrgId.setMsgErro(vErro);
			}
			req.setAttribute("OrgId", OrgId);
		}
		catch (Exception ue) {
			throw new sys.CommandException(
					"ParamOrgaoCmd 001: " + ue.getMessage());
		}
		return nextRetorno;
	}
	private Vector isParametros(String[] nomParametro, String[] nomDescricao,
			String[] codParametro,String[] valParametro,String[] numOrdem) {
		Vector vErro = new Vector();
		
		if ((valParametro.length != nomDescricao.length)
				|| (nomParametro.length != nomDescricao.length)
				|| (nomDescricao.length != codParametro.length)
				|| (numOrdem.length != nomDescricao.length))
			vErro.addElement("Parametros inconsistentes");
		return vErro;
	}
}