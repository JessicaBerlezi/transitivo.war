package REC;



/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
* <b>Description:</b>  Abrir Recurso Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class AbrirRecurso2aBean   extends AbrirRecBean   { 

  public AbrirRecurso2aBean()  throws sys.BeanException {

  super() ;	
  }
  
//--------------------------------------------------------------------------  
  public void setEventoOK(ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)  throws sys.BeanException  {
	if ("".equals(myAuto.getNomStatus())) {
		setMsg("Status: "+myAuto.getCodStatus()+" n�o cadastrado na Tabela.");
		setEventoOK("N");	
	}
	else {
  	  setMsg(myAuto.getNomStatus()+" (Data do Status: "+myAuto.getDatStatus()+")");
	  String complMsg = "" ;
	  try {
		  	setEventoOK("S");	  
		  // Status 
		  if ( (myAuto.getCodStatus().length()>1 ) && (myParam.getParamOrgao("STATUS_ABRIR_2INSTANCIA","30,35,36","4").indexOf(myAuto.getCodStatus())<0) )   
			{
		  	setEventoOK("N");
		  	complMsg = " - N�o pode entrar com Recurso de 2a Inst�ncia";
		  }
		      
		  // Status 30,35,36	
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_2INSTANCIA","30,35,36","4").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) {
		  	complMsg = " - Entrar com Recurso de 2a.Inst�ncia";
		  	setNFuncao("RECURSO 2a INST�NCIA") ;
		  	/*Michel
		  	setTpSolDP(getDP("2P",myParam,myAuto));
		  	setTpSolDC(getDC("2C",myParam,myAuto));
		  	*/
			setTpSolDP(getDP("2P",myParam,myAuto));		  	
			if ((!myAuto.getProprietario().getNomResponsavel().trim().equals(myAuto.getCondutor().getNomResponsavel().trim())) && (myAuto.getInfracao().getIndTipo().equals("C")) )
			  setTpSolDC(getDC("2C",myParam,myAuto));			  							
		  }		
		  if ( ("".equals(getTpSolDP())) && ("".equals(getTpSolDC())) ) {
			  setEventoOK("N");
			  complMsg = " - Nenhuma solicita��o efetuada.";
		  }		  
		  setMsg(getMsg() + complMsg) ;
	 }
	 catch (Exception ue) {
       throw new sys.BeanException("AbrirRecurso2aBean: " + ue.getMessage());
	 }
	 
   }
  }   
						
//--------------------------------------------------------------------------  
}
