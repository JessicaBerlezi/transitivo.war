package REC;

import java.util.Iterator;

public class AbrirRecBean    extends EventoBean     { 
	
	private String tpSolDP;  
	private String tpSolDC;  
	private String msgDP;  
	private String msgDC;  
	
	private String txtEMail;    
	private String txtMotivo;
	private String datEntrada;    
	
	//Folha de Rosto
	private String nomeImagem;
	private String alturaImageGra;
	private String larguraImageGra;
	private String alturaImagePeq;
	private String larguraImagePeq;
	private String observacao;
	private String sigProtocolo;
	
	
	public AbrirRecBean()  throws sys.BeanException {
		
		super() ;	
		tpSolDP    = "";  
		tpSolDC    = "";  
		msgDP      = "";  
		msgDC      = "";  
		datEntrada = sys.Util.formatedToday().substring(0,10) ;  
		txtEMail   = "";  
		txtMotivo  = "";
		alturaImageGra = "";
		larguraImageGra = "";
		alturaImagePeq = "";
		larguraImagePeq = "";
		nomeImagem = "";
		observacao = "";
		sigProtocolo = "";
		
	}
	
	
	public String getNomTpSol(String tpSol)   {
		String nom="";
		if ("TR".equals(tpSol)) nom="Troca de Real Infrator";
		if ("DP".equals(tpSol)) nom="Defesa Pr�via - Propriet�rio";	
		if ("DC".equals(tpSol)) nom="Defesa Pr�via - Condutor";	
		if ("1P".equals(tpSol)) nom="Rec 1a Inst�ncia - Propriet�rio";	
		if ("1C".equals(tpSol)) nom="Rec 1a Inst�ncia - Condutor";					
		if ("2P".equals(tpSol)) nom="Rec 2a Inst�ncia - Propriet�rio";	
		if ("2C".equals(tpSol)) nom="Rec 2a Inst�ncia - Condutor";					
		
		return nom;
	} 
	
//	--------------------------------------------------------------------------  
	public void setTpSolDP(String tpSolDP)  {
		if (tpSolDP==null) tpSolDP=" " ;
		this.tpSolDP = tpSolDP;
	}
	public String getTpSolDP()  {
		return this.tpSolDP;
	}
//	--------------------------------------------------------------------------  
	public void setTpSolDC(String tpSolDC)  {
		if (tpSolDC==null) tpSolDC=" " ;
		this.tpSolDC = tpSolDC;
	}
	public String getTpSolDC()  {
		return this.tpSolDC;
	}
//	--------------------------------------------------------------------------  
	public void setMsgDP(String msgDP)  {
		if (msgDP==null) msgDP=" " ;
		this.msgDP = msgDP;
	}
	public String getMsgDP()  {
		return this.msgDP;
	}
//	--------------------------------------------------------------------------  
	public void setMsgDC(String msgDC)  {
		if (msgDC==null) msgDC=" " ;
		this.msgDC = msgDC;
	}
	public String getMsgDC()  {
		return this.msgDC;
	}
//	--------------------------------------------------------------------------  
	public String getDP(String tpSol,ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)    throws sys.BeanException {
		String dp =tpSol;
		try {
			this.msgDP="";
			dp = DefPreviaOk(myAuto,tpSol,"P") ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return dp;
	}
//	--------------------------------------------------------------------------  
	public String getDC(String tpSol,ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)    throws sys.BeanException {
		String dc =tpSol;
		try {
			this.msgDC="";
			// Condutor nao identificado
			if ( "1".equals(myAuto.getIndCondutorIdentificado())==false ) {		
				// proprietario igual ao condutor
				if ( (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())) ){
					this.msgDC = "Condutor igual ao Propriet�rio" ;
					dc = "" ;					 		
				}
				// proprietario nao e condutor
				else {
					// Infracao de proprietario
					if ( ("P".equals(myAuto.getInfracao().getIndTipo())) ){
						dc = "" ;
						this.msgDC = "Infra��o de Propriet�rio." ;			
					}
					else {
						dc = DefPreviaOk(myAuto,tpSol,"C") ;							
					}	
				}
			}
			// Condutor Identificado	
			else {
				// proprietario nao e o condutor
				if (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())==false) 	{
					// Infracao de Proprietario 	
					if ("P".equals(myAuto.getInfracao().getIndTipo())) {
						dc = "" ;			
						this.msgDC = "Infra��o de Propriet�rio" ;
					}
					else dc = DefPreviaOk(myAuto,tpSol,"C") ;				
				}
				else {
					this.msgDC = "Condutor igual ao Propriet�rio" ;
					dc = "" ;					 
				}
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return dc;
	}
//	--------------------------------------------------------------------------  
	public String DefPreviaOk(REC.AutoInfracaoBean myAuto,String tpSol,String cond_prop)   throws sys.BeanException {
		String d =tpSol;
		try {
			ResponsavelBean myResp ;
			if ("P".equals(cond_prop)) myResp = myAuto.getProprietario() ;
			else myResp = myAuto.getCondutor() ;
			Iterator it = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean req  = new REC.RequerimentoBean();
			while (it.hasNext()) {
				req =(REC.RequerimentoBean)it.next() ;
				// verifica se e deste responsavel
				if ( (myResp.getNomResponsavel().trim().equals(req.getNomResponsavelDP().trim())) && (tpSol.equals(req.getCodTipoSolic())) &&
						("7,8".indexOf(req.getCodStatusRequerimento())<0) ) {
					if ("P".equals(cond_prop))  this.msgDP =req.getCodTipoSolic()+" interposta em "+req.getDatRequerimento();
					else 						this.msgDC =req.getCodTipoSolic()+" interposta em "+req.getDatRequerimento();
					d="";
					break ;
				}
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		return d;
	}
//	--------------------------------------------------------------------------  
	public void setDatEntrada(String datEntrada)   {
		if (datEntrada==null) datEntrada = sys.Util.formatedToday().substring(0,10) ;
		this.datEntrada=datEntrada ;	
	}
	public String getDatEntrada()   {
		return datEntrada ;	
	}
//	--------------------------------------------------------------------------  
	public void setTxtEMail(String txtEMail)   {
		if (txtEMail==null) txtEMail = "";
		this.txtEMail=txtEMail ;	
	}
	public String getTxtEMail()   {
		return txtEMail ;	
	}
	
//	--------------------------------------------------------------------------  
	public void setTxtMotivo(String txtMotivo)   {
		if (txtMotivo==null) txtMotivo = "";
		this.txtMotivo = txtMotivo ;	
	}
	public String getTxtMotivo()   {
		return txtMotivo ;	
	}
	
//	-------------------------------------------------------------------------- 
	public void setAlturaImageGra(String alturaImageGra) {
		if (alturaImageGra == null) alturaImageGra = "";
		else this.alturaImageGra = alturaImageGra;
	}
	public String getAlturaImageGra() {
		return alturaImageGra;
	}
//	-------------------------------------------------------------------------- 
	public void setNomeImagem(String nomeImagem) {
		if(nomeImagem == null) nomeImagem = "";
		else this.nomeImagem = nomeImagem;
	}
	public String getNomeImagem() {
		return nomeImagem;
	}
//	-------------------------------------------------------------------------- 	
	public void setLarguraImageGra(String larguraImageGra) {
		if(larguraImageGra == null) larguraImageGra = "";
		else this.larguraImageGra = larguraImageGra;
	}
	public String getLarguraImageGra() {
		return larguraImageGra;
	}
//	-------------------------------------------------------------------------- 	
	public void setAlturaImagePeq(String alturaImagePeq) {
		if(alturaImagePeq == null)alturaImagePeq = "";
		else this.alturaImagePeq = alturaImagePeq;
	}
	public String getAlturaImagePeq() {
		return alturaImagePeq;
	}
//	-------------------------------------------------------------------------- 	
	public void setLarguraImagePeq(String larguraImagePeq) {
		if(larguraImagePeq == null) larguraImagePeq = "";
		else this.larguraImagePeq = larguraImagePeq;
	}
	public String getLarguraImagePeq() {
		return larguraImagePeq;
	}
//	-------------------------------------------------------------------------- 	

	public void setObservacao(String observacao) {
		if (observacao == null) observacao = "";
		else this.observacao = observacao;
	}
	public String getObservacao() {
		return observacao;
	}

//	--------------------------------------------------------------------------
	public void setSigProtocolo(String sigProtocolo) {
		if(sigProtocolo == null) sigProtocolo = "";
		else this.sigProtocolo = sigProtocolo;
	}
	public String getSigProtocolo() {
		return sigProtocolo;
	}



	
}





