package REC;

public class ArquivoRequerimentoBean {

	private String codArquivo;
    private String nomeArquivo;
    private String dataRecebimento;
    private String numAnoExercicio;
    //1- Documento; 2- CRLV; 3- Procuração
    private String tipoArquivo;
    
	
	public ArquivoRequerimentoBean() throws sys.BeanException{
		codArquivo = "";
	    nomeArquivo = "";
	    dataRecebimento = "";
	    numAnoExercicio = "";
	    tipoArquivo = "";
	}


	public String getCodArquivo() {
		return codArquivo;
	}


	public void setCodArquivo(String codArquivo) {
		this.codArquivo = codArquivo;
	}


	public String getNomeArquivo() {
		return nomeArquivo;
	}


	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}


	public String getDataRecebimento() {
		return dataRecebimento;
	}


	public void setDataRecebimento(String dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}


	public String getNumAnoExercicio() {
		return numAnoExercicio;
	}


	public void setNumAnoExercicio(String numAnoExercicio) {
		this.numAnoExercicio = numAnoExercicio;
	}


	public String getTipoArquivo() {
		return tipoArquivo;
	}


	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}
	
}
