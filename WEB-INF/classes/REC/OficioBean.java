package REC;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
* <b>Description:</b>  Abrir Recurso Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha
* @version 1.0
*/

public class OficioBean    extends AutoInfracaoBean     { 

  public final String CADASTRA_PROCESSO = "PROC";   
  public final String ABRE_RECURSO = "0";
  public final String DEFINE_RELATOR = "2";
  public final String INFORMA_RESULTADO = "3";
  public final String ENVIA_DO = "4";
  public final String INFORMA_DATA_PUB = "5";
  public final String FIM_OPERACAO = "FIM";
  public final String CONCLUIDO = "9";
  
  public String etapa;
  public String tipoRecurso;
  
  public OficioBean()  throws sys.BeanException {
    super() ;	
	etapa = "";
	tipoRecurso = "";
  }
  
  public void setEtapaAuto(String etapa) {
		this.etapa = etapa;
		if (etapa == null)
			this.etapa = "";
  }
  public String getEtapaAuto() {
		return this.etapa;
  }
  
  public void setTipoRecurso(String tipoRecurso) {
	//Tipos de Recurso: DP, 1�, 2�
	if (tipoRecurso == null)
		this.tipoRecurso = "";
	else if (!tipoRecurso.equals("")){
		if(tipoRecurso.length()>2) tipoRecurso = tipoRecurso.substring(0,3).trim();
		else this.tipoRecurso = tipoRecurso;
	}
  }       
  
  public String getTipoRecurso() {
		return this.tipoRecurso;
  }

  
//-------------------- Verifica Fim de Opera��o -----------------------------------
  public String verificaStatusFim(AutoInfracaoBean myAuto, ACSS.UsuarioBean UsrLogado, 
   		RequerimentoBean myReq) throws sys.CommandException{
  	    Vector fimOperacao = new  Vector();
  	    String etapaFim = "";
		String temTR = "";
		String temPecunia = "";
		String temResPontos = "";
		try {
	 	 	myAuto.LeAutoInfracao(UsrLogado);
		 	myAuto.LeRequerimento(UsrLogado);  
		 	 //Verifica se o auto sofreu ajuste manual 413/415
        	myAuto.LeHistorico(UsrLogado);
			Iterator itx  = myAuto.getHistoricos().iterator() ;
			REC.HistoricoBean hist = new REC.HistoricoBean();
			if 	(itx.hasNext()){
				while (itx.hasNext()) {
					hist =(REC.HistoricoBean)itx.next() ;
					if ("413".equals(hist.getCodEvento())== true){
						temResPontos = "S"; 
						break;
					} 
					if ("415".equals(hist.getCodEvento())== true){
						temPecunia = "S"; 
						break;
					}	
				 }
			}
			
	        //Verifica se o auto est� no fim
			if("0,1,2,4,5".indexOf(myAuto.getCodStatus())>0){ 
			 	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de DP ,DC, TR com status 3,9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("TR,DP,DC".indexOf(requerimento.getCodTipoSolic())>=0) && ("3,9".indexOf(requerimento.getCodStatusRequerimento())>=0)){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        	if ("TR".indexOf(requerimento.getCodTipoSolic())>=0)
			        		temTR = "S";
			        }
				}
				if ((fimOperacao.size()==3 && temTR.equals("S"))
					||(fimOperacao.size()==2 && temResPontos.equals("S"))) etapaFim = CONCLUIDO;
	        	else if (fimOperacao.size()== 1 && (temTR.equals("")&& temPecunia.equals("S"))) etapaFim = CONCLUIDO;
	        	else if (fimOperacao.size()== 1 && (temTR.equals("")&& temResPontos.equals(""))) etapaFim = CONCLUIDO;
	        	
	        } 
			else if (("10,11,12,14,15,16".indexOf(myAuto.getCodStatus())>0)&& myAuto.getCodStatus().length()>1){
	        	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de 1P ,2P, TR com status 9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("1P,1C".indexOf(requerimento.getCodTipoSolic())>=0) && ("9".indexOf(requerimento.getCodStatusRequerimento())>=0)){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        	if ("TR".indexOf(requerimento.getCodTipoSolic())>=0)
			        		temTR = "S";
			        }
				}
	        } 
			else if(("30,35,82".indexOf(myAuto.getCodStatus())>=0 && (myAuto.getCodStatus().length()>1))){
	        	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de 2P ,2P, TR com status 9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("2P,2C".indexOf(requerimento.getCodTipoSolic())>=0) && ("9".indexOf(requerimento.getCodStatusRequerimento())>=0)){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        	if ("TR".indexOf(requerimento.getCodTipoSolic())>=0)
			        		temTR = "S";
			        }
				}
	        }
	        if (("10,11,12,14,15,16,30,35,82".indexOf(myAuto.getCodStatus())>0)&& myAuto.getCodStatus().length()>1){
			    if (fimOperacao.size()==2 && (temPecunia.equals("S")||temResPontos.equals("S"))) etapaFim = CONCLUIDO;
	        	else if (fimOperacao.size()== 1 && (temPecunia.equals("S"))) etapaFim = CONCLUIDO;
	        	else if (fimOperacao.size()== 1 && (temResPontos.equals(""))) etapaFim = CONCLUIDO;
	        }
	    }
		catch (Exception e){
			 throw new sys.CommandException("OficioCmd: " + e.getMessage());
		}
	    return etapaFim ;

  }
  
//-------------------- Verifica quantidadesde autos -----------------------------------
  public String qtdAutos(AutoInfracaoBean myAuto, ACSS.UsuarioBean UsrLogado, 
   		RequerimentoBean myReq) throws sys.CommandException{
	   	ArrayList fimOperacao = new  ArrayList();
  	    String QTDautos = "";
  	    String temPecunia = "";
  	    String temResPontos = "";
		try {
	 	 	myAuto.LeAutoInfracao(UsrLogado);
		 	myAuto.LeRequerimento(UsrLogado);  
		 	
            //Verifica se o auto sofreu ajuste manual 413/415
        	myAuto.LeHistorico(UsrLogado);
			Iterator itx  = myAuto.getHistoricos().iterator() ;
			REC.HistoricoBean hist = new REC.HistoricoBean();
			if 	(itx.hasNext()){
				while (itx.hasNext()) {
					hist =(REC.HistoricoBean)itx.next() ;
					if ("413".equals(hist.getCodEvento())== true){
						temResPontos = "S"; 
						break;
					}
					if ("415".equals(hist.getCodEvento())== true){
						temPecunia = "S"; 
						break;
					}	
				 }
			}
		 	
	        if("0,1,2,4,5".indexOf(myAuto.getCodStatus())>0){ 
			 	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de DP ,DC, TR com status 3,9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("TR,DP,DC".indexOf(requerimento.getCodTipoSolic())>=0)&& (!requerimento.getCodStatusRequerimento().equals("7"))){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        }
				}
	        } else if (("10,11,12,14,15,16".indexOf(myAuto.getCodStatus())>0)&& myAuto.getCodStatus().length()>1){
	        	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de 1P ,2P, TR com status 9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("TR,1P,1C".indexOf(requerimento.getCodTipoSolic())>=0)&& (!requerimento.getCodStatusRequerimento().equals("7"))){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        }
				}
	        } else if(("30,35,82".indexOf(myAuto.getCodStatus())>=0 && (myAuto.getCodStatus().length()>1))){
	        	Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
				//Requerimento de 2P ,2P, TR com status 9
				while (it.hasNext()) {
					requerimento =(REC.RequerimentoBean)it.next() ;
			        if (("TR,2P,2C".indexOf(requerimento.getCodTipoSolic())>=0)&& (!requerimento.getCodStatusRequerimento().equals("7"))){
			        	fimOperacao.add(requerimento.getCodTipoSolic());
			        }
				}
	        }
	        
			if (fimOperacao.size()>=2 ) QTDautos = "2";
			else if (fimOperacao.size()== 1 && temResPontos.equals("S") ) QTDautos = "2"; 
			else if (fimOperacao.size()== 1 && temPecunia.equals("S") ) QTDautos = "1";
			else if (fimOperacao.size()==1 ) QTDautos = "1";
	    }
		catch (Exception e){
			 throw new sys.CommandException("OficioCmd: " + e.getMessage());
		}
	    return QTDautos ;

  }
     
   
//-------------------- Verifica codRequerimento -----------------------------------
  public String verificaCodReq(AutoInfracaoBean myAuto,RequerimentoBean myReq,OficioBean OficioBean, ACSS.UsuarioBean UsrLogado) throws sys.CommandException{
	   	String codReq = "";
	   	String dataRec = "";
	    try {
	    	myAuto.LeRequerimento(UsrLogado);
		   	Iterator it = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
			while (it.hasNext()) {
				requerimento =(REC.RequerimentoBean)it.next() ;
				// Requerimento de DP ou DC com status 0,2,3,4,5
				if ( (OficioBean.getTipoRecurso().indexOf(requerimento.getCodTipoSolic())>=0) &&
					("0,2,3,4,5".indexOf(requerimento.getCodStatusRequerimento())>=0)){
						codReq = requerimento.getCodRequerimento();
			            myReq.setDatRequerimento(requerimento.getDatRequerimento()); 
				}
			}
	    }
		catch (Exception e){
			 throw new sys.CommandException("OficioCmd: " + e.getMessage());
		}
	    return codReq ;

  }
  
//------------------- Verifica em que etapa o auto se encontra -------------------
  
  public void verificaPosicaoAuto(AutoInfracaoBean myAuto, ACSS.UsuarioBean UsrLogado, 
   		RequerimentoBean myReq, Object AbrirRecursoBeanId) throws sys.CommandException { 
 	String statusReq = "";
 	String TipoRecP = "";
 	String TipoRecC = "";
   	try {
   		TipoRecP = (String) AbrirRecursoBeanId.getClass().getMethod("getMsgDP",null).invoke(AbrirRecursoBeanId,null);
        TipoRecC = (String) AbrirRecursoBeanId.getClass().getMethod("getMsgDC",null).invoke(AbrirRecursoBeanId,null);
  	 	myAuto.LeAutoInfracao(UsrLogado);
	 	myAuto.LeRequerimento(UsrLogado); 

	 	//Verifica tipo de solicita��o
        if (!myReq.getNumRequerimento().equals("")||!myReq.getCodRequerimento().equals("")){
            setTipoRecurso(myReq.getCodTipoSolic());
	 	}else{
	        if (!TipoRecP.equals("")) setTipoRecurso(TipoRecP.substring(0,3).trim());
	        if (!TipoRecC.equals("")) setTipoRecurso(TipoRecC.substring(0,3).trim());
	 	}

        //Verifico status do auto
	 	Iterator it = myAuto.getRequerimentos().iterator() ;
		REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
		//Requerimento de DP ou DC com status 0,2,3,4
		while (it.hasNext()) {
			requerimento =(REC.RequerimentoBean)it.next() ;
	        if ((getTipoRecurso().indexOf(requerimento.getCodTipoSolic())>=0) && ("0,2,3,4".indexOf(requerimento.getCodStatusRequerimento())>=0)){
	        	statusReq = requerimento.getCodStatusRequerimento();
				break;
	        }
		}
		//verifico em que posi��o est� o auto
        String fimOperacao = "";
        String QTDautos = "";
        fimOperacao = verificaStatusFim(myAuto,UsrLogado,myReq);
        QTDautos = qtdAutos( myAuto,UsrLogado,myReq);
        if("0,1,2,4,5".indexOf(myAuto.getCodStatus())>0){   
	      if(fimOperacao.equals(CONCLUIDO))
		      setEtapaAuto(FIM_OPERACAO);// auto concluido
        }
        else if (("10,11,12,14,15,16,30,35,82".indexOf(myAuto.getCodStatus())>0)&& myAuto.getCodStatus().length()>1){
          if(fimOperacao.equals(CONCLUIDO))
  		         setEtapaAuto(FIM_OPERACAO);// auto concluido
        }
        
        if (!getEtapaAuto().equals(FIM_OPERACAO)){
	        if (myAuto.getNumProcesso().equals(""))
		 		    setEtapaAuto(CADASTRA_PROCESSO) ; // auto sem processo
        	else if( ( (TipoRecP.equals("")&& QTDautos.equals("1"))
        			||(TipoRecP.equals("")&& !TipoRecC.equals("")&& QTDautos.equals("2"))
					||(!TipoRecP.equals("")&& TipoRecC.equals("")&& QTDautos.equals("2"))
					||(TipoRecP.equals("")&& TipoRecC.equals("")&& QTDautos.equals("2")))
					||statusReq.equals("") ){
		 		    setEtapaAuto(ABRE_RECURSO);// auto sem recurso aberto
		 	}
		 	else if(statusReq.equals(ABRE_RECURSO))
	        	    setEtapaAuto(DEFINE_RELATOR);// auto sem relator definido
	        
		 	else if(statusReq.equals(DEFINE_RELATOR))
	        	    setEtapaAuto(INFORMA_RESULTADO);// auto sem resultado
		    
	 	   else if (("10,11,12,14,15,16,30,35,82".indexOf(myAuto.getCodStatus())>0)&& myAuto.getCodStatus().length()>1){
			 	if(statusReq.equals(INFORMA_RESULTADO))
			 		setEtapaAuto(ENVIA_DO);// auto n�o foi enviado p/ DO
		        
			 	else if(statusReq.equals(ENVIA_DO))     
		        	setEtapaAuto(INFORMA_DATA_PUB) ;// auto sem data da Publicacao
		        
	       }
        }
     }
	 catch (Exception e){
		 throw new sys.CommandException("OficioCmd: " + e.getMessage());
	 }
   }
//------------------- Verifica em que etapa o auto se encontra -------------------
  
  public String getStatusReq(AutoInfracaoBean myAuto, ACSS.UsuarioBean UsrLogado, 
   		RequerimentoBean myReq, Object AbrirRecursoBeanId) throws sys.CommandException { 
 	String statusReq = "";
 	String TipoRecP = "";
 	String TipoRecC = "";
   	try {
   		TipoRecP = (String) AbrirRecursoBeanId.getClass().getMethod("getMsgDP",null).invoke(AbrirRecursoBeanId,null);
        TipoRecC = (String) AbrirRecursoBeanId.getClass().getMethod("getMsgDC",null).invoke(AbrirRecursoBeanId,null);
  	 	myAuto.LeAutoInfracao(UsrLogado);
	 	myAuto.LeRequerimento(UsrLogado); 

	 	//Verifica tipo de solicita��o
        if (!myReq.getNumRequerimento().equals("")||!myReq.getCodRequerimento().equals("")){
            setTipoRecurso(myReq.getCodTipoSolic());
	 	}else{
	        if (!TipoRecP.equals("")) setTipoRecurso(TipoRecP.substring(0,3).trim());
	        if (!TipoRecC.equals("")) setTipoRecurso(TipoRecC.substring(0,3).trim());
	 	}

        //Verifico status do auto
	 	Iterator it = myAuto.getRequerimentos().iterator() ;
		REC.RequerimentoBean requerimento  = new REC.RequerimentoBean();
		//Requerimento de DP ou DC com status 0,2,3,4
		while (it.hasNext()) {
			requerimento =(REC.RequerimentoBean)it.next() ;
	        if ((getTipoRecurso().indexOf(requerimento.getCodTipoSolic())>=0) && ("0,2,3,4".indexOf(requerimento.getCodStatusRequerimento())>=0)){
	        	statusReq = requerimento.getCodStatusRequerimento();
				break;
	        }
		}
     }
	 catch (Exception e){
		 throw new sys.CommandException("OficioCmd: " + e.getMessage());
	 }
	 return statusReq;
   }

  
}
