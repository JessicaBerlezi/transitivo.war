package REC;

import java.sql.Connection;

/**
* <b>Title:</b>        SMIT - MODULO RECURSO<br>
* <b>Description:</b>  Bean de Caixa de Auto de Infra��o Digitalizado<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      MIND Informatica<br>
* @author Glaucio Jannotti
* @version 1.0
*/

public class CaixAIDigitalizadoBean {

	private int codCaixAIDigitalizado;
	private String numCaixa;
	private String numPrateleira;
	private String numEstante;
	private String numBloco;
	private String numRua;
	private String numArea;

	public CaixAIDigitalizadoBean() {

		codCaixAIDigitalizado = 0;
		numCaixa = "";
		numPrateleira = "";
		numEstante = "";
		numBloco = "";
		numRua = "";
		numArea = "";
	}

	public CaixAIDigitalizadoBean(String numCaixa, String numPrateleira, String numEstante, String numBloco,
        String numRua, String numArea) {

		codCaixAIDigitalizado = 0;
		this.numCaixa = numCaixa;
		this.numPrateleira = "";
		this.numEstante = "";
		this.numBloco = "";
		this.numRua = "";
		this.numArea = "";
	}

	public String getNumCaixa() {
		return numCaixa;
	}

	public void setNumCaixa(String string) {
		numCaixa = string;
	}

	public String getNumArea() {
        if (numArea == null) return "";
        else return numArea;
	}

	public String getNumBloco() {
        if (numBloco == null) return "";
        else return numBloco;
	}

	public String getNumEstante() {
        if (numEstante == null) return "";
        else return numEstante;
	}

	public String getNumPrateleira() {
        if (numPrateleira == null) return "";
        else return numPrateleira;
	}

	public String getNumRua() {
        if (numRua == null) return "";
		else return numRua;
	}

	public void setNumArea(String string) {
		numArea = string;
	}

	public void setNumBloco(String string) {
		numBloco = string;
	}

	public void setNumEstante(String string) {
		numEstante = string;
	}

	public void setNumPrateleira(String string) {
		numPrateleira = string;
	}

	public void setNumRua(String string) {
		numRua = string;
	}

	public int getCodCaixAIDigitalizado() {
		return codCaixAIDigitalizado;
	}

	public void setCodCaixAIDigitalizado(int i) {
		codCaixAIDigitalizado = i;
	}
	
	public void grava() throws DaoException {
		DaoDigit.getInstance().GravaCaixAIDigitalizado(this);
	}
    
    public void grava(Connection conn) throws DaoException {
        DaoDigit.getInstance().GravaCaixAIDigitalizado(conn, this);
    }    

}