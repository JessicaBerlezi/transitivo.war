package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Orgao<br>
* <b>Description:</b>  Comando para Manter os Parametros por Orgao<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello
* @version 1.0
*/

public class FeriadoCmd extends sys.Command {

    private static final String jspPadrao="/REC/Feriado.jsp";    
  private String next;

  public FeriadoCmd() {
    next             =  jspPadrao;
  }

  public FeriadoCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	
	String nextRetorno  = jspPadrao ;
    try {  
		HttpSession session = req.getSession();	    	
		// cria os Beans do Usuario, se n�o existir
		REC.FeriadoBean FeriadoId = (REC.FeriadoBean)req.getAttribute("FeriadoId") ;
		if (FeriadoId==null)        FeriadoId = new REC.FeriadoBean() ;	  			
		
		TAB.MunicipioBean MunicipioId = (TAB.MunicipioBean)req.getAttribute("MunicipioId") ;
		if (MunicipioId==null)        MunicipioId = new TAB.MunicipioBean() ;
		
		ACSS.ParamSistemaBean parFer = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
				if (parFer == null)	parFer = new ACSS.ParamSistemaBean();
		
		String j_sigFuncao = req.getParameter("j_sigFuncao");		
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		    if(acao==null)               acao =" ";   
	  String codMunicipio = req.getParameter("codigomunicipio"); 		
		    if(codMunicipio==null)       codMunicipio ="0";
				
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		    if(atualizarDependente==null)atualizarDependente ="N";		
				
		    if(!"REC0994".equals(j_sigFuncao)) 
		    atualizarDependente ="S";
		    
		String nomMunicipio = req.getParameter("dscMun"); 
				if(nomMunicipio==null)       nomMunicipio ="";
				
		String datFeriado = req.getParameter("datFeriado"); 
				if(datFeriado==null)         datFeriado ="";		
				
		String txtFeriado = req.getParameter("txtFeriado"); 
				if(txtFeriado==null)         txtFeriado ="";			
						
		String codmunferiado = req.getParameter("codmunferiado"); 
				if(codmunferiado==null)      codmunferiado ="";											
		   		 
		String tipoFeriado = "";
		String CodUF = 	"";		 
		
		CodUF= (parFer.getParamSist("UF_FERIADO"));	
		if ("REC0994".equals(j_sigFuncao))		
		    tipoFeriado ="M";
		else if ("REC0992".equals(j_sigFuncao))
		    tipoFeriado ="N";
		else     
		    tipoFeriado ="E";  
		  
		  FeriadoId.setCodUF(CodUF);  
		  FeriadoId.setTipoFeriado(tipoFeriado);
	    FeriadoId.Le_Feriado(codMunicipio,0) ;
		  FeriadoId.setAtualizarDependente(atualizarDependente);	 
			
				
		if (acao.equals(" ")){			
			FeriadoId.setTxtFeriado(" ");	  				    
			FeriadoId.setDatFeriado(" ") ;				 
			FeriadoId.setCodMunicipio(" ");
			FeriadoId.setNomMunicipio(" ");
			FeriadoId.setTipoFeriado(tipoFeriado);
		}
		
		if ("buscaMunicipio".indexOf(acao)>=0) {
	      FeriadoId.getFeriados(21,3) ;
		  FeriadoId.setAtualizarDependente("S");		    		  		  	
	    }	  	    
		
//		Crio os beans se n�o Houver

			  FeriadoId.setCodMunicipio(codMunicipio);
			  FeriadoId.setDatFeriado(datFeriado);
			  FeriadoId.setNomMunicipio(nomMunicipio);
			  FeriadoId.setCodMunFeriado(codmunferiado);
		    FeriadoId.setTipoFeriado(tipoFeriado);
		  	
	    
		  Vector vErro =new Vector(); 
	    if(acao.compareTo("A")==0){			         
			String[] TxtFeriado    = req.getParameterValues("txtFeriado");
			if(TxtFeriado==null)     TxtFeriado = new String[0];
			         
			String[] DatFeriado    = req.getParameterValues("datFeriado");
			if(DatFeriado==null)     DatFeriado = new String[0];			
			
			String[] CodMunFeriado = req.getParameterValues("codmunFeriado");
			if(CodMunFeriado==null)  CodMunFeriado = new String[0]; 
			 
			String[] CodMunicipio  = req.getParameterValues("codMunicipio");
			if(CodMunicipio==null)   CodMunicipio = new String[0]; 
            
			vErro      = isFeriados(TxtFeriado,DatFeriado) ;

			if (vErro.size()==0) {
			
				Vector param = new Vector(); 
				  for (int i = 0; i<TxtFeriado.length;i++) {
				 
					FeriadoBean myFeriado = new FeriadoBean() ;			
				 				
					myFeriado.setTxtFeriado(TxtFeriado[i]);						  				    
				    myFeriado.setDatFeriado(DatFeriado[i]) ;				  	 
					myFeriado.setCodMunicipio(codMunicipio);	
					myFeriado.setCodMunFeriado(CodMunFeriado[i]);
					myFeriado.setNomMunicipio(nomMunicipio);
					myFeriado.setTipoFeriado(tipoFeriado);
					myFeriado.setCodUF(CodUF);				
					
					param.addElement(myFeriado) ; 					
				 
				}
				 
					FeriadoId.setFeriados(param) ;
					FeriadoId.isInsertFeriados() ;
				  FeriadoId.getFeriados(21,3) ;
				  FeriadoId.setNomMunicipio(req.getParameter("dscMun"));						
			}
			else FeriadoId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
	    	req.setAttribute("feriados",FeriadoId.getFeriados(0,0));
	    	nextRetorno = "/REC/FeriadoImp.jsp";
	    }
	    
		req.setAttribute("FeriadoId",FeriadoId) ;	
		req.setAttribute("MunicipioId",MunicipioId) ;				  	 
		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("FeriadoCmd001: " + ue.getMessage());
    }
	return nextRetorno;
}

private Vector isFeriados(String[] CodMunicipio,String[] TxtFeriado) {
		Vector vErro = new Vector() ;
		if ((CodMunicipio.length!=TxtFeriado.length)) vErro.addElement("Feriados inconsistentes") ;
		return vErro ;
  }

}