package REC;


import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Evento<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Eventos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class EventoCmd extends sys.Command {

  private static final String jspPadrao="/REC/Evento.jsp";    
  private String next;

  public EventoCmd() {
    next = jspPadrao;
  }

  public EventoCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do Usuario, se n�o existir
		TAB.EventoBean EventoId = (TAB.EventoBean)req.getAttribute("EventoId") ;
		if(EventoId == null) EventoId = new TAB.EventoBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null) acao =" ";   
		  
	   
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null || atualizarDependente.equals(""))
		  atualizarDependente ="N";  

		EventoId.setAtualizarDependente(atualizarDependente);
			 
		EventoId.getEvento(20,5) ;

		Vector vErro = new Vector(); 
					 
	    if(acao.compareTo("A") == 0){
			String[] codEvento= req.getParameterValues("codEvento");
			if(codEvento == null) codEvento = new String[0];  
			  
			String[] dscEvento = req.getParameterValues("dscEvento");
			if(dscEvento == null)  dscEvento = new String[0];         
    
			  
			vErro = isParametros(codEvento,dscEvento) ;
						 
            if (vErro.size()==0) {
				Vector evento = new Vector(); 
				for (int i = 0; i < dscEvento.length;i++) {
					TAB.EventoBean myEvento = new TAB.EventoBean() ;
					myEvento.setNomEvento(dscEvento[i]);
					myEvento.setCodEvento(codEvento[i]);	  
					evento.addElement(myEvento) ; 					
				}
				EventoId.setEvento(evento) ;
				EventoId.isInsertEvento() ;
			    EventoId.getEvento(20,5) ;						
			}
			else EventoId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
	    	req.setAttribute("eventos",EventoId.getEvento(0,0));
	    	nextRetorno = "/REC/EventoImp.jsp";
	    }
	    
		req.setAttribute("EventoId",EventoId) ;		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("JuntaCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  private Vector isParametros(String[] codEvento,String[] dscEvento) {
		Vector vErro = new Vector() ;		 
		if (dscEvento.length!=codEvento.length) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 
}


