package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;

public class AbrirRecurso1aCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/AbrirRecurso1a.jsp" ;  
   
  public AbrirRecurso1aCmd() 
  {  
    next             =  jspPadrao;
  }

  public AbrirRecurso1aCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  	
	    
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	  	// cria os Beans, se n�o existir
	  	AbrirRecurso1aBean AbrirRecursoBeanId     = (AbrirRecurso1aBean)req.getAttribute("AbrirRecursoBeanId") ;
	  	if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecurso1aBean() ;	  			
		        
		String sigFuncao = req.getParameter("j_sigFuncao");
		if (sigFuncao==null) sigFuncao="";      
		
	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  	AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
		if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
			try{
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
				AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			}catch (Exception e) {
				nextRetorno = "/REC/AbrirRecurso1a.jsp";
			}
			
		}	
		if  (acao.equals("AbreRecurso"))  {
		  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			nextRetorno = processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBeanId,sigFuncao);
		}
		
		if (acao.equals("Imprimehistorico")){
            //Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
			String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAuto"));
			AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
		    nextRetorno = "/REC/consultaHistImp.jsp" ;
		}

  		// processamento de saida dos formularios
	  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	  	req.setAttribute("AbrirRecursoBeanId",AbrirRecursoBeanId) ;		
    }
    catch (Exception ue) {
	      throw new sys.CommandException("AbrirRecurso1aCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }

  protected String processaDefesa(HttpServletRequest req,ParamOrgBean ParamOrgaoId,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado,REC.AbrirRecurso1aBean AbrirRecursoBeanId,String sigFuncao) throws sys.CommandException { 
	 	String nextRetorno=jspPadrao;		
	 	try {
	 		myAuto.setMsgErro("") ;
 			Vector vErro = new Vector() ;
 			String datEntrada = req.getParameter("datEntrada");
 			
 			String cdAta = req.getParameter("cdAta");
 			String txtAta = req.getParameter("dsAta");
 			Ata ata = new Ata();
 			
 			if(cdAta.length()>0 && cdAta != null){
 				ata.setCodAta(cdAta);
 			}
 			if(txtAta.length()>0 && txtAta != null){
 				ata.setDescAta(txtAta);
 			}
 			
			
	 		if (datEntrada==null) datEntrada = "";	
 			//validar data de entrada
	 		if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),datEntrada)>0) 
	 			vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;
			// validar decurso de prazo
			else {
				if ("REC0390".equals(sigFuncao)==false) 
				{
					/*Caso notiticacao sem sucesso, n�o criticar prazo*/
					boolean bNotificacao=true;
					if ( (myAuto.getCodStatus().equals("2")) ||(myAuto.getCodStatus().equals("12")) )
					  bNotificacao=false;
					myAuto.setDatLimiteRecurso(ParamOrgaoId);	
					if ( (sys.Util.DifereDias(myAuto.getDatLimiteRecursoFolga(),datEntrada)>0) && (bNotificacao) )
						vErro.addElement("Prazo limite: "+myAuto.getDatLimiteRecurso() + "\n") ;
				}	
			}			
	 		// ler radio - DC - DP 
	 		String solicDEF = req.getParameter("SolicDPBox");
			if (solicDEF==null) solicDEF="";
			if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
			if (solicDEF==null) solicDEF="";
			if (solicDEF.length()==0) vErro.addElement("Nenhuma solicita��o efetuada.\n") ;
 			if ( (solicDEF.equals("1C")) && ("".equals(AbrirRecursoBeanId.getTpSolDC())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDC()+" \n") ;
			if ( (solicDEF.equals("1P")) && ("".equals(AbrirRecursoBeanId.getTpSolDP())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDP()+" \n") ;
			String txtEMail = req.getParameter("txtEMail");
			if (txtEMail==null) txtEMail = "";
			String txtMotivo = req.getParameter("txtMotivo");
			if (txtMotivo == null) txtMotivo = "";
			
			if (vErro.size()>0) {
				 myAuto.setMsgErro(vErro) ;
				 nextRetorno = this.next ;
			}
			else {
				REC.RequerimentoBean myReq = new REC.RequerimentoBean() ;			
				myAuto.setMsgErro(atualizarAuto(req,myAuto,ParamOrgaoId,UsrLogado,myReq,datEntrada,txtEMail,solicDEF,txtMotivo, ata)) ;	
				if (myAuto.getMsgErro().length()==0) {
					if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) { 	
						myAuto.setMsgErro("Interposto recurso de 1a. Inst�ncia com Requerimento:"+myReq.getNumRequerimento()) ;
						myAuto.setMsgOk("S") ;
					}
					else if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2").indexOf("R")>=0){
						AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
						String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setNomeImagem(nomeImagem);

                        String alturaImageGra = "80";
                        AbrirRecBeanId.setAlturaImageGra(alturaImageGra);
                        
                        String larguraImageGra = "175";
                        AbrirRecBeanId.setLarguraImageGra(larguraImageGra);
                        
                        String alturaImagePeq = "80";
                        AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);
                        
                        String larguraImagePeq = "175";
                        AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);
                        
                        String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setObservacao(obs);
                        
                        String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setSigProtocolo(siglaProt);
                      
					    nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
					    req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	
					}
				    else
						nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;	
					//Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
					String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
				    if (!codEventoFiltro.equals("N")){
				       myAuto.filtraHistorico(myAuto,codEventoFiltro);
				    }
				    req.setAttribute("codEventoFiltro",codEventoFiltro);
				}								
			}
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("AbrirRecurso1aCmdb: " + e.getMessage());
 	  	}
 	 	return nextRetorno  ;
 	}
 	private Vector atualizarAuto(HttpServletRequest req,AutoInfracaoBean myAuto,ParamOrgBean myParam,ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,String datEntrada,String txtEMail,String solicDEF,String txtMotivo, Ata ata) throws sys.CommandException { 
		Vector vErro = new Vector() ;
		try { 	
			// preparar os Beans myAuto,myReq1,myHist (sem processo e num requerimento
			// Status 10,11,12,14,15,16	
            myParam.setNomUsrNameAlt(myUsrLog.getNomUserName());
			if ( (myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE","10,11,12,14,15,16,82","2").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) {
				if (("10,11,12,14,82".indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) myAuto.setDatStatus(datEntrada) ;			
				myAuto.setCodStatus("15");
			}
			myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 
			if ("1C".equals(solicDEF))  myReq.setCodResponsavelDP(myAuto.getCondutor().getCodResponsavel());   
			else myReq.setCodResponsavelDP(myAuto.getProprietario().getCodResponsavel());   
			myReq.setCodTipoSolic(solicDEF);
			myReq.setDatRequerimento(datEntrada);
			// preparar a data do Processo - se nao existir
			if (myAuto.getDatProcesso().length()==0) myAuto.setDatProcesso(myReq.getDatRequerimento());
			
			myReq.setCodStatusRequerimento("0");
			myReq.setNomUserNameDP(myUsrLog.getNomUserName());
			myReq.setCodOrgaoLotacaoDP(myUsrLog.getOrgao().getCodOrgao());
			myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(myAuto.getNumProcesso());
			myHis.setCodOrgao(myAuto.getCodOrgao());
			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodOrigemEvento("100");			
			myHis.setCodEvento("326");
			myHis.setNomUserName(myUsrLog.getNomUserName());
			myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			// complemento 01 - Num Requerimento no DAO
			myHis.setTxtComplemento02(myReq.getCodTipoSolic());
			if (solicDEF.equals("1C")) {
			   myHis.setTxtComplemento04(myAuto.getCondutor().getNomResponsavel());
			   myHis.setTxtComplemento05(myAuto.getCondutor().getIndCpfCnpj());				
			   myHis.setTxtComplemento06(myAuto.getCondutor().getNumCpfCnpj());				
			}	
			else { 
			   myHis.setTxtComplemento04(myAuto.getProprietario().getNomResponsavel());
			   myHis.setTxtComplemento05(myAuto.getProprietario().getIndCpfCnpj());				
			   myHis.setTxtComplemento06(myAuto.getProprietario().getNumCpfCnpj());
			}
			myHis.setTxtComplemento03(myReq.getDatRequerimento());	
			myHis.setTxtComplemento07(txtMotivo);
		    /*DPWEB*/
		    myHis.setTxtComplemento08("0");		   
			myHis.setTxtComplemento12(txtEMail);	
		    DaoBroker dao = DaoBrokerFactory.getInstance();
		    req.setAttribute("HistoricoBeanId",myHis) ;
		    req.setAttribute("ExisteProcesso",("".equals(myAuto.getNumProcesso()) ? "S" : "N")) ;
		    dao.atualizarAuto206(myAuto,myParam,myReq,myHis,myUsrLog, ata) ;
		    if (myAuto.getMsgErro().length()==0 && (txtEMail.indexOf("@")>=0)) {					
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.enviaEmailChangeParam(txtEMail,myHis);
		    }
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		return vErro ;
 	}	
}
