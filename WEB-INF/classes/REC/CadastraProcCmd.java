package REC;

import java.awt.Graphics2D;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import REG.Dao;

import ACSS.ParamSistemaBean;

import net.sourceforge.barbecue.Barcode;
import net.sourceforge.barbecue.BarcodeException;
import net.sourceforge.barbecue.BarcodeFactory;
public class CadastraProcCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/CadastraProc.jsp" ;  
   
  public CadastraProcCmd() {
    next = jspPadrao;
  }

  public CadastraProcCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    // cria os Beans, se n�o existir
	    String txtMotivo="";
	    
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	    
		ACSS.ParamSistemaBean paramRec = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (paramRec == null)	paramRec = new ACSS.ParamSistemaBean();
	    
	    AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";	    
	    if  (acao.equals("CadastraProc"))  {
			String dProc = req.getParameter("datprocesso");
			if (dProc==null) dProc="";
			String nProc = req.getParameter("numprocesso");
			if (nProc==null) nProc="";
			txtMotivo = req.getParameter("txtMotivo");
			if (txtMotivo==null) txtMotivo="";
	    	nextRetorno = TrocaProcesso(req,AutoInfracaoBeanId,UsrLogado,txtMotivo,nProc,dProc,paramRec,ParamOrgaoId);
	    }
	    if (acao.equals("imprimirInf")) {	
	    	txtMotivo = req.getParameter("txtMotivo")==null ? "" : req.getParameter("txtMotivo");
	    	nextRetorno = "/REC/CadProcImp.jsp" ;
	    }											
	    req.setAttribute("txtMotivo",txtMotivo) ;	    				
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;	    		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("CadastraProcCmd1: " + ue.getMessage());
    }
	return nextRetorno;
  }
  protected String TrocaProcesso(HttpServletRequest req,AutoInfracaoBean myAuto,
  ACSS.UsuarioBean UsrLogado,String txtMotivo,String nProc,String dProc,ACSS.ParamSistemaBean paramRec,ParamOrgBean ParamOrgaoId) throws sys.CommandException { 
   	String nextRetorno = next ;
   	try {
   		myAuto.setMsgErro("") ;
  		Vector vErro = new Vector() ;
  		
  		//validar data de Notificacao
		if (dProc.length()==0) vErro.addElement("Data do Processo n�o preenchida. \n") ;
   		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),dProc)>0) vErro.addElement("Data do Processo n�o pode ser superior a hoje. \n") ;
   		if (sys.Util.DifereDias(dProc,myAuto.getDatInfracao())>0) vErro.addElement("Data do Processo n�o pode ser anterior a data da Infra��o: "+myAuto.getDatInfracao()+". \n") ;
   		if (txtMotivo.length()==0) vErro.addElement("Motivo n�o informado. \n") ;
  		if (vErro.size()==0) {
			// Atualizar status do Auto			

//			myAuto.setDatProcesso(dProc);
//			myAuto.setNumProcesso(nProc);

	  		HistoricoBean myHis = new HistoricoBean() ;
	  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  			myHis.setNumProcesso(myAuto.getNumProcesso());
	  		myHis.setCodStatus(myAuto.getCodStatus());  
  			myHis.setDatStatus(myAuto.getDatStatus());
  			myHis.setCodEvento("410");
	  		myHis.setCodOrigemEvento("100");		
  			myHis.setNomUserName(UsrLogado.getNomUserName());
  			myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
	  		myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
	  		myHis.setTxtComplemento01(nProc);
	  		myHis.setTxtComplemento02(dProc);
	  		myHis.setTxtComplemento03(" ");
	  		myHis.setTxtComplemento04(txtMotivo);
	  		
	  		DaoBroker dao = DaoBrokerFactory.getInstance();			
  			dao.atualizarAuto410(myAuto,myHis,UsrLogado);
  			if (myAuto.getMsgErro().length() < 1){
				myAuto.setMsgOk("S") ;										
				myAuto.setMsgErro("Numero do Processo alterado para o Auto: "+myAuto.getNumAutoInfracao()) ;
  			}
  			if (UsrLogado.getCodOrgaoAtuacao().equals("258650"))
    			  GeraCodigoBarra(UsrLogado.getCodOrgaoAtuacao(),myAuto,myHis,paramRec,ParamOrgaoId);
  			
  			
		}
		else myAuto.setMsgErro(vErro) ;
   	}
    catch (Exception e){
  	    throw new sys.CommandException("CadastraProcCmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }
  
  public void GeraCodigoBarra(String sCodOrgao,AutoInfracaoBean myAuto,HistoricoBean myHis,ParamSistemaBean paramRec,ParamOrgBean ParamOrgaoId)
  throws BarcodeException,ServletException,IOException {
	  int iAltura=70;
	  boolean bImprimeTexto=true;
	  try
	  {
		  String sPath=getArquivo(paramRec,myHis.getDatProc(),myAuto.getNumProcesso().replaceAll("/",""));
		  String sAno =ParamOrgaoId.getParamOrgao("PROCESSO_ANO","2007","2");
		  try
		  {
			  drawingBarcodeDirectToGraphics(sPath,myAuto.getNumProcesso(),iAltura,bImprimeTexto);
			  REG.Dao daoReg = Dao.getInstance();
			  daoReg.GravaCodigoBarraProcesso(sCodOrgao,myHis.getDatProc(),myAuto.getNumProcesso(),sAno,0);
			  /*Gravando informa��o do codigo de barras Gerado*/					
		  }
		  catch (Exception e) {	  
		  }
	  }
	  catch (Exception e) {
	  }
  }

	public String getParametro(ACSS.ParamSistemaBean param,String sData) {        
		String parametro = "DIR_CODIGO_BARRA_PROCESSO";
		/*Novo Path*/
		parametro+="_"+sData.substring(6,10);
		return parametro;
	}
  
	public String getArquivo(ACSS.ParamSistemaBean param,String sData,String texto) {
		String pathArquivo;
		try {
			pathArquivo = param.getParamSist(getParametro(param,sData))+"/"+getImagem(sData)+
					"/CodigoBarraProcesso/"+texto+ ".jpg";				
			
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}    
	
	public String getImagem(String sData) {
		String pathImagem = "";
		pathImagem =sData.substring(6,10)+ 
		sData.substring(3,5) +
		"/" + sData.substring(0,2); 
		return pathImagem;	
	}

  
	public boolean drawingBarcodeDirectToGraphics(String sPath,String sTexto,int iAltura,boolean bImprimeTexto)
	throws BarcodeException,ServletException,IOException {
		try
		{
			Barcode barcode = BarcodeFactory.createCode128B(sTexto);
			barcode.setBarHeight((double)iAltura);
			barcode.setDrawingText(bImprimeTexto);
			BufferedImage image = getImage(barcode);
			File dir = new File(sPath);
			dir.mkdirs();    			
			ImageIO.write(image, "jpg", dir);			
		}
		catch (Exception e) 
		{
		}    	
		return true;
	}    
	
	public BufferedImage getImage(Barcode barcode) {
		
		BufferedImage bi = new BufferedImage(barcode.getWidth(), barcode.getHeight(), BufferedImage.TYPE_BYTE_INDEXED);
		Graphics2D g = bi.createGraphics();
		barcode.draw(g, 0, 0);
		bi.flush();
		return bi;
	}
}
