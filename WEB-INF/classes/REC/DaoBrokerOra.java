package REC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


import ACSS.Ata;
import ACSS.UsuarioBean;

import sys.DaoException;
import sys.ServiceLocatorException;

public class DaoBrokerOra extends sys.DaoBase implements DaoBroker{

	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "REC";

	public DaoBrokerOra() throws DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}

	/*****************************************
		Transa��es de acesso ao Broker
	 *****************************************/

	/**
	 *-----------------------------------------------------------
	 * Le Infracao e carrega no Bean - Transacao 041
	 *-----------------------------------------------------------
	 */
	public boolean LeAutoInfracao(AutoInfracaoBean myInf,String tpchave,ACSS.UsuarioBean myUsrLog)
			throws DaoException {
		//	  synchronized
		boolean bOk = true;
		Connection conn     = null;
		Statement stmt      = null;
		Statement stmt_Cons = null;		
		ResultSet rs        = null;
		ResultSet rs_Cons   = null;
		try 
		{
			if ("notificacao".equals(tpchave)) {
				myInf.setNumPlaca("");
				myInf.setNumAutoInfracao("");
				myInf.setNumProcesso("");
			} else {
				myInf.setNumNotificacao("");
				if ((myInf.getNumPlaca().trim().length() == 0)
						&& (myInf.getNumAutoInfracao().trim().length() == 0)) {
					myInf.setNumPlaca("");
					myInf.setNumAutoInfracao("");
				} else
					myInf.setNumProcesso("");
			}
			conn = serviceloc.getConnection(MYABREVSIST);
			stmt      = conn.createStatement();
			stmt_Cons = conn.createStatement();
			boolean existeAuto = false;	
			boolean flag       = true;
			String sCmd="SELECT " +
					"COD_AUTO_INFRACAO,"+
					"NUM_AUTO_INFRACAO,"+
					"TO_CHAR(DAT_INFRACAO,'DD/MM/YYYY') AS DAT_INFRACAO,"+
					"VAL_HOR_INFRACAO,"+
					"DSC_LOCAL_INFRACAO,"+
					"NUM_PLACA,"+
					"NUM_PROCESSO,"+
					"TO_CHAR(DAT_PROCESSO,'DD/MM/YYYY') AS DAT_PROCESSO,"+
					"COD_AGENTE,"+
					"COD_ORGAO,"+
					"DSC_LOCAL_APARELHO,"+
					"NUM_CERTIF_AFER_APARELHO,"+
					"NVL(DAT_ULT_AFER_APARELHO,TO_CHAR(SYSDATE,'DD/MM/YYYY')) AS DAT_ULT_AFER_APARELHO,"+
					"NUM_IDENT_APARELHO,"+
					"NUM_INMETRO_APARELHO,"+
					"COD_TIPO_DISP_REGISTRADOR,"+
					"TO_CHAR(DAT_VENCIMENTO,'DD/MM/YYYY') AS DAT_VENCIMENTO,"+
					"COD_MUNICIPIO,"+
					"VAL_VELOC_AFERIDA,"+
					"VAL_VELOC_CONSIDERADA,"+
					"DSC_SITUACAO,"+
					"IND_CONDUTOR_IDENTIFICADO,"+
					"NUM_LOTE,"+
					"COD_STATUS,"+
					"TO_CHAR(DAT_STATUS,'DD/MM/YYYY') AS DAT_STATUS,"+
					"DAT_PUBDO,"+
					"COD_ENVIO_PUBDO,"+
					"DAT_ENVDO,"+
					"IND_TP_CV,"+
					"COD_RESULT_PUBDO,"+
					"COD_BARRA,"+
					"IND_SITUACAO,"+
					"IND_FASE,"+
					"IND_PAGO,"+
					"NUM_IDENT_ORGAO,"+
					"DSC_RESUMO_INFRACAO,"+
					"DSC_MARCA_MODELO_APARELHO,"+
					"DSC_LOCAL_INFRACAO_NOTIF,"+
					"COD_INFRACAO,"+
					"DSC_INFRACAO,"+
					"DSC_ENQUADRAMENTO,"+
					"VAL_REAL,"+
					"NUM_PONTO,"+
					"DSC_COMPETENCIA,"+
					"IND_COND_PROP,"+
					"IND_SUSPENSAO,"+
					"VAL_REAL_DESCONTO,"+
					"COD_MARCA_MODELO,"+
					"COD_ESPECIE,"+
					"COD_CATEGORIA,"+
					"COD_TIPO,"+
					"COD_COR,"+
					"NOM_PROPRIETARIO,"+
					"TIP_DOC_PROPRIETARIO,"+
					"NUM_DOC_PROPRIETARIO,"+
					"TIP_CNH_PROPRIETARIO,"+
					"NUM_CNH_PROPRIETARIO,"+
					"DSC_END_PROPRIETARIO,"+
					"NUM_END_PROPRIETARIO,"+
					"DSC_COMP_PROPRIETARIO,"+
					"COD_MUN_PROPRIETARIO,"+
					"NUM_CEP_PROPRIETARIO,"+
					"UF_PROPRIETARIO,"+
					"NOM_CONDUTOR,"+
					"TIP_DOC_CONDUTOR,"+
					"NUM_DOC_CONDUTOR,"+
					"TIP_CNH_CONDUTOR,"+
					"NUM_CNH_CONDUTOR,"+
					"DSC_END_CONDUTOR,"+
					"NUM_END_CONDUTOR,"+
					"DSC_COMP_CONDUTOR,"+
					"COD_MUN_CONDUTOR,"+
					"NUM_CEP_CONDUTOR,"+
					"UF_CONDUTOR,"+
					"NUM_NOTIF_AUTUACAO,"+
					"DAT_NOTIF_AUTUACAO,"+
					"COD_ENT_AUTUACAO,"+
					"NUM_LOTE_AUTUACAO,"+
					"NUM_NOTIF_PENALIDADE,"+
					"DAT_NOTIF_PENALIDADE,"+
					"COD_ENT_PENALIDADE,"+
					"NUM_LOTE_PENALIDADE,"+
					"DSC_AGENTE,"+
					"DSC_UNIDADE,"+
					"DAT_PAGAMENTO,"+
					"NUM_AUTO_ORIGEM,"+
					"COD_INFRACAO_ORIGEM,"+
					"IND_TIPO_AUTO,"+
					"NUM_AUTO_RENAINF,"+
					"TXT_EMAIL,"+
					"DAT_INCLUSAO,"+
					"DAT_ALTERACAO,"+
					"SIT_AI_DIGITALIZADO,"+
					"SIT_AR_DIGITALIZADO,"+
					"DAT_DIGITALIZACAO,"+
					"DAT_REGISTRO,"+
					"SIT_DIGITALIZACAO,"+
					"VAL_VELOC_PERMITIDA,"+
					"IDENT_ORGAO,"+
					"COD_ACESSO_WEB,"+
					"OBS_CONDUTOR,"+
					"IND_PONTUACAO,"+
					"IND_PARCELAMENTO "+
					"FROM TSMI_AUTO_INFRACAO ";
			if ( (myInf.getNumAutoInfracao()!=null) && (myInf.getNumAutoInfracao().trim().length() >0) ){
				sCmd += " WHERE NUM_AUTO_INFRACAO  = '"+myInf.getNumAutoInfracao()+"'" ;
				flag = false;
			}
			if ( (myInf.getNumPlaca()!=null) && (myInf.getNumPlaca().trim().length() >0) ){
				if(flag)
					sCmd += " WHERE NUM_PLACA  = '"+myInf.getNumPlaca()+"'" ;
				else
					sCmd += " AND NUM_PLACA  = '"+myInf.getNumPlaca()+"'" ;
			}

			if ( (myInf.getProprietario()!=null) && (myInf.getProprietario().getNumCpfCnpj()!=null) && (myInf.getProprietario().getNumCpfCnpj().trim()!= "") ){
				if(flag)
					sCmd += " WHERE NUM_DOC_PROPRIETARIO  = '"+myInf.getProprietario().getNumCpfCnpj()+"'" ;
				else
					sCmd += " AND NUM_DOC_PROPRIETARIO    = '"+myInf.getProprietario().getNumCpfCnpj()+"'" ;
			}
			if ( (myInf.getNumRenavam()!=null) && (myInf.getNumRenavam().trim().length() >0) ){
				if(flag)
					sCmd += " WHERE NUM_RENAVAM  = '"+myInf.getNumRenavam()+"'" ;
				else
					sCmd += " AND NUM_RENAVAM    = '"+myInf.getNumRenavam()+"'" ;
			}
			if ( (myInf.getNumProcesso()!=null) && (myInf.getNumProcesso().trim().length() >0) ){
				if(flag)
					sCmd += " WHERE NUM_PROCESSO  = '"+myInf.getNumProcesso()+"'" ;
				else
					sCmd += " AND NUM_PROCESSO    = '"+myInf.getNumProcesso()+"'" ;
			}
			sCmd += " ORDER BY NUM_AUTO_INFRACAO DESC";
			rs = stmt.executeQuery(sCmd);
			if (rs.next()) 
			{
				myInf.setCodAutoInfracao(rs.getString("COD_AUTO_INFRACAO"));
				myInf.getInfracao().setCodInfracao(rs.getString("COD_INFRACAO"));
				myInf.getInfracao().setDscInfracao(rs.getString("DSC_INFRACAO"));
				myInf.getInfracao().setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));
				myInf.getInfracao().setNumPonto(rs.getString("NUM_PONTO"));
				myInf.getInfracao().setDscCompetencia(rs.getString("DSC_COMPETENCIA"));
				myInf.getInfracao().setIndTipo(rs.getString("IND_COND_PROP"));
				myInf.getInfracao().setIndSDD(rs.getString("IND_SUSPENSAO"));
				myInf.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));
				String datInfracao="";
				if(rs.getString("DAT_INFRACAO") != null){
					datInfracao=rs.getString("DAT_INFRACAO");
				}
				myInf.setDatInfracao(datInfracao);
				if(rs.getString("VAL_HOR_INFRACAO") != null){
					myInf.setValHorInfracao(rs.getString("VAL_HOR_INFRACAO"));
				}
				myInf.setCodAgente(rs.getString("COD_AGENTE"));
				VerificaAutoOracle(myInf, conn); /* Poder Gravar Data da Infracao */
				myInf.setCodOrgao(rs.getString("COD_ORGAO"));
				myInf.getOrgao().Le_Orgao(myInf.getCodOrgao(), 0);
				myInf.setSigOrgao(myInf.getOrgao().getSigOrgao());
				myInf.setNomOrgao(myInf.getOrgao().getNomOrgao());
				myInf.setDscLocalAparelho(rs.getString("DSC_LOCAL_APARELHO"));
				myInf.setNumCertAferAparelho(rs.getString("NUM_CERTIF_AFER_APARELHO"));
				myInf.setDatUltAferAparelho(rs.getString("DAT_ULT_AFER_APARELHO"));
				myInf.setNumIdentAparelho(rs.getString("NUM_IDENT_APARELHO"));
				myInf.setNumInmetroAparelho(rs.getString("NUM_INMETRO_APARELHO"));
				myInf.setCodTipDispRegistrador(rs.getString("COD_TIPO_DISP_REGISTRADOR"));
				myInf.getInfracao().setValReal(rs.getString("VAL_REAL"));
				myInf.setDatVencimento(rs.getString("DAT_VENCIMENTO"));
				myInf.setCodMunicipio(rs.getString("COD_MUNICIPIO"));
				sCmd = "SELECT NOM_MUNICIPIO FROM TSMI_MUNICIPIO WHERE COD_MUNICIPIO='"+myInf.getCodMunicipio()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.setNomMunicipio(rs_Cons.getString("NOM_MUNICIPIO"));
				}
				myInf.setValVelocPermitida(rs.getString("VAL_VELOC_PERMITIDA"));
				myInf.setValVelocAferida(rs.getString("VAL_VELOC_AFERIDA"));
				myInf.setValVelocConsiderada(rs.getString("VAL_VELOC_CONSIDERADA"));
				myInf.setDscSituacao(rs.getString("DSC_SITUACAO"));

				if (myInf.getDscSituacao().indexOf("*****") >= 0)
					myInf.setDscSituacao(" ");
				// Gravar dados de ve�culo
				myInf.setVeiculo(new VeiculoBean());

				myInf.getVeiculo().setCodVeiculo(myInf.getNumPlaca());
				myInf.getVeiculo().setNumPlaca(rs.getString("NUM_PLACA"));
				myInf.getVeiculo().setCodMarcaModelo(rs.getString("COD_MARCA_MODELO"));
				myInf.getVeiculo().setCodEspecie(rs.getString("COD_ESPECIE"));
				myInf.getVeiculo().setCodCategoria(rs.getString("COD_CATEGORIA"));
				myInf.getVeiculo().setCodTipo(rs.getString("COD_TIPO"));
				myInf.getVeiculo().setCodCor(rs.getString("COD_COR"));
				sCmd = "SELECT DSC_ESPECIE FROM TSMI_ESPECIE WHERE COD_ESPECIE='"+myInf.getVeiculo().getCodEspecie()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.getVeiculo().setDscEspecie(rs_Cons.getString("DSC_ESPECIE"));
				}

				sCmd = "SELECT DSC_CATEGORIA FROM TSMI_CATEGORIA WHERE COD_CATEGORIA='"+myInf.getVeiculo().getCodCategoria()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.getVeiculo().setDscCategoria(rs_Cons.getString("DSC_CATEGORIA"));
				}

				sCmd = "SELECT DSC_TIPO FROM TSMI_TIPO WHERE COD_TIPO = '"+myInf.getVeiculo().getCodTipo()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.getVeiculo().setDscTipo(rs_Cons.getString("DSC_TIPO"));
				}

				sCmd = "SELECT DSC_COR FROM TSMI_COR WHERE COD_COR = '"+myInf.getVeiculo().getCodCor()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.getVeiculo().setDscCor(rs_Cons.getString("DSC_COR"));
				}

				sCmd = "SELECT DSC_MARCA_MODELO FROM TSMI_MARCA_MODELO WHERE COD_MARCA_MODELO='"+myInf.getVeiculo().getCodMarcaModelo()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.getVeiculo().setDscMarcaModelo(rs_Cons.getString("DSC_MARCA_MODELO"));
				}

				/*PROPRIET�RIO*/
				ResponsavelBean proprietario = new ResponsavelBean();
				proprietario.setNomResponsavel(rs.getString("NOM_PROPRIETARIO"));
				proprietario.setIndCpfCnpj(rs.getString("TIP_DOC_PROPRIETARIO"));
				proprietario.setNumCpfCnpj(rs.getString("NUM_DOC_PROPRIETARIO"));
				if ("1".equals(proprietario.getIndCpfCnpj()))
					proprietario.setNumCpfCnpj(sys.Util.lPad(proprietario.getNumCpfCnpj(), "0", 11));
				else
					proprietario.setNumCpfCnpj(sys.Util.lPad(proprietario.getNumCpfCnpj(), "0", 14));
				proprietario.setCodResponsavel(proprietario.getNumCpfCnpj());
				proprietario.setIndTipoCnh(rs.getString("TIP_CNH_PROPRIETARIO"));
				proprietario.setNumCnh(rs.getString("NUM_CNH_PROPRIETARIO"));
				sys.EnderecoBean endereco = new sys.EnderecoBean();
				endereco.setTxtEndereco(rs.getString("DSC_END_PROPRIETARIO"));
				endereco.setNumEndereco(rs.getString("NUM_END_PROPRIETARIO"));
				endereco.setSeqEndereco(endereco.getNumEndereco());
				endereco.setTxtComplemento(rs.getString("DSC_COMP_PROPRIETARIO"));
				endereco.setCodCidade(rs.getString("COD_MUN_PROPRIETARIO"));
				sCmd = "SELECT NOM_MUNICIPIO FROM TSMI_MUNICIPIO WHERE COD_MUNICIPIO = '"+endereco.getCodCidade()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					endereco.setNomCidade(rs_Cons.getString("NOM_MUNICIPIO"));
				}
				endereco.setNumCEP(rs.getString("NUM_CEP_PROPRIETARIO"));
				endereco.setCodUF(rs.getString("UF_PROPRIETARIO"));
				proprietario.setEndereco(endereco);
				myInf.setProprietario(proprietario);


				/*CONDUTOR*/
				ResponsavelBean condutor = new ResponsavelBean();
				String nomCondutor = rs.getString("NOM_CONDUTOR"); 
				//Raj
				if(nomCondutor != null && nomCondutor.trim().length()>0){
					
//				}
//				if ((!nomCondutor.trim().equals(""))&&(nomCondutor != null)) {
					condutor.setNomResponsavel(nomCondutor);
					condutor.setIndCpfCnpj(rs.getString("TIP_DOC_CONDUTOR"));
					condutor.setNumCpfCnpj(rs.getString("NUM_DOC_CONDUTOR"));
					condutor.setCodResponsavel(condutor.getNumCpfCnpj());
					condutor.setIndTipoCnh(rs.getString("TIP_CNH_CONDUTOR"));
					condutor.setNumCnh(rs.getString("NUM_CNH_CONDUTOR"));
					endereco = new sys.EnderecoBean();
					endereco.setTxtEndereco(rs.getString("DSC_END_CONDUTOR"));
					endereco.setNumEndereco(rs.getString("NUM_END_CONDUTOR"));
					endereco.setSeqEndereco(endereco.getNumEndereco());
					endereco.setTxtComplemento(rs.getString("DSC_COMP_CONDUTOR"));
					endereco.setCodCidade(rs.getString("COD_MUN_CONDUTOR"));
					sCmd = "SELECT NOM_MUNICIPIO FROM TSMI_MUNICIPIO WHERE COD_MUNICIPIO='"+endereco.getCodCidade() + "'";
					rs_Cons = stmt_Cons.executeQuery(sCmd);
					while (rs_Cons.next()) {
						endereco.setNomCidade(rs_Cons.getString("NOM_MUNICIPIO"));
					}
					endereco.setNumCEP(rs.getString("NUM_CEP_CONDUTOR"));
					endereco.setCodUF(rs.getString("UF_CONDUTOR"));
					condutor.setEndereco(endereco);
					myInf.setCondutor(condutor);
					if (!proprietario.getNomResponsavel().equals(condutor.getNomResponsavel()))
						myInf.setIndCondutorIdentificado("1");
					else
						myInf.setIndCondutorIdentificado("0");
				} 
				else
				{
					myInf.setCondutor(myInf.getProprietario());
					myInf.setIndCondutorIdentificado("0");
				}

				myInf.setCodStatus(rs.getString("COD_STATUS"));
				if ("88".equals(myInf.getCodStatus())) 	myInf.setCodStatus("0");
				sCmd = "SELECT NOM_STATUS FROM TSMI_STATUS_AUTO WHERE COD_STATUS='"+myInf.getCodStatus()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				myInf.setNomStatus("Status: " + myInf.getCodStatus());
				while (rs_Cons.next()) {
					myInf.setNomStatus(rs_Cons.getString("NOM_STATUS"));
				}
				String datStatus="";
				if(rs.getString("DAT_STATUS") != null){
					datStatus=rs.getString("DAT_STATUS");
					myInf.setDatStatus(datStatus);
					myInf.setDatEnvioDO(rs.getString("DAT_ENVDO"));
					myInf.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
					myInf.setDatPublPDO(rs.getString("DAT_PUBDO"));
					myInf.setIndTPCV(rs.getString("IND_TP_CV"));
					myInf.setNumProcesso(rs.getString("NUM_PROCESSO"));
					String dat_Processo=rs.getString("DAT_PROCESSO");
					myInf.setDatProcesso(dat_Processo);
					myInf.getInfracao().setValRealDesconto(rs.getString("VAL_REAL_DESCONTO"));
					myInf.setCodResultPubDO(rs.getString("COD_RESULT_PUBDO"));
					myInf.setCodBarra(rs.getString("COD_BARRA"));
					myInf.setNumNotifAut(rs.getString("NUM_NOTIF_AUTUACAO"));
					myInf.setDatNotifAut(rs.getString("DAT_NOTIF_AUTUACAO"));
					myInf.setCodNotifAut(rs.getString("COD_ENT_AUTUACAO"));
					myInf.setNumARNotifAut(rs.getString("NUM_LOTE_AUTUACAO"));
					myInf.setNumNotifPen(rs.getString("NUM_NOTIF_PENALIDADE"));
					myInf.setDatNotifPen(rs.getString("DAT_NOTIF_PENALIDADE"));
					myInf.setCodNotifPen(rs.getString("COD_ENT_PENALIDADE"));
					myInf.setNumARNotifPen(rs.getString("NUM_LOTE_PENALIDADE"));

					myInf.setIndFase(rs.getString("IND_FASE"));
					myInf.setIndSituacao(rs.getString("IND_SITUACAO"));
					myInf.setIndPago(rs.getString("IND_PAGO"));
					myInf.setDscAgente(rs.getString("DSC_AGENTE"));
					myInf.setDscUnidade(rs.getString("DSC_UNIDADE"));
					myInf.setDscLote(rs.getString("NUM_LOTE"));
					myInf.setDatPag(rs.getString("DAT_PAGAMENTO"));
					myInf.setNumAutoOrigem(rs.getString("NUM_AUTO_ORIGEM"));
					myInf.setNumInfracaoOrigem(rs.getString("COD_INFRACAO_ORIGEM"));


					/*RENAINF*/
					myInf.setIdentAutoOrigem(rs.getString("IND_TIPO_AUTO"));
					myInf.setNumInfracaoRenainf(rs.getString("NUM_AUTO_RENAINF"));
				}
				
				
				

				/*OUTRO ORGAO - NITEROI*/

				//				myInf.setIdentOrgao(rs.getString("IDENT_ORGAO"));           FALTA!!!

				/* DPWEB */
				//				myInf.setCodAcessoWeb(rs.getString("COD_ACESSO_WEB"));      FALTA!!!
				//				myInf.setObsCondutor(rs.getString("OBS_CONDUTOR"));         FALTA!!!
				//				myInf.setIndPontuacao(rs.getString("IND_PONTUACAO"));       FALTA!!!

				/*PARCELAMENTO*/
				//				myInf.setIndParcelamento(rs.getString("IND_PARCELAMENTO")); FALTA!!!

				sCmd = "SELECT DAT_INCLUSAO, DAT_ALTERACAO, DAT_REGISTRO, DSC_RESUMO_INFRACAO, DSC_MARCA_MODELO_APARELHO, DSC_LOCAL_INFRACAO_NOTIF"+
						" FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) 
				{
					SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					if (rs_Cons.getDate("DAT_INCLUSAO") != null)
						myInf.setDatInclusao(df.format(rs_Cons.getDate("DAT_INCLUSAO")));
					else
						myInf.setDatInclusao(df.format(new Date()));
					if (rs_Cons.getDate("DAT_ALTERACAO") != null)
						myInf.setDatAlteracao(df.format(rs_Cons.getDate("DAT_ALTERACAO")));
					else
						myInf.setDatAlteracao("");
					if (rs_Cons.getDate("DAT_REGISTRO") != null)
						myInf.setDatRegistro(df.format(rs_Cons.getDate("DAT_REGISTRO")));
					else
						myInf.setDatRegistro("");
					myInf.setDscResumoInfracao(rs_Cons.getString("DSC_RESUMO_INFRACAO"));
					myInf.setDscMarcaModeloAparelho(rs_Cons.getString("DSC_MARCA_MODELO_APARELHO"));
					myInf.setDscLocalInfracaoNotificacao(rs_Cons.getString("DSC_LOCAL_INFRACAO_NOTIF"));
				}

				String cra = "N";
				sCmd = "SELECT IND_ENTREGUE,DSC_CODIGO_RETORNO FROM TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+myInf.getCodNotifAut()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					cra = rs_Cons.getString("IND_ENTREGUE");
					myInf.setNomMotivoNotifAut(rs_Cons.getString("DSC_CODIGO_RETORNO"));
					if ("S".equals(cra) == false)
						myInf.setDatNotifAut("");
				}

				String crb = "N";
				sCmd = "SELECT IND_ENTREGUE,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+myInf.getCodNotifPen()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					crb = rs_Cons.getString("IND_ENTREGUE");
					myInf.setNomMotivoNotifPen(rs_Cons.getString("DSC_CODIGO_RETORNO"));
					if ("S".equals(crb) == false)
						myInf.setDatNotifPen("");
				}

				if (sys.Util.DifereDias("16/07/2004", myInf.getDatInfracao()) < 0) {
					myInf.setDatNotificacao(myInf.getDatNotifPen());
					myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifPen());
				} else {
					if (myInf.getCodStatus().length() > 1) {
						myInf.setDatNotificacao(myInf.getDatNotifPen());
						myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifPen());
					} else {
						myInf.setDatNotificacao(myInf.getDatNotifAut());
						myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifAut());
					}
				}

				myInf.getInfracao().setCodAutoInfracao(myInf.getCodAutoInfracao());
				sCmd = "SELECT COD_AR_DIGITALIZADO FROM TSMI_AR_DIGITALIZADO WHERE NUM_AUTO_INFRACAO='"+myInf.getNumAutoInfracao()+"' AND "+
						"LENGTH(NUM_NOTIFICACAO) < 10 AND (COD_RETORNO IS NULL  OR COD_RETORNO NOT IN ('99','98')) ";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.setTemARDigitalizado(true);
				}

				sCmd = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO WHERE NUM_AUTO_INFRACAO='"+ myInf.getNumAutoInfracao()+"'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.setTemAIDigitalizado(true);
				}

				sCmd = "SELECT COD_FOTO_DIGITALIZADA FROM TSMI_FOTO_DIGITALIZADA  WHERE NUM_AUTO_INFRACAO='"+myInf.getNumAutoInfracao() + "'";
				rs_Cons = stmt_Cons.executeQuery(sCmd);
				while (rs_Cons.next()) {
					myInf.setTemFotoDigitalizada(true);
				}
			}
			if (stmt != null) stmt.close();
			if (stmt_Cons != null) stmt_Cons.close();			

			rs.close();
			rs_Cons.close();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
			bOk = false;
			myInf.setNumAutoInfracao("0");
			throw new DaoException(e.getMessage());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			bOk = false;
			myInf.setNumAutoInfracao("0");
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (stmt != null)
						stmt.close();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return bOk;
	}

	public  boolean LeHistorico (AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException {
		boolean bOk     = true ;
		String sComando = "";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt      = conn.createStatement();
			Statement stmt_Cons = conn.createStatement();			
			ResultSet rs       = null;
			ResultSet rs_Cons  = null;			
			ArrayList<HistoricoBean> historicos = new ArrayList<HistoricoBean>();
			sComando="SELECT "+
					"COD_AUTO_INFRACAO, "+
					"COD_HISTORICO_AUTO, "+
					"NUM_PROCESSO, "+
					"COD_STATUS, "+
					"DAT_STATUS, "+
					"COD_EVENTO, "+
					"COD_ORIGEM_EVENTO, "+
					"TXT_COMPLEMENTO_01, "+
					"TXT_COMPLEMENTO_02, "+
					"TXT_COMPLEMENTO_03, "+
					"TXT_COMPLEMENTO_04, "+
					"TXT_COMPLEMENTO_05, "+
					"TXT_COMPLEMENTO_06, "+
					"TXT_COMPLEMENTO_07, "+
					"TXT_COMPLEMENTO_08, "+
					"TXT_COMPLEMENTO_09, "+
					"TXT_COMPLEMENTO_10, "+
					"TXT_COMPLEMENTO_11, "+
					"NOM_USERNAME, "+
					"COD_ORGAO_LOTACAO, "+
					"DAT_PROC, "+
					"TXT_COMPLEMENTO_12 "+			
					"FROM TSMI_HISTORICO_AUTO WHERE COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"'";
			rs = stmt.executeQuery(sComando);
			while (rs.next()) 
			{
				HistoricoBean myHist = new HistoricoBean();
				myHist.setCodAutoInfracao(rs.getString("COD_AUTO_INFRACAO"));
				myHist.setCodHistoricoAuto(rs.getString("COD_HISTORICO_AUTO"));
				myHist.setNumProcesso(rs.getString("NUM_PROCESSO"));
				myHist.setCodStatus(rs.getString("COD_STATUS"));
				myHist.setDatStatus(rs.getString("DAT_STATUS"));
				myHist.setCodEvento(rs.getString("COD_EVENTO"));
				myHist.setCodOrigemEvento(rs.getString("COD_ORIGEM_EVENTO"));
				myHist.setTxtComplemento01(rs.getString("TXT_COMPLEMENTO_01"));
				myHist.setTxtComplemento02(rs.getString("TXT_COMPLEMENTO_02"));
				myHist.setTxtComplemento03(rs.getString("TXT_COMPLEMENTO_03"));
				myHist.setTxtComplemento04(rs.getString("TXT_COMPLEMENTO_04"));
				myHist.setTxtComplemento05(rs.getString("TXT_COMPLEMENTO_05"));
				myHist.setTxtComplemento06(rs.getString("TXT_COMPLEMENTO_06"));
				myHist.setTxtComplemento07(rs.getString("TXT_COMPLEMENTO_07"));
				myHist.setTxtComplemento08(rs.getString("TXT_COMPLEMENTO_08"));
				myHist.setTxtComplemento09(rs.getString("TXT_COMPLEMENTO_09"));
				myHist.setTxtComplemento10(rs.getString("TXT_COMPLEMENTO_10"));
				myHist.setTxtComplemento11(rs.getString("TXT_COMPLEMENTO_11"));
				myHist.setTxtComplemento12(rs.getString("TXT_COMPLEMENTO_12"));
				myHist.setNomUserName(rs.getString("NOM_USERNAME"));
				myHist.setCodOrgaoLotacao(rs.getString("COD_ORGAO_LOTACAO"));
				myHist.setDatProc(rs.getString("DAT_PROC"));


				if ((myHist.getCodEvento().trim().length() > 0) && ("000".equals(myHist.getCodEvento()) == false)) {
					String sCmd = "SELECT nom_status from TSMI_STATUS_AUTO WHERE cod_status='"+ myHist.getCodStatus()+"'";
					rs_Cons = stmt_Cons.executeQuery(sCmd);
					while (rs_Cons.next()) {
						myHist.setNomStatus(rs_Cons.getString("nom_status"));
					}
					sCmd = "SELECT dsc_origem_evento from TSMI_ORIGEM_EVENTO WHERE cod_origem_evento='"+myHist.getCodOrigemEvento()+"'";
					rs_Cons = stmt_Cons.executeQuery(sCmd);
					while (rs_Cons.next()) {
						myHist.setNomOrigemEvento(rs_Cons.getString("dsc_origem_evento"));
					}
					sCmd = "SELECT dsc_evento from TSMI_EVENTO WHERE cod_evento='"+ myHist.getCodEvento()+"'";
					rs_Cons = stmt_Cons.executeQuery(sCmd);
					while (rs_Cons.next()) {
						myHist.setNomEvento(rs_Cons.getString("dsc_evento"));
					}
					if ("214".equals(myHist.getCodEvento())) {
						sCmd = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio='"+myHist.getTxtComplemento06().trim() + "'";
						rs_Cons = stmt_Cons.executeQuery(sCmd);
						while (rs_Cons.next()) {
							myHist.setTxtComplemento06(rs_Cons.getString("nom_municipio"));
						}
						// Editar campos
						// CPF
						if (myHist.getTxtComplemento02().length() < 11)
							myHist.setTxtComplemento02(myHist.getTxtComplemento02()+"               ");
						myHist.setTxtComplemento02(myHist.getTxtComplemento02().substring(0,3)+"."+myHist.getTxtComplemento02().substring(3,6)+ "."+
								myHist.getTxtComplemento02().substring(6,9)+"-"+myHist.getTxtComplemento02().substring(9,11));
						// CEP
						if (myHist.getTxtComplemento08().length() < 8)
							myHist.setTxtComplemento08(myHist.getTxtComplemento08()+"               ");
						myHist.setTxtComplemento08(myHist.getTxtComplemento08().substring(0, 5)+"-"+myHist.getTxtComplemento08().substring(5,8));
						// CNH
						String p = myHist.getTxtComplemento09().substring(0,1);
						myHist.setTxtComplemento09(p+" - "+sys.Util.SemZeros(myHist.getTxtComplemento09().substring(1)));
					}
					if ("204,324,203,323".indexOf(myHist.getCodEvento()) >= 0) 
					{
						sCmd = "SELECT DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+myHist.getTxtComplemento01()+"'";
						rs_Cons=stmt_Cons.executeQuery(sCmd);
						while (rs.next()) {
							myHist.setTxtComplemento03(rs_Cons.getString("DSC_CODIGO_RETORNO"));
						}
					}
					if ("383,390".indexOf(myHist.getCodEvento()) >= 0) {
						sCmd = "SELECT COD_ENVIO_PUBDO from TSMI_REQUERIMENTO WHERE NUM_REQUERIMENTO='"+myHist.getTxtComplemento02()+"'";
						rs_Cons=stmt_Cons.executeQuery(sCmd);
						while (rs_Cons.next()) {
							myHist.setTxtComplemento03(rs_Cons.getString("COD_ENVIO_PUBDO"));
						}
					}
					if ("200".equals(myHist.getCodEvento())) 
					{
						sCmd = "SELECT ARQ.NOM_ARQUIVO, to_char(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy') AS DAT_RECEBIMENTO"
								+ "	FROM tsmi_arquivo_dol ARQ,  "
								+ "	TSMI_LOTE_DOL ADOL, TSMI_LINHA_LOTE LDOL"
								+ "	WHERE ARQ.COD_IDENT_ARQUIVO = 'DE01'"
								+ "	AND ADOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_dol"
								+ "	AND ADOL.COD_LOTE = LDOL.COD_LOTE "
								+ "	AND LDOL.NUM_AUTO_INFRACAO = '"
								+ myInf.getNumAutoInfracao()
								+ "'"
								+ "	AND ARQ.COD_STATUS = 2";

						rs_Cons = stmt_Cons.executeQuery(sCmd);
						while (rs_Cons.next()) {
							myHist.setTxtComplemento05(rs_Cons.getString("NOM_ARQUIVO"));
							myHist.setTxtComplemento06(rs_Cons.getString("DAT_RECEBIMENTO"));
						}
						if (!rs_Cons.next()) {
							sCmd = "SELECT ARQ.NOM_ARQUIVO, to_char(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy') AS DAT_RECEBIMENTO FROM "
									+ "	TSMI_ARQUIVO_RECEBIDO ARQ, TSMI_LINHA_ARQUIVO_REC LIN "
									+ "	WHERE ARQ.COD_IDENT_ARQUIVO = 'MR01' "
									+ "	AND ARQ.COD_ARQUIVO = LIN.COD_ARQUIVO "
									+ "	AND LIN.NUM_AUTO_INFRACAO = '"
									+ myInf.getNumAutoInfracao()
									+ "' "
									+ "	AND LIN.COD_ORGAO =  ARQ.COD_ORGAO "
									+ "	AND ARQ.COD_STATUS = 2";
							rs_Cons = stmt_Cons.executeQuery(sCmd);
							while (rs_Cons.next()) {
								myHist.setTxtComplemento05(rs_Cons.getString("NOM_ARQUIVO"));
								myHist.setTxtComplemento06(rs_Cons.getString("DAT_RECEBIMENTO"));
							}
						}
					}
					acertaComplemento(myHist);				
					historicos.add(myHist) ;
				}
			}
			myInf.setHistoricos(historicos);
			if (rs!=null) 	rs.close();	
			if (rs_Cons!=null) 	rs_Cons.close();			
			if (stmt!=null) stmt.close();	
			if (stmt_Cons!=null) stmt_Cons.close();

		}
		catch (Exception ex) {
			System.out.println("Comando:"+sComando);
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}






	/**
	 *-----------------------------------------------------------
	 * Le Historico - Transa��es 044
	 *-----------------------------------------------------------
	 */
	public  boolean LeHistorico_Ant (AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException {
		//	  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {

			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			Statement stmt1 = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	
			ResultSet rs1       = null;
			boolean continua   = true;
			String resultado   = "";
			String indContinua = "               ";
			List historicos    = new ArrayList();
			Vector h           = new Vector();


			sCmd  = " SELECT tbrk.num_auto,to_char(tbrk.dat_status,'dd/mm/yyyy')dat_status,tbrk.cod_status,tbrk.cod_evento,";
			sCmd += "tbrk.num_processo,tbrk.cod_origem_evento,tbrk.txt_username,";
			sCmd += "tbrk.cod_orgao_lotacao,to_char(tbrk.dat_processamento,'dd/mm/yyyy')dat_processamento,tbrk.txt_complemento01,";
			sCmd += "tbrk.txt_complemento02,tbrk.txt_complemento03,tbrk.txt_complemento04,";
			sCmd += "tbrk.txt_complemento05,tbrk.txt_complemento06,tbrk.txt_complemento07,";		
			sCmd += "tbrk.txt_complemento08,tbrk.txt_complemento09,tbrk.txt_complemento10,tbrk.txt_complemento11,";
			sCmd += "sta.nom_status";
			sCmd += " FROM TBRK_HISTORICO tbrk,TSMI_STATUS_AUTO sta";
			sCmd += " WHERE tbrk.cod_status = sta.cod_status" ;
			sCmd += " AND tbrk.NUM_AUTO  = '"+myInf.getNumAutoInfracao()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;	

			while (rs.next()){

				HistoricoBean myHis = new HistoricoBean();	  				  	  				  				
				myHis.setCodAutoInfracao(myInf.getCodAutoInfracao());
				myHis.setCodOrgao(myInf.getCodOrgao());			 
				myHis.setNumProcesso(rs.getString("num_processo"));
				myHis.setCodStatus(rs.getString("cod_status")); 
				myHis.setDatStatus(sys.Util.formataDataDDMMYYYY(rs.getString("dat_status")));
				myHis.setCodEvento(rs.getString("cod_evento"));	
				myHis.setCodOrigemEvento(rs.getString("cod_origem_evento"));
				myHis.setNomUserName(rs.getString("txt_username"));
				myHis.setCodOrgaoLotacao(rs.getString("cod_orgao_lotacao"));
				if ("0".equals(myHis.getCodOrgaoLotacao())) myHis.setCodOrgaoLotacao("");
				myHis.setDatProc(rs.getString("dat_processamento"));	  				  	  				  
				myHis.setTxtComplemento01(rs.getString("txt_complemento01"));
				myHis.setTxtComplemento02(rs.getString("txt_complemento02"));
				myHis.setTxtComplemento03(rs.getString("txt_complemento03"));	
				myHis.setTxtComplemento04(rs.getString("txt_complemento04"));
				myHis.setTxtComplemento05(rs.getString("txt_complemento05"));	
				myHis.setTxtComplemento06(rs.getString("txt_complemento06"));
				myHis.setTxtComplemento07(rs.getString("txt_complemento07"));
				myHis.setTxtComplemento08(rs.getString("txt_complemento08"));
				myHis.setTxtComplemento09(rs.getString("txt_complemento09"));
				myHis.setTxtComplemento10(rs.getString("txt_complemento10"));
				myHis.setTxtComplemento11(rs.getString("txt_complemento11"));						

				if ((myHis.getCodEvento().trim().length()>0) && ("000".equals(myHis.getCodEvento())==false)) {	
					sCmd  = "SELECT nom_status from TSMI_STATUS_AUTO WHERE cod_status = '"+myHis.getCodStatus()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myHis.setNomStatus(rs1.getString("nom_status"));	  				  	  				  
					}
					sCmd  = "SELECT dsc_origem_evento from TSMI_ORIGEM_EVENTO WHERE cod_origem_evento = '"+myHis.getCodOrigemEvento()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myHis.setNomOrigemEvento(rs1.getString("dsc_origem_evento"));							  				  	  				  
					}
					sCmd  = "SELECT dsc_evento from TSMI_EVENTO WHERE cod_evento = '"+myHis.getCodEvento()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myHis.setNomEvento(rs1.getString("dsc_evento"));	  				  	  				  
					}
					if ("214".equals(myHis.getCodEvento())) {	
						sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"
								+myHis.getTxtComplemento06().trim()+"'" ;
						rs1    = stmt1.executeQuery(sCmd) ;
						while (rs1.next()) {
							myHis.setTxtComplemento06(rs1.getString("nom_municipio"));	  				  	  				  
						}
						// Editar campos
						// CPF
						if (myHis.getTxtComplemento02().length()<11) myHis.setTxtComplemento02(myHis.getTxtComplemento02()+"               ") ;
						myHis.setTxtComplemento02(myHis.getTxtComplemento02().substring(0,3)+"."+
								myHis.getTxtComplemento02().substring(3,6)+"."+ 
								myHis.getTxtComplemento02().substring(6,9)+"-"+
								myHis.getTxtComplemento02().substring(9,11));
						//CEP
						if (myHis.getTxtComplemento08().length()<8) myHis.setTxtComplemento08(myHis.getTxtComplemento08()+"               ") ;
						myHis.setTxtComplemento08(myHis.getTxtComplemento08().substring(0,5)+"-"+
								myHis.getTxtComplemento08().substring(5,8));
						//CNH
						String p =myHis.getTxtComplemento09().substring(0,1);
						myHis.setTxtComplemento09(p+" - "+sys.Util.SemZeros(myHis.getTxtComplemento09().substring(1)));
					}
					acertaComplemento(myHis);
					h.addElement(myHis) ;


				}		

			}
			if (rs!=null) rs.close();		
			if (stmt!=null) stmt.close();
			if (rs1!=null) rs.close();		
			if (stmt1!=null) stmt.close();

			int t = h.size();
			for (int i=t-1;i>=0;i--) {
				HistoricoBean hh = (HistoricoBean)h.elementAt(i);
				historicos.add(hh) ;			
			}
			myInf.setHistoricos(historicos);
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}

	public  String ConsultaCodRequerimento(AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException {
		String sComando = "";
		Connection conn =null ;		
		String COD_REQUERIMENTO = "";
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt      = conn.createStatement();
			Statement stmt_Cons = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_Cons   = null;			
			
			sComando = " SELECT COD_REQUERIMENTO FROM  TSMI_REQUERIMENTO  WHERE COD_AUTO_INFRACAO ='"+myInf.getCodAutoInfracao()+"'";
			rs = stmt.executeQuery(sComando);
			while (rs.next()) {
				RequerimentoBean myReq = new RequerimentoBean();
				myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				COD_REQUERIMENTO = rs.getString("COD_REQUERIMENTO");
			}				
			if (rs!=null) 	rs.close();	
			if (rs_Cons!=null) 	rs_Cons.close();			
			if (stmt!=null) stmt.close();	
			if (stmt_Cons!=null) stmt_Cons.close();
		}
		catch (Exception ex) {
			System.out.println("Comando:"+sComando);
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return COD_REQUERIMENTO;
	}

	public  boolean LeRequerimento (AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException {
		boolean bOk     = true ;
		String sComando = "";
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt      = conn.createStatement();
			Statement stmt_Cons = conn.createStatement();
			ResultSet rs        = null;
			ResultSet rs_Cons   = null;			
			ArrayList<RequerimentoBean> requerimentos = new ArrayList<RequerimentoBean>();
			sComando="SELECT "+
					"COD_AUTO_INFRACAO," +
					"COD_REQUERIMENTO,"+
					"COD_TIPO_SOLIC,"+
					"NUM_REQUERIMENTO,"+
					"TO_CHAR(DAT_REQUERIMENTO,'DD/MM/YYYY') AS DAT_REQUERIMENTO,"+
					"COD_STATUS_REQUERIMENTO,"+
					"NOM_USERNAME_DP,"+
					"COD_ORGAO_LOTACAO_DP,"+
					"NOM_RESP_DP,"+
					"TO_CHAR(DAT_PROC_DP,'DD/MM/YYYY') AS DAT_PROC_DP,"+
					"TO_CHAR(DAT_JU,'DD/MM/YYYY') AS DAT_JU,"+
					"COD_JUNTA_JU,"+
					"COD_RELATOR_JU,"+
					"NOM_USERNAME_JU,"+
					"COD_ORGAO_LOTACAO_JU,"+
					"TO_CHAR(DAT_PROC_JU,'DD/MM/YYYY') AS DAT_PROC_JU,"+
					"DAT_RS,"+
					"COD_RESULT_RS,"+
					"TXT_MOTIVO_RS,"+
					"NOM_USERNAME_RS,"+
					"COD_ORGAO_LOTACAO_RS,"+
					"DAT_PROC_RS,"+
					"DAT_ATU_TR,"+    
					"NOM_CONDUTOR_TR,"+
					"NUM_CNH_TR,"+
					"COD_UF_CNH_TR,"+
					"NUM_CPF_TR,"+
					"COD_UF_TR,"+
					"NOM_USERNAME_TR,"+
					"COD_ORGAO_LOTACAO_TR,"+
					"DAT_PROC_TR,"+
					"DAT_PROC_EST,"+
					"IND_TIPO_CNH_TR,"+
					"DAT_ENVIO_GUIA,"+
					"NOM_USERNAME_ENVIO_GUIA,"+
					"DAT_RECEB_GUIA,"+
					"NOM_USERNAME_RECEB_GUIA,"+
					"COD_REMESSA,"+
					"COD_MOTIVO_DEFERIDO,"+
					"COD_ENVIO_PUBDO,"+
					"DAT_ENVIO_PUBDO,"+
					"DAT_PUBDO FROM TSMI_REQUERIMENTO WHERE COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"'";
			rs = stmt.executeQuery(sComando);
			while (rs.next()) {
				RequerimentoBean myReq = new RequerimentoBean();
				myReq.setCodAutoInfracao(rs.getString("COD_AUTO_INFRACAO"));
				myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				myReq.setCodTipoSolic(rs.getString("COD_TIPO_SOLIC"));
				myReq.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				myReq.setDatRequerimento(rs.getString("DAT_REQUERIMENTO"));
				myReq.setCodStatusRequerimento(rs.getString("COD_STATUS_REQUERIMENTO"));
				myReq.setNomUserNameDP(rs.getString("NOM_USERNAME_DP"));
				myReq.setCodOrgaoLotacaoDP(rs.getString("COD_ORGAO_LOTACAO_DP"));
				myReq.setNomResponsavelDP(rs.getString("NOM_RESP_DP"));
				myReq.setDatProcDP(rs.getString("DAT_PROC_DP"));
				myReq.setDatJU(rs.getString("DAT_JU"));
				myReq.setCodJuntaJU(rs.getString("COD_JUNTA_JU"));
				myReq.setCodRelatorJU(rs.getString("COD_RELATOR_JU"));
				myReq.setNomUserNameJU(rs.getString("NOM_USERNAME_JU"));
				myReq.setCodOrgaoLotacaoJU(rs.getString("COD_ORGAO_LOTACAO_JU"));
				myReq.setDatProcJU(rs.getString("DAT_PROC_JU"));
				myReq.setDatRS(rs.getString("DAT_RS"));
				myReq.setCodResultRS(rs.getString("COD_RESULT_RS"));
				myReq.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				myReq.setNomUserNameRS(rs.getString("NOM_USERNAME_RS"));
				myReq.setCodOrgaoLotacaoRS(rs.getString("COD_ORGAO_LOTACAO_RS"));
				myReq.setDatProcRS(rs.getString("DAT_PROC_RS"));
				myReq.setDatAtuTR(rs.getString("DAT_ATU_TR"));
				myReq.setNomCondutorTR(rs.getString("NOM_CONDUTOR_TR"));
				myReq.setNumCNHTR(rs.getString("NUM_CNH_TR"));
				myReq.setNumCPFTR(rs.getString("NUM_CPF_TR"));
				myReq.setCodUfTR(rs.getString("COD_UF_CNH_TR"));
				myReq.setNomUserNameTR(rs.getString("NOM_USERNAME_TR"));
				myReq.setCodOrgaoLotacaoTR(rs.getString("COD_ORGAO_LOTACAO_TR"));
				myReq.setDatProcTR(rs.getString("DAT_PROC_TR"));
				myReq.setDatProcEST(rs.getString("DAT_PROC_EST"));
				myReq.setIndTipoCNHTR(rs.getString("IND_TIPO_CNH_TR"));
				myReq.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				myReq.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
				myReq.setDatPublPDO(rs.getString("DAT_PUBDO"));
				myReq.setDatEnvioGuia(sys.Util.converteData(rs.getDate("DAT_ENVIO_GUIA")));
				myReq.setNomUserNameEnvio(rs.getString("NOM_USERNAME_ENVIO_GUIA"));
				myReq.setDatRecebimentoGuia(sys.Util.converteData(rs.getDate("DAT_RECEB_GUIA")));				
				myReq.setNomUserNameRecebimento(rs.getString("NOM_USERNAME_RECEB_GUIA"));
				myReq.setCodRemessa(rs.getString("COD_REMESSA"));
				myReq.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));
				String sComando_SQL="SELECT * FROM TSMI_JUNTA WHERE COD_JUNTA='"+myReq.getCodJuntaJU()+"'";
				rs_Cons = stmt_Cons.executeQuery(sComando_SQL);
				if (rs_Cons.next()) {
					myReq.setSigJuntaJU(rs_Cons.getString("SIG_JUNTA"));
				}
				sComando_SQL="SELECT * FROM TSMI_RELATOR WHERE NUM_CPF='"+myReq.getCodRelatorJU()+"' AND COD_JUNTA='"+myReq.getCodJuntaJU()+"'";
				rs_Cons = stmt_Cons.executeQuery(sComando_SQL);
				if (rs_Cons.next()) {
					myReq.setNomRelatorJU(rs_Cons.getString("NOM_RELATOR"));
				}
				requerimentos.add(myReq) ;
			}				
			myInf.setRequerimentos(requerimentos);
			if (rs!=null) 	rs.close();	
			if (rs_Cons!=null) 	rs_Cons.close();			
			if (stmt!=null) stmt.close();	
			if (stmt_Cons!=null) stmt_Cons.close();
		}
		catch (Exception ex) {
			System.out.println("Comando:"+sComando);
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}



	public  boolean LeRequerimento_Ant (AutoInfracaoBean myInf, UsuarioBean myUsuario) throws DaoException {
		//	  synchronized
		boolean bOk = true ;
		Connection conn =null ;					
		try {

			conn = serviceloc.getConnection(MYABREVSIST) ;	
			Statement stmt = conn.createStatement();
			Statement stmt1 = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;
			ResultSet rs1       = null;

			boolean continua   = true;

			List requerimentos = new ArrayList();
			int reg=0;

			sCmd  = " SELECT tbrk.num_auto,tbrk.txt_responsavel,tbrk.cod_status,to_char(tbrk.dat_req_abertura,'dd/mm/yyyy')dat_req_abertura,";
			sCmd += " tbrk.num_requerimento,tbrk.tip_solicitacao,tbrk.txt_user_abertura,tbrk.cod_orgao_lot_abertura,to_char(tbrk.dat_proc_abertura,'dd/mm/yyyy')dat_proc_abertura,";
			sCmd += " to_char(tbrk.dat_pj_parecer,'dd/mm/yyyy')dat_pj_parecer,tbrk.sit_parecer,tbrk.cod_resp_parecer,tbrk.txt_motivo_parecer,tbrk.txt_user_parecer,tbrk.cod_orgao_lot_parecer,";
			sCmd += " to_char(tbrk.dat_proc_parecer,'dd/mm/yyyy')dat_proc_parecer,to_char(tbrk.dat_junta_relator,'dd/mm/yyyy')dat_junta_relator,tbrk.cod_junta_relator,tbrk.cod_relator,tbrk.txt_user_relator,tbrk.cod_orgao_lot_relator,";
			sCmd += " to_char(tbrk.dat_proc_relator,'dd/mm/yyyy')dat_proc_relator,to_char(tbrk.dat_resultado,'dd/mm/yyyy')dat_resultado,tbrk.txt_resultado,tbrk.txt_motivo_resultado,tbrk.txt_user_resultado,tbrk.cod_orgao_lot_resultado,";
			sCmd += " to_char(tbrk.dat_proc_resultado,'dd/mm/yyyy')dat_proc_resultado,tbrk.txt_condutor_tri,tbrk.ind_tipo_cnh_tri,tbrk.num_cnh,tbrk.cod_uf_cnh,tbrk.num_cpf_tri,to_char(tbrk.dat_atu_tri,'dd/mm/yyyy')dat_atu_tri,tbrk.txt_user_tri,";
			sCmd += " tbrk.cod_orgao_lot_tri,to_char(tbrk.dat_proc_tri,'dd/mm/yyyy')dat_proc_tri";			
			sCmd += " FROM TBRK_REQUERIMENTO tbrk";			
			sCmd += " WHERE tbrk.NUM_AUTO  = '"+myInf.getNumAutoInfracao()+"'" ;
			/*	
			 *SELECT * FROM TSMI_RELATOR REL RIGHT OUTER JOIN TBRK_REQUERIMENTO TBRK JOIN tbrk_AUTO_INFRACAO AUTO
ON AUTO.NUM_AUTO = TBRK.NUM_AUTO ON TBRK.COD_RELATOR = REL.COD_RELATOR
WHERE AUTO.num_auto ='I30889803'
ORDER BY NUM_REQUERIMENTO DESC
			 *
			 *

			sCmd  = " SELECT * FROM TSMI_RELATOR REL RIGHT OUTER JOIN TBRK_REQUERIMENTO TBRK JOIN tbrk_AUTO_INFRACAO AUTO";
			sCmd += " ON AUTO.NUM_AUTO = TBRK.NUM_AUTO ON TBRK.COD_RELATOR = REL.COD_RELATOR";
		    sCmd += " WHERE AUTO.num_auto ='"+ myInf.getNumAutoInfracao()+"'";
			sCmd += " ORDER BY NUM_REQUERIMENTO DESC";
			 **/		
			rs = stmt.executeQuery(sCmd);


			while (rs.next()) {
				String codstat =""; 
				RequerimentoBean myReq = new RequerimentoBean();

				myReq.setCodRequerimento(Integer.toString(reg));		  
				myReq.setCodAutoInfracao(myInf.getCodAutoInfracao());				  				  	  				  
				myReq.setCodTipoSolic(rs.getString("tip_solicitacao"));			
				myReq.setCodStatusRequerimento(rs.getString("cod_status"));					
				codstat=myReq.getCodStatusRequerimento();
				if ((myReq.getCodTipoSolic().trim().length()>0) && 
						("7".equals(myReq.getCodStatusRequerimento())==false) ) {

					myReq.setNumRequerimento(rs.getString("num_requerimento"));
					myReq.setCodResponsavelDP(Integer.toString(reg));	 
					myReq.setNomResponsavelDP(rs.getString("txt_responsavel"));	  				  	  				  			 				  	  				  
					myReq.setCodStatusRequerimento(codstat);	  				  	  				  
					if ("88".equals(myReq.getCodStatusRequerimento())) myReq.setCodStatusRequerimento("0");
					// ABERTURA
					myReq.setNomUserNameDP(rs.getString("txt_user_abertura"));
					myReq.setCodOrgaoLotacaoDP(rs.getString("cod_orgao_lot_abertura"));
					myReq.setDatProcDP(rs.getString("dat_proc_abertura"));	  				  	  				  
					myReq.setDatRequerimento(rs.getString("dat_req_abertura"));					

					//PARECER JURIDICO
					String dx = (rs.getString("dat_pj_parecer"));
					if(dx == null)
						dx = "";
					if ("".equals(dx)==false) {
						myReq.setDatPJ(dx);
						myReq.setCodParecerPJ("");
						myReq.setCodRespPJ(rs.getString("cod_resp_parecer"));
						myReq.setTxtMotivoPJ(rs.getString("txt_motivo_parecer"));
						myReq.setNomUserNamePJ(rs.getString("txt_user_parecer"));
						myReq.setCodOrgaoLotacaoPJ(rs.getString("cod_orgao_lot_parecer"));
						myReq.setDatProcPJ(rs.getString("dat_proc_parecer"));	  				  	  				  
					}

					dx = (rs.getString("dat_junta_relator"));
					if(dx == null)
						dx = "";
					if ("".equals(dx)==false) {					

						// DEFINE/TROCA RELATOR				
						myReq.setDatJU(dx);
						myReq.setCodJuntaJU(rs.getString("cod_junta_relator"));
						myReq.setCodRelatorJU(rs.getString("cod_relator"));			
						myReq.setNomUserNameJU(rs.getString("txt_user_relator"));
						myReq.setCodOrgaoLotacaoJU(rs.getString("cod_orgao_lot_relator"));
						myReq.setDatProcJU(rs.getString("dat_proc_relator"));
					}

					dx = (rs.getString("dat_resultado"));
					if(dx == null)
						dx = "";
					if ("".equals(dx)==false) {		
						//						RESULTADO								
						myReq.setDatRS(dx);
						myReq.setCodResultRS(rs.getString("txt_resultado"));
						myReq.setTxtMotivoRS(rs.getString("txt_motivo_resultado"));
						myReq.setNomUserNameRS(rs.getString("txt_user_resultado"));
						myReq.setCodOrgaoLotacaoRS(rs.getString("cod_orgao_lot_resultado"));
						myReq.setDatProcRS(rs.getString("dat_resultado"));				
					}
					else setPosIni(getPosIni()+1+1000+20+6+8) ;
					// ATUALIZAR TRI
					myReq.setNomCondutorTR(rs.getString("txt_condutor_tri"));			
					myReq.setIndTipoCNHTR(rs.getString("ind_tipo_cnh_tri"));
					myReq.setNumCNHTR(rs.getString("num_cnh"));
					myReq.setCodUfCNHTR(rs.getString("cod_uf_cnh"));
					myReq.setNumCPFTR(rs.getString("num_cpf_tri"));		
					String datAtuTRI = rs.getString("dat_atu_tri");
					if(datAtuTRI != null)
						myReq.setDatAtuTR(datAtuTRI);
					myReq.setNomUserNameTR(rs.getString("txt_user_tri"));
					myReq.setCodOrgaoLotacaoTR(rs.getString("cod_orgao_lot_tri"));
					String datProcTRI = rs.getString("dat_proc_tri");
					if(datProcTRI != null)
						myReq.setDatProcTR(datProcTRI);						

					sCmd  = " SELECT I.cod_lote_relator from TSMI_ITEM_LOTE_RELATOR I, TSMI_LOTE_RELATOR L WHERE num_requerimento = '"+myReq.getNumRequerimento()+"'"+
							" AND I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR AND COD_STATUS <> 8 AND COD_STATUS <> 9 "+
							" AND L.COD_ORGAO = '"+myInf.getCodOrgao() +"'"+
							" ORDER BY COD_LOTE_RELATOR" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myReq.setNumGuiaDistr(rs.getString("cod_lote_relator"));	  				  	  				  
					}

					sCmd  = "SELECT nom_resp_parecer from TSMI_RESP_PARECER WHERE cod_resp_parecer = '"+myReq.getCodRespPJ()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myReq.setNomRespPJ(rs.getString("nom_resp_parecer"));	  				  	  				  
					}

					sCmd  = "SELECT Nom_Relator from TSMI_RELATOR WHERE cod_relator = '"+myReq.getCodRelatorJU()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myReq.setNomRelatorJU(rs.getString("Nom_Relator"));	  				  	  				  
					}					
					sCmd  = "SELECT sig_junta from TSMI_JUNTA WHERE cod_junta = '"+myReq.getCodJuntaJU()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myReq.setSigJuntaJU(rs.getString("sig_junta"));	  				  	  				  
					}
					requerimentos.add(myReq) ;
				}				
				reg++;
				myInf.setRequerimentos(requerimentos);
			}		

			if (rs!=null) 	rs.close();	
			if (rs1!=null) 	rs.close();	
			if (stmt!=null) stmt.close();	
			if (stmt1!=null)stmt1.close();	
			if (reg ==0)
				return false;	 	  
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}



	/******************************************
  ConsultaAuto e carrega no Bean - Transa��es 048
	 ******************************************/
	public  boolean ConsultaAutos (consultaAutoBean myConsult,UsuarioBean myUsuario) throws DaoException {
		boolean bOk = false;
		Connection conn = null;
		Statement stmt  = null;
		Statement stmt1 = null;		
		try 
		{	
			conn  = serviceloc.getConnection(MYABREVSIST) ;	
			stmt  = conn.createStatement();
			stmt1 = conn.createStatement();			
			ResultSet rs       = null;	
			ResultSet rs1      = null;	    
//			List <AutoInfracaoBean> autos = new <AutoInfracaoBean>ArrayList();
			List<AutoInfracaoBean> autos = new ArrayList<AutoInfracaoBean>();
			
			String sCmd="SELECT "+
			"T1.NUM_AUTO_INFRACAO, "+
			"T1.NUM_PLACA, "+
			"T1.COD_INFRACAO, "+
			"TO_CHAR(T1.DAT_INFRACAO,'DD/MM/YYYY') AS DAT_INFRACAO, "+
			"T1.NOM_PROPRIETARIO, "+
			"T1.COD_STATUS, "+
			"T2.NOM_STATUS, "+
			"T1.NUM_PROCESSO, "+
			"T1.NUM_RENAVAM, "+
			"T1.NUM_DOC_PROPRIETARIO, "+
			"T1.VAL_REAL_DESCONTO, "+
			"T1.VAL_REAL, "+
			"T1.VAL_HOR_INFRACAO, "+
			"T1.DSC_ENQUADRAMENTO, "+
			"TO_CHAR(T1.DAT_VENCIMENTO,'DD/MM/YYYY') AS DAT_VENCIMENTO, "+
			"T1.NUM_PONTO, "+
			"T1.DSC_INFRACAO, "+
			"T1.DSC_LOCAL_INFRACAO, "+
			"T1.COD_MUNICIPIO, "+
			"T4.NOM_MUNICIPIO, "+
			"T1.COD_ORGAO, "+
			"T3.SIG_ORGAO, "+			  	  				  
			"T3.NOM_ORGAO, "+						
			"T1.IND_FASE, "+	 
			"T1.IND_SITUACAO, "+	 											
			"T1.IND_PAGO, "+
			"T1.DAT_NOTIF_AUTUACAO, "+ 								 				
			"T1.COD_ENT_AUTUACAO, "+
			"T1.DAT_NOTIF_PENALIDADE, "+
			"T1.COD_ENT_PENALIDADE, "+
			"T1.DS_ATA, "+
			" T1.CD_NATUREZA" +
            " FROM TSMI_AUTO_INFRACAO T1,TSMI_STATUS_AUTO T2,TSMI_ORGAO T3,TSMI_MUNICIPIO T4 WHERE "+
			"T1.COD_STATUS=T2.COD_STATUS AND T1.COD_ORGAO=T3.COD_ORGAO AND T1.COD_MUNICIPIO=T4.COD_MUNICIPIO" +
            " AND T1.CD_NATUREZA = " + myUsuario.cdNatureza;

			if (myConsult.getNumAutoInfracao().trim().length() > 0)		 
				sCmd += " AND T1.NUM_AUTO_INFRACAO  = '"+myConsult.getNumAutoInfracao()+"'" ;

			if (myConsult.getNumPlaca().trim().length() > 0)		
				sCmd += " AND T1.NUM_PLACA  = '"+myConsult.getNumPlaca()+"'" ;

			if (myConsult.getNumCPF().trim().length() > 0)
				sCmd += " AND T1.NUM_CPF_CNPJ  = '"+myConsult.getNumCPF()+"'" ;

			if (myConsult.getNumRenavam().trim().length() > 0)	  
				sCmd += " AND T1.NUM_RENAVAM  = '"+myConsult.getNumRenavam()+"'" ;

			if (myConsult.getNumProcesso().trim().length() > 0)	  
				sCmd += " AND T1.NUM_PROCESSO  = '"+myConsult.getNumProcesso()+"'" ;

			sCmd += " order by T1.NUM_AUTO_INFRACAO desc";  		
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next())
			{
				bOk=true;			
				String resp="";
				AutoInfracaoBean myAuto = new AutoInfracaoBean();
				myAuto.setInfracao(new InfracaoBean());	
				myAuto.setProprietario(new ResponsavelBean());
				myAuto.setCondutor(new ResponsavelBean());															
				myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));				  
				VerificaAutoOracle(myAuto,conn);	
				myAuto.setCdNatureza(rs.getInt("CD_NATUREZA"));
				
				if (myAuto.getNumAutoInfracao().trim().length()>0) 
				{
					myAuto.setNumPlaca(rs.getString("NUM_PLACA"));						  
					VerificaAutoOracle(myAuto,conn);
					myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
					myAuto.getInfracao().setCodInfracao(rs.getString("COD_INFRACAO"));
					String datInfracao=rs.getString("DAT_INFRACAO");
					if(datInfracao != null){
						myAuto.setDatInfracao(datInfracao);
					}
					myAuto.getProprietario().setNomResponsavel(rs.getString("NOM_PROPRIETARIO"));
					resp = myAuto.getProprietario().getNomResponsavel();  
					myAuto.setCodStatus(rs.getString("COD_STATUS"));
					if ("88".equals(myAuto.getCodStatus())) myAuto.setCodStatus("0");							
					myAuto.setNomStatus("Status: "+myAuto.getCodStatus());					
					myAuto.setNomStatus(rs.getString("NOM_STATUS"));					
					myAuto.setNumProcesso(rs.getString("NUM_PROCESSO"));						
					myAuto.setNumRenavam(rs.getString("NUM_RENAVAM"));
					myAuto.getProprietario().setIndCpfCnpj("1");
					myAuto.getProprietario().setNumCpfCnpj(rs.getString("NUM_DOC_PROPRIETARIO"));
					if ("1".equals(myAuto.getProprietario().getIndCpfCnpj()))
						myAuto.getProprietario().setNumCpfCnpj(sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",11));	
					else 
						myAuto.getProprietario().setNumCpfCnpj(sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",14));
					myAuto.getInfracao().setValRealDesconto(rs.getString("VAL_REAL_DESCONTO"));
					myAuto.getInfracao().setValReal(rs.getString("VAL_REAL"));
					myAuto.setValHorInfracao(rs.getString("VAL_HOR_INFRACAO"));				  
					myAuto.getInfracao().setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));	
					String datVencimento=rs.getString("DAT_VENCIMENTO");
					myAuto.setDatVencimento(datVencimento); 							
					myAuto.getInfracao().setNumPonto(rs.getString("NUM_PONTO")); 
					myAuto.getInfracao().setDscInfracao(rs.getString("DSC_INFRACAO"));
					myAuto.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));	  			
					myAuto.setCodMunicipio(rs.getString("COD_MUNICIPIO"));
					myAuto.setNomMunicipio(rs.getString("NOM_MUNICIPIO"));				
					myAuto.setCodOrgao(rs.getString("COD_ORGAO"));
					myAuto.setSigOrgao(rs.getString("SIG_ORGAO")); 				  	  				  
					myAuto.setNomOrgao(rs.getString("NOM_ORGAO"));							
					myAuto.setIndFase(rs.getString("IND_FASE"));	 
					myAuto.setIndSituacao(rs.getString("IND_SITUACAO"));	 											
					myAuto.setIndPago(rs.getString("IND_PAGO"));	 
					myAuto.setDatNotifAut(rs.getString("DAT_NOTIF_AUTUACAO")); 								 				
					myAuto.setCodNotifAut(rs.getString("COD_ENT_AUTUACAO"));
					myAuto.setDatNotifPen(rs.getString("DAT_NOTIF_PENALIDADE"));
					myAuto.setCodNotifPen(rs.getString("COD_ENT_PENALIDADE"));
					myAuto.setDsAta(rs.getString("DS_ATA"));
					
					sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO='"+myAuto.getCodNotifAut()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					
					while (rs1.next()) 
					{
						myAuto.setNomMotivoNotifAut(rs1.getString("DSC_CODIGO_RETORNO"));
					}

					sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE num_codigo_retorno = '"+myAuto.getCodNotifPen()+"'" ;
					rs1    = stmt1.executeQuery(sCmd) ;
					while (rs1.next()) {
						myAuto.setNomMotivoNotifPen(rs1.getString("DSC_CODIGO_RETORNO"));
					}
					if (sys.Util.DifereDias("16/07/2004",myAuto.getDatInfracao())>=0) { 
						myAuto.setDatNotificacao(myAuto.getDatNotifPen());
						myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifPen());
					}
					else 
					{		
						if (myAuto.getCodStatus().length()>1)
						{ 
							myAuto.setDatNotificacao(myAuto.getDatNotifPen());
							myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifPen());				
						}
						else 
						{ 
							myAuto.setDatNotificacao(myAuto.getDatNotifAut()) ;
							myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifAut());				
						}
					}
					myAuto.getCondutor().setNomResponsavel(resp);						
					// Verificar se e chamda de Recebe Notificacao - entao so processa 2 ou 12
					if ("REC0680".equals(myConsult.getSigFuncao())) {
						if ( (myAuto.getCodStatus().equals("2")) || (myAuto.getCodStatus().equals("12")) ) autos.add(myAuto) ;
					}
					else autos.add(myAuto) ;
				}
			}
			myConsult.setAutos(autos);
			if (rs1 != null) rs.close();	
			if (rs != null) rs.close();
			stmt.close();
			stmt1.close();
		}
		catch (Exception ex) {

			System.out.println("Erro:"+ex.getMessage());
			bOk = false ;
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
		finally {
			if (conn != null) {
				try {
					if (stmt!=null) stmt.close();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) { }
			}
		}
		return bOk;
	}  

	public  boolean StatusAuto(consultaAutoBean myConsult,UsuarioBean myUsuario) throws DaoException{
		return true;
	}

	/**
  /******************************************
	  LeResponsavel - Transa��es 051
	 ******************************************/
	public  boolean LeResponsavel (ResponsavelBean myResp,String tpPesq) throws DaoException {
		boolean bOk     = true ;
		String sComando = "";
		Connection conn =null ;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt      = conn.createStatement();
			ResultSet rs       = null;
			sComando="SELECT "+
					"NOM_RESPONSAVEL,"+
					"COD_RESPONSAVEL,"+	
					"IND_CPF_CNPJ,"+
					"NUM_CPF_CNPJ,"+
					"NUM_CNH,"+
					"COD_UF_CNH,"+
					"SEQ_ENDERECO,"+
					"IND_TIPO_CNH "+
					"FROM TSMI_RESPONSAVEL WHERE NUM_CNH='"+myResp.getNumCnh()+"'";
			rs = stmt.executeQuery(sComando);
			while (rs.next()) 
			{
				myResp.setNomResponsavel(rs.getString("NOM_RESPONSAVEL"));
				myResp.setCodResponsavel(rs.getString("COD_RESPONSAVEL"));
				myResp.setIndCpfCnpj(rs.getString("IND_CPF_CNPJ"));
				myResp.setNumCpfCnpj(rs.getString("NUM_CPF_CNPJ"));
				myResp.setNumCnh(rs.getString("NUM_CNH"));
				myResp.setCodUfCnh(rs.getString("COD_UF_CNH"));
				myResp.setIndTipoCnh(rs.getString("IND_TIPO_CNH"));
			}
			if (rs!=null) 	rs.close();	
			if (stmt!=null) stmt.close();	
		}
		catch (Exception ex) {
			System.out.println("Comando:"+sComando);
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/******************************************
    Transa��es 204, 324
	 ******************************************/
	/**
	 *-----------------------------------------------------------
	 * Receb Notificacao - Ajusta Status - Transacao 204
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto204(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {

	}
	/**
	 *-----------------------------------------------------------
	 * Abrir Defesa Previa - 206,326,340
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto206(AutoInfracaoBean myAuto,REC.ParamOrgBean myParam,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario, Ata ata) throws DaoException {
		//	  synchronized
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			
			buscarReq("206",myAuto,myReq,myHis,myParam,conn);
			GravaAutoInfracaoLocal(myAuto, conn, ata);
			/* AtualizaCodigoBarraProcesso(myAuto); */
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn, ata);
			}
			// fechar a transa��o 
			//conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atribuir processos Raj
	 *-----------------------------------------------------------
	 */
	public void atualizarAutoProcessos(AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis, Ata ata) throws DaoException {
		//	  synchronized
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			GravaAutoInfracaoLocal(myAuto, conn, ata);
			/* AtualizaCodigoBarraProcesso(myAuto); */
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn, ata);
			}
			// fechar a transa��o 
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Requerimento com Parecer Juridico e grava Historico - Transa��es 207 
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto207(AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
	}

	/**
	 *-----------------------------------------------------------
	 * Atualiza Junta e Relator e grava Historico - Transa��es 208, 328, 335 
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto208(AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {			
		//	  synchronized
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			GravaAutoInfracaoLocal(myAuto, conn);
			/* AtualizaCodigoBarraProcesso(myAuto); */
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn);
			}
			// fechar a transa��o 
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}
		
		
		
		
		
	/**
	 *---------------------------------------
	 * Atualiza Resultado e grava Historico - 209, 329, 336
	 *---------------------------------------
	 */					
	public void atualizarAuto209(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario, Ata ata) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);
			GravaAutoInfracaoLocal(myAuto, conn, ata);
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn, ata);
			}
			conn.commit();
		} catch (Exception ex) {
			try {
				conn.rollback();
			} catch (SQLException ey) {
			}
			throw new DaoException(ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) {
				}
			}
		}
		return;
	}

		
		
		
		
	/**
	 *---------------------------------------
	 * Atualiza data de Envio para DO e Numero da CI e grava Historico
	 *     Transa��es 211, 331, 344
	 *  *---------------------------------------
	 */
	public void atualizarAuto211(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {

	}
	/**
	 *---------------------------------------
	 * Atualiza Data de envio para DO e grava Historico
	 * Transa��es 213, 333, 346
	 *---------------------------------------
	 */
	public void atualizarAuto213(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {

	}
	/**
	 *-----------------------------------------------------------
	 * executar Troca de Real Infrator - 214
	 *-----------------------------------------------------------
	 */

	public void atualizarAuto214(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,REC.ParamOrgBean myParam,UsuarioBean myUsuario) throws DaoException {
		//	  synchronized
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			buscarReq("214",myAuto,myReq,myHis,myParam,conn);
			myAuto.getCondutor().setCodResponsavel(myReq.getCodResponsavelDP());
			myAuto.getCondutor().setNomResponsavel(myReq.getNomCondutorTR());
			myAuto.getCondutor().setNumCnh(myReq.getNumCNHTR());
			myAuto.getCondutor().setIndTipoCnh(myReq.getIndTipoCNHTR());
			myAuto.getCondutor().setCodUfCnh(myReq.getCodUfCNHTR());
			myAuto.getCondutor().setNumCpfCnpj(myReq.getNumCPFTR());
			myAuto.getCondutor().getEndereco().setTxtEndereco(myReq.getTxtEnderecoTR());
			myAuto.getCondutor().getEndereco().setNomBairro(myReq.getNomBairroTR());
			myAuto.getCondutor().getEndereco().setNomCidade(myReq.getNomCidadeTR());
			myAuto.getCondutor().getEndereco().setCodCidade(myReq.getCodCidadeTR());
			myAuto.getCondutor().getEndereco().setCodUF(myReq.getCodUfTR());
			myAuto.getCondutor().getEndereco().setNumCEP(myReq.getNumCEPTREdt());
			GravaAutoInfracaoLocal(myAuto, conn);
			/* AtualizaCodigoBarraProcesso(myAuto); */
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn);
			}
			// fechar a transa��o 
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}
	/**
	 *---------------------------------------
	 * Estorno DP, 1A e 2A - 226 - 350 - 351 - 352 - 210 - 330 - 337
	 *---------------------------------------
	 */
	public void atualizarAuto226(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			GravaAutoInfracaoLocal(myAuto, conn);
			atualizarStatusReq(myReq, conn);
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn);
			}
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}

		
	
	
	public void atualizarStatusReq(RequerimentoBean myReq, Connection conn)	throws sys.DaoException {
		ResultSet rs   = null;
		Statement stmt = null;
		try {
			String codStatus = "";
			stmt = conn.createStatement();
			String sCmd = "SELECT * FROM TSMI_REQUERIMENTO WHERE COD_AUTO_INFRACAO='"+myReq.getCodAutoInfracao()+" AND NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next())	codStatus = rs.getString("COD_STATUS_REQUERIMENTO");
			if (codStatus.equals("2")) myReq.setCodStatusRequerimento("0");
			if (codStatus.equals("3")) myReq.setCodStatusRequerimento("2");
			stmt.close();
			rs.close();
		} catch (Exception ex) {
			throw new sys.DaoException("daoBroker: " + ex.getMessage());
		} finally {
			if (conn != null) {
				try {
					if (stmt != null)
						stmt.close();
				} catch (Exception ey) {
				}
			}
		}
	}

	
	
	
	
	public void atualizarAuto264(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {			
	}


	/**-----------------------------------------------------------
	 * Transa��o 250 - Atualiza data de vencimento do Auto
 ------------------------------------------------------------- */	 
	public void atualizaDataVenc(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,String novaData)throws DaoException {
		//	  synchronized
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			
	  		HistoricoBean myHis = new HistoricoBean() ;
	  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  			myHis.setNumProcesso(myAuto.getNumProcesso());
	  		myHis.setCodStatus(myAuto.getCodStatus());  
  			myHis.setDatStatus(myAuto.getDatStatus());
  			myHis.setCodEvento("250");
	  		myHis.setCodOrigemEvento("100");		
  			myHis.setNomUserName(myUsrLog.getNomUserName());
  			myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
	  		myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
	  		myHis.setTxtComplemento01(novaData);
			myAuto.setDatVencimento(novaData);
			GravaAutoInfracaoLocal(myAuto, conn);
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Historico */
				GravarHistorico(myHis, conn);
			}
			// fechar a transa��o 
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}

	public void atualizarAuto261(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {			
		Connection conn =null ;					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			// iniciar transa��o
			conn.setAutoCommit(false);
			GravaAutoInfracaoLocal(myAuto, conn);
			if (myAuto.getCodAutoInfracao().length() > 0) {
				/* Gravar Requerimento */
				GravarRequerimento(myReq, conn);
				/* Gravar Historico */
				GravarHistorico(myHis, conn);
			}
			conn.commit();
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ; 
	}


	/**
	 *---------------------------------------
	 * Atualiza data de Envio para Publica��o por Requerimento
	 *     Transa��es 263, 383, 390
	 *  *---------------------------------------
	 */
	public void atualizarAuto263(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 // iniciar transa��o
			 conn.setAutoCommit(false);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 if (myAuto.getCodAutoInfracao().length() > 0) {
				 /* Gravar Requerimento */
				 GravarRequerimento(myReq, conn);
				 /* Gravar Historico */
				 GravarHistorico(myHis, conn);
			 }
			 conn.commit();
		 }
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ; 
	 }

		
	 public void atualizarAuto382(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 // iniciar transa��o
			 conn.setAutoCommit(false);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 if (myAuto.getCodAutoInfracao().length() > 0) {
				 /* Gravar Requerimento */
				 GravarRequerimento(myReq, conn);
				 /* Gravar Historico */
				 GravarHistorico(myHis, conn);
			 }
			 conn.commit();
		 }
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ; 
	 }
		 
		 
		 
		 
		 
		 
		 /**
	  *-----------------------------------------------------------
	  * Movimento para Banco - Transa��es 407 
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto407(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {					

	 }

	 /*-----------------------------------------------------------
	  * Atualiza Numero do Processo e grava Historico - 410 
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto410(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {		
			//	  synchronized
			Connection conn =null ;					
			try {
				conn = serviceloc.getConnection(MYABREVSIST) ;	
				// iniciar transa��o
				conn.setAutoCommit(false);
				myAuto.setNumProcesso(myHis.getTxtComplemento01());				
				myAuto.setDatProcesso(myHis.getTxtComplemento02());
				GravaAutoInfracaoLocal(myAuto, conn);
				if (myAuto.getCodAutoInfracao().length() > 0) {
					/* Gravar Historico */
					GravarHistorico(myHis, conn);
				}
				// fechar a transa��o 
				conn.commit();
			}
			catch (Exception ex) {
				try {  conn.rollback(); }
				catch (SQLException ey) { }	  
				throw new DaoException(ex.getMessage());
			}
			finally {
				if (conn != null) {
					try { serviceloc.setReleaseConnection(conn); }
					catch (Exception ey) { }
				}
			}
			return ; 
		}

	 /*-----------------------------------------------------------
	  * Atualiza Status e grava Historico - 411 
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto411(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
			//	  synchronized
			Connection conn =null ;					
			try {
				conn = serviceloc.getConnection(MYABREVSIST) ;	
				// iniciar transa��o
				conn.setAutoCommit(false);
				myAuto.setCodStatus(myHis.getTxtComplemento01());
				myAuto.setDatStatus(myHis.getTxtComplemento02());				
				GravaAutoInfracaoLocal(myAuto, conn);
				if (myAuto.getCodAutoInfracao().length() > 0) {
					/* Gravar Historico */
					GravarHistorico(myHis, conn);
				}
				// fechar a transa��o 
				conn.commit();
			}
			catch (Exception ex) {
				try {  conn.rollback(); }
				catch (SQLException ey) { }	  
				throw new DaoException(ex.getMessage());
			}
			finally {
				if (conn != null) {
					try { serviceloc.setReleaseConnection(conn); }
					catch (Exception ey) { }
				}
			}
			return ; 
		}

	 /*---------------------------------------------
	  * Ajuste Automatico de CR da Venda - 412 
	  *-----------------------------------------------
	  */
	 public ArrayList ajustarCrVenda412(AutoInfracaoBean myAuto,ACSS.UsuarioBean myUsrLog)throws DaoException {
		 ArrayList resulTrans412 = new ArrayList();
		 return resulTrans412;
	 }
	 /**-----------------------------------------------------------
	  * Transa��o 413 - Ajuste manual do Responsavel pelos Pontos
 ------------------------------------------------------------- */	 
	 public void ajustarManual(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,HistoricoBean myHist)throws DaoException {
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 // iniciar transa��o
			 conn.setAutoCommit(false);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 if (myAuto.getCodAutoInfracao().length() > 0) {
				 /* Gravar Historico */
				 GravarHistorico(myHist, conn);
			 }
			 conn.commit();
		 }
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ; 
	 }

	 /**-----------------------------------------------------------
	  * Transa��o 414 - Ajuste CPF
------------------------------------------------------------- */	 
	 public void ajustarCPF(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,String indResp)throws DaoException {
	 }

	 /**-----------------------------------------------------------
	  * Transa��o 415 - Ajuste manual do Responsavel pela Pec�nia
------------------------------------------------------------- */	 
	 public void ajustarManualRespPecunia(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,HistoricoBean myHis)throws DaoException {
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 // iniciar transa��o
			 conn.setAutoCommit(false);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 if (myAuto.getCodAutoInfracao().length() > 0) {
				 /* Gravar Historico */
				 GravarHistorico(myHis, conn);
			 }
			 conn.commit();
		 }
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ; 
	 }

		 
		 
		 
		 
		 
	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
	  * @param conn Conex�o com a base de dados 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	  */

	 public String Transacao086(String codOrgao, String codJunta, String codAcao, 
			 String indContinua,	String nomJunta, String sigJunta, String tpJunta, String txtEnd, 
			 String numEnd, String txtCompl, String nomBairro, String numCep, 
			 String codMun, String numDddTel, String numTel, String numFax, Connection conn) throws DaoException {

		 return Transacao086(codOrgao, codJunta, codAcao, indContinua, nomJunta,
				 sigJunta, tpJunta,txtEnd, numEnd, txtCompl, nomBairro, numCep,
				 codMun, numDddTel, numTel, numFax);
	 }

	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	  */

	 public String Transacao086(String codOrgao, String codJunta, String codAcao, 
			 String indContinua,	String nomJunta, String sigJunta, String tpJunta,String txtEnd, 
			 String numEnd, String txtCompl, String nomBairro, String numCep, 
			 String codMun, String numDddTel, String numTel, String numFax) throws DaoException {
		 String resultado = "";

		 return resultado;
	 }


	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	  */

	 public String Transacao087(String codOrgao, String codJunta, String numCPF, 
			 String codAcao, String indContinua,	String nomRelator) throws DaoException {
		 String resultado = "";

		 return resultado;
	 }


	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Respons�vel pelo parecer Jur�dico a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String
	  */

	 public String Transacao088(String codOrgao, String numCPF, 
			 String codAcao, String indContinua,	String nomResponsavel) throws DaoException {
		 String resultado = "";

		 return resultado;
	 }

	 public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua) 
			 throws DaoException {
		 String resultado = "";
		 return resultado;
	 }

	 public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
			 throws DaoException {
		 String resultado = "";
		 return resultado;
	 }



	 public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk = false ;
		 return bOk;  
	 }
	 public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg) throws DaoException {

		 boolean bOK = false ;	
		 return bOK;
	 }

	 public void acertaComplemento (HistoricoBean myHis) throws DaoException {
	 }
	 /******************************************
  Verifica se o Auto ja esta no Oracle
	  ******************************************/
	 public void VerificaAutoOracle (AutoInfracaoBean myAuto,Connection conn) throws DaoException { 

	 }

	 public boolean consultaAutoSemAr (ConsultaAutosSemDigiBean myAutosSemDig,ACSS.UsuarioBean myUsrLog) throws DaoException{
		 return true;
	 }
	 public void buscarReq(String tran,AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,ParamOrgBean myParam,Connection conn) throws DaoException {
		 try {
			 // Pegar o Assunto
			 String nAss = "";
			 if ("1P,1C,2P,2C".indexOf(myReq.getCodTipoSolic())>=0) nAss = myParam.getParamOrgao("COD_ASSUNTO_PENALIDADE","4110","3") ;			
			 else nAss = myParam.getParamOrgao("COD_ASSUNTO_DEF_PREVIA","4110","1")	;

			 // pegar numero de processo e requerimento
			 String sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
					 " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
			 Statement stmt = conn.createStatement();			
			 ResultSet rs  = stmt.executeQuery(sCmd) ;
			 int numUltProc = 0;
			 boolean naoExisteNumeracao  = true ;
			 boolean naoExisteNumeracao2 = true ;		
			 while (rs.next()) {
				 numUltProc = rs.getInt("VAL_PARAMETRO");	  			 
				 naoExisteNumeracao	= false;			  
			 }
			 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
					 " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";	
			 rs  = stmt.executeQuery(sCmd) ;
			 int numUltReq  = 0;
			 while (rs.next()) {
				 numUltReq  = rs.getInt("VAL_PARAMETRO");
				 naoExisteNumeracao2	= false;			  
			 }
			 if ((naoExisteNumeracao) || (naoExisteNumeracao2))	{
				 //System.err.println("naoExisteNumeracao: "+naoExisteNumeracao2+" "+naoExisteNumeracao);		
				 throw new DaoException("Parametros para numera��o de processo n�o localizado para o Org�o: "+myAuto.getCodOrgao());
			 }
			 // atualizar NUMERO DE PROCESSO DO Auto de Infracao, se ja nao houver
			 String nProc = myAuto.getNumProcesso() ;
			 if (nProc.length()==0) {
				 numUltProc++;
				 nProc = "000000"+String.valueOf(numUltProc) ;
				 if(nAss.length()>0)
					 nProc = myParam.getParamOrgao("COD_SECRETARIA","SEC","2")+"/"+nProc.substring(nProc.length()-6,nProc.length())+"/"+nAss+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2");
				 else
					 nProc = myParam.getParamOrgao("COD_SECRETARIA","SEC","2")+"/"+nProc.substring(nProc.length()-6,nProc.length())+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2");

				 if(nProc.length()>20) nProc=nProc.substring(0,20);
				 // select para ver se existe o numero de processo na TSMI_AUTO_INFRACAO
				 // se existir avanca o contador ate nao achar - fazer +- 200/1000 vezes
				 // 
				 myAuto.setNumProcesso(nProc) ;
				 //			myAuto.setDatProcesso(myAuto.getDatStatus()) ;
				 myAuto.setDatProcesso(sys.Util.formatedToday().substring(0,10)); 
			 }
			 myHis.setNumProcesso(myAuto.getNumProcesso());
			 // atualizar NUMERO DE PROCESSO DO Auto de Infracao, se ja nao houver
			 sCmd = "UPDATE TSMI_AUTO_INFRACAO set COD_STATUS='"+myAuto.getCodStatus()+"',DAT_STATUS=to_date('"+myAuto.getDatStatus()+"','dd/mm/yyyy'), ";
			 sCmd += " NUM_PROCESSO='"+myAuto.getNumProcesso()+"',DAT_PROCESSO = to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),";
			 sCmd += " TXT_EMAIL='"+myHis.getTxtComplemento12()+"' ";		
			 sCmd += " where cod_auto_infracao='"+myAuto.getCodAutoInfracao()+"'";	
			 myAuto.setTxtEmail(myHis.getTxtComplemento12());
			 stmt.execute(sCmd);
			 // gravar no requerimentos
			 numUltReq++		;
			 String nReq = "000000"+String.valueOf(numUltReq) ;			    
			 //System.err.println("NumRequerimento:antes "+nReq );				
			 nReq = myParam.getParamOrgao("COD_REQ_ORGAO","REQ","2")+"/"+nReq.substring(nReq.length()-6,nReq.length())+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2") ;
			 
			 if(myReq.getCodTipoSolic().equals("DP"))
				 myReq.setNumRequerimento(String.format("%s%s%s", "REQ_", myAuto.getNumProcesso(), "_00"));
			 else if(myReq.getCodTipoSolic().equals("1P"))
				 myReq.setNumRequerimento(String.format("%s%s%s", "REQ_", myAuto.getNumProcesso(), "_01"));
			 else if(myReq.getCodTipoSolic().equals("2P"))
				 myReq.setNumRequerimento(String.format("%s%s%s", "REQ_", myAuto.getNumProcesso(), "_02"));
			 else if(myReq.getCodTipoSolic().equals("PAE"))
				 myReq.setNumRequerimento(String.format("%s%s%s", "REQ_", myAuto.getNumProcesso(), "_03"));
			 
			 //System.err.println("NumRequerimento: "+nReq );		
			 if ("206".equals(tran)) myHis.setTxtComplemento01(myReq.getNumRequerimento());
			 else myHis.setTxtComplemento11(myReq.getNumRequerimento());
			 // Update na Numeracao			
			 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltProc+"'" +
					 " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"'";		
			 stmt.execute(sCmd);
			 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltReq+"'" +
					 " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"'";		

			 stmt.execute(sCmd);		

		 }	
		 catch (SQLException sqle) {
			 System.out.println("Comando:"+sqle);



			 throw new DaoException(sqle.getMessage());
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }//fim do catch
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }

		 return ;		
	 }

	 /**-----------------------------------------------------------
	  * Inicio da Rotina de incremento autom�tico do 
	  * n� de Processo e do n� de Requerimento
	  * -----------------------------------------------------------
	  * @throws DaoException
	  * @throws ServiceLocatorException
	  * @throws SQLException
	  * @author wellem
	  * */
	 public void  IncrementaProcesso(AutoInfracaoBean myAuto,ParamOrgBean myParam,RequerimentoBean myReq,HistoricoBean myHis,String sErro) throws DaoException, ServiceLocatorException, SQLException {

		 Connection conn =null ;
		 ResultSet rs = null;	
		 conn = serviceloc.getConnection(MYABREVSIST);
		 Statement stmt = conn.createStatement();
		 conn.setAutoCommit(false);
		 try{
			 String sCmd  = "";
			 int numProc  = 0;
			 int numReq  = 0;

			 if (sErro.equals("039"))
			 {
				 //Select para pegar o �ltimo n�mero de processo
				 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " ;
				 sCmd +=" where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
				 rs    = stmt.executeQuery(sCmd) ;
				 while (rs.next()) {
					 //Nesse momento ocorre o incremento do processo

					 numProc= Integer.parseInt(rs.getString("VAL_PARAMETRO"))+1;
				 }
				 //Atualizo o �ltimo n�mero de processo j� com o incremento
				 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numProc+"'";
				 sCmd += " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"'";
				 stmt.execute(sCmd);
			 }

			 if (sErro.equals("718"))
			 {
				 //Select para pegar o �ltimo n�mero de Requerimento
				 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " ;
				 sCmd +=" where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
				 rs    = stmt.executeQuery(sCmd) ;
				 while (rs.next()) {
					 //Nesse momento ocorre o incremento do Requerimento
					 numReq= Integer.parseInt(rs.getString("VAL_PARAMETRO"))+1 ;
				 }
				 ////Atualizo o �ltimo n�mero de Requerimento j� com o incremento
				 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numReq+"'";
				 sCmd += " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"'";
				 stmt.execute(sCmd);
			 }	

			 rs.close();	
			 conn.commit();

			 if (conn != null) stmt.close();
		 }
		 catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }//fim do catch
		 finally {
			 if (conn != null) {
				 try { 
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ;
	 }

	 public boolean LeAutoInfracaoLocal(AutoInfracaoBean myAuto, String tipo) throws DaoException {
		 return true;
	 }



	 /****************************************************
	  * M�todos para acessar o Auto de Infra��o no Oracle
	  ****************************************************/
	 public void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto)
			 throws DaoException {
		 Connection conn = null;
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 conn.commit();
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 serviceloc.setReleaseConnection(conn);
				 } catch (Exception e) {
					 System.out.println(e);
				 }
			 }
		 }
	 }

	 public void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto, Connection conn, Ata ata)
			 throws DaoException, SQLException {
		 String sqlIns = "";
		 String sqlComando = "";

		 PreparedStatement stmSel = null;
		 PreparedStatement stmIns = null;
		 Statement stmtComando = null;
		 Statement stmSeq = null;

		 ResultSet rsSel = null;
		 ResultSet rsComando = null;
		 ResultSet rsSeq = null;

		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 // Verifica se o auto existe
			 String sqlSel = "SELECT NUM_AUTO_INFRACAO FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = :NUM_AUTO_INFRACAO";
			 stmSel = conn.prepareStatement(sqlSel);
			 stmSel.setString(1, myAuto.getNumAutoInfracao());
			rsSel = stmSel.executeQuery();
			 if (!rsSel.next()) {
				 rsSel.close();
				 // Carrega a sequence do auto
				 String sqlSeq = "SELECT SEQ_TSMI_AUTO_INFRACAO.NEXTVAL CODIGO FROM DUAL";
				 stmSeq = conn.createStatement();
				 rsSeq = stmSeq.executeQuery(sqlSeq);
				 if (rsSeq.next()) {
					 myAuto.setCodAutoInfracao(rsSeq.getString("CODIGO"));
				 }
				 stmSeq.close();

				 sqlIns = "INSERT INTO TSMI_AUTO_INFRACAO ";
				 sqlIns += "(COD_AUTO_INFRACAO,";
				 sqlIns += "NUM_AUTO_INFRACAO,";
				 sqlIns += "DAT_INFRACAO,";
				 sqlIns += "VAL_HOR_INFRACAO,";
				 sqlIns += "DSC_LOCAL_INFRACAO,";
				 sqlIns += "NUM_PLACA,";
				 sqlIns += "NUM_PROCESSO,";
				 sqlIns += "DAT_PROCESSO,";
				 sqlIns += "COD_AGENTE,";
				 sqlIns += "COD_ORGAO,";
				 sqlIns += "DSC_LOCAL_APARELHO,";
				 sqlIns += "NUM_CERTIF_AFER_APARELHO,";
				 /*
				  * sqlIns += "DAT_ULT_AFER_APARELHO,";
				  */
				 sqlIns += "NUM_IDENT_APARELHO,";
				 sqlIns += "NUM_INMETRO_APARELHO,";
				 sqlIns += "COD_TIPO_DISP_REGISTRADOR,";
				 sqlIns += "DAT_VENCIMENTO,";
				 sqlIns += "COD_MUNICIPIO,";
				 sqlIns += "VAL_VELOC_PERMITIDA,";
				 sqlIns += "VAL_VELOC_AFERIDA,";
				 sqlIns += "VAL_VELOC_CONSIDERADA,";
				 sqlIns += "DSC_SITUACAO,";
				 sqlIns += "IND_CONDUTOR_IDENTIFICADO,";
				 sqlIns += "NUM_LOTE,";
				 sqlIns += "COD_STATUS,";
				 sqlIns += "DAT_STATUS,";
				 sqlIns += "DAT_PUBDO,";
				 sqlIns += "COD_ENVIO_PUBDO,";
				 sqlIns += "DAT_ENVDO,";
				 sqlIns += "IND_TP_CV,";
				 sqlIns += "COD_RESULT_PUBDO,";
				 sqlIns += "COD_BARRA,";
				 sqlIns += "IND_SITUACAO,";
				 sqlIns += "IND_FASE,";
				 sqlIns += "IND_PAGO,";
				 sqlIns += "NUM_IDENT_ORGAO,";
				 sqlIns += "DSC_RESUMO_INFRACAO,";
				 sqlIns += "DSC_MARCA_MODELO_APARELHO,";
				 sqlIns += "DSC_LOCAL_INFRACAO_NOTIF,";
				 sqlIns += "COD_INFRACAO,";
				 sqlIns += "DSC_INFRACAO,";
				 sqlIns += "DSC_ENQUADRAMENTO,";
				 sqlIns += "VAL_REAL,";
				 sqlIns += "NUM_PONTO,";
				 sqlIns += "DSC_COMPETENCIA,";
				 sqlIns += "IND_COND_PROP,";
				 sqlIns += "IND_SUSPENSAO,";
				 sqlIns += "VAL_REAL_DESCONTO,";
				 sqlIns += "COD_MARCA_MODELO,";
				 sqlIns += "COD_ESPECIE,";
				 sqlIns += "COD_CATEGORIA,";
				 sqlIns += "COD_TIPO,";
				 sqlIns += "COD_COR,";
				 sqlIns += "NOM_PROPRIETARIO,";
				 sqlIns += "TIP_DOC_PROPRIETARIO,";
				 sqlIns += "NUM_DOC_PROPRIETARIO,";
				 sqlIns += "TIP_CNH_PROPRIETARIO,";
				 sqlIns += "NUM_CNH_PROPRIETARIO,";
				 sqlIns += "DSC_END_PROPRIETARIO,";
				 sqlIns += "NUM_END_PROPRIETARIO,";
				 sqlIns += "DSC_COMP_PROPRIETARIO,";
				 sqlIns += "COD_MUN_PROPRIETARIO,";
				 sqlIns += "NUM_CEP_PROPRIETARIO,";
				 sqlIns += "UF_PROPRIETARIO,";
				 sqlIns += "NOM_CONDUTOR,";
				 sqlIns += "TIP_DOC_CONDUTOR,";
				 sqlIns += "NUM_DOC_CONDUTOR,";
				 sqlIns += "TIP_CNH_CONDUTOR,";
				 sqlIns += "NUM_CNH_CONDUTOR,";
				 sqlIns += "DSC_END_CONDUTOR,";
				 sqlIns += "NUM_END_CONDUTOR,";
				 sqlIns += "DSC_COMP_CONDUTOR,";
				 sqlIns += "COD_MUN_CONDUTOR,";
				 sqlIns += "NUM_CEP_CONDUTOR,";
				 sqlIns += "UF_CONDUTOR,";
				 sqlIns += "NUM_NOTIF_AUTUACAO,";
				 sqlIns += "DAT_NOTIF_AUTUACAO,";
				 sqlIns += "COD_ENT_AUTUACAO,";
				 sqlIns += "NUM_LOTE_AUTUACAO,";
				 sqlIns += "NUM_NOTIF_PENALIDADE,";
				 sqlIns += "DAT_NOTIF_PENALIDADE,";
				 sqlIns += "COD_ENT_PENALIDADE,";
				 sqlIns += "NUM_LOTE_PENALIDADE,";
				 sqlIns += "DSC_AGENTE,";
				 sqlIns += "DSC_UNIDADE,";
				 sqlIns += "DAT_PAGAMENTO,";
				 sqlIns += "NUM_AUTO_ORIGEM,";
				 sqlIns += "COD_INFRACAO_ORIGEM,";
				 sqlIns += "IND_TIPO_AUTO,";
				 sqlIns += "NUM_AUTO_RENAINF,";
				 sqlIns += "TXT_EMAIL,";
				 sqlIns += "DAT_INCLUSAO,";
				 sqlIns += "DAT_REGISTRO)";
				 sqlIns += " VALUES (:COD_AUTO_INFRACAO,";
				 sqlIns += ":NUM_AUTO_INFRACAO,";
				 sqlIns += ":DAT_INFRACAO,";
				 sqlIns += ":VAL_HOR_INFRACAO,";
				 sqlIns += ":DSC_LOCAL_INFRACAO,";
				 sqlIns += ":NUM_PLACA,";
				 sqlIns += ":NUM_PROCESSO,";
				 sqlIns += ":DAT_PROCESSO,";
				 sqlIns += ":COD_AGENTE,";
				 sqlIns += ":COD_ORGAO,";// 10
				 sqlIns += ":DSC_LOCAL_APARELHO,";
				 sqlIns += ":NUM_CERTIF_AFER_APARELHO,";
				 /*
				  * sqlIns += ":DAT_ULT_AFER_APARELHO,";
				  */
				 sqlIns += ":NUM_IDENT_APARELHO,";
				 sqlIns += ":NUM_INMETRO_APARELHO,";
				 sqlIns += ":COD_TIPO_DISP_REGISTRADOR,";
				 sqlIns += ":DAT_VENCIMENTO,";
				 sqlIns += ":COD_MUNICIPIO,";
				 sqlIns += ":VAL_VELOC_PERMITIDA,";
				 sqlIns += ":VAL_VELOC_AFERIDA,";
				 sqlIns += ":VAL_VELOC_CONSIDERADA,";
				 sqlIns += ":DSC_SITUACAO,";
				 sqlIns += ":IND_CONDUTOR_IDENTIFICADO,";
				 sqlIns += ":NUM_LOTE,";
				 sqlIns += ":COD_STATUS,";
				 sqlIns += ":DAT_STATUS,";
				 sqlIns += ":DAT_PUBDO,";
				 sqlIns += ":COD_ENVIO_PUBDO,";
				 sqlIns += ":DAT_ENVDO,";
				 sqlIns += ":IND_TP_CV,";
				 sqlIns += ":COD_RESULT_PUBDO,";
				 sqlIns += ":COD_BARRA,";
				 sqlIns += ":IND_SITUACAO,";
				 sqlIns += ":IND_FASE,";
				 sqlIns += ":IND_PAGO,";
				 sqlIns += ":NUM_IDENT_ORGAO,";
				 sqlIns += ":DSC_RESUMO_INFRACAO,";
				 sqlIns += ":DSC_MARCA_MODELO_APARELHO,";
				 sqlIns += ":DSC_LOCAL_INFRACAO_NOTIF,";
				 sqlIns += ":COD_INFRACAO,";
				 sqlIns += ":DSC_INFRACAO,";
				 sqlIns += ":DSC_ENQUADRAMENTO,";
				 sqlIns += ":VAL_REAL,";
				 sqlIns += ":NUM_PONTO,";
				 sqlIns += ":DSC_COMPETENCIA,";
				 sqlIns += ":IND_COND_PROP,";
				 sqlIns += ":IND_SUSPENSAO,";
				 sqlIns += ":VAL_REAL_DESCONTO,";
				 sqlIns += ":COD_MARCA_MODELO,";
				 sqlIns += ":COD_ESPECIE,";
				 sqlIns += ":COD_CATEGORIA,";
				 sqlIns += ":COD_TIPO,";
				 sqlIns += ":COD_COR,";
				 sqlIns += ":NOM_PROPRIETARIO,";
				 sqlIns += ":TIP_DOC_PROPRIETARIO,";
				 sqlIns += ":NUM_DOC_PROPRIETARIO,";
				 sqlIns += ":TIP_CNH_PROPRIETARIO,";
				 sqlIns += ":NUM_CNH_PROPRIETARIO,";
				 sqlIns += ":DSC_END_PROPRIETARIO,";
				 sqlIns += ":NUM_END_PROPRIETARIO,";
				 sqlIns += ":DSC_COMP_PROPRIETARIO,";
				 sqlIns += ":COD_MUN_PROPRIETARIO,";
				 sqlIns += ":NUM_CEP_PROPRIETARIO,";
				 sqlIns += ":UF_PROPRIETARIO,";
				 sqlIns += ":NOM_CONDUTOR,";
				 sqlIns += ":TIP_DOC_CONDUTOR,";
				 sqlIns += ":NUM_DOC_CONDUTOR,";
				 sqlIns += ":TIP_CNH_CONDUTOR,";
				 sqlIns += ":NUM_CNH_CONDUTOR,";
				 sqlIns += ":DSC_END_CONDUTOR,";
				 sqlIns += ":NUM_END_CONDUTOR,";
				 sqlIns += ":DSC_COMP_CONDUTOR,";
				 sqlIns += ":COD_MUN_CONDUTOR,";
				 sqlIns += ":NUM_CEP_CONDUTOR,";
				 sqlIns += ":UF_CONDUTOR,";
				 sqlIns += ":NUM_NOTIF_AUTUACAO,";
				 sqlIns += ":DAT_NOTIF_AUTUACAO,";
				 sqlIns += ":COD_ENT_AUTUACAO,";
				 sqlIns += ":NUM_LOTE_AUTUACAO,";
				 sqlIns += ":NUM_NOTIF_PENALIDADE,";
				 sqlIns += ":DAT_NOTIF_PENALIDADE,";
				 sqlIns += ":COD_ENT_PENALIDADE,";
				 sqlIns += ":NUM_LOTE_PENALIDADE,";
				 sqlIns += ":DSC_AGENTE,";
				 sqlIns += ":DSC_UNIDADE,";
				 sqlIns += ":DAT_PAGAMENTO,";
				 sqlIns += ":NUM_AUTO_ORIGEM,";
				 sqlIns += ":COD_INFRACAO_ORIGEM,";
				 sqlIns += ":IND_TIPO_AUTO,";
				 sqlIns += ":NUM_AUTO_RENAINF,";
				 sqlIns += ":TXT_EMAIL,";
				 sqlIns += ":DAT_INCLUSAO,";
				 sqlIns += ":DAT_REGISTRO)";

				 stmIns = conn.prepareStatement(sqlIns);
				 converteInt(myAuto.getCodAutoInfracao(), 1, stmIns);
				 stmIns.setString(2, myAuto.getNumAutoInfracao());
				 stmIns.setDate(3, converteData(myAuto.getDatInfracao()));



				 stmIns.setString(4, myAuto.getValHorInfracao());
				 stmIns.setString(5, myAuto.getDscLocalInfracao());
				 stmIns.setString(6, myAuto.getNumPlaca());
				 stmIns.setString(7, myAuto.getNumProcesso());
				 stmIns.setDate(8, converteData(myAuto.getDatProcesso()));



				 stmIns.setString(9, myAuto.getCodAgente());
				 converteInt(myAuto.getCodOrgao(), 10, stmIns);
				 stmIns.setString(11, myAuto.getDscLocalAparelho());
				 stmIns.setString(12, myAuto.getNumCertAferAparelho());
				 // stmIns.setDate(13,
				 // converteData(myAuto.getDatUltAferAparelho()));
				 stmIns.setString(13, myAuto.getNumIdentAparelho());
				 stmIns.setString(14, myAuto.getNumInmetroAparelho());
				 stmIns.setString(15, myAuto.getCodTipDispRegistrador());
				 stmIns.setDate(16, converteData(myAuto.getDatVencimento()));
				 converteInt(myAuto.getCodMunicipio(), 17, stmIns);
				 converteInt(myAuto.getValVelocPermitida(), 18, stmIns);
				 converteDouble(myAuto.getValVelocAferida(), 19, stmIns);
				 converteDouble(myAuto.getValVelocConsiderada(), 20, stmIns);
				 stmIns.setString(21, myAuto.getDscSituacao());
				 stmIns.setString(22, myAuto.getIndCondutorIdentificado());
				 stmIns.setString(23, myAuto.getCodLote());
				 converteInt(myAuto.getCodStatus(), 24, stmIns);
				 stmIns.setDate(25, converteData(myAuto.getDatStatus()));
				 stmIns.setDate(26, converteData(myAuto.getDatPublPDO()));
				 stmIns.setString(27, myAuto.getCodEnvioDO());
				 stmIns.setDate(28, converteData(myAuto.getDatEnvioDO()));
				 stmIns.setString(29, myAuto.getIndTPCV());
				 stmIns.setString(30, myAuto.getCodResultPubDO());
				 stmIns.setString(31, myAuto.getCodBarra());
				 stmIns.setString(32, myAuto.getIndSituacao());
				 stmIns.setString(33, myAuto.getIndFase());
				 stmIns.setString(34, myAuto.getIndPago());
				 stmIns.setString(35, myAuto.getIdentOrgao());
				 stmIns.setString(36, myAuto.getDscResumoInfracao());
				 stmIns.setString(37, myAuto.getDscMarcaModeloAparelho());
				 stmIns.setString(38, myAuto.getDscLocalInfracaoNotificacao());
				 converteInt(myAuto.getInfracao().getCodInfracao(), 39, stmIns);
				 stmIns.setString(40, myAuto.getInfracao().getDscInfracao());
				 stmIns.setString(41, myAuto.getInfracao().getDscEnquadramento());
				 converteDouble(myAuto.getInfracao().getValReal(), 42, stmIns);
				 stmIns.setString(43, myAuto.getInfracao().getNumPonto());
				 stmIns.setString(44, myAuto.getInfracao().getDscCompetencia());
				 stmIns.setString(45, myAuto.getInfracao().getIndTipo());
				 stmIns.setString(46, myAuto.getInfracao().getIndSDD());
				 converteDouble(myAuto.getInfracao().getValRealDesconto(), 47,
						 stmIns);
				 converteInt(myAuto.getVeiculo().getCodMarcaModelo(), 48, stmIns);
				 converteInt(myAuto.getVeiculo().getCodEspecie(), 49, stmIns);
				 converteInt(myAuto.getVeiculo().getCodCategoria(), 50, stmIns);
				 converteInt(myAuto.getVeiculo().getCodTipo(), 51, stmIns);
				 converteInt(myAuto.getVeiculo().getCodCor(), 52, stmIns);
				 stmIns.setString(53, myAuto.getProprietario()
						 .getNomResponsavel());
				 stmIns.setString(54, myAuto.getProprietario().getIndCpfCnpj());
				 stmIns.setString(55, myAuto.getProprietario().getNumCpfCnpj());
				 stmIns.setString(56, myAuto.getProprietario().getIndTipoCnh());
				 stmIns.setString(57, myAuto.getProprietario().getNumCnh());
				 stmIns.setString(58, myAuto.getProprietario().getEndereco()
						 .getTxtEndereco());
				 stmIns.setString(59, myAuto.getProprietario().getEndereco()
						 .getNumEndereco());
				 stmIns.setString(60, myAuto.getProprietario().getEndereco()
						 .getTxtComplemento());
				 converteInt(myAuto.getProprietario().getEndereco()
						 .getCodCidade(), 61, stmIns);
				 stmIns.setString(62, myAuto.getProprietario().getEndereco()
						 .getNumCEP());
				 stmIns.setString(63, myAuto.getProprietario().getEndereco()
						 .getCodUF());
				 stmIns.setString(64, myAuto.getCondutor().getNomResponsavel());
				 stmIns.setString(65, myAuto.getCondutor().getIndCpfCnpj());
				 stmIns.setString(66, myAuto.getCondutor().getNumCpfCnpj());
				 stmIns.setString(67, myAuto.getCondutor().getIndTipoCnh());
				 stmIns.setString(68, myAuto.getCondutor().getNumCnh());
				 stmIns.setString(69, myAuto.getCondutor().getEndereco()
						 .getTxtEndereco());
				 stmIns.setString(70, myAuto.getCondutor().getEndereco()
						 .getNumEndereco());
				 stmIns.setString(71, myAuto.getCondutor().getEndereco()
						 .getTxtComplemento());
				 converteInt(myAuto.getCondutor().getEndereco().getCodCidade(),
						 72, stmIns);
				 stmIns.setString(73, myAuto.getCondutor().getEndereco()
						 .getNumCEP());
				 stmIns.setString(74, myAuto.getCondutor().getEndereco()
						 .getCodUF());
				 converteInt(myAuto.getNumNotifAut(), 75, stmIns);
				 stmIns.setDate(76, converteData(myAuto.getDatNotifAut()));
				 stmIns.setString(77, myAuto.getCodNotifAut());
				 stmIns.setString(78, myAuto.getNumARNotifAut());
				 converteInt(myAuto.getNumNotifPen(), 79, stmIns);
				 stmIns.setDate(80, converteData(myAuto.getDatNotifPen()));
				 stmIns.setString(81, myAuto.getCodNotifPen());
				 stmIns.setString(82, myAuto.getNumARNotifPen());
				 stmIns.setString(83, myAuto.getDscAgente());
				 stmIns.setString(84, myAuto.getDscUnidade());
				 stmIns.setDate(85, converteData(myAuto.getDatPag()));
				 stmIns.setString(86, myAuto.getNumAutoOrigem());
				 converteInt(myAuto.getNumInfracaoOrigem(), 87, stmIns);
				 stmIns.setString(88, myAuto.getIdentAutoOrigem());
				 stmIns.setString(89, myAuto.getNumInfracaoRenainf());
				 stmIns.setString(90, myAuto.getTxtEmail());
				 stmIns.setDate(91, converteData(myAuto.getDatInclusao()));
				 stmIns.setDate(92, converteData(myAuto.getDatRegistro()));
				 stmIns.executeUpdate();
				 stmIns.close();

			 } else {
				 stmtComando = conn.createStatement();
				 sqlComando = "SELECT * FROM TSMI_INFRACAO WHERE COD_INFRACAO='"
						 + myAuto.getInfracao().getCodInfracao() + "'";
				 rsComando = stmtComando.executeQuery(sqlComando);
				 if (!rsComando.next()) {
					 sqlComando = "INSERT INTO TSMI_INFRACAO (COD_INFRACAO,DSC_INFRACAO,NUM_PONTO) VALUES "
							 + "('"
							 + myAuto.getInfracao().getCodInfracao()
							 + "','',0)";
					 stmtComando.execute(sqlComando);
				 }
				 rsComando.close();
				 rsSel.close();

				 if (myAuto.getDatAlteracao().equals("")) {
					 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					 myAuto.setDatAlteracao(df.format(new Date()));
				 }
				 if (myAuto.getDatProcesso() == null)
					 myAuto.setDatProcesso("");
				 if (myAuto.getDatUltAferAparelho() == null)
					 myAuto.setDatUltAferAparelho("");
				 if (myAuto.getDatVencimento() == null)
					 myAuto.setDatVencimento("");
				 if (myAuto.getDatPublPDO() == null)
					 myAuto.setDatPublPDO("");
				 if (myAuto.getDatEnvioDO() == null)
					 myAuto.setDatEnvioDO("");
				 if (myAuto.getDatNotifAut() == null)
					 myAuto.setDatNotifAut("");
				 if (myAuto.getDatNotifPen() == null)
					 myAuto.setDatNotifPen("");
				 if (myAuto.getDatPag() == null)
					 myAuto.setDatPag("");
				 double valVelPermitida = 0;
				 try {
					 valVelPermitida = Double.parseDouble(myAuto
							 .getValVelocPermitida());
				 } catch (Exception e) {
					 valVelPermitida = 0;
				 }
				 double valVelAferida = 0;
				 try {
					 valVelAferida = Double.parseDouble(myAuto
							 .getValVelocAferida());
				 } catch (Exception e) {
					 valVelAferida = 0;
				 }
				 double valVelConsiderada = 0;
				 try {
					 valVelConsiderada = Double.parseDouble(myAuto
							 .getValVelocConsiderada());
				 } catch (Exception e) {
					 valVelConsiderada = 0;
				 }
				 double valReal = 0;
				 try {
					 valReal = Double.parseDouble(myAuto.getInfracao()
							 .getValReal());
				 } catch (Exception e) {
					 valReal = 0;
				 }
				 double valRealDesconto = 0;
				 try {
					 valRealDesconto = Double.parseDouble(myAuto.getInfracao()
							 .getValRealDesconto());
				 } catch (Exception e) {
					 valRealDesconto = 0;
				 }

				 sqlComando = "UPDATE TSMI_AUTO_INFRACAO SET ";
				 sqlComando += "DAT_INFRACAO          = to_date('"
						 + myAuto.getDatInfracao() + "','dd/mm/yyyy'),";
				 sqlComando += "VAL_HOR_INFRACAO      = '"
						 + myAuto.getValHorInfracao() + "',";
				 sqlComando += "DSC_LOCAL_INFRACAO    = '"
						 + myAuto.getDscLocalInfracao() + "',";
				 sqlComando += "NUM_PLACA             = '"
						 + myAuto.getNumPlaca() + "',";
				 sqlComando += "NUM_PROCESSO          = '"
						 + myAuto.getNumProcesso() + "',";
				 sqlComando += "DAT_PROCESSO          = to_date('"
						 + myAuto.getDatProcesso() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_AGENTE            = '"
						 + myAuto.getCodAgente() + "',";
				 sqlComando += "COD_ORGAO             = '"
						 + myAuto.getCodOrgao() + "',";
				 sqlComando += "DSC_LOCAL_APARELHO    = '"
						 + myAuto.getDscLocalAparelho() + "',";
				 sqlComando += "NUM_CERTIF_AFER_APARELHO  = '"
						 + myAuto.getNumCertAferAparelho() + "',";
				 /*
				  * sqlComando +=
				  * "DAT_ULT_AFER_APARELHO = to_date('"+myAuto.getDatUltAferAparelho
				  * ()+"','dd/mm/yyyy'),";
				  */
				 
				 //Raj
				 
				//if(req.getParameter("acao").equals("InformaResult")){
				// myAuto.setCodStatus("10");
				//}
				 
				 sqlComando += "NUM_IDENT_APARELHO    = '"
						 + myAuto.getNumIdentAparelho() + "',";
				 sqlComando += "NUM_INMETRO_APARELHO  = '"
						 + myAuto.getNumInmetroAparelho() + "',";
				 sqlComando += "COD_TIPO_DISP_REGISTRADOR = '"
						 + myAuto.getCodTipDispRegistrador() + "',";
				 sqlComando += "DAT_VENCIMENTO        = to_date('"
						 + myAuto.getDatVencimento() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_MUNICIPIO         = '"
						 + myAuto.getCodMunicipio() + "',";
				 sqlComando += "VAL_VELOC_PERMITIDA   =  " + valVelPermitida
						 + ",";
				 sqlComando += "VAL_VELOC_AFERIDA     =  " + valVelAferida + ",";
				 sqlComando += "VAL_VELOC_CONSIDERADA =  " + valVelConsiderada
						 + ",";
				 sqlComando += "DSC_SITUACAO          = '"
						 + myAuto.getDscSituacao() + "',";
				 sqlComando += "IND_CONDUTOR_IDENTIFICADO = '"
						 + myAuto.getIndCondutorIdentificado() + "',";
				 sqlComando += "NUM_LOTE              = '" + myAuto.getCodLote()
						 + "',";
				 sqlComando += "COD_STATUS            = '"
						 + myAuto.getCodStatus() + "',";
				 sqlComando += "DAT_STATUS            = to_date('"
						 + myAuto.getDatStatus() + "','dd/mm/yyyy'),";
				 sqlComando += "DAT_PUBDO             = to_date('"
						 + myAuto.getDatPublPDO() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_ENVIO_PUBDO       = '"
						 + myAuto.getCodEnvioDO() + "',";
				 sqlComando += "DAT_ENVDO             = to_date('"
						 + myAuto.getDatEnvioDO() + "','dd/mm/yyyy'),";
				 sqlComando += "IND_TP_CV             = '" + myAuto.getIndTPCV()
						 + "',";
				 sqlComando += "COD_RESULT_PUBDO      = '"
						 + myAuto.getCodResultPubDO() + "',";
				 sqlComando += "COD_BARRA             = '"
						 + myAuto.getCodBarra() + "',";
				 sqlComando += "IND_SITUACAO          = '"
						 + myAuto.getIndSituacao() + "',";
				 sqlComando += "IND_FASE              = '" + myAuto.getIndFase()
						 + "',";
				 sqlComando += "IND_PAGO              = '" + myAuto.getIndPago()
						 + "',";
				 sqlComando += "NUM_IDENT_ORGAO       = '"
						 + myAuto.getIdentOrgao() + "',";
				 sqlComando += "DSC_RESUMO_INFRACAO   = '"
						 + myAuto.getDscResumoInfracao() + "',";
				 sqlComando += "DSC_MARCA_MODELO_APARELHO = '"
						 + myAuto.getDscMarcaModeloAparelho() + "',";
				 sqlComando += "DSC_LOCAL_INFRACAO_NOTIF  = '"
						 + myAuto.getDscLocalInfracaoNotificacao() + "',";
				 sqlComando += "COD_INFRACAO          = '"
						 + myAuto.getInfracao().getCodInfracao() + "',";
				 sqlComando += "DSC_INFRACAO          = '"
						 + myAuto.getInfracao().getDscInfracao() + "',";
				 sqlComando += "DSC_ENQUADRAMENTO     = '"
						 + myAuto.getInfracao().getDscEnquadramento() + "',";
				 sqlComando += "VAL_REAL              =  " + valReal + ",";
				 sqlComando += "NUM_PONTO             = '"
						 + myAuto.getInfracao().getNumPonto() + "',";
				 sqlComando += "DSC_COMPETENCIA       = '"
						 + myAuto.getInfracao().getDscCompetencia() + "',";
				 sqlComando += "IND_COND_PROP         = '"
						 + myAuto.getInfracao().getIndTipo() + "',";
				 sqlComando += "IND_SUSPENSAO         = '"
						 + myAuto.getInfracao().getIndSDD() + "',";
				 sqlComando += "VAL_REAL_DESCONTO     =  " + valRealDesconto
						 + ",";
				 sqlComando += "COD_MARCA_MODELO      = '"
						 + myAuto.getVeiculo().getCodMarcaModelo() + "',";
				 sqlComando += "COD_ESPECIE           = '"
						 + myAuto.getVeiculo().getCodEspecie() + "',";
				 sqlComando += "COD_CATEGORIA         = '"
						 + myAuto.getVeiculo().getCodCategoria() + "',";
				 sqlComando += "COD_TIPO              = '"
						 + myAuto.getVeiculo().getCodTipo() + "',";
				 sqlComando += "COD_COR               = '"
						 + myAuto.getVeiculo().getCodCor() + "',";
				 sqlComando += "NOM_PROPRIETARIO      = '"
						 + myAuto.getProprietario().getNomResponsavel() + "',";
				 sqlComando += "TIP_DOC_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getIndCpfCnpj() + "',";
				 sqlComando += "NUM_DOC_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getNumCpfCnpj() + "',";
				 sqlComando += "TIP_CNH_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getIndTipoCnh() + "',";
				 sqlComando += "NUM_CNH_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getNumCnh() + "',";
				 sqlComando += "DSC_END_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getEndereco()
						 .getTxtEndereco() + "',";
				 sqlComando += "NUM_END_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getEndereco()
						 .getNumEndereco() + "',";
				 sqlComando += "DSC_COMP_PROPRIETARIO = '"
						 + myAuto.getProprietario().getEndereco()
						 .getTxtComplemento() + "',";
				 sqlComando += "COD_MUN_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getEndereco().getCodCidade()
						 + "',";
				 sqlComando += "NUM_CEP_PROPRIETARIO  = '"
						 + myAuto.getProprietario().getEndereco().getNumCEP()
						 + "',";
				 sqlComando += "UF_PROPRIETARIO       = '"
						 + myAuto.getProprietario().getEndereco().getCodUF()
						 + "',";
				 sqlComando += "NOM_CONDUTOR          = '"
						 + myAuto.getCondutor().getNomResponsavel() + "',";
				 sqlComando += "TIP_DOC_CONDUTOR      = '"
						 + myAuto.getCondutor().getIndCpfCnpj() + "',";
				 sqlComando += "NUM_DOC_CONDUTOR      = '"
						 + myAuto.getCondutor().getNumCpfCnpj() + "',";
				 sqlComando += "TIP_CNH_CONDUTOR      = '"
						 + myAuto.getCondutor().getIndTipoCnh() + "',";
				 sqlComando += "NUM_CNH_CONDUTOR      = '"
						 + myAuto.getCondutor().getNumCnh() + "',";
				 sqlComando += "DSC_END_CONDUTOR      = '"
						 + myAuto.getCondutor().getEndereco().getTxtEndereco()
						 + "',";
				 sqlComando += "NUM_END_CONDUTOR      = '"
						 + myAuto.getCondutor().getEndereco().getNumEndereco()
						 + "',";
				 sqlComando += "DSC_COMP_CONDUTOR     = '"
						 + myAuto.getCondutor().getEndereco()
						 .getTxtComplemento() + "',";
				 sqlComando += "COD_MUN_CONDUTOR      = '"
						 + myAuto.getCondutor().getEndereco().getCodCidade()
						 + "',";
				 sqlComando += "NUM_CEP_CONDUTOR      = '"
						 + myAuto.getCondutor().getEndereco().getNumCEP() + "',";
				 sqlComando += "UF_CONDUTOR           = '"
						 + myAuto.getCondutor().getEndereco().getCodUF() + "',";
				 sqlComando += "NUM_NOTIF_AUTUACAO    = '" + myAuto.getNumNotifAut() + "',";
				 sqlComando += "DAT_NOTIF_AUTUACAO    = to_date('" + myAuto.getDatNotifAut() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_ENT_AUTUACAO      = '" + myAuto.getCodNotifAut() + "',";
				 sqlComando += "NUM_LOTE_AUTUACAO     = '" + myAuto.getNumARNotifAut() + "',";
				 sqlComando += "NUM_NOTIF_PENALIDADE  = '" + myAuto.getNumNotifPen() + "',";
				 sqlComando += "DAT_NOTIF_PENALIDADE  = to_date('" + myAuto.getDatNotifPen() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_ENT_PENALIDADE    = '" + myAuto.getCodNotifPen() + "',";
				 sqlComando += "NUM_LOTE_PENALIDADE   = '" + myAuto.getNumARNotifPen() + "',";
				 sqlComando += "DSC_AGENTE            = '"
						 + myAuto.getDscAgente() + "',";
				 sqlComando += "DSC_UNIDADE           = '"
						 + myAuto.getDscUnidade() + "',";
				 sqlComando += "DAT_PAGAMENTO         = to_date('"
						 + myAuto.getDatPag() + "','dd/mm/yyyy'),";
				 sqlComando += "NUM_AUTO_ORIGEM       = '"
						 + myAuto.getNumAutoOrigem() + "',";
				 sqlComando += "COD_INFRACAO_ORIGEM   = '" + myAuto.getNumInfracaoOrigem() + "',";
				 sqlComando += "IND_TIPO_AUTO         = '" + myAuto.getIdentAutoOrigem() + "',";
				 sqlComando += "NUM_AUTO_RENAINF      = '" + myAuto.getNumInfracaoRenainf() + "',";
				 sqlComando += "TXT_EMAIL             = '" + myAuto.getTxtEmail() + "',";
				 sqlComando += "DAT_INCLUSAO          = to_date('"
						 + myAuto.getDatInclusao() + "','dd/mm/yyyy'),";
				 sqlComando += "DAT_ALTERACAO         = to_date('"
						 + myAuto.getDatAlteracao() + "','dd/mm/yyyy'),";
				 sqlComando += "DAT_REGISTRO          = to_date('"
						 + myAuto.getDatRegistro() + "','dd/mm/yyyy'),";
				 sqlComando += "COD_ATA  = "
						 + ata.getCodAta() + ",";
				 sqlComando += "DS_ATA  = '"
						 + ata.getDescAta() + "'";
				 sqlComando += " WHERE NUM_AUTO_INFRACAO = '"
						 + myAuto.getNumAutoInfracao() + "'";
				 stmtComando.executeUpdate(sqlComando);
				 stmtComando.close();
			 }
			 stmSel.close();
		 } catch (SQLException e) {
			 System.out.println("* SQL Inv�lido : sqlIns:" + sqlIns
					 + " sqlComando:" + sqlComando);
			 throw new DaoException(e.getMessage());
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (stmSel != null)
				 stmSel.close();
			 if (stmIns != null)
				 stmIns.close();
			 if (stmtComando != null)
				 stmtComando.close();
			 if (stmSeq != null)
				 stmSeq.close();
			 if (rsSel != null)
				 rsSel.close();
			 if (rsComando != null)
				 rsComando.close();
			 if (rsSeq != null)
				 rsSeq.close();
		 }
	 }

	 public  boolean LeRequerimentoLocal (AutoInfracaoBean myInf, Connection conn) throws sys.DaoException {
		 return true;
	 }

	 public void GravarRequerimento(RequerimentoBean myReq, Connection conn)
			 throws DaoException {
		 String sCmd = "";
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 Statement stmt = conn.createStatement();
			 sCmd = "SELECT * FROM TSMI_REQUERIMENTO WHERE "
					 + "COD_AUTO_INFRACAO='" + myReq.getCodAutoInfracao()
					 + "' and " + "NUM_REQUERIMENTO='"
					 + myReq.getNumRequerimento() + "'";
			 ResultSet rs = stmt.executeQuery(sCmd);
			 if (rs.next()) {
				 sCmd = "UPDATE TSMI_REQUERIMENTO SET " + "COD_TIPO_SOLIC='"
						 + myReq.getCodTipoSolic()
						 + "',"
						 + "DAT_REQUERIMENTO=to_date('"
						 + myReq.getDatRequerimento()
						 + "','dd/mm/yyyy'),"
						 + "COD_STATUS_REQUERIMENTO='"
						 + myReq.getCodStatusRequerimento()
						 + "',"
						 + "NOM_USERNAME_DP='"
						 + myReq.getNomUserNameDP()
						 + "',"
						 + "COD_ORGAO_LOTACAO_DP='"
						 + myReq.getCodOrgaoLotacaoDP()
						 + "',"
						 + "NOM_RESP_DP='"
						 + myReq.getNomResponsavelDP()
						 + "',"
						 + "DAT_PROC_DP=to_date('"
						 + myReq.getDatProcDP()
						 + "','dd/mm/yyyy'),"
						 + "DAT_PJ=to_date('"
						 + myReq.getDatPJ()
						 + "','dd/mm/yyyy'),"
						 + "COD_PARECER_PJ='"
						 + myReq.getCodParecerPJ()
						 + "',"
						 + "COD_RESP_PJ='"
						 + myReq.getCodRespPJ()
						 + "',"
						 + "TXT_MOTIVO_PJ='"
						 + myReq.getTxtMotivoPJ()
						 + "',"
						 + "NOM_USERNAME_PJ='"
						 + myReq.getNomUserNamePJ()
						 + "',"
						 + "COD_ORGAO_LOTACAO_PJ='"
						 + myReq.getCodOrgaoLotacaoPJ()
						 + "',"
						 + "DAT_PROC_PJ=to_date('"
						 + myReq.getDatProcPJ()
						 + "','dd/mm/yyyy'),"
						 + "DAT_JU=to_date('"
						 + myReq.getDatJU()
						 + "','dd/mm/yyyy'), "
						 + "COD_JUNTA_JU='"
						 + myReq.getCodJuntaJU()
						 + "',"
						 + "COD_RELATOR_JU='"
						 + myReq.getCodRelatorJU()
						 + "',"
						 + "NOM_USERNAME_JU='"
						 + myReq.getNomUserNameJU()
						 + "',"
						 + "COD_ORGAO_LOTACAO_JU='"
						 + myReq.getCodOrgaoLotacaoJU()
						 + "',"
						 + "DAT_PROC_JU=to_date('"
						 + myReq.getDatProcJU()
						 + "','dd/mm/yyyy'),"
						 + "DAT_RS=to_date('"
						 + myReq.getDatRS()
						 + "','dd/mm/yyyy'), "
						 + "COD_RESULT_RS='"
						 + myReq.getCodResultRS()
						 + "',"
						 + "TXT_MOTIVO_RS='"
						 + myReq.getTxtMotivoRS()
						 + "',"
						 + "NOM_USERNAME_RS='"
						 + myReq.getNomUserNameRS()
						 + "',"
						 + "COD_ORGAO_LOTACAO_RS='"
						 + myReq.getCodOrgaoLotacaoRS()
						 + "',"
						 + "DAT_PROC_RS=to_date('"
						 + myReq.getDatProcRS()
						 + "','dd/mm/yyyy'),"
						 + "NOM_CONDUTOR_TR='"
						 + myReq.getNomCondutorTR()
						 + "',"
						 + "IND_TIPO_CNH_TR='"
						 + myReq.getIndTipoCNHTR()
						 + "',"
						 + "NUM_CNH_TR='"
						 + myReq.getNumCNHTR()
						 + "',"
						 + "COD_UF_CNH_TR='"
						 + myReq.getCodUfCNHTR()
						 + "',"
						 + "NUM_CPF_TR='"
						 + myReq.getNumCPFTR()
						 + "',"
						 + "DAT_ATU_TR=to_date('"
						 + myReq.getDatAtuTR()
						 + "','dd/mm/yyyy'),"
						 + "NOM_USERNAME_TR='"
						 + myReq.getNomUserNameTR()
						 + "',"
						 + "COD_ORGAO_LOTACAO_TR="
						 + (myReq.getCodOrgaoLotacaoTR().equals("0") ? "null"
								 : "'" + myReq.getCodOrgaoLotacaoTR() + "'")
								 + "," + "SIT_PROTOCOLO='" + myReq.getSitProtocolo()
								 + "'," + "COD_ENVIO_PUBDO= '" + myReq.getCodEnvioDO()
								 + "'," + "DAT_ENVIO_PUBDO= to_date('"
								 + myReq.getDatEnvioDO() + "','dd/mm/yyyy'),"
								 + "DAT_PUBDO= to_date('" + myReq.getDatPublPDO()
								 + "','dd/mm/yyyy'), " + "COD_MOTIVO_DEFERIDO= '"
								 + myReq.getCodMotivoDef() + "' " + "WHERE "
								 + "COD_AUTO_INFRACAO='" + myReq.getCodAutoInfracao()
								 + "' and " + "NUM_REQUERIMENTO='"
								 + myReq.getNumRequerimento() + "'";
			 } else {
				 sCmd = "INSERT INTO TSMI_REQUERIMENTO " + "(COD_REQUERIMENTO,"
						 + "COD_AUTO_INFRACAO," + "COD_TIPO_SOLIC,"
						 + "NUM_REQUERIMENTO," + "DAT_REQUERIMENTO,"
						 + "COD_STATUS_REQUERIMENTO," + "NOM_USERNAME_DP,"
						 + "COD_ORGAO_LOTACAO_DP," + "NOM_RESP_DP,"
						 + "DAT_PROC_DP," + "DAT_PJ," + "COD_PARECER_PJ,"
						 + "COD_RESP_PJ," + "TXT_MOTIVO_PJ,"
						 + "NOM_USERNAME_PJ," + "COD_ORGAO_LOTACAO_PJ,"
						 + "DAT_PROC_PJ," + "DAT_JU," + "COD_JUNTA_JU,"
						 + "COD_RELATOR_JU," + "NOM_USERNAME_JU,"
						 + "COD_ORGAO_LOTACAO_JU," + "DAT_PROC_JU," + "DAT_RS,"
						 + "COD_RESULT_RS," + "TXT_MOTIVO_RS,"
						 + "NOM_USERNAME_RS," + "COD_ORGAO_LOTACAO_RS,"
						 + "DAT_PROC_RS," + "NOM_CONDUTOR_TR,"
						 + "IND_TIPO_CNH_TR," + "NUM_CNH_TR," + "COD_UF_CNH_TR,"
						 + "NUM_CPF_TR," + "DAT_ATU_TR," + "NOM_USERNAME_TR,"
						 + "COD_ORGAO_LOTACAO_TR," + "SIT_PROTOCOLO,"
						 + "COD_ENVIO_PUBDO," + "DAT_ENVIO_PUBDO,"
						 + "DAT_PUBDO," + "COD_MOTIVO_DEFERIDO, COD_ATA) " + " VALUES ("
						 + " seq_tsmi_requerimento.nextval," + " '"
						 + myReq.getCodAutoInfracao()
						 + "',"
						 + "'"
						 + myReq.getCodTipoSolic()
						 + "',"
						 + " '"
						 + myReq.getNumRequerimento()
						 + "',"
						 + " to_date('"
						 + myReq.getDatRequerimento()
						 + "','dd/mm/yyyy'),"
						 + " '"
						 + myReq.getCodStatusRequerimento()
						 + "',"
						 + " '"
						 + myReq.getNomUserNameDP()
						 + "',"
						 + " '"
						 + myReq.getCodOrgaoLotacaoDP()
						 + "',"
						 + " '"
						 + myReq.getNomResponsavelDP()
						 + "',"
						 + " to_date('"
						 + myReq.getDatProcDP()
						 + "','dd/mm/yyyy'),"
						 + " to_date('"
						 + myReq.getDatPJ()
						 + "','dd/mm/yyyy'),"
						 + " '"
						 + myReq.getCodParecerPJ()
						 + "',"
						 + " '"
						 + myReq.getCodRespPJ()
						 + "',"
						 + " '"
						 + myReq.getTxtMotivoPJ()
						 + "',"
						 + " '"
						 + myReq.getNomUserNamePJ()
						 + "',"
						 + " '"
						 + myReq.getCodOrgaoLotacaoPJ()
						 + "',"
						 + " to_date('"
						 + myReq.getDatProcPJ()
						 + "','dd/mm/yyyy'),"
						 + " to_date('"
						 + myReq.getDatJU()
						 + "','dd/mm/yyyy'), "
						 + " '"
						 + myReq.getCodJuntaJU()
						 + "',"
						 + " '"
						 + myReq.getCodRelatorJU()
						 + "',"
						 + " '"
						 + myReq.getNomUserNameJU()
						 + "',"
						 + " '"
						 + myReq.getCodOrgaoLotacaoJU()
						 + "',"
						 + " to_date('"
						 + myReq.getDatProcJU()
						 + "','dd/mm/yyyy'),"
						 + " to_date('"
						 + myReq.getDatRS()
						 + "','dd/mm/yyyy'), "
						 + " '"
						 + myReq.getCodResultRS()
						 + "',"
						 + " '"
						 + myReq.getTxtMotivoRS()
						 + "',"
						 + " '"
						 + myReq.getNomUserNameRS()
						 + "',"
						 + " '"
						 + myReq.getCodOrgaoLotacaoRS()
						 + "',"
						 + " to_date('"
						 + myReq.getDatProcRS()
						 + "','dd/mm/yyyy'),"
						 + " '"
						 + myReq.getNomCondutorTR()
						 + "',"
						 + " '"
						 + myReq.getIndTipoCNHTR()
						 + "',"
						 + " '"
						 + myReq.getNumCNHTR()
						 + "',"
						 + "'"
						 + myReq.getCodUfCNHTR()
						 + "',"
						 + " '"
						 + myReq.getNumCPFTR()
						 + "',"
						 + " to_date('"
						 + myReq.getDatAtuTR()
						 + "','dd/mm/yyyy'),"
						 + " '"
						 + myReq.getNomUserNameTR()
						 + "',"
						 + (myReq.getCodOrgaoLotacaoTR().equals("0") ? "null"
								 : "'" + myReq.getCodOrgaoLotacaoTR() + "'")
								 + ","
								 + " '"
								 + myReq.getSitProtocolo()
								 + "',"
								 + " '"
								 + myReq.getCodEnvioDO()
								 + "',"
								 + " to_date('"
								 + myReq.getDatEnvioDO()
								 + "','dd/mm/yyyy'),"
								 + " to_date('"
								 + myReq.getDatPublPDO()
								 + "','dd/mm/yyyy'),"
								 + " '"
								 + myReq.getCodMotivoDef()
								 + "', "
								 + myReq.getCodAta() + ")";
			 }
			 
			 stmt.executeQuery(sCmd);
			 conn.commit();
			 stmt.close();
			 rs.close();
		 } catch (SQLException e) {
			 System.out.println("* SQL Inv�lido :" + sCmd);
			 throw new DaoException(e.getMessage());
		 } catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return;
	 }

	 public void GravarHistorico(AutoInfracaoBean myInf) throws DaoException,
	 ServiceLocatorException, SQLException {
		 Connection conn = null;
		 ResultSet rs = null;
		 String sCmd = "";
		 conn = serviceloc.getConnection(MYABREVSIST);
		 Statement stmt = conn.createStatement();
		 conn.setAutoCommit(false);
		 try {
			 sCmd = "DELETE FROM TSMI_HISTORICO_AUTO WHERE Cod_Auto_Infracao='"
					 + myInf.getCodAutoInfracao() + "'";
			 stmt.executeQuery(sCmd);
			 Iterator it = myInf.getHistoricos().iterator();
			 REC.HistoricoBean myhist = new REC.HistoricoBean();
			 while (it.hasNext()) {
				 myhist = (REC.HistoricoBean) it.next();
				 GravarHistorico(myhist, conn);
			 }
			 conn.commit();
			 if (rs != null)
				 rs.close();
			 if (conn != null)
				 stmt.close();
		 } catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn);
				 } catch (Exception ey) {
				 }
			 }
		 }
	 }

	 public void GravarHistorico(HistoricoBean myHis, Connection conn, Ata ata)
			 throws DaoException {
		 String sCmd = "";
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 sCmd = "INSERT INTO TSMI_HISTORICO_AUTO " + "(COD_HISTORICO_AUTO,"
					 + "COD_AUTO_INFRACAO," + "NUM_PROCESSO," + "COD_STATUS,"
					 + "DAT_STATUS," + "COD_EVENTO," + "COD_ORIGEM_EVENTO,"
					 + "NOM_USERNAME," + "COD_ORGAO_LOTACAO," + "DAT_PROC,"
					 + "TXT_COMPLEMENTO_01," + "TXT_COMPLEMENTO_02,"
					 + "TXT_COMPLEMENTO_03," + "TXT_COMPLEMENTO_04,"
					 + "TXT_COMPLEMENTO_05," + "TXT_COMPLEMENTO_06,"
					 + "TXT_COMPLEMENTO_07," + "TXT_COMPLEMENTO_08,"
					 + "TXT_COMPLEMENTO_09," + "TXT_COMPLEMENTO_10,"
					 + "TXT_COMPLEMENTO_11," + "TXT_COMPLEMENTO_12, DS_ATA, COD_ATA) "
					 + " VALUES (" + " seq_tsmi_historico_auto.nextval," + " '"
					 + myHis.getCodAutoInfracao()
					 + "',"
					 + " '"
					 + myHis.getNumProcesso()
					 + "',"
					 + " '"
					 + myHis.getCodStatus()
					 + "',"
					 + " to_date('"
					 + myHis.getDatStatus()
					 + "','dd/mm/yyyy'), "
					 + " '"
					 + myHis.getCodEvento()
					 + "',"
					 + " '"
					 + myHis.getCodOrigemEvento()
					 + "',"
					 + " '"
					 + myHis.getNomUserName()
					 + "',"
					 + " '"
					 + myHis.getCodOrgaoLotacao()
					 + "',"
					 + " to_date('"
					 + myHis.getDatProc()
					 + "','dd/mm/yyyy'), "
					 + " '"
					 + myHis.getTxtComplemento01()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento02()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento03()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento04()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento05()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento06()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento07()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento08()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento09()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento10()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento11()
					 + "',"
					 + " '"
					 + myHis.getTxtComplemento12() +
					 "','" + ata.getDescAta() +
					 "'," + ata.getCodAta() +")";
			 Statement stmt = conn.createStatement();
			 System.err.println("gravarHistorico: "+sCmd );
			 stmt.execute(sCmd);
			 stmt.close();
		 } catch (SQLException e) {
			 System.out.println("* SQL Inv�lido :"+sCmd);
			 throw new DaoException(e.getMessage());
		 } catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return;
	 }


	 public void  gravarHistorico(HistoricoBean myHist) throws DaoException, ServiceLocatorException, SQLException {
		 String sCmd = "";
		 Connection conn = null;
		 conn = serviceloc.getConnection(MYABREVSIST);
		 Statement stmt = conn.createStatement();
		 conn.setAutoCommit(false);
		 try {
			 sCmd = "INSERT INTO TSMI_HISTORICO_AUTO " + "(COD_HISTORICO_AUTO,"
					 + "COD_AUTO_INFRACAO," + "NUM_PROCESSO," + "COD_STATUS,"
					 + "DAT_STATUS," + "COD_EVENTO," + "COD_ORIGEM_EVENTO,"
					 + "NOM_USERNAME," + "COD_ORGAO_LOTACAO," + "DAT_PROC,"
					 + "TXT_COMPLEMENTO_01," + "TXT_COMPLEMENTO_02,"
					 + "TXT_COMPLEMENTO_03," + "TXT_COMPLEMENTO_04,"
					 + "TXT_COMPLEMENTO_05," + "TXT_COMPLEMENTO_06,"
					 + "TXT_COMPLEMENTO_07," + "TXT_COMPLEMENTO_08,"
					 + "TXT_COMPLEMENTO_09," + "TXT_COMPLEMENTO_10,"
					 + "TXT_COMPLEMENTO_11," + "TXT_COMPLEMENTO_12) "
					 + " VALUES (" + " seq_tsmi_historico_auto.nextval," + " '"
					 + myHist.getCodAutoInfracao()
					 + "',"
					 + " '"
					 + myHist.getNumProcesso()
					 + "',"
					 + " '"
					 + myHist.getCodStatus()
					 + "',"
					 + " to_date('"
					 + myHist.getDatStatus()
					 + "','dd/mm/yyyy'), "
					 + " '"
					 + myHist.getCodEvento()
					 + "',"
					 + " '"
					 + myHist.getCodOrigemEvento()
					 + "',"
					 + " '"
					 + myHist.getNomUserName()
					 + "',"
					 + " '"
					 + myHist.getCodOrgaoLotacao()
					 + "',"
					 + " to_date('"
					 + myHist.getDatProc()
					 + "','dd/mm/yyyy'), "
					 + " '"
					 + myHist.getTxtComplemento01()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento02()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento03()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento04()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento05()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento06()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento07()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento08()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento09()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento10()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento11()
					 + "',"
					 + " '"
					 + myHist.getTxtComplemento12() + "')";
			 stmt.execute(sCmd);
			 conn.commit();
			 if (conn != null) 	 stmt.close();
		 } catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn);
				 } catch (Exception ey) {
				 }
			 }
		 }
	 }

	 public  boolean LeRequerimentoLocal (RequerimentoBean myReq, Connection conn) throws sys.DaoException {
		 return true;
	 }

	 public List getAutosPendentes(String Placa,String Auto ) throws DaoException {
		 return null;

	 }	
	 public List getAutosPendentesDol(String Placa,String Auto ) throws DaoException {
		 return null;
	 }
	 public void LeInfracao(InfracaoBean bean,String tpChave ) throws DaoException {
	 }

	 public void AtualizaCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException {
		 Connection conn = null;
		 try {
			 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 conn = serviceloc.getConnection(MYABREVSIST);                        
			 Statement stmt = conn.createStatement();
			 String sql = "";

			 sql = "UPDATE TSMI_CONTROLE_PROCESSO SET " +
					 " SIT_UTILIZADO='S' " +
					 " WHERE NUM_PROCESSO = " + myAuto.getNumProcesso();
			 stmt.executeUpdate(sql);
			 conn.commit();
			 stmt.close();

		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 serviceloc.setReleaseConnection(conn);
				 } catch (sys.ServiceLocatorException e) {
					 throw new DaoException(e.getMessage());
				 }
			 }
		 }		
	 }

	 public void BuscarDataCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException {
		 Connection conn = null;
		 try {
			 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			 conn = serviceloc.getConnection(MYABREVSIST);                        
			 Statement stmt = conn.createStatement();
			 ResultSet rs   = null;
			 String sql = "";
			 sql = "SELECT * FROM TSMI_CONTROLE_PROCESSO " +
					 " WHERE NUM_PROCESSO = " + myAuto.getNumProcesso();
			 if (rs.next())
				 myAuto.setDtGerCBProcesso("DAT_GERACAO");

			 conn.commit();
			 stmt.close();
			 rs.close();

		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 serviceloc.setReleaseConnection(conn);
				 } catch (sys.ServiceLocatorException e) {
					 throw new DaoException(e.getMessage());
				 }
			 }
		 }		
	 }

	 public void BuscarDataCodigoBarraNotificacao(REC.AutoInfracaoBean myAuto) throws DaoException {
		 Connection conn = null;
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);                        
			 Statement stmt = conn.createStatement();
			 ResultSet rs   = null;
			 /*Pegar Data e Sequencia C�digo de Barra Notificaca��o Autua��o*/
			 String sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
					 "NUM_NOTIFICACAO='"+myAuto.getNumNotifAut()+"'";
			 rs    = stmt.executeQuery(sCmd) ;
			 while (rs.next()) {
				 myAuto.setDtGerCBNotificacaoAut(rs.getString("DAT_GERACAO_CB_AI"));
				 myAuto.setSeqGeracaoCBNotificacaoAut(rs.getInt("SEQ_GERACAO_CB_AI"));
			 }
			 /*Pegar Data e Sequencia C�digo de Barra Notificaca��o Penalidade*/
			 sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
					 "NUM_NOTIFICACAO='"+myAuto.getNumNotifPen()+"'";
			 rs    = stmt.executeQuery(sCmd) ;
			 while (rs.next()) {
				 myAuto.setDtGerCBNotificacaoPen(rs.getString("DAT_GERACAO_CB_AI"));
				 myAuto.setSeqGeracaoCBNotificacaoPen(rs.getInt("SEQ_GERACAO_CB_AI"));
			 }
			 stmt.close();
			 rs.close();

		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 serviceloc.setReleaseConnection(conn);
				 } catch (sys.ServiceLocatorException e) {
					 throw new DaoException(e.getMessage());
				 }
			 }
		 }		
	 }

	 public void processaPenalidadeDER(AutoInfracaoBean myAuto,
			 UsuarioBean myUsrLog) throws DaoException {
		 // TODO Auto-generated method stub

	 }


	 private String converteData(java.sql.Date data) {
		 String Retorno = null;
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 try {
			 Retorno = df.format(data);
		 } catch (Exception e) {
			 Retorno = "";
		 }
		 return Retorno;
	 }

	 private void converteInt(String numero, int parametro, PreparedStatement stm)
			 throws DaoException {
		 try {
			 stm.setInt(parametro, Integer.parseInt(numero));
		 } catch (Exception e) {
			 try {
				 stm.setNull(parametro, Types.INTEGER);
			 } catch (Exception e1) {
			 }
		 }
	 }

	 private void converteDouble(String numero, int parametro,
			 PreparedStatement stm) throws DaoException {
		 try {
			 stm.setDouble(parametro, Double.parseDouble(numero));
		 } catch (Exception e) {
			 try {
				 stm.setNull(parametro, Types.DOUBLE);
			 } catch (Exception e1) {
			 }
		 }
	 }

	 private java.sql.Date converteData(String data) {
		 java.sql.Date Retorno = null;
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 try {
			 Retorno = new java.sql.Date(df.parse(data).getTime());
		 } catch (Exception e) {
			 Retorno = null;
		 }
		 return Retorno;
	 }

	@Override
	public void atualizarAuto206(AutoInfracaoBean myAuto, ParamOrgBean myParam,
			RequerimentoBean myReq, HistoricoBean myHis, UsuarioBean myUsuario)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto, Connection conn)
			throws DaoException, SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void GravarHistorico(HistoricoBean myHist, Connection conn)
			throws DaoException, ServiceLocatorException, SQLException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void atualizarProcesso(AutoInfracaoBean myAuto,
			ParamOrgBean myParam, RequerimentoBean myReq, HistoricoBean myHis,
			UsuarioBean myUsuario, Ata ata) throws DaoException {
		// TODO Auto-generated method stub
		
	}


}