package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;

 

/**
* <b>Title:</b>      Recurso - Ajuste Manual<br>
* <b>Description:</b>Transacao 413:Ajuste Manual do Respons�vewl pelos pontos<br>
* <b>Copyright:</b>  Copyright (c) 2004<br>
* <b>Company:</b>    MIND - Informatica<br>
* @author Luciana Rocha
* @version 1.0
*/
public class AjusteManualRespPecuniaCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteManualRespPecunia.jsp";

	public AjusteManualRespPecuniaCmd() {
		next = jspPadrao;
	}

	public AjusteManualRespPecuniaCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	  public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			// === cria os Beans de sessao, se n�o existir ===
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;

	        AutoInfracaoBean AutoInfracaoBeanEspelhoId = (AutoInfracaoBean)session.getAttribute("AutoInfracaoBeanEspelhoId") ;
	        if (AutoInfracaoBeanEspelhoId==null) AutoInfracaoBeanEspelhoId = new AutoInfracaoBean() ;
	        
			RequerimentoBean RequerimentoEspelhoId = (RequerimentoBean)session.getAttribute("RequerimentoEspelhoId") ;
			if (RequerimentoEspelhoId==null)  RequerimentoEspelhoId = new RequerimentoBean() ;	  	

            HistoricoBean HistoricoEspelhoId = (HistoricoBean)session.getAttribute("HistoricoEspelhoId") ;
            if (HistoricoEspelhoId==null)  HistoricoEspelhoId = new HistoricoBean() ;
            HistoricoBean myHist = new HistoricoBean();


            /* ========= flagMantemDados -> Flag de controle ============
             * Controla os dadosdigitados pelo usuario Responsavel indicar 
             * dados que devem permanecer na tela.                       */
            String flagMantemDados = (String)session.getAttribute("flagMantemDados"); 
            if (flagMantemDados == null)  flagMantemDados = "N";
   
            
	        // === obtem e valida os parametros recebidos === 
            String acao = req.getParameter("acao");  
            if(acao==null){
            	acao = " ";
            	AutoInfracaoBeanEspelhoId = new AutoInfracaoBean();
            	RequerimentoEspelhoId = new RequerimentoBean() ;
                HistoricoEspelhoId = new HistoricoBean();
            	flagMantemDados = "N";
            	session.setAttribute("flagMantemDados",flagMantemDados);
            }
           
            if ("Novo".indexOf(acao)<0)   {
                sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ; 
                cmd.setNext(this.next) ;
                nextRetorno = cmd.execute(req);     
            }

			// === cria os Beans, se n�o existir ===
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;
   
			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  	
            
          
            // === Valida a acao ===
			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AutoInfracaoBeanId.setMsgOk("S");
                if(flagMantemDados.equals("S")){
                	copyDadosAuto(AutoInfracaoBeanId, AutoInfracaoBeanEspelhoId);
                	copyDadosRequerimento(RequerimentoId, RequerimentoEspelhoId);
                    copyDadosHistorico(myHist, HistoricoEspelhoId);
                }
			}
			else if(acao.equals("Novo")){
                if(flagMantemDados.equals("S")){
                	copyDadosAuto(AutoInfracaoBeanId, AutoInfracaoBeanEspelhoId);
                	copyDadosRequerimento(RequerimentoId, RequerimentoEspelhoId);
                    copyDadosHistorico(myHist, HistoricoEspelhoId);
                	AutoInfracaoBeanId.setMsgOk("S");
                }
                req.setAttribute("LeAI","S");   
            }
			else if (acao.equals("ConfirmaAjuste")) { 	
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
				  // * Dados do Proprietario ==== Come�o a setar as informa��es do Proprietario =====
				  AutoInfracaoBeanId.getProprietario().setNomResponsavel(req.getParameter("nomResp"));
				  AutoInfracaoBeanId.getProprietario().setNumCnh(req.getParameter("numCNH"));
				  AutoInfracaoBeanId.getProprietario().setCodUfCnh(req.getParameter("codUfCNHTR"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNomBairro(req.getParameter("nomBairro"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNomCidade(req.getParameter("nomCidade"));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setNumCEP(req.getParameter("numCEPTR").replaceAll("-",""));
				  AutoInfracaoBeanId.getProprietario().getEndereco().setCodUF(req.getParameter("codUf"));
				  AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
				  AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNH"));
				  if ("1".equals(AutoInfracaoBeanId.getProprietario().getIndTipoCnh())) AutoInfracaoBeanId.getProprietario().setIndTipoCnh("1");
				  else  AutoInfracaoBeanId.getProprietario().setIndTipoCnh("2");
                  if ("0".equals(AutoInfracaoBeanId.getProprietario().getIndTipoCnh())) AutoInfracaoBeanId.getProprietario().setIndTipoCnh("0");
				  
   
				  // * Dados do Condutor === Come�o a setar as informa��es do resp. pelos pontos.===
				  AutoInfracaoBeanId.getCondutor().setNomResponsavel(req.getParameter("nomResp"));
				  AutoInfracaoBeanId.getCondutor().setNumCnh(req.getParameter("numCNH"));
				  AutoInfracaoBeanId.getCondutor().setCodUfCnh(req.getParameter("codUfCNHTR"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setNomBairro(req.getParameter("nomBairro"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setNomCidade(req.getParameter("nomCidade"));
				  AutoInfracaoBeanId.getCondutor().getEndereco().setNumCEP(req.getParameter("numCEPTR").replaceAll("-",""));
 				  AutoInfracaoBeanId.getCondutor().getEndereco().setCodUF(req.getParameter("codUf"));
				  AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
				  AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNH"));
				  if ("1".equals(AutoInfracaoBeanId.getCondutor().getIndTipoCnh())) AutoInfracaoBeanId.getCondutor().setIndTipoCnh("1");
				  else  AutoInfracaoBeanId.getCondutor().setIndTipoCnh("2");
                  if ("0".equals(AutoInfracaoBeanId.getCondutor().getIndTipoCnh())) AutoInfracaoBeanId.getCondutor().setIndTipoCnh("0");
				 
				  String numCPFpnt=req.getParameter("cpfpnthidden");	
				  AutoInfracaoBeanId.getCondutor().setNumCpfCnpj(numCPFpnt); 
				  if (AutoInfracaoBeanId.getCondutor().getNumCpfCnpj().length() == 11) AutoInfracaoBeanId.getCondutor().setIndCpfCnpj("1");
				  else  AutoInfracaoBeanId.getCondutor().setIndCpfCnpj("2");				  
				 
                  //Valida se � CPF ou CPNJ    
				  String numCPF = req.getParameter("cpfhidden");
				  AutoInfracaoBeanId.getProprietario().setNumCpfCnpj(numCPF);
				  if (AutoInfracaoBeanId.getProprietario().getNumCpfCnpj().length() == 11) AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("1");
				  else  AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("2");
				  
			      RequerimentoId.setDatRequerimento(req.getParameter("dataReq"));
                  
                  String decisaoJur = req.getParameter("decisaoJur");
                  if(decisaoJur == null) decisaoJur = "0";
                  myHist.setTxtComplemento11(decisaoJur);
                  
                  //Envia as transacoes - 413 e 415
				  DaoBroker dao = DaoBrokerFactory.getInstance();				  
				  dao.ajustarManual(AutoInfracaoBeanId,UsrLogado,RequerimentoId,myHist);				  
				  dao.ajustarManualRespPecunia(AutoInfracaoBeanId,UsrLogado,RequerimentoId);
				    
				  //Copia os dados vindos na tela em um bean espelho para permitir a
				  //permanecia dos dados na tela.  
				  BeanUtils.copyProperties(AutoInfracaoBeanEspelhoId,AutoInfracaoBeanId);
				  BeanUtils.copyProperties(RequerimentoEspelhoId,RequerimentoId);
                  BeanUtils.copyProperties(HistoricoEspelhoId,myHist);
			  
				  if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
					   AutoInfracaoBeanId.setMsgErro("Ajuste Manual do Respons�vel pela pec�nia realizado com sucesso.");
				  }
				  
				  //Controle da permanencia dos dados na tela
				  AutoInfracaoBeanId.setMsgOk("S");
                  flagMantemDados = "S";
	              session.setAttribute("flagMantemDados",flagMantemDados);
				}
			}

	        session.setAttribute("AutoInfracaoBeanEspelhoId",AutoInfracaoBeanEspelhoId) ;
	        session.setAttribute("RequerimentoEspelhoId",RequerimentoEspelhoId) ;
            session.setAttribute("HistoricoEspelhoId",HistoricoEspelhoId) ;

			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			req.setAttribute("RequerimentoId",RequerimentoId) ;
			req.setAttribute("UsrLogado",UsrLogado) ;
		}
		catch (Exception ue) {
			  throw new sys.CommandException("AjusteManualRespPecuniaCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	  }
	  
	  public void copyDadosAuto(AutoInfracaoBean dest, AutoInfracaoBean org) {
		  dest.getProprietario().setNomResponsavel(org.getProprietario().getNomResponsavel());
		  dest.getProprietario().setNumCnh(org.getProprietario().getNumCnh());
		  dest.getProprietario().setCodUfCnh(org.getProprietario().getCodUfCnh());
		  dest.getProprietario().getEndereco().setTxtEndereco(org.getProprietario().getEndereco().getTxtEndereco());
		  dest.getProprietario().setIndTipoCnh(org.getProprietario().getIndTipoCnh());
		  dest.getCondutor().setNomResponsavel(org.getCondutor().getNomResponsavel());
		  dest.getCondutor().setNumCnh(org.getCondutor().getNumCnh());
		  dest.getCondutor().setCodUfCnh(org.getCondutor().getCodUfCnh());
		  dest.getCondutor().getEndereco().setTxtEndereco(org.getCondutor().getEndereco().getTxtEndereco());
		  dest.getCondutor().setIndTipoCnh(org.getCondutor().getIndTipoCnh());
		  dest.getCondutor().setNumCpfCnpj(org.getCondutor().getNumCpfCnpj()); 
		  dest.getProprietario().setNumCpfCnpj(org.getProprietario().getNumCpfCnpj());
		  dest.getProprietario().getEndereco().setNumEndereco(org.getProprietario().getEndereco().getNumEndereco());
		  dest.getProprietario().getEndereco().setTxtComplemento(org.getProprietario().getEndereco().getTxtComplemento());
		  dest.getProprietario().getEndereco().setNomBairro(org.getProprietario().getEndereco().getNomBairro());
		  dest.getProprietario().getEndereco().setNomCidade(org.getProprietario().getEndereco().getNomCidade());
		  dest.getProprietario().getEndereco().setNumCEPEdt(org.getProprietario().getEndereco().getNumCEPEdt());
		  dest.getProprietario().getEndereco().setCodUF(org.getProprietario().getEndereco().getCodUF());
	  }
	  
	  public void copyDadosRequerimento(RequerimentoBean dest, RequerimentoBean org) {
		  dest.setDatRequerimento(org.getDatRequerimento());
	  }
      
      public void copyDadosHistorico(HistoricoBean dest, HistoricoBean org) {
          dest.setTxtComplemento11(org.getTxtComplemento11());
      }

}