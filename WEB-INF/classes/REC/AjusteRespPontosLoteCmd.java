package REC;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



/**
 * <b>Title:</b>      Recurso - Ajuste Manual<br>
 * <b>Description:</b>Transacao 413:Ajuste Manual do Respons�vewl pelos pontos<br>
 * <b>Copyright:</b>  Copyright (c) 2004<br>
 * <b>Company:</b>    MIND - Informatica<br>
 * @author Luciana Rocha
 * @version 1.0
 */
public class AjusteRespPontosLoteCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteRespPontosLote.jsp";



	public AjusteRespPontosLoteCmd() {
		next = jspPadrao;
	}

	public AjusteRespPontosLoteCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;

			// cria os Beans, se n�o existir

			consultaAutoBean consultaAutoId                 = (consultaAutoBean)session.getAttribute("consultaAutoId") ;
			if (consultaAutoId==null)  	consultaAutoId       = new consultaAutoBean() ;	  		    

			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;

			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;


			String dProc = req.getParameter("datprocesso");
			if (dProc==null) dProc="";
			String nProc = req.getParameter("numprocesso");
			if (nProc==null) nProc="";
			String txtMotivo = req.getParameter("txtMotivo");
			if (txtMotivo==null) txtMotivo="";


			HistoricoBean myHist = new HistoricoBean();


			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";


			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AutoInfracaoBeanId.setMsgOk("S");
				consultaAutoId.setNumPlaca(AutoInfracaoBeanId.getNumPlaca());
				consultaAutoId.ConsultaAutos(UsrLogado);


			}else if (acao.equals("ConfirmaAjuste")) { 	
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {

					String decisaoJur = req.getParameter("decisaoJur");
					if(decisaoJur == null) decisaoJur = "0";

					myHist.setTxtComplemento11(decisaoJur);

					DaoBroker dao = DaoBrokerFactory.getInstance();

					String procSelec[] = req.getParameterValues("Selecionado");
					String procSelecPlaca[] = req.getParameterValues("SelecionadoPlaca");
					String procSelecAuto[] = req.getParameterValues("SelecionadoAuto");
					String procSelecProcesso[] = req.getParameterValues("SelecionadoProcesso");    					
					int n =0 ;
					String sNumPLaca="",sNnumAutoInfracao="",sNumProcesso="";
					
					if (procSelec!=null)	
					{
						for (int i=0;  i<procSelec.length; i++) 
						{					
							n                 = Integer.parseInt(procSelec[i])-1;
							sNumPLaca         = procSelecPlaca[n];
							sNnumAutoInfracao = procSelecAuto[n];
							sNumProcesso      = procSelecProcesso[n];
							
							System.out.println("NUmero Auto:"+sNnumAutoInfracao+" - Placa:"+sNumPLaca);
							
							

							if (n>=0)  {
								AutoInfracaoBeanId.setNumPlaca(sNumPLaca);
								AutoInfracaoBeanId.setNumAutoInfracao(sNnumAutoInfracao);
								AutoInfracaoBeanId.setNumProcesso(sNumProcesso);
								AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
								AutoInfracaoBeanId.getProprietario().setNomResponsavel(req.getParameter("nomResp"));
								AutoInfracaoBeanId.getProprietario().setNumCnh(req.getParameter("numCNH"));
								AutoInfracaoBeanId.getProprietario().setCodUfCnh(req.getParameter("codUfCNHTR"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setNomBairro(req.getParameter("nomBairro"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setNomCidade(req.getParameter("nomCidade"));
								AutoInfracaoBeanId.getProprietario().getEndereco().setNumCEP(req.getParameter("numCEPTR").replaceAll("-",""));
								AutoInfracaoBeanId.getProprietario().getEndereco().setCodUF(req.getParameter("codUf"));
								AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
								AutoInfracaoBeanId.getProprietario().setIndTipoCnh(req.getParameter("indTipoCNH"));
								if ("1".equals(AutoInfracaoBeanId.getProprietario().getIndTipoCnh())) AutoInfracaoBeanId.getProprietario().setIndTipoCnh("1");
								else  AutoInfracaoBeanId.getProprietario().setIndTipoCnh("2");
								if ("0".equals(AutoInfracaoBeanId.getProprietario().getIndTipoCnh())) AutoInfracaoBeanId.getProprietario().setIndTipoCnh("0");


								AutoInfracaoBeanId.getCondutor().setNomResponsavel(req.getParameter("nomResp"));
								AutoInfracaoBeanId.getCondutor().setNumCnh(req.getParameter("numCNH"));
								AutoInfracaoBeanId.getCondutor().setCodUfCnh(req.getParameter("codUfCNHTR"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setNomBairro(req.getParameter("nomBairro"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setNomCidade(req.getParameter("nomCidade"));
								AutoInfracaoBeanId.getCondutor().getEndereco().setNumCEP(req.getParameter("numCEPTR").replaceAll("-",""));
								AutoInfracaoBeanId.getCondutor().getEndereco().setCodUF(req.getParameter("codUf"));
								AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
								AutoInfracaoBeanId.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNH"));
								if ("1".equals(AutoInfracaoBeanId.getCondutor().getIndTipoCnh())) AutoInfracaoBeanId.getCondutor().setIndTipoCnh("1");
								else  AutoInfracaoBeanId.getCondutor().setIndTipoCnh("2");
								if ("0".equals(AutoInfracaoBeanId.getCondutor().getIndTipoCnh())) AutoInfracaoBeanId.getCondutor().setIndTipoCnh("0");



								String numCPFpnt=req.getParameter("cpfpnthidden");	
								AutoInfracaoBeanId.getCondutor().setNumCpfCnpj(numCPFpnt); 
								if (AutoInfracaoBeanId.getCondutor().getNumCpfCnpj().length() == 11) AutoInfracaoBeanId.getCondutor().setIndCpfCnpj("1");
								else  AutoInfracaoBeanId.getCondutor().setIndCpfCnpj("2");				  


								String numCPF = req.getParameter("cpfhidden");
								AutoInfracaoBeanId.getProprietario().setNumCpfCnpj(numCPF);
								if (AutoInfracaoBeanId.getProprietario().getNumCpfCnpj().length() == 11) AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("1");
								else  AutoInfracaoBeanId.getProprietario().setIndCpfCnpj("2");




								RequerimentoId.setDatRequerimento(req.getParameter("dataReq"));
								dao.ajustarManual(AutoInfracaoBeanId,UsrLogado,RequerimentoId,myHist);
								dao.ajustarManualRespPecunia(AutoInfracaoBeanId,UsrLogado,RequerimentoId);

								HistoricoBean myHis = new HistoricoBean() ;
								myHis.setCodAutoInfracao(AutoInfracaoBeanId.getCodAutoInfracao());   
								myHis.setNumAutoInfracao(AutoInfracaoBeanId.getNumAutoInfracao()); 
								myHis.setNumProcesso(AutoInfracaoBeanId.getNumProcesso());
								myHis.setCodStatus(AutoInfracaoBeanId.getCodStatus());  
								myHis.setDatStatus(AutoInfracaoBeanId.getDatStatus());
								myHis.setCodEvento("410");
								myHis.setCodOrigemEvento("100");		
								myHis.setNomUserName(UsrLogado.getNomUserName());
								myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
								myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
								myHis.setTxtComplemento01(nProc);
								myHis.setTxtComplemento02(dProc);
								myHis.setTxtComplemento03(" ");
								myHis.setTxtComplemento04(txtMotivo);									
								dao.atualizarAuto410(AutoInfracaoBeanId,myHis,UsrLogado);
							}
						}
					}
					if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
						AutoInfracaoBeanId.setMsgErro("Ajuste Manual do Respons�vel pelos pontos realizado com sucesso.") ;
						nextRetorno	= "/REC/AjusteRespPontosLote.jsp" ;
					}
				}
			}
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId);
			req.setAttribute("consultaAutoId",consultaAutoId);
			req.setAttribute("myHist", myHist);
			req.setAttribute("RequerimentoId",RequerimentoId) ;
			req.setAttribute("UsrLogado",UsrLogado) ;
			nextRetorno	= "/REC/AjusteRespPontosLote.jsp" ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AjusteManualRespPontosCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	}

}