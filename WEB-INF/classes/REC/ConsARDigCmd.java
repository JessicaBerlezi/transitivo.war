package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class ConsARDigCmd extends sys.Command{

	private String next;
	private static final String jspPadrao = "/REC/ConsARDig.jsp";

	public ConsARDigCmd() {
		next = jspPadrao;
	}

	public ConsARDigCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
				
		try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            
		    String acao = req.getParameter("acao");
		    if (acao == null) acao = "";

            String datInicial = req.getParameter("datInicial");
            if (datInicial == null) datInicial = df.format(new Date());
            
            String datFinal = req.getParameter("datFinal");
            if (datFinal == null) datFinal = df.format(new Date());
            
            LogARDigitalizadoBean LogARDigitalizadoBeanId = new LogARDigitalizadoBean();
            
		    if (acao.equals("busca"))
		        LogARDigitalizadoBeanId.carrega(df.parse(datInicial), df.parse(datFinal));
            
            else if (acao.equals("retorna"))
                acao = "";
                        
            req.setAttribute("acao", acao);
            req.setAttribute("LogARDigitalizadoBeanId", LogARDigitalizadoBeanId);
            req.setAttribute("datInicial", datInicial);
            req.setAttribute("datFinal", datFinal);

		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		return nextRetorno;
	}

}