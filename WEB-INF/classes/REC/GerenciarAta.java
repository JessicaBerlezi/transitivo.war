package REC;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class GerenciarAta extends  sys.Command{
	private String next;
	private static final String jspPadrao="/REC/AtaPublicacao.jsp";
	
	public static ArrayList<HashMap<String, String>> listProcessos = new ArrayList<HashMap<String, String>>();
	public static ArrayList<HashMap<String, String>> listAtas = new ArrayList<HashMap<String, String>>();
	public static String tipoAta;

	public GerenciarAta() {
		next = jspPadrao;
	}

	public GerenciarAta(String next) {
		this.next = next;
	}

	protected String gerarRelatorio(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno=jspPadrao;

		try {
			listProcessos.clear();
			listAtas.clear();
			tipoAta = req.getParameter("tipoAta");
			final String codOrgao = req.getParameter("codOrgao");
			String json = req.getParameter("listAtas");			
			JSONArray jArray = (JSONArray)new JSONParser().parse(json);
			ArrayList<Ata> atas = new ArrayList<Ata>();
			
			for(Object item: jArray){
				JSONObject jObj = (JSONObject)item;
				Ata ata = new Ata();				
				ata.setCodAta(jObj.get("codAta").toString());
				
				atas.add(ata);
			}
			
			AutoInfracaoBean auto = new AutoInfracaoBean();			
			listProcessos = auto.getProcessos(atas, codOrgao);
			listAtas = getAtasfromProcessos(listProcessos);

			nextRetorno = "REC/RelatorioAtaPublicacao.jsp";
		}
		catch (Exception e){
			nextRetorno = "/REC/ErroDataAta.jsp" ;

			return nextRetorno;
		}
		return nextRetorno;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId");
			
			if (UsrLogado==null)
				UsrLogado = new ACSS.UsuarioBean();
			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId");
			
			if (UsuarioFuncBeanId==null)
				UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();
			
			ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId");
			
			if (ParamOrgaoId==null)
				ParamOrgaoId = new ParamOrgBean();
			
			ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			
			if (parSis == null)
				parSis = new ParamSistemaBean();
			
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId");
			
			// cria os Beans, se n�o existir
			AbrirRecursoDPBean AbrirRecursoBeanId = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId");
			
			if (AbrirRecursoBeanId==null)
				AbrirRecursoBeanId = new AbrirRecursoDPBean();

			if (AutoInfracaoBeanId==null){
				AutoInfracaoBeanId = new AutoInfracaoBean();
				
				if (AutoInfracaoBeanId.getProprietario()==null)
					AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());
				
				if (AutoInfracaoBeanId.getCondutor()==null)
					AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());
				
				if (AutoInfracaoBeanId.getVeiculo()==null)
					AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());
				
				if (AutoInfracaoBeanId.getInfracao()==null)
					AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());
				
				req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			}
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao"); 

			if(acao==null)
				acao = " ";

			if(acao.equals("gerarRelatorio"))
				nextRetorno = gerarRelatorio(req);

			req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			req.setAttribute("AbrirRecursoBeanId", AbrirRecursoBeanId);
			req.setAttribute("ParamOrgaoId", ParamOrgaoId);
			req.setAttribute("codOrgao", ParamOrgaoId.getCodOrgao());
		}
		catch (Exception ue) {
			throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
		}
		
		return nextRetorno;
	}

	public ArrayList<HashMap<String, String>> getAtasfromProcessos(ArrayList<HashMap<String, String>> processos){
		ArrayList<HashMap<String, String>> atas = new ArrayList<HashMap<String,String>>();

		for(HashMap<String, String> p : processos){
			boolean existeAta = false;

			if(atas.size() > 0){
				for(HashMap<String, String> a : atas){
					if(p.get("desAta").equals(a.get("desAta")))
						existeAta = true;
				}
				
				if(!existeAta){
					HashMap<String, String> newAta = new HashMap<String, String>();
					newAta.put("desAta", p.get("desAta"));
					newAta.put("data", p.get("data"));
					newAta.put("dataSessao", p.get("dataSessao"));

					atas.add(newAta);
				}
			}
			else{
				HashMap<String, String> newAta = new HashMap<String, String>();
				newAta.put("desAta", p.get("desAta"));
				newAta.put("data", p.get("data"));
				newAta.put("dataSessao", p.get("dataSessao"));

				atas.add(newAta);
			}
		}

		return atas;
	}
}
