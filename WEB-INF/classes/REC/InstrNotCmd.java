package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de Parametros do Orgao<br>
* <b>Description:</b>  Comando para Manter os Parametros por Orgao<br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class InstrNotCmd extends sys.Command {

    private static final String jspPadrao="/REC/InstrucaoPreenchim.jsp";    
  private String next;

  public InstrNotCmd() {
    next             =  jspPadrao;
  }

  public InstrNotCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      	
		// cria os Beans do Usuario, se n�o existir
		REC.InstrNotBean InstrId = (REC.InstrNotBean)req.getAttribute("InstrId") ;
		if (InstrId==null)        InstrId = new REC.InstrNotBean() ;	  			
							
		// obtem e valida os parametros recebidos					
		String acao           = req.getParameter("acao");  
		if(acao==null)       acao =" ";   
	  String codOrgao = req.getParameter("codOrgao"); 		
		if(codOrgao==null)       codOrgao ="0"; 
		String codNotificacao = req.getParameter("codNotificacao"); 		
		if(codNotificacao==null)       codNotificacao ="0"; 
		String dscNotificacao = req.getParameter("dscNot"); 		
		if(dscNotificacao==null)       dscNotificacao ="";		
		String sobrescreve = req.getParameter("sobrescreve");		
		if (sobrescreve==null) 	sobrescreve = "false";
		if (sobrescreve=="false") 	InstrId.setSobrescreve(false);
		else InstrId.setSobrescreve(true);
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente==null)       atualizarDependente ="N";   		 
	
	    InstrId.Le_InstrNot(codOrgao,codNotificacao,0) ;
		InstrId.setAtualizarDependente(atualizarDependente);			  						  		
	    InstrId.setDscNotificacao(dscNotificacao);
		
		if ("buscaInstrucao".indexOf(acao)>=0) {

	      InstrId.getParametros(50,0) ;	

		  InstrId.setAtualizarDependente("S");	
		  		  	
	    }	 
	    
		if (acao.compareTo("Copiar") == 0) {
						String codOrgaoCp = req.getParameter("codOrgaoCp");						
			      InstrId.setCodOrgaoCp(codOrgaoCp);
			      InstrId.CopiaInstrucoes(InstrId);
					} 
	    
		Vector vErro =new Vector(); 
	    if(acao.compareTo("A")==0){
			String[] numLinhaNot = req.getParameterValues("numLinhaNot");
			if(numLinhaNot==null)  numLinhaNot = new String[0];         
			String[] txtLinhaNot = req.getParameterValues("txtLinhaNot");
			if(txtLinhaNot==null)  txtLinhaNot = new String[0];         
			String[] codTipoLinhaNot = req.getParameterValues("codTipoLinhaNot");
			if(codTipoLinhaNot==null)  codTipoLinhaNot = new String[0]; 
            
			vErro      = isParametros(codTipoLinhaNot,txtLinhaNot) ;

			if (vErro.size()==0) {
			
				Vector param = new Vector(); 
				for (int i = 0; i<txtLinhaNot.length;i++) {
				 
					InstrNotBean myParam = new InstrNotBean() ;		
//					System.out.println("tenho" + i + "Linhas");				
			
				 				
					myParam.setNumLinhaNot(numLinhaNot[i]);	  				    
				  myParam.setTxtLinhaNot(txtLinhaNot[i]) ;
				  myParam.setCodTipoLinhaNot(codTipoLinhaNot[i]);
					myParam.setCodOrgao(codOrgao);
					myParam.setCodNotificacao(codNotificacao);
					
					param.addElement(myParam) ; 					
				 
				}
				
				InstrId.setParametros(param) ;
				InstrId.isInsertParametros() ;
			  InstrId.getParametros(50,0) ;						
			}
			else InstrId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
			req.setAttribute("parametros",InstrId.getParametros(0,0));
	    	nextRetorno = "/REC/InstrucaoPreenchimImp.jsp";
	    }
	    
		req.setAttribute("InstrId",InstrId) ;		 
		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("InstrNotCmd001: " + ue.getMessage());
    }
	return nextRetorno;
}

private Vector isParametros(String[] codTipoLinhaNot,String[] txtLinhaNot) {
		Vector vErro = new Vector() ;
		if ((codTipoLinhaNot.length!=txtLinhaNot.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  }

}