package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <b>Title: </b> SMIT - REC - Foto Digitalizada Bean <br>
 * <b>Description: </b> Foto de Auto de Infra��o Digitalizada Bean <br>
 * <b>Copyright: </b> Copyright (c) 2004 <br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class FotoDigitalizadaRobBean extends sys.HtmlPopupBean {
    
    private int codFotoDigitalizada;
    private String numAutoInfracao;
    private Date datDigitalizacao;
    
    public FotoDigitalizadaRobBean() {
        
        super();
        codFotoDigitalizada = 0;
        numAutoInfracao = "";
        datDigitalizacao = null;
    }
    
    public FotoDigitalizadaRobBean(String numAutoInfracao, Date datDigitalizacao) {
        
        this();
        this.numAutoInfracao = numAutoInfracao;
        this.datDigitalizacao = datDigitalizacao;
    }
        
    public String getNumAutoInfracao() {
        return numAutoInfracao;
    }
    public void setNumAutoInfracao(String string) {
        numAutoInfracao = string;
    }
        
    public int getCodFotoDigitalizada() {
        return codFotoDigitalizada;
    }    
    public void setCodFotoDigitalizada(int i) {
        codFotoDigitalizada = i;
    }
    
    public Date getDatDigitalizacao() {
        return datDigitalizacao;
    }
    public void setDatDigitalizacao(Date datDigitalizacao) {
        this.datDigitalizacao = datDigitalizacao;
    }
    
    public String grava() throws DaoException {
        return DaoRobDigit.getInstance().GravaFotoDigitalizada(this);
    }
    
    public String getImagem() {
        String pathImagem = "";
        
		
        pathImagem =  getDataDigitalizacaoString().substring(6,10)+ 
        getDataDigitalizacaoString().substring(3,5) +
        "/" + getDataDigitalizacaoString().substring(0,2); 
        return pathImagem;
    }
    
    public String getParametro(ACSS.ParamSistemaBean param) {        
        String parametro = "DIR_FOTO_DIGITALIZADA";
        /*
        parametro+="_"+getDataDigitalizacaoString().substring(6,10);
        */
        return parametro;
    }

    public String getArquivo(ACSS.ParamSistemaBean param) {
        String pathArquivo;
        try {
            pathArquivo = "E:/PROD-smit/digitalizado/"+this.getImagem();
			if (this.numAutoInfracao.length() >= 6)
				pathArquivo += "/Foto/"+ this.numAutoInfracao + ".jpg";
        
        } catch (Exception e) {
            pathArquivo = "";
        }
        return pathArquivo;
    }
    
    public String getDataDigitalizacaoString() {
        
        String datString = "";
        if (datDigitalizacao != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            datString = df.format(this.datDigitalizacao);
        }
        return datString;
    }
    
    public static FotoDigitalizadaRobBean consultaFoto(String[] campos, String[] valores) 
    throws sys.BeanException {
        
        FotoDigitalizadaRobBean Foto = null;
        try {
            Foto = DaoRobDigit.getInstance().ConsultaFotoDigitalizada(campos, valores);
            if (Foto == null) {
                Foto = new FotoDigitalizadaRobBean();
                Foto.setMsgErro("FOTO DE AUTO DE INFRA��O N�O ENCONTRADA !");
            }
        } catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
        return Foto;		
    }
    
    public static FotoDigitalizadaRobBean[] consultaFotos(String[] campos, String[] valores) 
    throws sys.BeanException {
        
        try {
            return DaoRobDigit.getInstance().ConsultaFotoDigitalizadas(campos, valores);			     
        } 
        catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
    }
    
    public String getNomFoto(){
    	return getImagem() + "/Foto/"+this.numAutoInfracao + ".jpg";
    }
    
}