package REC;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class GerenciarInfoResultProcessoCmd extends  sys.Command{
	private String next;
	private static final String jspPadrao="/REC/InfoResultProcesso.jsp" ;


	public GerenciarInfoResultProcessoCmd() {
		next = jspPadrao;
	}
	public GerenciarInfoResultProcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			 HttpSession session   = req.getSession() ;								
			    ACSS.UsuarioBean UsrLogado          = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			    if (UsrLogado==null)  UsrLogado     = new ACSS.UsuarioBean() ;
				ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
				if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
			    ParamOrgBean ParamOrgaoId           = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			    ParamSistemaBean parSis             = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
				if (parSis == null)	parSis          = new ParamSistemaBean();
			  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			 // cria os Beans, se n�o existir
				AbrirRecursoDPBean AbrirRecursoBeanId     = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId") ;
				if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecursoDPBean() ;	  			
				
			  	if (AutoInfracaoBeanId==null)  	{
			  		AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
			  		if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
			  		if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
			  		if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
			  		if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
			  		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
			  	}
			// obtem e valida os parametros recebidos					
			String acao           = req.getParameter("acao");
			
			if(acao==null)   acao = " ";
			
			String tpRequerimento = req.getParameter("tpRequerimento");
			String cdAta = req.getParameter("idNrAtaName");
	    	String txtAta = req.getParameter("dsAta");
	    	String codRelator = req.getParameter("idCpfAtaRelator");
	    	String dsRelator = req.getParameter("dsRelator");
	    	String codCRelator = req.getParameter("cdRelatorC");
	    	String dsCRelator = req.getParameter("nameRelator");
	    	
	    	String indices = req.getParameter("hdindice");
	    	String indicesC = req.getParameter("hdindiceC");
	    	String tipoStatus = req.getParameter("tipoStatus");
	    	
	    	String idNrAtaNameExtra =  req.getParameter("idNrAtaNameExtra");
	    	String dsAtaExtra =  req.getParameter("dsAtaExtra");
	    	String tpRequerimentoExtra =  req.getParameter("tpRequerimentoExtra");
	    	String cdRelatorExtra =  req.getParameter("cdRelatorExtra");
	    	String dsRelatorExtra =  req.getParameter("dsRelatorExtra");
	    	
	    	
	    	if(acao.equals("infResult")) {
	    		if(Ata.arListBuscaInfResult != null)
	    			Ata.arListBuscaInfResult.clear();
	    		
				nextRetorno = processaProcessos(req, tpRequerimento, cdAta, txtAta, codRelator, dsRelator );
				
				req.setAttribute("SemAtribuicao","processo sem atribuicao") ;
				req.setAttribute("ComAtribuicao","processo com atribuicao") ;	
			}
	    	
	    	if(acao.equals("Processar")){
	    		
	    		Ata attt = new Ata();
	    		//atualiza array

	    		if(tpRequerimentoExtra.equals("Def.")){
					tpRequerimentoExtra = "Def. Previa";
				} else if(tpRequerimentoExtra.equals("1� I")){
					tpRequerimentoExtra = "1� INST�NCIA";
				} else if(tpRequerimentoExtra.equals("2� ")){
					tpRequerimentoExtra = "2� INST�NCIA";
				} else if(tpRequerimentoExtra.equals("PAE"))
					tpRequerimentoExtra = "PAE";
				else if(tpRequerimentoExtra.equals("SELE")){
					tpRequerimentoExtra = "SELECIONE";
				}
				
	    		//Atualizar
	    		//attt.getProcessoInfResultMap(tpRequerimentoExtra, idNrAtaNameExtra, dsAtaExtra, cdRelatorExtra, dsRelatorExtra);
	    		
	    		if(((indices.length()> 0)&&(indices != null))){ // for example0,3
					String[] requerimentos = indices.split(",");
					
					for (String codRequerimento : requerimentos) {
						attt.infResult(tipoStatus, codRequerimento, UsrLogado.getNomUserName());
					}
					
//					for(int i =0; i < parts.length; i ++){
//						int indice = Integer.valueOf(parts[i]);
//						attt.infResult(tipoStatus, indice);
//					}
				}
	    		
//				nextRetorno = processaProcessos(req, tpRequerimento, cdAta, txtAta, codRelator, dsRelator );
				
				//Atualizar
	    		attt.getProcessoInfResultMap(tpRequerimentoExtra, idNrAtaNameExtra, dsAtaExtra, cdRelatorExtra, dsRelatorExtra);
				req.setAttribute("SemAtribuicao","processo sem atribuicao") ;
				req.setAttribute("ComAtribuicao","processo com atribuicao") ;	
			}
	    	
			if(acao.equals("buscaProcessos")){
				nextRetorno = processaProcessos(req, tpRequerimento, cdAta, txtAta, codRelator, dsRelator );
				req.setAttribute("SemAtribuicao","processo sem atribuicao") ;
				req.setAttribute("ComAtribuicao","processo com atribuicao") ;	
			}
			
			if(acao.equals("Inclui")){ //Incluir processos n�o atribuidos a relator(o relator informado passa a ser o resposanvel pelo processo)
				Ata attt = new Ata();
				if(((indices.length()> 0)&&(indices != null))){ // for example0,3
					String[] parts = indices.split(",");
					for(int i =0; i < parts.length; i ++){
						int indice = Integer.valueOf(parts[i]);
						attt.transfereDadosSemAtribuicaoPAtribuicao(codCRelator, dsCRelator, indice);
					}
				}
				req.setAttribute("SemAtribuicao","processo sem atribuicao") ;
				req.setAttribute("ComAtribuicao","processo com atribuicao") ;	
			}
			
			if(acao.equals("Exclui")){ //Exclui processos atribuidos a relator(O relator deixa de ser o responsavel pelo processo)
				Ata attt = new Ata();
				if(((indicesC.length()> 0)&&(indicesC != null))){ // for example0,3
					String[] parts = indicesC.split(",");
					for(int i =0; i < parts.length; i ++){
						int indice = Integer.valueOf(parts[i]);
						attt.transfereDadosComAtribuicaoPAtribuicao(codCRelator, dsCRelator, indice);
					}
				}
				req.setAttribute("SemAtribuicao","processo sem atribuicao");
				req.setAttribute("ComAtribuicao","processo com atribuicao");	
			}

			if(acao.equals("gerarRelatorio")){
				String codAta = req.getParameter("codAta");
				String codOrgao = req.getParameter("codOrgao");
				
				req.setAttribute("codAta", codAta);
				req.setAttribute("codOrgao", codOrgao);
				req.setAttribute("codRelator", codRelator);
				
				nextRetorno = "/REC/RelatorioProcessosPorAta.jsp";
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}
	
	protected String processaProcessos(HttpServletRequest req, String tipoRequerimento, String codAta, String descAta, String codRelator, String nomeRelator) throws sys.CommandException {
	 	String nextRetorno=jspPadrao;
		String codOrgao = req.getParameter("codOrgao");
	 	try {
	 		String acao           = req.getParameter("acao");  
	 		Ata ata = new Ata();
	 		if(acao==null)   acao = " ";
	 		
	 		if(acao.equals("infResult")){
	 			ata.getProcessoInfResultMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator);
			}
	 		
	 		if(acao.equals("Processar")){
	 			ata.getProcessoSemAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
	 			ata.getProcessoComAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
			}
	 		
	 		
			if(acao.equals("buscaProcessos")){
	 			ata.getProcessoSemAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
	 			ata.getProcessoComAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
			}
 		}
 	  	catch (Exception e){
 	  		nextRetorno = "/REC/ErroDataAta.jsp" ;
			return nextRetorno;
 	  	}
 	 	return nextRetorno  ;
 	}
}
