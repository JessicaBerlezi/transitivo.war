package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import ACSS.UsuarioBean;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Informar Resultado Bean<br>
* <b>Description:</b>  Informar Resultado Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello
* @version 1.0
*/

public class consultaAutoBean   { 

private String msgErro ; 
private String msgOk ; 
private String statusEscolhidos; 
private String orgaosEscolhidos;
private String [] statusMarcados;
private String [] orgaosMarcados;
private String numAutoInfracao;
private String numPlaca;
private String numRenavam; 
private String numCPF;
private String indTipoCNH;    
private String numCNH;
private String datInfracaoDe  ;
private String datInfracaoAte  ;
private String datReferencia ;
private String numProcesso;
private String abrevSist;
private String sigFuncao;
private String naoNotificado;

private String indFase;    
private String indSituacao;    
private String indPago;    

private List Autos;  
private List notifs; 
private String ordClass ;
private String ordem ;
//
private String codOrgaoAtuacao;


  public consultaAutoBean()  throws sys.BeanException {
	msgErro          = "" ;
	msgOk            = "" ;	
	statusEscolhidos = "";
	orgaosEscolhidos = "";
	statusMarcados   = new String [0];
	orgaosMarcados   = new String [0];
	numAutoInfracao  = "";
	numPlaca         = "";
	numRenavam       = "";	
	numCPF         	 = "";
	indTipoCNH  	 = "2"; 	
	numCNH         	 = "";
	datInfracaoDe    = "22/01/1998";
	datInfracaoAte   = sys.Util.formatedToday().substring(0,10);
	datReferencia    = sys.Util.formatedToday().substring(0,10);
	numProcesso      = "";
	abrevSist        = "";	
	sigFuncao        = "";	
	codOrgaoAtuacao	 = "";
	ordClass         = "ascendente";
	ordem            = "Data";
	naoNotificado    = "1" ;

	indFase          = "9";    
	indSituacao      = "9  ";    
	indPago          = "9";    

	Autos  		     = new ArrayList() ;		
	notifs 		     = new ArrayList() ;
  }  
  
//--------------------------------------------------------------------------  
 public void setNumAutoInfracao(String numAutoInfracao)  {
  this.numAutoInfracao=numAutoInfracao ;
    if (numAutoInfracao==null) this.numAutoInfracao= "";
  }  
  public String getNumAutoInfracao()  {
  	return this.numAutoInfracao;
  }  
  public void setNumPlaca(String numPlaca)  {
  this.numPlaca=numPlaca ;
    if (numPlaca==null) this.numPlaca= "";
  }  
  public String getNumPlaca()  {
  	return this.numPlaca;
  }
  public void setNumRenavam(String numRenavam)  {
  this.numRenavam=numRenavam ;
    if (numRenavam==null) this.numRenavam= "";
  }  
  public String getNumRenavam()  {
  	return this.numRenavam;
  }  
  //--------------------------------------------------------------------------
  public void setDatInfracaoDe(String datInfracaoDe)  {
  this.datInfracaoDe=datInfracaoDe ;
    if (datInfracaoDe==null) this.datInfracaoDe= "22/01/1998";
  }  
  public String getDatInfracaoDe()  {
  	return this.datInfracaoDe;
  }
//--------------------------------------------------------------------------  
public void setDatInfracaoAte(String datInfracaoAte)  {
  this.datInfracaoAte=datInfracaoAte ;
    if (datInfracaoAte==null) this.datInfracaoAte= sys.Util.formatedToday().substring(0,10);;
  }  
  public String getDatInfracaoAte()  {
  	return this.datInfracaoAte;
  }
//--------------------------------------------------------------------------    
public void setMsgErro(Vector vErro)   {
   	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
     }
   }
  public void setMsgErro(String msgErro)   {
  	this.msgErro = msgErro ;
   }   
   
   public String getMsgErro()   {
   	return this.msgErro;
   }   
   public void setMsgOk(String msgOk)   {
	this.msgOk = msgOk ;
	}   
   public String getMsgOk()   {
		return this.msgOk;
	}     
//--------------------------------------------------------------------------   
 public void setNumCPFEdt(String numCPF)  {
    if ((numCPF==null) || ("".equals(numCPF)))this.numCPF = "";
	else {
	    if (numCPF.length()<14) numCPF += "               ";
   		this.numCPF=numCPF.substring(0,3)+ numCPF.substring(4,7) + numCPF.substring(8,11)+ numCPF.substring(12,14);
	}	
  }  
  public String getNumCPFEdt()  {
  if ("".equals(this.numCPF.trim())) return this.numCPF.trim() ;
  	return this.numCPF=numCPF.substring(0,3)+ "." + numCPF.substring(3,6) 
  		+ "." + numCPF.substring(6,9) + "-" + numCPF.substring(9,11) ;
  } 
 public void setNumCPF(String numCPF)  {
  this.numCPF=numCPF ;
    if (numCPF==null) this.numCPF= "";
  }  
  public String getNumCPF()  {
  	return this.numCPF;
  }   
//--------------------------------------------------------------------------   
public void setNumProcesso(String numProcesso)  {
  this.numProcesso=numProcesso ;
    if (numProcesso==null) this.numProcesso= "";
  }  
  public String getNumProcesso()  {
  	return this.numProcesso;
  }
//--------------------------------------------------------------------------  
  public void setStatusEscolhidos(String statusEscolhidos)  {
  this.statusEscolhidos=statusEscolhidos ;
    if (statusEscolhidos==null) this.statusEscolhidos= "";	
  }  
  public String getStatusEscolhidos()  {
  	return statusEscolhidos;
  } 
//--------------------------------------------------------------------------  
  public void setStatusMarcados(String[] statusMarcados)  {
    if (statusMarcados==null) statusMarcados= new String [0];
  	this.statusMarcados=statusMarcados ;
    String o = "";
	String aux = "" ;
    for (int i = 0; i<this.statusMarcados.length;i++) {				 
    	aux = "0000000000"+this.statusMarcados[i] ;
    	o += aux.substring(aux.length()-10,aux.length()) +",";				 
    }
	if (o.length()>1) o = o.substring(0,o.length()-1);		
    setStatusEscolhidos(o);
  }  
  public String[] getStatusMarcados()  {
  	return statusMarcados;
  } 
//--------------------------------------------------------------------------   
public void setNaoNotificado(String naoNotificado)  {
  this.naoNotificado=naoNotificado ;
    if (naoNotificado==null) this.naoNotificado= "";
	if ("1".equals(this.naoNotificado))  {
		setStatusEscolhidos("0000000000,0000000001,0000000002,0000000010,0000000011,0000000012");
	}
  }  
  public String getNaoNotificado()  {
  	return this.naoNotificado;
  } 
  //--------------------------------------------------------------------------  
  public void setOrgaosMarcados(String[] orgaosMarcados)  {
    if (orgaosMarcados==null) orgaosMarcados= new String [0];
	this.orgaosMarcados=orgaosMarcados ;
    String o = "";
	String aux = "" ;
    for (int i = 0; i<this.orgaosMarcados.length;i++) {				 
    	aux = "000000"+this.orgaosMarcados[i] ;
    	o += aux.substring(aux.length()-6,aux.length()) +",";				 
    }
	if (o.length()>1) o = o.substring(0,o.length()-1);
    	setOrgaosEscolhidos(o);
  }  
  public String[] getOrgaosMarcados()  {
  	return orgaosMarcados;
  } 
//--------------------------------------------------------------------------  
  public void setOrgaosEscolhidos(String orgaosEscolhidos)  {
  this.orgaosEscolhidos=orgaosEscolhidos ;
    if (orgaosEscolhidos==null) this.orgaosEscolhidos= "";
  }  
  public String getOrgaosEscolhidos()  {
  	return orgaosEscolhidos;
  } 
//--------------------------------------------------------------------------  
 public void setNumCNH(String numCNH)  {
  this.numCNH=numCNH ;
    if (numCNH==null) this.numCNH= "";
  }  
  public String getNumCNH()  {
  	return this.numCNH;
  }   
//--------------------------------------------------------------------------  
 public void setIndTipoCNH(String indTipoCNH)  {
  this.indTipoCNH=indTipoCNH ;
    if (indTipoCNH==null) this.indTipoCNH= "";
  }  
  public String getIndTipoCNH()  {
  	return this.indTipoCNH;
  }       
//--------------------------------------------------------------------------    
  public void setAutos(List Autos)  {
	 this.Autos=Autos ;	
   }
  public List getAutos()  {
	return this.Autos;
  }
  public AutoInfracaoBean getAutos(int i)  throws sys.BeanException {
	try {
	  	if ((i<0) || (i>=this.Autos.size()) ) return (new AutoInfracaoBean()) ;
	}
    catch (Exception e) { 			
	    throw new sys.BeanException(e.getMessage());
    }
    return (AutoInfracaoBean)this.Autos.get(i);
  }
//--------------------------------------------------------------------------    
  public void setNotifs(List notifs)  {
	 this.notifs=notifs ;	
   }
  public List getNotifs()  {
	return this.notifs;
  }
  public InstrNotBean getNotifs(int i)  throws sys.BeanException {
	try {
		if ((i<0) || (i>=this.notifs.size()) ) return (new InstrNotBean()) ;
	}
	catch (Exception e) { 			
		throw new sys.BeanException(e.getMessage());
	}
	return (InstrNotBean)this.notifs.get(i);
  }  
//--------------------------------------------------------------------------  
  public void setAbrevSist(String abrevSist)  {
	this.abrevSist=abrevSist ;
	  if (abrevSist==null) this.abrevSist= "";
	}  
	public String getAbrevSist()  {
	  return this.abrevSist;
	}   
//	--------------------------------------------------------------------------  
	public void setSigFuncao(String sigFuncao)  {
	  this.sigFuncao=sigFuncao ;
		if (sigFuncao==null) this.sigFuncao= "";
	  }  
	  public String getSigFuncao()  {
		return this.sigFuncao;
	  }  	
//--------------------------------------------------------------------------
public void setCodOrgaoAtuacao(String codOrgaoAtuacao)  {
  this.codOrgaoAtuacao=codOrgaoAtuacao ;
    if (codOrgaoAtuacao==null) this.codOrgaoAtuacao= "";
  }  
  public String getCodOrgaoAtuacao()  {
  	return this.codOrgaoAtuacao;
  }
//--------------------------------------------------------------------------  
 public void setIndFase(String indFase)  {
  if ((indFase==null) || (indFase.length()==0)) indFase= "9";
  if ("129".indexOf(indFase)<0) indFase= "9";
  this.indFase=indFase ;
  }  
  public String getIndFase()  {
  	return this.indFase;
  }   
  
  public String getIndFaseDesc()  {
	  String desc = "Todas";
	  if (getIndFase().equals("1"))        desc  = "Autua��o";	 
	  if ("2,3".indexOf(getIndFase())>=0)  desc  = "Penalidade";	  
	  return desc;
	  }   
  
  
//--------------------------------------------------------------------------  
 public void setIndSituacao(String indSituacao)  {
    if ((indSituacao==null) || (indSituacao.length()==0)) indSituacao= "9  ";
    if ("123459".indexOf(indSituacao)<0) indSituacao= "9  ";
    this.indSituacao=indSituacao ;
  }  
  public String getIndSituacao()  {
  	return this.indSituacao;
  }  
  public String getIndSituacaoDesc()  {
	  String desc = "Todas";
	  if (getIndSituacao().equals("1"))	desc="Ativo";
	  if (getIndSituacao().equals("2"))	desc="Em Recurso";
	  if (getIndSituacao().equals("3"))	desc="Suspensos";				
	  if (getIndSituacao().equals("4")) desc="Cancelados";    
	  if (getIndSituacao().equals("5"))	desc="Agravos";	  
	  return desc;
  }  
//--------------------------------------------------------------------------  
 public void setIndPago(String indPago)  {
	 if ((indPago==null) || (indPago.length()==0)) indPago= "9";
	 if ("129".indexOf(indFase)<0) indFase= "9";
	 this.indPago=indPago ;
  }  
  public String getIndPago()  {
  	return this.indPago;
  }    
  
  
  public String getIndPagoDesc()  {
	  String desc = "Todas";
	  if (getIndPago().equals("2")) desc="Pago";
	  if (getIndPago().equals("1"))	desc="N�o Pago";	
	  return desc;
  }  
//--------------------------------------------------------------------------    
public AutoInfracaoBean getAutos(String mynumAutoInfracao)  throws sys.BeanException {
	  try { 	 
	  	AutoInfracaoBean Aut = new AutoInfracaoBean();					
  		Iterator itx  = getAutos().iterator() ;	
	  	boolean achou = false;
  		while (itx.hasNext()) {
  			Aut =(AutoInfracaoBean)itx.next() ;
	  		if (Aut.getNumAutoInfracao().equals(mynumAutoInfracao)) {
  				achou = true;
  				break;
	  		}
  		}
	  	if (achou==false) Aut = new AutoInfracaoBean();
		return Aut;		
	}
	catch (Exception ex) {
	  		throw new sys.BeanException(ex.getMessage());
	}
  }
  public void ConsultaAutos(UsuarioBean myUsuario)  throws sys.BeanException {
	try {			
	      DaoBroker dao = DaoBrokerFactory.getInstance();	      
		  if ( dao.ConsultaAutos(this,myUsuario)) {
			Classifica("Data");
		  }	   		   
	}// fim do try
	catch (Exception ex) {
		throw new sys.BeanException(ex.getMessage());
	}
	return;
  }
   
  public void ConsultaAutosPostos(UsuarioBean UsrLogado)  throws sys.BeanException {
  	try {      
  		ArrayList autosposto = new ArrayList();  
  		for(int t=0;t<this.getAutos().size();t++){  			
  			AutoInfracaoBean auto = (AutoInfracaoBean) this.getAutos().get(t);
  			auto.LeHistorico(UsrLogado);  			 		
  			Iterator itx  = auto.getHistoricos().iterator() ;
  			REC.HistoricoBean hist = new REC.HistoricoBean();
  			while (itx.hasNext()) {	    			
  				hist =(REC.HistoricoBean)itx.next() ;  				
  				if ((hist.getCodStatus().equals("2")) || ((hist.getCodStatus().equals("12")))){  					
					autosposto.add(auto);				
  					break;
  				}							
  			}	
  		}	
  		this.setAutos(autosposto);
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.BeanException(ex.getMessage());
  	}
  	return;
  } 
  
  /**--------------------------------------------------------------------
   * M�todo de ordena��o de uma lista 
   *----------------------------------------------------------------------
   */
//	--------------------------------------------------------------------------
	  public void setOrdClass(String ordClass)  {
		  this.ordClass=ordClass ;
		  if (ordClass==null) this.ordClass= "ascendente";
	  } 
	  public String getOrdClass()  {
		  return this.ordClass;
	  }
//	--------------------------------------------------------------------------
	  public void setOrdem(String ordem)  {
		  this.ordem=ordem ;
		  if (ordem==null) this.ordem= "Data";
	  }  
	  public String getOrdem()  {
		  return this.ordem;
	  }
	  public String getNomOrdem()  {
		  String nomOrdem = "Data Infra��o, Org�o e Num Auto" ;
		  if (this.ordem.equals("Orgao"))       nomOrdem = "Org�o e Num Auto" ;
		  if (this.ordem.equals("Placa"))       nomOrdem = "Placa e Data Infra��o" ;   
		  if (this.ordem.equals("Processo"))    nomOrdem = "Processo" ;   
		  if (this.ordem.equals("Auto"))        nomOrdem = "Auto de Infra��o" ;   
		  if (this.ordem.equals("Responsavel")) nomOrdem = "Respons�vel e Placa" ;  
		  if (this.ordem.equals("Status"))      nomOrdem = "Status e Data Infra��o" ;
		  if (this.ordem.equals("DatProcesso"))         nomOrdem = "Data do Processo e Processo"  ;  
		  if (this.ordem.equals("Enquadramento"))       nomOrdem = "Enquadramento e Processo"  ; 			    
		  return nomOrdem+ " ("+getOrdClass()+")" ;
	}
//	--------------------------------------------------------------------------
	  public void Classifica(String ordemSol) throws sys.BeanException {
		  int ord = 0;
		  if ((ordemSol==null) || (ordem.equals(""))) ord = 0 ;
		  if (ordemSol.equals("Orgao"))               ord = 1 ;
		  if (ordemSol.equals("Placa"))               ord = 2 ;   
		  if (ordemSol.equals("Processo"))            ord = 3 ;  
		  if (ordemSol.equals("Responsavel"))         ord = 4 ; 
		  if (ordemSol.equals("Auto"))                ord = 5 ;  
		  if (ordemSol.equals("Status"))              ord = 6 ; 
		  if (ordemSol.equals("DatProcesso"))         ord = 7 ;  
		  if (ordemSol.equals("Enquadramento"))       ord = 8 ; 			
		  boolean troca    = false ;
		  boolean trocaasc = true ;
		  boolean trocades = false ;
		  if (ordemSol.equals(getOrdem()))   {
			  if ( getOrdClass().equals("ascendente")) {
				  trocaasc = false ;
				  trocades = true ;
				  setOrdClass("descendente"); 
			  } 
			  else {
				  trocaasc = true ;
				  trocades = false;
				  setOrdClass("ascendente"); 
			  }
		  }
		  else setOrdClass("ascendente");
		  setOrdem(ordemSol) ;
		  int tam = getAutos().size() ;
		  AutoInfracaoBean tmp = new AutoInfracaoBean();
		  String n1,n2,d1,d2,o1,o2,s1,s2 ;  
		  for (int i=0; i<tam; i++)  {  
			  for (int j=i+1; j<tam; j++)  {
				  troca=false;
				  // compara os campos
				  switch (ord) {
				  // "Data Infra��o, Org�o e Num Auto" ;
				  case 0:
					  d1 = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()); 
					  d2 = sys.Util.formataDataYYYYMMDD(getAutos(j).getDatInfracao()); 
					  o1 = sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) ;
					  o2 = sys.Util.lPad(getAutos(j).getCodOrgao(),"0",6) ;
					  n1 = sys.Util.rPad(getAutos(i).getNumAutoInfracao()," ",12);
					  n2 = sys.Util.rPad(getAutos(j).getNumAutoInfracao()," ",12);
					  if ((d1+o1+n1).compareTo(d2+o2+n2)>0 ) troca=trocaasc;
					  else troca=trocades ;
					  break;
				  // "Org�o e Num Auto" ;
				  case 1:
					  o1 = sys.Util.lPad(getAutos(i).getCodOrgao(),"0",6) ;
					  o2 = sys.Util.lPad(getAutos(j).getCodOrgao(),"0",6) ;
					  if (o1.compareTo(o2)>0 ) troca=trocaasc;	
					  else 	{
						  if ( (o1.equals(o2)) &&
							   (getAutos(i).getNumAutoInfracao().compareTo(getAutos(j).getNumAutoInfracao())>0)) troca=trocaasc;
						   else troca=trocades ;
					  }			
					  break;
				  // "Placa e Data Infra��o" ;   
				  case 2:
					  d1 = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()); 
					  d2 = sys.Util.formataDataYYYYMMDD(getAutos(j).getDatInfracao()); 			 
					  if (getAutos(i).getNumPlaca().compareTo(getAutos(j).getNumPlaca())>0 ) troca=trocaasc;	
					  else 	{
						  if ( (getAutos(i).getNumPlaca().equals(getAutos(j).getNumPlaca())) &&
							   (d1.compareTo(d2)>0)) troca=trocaasc;
						   else troca=trocades ;
					  }			
					  break;
				  // "Processo" ;   
				  case 3:
					  if (getAutos(i).getNumProcesso().compareTo(getAutos(j).getNumProcesso())>0 ) troca=trocaasc;
					  else troca=trocades ;	
					  break;
				  // "Respons�vel e Placa" ; 
				  case 4:
					  if (getAutos(i).getProprietario().getNomResponsavel().compareTo(getAutos(j).getProprietario().getNomResponsavel())>0 ) troca=trocaasc;	
					  else 	{
						  if ( (getAutos(i).getProprietario().getNomResponsavel().equals(getAutos(j).getProprietario().getNomResponsavel())) &&
							   (getAutos(i).getNumPlaca().compareTo(getAutos(j).getNumPlaca())>0)) troca=trocaasc;
						   else troca=trocades ;
					  }	
				  // Status e Data da Infracao
				  case 6:	
					  s1 = sys.Util.lPad(getAutos(i).getCodStatus(),"0",3) ;
					  s2 = sys.Util.lPad(getAutos(j).getCodStatus(),"0",3) ;
					  d1 = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatInfracao()); 
					  d2 = sys.Util.formataDataYYYYMMDD(getAutos(j).getDatInfracao()); 
					  if (s1.compareTo(s2)>0 ) troca=trocaasc;	
					  else 	{
						  if ( (s1.equals(s2)) &&
							   (d1.compareTo(d2)>0)) troca=trocaasc;
						   else troca=trocades ;
					  }			
					  break;
					// "Data Processo, Processo" ;
					case 7:
						d1 = sys.Util.formataDataYYYYMMDD(getAutos(i).getDatProcesso()); 
						d2 = sys.Util.formataDataYYYYMMDD(getAutos(j).getDatProcesso()); 
						o1 = sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
						o2 = sys.Util.rPad(getAutos(j).getNumProcesso()," ",20) ;
						if ((d1+o1).compareTo(d2+o2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
					// "Enquadramento, Processo" ;
					case 8:
						d1 = sys.Util.rPad(getAutos(i).getInfracao().getDscEnquadramento()," ",50); 
						d2 = sys.Util.rPad(getAutos(j).getInfracao().getDscEnquadramento()," ",50); 
						o1 = sys.Util.rPad(getAutos(i).getNumProcesso()," ",20) ;
						o2 = sys.Util.rPad(getAutos(j).getNumProcesso()," ",20) ;
						if ((d1+o1).compareTo(d2+o2)>0 ) troca=trocaasc;
						else troca=trocades ;
						break;
				  default:
					  if (getAutos(i).getNumAutoInfracao().compareTo(getAutos(j).getNumAutoInfracao())>0 ) troca=trocaasc;	
					  else troca=trocades ;	
			  }	
			  if (troca) 	{		
				 tmp = (AutoInfracaoBean)getAutos(i);
				 getAutos().set(i,getAutos(j));
				 getAutos().set(j,tmp);
			  }
		  }
	  }
	}
	  
//	  --------------------------------------------------------------------------
	  public List getAutosPendentes(String numPlaca,String Auto)  {
		  List myAutosPendentes = null;
		  try  {
			  DaoBroker dao = DaoBrokerFactory.getInstance();	
			  if( (myAutosPendentes = dao.getAutosPendentes(numPlaca,Auto)) == null )
				  setMsgErro("Erro de Leitura:");
		  }
		  catch (Exception e) {
			  setMsgErro("Erro de Leitura: " + e.getMessage());
		  }
		  return myAutosPendentes ;
	  }
	  
//	  --------------------------------------------------------------------------
	  public List getAutosPendentesDol(String numPlaca,String Auto)  {
		  List myAutosPendentesDol = null;
		  try  {
			  DaoBroker dao = DaoBrokerFactory.getInstance();	
			  if( (myAutosPendentesDol = dao.getAutosPendentesDol(numPlaca,Auto)) == null )
				  setMsgErro("Erro de Leitura:");
		  }
		  catch (Exception e) {
			  setMsgErro("Erro de Leitura: " + e.getMessage());
		  }
		  return myAutosPendentesDol ;
	  }

	public String getDatReferencia() {
		return datReferencia;
	}

	
	public void setDatReferencia(String datReferencia)  {
		this.datReferencia=datReferencia ;
		if (datReferencia==null) this.datReferencia= sys.Util.formatedToday().substring(0,10);;
	}  
	
	  
	  
	  
//	--------------------------------------------------------------------------

  
}