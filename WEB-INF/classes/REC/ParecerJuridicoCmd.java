package REC;

import java.util.Iterator;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;
public class ParecerJuridicoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/ParecerJuridico.jsp" ;  
   
  public ParecerJuridicoCmd() {
    next             =  jspPadrao;
  }

  public ParecerJuridicoCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			

	    // cria os Beans, se n�o existir
	    RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
	    if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			

	    ParecerJuridicoBean ParecerJuridicoId     = (ParecerJuridicoBean)req.getAttribute("ParecerJuridicoId") ;
	    if (ParecerJuridicoId==null)  ParecerJuridicoId = new ParecerJuridicoBean() ;	  			
	    ParecerJuridicoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));

	    AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			

	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    ParecerJuridicoId.setEventoOK(AutoInfracaoBeanId) ;
	    }

	    if  (acao.equals("LeReq"))  {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	        ParecerJuridicoId.setEventoOK(AutoInfracaoBeanId) ;
	        RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
	        if (RequerimentoId.getCodRequerimento().length()!=0) {
	    		RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
	    		if (RequerimentoId.getDatPJ().length()==0) RequerimentoId.setDatPJ(sys.Util.formatedToday().substring(0,10)) ;			
	        }
	    }	
	    if  (acao.equals("ParecerJuridico"))  {
	      	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	    	ParecerJuridicoId.setEventoOK(AutoInfracaoBeanId) ;
	    	nextRetorno = processaParecer(ParecerJuridicoId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
	    }
		if (acao.equals("imprimirInf")) {	
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				ParecerJuridicoId.setEventoOK(AutoInfracaoBeanId) ;
				nextRetorno = "/REC/AutoImp.jsp" ;
		}	
				
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	    req.setAttribute("RequerimentoId",RequerimentoId) ;		
	    req.setAttribute("ParecerJuridicoId",ParecerJuridicoId) ;		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("ParecerJuridicoCmd1: " + ue.getMessage());
    }
	return nextRetorno;
  }
  private String processaParecer(ParecerJuridicoBean ParecerJuridicoId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
   	String nextRetorno = next ;
   	try {
   		myAuto.setMsgErro("") ;
  		Vector vErro = new Vector() ;
  		// validar se status e igual a 5
  		if ("S".equals(ParecerJuridicoId.getEventoOK())==false) vErro.addElement(ParecerJuridicoId.getMsg()+" \n") ;
  		myReq.setCodRequerimento(req.getParameter("codRequerimento"));
  		if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado. \n") ;
  		// localizar o requerimento e validar status
  		boolean existe = false ;		
  		Iterator it = myAuto.getRequerimentos().iterator() ;
  		REC.RequerimentoBean reqIt  = new REC.RequerimentoBean();
  		while (it.hasNext()) {
  			reqIt =(REC.RequerimentoBean)it.next() ;
  			if ( (ParecerJuridicoId.getTipoReqValidos().indexOf(reqIt.getCodTipoSolic())>=0) && 
  			     (ParecerJuridicoId.getStatusReqValido().indexOf(reqIt.getCodStatusRequerimento())>=0) &&
  				 (reqIt.getCodRequerimento().equals(myReq.getCodRequerimento())) ) { 
  				  myReq.copiaRequerimento(reqIt) ;					   
  				  existe = true ;
  				  break ;
  			}
  		}
  		if (existe==false) vErro.addElement("Requerimento n�o localizado no Auto de Infra��o. \n") ;
  		String datPJ = req.getParameter("datPJ");
  		if (datPJ==null) datPJ = "";
  		if (sys.Util.DifereDias( datPJ,myReq.getDatRequerimento())>0) vErro.addElement("Data n�o pode ser anterior ao Requerimento ("+myReq.getDatRequerimento()+"). \n") ;
		String sta = "2" ;
		if ("2".equals(myReq.getCodStatusRequerimento())) {
			// Estorno de Parecer		
			//validar data de entrada
			if (datPJ.length()==0) vErro.addElement("Data do Estorno do Parecer n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),datPJ)>0) vErro.addElement("Data do Estorno n�o pode ser superior a hoje. \n") ;
			if (sys.Util.DifereDias( datPJ,myReq.getDatPJ())>0) vErro.addElement("Data do Estorno n�o pode ser anterior ao Parecer. \n") ;
			sta ="0" ;
			myReq.setCodParecerPJ("");
	        myReq.setCodRespPJ("");
	        myReq.setCodJuntaJU("");
	        myReq.setCodRelatorJU("");
		}
		// Ver se � Inclusao de Resultado
		else {
			//validar data de entrada
			if (datPJ.length()==0) vErro.addElement("Data do Parecer n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),datPJ)>0) vErro.addElement("Data do Parecer n�o pode ser superior a hoje. \n") ;
			myReq.setCodRespPJ(req.getParameter("codRespPJ"));
			if (myReq.getCodRespPJ().length()==0) vErro.addElement("Respons�vel pelo Parecer n�o selecionado. \n") ;
			sta ="2" ;
			myReq.setCodParecerPJ(req.getParameter("codParecerPJ"));
			if (myReq.getCodParecerPJ().length()==0) vErro.addElement("Parecer n�o selecionado. \n") ;
		}		
		myReq.setTxtMotivoPJ(req.getParameter("txtMotivoPJ"));
		if (myReq.getTxtMotivoPJ().length()==0) vErro.addElement("Preenchimento do Motivo � obrigat�rio. \n") ;		

		myReq.setCodStatusRequerimento(sta);		
		myReq.setDatPJ(datPJ);
		myReq.setDatJU(datPJ);
		
		if (vErro.size()==0) {
			if ("2".equals(sta)) {
				// localizar se ja tem junta
				Iterator itx = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean reqItx  = new REC.RequerimentoBean();
				while (itx.hasNext()) {
					reqItx =(REC.RequerimentoBean)itx.next() ;
					// Requerimento de DP ou DC com status 0,2
					if ( ("D".equals(reqItx.getCodTipoSolic().substring(0,1))) &&
						 ( (reqItx.getCodJuntaJU().length()>0) && (reqItx.getCodRelatorJU().length()>0) ) ) {
							// vai para mesma junta e relator
							myReq.setCodJuntaJU(reqItx.getCodJuntaJU());
							myReq.setCodRelatorJU(reqItx.getCodRelatorJU());
							break;
					}
				}	
			}
			myReq.setNomUserNamePJ(UsrLogado.getNomUserName());
			myReq.setCodOrgaoLotacaoPJ(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcPJ(sys.Util.formatedToday().substring(0,10));		
			myReq.setNomUserNameJU(UsrLogado.getNomUserName());
			myReq.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcJU(sys.Util.formatedToday().substring(0,10));		
			atualizarReq(ParecerJuridicoId,myAuto,myReq,UsrLogado) ;	
			if (myAuto.getMsgErro().length()==0) {  		
				myAuto.setMsgErro("Parecer "+("0".equals(sta) ? "estornado" : "inclu�do")+" para o Requerimento:"+myReq.getNumRequerimento()) ;	
				myAuto.setMsgOk("S") ;
			}						
		}
		else myAuto.setMsgErro(vErro) ;
	}
    catch (Exception e){
  	    throw new sys.CommandException("ParecerJuridicoCmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }
  private void atualizarReq(ParecerJuridicoBean ParecerJuridicoId,AutoInfracaoBean myAuto,RequerimentoBean myReq,UsuarioBean UsrLogado) throws sys.CommandException { 
  	try { 	
  		// preparar Bean de Historico
  		HistoricoBean myHis = new HistoricoBean() ;

  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
		myHis.setCodOrgao(myAuto.getCodOrgao());
  		myHis.setCodStatus(myAuto.getCodStatus());  
  		myHis.setDatStatus(myAuto.getDatStatus());
  		myHis.setCodEvento(ParecerJuridicoId.getCodEvento());
  		myHis.setCodOrigemEvento(ParecerJuridicoId.getOrigemEvento());
		
  		myHis.setNomUserName(myReq.getNomUserNamePJ());
  		myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoPJ());
  		myHis.setDatProc(myReq.getDatProcPJ());

  		myHis.setTxtComplemento01(myReq.getNumRequerimento());	
  		myHis.setTxtComplemento02(myReq.getDatPJ());	
  		myHis.setTxtComplemento03(myReq.getCodParecerPJ());	
  		myHis.setTxtComplemento04(myReq.getTxtMotivoPJ());	
  		myHis.setTxtComplemento05(myReq.getCodRespPJ());	
//  		myHis.setTxtComplemento06(myReq.getCodJuntaJU());	
//  		myHis.setTxtComplemento07(myReq.getCodRelatorJU());	

  	    DaoBroker dao = DaoBrokerFactory.getInstance();
  	    dao.atualizarAuto207(myAuto,myReq,myHis,UsrLogado) ; 
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException(ex.getMessage());
  	}
  	return  ;
  }	
}
