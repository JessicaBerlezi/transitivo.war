package REC;

import java.util.Iterator;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
* <b>Description:</b>  Abrir Recurso Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class AbrirRecursoBean   extends EventoBean   { 

  private String tpSolDP;  
  private String tpSolDC;  
  private String tpSolTR;  
  private String msgDP;  
  private String msgDC;  
  private String msgTR;  
  private String obrigaTR;    

  private String datEntrada;    

  private String getNFuncao;
  private String nomCondutorTR;
  public AbrirRecursoBean()  throws sys.BeanException {

  super() ;	
  tpSolDP    = "";  
  tpSolDC    = "";  
  tpSolTR    = "";  
  msgDP      = "";  
  msgDC      = "";  
  msgTR      = "";  
  obrigaTR   = "N"; 
  datEntrada = sys.Util.formatedToday().substring(0,10) ;  
  getNFuncao = ""; 
  nomCondutorTR = ""; 
  
  }
  
//--------------------------------------------------------------------------  
  public void setEventoOK(ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)  throws sys.BeanException  {
	if ("".equals(myAuto.getNomStatus())) {
		setMsg("");
		setEventoOK("S");	
	}
	else {
  	  setMsg(myAuto.getNomStatus()+" (Data do Status: "+myAuto.getDatStatus()+")");
	  if (myAuto.getNumProcesso().length()==0) setMsg(getMsg() + " - sem Processo");
	  String complMsg = "" ;
	  try {
		  	setEventoOK("S");	  
		  // Status 0,1,2,4,5,10,11,12,14,15,16
		  if ( ((myAuto.getCodStatus().length()==1) && (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA","10,11,12,14,15,16,82","3").indexOf(myAuto.getCodStatus())<0) )  ||
		       ((myAuto.getCodStatus().length()>1 ) && ((myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE","10,11,12,14,15,16,82","3")+","+myParam.getParamOrgao("STATUS_ABRIR_2INSTANCIA","30,35,36","4")).indexOf(myAuto.getCodStatus())<0) ) )  
			{
		  	setEventoOK("N");
		  	complMsg = " - N�o pode entrar com Recurso/Def.Pr�via/TRI";
		  }
		  // Status 0,1,2,4,5
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA","0,1,2,4,5","1").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()<2) )	{
		  	complMsg = " - Defesa Pr�via/TRI";
		  	setNFuncao("DEFESA PR�VIA") ;
			
			tpSolTR = getTR(myParam,myAuto);
			tpSolDP = getDP("DP",myParam,myAuto);
			tpSolDC = getDC("DC",myParam,myAuto);			
		  }
		      
		  // Status 10,11,12,14,15,16	
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE","10,11,12,14,15,16,82","3").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) {
		  	complMsg = " - Entrar com Recurso 1a.Inst�ncia";
		  	setNFuncao("RECURSO 1a INST�NCIA") ;
		  	tpSolTR = "";
			msgTR   = "";
		  	tpSolDP = getDP("1P",myParam,myAuto);
		  	tpSolDC = getDC("1C",myParam,myAuto);			
		  }
		  // Status 30,35,36	
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_2INSTANCIA","30,35,36","4").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) {
		  	complMsg = " - Entrar com Recurso 2a.Inst�ncia";
		  	setNFuncao("RECURSO 2a INST�NCIA") ;
		  	tpSolTR = "";
		  	msgTR   = "";
		  	tpSolDP = getDP("2P",myParam,myAuto);
		  	tpSolDC = getDC("2C",myParam,myAuto);						
		  }		
		  if ( ("".equals(tpSolTR)) && ("".equals(tpSolDP)) && ("".equals(tpSolDC)) ) {
			  setEventoOK("N");
			  complMsg = " - N�o pode Entrar com Recurso/Def.Pr�via/TRI";
		  }
		  setMsg(getMsg() + complMsg) ;
	 }
	 catch (Exception ue) {
       throw new sys.BeanException("AbrirRecursoCmd: " + ue.getMessage());
	 }
	 
   }
  }   
   public String getNomTpSol(String tpSol)   {
   		String nom="";
		if ("TR".equals(tpSol)) nom="Troca de Real Infrator";
		if ("DP".equals(tpSol)) nom="Defesa Pr�via - Propriet�rio";	
		if ("DC".equals(tpSol)) nom="Defesa Pr�via - Condutor";	
		if ("1P".equals(tpSol)) nom="Recurso 1a Inst�ncia - Propriet�rio";	
		if ("1C".equals(tpSol)) nom="Recurso 1a Inst�ncia - Condutor";					
		if ("2P".equals(tpSol)) nom="Recurso 2a Inst�ncia - Propriet�rio";	
		if ("2C".equals(tpSol)) nom="Recurso 2a Inst�ncia - Condutor";					
						
    	return nom;
    } 
  
//--------------------------------------------------------------------------  
  public void setTpSolDP(String tpSolDP)  {
  	if (tpSolDP==null) tpSolDP=" " ;
  	this.tpSolDP = tpSolDP;
  }
  public String getTpSolDP()  {
  	return this.tpSolDP;
  }
//--------------------------------------------------------------------------  
  public void setTpSolDC(String tpSolDC)  {
  	if (tpSolDC==null) tpSolDC=" " ;
  	this.tpSolDC = tpSolDC;
  }
  public String getTpSolDC()  {
  	return this.tpSolDC;
  }
//--------------------------------------------------------------------------  
  public void setTpSolTR(String tpSolTR)  {
  	if (tpSolTR==null) tpSolTR=" " ;
  	this.tpSolTR = tpSolTR;
  }
  public String getTpSolTR()  {
  	return this.tpSolTR;
  }
//--------------------------------------------------------------------------  
  public void setMsgDP(String msgDP)  {
  	if (msgDP==null) msgDP=" " ;
  	this.msgDP = msgDP;
  }
  public String getMsgDP()  {
  	return this.msgDP;
  }
//--------------------------------------------------------------------------  
  public void setMsgDC(String msgDC)  {
  	if (msgDC==null) msgDC=" " ;
  	this.msgDC = msgDC;
  }
  public String getMsgDC()  {
  	return this.msgDC;
  }
//--------------------------------------------------------------------------  
  public void setMsgTR(String msgTR)  {
  	if (msgTR==null) msgTR=" " ;
  	this.msgTR = msgTR;
  }
  public String getMsgTR()  {
  	return this.msgTR;
  }
//--------------------------------------------------------------------------  
  public String getTR(ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)    throws sys.BeanException {
  	String tr ="";
	try {
  		this.msgTR="";
		tr ="TR";
		if ( "1".equals(myAuto.getIndCondutorIdentificado())==false ) {		
			// Infracao de proprietario e
			// proprietario igual ao condutor e proprietario e juridica (indCpfCnpj<>1)
			if ( ("P".equals(myAuto.getInfracao().getIndTipo())) && 
				 (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())) &&
				 ("1".equals(myAuto.getProprietario().getIndCpfCnpj())==false) ){
		  		tr = "" ;	
				this.msgTR = "Infra��o de Propriet�rio - n�o pode ter TRI" ;
			}
			else {
				Iterator it = myAuto.getRequerimentos().iterator() ;
				REC.RequerimentoBean req  = new REC.RequerimentoBean();		
	    		while (it.hasNext()) {
		  			req =(REC.RequerimentoBean)it.next() ;
	  				// Requerimento de TR ja interpos TRI
		  			if ( ("TR".indexOf(req.getCodTipoSolic())>=0) && ("7,8".indexOf(req.getCodStatusRequerimento())<0) ){
			  			this.msgTR =req.getCodTipoSolic()+" interposta em "+req.getDatRequerimento();
						tr ="";
	  					break ;
	  				}
		  		}
			}
		}
		else {
			this.msgTR="Condutor Identificado.";
			tr ="";
		}
  	}
	catch (Exception ex) {
  		throw new sys.BeanException(ex.getMessage());
  	}
  	return tr;
  }
//--------------------------------------------------------------------------  
  public String getDP(String tpSol,ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)    throws sys.BeanException {
	  String dp =tpSol;
	try {
		this.msgDP="";
		dp = DefPreviaOk(myAuto,tpSol,"P") ;
	}
  	catch (Exception ex) {
  		throw new sys.BeanException(ex.getMessage());
  	}
  	return dp;
  }
//--------------------------------------------------------------------------  
  public String getDC(String tpSol,ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)    throws sys.BeanException {
	String dc =tpSol;
	try {
		this.msgDC="";
		// Condutor nao identificado
		if ( "1".equals(myAuto.getIndCondutorIdentificado())==false ) {		
			// proprietario nao e o condutor
			if ( (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())==false) ){
				dc = DefPreviaOk(myAuto,tpSol,"C") ;							}
			// proprietario e igual ao condutor
			else {
				// Infracao de condutor
				if ( ("P".equals(myAuto.getInfracao().getIndTipo())==false) ){
					this.obrigaTR="S";
					dc = DefPreviaOk(myAuto,tpSol,"C") ;				
				}
				else {
					dc = "" ;
					this.msgDC = "Condutor Identificado." ;			
				}
			}
		}
		// Condutor Identificado
		else {
			// Infracao de Condutor e
		
			if ("P".equals(myAuto.getInfracao().getIndTipo())==false) {
				// proprietario nao e o condutor)
				if (myAuto.getProprietario().getCodResponsavel().equals(myAuto.getCondutor().getCodResponsavel())) 	{
					 this.msgDC = "Condutor igual ao Propriet�rio" ;
					dc = "" ;					 
				}
				else dc = DefPreviaOk(myAuto,tpSol,"C") ;				
			}
			else {
				dc = "" ;			
				this.msgDC = "Infra��o de Propriet�rio" ;
			}
		}
	}
	catch (Exception ex) {
		throw new sys.BeanException(ex.getMessage());
	}
  	return dc;
  }
//--------------------------------------------------------------------------  
  public String DefPreviaOk(REC.AutoInfracaoBean myAuto,String tpSol,String cond_prop)   throws sys.BeanException {
	String d =tpSol;
	try {
		ResponsavelBean myResp ;
		if ("P".equals(cond_prop)) myResp = myAuto.getProprietario() ;
		else myResp = myAuto.getCondutor() ;
	  	Iterator it = myAuto.getRequerimentos().iterator() ;
  		REC.RequerimentoBean req  = new REC.RequerimentoBean();
	  	while (it.hasNext()) {
  			req =(REC.RequerimentoBean)it.next() ;
			// verifica se e deste responsavel
  			if ( (myResp.getCodResponsavel().equals(req.getCodResponsavelDP())) && (tpSol.equals(req.getCodTipoSolic())) &&
  			     ("7,8".indexOf(req.getCodStatusRequerimento())<0) ) {
				if ("P".equals(cond_prop))  this.msgDP =req.getCodTipoSolic()+" interposta em "+req.getDatRequerimento();
				else 						this.msgDC =req.getCodTipoSolic()+" interposta em "+req.getDatRequerimento();
				d="";
				break ;
			}
  		}
	}
	catch (Exception ex) {
		throw new sys.BeanException(ex.getMessage());
	}
  	return d;
  }
//--------------------------------------------------------------------------  
  public String getObrigaTR()   {
	return obrigaTR ;	
  }
//--------------------------------------------------------------------------  
  public void setDatEntrada(String datEntrada)   {
  	if (datEntrada==null) datEntrada = sys.Util.formatedToday().substring(0,10) ;
	this.datEntrada=datEntrada ;	
  }
  public String getDatEntrada()   {
	return datEntrada ;	
  }
//--------------------------------------------------------------------------  
  public void setNomCondutorTR(String nomCondutorTR)  {
  	if (nomCondutorTR==null) nomCondutorTR=" " ;
  	this.nomCondutorTR = nomCondutorTR;
  }
  public String getNomCondutorTR()  {
  	return this.nomCondutorTR;
  }
//--------------------------------------------------------------------------  
}
