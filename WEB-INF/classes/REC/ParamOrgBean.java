package REC;


import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import sys.BeanException;
import sys.ServiceLocator;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ParamOrgBean extends sys.HtmlPopupBean {
	private static final String MYABREVSIST = "REC";
	private String codOrgao;
	private String codParametro;
	
	private String nomParam;
	private String nomDescricao;
	private String valParametro;
	private String numOrdem;
	private String codGrupo;
    private String nomUsrNameAlt;	

	private List nomParamOrgao;
	private List valParamOrgao;
	private String codOrgaoCp;
	

	public ParamOrgBean() {
		super();
		setTabela("TCAU_PARAM_ORGAO");
		setPopupWidth(35);
		codOrgao = "";
		codParametro = "";
		nomParam = "";
		nomDescricao = "";
		valParametro = "";
		numOrdem = "";
		nomUsrNameAlt = "";

		nomParamOrgao = new ArrayList();
		valParamOrgao = new ArrayList();
		codOrgaoCp = "";
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}

	public void setCodOrgaoCp(String codOrgaoCp) {
		this.codOrgaoCp = codOrgaoCp;
		if (codOrgaoCp == null)
			this.codOrgaoCp = "";
	}
	public String getCodOrgaoCp() {
		return this.codOrgaoCp;
	}

	public void setCodParametro(String codParametro) {
		this.codParametro = codParametro;
		if (codParametro == null)
			this.codParametro = "";
	}
	public String getCodParametro() {
		return this.codParametro;
	}

	public void setNomParam(String nomParam) {
		this.nomParam = nomParam;
		if (nomParam == null)
			this.nomParam = "";
	}
	public String getNomParam() {
		return this.nomParam;
	}
	

	public void setNomDescricao(String nomDescricao) {
		this.nomDescricao = nomDescricao;
		if (nomDescricao == null)
			this.nomDescricao = "";
	}
	public String getNomDescricao() {
		return this.nomDescricao;
	}

	public void setValParametro(String valParametro) {
		this.valParametro = valParametro;
		if (valParametro == null)
			this.valParametro = "";
	}
	public String getValParametro() {
		return this.valParametro;
	}

	public void setNumOrdem(String numOrdem) {
		this.numOrdem = numOrdem;
		if (numOrdem == null)
			this.numOrdem = "";
	}
	public String getNumOrdem() {
		return this.numOrdem;
	}

	public void setCodGrupo(String codGrupo) {
		this.codGrupo = codGrupo;
		if (codGrupo == null)
			this.codGrupo = "";
	}
	public String getCodGrupo() {
		return this.codGrupo;
	}
	
    public void setNomUsrNameAlt(String nomUsrNameAlt) {
        this.nomUsrNameAlt = nomUsrNameAlt;
        if(nomUsrNameAlt == null) 
            nomUsrNameAlt = "";
    }
    
    public String getNomUsrNameAlt() {
        return nomUsrNameAlt;
    }
	
	public void Le_ParamOrg() {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if (daoTabela.ParamOrgLeBean(this) == false) {
				this.codParametro = "0";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codParametro = "0";
			setPkid("0");
			setMsgErro(
				"Erro na leitura de Parametros do �rg�o: " + e.getMessage());
		} // fim do catch 
	}

	//--------------------------------------------------------------------------
	public void setNomParamOrgao(List nomParamOrgao) {
		this.nomParamOrgao = nomParamOrgao;
	}

	public List getNomParamOrgao() {
		return this.nomParamOrgao;
	}
	public String getParamOrgao(String myNomParam) throws sys.BeanException {
		try {
			 return getParamOrgao(myNomParam,"","7") ;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public String getParamOrgao(String myNomParam,String Padrao,String grupo) throws sys.BeanException {		
		Connection conn = null;				
		try {					
			String myVal = "";
			String re = "";
			Iterator itx = getNomParamOrgao().iterator();
			int i = 0;		
			boolean achou = false ;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myNomParam)) {						
					myVal = getValParamOrgao(i);
					achou = true ;
					break;
				}
				i++;
			}
			if (!achou){				     	   
				conn = ServiceLocator.getInstance().getConnection(MYABREVSIST);
				Statement stmt = conn.createStatement();					       
				this.setCodParametro("0");
				this.setNomParam(myNomParam);
				this.setNomDescricao(myNomParam);									
				this.setValParametro(Padrao);
				this.setNumOrdem("0");
				this.setCodOrgao(this.getCodOrgao());
				this.setCodGrupo(grupo);									
				REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();	
				daoTabela.ParamInsert(this,stmt) ;	
				stmt.close();
				conn.commit();
				this.getNomParamOrgao().add(myNomParam);
				this.getValParamOrgao().add(Padrao);							   
			}				
			return myVal;					
		}		
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					ServiceLocator.getInstance().setReleaseConnection(conn);
				}
				catch (Exception ey) {		}
			}
		}
	}
//--------------------------------------------------------------------------
	public void setValParamOrgao(List valParamOrgao) {
		this.valParamOrgao = valParamOrgao;
	}
	public void setValParamOrgao(String myNomParam,String myValParam) {
		Iterator itx = getNomParamOrgao().iterator();
		int i = 0;
		String re = "";
		while (itx.hasNext()) {
			re = (String) itx.next();
			if (re.equals(myNomParam)) {
				getValParamOrgao().set(i,myValParam);
				break;
			}
			i++;
		}
	}

	public List getValParamOrgao() {
		return this.valParamOrgao;
	}

	public String getValParamOrgao(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getValParamOrgao().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}

	//--------------------------------------------------------------------------
	public void PreparaParam(ACSS.UsuarioBean UsrLog)
		throws sys.BeanException {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.PreparaParam(this, UsrLog);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}

	//--------------------------------------------------------------------------

	//Verifica Se Parametro Existe
		public void CopiaParamOrgao(ACSS.OrgaoBean myOrg, ParamOrgBean myParam) throws BeanException {
			Vector vErro = new Vector();
			try {
				DaoTabelas daoTabela = DaoTabelas.getInstance();
				daoTabela.CopiaParam(myOrg, myParam);
			}
			catch (Exception e) {
				throw new sys.BeanException(
					"Erro ao efetuar verifica��o: " + e.getMessage());
			}
			setMsgErro(vErro);
		}
	
}