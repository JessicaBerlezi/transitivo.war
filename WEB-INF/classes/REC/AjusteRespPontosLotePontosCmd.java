package REC;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



/**
 * <b>Title:</b>      Recurso - Ajuste Manual<br>
 * <b>Description:</b>Transacao 413:Ajuste Manual do Respons�vewl pelos pontos<br>
 * <b>Copyright:</b>  Copyright (c) 2004<br>
 * <b>Company:</b>    MIND - Informatica<br>
 * @author Luciana Rocha
 * @version 1.0
 */
public class AjusteRespPontosLotePontosCmd extends sys.Command {

	private String next;
	private static final String jspPadrao = "/REC/AjusteRespPontosLotePontos.jsp";

	public AjusteRespPontosLotePontosCmd() {
		next = jspPadrao;
	}

	public AjusteRespPontosLotePontosCmd(String next) {
		this.next = next;
	}
	public void setNext(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		try {     
			sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
			cmd.setNext(this.next) ;
			nextRetorno = cmd.execute(req);		

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;


			consultaAutoBean consultaAutoId                 = (consultaAutoBean)session.getAttribute("consultaAutoId") ;
			if (consultaAutoId==null)  	consultaAutoId       = new consultaAutoBean() ;	  		    
			
			// cria os Beans, se n�o existir
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null) AutoInfracaoBeanId = new AutoInfracaoBean() ;

			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;

			
			String dProc = req.getParameter("datprocesso");
			if (dProc==null) dProc="";
			String nProc = req.getParameter("numprocesso");
			if (nProc==null) nProc="";
			String txtMotivo = req.getParameter("txtMotivo");
			if (txtMotivo==null) txtMotivo="";
			
			
			HistoricoBean myHist = new HistoricoBean();

			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if(acao==null) acao = " ";


			if((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AutoInfracaoBeanId.setMsgOk("S");
				consultaAutoId.setNumPlaca(AutoInfracaoBeanId.getNumPlaca());
				consultaAutoId.ConsultaAutos(UsrLogado);
			}else if (acao.equals("ConfirmaAjuste")) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
					String procClicado[]       = req.getParameterValues("Clicado");
					String procSelec[]         = req.getParameterValues("Selecionado");
					String procSelecPlaca[]    = req.getParameterValues("SelecionadoPlaca");
					String procSelecAuto[]     = req.getParameterValues("SelecionadoAuto");
					String procSelecProcesso[] = req.getParameterValues("SelecionadoProcesso");
					AutoInfracaoBean AutoPontos = new AutoInfracaoBean() ;
					int n =0 ;
					String sNumPLaca="",sNnumAutoInfracao="",sNumProcesso="";
					if (procSelec!=null)	
					{
						for (int i=0;  i<procClicado.length; i++) 
						{					
							n                 = Integer.parseInt(procClicado[i]);
							sNumPLaca         = procSelecPlaca[i];
							sNnumAutoInfracao = procSelecAuto[i];
							sNumProcesso      = procSelecProcesso[i];
							
							System.out.println("Auto:"+sNumPLaca+" Auto:"+sNnumAutoInfracao);
							
							
							sNumProcesso      = nProc;
							if (n==1)  
							{
								AutoPontos = new AutoInfracaoBean();
								AutoPontos.setNumPlaca(sNumPLaca);
								AutoPontos.setNumAutoInfracao(sNnumAutoInfracao);
								AutoPontos.LeAutoInfracao(UsrLogado);
								AutoPontos.setNumProcesso(nProc);
								
								AutoPontos.getCondutor().setNomResponsavel(req.getParameter("nomResp"));
								AutoPontos.getCondutor().setNumCnh(req.getParameter("numCNH"));
								AutoPontos.getCondutor().setCodUfCnh(req.getParameter("codUfCNHTR"));
								AutoPontos.getCondutor().getEndereco().setTxtEndereco(req.getParameter("txtEndereco"));
								AutoPontos.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNHPGU"));
								AutoPontos.getCondutor().setIndTipoCnh(req.getParameter("indTipoCNH"));
								if ("0".equals(AutoPontos.getCondutor().getIndTipoCnh())) AutoPontos.getCondutor().setIndTipoCnh("1");
								else  AutoPontos.getCondutor().setIndTipoCnh("2");
								AutoPontos.getCondutor().setNumCpfCnpj(req.getParameter("numCPF"));
								AutoPontos.getCondutor().getEndereco().setNumEndereco(req.getParameter("txtNumEndereco"));
								AutoPontos.getCondutor().getEndereco().setTxtComplemento(req.getParameter("txtComplemento"));
								AutoPontos.getCondutor().getEndereco().setNomBairro(req.getParameter("nomBairro"));
								AutoPontos.getCondutor().getEndereco().setNomCidade(req.getParameter("nomCidade"));
								AutoPontos.getCondutor().getEndereco().setNumCEPEdt(req.getParameter("numCEPTR"));
								AutoPontos.getCondutor().getEndereco().setCodUF(req.getParameter("codUf"));
								RequerimentoId.setDatRequerimento(req.getParameter("dataReq"));
								String decisaoJur = req.getParameter("decisaoJur");
								if(decisaoJur == null) decisaoJur = "0";
								myHist.setTxtComplemento11(decisaoJur);
								
								
								
								
								DaoBroker dao = DaoBrokerFactory.getInstance();
								dao.ajustarManual(AutoPontos,UsrLogado,RequerimentoId,myHist);
								
								
								HistoricoBean myHis = new HistoricoBean() ;
								myHis.setCodAutoInfracao(AutoInfracaoBeanId.getCodAutoInfracao());   
								myHis.setNumAutoInfracao(AutoInfracaoBeanId.getNumAutoInfracao()); 
								myHis.setNumProcesso(AutoInfracaoBeanId.getNumProcesso());
								myHis.setCodStatus(AutoInfracaoBeanId.getCodStatus());  
								myHis.setDatStatus(AutoInfracaoBeanId.getDatStatus());
								myHis.setCodEvento("410");
								myHis.setCodOrigemEvento("100");		
								myHis.setNomUserName(UsrLogado.getNomUserName());
								myHis.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
								myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
								myHis.setTxtComplemento01(nProc);
								myHis.setTxtComplemento02(dProc);
								myHis.setTxtComplemento03(" ");
								myHis.setTxtComplemento04(txtMotivo);									
								dao.atualizarAuto410(AutoInfracaoBeanId,myHis,UsrLogado);
							}
						}
					}
					if (AutoInfracaoBeanId.getMsgErro().length()==0) {					
						AutoInfracaoBeanId.setMsgErro("Ajuste Manual do Respons�vel pelos pontos realizado com sucesso.") ;
						nextRetorno	= "/REC/AjusteManualRespPontosImp.jsp" ;
					}
				}
			}
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			req.setAttribute("consultaAutoId",consultaAutoId);			
			req.setAttribute("myHist", myHist);
			req.setAttribute("RequerimentoId",RequerimentoId) ;
			req.setAttribute("UsrLogado",UsrLogado) ;
		}
		catch (Exception ue) {
			throw new sys.CommandException("AjusteManualRespPontosCmd: " + ue.getMessage());
		}		
		return nextRetorno;
	}

}