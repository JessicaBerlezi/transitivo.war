package REC;

import java.util.Vector;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Responsavel<br>
* <b>Description:</b>  Bean de Responsavel - Condutor e Proprietario <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ResponsavelBean   { 

  private String msgErro ;  

  private String PkResponsavel;
  private String codResponsavel;
  private String nomResponsavel; 
  private String indCpfCnpj; // 1 - CPF 2 - CNPJ
  private String numCpfCnpj; 
  private String numCnh;
  private String codUfCnh;   
  private String indTipoCnh; 
  private String datValidade;   
  private String categoriaCnh;   
    
  private sys.EnderecoBean endereco;
  
  public ResponsavelBean()  throws sys.BeanException {

    msgErro   = "" ;		

	PkResponsavel       = "";
	codResponsavel      = "";
	nomResponsavel      = ""; 
	indCpfCnpj          = "1"; 
	numCpfCnpj          = ""; 
	numCnh              = ""; 
	codUfCnh            = "";
	indTipoCnh          = "0";
	datValidade         = "";  
	categoriaCnh        = "";   
	endereco    = new sys.EnderecoBean() ;
	
  }

  public void setMsgErro(Vector vErro)   {
   	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
     }
   }
  public void setMsgErro(String msgErro)   {
  	this.msgErro = msgErro ;
   }   
  public String getMsgErro()   {
   	return this.msgErro;
   }     
   public void setEndereco(sys.EnderecoBean endereco)  {
   	this.endereco = endereco;
   if (endereco==null) this.endereco= new sys.EnderecoBean() ;	
   }  

   public sys.EnderecoBean getEndereco()  {
   	return this.endereco;
   }

   public void setPkResponsavel(String PkResponsavel)  {
	  this.PkResponsavel=PkResponsavel ;
	  if (PkResponsavel==null) this.PkResponsavel= "";
   }  
   public String getPkResponsavel()  {
	  return this.PkResponsavel;
   }

   
 public void setCodResponsavel(String codResponsavel)  {
    this.codResponsavel=codResponsavel ;
 	if (codResponsavel==null) this.codResponsavel= "";
 }  
 public String getCodResponsavel()  {
   	return this.codResponsavel;
 }
 public void setNomResponsavel(String nomResponsavel)  {
    this.nomResponsavel=nomResponsavel ;
 	if (nomResponsavel==null) this.nomResponsavel= "";
 }  
 public String getNomResponsavel()  {
   	return this.nomResponsavel;
 }
 public void setIndCpfCnpj(String indCpfCnpj)  {
    this.indCpfCnpj=indCpfCnpj ;
    if (this.indCpfCnpj==null) this.indCpfCnpj= "1";
    if ("12".indexOf(this.indCpfCnpj)<0) this.indCpfCnpj= "1";    
 }  
 public String getIndCpfCnpj()  {
  	return this.indCpfCnpj;
 }

  public void setNumCpfCnpj(String numCpfCnpj)  {
  	this.numCpfCnpj=numCpfCnpj ;
    if (numCpfCnpj==null) this.numCpfCnpj= "1";
  }  
  public String getNumCpfCnpj()  {
  	return this.numCpfCnpj;
  }
  public void setNumCpfCnpjEdt(String numCpfCnpj)  {
    if (numCpfCnpj==null)      numCpfCnpj = "";
    // checar o indicador de CPF ou CNPJ  1 - CPF 2 - CNPJ
    if ("1".equals(this.indCpfCnpj)) {
    	if (numCpfCnpj.length()<14) numCpfCnpj += "               ";
	    this.numCpfCnpj=numCpfCnpj.substring(0,3)+numCpfCnpj.substring(4,7)+numCpfCnpj.substring(8,11)+numCpfCnpj.substring(12,14) ;
    }
	else {
		if (numCpfCnpj.length()<18) numCpfCnpj += "                    ";
		this.numCpfCnpj=numCpfCnpj.substring(0,2)+numCpfCnpj.substring(3,6)+numCpfCnpj.substring(7,10)+numCpfCnpj.substring(11,15)+numCpfCnpj.substring(16,18) ;
	}
  }  
  public String getNumCpfCnpjEdt()  {
	if ("".equals(this.numCpfCnpj)) return this.numCpfCnpj ;
    if ("1".equals(this.indCpfCnpj)) {
    	if (this.numCpfCnpj.length()<14) this.numCpfCnpj += "               ";
	  	return this.numCpfCnpj.substring(0,3)+"."+this.numCpfCnpj.substring(3,6)+"."+this.numCpfCnpj.substring(6,9)+"-"+this.numCpfCnpj.substring(9,11) ;
    }
	else { 
		if (this.numCpfCnpj.length()<18) this.numCpfCnpj += "                    ";
		return this.numCpfCnpj.substring(0,2)+"."+this.numCpfCnpj.substring(2,5)+"."+this.numCpfCnpj.substring(5,8)+"/"+this.numCpfCnpj.substring(8,12)+"-"+this.numCpfCnpj.substring(12,14);
	}
  }
  
  public void setNumCnh(String numCnh)  {
  this.numCnh=numCnh ;
    if ( (numCnh==null) || (numCnh.length()==0) ) this.numCnh= "0";
  }  
  public String getNumCnh()  {
  	return this.numCnh;
  }
  public void setCodUfCnh(String codUfCnh)  {
  this.codUfCnh=codUfCnh ;
    if (codUfCnh==null) this.codUfCnh= "";
  }  
  public String getCodUfCnh()  {
  	return this.codUfCnh;
  }
  public void setIndTipoCnh(String indTipoCnh)  {
  this.indTipoCnh=indTipoCnh ;
    if ((indTipoCnh==null) || (indTipoCnh.length()==0))this.indTipoCnh= "0";
  }  
  public String getIndTipoCnh()  {
  	return this.indTipoCnh;
  }
  public void setDatValidade(String datValidade)  {
  this.datValidade=datValidade ;
    if (datValidade==null) this.datValidade= "";
  }  
  public String getDatValidade()  {
  	return this.datValidade;
  }
  public void setCategoriaCnh(String categoriaCnh)  {
  this.categoriaCnh=categoriaCnh ;
    if (categoriaCnh==null) this.categoriaCnh= "";
  }  
  public String getCategoriaCnh()  {
  	return this.categoriaCnh;
  }
  
//--------------------------------------------------------------------------
public void LeResponsavel()  throws sys.BeanException {
     try {   LeResponsavel("codigo") ;   
	 } // fim do try
     catch (Exception ex) {
     	throw new sys.BeanException(ex.getMessage());
     }
	return;
}
//--------------------------------------------------------------------------
public void LeResponsavel(String tpchave) throws sys.BeanException {
     try { 	 
	       DaoBroker dao = DaoBrokerFactory.getInstance();
    	   if ( dao.LeResponsavel(this, tpchave) == false) {
				setCodResponsavel("0") ;
           }	   		   
     }// fim do try
     catch (Exception ex) {
     	setCodResponsavel("0") ;
     	throw new sys.BeanException(ex.getMessage());
     }
  }
  
//--------------------------------------------------------------------------
  
}
