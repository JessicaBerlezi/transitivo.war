package REC;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Parametros do Orgaos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class MotivoBean extends sys.HtmlPopupBean {

	private String codOrgao;
	private String codMotivo;	
	private String dscMotivo;
	private String codOrgaoCp;
	private List dscMotivoOrgao;
	private List codMotivoOrgao;
	private Vector motivo; 
	private Vector motivoExcluido;
	

	

	public MotivoBean() {
		super();
		setTabela("TSMI_MOTIVO");
		setPopupWidth(35);	
		codMotivo = "";		
		dscMotivo = "";
		codOrgaoCp = "";
		dscMotivoOrgao = new ArrayList();
		codMotivoOrgao = new ArrayList();
		motivo      = new Vector();	
		motivoExcluido = new Vector();
	}

	public void setCodOrgao(String codOrgao) {
		this.codOrgao = codOrgao;
		if (codOrgao == null)
			this.codOrgao = "";
	}
	public String getCodOrgao() {
		return this.codOrgao;
	}

	public void setCodMotivo(String codMotivo) {
		this.codMotivo = codMotivo;
		if (codMotivo == null)
			this.codMotivo = "";
	}
	public String getCodMotivo() {
		return this.codMotivo;
	}

	public void setDscMotivo(String dscMotivo) {
		this.dscMotivo = dscMotivo;
		if (dscMotivo == null)
			this.dscMotivo = "";
	}
	public String getDscMotivo() {
		return this.dscMotivo;
	}

	public void setCodOrgaoCp(String codOrgaoCp) {
		this.codOrgaoCp = codOrgaoCp;
		if (codOrgaoCp == null)
			this.codOrgaoCp = "";
	}
	public String getCodOrgaoCp() {
		return this.codOrgaoCp;
	}


	public void Le_MotivoOrg() {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if (daoTabela.MotivoLeBean(this) == false) {
				this.codMotivo = "0";
				setPkid("0");
			}
		} // fim do try
		catch (Exception e) {
			this.codMotivo = "0";
			setPkid("0");
			setMsgErro(
				"Erro na leitura de Parametros do �rg�o: " + e.getMessage());
		} // fim do catch 
	}

	//--------------------------------------------------------------------------
	public void setDscMotivoOrgao(List dscMotivoOrgao) {
		this.dscMotivoOrgao = dscMotivoOrgao;
	}

	public List getDscMotivoOrgao() {
		return this.dscMotivoOrgao;
	}
	public String getDscMotivo(String myDscMotivo) throws sys.BeanException {
		try {
			String myCod = "";
			String re = "";
			Iterator itx = getDscMotivoOrgao().iterator();
			int i = 0;
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (re.equals(myDscMotivo)) {
					myCod = getCodMotivoOrgao(i);
					break;
				}
				i++;
			}
			return myCod;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}
//--------------------------------------------------------------------------
	public void setCodMotivoOrgao(List codMotivoOrgao) {
		this.codMotivoOrgao = codMotivoOrgao;
	}
	public void setCodMotivoOrgao(String myDscMotivo,String myCodMotivo) {
		Iterator itx = getDscMotivoOrgao().iterator();
		int i = 0;
		String re = "";
		while (itx.hasNext()) {
			re = (String) itx.next();
			if (re.equals(myDscMotivo)) {
				getCodMotivoOrgao().set(i,myCodMotivo);
				break;
			}
			i++;
		}
	}

	public List getCodMotivoOrgao() {
		return this.codMotivoOrgao;
	}

	public String getCodMotivoOrgao(int i) throws sys.BeanException {
		try {
			int j = 0;
			String myVal = "";
			String re = "";
			Iterator itx = getCodMotivoOrgao().iterator();
			while (itx.hasNext()) {
				re = (String) itx.next();
				if (j == i) {
					myVal = re;
					break;
				}
				j++;
			}
			return myVal;
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}

	}

	//--------------------------------------------------------------------------
	public void PreparaMotivo(ACSS.UsuarioBean UsrLog)
		throws sys.BeanException {
		try {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.PreparaMotivo(this, UsrLog);
		} // fim do try
		catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		} // fim do catch 
	}

	//--------------------------------------------------------------------------

	//Verifica Se Parametro Existe
		public void CopiaMotivoOrgao(ACSS.OrgaoBean myOrg) throws sys.BeanException {
			try {
				DaoTabelas daoTabela = DaoTabelas.getInstance();
				daoTabela.CopiaMotivo(myOrg, this);
			}
			catch (Exception e) {
				throw new sys.BeanException(
					"Erro ao efetuar verifica��o: " + e.getMessage());
			}
		}
		
//-------------------------------------------------------------------------------

  public Vector getMotivo(int min,int livre,ACSS.OrgaoBean OrgId)  {
		 try  {
  
			  REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();
			  daoTabela.GetMotivo(OrgId, this);
				  if (this.motivo.size()<min-livre) 
				  livre = min-motivo.size() ;	  	
					  for (int i=0;i<livre; i++) 	{
						  this.motivo.addElement(new REC.MotivoBean()) ;
				  }
		 }
		 catch (Exception e) {
			  setMsgErro("Orgao007 - Leitura: " + e.getMessage());
		 }//fim do catch
	  return this.motivo ;
		  }
  
	public Vector getMotivo()  {
		 return this.motivo;
	  }   
  
	public REC.MotivoBean getMotivo(int i)  { 
	  return (REC.MotivoBean)this.motivo.elementAt(i);
	}
 
	public void setMotivo(Vector motivo)  {
	   this.motivo = motivo ;
	}  
	
    //	Vetor respons�vel por guardar os dados para exclus�o 
	public void setMotivoExcluido(Vector motivoExcluido)  {
	  this.motivoExcluido = motivoExcluido ;
	}  
	public Vector getMotivoExcluido()  {
	 return this.motivoExcluido;
	} 
		
		
	public boolean isInsertMotivo(ACSS.OrgaoBean OrgId)   {
		  boolean retorno = true;
		  Vector vErro = new Vector() ;
			  // Verificada a valida��o dos dados nos campos
			  if( isValidMotivo(vErro) )   {
					  try  {
					  REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
					  if (daoTabela.InsertMotivo(this,OrgId,vErro))    {	
							  vErro.addElement("Motivo(s) do Org�o "
								   + OrgId.getSigOrgao()+" atualizado(s):"+ " \n");
						  }
						  else{		
							   vErro.addElement("Motivo(s) do Org�o "
								   +OrgId.getSigOrgao()+" n�o atualizado(s):"+ " \n");     	   
						  }
					  }
			  catch (Exception e) {
				   vErro.addElement("Motivo(s) do Org�o "+OrgId.getSigOrgao()+" n�o atualizado(s):"+ " \n") ;
				   vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
				   retorno = false;
			  }//fim do catch
		   }
		else retorno = false;   
		   this.setMsgErro(vErro);
		   return retorno;
		 }
   
	 public boolean isValidMotivo(Vector vErro) {
		if("0".equals(this.codOrgao)) 
		  vErro.addElement("Orgao n�o selecionado. \n") ;
  	
		   String sDesc = "";
		   for (int i = 0 ; i< this.motivo.size(); i++) {
			   REC.MotivoBean myMotivo = (REC.MotivoBean)this.motivo.elementAt(i) ;
			   sDesc = myMotivo.getDscMotivo();
			   if((sDesc.length()==0) ||("".equals(sDesc)))  continue ;
  		  
			   // verificar duplicata 
			   for (int m = 0 ; m <this.motivo.size(); m++) {
				   REC.MotivoBean myMotivoOutro = (REC.MotivoBean)this.motivo.elementAt(m) ;		
				   if ((m!=i) && (myMotivoOutro.getDscMotivo().trim().length()!=0) && (myMotivoOutro.getDscMotivo().equals(sDesc))) {
					   vErro.addElement("Motivo em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
				   }
			   }
		   }
		   return (vErro.size() == 0) ;  
		 }
		
	
}