package REC;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import sys.BeanException;

/**
 * <b>Title: </b> SMIT - REC - AR Digitalizado Bean <br>
 * <b>Description: </b> Aviso de Recebimento Digitalizado Bean <br>
 * <b>Copyright: </b> Copyright (c) 2004 <br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class ARDigitalizadoBean extends sys.HtmlPopupBean {
	
	private int codARDigitalizado;
	private String numNotificacao;
	private String numAutoInfracao;    
	private String numLote;
	private String numCaixa;
	private Date datDigitalizacao;
	private String datEntrega;
	private String codRetorno;
	private String codRetornoVex;
	private Vector listaArq;
    private String indFatura;
    private String indEntrega;
	
	
	public ARDigitalizadoBean() {
		
		super();
		codARDigitalizado = 0;
		numNotificacao = "";
		numAutoInfracao = "";
		numLote = "";
		numCaixa = "";
		datDigitalizacao = null;
		datEntrega        = ""     ;
		codRetorno        = ""     ;
		codRetornoVex        = ""  ;
		listaArq = new Vector();
        indFatura        = ""     ;
	    indEntrega        = ""     ;
		
		
	}
	
	public ARDigitalizadoBean(String numNotificacao, String numAutoInfracao, String numLote, String numCaixa,
			Date datDigitalizacao) {
		
		this();
		this.numNotificacao = numNotificacao;
		this.numAutoInfracao = numAutoInfracao;
		this.numLote = numLote;
		this.numCaixa = numCaixa;
		this.datDigitalizacao = datDigitalizacao;
	}
	
	public ARDigitalizadoBean(String numNotificacao, String numAutoInfracao, String numLote, String numCaixa,
			Date datDigitalizacao, String datEntrega, String codRetorno) {
		
		this();
		this.numNotificacao   = numNotificacao;
		this.numAutoInfracao  = numAutoInfracao;
		this.numLote          = numLote;
		this.numCaixa         = numCaixa;
		this.datDigitalizacao = datDigitalizacao;
		this.datEntrega       = datEntrega;
		this.codRetorno       = codRetorno;
	}
	
	
	public String getNumNotificacao() {
		if (numNotificacao == null) return "";
		else return numNotificacao;
	}
	
	public void setNumNotificacao(String string) {
		numNotificacao = string;
	}
	
	public String getNumAutoInfracao() {
		return numAutoInfracao;
	}
	public void setNumAutoInfracao(String string) {
		numAutoInfracao = string;
	}
	
	public String getNumCaixa() {
		return numCaixa;
	}
	
	public void setNumCaixa(String string) {
		numCaixa = string;
	}
	
	public String getNumLote() {
		return numLote;
	}
	
	public void setNumLote(String string) {
		numLote = string;
	}
	
	public int getCodARDigitalizado() {
		return codARDigitalizado;
	}
	
	public void setCodARDigitalizado(int i) {
		codARDigitalizado = i;
	}
	
	public Date getDatDigitalizacao() {
		return datDigitalizacao;
	}
	
	public void setDatDigitalizacao(Date datDigitalizacao) {
		this.datDigitalizacao = datDigitalizacao;
	}
	
	public String getDatEntrega() {
		return datEntrega;
	}
	
	public void setDatEntrega(String datEntrega) {
		this.datEntrega = datEntrega;
	}   
	
	
	public String getCodRetorno() {
		return codRetorno;
	}
	
	public void setCodRetorno(String codRetorno) {
		this.codRetorno = codRetorno;
	}   
	
	public String getCodRetornoVex() {
		return codRetornoVex;
	}
	
	public void setCodRetornoVex(String codRetornoVex) {
		this.codRetornoVex = codRetornoVex;
	}   

	
	public void setListaArqs(Vector listaArqs) { 
		this.listaArq = listaArqs; 
	}
	public Vector getListaArqs(){ return this.listaArq; }
	public ARDigitalizadoBean getListaArqs(int i) { 
	  return ( ARDigitalizadoBean )this.listaArq.elementAt(i); 
	}   
		
	
	public String getNumCaixaFrt() {
		boolean achou = false;
		char ch ;
		String numCaixaFrt = "";
		
		for (int h=0; h<this.numCaixa.length(); h++) {
			ch = this.numCaixa.charAt(h);
			
			if (achou == true )
				numCaixaFrt = numCaixaFrt + ch;
			if (ch == '-'){
				achou = true ;
			}
		}
		return numCaixaFrt;
	}
	
	
	public String grava() throws DaoException {
		return DaoDigit.getInstance().GravaARDigitalizado(this);
	}
	
	public String getImagem() {        
		String pathImagem = "";
		
		pathImagem =  getDataDigitalizacaoString().substring(6,10)+ 
		getDataDigitalizacaoString().substring(3,5) +
		"/" + getDataDigitalizacaoString().substring(0,2); 
		return pathImagem;
	}   
	
	public String getParametro(ACSS.ParamSistemaBean param) {        
		String parametro = "DIR_AR_DIGITALIZADO";
		/*Novo Path*/
		parametro+="_"+getDataDigitalizacaoString().substring(6,10);
		return parametro;
	} 
	
	public String getArquivo(ACSS.ParamSistemaBean param) {
		String pathArquivo;
		try {
			pathArquivo = param.getParamSist(this.getParametro(param))+ "/" +this.getImagem();
			
			if (((this.numNotificacao == null) || this.numNotificacao.equals(""))
					&& (this.numAutoInfracao.length() >= 6)) {
				SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
				pathArquivo += "/AR" +"/"+ this.numAutoInfracao + "_" + df.format(this.datDigitalizacao)+".gif";
				
			} else if (this.numNotificacao.length() >= 5)
				pathArquivo += "/AR/" + this.numNotificacao + ".gif";
			
		} catch (Exception e) {
			pathArquivo = "";
		}
		return pathArquivo;
	}    
	
	public String getDataDigitalizacaoString() {
		
		String datString = "";
		if (datDigitalizacao != null) {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			datString = df.format(this.datDigitalizacao);
		}
		return datString;
	}
	
	public static ARDigitalizadoBean consultaAR(String[] campos, String[] valores) 
	throws sys.BeanException {
		
		ARDigitalizadoBean AR = null;
		try {
			AR = DaoDigit.getInstance().ConsultaARDigitalizado(campos, valores);
			if (AR == null) {
				AR = new ARDigitalizadoBean();
				AR.setMsgErro("AVISO DE RECEBIMENTO DIGITALIZADO N�O ENCONTRADO !");
			}
		} catch (DaoException e) {
			throw new sys.BeanException(e.getMessage());
		}
		return AR;		
	}
	
	public static ARDigitalizadoBean[] consultaARs(String[] campos, String[] valores) 
	throws sys.BeanException {
		
		try {
			return DaoDigit.getInstance().ConsultaARDigitalizados(campos, valores);			     
		} 
		catch (DaoException e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	
	public String verificaImagemAR(String numNotifica) throws BeanException{
		String tipoArquivo = "";
		String numRetorno = "";
		String dscLinhaArq = "";
		try {
			dscLinhaArq = DaoDigit.getInstance().verificaDscArq(numNotifica);	
			if (!dscLinhaArq.equals("")){
				numRetorno = dscLinhaArq.substring(44,46);
				tipoArquivo = DaoDigit.getInstance().verificaNumRetorno(numRetorno);
			}
			else tipoArquivo = "";
			
		} 
		catch (DaoException e) {
			throw new sys.BeanException(e.getMessage());
		}
		return tipoArquivo;
	}
	
	public String getNomFoto(){
		
		if (((this.numNotificacao == null) || this.numNotificacao.equals(""))
				&& (this.numAutoInfracao.length() >= 6)) {
			return getImagem() + "/AR/"+this.numAutoInfracao + ".gif";
		}else if (this.numNotificacao.length() >= 5)
			return getImagem() + "/AR/"+this.numNotificacao + ".gif";
		return " ";		
	}
	
	public boolean gravaTmp() throws DaoException {
		return DaoDigit.getInstance().GravaARDigitalizadoTmp(this);
	}
	
	
	public String ConsultaCodRet(String codRet)  throws sys.BeanException {
		try { 	 
			return DaoDigit.getInstance().ConsultaCodRet(codRet);
		}// fim do try
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
	}
	
	public String getIndFatura() {
		return indFatura;
	}
	
	public void setIndFatura(String indFatura) {
		this.indFatura = indFatura;
	}   

	public String getIndEntrega() {
		return indEntrega;
	}
	
	public void setIndEntrega(String indEntrega) {
		this.indEntrega = indEntrega;
	}   
	
}