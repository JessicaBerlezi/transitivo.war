package REC;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

/**
 * <b>Title: </b> SMIT - MODULO RECURSO <br>
 * <b>Description: </b> DaoDigit - Acesso a Base de Dados de Digitaliza��o <br>
 * <b>Copyright: </b> Copyright (c) 2004 <br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class DaoDigit {
	
	private static final String MYABREVSIST = "REC";
	
	private static DaoDigit instance;
	
	private sys.ServiceLocator serviceloc;
	
	public static DaoDigit getInstance() throws DaoException {
		if (instance == null)
			instance = new DaoDigit();
		return instance;
	}
	
	private DaoDigit() throws DaoException {
		try {
			serviceloc = sys.ServiceLocator.getInstance();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public Connection getConnection() throws sys.ServiceLocatorException {
		return serviceloc.getConnection(MYABREVSIST);
	}
	
	public void setReleaseConnection(Connection conn) throws sys.ServiceLocatorException {
		serviceloc.setReleaseConnection(conn);
	}
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a AI Digitalizado
	 * -----------------------------------------------------------
	 */
	
	public String GravaAIDigitalizado(AIDigitalizadoBean bean) throws DaoException {
		
		String retorno = "";
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt = conn.createStatement();
			String sql = "";
			
			//Verifica se o registro j� existe na base
			String[] campos = { "COD_AI_DIGITALIZADO" };
			String[] valores = { String.valueOf(bean.getCodAIDigitalizado()) };
			if (ConsultaAIDigitalizados(campos, valores).length == 0) {
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_TSMI_AI_DIGITALIZADO.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				bean.setCodAIDigitalizado(rs.getInt("CODIGO"));
				rs.close();                
				
				String codCaixa = "NULL";
				if (bean.getCaixa().getCodCaixAIDigitalizado() != 0)
					codCaixa = String.valueOf(bean.getCaixa().getCodCaixAIDigitalizado());                
				
				sql = "INSERT INTO TSMI_AI_DIGITALIZADO (COD_AI_DIGITALIZADO, NUM_AUTO_INFRACAO, "
					+ "NUM_LOTE, COD_CXA_AI_DIGITALIZADO, DAT_DIGITALIZACAO) VALUES ("
					+ bean.getCodAIDigitalizado() + ", " 
					+ "'" + bean.getNumAutoInfracao() + "', "
					+ "'" + bean.getNumLote() + "', "
					+ codCaixa + ", "
					+ "TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'))";
				stmt.execute(sql);
				conn.commit();                
				retorno = "I";
				
			} else {
				sql = "UPDATE TSMI_AI_DIGITALIZADO SET "
					+ "NUM_AUTO_INFRACAO = '" + bean.getNumAutoInfracao() + "', "
					+ "NUM_LOTE = '" + bean.getNumLote() + "', "
					+ "COD_CXA_AI_DIGITALIZADO = " + bean.getCaixa().getCodCaixAIDigitalizado() + ", "
					+ "DAT_DIGITALIZACAO = TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY') "
					+ "WHERE COD_AI_DIGITALIZADO = " + bean.getCodAIDigitalizado();
				stmt.execute(sql);
				conn.commit();
				retorno = "S";
			}
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	public AIDigitalizadoBean[] ConsultaAIDigitalizados(String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = " SELECT COD_AI_DIGITALIZADO, NUM_AUTO_INFRACAO, NUM_LOTE, COD_CXA_AI_DIGITALIZADO, DAT_DIGITALIZACAO"
				+ " FROM TSMI_AI_DIGITALIZADO";
			if ((campos != null) && (valores != null))
				sql += sys.Util.montarWHERE(campos, valores);
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetAIs = new Vector();
			while (rs.next()) {
				
				//Cria o Bean de Caixa de AI Dig
				CaixAIDigitalizadoBean[] myCaixa = ConsultaCaixAIDigitalizado(conn, new String[] {"COD_CXA_AI_DIGITALIZADO"}, new String[] {rs.getString("COD_CXA_AI_DIGITALIZADO")});
				if (myCaixa.length == 0)
					myCaixa = new CaixAIDigitalizadoBean[] {new CaixAIDigitalizadoBean()};                                
				
				//Cria o Bean de AIDitalizado
				AIDigitalizadoBean myAI = new AIDigitalizadoBean();
				myAI.setCodAIDigitalizado(rs.getInt("COD_AI_DIGITALIZADO"));
				myAI.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				myAI.setNumLote(rs.getString("NUM_LOTE"));
				myAI.setDatDigitalizacao(rs.getDate("DAT_DIGITALIZACAO"));
				
				myAI.setCaixa(myCaixa[0]);                
				
				vetAIs.add(myAI);
			}
			AIDigitalizadoBean[] colAIs = new AIDigitalizadoBean[vetAIs.size()];
			vetAIs.toArray(colAIs);
			
			rs.close();
			stmt.close();
			
			return colAIs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public AIDigitalizadoBean ConsultaAIDigitalizado(String[] campos, String[] valores)
	throws DaoException {
		
		try {
			AIDigitalizadoBean[] beans = ConsultaAIDigitalizados(campos, valores);
			if (beans.length > 0)
				return beans[0];
			else
				return null;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void GravaCaixAIDigitalizado(Connection conn, CaixAIDigitalizadoBean bean) throws DaoException {
		
		try {
			Statement stmt = conn.createStatement();
			String sql = "";
			
			//Verifica se o registro j� existe na base
			if (ConsultaCaixAIDigitalizado(conn, new String[] {"COD_CXA_AI_DIGITALIZADO"},
					new String[] {String.valueOf(bean.getCodCaixAIDigitalizado())}).length == 0) {
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_TSMI_CXA_AI_DIGITALIZADO.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				bean.setCodCaixAIDigitalizado(rs.getInt("CODIGO"));
				rs.close();
				
				sql = "INSERT INTO TSMI_CXA_AI_DIGITALIZADO (COD_CXA_AI_DIGITALIZADO, NUM_CAIXA," 
					+ " NUM_PRATELEIRA, NUM_ESTANTE, NUM_BLOCO, NUM_RUA, NUM_AREA) VALUES ('"
					+ bean.getCodCaixAIDigitalizado() + "', '"                        
					+ bean.getNumCaixa() + "', '"
					+ bean.getNumPrateleira() + "', '"
					+ bean.getNumEstante() + "', '"
					+ bean.getNumBloco() + "', '"
					+ bean.getNumRua() + "', '"
					+ bean.getNumArea() + "')";
				stmt.execute(sql);
				conn.commit();
				
			} else {
				sql = "UPDATE TSMI_CXA_AI_DIGITALIZADO SET " 
					+ "NUM_CAIXA = '" + bean.getNumCaixa() + "', "
					+ "NUM_PRATELEIRA = '" + bean.getNumPrateleira() + "', "
					+ "NUM_ESTANTE = '" + bean.getNumEstante() + "', "
					+ "NUM_BLOCO = '" + bean.getNumBloco() + "', "
					+ "NUM_RUA = '" + bean.getNumRua() + "', "
					+ "NUM_AREA = '" + bean.getNumArea()
					+ "' WHERE COD_CXA_AI_DIGITALIZADO = '" + bean.getCodCaixAIDigitalizado() + "'";
				stmt.execute(sql);
				conn.commit();
			}
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void GravaCaixAIDigitalizado(CaixAIDigitalizadoBean bean) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			GravaCaixAIDigitalizado(conn, bean);
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}     
	
	public CaixAIDigitalizadoBean[] ConsultaCaixAIDigitalizado(Connection conn, String[] campos, String[] valores)
	throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			Statement stmt = conn.createStatement();
			String sql = "SELECT COD_CXA_AI_DIGITALIZADO, NUM_CAIXA, NUM_PRATELEIRA, NUM_ESTANTE,"
				+ " NUM_BLOCO, NUM_RUA, NUM_AREA FROM TSMI_CXA_AI_DIGITALIZADO";
			if ((campos != null) && (valores != null))
				sql += sys.Util.montarWHERE(campos, valores);
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetCaixas = new Vector();            
			while (rs.next()) {
				//Cria o Bean de CaixAIDitalizado
				CaixAIDigitalizadoBean caixa = new CaixAIDigitalizadoBean(); 
				caixa = new CaixAIDigitalizadoBean();
				caixa.setCodCaixAIDigitalizado(rs.getInt("COD_CXA_AI_DIGITALIZADO"));
				caixa.setNumCaixa(rs.getString("NUM_CAIXA"));
				caixa.setNumPrateleira(rs.getString("NUM_PRATELEIRA"));
				caixa.setNumEstante(rs.getString("NUM_ESTANTE"));
				caixa.setNumBloco(rs.getString("NUM_BLOCO"));
				caixa.setNumRua(rs.getString("NUM_RUA"));
				caixa.setNumArea(rs.getString("NUM_AREA"));
				vetCaixas.add(caixa);
			}
			CaixAIDigitalizadoBean[] colCaixas = new CaixAIDigitalizadoBean[vetCaixas.size()];
			vetCaixas.toArray(colCaixas);
			
			rs.close();
			stmt.close();           			 
			return colCaixas;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public CaixAIDigitalizadoBean[] ConsultaCaixAIDigitalizado(String[] campos, String[] valores)
	throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			return ConsultaCaixAIDigitalizado(conn, campos, valores);
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public void GravaLogAIDigitalizado(LogAIDigitalizadoBean bean) throws DaoException {
		
		Connection conn = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt  = conn.createStatement();
			//GravAI Log de Processamento da Linha
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String sCmd = "INSERT INTO TSMI_PROC_AI_DIGITALIZADO_LOG (COD_PROC_AI_DIGITALIZADO_LOG, DAT_PROC, "
				+ "NOM_ARQUIVO, NUM_CAIXA, NUM_LOTE, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME) "
				+ "VALUES (SEQ_TSMI_PROC_AI_DIGIT_LOG.NEXTVAL, "
				+ "TO_DATE('" + df.format(bean.getDatProcessamento()) + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "'" + bean.getNomArquivo() + "', "
				+ "'" + bean.getNumCaixa() + "', "
				+ "'" + bean.getNumLote() + "', "                
				+ "'" + (!bean.getCodOperacao().equals("R") ? "" : bean.getDscRetorno()) + "', "
				+ "'" + bean.getCodOperacao() + "', " 
				+ "'" + bean.getNomUsuario() + "')";
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public LogAIDigitalizadoBean[] ConsultaLogAIDigitalizados(Date datInicial, Date datFinal) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			sql = "SELECT DAT_PROC, NOM_ARQUIVO, NUM_CAIXA, NUM_LOTE, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME "
				+ "FROM TSMI_PROC_AI_DIGITALIZADO_LOG "
				+ "WHERE TRUNC(DAT_PROC) BETWEEN "
				+ "TRUNC(TO_DATE('" + df.format(datInicial) + "', 'DD/MM/YYYY')) AND "
				+ "TRUNC(TO_DATE('" + df.format(datFinal) + "', 'DD/MM/YYYY')) "
				+ "ORDER BY DAT_PROC";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetLogs = new Vector();
			while (rs.next()) {                                
				
				//Cria o Bean de LogAIDitalizado
				LogAIDigitalizadoBean myLog = new LogAIDigitalizadoBean();
				myLog.setDatProcessamento(rs.getTimestamp("DAT_PROC"));
				myLog.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myLog.setNumCaixa(rs.getString("NUM_CAIXA"));
				myLog.setNumLote(rs.getString("NUM_LOTE"));
				myLog.setCodOperacao(rs.getString("COD_OPERACAO"));
				myLog.setDscRetorno(rs.getString("DSC_RETORNO"));
				myLog.setNomUsuario(rs.getString("NOM_USERNAME"));
				
				vetLogs.add(myLog);
			}
			LogAIDigitalizadoBean[] colLogs = new LogAIDigitalizadoBean[vetLogs.size()];
			vetLogs.toArray(colLogs);
			
			rs.close();
			stmt.close();
			
			return colLogs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a AR Digitalizado
	 * -----------------------------------------------------------
	 */
	
	public String GravaARDigitalizado(ARDigitalizadoBean bean) throws DaoException {
		
		String retorno = "";
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			//Verifica se o registro j� existe na base
			String[] campos = { "COD_AR_DIGITALIZADO" };
			String[] valores = { String.valueOf(bean.getCodARDigitalizado()) };
			if (ConsultaARDigitalizados(campos, valores).length == 0) {
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_TSMI_AR_DIGITALIZADO.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				bean.setCodARDigitalizado(rs.getInt("CODIGO"));
				rs.close();
				
				sql = "INSERT INTO TSMI_AR_DIGITALIZADO (COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, "
					+ "NUM_AUTO_INFRACAO, NUM_LOTE, NUM_CAIXA, DAT_DIGITALIZACAO) VALUES ("
					+ bean.getCodARDigitalizado() + ", "
					+ "'" + bean.getNumNotificacao() + "', "
					+ "'" + bean.getNumAutoInfracao() + "', "
					+ "'" + bean.getNumLote() + "', " 
					+ "'" + bean.getNumCaixa() + "', "
					+ "TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'))";
				stmt.execute(sql);
				conn.commit();
				retorno = "I";
				
			} else {
				sql = "UPDATE TSMI_AR_DIGITALIZADO SET "
					+ "NUM_NOTIFICACAO = '" + bean.getNumNotificacao() + "', "
					+ "NUM_AUTO_INFRACAO = '" + bean.getNumAutoInfracao() + "', "
					+ "NUM_LOTE = '" + bean.getNumLote() + "', "
					+ "NUM_CAIXA = '" + bean.getNumCaixa() + "', "
					+ "DAT_DIGITALIZACAO = TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY') "
					+ "WHERE COD_AR_DIGITALIZADO = '" + bean.getCodARDigitalizado() + "'";
				stmt.execute(sql);
				conn.commit();
				retorno = "S";
			}
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	public ARDigitalizadoBean[] ConsultaARDigitalizados(String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "SELECT COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, NUM_AUTO_INFRACAO, NUM_LOTE, NUM_CAIXA, DAT_DIGITALIZACAO"
				+ " FROM TSMI_AR_DIGITALIZADO";
			if ((campos != null) && (valores != null))
				sql += sys.Util.montarWHERE(campos, valores);
			sql += "AND length (NUM_NOTIFICACAO) < 10 "+
		       "AND (COD_RETORNO is null " +
		       "	  OR " +
		       "	  COD_RETORNO NOT IN ('99','98')) ";	
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetARs = new Vector();
			String numNotificacao="";
			while (rs.next()) {
				//Cria o Bean de ARDigitalizado
				ARDigitalizadoBean myAR = new ARDigitalizadoBean();                                
				myAR.setCodARDigitalizado(rs.getInt("COD_AR_DIGITALIZADO"));
				numNotificacao=rs.getString("NUM_NOTIFICACAO");
				numNotificacao=sys.Util.lPad(numNotificacao,"0",9);
				myAR.setNumNotificacao(numNotificacao);
				myAR.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));                
				myAR.setNumLote(rs.getString("NUM_LOTE"));
				myAR.setNumCaixa(rs.getString("NUM_CAIXA"));
				myAR.setDatDigitalizacao(rs.getDate("DAT_DIGITALIZACAO"));
				vetARs.add(myAR);
			}            
			ARDigitalizadoBean[] colARs = new ARDigitalizadoBean[vetARs.size()];
			vetARs.toArray(colARs);
			
			rs.close();
			stmt.close();
			
			return colARs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}  finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}		
	
	public ARDigitalizadoBean ConsultaARDigitalizado(String[] campos, String[] valores)
	throws DaoException {
		
		try {
			ARDigitalizadoBean[] beans = ConsultaARDigitalizados(campos, valores);
			if (beans.length > 0)
				return beans[0];
			else
				return null;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void GravaLogARDigitalizado(LogARDigitalizadoBean bean) throws DaoException {
		
		Connection conn = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt  = conn.createStatement();
			//Gravar Log de Processamento da Linha
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String sCmd = "INSERT INTO TSMI_PROC_AR_DIGITALIZADO_LOG (COD_PROC_AR_DIGITALIZADO_LOG, DAT_PROC, "
				+ "NOM_ARQUIVO, NUM_CAIXA, NUM_LOTE, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME) "
				+ "VALUES (SEQ_TSMI_PROC_AR_DIGIT_LOG.NEXTVAL, "
				+ "TO_DATE('" + df.format(bean.getDatProcessamento()) + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "'" + bean.getNomArquivo() + "', "
				+ "'" + bean.getNumCaixa() + "', "
				+ "'" + bean.getNumLote() + "', "                
				+ "'" + (!bean.getCodOperacao().equals("R") ? "" : bean.getDscRetorno()) + "', "
				+ "'" + bean.getCodOperacao() + "', "
				+ "'" + bean.getNomUsuario() + "')";
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public LogARDigitalizadoBean[] ConsultaLogARDigitalizados(Date datInicial, Date datFinal) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			sql = "SELECT DAT_PROC, NOM_ARQUIVO, NUM_CAIXA, NUM_LOTE, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME "
				+ "FROM TSMI_PROC_AR_DIGITALIZADO_LOG "
				+ "WHERE TRUNC(DAT_PROC) BETWEEN "
				+ "TRUNC(TO_DATE('" + df.format(datInicial) + "', 'DD/MM/YYYY')) AND "
				+ "TRUNC(TO_DATE('" + df.format(datFinal) + "', 'DD/MM/YYYY')) "
				+ "ORDER BY DAT_PROC";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetLogs = new Vector();
			while (rs.next()) {                                
				
				//Cria o Bean de LogAIDitalizado
				LogARDigitalizadoBean myLog = new LogARDigitalizadoBean();
				myLog.setDatProcessamento(rs.getTimestamp("DAT_PROC"));
				myLog.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myLog.setNumCaixa(rs.getString("NUM_CAIXA"));
				myLog.setNumLote(rs.getString("NUM_LOTE"));
				myLog.setCodOperacao(rs.getString("COD_OPERACAO"));
				myLog.setDscRetorno(rs.getString("DSC_RETORNO"));
				myLog.setNomUsuario(rs.getString("NOM_USERNAME"));
				
				vetLogs.add(myLog);
			}
			LogARDigitalizadoBean[] colLogs = new LogARDigitalizadoBean[vetLogs.size()];
			vetLogs.toArray(colLogs);
			
			rs.close();
			stmt.close();
			
			return colLogs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}    
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a Foto Digitalizada
	 * -----------------------------------------------------------
	 */
	
	public String GravaFotoDigitalizada(FotoDigitalizadaBean bean) throws DaoException {
		
		String retorno = "";
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			//Verifica se o registro j� existe na base
			String[] campos = { "COD_FOTO_DIGITALIZADA" };
			String[] valores = { String.valueOf(bean.getCodFotoDigitalizada()) };
			if (ConsultaFotoDigitalizadas(campos, valores).length == 0) {
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_TSMI_FOTO_DIGITALIZADA.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				bean.setCodFotoDigitalizada(rs.getInt("CODIGO"));
				rs.close();
				
				sql = "INSERT INTO TSMI_FOTO_DIGITALIZADA (COD_FOTO_DIGITALIZADA, NUM_AUTO_INFRACAO, "
					+ "DAT_DIGITALIZACAO) VALUES ("
					+ bean.getCodFotoDigitalizada() + ", "
					+ "'" + bean.getNumAutoInfracao() + "', "
					+ "TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'))";
				stmt.execute(sql);
				conn.commit();
				retorno = "I";
				
			} else {
				sql = "UPDATE TSMI_FOTO_DIGITALIZADA SET "
					+ "NUM_AUTO_INFRACAO = '" + bean.getNumAutoInfracao() + "', "
					+ "DAT_DIGITALIZACAO = TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY') "
					+ "WHERE COD_FOTO_DIGITALIZADA = '" + bean.getCodFotoDigitalizada() + "'";
				stmt.execute(sql);
				conn.commit();
				retorno = "S";
			}
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	public FotoDigitalizadaBean[] ConsultaFotoDigitalizadas(String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "SELECT COD_FOTO_DIGITALIZADA, NUM_AUTO_INFRACAO, DAT_DIGITALIZACAO"
				+ " FROM TSMI_FOTO_DIGITALIZADA";
			if ((campos != null) && (valores != null))
				sql += sys.Util.montarWHERE(campos, valores);
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetARs = new Vector();            
			while (rs.next()) {
				//Cria o Bean de FotoDigitalizada
				FotoDigitalizadaBean myAR = new FotoDigitalizadaBean();
				myAR.setCodFotoDigitalizada(rs.getInt("COD_FOTO_DIGITALIZADA"));
				myAR.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				myAR.setDatDigitalizacao(rs.getDate("DAT_DIGITALIZACAO"));
				
				vetARs.add(myAR);
			}            
			FotoDigitalizadaBean[] colARs = new FotoDigitalizadaBean[vetARs.size()];
			vetARs.toArray(colARs);
			
			rs.close();
			stmt.close();
			
			return colARs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}  finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public FotoDigitalizadaBean ConsultaFotoDigitalizada(String[] campos, String[] valores)
	throws DaoException {
		
		try {
			FotoDigitalizadaBean[] beans = ConsultaFotoDigitalizadas(campos, valores);
			if (beans.length > 0)
				return beans[0];
			else
				return null;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public void GravaLogFotoDigitalizada(LogFotoDigitalizadaBean bean) throws DaoException {
		
		Connection conn = null;        
		try {
			conn = serviceloc.getConnection(MYABREVSIST);            
			Statement stmt  = conn.createStatement();
			//Gravar Log de Processamento da Linha
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			String sCmd = "INSERT INTO TSMI_PROC_FOT_DIGITALIZADA_LOG (COD_PROC_FOT_DIGITALIZADA_LOG, DAT_PROC, "
				+ "NOM_ARQUIVO, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME) "
				+ "VALUES (SEQ_TSMI_PROC_FOT_DIGIT_LOG.NEXTVAL, "
				+ "TO_DATE('" + df.format(bean.getDatProcessamento()) + "', 'DD/MM/YYYY HH24:MI:SS'), "
				+ "'" + bean.getNomArquivo() + "', "
				+ "'" + (!bean.getCodOperacao().equals("R") ? "" : bean.getDscRetorno()) + "', "
				+ "'" + bean.getCodOperacao() + "', "
				+ "'" + bean.getNomUsuario() + "')";
			stmt.execute(sCmd);
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					serviceloc.setReleaseConnection(conn); }
				catch (Exception e) { 
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public LogFotoDigitalizadaBean[] ConsultaLogFotoDigitalizadas(Date datInicial, Date datFinal) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sql = "";
			
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			sql = "SELECT DAT_PROC, NOM_ARQUIVO, DSC_RETORNO, COD_OPERACAO, NOM_USERNAME "
				+ "FROM TSMI_PROC_FOT_DIGITALIZADA_LOG "
				+ "WHERE TRUNC(DAT_PROC) BETWEEN "
				+ "TRUNC(TO_DATE('" + df.format(datInicial) + "', 'DD/MM/YYYY')) AND "
				+ "TRUNC(TO_DATE('" + df.format(datFinal) + "', 'DD/MM/YYYY')) "
				+ "ORDER BY DAT_PROC";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector vetLogs = new Vector();
			while (rs.next()) {                                
				
				//Cria o Bean de LogAIDitalizado
				LogFotoDigitalizadaBean myLog = new LogFotoDigitalizadaBean();
				myLog.setDatProcessamento(rs.getTimestamp("DAT_PROC"));
				myLog.setNomArquivo(rs.getString("NOM_ARQUIVO"));
				myLog.setCodOperacao(rs.getString("COD_OPERACAO"));
				myLog.setDscRetorno(rs.getString("DSC_RETORNO"));
				myLog.setNomUsuario(rs.getString("NOM_USERNAME"));
				
				vetLogs.add(myLog);
			}
			LogFotoDigitalizadaBean[] colLogs = new LogFotoDigitalizadaBean[vetLogs.size()];
			vetLogs.toArray(colLogs);
			
			rs.close();
			stmt.close();
			
			return colLogs;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}        
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a Etiquetas de Auto Digitalizado
	 * -----------------------------------------------------------
	 */
	
	public boolean ConsultaEtiquetaAutoInfracao(String codOrgao, String numLote, String numCaixa, ArrayList listAuto)
	throws DaoException {
		
		Connection conn = null;
		boolean existe = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = " SELECT COD_ETQ_AUTO_INFRACAO, NUM_AUTO_INFRACAO FROM TSMI_ETQ_AUTO_INFRACAO "
				+ " WHERE NUM_LOTE='" + numLote + "'"
				+ " AND NUM_CAIXA='" + numCaixa + "'"
				+ " AND COD_ORGAO =" + codOrgao
				+ " ORDER BY COD_ETQ_AUTO_INFRACAO";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				EtiquetaAutoBean etqBean = new EtiquetaAutoBean();
				etqBean.setCodEtqAutoInfracao(rs.getInt("COD_ETQ_AUTO_INFRACAO"));
				etqBean.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				listAuto.add(etqBean);
				existe = true;
			}
			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return existe;
	}
	
	public String VerificaEtiquetaAutoInfracao(String codOrgao, String numLote, String numCaixa, String vetAuto[])
	throws DaoException {
		String msgErro = "";
		StringBuffer erros = new StringBuffer();
		
		Connection conn = null;
		ResultSet rs    = null;
		
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "";
			for (int i = 0; i < vetAuto.length; i++) {				
				
				if(vetAuto[i] != "")
				{	
					sCmd = " SELECT /*+RULE*/ COD_ETQ_AUTO_INFRACAO, NUM_AUTO_INFRACAO FROM TSMI_ETQ_AUTO_INFRACAO "
						+ " WHERE (NUM_LOTE<>'" + numLote + "' OR NUM_CAIXA<> '" + numCaixa + "') AND NUM_AUTO_INFRACAO = '"
						+ vetAuto[i] + "' AND COD_ORGAO = " + codOrgao;				
					rs = stmt.executeQuery(sCmd);
					if (rs.next()) {
						erros.append("Auto de infra��o j� existe: "+vetAuto[i] + " em outro lote/caixa \n ");                        
					}
					
					//
					//sCmd = " SELECT /*+RULE*/ COD_ETQ_AUTO_INFRACAO, NUM_AUTO_INFRACAO FROM TSMI_ETQ_AUTO_INFRACAO "
					//	+ " WHERE (NUM_LOTE='" + numLote + "' OR NUM_CAIXA= '" + numCaixa + "') AND NUM_AUTO_INFRACAO = '"
					//	+ vetAuto[i] + "' AND COD_ORGAO = " + codOrgao;				
					//rs = stmt.executeQuery(sCmd);
					//if (rs.next()) {
					//	erros.append("Auto de infra��o j� existe "+vetAuto[i]+" no lote: "+ numLote+" e na caixa : "+numCaixa+" \n ") ;
					//	
					//}
					//
				}
			}
			stmt.close();			
			if (rs!=null)
				rs.close();
			msgErro += erros.toString();
			return msgErro;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public void AtualizaEtiquetaAutoInfracao(String codOrgao, String numLote, String numCaixa, String vetAuto[],
			String vetEtq[]) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			
			Statement stmtIns = conn.createStatement();
			for (int i = 0; i < vetEtq.length; i++) {
				
				if (!vetEtq[i].equals("0") && !vetAuto[i].trim().equals("")) {
					String cmdIns = "UPDATE TSMI_ETQ_AUTO_INFRACAO SET NUM_AUTO_INFRACAO='"
						+ vetAuto[i].trim() + "'" + " WHERE COD_ETQ_AUTO_INFRACAO=" + vetEtq[i];
					stmtIns.execute(cmdIns);
				}
				if (!vetEtq[i].equals("0") && vetAuto[i].trim().equals("")) {
					String cmdIns = "DELETE TSMI_ETQ_AUTO_INFRACAO"
						+ " WHERE COD_ETQ_AUTO_INFRACAO=" + vetEtq[i];
					stmtIns.execute(cmdIns);
				}
				if (vetEtq[i].equals("0") && !vetAuto[i].trim().equals("")) {
					String cmdIns = "INSERT INTO TSMI_ETQ_AUTO_INFRACAO (NUM_CAIXA, COD_ETQ_AUTO_INFRACAO,"
						+ " NUM_LOTE, NUM_AUTO_INFRACAO, COD_ORGAO) VALUES ('" + numCaixa
						+ "', SEQ_TSMI_ETQ_AUTO_INFRACAO.NEXTVAL, '" + numLote + "', '"
						+ vetAuto[i] + "'," + codOrgao + ")";
					stmtIns.execute(cmdIns);
				}
			}
			
			stmtIns.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}    
	
	public boolean atualizaCaixaLoteAutoInfracao(String codOrgao, String numLote, String numCaixa,
			String numCaixaBackUp, String numLoteBackUp) throws DaoException {
		boolean existe = false; 
		String cmdIns = "";  
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			Statement stmtIns = conn.createStatement();
			cmdIns = "SELECT num_lote FROM TSMI_ETQ_AUTO_INFRACAO"
				+ " WHERE num_lote = '"+ numLote + "' AND COD_ORGAO = " + codOrgao;
			ResultSet rs = stmt.executeQuery(cmdIns);
			
			while(rs.next()){
				if (!numLoteBackUp.equals(rs.getString("num_lote")))
					existe = true;
			}
			
			if (existe == false){
				cmdIns = "UPDATE TSMI_ETQ_AUTO_INFRACAO SET " +
				"num_caixa ='"+numCaixa + "'," +
				"num_lote = '"+numLote+"'" +
				" WHERE num_caixa = '" + numCaixaBackUp + "' and num_lote = '"+ numLoteBackUp+"'"+
				" and cod_orgao = "+codOrgao;
				stmtIns.execute(cmdIns);
			}
			
			stmtIns.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return existe;
	}
	
	/**
	 * M�todo responsavel por consultar dados
	 * na tabela TSMI_ETQ_AUTO_INFRACAO
	 */
	public boolean ConsultaAutoInfracao(String codOrgao, String numLote, String numCaixa,
			String numAuto, EtiquetaAutoBean bean) throws DaoException {
		
		Connection conn = null;
		boolean ok = false;
		String sCmd = "";
		String orderBy = "";
		String condicao = "WHERE COD_ORGAO =" + codOrgao;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			bean.setDados(new ArrayList());
			//Montar Order By
			if (bean.getOrdem().equals("Caixa"))  orderBy = " order by num_caixa ";
			if (bean.getOrdem().equals("Lote"))  orderBy = " order by num_lote" ;
			if (bean.getOrdem().equals("Auto"))  orderBy = " order by num_auto_infracao" ;
			
			//verifica condicao WHERE 
			if (!bean.getNumAutoInfracao().equals("")) condicao += " AND num_auto_infracao = '" + bean.getNumAutoInfracao() +"'";
			if (!bean.getNumCaixa().equals(""))condicao += " AND num_caixa = '"+ bean.getNumCaixa()+"'";
			if (!bean.getNumLote().equals("")) condicao += " AND num_lote = '"+ bean.getNumLote()+"'";
			
			sCmd = " SELECT num_auto_infracao,num_caixa,num_lote  FROM TSMI_ETQ_AUTO_INFRACAO "
				+ condicao + orderBy ;
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				EtiquetaAutoBean dados = new EtiquetaAutoBean();
				dados.setNumAutoInfracao(rs.getString("num_auto_infracao"));
				dados.setNumCaixa(rs.getString("num_caixa"));
				dados.setNumLote(rs.getString("num_lote"));
				bean.getDados().add(dados);
				ok = true;
			}
			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return ok;
	}
	
	/**
	 * M�todo responsavel por consultar dados
	 * na tabela TSMI_ETQ_AUTO_INFRACAO
	 * Trazendo os lotes de uma Caixa espec�fica
	 */
	public boolean ConsultaLoteImp(String codOrgao, String numCaixa,EtiquetaAutoBean bean) throws DaoException {
		
		Connection conn = null;
		boolean ok = false;
		String sCmd = "";
		String orderBy = "";            
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			bean.setDados(new ArrayList());
			//Montar Order By
			
			orderBy = " order by num_lote" ;            
			
			//verifica condicao WHERE               
			
			sCmd = " SELECT distinct num_caixa,num_lote  FROM TSMI_ETQ_AUTO_INFRACAO Where" +
			" num_caixa = '"+ bean.getNumCaixa()+"' and cod_orgao = " + codOrgao 
			+ orderBy;
			ResultSet rs = stmt.executeQuery(sCmd);            
			while (rs.next()) {
				EtiquetaAutoBean dados = new EtiquetaAutoBean();                    
				dados.setNumCaixa(rs.getString("num_caixa"));
				dados.setNumLote(rs.getString("num_lote"));
				bean.getDados().add(dados);
				ok = true;
			}
			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			ok = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return ok;
	}
	
	public boolean ConsultaMultiEtiquetaAutoInfracao(String montaIn, String codOrgao, String numCaixa, 
			ArrayList listAuto) throws DaoException {
		
		Connection conn = null;
		boolean existe = false;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();                			
			
			String sCmd = " SELECT COD_ETQ_AUTO_INFRACAO, NUM_AUTO_INFRACAO, NUM_LOTE FROM TSMI_ETQ_AUTO_INFRACAO "				
				+ " WHERE NUM_LOTE IN (" + montaIn + " )"
				+ " AND NUM_CAIXA='" + numCaixa + "'"
				+ " AND COD_ORGAO=" + codOrgao				
				+ " ORDER BY NUM_LOTE,COD_ETQ_AUTO_INFRACAO";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next()) {
				EtiquetaAutoBean etqBean = new EtiquetaAutoBean();
				etqBean.setNumLote(rs.getString("NUM_LOTE"));                   
				etqBean.setCodEtqAutoInfracao(rs.getInt("COD_ETQ_AUTO_INFRACAO"));                  
				etqBean.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));                  
				listAuto.add(etqBean);
				existe = true;
			}
			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return existe;
	}
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a Codigo de Retorno
	 * -----------------------------------------------------------
	 */
	
	/*----------------------------------------------------------------
	 * Fun��o Principal, utilizada pela classe CodRetornoBean, para 
	 * realizar uma inser��o, altera��o ou dele��o de um retorno
	 * ---------------------------------------------------------------
	 */
	public boolean AlterRetorno(CodigoRetornoBean myRetorno, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null; 
		Vector listRetorno = myRetorno.getRetorno() ;
		Vector listBase    = ConsultaCodigoRetorno(null, null);					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			for (int i = 0 ; i< listRetorno.size(); i++) {
				CodigoRetornoBean Retorno = (CodigoRetornoBean)listRetorno.elementAt(i) ;
				if( Retorno.getCodCodigoRetorno() != 0 ){
					if( (Retorno.getNumRetorno().compareTo("") == 0) || (Retorno.getDescRetorno().compareTo("") == 0) ){
						if( DeleteRetorno(Retorno,stmt,new String[] {"COD_CODIGO_RETORNO"}, new String[] {String.valueOf(Retorno.getCodCodigoRetorno())}) == false ){
							CodigoRetornoBean aux = (CodigoRetornoBean)listBase.elementAt(i);
							listRetorno.remove(i);
							listRetorno.add(i,aux);
							vErro.addElement("Retorno: Cod Retorno = " + aux.getNumRetorno() + " n�o deletado: \n");
							retorno = false;
						}
						else{
							listRetorno.remove(i);
							i--;
						} 
					}
					else{
						CodigoRetornoBean aux = null;
						for(int x=0; x < listBase.size(); x++){
							aux = (CodigoRetornoBean)listBase.elementAt(x);
							if(Retorno.getCodCodigoRetorno() == aux.getCodCodigoRetorno())
								break;
						}
						if( (Retorno.getNumRetorno().compareTo(aux.getNumRetorno()) != 0) || (Retorno.getDescRetorno().compareTo(aux.getDescRetorno()) != 0) || (Retorno.getIndEntregue() != aux.getIndEntregue()) ) {
							if( AlterRetorno(Retorno,stmt,new String[] {"COD_CODIGO_RETORNO"}, new String[] {String.valueOf(Retorno.getCodCodigoRetorno())}) == false ){
								listRetorno.remove(i);
								listRetorno.add(i,aux);
								vErro.addElement("Retorno: Cod Retorno = " + aux.getNumRetorno() + " n�o alterado: \n");
								retorno = false;
							}
						}
					} 
				}
				else{
					if( Retorno.getNumRetorno().compareTo("") != 0 ){
						if(InsertRetorno(Retorno,stmt) == false ){
							listRetorno.remove(i);
							i--;
							vErro.addElement("Retorno: " + Retorno.getNumRetorno() + " n�o inserido: \n");
							retorno = false;
						}
					}
				}					
			}
			// fechar a transa��o
			stmt.close();
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {   
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myRetorno.setMsgErro(vErro);
		/*ordenacaoRetorno(listRetorno);*/
		myRetorno.setRetorno(listRetorno);
		return retorno;
	}
	
	public void LeRetorno(CodigoRetornoBean myRetorno) throws DaoException {
		Connection conn = null; 
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			ResultSet rs=null;
			String sCmd="SELECT * FROM TSMI_CODIGO_RETORNO " +
			"WHERE "+
			"NUM_CODIGO_RETORNO='"+myRetorno.getNumRetorno()+"'";
			rs=stmt.executeQuery(sCmd);
			if (rs.next())
			{
				myRetorno.setCodCodigoRetorno(rs.getInt("COD_CODIGO_RETORNO"));
				myRetorno.setNumRetorno(rs.getString("NUM_CODIGO_RETORNO"));
				myRetorno.setDescRetorno(rs.getString("DSC_CODIGO_RETORNO"));
				if ((rs.getString("IND_ENTREGUE").compareTo("N")) == 0)
					myRetorno.setIndEntregue(false);
				else
					myRetorno.setIndEntregue(true);
				myRetorno.setIndCSS(rs.getString("IND_CSS"));
			}
			// fechar a transa��o
			rs.close();
			stmt.close();
		}
		catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {   
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return;
	}
	
	
	
	
	/*--------------------------------------------------
	 * Fun��o para deletar um retorno na base de dados
	 * -------------------------------------------------
	 */
	public boolean DeleteRetorno(CodigoRetornoBean myRetorno, Statement stmt, String[] campos,
			String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			String sCmd = "DELETE TSMI_CODIGO_RETORNO ";
			if ((campos != null) && (valores != null))
				sCmd += sys.Util.montarWHERE(campos, valores);
			
			stmt.execute(sCmd);
			TAB.DaoBroker broker = new TAB.DaoBroker();
			String result = broker.Transacao091(valores[0],"3","  "," "," ");
			if (!result.substring(0,3).equals(("000")))
				throw new DaoException("Erro na Atualiza��o da Base de Dados: " + result);  
		} catch (Exception ex) {
			return false;
		}
		return true;
	}
	
	/*--------------------------------------------------
	 * Fun��o para alterar um retorno na base de dados
	 * -------------------------------------------------
	 */
	public boolean AlterRetorno(CodigoRetornoBean myRetorno, Statement stmt, String[] campos,
			String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			String sCmd = "UPDATE TSMI_CODIGO_RETORNO SET ";
			sCmd += "NUM_CODIGO_RETORNO = '" + myRetorno.getNumRetorno() + "' , ";
			sCmd += "DSC_CODIGO_RETORNO = '" + myRetorno.getDescRetorno() + "' , ";
			if (myRetorno.getIndEntregue() == true)
				sCmd += "IND_ENTREGUE = 'S',IND_CSS='"+myRetorno.getIndCSS()+"' ";
			else
				sCmd += "IND_ENTREGUE = 'N',IND_CSS='"+myRetorno.getIndCSS()+"' ";
			
			
			if ((campos != null) && (valores != null))
				sCmd += sys.Util.montarWHERE(campos, valores);
			
			stmt.execute(sCmd);            
			TAB.DaoBroker broker = new TAB.DaoBroker();
			String result = broker.Transacao091(myRetorno.getNumRetorno(),"2","  ",myRetorno.getDescRetorno(),myRetorno.getIndEntregue()== true ? "S":"N");
			if (result.substring(0,3).equals(("017"))) {
				result = broker.Transacao091(myRetorno.getNumRetorno(),"1","  ",myRetorno.getDescRetorno(),myRetorno.getIndEntregue()== true ? "S":"N");
			}
			if (!result.substring(0,3).equals(("000")))
				throw new DaoException("Erro na Atualiza��o da Base de Dados: " + result);  
		} catch (Exception ex) {
			System.err.println(ex);
			return false;
		}
		return true;
	}
	
	/*--------------------------------------------------
	 * Fun��o para inserir um retorno na base de dados
	 * -------------------------------------------------
	 */
	public boolean InsertRetorno(CodigoRetornoBean myRetorno, Statement stmt) throws DaoException {
		Vector lista;
		CodigoRetornoBean auxiliar;
		try {
			String sCmd = "INSERT INTO TSMI_CODIGO_RETORNO (COD_CODIGO_RETORNO, NUM_CODIGO_RETORNO";
			sCmd += ", DSC_CODIGO_RETORNO, IND_ENTREGUE, IND_CSS) VALUES (SEQ_TSMI_CODIGO_RETORNO.NEXTVAL, ";
			sCmd += "'" + myRetorno.getNumRetorno() + "', ";
			sCmd += "'" + myRetorno.getDescRetorno() + "',";
			if (myRetorno.getIndEntregue() == true)
				sCmd += "'S','"+myRetorno.getIndCSS()+"') ";
			else
				sCmd += "'N','"+myRetorno.getIndCSS()+"') ";
			stmt.execute(sCmd);
			
			TAB.DaoBroker broker = new TAB.DaoBroker();
			String result = broker.Transacao091(myRetorno.getNumRetorno(),"1","  ",myRetorno.getDescRetorno(),myRetorno.getIndEntregue()== true ? "S":"N");
			if (result.substring(0,3).equals(("016"))) {
				result = broker.Transacao091(myRetorno.getNumRetorno(),"2","  ",myRetorno.getDescRetorno(),myRetorno.getIndEntregue()== true ? "S":"N");
			}
			
			if (!result.substring(0,3).equals(("000")))
				throw new DaoException("Erro na Atualiza��o da Base de Dados: " + result);  
			
			lista = ConsultaCodigoRetorno(new String[] {"NUM_CODIGO_RETORNO"},
					new String[] { myRetorno.getNumRetorno()});
			auxiliar = (CodigoRetornoBean) lista.get(0);
			myRetorno.setCodCodigoRetorno(auxiliar.getCodCodigoRetorno());
		} catch (Exception ex) {
			System.err.println(ex);
			return false;
		}
		return true;
	}
	
	
	
	/*-----------------------------------------------------
	 * Fun��o para consultar um retorno na base de dados
	 * ----------------------------------------------------
	 */
	public Vector ConsultaCodigoRetorno(Connection conn, String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT COD_CODIGO_RETORNO, NUM_CODIGO_RETORNO, DSC_CODIGO_RETORNO, IND_ENTREGUE,IND_CSS FROM TSMI_CODIGO_RETORNO";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			sql += " ORDER BY NUM_CODIGO_RETORNO ASC ";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector retornos = new Vector();
			while (rs.next()) {
				CodigoRetornoBean myCodigoRetorno = new CodigoRetornoBean();
				myCodigoRetorno.setCodCodigoRetorno(rs.getInt("COD_CODIGO_RETORNO"));
				myCodigoRetorno.setNumRetorno(rs.getString("NUM_CODIGO_RETORNO"));
				myCodigoRetorno.setDescRetorno(rs.getString("DSC_CODIGO_RETORNO"));
				if ((rs.getString("IND_ENTREGUE").compareTo("N")) == 0)
					myCodigoRetorno.setIndEntregue(false);
				else
					myCodigoRetorno.setIndEntregue(true);
				myCodigoRetorno.setIndCSS(rs.getString("IND_CSS"));
				
				retornos.addElement(myCodigoRetorno);
			}
			
			rs.close();
			stmt.close();
			return retornos;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public Vector ConsultaCodigoRetorno(String[] campos, String[] valores) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			return ConsultaCodigoRetorno(conn, campos, valores);
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}    
	
	/**
	 * --------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista de retornos recebidos A ordena��o �
	 * feita pelo campo NUM_CODIGO_RETORNO
	 * ----------------------------------------------------------------------
	 */
	private void ordenacaoRetorno(Vector lista){
		for(int i = 0; i < (lista.size() - 1); i++){
			for(int x = (i + 1); x < lista.size(); x++){
				CodigoRetornoBean segundo = (CodigoRetornoBean)lista.elementAt(x);
				if( segundo.getCodCodigoRetorno() == 0 ){
					break;
				}
				if( (Integer.parseInt(((CodigoRetornoBean)lista.elementAt(i)).getNumRetorno())) > (Integer.parseInt(segundo.getNumRetorno())) ){
					lista.set(x,(CodigoRetornoBean)lista.elementAt(i));
					lista.set(i,segundo);
				}
			}
		}
	}
	
	public String BuscaARDigitalizado(String numNotificacao) throws DaoException {
		
		String codNotificacao = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			String sCmd = "SELECT NUM_NOTIFICACAO FROM TSMI_AR_DIGITALIZADO "
				+ " WHERE NUM_NOTIFICACAO = '" + numNotificacao + "'";            
			ResultSet rs = stmt.executeQuery(sCmd);
			while (rs.next())
				codNotificacao = rs.getString("NUM_NOTIFICACAO");
			
			rs.close();
			stmt.close();                
			return codNotificacao;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	
	/**
	 * ----------------------------------------------------------- 
	 * DAO relativos a Tabela de Convers�o de Alfa
	 * -----------------------------------------------------------
	 */    
	
	/**
	 * M�todo para buscar o n�mero do auto de infra��o de um AR digitalizado.
	 * O n�mero do auto ser� procurado nas linhas dos arquivos EMITEVEX recebidos.
	 * @author Glaucio Jannotti
	 * @param numNotificacao N�mero da notifica��o
	 * @return Retorna o n�mero do auto de infra��o localizado. Se n�o localizar, retorna "".
	 */
	public String BuscaNumAutoInfracao(String numNotificacao) throws DaoException {
		
		String numAuto = "";
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			String sCmd = "SELECT NUM_AUTO_INFRACAO FROM TSMI_CONTROLE_VEX_MASTER_V1 "+
				          " WHERE NUM_NOTIFICACAO = '"+numNotificacao+"'";            
			ResultSet rs = stmt.executeQuery(sCmd);
			if (rs.next())
				numAuto = rs.getString("NUM_AUTO_INFRACAO");
			rs.close();
			stmt.close();                
			return numAuto;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public String verificaDscArq(String numNotifica) throws DaoException {
		String dscLinha = "";
		Connection conn = null;
		String sCmd = ""; 
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd += "	select dsc_linha_arq_rec from tsmi_linha_arquivo_rec t";
			sCmd += " where t.cod_arquivo in ";
			sCmd += " (select cod_arquivo from tsmi_arquivo_recebido t ";
			sCmd += " where t.cod_ident_arquivo = 'CODRET')and num_notificacao ='"+numNotifica+"'";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next())
				dscLinha = rs.getString("dsc_linha_arq_rec");
			
			rs.close();
			stmt.close();                
			return dscLinha;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	public String verificaNumRetorno(String numRetorno) throws DaoException {
		String indEntrega = "";
		Connection conn = null;
		String sCmd = ""; 
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			
			sCmd += "	select ind_entregue from tsmi_codigo_retorno ";
			sCmd += " where num_codigo_retorno = '"+ numRetorno +"'";
			
			ResultSet rs = stmt.executeQuery(sCmd);
			
			while (rs.next())
				indEntrega = rs.getString("ind_entregue");
			
			rs.close();
			stmt.close();                
			return indEntrega;
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}
	
	
	/*----------------------------------------------------------------
	 * Fun��o Principal, utilizada pela classe MotivoCancelBean, para 
	 * realizar uma inser��o, altera��o ou dele��o de um retorno
	 * ---------------------------------------------------------------
	 */
	public boolean AlterMotivoCancel(MotivoCancelBean myMotivo, Vector vErro) throws DaoException {
		boolean retorno = true;
		Connection conn = null; 
		Vector listMotivoCancel = myMotivo.getMotivoCancel();
		Vector listBase = ConsultaMotivoCancel(null, null);					
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			Statement stmt  = conn.createStatement() ;
			
			// iniciar transa��o
			for (int i = 0 ; i< listMotivoCancel.size(); i++) {
				MotivoCancelBean motivoCancel = (MotivoCancelBean)listMotivoCancel.elementAt(i) ;
				if( motivoCancel.getCodMotivoCancel() != 0 ){
					if( (motivoCancel.getNumMotivoCancel().compareTo("") == 0) || (motivoCancel.getDescMotivoCancel().compareTo("") == 0) ){
						if( DeleteMotivoCancel(motivoCancel,stmt,new String[] {"COD_MOTIVO_CANCELAMENTO"}, new String[] {String.valueOf(motivoCancel.getCodMotivoCancel())}) == false ){
							MotivoCancelBean aux = (MotivoCancelBean)listBase.elementAt(i);
							listMotivoCancel.remove(i);
							listMotivoCancel.add(i,aux);
							vErro.addElement("Motivo Cancelamento: Cod Motivo = " + aux.getNumMotivoCancel() + " n�o deletado: \n");
							retorno = false;
						}
						else{
							listMotivoCancel.remove(i);
							i--;
						} 
					}
					else{
						MotivoCancelBean aux = null;
						for(int x=0; x < listBase.size(); x++){
							aux = (MotivoCancelBean)listBase.elementAt(x);
							if(motivoCancel.getCodMotivoCancel() == aux.getCodMotivoCancel())
								break;
						}
						if( (motivoCancel.getNumMotivoCancel().compareTo(aux.getNumMotivoCancel()) != 0) || (motivoCancel.getDescMotivoCancel().compareTo(aux.getDescMotivoCancel()) != 0)) {
							if( AlterMotivoCancel(motivoCancel,stmt,new String[] {"COD_MOTIVO_CANCELAMENTO"}, new String[] {String.valueOf(motivoCancel.getCodMotivoCancel())}) == false ){
								listMotivoCancel.remove(i);
								listMotivoCancel.add(i,aux);
								vErro.addElement("Motivo Cancelamento: Cod Motivo = " + aux.getNumMotivoCancel() + " n�o alterado: \n");
								retorno = false;
							}
						}
					} 
				}
				else{
					if( motivoCancel.getNumMotivoCancel().compareTo("") != 0 ){
						if(InsertMotivoCancel(motivoCancel,stmt) == false ){
							listMotivoCancel.remove(i);
							i--;
							vErro.addElement("Retorno: " + motivoCancel.getNumMotivoCancel() + " n�o inserido: \n");
							retorno = false;
						}
					}
				}					
			}
			// fechar a transa��o
			stmt.close();
		}
		catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		}
		finally {
			if (conn != null) {
				try {   
					conn.rollback();
					serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		myMotivo.setMsgErro(vErro);
		ordenacaoMotivoCancel(listMotivoCancel);
		myMotivo.setMotivoCancel(listMotivoCancel);
		return retorno;
	}
	
	/*--------------------------------------------------
	 * Fun��o para deletar um MOTIVO na base de dados
	 * -------------------------------------------------
	 */
	public boolean DeleteMotivoCancel(MotivoCancelBean myMotivo, Statement stmt, String[] campos,
			String[] valores) throws DaoException {
		boolean bOk = true;
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			String sCmd = "DELETE TSMI_MOTIVO_CANCELAMENTO ";
			if ((campos != null) && (valores != null))
				sCmd += sys.Util.montarWHERE(campos, valores);
			
			stmt.execute(sCmd);
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException("Erro na Atualiza��o da Base de Dados: " + ex.getMessage());
		}
		return bOk;
	}
	
	/*--------------------------------------------------
	 * Fun��o para alterar um MOTIVO na base de dados
	 * -------------------------------------------------
	 */
	public boolean AlterMotivoCancel(MotivoCancelBean myMotivo, Statement stmt, String[] campos,
			String[] valores) throws DaoException {
		boolean bOk = true;
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			String sCmd = "UPDATE TSMI_MOTIVO_CANCELAMENTO SET ";
			sCmd += "NUM_MOTIVO_CANCELAMENTO = '" + myMotivo.getNumMotivoCancel().trim() + "' , ";
			sCmd += "DSC_MOTIVO_CANCELAMENTO = '" + myMotivo.getDescMotivoCancel().trim() + "'  ";
			if ((campos != null) && (valores != null))
				sCmd += sys.Util.montarWHERE(campos, valores);
			stmt.execute(sCmd);            
			
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException("Erro na Altera��o da Base de Dados: " + ex.getMessage());
		}
		return bOk;
	}
	
	/*--------------------------------------------------
	 * Fun��o para inserir um MOTIVO na base de dados
	 * -------------------------------------------------
	 */
	public boolean InsertMotivoCancel(MotivoCancelBean myMotivo, Statement stmt) throws DaoException {
		Vector lista;
		boolean bOk = true;
		MotivoCancelBean auxiliar;
		try {
			String sCmd = "INSERT INTO TSMI_MOTIVO_CANCELAMENTO (COD_MOTIVO_CANCELAMENTO, NUM_MOTIVO_CANCELAMENTO";
			sCmd += ", DSC_MOTIVO_CANCELAMENTO) VALUES (SEQ_TSMI_MOTIVO_CANCELAMENTO.NEXTVAL, ";
			sCmd += "'" + myMotivo.getNumMotivoCancel().trim() + "', ";
			sCmd += "'" + myMotivo.getDescMotivoCancel().trim() + "')";
			
			stmt.execute(sCmd);
			lista = ConsultaMotivoCancel(new String[] {"NUM_MOTIVO_CANCELAMENTO"},
					new String[] { myMotivo.getNumMotivoCancel().trim()});
			auxiliar = (MotivoCancelBean) lista.get(0);
			myMotivo.setCodMotivoCancel(auxiliar.getCodMotivoCancel());
		} catch (Exception ex) {
			bOk = false;
			throw new DaoException("Erro na inclus�o da Base de Dados: " + ex.getMessage());
		}
		return bOk;
	}
	
	/*-----------------------------------------------------
	 * Fun��o para consultar um MOTIVO na base de dados
	 * ----------------------------------------------------
	 */
	public Vector ConsultaMotivoCancel(Connection conn, String[] campos, String[] valores) throws DaoException {
		
		if ((campos != null) && (valores != null) && (campos.length != valores.length))
			throw new DaoException("A quantidade de campos difere da quantidade de valores");
		
		try {
			Statement stmt = conn.createStatement();
			
			String sql = "SELECT COD_MOTIVO_CANCELAMENTO, NUM_MOTIVO_CANCELAMENTO, DSC_MOTIVO_CANCELAMENTO FROM TSMI_MOTIVO_CANCELAMENTO";
			if ((campos != null) && (valores != null))            
				sql += sys.Util.montarWHERE(campos, valores);
			sql += " ORDER BY NUM_MOTIVO_CANCELAMENTO ASC ";
			
			ResultSet rs = stmt.executeQuery(sql);
			
			Vector motivoCancel = new Vector();
			while (rs.next()) {
				MotivoCancelBean myMotivo = new MotivoCancelBean();
				myMotivo.setCodMotivoCancel(rs.getInt("COD_MOTIVO_CANCELAMENTO"));
				myMotivo.setNumMotivoCancel(rs.getString("NUM_MOTIVO_CANCELAMENTO").trim());
				myMotivo.setDescMotivoCancel(rs.getString("DSC_MOTIVO_CANCELAMENTO").trim());
				motivoCancel.addElement(myMotivo);
			}
			
			rs.close();
			stmt.close();
			return motivoCancel;
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
	}
	
	public Vector ConsultaMotivoCancel(String[] campos, String[] valores) throws DaoException {
		
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			return ConsultaMotivoCancel(conn, campos, valores);
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
	}    
	
	/**
	 * --------------------------------------------------------------------
	 * M�todo de ordena��o de uma lista de retornos recebidos A ordena��o �
	 * feita pelo campo NUM_MOTIVO_CANCELAMENTO
	 * ----------------------------------------------------------------------
	 */
	private void ordenacaoMotivoCancel(Vector lista){
		for(int i = 0; i < (lista.size() - 1); i++){
			for(int x = (i + 1); x < lista.size(); x++){
				MotivoCancelBean segundo = (MotivoCancelBean)lista.elementAt(x);
				if( segundo.getCodMotivoCancel() == 0 ){
					break;
				}
				if( (Integer.parseInt(((MotivoCancelBean)lista.elementAt(i)).getNumMotivoCancel())) > (Integer.parseInt(segundo.getNumMotivoCancel())) ){
					lista.set(x,(MotivoCancelBean)lista.elementAt(i));
					lista.set(i,segundo);
				}
			}
		}
	}	
	
	public boolean GravaARDigitalizadoTmp(ARDigitalizadoBean bean) throws DaoException {
		
		boolean retorno = true;
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			
			//Pega o valor da sequence para o campo CodAIDigitalizado
			sql = "SELECT SEQ_TSMI_AR_DIGITALIZADO_TMP.NEXTVAL AS CODIGO FROM DUAL";
			ResultSet rs = stmt.executeQuery(sql);
			rs.next();
			bean.setCodARDigitalizado(rs.getInt("CODIGO"));
			rs.close();
			
			sql = "INSERT INTO TSMI_AR_DIGITALIZADO_TMP (COD_AR_DIGITALIZADO, NUM_NOTIFICACAO, "
				+ "NUM_AUTO_INFRACAO, NUM_LOTE, NUM_CAIXA, DAT_DIGITALIZACAO," 
				+ "DAT_ENTREGA,DAT_PROC, "
				+ "NUM_CODIGO_RETORNO "
				+ ") VALUES ("
				+ bean.getCodARDigitalizado() + ", "
				+ "'" + bean.getNumNotificacao() + "', "
				+ "'" + bean.getNumAutoInfracao() + "', "
				+ "'" + bean.getNumLote() + "', " 
				+ "'" + bean.getNumCaixaFrt() + "', "
				+ "TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'),"
				+ "TO_DATE('" + bean.getDatEntrega() + "', 'DD/MM/YYYY'),SYSDATE,"
				+ "'" + bean.getCodRetorno() + "') ";
			stmt.execute(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			retorno = false;
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	public String ConsultaCodRet(String codRet) throws DaoException {
		Connection conn =null ;
		String sCmd = "";
		String codRetDETRAN = codRet;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);	
			Statement stmt = conn.createStatement();
			sCmd =  "SELECT NUM_CODIGO_RETORNO " +
			"FROM TSMI_CODIGO_RETORNO " +
			"WHERE NUM_CODIGO_RETORNO_VEX = '"+codRet+"' " ;
			
			ResultSet rs    = stmt.executeQuery(sCmd) ;			
			while (rs.next()) {
				codRetDETRAN = rs.getString("NUM_CODIGO_RETORNO");
			}	
			if (rs != null) rs.close();
			stmt.close();
		}
		catch (Exception ex) {
			throw new DaoException("DaoBrokerAdabas-ConsultaCodRet: "+ex.getMessage()+" - "+sCmd);
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return codRetDETRAN;
	}	

	public String GravaFotoDigitalizadaDnf(FotoDanificadaBean bean) throws DaoException {
		
		String retorno = "";
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
				
				//Pega o valor da sequence para o campo CodAIDigitalizado
				sql = "SELECT SEQ_TSMI_FOTO_DANIFICADA.NEXTVAL AS CODIGO FROM DUAL";
				ResultSet rs = stmt.executeQuery(sql);
				rs.next();
				bean.setCodFotoDigitalizada(rs.getInt("CODIGO"));
				rs.close();
				
				sql = "INSERT INTO TSMI_FOTO_DANIFICADA (COD_FOTO_DANIFICADA, NUM_AUTO_INFRACAO, "
					+ "DAT_DIGITALIZACAO) VALUES ("
					+ bean.getCodFotoDigitalizada() + ", "
					+ "'" + bean.getNumAutoInfracao() + "', "
					+ "TO_DATE('" + df.format(bean.getDatDigitalizacao()) + "', 'DD/MM/YYYY'))";
				stmt.execute(sql);
				conn.commit();
				retorno = "I";
				
		   stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}
		return retorno;
	}
	
	
	
}