package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Retornos<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Sergio Roberto Jr.	
* @version 1.0
*/

public class CodigoRetornoCmd extends sys.Command {


  private static final String jspPadrao="/REC/CodRetorno.jsp";    
  private String next;


  public CodigoRetornoCmd() {
    next = jspPadrao;
  }

  public CodigoRetornoCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// Cria a Bean CodRetorno para realizar a consulta aos retornos	
		CodigoRetornoBean CodRetornoId = new CodigoRetornoBean();
								  
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)  acao ="";   
		  
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente == null || atualizarDependente.equals(""))
		  atualizarDependente ="N";  
		CodRetornoId.setAtualizarDependente(atualizarDependente);
		
	    if(acao.compareTo("")==0) {
		  CodRetornoId.getRetorno(20,5);
		  CodRetornoId.setAtualizarDependente("S");		  	
	    }
		 
	    if(acao.compareTo("Atualizar") == 0){
			 
			String[] codCodigoRetorno= req.getParameterValues("codCodigoRetorno");
			if(codCodigoRetorno == null) 
				codCodigoRetorno = new String[0];
			  
			String[] numRetorno= req.getParameterValues("numRetorno");
			if(numRetorno == null) 
				numRetorno = new String[0];
							  
			String[] descRetorno = req.getParameterValues("descRetorno");
			if(descRetorno == null)  
				descRetorno = new String[0];         

			String[] indEntregue = req.getParameterValues("indEntregue");
			if(indEntregue == null)
				indEntregue = new String[0];          
						 
			Vector retorno = new Vector(); 
			for (int i = 0; i < codCodigoRetorno.length;i++) {
				CodigoRetornoBean Alter = new CodigoRetornoBean() ;		
				Alter.setCodCodigoRetorno(Integer.parseInt(codCodigoRetorno[i]));
				Alter.setNumRetorno(numRetorno[i]);
				Alter.setDescRetorno(descRetorno[i]);	  
				if( indEntregue[i].compareTo("false") == 0 )
					Alter.setIndEntregue(false);
				else
					Alter.setIndEntregue(true);												
				retorno.addElement(Alter) ; 					
			}	
			
			CodRetornoId.setRetorno(retorno);
			CodRetornoId.isAlterRetorno();
		    if (CodRetornoId.getMsgErro().length()==0)
			      CodRetornoId.setMsgErro("C�digo Retorno Atualizado com Sucesso.");
	    }
	    
	    if(acao.compareTo("I") == 0){
			req.setAttribute("vRetorno",CodRetornoId.getRetorno(0,0));
			nextRetorno = "/REC/CodRetornoImp.jsp";
	    }
	
		req.setAttribute("CodRetornoID", CodRetornoId);		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("RetornoCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }  


}