package REC;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Relator<br>
* <b>Copyright:</b>    Copyright (c) 2006<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Wellem Mello
* @version 1.0
*/

public class UsuarioRelatorCmd extends sys.Command {


  private static final String jspPadrao="/REC/UsuarioRelator.jsp";    
  private String next;

  public UsuarioRelatorCmd() {
    next = jspPadrao;
  }

  public UsuarioRelatorCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = next ;
	HttpSession session   = req.getSession() ;
    try {       	
//		 cria os Beans do org�o, se n�o existir
		ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null)
		  OrgaoId = new ACSS.OrgaoBean() ;	 
		// cria os Beans da Junta, se n�o existir
		JuntaBean JuntaId = (JuntaBean)req.getAttribute("JuntaId") ;
		if(JuntaId == null)
		  JuntaId = new JuntaBean() ;	 
		
		ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;
		
		RelatorBean RelatorId = (RelatorBean)req.getAttribute("RelatorId") ;
		if(RelatorId == null)
		  RelatorId = new RelatorBean() ;	
							
		
		NivelUsuarioRelatorBean NivUsrId = (NivelUsuarioRelatorBean)req.getAttribute("NivUsrId") ;
		if(NivUsrId == null)
		   NivUsrId = new NivelUsuarioRelatorBean() ;	
		
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)  acao =" ";   

		String atualizarDependente = req.getParameter("atualizarDependente");
		if (atualizarDependente == null)
			atualizarDependente = "N";
		
		String codNivelUsuario = req.getParameter("codNivelUsuario");
		if (codNivelUsuario == null) codNivelUsuario = "0";

	    String codOrgao = UsrLogado.getCodOrgaoAtuacao();	 
		if(codOrgao == null) codOrgao =""; 
		
	    String codJunta = req.getParameter("codJunta"); 		
		if(codJunta == null)  codJunta ="0";		
		
		String codRelator = req.getParameter("codRelator"); 		
		if(codRelator == null)  codRelator ="0";	
		
		String nomRelator = req.getParameter("nomRelator"); 		
		if(nomRelator == null)  nomRelator ="0";			
		
       // Carrega bean
		OrgaoId.Le_Orgao(codOrgao,0) ;
		JuntaId.Le_Junta(codJunta,0) ;
		RelatorId.setCodOrgao(codOrgao);
		RelatorId.setCodJunta(codJunta);
		RelatorId.setAtualizarDependente(atualizarDependente);
		NivUsrId.Le_Niveis(codNivelUsuario,0,UsrLogado);
		NivUsrId.setCodRelator(codRelator);
		RelatorId.setNomRelator(nomRelator);
	    RelatorId.setCodRelator(codRelator);
	    
		//Verifica que acao devera ser utilizada
        if(acao.compareTo("buscaRelator")==0) 
        {	    	
	    	RelatorId.getRelatores();  
	    	
        }
        if(acao.compareTo("buscaNivel")==0) 
        {
    	    RelatorId.setCodRelator(codRelator);
    	    RelatorId.setNomRelator(nomRelator);
	    	NivUsrId.getNivelUsuarioRelator(20, 6,JuntaId,RelatorId);
	    	NivUsrId.Le_NivelUsuario(UsrLogado);
			List Usuarios = RelatorId.getUsuarios(NivUsrId,UsrLogado);			
			RelatorId.setAtualizarDependente("S");
			req.setAttribute("usuarios",Usuarios);
	    }
		else if (acao.compareTo("A") == 0) 
		{
			String[] usuariosPorNivel = req.getParameterValues("usuariosPorNivel");
			if (usuariosPorNivel == null) usuariosPorNivel = new String[0];
			
			Vector nivelUsuario = new Vector();
			for (int i = 0; i < usuariosPorNivel.length; i++) {
			    NivelUsuarioRelatorBean myNivelUsuario  = new NivelUsuarioRelatorBean();
			    myNivelUsuario.setCodNivelUsuario(codNivelUsuario);
			    myNivelUsuario.setCodUsuario(usuariosPorNivel[i]);
			    myNivelUsuario.setCodRelator(codRelator);
			    nivelUsuario.addElement(myNivelUsuario);
			}
			NivUsrId.setNivelUsuario(nivelUsuario);
			NivUsrId.isInsertNivelUsuario();
			NivUsrId.Le_NivelUsuario(UsrLogado);
			List Usuarios = RelatorId.getUsuarios(NivUsrId,UsrLogado);
			req.setAttribute("usuarios",Usuarios);			
		}
		else if (acao.equals("I"))
		{
			NivUsrId.Le_NivelUsuario(UsrLogado);
			nextRetorno = "/REC/UsuarioRelatorImp.jsp";
			String tituloConsulta = "CONTROLE DE RELATORES";
			req.setAttribute("tituloConsulta",tituloConsulta);
		}
		else if (acao.equals("voltar")) {
			RelatorId.setAtualizarDependente("N");
			JuntaId.setCodJunta("0");
		}
	
		req.setAttribute("RelatorId", RelatorId);
		req.setAttribute("JuntaId", JuntaId);
		req.setAttribute("NivUsrId", NivUsrId);

    }
    catch (Exception ue) {
      throw new sys.CommandException("UsuarioRelatorCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }
  
 
}