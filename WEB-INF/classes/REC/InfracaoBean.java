package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
/**
 * <b>Title:</b> SMIT - Entrada de Recurso - Infracao Bean<br>
 * <b>Description:</b> Infracao Bean <br>
 * <b>Copyright:</b> Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * 
 * @author Sergio Monteiro
 * @version 1.0
 */

public class InfracaoBean {

    private String msgErro;

    private String codInfracao;

    private String codAutoInfracao;

    private String dscInfracao;

    private String dscEnquadramento;

    private String numPonto;

    private String dscCompetencia;

    private String dscGravidade;

    private String valRealDesconto;

    private String valReal;

    private String indTipo;

    private String indSDD;

    private List infracoes;

    private String infs;

    public InfracaoBean() throws sys.BeanException {

        msgErro = "";
        infracoes = new ArrayList();
        codInfracao = "";
        dscGravidade = "";
        valRealDesconto = "";
        valReal = "";
        codAutoInfracao = "";
        dscInfracao = "";
        dscEnquadramento = "";
        numPonto = "";
        dscCompetencia = "M"; // E-Estadual M-Municipal
        indTipo = "C"; // P-Proprietario C-Condutor
        indSDD = "N"; // S-Sim N-Nao
        infs = "";

    }

    public void setMsgErro(Vector vErro) {
        for (int j = 0; j < vErro.size(); j++) {
            this.msgErro += vErro.elementAt(j);
        }
    }

    public void setMsgErro(String msgErro) {
        this.msgErro = msgErro;
    }

    public String getMsgErro() {
        return this.msgErro;
    }

    public void setCodInfracao(String codInfracao) {
        this.codInfracao = codInfracao;
        if (codInfracao == null)
            this.codInfracao = "";
    }

    public String getCodInfracao() {
        return this.codInfracao;
    }

    public void setCodAutoInfracao(String codAutoInfracao) {
        this.codAutoInfracao = codAutoInfracao;
        if (codAutoInfracao == null)
            this.codAutoInfracao = "";
    }

    public String getCodAutoInfracao() {
        return this.codAutoInfracao;
    }

    public void setValRealDesconto(String valRealDesconto) {
        this.valRealDesconto = valRealDesconto;
        if (valRealDesconto == null)
            this.valRealDesconto = "";
    }

    public String getValRealDesconto() {
        return this.valRealDesconto;
    }

    public String getValRealDescontoEdt() {
        return sys.Util
                .ComEdt(sys.Util.SemEdt(this.valRealDesconto, '.'), 7, 2);
    }

    public void setValReal(String valReal) {
        this.valReal = valReal;
        if (valReal == null)
            this.valReal = "";
    }

    public String getValReal() {
        return this.valReal;
    }

    public String getValRealEdt() {
        return sys.Util.ComEdt(sys.Util.SemEdt(this.valReal, '.'), 7, 2);
    }

    public void setDscInfracao(String dscInfracao) {
        this.dscInfracao = dscInfracao;
        if (dscInfracao == null)
            this.dscInfracao = "";
    }

    public String getDscInfracao() {
        return this.dscInfracao;
    }

    public void setDscGravidade(String dscGravidade) {
        this.dscGravidade = dscGravidade;
        if (dscGravidade == null)
            this.dscGravidade = "";
    }

    public String getDscGravidade() {
        return this.dscGravidade;
    }

    public void setDscEnquadramento(String dscEnquadramento) {
        this.dscEnquadramento = dscEnquadramento;
        if (dscEnquadramento == null)
            this.dscEnquadramento = "";
    }

    public String getDscEnquadramento() {
        return this.dscEnquadramento;
    }

    public void setDscCompetencia(String dscCompetencia) {
        this.dscCompetencia = dscCompetencia;
        if (dscCompetencia == null)
            this.dscCompetencia = "M";
    }

    public String getDscCompetencia() {
        return this.dscCompetencia;
    }

    public void setNumPonto(String numPonto) {
        this.numPonto = numPonto;
        if (numPonto == null)
            this.numPonto = "0";
        this.setDscGravidade("LEVE");
        if (sys.Util.isNumber(this.numPonto)) {
            int num = Integer.parseInt(this.numPonto);
            if (num >= 7)
                this.setDscGravidade("GRAV�SSIMA");
            else {
                if (num >= 5)
                    this.setDscGravidade("GRAVE");
                else {
                    if (num >= 4)
                        this.setDscGravidade("M�DIA");
                }
            }
        }
    }

    public String getNumPonto() {
        return this.numPonto;
    }

    public void setIndSDD(String indSDD) {
        this.indSDD = indSDD;
        if (indSDD == null)
            this.indSDD = "N";
    }

    public String getIndSDD() {
        return this.indSDD;
    }

    public void setIndTipo(String indTipo) {
        this.indTipo = indTipo;
        if (indTipo == null)
            this.indTipo = "C";
    }

    public String getIndTipo() {
        return this.indTipo;
    }

    public void setInfracoes(List infracoes) {
        this.infracoes = infracoes;
    }

    public List getInfracoes() {
        return infracoes;
    }

    // --------------------------------------------------------------------------
    public void LeInfracao() throws sys.BeanException {
        try {
            LeInfracao("codigo");
        } // fim do try
        catch (Exception ex) {
            throw new sys.BeanException(ex.getMessage());
        }
        return;
    }

    // --------------------------------------------------------------------------
    public void LeInfracao(String tpchave) throws sys.BeanException {
        try {
            DaoBroker dao = DaoBrokerFactory.getInstance();
            dao.LeInfracao(this, tpchave);
        }
        catch (Exception ex) {
            throw new sys.BeanException(ex.getMessage());
        }
    }
    // --------------------------------------------------------------------------
    public void setInfs(InfracaoBean infracaoBean) throws sys.BeanException {

        this.infs="";
        String aux="";
        for(int f=0;f<35;f++)   
            aux=aux+"&nbsp;";
        
        StringBuffer popupBuffer = new StringBuffer();
        popupBuffer.append("<select style='border-style: outset;' name='codInfracao' size='0'> \n");   
        popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");                   
  
        try {
          Iterator it = infracaoBean.getInfracoes().iterator() ;
          InfracaoBean infracaoBeanResult  = new InfracaoBean();
          while (it.hasNext()) {
              infracaoBeanResult =(InfracaoBean)it.next() ;
              popupBuffer.append("<OPTION VALUE='"+infracaoBeanResult.getCodInfracao()+"'>"+infracaoBeanResult.getDscEnquadramento()+"</OPTION> \n");
          }
        }
        catch (Exception ex) {
            throw new sys.BeanException(ex.getMessage());
        }
        popupBuffer.append("</SELECT> \n");   
        this.infs = popupBuffer.toString() ;
      }
      public String getInfs()   {
        return  this.infs ;
      }
}
