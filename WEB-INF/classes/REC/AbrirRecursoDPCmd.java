package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;

public class AbrirRecursoDPCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/AbrirRecursoDP.jsp" ;  
   
  public AbrirRecursoDPCmd() {
    next             =  jspPadrao;
  }

  public AbrirRecursoDPCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	  	// cria os Beans, se n�o existir
		AbrirRecursoDPBean AbrirRecursoBeanId     = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId") ;
		if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecursoDPBean() ;	  			

	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  {
			AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  	}
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
		if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
			AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
		}	
		if  (acao.equals("AbreRecurso"))  {
			String cdOrgao = ParamOrgaoId.getCodOrgao();
		  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			nextRetorno = processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBeanId);
			ParamOrgaoId.setCodOrgao(cdOrgao);
		}	
		
		if (acao.equals("Imprimehistorico")){
            //Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
			String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAuto"));
			AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
		    nextRetorno = "/REC/consultaHistImp.jsp" ;
		}
		
		
		if (acao.equals("ImprimeAuto")){
            //Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
			String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAuto"));
			AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
 		    nextRetorno = "/REC/consultaHistImp.jsp" ;
		}
		
  		// processamento de saida dos formularios
	  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	  	req.setAttribute("AbrirRecursoBeanId",AbrirRecursoBeanId) ;		
    }
    catch (Exception ue) {
	      throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }

  protected String processaDefesa(HttpServletRequest req,ParamOrgBean ParamOrgaoId,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado,REC.AbrirRecursoDPBean AbrirRecursoBeanId) throws sys.CommandException {
	 	String nextRetorno=jspPadrao;
	 	String sMensagem  = "";
	 	try {
	 		myAuto.setMsgErro("") ;
 			Vector vErro = new Vector() ;
 			String datEntrada = req.getParameter("datEntrada");
 			String txtMotivo = req.getParameter("txtMotivo");
 			String txtProcesso = req.getParameter("numProcesso");
 			String cdAta = req.getParameter("cdAta");
 			String txtAta = req.getParameter("dsAta");
 			Ata ata = new Ata();
 			
 			if(cdAta.length()>0 && cdAta != null){
 				ata.setCodAta(cdAta);
 			}
 			if(txtAta.length()>0 && txtAta != null){
 				ata.setDescAta(txtAta);
 			}
 			
 			
 			
 			
 			
	 		if (datEntrada==null) datEntrada = "";
	 		
	 			
	 		sMensagem=datEntrada+" ";
 			//validar data de entrada
	 		if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),datEntrada)>0)
	 			 vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;
	 		else {
//				myAuto.setDatLimiteRecurso(ParamOrgaoId); 
				/*Caso notiticacao sem sucesso, n�o criticar prazo*/
				boolean bNotificacao=true;
/*				if ( (myAuto.getCodStatus().equals("2")) ||(myAuto.getCodStatus().equals("12")) )*/
			    if ("0,1,2".indexOf(myAuto.getCodStatus())>=0)		
				  bNotificacao=false;
				
				if ( (sys.Util.DifereDias(myAuto.getDatLimiteRecursoFolga(),datEntrada)>0) && (bNotificacao) )
				{
					vErro.addElement("Prazo limite: "+myAuto.getDatLimiteRecurso() + "\n") ;	
		 		}
			}
					 	
	 		// ler radio - DC - DP 
	 		String solicDEF = req.getParameter("SolicDPBox");
			if (solicDEF==null) solicDEF="";
			if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
			if (solicDEF==null) solicDEF="";
			if (solicDEF.length()==0) vErro.addElement("Nenhuma solicita��o efetuada.\n") ;
 			if ( (solicDEF.equals("DC")) && ("".equals(AbrirRecursoBeanId.getTpSolDC())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDC()+" \n") ;
			if ( (solicDEF.equals("DP")) && ("".equals(AbrirRecursoBeanId.getTpSolDP())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDP()+" \n") ;
			String txtEMail = req.getParameter("txtEMail");
			if (txtEMail==null) txtEMail = "";
			
			if (vErro.size()<0) {
				 myAuto.setMsgErro(vErro) ;
				 nextRetorno = this.next ;
			}   
			else {
				REC.RequerimentoBean myReq = new REC.RequerimentoBean() ;						
				myAuto.setMsgErro(atualizarAuto(req,myAuto,ParamOrgaoId,UsrLogado,myReq,datEntrada,txtEMail,txtMotivo, txtProcesso,solicDEF, ata)) ;	
				if (myAuto.getMsgErro().length()==0) {					
					if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) { 	
						myAuto.setMsgErro("Defesa Pr�via interposta com Requerimento:"+myReq.getNumRequerimento()) ;
						myAuto.setMsgOk("S") ;
					}
					else if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2").indexOf("R")>=0){
						AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
						String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setNomeImagem(nomeImagem);

                        String alturaImageGra = "80";
                        AbrirRecBeanId.setAlturaImageGra(alturaImageGra);
                        
                        String larguraImageGra = "175";
                        AbrirRecBeanId.setLarguraImageGra(larguraImageGra);
                        
                        String alturaImagePeq = "80";
                        AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);
                        
                        String larguraImagePeq = "175";
                        AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);
                        
                        String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setObservacao(obs);
                        
                        String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setSigProtocolo(siglaProt);
                        
                        /*Atualizar SIT_UTILIZADO e Setar o Caminho*/
                        
                        
                        
                        
					    nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
					    req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	
					
					}else
							nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;
                    //Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
					String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
				    if (!codEventoFiltro.equals("N")){
				       myAuto.filtraHistorico(myAuto,codEventoFiltro);
				    }
				    req.setAttribute("codEventoFiltro",codEventoFiltro);

					    	
				}	 							
			}
 		}
 	  	catch (Exception e){
 		    throw new sys.CommandException("AbrirRecursoDPCmdb: " + e.getMessage()+" "+sMensagem);
 	  	}
 	 	return nextRetorno  ;
 	}
 	private Vector atualizarAuto(HttpServletRequest req,AutoInfracaoBean myAuto,ParamOrgBean myParam,ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,String datEntrada,String txtEMail,String txtMotivo,String txtProcesso,String solicDEF, Ata ata) throws sys.CommandException { 
		Vector vErro = new Vector() ;
		//Raj 
		String bla = req.getParameter("acao");
		try { 	
			// preparar o Bean myHist (sem processo e num requerimento
			// Status 0,1,2,4,5
            myParam.setNomUsrNameAlt(myUsrLog.getNomUserName());
			if (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA","0,1,2,4,5","1").indexOf(myAuto.getCodStatus())>=0) {
				if ("0,1,2,4".indexOf(myAuto.getCodStatus())>=0) myAuto.setDatStatus(datEntrada) ;	
					if(bla.equals("AbreRecurso")){
						myAuto.setCodStatus("5");
					}
			}
			
			myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 

			if ("DC,1C,2C".indexOf(solicDEF)>=0)  myReq.setCodResponsavelDP(myAuto.getCondutor().getCodResponsavel());   
			else myReq.setCodResponsavelDP(myAuto.getProprietario().getCodResponsavel());   

			myReq.setCodTipoSolic(solicDEF);
			myReq.setDatRequerimento(datEntrada);
			// preparar a data do Processo - se nao existir
			if (myAuto.getDatProcesso().length()==0) myAuto.setDatProcesso(myReq.getDatRequerimento());
			myReq.setCodStatusRequerimento("0");
			myReq.setNomUserNameDP(myUsrLog.getNomUserName());
			myReq.setNomResponsavelDP(myUsrLog.getNomUserName());
						
			System.out.println("Username:"+myReq.getNomUserNameDP());
			
			
			
			myReq.setCodOrgaoLotacaoDP(myUsrLog.getOrgao().getCodOrgao());
			myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(txtProcesso);
			myHis.setCodOrgao(myAuto.getCodOrgao());
			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodOrigemEvento("100");			
			myHis.setCodEvento("206");
			myHis.setNomUserName(myUsrLog.getNomUserName());
			myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			// complemento 01 - Num Requerimento no DAO
			myHis.setTxtComplemento02(myReq.getCodTipoSolic());
			if (solicDEF.equals("DC")) {
				myHis.setTxtComplemento04(myAuto.getCondutor().getNomResponsavel());
				myHis.setTxtComplemento05(myAuto.getCondutor().getIndCpfCnpj());				
				myHis.setTxtComplemento06(myAuto.getCondutor().getNumCpfCnpj());				
			}	
			else { 
			    myHis.setTxtComplemento04(myAuto.getProprietario().getNomResponsavel());
			    myHis.setTxtComplemento05(myAuto.getProprietario().getIndCpfCnpj());				
			    myHis.setTxtComplemento06(myAuto.getProprietario().getNumCpfCnpj());
			}
		   myHis.setTxtComplemento03(myReq.getDatRequerimento());
		   myHis.setTxtComplemento07(txtMotivo);
		   /*DPWEB*/
		   myHis.setTxtComplemento08("0");		   
		   myHis.setTxtComplemento12(txtEMail);	
		   	
		   DaoBroker dao = DaoBrokerFactory.getInstance();
		   req.setAttribute("HistoricoBeanId",myHis) ;
		   req.setAttribute("ExisteProcesso",("".equals(myAuto.getNumProcesso()) ? "S" : "N")) ;
		   myAuto.setNumProcesso(txtProcesso);
		   dao.atualizarAuto206(myAuto,myParam,myReq,myHis,myUsrLog, ata);
		   if (myAuto.getMsgErro().length()==0  && (txtEMail.indexOf("@")>=0))
		   {					
				ConteudoEmailBean email = new ConteudoEmailBean();
				email.enviaEmailChangeParam(txtEMail,myHis);
		   }
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		return vErro ;
 	}	
}


