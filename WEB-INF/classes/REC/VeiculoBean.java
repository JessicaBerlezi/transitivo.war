package REC;

import java.util.Vector;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Veiculo<br>
* <b>Description:</b>  Bean de Veiculo  <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class VeiculoBean   { 

  private String msgErro ; 
   
  private String codVeiculo;                
  private String numPlaca;
  private String codMarcaModelo;
  private String dscMarcaModelo;
  private String codEspecie;
  private String dscEspecie;
  private String codCategoria; 
  private String dscCategoria; 
  private String codTipo; 
  private String dscTipo;   
  private String codCor;     
  private String dscCor;           
    
  public VeiculoBean() throws sys.BeanException {
    msgErro        = "" ;		
	codVeiculo     = "" ;
	numPlaca       = "" ;
    codMarcaModelo = "" ;
    dscMarcaModelo = "" ;
    codEspecie     = "" ;
    dscEspecie     = "" ;
    codCategoria   = "" ; 
    dscCategoria   = "" ; 
    codTipo        = "" ; 
    dscTipo        = "" ;   
    codCor         = "" ;     
    dscCor         = "" ;           
	
  }

  public void setMsgErro(Vector vErro)   {
   	   for (int j=0; j<vErro.size(); j++) {
   		  this.msgErro += vErro.elementAt(j) ;
     }
   }
  public void setMsgErro(String msgErro)   {
  	this.msgErro = msgErro ;
   }   
  public String getMsgErro()   {
   	return this.msgErro;
   }     

   public void setCodVeiculo(String codVeiculo)  {
   this.codVeiculo=codVeiculo ;
     if (codVeiculo==null) this.codVeiculo= "";
   }  
   public String getCodVeiculo()  {
   	return this.codVeiculo;
   }
   public void setNumPlaca(String numPlaca)  {
   this.numPlaca=numPlaca ;
     if (numPlaca==null) this.numPlaca= "";
   }  
   public String getNumPlaca()  {
   	return this.numPlaca;
   }
   public void setNumPlacaEdt(String numPlaca)  {
     if (numPlaca==null)      numPlaca = "";
     if (numPlaca.length()<8) numPlaca += "        ";
     this.numPlaca=numPlaca.substring(0,3)+numPlaca.substring(4,8) ;
   }  
   public String getNumPlacaEdt()  {
   	if ("".equals(this.numPlaca)) return this.numPlaca ;
   	return this.numPlaca.substring(0,3)+" "+this.numPlaca.substring(3,7);
   }
   
   public void setCodMarcaModelo(String codMarcaModelo)  {
	   this.codMarcaModelo=codMarcaModelo ;
	   if (codMarcaModelo==null) this.codMarcaModelo= "";
   }  
   public String getCodMarcaModelo()  {
	   return this.codMarcaModelo;
   }
   
   public void setDscMarcaModelo(String dscMarcaModelo)  {
   this.dscMarcaModelo=dscMarcaModelo ;
     if (dscMarcaModelo==null) this.dscMarcaModelo= "";
   }  
   public String getDscMarcaModelo()  {
   	return this.dscMarcaModelo;
   }
   
   public void setCodEspecie(String codEspecie)  {
   this.codEspecie=codEspecie ;
     if (codEspecie==null) this.codEspecie= "";
   }  
   public String getCodEspecie()  {
   	return this.codEspecie;
   }
   public void setDscEspecie(String dscEspecie)  {
   this.dscEspecie=dscEspecie ;
     if (dscEspecie==null) this.dscEspecie= "";
   }  
   public String getDscEspecie()  {
   	return this.dscEspecie;
   }

   public void setCodCategoria(String codCategoria)  {
   this.codCategoria=codCategoria ;
     if (codCategoria==null) this.codCategoria= "";
   }  
   public String getCodCategoria()  {
   	return this.codCategoria;
   }
   public void setDscCategoria(String dscCategoria)  {
   this.dscCategoria=dscCategoria ;
     if (dscCategoria==null) this.dscCategoria= "";
   }  
   public String getDscCategoria()  {
   	return this.dscCategoria;
   }

   public void setCodTipo(String codTipo)  {
   this.codTipo=codTipo ;
     if (codTipo==null) this.codTipo= "";
   }  
   public String getCodTipo()  {
   	return this.codTipo;
   }
   public void setDscTipo(String dscTipo)  {
   this.dscTipo=dscTipo ;
     if (dscTipo==null) this.dscTipo= "";
   }  
   public String getDscTipo()  {
   	return this.dscTipo;
   }

   public void setCodCor(String codCor)  {
   this.codCor=codCor ;
     if (codCor==null) this.codCor= "";
   }  
   public String getCodCor()  {
   	return this.codCor;
   }
   public void setDscCor(String dscCor)  {
   this.dscCor=dscCor ;
     if (dscCor==null) this.dscCor= "";
   }  
   public String getDscCor()  {
   	return this.dscCor;
   }
  
  
//--------------------------------------------------------------------------
public void LeVeiculo()  throws sys.BeanException {
     try {   LeVeiculo("codigo") ;   
	 } // fim do try
     catch (Exception ex) {
     	throw new sys.BeanException(ex.getMessage());
     }
	return;
}
//--------------------------------------------------------------------------
public void LeVeiculo(String tpchave) throws sys.BeanException {
     try { 	 
	       Dao dao = Dao.getInstance();
    	   if ( dao.LeVeiculo(this, tpchave) == false) {
				setCodVeiculo("0") ;
           }	   		   
     }// fim do try
     catch (Exception ex) {
     	setCodVeiculo("0") ;
     	throw new sys.BeanException(ex.getMessage());
     }
  }
//--------------------------------------------------------------------------
  
}
