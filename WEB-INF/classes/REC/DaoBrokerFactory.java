package REC;

public class DaoBrokerFactory {

	private static DaoBroker instance;

	public static DaoBroker getInstance() throws sys.DaoException, sys.ServiceLocatorException {

        if (instance == null) { 
            if (sys.ServiceLocator.getInstance().getQualBroker().equals("1"))
                instance = new DaoBrokerOra();
            else
                instance = new DaoBrokerAdabas();       
        }
        return instance;
    }
	
}


