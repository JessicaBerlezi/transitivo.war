package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GuiaReemiteCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaReemite.jsp" ;  
   
   public GuiaReemiteCmd() {
      next = jspPadrao;
   }

   public GuiaReemiteCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
		  ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
      	  GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId  = new GuiaDistribuicaoBean() ;	  	
      	  //Carrego os Beans		
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
		  GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
     	  
		  String dataIni = req.getParameter("De");
		  if (dataIni == null) dataIni = "";
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  if (dataFim == null) dataFim = "";
		  GuiaDistribuicaoId.setDataFim(dataFim);
		  
		  String acao    = req.getParameter("acao");   
		  if (acao==null) 	{
			GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());	
			GuiaDistribuicaoId.setMsgErro("");	
			GuiaDistribuicaoId.setTipoGuia(GuiaDistribuicaoId.GUIA_AUTOMATICA);
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			acao = " ";
		  	session.removeAttribute("GuiaDistribuicaoId") ;
		  }  
		  else {
			if ("LeGuia,Classifica,MostraAuto,imprimirInf,ImprimirGuia".indexOf(acao)<0)	{
				sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
				cmd.setNext(this.next) ;
				nextRetorno = cmd.execute(req);
			}		
		  }
	
		  if (acao.equals("LeGuia")) {
		    GuiaDistribuicaoId = new GuiaDistribuicaoBean() ;		
		    GuiaDistribuicaoId.setIndSessao(ParamOrgaoId.getParamOrgao("LAYOUT_GUIA_DISTRIBUICAO","N","2"));
		    GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
		    GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		    GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		    GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());						
			GuiaDistribuicaoId.setNumGuia(req.getParameter("numGuia"));
			GuiaDistribuicaoId.LeGuia();
		    if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO PARA ENVIAR AOS RELATORES") ;
			nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp";
		  }	        
          if (acao.equals("Classifica")) { 
          	GuiaDistribuicaoId.Classifica(req.getParameter("ordem"))	;
          	nextRetorno = "/REC/GuiaDistribuicaoMostra.jsp";
      	  }

		  if  (acao.equals("ImprimirGuia"))  {	 
			  GuiaDistribuicaoId.LeGuia();    	  
		  	if (GuiaDistribuicaoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS 	NA GUIA");				  	    
			if("S".equals(req.getParameter("todos")))
				req.setAttribute("imprimeAI", "S");
			else
				req.setAttribute("imprimeAI", "N");
		  	
            // fazer iterator e pegar todos os processos
			Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;					
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();			
			List AIDigs = new ArrayList() ;
			while (it.hasNext()) {
				myAuto = (REC.AutoInfracaoBean)it.next() ;
				/*
				 *  N�o imprimir Mensagem Erro 22/02/2006 - Michel
				 *  myAuto.setMsgErro("");
				 */
				myAuto.LeAIDigitalizado();
				AIDigs.add(myAuto);
				// Gera transacao 208/328/335 para cada Auto				
			}
			GuiaDistribuicaoId.setAutos(AIDigs);
			
			
			session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId);
			req.setAttribute("AutoInfracaoBeanId",myAuto) ;
			
			//Verifica qual layout de impress�o ser� impresso para Niter�i
			String layout = req.getParameter("layout");
			if (layout == null) layout = "";
			
			if(req.getParameter("j_sigFuncao").equals("REC0286"))
			   nextRetorno = "/REC/SolicitaProcessoImp.jsp" ; 
			else if (req.getParameter("j_sigFuncao").equals("REC0239")
					&& GuiaDistribuicaoId.getCodStatus().equals("3") 
					&& layout.equals("ata"))
            {//Imprimir Ata Defesa Pr�via Niter�i
                ArrayList guias = GuiaDistribuicaoId.LeGuiaNit();
                nextRetorno = "/REC/GuiaPublicaAtaNitDefesaImp.jsp" ;
                req.setAttribute("guias",guias);
            }
			else	
		  	   nextRetorno = "/REC/GuiaPreparaImp.jsp" ;      	  
		  }
      	  if (acao.equals("MostraAuto"))  {
        	AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			  			
			if (acao.equals("MostraAuto")) { 
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
				nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
			}
			else {
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
				nextRetorno = "/REC/AutoImp.jsp" ;
			}					 
			// Verifica se o Usuario logado ve Todos os Orgaos
			String temp = UsrLogado.getCodOrgaoAtuacao();
			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
			AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
			AutoInfracaoBeanId.LeRequerimento(UsrLogado);
			UsrLogado.setCodOrgaoAtuacao(temp);	
			req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
      	  }		
          // processamento de saida dos formularios
          if (acao.equals("R")){
          	session.removeAttribute("GuiaDistribuicaoId") ;
          	if(req.getParameter("j_sigFuncao").equals("REC0286"))
          	{
          	  GuiaDistribuicaoId.setTipoGuia(GuiaDistribuicaoId.GUIA_AUTOMATICA);
          	  session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
          	}   
          }
      	  else session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaReemiteCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }
}
