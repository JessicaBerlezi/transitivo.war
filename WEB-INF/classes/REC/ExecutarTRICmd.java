package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.UsuarioBean;

public class ExecutarTRICmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/ExecutarTRI.jsp" ;  
   
  public ExecutarTRICmd() {
    next             =  jspPadrao;
  }

  public ExecutarTRICmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		

	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	  	// cria os Beans, se n�o existir
	  	RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
	  	if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
	  	ExecutarTRIBean ExecutarTRIId   = (ExecutarTRIBean)req.getAttribute("ExecutarTRIId") ;
	  	if (ExecutarTRIId==null)  ExecutarTRIId = new ExecutarTRIBean() ;	  			
	  	ExecutarTRIId.setJ_sigFuncao(req.getParameter("j_sigFuncao"));
		req.setAttribute("proxAcao","InformaTRI");
	  	AutoInfracaoBean AutoInfracaoBeanId   = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
	  	if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
	  	    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	  	    ExecutarTRIId.setEventoOK(AutoInfracaoBeanId) ;
			if (RequerimentoId.getDatAtuTR().length()==0) RequerimentoId.setDatAtuTR(sys.Util.formatedToday().substring(0,10));
	  	}
		if  (acao.equals("InformaTRI"))  {
		  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	ExecutarTRIId.setEventoOK(AutoInfracaoBeanId) ;
		  	Vector vErro = new Vector() ;
		  	RequerimentoId.setDatAtuTR(req.getParameter("datEntrada"));
		  	String datNot = AutoInfracaoBeanId.getDatNotificacao() ;
		  	
		  	String siglaFuncao    = req.getParameter("j_sigFuncao");

			validarData(vErro,AutoInfracaoBeanId,RequerimentoId,ParamOrgaoId,acao,siglaFuncao);

		  	RequerimentoId.setNumCNHTR(req.getParameter("numCNHTR"));
		  	if (RequerimentoId.getNumCNHTR().length()==0) vErro.addElement("CNH do Real Infrator n�o preenchida. \n") ;
			String iCnh = req.getParameter("indTipoCNHTR");	  						
			if ((iCnh==null) || ("0".equals(iCnh))) iCnh="2" ;
		  	RequerimentoId.setIndTipoCNHTR(iCnh);
		  	if (RequerimentoId.getIndTipoCNHTR().length()==0) vErro.addElement("Tipo CNH do Real Infrator n�o preenchida. \n") ;

		  	RequerimentoId.setCodUfCNHTR(req.getParameter("codUfCNHTR"));
		  	if (RequerimentoId.getCodUfCNHTR().length()==0) vErro.addElement("UF da CNH do Real Infrator n�o preenchida. \n") ;
		  	AutoInfracaoBeanId.setMsgErro(vErro) ;
			// Buscar Condutor
			if (vErro.size()==0)  {
				ExecutarTRIId.buscaCondutor(RequerimentoId) ;
				AutoInfracaoBeanId.setMsgErro(RequerimentoId.getMsgErro()) ;
				if (AutoInfracaoBeanId.getMsgErro().length()==0) {
					if (RequerimentoId.getCodResponsavelDP().equals(AutoInfracaoBeanId.getProprietario().getCodResponsavel()))
						vErro.addElement("Propriet�rio e Real Infrator s�o os mesmos!!! \n") ;		
					if (RequerimentoId.getCodResponsavelDP().equals(AutoInfracaoBeanId.getCondutor().getCodResponsavel()))
						vErro.addElement("Real Infrator e atual Condutor s�o os mesmos!!! \n") ;		
						AutoInfracaoBeanId.setMsgErro(vErro) ;
				}	
			}
			if (AutoInfracaoBeanId.getMsgErro().length()==0) req.setAttribute("proxAcao","ExecutaTRI");
			nextRetorno  = next	;		
		}	
		if  (acao.equals("ExecutaTRI"))  {
		  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	ExecutarTRIId.setEventoOK(AutoInfracaoBeanId) ;			
			ExecutarTRIId.setTxtMotivo(req.getParameter("txtMotivo"));
		  	nextRetorno = processaExecutarTRI(ExecutarTRIId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado,ParamOrgaoId);
		}
		if (acao.equals("imprimirInf")) {	
		    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    ExecutarTRIId.setEventoOK(AutoInfracaoBeanId) ;
			nextRetorno = "/REC/TrocaTrImp.jsp" ;
		}				
	  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	  	req.setAttribute("RequerimentoId",RequerimentoId) ;		
	  	req.setAttribute("ExecutarTRIId",ExecutarTRIId) ;		
    }
    catch (Exception ue) {
          throw new sys.CommandException("ExecutarTRICmd: " + ue.getMessage());
    }		
    return nextRetorno;
  }

  private String processaExecutarTRI(ExecutarTRIBean ExecutarTRIId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado,ParamOrgBean ParamOrgaoId) throws sys.CommandException { 
   	String nextRetorno = next ;	
   	try {
	   	myAuto.setMsgErro("") ;
	   	
   		Vector vErro = new Vector() ;
        //Set do UserName para controle de alteracao do parametro
        ParamOrgaoId.setNomUsrNameAlt(UsrLogado.getNomUserName());
	   	// validar se status e igual a 5
	   	if ("S".equals(ExecutarTRIId.getEventoOK())==false) vErro.addElement(ExecutarTRIId.getMsg()+" \n") ;
	   	myReq.setDatAtuTR(req.getParameter("datEntrada"));
	   	
	   	String acao           = req.getParameter("acao");  
	   	String siglaFuncao    = req.getParameter("j_sigFuncao");
	   	
		validarData(vErro,myAuto,myReq,ParamOrgaoId,acao,siglaFuncao);
			 			 
	   	myReq.setNumCNHTR(req.getParameter("numCNHTR"));
	   	if (myReq.getNumCNHTR().length()==0) vErro.addElement("CNH do Real Infrator n�o preenchida. \n") ;

	   	String iCnh = req.getParameter("indTipoCNHTR");	   	
	   	if ((iCnh==null) || ("0".equals(iCnh))) iCnh="2" ;
	   	myReq.setIndTipoCNHTR(iCnh);
	   	if (myReq.getIndTipoCNHTR().length()==0) vErro.addElement("Tipo CNH do Real Infrator n�o preenchida. \n") ;

	   	myReq.setCodUfCNHTR(req.getParameter("codUfCNHTR"));
	   	if (myReq.getCodUfCNHTR().length()==0) vErro.addElement("UF da CNH do Real Infrator n�o preenchida. \n") ;
	   	myReq.setCodResponsavelDP(req.getParameter("CodResponsavelDP"));		
		myReq.setNomCondutorTR(req.getParameter("nomCondutorTR"));
		myReq.setNumCPFTR(req.getParameter("cpfpnthidden"));
		myReq.setTxtEnderecoTR(req.getParameter("txtEnderecoTR"));
		myReq.setNomBairroTR(req.getParameter("nomBairroTR"));
		myReq.setNomCidadeTR(req.getParameter("nomCidadeTR"));
		myReq.setCodCidadeTR(req.getParameter("codCidadeTR"));
		myReq.setCodUfTR(req.getParameter("codUFTR"));
		myReq.setNumCEPTREdt(req.getParameter("numCEPTR"));
		
		if (vErro.size()==0) {
			myAuto.setDatStatus(myReq.getDatAtuTR());
			if ("0,1,2,4".indexOf(myAuto.getCodStatus())>=0) myAuto.setCodStatus("4");  			
  			myReq.setNomUserNameTR(UsrLogado.getNomUserName());
  			myReq.setCodOrgaoLotacaoTR(UsrLogado.getOrgao().getCodOrgao());
  			myReq.setDatProcTR(sys.Util.formatedToday().substring(0,10));
			myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao())	;
			myReq.setCodTipoSolic("TR");
			myReq.setCodStatusRequerimento("9");
			myReq.setDatRequerimento(myReq.getDatAtuTR());
			myReq.setNomUserNameDP(myReq.getNomUserNameTR());
			myReq.setCodOrgaoLotacaoDP(myReq.getCodOrgaoLotacaoTR());
			myReq.setDatProcDP(myReq.getDatProcTR());
  			HistoricoBean myHis = new HistoricoBean() ;
  			atualizarReq(ExecutarTRIId,myAuto,myReq,ParamOrgaoId,req,myHis,UsrLogado) ;
  			if (myAuto.getMsgErro().trim().length()==0) {
  			    if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) {
			  		myAuto.setMsgOk("S") ;										
  			    	myAuto.setMsgErro("Troca de Real Infrator realizada com Requerimento:"+myReq.getNumRequerimento()) ;
  			    }
  				else { 		
  					req.setAttribute("HistoricoBeanId",myHis) ;
  					req.setAttribute("AutoInfracaoBeanId",myAuto) ;
  					if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2").indexOf("R")>=0)
  					{
  						AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
						String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setNomeImagem(nomeImagem);

                        String alturaImageGra = "80";
                        AbrirRecBeanId.setAlturaImageGra(alturaImageGra);
                        
                        String larguraImageGra = "175";
                        AbrirRecBeanId.setLarguraImageGra(larguraImageGra);
                        
                        String alturaImagePeq = "80";
                        AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);
                        
                        String larguraImagePeq = "175";
                        AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);
                        
                        String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setObservacao(obs);
                        
                        String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
                        AbrirRecBeanId.setSigProtocolo(siglaProt);
                        
					    nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
					    req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	
  					}
  				    else
  						nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;	
  				}
  			}
  		}
	  	else { 
			myAuto.setMsgErro(vErro) ;
	  	}
   	}
    catch (Exception e){
  	    throw new sys.CommandException("ExecutarTRICmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }
  private void atualizarReq(ExecutarTRIBean ExecutarTRIId,AutoInfracaoBean myAuto,RequerimentoBean myReq,ParamOrgBean ParamOrgaoId,HttpServletRequest req,HistoricoBean myHis,UsuarioBean UsrLogado)
   throws sys.CommandException { 
  	String nextRetorno=jspPadrao;
  	try { 	
  		// preparar Bean de Historico
  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
  		myHis.setCodOrgao(myAuto.getCodOrgao());
  		myHis.setCodStatus(myAuto.getCodStatus());  
  		myHis.setDatStatus(myAuto.getDatStatus());
		myHis.setCodEvento(ExecutarTRIId.getCodEvento());
		myHis.setCodOrigemEvento(ExecutarTRIId.getOrigemEvento());
  		myHis.setNomUserName(myReq.getNomUserNameTR());
  		myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoTR());
  		myHis.setDatProc(myReq.getDatProcTR());
  		myHis.setTxtComplemento01(myReq.getDatAtuTR());	
  		myHis.setTxtComplemento02(myReq.getNumCPFTR());	
  		myHis.setTxtComplemento03(myReq.getNomCondutorTR());	
  		myHis.setTxtComplemento04(myReq.getTxtEnderecoTR());	
  		myHis.setTxtComplemento05(myReq.getNomBairroTR());	
  		myHis.setTxtComplemento06(myReq.getCodCidadeTR());	
  		myHis.setTxtComplemento07(myReq.getCodUfTR());	
  		myHis.setTxtComplemento08(myReq.getNumCEPTR());	
  		myHis.setTxtComplemento09(myReq.getIndTipoCNHTR()+sys.Util.lPad(myReq.getNumCNHTR(),"0",11));	
  		myHis.setTxtComplemento10(myReq.getCodUfCNHTR());	
  		myHis.setTxtComplemento11(myReq.getNumRequerimento());
  		req.setAttribute("ExisteProcesso",("".equals(myAuto.getNumProcesso()) ? "S" : "N")) ;			
  	    DaoBroker dao = DaoBrokerFactory.getInstance();
		dao.atualizarAuto214(myReq,myHis,myAuto,ParamOrgaoId,UsrLogado);
		
		/*Atualizar Processo utilizado*/		   
		if (UsrLogado.equals("258650"))
		{
			dao.BuscarDataCodigoBarraProcesso(myAuto);   
			dao.AtualizaCodigoBarraProcesso(myAuto);
		}
		
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException("ExecutarTRICmd2: "+ ex.getMessage());
  	}
  	return  ;
  }	
  

  private void validarData(Vector vErro,AutoInfracaoBean myAuto,RequerimentoBean myReq,ParamOrgBean ParamOrgaoId)
  throws sys.CommandException, sys.BeanException { 
  	//validar data de entrada
  	if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),myReq.getDatAtuTR())>0) 
  		vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;  
  	if (sys.Util.DifereDias(myReq.getDatAtuTR(),myAuto.getDatInfracao())>0)
  		vErro.addElement("Data de Entrada n�o pode ser anterior a data da Infra��o:"+myAuto.getDatInfracao()+". \n") ;			
  	else  {
  		myAuto.setDatLimiteRecurso(ParamOrgaoId);	
        if(!myAuto.getCodStatus().equals("2"))
        {
            if (sys.Util.DifereDias(myAuto.getDatLimiteRecursoFolga(),myReq.getDatAtuTR())>0) 
              vErro.addElement("Prazo limite: "+myAuto.getDatLimiteRecursoFolga() + "\n") ; 
        }

 	}  
  }
  
  /**
   * Alterar Acerto Troca de servi�o TRI.
   */
  	private void validarData(Vector vErro,AutoInfracaoBean myAuto,RequerimentoBean myReq,ParamOrgBean ParamOrgaoId,String acao,String siglaFuncao)
		  throws sys.CommandException, sys.BeanException {
  		
  		validarData(vErro, myAuto, myReq, ParamOrgaoId);
  		
  		if(!myAuto.getDatNotificacao().equals("")){
  			if  (siglaFuncao.equals("REC0141"))  {
  		  		if (sys.Util.DifereDias(myAuto.getDatNotificacao(), sys.Util.formatedToday().substring(0,10))>0)
  		  			vErro.addElement("Data de Entrada n�o pode ser superior a 15 dias da data de notifica��o. \n") ;
  	  		}
  		}
	}	
}