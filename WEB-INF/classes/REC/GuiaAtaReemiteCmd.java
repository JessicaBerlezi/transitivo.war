package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class GuiaAtaReemiteCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaAtaReemite.jsp" ;  
   
   public GuiaAtaReemiteCmd() {
      next = jspPadrao;
   }

   public GuiaAtaReemiteCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          
          if (UsrLogado==null)  UsrLogado = new ACSS.UsuarioBean() ;	  			
		  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
		  GuiaDistribuicaoBean GuiaDistribuicaoId = new GuiaDistribuicaoBean();
		  
		  //Carrego os Beans		
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
      	  
		  String dataIni = req.getParameter("De");
		  GuiaDistribuicaoId.setDataIni(dataIni);
		  
		  String dataFim = req.getParameter("Ate");
		  GuiaDistribuicaoId.setDataFim(dataFim);
      	  
		  String acao    = req.getParameter("acao");  
		  GuiaDistribuicaoId.setColunaValue("COD_ENVIO_PUBDO");
		  GuiaDistribuicaoId.setPopupNome("codEnvioDO");
		  String sql = "ATA, SELECT DISTINCT COD_ENVIO_PUBDO ,'ATA: ' || COD_ENVIO_PUBDO || '    ENVIADA: '  || TO_DATE(DAT_ENVIO_PUBDO,'DD/MM/YYYY') || '    PUBLICADA: ' || DAT_PUBDO || '    STATUS: ' || decode(COD_STATUS,4,'Enviado p/ Publica��o',9,'Publicado') AS ATA "+ 
		  " FROM TSMI_LOTE_RELATOR L JOIN TSMI_JUNTA J"+
		  " ON L.COD_JUNTA = J.COD_JUNTA"+
		  " WHERE IND_TIPO = "+ GuiaDistribuicaoId.getTipoJunta() + 
		  " AND (COD_STATUS = 4 OR COD_STATUS = 9)" +
		  " AND L.COD_ORGAO = " + GuiaDistribuicaoId.getCodOrgaoAtuacao()+
		  " AND L.DT_PROC BETWEEN TO_DATE('"+GuiaDistribuicaoId.getDataIni()+"','DD/MM/YYYY') AND TO_DATE('"+GuiaDistribuicaoId.getDataFim()+"','DD/MM/YYYY') "+
		  " ORDER BY ATA";
		  if (acao==null)
		  	acao = "";

		  if  (acao.equals("ImprimirAta"))  {	 
		  	GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("codEnvioDO"));
		  	int ordem = 2;
		  	if(req.getParameter("layout").equals("Relatorio")){
				if(ParamOrgaoId.getParamOrgao("LAYOUT_ATA","S","2").indexOf("S")>=0){
					ordem = 3;
					GuiaDistribuicaoId.consultaAtaImprime(ordem);	
					nextRetorno = "/REC/GuiaPublicaAtaNitImp.jsp";
				}else{
					GuiaDistribuicaoId.consultaAtaImprime(ordem);	
		  		    nextRetorno = "/REC/GuiaPublicaAtaImp.jsp";
				}
		  	}
		  	else{
		  		String layOutCabAta = "N";
		  		ACSS.OrgaoBean myOrg = new ACSS.OrgaoBean();
		  		GuiaDistribuicaoId.consultaAtaImprime(ordem);	
		  		if(ParamOrgaoId.getParamOrgao("LAYOUT_CABECALHO_ATA","N","3").indexOf("S")>=0){
		  			layOutCabAta = "S";
		  			myOrg.Le_Orgao(UsrLogado.getCodOrgaoAtuacao(),0);
		  			req.setAttribute("myOrg",myOrg);
		  			
		  		}
		  		req.setAttribute("layOutCabAta",layOutCabAta);
		  		nextRetorno = "/REC/GuiaPublicaDO.jsp" ;   
		  	}
		  }

		  
		  if  (acao.equals("GravarAta"))  {	 		  	
		  	GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("codEnvioDO"));
		  	GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());
		  	GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  	GuiaDistribuicaoId.setChecked(GuiaDistribuicaoId.getCodEnvioDO());
		  	GuiaDistribuicaoId.consultaAta(4);	
		  	
		  	if(req.getParameter("confirmaExclusao").equals("S"))
		  		GuiaDistribuicaoId.setCodStatus("-1");	
            if(ParamOrgaoId.getParamOrgao("LAYOUT_ATA","S","2").indexOf("S")>=0)
            {
            	GuiaDistribuicaoId.consultaAta(1);
                if(!GuiaDistribuicaoId.gravaArquivoNit((ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId"),ParamOrgaoId,GuiaDistribuicaoId)){
                    req.setAttribute("confirmaExclusao","S");
                }
            }
            else 
            {
    		  	if(!GuiaDistribuicaoId.gravaArquivo((ParamSistemaBean)req.getSession().getAttribute("ParamSistemaBeanId"),ParamOrgaoId)){
    		  		req.setAttribute("confirmaExclusao","S");
    		  	}
            }
		  }
		  GuiaDistribuicaoId.setPopupString(sql);
		  session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;  
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaAtaReemiteCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }
}
