package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

public class ConsAIDigCmd extends sys.Command{

    private String next;
    private static final String jspPadrao = "/REC/ConsAIDig.jsp";

    public ConsAIDigCmd() {
        next = jspPadrao;
    }

    public ConsAIDigCmd(String next) {
        this.next = next;
    }

    public String execute(HttpServletRequest req) throws sys.CommandException {
        String nextRetorno = next;
                
        try {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            
            String acao = req.getParameter("acao");
            if (acao == null) acao = "";

            String datInicial = req.getParameter("datInicial");
            if (datInicial == null) datInicial = df.format(new Date());
            
            String datFinal = req.getParameter("datFinal");
            if (datFinal == null) datFinal = df.format(new Date());
            
            LogAIDigitalizadoBean LogAIDigitalizadoBeanId = new LogAIDigitalizadoBean();
            
            if (acao.equals("busca"))
                LogAIDigitalizadoBeanId.carrega(df.parse(datInicial), df.parse(datFinal));
            
            else if (acao.equals("retorna"))
                acao = "";
                        
            req.setAttribute("acao", acao);
            req.setAttribute("LogAIDigitalizadoBeanId", LogAIDigitalizadoBeanId);
            req.setAttribute("datInicial", datInicial);
            req.setAttribute("datFinal", datFinal);

        } catch (Exception e) {
            throw new sys.CommandException(e.getMessage());
        }
        return nextRetorno;
    }

}