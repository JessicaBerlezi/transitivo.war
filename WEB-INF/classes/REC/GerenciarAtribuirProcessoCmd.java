package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class GerenciarAtribuirProcessoCmd extends  sys.Command{
	private String next;
	private static final String jspPadrao="/REC/AtribuirProcesso.jsp" ;


	public GerenciarAtribuirProcessoCmd() {
		next = jspPadrao;
	}
	public GerenciarAtribuirProcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next;
		String codOrgao = req.getParameter("codOrgao");
		
		try {     
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean UsrLogado          = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado     = new ACSS.UsuarioBean() ;
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
			ParamOrgBean ParamOrgaoId           = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			ParamSistemaBean parSis             = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis          = new ParamSistemaBean();
			AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			// cria os Beans, se n�o existir
			AbrirRecursoDPBean AbrirRecursoBeanId     = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId") ;

			if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecursoDPBean() ;	  			

			if (AutoInfracaoBeanId==null)  	{
				AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
				if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
			}
			// obtem e valida os parametros recebidos					
			String acao           = req.getParameter("acao");  

			if(acao==null)   acao = " ";

			String tpRequerimento = req.getParameter("tpRequerimento");
			String cdAta = req.getParameter("cdAta");
			String txtAta = req.getParameter("dsAta");
			String codRelator = req.getParameter("idCpfAtaRelator");
			String dsRelator = req.getParameter("dsRelator");
			String codCRelator = req.getParameter("cdRelatorC");
			String dsCRelator = req.getParameter("nameRelator");


			String tpRequerimentoExtra = req.getParameter("tpRequerimentoExtra");
			String cdAtaExtra = req.getParameter("idNrAtaNameExtra");
			String txtAtaExtra = req.getParameter("dsAtaExtra");


			String indices = req.getParameter("hdindice");
			String indicesC = req.getParameter("hdindiceC");

			if(acao.equals("buscaProcessos")){
				nextRetorno = processaProcessos(req, tpRequerimento, cdAta, txtAta, codRelator, dsRelator );
				req.setAttribute("SemAtribuicao","processo sem atribuicao");
				req.setAttribute("ComAtribuicao","processo com atribuicao");
			}

			if(acao.equals("imprimirInf")){
				try{
					nextRetorno = processaProcessosPrint(req, tpRequerimento, cdAta, txtAta, codRelator, dsRelator );
				}catch(Exception e){
					
				}
			}
			
			if(acao.equals("Inclui")){ //Incluir processos n�o atribuidos a relator(o relator informado passa a ser o resposanvel pelo processo)
				Ata attt = new Ata();
				if(((indices.length()> 0)&&(indices != null))){ // for example 0,3
					String[] requerimentos = indices.split(",");
					
					for(String codRequerimento : requerimentos)
					{
						if(tpRequerimentoExtra.equals("1")){
							tpRequerimentoExtra = "Def. Previa";
						}
						if(tpRequerimentoExtra.equals("2")){
							tpRequerimentoExtra = "1� INST�NCIA";
						}
						if(tpRequerimentoExtra.equals("3")){
							tpRequerimentoExtra = "2� INST�NCIA";
						}
						if(tpRequerimentoExtra.equals("0")){
							tpRequerimentoExtra = "SELECIONE";
						}

						attt.transfereDadosSemAtribuicaoPAtribuicao(codRelator, dsRelator, codRequerimento, UsrLogado.getNomUserName());
						
						attt.getProcessoSemAtribuicaoMap(tpRequerimentoExtra, cdAta, txtAta, codRelator, dsRelator, codOrgao);
						attt.getProcessoComAtribuicaoMap(tpRequerimentoExtra, cdAta, txtAta, codRelator, dsRelator, codOrgao);
					}
				}
				req.setAttribute("AtualizaSemAtribuicao","processo sem atribuicao");
			}
			else if(acao.equals("Exclui")){ //Exclui processos atribuidos a relator(O relator deixa de ser o responsavel pelo processo)
				Ata attt = new Ata();
				if(((indicesC.length()> 0)&&(indicesC != null))){ // for example0,3
					String[] requerimentos = indicesC.split(",");

					for(String codRequerimento : requerimentos)
					{
						if(tpRequerimentoExtra.equals("1")){
							tpRequerimentoExtra = "Def. Previa";
						}
						if(tpRequerimentoExtra.equals("2")){
							tpRequerimentoExtra = "1� INST�NCIA";
						}
						if(tpRequerimentoExtra.equals("3")){
							tpRequerimentoExtra = "2� INST�NCIA";
						}
						if(tpRequerimentoExtra.equals("0")){
							tpRequerimentoExtra = "SELECIONE";
						}

						attt.transfereDadosComAtribuicaoPAtribuicao(codRelator, dsRelator, codRequerimento, UsrLogado.getNomUserName());

						attt.getProcessoSemAtribuicaoMap(tpRequerimentoExtra, cdAta, txtAta, codRelator, dsRelator, codOrgao);
						attt.getProcessoComAtribuicaoMap(tpRequerimentoExtra, cdAta, txtAta, codRelator, dsRelator, codOrgao);
					}
				}	
			}
		}
		catch (Exception ue) {
			throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

	protected String processaProcessos(HttpServletRequest req, String tipoRequerimento, String codAta, String descAta, String codRelator, String nomeRelator) throws sys.CommandException {
		String nextRetorno= "OK"; //jspPadrao;
		String codOrgao = req.getParameter("codOrgao");
		
		try {
			String acao           = req.getParameter("acao");  
			Ata ata = new Ata();
			if(acao==null)   acao = " ";
			if(acao.equals("buscaProcessos")){
				ata.getProcessoSemAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
				ata.getProcessoComAtribuicaoMap(tipoRequerimento, codAta, descAta, codRelator, nomeRelator, codOrgao);
			}
		}
		catch (Exception e){
			nextRetorno = "N�o OK"; //"/REC/ErroDataAta.jsp";
			return nextRetorno;
		}
		return nextRetorno;
	}
	
	protected String processaProcessosPrint(HttpServletRequest req, String tipoRequerimento, String codAta, String descAta, String codRelator, String nomeRelator) throws sys.CommandException {
	 	String nextRetorno=jspPadrao;
	 	try {
	 		//Retorna Html
	 		req.setAttribute("codAta", codAta);
	 		req.setAttribute("codRelator", codRelator);
	 	 	nextRetorno = "/REC/AtaDistribuicaoImpPorRelator.jsp";
 		}
 	  	catch (Exception e){
 	  		nextRetorno = "/REC/ErroDataAta.jsp" ;
			return nextRetorno;
 	  	}
 	 	return nextRetorno  ;
 	}
	
}
