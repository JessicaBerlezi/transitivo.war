package REC;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import ACSS.ParamSistemaBean;

public class ProcessaAutoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/AbrirRecursoDP.jsp" ;  
   
  public ProcessaAutoCmd() {
    next  =  jspPadrao;
  }

  public ProcessaAutoCmd(String next) {
    this.next = next;
  }
  public void setNext(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
  	String sMensagem    = "";
    try {     
	    // cria os Beans de sessao, se n�o existir //
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado          = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado     = new ACSS.UsuarioBean() ;
		ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
	    ParamOrgBean ParamOrgaoId           = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
	    ParamSistemaBean parSis             = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (parSis == null)	parSis          = new ParamSistemaBean();
	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  	{
	  		AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  		if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
	  		if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
	  		if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
	  		if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
	  		req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
	  	}
	  	
		// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
	  	if ( ("Novo".equals(acao)) || ("valBiometria".equals(acao))  ){
		  	req.setAttribute("LeAI","S");
			acao =" ";
		  	AutoInfracaoBeanId     = new AutoInfracaoBean() ;
	  	}	
		else {	
		  	if (" ".equals(acao)) req.setAttribute("LeAI","S");
			else {	
				req.setAttribute("LeAI","N");				
	  			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));  
		  		AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));  
	  			AutoInfracaoBeanId.setNumProcesso(req.getParameter("numProcesso"));
	  			String numRenavam = req.getParameter("numRenavam");
	  			if (numRenavam==null) numRenavam="";
	  			AutoInfracaoBeanId.setNumRenavam(numRenavam);
	  			String codAcessoWeb = req.getParameter("numAcesso");
	  			if (codAcessoWeb==null) codAcessoWeb="";
	  			AutoInfracaoBeanId.setCodAcessoWeb(codAcessoWeb);
				String temp = UsrLogado.getCodOrgaoAtuacao();
				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999");
				
				//se for referente a ata nao entro nesse escopo
				if(!acao.equals("IncluirAta") && !acao.equals("ExcluirAta") &&  !acao.equals("AlterarAta") &&  !acao.equals("alterar")){
					
					try{
						AutoInfracaoBeanId.LeAutoInfracao("codigo",UsrLogado)	;
						UsrLogado.setCodOrgaoAtuacao(temp);
						AutoInfracaoBeanId.LeRequerimento(UsrLogado);
					}catch (Exception e) {
						nextRetorno = "/REC/AbrirRecursoDP.jsp" ;
					}
				}
			
				/*Retiramos-17/08/2005
				AutoInfracaoBeanId.LeHistorico();*/
				
				/*Jogamos pra cima/*
				if ( (ParamOrgaoId.getParamOrgao("STATUS_ABRIR_2INSTANCIA","30,35,36","4").indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1) ) 				
					AutoInfracaoBeanId.LeHistorico();
				*/
				
  				if (AutoInfracaoBeanId.getMsgErro().length()!=0) req.setAttribute("LeAI","S");
				// mostra requerimentos do auto 
	  			if  (acao.equals("requerimento"))  {									
	  				nextRetorno = "/REC/mostraRequerimento.jsp" ;
		  		}
	  			
				// mostra Historico do Auto
	  			if  (acao.equals("historico"))  {	
		  			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;				
		  			nextRetorno = "/REC/mostraHistorico.jsp" ;
			  	}
	  			
//				Imprime Historico do Auto
				if  (acao.equals("Imprimehistorico"))  {								
					AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
					nextRetorno = "/REC/consultaHistImp.jsp" ;
				}			  

//				Imprime Requerimento do Auto
				if  (acao.equals("Imprimerequerimento"))  {								
					AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
					nextRetorno = "/REC/consultaReqImp.jsp" ;
				}			  		
				
				// mostra Auto Digitalizado
				if  ( (acao.equals("AIDigitalizado")) && (AutoInfracaoBeanId.getMsgErro().length()==0)   )  {	
		  			AutoInfracaoBeanId.LeAIDigitalizado();	
		  			
		  			String codInfracaoAdm = ParamOrgaoId.getParamOrgao("COD_INFRACAO_ADM","69200,99800","10");
		  			String codAgente=parSis.getParamSist("CODIGO_AGENTE_PALM");
		  			if(codInfracaoAdm.indexOf(AutoInfracaoBeanId.getInfracao().getCodInfracao())>=0)
						nextRetorno = "/REC/MultaAdm.jsp";
		  			else  if  ( (codAgente.indexOf(AutoInfracaoBeanId.getCodAgente())>=0) &&
		    				  (AutoInfracaoBeanId.getCodAgente().length()==2)
	  				          )
						nextRetorno = "/REC/MultaPalm.jsp";
					else nextRetorno = "/REC/VisAIDig.jsp";
					
					if (UsrLogado.getCodOrgaoAtuacao().equals("258650"))
					{
						if (AutoInfracaoBeanId.getIdentOrgao().trim().length()>0)
						{
							/*Palm - Radar*/
							if ( (AutoInfracaoBeanId.getIdentOrgao().substring(1,2).equals("1")) 
							   )
							{
								AutoInfracaoBeanId.LeFotoDigitalizada(); 
								nextRetorno = "/REC/AutoNiteroiImp.jsp" ;
							}
						}
					}
		  		}
				
				if (acao.equals("chamaImagem")) {					
					AutoInfracaoBeanId.LeAIDigitalizado();
					nextRetorno = "/REC/VisAIDigImagem.jsp" ;					
				}
				
				// mostra AR Digitalizado		  			  	
				if  (acao.equals("ARDigitalizado"))  {
					String numNotificacao = req.getParameter("numNotificacao");
					AutoInfracaoBeanId.setNumNotificacao(numNotificacao);
					AutoInfracaoBeanId.LeARDigitalizado() ;	
					nextRetorno = "/REC/VisARDig.jsp" ;
				}
				
				//
				if  (acao.equals("VisARDigImagem"))  {
					String numNotificacao = req.getParameter("numNotificacao");
					AutoInfracaoBeanId.setNumNotificacao(numNotificacao);
					AutoInfracaoBeanId.LeARDigitalizado();	
					nextRetorno = "/REC/VisARDigImagem.jsp" ;
				}					
				
				 // mostra Foto Digitalizada                       
                if  (acao.equals("FotoDigitalizada"))  {
                    AutoInfracaoBeanId.LeFotoDigitalizada() ; 
                    nextRetorno = "/REC/VisFotoDig.jsp" ;
                }
                
                //
                if  (acao.equals("VisFotoDigImagem"))  {                  
                    AutoInfracaoBeanId.LeFotoDigitalizada();  
                    nextRetorno = "/REC/VisFotoDigImagem.jsp" ;
                }				
                
				// imprime 2 via da notificacao
				if (acao.equals("ImprimeNotif")) {
					//Seta o c�digo da Notifica��o para passar para o Notificacao.jsp
				    ACSS.UsuarioBean UsrLogadoRec       = new ACSS.UsuarioBean() ;	
				    ParamOrgBean ParamOrgaoRecId        = new ParamOrgBean() ;
					UsrLogadoRec.setCodOrgaoAtuacao(AutoInfracaoBeanId.getCodOrgao());
					ParamOrgaoRecId.PreparaParam(UsrLogadoRec);
					
					InstrNotBean NotifBeanId = new InstrNotBean() ;					
					NotifBeanId.setCodNotificacao(AutoInfracaoBeanId.getCodNotificacao(ParamOrgaoId));
					NotifBeanId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
					NotifBeanId.LeNotificacoes();
					NotifBeanId.setTxtEnderecoRecurso(ParamOrgaoRecId.getParamOrgao("ENDERECO_RECURSO"));
					AutoInfracaoBeanId.LeFotoDigitalizada();
					AutoInfracaoBeanId.LeHistorico(UsrLogado) ; // Altera��o realizada em 14/06/2012 para visualiza��o das datas de notifica��o
					req.setAttribute("NotifBeanId",NotifBeanId);
					/*PMN*/
					if("258650".equals(AutoInfracaoBeanId.getCodOrgao()))
						nextRetorno="/REC/NotificacaoNit.jsp";
					else
						nextRetorno = "/REC/Notificacao.jsp" ;						
				}
				
				if (acao.equals("imprimirInf")) {
					AutoInfracaoBeanId.LeFotoDigitalizada() ; 
					nextRetorno = "/REC/AutoImp.jsp" ;
				}		
				
				if (acao.equals("imprimirInfRaj")) {
		REC.RequerimentoBean requimentoBean          = (REC.RequerimentoBean)session.getAttribute("RequerimentoBean") ;
	    if (requimentoBean==null)  requimentoBean     = new REC.RequerimentoBean() ;
					AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
		  			String c = req.getParameter("NumProcessoRequerimentoRaj");
		  			String assuntoP = 	req.getParameter("AssuntoProcesso");
		  			requimentoBean.setNumRequerimento(req.getParameter("NumProcessoRequerimentoRaj"));
		  			AutoInfracaoBeanId.setDscResumoInfracao(assuntoP);
		  			session.setAttribute("RequerimentoBean", requimentoBean);
		  			System.out.print("c : " + c);
		  			
					String numProcesso = req.getParameter("NumProcesso");
					String assunto = req.getParameter("TxtComplemento03");
					String assuntoPp = 	req.getParameter("AssuntoProcesso");	
							System.out.print("numProcesso : " + numProcesso + " / " + "assunto : " + " / " + assunto + " / " +  "assuntoPp : " + assuntoPp);
					nextRetorno = "/REC/AutoImpRaj.jsp" ;
				}
				
				// mostra Fator Gerador
				if  (acao.equals("ApresFtGerador"))  {	
					AutoInfracaoBeanId.setNumAutoInfracao(AutoInfracaoBeanId.getNumAutoOrigem());
					AutoInfracaoBeanId.LeAutoInfracao("codigo",UsrLogado)	;					
					nextRetorno = "/REC/AbrirConsultaAuto.jsp";
				}
			  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;			
			}	
		}
    }
    catch (Exception ue) {
	      throw new sys.CommandException("ProcessaAutoCmd: " + ue.getMessage()+" "+sMensagem);
    }
	return nextRetorno ;
  }
  
}
