package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


public class GuiaInformaPublicacaoCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/GuiaInformaPublicacao.jsp" ;  
   
   public GuiaInformaPublicacaoCmd() {
      next = jspPadrao;
   }

   public GuiaInformaPublicacaoCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	   String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
          HttpSession session   = req.getSession() ;								
          ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
          if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
          ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
          if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  			
		  ParamOrgBean ParamOrgaoId                       = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		  if (ParamOrgaoId==null)  ParamOrgaoId           = new ParamOrgBean() ;	  			
      	  GuiaDistribuicaoBean GuiaDistribuicaoId         = (GuiaDistribuicaoBean)session.getAttribute("GuiaDistribuicaoId") ;
      	  if (GuiaDistribuicaoId==null)  GuiaDistribuicaoId= new GuiaDistribuicaoBean() ;	  	
      		//Carrego os Beans		
		  GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
      	  GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
		  GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
		  GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
		  GuiaDistribuicaoId.setMsgErro("");
		  String acao    = req.getParameter("acao");   
		  if (acao==null)	{
			GuiaDistribuicaoId         = new GuiaDistribuicaoBean() ;
			GuiaDistribuicaoId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));        	  
			GuiaDistribuicaoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			GuiaDistribuicaoId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
			GuiaDistribuicaoId.setNomUserName(UsrLogado.getNomUserName());		
			GuiaDistribuicaoId.setMsgErro("");	
		  	GuiaDistribuicaoId.setCodStatus("4");
			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
		  	req.setAttribute("atas",GuiaDistribuicaoId.consultaAtasPublicar(UsrLogado));
		  	acao = " ";
		  }

		  if (acao.equals("LeATA")) {					
			GuiaDistribuicaoId.setDatPubliPDO(req.getParameter("datPubliPDO"));
			GuiaDistribuicaoId.setCodEnvioDO(req.getParameter("atas"));
			Vector vErro = new Vector();
			if (GuiaDistribuicaoId.getDatPubliPDO().length()==0) 
				vErro.addElement("Data de Publica��o n�o preenchida. \n") ;
			
			/*/////////////////////////////--ATEN��O--///////////////////////////////////
			 * Criticas relativas a data de publica��o s�o controladas pelos par�metros 
			 * por sistema: 
			 * Nome: NUM_DIAS_ANTERIOR_PUBDO e NUM_DIAS_SUPERIOR_PUBDO
			 * Valor DEFAULT (respectivamente): -180 e 60
			 * C�d. Sistema: 45 (M�dulo Recurso)
			 * Descri��o: CONTROLE DE N�MEROS DE DIAS PARA CRITICA DE DATA 
			 * DE PUBLICA��O ANTERIOR E SUPERIOR.  
			 * Essa critica de datas existe para evitar que o auto fique parado, ou seja,
			 * n�o prospere. Caso n�o tenha essa critica o auto pode entrar em efeito 
			 * suspensivo ou ser cancelado. Data do coment�rio: 25/10/2006
			 */////////////////////////////////////////////////////////////////////////*/ 
			 ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
             param.setCodSistema("45"); //M�DULO RECURSO
             param.PreparaParam();
			 String dtbase = sys.Util.addData(sys.Util.formatedToday().substring(0,10),Integer.parseInt(param.getParamSist("NUM_DIAS_ANTERIOR_PUBDO"))) ;			
			 if (sys.Util.DifereDias(GuiaDistribuicaoId.getDatPubliPDO(),dtbase)>0) 
				vErro.addElement("Data de Publica��o n�o pode ser anterior a "+dtbase+". \n") ;
			 dtbase = sys.Util.addData(sys.Util.formatedToday().substring(0,10),Integer.parseInt(param.getParamSist("NUM_DIAS_SUPERIOR_PUBDO"))) ;			
			 if (sys.Util.DifereDias(dtbase,GuiaDistribuicaoId.getDatPubliPDO())>0) 
				vErro.addElement("Data de Publica��o n�o pode ser superior a "+dtbase+". \n") ;
             
			 /*//////////////////////// - FIM : 25/10/2006 - //////////////////////// */		
			
			 if(GuiaDistribuicaoId.getCodEnvioDO().length()==0)	vErro.addElement("Nenhuma ATA selecionada.");
			 GuiaDistribuicaoId.setMsgErro(vErro);
			 if (GuiaDistribuicaoId.getMsgErro().length()==0){
				Vector guias = GuiaDistribuicaoId.LeGuiasAta();				
		    	List lGuias = new ArrayList();
		    	GuiaDistribuicaoBean auxGuia = null ;		    	
				for(int i=0;i<guias.size();i++){
					auxGuia = new GuiaDistribuicaoBean();
					auxGuia.setNumGuia((String)guias.get(i));
					auxGuia.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
					auxGuia.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
					auxGuia.setNomUserName(UsrLogado.getNomUserName());
					auxGuia.LeGuia();
					auxGuia.setDatPubliPDO(GuiaDistribuicaoId.getDatPubliPDO());					
					lGuias.add(auxGuia);
				}
				GuiaDistribuicaoId.setDatEnvioDO(auxGuia.getDatEnvioDO()) ;				
				if (lGuias.size()==0) { 						
					GuiaDistribuicaoId.setMsgErro("Nenhuma Guia localizada.");
				  	req.setAttribute("atas",GuiaDistribuicaoId.consultaAtasPublicar(UsrLogado));					
				}
				else {	
					GuiaDistribuicaoId.setAutos(lGuias);
					nextRetorno = "/REC/GuiaInformaPublicacaoConfirma.jsp";
				}
				req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO") ;
		    }
			else
			  	req.setAttribute("atas",GuiaDistribuicaoId.consultaAtasPublicar(UsrLogado));	
		   }			
		   if (acao.equals("AtualizaPublicacao")) {  
		    if(AtualizaPublicacaoDO(GuiaDistribuicaoId))	req.setAttribute("flag","S");
		    else		    	req.setAttribute("flag","N");
		    req.setAttribute("numGuias",req.getParameter("numGuias"));
		    req.setAttribute("numReqs",req.getParameter("numReqs"));
	    	nextRetorno = "/REC/GuiaInformaPublicacaoConfirma.jsp";				
		   }			
		  if(acao.equals("Imprime")){
		  	nextRetorno = "/REC/GuiaPublicaAtaImp.jsp";	
		  }
		  if (acao.equals("R")) session.removeAttribute("GuiaDistribuicaoId") ;
	      else 				    session.setAttribute("GuiaDistribuicaoId",GuiaDistribuicaoId) ;			
      }
      catch (Exception ue) {
            throw new sys.CommandException("GuiaInformaPublicacaoDOCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }

public boolean AtualizaPublicacaoDO(GuiaDistribuicaoBean GuiaDistribuicaoId) throws sys.CommandException {
   	GuiaDistribuicaoId.setMsgErro("");
   	try {	
   		GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
   		if(dao.GuiaInformaPublicacaoDO(GuiaDistribuicaoId) == 0) {
   			GuiaDistribuicaoId.setMsgErro("Nenhuma Guia atualizada. \n"+
   					GuiaDistribuicaoId.getMsgErro()) ;   			
   			return false;
   		}
   	}
   	catch (Exception ue) {
   		throw new sys.CommandException("GuiaInformaPublicacaoDOCmd: " + ue.getMessage());
   	}
   	return true;
} 	

   
}
