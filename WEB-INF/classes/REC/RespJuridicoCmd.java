package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;


/**
* <b>Title:</b>        Controle de Acesso - Manutencao de PerfisSistema<br>
* <b>Description:</b>  Comando para Inclui/Alterar/Excluir/Consultar e Listar Relator<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author 			   Luciana Rocha	
* @version 1.0
*/

public class RespJuridicoCmd extends sys.Command {


  private static final String jspPadrao="/REC/RespJuridico.jsp";    
  private String next;

  public RespJuridicoCmd() {
    next = jspPadrao;
  }

  public RespJuridicoCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = jspPadrao ;
    try {      
		// cria os Beans do org�o, se n�o existir
		ACSS.OrgaoBean OrgaoId = (ACSS.OrgaoBean)req.getAttribute("OrgaoId") ;
		if(OrgaoId == null)
		  OrgaoId = new ACSS.OrgaoBean() ;	 
		
		RespJuridicoBean RespJuridicoId = (RespJuridicoBean)req.getAttribute("RespJuridicoId") ;
		if(RespJuridicoId == null)
			RespJuridicoId = new RespJuridicoBean() ;	
							
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null)  acao =" ";   
		  
	    String codOrgao = req.getParameter("codOrgao"); 			
		if(codOrgao == null) codOrgao ="";  
		
		String atualizarDependente = "N";
		
	    OrgaoId.Le_Orgao(codOrgao,0) ;
	    RespJuridicoId.setCodOrgao(codOrgao);
			 
	    if(acao.compareTo("buscaRespJuridico")==0) {
		  List respJuridico = RespJuridicoId.getRespParecer(10,5);
		  atualizarDependente ="S"; 	
		  req.setAttribute("RespJuridico",respJuridico);
	    }
					 
	    if(acao.compareTo("A") == 0){
			String[] codRespJuridico = req.getParameterValues("codRespJuridico");
			  
			String[] nomRespParecer = req.getParameterValues("nomRespParecer");  
			String[] nomRespParecer_aux = req.getParameterValues("nomRespParecer_aux"); 

			String[] numCpf = req.getParameterValues("numCpf");
			String[] numCpf_aux = req.getParameterValues("numCpf_aux");     
			  
			Vector vErro = new Vector(); 
			vErro = isParametros(codRespJuridico,nomRespParecer,numCpf) ;
						 
            if (vErro.size()==0) {
				List respJuridico 	  = new ArrayList(); 
				List respJuridico_aux = new ArrayList();
				RespJuridicoBean myRespJuridico; 
				RespJuridicoBean myRespJuridico_aux; 
				for (int i = 0; i < codRespJuridico.length;i++) {
					myRespJuridico 	  = new RespJuridicoBean();	
					myRespJuridico_aux = new RespJuridicoBean();	
					myRespJuridico.setCodOrgao(codOrgao);
					myRespJuridico_aux.setCodOrgao(codOrgao);
					myRespJuridico.setCodRespParecer(codRespJuridico[i]);
					myRespJuridico_aux.setCodRespParecer(codRespJuridico[i]);
					myRespJuridico.setNomRespParecer(nomRespParecer[i]);	
					myRespJuridico_aux.setNomRespParecer(nomRespParecer_aux[i]);	  
				    myRespJuridico.setNumCpfEdt(numCpf[i]);		
				    myRespJuridico_aux.setNumCpfEdt(numCpf_aux[i]);	
				    respJuridico.add(myRespJuridico); 	
				    respJuridico_aux.add(myRespJuridico_aux);
				}
				if( RespJuridicoId.isInsertRelator(respJuridico, respJuridico_aux) )
					RespJuridicoId.setMsgErro("Dados Alterados com Sucesso");
				atualizarDependente ="S"; 
				req.setAttribute("RespJuridico",respJuridico);
			}
			else RespJuridicoId.setMsgErro(vErro) ;  	      
	    }
	    
	    if(acao.compareTo("I") == 0){
			List respJuridico = RespJuridicoId.getRespParecer(0,0);
			req.setAttribute("respJuridico",respJuridico);
			nextRetorno  = "/REC/RespJuridicoImp.jsp";
	    }
	    
	    req.setAttribute("atualizarDependente",atualizarDependente);
		req.setAttribute("OrgaoId",OrgaoId) ;		 
		req.setAttribute("RespJuridicoId",RespJuridicoId) ;
    }
    catch (Exception e) {
      throw new sys.CommandException("RespJuridicoCmd 001: " + e.getMessage());
    }
	return nextRetorno;
  }
  
 
  private Vector isParametros(String[] codRelator,String[] nomRelator,String[] numCpf) {
		Vector vErro = new Vector() ;		 
		if ((codRelator.length!=nomRelator.length) || (nomRelator.length!=numCpf.length)) vErro.addElement("Parametros inconsistentes") ;
		return vErro ;
  } 

}