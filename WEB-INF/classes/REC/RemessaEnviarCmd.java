package REC;

import java.util.ArrayList;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import sys.BeanException;
import sys.DaoException;

public class RemessaEnviarCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/RemessaEnviar.jsp" ;  
   
   public RemessaEnviarCmd() {
      next = jspPadrao;
   }

   public RemessaEnviarCmd(String next) {
      this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	  String nextRetorno = next ;
      try {     
      	  // cria os Beans de sessao, se n�o existir
    	  HttpSession session   = req.getSession() ;								
    	  ACSS.UsuarioBean 	UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
    	  if (UsrLogado==null) 
    	  	UsrLogado = new ACSS.UsuarioBean() ;	  
    	  
    	  ACSS.UsuarioFuncBean  UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
    	  if (UsuarioFuncBeanId==null)  
    	  	UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;
    	  
    	  ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
    	  if (ParamOrgaoId==null) 
    	  	ParamOrgaoId = new ParamOrgBean() ;	
    	  
    	  RemessaBean RemessaId = (RemessaBean)session.getAttribute("RemessaId") ;
    	  if (RemessaId==null) 
    	  	RemessaId = new RemessaBean() ;	  	
    	  
    	  //Carrega os Beans		
    	  RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
    	  RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
    	  RemessaId.setUsername(UsrLogado.getNomUserName());	
    	  String indRecurso = UsuarioFuncBeanId.getJ_sigFuncao();		 
    	  
    	  String acao = req.getParameter("acao");   
    	  if (acao==null)	
    	  {
    		  session.removeAttribute("RemessaId") ;
    		  acao = " ";
    	  }
    	  else {
    		  if ("Classifica,Voltar,MostraAuto,imprimirInf,AtualizaEnvio,ImprimirGuia,MostraAI".indexOf(acao)<0)	{
    			  sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
    			  cmd.setNext(this.next) ;
    			  nextRetorno = cmd.execute(req);
    		  }		
    	  }
    	  
    	  if ( (acao.equals(" ")) || (acao.equals("Novo")) ){
    		  RemessaId = new RemessaBean() ;	
    		  RemessaId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
    		  RemessaId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
    		  RemessaId.setCodOrgaoLotacao(UsrLogado.getOrgao().getCodOrgao());
    		  RemessaId.setUsername(UsrLogado.getNomUserName());
    		  if("REC0127,REC0128,REC0129".indexOf(indRecurso)>0)
    		     RemessaId.setDatInicio(ParamOrgaoId.getParamOrgao("DAT_INICIO_REMESSA","","7"));
    		  else 
    		  	 RemessaId.setDatInicio(ParamOrgaoId.getParamOrgao("DAT_INICIO_REMESSA_CEDOC","","7"));
    		  RemessaId.setStatusRemessa("0");
    		  
              /* Informa a propriedade que indica o Tipo de Recurso */
    		  //Guia de Remessa
    		  if  ("REC0127".equals(indRecurso)) 
    			RemessaId.setIndRecurso("5");//**> Defesa Previa <**
			  else if  ("REC0128".equals(indRecurso)) 
			    RemessaId.setIndRecurso("15");//**> 1� Instancia <**
			  else if  ("REC0129".equals(indRecurso)) 
			    RemessaId.setIndRecurso("35");//**> 2� Instancia <**
			  
			  //Guia de Remessa CEDOC
    		  else if  ("REC0135".equals(indRecurso)) 
    			RemessaId.setIndRecurso("98");//**> Defesa Previa <**
			  else if  ("REC0136".equals(indRecurso)) 
			    RemessaId.setIndRecurso("96");//**> 1� Instancia <**
			  else if  ("REC0137".equals(indRecurso)) 
			    RemessaId.setIndRecurso("97");//**> 2� Instancia <**
			    
    		  RemessaId.PreparaRemessa(UsrLogado);	
    		  if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","NENHUM AUTO/PROCESSO LOCALIZADO. ") ;
    	  }		
    	  
    	  if (acao.equals("Classifica")) RemessaId.Classifica(req.getParameter("ordem"))	;
    	  
    	  if (acao.equals("AtualizaEnvio")) 
    	  {  
    		  int inc = AtualizaEnvio(req,RemessaId,UsrLogado) ;
    		  if (inc>0) {
    			  RemessaId.setMsgErro("Incluidos "+inc+" Processos/Requerimentos para Guia de Remessa "+RemessaId.getCodRemessa()); 			
    			  nextRetorno = "/REC/RemessaMostra.jsp" ;
    		  }
    		  else {
    			  RemessaId.setMsgErro(RemessaId.getMsgErro()+" \n Nenhum Processos/Requerimentos incluido ");
    			  nextRetorno = "/REC/RemessaMostra.jsp" ; 
    		  }				
    	  }
    	  
    	  if  (acao.equals("ImprimirRemessa"))  
    	  {	      	  
    		  if (RemessaId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS SELECIONADOS");				  	    
    		  nextRetorno = "/REC/RemessaPreparaImp.jsp" ;      	  
    	  }
    	  
    	  if (acao.equals("MostraAuto"))  
    	  {
    		  AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean();	  			  			
    		  if (acao.equals("MostraAuto")) { 
    			  AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca")); 
    			  AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));
    			  AutoInfracaoBeanId.LeRequerimento(UsrLogado);
    			  nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
    		  }
    		  else {
    			  AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
    			  AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));			
    			  nextRetorno = "/REC/AutoImp.jsp" ;
    		  }		
    		  
    		  // Verifica se o Usuario logado ve Todos os Orgaos
    		  String temp = UsrLogado.getCodOrgaoAtuacao();
    		  if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999"); 
    		  AutoInfracaoBeanId.LeAutoInfracao(UsrLogado)	;
    		  UsrLogado.setCodOrgaoAtuacao(temp);	
    		  req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
    	  }		
    	  
    	  if (acao.equals("MostraAI"))  
    	  {			
    		  AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	  			  			
    		  myAuto.setNumPlaca(req.getParameter("mostraplaca")); 
    		  myAuto.setNumAutoInfracao(req.getParameter("mostranumauto"));
    		  myAuto.LeAIDigitalizado();
    		  req.setAttribute("AutoInfracaoBeanId",myAuto) ;
    		  nextRetorno = "/REC/VisAIDig.jsp";
    	  }
    	  
    	  if (acao.equals("R")) session.removeAttribute("RemessaId") ;
    	  else session.setAttribute("RemessaId",RemessaId) ;
      }
      catch (Exception ue) {
    	  throw new sys.CommandException("RemessaEnviarCmd: " + ue.getMessage());
      }
      return nextRetorno;
   }
   
   public int AtualizaEnvio(HttpServletRequest req,RemessaBean RemessaId,
	   ACSS.UsuarioBean UsrLogado) throws sys.CommandException, DaoException, BeanException {
	   int inc = 0 ;
	   Vector vErro = new Vector() ;
       // buscar os autos selecionados
	   String procSelec[] = req.getParameterValues("Selecionado");
	   int n =0 ;				
	   ArrayList autosSelec = new ArrayList();			
	   if (procSelec!=null)	{
		   for (int i=0;  i<procSelec.length; i++) {					
			   n = Integer.parseInt(procSelec[i]) ;	
			   if ((n>=0) && (n<RemessaId.getAutos().size())) { 
				   autosSelec.add((AutoInfracaoBean)RemessaId.getAutos().get(n));
			   }
		   }
		   RemessaId.setAutos(autosSelec);
	   }
	   else vErro.addElement("Nenhum processo selecionado.") ;
	   
	   if ((vErro.size()==0) && (RemessaId.getAutos().size()==0)) vErro.addElement("Nenhum processo selecionado.") ;
	   
	   if (vErro.size()>0) RemessaId.setMsgErro(vErro);
	   else{	 
		   GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
		   inc = dao.RemessaGrava(RemessaId) ; 
	   }	
	   
	   return inc;   
   }
}
	  
   	   
  
