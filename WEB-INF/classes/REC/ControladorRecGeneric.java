package REC;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ACSS.Ata;
import ACSS.Lote;
import UTIL.Convert;
import UTIL.DataUtils;
import UTIL.TipoRecGeneric;

public class ControladorRecGeneric extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static String codOrgao = "";

	public ControladorRecGeneric() {
		super();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int tipoRec = Integer.parseInt(request.getParameter("tipoRec"));

		switch (tipoRec) {
		case TipoRecGeneric.MONTAR_RELATOR:
			montarRelator(request, response);
			break;
		case TipoRecGeneric.LISTA_PROCESSOS:
			listarProcessos(request, response);
			break;
		case TipoRecGeneric.MONTAR_RELATOR_ABRIR_PROCESSO:
			montarRelatorAbrirProcesso(request, response);
			break;
		case TipoRecGeneric.VALIDA_ABRIR_PROCESSO_REQUERIMENTO:
			verificaRequerimento(request, response);
			break;
		case TipoRecGeneric.ATA_PUBLICACAO:
			getAtaPublicacao(request, response);
			break;
		case TipoRecGeneric.CONSULTAR_PROCESSOS_SEM_ATA:
			consultarProcessosSemAta(request, response);
			break;
		case TipoRecGeneric.CONSULTAR_PROCESSOS_COM_ATA:
			consultarProcessosComAta(request, response);
			break;
		case TipoRecGeneric.CONSULTAR_ATA:
			consultarAtas(request, response);
			break;
		case TipoRecGeneric.ALTERAR_ATA_PROCESSO:
			alterarAtaProcesso(request, response);
			break;
		case TipoRecGeneric.LISTA_AUTO_LOTE:
			listaAutoLote(request, response);
			break;
		case TipoRecGeneric.PROCESSA_AUTO_LOTE:
			processaAutoLote(request, response);
			break;
		case TipoRecGeneric.REQUERIMENTO_POR_AUTO:
			consultarRequerimentosPorAuto(request, response);
			break;
		case TipoRecGeneric.GERENCIAR_ATA_CONSULTAR_ATAS:
			consultarAtasPorStatus(request, response);
			break;
		case TipoRecGeneric.ABRIR_PROCESSO_CONSULTAR_AUTOS:
			abrirProcessoConsultarAutos(request, response);
			break;
		case TipoRecGeneric.ABRIR_PROCESSO_ABRIR_REQUERIMENTO:
			new GerenciarAbrirProcesso().abrirRequerimento(request, response);
			break;
		case TipoRecGeneric.ALTERAR_DATA_REQUERIMENTO:
			alterarDataRequerimento(request, response);
			break;
		case TipoRecGeneric.DELETAR_REQUERIMENTO:
			deletarRequerimento(request, response);
			break;
		case TipoRecGeneric.INFORMAR_RESULTADO:
			informarResultado(request, response);
			break;
		}
	}

	/**
	 * Retorna via response um ...... /* Legenda: * 1 = Def. Previa; * 2 = 1�
	 * INST�NCIA; * 3 = 2� INST�NCIA;
	 */
	public void verificaRequerimento(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String tpRequerimento = request.getParameter("tpRequerimento");
		String numAuto = request.getParameter("numAutoInfraca");
		String resp = null;
		String respMsg = null;

		if (tpRequerimento.equals("1")) {
			if (!validaStatusRequerimentoAuto(tpRequerimento, numAuto)) {
				resp = "INVALIDO";
				respMsg = "J� FOI ATRIBUIDO Def. Previa para o Auto: " + numAuto;
				response.setContentType("text/html");
				response.getWriter().write(resp + "&" + respMsg);
			} else {
				// resp ="valido";
				resp = "";
				response.setContentType("text/html");
				response.getWriter().write(resp);
			}
		} else if (tpRequerimento.equals("2")) {
			if (!validaStatusRequerimentoAuto(tpRequerimento, numAuto)) {
				resp = "INVALIDO";
				respMsg = " J� FOI ATRIBUIDO 1� INST�NCIA para o Auto: " + numAuto;
				response.setContentType("text/html");
				response.getWriter().write(resp + "&" + respMsg);
			} else {
				// resp ="valido";
				resp = "";
				response.setContentType("text/html");
				response.getWriter().write(resp);
			}
		} else if (tpRequerimento.equals("3")) {
			if (!validaStatusRequerimentoAuto(tpRequerimento, numAuto)) {
				resp = "INVALIDO";
				respMsg = " J� FOI ATRIBUIDO 2� INST�NCIA para o Auto: " + numAuto;
				response.setContentType("text/html");
				response.getWriter().write(resp + "&" + respMsg);
			} else {
				// resp ="valido";
				resp = "";
				response.setContentType("text/html");
				response.getWriter().write(resp);
			}
		} else {
			// resp ="valido";
			resp = "";
			response.setContentType("text/html");
			response.getWriter().write(resp);
		}
	}

	public boolean validaStatusRequerimentoAuto(String tpRequerimento, String numAuto) {
		boolean returno = false;
		try {
			Ata ata = new Ata();
			returno = ata.validaStatusRequerimentoAuto(tpRequerimento, numAuto);
			return returno;
		} catch (Exception e) {

		}
		return returno;
	}

	public void listarProcessos(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// String codAta = request.getParameter("codAta");
		codOrgao = request.getParameter("codOrgao");

		Ata newAta = new Ata();
		ArrayList<HashMap<String, String>> semAtribuicao = newAta.getProcessoSemAtribuicaoMapList();
		ArrayList<HashMap<String, String>> comAtribuicao = newAta.getProcessoComAtribuicaoMapList();

		JSONArray listaRequerimentos = new JSONArray();
		JSONArray listaSem = new JSONArray();
		JSONArray listaCom = new JSONArray();

		listaRequerimentos.add(listaSem);
		listaRequerimentos.add(listaCom);

		try {
			if (semAtribuicao.size() > 0) {
				for (HashMap<String, String> row : semAtribuicao) {
					JSONObject jsonObj = new JSONObject();

					jsonObj.put("cod_requerimento", row.get("cod_requerimento"));
					jsonObj.put("num_processo", row.get("num_processo"));
					jsonObj.put("dat_processo", dateToDateBr(row.get("dat_processo")));
					jsonObj.put("ano", row.get("ano"));
					listaSem.add(jsonObj);
				}
			}

			if (comAtribuicao.size() > 0) {
				for (HashMap<String, String> row : comAtribuicao) {
					JSONObject jsonObj = new JSONObject();

					jsonObj.put("cod_requerimento", row.get("cod_requerimento"));
					jsonObj.put("num_processo", row.get("num_processo"));
					jsonObj.put("dat_processo", dateToDateBr(row.get("dat_processo")));
					jsonObj.put("nom_relator", row.get("nom_relator"));
					jsonObj.put("ano", row.get("ano"));
					listaCom.add(jsonObj);
				}
			}
		} catch (Exception ex) {

		}

		response.setContentType("application/json");
		response.getWriter().print(listaRequerimentos);
	}

	/**
	 * Retorna via response um controle HTML "select" populado com uma lista de
	 * relatores.
	 */
	public void montarRelator(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String junta = request.getParameter("valor");
		String seletor = request.getParameter("seletor");
		String resp = null;

		HttpSession session = request.getSession();
		REC.ParamOrgBean pob = (REC.ParamOrgBean) session.getAttribute("ParamOrgBeanId");

		resp = new Ata().montarRelator(seletor, junta, pob.getCodOrgao(),
				session.getAttribute("codNatureza").toString());

		response.setContentType("text/html");
		response.getWriter().write(resp);
	}

	/**
	 * Converte uma string para uma data BR.
	 * 
	 * @param data
	 *            string da data para ser formatada.
	 * @return String
	 */
	public static String dateToDateBr(String data) {
		if (data != null && data != "") {
			try {
				SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date dt = new Date(dtFormat.parse(data).getTime());
				SimpleDateFormat dtFinal = new SimpleDateFormat("dd/MM/yyyy");

				return dtFinal.format(dt);
			} catch (Exception ex) {
				return data;
			}
		} else {
			return data;
		}
	}

	/**
	 * Retorna via response um controle HTML "select" populado com uma lista de
	 * relatores caso a ata selecionada tem disponibilidade de numero(processo).
	 */
	public void montarRelatorAbrirProcesso(HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		String junta = request.getParameter("valor");
		String seletor = request.getParameter("seletor");
		String dsAta = request.getParameter("idAta");
		int codAta = 0;
		String resp = null;
		String respMsg = null;

		try {
			Ata att = new Ata();
			codAta = att.getCodAta(dsAta);
		} catch (Exception e) {
		}

		HttpSession session = request.getSession();
		REC.ParamOrgBean pob = (REC.ParamOrgBean) session.getAttribute("ParamOrgBeanId");

		if (new Ata().getVerificaQtdeNrProcesso(String.valueOf(codAta)).equals("N_OK_ATEN��O")) {
			respMsg = "N�O EXISTE QTDE DE PROCESSOS DISPONIVEIS PARA ATA, DESEJA INCLUIR UM NOVO PROCESSO NESSA ATA?";
			resp = new Ata().montarRelator(seletor, junta, pob.getCodOrgao(),
					session.getAttribute("codNatureza").toString());
			response.setContentType("text/html");
			response.getWriter().write(resp + "&" + respMsg);
		} else {
			resp = new Ata().montarRelator(seletor, junta, pob.getCodOrgao(),
					session.getAttribute("codNatureza").toString());
			response.setContentType("text/html");
			response.getWriter().write(resp);
		}
	}

	public void getAtaPublicacao(HttpServletRequest request, HttpServletResponse response) {
		final String year = (String) request.getParameter("year");
		final String month = (String) request.getParameter("month");
		final String tipoAta = (String) request.getParameter("tipoAta");
		final String codOrgao = request.getParameter("codOrgao");
		final String cdNatureza = request.getParameter("cdNatureza");

		try {
			ArrayList<Ata> listAta = new ArrayList<Ata>();
			listAta = new Ata().getAtasPublicacao(tipoAta, year, month, codOrgao, cdNatureza);
			JSONArray jsonList = new JSONArray();

			for (Ata ata : listAta) {
				JSONObject obj = new JSONObject();
				obj.put("codAta", ata.getCodAta());
				obj.put("descAta", ata.getDescAta());

				jsonList.add(obj);
			}

			response.setContentType("application/json");
			PrintWriter out = response.getWriter();

			out.print(jsonList);
		} catch (Exception e) {

		}
	}

	public void consultarProcessosSemAta(HttpServletRequest request, HttpServletResponse response) {
		try {
			String codOrgao = request.getParameter("codOrgao");
			String tipoAta = request.getParameter("tipoAta");
			String tipoAtaConvertido = tipoAta;
			int cdNatureza = Integer.parseInt(request.getParameter("cdNatureza"));

			switch (tipoAta) {
			case "Def. Previa":
				tipoAtaConvertido = "DP";
				break;
			case "1� INST�NCIA":
				tipoAtaConvertido = "1P";
				break;
			case "2� INST�NCIA":
				tipoAtaConvertido = "2P";
				break;
			default:
				break;
			}

			ArrayList<HashMap<String, String>> processos = new AutoInfracaoBean().consultarProcessosSemAta(codOrgao,
					tipoAtaConvertido, cdNatureza);
			JSONArray arrayJson = Convert.arrayListToJsonArray(processos);

			response.setContentType("application/json");
			response.getWriter().print(arrayJson);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE ControladorRecReneric M�TODO: consultarProcessosSemAta().");
		}
	}

	public void consultarProcessosComAta(HttpServletRequest request, HttpServletResponse response) {
		try {
			String codOrgao = request.getParameter("codOrgao");
			String codAta = request.getParameter("codAta");
			ArrayList<HashMap<String, String>> processos = new AutoInfracaoBean().consultarProcessosComAta(codOrgao,
					codAta);
			JSONArray arrayJson = Convert.arrayListToJsonArray(processos);

			response.setContentType("application/json");
			response.getWriter().print(arrayJson);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE ControladorRecReneric M�TODO: consultarProcessosComAta().");
		}
	}

	public void consultarAtas(HttpServletRequest request, HttpServletResponse response) {
		try {
			String tipoAta = request.getParameter("tipoAta");
			String codOrgao = request.getParameter("codOrgao");
			String cdNatureza = request.getParameter("cdNatureza");
			ArrayList<HashMap<String, String>> atas = new Ata().consultarAtas(tipoAta, codOrgao, cdNatureza);
			JSONArray jsonAtas = Convert.arrayListToJsonArray(atas);

			response.setContentType("application/json");
			response.getWriter().print(jsonAtas);
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE ControladorRecReneric M�TODO: consultarProcessosComAta().");
		}
	}

	public void alterarAtaProcesso(HttpServletRequest request, HttpServletResponse response) {
		String tipoAlteracao = request.getParameter("tipoAlteracao");
		String codOrgao = request.getParameter("codOrgao");
		String strProcessos = request.getParameter("processos");
		ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) request.getSession().getAttribute("UsuarioBeanId");

		try {
			ArrayList<HashMap<String, String>> processos = Convert.jsonStringToArrayList(strProcessos);
			String codAta = request.getParameter("codAta");
			String desAta = request.getParameter("desAta");

			if (tipoAlteracao.equals("atribuirAta")) {
				new AutoInfracaoBean().atribuirAta(processos, codAta, desAta, codOrgao, UsrLogado.getNomUserName());
			} else if (tipoAlteracao.equals("removerAta")) {
				new AutoInfracaoBean().removerAta(processos, codOrgao, desAta, UsrLogado.getNomUserName(), codAta);
			}
		} catch (Exception ex) {
			System.out.println("ERRO CLASSE ControladorRecReneric M�TODO: alterarAtaProcesso().");
		}
	}

	public void listaAutoLote(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String codOrgao = request.getParameter("codOrgao");
		String dsLote = request.getParameter("dsLote");

		String resp = null;
		Lote lote = new Lote();
		resp = "";

		try {
			String varParanGeneric = "USA_DATA_BASE_LINK";
			if (lote.getDbLink(codOrgao, varParanGeneric)) {
				varParanGeneric = "NOME_DATA_BASE_LINK";
				String nomeDbLink = lote.getNomeDbLink(codOrgao, varParanGeneric);
				ArrayList<HashMap<String, String>> list = new ArrayList<>();
				list = lote.getLoteAuto(nomeDbLink, dsLote, "");

				JSONArray jsonAtas = Convert.arrayListToJsonArray(list);

				response.setContentType("application/json");
				response.getWriter().print(jsonAtas);
				list.clear();
			} else {
				varParanGeneric = "DONO_DA_TABELA";
				String donoTabela = lote.getDonoTabela(codOrgao, varParanGeneric);
				ArrayList<HashMap<String, String>> list = new ArrayList<>();
				list = lote.getLoteAuto("", dsLote, donoTabela);

				JSONArray jsonAtas = Convert.arrayListToJsonArray(list);

				response.setContentType("application/json");
				response.getWriter().print(jsonAtas);
				list.clear();
			}
		} catch (Exception e) {
			e.getMessage().toString();
		}
	}

	public void processaAutoLote(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String acao = request.getParameter("acao"); // EfeitoSuspensivo
		String dsAcao = request.getParameter("dsAcao");// REMOVER
		String codOrgao = request.getParameter("codOrgao");// 258470
		String strProcessos = request.getParameter("processos"); // [{"nrSerie":"NT
																	// 119601"},{"nrSerie":"NT
																	// 119603"}]

		String resp = null;
		Lote lote = new Lote();
		resp = "";
		ArrayList<HashMap<String, String>> processos = new ArrayList<>();
		try {
			if (acao.equals("EfeitoSuspensivo")) {
				processos = Convert.jsonStringToArrayList(strProcessos);
				lote.processaLoteCmd(processos, dsAcao, codOrgao);

				// JSONArray jsonAtas = Convert.arrayListToJsonArray(list);
				// response.setContentType("application/json");
				// response.getWriter().print(jsonAtas);
			}

		} catch (Exception e) {
			e.getMessage().toString();
		}
	}

	public void consultarRequerimentosPorAuto(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");
		JSONArray jsonResult = null;

		try {
			int tipoRec = Integer.parseInt(request.getParameter("tipoRec"));

			if (tipoRec == TipoRecGeneric.REQUERIMENTO_POR_AUTO) {
				String numAuto = request.getParameter("numAuto");
				String numProcesso = request.getParameter("numProcesso");
				int cdNatureza = Integer.parseInt(request.getParameter("cdNatureza"));

				numAuto = numAuto.trim().toUpperCase();
				numProcesso = numProcesso.trim().toUpperCase();

				AutoInfracaoBean auto = null;

				if (!numAuto.equals(""))
					auto = AutoInfracaoBean.consultarAutoPor(numAuto, cdNatureza);
				else if (!numProcesso.equals(""))
					auto = AutoInfracaoBean.consultarAutoPorNumProcesso(numProcesso, cdNatureza);

				if (auto != null) {
					List<HashMap<String, String>> listaRequerimentos = RequerimentoBean
							.consultarRequerimentosPorAuto(auto.getCodAutoInfracao());

					if (listaRequerimentos != null)
						jsonResult = Convert.arrayListToJsonArray(listaRequerimentos);
				}
			}

			response.getWriter().print(jsonResult);
		} catch (Exception ex) {

		}
	}

	@SuppressWarnings("unchecked")
	public void consultarAtasPorStatus(HttpServletRequest request, HttpServletResponse response) {
		try {
			String statusAta = request.getParameter("statusAta");
			String cdNatureza = request.getParameter("cdNatureza");
			List<Ata> listaAtas = Ata.consultarAtasPorStatus(statusAta, cdNatureza);
			JSONArray jsonResult = new JSONArray();

			for (Ata ata : listaAtas) {
				JSONObject obj = new JSONObject();

				obj.put("COD_ATA", ata.getCodAta());
				obj.put("TIPO_ATA", ata.getTpAta());
				obj.put("DES_ATA", ata.getDescAta());
				obj.put("DT_SESSAO", ata.getDataSessao());
				obj.put("DT_DISTRIBUICAO", ata.getDataDistribuicao());
				obj.put("JUNTA_ATA", ata.getJuntaAta());
				obj.put("DT_AVISO", ata.getDataAviso());
				obj.put("STATUS", ata.getStatusAta());
				obj.put("QTDE_PROCESSO", ata.getQtdeAtaProcesso());
				obj.put("NR_ATA", ata.getNrAta());
				obj.put("ANO_ATA", ata.getAnoAta());
				obj.put("DAT_INCLUSAO", ata.getDataInclusao());
				obj.put("COD_ORGAO", ata.getCodOrgao());
				obj.put("DATA_PUBLICACAO", ata.getDataPublicacao());
				obj.put("CD_NATUREZA", ata.getCdNatureza());

				jsonResult.add(obj);
			}

			response.setContentType("application/json");
			response.getWriter().print(jsonResult);
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}
	}

	public void abrirProcessoConsultarAutos(HttpServletRequest request, HttpServletResponse response) {
		new GerenciarAbrirProcesso().consultarAuto(request, response);
	}

	public void alterarDataRequerimento(HttpServletRequest request, HttpServletResponse response) {
		try {
			String codRequerimento = request.getParameter("codRequerimento");
			String novaData = request.getParameter("novaData");
			RequerimentoBean requerimento = RequerimentoBean.consultarRequerimentoPor(codRequerimento);

			if (!DataUtils.validarDataBR(novaData)) {
				response.sendError(1);

				return;
			} else if (requerimento == null) {
				response.sendError(2);

				return;
			}

			requerimento.setDatRequerimento(novaData);
			requerimento.atualizarDataRequerimento();
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}
	}

	public void deletarRequerimento(HttpServletRequest request, HttpServletResponse response) {
		try {
			String codRequerimento = request.getParameter("codRequerimento");
			int codNatureza = Integer.parseInt(request.getParameter("codNatureza"));
			RequerimentoBean requerimento = RequerimentoBean.consultarRequerimentoPor(codRequerimento);

			if (requerimento == null) {
				response.sendError(1);

				return;
			} else if (!requerimento.getCodResultRS().isEmpty()) {
				response.sendError(2);

				return;
			}

			requerimento.deletar();

			AutoInfracaoBean auto = AutoInfracaoBean
					.consultarAutoPor(Integer.parseInt(requerimento.getCodAutoInfracao()));
			List<HashMap<String, String>> listRequerimentos = RequerimentoBean
					.consultarRequerimentosPorAuto(requerimento.getCodAutoInfracao());

			if (requerimento.getCodTipoSolic().equals("DP")) {
				auto.setCodStatus("0");

				if (listRequerimentos.size() == 0)
					auto.setNumProcesso("");
			} else if (requerimento.getCodTipoSolic().equals("1P")) {
				if (listRequerimentos.size() < 1) {
					auto.setCodStatus(AutoInfracaoBean.STATUS_REGISTRADO);
					auto.setNumProcesso("");
				} else {
					for (int i = 0; i < listRequerimentos.size(); i++) {
						if (listRequerimentos.get(i).get("tipoRequerimento").equals("DP")) {
							auto.setCodStatus(AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA);

							break;
						}
					}
				}
			} else if (requerimento.getCodTipoSolic().equals("2P")) {
				for (int i = 0; i < listRequerimentos.size(); i++) {
					if (listRequerimentos.get(i).get("tipoRequerimento").equals("1P")) {
						auto.setCodStatus(AutoInfracaoBean.STATUS_AGUARDANDO_2P);

						break;
					}
				}

				for (int i = 0; i < listRequerimentos.size(); i++) {
					if (listRequerimentos.get(i).get("tipoRequerimento").equals("DP")) {
						auto.setCodStatus(AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA);

						break;
					}
				}
			} else if (requerimento.getCodTipoSolic().equals("PAE")) {
				for (int i = 0; i < listRequerimentos.size(); i++) {
					if (listRequerimentos.get(i).get("tipoRequerimento").equals("2P")) {
						auto.setCodStatus(AutoInfracaoBean.STATUS_RECURSO_2P);

						break;
					}
				}

				for (int i = 0; i < listRequerimentos.size(); i++) {
					if (listRequerimentos.get(i).get("tipoRequerimento").equals("1P")) {
						auto.setCodStatus(AutoInfracaoBean.STATUS_AGUARDANDO_2P);

						break;
					}
				}

				for (int i = 0; i < listRequerimentos.size(); i++) {
					if (listRequerimentos.get(i).get("tipoRequerimento").equals("DP")) {
						auto.setCodStatus(AutoInfracaoBean.STATUS_PENALIDADE_REGISTRADA);

						break;
					}
				}
			}

			AutoInfracaoBean.atualizar(auto);
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}
	}

	public void informarResultado(HttpServletRequest request, HttpServletResponse response) {
		try {
			String[] itensMarcados = request.getParameter("itensMarcados").split(",");
			String resultado = request.getParameter("resultado");

			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) request.getSession().getAttribute("UsuarioBeanId");

			RequerimentoBean.informarResultado(itensMarcados, resultado, UsrLogado.getNomUserName());
		} catch (Exception ex) {
			System.out.println("ERROR: " + ex.getMessage());
		}
	}
}