package REC;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import ACSS.Ata;
import UTIL.DataUtils;
import sys.BeanException;

public class GerenciarAbrirProcesso {
	@SuppressWarnings("unchecked")
	public void consultarAuto(HttpServletRequest request, HttpServletResponse response) {
		try {
			String placa = request.getParameter("placa");
			String numeroAuto = request.getParameter("numeroAuto");
			String numeroProcesso = request.getParameter("numeroProcesso");
			String dataInicial = request.getParameter("dataInicial");
			String dataFinal = request.getParameter("dataFinal");
			String orgao = request.getParameter("orgao");
			String codNatureza = request.getParameter("codNatureza");

			List<AutoInfracaoBean> listaAutos = AutoInfracaoBean.consultarAutosPor(placa, numeroAuto, numeroProcesso,
					orgao, codNatureza, dataInicial, dataFinal);
			JSONArray jsonArray = new JSONArray();

			for (AutoInfracaoBean item : listaAutos) {
				JSONObject obj = new JSONObject();

				obj.put("codAuto", item.getCodAutoInfracao());
				obj.put("descricaoOrgao", item.getSigOrgao());
				obj.put("sigOrgao", item.getSigOrgao());
				obj.put("numeroAuto", item.getNumAutoInfracao());
				obj.put("placa", item.getNumPlaca());
				obj.put("descricaoAta", item.getDsAta().isEmpty() ? "N/A" : item.getDsAta());
				obj.put("numeroProcesso", item.getNumProcesso().isEmpty() ? "N/A" : item.getNumProcesso());
				obj.put("situacao", AutoInfracaoBean.getDescricaoStatusPorCodStatus(item.getCodStatus()));
				obj.put("codSituacao", item.getCodStatus());
				obj.put("dataAuto", DataUtils.dateToDateBr(item.getDatInfracao()));
				obj.put("infracao", item.getInfracao().getCodInfracao() + " - " + item.getInfracao().getDscInfracao());
				obj.put("vencimento", DataUtils.dateToDateBr(item.getDatVencimento()));
				obj.put("local", item.getDscLocalInfracao());
				obj.put("valor", item.getInfracao().getValRealEdt().replace(",", "."));
				obj.put("enquadramento", item.getInfracao().getDscEnquadramento());
				obj.put("proprietario", item.getProprietario().getNomResponsavel());
				obj.put("respPontos", item.getCondutor().getNomResponsavel().trim());
				obj.put("dataNotificacao", DataUtils.dateToDateBr(item.getDatNotificacao()));
				obj.put("numeroProcessoSujerido", new Ata().getNumProcAuto(orgao, "ULT_NR_PROCESSO_JARI"));

				jsonArray.add(obj);
			}

			response.setHeader("Content-Type", "text/plain; charset=UTF-8");
			response.getWriter().print(jsonArray);
		} catch (Exception ex) {
			ex.printStackTrace();

			response.setStatus(500);
		}
	}

	public void abrirRequerimento(HttpServletRequest request, HttpServletResponse response) {
		response.setHeader("Content-Type", "text/plain; charset=UTF-8");

		try {
			String orgao = request.getParameter("orgao");
			String codAta = request.getParameter("ata");
			String codRelator = request.getParameter("relator");
			int codAuto = Integer.parseInt(request.getParameter("codAuto"));
			int codNatureza = Integer.parseInt(request.getParameter("codNatureza"));
			String numeroNovoProcesso = request.getParameter("numeroNovoProcesso");
			String tipoRequerimento = request.getParameter("tipoRequerimento");
			String dataRequerimento = request.getParameter("dataRequerimento");
			String nomeRequerente = request.getParameter("nomeRequerente");

			ACSS.UsuarioBean usuarioLogado = (ACSS.UsuarioBean) request.getSession().getAttribute("UsuarioBeanId");
			
			AutoInfracaoBean auto = AutoInfracaoBean.consultarAutoPor(codAuto);

			if (auto == null) {
				response.setStatus(500);
				response.getWriter().print("Auto de Infra��o inv�lido.");

				return;
			}
			
			if(!auto.permitirAbrirRequerimento()) {
				response.setStatus(500);
				response.getWriter().print("N�o � poss�vel abrir um processo em um auto com este status.");

				return;
			}
			
			if(AutoInfracaoBean.verificarNumeroProcesso(numeroNovoProcesso, auto.getNumAutoInfracao())) {
				response.setStatus(500);
				response.getWriter().print("O n�mero de Processo informado j� est� em uso.");

				return;
			}

			String requerimentoValido = verificarRequerimento(String.valueOf(codAuto), tipoRequerimento);

			if (!requerimentoValido.isEmpty()) {
				response.setStatus(500);
				response.getWriter().print(requerimentoValido);

				return;
			}

			RequerimentoBean requerimento = new RequerimentoBean();

			requerimento.setCodAutoInfracao(String.valueOf(codAuto));

			Ata ata = Ata.consultarAtaPor(codAta);

			if (ata != null)
				requerimento.setCodAta(ata.getCodAta());

			if (!codRelator.isEmpty() && !codRelator.equals("SELECIONE"))
				requerimento.setCodRelatorJU(codRelator);

			requerimento.setDatRequerimento(dataRequerimento);
			requerimento.setCodOrgaoLotacaoDP(orgao);
			requerimento.setNumRequerimento("REQ_" + numeroNovoProcesso);
			requerimento.setCodTipoSolic(tipoRequerimento);
			requerimento.setCodStatusRequerimento("0");
			requerimento.setDatJU(request.getParameter("datJU"));
			requerimento.setCodOrgaoLotacaoJU(usuarioLogado.getOrgao().getCodOrgao());
			requerimento.setDatProcJU(sys.Util.formatedToday().substring(0, 10));
			requerimento.setDatRequerimento(ControladorRecGeneric.dateToDateBr(requerimento.getDatRequerimento()));
			requerimento.setDatProcDP(ControladorRecGeneric.dateToDateBr(requerimento.getDatProcDP()));
			requerimento.setDatPJ(ControladorRecGeneric.dateToDateBr(requerimento.getDatPJ()));
			requerimento.setDatProcPJ(ControladorRecGeneric.dateToDateBr(requerimento.getDatProcPJ()));
			requerimento.setDatJU(ControladorRecGeneric.dateToDateBr(requerimento.getDatJU()));
			requerimento.setDatProcJU(ControladorRecGeneric.dateToDateBr(requerimento.getDatProcJU()));
			requerimento.setDatRS(ControladorRecGeneric.dateToDateBr(requerimento.getDatRS()));
			requerimento.setDatProcRS(ControladorRecGeneric.dateToDateBr(requerimento.getDatProcRS()));
			requerimento.setDatAtuTR(ControladorRecGeneric.dateToDateBr(requerimento.getDatAtuTR()));
			requerimento.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(requerimento.getDatEnvioDO()));
			requerimento.setDatPublPDO(ControladorRecGeneric.dateToDateBr(requerimento.getDatPublPDO()));

			String codEvento = "";
			String codStatusAuto = "0";

			if (tipoRequerimento.equals("DP")) {
				codEvento = HistoricoBean.EVENTO_ABRIR_DP;
				codStatusAuto = AutoInfracaoBean.STATUS_DP;
				requerimento.setNumRequerimento(requerimento.getNumRequerimento() + "_00");
			} else if (tipoRequerimento.equals("1P")) {
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_1P;
				codStatusAuto = AutoInfracaoBean.STATUS_RECURSO_1P;
				requerimento.setNumRequerimento(requerimento.getNumRequerimento() + "_01");
			} else if (tipoRequerimento.equals("2P")) {
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_2P;
				codStatusAuto = AutoInfracaoBean.STATUS_RECURSO_2P;
				requerimento.setNumRequerimento(requerimento.getNumRequerimento() + "_02");
			} else if (tipoRequerimento.equals("PAE")) {
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_PAE;
				codStatusAuto = AutoInfracaoBean.STATUS_PAE;
				requerimento.setNumRequerimento(requerimento.getNumRequerimento() + "_03");
			}

			auto.setCodStatus(codStatusAuto);
			auto.setNumProcesso(numeroNovoProcesso);
			auto.setDatProcesso(dataRequerimento);
			
			if(auto.getProprietario().getNomResponsavel().trim().isEmpty())
				auto.getProprietario().setNomResponsavel(nomeRequerente);

			RequerimentoBean.cadastrar(requerimento);
			AutoInfracaoBean.atualizar(auto);
			HistoricoBean.cadastrarHistorico(requerimento, numeroNovoProcesso, orgao,
					ata != null ? ata.getDescAta() : "", usuarioLogado.getNomUsuario(),
					ata != null ? ata.getCodAta() : "", codEvento);
		} catch (Exception ex) {
			ex.printStackTrace();

			response.setStatus(500);
		}
	}

	private String verificarRequerimento(String codAuto, String tipoRequerimento) throws Exception {
		String mensagem = "";
		HashMap<String, String> requerimentoMesmoTipo = buscarRequerimentoPorTipo(codAuto, tipoRequerimento);
		HashMap<String, String> requerimentoParaValidacao = null;

		if (requerimentoMesmoTipo != null)
			mensagem = "J� existe um requerimento do tipo selecionado para este Auto.";
		else {
			switch (tipoRequerimento) {
				case "DP":
					if (buscarRequerimentoPorTipo(codAuto, "1P") != null)
						mensagem = "J� existe um requerimento de 1� Inst�ncia para este Auto.";
	
					break;
				case "1P":
					requerimentoParaValidacao = buscarRequerimentoPorTipo(codAuto, "DP");
	
					if (requerimentoParaValidacao != null && (requerimentoParaValidacao.get("resultado") == null || requerimentoParaValidacao.get("resultado").equals("")))
						mensagem = "Existe um requerimento de Defesa Pr�via sem resultado para este Auto.";
					else if(requerimentoParaValidacao != null && requerimentoParaValidacao.get("resultado").equals("D"))
						mensagem = "Um requerimento de Defesa Pr�via deste Auto j� foi Deferido.";
					else if (buscarRequerimentoPorTipo(codAuto, "2P") != null)
						mensagem = "J� existe um requerimento de 2� Inst�ncia para este Auto.";
	
					break;
				case "2P":
					requerimentoParaValidacao = buscarRequerimentoPorTipo(codAuto, "DP");
	
					if (requerimentoParaValidacao != null && (requerimentoParaValidacao.get("resultado") == null || requerimentoParaValidacao.get("resultado").equals("")))
						mensagem = "Existe um requerimento de Defesa Pr�via sem resultado para este Auto.";
					else if(requerimentoParaValidacao != null && requerimentoParaValidacao.get("resultado").equals("D"))
						mensagem = "Um requerimento de Defesa Pr�via deste Auto j� foi Deferido.";
					
					if(!mensagem.isEmpty())
						return mensagem;
					
					requerimentoParaValidacao = buscarRequerimentoPorTipo(codAuto, "1P");
	
					if (requerimentoParaValidacao != null && (requerimentoParaValidacao.get("resultado") == null || requerimentoParaValidacao.get("resultado").equals("")))
						mensagem = "Existe um requerimento de 1� Inst�ncia sem resultado para este Auto.";
					else if(requerimentoParaValidacao != null && requerimentoParaValidacao.get("resultado").equals("D"))
						mensagem = "Um requerimento de 1� Inst�ncia deste Auto j� foi Deferido.";
					else if(requerimentoParaValidacao == null)
						mensagem = "N�o � poss�vel abrir um requerimento de 2� Inst�ncia antes de julgar um requerimento de 1� Inst�ncia.";
	
					break;
				default:
					mensagem = "";
					
					break;
			}
		}

		return mensagem;
	}

	public HashMap<String, String> buscarRequerimentoPorTipo(String codAuto, String tipoRequerimento) throws Exception {
		List<HashMap<String, String>> listaRequerimentos = RequerimentoBean
				.consultarRequerimentosPorAuto(String.valueOf(codAuto));

		for (HashMap<String, String> item : listaRequerimentos) {
			if (item.get("tipoRequerimento").equals(tipoRequerimento))
				return item;
		}

		return null;
	}
}