package REC;

/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Definir Relator/Junta Bean<br>
* <b>Description:</b>  Definir Relator/Junta Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class DefinirRelatorBean extends EventoBean  { 

	private String tipoJunta;
  
  public DefinirRelatorBean()  throws sys.BeanException {

	super() ;
	tipoJunta        = "0";		
  }
//--------------------------------------------------------------------------  
  public void setJ_sigFuncao(String j_sigFuncao,String obrParJur)   {
  	if (j_sigFuncao==null) j_sigFuncao = "" ;
  	setCFuncao(j_sigFuncao) ;
  	// Defesa Previa - REC0230
  	setStatusValido("5") ;
  	setStatusTPCVValido("0,1,2,3,4") ;		
  	setNFuncao("DEFINIR/TROCAR RELATOR DE DEF. PR�VIA") ;
  	setTipoReqValidos("DP,DC") ;
	// Testar parametro de Orgao - para verificar se � obrigat�rio Parecer Juridico.
	if (obrParJur==null) obrParJur = "N";
	if ("S".equals(obrParJur)) setStatusReqValido("0,1,2") ;
  	else setStatusReqValido("0,1,2") ;	
  	setOrigemEvento("100") ;
  	setCodEvento("208") ;	
	this.tipoJunta="0";
  	// 1a Instancia 
  	if ("REC0330".equals(j_sigFuncao)) {
	  	setStatusValido("15") ;
  		setStatusTPCVValido("") ;		
	  	setNFuncao("DEFINIR/TROCAR RELATOR DE 1a. INST�NCIA") ;
  		setTipoReqValidos("1P,1C") ;	
	  	setStatusReqValido("0,1,2") ;	
  		setOrigemEvento("100") ;
	  	setCodEvento("328") ;		
		this.tipoJunta="1";
  	}
  	if ("REC0430".equals(j_sigFuncao)) {
  	  	setStatusValido("35") ;
  		setStatusTPCVValido("") ;		
  	  	setNFuncao("DEFINIR/TROCAR RELATOR DE 2a. INST�NCIA") ;
  		setTipoReqValidos("2P,2C") ;	
  	  	setStatusReqValido("0,1,2") ;	
	  	setOrigemEvento("100") ;
  		setCodEvento("335") ;
  		//Raj	
  		//this.tipoJunta="2";
		this.tipoJunta="1";
  	}
    return ;	
  }   
//--------------------------------------------------------------------------
public void setTipoJunta(String tipoJunta)  {
  this.tipoJunta=tipoJunta ;
	if (tipoJunta==null) this.tipoJunta= "";
  }  
  public String getTipoJunta()  {
	return this.tipoJunta;
  }  
//--------------------------------------------------------------------------  
}
