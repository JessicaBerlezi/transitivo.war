package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class PenalidadeAutuacaoDERCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/PenalidadeAutuacaoDER.jsp" ;
  
   
  public PenalidadeAutuacaoDERCmd() {
    next             =  jspPadrao;
  }

  public PenalidadeAutuacaoDERCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		
	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			
	  	// cria os Beans, se n�o existir

	  	AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	  	if (AutoInfracaoBeanId==null)  {
			AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  	}
	  	// obtem e valida os parametros recebidos					
	  	String acao           = req.getParameter("acao");  
	  	if(acao==null)   acao = " ";
		if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
			AutoInfracaoBeanId.LeRequerimento(UsrLogado);
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado);
		}	
		if  (acao.equals("ProcessaPenalidade"))  {
		  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			nextRetorno = processaPenalidadeDER(AutoInfracaoBeanId,UsrLogado);
			
		}	
  		// processamento de saida dos formularios
	  	req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	
    }
    catch (Exception ue) {
	      throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }

  protected String processaPenalidadeDER(AutoInfracaoBean myAuto,ACSS.UsuarioBean myUsrLog) throws sys.CommandException 
  {
	  String nextRetorno=jspPadrao;
	  String sMensagem  = "";
	  try 
	  {
		  myAuto.setMsgErro("") ;
		  DaoBroker dao = DaoBrokerFactory.getInstance();
		  dao.processaPenalidadeDER(myAuto,myUsrLog);
		}
	  	catch (Exception e){
		    throw new sys.CommandException("AbrirRecursoDPCmdb: " + e.getMessage()+" "+sMensagem);
	  	}
	 	return nextRetorno  ;
	}
}
