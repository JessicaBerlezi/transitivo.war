package REC;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import sys.Util;
import ACSS.UsuarioBean;
/**
 * <b>Title:</b>        	SMIT-REC - Bean de Relator <br>
 * <b>Description:</b>  	Bean dados das Juntas - Tabela de Relator<br>
 * <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author  				Luciana Rocha
 * @version 				1.0
 */

public class RelatorBean extends sys.HtmlPopupBean { 
	
	private String codRelator;
	private String nomRelator;
	private String numCpf;
	private String codJunta;
	private String sigJunta;
	private String codOrgao;
	private String nomTitulo;
	private List relatores;
	private String atualizarDependente;
	private ArrayList listNivelUsuario;
	private String codUsuario;
	public final String PRESIDENTE = "0";
	public final String RELATOR = "1";
	private String indAtivo;
	private int iProcessoPorGuia;
	private int qtdProcDistrib;	
	
	public RelatorBean(){
		super();
		setTabela("TSMI_RELATOR");	
		setPopupWidth(35);
		codRelator	= "";	  	
		nomRelator	= "";
		numCpf	    = "";		
		codJunta	= "";
		sigJunta    = "";
		codOrgao    = "";
		nomTitulo	= "";
		relatores   = new ArrayList();
		atualizarDependente = "N";
		codUsuario  ="";
		indAtivo    = "";	
		iProcessoPorGuia = 0;
		qtdProcDistrib   = 0;		
	}

	public void setProcessoPorGuia(int iProcessoPorGuia)  {
		this.iProcessoPorGuia = iProcessoPorGuia ;
	}
	public int getProcessoPorGuia()  {
		return this.iProcessoPorGuia;
	}
	
	
	
	public void setCodRelator(String codRelator)  {
		this.codRelator = codRelator ;
		if (codRelator == null) 
			this.codRelator ="" ;
	}
	public String getCodRelator()  {
		return this.codRelator;
	}
	
	
	public void setNomRelator(String nomRelator){
		this.nomRelator = nomRelator;
		if (nomRelator == null)
			this.nomRelator = ""; 
	}
	public String getNomRelator()  {
		return this.nomRelator;
	}
	
	
	public void setNumCpf(String numCpf)  {
		this.numCpf = numCpf ;
		if (numCpf == null)	 this.numCpf="" ;
	}  
	public String getNumCpf()  {
		return this.numCpf;
	}
	
	public void setNumCpfEdt(String numCpf)  {
		if (numCpf==null)      numCpf = "";
		if (numCpf.length()<14) numCpf += "               ";
		this.numCpf=numCpf.substring(0,3)
		+ numCpf.substring(4,7) 
		+ numCpf.substring(8,11)
		+ numCpf.substring(12,14);
	}  
	public String getNumCpfEdt()  {
		if ("".equals(this.numCpf.trim())) return this.numCpf.trim() ;
		return this.numCpf=numCpf.substring(0,3)+ "." + numCpf.substring(3,6) 
		+ "." + numCpf.substring(6,9) + "-" + numCpf.substring(9,11) ;
	}
	
	public void setCodJunta(String codJunta)  {
		this.codJunta = codJunta ;
		if (codJunta == null) 
			this.codJunta = "" ;
	}  
	public String getCodJunta()  {
		return this.codJunta;
	}
	
	public void setSigJunta(String sigJunta){
		this.sigJunta = sigJunta;
		if (sigJunta == null)
			this.sigJunta = ""; 
	}
	public String getSigJunta()  {
		return this.sigJunta;
	}
	
	public void setCodOrgao(String codOrgao)  {
		this.codOrgao = codOrgao ;
		if (codOrgao == null) 
			codOrgao = "";
	}  
	public String getCodOrgao()  {
		return codOrgao;
	}
	
	public String getNomTitulo()  {
		return this.nomTitulo;
	}
	
	public void setNomTitulo(String nomTitulo)  {
		this.nomTitulo = nomTitulo ;
		if (nomTitulo == null) 
			this.nomTitulo = "";
	}
	
	public String getRelator(){
		String myRelator = "" ; 
		try    {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			myRelator = daoTabela.getRelator() ;
			
		}
		catch(Exception e){
		}	
		return myRelator ;
	}
	
	public void setRelatores(List relatores){
		this.relatores = relatores;
		if (relatores == null) relatores = new ArrayList();
	}
	
	public List getRelatores() {
		return this.relatores;
	}
	
	public RelatorBean getRelatores(int i) {
		return (RelatorBean) this.relatores.get(i);
	}
	
//	--------------------------------------------------------------------------
	public void setAtualizarDependente(String atualizarDependente) {
		this.atualizarDependente = atualizarDependente;
		if (this.atualizarDependente == null) 
			this.atualizarDependente = "N";
	}
	public String getAtualizarDependente() {
		return this.atualizarDependente;
	}
	//--------------------------------------------------------------------------
	
	
	
	
	/**-------------------------------------------------------------------
	 /*  C�digo Relativo a Junta (em REC)
	  /*--------------------------------------------------------------------
	   **/ 
	public List getJuntaRelator()  {
		List myJunta = null;
		try  {
			
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if( (myJunta = daoTabela.getRelator(codJunta)) == null )
				setMsgErro("Relator009 - Erro de Leitura Smit: ");
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public List getRelatorBroker()  {
		List myJunta = null;
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if( (myJunta = daoTabela.getRelatorBroker(this)) == null )
				setMsgErro("Relator009 - Erro de Leitura Detran: ");
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public void getRelatores(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta, RelatorBean RelatorId)  {
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.getRelatores(UsuarioBeanId,TipoJunta, RelatorId);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}

	public void getRelatoresAtivos(ACSS.UsuarioBean UsuarioBeanId,String TipoJunta)  {
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.getRelatoresAtivos(UsuarioBeanId,TipoJunta, this);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}
	
	public void getRelatoresAutomatica (ACSS.UsuarioBean UsuarioBeanId,String TipoJunta)  {
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			daoTabela.getRelatoresAutomatica(UsuarioBeanId,TipoJunta, this);
		}
		catch (Exception e) {
			setMsgErro("Relator007 - Leitura: " + e.getMessage());
		}//fim do catch
	}
	
	public boolean compareSmitBroker(List relatorSmit, List relatorBroker){
		RelatorBean relator, relator_aux;
		boolean flag = true;
		if( (relatorSmit == null) || (relatorBroker == null) )
			return false;
		
		for(int pos=0;pos<relatorSmit.size();pos++,flag=true){
			relator = (RelatorBean)relatorSmit.get(pos);
			for(int i= 0;i<relatorBroker.size();i++){
				relator_aux = (RelatorBean)relatorBroker.get(i);
				if(Util.lPad(relator.getNumCpf(),"0",11).equals(relator_aux.getNumCpf())){
					if(pos != i){
						relator = (RelatorBean)relatorBroker.set(pos,relator_aux);
						relatorBroker.set(i,relator);
					}
					flag=false;
					break;
				}
			}
			if(flag){
				if(pos>=relatorBroker.size())
					continue;
				relator = (RelatorBean)relatorBroker.set(pos,new RelatorBean());
				relatorBroker.add(relator);
			}
		}
		if( relatorSmit.size() > relatorBroker.size() ){
			for(int pos = relatorBroker.size(); pos < relatorSmit.size(); pos++)
				relatorBroker.add(new RelatorBean());
		}
		if( relatorSmit.size() < relatorBroker.size() ){
			for(int pos = relatorSmit.size(); pos < relatorBroker.size(); pos++)
				relatorSmit.add(new RelatorBean());
		}
		if( relatorSmit.size() < 10 ){
			for(int pos=relatorSmit.size();pos < 10;pos++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		else{
			for(int i=0;i<5;i++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		return true;
	}
	
	public boolean isInsertRelatorBroker(List relatorSmit, List relatorSmit_aux, List relatorBroker, List relatorBroker_aux)   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		
		RelatorBean myRelatorSmit, myRelatorSmit_aux, myRelatorBroker, myRelatorBroker_aux;
		// Verificada a valida��o dos dados nos campos
		try  {
			DaoTabelas daoTabela = DaoTabelas.getInstance();		   
			for(int pos=0; pos < relatorSmit.size();pos++){
				myRelatorSmit       = (RelatorBean)relatorSmit.get(pos);
				myRelatorSmit_aux   = (RelatorBean)relatorSmit_aux.get(pos);
				myRelatorBroker     = (RelatorBean)relatorBroker.get(pos);
				myRelatorBroker_aux = (RelatorBean)relatorBroker_aux.get(pos);	  		
				if( (myRelatorSmit.getNomRelator().equals(myRelatorSmit_aux.getNomRelator())) && (myRelatorSmit.getNumCpf().equals(myRelatorSmit_aux.getNumCpf())) && (myRelatorBroker.getNomRelator().equals(myRelatorBroker_aux.getNomRelator())) && (myRelatorBroker.getNumCpf().equals(myRelatorBroker_aux.getNumCpf())) )
					continue;
				
				if( (myRelatorSmit.getCodRelator().equals("")) && (myRelatorSmit.getNomRelator().equals("")) && ( (myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ) )
					continue;
				if( (!myRelatorSmit.getCodRelator().equals("")) && (!myRelatorSmit.getNomRelator().equals("")) && (!myRelatorBroker.getCodRelator().equals("")) ){
					if( daoTabela.RelatorUpdateBroker(myRelatorBroker,myRelatorBroker_aux) )
					{
						if( !daoTabela.RelatorUpdate(myRelatorSmit) ){
							vErro.add("Erro: 001 | Altera��o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorUpdateBroker(myRelatorBroker_aux);
						}
					}
					else{
						vErro.add("Erro: 001 | Altera��o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				if( (myRelatorSmit.getCodRelator().equals("")) && (!myRelatorSmit.getNomRelator().equals("")) && (!myRelatorBroker.getCodRelator().equals("")) && (!myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ){
					if( daoTabela.RelatorUpdateBroker(myRelatorBroker,myRelatorBroker_aux) )
					{
						if( !daoTabela.RelatorInsert(myRelatorSmit) ){
							vErro.add("Erro: 002 | Inclus�o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorUpdateBroker(myRelatorBroker_aux);
						}
					}
					else{
						vErro.add("Erro: 002 | Altera��o no Detran Relator: "+myRelatorBroker.getNomRelator()+myRelatorBroker.getCodRelator() +"\n");
						retorno = false;
					}
				}
				if( (!myRelatorSmit.getCodRelator().equals("")) && (!myRelatorSmit.getNomRelator().equals("")) && ((myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11)))) ){
					myRelatorBroker.setCodRelator(myRelatorSmit.getCodRelator());
					if( daoTabela.RelatorInsertBroker(myRelatorBroker) )
					{
						if( !daoTabela.RelatorUpdate(myRelatorSmit) ){
							vErro.add("Erro: 003 | Altera��o no Smit Relator: "+myRelatorSmit.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorDeleteBroker(myRelatorBroker);
						}
					}
					else{
						vErro.add("Erro: 003 | Inclus�o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				if( (myRelatorSmit.getCodRelator().equals("")) && (!myRelatorSmit.getNomRelator().equals("")) && ((myRelatorBroker.getCodRelator().equals("")) || (myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11)))) ){
					if( daoTabela.RelatorInsert(myRelatorSmit) )
					{
						myRelatorBroker.setCodRelator(myRelatorSmit.getCodRelator());
						if( !daoTabela.RelatorInsertBroker(myRelatorBroker) ){
							vErro.add("Erro: 004 | Inclus�o no Detran Relator: "+myRelatorBroker.getNomRelator()+ "\n");
							retorno = false;
							daoTabela.RelatorDelete(myRelatorSmit);
						}
					}
					else{
						vErro.add("Erro: 004 | Inclus�o no Smit Relator: "+myRelatorBroker.getNomRelator()+ "\n");
						retorno = false;
					}
				}
				if( (myRelatorSmit.getNomRelator().equals("")) && (myRelatorBroker.getNomRelator().equals(""))){
					boolean sucesso = true;
					if( (!myRelatorBroker.getCodRelator().equals("")) || (!myRelatorBroker.getCodRelator().equals(Util.lPad(""," ",11))) ){
						if( !daoTabela.RelatorDeleteBroker(myRelatorBroker_aux) ){
							vErro.add("Erro: 005 | Exclus�o no Detran Relator: "+myRelatorBroker_aux.getNomRelator()+ "\n");
							sucesso = false;
							retorno = false;
						}
					}
					if(sucesso){
						if( !myRelatorSmit.getCodRelator().equals("") ){
							if( !daoTabela.RelatorDelete(myRelatorSmit) ){
								vErro.add("Erro: 005 | Exclus�o no Smit Relator: "+myRelatorSmit_aux.getNomRelator()+ "\n");
								if( !myRelatorBroker.getCodRelator().equals(""))
									daoTabela.RelatorInsertBroker(myRelatorBroker_aux);
								sucesso = false;
								retorno = false;
							}
						}
					}
					if(sucesso){
						relatorSmit.remove(pos);
						relatorSmit_aux.remove(pos);
						relatorBroker.remove(pos);
						relatorBroker_aux.remove(pos);
						pos--;
					}
				}
			}
		}
		catch (Exception e) {
			vErro.addElement("Erro na altera��o: "+ e.getMessage() +"\n");
			retorno = false;
		}//fim do catch
		setMsgErro(vErro);
		
		if(relatorSmit.size()<10){
			for(int i=relatorSmit.size(); i<10;i++){
				relatorSmit.add(new RelatorBean());
				relatorBroker.add(new RelatorBean());
			}
		}
		
		return retorno;
	}
	
	public boolean isInsertRelator(List myRelator, List myRelator_aux)   {
		boolean retorno = true;
		Vector vErro = new Vector() ;
		try  {
			
			DaoTabelas daoTabela = DaoTabelas.getInstance();	
			retorno = daoTabela.JuntaInsertRelator(vErro,myRelator,myRelator_aux);    	   
		}
		catch (Exception e) {
			vErro.addElement("Erro na altera��o: "+ e.getMessage() +"\n");
			retorno = false;
		}
		setMsgErro(vErro);
		return retorno;
	}
	
	public void Le_Relator(String chave)  {
		try { 
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if ( daoTabela.RelatorLeBean(this, chave) == false) {
				this.codJunta	  = "0";
				this.codRelator	  = "0";
				setPkid("0") ;		
			}
		}// fim do try
		catch (Exception e) {
			this.codRelator	  = "0";	 	
			this.codJunta = "0" ;
			setPkid("0") ;		        			
		}
	}
	
	public String getRelatores(REC.JuntaBean myJunta){
		String myRelator = "" ; 
		try    {
			DaoTabelas dao = DaoTabelas.getInstance();	   
			myRelator = dao.getRelatores(myJunta) ;
		}
		catch(Exception e){	}	
		return myRelator ;
	}	
	
	public List getUsuarios(NivelUsuarioRelatorBean myRelats, UsuarioBean myUsrLogado)  {
		List myRelatores = null;
		try  {
			DaoTabelas dao = DaoTabelas.getInstance();
			if( (myRelatores = dao.getUsuarios(myRelats,myUsrLogado)) == null )
				setMsgErro("Erro de Leitura:");
		}
		catch (Exception e) {
			setMsgErro("Erro de Leitura: " + e.getMessage());
		}
		return myRelatores ;
	}
	
	public void setListNivelUsuario(ArrayList listNivelUsuario) {
		if(listNivelUsuario == null) listNivelUsuario = new ArrayList();
		else this.listNivelUsuario = listNivelUsuario;
	}
	public ArrayList getListNivelUsuario() {
		return this.listNivelUsuario;
	}
	
	public String getCodUsuario() {
		return codUsuario;
	}
	
	public void setCodUsuario(String codUsuario) {
		this.codUsuario = codUsuario;
	}
	
	public void setIndAtivo(String indAtivo) {
		if(indAtivo == null) indAtivo = "";
		else this.indAtivo = indAtivo;
	}
	public String getIndAtivo() {
		return indAtivo;
	}
	
	public void setQtdProcDistrib(int qtdProcDistrib)  {		
		this.qtdProcDistrib = qtdProcDistrib ;
	}
	public int getQtdProcDistrib()  {
		return this.qtdProcDistrib;
	}
}
