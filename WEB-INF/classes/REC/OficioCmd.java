package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class OficioCmd extends sys.Command {
  private final String SIM = "S";
  private final String NAO = "N";
  
  private String next;

  private static final String jspPadrao="/REC/Oficio.jsp" ;  
   
   public OficioCmd() {
	  next = jspPadrao;
   }

   public OficioCmd(String next) {
	  this.next = next;
   }

   public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {    
	 	sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
	    cmd.setNext(this.next) ;
	    nextRetorno = cmd.execute(req);
	
		HttpSession session = req.getSession() ;								
		ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
		if (UsrLogado==null)UsrLogado = new ACSS.UsuarioBean() ;
	   
	    ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
		if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  					    
			
		ACSS.ParamSistemaBean paramRec = (ACSS.ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
		if (paramRec == null)	paramRec = new ACSS.ParamSistemaBean();

		ParamOrgBean ParamOrgaoId = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
		if (ParamOrgaoId==null)ParamOrgaoId = new ParamOrgBean() ;	  			
		
		OficioBean OficioBeanId = (OficioBean) req.getAttribute("OficioBeanId") ;
		if (OficioBeanId == null) OficioBeanId = new OficioBean();
		
	    AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)session.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  	{
	  		AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	  		if(AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
	  		if(AutoInfracaoBeanId.getCondutor()==null) AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
	  		if(AutoInfracaoBeanId.getVeiculo()==null) AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
	  		if(AutoInfracaoBeanId.getInfracao()==null)AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
	  	}
	
		AbrirRecursoDPBean AbrirRecursoBeanDPId = (AbrirRecursoDPBean)session.getAttribute("AbrirRecursoBeanDPId") ;
		if (AbrirRecursoBeanDPId==null)  AbrirRecursoBeanDPId = new AbrirRecursoDPBean() ;	  			
		AbrirRecursoDPCmd AbrirRecursoBeanDPCmd = new AbrirRecursoDPCmd() ;	  			

		AbrirRecurso1aBean AbrirRecursoBean1aId = (AbrirRecurso1aBean)session.getAttribute("AbrirRecursoBean1aId") ;
		if (AbrirRecursoBean1aId==null)  AbrirRecursoBean1aId = new AbrirRecurso1aBean() ;
		AbrirRecurso1aCmd AbrirRecurso1aCmd = new AbrirRecurso1aCmd();
	
		AbrirRecurso2aBean AbrirRecursoBean2aId = (AbrirRecurso2aBean)session.getAttribute("AbrirRecursoBean2aId") ;
		if (AbrirRecursoBean2aId==null)  AbrirRecursoBean2aId = new AbrirRecurso2aBean() ;	  			
		AbrirRecurso2aCmd AbrirRecurso2aCmd = new AbrirRecurso2aCmd();
		
		RequerimentoBean RequerimentoId = (RequerimentoBean)session.getAttribute("RequerimentoId") ;
		if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
			
		DefinirRelatorBean DefinirRelatorId = (DefinirRelatorBean)req.getAttribute("DefinirRelatorId") ;
		if (DefinirRelatorId==null)  DefinirRelatorId = new DefinirRelatorBean() ;	
		DefinirRelatorCmd DefinirRelatorCmd = new DefinirRelatorCmd();
			
		InformarResultBean InformarResultId = (InformarResultBean)req.getAttribute("InformarResultId") ;
		if (InformarResultId==null)  InformarResultId = new InformarResultBean() ;
		InformarResultCmd  InformarResultCmd = new InformarResultCmd();
		
		CadastraProcCmd CadastraProcCmd = new CadastraProcCmd();
		EnviarDOSimplesCmd EnviarDOSimplesCmd = new EnviarDOSimplesCmd();
		AtualizDOSimplesCmd AtualizDOSimplesCmd = new AtualizDOSimplesCmd();
		
	    //Informa a SigFuncao	   
		AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));  
	  	AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
	  	
		// obtem e valida os parametros recebidos					
	  	String acao = req.getParameter("acao");  
	  	if(acao == null) acao = "";
		String mostraResultado = req.getParameter("mostraResultado");
		if (mostraResultado == null) mostraResultado = "";
		
		String numProc = req.getParameter("numprocesso");
		if (numProc == null) numProc = "";
	    
		String dataProc = req.getParameter("datprocesso");
		if (dataProc == null) dataProc = "";
	    
		String txtMotivoProc = req.getParameter("txtMotivo");
		if (txtMotivoProc == null) txtMotivoProc = "";
		
		String txtEmailRecurso = req.getParameter("txtEMail");
		if (txtEmailRecurso == null) txtEmailRecurso = "";
		
		String datEnvioDO = req.getParameter("dataEnvio");
		if (datEnvioDO == null) datEnvioDO = "";

		String codEnvioDO = req.getParameter("codEnvioDO");
	    if (codEnvioDO == null) codEnvioDO = "";  
		
		String tipoRecursoDP = req.getParameter("SolicDPBox");
	    if (tipoRecursoDP == null) tipoRecursoDP = ""; 
	    
		String tipoRecursoDC = req.getParameter("SolicDCBox");
	    if (tipoRecursoDC == null) tipoRecursoDC = "";  
	    
		String tipoRecursoDP2 = req.getParameter("SolicDPBox2");
	    if (tipoRecursoDP2 == null) tipoRecursoDP2 = ""; 
	    
		String tipoRecursoDC2 = req.getParameter("SolicDCBox2");
	    if (tipoRecursoDC2 == null) tipoRecursoDC2 = "";  
		
	    String tipoRecurso ="";
		if(!tipoRecursoDP.equals(""))tipoRecurso=tipoRecursoDP.trim();
		else if(!tipoRecursoDP2.equals(""))tipoRecurso=tipoRecursoDP2.trim();
		if(!tipoRecursoDC.equals(""))tipoRecurso=tipoRecursoDC.trim();
		else if(!tipoRecursoDC2.equals(""))tipoRecurso=tipoRecursoDC2.trim();
		if(tipoRecurso == null) tipoRecurso = "";
		if(tipoRecurso.length()>2) tipoRecurso = tipoRecurso.substring(0,3).trim();
		OficioBeanId.setTipoRecurso(tipoRecurso);
	    String codReq = req.getParameter("codRequerimento");
	    if (codReq == null) codReq = "";
	    RequerimentoId.setCodRequerimento(codReq);

		if (acao.equals("")){
			AutoInfracaoBeanId = new AutoInfracaoBean();
			tipoRecurso = "";
		}
	
		if (acao.equals("Limpar")){
		     //Limpa a tela
		     AutoInfracaoBeanId = new AutoInfracaoBean();
		     AbrirRecursoBeanDPId  = new AbrirRecursoDPBean();
		     AbrirRecursoBean1aId = new AbrirRecurso1aBean();
		     AbrirRecursoBean2aId = new AbrirRecurso2aBean();
		     RequerimentoId = new RequerimentoBean();
		     mostraResultado = "";
		}
		
	    if (acao.equals("MostraAuto"))	{						  
			 if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
			 if  (AutoInfracaoBeanId.getCondutor()==null) AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
			 if  (AutoInfracaoBeanId.getVeiculo()==null) AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
			 if  (AutoInfracaoBeanId.getInfracao()==null) AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
	
			 //Passa parametros necessarios para abrir o jsp AbrirConsultaAuto
			 AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
			 AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));
			 nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
		}
	    
	  	if((acao.equals("LeAI")&& (AutoInfracaoBeanId.getMsgErro().length()==0))||acao.equals("LeReq")){
	 	    AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
	  		AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;     

	  		//Prepara para saber qual recurso est� dispon�vel para o auto
	  		if ("0,1,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){
	        	AbrirRecursoBeanDPId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
	        	OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBeanDPId);
	        	InformarResultId.setJ_sigFuncao("");
	    		DefinirRelatorId.setJ_sigFuncao("",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
	     
	  		}
	  		
  			if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
  				AbrirRecursoBean1aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
  				OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBean1aId);
  				DefinirRelatorId.setJ_sigFuncao("REC0330",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
  				InformarResultId.setJ_sigFuncao("REC0343");
	    		
  			}
	  		   
  			if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
  				AbrirRecursoBean2aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
  				OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBean2aId);
  				DefinirRelatorId.setJ_sigFuncao("REC0430",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
  				InformarResultId.setJ_sigFuncao("REC0443");
  			}
  				
    	  			  
  			if (RequerimentoId.getCodRequerimento().equals("")){
  		        String codRequerimento = OficioBeanId.verificaCodReq(AutoInfracaoBeanId,RequerimentoId,OficioBeanId,UsrLogado);
  		        RequerimentoId.setCodRequerimento(codRequerimento);
  			}
  			
	        InformarResultId.setEventoOK(AutoInfracaoBeanId) ;

	        if (RequerimentoId.getCodRequerimento().length()!=0) 
				RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
  
		  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
   
		    InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
		    DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;
		    
		    if(AutoInfracaoBeanId.getMsgErro().length()== 0){
		    	String QTDautos = "";
		    	if(acao.equals("LeAI")){
		               RequerimentoId = new RequerimentoBean() ;
		    	}
		    	mostraResultado = SIM;
		    }
		    
            // Se o auto tiver percorrido todas as etapas
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.FIM_OPERACAO)){
		    	mostraResultado = NAO;
		    	AutoInfracaoBeanId.setMsgErro("Auto de Infra��o j� conclu�do.");
		    }
	  	}
	  	
	  	if (acao.equals("executarAutosDP")){
	        // Prepara o Auto		 
	  		String statusReq = "";
	  		AutoInfracaoBeanId.LeRequerimento(UsrLogado);   
	    	AbrirRecursoBeanDPId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBeanDPId);
		
			//Cadastrar n�mero do processo
			if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.CADASTRA_PROCESSO )) {
				CadastraProcCmd.TrocaProcesso(req,AutoInfracaoBeanId,UsrLogado,txtMotivoProc,numProc,dataProc,paramRec,ParamOrgaoId);
				AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
				if(!AutoInfracaoBeanId.getNumProcesso().equals(""))
				   OficioBeanId.setEtapaAuto(OficioBeanId.ABRE_RECURSO);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO CADASTRAR PROCESSO - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
			}
	
		    //Processa Defesa Pr�via   
			if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.ABRE_RECURSO)){
			  	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			  	AbrirRecursoBeanDPId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
			  	AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			  	AbrirRecursoBeanDPCmd.processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBeanDPId);
				statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBeanDPId);
			  	if(statusReq.equals("0"))
			  	  OficioBeanId.setEtapaAuto(OficioBeanId.DEFINE_RELATOR);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO ABRIR RECURSO - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
			} 
			
			//Definir Relator
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.DEFINE_RELATOR)){
		    	DefinirRelatorId.setJ_sigFuncao("",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;
				DefinirRelatorCmd.processaTroca(DefinirRelatorId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
				statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBeanDPId);
				if("0,2".indexOf(statusReq)>=0)		   
					OficioBeanId.setEtapaAuto(OficioBeanId.INFORMA_RESULTADO);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO DEFINIR RELATOR - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
		    }
		    
		    //Informa Resultado
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.INFORMA_RESULTADO)){
  				InformarResultId.setJ_sigFuncao("");
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
				InformarResultCmd.processaResult(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
		    }
		 }
	  	
	  	 if (acao.equals("executarAutos1a2a")){
	        //Prepara o Auto	
			AutoInfracaoBeanId.LeRequerimento(UsrLogado); 
			String statusReq = "";
			if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
  				AbrirRecursoBean1aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
  				OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBean1aId);
  			}
  			if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
  				AbrirRecursoBean2aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
  			    OficioBeanId.verificaPosicaoAuto(AutoInfracaoBeanId,UsrLogado,RequerimentoId,AbrirRecursoBean2aId);
  			}
			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			
			//Cadastrar n�mero do processo
			if(OficioBeanId.getEtapaAuto().equals(OficioBeanId.CADASTRA_PROCESSO )){
				 CadastraProcCmd.TrocaProcesso(req,AutoInfracaoBeanId,UsrLogado,txtMotivoProc,numProc,dataProc,paramRec,ParamOrgaoId);
				 AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
				 if(!AutoInfracaoBeanId.getNumProcesso().equals(""))
				    OficioBeanId.setEtapaAuto(OficioBeanId.ABRE_RECURSO);
				 else AutoInfracaoBeanId.setMsgErro("ERRO AO CADASTRAR PROCESSO - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
			}
	
		    //Processa 1� e 2� instancia
			if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.ABRE_RECURSO)){
  		  	    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			  	if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0)&& (AutoInfracaoBeanId.getCodStatus().length()>1)){
		  			AbrirRecursoBean1aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
		  			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
		  			AbrirRecurso1aCmd.processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBean1aId,"");
		  			statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean1aId);
			  	}
			  	
		  		if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0)&& (AutoInfracaoBeanId.getCodStatus().length()>1)){
		  			AbrirRecursoBean2aId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId);
		  			AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
		  			AbrirRecurso2aCmd.processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBean2aId,"");
		  			statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean2aId);
		  		}
  		
			  	if(statusReq.equals("0"))
		  		  OficioBeanId.setEtapaAuto(OficioBeanId.DEFINE_RELATOR);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO ABRIR RECURSO - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
			}
		
			//Definir Relator
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.DEFINE_RELATOR)){
		    	if(("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
		    		DefinirRelatorId.setJ_sigFuncao("REC0330",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		    		statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean1aId);
		    	}else if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
		    		DefinirRelatorId.setJ_sigFuncao("REC0430",ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
		    		statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean2aId);
		    	}
		    	
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;
				DefinirRelatorCmd.processaTroca(DefinirRelatorId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
				if("0,2".indexOf(statusReq)>=0)
				   OficioBeanId.setEtapaAuto(OficioBeanId.INFORMA_RESULTADO);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO DEFINIR RELATOR - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
			}
		    
		    //Informa Resultado
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.INFORMA_RESULTADO)){
		    	if(("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
	  				InformarResultId.setJ_sigFuncao("REC0343");
	  				statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean1aId);
		    	}else if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
	    			InformarResultId.setJ_sigFuncao("REC0443");
		    		statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean2aId);
	    		}
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
				InformarResultCmd.processaResult(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
				if(statusReq.equals("2"))
				  OficioBeanId.setEtapaAuto(OficioBeanId.ENVIA_DO);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO INFORMAR RESULTADO - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
		    }
		    
		    //Envia para DO
		    if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.ENVIA_DO)){
		    	if(("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
		    		InformarResultId.setJ_sigFuncao("REC0345");
		    		statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean1aId);
		    	}else if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1))){
		    		InformarResultId.setJ_sigFuncao("REC0445");
		    		statusReq = OficioBeanId.getStatusReq(AutoInfracaoBeanId,UsrLogado,RequerimentoId, AbrirRecursoBean2aId);
		    	}
		    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
				EnviarDOSimplesCmd.processaEnviaDO(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
				if(statusReq.equals("3"))
				  OficioBeanId.setEtapaAuto(OficioBeanId.INFORMA_DATA_PUB);
				else AutoInfracaoBeanId.setMsgErro("ERRO AO EFETUAR ENVIO PARA D.O. - AUTO: "+AutoInfracaoBeanId.getNumAutoInfracao());
		    }  
		    
		    //Atualiza data de Publica��o
			if (OficioBeanId.getEtapaAuto().equals(OficioBeanId.INFORMA_DATA_PUB)){
				if(("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1)))
					InformarResultId.setJ_sigFuncao("REC0360");
		    	else if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0 && (AutoInfracaoBeanId.getCodStatus().length()>1)))
		    		InformarResultId.setJ_sigFuncao("REC0460");
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				InformarResultId.setEventoOK(AutoInfracaoBeanId) ;
				AtualizDOSimplesCmd.processaAtualizDO(InformarResultId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
			}
	  	 }
		
		 // processamento de saida dos formularios
	  	 session.setAttribute("UsuarioBeanId",UsrLogado) ;
	     session.setAttribute("UsuarioFuncBeanId",UsuarioFuncBeanId) ;
	     session.setAttribute("ParamOrgaoId",ParamOrgaoId) ;
	     session.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	     session.setAttribute("AbrirRecursoBeanDPId",AbrirRecursoBeanDPId) ;
	     session.setAttribute("AbrirRecursoBean1aId",AbrirRecursoBean1aId) ;
	     session.setAttribute("AbrirRecursoBean2aId",AbrirRecursoBean2aId) ;
	     session.setAttribute("RequerimentoId",RequerimentoId) ;
	     req.setAttribute("OficioBeanId",OficioBeanId) ;
	     req.setAttribute("InformarResultId",InformarResultId) ;  
		 req.setAttribute("DefinirRelatorId",DefinirRelatorId) ;
		 req.setAttribute("mostraResultado",mostraResultado) ;
		 req.setAttribute("codEnvioDO",codEnvioDO) ;
	     req.setAttribute("datEnvioDO",datEnvioDO) ;
		 req.setAttribute("txtEmailRecurso",txtEmailRecurso) ;  
		 req.setAttribute("tipoRecurso",tipoRecurso) ;  

    }
    catch (Exception ue) {
	      throw new sys.CommandException("OficioCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }
   


}
	  