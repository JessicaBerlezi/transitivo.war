package REC;

import java.util.Iterator;

   

/**
 * <b>Title:</b>        SMIT - Revis�o de requrimento - <br>
 * <b>Description:</b>  Permite alterar o status dos requeriemntos 9 <br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Luciana Rocha
 * @version 1.0
 */

public class RevisaoRequerimentoBean extends EventoBean  {
	
	private String reqs;
	private String eventoOK;
	private String msg;
	private String comboReqs;
	private String codEvento ;
	
	
	public RevisaoRequerimentoBean() throws sys.BeanException {
		reqs      = "";
		eventoOK  = "N";
		msg       = "";
		codEvento = "264";
	}
	
	//--------------------------------------------------------------------------  
	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "N";
		this.eventoOK = eventoOK;
	}
	
	
	public void setJ_sigFuncao(String j_sigFuncao)   {  	
		if (j_sigFuncao==null) j_sigFuncao = "" ;
		setCFuncao(j_sigFuncao) ;
		
		// Revis�o de Requerimento de Defesa Pr�via
		setStatusValido("10,11,12,14,99,98") ;
		setStatusTPCVValido("5") ;		
		setNFuncao("Revis�o de Requerimento de Def. Pr�via") ;
		setTipoReqValidos("DP,DC,TR") ;
		setStatusReqValido("9") ;	
		setOrigemEvento("100") ;
		setCodEvento("264") ;	
		
		// Revis�o de Requerimento de 1� inst�ncia
		if ("REC0391".equals(j_sigFuncao)) {
			setStatusValido("99,96,30,15") ;
			setStatusTPCVValido("15") ;		
			setNFuncao("Revis�o de Requerimento de  1a. Inst�ncia") ;
			setTipoReqValidos("1P,1C") ;	
			setStatusReqValido("9") ;	
			setOrigemEvento("100") ;
			setCodEvento("384") ;			
		}
		
		// Revis�o de Requerimento de 2a Instancia	
		if ("REC0484".equals(j_sigFuncao)) {
			setStatusValido("99,97") ;
			setStatusTPCVValido("35") ;		
			setNFuncao("Revis�o de Requerimento de 2a. Inst�ncia") ;
			setTipoReqValidos("2P,2C") ;
			setStatusReqValido("9") ;	
			setOrigemEvento("100") ;
			setCodEvento("391") ;							
		}
	} 
	
	
	public void setEventoOKRevisao(REC.AutoInfracaoBean myAuto)	throws sys.BeanException 
	{
		setMsg("");
		setEventoOK("N");	
		if ("".equals(myAuto.getNomStatus())) return ;
		setMsg(myAuto.getNomStatus()+" ("+myAuto.getDatStatus()+")") ;
		
		
		if (getCodEvento().equals("264")) 
		{
			if (!myAuto.getCodStatus().equals("10") && !myAuto.getCodStatus().equals("11") && !myAuto.getCodStatus().equals("12") && !myAuto.getCodStatus().equals("99") && !myAuto.getCodStatus().equals("98") && !myAuto.getCodStatus().equals("14") ) 
			{
				this.msg += " - N�o � poss�vel acerto em requerimento para revis�o.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();
				while (it.hasNext()) 
				{
					req = (REC.RequerimentoBean) it.next();
					
					if (req.getCodStatusRequerimento().equals("9"))
					{
						setEventoOK("S");						
						break;
					}
						
					
						
				}
			} catch (Exception ue) {
				throw new sys.BeanException("RevisaoRequerimentoBean: " + ue.getMessage());
			}
		}
		
		
		if (getCodEvento().equals("384")) 
		{
			if (!myAuto.getCodStatus().equals("99") && !myAuto.getCodStatus().equals("96") && !myAuto.getCodStatus().equals("30") && !myAuto.getCodStatus().equals("15")) 
			{
				this.msg += " - N�o � poss�vel acerto em requerimento para revis�o.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();
				while (it.hasNext()) 
				{
					req = (REC.RequerimentoBean) it.next();
					
					if (req.getCodStatusRequerimento().equals("9"))
					{
						setEventoOK("S");
						break;
					}
				}
			} catch (Exception ue) {
				throw new sys.BeanException("RevisaoRequerimentoBean: " + ue.getMessage());
			}
		}
		
		
		if (getCodEvento().equals("391")) 
		{
			if (!myAuto.getCodStatus().equals("99") && !myAuto.getCodStatus().equals("97")) 
			{
				this.msg += " - N�o � poss�vel acerto em requerimento para revis�o.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();
				while (it.hasNext()) 
				{
					req = (REC.RequerimentoBean) it.next();
					
					if (req.getCodStatusRequerimento().equals("9"))
					{
						setEventoOK("S");
						break;
					}
				}
			} catch (Exception ue) {
				throw new sys.BeanException("RevisaoRequerimentoBean: " + ue.getMessage());
			}
		}
		
		
		
		return;
	}
	
	public String getEventoOK() {
		return this.eventoOK;
	}
//	--------------------------------------------------------------------------  
	public String getMsg() {
		return msg;
	}
	
//	--------------------------------------------------------------------------  
	
	
	public void setReqs(REC.AutoInfracaoBean myAuto,REC.RequerimentoBean myReq) throws sys.BeanException {
		this.reqs="";
		String aux="";
		for(int f=0;f<35;f++) 	aux=aux+"&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append("<select style='border-style: outset;' name='codRequerimento' size='0' onChange='javascript: valida(\"LeReq\",this.form)'> \n");	  
		popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");	    			  
		String seleciona="";	
		String nom = "";
		try {
			Iterator it = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean req  = new REC.RequerimentoBean();
			while (it.hasNext()) {
				req =(REC.RequerimentoBean)it.next() ;
				// Requerimento de DP ou DC com status 2,3
				if ( (getTipoReqValidos().indexOf(req.getCodTipoSolic())>=0) && (getStatusReqValido().indexOf(req.getCodStatusRequerimento())>=0) ){
					seleciona = (myReq.getCodRequerimento().equals(req.getCodRequerimento())) ? "selected" : "";
					nom = (req.getNomResponsavelDP().length()>20 ? req.getNomResponsavelDP().substring(0,20) : req.getNomResponsavelDP()) ;
					popupBuffer.append("<OPTION VALUE='"+req.getCodRequerimento()+"' "+seleciona+" >"+req.getCodTipoSolic()+" - "+nom+" - "+req.getNumRequerimento()+"</OPTION> \n");
				}
			}
		}
		catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");	  
		this.reqs = popupBuffer.toString() ;
	}
	public String getReqs()   {
		return  ("S".equals(this.eventoOK)) ? this.reqs : this.msg ;
	}
	
	public String getComboReqs() {
		return (("S".equals(this.eventoOK)) ? this.comboReqs : "&nbsp;");
	}
	
}
