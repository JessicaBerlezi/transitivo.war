package REC;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Informar Resultado Bean<br>
* <b>Description:</b>  Informar Resultado Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello 
* @version 1.0
*/
     
public class CancelamentoBean    extends sys.HtmlPopupBean { 

  private String nomStatus;
  private String codStatus; 
  
  private String popupMulti;

  private String sigFuncao;
  private String txtMotivo;
  private String indOper;
    
  public CancelamentoBean()  throws sys.BeanException {

    nomStatus        = "";
    codStatus        = "";

	sigFuncao        = "";
	txtMotivo        = "";
	indOper          = "1";
	  
  }  

//--------------------------------------------------------------------------  
  public void setNomStatus(String nomStatus)  {
  this.nomStatus=nomStatus ;
    if (nomStatus==null) this.nomStatus= "";
  }  
  public String getNomStatus()  {
  	return nomStatus;
  }
  
  public void setIndOper(String indOper)  {
	this.indOper=indOper ;
	  if (indOper==null) this.indOper= "1";
	}  
	public String getIndOper()  {
	  return indOper;
	}
  
  public void setCodStatus(String codStatus)  {
  this.codStatus=codStatus ;
    if (codStatus==null) this.codStatus= "";
  }  
  public String getCodStatus()  {
  	return this.codStatus;
  }
// --------------------------------------------------------------------------  
   public void setSigFuncao(String sigFuncao)   {
	 if (sigFuncao==null) sigFuncao="";      
	 this.sigFuncao=sigFuncao ;	
   } 
   public String getSigFuncao()   {
	 return this.sigFuncao;	
   } 
// --------------------------------------------------------------------------     
   public void setTxtMotivo(String txtMotivo)   {
    if (txtMotivo==null) txtMotivo = "";
    this.txtMotivo = txtMotivo ;	
   }
   public String getTxtMotivo()   {
    return txtMotivo ;	
   }


}
