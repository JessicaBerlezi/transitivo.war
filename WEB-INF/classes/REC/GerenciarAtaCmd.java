package REC;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import UTIL.Logs;
import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class GerenciarAtaCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/REC/GerenciarAta.jsp";

	public GerenciarAtaCmd() {
		next = jspPadrao;
	}

	public GerenciarAtaCmd(String next) {
		this.next = next;
	}

	protected String processaAta(HttpServletRequest req, ParamOrgBean ParamOrgaoId, AutoInfracaoBean myAuto,
			ACSS.UsuarioBean UsrLogado, REC.AbrirRecursoDPBean AbrirRecursoBeanId) throws sys.CommandException {
		String nextRetorno = jspPadrao;
		String sMensagem = "";

		try {
			myAuto.setMsgErro("");

			String acao = req.getParameter("acao");

			Ata ata = new Ata();

			if (acao == null)
				acao = " ";

			if (acao.equals("IncluirAta")) {
				String nrAta = req.getParameter("nrAta");
				String nAta = req.getParameter("numPlaca");
				String nJunta = req.getParameter("numAutoInfracao");
				String qtdeProcesso = req.getParameter("qtdeprocesso");
				String dataSessao = req.getParameter("De");
				String tpAta = req.getParameter("indPagoradio");

				ata.setCdNatureza(Integer.parseInt(req.getParameter("codNatureza").toString()));
				ata.setDescAta(nAta);
				ata.setNrAta(Integer.valueOf(nrAta));
				ata.setJuntaAta(nJunta);
				ata.setDataSessao(dataSessao.replaceAll("-", "/"));
				ata.setTpAta(tpAta);
				ata.setQtdeAtaProcesso(Integer.valueOf(qtdeProcesso));
				ata.setStatusAta("A");
				ata.setCodOrgao(ControladorRecGeneric.codOrgao);
				/// forma��o: 000/TP/YY, onde 000 � a sequencia num�rica, TP � o
				/// tipo da ata(Def.Previa, 1P OU 2P) e YY � o ano corrente
				String anoVigente = "";
				Calendar data = Calendar.getInstance();
				anoVigente = String.valueOf(data.get(Calendar.YEAR)); // Thu Mar
																		// 19
																		// 15:56:58
																		// BRT
																		// 2015
				ata.setAnoAta(Integer.valueOf(anoVigente));

				String dataAgora = new SimpleDateFormat("dd/MM/yyyy").format(System.currentTimeMillis());
				ata.setDataInclusao(dataAgora);

				// Verifica se a data da sess�o informada �
				// sabado|domingo|feriado, retorna true se a data for uma das
				// op��es
				if (!ata.validaData(ata.getDataSessao())) {
					Date d = new Date();

					// Converte data sess�o(string) informada em Date
					d = ata.convertStringInDate(dataSessao);
					do {
						d.setDate(d.getDate() - 1); // Dimimui 1 dia
						// Data do aviso � data da Sess�o menos 1
						ata.setDataAviso(ata.convertDateInString(d)); // converte
																		// a
																		// data
																		// em
																		// string
																		// e
																		// atribui
																		// a
																		// aviso
					} while (ata.validaData(ata.getDataAviso())); // enquanto
																	// for
																	// feriado
																	// incrementa
																	// -1 dia

					if (!ata.validaData(ata.getDataSessao()) && !ata.validaData(ata.getDataAviso())) { // se
																										// os
																										// dois
																										// forem
																										// falsos
																										// �
																										// porque
																										// eles
																										// nao
																										// s�o
																										// sabado,
																										// domingo
																										// e
																										// nem
																										// feriado.
						Date dd = new Date();
						// Converte data aviso informada em Date
						dd = ata.convertStringInDate(ata.getDataAviso());
						do {
							dd.setDate(dd.getDate() - 1); // Dimimui 1 dia

							// Data do distribui��o � data da Aviso menos 1
							ata.setDataDistribuicao(ata.convertDateInString(dd)); // 1
																					// dia
						} while (ata.validaData(ata.getDataDistribuicao())); // enquanto
																				// for
																				// feriado
																				// incrementa
																				// -1
																				// dia
					}

					ata.isInsert(ata);
				} else {
					// informa que a data da sessao nao � valida e sugere a
					// proxima data nao sendo sabado|domingo|feriado
					throw new Exception("Informe outra data!");
				}

			}

			if (acao.equals("AlterarAta")) {
				String codAta = req.getParameter("codAta");
				nextRetorno = "/REC/AlterarAta.jsp";
				return nextRetorno;
			}

			if (acao.equals("alterar")) {
				String codAta = req.getParameter("codAta");
				String qtdeProcessp = req.getParameter("qtdeProcessoAta");
				String status = req.getParameter("statusAta");
				String dtPublicacao = req.getParameter("dtPublicacao");
				String dtSessao = req.getParameter("dtSessao");
				String dtDistribuicao = req.getParameter("dtDistribuicao");
				String dtAviso = req.getParameter("dtAviso");

				ata = Ata.consultarAtaPor(codAta);

				ata.setQtdeAtaProcesso(Integer.valueOf(qtdeProcessp));
				ata.setStatusAta(status);
				ata.setDataPublicacao(dtPublicacao);
				ata.setDataSessao(dtSessao);
				ata.setDataDistribuicao(dtDistribuicao);
				ata.setDataAviso(dtAviso);
				ata.isAltera(ata);
			}

			if (acao.equals("ExcluirAta")) {
				String codAta = req.getParameter("codAta");
				ata.setCodAta(codAta);
				ata.isDelete(ata);
			}
		} catch (Exception e) {
			Logs.gravarLog("exce��o no metodo: processaAta: " + e.getMessage());

			nextRetorno = "/REC/ErroDataAta.jsp";
			return nextRetorno;
			// throw new sys.CommandException("AbrirRecursoDPCmdb: " +
			// e.getMessage()+" "+sMensagem);
		}
		return nextRetorno;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {
			HttpSession session = req.getSession();

			int codNatureze = Integer.parseInt(session.getAttribute("codNatureza").toString());

			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)
				UsrLogado = new ACSS.UsuarioBean();
			ACSS.UsuarioFuncBean UsuarioFuncBeanId = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (UsuarioFuncBeanId == null)
				UsuarioFuncBeanId = new ACSS.UsuarioFuncBean();
			ParamOrgBean ParamOrgaoId = (ParamOrgBean) session.getAttribute("ParamOrgBeanId");
			if (ParamOrgaoId == null)
				ParamOrgaoId = new ParamOrgBean();
			ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)
				parSis = new ParamSistemaBean();
			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean) req.getAttribute("AutoInfracaoBeanId");
			// cria os Beans, se n�o existir
			AbrirRecursoDPBean AbrirRecursoBeanId = (AbrirRecursoDPBean) req.getAttribute("AbrirRecursoBeanId");
			if (AbrirRecursoBeanId == null)
				AbrirRecursoBeanId = new AbrirRecursoDPBean();

			if (AutoInfracaoBeanId == null) {
				AutoInfracaoBeanId = new AutoInfracaoBean();
				if (AutoInfracaoBeanId.getProprietario() == null)
					AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());
				if (AutoInfracaoBeanId.getCondutor() == null)
					AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());
				if (AutoInfracaoBeanId.getVeiculo() == null)
					AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());
				if (AutoInfracaoBeanId.getInfracao() == null)
					AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());

				req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			}
			// obtem e valida os parametros recebidos
			String acao = req.getParameter("acao");

			if (acao == null)
				acao = " ";

			if (acao.equals("IncluirAta") || acao.equals("AlterarAta") || acao.equals("ExcluirAta")
					|| acao.equals("alterar")) {
				nextRetorno = processaAta(req, ParamOrgaoId, AutoInfracaoBeanId, UsrLogado, AbrirRecursoBeanId);
			}

			// if ( (acao.equals("LeAI")) &&
			// (AutoInfracaoBeanId.getMsgErro().length()==0) ) {
			// AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			// AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
			// AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			// }
			// if (acao.equals("AbreRecurso")) {
			// AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			// AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
			// AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			// nextRetorno =
			// processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBeanId);
			// }
			//
			// if (acao.equals("Imprimehistorico")){
			// //Imprimir hist�rico com filtro de eventos, conforme
			// par�metro LISTAGEM_DE_EVENTOS
			// String codEventoFiltro =
			// ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			// AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAuto"));
			// AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			// AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
			// AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			// AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
			// nextRetorno = "/REC/consultaHistImp.jsp" ;
			// }

			// if (acao.equals("ImprimeAuto")){
			// //Imprimir hist�rico com filtro de eventos, conforme
			// par�metro LISTAGEM_DE_EVENTOS
			// String codEventoFiltro =
			// ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
			// AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAuto"));
			// AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));
			// AutoInfracaoBeanId.setNumProcesso(req.getParameter("NumProcesso"));
			// AutoInfracaoBeanId.LeHistorico(UsrLogado) ;
			// AutoInfracaoBeanId.filtraHistorico(AutoInfracaoBeanId,codEventoFiltro);
			// nextRetorno = "/REC/consultaHistImp.jsp" ;
			// }

			// processamento de saida dos formularios
			req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			req.setAttribute("AbrirRecursoBeanId", AbrirRecursoBeanId);
			req.setAttribute("ParamOrgaoId", ParamOrgaoId);
			req.setAttribute("codOrgao", ParamOrgaoId.getCodOrgao());
		} catch (Exception ue) {
			throw new sys.CommandException("AbrirRecursoDPCmd: " + ue.getMessage());
		}
		return nextRetorno;
	}

}
