package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

/**
* <b>Title:</b>Motivo de Cancelamento<br>
* <b>Description:</b>Cadastra os motivos do cancelamento dos autos<br>
* <b>Copyright:</b>Copyright (c) 2005<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Luciana Rocha	
* @version 1.0
*/

public class MotivoCancelCmd extends sys.Command {


  private static final String jspPadrao="/REC/MotivoCancel.jsp";    
  private String next;


  public MotivoCancelCmd() {
    this.next = jspPadrao;
  }

  public MotivoCancelCmd(String next) {
    this.next = jspPadrao;
  }
  
  public String execute(HttpServletRequest req) throws sys.CommandException {
	String nextRetorno  = next ;
    try {      
		// Cria a Bean 
		MotivoCancelBean motivoCancelBean = new MotivoCancelBean();
								  
		// obtem e valida os parametros recebidos					
		String acao = req.getParameter("acao");  
		if(acao == null) acao ="";   
		  
		String atualizarDependente = req.getParameter("atualizarDependente"); 
		if(atualizarDependente == null || atualizarDependente.equals(""))
		   atualizarDependente ="N";  
		motivoCancelBean.setAtualizarDependente(atualizarDependente);
		
	    if(acao.compareTo("")==0) {
		  motivoCancelBean.getMotivoCancel(20,5);
		  motivoCancelBean.setAtualizarDependente("S");		  	
	    }
	    else if(acao.compareTo("Atualizar") == 0){
			String[] codMotivoCancelamento = req.getParameterValues("codMotivoCancelamento");
			if(codMotivoCancelamento == null) 
				codMotivoCancelamento = new String[0];
			  
			String[] numMotivoCancelamento = req.getParameterValues("numMotivoCancelamento");
			if(numMotivoCancelamento == null) 
				numMotivoCancelamento = new String[0];
							  
			String[] descMotivoCancelamento = req.getParameterValues("descMotivoCancelamento");
			if(descMotivoCancelamento == null)  
				descMotivoCancelamento = new String[0];         

			Vector motivoCancel = new Vector(); 
			for (int i = 0; i < codMotivoCancelamento.length;i++) {
				MotivoCancelBean Alter = new MotivoCancelBean() ;		
				Alter.setCodMotivoCancel(Integer.parseInt(codMotivoCancelamento[i]));
				Alter.setNumMotivoCancel(numMotivoCancelamento[i]);
				Alter.setDescMotivoCancel(descMotivoCancelamento[i]);	  
				motivoCancel.addElement(Alter) ; 					
			}	
			
			motivoCancelBean.setMotivoCancel(motivoCancel);
			motivoCancelBean.isAlterMotivoCancel();
	    }
	    else if(acao.compareTo("I") == 0){
			req.setAttribute("vMotivoCancel",motivoCancelBean.getMotivoCancel(0,0));
			nextRetorno = "/REC/MotivoCancelImp.jsp";
	    }
	
		req.setAttribute("motivoCancelBean", motivoCancelBean);		 
    }
    catch (Exception ue) {
      throw new sys.CommandException("MotivoCancelCmd 001: " + ue.getMessage());
    }
	return nextRetorno;
  }  


}