 package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

/**
* <b>Title:</b>        Controle de Acesso <br>
* <b>Description:</b>  Bean dados dos Feriados<br>
* <b>Copyright:</b>    Copyright (c) 2003<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Wellem Mello
* @version 1.0
*/

public class FeriadoBean extends sys.HtmlPopupBean { 

	 private String codMunicipio;  
	 private String datFeriado;   
	 private String nomMunicipio;
	 private String codMunFeriado;
	 private String atualizarDependente;
	 private String txtFeriado;	 
	 private Vector feriados;	
	 private List   municipios;	
	 private String tipoFeriado;	
	 private String  codUF;
      
   public FeriadoBean() throws sys.BeanException {
    super();	  
	  codMunicipio   = " ";
	  datFeriado = "";	   
	  txtFeriado = "";
	  nomMunicipio = "";
	  codMunFeriado = "";	  
	  tipoFeriado = "";
	  codUF = "";
	  atualizarDependente="N";	  	  
	  feriados  = new Vector() ;	   
	  municipios      = new ArrayList() ;   
  }

//--------------------------------------------------------------------------
public void setCodMunicipio(String codMunicipio) {
	 if (codMunicipio == null ) 	this.codMunicipio = "0";
	 else    					this.codMunicipio = codMunicipio ;
  }
public String getCodMunicipio()  { return this.codMunicipio; }
  
//--------------------------------------------------------------------------
public void setCodUF(String  codUF) {
	 if (codUF == null ) 	this.codUF = new String ();
	 else    					this.codUF = codUF ;
  }
public String  getCodUF()  { return this.codUF; }
    
  
//--------------------------------------------------------------------------
  public void setDatFeriado(String datFeriado)  {
		  if (datFeriado==null) datFeriado=" " ;		  
		  this.datFeriado = datFeriado;
	  }
	  public String getDatFeriado()  {
		  return this.datFeriado;
	  }  
	
//	--------------------------------------------------------------------------
	public void setTipoFeriado(String tipoFeriado)  {
			if (tipoFeriado==null) tipoFeriado="M" ;		  
			this.tipoFeriado = tipoFeriado;
		}
		public String getTipoFeriado()  {
			return this.tipoFeriado;
		}	  
//--------------------------------------------------------------------------
  public void setCodMunFeriado(String codMunFeriado)  {
		   if (codMunFeriado==null) codMunFeriado=" " ;		  
		   this.codMunFeriado = codMunFeriado;
	   }
	   public String getCodMunFeriado()  {
		   return this.codMunFeriado;
	   }  	   
//--------------------------------------------------------------------------
	public void setAtualizarDependente(String atualizarDependente)   {
		this.atualizarDependente = atualizarDependente ;
	}
	public String getAtualizarDependente() {	return this.atualizarDependente; }

//--------------------------------------------------------------------------

  public void setNomMunicipio(String nomMunicipio)  {
	  if (nomMunicipio == null)	this.nomMunicipio =""; 
	  else				  	this.nomMunicipio = nomMunicipio;
	}
  public String getNomMunicipio()  { return this.nomMunicipio; }

//	--------------------------------------------------------------------------
	public void setTxtFeriado(String txtFeriado)  {
		  if (txtFeriado==null) txtFeriado=" " ;		  
		  this.txtFeriado = txtFeriado;
	  }
	  public String getTxtFeriado()  {
		  return this.txtFeriado;
	  }  
//--------------------------------------------------------------------------		
  public void setMunicipios(List municipios)  {
	  this.municipios=municipios ;	
	}
   public List getMunicipios()  {
	 return this.municipios;
   }
//--------------------------------------------------------------------------
public void Le_Feriado(String tpsist,int pos)  throws sys.BeanException {
	 try {   
		DaoTabelas daoTabela = DaoTabelas.getInstance();
		  this.codMunicipio=tpsist;			 	    	
			if ( daoTabela.FeriadosLeBean(this, pos) == false)
		  this.codMunicipio	  = tpsist;			
	 } // fim do try
	 catch (Exception ex) {
		this.codMunicipio = "0" ;	
		throw new sys.BeanException(ex.getMessage());
	 }//fim do catch
	return;
}
//--------------------------------------------------------------------------
public void setFeriados(Vector feriados)  {
   	this.feriados = feriados ;   	
   }

public Vector getFeriados(int min,int livre)  {
      try  {
		DaoTabelas daoTabela = DaoTabelas.getInstance();
		daoTabela.FeriadosGetFer(this);
	   	if (this.feriados.size()<min-livre) livre = min-feriados.size() ;		  
   		for (int i=0;i<livre; i++) 	{
   			this.feriados.addElement(new FeriadoBean()) ;
	   	}
      }
      catch (Exception e) {
           setMsgErro("Feriados007 - Leitura: " + e.getMessage());
      }//fim do catch
    return this.feriados ;
   }


public Vector getFeriados()  {
      return this.feriados;
   }

public  FeriadoBean getFeriados(int i)  { return (FeriadoBean)this.feriados.elementAt(i); }

public boolean isValidFeriados(Vector vErro) {
	   if ("0".equals(this.codMunicipio)) vErro.addElement("Munic�pio n�o selecionado. \n") ;
	   String sDesc   = ""  ;
	   String sData = "";
	   for (int i = 0 ; i<this.feriados.size(); i++) {
		   	FeriadoBean myFeriado = (FeriadoBean)this.feriados.elementAt(i) ;
		    sDesc = myFeriado.getTxtFeriado();
		   	sData = myFeriado.getDatFeriado();
		   	if ((sDesc.length()==0) ||("".equals(sDesc)) || (sData.length()==0) ||("".equals(sData)))  continue ;
		   	// verificar duplicata 
		   	for (int m = 0 ; m<this.feriados.size(); m++) {
		   		FeriadoBean myFeriadoOutra = (FeriadoBean)this.feriados.elementAt(m) ;		
		   	   if ((m!=i) && (myFeriadoOutra.getTxtFeriado().trim().length()!=0) && (myFeriadoOutra.getTxtFeriado().equals(sDesc)) &&
			   (myFeriadoOutra.getDatFeriado().trim().length()!=0) && (myFeriadoOutra.getDatFeriado().equals(sData))) {
		   		 vErro.addElement("Feriados em duplicata  !!!!: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
		   	   }
		   	}
	   }
	   return (vErro.size()==0) ;  
   }

public boolean isInsertFeriados()   {
	boolean retorno = true;
	Vector vErro = new Vector() ;
    // Verificada a valida��o dos dados nos campos
       try  {
            DaoTabelas daoTabela = DaoTabelas.getInstance();		   
          if (daoTabela.FeriadosPreparaInsert(this,vErro))    {					   
               vErro.addElement("Dados Atualizados");
   	   }
   		   else    {			   		   
   		      vErro.addElement("Dados N�o Atualizados");     	   
   		   }
       }
       catch (Exception e) {
            vErro.addElement("Dados N�o Atualizados") ;
            vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
        	   retorno = false;
       }//fim do catch
   
   
    setMsgErro(vErro);
    return retorno;
  }  
 
public FeriadoBean getMunicipios(String myCodMunicipio) throws sys.BeanException {
	  try { 	 
		  FeriadoBean	his = new FeriadoBean();					
	  	Iterator itx  = getMunicipios().iterator() ;	
	  	boolean achou = false;
	  	while (itx.hasNext()) {
	  		his =(FeriadoBean)itx.next() ;
  			if (his.codMunicipio.equals(myCodMunicipio)) {
  				achou = true;
  				break;
	  		}
  		}
	  	if (achou==false) his = new FeriadoBean();
  		return his;
	}		
	catch (Exception ex) {
		throw new sys.BeanException(ex.getMessage());
	}		
  } 
}
 
