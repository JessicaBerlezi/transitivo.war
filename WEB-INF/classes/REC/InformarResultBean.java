package REC;



/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Informar Resultado Bean<br>
* <b>Description:</b>  Informar Resultado Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class InformarResultBean extends EventoBean  {   
  
  public InformarResultBean()  throws sys.BeanException {

  super() ;
  
  }
//--------------------------------------------------------------------------  
  public void setJ_sigFuncao(String j_sigFuncao)   {
	if (j_sigFuncao==null) j_sigFuncao = "" ;
	setCFuncao(j_sigFuncao) ;
	// Resultado de Defesa Previa - REC0240
	setStatusValido("5") ;
	setStatusTPCVValido("0,1,2,3,4") ;		
	setNFuncao("Resultado Def. Pr�via") ;
	setTipoReqValidos("DP,DC") ;
	setStatusReqValido("2") ;	
	setOrigemEvento("100") ;
	setCodEvento("209") ;							
	// Resultado de 1a Instancia
    if ("REC0343".equals(j_sigFuncao)) {
      	setStatusValido("15") ;
      	setStatusTPCVValido("") ;		
      	setNFuncao("Resultado de 1a. Inst�ncia") ;
      	setTipoReqValidos("1P,1C") ;	
      	setStatusReqValido("2") ;	
      	setOrigemEvento("100") ;
      	setCodEvento("329") ;			
    }
	// Resultado de 2a Instancia	
	if ("REC0443".equals(j_sigFuncao)) {
    	setStatusValido("35") ;
    	setStatusTPCVValido("") ;		
    	setNFuncao("Resultado de 2a. Inst�ncia") ;
    	setTipoReqValidos("2P,2C") ;
    	setStatusReqValido("2") ;	
    	setOrigemEvento("100") ;
    	setCodEvento("336") ;							
    }

	// Enviar DO 1a Instancia	
	if ( ("REC0345".equals(j_sigFuncao))  ) {
		setStatusValido("15") ;
		setStatusTPCVValido("") ;		
		setNFuncao("Enviar para Publica��o") ;
		setTipoReqValidos("1P,1C") ;
		setStatusReqValido("3") ;	
		setOrigemEvento("100") ;
		setCodEvento("381") ;	
	}    	
	// Enviar DO 2a Instancia	
	if ( ("REC0445".equals(j_sigFuncao))  ) {
		setStatusValido("35") ;
		setStatusTPCVValido("") ;		
		setNFuncao("Enviar para Publica��o") ;
		setTipoReqValidos("2P,2C") ;
		setStatusReqValido("3") ;	
		setOrigemEvento("100") ;
		setCodEvento("388") ;
	}    
	// Enviar DO 1a Instancia	
	if ( ("REC0360".equals(j_sigFuncao))  ) {
		setStatusValido("15") ;
		setStatusTPCVValido("") ;		
		setNFuncao("Atualizar data de Publica��o") ;
		setTipoReqValidos("1P,1C") ;
		setStatusReqValido("4") ;	
		setOrigemEvento("100") ;
		setCodEvento("383") ;	
	}    	
	// Enviar DO 2a Instancia	
	if ( ("REC0460".equals(j_sigFuncao))  ) {
		setStatusValido("35") ;
		setStatusTPCVValido("") ;		
		setNFuncao("Atualizar data de Publica��o") ;
		setTipoReqValidos("2P,2C") ;
		setStatusReqValido("4") ;	
		setOrigemEvento("100") ;
		setCodEvento("390") ;
	}    

	return ;	
  } 
//--------------------------------------------------------------------------  
}
