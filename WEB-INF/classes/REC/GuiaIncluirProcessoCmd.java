package REC;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GuiaIncluirProcessoCmd extends sys.Command {
	private String next;
	private static final String jspPadrao = "/REC/GuiaIncluirProcesso.jsp";

	public GuiaIncluirProcessoCmd() {
		next = jspPadrao;
	}

	public GuiaIncluirProcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno = next;
		try {
			sys.Command cmd = (sys.Command) Class.forName("REC.ProcessaAutoCmd").newInstance();
			cmd.setNext(this.next);
			nextRetorno = cmd.execute(req);
			// cria os Beans de sessao, se n�o existir
			HttpSession session = req.getSession();
			ACSS.UsuarioBean UsrLogado = (ACSS.UsuarioBean) session.getAttribute("UsuarioBeanId");
			if (UsrLogado == null)UsrLogado = new ACSS.UsuarioBean();

			ACSS.UsuarioFuncBean usrFunc = (ACSS.UsuarioFuncBean) session.getAttribute("UsuarioFuncBeanId");
			if (usrFunc == null) usrFunc = new ACSS.UsuarioFuncBean();

			ParamOrgBean ParamOrgaoId = (ParamOrgBean) session.getAttribute("ParamOrgBeanId");
			if (ParamOrgaoId == null)ParamOrgaoId = new ParamOrgBean();

			AutoInfracaoBean AutoInfracaoBeanId =(AutoInfracaoBean) req.getAttribute("AutoInfracaoBeanId");
			RequerimentoBean RequerimentoId = (RequerimentoBean) req.getAttribute("RequerimentoId");
			if (RequerimentoId == null)	RequerimentoId = new RequerimentoBean();
			
			GuiaIncluirProcessoBean GuiaIncluirProcessoBeanId =	(GuiaIncluirProcessoBean) req.getAttribute("GuiaIncluirProcessoBeanId");
			if (GuiaIncluirProcessoBeanId == null)	GuiaIncluirProcessoBeanId = new GuiaIncluirProcessoBean();
			GuiaIncluirProcessoBeanId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));			
			
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");
			session.setAttribute("codReq", "");
			if (acao == null){
				acao = " ";
			}

			String sigFuncao = usrFunc.getJ_sigFuncao();

			/* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia */
			GuiaDistribuicaoBean GuiaDistribuicaoId  = new GuiaDistribuicaoBean();
			GuiaDistribuicaoId.setControleGuia(ParamOrgaoId.getParamOrgao("CONTROLE_DE_GUIA","N","7"));
			
			if ((acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length() == 0)) {
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				//Bahia
				GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,AutoInfracaoBeanId, sigFuncao,"",UsrLogado);
				session.setAttribute("codReq", GuiaIncluirProcessoBeanId.getCodReq());
			}
			
			if (acao.equals("IncluirProcesso")) {
				
				String codLoteRelator = req.getParameter("codLoteRelator");
				String codReq = req.getParameter("codReq");
				String existeGuia = "";
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				//Bahia
				
				existeGuia = GuiaIncluirProcessoBeanId.ConsultaReqGuia(AutoInfracaoBeanId,codReq);
				session.setAttribute("codReq", codReq);
					
				GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,AutoInfracaoBeanId,sigFuncao,"",UsrLogado);
				GuiaIncluirProcessoBeanId.incluirProcessoGuia(AutoInfracaoBeanId,codLoteRelator,codReq,UsrLogado,existeGuia);
				if (AutoInfracaoBeanId.getMsgErro().length()==0){
					AutoInfracaoBeanId.setMsgErro("Requerimento inclu�do na guia " + codLoteRelator + " com sucesso.");
					AutoInfracaoBeanId.setMsgOk("S") ;
				}
			}	

			//Bahia
			if (acao.equals("LeReq")) {
				String codReq = req.getParameter("codReq");
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				GuiaIncluirProcessoBeanId.setEventoOK(GuiaDistribuicaoId,AutoInfracaoBeanId,sigFuncao,codReq,UsrLogado);
				session.setAttribute("codReq", codReq);
			}	

			req.setAttribute("AutoInfracaoBeanId", AutoInfracaoBeanId);
			req.setAttribute("GuiaIncluirProcessoBeanId", GuiaIncluirProcessoBeanId);
		} catch (Exception ue) {
			throw new sys.CommandException("GuiaIncluirProcessoCmd1: " + ue.getMessage());
		}
		return nextRetorno;
	}

}
