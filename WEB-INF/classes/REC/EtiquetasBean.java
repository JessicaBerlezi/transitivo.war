package REC;

import java.util.Iterator;

/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Parecer Juridico Bean<br>
* <b>Description:</b>  Parecer Juridico Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Elisson Dias
* @version 1.0
*/

public class EtiquetasBean   { 

  private String eventoOK;  
  private String msg;
  private String reqs;
  
  public EtiquetasBean()  throws sys.BeanException {

    eventoOK        = "N";
    msg             = "";
    reqs            = "";	
  
  }
  
//--------------------------------------------------------------------------  
  public void setEventoOK(String eventoOK)   {
  	if (eventoOK==null) eventoOK="N";
  	this.eventoOK = eventoOK ;
   }   
   public void setEventoOK(REC.AutoInfracaoBean myAuto) throws sys.BeanException   {
	   this.msg      = "";
	   this.eventoOK = "N";	
	   if ("".equals(myAuto.getNomStatus())) return ;
	   this.msg = myAuto.getNomStatus()+" ("+myAuto.getDatStatus()+")";
	   try {
			Iterator it = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean req  = new REC.RequerimentoBean();
			boolean existe = false ;
			while (it.hasNext()) {
				req = (REC.RequerimentoBean)it.next();
				if("TR,DP,DC,1P,1C,2P,2C,RD,DI,1I,2I".indexOf(req.getCodTipoSolic())>=0) {
					existe = true ;
					break;
				}							   			
			}
			if (existe)  this.eventoOK="S";	
			else 	this.msg += " - Sem Requerimento de TRI,DP,DC,1P,1C,2P,2C,RD,DI,1I,2I para reemitir etiqueta.";					   	
	   }	   
	   catch (Exception ue) {
         throw new sys.BeanException("EtiquetasBean: " + ue.getMessage());
	   }
	  return ;	
  } 
  public String getEventoOK()   {
   	return this.eventoOK;
   } 
//--------------------------------------------------------------------------  
  public String getMsg()   {
	return msg ;	
  }
//--------------------------------------------------------------------------  
  public void setReqs(REC.AutoInfracaoBean myAuto) throws sys.BeanException {
	this.reqs="";
  	String aux="";
    for(int f=0;f<35;f++) 	aux=aux+"&nbsp;";
	StringBuffer popupBuffer = new StringBuffer();
	popupBuffer.append("<select style='border-style: outset;' name='codRequerimentoAuto' size='1'> \n");	  
	//popupBuffer.append("<OPTION VALUE=''>"+aux+"</OPTION> \n");	    			  
	String seleciona="";	
    try {
	  Iterator it = myAuto.getRequerimentos().iterator() ;
	  REC.RequerimentoBean req  = new REC.RequerimentoBean();
	  String nom = "";
	  String sel = " selected ";	  
	  while (it.hasNext()) {
	  	req =(REC.RequerimentoBean)it.next() ;
		if("TR,DP,DC,1P,1C,2P,2C,RD,DI,1I,2I".indexOf(req.getCodTipoSolic())>=0) {
			nom=req.getCodTipoSolic() + " " + req.getNumRequerimento()+" ("+req.getDatRequerimento()+")";
			popupBuffer.append("<OPTION VALUE='" + req.getCodRequerimento() + "'" + sel + ">" + nom + "</OPTION> \n");
			sel="";
		}  
	  }
    }
    catch (Exception ex) {
	  	throw new sys.BeanException(ex.getMessage());
    }
    popupBuffer.append("</SELECT> \n");	  
	this.reqs = popupBuffer.toString() ;
  }
  public String getReqs()   {
  	return (("S".equals(this.eventoOK)) ? this.reqs : this.msg) ;
   } 
  
//--------------------------------------------------------------------------  
}
