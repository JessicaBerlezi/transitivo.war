package REC;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sys.BeanException;
import ACSS.UsuarioBean;

public class RemessaIncluirProcBean {

	private String eventoOK;
	private String msg;
	private String comboReqs;
	private String comboGuias;
	private List guias;
    private String codEvento ;
	private String codReq ;

	public RemessaIncluirProcBean() throws sys.BeanException {

		eventoOK = "N";
		msg = "";
		comboReqs = "";
		comboGuias = "";
		codEvento = "208";
		guias = new ArrayList();

	}

	//--------------------------------------------------------------------------  
	public void setEventoOK(String eventoOK) {
		if (eventoOK == null)
			eventoOK = "N";
		this.eventoOK = eventoOK;
	}

	public void setCodReq(String CodReq) {
		this.codReq = CodReq;
	}


	public void setEventoOK(REC.AutoInfracaoBean myAuto, String sigFuncao, String codReq,UsuarioBean UsrLogado)
		throws sys.BeanException {
		this.msg = "";
		this.eventoOK = "N";
		if ("".equals(myAuto.getNomStatus()))
			return;
		this.msg = myAuto.getNomStatus() + " (" + myAuto.getDatStatus() + ")";
		
		String tipoSolic = "";
		Iterator itSolic = myAuto.getRequerimentos().iterator();
		REC.RequerimentoBean reqSolic = new REC.RequerimentoBean();
		while (itSolic.hasNext()){
			reqSolic = (REC.RequerimentoBean) itSolic.next();
			if (reqSolic.getNumRequerimento().equals(codReq)){
				tipoSolic = reqSolic.getCodTipoSolic();
				break;
			}
		}

		boolean existe = false;
		boolean temGuia = false;
		String existeGuia = "";
		if ("DP,DC".indexOf(tipoSolic)>=0) {
            this.codEvento="208" ;
			if (!myAuto.getCodStatus().equals("5")) {

				this.msg += " - N�o � poss�vel incluir processos deste auto na guia.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();

				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();
					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					if (codReq.equals(req.getNumRequerimento())){
						existeGuia = ConsultaReqRemessa(myAuto,req.getNumRequerimento());
						this.codReq = req.getNumRequerimento();	
						if (existeGuia.length()>0) {	
							this.msg = "Requerimento incluido na Guia " + existeGuia;
						}
						
							if (req.getCodStatusRequerimento().equals("0")
								|| req.getCodStatusRequerimento().equals("1")
								|| req.getCodStatusRequerimento().equals("2")) {
								existe = true;
								consultaRemessaIncluirProcesso(tipoSolic);
								if (!guias.isEmpty()) {
									temGuia = true;
								}
								
								break;
							}
							if (req.getCodStatusRequerimento().equals("3")) {
								existe = true;
								consultaRemessaIncluirProcesso(tipoSolic);
								if (!guias.isEmpty()) {
									temGuia = true;
								}
								break;
							}
							if (req.getCodStatusRequerimento().equals("4")) {
								existe = true;
								consultaRemessaIncluirProcesso(tipoSolic);
								if (!guias.isEmpty()) {
										temGuia = true;
								}
								break;
							}
					}
				
				}
			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		if ("1P,1C".indexOf(tipoSolic)>=0) {
			this.codEvento="328" ;
			if (!myAuto.getCodStatus().equals("15") && !myAuto.getCodStatus().equals("16")) {

				this.msg += " - N�o � poss�vel incluir processos deste auto na guia.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();
				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();

					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					
					if (codReq.equals(req.getNumRequerimento())){

						if (req.getCodStatusRequerimento().equals("0")
							|| req.getCodStatusRequerimento().equals("1")
							|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
								temGuia = true;
							}								
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
							
						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
									temGuia = true;
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		if ("2P,2C".indexOf(tipoSolic)>=0) {
			this.codEvento="335" ;
			if (!myAuto.getCodStatus().equals("35") && !myAuto.getCodStatus().equals("36")) {

				this.msg += " - N�o � poss�vel incluir processos deste auto na guia.";
				return;
			}
			try {
				Iterator it = myAuto.getRequerimentos().iterator();
				REC.RequerimentoBean req = new REC.RequerimentoBean();

				while (it.hasNext()) {
					req = (REC.RequerimentoBean) it.next();
					if ("".equals(codReq)){
						temGuia = true;
						existe = true;
						this.msg = "";
						break;
					}
					if (codReq.equals(req.getNumRequerimento())){

						if (req.getCodStatusRequerimento().equals("0")
							|| req.getCodStatusRequerimento().equals("1")
							|| req.getCodStatusRequerimento().equals("2")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
								
							break;
						}
						if (req.getCodStatusRequerimento().equals("3")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
								temGuia = true;
							}
							break;
							
						}
						if (req.getCodStatusRequerimento().equals("4")) {
							existe = true;
							consultaRemessaIncluirProcesso(tipoSolic);
							if (!guias.isEmpty()) {
									temGuia = true; 
							}
							break;
						}
					}

				}

			} catch (Exception ue) {
				throw new sys.BeanException("GuiaIncluirProcessoBean: " + ue.getMessage());
			}
		}

		
		if (existe && temGuia)
			this.eventoOK = "S";
		else if (existe && !temGuia)
			this.msg += " - N�o existe guia dispon�vel para incluir novos processos.";
		else
			this.msg
				+= " - N�o existe requerimento para este auto em status v�lido para inclu�-lo na guia.";

	}
	public String getEventoOK() {
		return this.eventoOK;
	}
	//--------------------------------------------------------------------------  
	public String getMsg() {
		return msg;
	}

	//--------------------------------------------------------------------------  
	public String getCodReq() {
		return codReq;
	}

	//--------------------------------------------------------------------------  
	public void setComboGuias(REC.AutoInfracaoBean myAuto) throws sys.BeanException {
		this.comboGuias = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append(
			"<select style='border-style: outset;' name='codRemessa' size='1'> \n");	    			  
		try {
			Iterator it = guias.iterator();
			String nom = "";
			String cod = "";
			while (it.hasNext()) {
				
				StatusBean status = (StatusBean) it.next();
				cod = status.getCodStatus();
				nom = status.getNomStatus();

				if (nom.trim().equals("0")){
					nom = "Envio";
			    }else if (nom.trim().equals("1")){
			    	nom = "Recebimento";
			    }
				
				popupBuffer.append("<OPTION VALUE='" + cod + "'" + "> Guia: "+ cod + " - Status: " + nom + "</OPTION> \n");
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboGuias = popupBuffer.toString();
	}
	public String getComboGuias() {
		return (("S".equals(this.eventoOK)) ? this.comboGuias : this.msg);
	}

	public void setComboReqs(REC.AutoInfracaoBean myAuto,String codReq) throws sys.BeanException {
		this.comboReqs = "";
		String aux = "";
		for (int f = 0; f < 35; f++)
			aux = aux + "&nbsp;";
		StringBuffer popupBuffer = new StringBuffer();
		popupBuffer.append(
                                                                    	  //Bahia
            "<select style='border-style: outset;' name='codReq' size='1'onChange='javascript: valida(\"LeReq\",this.form)'> \n");	    			  
		try {
			Iterator it = myAuto.getRequerimentos().iterator();
			REC.RequerimentoBean req = new REC.RequerimentoBean();
			String nom = "";
			popupBuffer.append("<OPTION selected VALUE='' ></OPTION> \n");
			while (it.hasNext()) {				
				req =(REC.RequerimentoBean)it.next() ;
				    if (req.getCodStatusRequerimento().equals("0") ||
					    req.getCodStatusRequerimento().equals("1") ||				
					    req.getCodStatusRequerimento().equals("2") ||				
					    req.getCodStatusRequerimento().equals("3") ||				
					    req.getCodStatusRequerimento().equals("4")) {				
					nom=req.getCodTipoSolic() + " " + req.getNumRequerimento()+" ("+req.getDatRequerimento()+")";
					if (req.getNumRequerimento().equals(codReq))
      					popupBuffer.append("<OPTION selected VALUE='" + req.getNumRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                else
    					popupBuffer.append("<OPTION VALUE='" + req.getNumRequerimento() + "'" + ">" + nom + "</OPTION> \n");
	                
				}			
			}
		} catch (Exception ex) {
			throw new sys.BeanException(ex.getMessage());
		}
		popupBuffer.append("</SELECT> \n");
		this.comboReqs = popupBuffer.toString();
	}
	
	public String getComboReqs() {
		return (("S".equals(this.eventoOK)) ? this.comboReqs : "&nbsp;");
	}


	public void consultaRemessaIncluirProcesso(String tipoSolic)
		throws BeanException {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			this.guias = dao.consultaRemessaIncluirProcesso(tipoSolic);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}
	public void incluirProcessoRemessa(AutoInfracaoBean myAuto, 
	String codRemessa, String codReq,ACSS.UsuarioBean UsrLogado,String existeGuia)
		throws BeanException  {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			dao.incluirProcessoRemessa(myAuto,codRemessa, codReq,codEvento,UsrLogado, existeGuia);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}


	public String ConsultaReqRemessa(AutoInfracaoBean myAuto, 
	String codReq)
		throws BeanException  {
		try {
			GuiaDaoBroker dao = GuiaDaoBroker.getInstance();
			return dao.ConsultaReqRemessa(myAuto,codReq);
		} catch (Exception e) {
			throw new sys.BeanException(e.getMessage());
		}
	}



	//--------------------------------------------------------------------------  
}
