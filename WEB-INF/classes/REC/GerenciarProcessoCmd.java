package REC;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.util.CalendarComparator;

import sys.ControladorServlet;
import ACSS.Ata;
import ACSS.ParamSistemaBean;

public class GerenciarProcessoCmd extends  sys.Command{
	private String next;
	private static final String jspPadrao="/REC/AbrirProcesso.jsp" ;
	public GerenciarProcessoCmd() {
		next             =  jspPadrao;
	}
	public GerenciarProcessoCmd(String next) {
		this.next = next;
	}

	public String execute(HttpServletRequest req) throws sys.CommandException {
		String nextRetorno  = next ;
		String sMensagem    = "";
		try {  
			req.getSession().setAttribute("checkNumeroProcesso", "");

			// cria os Beans de sessao, se n�o existir
			HttpSession session   = req.getSession() ;	

			ACSS.UsuarioBean UsrLogado                      = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
			if (UsrLogado==null)  UsrLogado                 = new ACSS.UsuarioBean() ;	  			
			ACSS.UsuarioFuncBean UsuarioFuncBeanId          = (ACSS.UsuarioFuncBean)session.getAttribute("UsuarioFuncBeanId") ;
			if (UsuarioFuncBeanId==null)  UsuarioFuncBeanId = new ACSS.UsuarioFuncBean() ;	  				
			ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
			if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;
			ParamSistemaBean parSis = (ParamSistemaBean) session.getAttribute("ParamSistemaBeanId");
			if (parSis == null)	parSis = new ParamSistemaBean();
			consultaAutoBean consultaAutoId                 = (consultaAutoBean)session.getAttribute("consultaAutoId") ;
			if (consultaAutoId==null)  	consultaAutoId       = new consultaAutoBean() ;	  		    
			consultaAutoId.setAbrevSist(UsuarioFuncBeanId.getAbrevSistema());	
			consultaAutoId.setSigFuncao(req.getParameter("j_sigFuncao"));	

			//Ler auto

			AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
			if (AutoInfracaoBeanId==null)  	{
				AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
				if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
			}

			AbrirRecursoDPBean AbrirRecursoBeanId     = (AbrirRecursoDPBean)req.getAttribute("AbrirRecursoBeanId") ;
			if (AbrirRecursoBeanId==null)  AbrirRecursoBeanId = new AbrirRecursoDPBean() ;	 


			String acao = req.getParameter("acao");  

			if (acao==null)	{
				session.removeAttribute("consultaAutoId") ;
				consultaAutoId       = new consultaAutoBean() ;	  		      
				acao = " ";
			}
			if (acao.equals("ValidaAta"))	{
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				String retorno = "";
				String tpRequerimento = req.getParameter("tpRequerimento"); //Def. Previa
				String codRelator = req.getParameter("idCpfAtaRelator"); //303
				String dsRelator = req.getParameter("dsRelator"); //JOS� ANTONIO LAMPE DA SILVA

				String idAta =  req.getParameter("idNrAtaName");//49
				String txtAta  = req.getParameter("dsAta");//15DP/2015
				String txtProcesso = req.getParameter("numProcesso");//00487/2015

				Ata ata = new Ata();
				if(ata.getVerificaQtdeNrProcesso(idAta).equals("N_OK_ATEN��O")){ //_OK
					retorno = "N_OK";
				}else{
					retorno = "OK";
				}

				ControladorServlet.requestG.setAttribute("chave", retorno);
				ControladorServlet.requestG.setAttribute("chave_idAta", idAta);
				ControladorServlet.requestG.setAttribute("chave_tp_requerimento", tpRequerimento);
				ControladorServlet.requestG.setAttribute("chave_cd_relator", codRelator);
				ControladorServlet.requestG.setAttribute("chave_ds_relator", dsRelator);

				ControladorServlet.requestG.setAttribute("chave_tx_ata", txtAta);
				ControladorServlet.requestG.setAttribute("chave_tx_processo", txtProcesso);


			}
			
			if (acao.equals("NovoProcesso")) {
				String codNatureza = req.getParameter("cdNatureza");
				String numeroProcesso = req.getParameter("numProcesso");
				String numeroAuto = req.getParameter("numAutoInfracao");
				
				if(codNatureza.equals("1")) {
					boolean numeroProcessoJaExiste = AutoInfracaoBean.verificarNumeroProcesso(numeroProcesso, numeroAuto);
					
					if(numeroProcessoJaExiste) {
						req.getSession().setAttribute("checkNumeroProcesso", "O n�mero de processo informado j� est� em uso.");

						return "/REC/AbrirProcesso.jsp";
					}
				} else {
					numeroProcesso = new Ata().getNumProcAuto("258470", "ULT_NR_PROCESSO_JARI");
					
					AutoInfracaoBeanId.setNumProcesso("PMME/" + numeroProcesso);
				}
				
				if(numeroProcesso.isEmpty()) {
					req.getSession().setAttribute("checkNumeroProcesso", "O n�mero de processo � obrigat�rio.");

					return "/REC/AbrirProcesso.jsp";
				}

				String tpRequerimento = req.getParameter("tpRequerimento");
				String numAuto = req.getParameter("numAutoInfracao");

				AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));  
				AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));  
				AutoInfracaoBeanId.setNumProcesso(req.getParameter("numProcesso"));
				String numRenavam = req.getParameter("numRenavam");
				
				if (numRenavam==null)
					numRenavam="";
				
				AutoInfracaoBeanId.setNumRenavam(numRenavam);
				String codAcessoWeb = req.getParameter("numAcesso");
				
				if (codAcessoWeb==null)
					codAcessoWeb="";
				
				AutoInfracaoBeanId.setCodAcessoWeb(codAcessoWeb);
				String temp = UsrLogado.getCodOrgaoAtuacao();
				
				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0)
					UsrLogado.setCodOrgaoAtuacao("999999");

				AutoInfracaoBeanId.LeAutoInfracao("codigo",UsrLogado)	;
				AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
				AbrirRecursoBeanId.setEventoOK(ParamOrgaoId,AutoInfracaoBeanId) ;
				AutoInfracaoBeanId.LeHistorico(UsrLogado) ;

				AutoInfracaoBeanId.setIndSituacao("");
				AutoInfracaoBeanId.setCdNatureza(UsrLogado.cdNatureza);
				nextRetorno = processaDefesa(req,ParamOrgaoId,AutoInfracaoBeanId,UsrLogado,AbrirRecursoBeanId);

				//Definindo relator - inicio
				RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId");
				
				if (RequerimentoId==null)
					RequerimentoId = new RequerimentoBean();
				
				DefinirRelatorBean DefinirRelatorId = (DefinirRelatorBean)req.getAttribute("DefinirRelatorId");
				
				if (DefinirRelatorId==null)
					DefinirRelatorId = new DefinirRelatorBean();
				
				DefinirRelatorId.setJ_sigFuncao(req.getParameter("j_sigFuncao"), ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));
				
				DefinirRelatorId.setEventoOK(AutoInfracaoBeanId);

				nextRetorno = processaTroca(DefinirRelatorId, req, RequerimentoId, AutoInfracaoBeanId, UsrLogado);

				req.setAttribute("LeAI","S");
				acao = " ";
				AutoInfracaoBeanId = new AutoInfracaoBean();
				consultaAutoId = new consultaAutoBean();
				//					    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
				//					    req.setAttribute("RequerimentoId",RequerimentoId) ;		
				//					    req.setAttribute("DefinirRelatorId",DefinirRelatorId) ;	
				//Definindo relator - fim
			} else {
				if ("Classifica,ConsultaAuto,MostraAuto,imprimirInf,ImprimeAuto,RecebeNotif,ImprimeNots".indexOf(acao)<0){
					sys.Command cmd = (sys.Command) Class.forName("REC.ProcessaAutoCmd").newInstance();
					
					cmd.setNext(this.next);
					
					nextRetorno = cmd.execute(req);
				}		
			}

			//Carrego os Beans  
			consultaAutoId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));		
			consultaAutoId.setNumCPFEdt(req.getParameter("numCpf"));
			consultaAutoId.setNumPlaca(req.getParameter("numPlaca"));
			consultaAutoId.setNumRenavam(req.getParameter("numRenavam"));
			consultaAutoId.setNumProcesso(req.getParameter("numProcesso"));
			consultaAutoId.setDatInfracaoDe(req.getParameter("De"));
			consultaAutoId.setDatInfracaoAte(req.getParameter("Ate"));
			consultaAutoId.setIndFase(req.getParameter("indFase"));
			String Sit9 = req.getParameter("Sit9");


			//Ler auto

			AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));  
			AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca"));  
			AutoInfracaoBeanId.setNumProcesso(req.getParameter("numProcesso"));
			String numRenavam = req.getParameter("numRenavam");
			if (numRenavam==null) numRenavam="";
			AutoInfracaoBeanId.setNumRenavam(numRenavam);
			String codAcessoWeb = req.getParameter("numAcesso");
			if (codAcessoWeb==null) codAcessoWeb="";
			AutoInfracaoBeanId.setCodAcessoWeb(codAcessoWeb);
			String temp = UsrLogado.getCodOrgaoAtuacao();
			//			if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999");



			if (Sit9==null) Sit9="";
			if ("9".equals(Sit9)) consultaAutoId.setIndSituacao("9");
			else {
				String Sit1 = req.getParameter("Sit1");
				if (Sit1==null) Sit1="";
				String Sit2 = req.getParameter("Sit2");
				if (Sit2==null) Sit2="";
				String Sit3 = req.getParameter("Sit3");
				if (Sit3==null) Sit3="";
				String Sit4 = req.getParameter("Sit4");
				if (Sit4==null) Sit4="";
				consultaAutoId.setIndSituacao(Sit1+Sit2+Sit3+Sit4);
			}

			/*Agravo*/
			if (consultaAutoId.getSigFuncao().equals("REC0610")) consultaAutoId.setIndSituacao("5"); 

			consultaAutoId.setIndPago(req.getParameter("indPago"));
			consultaAutoId.setCodOrgaoAtuacao(UsrLogado.getCodOrgaoAtuacao());
			if (acao.equals("Classifica")) 
				consultaAutoId.Classifica(req.getParameter("ordem"));

			if (acao.equals("ConsultaAuto")) {
				String numProcessoOriginal=consultaAutoId.getNumProcesso();

				if (numProcessoOriginal==null)
					numProcessoOriginal = "";

				if(!"".equals(numProcessoOriginal)){
					String numprocessoA="";
					int pos =numProcessoOriginal.indexOf("A/");

					if (pos>=0)
						consultaAutoId.ConsultaAutos(UsrLogado);				
					else{
						pos=numProcessoOriginal.lastIndexOf("/");

						if (pos<0)
							numprocessoA=numProcessoOriginal+"A";
						else
							numprocessoA=numProcessoOriginal.substring(0,pos) + "A" + numProcessoOriginal.substring((pos), numProcessoOriginal.length());

						consultaAutoId.setNumProcesso(numprocessoA);
						consultaAutoId.ConsultaAutos(UsrLogado);

						if (consultaAutoId.getAutos().size()>0) 
							req.setAttribute("processoA","EXISTE PROCESSO COM MESMO N�MERO + 'A'.");					

						consultaAutoId.setNumProcesso(numProcessoOriginal);
						consultaAutoId.ConsultaAutos(UsrLogado);
					}
				}else
					consultaAutoId.ConsultaAutos(UsrLogado);			

				if ("1".equals(UsuarioFuncBeanId.getIndRecebNot())==true && consultaAutoId.getSigFuncao().equals("REC0600"))
				{		
					consultaAutoId.ConsultaAutosPostos(UsrLogado);		 
				}
				if (consultaAutoId.getAutos().size()==0) { 
					if ("REC0680".equals(consultaAutoId.getSigFuncao()))
						req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
					else
						req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");
				}


				try{
					AutoInfracaoBeanId.LeAutoInfracao("codigo",UsrLogado);
					//					UsrLogado.setCodOrgaoAtuacao(temp);
					AutoInfracaoBeanId.LeRequerimento(UsrLogado);
				}catch (Exception e) {
					nextRetorno = "/REC/AbrirProcesso.jsp";
				}
			}		 

			if (acao.equals("Novo"))  		  consultaAutoId = new consultaAutoBean() ;
			if ((acao.equals("MostraAuto")) || (acao.equals("imprimirInf")) )  {

				//				AutoInfracaoBean AutoInfracaoBeanId = new AutoInfracaoBean() ;


				if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				if (acao.equals("MostraAuto")) { 
					AutoInfracaoBeanId.setNumPlaca(req.getParameter("mostraplaca"));				
					AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("mostranumauto"));		
					AutoInfracaoBeanId.setAutoAntigo(req.getParameter("mostraAutoAntigo"));
					nextRetorno = "/REC/AbrirConsultaAuto.jsp" ;
				}
				else {

					AutoInfracaoBeanId.setNumPlaca(req.getParameter("numPlaca")); 
					AutoInfracaoBeanId.setNumAutoInfracao(req.getParameter("numAutoInfracao"));
					AutoInfracaoBeanId.LeFotoDigitalizada() ; 
					nextRetorno = "/REC/AutoImp.jsp" ;
				}					 
				// Verifica se o Usuario logado ve Todos os Orgaos
				//				if ("Ss1".indexOf(UsuarioFuncBeanId.getIndVisTodosOrg())>=0) UsrLogado.setCodOrgaoAtuacao("999999");
				AutoInfracaoBeanId.LeAutoInfracao(UsrLogado);
				String codInfracaoAdm = ParamOrgaoId.getParamOrgao("COD_INFRACAO_ADM","69200,99800","10");
				if(codInfracaoAdm.indexOf(AutoInfracaoBeanId.getInfracao().getCodInfracao())>=0)
					AutoInfracaoBeanId.setTemAIDigitalizado(true);
				String codAgente=parSis.getParamSist("CODIGO_AGENTE_PALM");
				if  ( (codAgente.indexOf(AutoInfracaoBeanId.getCodAgente())>=0) &&
						(AutoInfracaoBeanId.getCodAgente().length()==2)
						)
					AutoInfracaoBeanId.setTemAIDigitalizado(true);  				
				if (temp.equals("258650"))
				{
					if (AutoInfracaoBeanId.getIdentOrgao().trim().length()>0)
					{
						/*Palm - Radar*/
						if ( (AutoInfracaoBeanId.getIdentOrgao().substring(1,2).equals("1")) 
								)
							AutoInfracaoBeanId.setTemAIDigitalizado(true);
					}
				}			
				UsrLogado.setCodOrgaoAtuacao(temp);	
				AutoInfracaoBeanId.LeRequerimento(UsrLogado);  
				//AutoInfracaoBeanId.LeHistorico(UsrLogado);     
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
			}

			if  (acao.equals("ImprimeAuto"))  {		
				if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS COM OS DADOS FORNECIDOS");
				nextRetorno = "/REC/consultaAutoImp.jsp" ;
			}	

			if  (acao.equals("ImprimeNots"))  {
				if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
				else {
					processaImprimeNotif(consultaAutoId,ParamOrgaoId,UsrLogado);
					nextRetorno = "/REC/ImprimeNotificacoes.jsp" ;
				}			
			}		

			if  (acao.equals("RecebNotif"))  {				
				if (consultaAutoId.getAutos().size()==0) req.setAttribute("semAuto","N�O EXISTEM AUTOS PENDENTES DE RECEBIMENTO COM OS DADOS FORNECIDOS");
				else {
					consultaAutoId.setMsgErro("");
					processaRecebeNotif(consultaAutoId,ParamOrgaoId,UsrLogado);
					if (consultaAutoId.getMsgErro().length()==0) { 
						consultaAutoId.setMsgErro("RECEBIDOS "+consultaAutoId.getAutos().size()+" AUTOS.");
						consultaAutoId.setMsgOk("S");
					}				
				}			
			}			

			if (acao.equals("R"))	{
				nextRetorno ="" ;
				consultaAutoId       = new consultaAutoBean() ;	  				
				session.removeAttribute("consultaAutoId");
			}
			else
				session.setAttribute("consultaAutoId",consultaAutoId) ;
			String test = (String) session.getAttribute("retornoIdAta");
		}
		catch (Exception ue) {
			throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
		}
		return nextRetorno;
	}


	private Ata Ata() {
		// TODO Auto-generated method stub
		return null;
	}
	public void processaImprimeNotif(consultaAutoBean consultaAutoId,ParamOrgBean ParamOrgaoId,
			ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
		String sMensagem="";
		try {
			consultaAutoId.setMsgErro("IMPRESSOS "+consultaAutoId.getAutos().size()+" AUTOS.");	
			//Seta o c�digo da Notifica��o para passar para o Notificacao.jsp
			Iterator it  = consultaAutoId.getAutos().iterator() ;
			List notifs  = new ArrayList() ;		
			int posNot = 0 ;

			while (it.hasNext()) {
				REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();				
				not   = (REC.AutoInfracaoBean)it.next() ;	
				// temos que ler todos os dados da notificacao e atualizar o objetio na lista
				/*Org�o de Autua��o-Armazenando Org�o*/
				String temp = UsrLogado.getCodOrgaoAtuacao();				
				UsrLogado.setCodOrgaoAtuacao("999999");
				not.LeAutoInfracao(UsrLogado);
				consultaAutoId.getAutos().set(posNot,not);
				InstrNotBean NotifBeanId = new InstrNotBean();
				ACSS.UsuarioBean UsrLogadoRec       = new ACSS.UsuarioBean() ;	
				ParamOrgBean ParamOrgaoRecId        = new ParamOrgBean() ;
				UsrLogadoRec.setCodOrgaoAtuacao(not.getCodOrgao());
				ParamOrgaoRecId.PreparaParam(UsrLogadoRec);			  		

				not.LeHistorico(UsrLogado);				
				NotifBeanId.setCodNotificacao(not.getCodNotificacao(ParamOrgaoId));  	
				NotifBeanId.setCodOrgao(UsrLogado.getCodOrgaoAtuacao());
				NotifBeanId.setTxtEnderecoRecurso(ParamOrgaoRecId.getParamOrgao("ENDERECO_RECURSO"));				
				NotifBeanId.LeNotificacoes();
				not.LeFotoDigitalizada(); 
				notifs.add(NotifBeanId);
				UsrLogado.setCodOrgaoAtuacao(temp);				
				posNot++;
			}
			consultaAutoId.setNotifs(notifs);
		}
		catch (Exception ue) {
			throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
		}
	}


	public void processaRecebeNotif(consultaAutoBean consultaAutoId,ParamOrgBean ParamOrgaoId,
			ACSS.UsuarioBean UsrLogado) throws sys.CommandException {
		String sMensagem="";		
		try {	
			//Seta o c�digo da Notifica��o para passar para o Notificacao.jsp
			Iterator it  = consultaAutoId.getAutos().iterator() ;	
			DaoBroker dao = DaoBrokerFactory.getInstance();
			REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();
			HistoricoBean myHis = new HistoricoBean() ;	
			String proxStatus   = "";
			String codEvento    = "204";
			Vector vErro = new Vector();

			while (it.hasNext()) {
				myAuto   = (REC.AutoInfracaoBean)it.next() ;

				if (myAuto.getDatStatus().length()==0) myAuto.setDatStatus(sys.Util.formatedToday().substring(0,10)); 				

				myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
				myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
				myHis.setNumProcesso(myAuto.getNumProcesso());
				if (myAuto.getCodStatus().length()==1) {
					proxStatus = "4";	
					codEvento    = "204";				
				}
				else {
					proxStatus = "14";
					codEvento    = "324";
				}
				myHis.setCodStatus(proxStatus);  
				myHis.setDatStatus(sys.Util.formatedToday().substring(0,10));
				myHis.setCodEvento(codEvento);
				myHis.setCodOrigemEvento("100");
				myHis.setNomUserName(UsrLogado.getNomUserName());
				myHis.setCodOrgaoLotacao(UsrLogado.getCodOrgaoAtuacao());
				myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
				myHis.setTxtComplemento01(sys.Util.formatedToday().substring(0,10));	
				myHis.setTxtComplemento02("34");
				dao.atualizarAuto204(myAuto,myHis,UsrLogado) ; 
				if (myAuto.getMsgErro().length()>0) 
					vErro.addElement("Auto: "+myAuto.getNumAutoInfracao()+" - "+myAuto.getMsgErro()+" /n");				
			}
			if (vErro.size()>0) consultaAutoId.setMsgErro(vErro);
		}
		catch (Exception ue) {
			throw new sys.CommandException("consultaAutoCmd: " + ue.getMessage()+" "+sMensagem);
		}
	} 	

	/*
	 * Raj
	 */

	protected String processaDefesa(HttpServletRequest req,ParamOrgBean ParamOrgaoId,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado,REC.AbrirRecursoDPBean AbrirRecursoBeanId) throws sys.CommandException {
		String nextRetorno=jspPadrao;
		String sMensagem  = "";
		try {
			myAuto.setMsgErro("") ;
			Vector vErro = new Vector() ;
			String datEntrada = req.getParameter("datEntrada");
			String txtMotivo = req.getParameter("txtMotivo");
			String txtProcesso = req.getParameter("numProcesso");
			String cdAta = req.getParameter("idNrAtaName");
			String txtAta = req.getParameter("dsAta");

			Ata ata = new Ata();

			if(cdAta.length()>0 && cdAta != null){
				ata.setCodAta(cdAta);
			}
			if(txtAta.length()>0 && txtAta != null){
				ata.setDescAta(txtAta);
			}

			if (datEntrada==null) datEntrada = "";

			sMensagem=datEntrada + " ";
			
			//validar data de entrada
			if (sys.Util.DifereDias(sys.Util.formatedToday().substring(0,10),datEntrada)>0)
				vErro.addElement("Data de Entrada n�o pode ser superior a hoje. \n") ;
			else {
				//				myAuto.setDatLimiteRecurso(ParamOrgaoId); 
				/*Caso notiticacao sem sucesso, n�o criticar prazo*/
				boolean bNotificacao=true;
				/*				if ( (myAuto.getCodStatus().equals("2")) ||(myAuto.getCodStatus().equals("12")) )*/
				if ("0,1,2".indexOf(myAuto.getCodStatus())>=0)		
					bNotificacao=false;

				if ( (sys.Util.DifereDias(myAuto.getDatLimiteRecursoFolga(),datEntrada)>0) && (bNotificacao) )
				{
					vErro.addElement("Prazo limite: "+myAuto.getDatLimiteRecurso() + "\n") ;	
				}
			}

			// ler radio - DC - DP 
			String solicDEF = req.getParameter("SolicDPBox");
			if (solicDEF==null) solicDEF="";
			if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
			if (solicDEF==null) solicDEF="";
			if (solicDEF.length()==0) vErro.addElement("Nenhuma solicita��o efetuada.\n") ;
			if ( (solicDEF.equals("DC")) && ("".equals(AbrirRecursoBeanId.getTpSolDC())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDC()+" \n") ;
			if ( (solicDEF.equals("DP")) && ("".equals(AbrirRecursoBeanId.getTpSolDP())) ) vErro.addElement(AbrirRecursoBeanId.getMsgDP()+" \n") ;
			String txtEMail = req.getParameter("txtEMail");
			if (txtEMail==null) txtEMail = "";

			if (vErro.size()<0) {
				myAuto.setMsgErro(vErro) ;
				nextRetorno = this.next ;
			}   
			else {
				REC.RequerimentoBean myReq = new REC.RequerimentoBean() ;						
				myAuto.setMsgErro(atualizarAuto(req,myAuto,ParamOrgaoId,UsrLogado,myReq,datEntrada,txtEMail,txtMotivo, txtProcesso,solicDEF, ata)) ;	
				if (myAuto.getMsgErro().length()==0) {					
					if ("N".equals(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2"))) { 	
						myAuto.setMsgErro("Defesa Pr�via interposta com Requerimento:"+myReq.getNumRequerimento()) ;
						myAuto.setMsgOk("S") ;
					}
					else if(ParamOrgaoId.getParamOrgao("ORG_EMT_ETQ","S","2").indexOf("R")>=0){
						AbrirRecBean AbrirRecBeanId = new AbrirRecBean();
						String nomeImagem = ParamOrgaoId.getParamOrgao("LOGO_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setNomeImagem(nomeImagem);

						String alturaImageGra = "80";
						AbrirRecBeanId.setAlturaImageGra(alturaImageGra);

						String larguraImageGra = "175";
						AbrirRecBeanId.setLarguraImageGra(larguraImageGra);

						String alturaImagePeq = "80";
						AbrirRecBeanId.setAlturaImagePeq(alturaImagePeq);

						String larguraImagePeq = "175";
						AbrirRecBeanId.setLarguraImagePeq(larguraImagePeq);

						String obs = ParamOrgaoId.getParamOrgao("OBS_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setObservacao(obs);

						String siglaProt = ParamOrgaoId.getParamOrgao("SIG_PROT_FOLHA_DE_ROSTO","N","2");
						AbrirRecBeanId.setSigProtocolo(siglaProt);

						/*Atualizar SIT_UTILIZADO e Setar o Caminho*/




						nextRetorno = "/REC/FolhaDeRosto.jsp" ;	
						req.setAttribute("AbrirRecBeanId",AbrirRecBeanId) ;	

					}else
						nextRetorno = "/REC/AbrirRecursoMostra.jsp" ;
					//Imprimir hist�rico com filtro de eventos, conforme par�metro LISTAGEM_DE_EVENTOS
					String codEventoFiltro = ParamOrgaoId.getParamOrgao("LISTAGEM_DE_EVENTOS","N","2");
					if (!codEventoFiltro.equals("N")){
						myAuto.filtraHistorico(myAuto,codEventoFiltro);
					}
					req.setAttribute("codEventoFiltro",codEventoFiltro);


				}	 							
			}
		}
		catch (Exception e){
			throw new sys.CommandException("AbrirRecursoDPCmdb: " + e.getMessage()+" "+sMensagem);
		}
		return nextRetorno  ;
	}


	public String processaTroca(DefinirRelatorBean DefinirRelatorId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		String nextRetorno = next ;
		String codEvento = HistoricoBean.EVENTO_ABRIR_DP;
		try {

			String bla = req.getParameter("acao");

			String tpRequerimento = req.getParameter("tpRequerimento");
			String codTipoSolic= "";
			String numAuto = req.getParameter("numAutoInfracao");
			String numPlaca = req.getParameter("dsPlaca");
			String codRelator = req.getParameter("idCpfAtaRelator");
			String dsRelator = req.getParameter("dsRelator");

			myAuto.setIndSituacao("");

			if(tpRequerimento.equals("Def. Previa")){
				codTipoSolic = "DP";
				myAuto.setCodStatus(AutoInfracaoBean.STATUS_DP);
				myReq.setCodTipoSolic(codTipoSolic);
				myReq.setCodStatusRequerimento("0");
				codEvento = HistoricoBean.EVENTO_ABRIR_DP;
			} else if(tpRequerimento.equals("1� INST�NCIA")){
				codTipoSolic = "1P";
				myAuto.setCodStatus(AutoInfracaoBean.STATUS_RECURSO_1P);
				myReq.setCodTipoSolic(codTipoSolic);
				myReq.setCodStatusRequerimento("0");
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_1P;
			} else if(tpRequerimento.equals("2� INST�NCIA")){
				myAuto.setCodStatus(AutoInfracaoBean.STATUS_RECURSO_2P);
				codTipoSolic = "2P";
				myReq.setCodTipoSolic(codTipoSolic);
				myReq.setCodStatusRequerimento("0");
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_2P;
			} else if(tpRequerimento.equals("P.A.E")){
				myAuto.setCodStatus(AutoInfracaoBean.STATUS_PAE);
				codTipoSolic = "PAE";
				myReq.setCodTipoSolic(codTipoSolic);
				myReq.setCodStatusRequerimento("0");
				codEvento = HistoricoBean.EVENTO_ABRIR_RECURSO_PAE;
			}

			RequerimentoBean RequerimentoId = (RequerimentoBean)req.getAttribute("RequerimentoId");
			
			if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;

			AutoInfracaoBean AutoInfracaoBeanId = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId");
			
			if (AutoInfracaoBeanId==null)  	{
				AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
				
				if  (AutoInfracaoBeanId.getProprietario()==null) AutoInfracaoBeanId.setProprietario(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getCondutor()==null)     AutoInfracaoBeanId.setCondutor(new REC.ResponsavelBean());		
				if  (AutoInfracaoBeanId.getVeiculo()==null)      AutoInfracaoBeanId.setVeiculo(new REC.VeiculoBean());		
				if  (AutoInfracaoBeanId.getInfracao()==null)     AutoInfracaoBeanId.setInfracao(new REC.InfracaoBean());		
				
				req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;		
			}

			DaoBroker dao = DaoBrokerFactory.getInstance();

			AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
			myReq.setCodRequerimento(dao.ConsultaCodRequerimento(myAuto, UsrLogado));

			myAuto.setMsgErro("");
			
			Vector vErro = new Vector();
			
			// validar se status permite recurso ou defesa previa : 5,25,35
			if ("S".equals(DefinirRelatorId.getEventoOK())==false) vErro.addElement(DefinirRelatorId.getMsg()+" \n") ;
			//		  		myReq.setCodRequerimento(req.getParameter("codRequerimento"));


			//para o Oficio Funcionar
			if (myReq.getCodRequerimento().equals("")){
				OficioBean OficioBean = new OficioBean();
				String solicDEF = req.getParameter("SolicDPBox");
				if (solicDEF==null) solicDEF="";
				if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
				if (solicDEF==null) solicDEF="";
				OficioBean.setTipoRecurso(solicDEF);
				String codRequerimento = OficioBean.verificaCodReq(myAuto,myReq,OficioBean,UsrLogado);
				myReq.setCodRequerimento(codRequerimento);
			}

			if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado. \n") ;
			if (vErro.size()==0) myReq = myAuto.getRequerimentos(myReq.getCodRequerimento()) ;
			
			myReq.setDatJU(req.getParameter("datJU"));
			
			//validar data de entrada
			//		  		if (myReq.getDatJU().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
			if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),myReq.getDatJU())>0) vErro.addElement("Data n�o pode ser superior a hoje. \n") ;
			/*
			 * Michel 20/02/2006
		   		if (sys.Util.DifereDias( myReq.getDatJU(),myAuto.getDatStatus())>0) vErro.addElement("Data n�o pode ser anterior a data do Status: "+myAuto.getDatStatus()+". \n") ;
			 */
			//		   		myReq.setCodJuntaJU(req.getParameter("codJuntaJU"));
			//		   		myReq.setCodRelatorJU(req.getParameter("codRelatorJU"));


			if(codRelator == null){
				myReq.setCodRelatorJU("0");
				myReq.setNomUserNameJU("SELECIONE");
			}else{
				if((codRelator.equals("SELECIONE")) && (dsRelator.equals("SELECIONE"))){
					myReq.setCodRelatorJU("0");
					myReq.setNomUserNameJU("SELECIONE");
				}else{
					myReq.setCodRelatorJU(codRelator);
					myReq.setNomUserNameJU(dsRelator);
				}
			}


			//		   		if ( (myReq.getCodJuntaJU().length()==0) || (myReq.getCodRelatorJU().length()==0) ) vErro.addElement("Junta/Relator n�o selecionado. \n") ;
			//			  	myReq.setNomUserNameJU(UsrLogado.getNomUserName());
			myReq.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
			myReq.setDatProcJU(sys.Util.formatedToday().substring(0,10));
			
			myReq.setDatRequerimento(ControladorRecGeneric.dateToDateBr(myReq.getDatRequerimento()));
			myReq.setDatProcDP(ControladorRecGeneric.dateToDateBr(myReq.getDatProcDP()));
			myReq.setDatPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatPJ()));
			myReq.setDatProcPJ(ControladorRecGeneric.dateToDateBr(myReq.getDatProcPJ()));
			myReq.setDatJU(ControladorRecGeneric.dateToDateBr(myReq.getDatJU()));
			myReq.setDatProcJU(ControladorRecGeneric.dateToDateBr(myReq.getDatProcJU()));
			myReq.setDatRS(ControladorRecGeneric.dateToDateBr(myReq.getDatRS()));
			myReq.setDatProcRS(ControladorRecGeneric.dateToDateBr(myReq.getDatProcRS()));
			myReq.setDatAtuTR(ControladorRecGeneric.dateToDateBr(myReq.getDatAtuTR()));
			myReq.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myReq.getDatEnvioDO()));
			myReq.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myReq.getDatPublPDO()));
			
			if (vErro.size()==0) {				
				atualizarReq(codEvento, DefinirRelatorId, myAuto, myReq, UsrLogado);	
				
				if (myAuto.getMsgErro().length()==0) {  		
					myAuto.setMsgErro("Junta/Relator alterado para o Requerimento:" + myReq.getNumRequerimento());	
					myAuto.setMsgOk("S") ;
				}													
			}
			else myAuto.setMsgErro(vErro);
		}
		catch (Exception e){
			throw new sys.CommandException("DefinirRelatorCmd: " + e.getMessage());
		}
		return nextRetorno  ;
	}


	private Vector atualizarAuto(HttpServletRequest req,AutoInfracaoBean myAuto,ParamOrgBean myParam,ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,String datEntrada,String txtEMail,String txtMotivo,String txtProcesso,String solicDEF, Ata ata) throws sys.CommandException { 
		Vector vErro = new Vector() ;
		//Raj 
		String bla = req.getParameter("acao");

		String tpRequerimento = req.getParameter("tpRequerimento");
		String codTipoSolic= "";
		String numAuto = req.getParameter("numAutoInfracao");
		String numPlaca = req.getParameter("dsPlaca");
		String codRelator = req.getParameter("idCpfAtaRelator");
		

		String dsRelator = req.getParameter("dsRelator");
		try {
			// preparar o Bean myHist (sem processo e num requerimento
			// Status 0,1,2,4,5
			myParam.setNomUsrNameAlt(myUsrLog.getNomUserName());
			
			if (myParam.getParamOrgao("STATUS_ABRIR_DEFPREVIA", "0,1,2,4,5","1").indexOf(myAuto.getCodStatus()) >= 0) {
				if ("0,1,2,4".indexOf(myAuto.getCodStatus()) >= 0)
					myAuto.setDatStatus(datEntrada);

				if(tpRequerimento.equals("Def. Previa")){
					codTipoSolic = "DP";
					myAuto.setCodStatus("5");
					myReq.setCodTipoSolic(codTipoSolic);
					myReq.setCodStatusRequerimento("0");
				} else if(tpRequerimento.equals("1� INST�NCIA")){
					codTipoSolic = "1P";
					myAuto.setCodStatus("15");
					myReq.setCodTipoSolic(codTipoSolic);
				} else if(tpRequerimento.equals("2� INST�NCIA")){
					myAuto.setCodStatus("35");
					codTipoSolic = "2P";
					myReq.setCodTipoSolic(codTipoSolic);
				} else if(tpRequerimento.equals("P.A.E")){
					myAuto.setCodStatus(AutoInfracaoBean.STATUS_PAE);
					codTipoSolic = "PAE";
					myReq.setCodTipoSolic(codTipoSolic);
				}
			}

			myReq.setCodAta(ata.getCodAta());
			myReq.setCodAutoInfracao(myAuto.getCodAutoInfracao()); 

			if ("DC,1C,2C".indexOf(solicDEF)>=0)
				myReq.setCodResponsavelDP(myAuto.getCondutor().getCodResponsavel());   
			else
				myReq.setCodResponsavelDP(myAuto.getProprietario().getCodResponsavel());   

			// myReq.setCodTipoSolic(solicDEF);
			myReq.setDatRequerimento(datEntrada);
			
			// preparar a data do Processo - se nao existir
			if (myAuto.getDatProcesso().length()==0)
				myAuto.setDatProcesso(myReq.getDatRequerimento());
			
			myReq.setCodStatusRequerimento("0");
			myReq.setNomUserNameDP(myUsrLog.getNomUserName());
			myReq.setNomResponsavelDP(myUsrLog.getNomUserName());

			HistoricoBean myHis = new HistoricoBean();

			if(tpRequerimento.equals("Def. Previa")) {
				codTipoSolic = "DP";
				myAuto.setCodStatus("5");
				myReq.setCodTipoSolic(codTipoSolic);
				myReq.setCodStatusRequerimento("0");
				ata.setTpAta(tpRequerimento);			
				myHis.setCodEvento(HistoricoBean.EVENTO_ABRIR_DP);
			} else if(tpRequerimento.equals("1� INST�NCIA")) {
				codTipoSolic = "1P";
				myAuto.setCodStatus("15");
				myReq.setCodTipoSolic(codTipoSolic);
				ata.setTpAta(tpRequerimento);
				myHis.setCodEvento(HistoricoBean.EVENTO_ABRIR_RECURSO_1P);
			} else if(tpRequerimento.equals("2� INST�NCIA")) {
				codTipoSolic = "2P";
				myAuto.setCodStatus("35");
				myReq.setCodTipoSolic(codTipoSolic);
				ata.setTpAta(tpRequerimento);
				myHis.setCodEvento(HistoricoBean.EVENTO_ABRIR_RECURSO_2P);
			} else if(tpRequerimento.equals("P.A.E")) {
				codTipoSolic = "PAE";
				myAuto.setCodStatus(AutoInfracaoBean.STATUS_PAE);
				myReq.setCodTipoSolic(codTipoSolic);
				ata.setTpAta(tpRequerimento);
				myHis.setCodEvento(HistoricoBean.EVENTO_ABRIR_RECURSO_PAE);
			}

			if(codRelator == null) {
				myReq.setCodRelatorJU("0");
				myReq.setNomUserNameJU("SELECIONE");
			} else {
				if((codRelator.equals("SELECIONE")) && (dsRelator.equals("SELECIONE"))) {
					myReq.setCodRelatorJU("0");
					myReq.setNomUserNameJU("SELECIONE");
				} else {
					myReq.setCodRelatorJU(codRelator);
					myReq.setNomUserNameJU(dsRelator);
				}
			}

			System.out.println("Username:" + myReq.getNomUserNameDP());

			myReq.setCodOrgaoLotacaoDP(myUsrLog.getOrgao().getCodOrgao());
			myReq.setDatProcDP(sys.Util.formatedToday().substring(0,10));
			
			// preparar Bean de Historico
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(txtProcesso);
			myHis.setCodOrgao(myAuto.getCodOrgao());

			myHis.setCodStatus(myAuto.getCodStatus());  
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodOrigemEvento("100");
			myHis.setNomUserName(myUsrLog.getNomUserName());
			myHis.setCodOrgaoLotacao(myUsrLog.getOrgao().getCodOrgao());
			myHis.setDatProc(sys.Util.formatedToday().substring(0,10));
			// complemento 01 - Num Requerimento no DAO
			myHis.setTxtComplemento02(myReq.getCodTipoSolic());
			
			if (solicDEF.equals("DC")) {
				myHis.setTxtComplemento04(myAuto.getCondutor().getNomResponsavel());
				myHis.setTxtComplemento05(myAuto.getCondutor().getIndCpfCnpj());				
				myHis.setTxtComplemento06(myAuto.getCondutor().getNumCpfCnpj());				
			}	
			else { 
				myHis.setTxtComplemento04(myAuto.getProprietario().getNomResponsavel());
				myHis.setTxtComplemento05(myAuto.getProprietario().getIndCpfCnpj());				
				myHis.setTxtComplemento06(myAuto.getProprietario().getNumCpfCnpj());
			}
			
			myHis.setTxtComplemento03(myReq.getDatRequerimento());
			myHis.setTxtComplemento07(txtMotivo);
			myHis.setTxtComplemento08("0");		   
			myHis.setTxtComplemento12(txtEMail);	

			DaoBroker dao = DaoBrokerFactory.getInstance();
			
			req.setAttribute("HistoricoBeanId", myHis);
			req.setAttribute("ExisteProcesso", ("".equals(myAuto.getNumProcesso()) ? "S" : "N"));

			myAuto.setNumProcesso(txtProcesso);
			myAuto.setNumPlaca(numPlaca);
			myAuto.setTpRequerimento(tpRequerimento);

			myAuto.setDatInfracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInfracao()));
			myAuto.setDatProcesso(ControladorRecGeneric.dateToDateBr(myAuto.getDatProcesso()));
			myAuto.setDatVencimento(ControladorRecGeneric.dateToDateBr(myAuto.getDatVencimento()));
			myAuto.setDatStatus(ControladorRecGeneric.dateToDateBr(myAuto.getDatStatus()));
			myAuto.setDatPublPDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatPublPDO()));
			myAuto.setDatEnvioDO(ControladorRecGeneric.dateToDateBr(myAuto.getDatEnvioDO()));
			myAuto.setDatNotifAut(ControladorRecGeneric.dateToDateBr(myAuto.getDatNotifAut()));
			myAuto.setDatNotifPen(ControladorRecGeneric.dateToDateBr(myAuto.getDatNotifPen()));
			myAuto.setDatPag(ControladorRecGeneric.dateToDateBr(myAuto.getDatPag()));
			myAuto.setDatInclusao(ControladorRecGeneric.dateToDateBr(myAuto.getDatInclusao()));
			myAuto.setDatAlteracao(ControladorRecGeneric.dateToDateBr(myAuto.getDatAlteracao()));
			myAuto.setDatRegistro(ControladorRecGeneric.dateToDateBr(myAuto.getDatRegistro()));

			System.out.println(ata.getTpAta());
			
			dao.atualizarAuto206(myAuto, myParam, myReq, myHis, myUsrLog, ata);

			/*
			 * Verifica se a Ata no parametro acima ainda tem viabilidade de distribuir processo
			 * se qtde_informado de processos � maior que qtde_atribuidos, sen�o fechar a ata e incluir um nova
			 */

			try {
				if(ata.validaQtdeProcesso(ata.getCodAta())) { //se for o ultimo numero de processo
					if(ata.verificaExisteAtaAberta(ata.getCodAta())) { //se nao existir ata aberta
						Ata devolve = new Ata();
						devolve =  devolve.populaAta(Integer.valueOf(ata.getCodAta()));
						devolve.setCodAta(ata.getCodAta());
						// devolve.setDescAta(ata.getDescAta());
						devolve.setStatusAta("F"); //upDate na anterior
						devolve.isAltera(devolve);

						Ata ataInsert = new Ata();
						//recupera proxima ata
						ataInsert = devolve.getNextNrAta(devolve);
						ataInsert.setCodAta("");
						ataInsert.setStatusAta("A");
						String descAta = String.valueOf(ataInsert.getNrAta()) + codTipoSolic + "/" + ataInsert.getAnoAta();
						ataInsert.setDescAta(descAta);

						int valParamProcess = ataInsert.getValParamProcess(myReq.getCodOrgaoLotacaoDP(), "VAL_GERENCIAMENTO_ATA_DP_1P_2P", ataInsert.getTpAta());
						ataInsert.setQtdeAtaProcesso(valParamProcess);

						if(ataInsert.isInsert(ataInsert)) { }
					} else {
						Ata devolve = new Ata();
						devolve =  devolve.populaAta(Integer.valueOf(ata.getCodAta()));
						devolve.setCodAta(ata.getCodAta());
						devolve.setDescAta(ata.getDescAta());
						devolve.setStatusAta("F"); //upDate na anterior
						devolve.isAltera(devolve);
						
						if(ata.verificaExisteAtaAberta(ata.getCodAta())) {
							Ata ataInsert = new Ata();
							//recupera proxima ata
							ataInsert = devolve.getNextNrAta(devolve);
							ataInsert.setCodAta("");
							ataInsert.setStatusAta("A");
							
							String descAta = String.valueOf(ataInsert.getNrAta()) + codTipoSolic + "/" + ataInsert.getAnoAta();
							
							ataInsert.setDescAta(descAta);

							int valParamProcess = ataInsert.getValParamProcess(myReq.getCodOrgaoLotacaoDP(), "VAL_GERENCIAMENTO_ATA_DP_1P_2P", ataInsert.getTpAta());
							
							ataInsert.setQtdeAtaProcesso(valParamProcess);

							if(ataInsert.isInsert(ataInsert)) { }
						}
					}
				}

				//			   
				//			   if(ata.getNexAta(ata.getCodAta())){//fechar a ata e incluir um novas caso nao tenha uma outra ata aberta
				//				   //Popula Ata
				//				   Ata devolve = new Ata();
				//				   devolve =  devolve.populaAta(Integer.valueOf(ata.getCodAta()));
				//				   devolve.setCodAta(ata.getCodAta());
				//				   devolve.setDescAta(ata.getDescAta());
				//				   devolve.setStatusAta("F"); //upDate na anterior
				//				   devolve.isAltera(devolve);
				//				   
				//				   Ata ataInsert = new Ata();
				//				   //recupera proxima ata
				//				   ataInsert = devolve.getNextNrAta(devolve);
				//				   ataInsert.setCodAta("");
				//				   ataInsert.setStatusAta("A");
				//				   String descAta = String.valueOf(ataInsert.getNrAta()) + codTipoSolic + "/" + ataInsert.getAnoAta();
				//				   ataInsert.setDescAta(descAta);
				//				   
				//				   
				//				   int valParamProcess = ataInsert.getValParamProcess(myReq.getCodOrgaoLotacaoDP(), "VAL_GERENCIAMENTO_ATA_DP_1P_2P", ataInsert.getTpAta());
				//				   ataInsert.setQtdeAtaProcesso(valParamProcess);
				//				   
				//				   if(ataInsert.isInsert(ataInsert)){
				//				   }
				//			   }
			} catch(Exception e) {
				e.getMessage().toString();
			}

			if (myAuto.getMsgErro().length() == 0  && (txtEMail.indexOf("@") >= 0)) {					
				ConteudoEmailBean email = new ConteudoEmailBean();
				
				email.enviaEmailChangeParam(txtEMail, myHis);
			}
		} catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		
		return vErro;
	}

	private void atualizarReq(String codEvento, DefinirRelatorBean DefinirRelatorId,AutoInfracaoBean myAuto,RequerimentoBean myReq, ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
		try { 	
			// preparar Bean de Historico
			HistoricoBean myHis = new HistoricoBean() ;
			myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
			myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
			myHis.setNumProcesso(myAuto.getNumProcesso());
			myHis.setCodStatus(myAuto.getCodStatus());
			myHis.setDatStatus(myAuto.getDatStatus());
			myHis.setCodEvento(codEvento);
			myHis.setCodOrigemEvento(DefinirRelatorId.getOrigemEvento());		
			myHis.setNomUserName(myReq.getNomUserNameJU());
			myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoJU());
			myHis.setDatProc(myReq.getDatProcRS());
			myHis.setTxtComplemento01(myReq.getNumRequerimento());	
			myHis.setTxtComplemento02(myReq.getDatJU());	
			myHis.setTxtComplemento03(myReq.getCodJuntaJU());	
			myHis.setTxtComplemento04(myReq.getCodRelatorJU());	
			DaoBroker dao = DaoBrokerFactory.getInstance();
			DefinirRelatorId.removeAutoGuia(myAuto,myReq);
			dao.atualizarAuto208(myAuto, myReq, myHis, UsrLogado); 
		}// fim do try
		catch (Exception ex) {
			throw new sys.CommandException(ex.getMessage());
		}
		
		return;
	}



	protected String processaAta(HttpServletRequest req,ParamOrgBean ParamOrgaoId,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado,REC.AbrirRecursoDPBean AbrirRecursoBeanId) throws sys.CommandException {
		String nextRetorno=jspPadrao;
		String sMensagem  = "";
		try {
			myAuto.setMsgErro("") ;

			String acao           = req.getParameter("acao");  

			Ata ata = new Ata();

			if(acao==null)   acao = " ";

			if(acao.equals("IncluirAta")){
				String nAta = req.getParameter("numPlaca");
				String nJunta = req.getParameter("numAutoInfracao");
				String dataSessao = req.getParameter("De");
				String tpAta = req.getParameter("indPagoradio");

				ata.setDescAta(nAta);
				ata.setJuntaAta(nJunta);
				ata.setDataSessao(dataSessao.replaceAll("-","/"));
				ata.setTpAta(tpAta);
				ata.setStatusAta("A");

				//Verifica se a data da sess�o informada � sabado|domingo|feriado, retorna true se a data for uma das op��es
				if(!ata.validaData(ata.getDataSessao())){
					Date d  = new Date();

					//Converte data sess�o(string) informada em Date
					d = ata.convertStringInDate(dataSessao);
					do{
						d.setDate(d.getDate() - 1); //Dimimui 1 dia
						//Data do aviso � data da Sess�o menos 1
						ata.setDataAviso(ata.convertDateInString(d)); // converte a data em string e atribui a aviso 
					}while(ata.validaData(ata.getDataAviso())); //enquanto for feriado incrementa -1 dia


					if(!ata.validaData(ata.getDataSessao()) && !ata.validaData(ata.getDataAviso())){ //se os dois forem falsos � porque eles nao s�o sabado, domingo e nem feriado.
						Date dd  = new Date();
						//Converte data aviso informada em Date
						dd = ata.convertStringInDate(ata.getDataAviso());
						do{
							dd.setDate(dd.getDate() - 1); //Dimimui 1 dia

							//Data do distribui��o �  data da Aviso menos 1
							ata.setDataDistribuicao(ata.convertDateInString(dd)); //1 dia
						}while(ata.validaData(ata.getDataDistribuicao())); //enquanto for feriado incrementa -1 dia
					}

					ata.isInsert(ata);
				}else{
					//informa que a data da sessao nao � valida e sugere a proxima data nao sendo sabado|domingo|feriado
					throw new Exception("Informe outra data!");
				}

			}

			if(acao.equals("AlterarAta")){
				String codAta = req.getParameter("codAta"); 
				nextRetorno = "/REC/AlterarAta.jsp" ;
				return nextRetorno;
			}

			if(acao.equals("alterar")){
				String codAta = req.getParameter("codAta");
				String tpAta = req.getParameter("tpAta"); 
				String dsAta = req.getParameter("dsAta"); 
				String dtSessao = req.getParameter("dtSessao");
				String dtAtribuicao = req.getParameter("dtDistribuicao");
				String juntaAta = req.getParameter("juntaATA");
				String dtAviso = req.getParameter("dtAviso");
				String status = req.getParameter("statusAta");

				ata.setCodAta(codAta);
				ata.setTpAta(tpAta);
				ata.setDescAta(dsAta);
				ata.setDataSessao(dtSessao);
				ata.setDataDistribuicao(dtAtribuicao);
				ata.setJuntaAta(juntaAta);
				ata.setDataAviso(dtAviso);
				ata.setStatusAta(status);
				ata.isAltera(ata);
			}

			if(acao.equals("ExcluirAta")){
				String codAta = req.getParameter("codAta"); 
				ata.setCodAta(codAta);
				ata.isDelete(ata);
			}
		}
		catch (Exception e){
			nextRetorno = "/REC/ErroDataAta.jsp" ;
			return nextRetorno;
			//	 		    throw new sys.CommandException("AbrirRecursoDPCmdb: " + e.getMessage()+" "+sMensagem);
		}
		return nextRetorno  ;
	}
}
