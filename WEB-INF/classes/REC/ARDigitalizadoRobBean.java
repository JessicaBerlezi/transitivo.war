package REC;

import java.text.SimpleDateFormat;
import java.util.Date;

import sys.BeanException;

/**
 * <b>Title: </b> SMIT - REC - AR Digitalizado Bean <br>
 * <b>Description: </b> Aviso de Recebimento Digitalizado Bean <br>
 * <b>Copyright: </b> Copyright (c) 2004 <br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
 * @author Glaucio Jannotti
 * @version 1.0
 */

public class ARDigitalizadoRobBean extends sys.HtmlPopupBean {
    
    private int codARDigitalizado;
    private String numNotificacao;
    private String numAutoInfracao;    
    private String numLote;
    private String numCaixa;
    private Date datDigitalizacao;
    
    public ARDigitalizadoRobBean() {
        
        super();
        codARDigitalizado = 0;
        numNotificacao = "";
        numAutoInfracao = "";
        numLote = "";
        numCaixa = "";
        datDigitalizacao = null;
    }
    
    public ARDigitalizadoRobBean(String numNotificacao, String numAutoInfracao, String numLote, String numCaixa,
            Date datDigitalizacao) {
        
        this();
        this.numNotificacao = numNotificacao;
        this.numAutoInfracao = numAutoInfracao;
        this.numLote = numLote;
        this.numCaixa = numCaixa;
        this.datDigitalizacao = datDigitalizacao;
    }
    
    public String getNumNotificacao() {
        if (numNotificacao == null) return "";
        else return numNotificacao;
    }
    
    public void setNumNotificacao(String string) {
        numNotificacao = string;
    }
    
    public String getNumAutoInfracao() {
        return numAutoInfracao;
    }
    public void setNumAutoInfracao(String string) {
        numAutoInfracao = string;
    }
    
    public String getNumCaixa() {
        return numCaixa;
    }
    
    public void setNumCaixa(String string) {
        numCaixa = string;
    }
    
    public String getNumLote() {
        return numLote;
    }
    
    public void setNumLote(String string) {
        numLote = string;
    }
    
    public int getCodARDigitalizado() {
        return codARDigitalizado;
    }
    
    public void setCodARDigitalizado(int i) {
        codARDigitalizado = i;
    }
    
    public Date getDatDigitalizacao() {
        return datDigitalizacao;
    }
    
    public void setDatDigitalizacao(Date datDigitalizacao) {
        this.datDigitalizacao = datDigitalizacao;
    }
    
    public String grava() throws DaoException {
        return DaoRobDigit.getInstance().GravaARDigitalizado(this);
    }

    public String getImagem() {        
        String pathImagem = "";
        
        pathImagem =  getDataDigitalizacaoString().substring(6,10)+ 
		getDataDigitalizacaoString().substring(3,5) +
		"/" + getDataDigitalizacaoString().substring(0,2); 
        return pathImagem;
    }   
    
    public String getParametro(ACSS.ParamSistemaBean param) {        
        String parametro = "DIR_AR_DIGITALIZADO";
        /*
        parametro+="_"+getDataDigitalizacaoString().substring(6,10);
        */
        return parametro;
    } 
    
    public String getArquivo(ACSS.ParamSistemaBean param) {
        String pathArquivo;
        try {
            pathArquivo = "E:/PROD-smit/digitalizado/" +this.getImagem();
            
			if (((this.numNotificacao == null) || this.numNotificacao.equals(""))
			&& (this.numAutoInfracao.length() >= 6)) {
				SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
				pathArquivo += "/AR" +"/"+ this.numAutoInfracao + "_" + df.format(this.datDigitalizacao)+".gif";
				
			} else if (this.numNotificacao.length() >= 5)
				pathArquivo += "/AR/" + this.numNotificacao + ".gif";;
            
        } catch (Exception e) {
            pathArquivo = "";
        }
        return pathArquivo;
    }    
    
    public String getDataDigitalizacaoString() {
        
        String datString = "";
        if (datDigitalizacao != null) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            datString = df.format(this.datDigitalizacao);
        }
        return datString;
    }
    
    public static ARDigitalizadoRobBean consultaAR(String[] campos, String[] valores) 
    throws sys.BeanException {
        
        ARDigitalizadoRobBean AR = null;
        try {
            AR = DaoRobDigit.getInstance().ConsultaARDigitalizado(campos, valores);
            if (AR == null) {
                AR = new ARDigitalizadoRobBean();
                AR.setMsgErro("AVISO DE RECEBIMENTO DIGITALIZADO N�O ENCONTRADO !");
            }
        } catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
        return AR;		
    }
    
    public static ARDigitalizadoRobBean[] consultaARs(String[] campos, String[] valores) 
    throws sys.BeanException {
        
        try {
            return DaoRobDigit.getInstance().ConsultaARDigitalizados(campos, valores);			     
        } 
        catch (DaoException e) {
            throw new sys.BeanException(e.getMessage());
        }
    }
    
    public String verificaImagemAR(String numNotifica) throws BeanException{
    	String tipoArquivo = "";
    	String numRetorno = "";
		  String dscLinhaArq = "";
		  try {
			   dscLinhaArq = DaoRobDigit.getInstance().verificaDscArq(numNotifica);	
			   if (!dscLinhaArq.equals("")){
				    numRetorno = dscLinhaArq.substring(44,46);
						tipoArquivo = DaoRobDigit.getInstance().verificaNumRetorno(numRetorno);
			   }
			   else tipoArquivo = "";

			} 
			catch (DaoException e) {
					throw new sys.BeanException(e.getMessage());
			}
    	return tipoArquivo;
    }
    
	public String getNomFoto(){
			return getImagem() + "/AR/"+this.numAutoInfracao + ".jpg";
	}
}