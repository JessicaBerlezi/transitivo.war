package REC;


public class EstornoAbrirRecursoBean extends EventoBean  {   
    
    public EstornoAbrirRecursoBean()  throws sys.BeanException {        
        super() ;      
    }
  
    public void setJ_sigFuncao(String j_sigFuncao)   {  	
        if (j_sigFuncao==null) j_sigFuncao = "" ;
        setCFuncao(j_sigFuncao) ;
        
        // Estorno de Requerimento de Defesa Previa 
        setStatusValido("5") ;
        setStatusTPCVValido("0,1,2,3,4") ;		
        setNFuncao("Estorno de Def. Pr�via") ;
        setTipoReqValidos("DP,DC,TR") ;
        setStatusReqValido("0,1,2") ;	
        setOrigemEvento("100") ;
        setCodEvento("226") ;							
        // Estorno de Requerimento de 1a Instancia
        if ("REC0820".equals(j_sigFuncao)) {
            setStatusValido("15") ;
            setStatusTPCVValido("10,11,12,14") ;		
            setNFuncao("Estorno de  1a. Inst�ncia") ;
            setTipoReqValidos("1P,1C") ;	
            setStatusReqValido("0,2") ;	
            setOrigemEvento("100") ;
            setCodEvento("350") ;			
        }
        // Estorno de Requerimento de 2a Instancia	
        if ("REC0830".equals(j_sigFuncao)) {
            setStatusValido("35") ;
            setStatusTPCVValido("30") ;		
            setNFuncao("Estorno de 2a. Inst�ncia") ;
            setTipoReqValidos("2P,2C") ;
            setStatusReqValido("0,2") ;	
            setOrigemEvento("100") ;
            setCodEvento("351") ;							
        }
        // Estorno de Resultado Def Previa	
        if ("REC0244".equals(j_sigFuncao)) {
            setStatusValido("5") ;
            setStatusTPCVValido("0,1,2,4") ;		
            setNFuncao("Estorno de Resultado de Def. Pr�via") ;
            setTipoReqValidos("DP,DC") ;
            setStatusReqValido("3") ;	
            setOrigemEvento("100") ;
            setCodEvento("210") ;							
        } 
        // Estorno Resultado de 1a Instancia
        if ("REC0344".equals(j_sigFuncao)) {
            setStatusValido("15") ;
            setStatusTPCVValido("10,11,12,14") ;		
            setNFuncao("Estorno de Resultado 1a. Inst�ncia") ;
            setTipoReqValidos("1P,1C") ;	
            setStatusReqValido("3") ;	
            setOrigemEvento("100") ;
            setCodEvento("330") ;			
        }
        // Estorno Resultado de 2a Instancia	
        if ("REC0444".equals(j_sigFuncao)) {
            setStatusValido("35") ;
            setStatusTPCVValido("30") ;		
            setNFuncao("Estorno de Resultado de 2a. Inst�ncia") ;
            setTipoReqValidos("2P,2C") ;
            setStatusReqValido("3") ;	
            setOrigemEvento("100") ;
            setCodEvento("337") ;							
        }
        // Estorno Troca de Real Infrator	
        if ("REC0840".equals(j_sigFuncao)) {
            setStatusValido("5,4") ;
            setStatusTPCVValido("0,1,2,4") ;		
            setNFuncao("Estorno de Troca de Real Infrator") ;
            setTipoReqValidos("TR") ;
            setStatusReqValido("0,9") ;	
            setOrigemEvento("100") ;
            setCodEvento("352") ;							
        } 
        // Estorno Parecer Juridico
        if ( ("REC0850".equals(j_sigFuncao)) || ("REC0211".equals(j_sigFuncao)) ) {
            setStatusValido("5") ;
            setStatusTPCVValido("0,1,2,3,4") ;		
            setNFuncao("Estorno de Parecer Juridico") ;
            setTipoReqValidos("DP,DC") ;
            setStatusReqValido("0,1") ;	
            setOrigemEvento("100") ;
            setCodEvento("226") ;							
        } 	
        //	Estorno de Troca de Junta/Relator - DP
        if (  ("REC0221".equals(j_sigFuncao))	 ){	 
            setStatusValido("5") ;
            setStatusTPCVValido("0,1,2,4") ;		
            setNFuncao("ESTORNO DE JUNTA/RELATOR") ;
            setTipoReqValidos("DP,DC") ;	
            setStatusReqValido("1,2") ;	
            setOrigemEvento("100") ;
            setCodEvento("215") ;							
        }  
        //	Estorno de Troca de Junta/Relator - 1Instancia
        if (  ("REC0331".equals(j_sigFuncao))	 ){	 
            setStatusValido("15") ;
            setStatusTPCVValido("10,11,12,14") ;		
            setNFuncao("ESTORNO DE JUNTA/RELATOR") ;
            setTipoReqValidos("1P,1C") ;	
            setStatusReqValido("1,2") ;	
            setOrigemEvento("100") ;
            setCodEvento("215") ;							
        }  	
        //	Estorno de Troca de Junta/Relator - 2Instancia	
        if (  ("REC0431".equals(j_sigFuncao))	 ){	 
            setStatusValido("35") ;
            setStatusTPCVValido("30") ;		
            setNFuncao("ESTORNO DE JUNTA/RELATOR") ;
            setTipoReqValidos("2P,2C") ;	
            setStatusReqValido("1,2") ;	
            setOrigemEvento("100") ;
            setCodEvento("215") ;							
        }  
        
        //	Estorno de Renuncia Defesa Pr�via	
        if (  ("REC0811".equals(j_sigFuncao))	 ){	 
            setStatusValido("10") ;
            setStatusTPCVValido("") ;		
            setNFuncao("ESTORNO DE RENUNCIA DE DEFESA PREVIA") ;
            setTipoReqValidos("RD") ;	
            setStatusReqValido("9") ;	
            setOrigemEvento("100") ;
            setCodEvento("239") ;							
        }  
        // Estorno de Indeferimento de Plano	
        if ("REC0812".equals(j_sigFuncao)) {
            setStatusValido("35") ;
            setStatusTPCVValido("30") ;		
            setNFuncao("ESTORNO DE IND. DE PLANO") ;
            setTipoReqValidos("DI,1I,2I") ;
            setStatusReqValido("0,1,3,4") ;	
            setOrigemEvento("100") ;
            setCodEvento("237") ;							
        }
        
        //	Estorno DE ENVIO P/PUBLICA��O 1�INST�NCIA	
        if (  ("REC0348".equals(j_sigFuncao))	 ){	 
            setStatusValido("15") ;
            setStatusReqValido("4") ;
            setTipoReqValidos("1I,1P,1C,1R") ;
            setNFuncao("ESTORNO DE ENVIO P/PUBLICA��O 1�INST�NCIA") ;
            setOrigemEvento("100") ;
            setCodEvento("382") ;							
        }  
        
        // Estorno DE ENVIO P/PUBLICA��O 2�INST�NCIA
        if ("REC0448".equals(j_sigFuncao)) {
            setStatusValido("35") ;
            setStatusTPCVValido("30") ;	
            setTipoReqValidos("2I,2P,2C,2R") ;
            setStatusReqValido("4") ;	
            setNFuncao("ESTORNO DE ENVIO P/PUBLICA��O 2�INST�NCIA") ;
            setOrigemEvento("100") ;
            setCodEvento("389") ;							
        }
        
        return ;	
    } 
  
}
