package REC;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <b>Title:</b>        Etiquetas para Auto Digitalizado<br>
 * <b>Description:</b>  Comando para Etiquetas de Auto Digitalizados<br>
 * <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      MIND Inform�tica<br>
 * @author 			    Elisson Dias	
 * @version 1.0
 */

public class EtiquetaAutoCmd extends sys.Command {
	
	private static final String jspPadrao="/REC/EtiquetaAuto.jsp";
	
	public EtiquetaAutoCmd() {
		super();
	}
	
	public String execute(HttpServletRequest req) throws sys.CommandException {
		
		String nextRetorno = jspPadrao ;		
		try {
			HttpSession session   = req.getSession() ;								
			ACSS.UsuarioBean usuario = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId");
			if (usuario == null) usuario = new ACSS.UsuarioBean();			
			
			String msgErro="";
			
			// obtem e valida os parametros recebidos					
			String acao = req.getParameter("acao");  
			if (acao == null)
				acao = "";
			
			String numLote = req.getParameter("numLote");
			if (numLote == null) 
				numLote = "";
			else
				numLote = numLote.trim();
			
			String numCaixa = req.getParameter("numCaixa");			
			if (numCaixa == null)
				numCaixa = "";
			else
				numCaixa=numCaixa.trim();
			
			String numLoteBackup = req.getParameter("numLoteBackUp");
			if (numLoteBackup == null)
				numLoteBackup = "";
			else
				numLoteBackup=numLoteBackup.trim();
			
			String numCaixaBackup =  req.getParameter("numCaixaBackUp");			
			if (numCaixaBackup == null)
				numCaixaBackup = "";
			else
				numCaixaBackup=numCaixaBackup.trim();
			
			ArrayList listAuto = new ArrayList();
			
			if (acao.equals("retorna")) {
				numCaixa="";
				numLote="";
				numCaixaBackup="";
				numLoteBackup="";
				
			} else if (acao.equals("buscaEtiquetas")) {
				try {					
					DaoDigit dao = DaoDigit.getInstance();			
					dao.ConsultaEtiquetaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,listAuto);
					numLoteBackup = numLote;
					numCaixaBackup = numCaixa;
					
				} catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}
				
			} else if (acao.equals("atualizaEtiquetas")) {
				
				String vetAutos[] = req.getParameterValues("numAutoInfracao");
				String vetEtq[] = req.getParameterValues("codEtq");
				
				//Atualiza a caixa e lote
				if ((!numCaixa.equals(numCaixaBackup) )|| (!numLote.equals(numLoteBackup))){
					try {					
						DaoDigit dao = DaoDigit.getInstance();			
						if(dao.atualizaCaixaLoteAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,numCaixaBackup,numLoteBackup))
							msgErro="N�mero de Lote e caixa j� existentes. N�o foi poss�vel atualizar.\n";
						
						else {
							numLoteBackup = numLote;
							numCaixaBackup = numCaixa;						
						}
					} catch (Exception e) {					
						throw new sys.CommandException(e.getMessage());
					}
				}
				
				//Monta uma lista de beans de EtiquetaAuto				
				ArrayList vectAutos = new ArrayList();
				for (int i=0;i<vetAutos.length;i++) {
					if (!vetAutos[i].equals("")) {
						EtiquetaAutoBean etqBean = new EtiquetaAutoBean();
						etqBean.setNumAutoInfracao(vetAutos[i]);
						etqBean.setCodEtqAutoInfracao(Integer.parseInt(vetEtq[i]));
						listAuto.add(etqBean);						
						vectAutos.add(vetAutos[i]);
					}
				}
				
				//Verifica se existe um auto repetido na lista
				String erros="";
				for (int i=0;i<vectAutos.size();i++) {
					if (!vectAutos.get(i).equals("")) {
						int ult=vectAutos.lastIndexOf(vectAutos.get(i));
						if (ult!=i) {
							msgErro="N�mero de auto duplicado neste lote: ";
							erros+=vectAutos.get(i) + "; " ;
						}
					}
				}
				msgErro+=erros;
				
				//Atualiza auto na base para caixa e lote
				try {
					DaoDigit dao = DaoDigit.getInstance();					
					if (msgErro.equals("")) {
						//Verifica se existe um auto repetido em outro lote e caixa
						msgErro=dao.VerificaEtiquetaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,vetAutos);
						if (msgErro.equals("")) {							
							dao.AtualizaEtiquetaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,vetAutos,vetEtq);
							listAuto.clear();
							dao.ConsultaEtiquetaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,listAuto);
							msgErro="Etiquetas gravadas com sucesso!";
						}					
					}						
				} catch (Exception e) {
					throw new sys.CommandException(e.getMessage());
				}			
				
				//Chama o JSP para consultar etiquetas
			} else if((acao.equals("consultaAutoEtiq"))||(acao.equals("chamaAutoEtiq"))||(acao.equals("buscaAutoEtiq"))){
				
				nextRetorno = this.consultaEtiqueta(req, usuario);
				numCaixa = (String) req.getAttribute("numCaixa");
				numLote = (String) req.getAttribute("numLote");
				numCaixaBackup = numCaixa;
				numLoteBackup = numLote;
				listAuto = (ArrayList)	req.getAttribute("listAuto");
				
			}else if (acao.equals("mostraMultiEtiquetas") || acao.equals("buscaMultiEtiquetas") || acao.equals("imprimeMultiEtiquetas")) {
				
				nextRetorno = this.multiEtiqueta(req, usuario);
				numCaixa = (String) req.getAttribute("numCaixa");
				numLote = (String) req.getAttribute("numLote");
				numCaixaBackup = numCaixa;
				numLoteBackup = numLote;
				listAuto = (ArrayList)	req.getAttribute("listAuto");			
			} 
			
			req.setAttribute("msgErro",msgErro);
			req.setAttribute("numCaixa",numCaixa);
			req.setAttribute("numLote",numLote);
			req.setAttribute("numCaixaBackup",numCaixaBackup);
			req.setAttribute("numLoteBackup",numLoteBackup);
			req.setAttribute("listAuto",listAuto);
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		
		return nextRetorno;
	}
	
	public String consultaEtiqueta(HttpServletRequest req, ACSS.UsuarioBean usuario) throws sys.CommandException {
		
		String nextRetorno  = "/REC/ConsultaEtiquetaAuto.jsp" ;		
		try {    
			// Cria o bean
			EtiquetaAutoBean bean = (EtiquetaAutoBean)req.getAttribute("bean");
			if(bean == null) bean = new EtiquetaAutoBean();
			
			String verTable = req.getParameter("verTable");
			if (verTable == null)verTable = "N";
			
			String acao = req.getParameter("acao");  
			if (acao == null)acao = "";
			
			String numLote = req.getParameter("numLote");
			if (numLote == null) numLote = "";
			else numLote=numLote.trim();
			
			String numCaixa = req.getParameter("numCaixa");			
			if (numCaixa == null)numCaixa = "";
			else numCaixa=numCaixa.trim();
			
			String numAuto = req.getParameter("numAuto");			
			if (numAuto == null) numAuto = "";
			else numAuto = numAuto.trim();
			
			String ordem = req.getParameter("ordem");			
			if (ordem == null) ordem = "";
			else ordem = ordem.trim();
			
			//atribuindo valores
			bean.setNumCaixa(numCaixa);
			bean.setNumLote(numLote);
			bean.setNumAutoInfracao(numAuto);
			bean.setOrdem(ordem);	
			
			//Execucao dos metodos
			
			if(acao.equals("consultaAutoEtiq")){
				verTable = "N";
			}
			
			if (acao.equals("retorna")) {
				bean.setNumCaixa("");
				bean.setNumLote("");
				bean.setNumAutoInfracao("");
			}
			
			if (acao.equals("buscaAutoEtiq")) {
				verTable = "S";
				bean.consultaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLote,numCaixa,numAuto);
			}
			
			//Chama o jsp EtiquetaAuto
			ArrayList listAuto = new ArrayList();
			if(acao.equals("chamaAutoEtiq")){
				// Parametros selecionadas da linha
				String numLoteSel = req.getParameter("numLoteSel");
				if (numLoteSel == null) numLoteSel = "";
				
				String numCaixaSel = req.getParameter("numCaixaSel");			
				if (numCaixaSel == null) numCaixaSel = "";
				
				String numAutoSel = req.getParameter("numAutoSel");			
				if (numAutoSel == null) numAutoSel = "";
				
				try {
					DaoDigit dao = DaoDigit.getInstance();			
					dao.ConsultaEtiquetaAutoInfracao(usuario.getCodOrgaoAtuacao(),numLoteSel,numCaixaSel,listAuto);
				} 
				catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}
				nextRetorno = "/REC/EtiquetaAuto.jsp" ;
				numCaixa = numCaixaSel;
				numLote = numLoteSel;
			}
			
			req.setAttribute("bean",bean);
			req.setAttribute("numCaixa",numCaixa);
			req.setAttribute("numLote",numLote);
			req.setAttribute("listAuto",listAuto);
			req.setAttribute("numAuto",numAuto);
			req.setAttribute("verTable",verTable);
			
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		
		return nextRetorno;
	} 
	
	public String multiEtiqueta(HttpServletRequest req, ACSS.UsuarioBean usuario) throws sys.CommandException {
		
		String nextRetorno  = "/REC/MultiEtiquetaAuto.jsp";
		
		try {    
			// Cria o bean
			EtiquetaAutoBean bean = (EtiquetaAutoBean)req.getAttribute("bean");
			if(bean == null) bean = new EtiquetaAutoBean();
			
			String verTable = req.getParameter("verTable");
			if (verTable == null)verTable = "N";
			
			String acao = req.getParameter("acao");  
			if (acao == null)acao = "";
			
			String numLote = req.getParameter("numLote");
			if (numLote == null) numLote = "";
			else numLote=numLote.trim();
			
			String numCaixa = req.getParameter("numCaixa");			
			if (numCaixa == null)numCaixa = "";
			else numCaixa=numCaixa.trim();
			
			String numAuto = req.getParameter("numAuto");			
			if (numAuto == null) numAuto = "";
			else numAuto = numAuto.trim();
			
			String ordem = req.getParameter("ordem");			
			if (ordem == null) ordem = "";
			else ordem = ordem.trim();
			
			String[] imprimeLotes = req.getParameterValues("imp");
			if(imprimeLotes==null)  imprimeLotes = new String[0];				
			
			String montaIn="";
			for (int i = 0; i<imprimeLotes.length;i++) {
				if (i<imprimeLotes.length -1 ){				
					montaIn= montaIn +"'"+ imprimeLotes[i]+ "',";						
				}
				else{
					montaIn= montaIn + "'"+ imprimeLotes[i]+ "'";
				}
			}		
			
			//atribuindo valores
			bean.setNumCaixa(numCaixa);
			
			bean.setNumAutoInfracao(numAuto);
			bean.setOrdem(ordem);	
			
			//Execucao dos metodos
			
			if(acao.equals("mostraMultiEtiquetas")){
				verTable = "N";
			}
			
			if (acao.equals("retorna")) {
				bean.setNumCaixa("");
				bean.setNumLote("");
				bean.setNumAutoInfracao("");
			}
			
			if (acao.equals("buscaMultiEtiquetas")) {
				verTable = "S";
				bean.consultaLotes(usuario.getCodOrgaoAtuacao(), numCaixa);
			}
			
			//Chama o jsp EtiquetaAuto
			ArrayList listAuto = new ArrayList();
			if(acao.equals("imprimeMultiEtiquetas")){
				
				try {
					DaoDigit dao = DaoDigit.getInstance();					
					dao.ConsultaMultiEtiquetaAutoInfracao(montaIn,usuario.getCodOrgaoAtuacao(),numCaixa,listAuto);					
					//Rotina para gerar os codigos de barras
					if (listAuto.size() > 0)
						nextRetorno="/REC/EtiquetaAutoImpressao.jsp";			
				} 
				catch (Exception e) {					
					throw new sys.CommandException(e.getMessage());
				}			 
				
			}		
			
			req.setAttribute("bean",bean);
			req.setAttribute("numCaixa",numCaixa);
			req.setAttribute("numLote",numLote);
			req.setAttribute("listAuto",listAuto);
			req.setAttribute("numAuto",numAuto);
			req.setAttribute("verTable",verTable);
			
		} catch (Exception e) {
			throw new sys.CommandException(e.getMessage());
		}
		
		return nextRetorno;
	} 
	
}