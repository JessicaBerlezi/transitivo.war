package REC;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class DefinirRelatorCmd extends sys.Command {
  private String next;
  private static final String jspPadrao="/REC/DefinirRelator.jsp" ;  
   
  public DefinirRelatorCmd() {
    next             =  jspPadrao;
  }

  public DefinirRelatorCmd(String next) {
    this.next = next;
  }

  public String execute(HttpServletRequest req) throws sys.CommandException {
  	String nextRetorno  = next ;
    try {     
	    sys.Command cmd = (sys.Command)Class.forName("REC.ProcessaAutoCmd").newInstance() ;	
		cmd.setNext(this.next) ;
    	nextRetorno = cmd.execute(req);		

	    // cria os Beans de sessao, se n�o existir
	    HttpSession session   = req.getSession() ;								
	    ACSS.UsuarioBean UsrLogado            = (ACSS.UsuarioBean)session.getAttribute("UsuarioBeanId") ;
	    if (UsrLogado==null)  UsrLogado       = new ACSS.UsuarioBean() ;	  			
	    ParamOrgBean ParamOrgaoId        = (ParamOrgBean)session.getAttribute("ParamOrgBeanId") ;
	    if (ParamOrgaoId==null)  ParamOrgaoId = new ParamOrgBean() ;	  			

	  	// cria os Beans, se n�o existir
	  	RequerimentoBean RequerimentoId     = (RequerimentoBean)req.getAttribute("RequerimentoId") ;
	  	if (RequerimentoId==null)  RequerimentoId = new RequerimentoBean() ;	  			
	  	DefinirRelatorBean DefinirRelatorId     = (DefinirRelatorBean)req.getAttribute("DefinirRelatorId") ;
	  	if (DefinirRelatorId==null)  DefinirRelatorId = new DefinirRelatorBean() ;	  			
	  	DefinirRelatorId.setJ_sigFuncao(req.getParameter("j_sigFuncao"),ParamOrgaoId.getParamOrgao("EXIGE_PARECER_JURIDICO","N","7"));

	    AutoInfracaoBean AutoInfracaoBeanId     = (AutoInfracaoBean)req.getAttribute("AutoInfracaoBeanId") ;
	    if (AutoInfracaoBeanId==null)  AutoInfracaoBeanId = new AutoInfracaoBean() ;	  			
	    // obtem e valida os parametros recebidos					
	    String acao           = req.getParameter("acao");  
	    if(acao==null)   acao = " ";
	    if  ( (acao.equals("LeAI")) && (AutoInfracaoBeanId.getMsgErro().length()==0) )  {
		    AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
		    DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;		    
	    }
	    if  (acao.equals("LeReq"))   {
	    	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	        DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;
	        RequerimentoId.setCodRequerimento(req.getParameter("codRequerimento"));
	        if (RequerimentoId.getCodRequerimento().length()!=0) 
				RequerimentoId = AutoInfracaoBeanId.getRequerimentos(RequerimentoId.getCodRequerimento()) ;
			if (RequerimentoId.getDatJU().trim().length()==0) 
				RequerimentoId.setDatJU(sys.Util.formatedToday().substring(0,10));
	    }	
	    if  (acao.equals("InformaRelator"))  {
	      	AutoInfracaoBeanId.LeRequerimento(UsrLogado) ;
	    	DefinirRelatorId.setEventoOK(AutoInfracaoBeanId) ;
	    	nextRetorno = processaTroca(DefinirRelatorId,req,RequerimentoId,AutoInfracaoBeanId,UsrLogado);
	    }
	    req.setAttribute("AutoInfracaoBeanId",AutoInfracaoBeanId) ;
	    req.setAttribute("RequerimentoId",RequerimentoId) ;		
	    req.setAttribute("DefinirRelatorId",DefinirRelatorId) ;		
	}
    catch (Exception ue) {
	      throw new sys.CommandException("DefinirRelatorCmd: " + ue.getMessage());
    }
	return nextRetorno;
  }

  public String processaTroca(DefinirRelatorBean DefinirRelatorId,HttpServletRequest req,RequerimentoBean myReq,AutoInfracaoBean myAuto,ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
   	String nextRetorno = next ;
   	try {
   		myAuto.setMsgErro("") ;
  		Vector vErro = new Vector() ;
  		// validar se status permite recurso ou defesa previa : 5,25,35
  		if ("S".equals(DefinirRelatorId.getEventoOK())==false) vErro.addElement(DefinirRelatorId.getMsg()+" \n") ;
  		myReq.setCodRequerimento(req.getParameter("codRequerimento"));
  		//para o Oficio Funcionar
  		if (myReq.getCodRequerimento().equals("")){
  			OficioBean OficioBean = new OficioBean();
	 		String solicDEF = req.getParameter("SolicDPBox");
			if (solicDEF==null) solicDEF="";
			if ("".equals(solicDEF)) solicDEF = req.getParameter("SolicDCBox");
			if (solicDEF==null) solicDEF="";
			OficioBean.setTipoRecurso(solicDEF);
  			String codRequerimento = OficioBean.verificaCodReq(myAuto,myReq,OficioBean,UsrLogado);
  			myReq.setCodRequerimento(codRequerimento);
  		}
  		
  		if (myReq.getCodRequerimento().length()==0) vErro.addElement("Requerimento n�o selecionado. \n") ;
  		if (vErro.size()==0) myReq = myAuto.getRequerimentos(myReq.getCodRequerimento()) ;
  		myReq.setDatJU(req.getParameter("datJU"));
  		/*Setar Status Requerimento 02 - Definir Relator*/
		myReq.setCodStatusRequerimento("2");   		
  		//validar data de entrada
  		if (myReq.getDatJU().length()==0) vErro.addElement("Data n�o preenchida. \n") ;
   		if (sys.Util.DifereDias( sys.Util.formatedToday().substring(0,10),myReq.getDatJU())>0) vErro.addElement("Data n�o pode ser superior a hoje. \n") ;
   		/*
   		 * Michel 20/02/2006
   		if (sys.Util.DifereDias( myReq.getDatJU(),myAuto.getDatStatus())>0) vErro.addElement("Data n�o pode ser anterior a data do Status: "+myAuto.getDatStatus()+". \n") ;
   		*/
   		myReq.setCodJuntaJU(req.getParameter("codJuntaJU"));
   		myReq.setCodRelatorJU(req.getParameter("codRelatorJU"));
   		if ( (myReq.getCodJuntaJU().length()==0) || (myReq.getCodRelatorJU().length()==0) ) vErro.addElement("Junta/Relator n�o selecionado. \n") ;
	  	myReq.setNomUserNameJU(UsrLogado.getNomUserName());
  		myReq.setCodOrgaoLotacaoJU(UsrLogado.getOrgao().getCodOrgao());
	  	myReq.setDatProcJU(sys.Util.formatedToday().substring(0,10));
  		if (vErro.size()==0) {
  			atualizarReq(DefinirRelatorId,myAuto,myReq,UsrLogado) ;	
  			if (myAuto.getMsgErro().length()==0) {  		
    			myAuto.setMsgErro("Junta/Relator alterado para o Requerimento:"+myReq.getNumRequerimento()) ;	
    			myAuto.setMsgOk("S") ;
  			}													
  		}
	  	else myAuto.setMsgErro(vErro);
   	}
    catch (Exception e){
  	    throw new sys.CommandException("DefinirRelatorCmd: " + e.getMessage());
    }
   	return nextRetorno  ;
  }
  private void atualizarReq(DefinirRelatorBean DefinirRelatorId,AutoInfracaoBean myAuto,RequerimentoBean myReq, ACSS.UsuarioBean UsrLogado) throws sys.CommandException { 
  	try { 	
  		// preparar Bean de Historico
  		HistoricoBean myHis = new HistoricoBean() ;
  		myHis.setCodAutoInfracao(myAuto.getCodAutoInfracao());   
  		myHis.setNumAutoInfracao(myAuto.getNumAutoInfracao()); 
  		myHis.setNumProcesso(myAuto.getNumProcesso());
  		myHis.setCodStatus(myAuto.getCodStatus());
  		myHis.setDatStatus(myAuto.getDatStatus());
		myHis.setCodEvento(DefinirRelatorId.getCodEvento());
		myHis.setCodOrigemEvento(DefinirRelatorId.getOrigemEvento());		
  		myHis.setNomUserName(myReq.getNomUserNameJU());
  		myHis.setCodOrgaoLotacao(myReq.getCodOrgaoLotacaoJU());
  		myHis.setDatProc(myReq.getDatProcRS());
  		myHis.setTxtComplemento01(myReq.getNumRequerimento());	
  		myHis.setTxtComplemento02(myReq.getDatJU());	
  		myHis.setTxtComplemento03(myReq.getCodJuntaJU());	
  		myHis.setTxtComplemento04(myReq.getCodRelatorJU());	
  	    DaoBroker dao = DaoBrokerFactory.getInstance();
  	    DefinirRelatorId.removeAutoGuia(myAuto,myReq);
  	    dao.atualizarAuto208(myAuto,myReq,myHis,UsrLogado); 
  	}// fim do try
  	catch (Exception ex) {
  		throw new sys.CommandException(ex.getMessage());
  	}
  	return  ;
  }	
  
}