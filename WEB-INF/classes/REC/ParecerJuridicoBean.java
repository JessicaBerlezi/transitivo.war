package REC;

import java.util.Iterator;


/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Parecer Juridico Bean<br>
* <b>Description:</b>  Parecer Juridico Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class ParecerJuridicoBean  extends EventoBean  { 

  private String tipoJunta;
	
  public ParecerJuridicoBean()  throws sys.BeanException {

  super() ;
  tipoJunta        = "0";
  }
//--------------------------------------------------------------------------  
  public void setJ_sigFuncao(String j_sigFuncao)   {
	if (j_sigFuncao==null) j_sigFuncao = "" ;
	setCFuncao(j_sigFuncao) ;
	// Parecer Juridico
	setStatusValido("5") ;
	setStatusTPCVValido("0,1,2,3,4") ;		
	setNFuncao("Parecer Jur�dico") ;
	setTipoReqValidos("DP,DC") ;
	setStatusReqValido("0") ;	
	setOrigemEvento("100") ;
	setCodEvento("207") ;	
	this.tipoJunta="0";
	// Estorno de Parecer Juridico
	if ("REC0225".equals(j_sigFuncao)) {
	  	setStatusValido("5") ;
	  	setStatusTPCVValido("0,1,2,3,4") ;		
	  	setNFuncao("Estorno de Parecer Jur�dico") ;
	  	setTipoReqValidos("DP,DC") ;	
	  	setStatusReqValido("2") ;	
	  	setOrigemEvento("100") ;
	  	setCodEvento("207") ;	
		this.tipoJunta="0";
	}
  }
  public String getNFuncao()   {
	   return ("REC0225".equals(getCFuncao())) ? "Estorno de Parecer Jur�dico e Junta" : "Inclus�o de Parecer Jur�dico e Junta";
  } 
	
   public void setEventoOK(REC.AutoInfracaoBean myAuto) throws sys.BeanException   {
	   setMsg("");
	   setEventoOK("N");	
	   if ("".equals(myAuto.getNomStatus())) return ;
	   setMsg(myAuto.getNomStatus()+" ("+myAuto.getDatStatus()+")") ;
	   try {
	   	if ( (getStatusValido().equals(myAuto.getCodStatus())) ||
	   	     (("".equals(getStatusTPCVValido())==false) && ("1".equals(myAuto.getIndTPCV())) && (getStatusTPCVValido().indexOf(myAuto.getCodStatus())>=0)) ) 
	   		{
	      		Iterator it = myAuto.getRequerimentos().iterator() ;
	      		REC.RequerimentoBean req  = new REC.RequerimentoBean();
	      		boolean existe   = false ;
	      		boolean existeTR = false ;
				
	      		while (it.hasNext()) {
	   				req =(REC.RequerimentoBean)it.next() ;
					// TR nao concluido
					if ( ("TR".equals(req.getCodTipoSolic())) && ("0".equals(req.getCodStatusRequerimento())) ) {
						existeTR = true ;
						break ;
					}
	      			if ( (getTipoReqValidos().indexOf(req.getCodTipoSolic())>=0) && (getStatusReqValido().indexOf(req.getCodStatusRequerimento())>=0) ){
	      				existe = true ;
	      				break ;
	      			}
	      		}
	      		if (existe) {
					if (existeTR) {
						 setEventoOK("N");	
						 setMsg(getMsg() + " - Existe solicita��o de TRI pendente.");						 				
					}
					else setEventoOK("S");	
	      		}
	      		else 	setMsg(getMsg() + " - Sem requerimento para "+getNFuncao()+".");					   	
	      	}
	      	else {
	   		setMsg(getMsg() + " - Status do Auto n�o permite "+getNFuncao());				
	      	}
	   }
	   catch (Exception ue) {
	     throw new sys.BeanException("DefinirRelatorBean: " + ue.getMessage());
	   }

	  return ;	
  } 
//--------------------------------------------------------------------------
public void setTipoJunta(String tipoJunta)  {
  this.tipoJunta=tipoJunta ;
	if (tipoJunta==null) this.tipoJunta= "";
  }  
  public String getTipoJunta()  {
	return this.tipoJunta;
  }    
}
