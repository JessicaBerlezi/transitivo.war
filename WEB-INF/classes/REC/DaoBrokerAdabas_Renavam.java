package REC;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import ACSS.UsuarioBean;
import sys.BeanException;
import sys.DaoException;
import sys.ServiceLocatorException;
import sys.Util;

public class DaoBrokerAdabas_Renavam extends sys.DaoBase implements DaoBroker
{

	private sys.ServiceLocator serviceloc ;  
	private static final String MYABREVSIST = "REC";

	public DaoBrokerAdabas_Renavam() throws DaoException {
		super();
		try {	
			serviceloc =  sys.ServiceLocator.getInstance();
		}
		catch (Exception e) { 			
			throw new DaoException(e.getMessage());
		}
	}
	
	/*****************************************
	 Transa��es de acesso ao Broker
	 *****************************************/
	
	/**
	 *-----------------------------------------------------------
	 * Le Infracao e carrega no Bean - Transacao 041 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public boolean LeAutoInfracao(AutoInfracaoBean myInf,String tpchave,ACSS.UsuarioBean myUsrLog)
	throws DaoException {
		boolean bOk = true;
		Connection conn = null; 
		Statement stmt = null;
		try {
			if ("notificacao".equals(tpchave))  {
				myInf.setNumPlaca("");
				myInf.setNumAutoInfracao("");
				myInf.setNumProcesso("") ;				  
			}
			else {
				myInf.setNumNotificacao("");
				if ( (myInf.getNumPlaca().trim().length()==0) && (myInf.getNumAutoInfracao().trim().length()==0) ) {		
					myInf.setNumPlaca("");
					myInf.setNumAutoInfracao("");
				}
				else myInf.setNumProcesso("") ;
			}
			String numAuto        = sys.Util.rPad(myInf.getNumAutoInfracao()," ",12);
			String numPlaca       = sys.Util.rPad(myInf.getNumPlaca()," ",7);
			String codOrgao       = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",6);
			String numProcesso    = sys.Util.rPad(myInf.getNumProcesso()," ",20);
			String numNotificacao = sys.Util.rPad(myInf.getNumNotificacao()," ",9);
			String vagoWEB        = sys.Util.lPad(myUsrLog.getCodOrgaoAtuacao(),"0",13);
			/*DPWEB*/
			/*String numRenavam     = sys.Util.lPad(myInf.getNumRenavam(),"0",9);*/
			
			String numRenavam     = sys.Util.lPad(myInf.getNumRenavam(),"0",11);
			
			String codEletronico  = sys.Util.lPad(myInf.getCodAcessoWeb(),"0",4);
			String codOrgaoLotacao= sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6);
			String nomUsername    = sys.Util.rPad(myUsrLog.getNomUserName()," ",20);
			

			String resultado = chamaTransBRK("041",numAuto+numPlaca+codOrgao+numProcesso+numNotificacao+numRenavam+codEletronico+codOrgaoLotacao+nomUsername,"",myUsrLog);
			conn = serviceloc.getConnection(MYABREVSIST);	
			stmt = conn.createStatement();
			if (verErro(myInf,resultado,"Auto de Infra��o n�o localizado.",stmt)==false) return false;
			ResultSet rs       = null;
			String sCmd        = "";
			myInf.setMsgErro(""); 
			myInf.setInfracao(new InfracaoBean());
			//			Ler Auto
			setPosIni(3);
			myInf.setNumAutoInfracao(getPedaco(resultado,12," "," "));
			setPosIni(3+395);
			myInf.setNumPlaca(getPedaco(resultado,7,"R"," "));	
			VerificaAutoOracle(myInf,conn);		
			setPosIni(3+12);
			myInf.getInfracao().setCodAutoInfracao(myInf.getCodAutoInfracao());
			myInf.getInfracao().setCodInfracao(getPedaco(resultado,5," "," "));	  				  	  				  
			myInf.getInfracao().setDscInfracao(getPedaco(resultado,80," "," "));
			myInf.getInfracao().setDscEnquadramento(getPedaco(resultado,20," "," "));	  				  	  				  
			myInf.getInfracao().setNumPonto(getPedacoNum(resultado,1,0,"."));	  				  	  				  
			myInf.getInfracao().setDscCompetencia(getPedaco(resultado,2,"R"," "));	  				  	  				  
			myInf.getInfracao().setIndTipo(getPedaco(resultado,1," ", " "));
			myInf.getInfracao().setIndSDD(getPedaco(resultado,1," "," "));
			myInf.setDscLocalInfracao(getPedaco(resultado,80," "," "));	  			
			myInf.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));
			myInf.setValHorInfracao(getPedaco(resultado,4," "," "));				  
			myInf.setCodAgente(getPedacoNum(resultado,2,0,"."));
			VerificaAutoOracle(myInf,conn); /*Poder Gravar Data da Infracao*/			
			
			//			Ler Orgao do Auto
			myInf.setCodOrgao(getPedacoNum(resultado,6,0,"."));
			myInf.getOrgao().Le_Orgao(myInf.getCodOrgao(),0);
			myInf.setSigOrgao(myInf.getOrgao().getSigOrgao());	  				  	  				  
			myInf.setNomOrgao(myInf.getOrgao().getNomOrgao());	 
			myInf.setDscLocalAparelho(getPedaco(resultado,40," "," "));	  
			myInf.setNumCertAferAparelho(getPedaco(resultado,10," "," "));	  			
			myInf.setDatUltAferAparelho(getPedacoDt(resultado,8));	  
			myInf.setNumIdentAparelho(getPedaco(resultado,30, " "," "));	  			
			myInf.setNumInmetroAparelho(getPedaco(resultado,9," "," "));	  
			myInf.setCodTipDispRegistrador(getPedaco(resultado,1," "," "));	  			
			myInf.getInfracao().setValReal(getPedacoNum(resultado,9,2,"."));
			myInf.setDatVencimento(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));	  
			myInf.setCodMunicipio(getPedacoNum(resultado,5,0,"."));
			//			Ler Municipio do Auto
			sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+myInf.getCodMunicipio()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setNomMunicipio(rs.getString("nom_municipio"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			myInf.setValVelocPermitida(getPedacoNum(resultado,9,2,"."));	  
			myInf.setValVelocAferida(getPedacoNum(resultado,9,2,"."));	  
			myInf.setValVelocConsiderada(getPedacoNum(resultado,9,2,"."));	  
			myInf.setDscSituacao(getPedaco(resultado,26,"R"," "));
			
			if (myInf.getDscSituacao().indexOf("*****")>=0) myInf.setDscSituacao(" ");
			// Gravar dados de ve�culo				
			myInf.setVeiculo(new VeiculoBean());
			
			myInf.getVeiculo().setCodVeiculo(myInf.getNumPlaca());
			myInf.getVeiculo().setNumPlaca(getPedaco(resultado,7,"R"," "));	
			myInf.getVeiculo().setCodMarcaModelo(getPedacoNum(resultado,7,0,"."));
			myInf.getVeiculo().setCodEspecie(getPedacoNum(resultado,2,0,"."));
			myInf.getVeiculo().setCodCategoria(getPedacoNum(resultado,2,0,"."));
			myInf.getVeiculo().setCodTipo(getPedacoNum(resultado,3,0,"."));
			myInf.getVeiculo().setCodCor(getPedacoNum(resultado,3,0,"."));
			
			sCmd  = "SELECT dsc_especie from TSMI_ESPECIE WHERE cod_especie = '"+myInf.getVeiculo().getCodEspecie()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.getVeiculo().setDscEspecie(rs.getString("dsc_especie"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			sCmd  = "SELECT dsc_categoria from TSMI_CATEGORIA WHERE cod_categoria = '"+myInf.getVeiculo().getCodCategoria()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.getVeiculo().setDscCategoria(rs.getString("dsc_categoria"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			sCmd  = "SELECT dsc_tipo from TSMI_TIPO WHERE cod_TIPO = '"+myInf.getVeiculo().getCodTipo()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.getVeiculo().setDscTipo(rs.getString("dsc_tipo"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			sCmd  = "SELECT dsc_cor from TSMI_COR WHERE cod_cor = '"+myInf.getVeiculo().getCodCor()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.getVeiculo().setDscCor(rs.getString("dsc_cor"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			sCmd  = "SELECT dsc_marca_modelo from TSMI_MARCA_MODELO WHERE cod_marca_modelo = '"+myInf.getVeiculo().getCodMarcaModelo()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.getVeiculo().setDscMarcaModelo(rs.getString("dsc_marca_modelo"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			// Gravar dados do Propriet�rio
			ResponsavelBean proprietario = new ResponsavelBean();
			proprietario.setNomResponsavel(getPedaco(resultado,60," "," "));
			proprietario.setIndCpfCnpj(getPedaco(resultado,1," "," "));
			proprietario.setNumCpfCnpj(getPedacoNum(resultado,14,0,"."));
			if ("1".equals(proprietario.getIndCpfCnpj())) proprietario.setNumCpfCnpj(sys.Util.lPad(proprietario.getNumCpfCnpj(),"0",11));
			else proprietario.setNumCpfCnpj(sys.Util.lPad(proprietario.getNumCpfCnpj(),"0",14));
			proprietario.setCodResponsavel(proprietario.getNumCpfCnpj());
			proprietario.setIndTipoCnh(getPedaco(resultado,1," "," "));
			proprietario.setNumCnh(getPedaco(resultado,11," "," "));
			sys.EnderecoBean endereco = new sys.EnderecoBean();
			endereco.setTxtEndereco(getPedaco(resultado,27,"R"," "));
			endereco.setNumEndereco(getPedaco(resultado,5," "," "));
			endereco.setSeqEndereco(endereco.getNumEndereco());
			endereco.setTxtComplemento(getPedaco(resultado,11," "," "));
			endereco.setCodCidade(getPedacoNum(resultado,5,0,"."));
			sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+ endereco.getCodCidade()+"'" ;
			
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				endereco.setNomCidade(rs.getString("nom_municipio"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			endereco.setNumCEP(getPedaco(resultado,8," "," "));
			endereco.setCodUF(getPedaco(resultado,2," "," "));
			
			proprietario.setEndereco(endereco);
			
			myInf.setProprietario(proprietario);				
			
			// Gravar dados do Condutor
			
			ResponsavelBean condutor = new ResponsavelBean();
			
			String nomCondutor = getPedaco(resultado,60," "," ");
			if(!nomCondutor.trim().equals("")) { 
				condutor.setNomResponsavel(nomCondutor);
				condutor.setIndCpfCnpj(getPedaco(resultado,1," ", " "));
				condutor.setNumCpfCnpj(sys.Util.lPad(getPedacoNum(resultado,14,0,"."),"0",14));
				condutor.setCodResponsavel(condutor.getNumCpfCnpj());
				condutor.setIndTipoCnh(getPedaco(resultado,1," ", " "));
				condutor.setNumCnh(getPedaco(resultado,11," "," "));
				endereco = new sys.EnderecoBean();
				endereco.setTxtEndereco(getPedaco(resultado,27,"R"," "));
				endereco.setNumEndereco(getPedaco(resultado,5," "," "));
				endereco.setSeqEndereco(endereco.getNumEndereco());
				endereco.setTxtComplemento(getPedaco(resultado,11," "," "));
				endereco.setCodCidade(getPedacoNum(resultado,5,0,"."));
				sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"
					+ endereco.getCodCidade()+"'" ;
				rs    = stmt.executeQuery(sCmd) ;
				while (rs.next()) {
					endereco.setNomCidade(rs.getString("nom_municipio"));	  				  	  				  
				}
				if (rs!=null) rs.close();
				
				endereco.setNumCEP(getPedaco(resultado,8," "," "));
				endereco.setCodUF(getPedaco(resultado,2," "," "));
				condutor.setEndereco(endereco);
				myInf.setCondutor(condutor);
				if (!proprietario.getNomResponsavel().equals(condutor.getNomResponsavel()) ) 
					myInf.setIndCondutorIdentificado("1");
				else myInf.setIndCondutorIdentificado("0");
			}
			else {
				setPosIni(712);	//Estava 709 == Wellem 10-09-2010
				myInf.setCondutor(myInf.getProprietario());
				myInf.setIndCondutorIdentificado("0");
			}
			String cs= getPedacoNum(resultado,3,0,".");
			myInf.setCodStatus(cs);
			
			if ("88".equals(myInf.getCodStatus())) myInf.setCodStatus("0");
			sCmd  = "SELECT nom_status from TSMI_STATUS_AUTO WHERE cod_status = '"+myInf.getCodStatus()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			myInf.setNomStatus("Status: "+myInf.getCodStatus());
			while (rs.next()) {
				myInf.setNomStatus(rs.getString("nom_status"));	  				  	  				  
			}
			if (rs!=null) rs.close();
			myInf.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));	  			
			myInf.setDatEnvioDO(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));	  			
			myInf.setCodEnvioDO(getPedaco(resultado,20," "," "));		
			myInf.setDatPublPDO(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));	  		
			
			myInf.setIndTPCV(getPedaco(resultado,1," "," "));
			myInf.setNumProcesso(getPedaco(resultado,20," "," "));
			myInf.setDatProcesso(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));
			myInf.getInfracao().setValRealDesconto(getPedacoNum(resultado,9,2,"."));
			myInf.setCodResultPubDO(getPedaco(resultado,1," "," "));
			myInf.setCodBarra(getPedaco(resultado,48," "," "));				
			myInf.setNumNotifAut(getPedaco(resultado,9," ","."));

			/*Pegar Data C�digo de Barra Processo
			sCmd  = "SELECT * FROM TSMI_CONTROLE_PROCESSO WHERE "+
				    "NUM_PROCESSO='"+myInf.getNumProcesso()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setDtGerCBProcesso(rs.getString("DAT_GERACAO"));
			}
			if (rs!=null) rs.close();*/
			
			/*Pegar Data e Sequencia C�digo de Barra Notificaca��o Autua��o
			sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
				    "NUM_NOTIFICACAO='"+myInf.getNumNotifAut()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setDtGerCBNotificacaoAut(rs.getString("DAT_GERACAO_CB_AI"));
				myInf.setSeqGeracaoCBNotificacaoAut(rs.getInt("SEQ_GERACAO_CB_AI"));
			}
			if (rs!=null) rs.close();
			*/
			
			myInf.setDatNotifAut(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));				
			myInf.setCodNotifAut(getPedaco(resultado,2," ","."));
			myInf.setNumARNotifAut(getPedaco(resultado,8," ",".")) ;
			myInf.setNumNotifPen(getPedaco(resultado,9," ","."));

			/*Pegar Data e Sequencia C�digo de Barra Notificaca��o Penalidade*/
			/*
			sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
				    "NUM_NOTIFICACAO='"+myInf.getNumNotifPen()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setDtGerCBNotificacaoPen(rs.getString("DAT_GERACAO_CB_AI"));
				myInf.setSeqGeracaoCBNotificacaoPen(rs.getInt("SEQ_GERACAO_CB_AI"));
			}
			if (rs!=null) rs.close();*/
			
			myInf.setDatNotifPen(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));				
			myInf.setCodNotifPen(getPedaco(resultado,2," ","."));
			myInf.setNumARNotifPen(getPedaco(resultado,8," ",".")) ;
			myInf.setIndFase(getPedaco(resultado,1," "," "));		
			myInf.setIndSituacao(getPedaco(resultado,1," "," "));				
			myInf.setIndPago(getPedaco(resultado,1," "," "));	
			myInf.setDscAgente(getPedaco(resultado,15," "," "));
			myInf.setDscUnidade(getPedacoNum(resultado,4,0,"."));
			myInf.setDscLote(getPedacoNum(resultado,5,0,"."));
			myInf.setDatPag(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));
			myInf.setNumAutoOrigem(getPedaco(resultado,12," "," "));
			myInf.setNumInfracaoOrigem(getPedacoNum(resultado,3,0,"."));
			/*Renainf*/			
			myInf.setIdentAutoOrigem(getPedacoNum(resultado,1,0,"."));
			myInf.setNumInfracaoRenainf(getPedaco(resultado,12," "," "));			
			/*Outro Orgao - Niteroi*/
			myInf.setIdentOrgao(getPedaco(resultado,9," "," "));
			/*DPWEB*/
			myInf.setCodAcessoWeb(getPedacoNum(resultado,4,0,"."));
			myInf.setObsCondutor(getPedaco(resultado,60," "," "));
			myInf.setIndPontuacao(getPedacoNum(resultado,1,0,"."));
			
			String sEspacamento=getPedaco(resultado,150," "," ");
			
			
			myInf.setIndParcelamento(getPedaco(resultado,1," "," "));
			
			
			
			
		  
			 
			sCmd  = "SELECT DAT_INCLUSAO, DAT_ALTERACAO, DAT_REGISTRO, DSC_RESUMO_INFRACAO, DSC_MARCA_MODELO_APARELHO, DSC_LOCAL_INFRACAO_NOTIF";
			sCmd  += " FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = '" + myInf.getNumAutoInfracao() + "'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
				if (rs.getDate("DAT_INCLUSAO") != null)
					myInf.setDatInclusao(df.format(rs.getDate("DAT_INCLUSAO")));
				else
					myInf.setDatInclusao(df.format(new Date()));
				if (rs.getDate("DAT_ALTERACAO") != null)
					myInf.setDatAlteracao(df.format(rs.getDate("DAT_ALTERACAO")));
				else
					myInf.setDatAlteracao("");
				if (rs.getDate("DAT_REGISTRO") != null)
					myInf.setDatRegistro(df.format(rs.getDate("DAT_REGISTRO")));
				else
					myInf.setDatRegistro("");				
				myInf.setDscResumoInfracao(rs.getString("DSC_RESUMO_INFRACAO"));
				myInf.setDscMarcaModeloAparelho(rs.getString("DSC_MARCA_MODELO_APARELHO"));
				myInf.setDscLocalInfracaoNotificacao(rs.getString("DSC_LOCAL_INFRACAO_NOTIF"));
			}
			if (rs!=null) rs.close();
			String cra = "N";				
			sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE num_codigo_retorno = '"+myInf.getCodNotifAut()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				cra =rs.getString("ind_entregue");
				myInf.setNomMotivoNotifAut(rs.getString("DSC_CODIGO_RETORNO"));		  	
				if ("S".equals(cra)==false) myInf.setDatNotifAut("");
			}
			if (rs!=null) rs.close();
			String crb = "N";
			sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE num_codigo_retorno = '"+myInf.getCodNotifPen()+"'" ;
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				crb =rs.getString("ind_entregue");
				myInf.setNomMotivoNotifPen(rs.getString("DSC_CODIGO_RETORNO"));
				if ("S".equals(crb)==false)	myInf.setDatNotifPen("");
			}
			if (rs!=null) rs.close();
			if (sys.Util.DifereDias("16/07/2004",myInf.getDatInfracao())<0) { 
				myInf.setDatNotificacao(myInf.getDatNotifPen());
				myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifPen());
			}
			else {		
				if (myInf.getCodStatus().length()>1){ 
					myInf.setDatNotificacao(myInf.getDatNotifPen());
					myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifPen());				
				}
				else { 
					myInf.setDatNotificacao(myInf.getDatNotifAut()) ;
					myInf.setNomMotivoNotificacao(myInf.getNomMotivoNotifAut());				
				}
			}
			
			myInf.getInfracao().setCodAutoInfracao(myInf.getCodAutoInfracao());
			// Verifica se tem AR Digitalizado
			sCmd  = "SELECT COD_AR_DIGITALIZADO FROM TSMI_AR_DIGITALIZADO "+
			"WHERE NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"' "+
			"AND length (NUM_NOTIFICACAO) < 10 "+
			"AND (COD_RETORNO is null " +
			"	  OR " +
			"	  COD_RETORNO NOT IN ('99','98')) ";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setTemARDigitalizado(true);			
			}
			if (rs!=null) rs.close();
			// Verifica se tem AI Digitalizado
			sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
			" WHERE NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setTemAIDigitalizado(true);			
			}
			if (rs!=null) rs.close();
			// Verifica se tem Foto Digitalizada
			sCmd  = "SELECT COD_FOTO_DIGITALIZADA FROM TSMI_FOTO_DIGITALIZADA "+
			" WHERE NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myInf.setTemFotoDigitalizada(true);			
			}		    
			if (rs!=null) rs.close();
			if (stmt!=null) stmt.close();
			
			/*FIXME: Solicita��o atrav�s da CI CJC/DRI N�112/2012, para adicionar um nome do Agente se o C�digo da infra��o for 69200
			/ @jefferson.trindade 02/07/2012*/
			if (myInf.getInfracao().getCodInfracao().equals("69200")){
				myInf.setDscAgente("24/001951-3");
				myInf.setNomAgente("Frederico Loureiro Filho");
			}
			
		}
		catch (SQLException e) {
			bOk = false ;
			myInf.setNumAutoInfracao("0") ;
			throw new DaoException(e.getMessage()); 
		}
		catch (Exception ex) {
			bOk = false ;
			myInf.setNumAutoInfracao("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try {
					if (stmt!=null) stmt.close();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/**
	 *-----------------------------------------------------------
	 * Le Historico - Transa��es 044 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public  boolean LeHistorico (AutoInfracaoBean myInf,UsuarioBean myUsuario) throws DaoException {
		boolean bOk = true ;
		Connection conn =null ;
		Statement  stmt = null; 
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String indContinua = "               ";
			List historicos    = new ArrayList();
			Vector h           = new Vector();
			
			String codOrgao       = sys.Util.lPad(myUsuario.getCodOrgaoAtuacao(),"0",6);
			String codOrgaoLotacao= sys.Util.lPad(myUsuario.getOrgao().getCodOrgao(),"0",6);
			String nomUsername    = sys.Util.rPad(myUsuario.getNomUserName()," ",20);
			
			int reg = 0 ;
			while (continua) {
				resultado = chamaTransBRK("044",sys.Util.rPad(myInf.getNumAutoInfracao()," ",12),indContinua+codOrgao+codOrgaoLotacao+nomUsername,myUsuario);				
				if (verErro(myInf,resultado,"Auto de Infra��o - Ler Historicos.",stmt)==false) return false;
				String linha = "";
				for (int i=0; i < 2; i++) {
					linha = resultado.substring((i*1482)+3,(i+1)*1482+3);
					HistoricoBean myHis = new HistoricoBean();
					myHis.setCodHistoricoAuto(Integer.toString(reg*2+i));	  				  	  				  				
					myHis.setCodAutoInfracao(myInf.getCodAutoInfracao());
					myHis.setCodOrgao(myInf.getCodOrgao());
					setPosIni(12);
					myHis.setNumProcesso(getPedaco(linha,20," "," "));
					myHis.setCodStatus(getPedacoNum(linha,3,0,"."));	 
					
					myHis.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					
					if (myHis.getCodStatus().equals("1")){// Notificado de Autua��o
						myInf.setDatNotifAut(myHis.getDatStatus());
					}
					if (myHis.getCodStatus().equals("11")){// Notificado de Penalidade
						myInf.setDatNotifPen(myHis.getDatStatus());
					}
					
					myHis.setCodEvento(getPedaco(linha,3," "," "));	
					if (myHis.getCodEvento().equals("907")){
						myInf.setIndCondutorIdentificado("0");
					}
					myHis.setCodOrigemEvento(getPedaco(linha,3," "," "));
					myHis.setNomUserName(getPedaco(linha,20," "," "));
					myHis.setCodOrgaoLotacao(getPedacoNum(linha,6,0,"."));
					if ("0".equals(myHis.getCodOrgaoLotacao())) myHis.setCodOrgaoLotacao("");
					
					sCmd  = "SELECT sig_Orgao,nom_Orgao from TSMI_ORGAO WHERE cod_orgao = '"+myHis.getCodOrgaoLotacao()+"'" ;
					rs    = stmt.executeQuery(sCmd) ;
					if (rs.next()) myHis.setSigOrgaoLotacao(rs.getString("SIG_ORGAO"));
					
					
					
					
					
					myHis.setDatProc(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));	  				  	  				  
					myHis.setTxtComplemento01(getPedaco(linha,30," "," "));	
					myHis.setTxtComplemento02(getPedaco(linha,45," "," "));	
					myHis.setTxtComplemento03(getPedaco(linha,50," "," "));	
					myHis.setTxtComplemento04(getPedaco(linha,1000," "," "));	
					myHis.setTxtComplemento05(getPedaco(linha,30," "," "));	
					myHis.setTxtComplemento06(getPedaco(linha,50," "," "));	
					myHis.setTxtComplemento07(getPedaco(linha,50," "," "));	
					myHis.setTxtComplemento08(getPedaco(linha,50," "," "));	
					myHis.setTxtComplemento09(getPedaco(linha,12," "," "));	
					myHis.setTxtComplemento10(getPedaco(linha,2," "," "));
					myHis.setTxtComplemento11(getPedaco(linha,30," "," "));					
					myHis.setTxtComplemento12(getPedaco(linha,50," "," "));						
					if ((myHis.getCodEvento().trim().length()>0) && ("000".equals(myHis.getCodEvento())==false)) {	
						sCmd  = "SELECT nom_status from TSMI_STATUS_AUTO WHERE cod_status = '"+myHis.getCodStatus()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myHis.setNomStatus(rs.getString("nom_status"));	  				  	  				  
						}
						sCmd  = "SELECT dsc_origem_evento from TSMI_ORIGEM_EVENTO WHERE cod_origem_evento = '"+myHis.getCodOrigemEvento()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myHis.setNomOrigemEvento(rs.getString("dsc_origem_evento"));	  				  	  				  
						}
						sCmd  = "SELECT dsc_evento from TSMI_EVENTO WHERE cod_evento = '"+myHis.getCodEvento()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myHis.setNomEvento(rs.getString("dsc_evento"));	  				  	  				  
						}
						if ("214".equals(myHis.getCodEvento())) {	
							sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"
								+myHis.getTxtComplemento06().trim()+"'" ;
							rs    = stmt.executeQuery(sCmd) ;
							while (rs.next()) {
								myHis.setTxtComplemento06(rs.getString("nom_municipio"));	  				  	  				  
							}
							// Editar campos
							// CPF
							if (myHis.getTxtComplemento02().length()<11) myHis.setTxtComplemento02(myHis.getTxtComplemento02()+"               ") ;
							myHis.setTxtComplemento02(myHis.getTxtComplemento02().substring(0,3)+"."+
									myHis.getTxtComplemento02().substring(3,6)+"."+ 
									myHis.getTxtComplemento02().substring(6,9)+"-"+
									myHis.getTxtComplemento02().substring(9,11));
							//CEP
							if (myHis.getTxtComplemento08().length()<8) myHis.setTxtComplemento08(myHis.getTxtComplemento08()+"               ") ;
							myHis.setTxtComplemento08(myHis.getTxtComplemento08().substring(0,5)+"-"+
									myHis.getTxtComplemento08().substring(5,8));
							//CNH
							String p =myHis.getTxtComplemento09().substring(0,1);
							myHis.setTxtComplemento09(p+" - "+sys.Util.SemZeros(myHis.getTxtComplemento09().substring(1)));
						}
						if ("204,324,203,323".indexOf(myHis.getCodEvento())>=0) 
						{
							sCmd  = "SELECT DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE NUM_CODIGO_RETORNO = '"+myHis.getTxtComplemento01()+"'" ;
							rs    = stmt.executeQuery(sCmd) ;
							while (rs.next()) {
								myHis.setTxtComplemento03(rs.getString("DSC_CODIGO_RETORNO"));	  				  	  				  
							}
						}
                        if ("383,390".indexOf(myHis.getCodEvento())>=0) 
                        {
                            sCmd  = "SELECT COD_ENVIO_PUBDO from TSMI_REQUERIMENTO WHERE NUM_REQUERIMENTO = '"+myHis.getTxtComplemento02()+"'" ;
                            rs    = stmt.executeQuery(sCmd) ;
                            while (rs.next()) {
                                myHis.setTxtComplemento03(rs.getString("COD_ENVIO_PUBDO"));                                        
                            }
                        }
                        if ("903".indexOf(myHis.getCodEvento())>=0) 
                        {
                        	/*
                            sCmd  = "SELECT TXT_COMPLEMENTO_03 from TSMI_HISTORICO_AUTO WHERE COD_AUTO_INFRACAO = '"+myHis.getCodAutoInfracao()+"'" ;
                            rs    = stmt.executeQuery(sCmd) ;
                            while (rs.next()) {
                                myHis.setTxtComplemento03(rs.getString("TXT_COMPLEMENTO_03"));                                        
                            }
                            */
                        }
                        if ("200".equals(myHis.getCodEvento())) 
                        {	
                        	sCmd = "SELECT ARQ.NOM_ARQUIVO, to_char(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy') AS DAT_RECEBIMENTO" + 
                        		"	FROM tsmi_arquivo_dol ARQ,  " +
                        		"	TSMI_LOTE_DOL ADOL, TSMI_LINHA_LOTE LDOL" +
                        		"	WHERE ARQ.COD_IDENT_ARQUIVO = 'DE01'" +
                        		"	AND ADOL.COD_ARQUIVO_DOL = ARQ.COD_ARQUIVO_dol" +
                        		"	AND ADOL.COD_LOTE = LDOL.COD_LOTE " +
                        		"	AND LDOL.NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"'" + 
                        		"	AND ARQ.COD_STATUS = 2"	;	

	                    	rs    = stmt.executeQuery(sCmd) ;
	                        while (rs.next()) {
	                            myHis.setTxtComplemento05(rs.getString("NOM_ARQUIVO"));  
	                            myHis.setTxtComplemento06(rs.getString("DAT_RECEBIMENTO")); 
	                        }
	                        if(!rs.next()){
	                        	sCmd = "SELECT ARQ.NOM_ARQUIVO, to_char(ARQ.DAT_RECEBIMENTO,'dd/mm/yyyy') AS DAT_RECEBIMENTO FROM " +
	                    		"	TSMI_ARQUIVO_RECEBIDO ARQ, TSMI_LINHA_ARQUIVO_REC LIN " +
	                    		"	WHERE ARQ.COD_IDENT_ARQUIVO = 'MR01' " +
	                    		"	AND ARQ.COD_ARQUIVO = LIN.COD_ARQUIVO " +
	                    		"	AND LIN.NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao()+"' " +
	                    		"	AND LIN.COD_ORGAO =  ARQ.COD_ORGAO " +
	                    		"	AND ARQ.COD_STATUS = 2";
	                    	    rs    = stmt.executeQuery(sCmd) ;
	                            while (rs.next()) {
	                                myHis.setTxtComplemento05(rs.getString("NOM_ARQUIVO"));  
	                                myHis.setTxtComplemento06(rs.getString("DAT_RECEBIMENTO")); 
	                            }
	                        }
                        }                        
						acertaComplemento(myHis);
						h.addElement(myHis) ;
					}
				}
				reg++;
				setPosIni(2*1482+3);
				indContinua = getPedaco(resultado,15," "," ") ;
				if (indContinua.length()<=0) continua=false;
			}
			int t = h.size();
			for (int i=t-1;i>=0;i--) {
				HistoricoBean hh = (HistoricoBean)h.elementAt(i);
				historicos.add(hh) ;			
			}
			myInf.setHistoricos(historicos);
			
			rs.close();
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try 
				{
					stmt.close();					
					serviceloc.setReleaseConnection(conn);
				}
				catch (Exception ey) { }
			}
		}
		return bOk;
	}	
	
	/**
	 *-----------------------------------------------------------
	 * Le Requerimento - Transa��es 045 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public  boolean LeRequerimento (AutoInfracaoBean myInf,UsuarioBean myUsuario) throws DaoException {
		boolean bOk = true ;
		Connection conn =null ;
		Statement stmt = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;	
			stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			
			boolean continua   = true;
			String resultado   = "";
			String indContinua = "               ";
			List requerimentos = new ArrayList();
			int reg=0;
			
			String codOrgao       = sys.Util.lPad(myUsuario.getCodOrgaoAtuacao(),"0",6);
			String codOrgaoLotacao= sys.Util.lPad(myUsuario.getOrgao().getCodOrgao(),"0",6);
			String nomUsername    = sys.Util.rPad(myUsuario.getNomUserName()," ",20);
			
			while (continua) {
				resultado = chamaTransBRK("045",sys.Util.rPad(myInf.getNumAutoInfracao()," ",12),indContinua+codOrgao+codOrgaoLotacao+nomUsername,myUsuario);						
				if (verErro(myInf,resultado,"Auto de Infra��o - Ler Requerimentos.",stmt)==false) return false;
				String linha = resultado.substring(3);
				RequerimentoBean myReq = new RequerimentoBean();
				myReq.setCodRequerimento(Integer.toString(reg));		  
				myReq.setCodAutoInfracao(myInf.getCodAutoInfracao());
				setPosIni(12);						  				  	  				  
				myReq.setCodTipoSolic(getPedaco(linha,2," "," "));
				setPosIni(104);				
				myReq.setCodStatusRequerimento(getPedacoNum(linha,3,0,"."));					
				setPosIni(14);							
				
				if ((myReq.getCodTipoSolic().trim().length()>0) && 
						("7".equals(myReq.getCodStatusRequerimento())==false) ) {
					
					myReq.setNumRequerimento(getPedaco(linha,30," "," "));
					myReq.setCodResponsavelDP(Integer.toString(reg));	 
					myReq.setNomResponsavelDP(getPedaco(linha,60," "," "));	  				  	  				  			 				  	  				  
					myReq.setCodStatusRequerimento(getPedacoNum(linha,3,0,"."));	  				  	  				  
					if ("88".equals(myReq.getCodStatusRequerimento())) myReq.setCodStatusRequerimento("0");
					// ABERTURA
					myReq.setNomUserNameDP(getPedaco(linha,20," "," "));
					myReq.setCodOrgaoLotacaoDP(getPedacoNum(linha,6,0,"."));
					myReq.setDatProcDP(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));	  				  	  				  
					myReq.setDatRequerimento(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));					
					
					//PARECER JURIDICO
					String dx = getPedacoDt(linha,8);
					if ("00000000".equals(dx)==false) {
						myReq.setDatPJ(sys.Util.formataDataDDMMYYYY(dx));
						myReq.setCodParecerPJ(getPedaco(linha,1," "," "));
						myReq.setCodRespPJ(getPedacoNum(linha,11,0,"."));
						myReq.setTxtMotivoPJ(getPedaco(linha,1000," "," "));
						myReq.setNomUserNamePJ(getPedaco(linha,20," "," "));
						myReq.setCodOrgaoLotacaoPJ(getPedacoNum(linha,6,0,"."));
						myReq.setDatProcPJ(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));	  				  	  				  
					}
					else setPosIni(getPosIni()+1+1000+11+20+6+8) ;
					
					// DEFINE/TROCA RELATOR
					dx = getPedacoDt(linha,8);
					
					if ("00000000".equals(dx)==false) {					
						myReq.setDatJU(sys.Util.formataDataDDMMYYYY(dx));
						myReq.setCodJuntaJU(getPedacoNum(linha,6,0,"."));
						myReq.setCodRelatorJU(getPedacoNum(linha,11,0,"."));			
						myReq.setNomUserNameJU(getPedaco(linha,20," "," "));
						myReq.setCodOrgaoLotacaoJU(getPedacoNum(linha,6,0,"."));
						myReq.setDatProcJU(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					}
					else setPosIni(getPosIni()+6+11+20+6+8) ;
					
					// RESULTADO
					dx = getPedacoDt(linha,8);
					if ("00000000".equals(dx)==false) {										
						myReq.setDatRS(sys.Util.formataDataDDMMYYYY(dx));
						myReq.setCodResultRS(getPedaco(linha,1," "," "));
						myReq.setTxtMotivoRS(getPedaco(linha,1000," "," "));
						myReq.setNomUserNameRS(getPedaco(linha,20," "," "));
						myReq.setCodOrgaoLotacaoRS(getPedacoNum(linha,6,0,"."));
						myReq.setDatProcRS(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));				
					}
					else setPosIni(getPosIni()+1+1000+20+6+8) ;
					
					// ATUALIZAR TRI
					myReq.setNomCondutorTR(getPedaco(linha,60," "," "));					
					myReq.setIndTipoCNHTR(getPedaco(linha,1," "," "));
					myReq.setNumCNHTR(getPedacoNum(linha,11,0,"."));
					myReq.setCodUfCNHTR(getPedaco(linha,2," "," "));
					myReq.setNumCPFTR(getPedaco(linha,11," "," "));										
					myReq.setDatAtuTR(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					myReq.setNomUserNameTR(getPedaco(linha,20," "," "));
					myReq.setCodOrgaoLotacaoTR(getPedacoNum(linha,6,0,"."));
					myReq.setDatProcTR(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					/*DPWEB*/
					myReq.setIndReqWEB(getPedacoNum(linha,1,0,"."));
					
					sCmd =  "SELECT * FROM TSMI_REQUERIMENTO_WEB WHERE "+
					"COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"' and "+              
					"NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
					rs = stmt.executeQuery(sCmd) ;			 
					if (rs.next())
					{
						myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));		  
						myReq.setNomRequerente(rs.getString("NOM_REQUERENTE"));
						myReq.setTxtJustificativa(rs.getString("TXT_MOTIVO"));				 
						myReq.setTxtEmail(rs.getString("TXT_EMAIL"));						
					}
					
					//DPWEB - Arquivo Requerimento
					sCmd =  "SELECT * FROM SMIT.TSMI_ARQUIVO_REQUERIMENTO_WEB WHERE "+
							"COD_REQUERIMENTO='"+myReq.getCodRequerimento()+"'";
							rs = stmt.executeQuery(sCmd) ;			 
							while (rs.next())
							{
								ArquivoRequerimentoBean arquivoRequerimento = new ArquivoRequerimentoBean();
								arquivoRequerimento.setCodArquivo(rs.getString("COD_ARQUIVO"));
								arquivoRequerimento.setNomeArquivo(rs.getString("NOM_ARQUIVO"));
								arquivoRequerimento.setDataRecebimento(rs.getString("DAT_RECEBIMENTO"));
								arquivoRequerimento.setNumAnoExercicio(rs.getString("NUM_ANO_EXERCICIO"));
								arquivoRequerimento.setTipoArquivo(rs.getString("COD_TIPO_ARQUIVO"));
								myReq.getArquivosRequerimentoBean().add(arquivoRequerimento);
							}
							
					sCmd  = "SELECT * from TSMI_REQUERIMENTO WHERE "+
					"COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"' AND "+
					"NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
					rs    = stmt.executeQuery(sCmd) ;
					if (rs.next())
					{
						myReq.setDatRecebimentoGuia(converteData(rs.getDate("DAT_RECEB_GUIA")));
						myReq.setDatEnvioGuia(converteData(rs.getDate("DAT_ENVIO_GUIA")));
						myReq.setNomUserNameRecebimento(rs.getString("NOM_USERNAME_RECEB_GUIA"));
						myReq.setNomUserNameEnvio(rs.getString("NOM_USERNAME_ENVIO_GUIA"));
						myReq.setCodRemessa(rs.getString("COD_REMESSA"));
                        myReq.setDatEnvioDO(converteData(rs.getDate("DAT_ENVIO_PUBDO")));
                        myReq.setDatPublPDO(converteData(rs.getDate("DAT_PUBDO")));
                        myReq.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
                        myReq.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));                        
					}
					
					sCmd  = "SELECT I.cod_lote_relator,I.TXT_MOTIVO_RS from TSMI_ITEM_LOTE_RELATOR I, TSMI_LOTE_RELATOR L WHERE num_requerimento = '"+myReq.getNumRequerimento()+"'"+
					" AND I.COD_LOTE_RELATOR = L.COD_LOTE_RELATOR AND COD_STATUS <> 8"+
					/*
					" AND L.COD_ORGAO = '"+myInf.getCodOrgao() +"'"+ --> AUTO DE INFRA��O UNICO0
					*/
					" AND I.NUM_AUTO_INFRACAO = '"+myInf.getNumAutoInfracao() +"'"+
					" ORDER BY COD_LOTE_RELATOR" ;					
					rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myReq.setNumGuiaDistr(rs.getString("cod_lote_relator"));
						myReq.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
					}
					
					sCmd  = "SELECT nom_resp_parecer from TSMI_RESP_PARECER WHERE cod_resp_parecer = '"+myReq.getCodRespPJ()+"'" ;
					rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myReq.setNomRespPJ(rs.getString("nom_resp_parecer"));	  				  	  				  
					}
					
					sCmd  = "SELECT Nom_Relator from TSMI_RELATOR WHERE num_cpf = '"+myReq.getCodRelatorJU()+"'" ;
					rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myReq.setNomRelatorJU(rs.getString("Nom_Relator"));	  				  	  				  
					}					
					sCmd  = "SELECT sig_junta from TSMI_JUNTA WHERE cod_junta = '"+myReq.getCodJuntaJU()+"'" ; 
					rs    = stmt.executeQuery(sCmd) ;
					while (rs.next()) {
						myReq.setSigJuntaJU(rs.getString("sig_junta"));	  				  	  				  
					}
					requerimentos.add(myReq) ;
				}					
				/*else setPosIni(2392);*/
				/*DPWEB*/
//				else setPosIni(2391); antigo				
				else setPosIni(2433);				

				
				indContinua = getPedaco(linha,15," "," ") ;
				if ((indContinua.trim().length()<=0) || (reg>30)) continua=false;
				reg++;
			}
			
			/*Ler Indeferimento de Plano*/
			sCmd =  "SELECT * FROM TSMI_REQUERIMENTO WHERE "+
			"COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"' AND "+
			"COD_TIPO_SOLIC IN ('DI','1I','2I') "+
			"ORDER BY DAT_REQUERIMENTO DESC";
			rs = stmt.executeQuery(sCmd) ;
			while (rs.next())
			{
				 RequerimentoBean myReq = new RequerimentoBean();				
				 myReq.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				 myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				 myReq.setCodTipoSolic(rs.getString("COD_TIPO_SOLIC"));				 
				 myReq.setDatRequerimento(converteData(rs.getDate("DAT_REQUERIMENTO")));
				 myReq.setCodStatusRequerimento(rs.getString("COD_STATUS_REQUERIMENTO"));
				 myReq.setNomUserNameDP(rs.getString("NOM_USERNAME_DP"));
				 myReq.setCodOrgaoLotacaoDP(rs.getString("COD_ORGAO_LOTACAO_DP"));
				 myReq.setNomResponsavelDP(rs.getString("NOM_RESP_DP"));			
				 myReq.setDatProcDP(converteData(rs.getDate("DAT_PROC_DP")));
				 myReq.setDatPJ(converteData(rs.getDate("DAT_PJ")));                
				 myReq.setCodParecerPJ(rs.getString("COD_PARECER_PJ"));
				 myReq.setCodRespPJ(rs.getString("COD_RESP_PJ"));
				 myReq.setTxtMotivoPJ(rs.getString("TXT_MOTIVO_PJ"));
				 myReq.setNomUserNamePJ(rs.getString("NOM_USERNAME_PJ"));
				 myReq.setCodOrgaoLotacaoPJ(rs.getString("COD_ORGAO_LOTACAO_PJ"));
				 myReq.setDatProcPJ(converteData(rs.getDate("DAT_PROC_PJ")));
				 myReq.setDatJU(converteData(rs.getDate("DAT_JU")));
				 myReq.setCodJuntaJU(rs.getString("COD_JUNTA_JU"));
				 myReq.setCodRelatorJU(rs.getString("COD_RELATOR_JU"));
				 myReq.setNomUserNameJU(rs.getString("NOM_USERNAME_JU"));
				 myReq.setCodOrgaoLotacaoJU(rs.getString("COD_ORGAO_LOTACAO_JU"));
				 myReq.setDatProcJU(converteData(rs.getDate("DAT_PROC_JU")));
				 myReq.setDatRS(converteData(rs.getDate("DAT_RS")));
				 myReq.setCodResultRS(rs.getString("COD_RESULT_RS"));
				 myReq.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				 myReq.setNomUserNameRS(rs.getString("NOM_USERNAME_RS"));
				 myReq.setCodOrgaoLotacaoRS(rs.getString("COD_ORGAO_LOTACAO_RS"));
				 myReq.setDatProcRS(converteData(rs.getDate("DAT_PROC_RS")));
				 myReq.setNomCondutorTR(rs.getString("NOM_CONDUTOR_TR"));
				 myReq.setIndTipoCNHTR(rs.getString("IND_TIPO_CNH_TR"));
				 myReq.setNumCNHTR(rs.getString("NUM_CNH_TR"));
				 myReq.setCodUfCNHTR(rs.getString("COD_UF_CNH_TR"));
				 myReq.setNumCPFTR(rs.getString("NUM_CPF_TR"));	
				 myReq.setDatAtuTR(converteData(rs.getDate("DAT_ATU_TR")));
				 myReq.setNomUserNameTR(rs.getString("NOM_USERNAME_TR"));
				 myReq.setCodOrgaoLotacaoTR(rs.getString("COD_ORGAO_LOTACAO_TR"));
				 myReq.setSitProtocolo(rs.getString("SIT_PROTOCOLO"));
				 myReq.setDatRecebimentoGuia(converteData(rs.getDate("DAT_RECEB_GUIA")));
				 myReq.setDatEnvioGuia(converteData(rs.getDate("DAT_ENVIO_GUIA")));
				 myReq.setNomUserNameRecebimento(rs.getString("NOM_USERNAME_RECEB_GUIA"));
				 myReq.setNomUserNameEnvio(rs.getString("NOM_USERNAME_ENVIO_GUIA"));
				 myReq.setCodRemessa(rs.getString("COD_REMESSA"));		
				 requerimentos.add(myReq) ;
			}
			myInf.setRequerimentos(requerimentos);
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { 
					stmt.close();
					serviceloc.setReleaseConnection(conn); 
				}
				catch (Exception ey) { }
			}
		}
		return bOk;
	}
	
	/******************************************
	 ConsultaAuto e carrega no Bean - Transa��es 048 - 11/11/2005
	******************************************/
	public  boolean ConsultaAutos (consultaAutoBean myConsult,UsuarioBean myUsuario) throws DaoException {
		boolean bOk = true;
		Connection conn = null;
		Statement stmt = null;
		try {
			String opcEscolha = "";
			if ("".equals(myConsult.getNumCPF().trim())==false) opcEscolha += sys.Util.lPad(myConsult.getNumCPF(),"0",11) ;
			else opcEscolha += sys.Util.lPad(" "," ",11) ;
			if (myConsult.getNumPlaca().length() > 0)           opcEscolha += sys.Util.rPad(myConsult.getNumPlaca()," ",7) ;
			else opcEscolha += sys.Util.lPad(" "," ",7) ;
			
			/*if (myConsult.getNumRenavam().length() > 0)         opcEscolha += sys.Util.lPad(myConsult.getNumRenavam(),"0",9) ;*/
			
			if (myConsult.getNumRenavam().length() > 0)         opcEscolha += sys.Util.lPad(myConsult.getNumRenavam(),"0",11) ;
			
			else opcEscolha += sys.Util.lPad(" "," ",9) ;
			if (myConsult.getNumProcesso().length() > 0)        opcEscolha += sys.Util.rPad(myConsult.getNumProcesso()," ",20) ;
			else opcEscolha += sys.Util.lPad(" "," ",20);
			if (myConsult.getNumAutoInfracao().length() > 0)    opcEscolha += sys.Util.rPad(myConsult.getNumAutoInfracao()," ",12) ;
			else opcEscolha += sys.Util.lPad(" "," ",12);
			
			opcEscolha += sys.Util.lPad(" "," ",3);
			
			if (myConsult.getDatInfracaoDe().length() > 0)      opcEscolha += sys.Util.formataDataYYYYMMDD(myConsult.getDatInfracaoDe()) ;
			else opcEscolha += sys.Util.lPad(" "," ",8);
			if (myConsult.getDatInfracaoAte().length() > 0)     opcEscolha += sys.Util.formataDataYYYYMMDD(myConsult.getDatInfracaoAte()) ;
			else opcEscolha += sys.Util.lPad(" "," ",8);
			
			if (myConsult.getIndFase().length() > 0)            opcEscolha += myConsult.getIndFase() ;
			else opcEscolha += "9";
			if (myConsult.getIndSituacao().length() > 0)        opcEscolha += sys.Util.rPad(myConsult.getIndSituacao()," ",3) ;
			else opcEscolha += "9  ";
			if (myConsult.getIndPago().length() > 0)            opcEscolha += myConsult.getIndPago() ;
			else opcEscolha += "9";
			
			conn = serviceloc.getConnection(MYABREVSIST);	
			stmt = conn.createStatement();
			String sCmd        = "";
			ResultSet rs       = null;	  
			boolean continua   = true;
			String resultado   = "";
			String indContinua = "            ";
			List autos = new ArrayList();
			int reg=0;
			while (continua) {
				resultado = chamaTransBRK("048",opcEscolha,indContinua,myUsuario);
				AutoInfracaoBean myAt = new AutoInfracaoBean();							
				if (verErro(myAt,resultado,"Erro em Consulta Autos (Transa��o 048).")==false) { 
					myConsult.setMsgErro(myAt.getMsgErro());
					return false;					
				}
				String linha = resultado.substring(3);
				int i = 0;
				for (i=0; i < 6; i++) {
					linha = resultado.substring((i*466)+3,(i+1)*466+3);   /*linha = resultado.substring((i*464)+3,(i+1)*464+3);*/
					//System.out.println("Registro : " + i+"   "+linha.length());
					AutoInfracaoBean myAuto = new AutoInfracaoBean();
					myAuto.setInfracao(new InfracaoBean());	
					myAuto.setProprietario(new ResponsavelBean());
					myAuto.setCondutor(new ResponsavelBean());
					setPosIni(0);											
					myAuto.setNumAutoInfracao(getPedaco(linha,12," "," ")); /*********OK*/
					if (myAuto.getNumAutoInfracao().trim().length()>0) { 
						myAuto.setNumPlaca(getPedaco(linha,7," "," ")); /*********OK*/
						
						/*
						VerificaAutoOracle(myAuto,conn); N�O GRAVAR NA TSMI_AUTO_INFRACAO
						*/
												
						myAuto.getInfracao().setCodAutoInfracao(myAuto.getCodAutoInfracao());					
						myAuto.getInfracao().setCodInfracao(getPedaco(linha,5," "," ")); /*********OK*/
						
												
						myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8))); /*********OK*/
						myAuto.getProprietario().setNomResponsavel(getPedaco(linha,60," "," ")); /*********OK*/
						myAuto.setCodStatus(getPedacoNum(linha,3,0,"."));  /*********OK*/
						if ("88".equals(myAuto.getCodStatus())) myAuto.setCodStatus("0");
						sCmd  = "SELECT nom_status from TSMI_STATUS_AUTO WHERE cod_status = '"+myAuto.getCodStatus()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						myAuto.setNomStatus("Status: "+myAuto.getCodStatus());
						while (rs.next()) {
							myAuto.setNomStatus(rs.getString("nom_status"));	  				  	  				  
						}
						myAuto.setNumProcesso(getPedaco(linha,20," "," ")); /*********OK*/ 
						
						
						
						myAuto.setNumRenavam(getPedaco(linha,11," "," ")); /*********OK*/    /*myAuto.setNumRenavam(getPedaco(linha,9," "," "));*/
						
						
						
						
						
						//proprietario.setIndCpfCnpj(getPedaco(linha,1," "," "));
						myAuto.getProprietario().setIndCpfCnpj("1");
						myAuto.getProprietario().setNumCpfCnpj(getPedacoNum(linha,11,0,"."));/*********OK*/
						if ("1".equals(myAuto.getProprietario().getIndCpfCnpj()))
							myAuto.getProprietario().setNumCpfCnpj(sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",11));	
						else myAuto.getProprietario().setNumCpfCnpj(sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",14));
						
						myAuto.getInfracao().setValRealDesconto(getPedacoNum(linha,9,2,".")); /*********OK*/
						myAuto.getInfracao().setValReal(getPedacoNum(linha,9,2,".")); /*********OK*/
						myAuto.setValHorInfracao(getPedaco(linha,4," "," "));	/*********OK*/			  
						myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," ")); /*********OK*/
						myAuto.setDatVencimento(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8))); /*********OK*/
						myAuto.getInfracao().setNumPonto(getPedacoNum(linha,1,0,"."));	  /*********OK*/
						myAuto.getInfracao().setDscInfracao(getPedaco(linha,80," "," ")); /*********OK*/
						myAuto.setDscLocalInfracao(getPedaco(linha,80," "," "));	/*********OK*/
						myAuto.setCodMunicipio(getPedacoNum(linha,5,0,".")); /*********OK*/
						
						//			Ler Municipio do Auto
						sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"+myAuto.getCodMunicipio()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myAuto.setNomMunicipio(rs.getString("nom_municipio"));	  				  	  				  
						}
						myAuto.setCodOrgao(getPedacoNum(linha,6,0,".")); /*********OK*/
						//			Ler Orgao do Auto
						sCmd  = "SELECT sig_Orgao,nom_Orgao from TSMI_ORGAO WHERE cod_orgao = '"+myAuto.getCodOrgao()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myAuto.setSigOrgao(rs.getString("sig_Orgao"));	  				  	  				  
							myAuto.setNomOrgao(rs.getString("nom_Orgao"));	  				  	  				  
						}
						myAuto.setIndFase(getPedaco(linha,1," "," ")); /*********OK*/
						
						myAuto.setIndSituacao(getPedaco(linha,1," "," "));	/*********OK*/
						
						myAuto.setIndPago(getPedaco(linha,1," "," ")); /*********OK*/
						myAuto.setDatNotifAut(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8))); /*********OK*/				
						myAuto.setCodNotifAut(getPedaco(linha,2," ","."));	/*********OK*/			
						myAuto.setDatNotifPen(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8))); /*********OK*/				
						myAuto.setCodNotifPen(getPedaco(linha,2," ",".")); /*********OK*/
						sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE num_codigo_retorno = '"+myAuto.getCodNotifAut()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myAuto.setNomMotivoNotifAut(rs.getString("DSC_CODIGO_RETORNO"));
						}
						sCmd  = "SELECT ind_entregue,DSC_CODIGO_RETORNO from TSMI_CODIGO_RETORNO WHERE num_codigo_retorno = '"+myAuto.getCodNotifPen()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {	
							myAuto.setNomMotivoNotifPen(rs.getString("DSC_CODIGO_RETORNO"));
						}
						if (sys.Util.DifereDias("16/07/2004",myAuto.getDatInfracao())>=0) { 
							myAuto.setDatNotificacao(myAuto.getDatNotifPen());
							myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifPen());
						}
						else {		
							if (myAuto.getCodStatus().length()>1){ 
								myAuto.setDatNotificacao(myAuto.getDatNotifPen());
								myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifPen());				
							}
							else { 
								myAuto.setDatNotificacao(myAuto.getDatNotifAut()) ;
								myAuto.setNomMotivoNotificacao(myAuto.getNomMotivoNotifAut());				
							}
						}
						myAuto.getCondutor().setNomResponsavel(getPedaco(linha,60," "," "));
						myAuto.setAutoAntigo(getPedaco(linha,1," "," ")); /*********OK*/
						/*Renainf*/
						myAuto.setIdentAutoOrigem(getPedaco(linha,1," "," "));	/*********OK*/			
						
						myAuto.setNumInfracaoRenainf(getPedaco(linha,12," "," ")); /*********OK*/
						 /*Outro Orgao - Niteroi*/
						myAuto.setIdentOrgao(getPedaco(linha,9," "," ")); /*********OK*/
						myAuto.setIndParcelamento(getPedaco(linha,1," "," ")); /*********OK*/
						sCmd  = "SELECT NUM_AUTO_ORIGEM, COD_INFRACAO_ORIGEM from TSMI_AUTO_INFRACAO WHERE COD_AUTO_INFRACAO = '"+myAuto.getCodAutoInfracao()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {	
							myAuto.setNumAutoOrigem(rs.getString("NUM_AUTO_ORIGEM"));
							myAuto.setNumInfracaoOrigem(rs.getString("COD_INFRACAO_ORIGEM"));
						}
						
						
						// Verificar se e chamda de Recebe Notificacao - entao so processa 2 ou 12
						if ("REC0680".equals(myConsult.getSigFuncao())) {
							if ( (myAuto.getCodStatus().equals("2")) || (myAuto.getCodStatus().equals("12")) ) autos.add(myAuto) ;
						}
						else autos.add(myAuto) ;
					}		
				}

				setPosIni(3+6*466);  /*setPosIni(3+6*464);*/
				indContinua = getPedaco(resultado,12," "," ");				
				if (indContinua.trim().length()<=0) continua=false;
				reg++;
			}
			myConsult.setAutos(autos);	
			if (rs != null)
				rs.close();
			
			stmt.close();
			conn.close();
		}
		catch (Exception ex) {
			bOk = false ;
			throw new DaoException("daoBroker: "+ex.getMessage());	  
		} 
		finally {
			if (conn != null) {
				try {
					if (stmt!=null) stmt.close();
					serviceloc.setReleaseConnection(conn);
				} catch (Exception ey) { }
			}
		}
		return bOk;
	}  	      
	
	
	/******************************************
	 ConsultaAuto e carrega no Bean - Transa��es 050 - 16/07/2009
	******************************************/
	 public  boolean StatusAuto(consultaAutoBean myConsult,UsuarioBean myUsuario) throws DaoException{
		 boolean bOk = true ;
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 boolean continua   = true;
			 String resultado   = "";
			 String parteVar    = "";
			 String indContinua = sys.Util.rPad(" "," ",12);   
			 while (continua) 
			 {
				 parteVar = sys.Util.lPad(myConsult.getOrgaosEscolhidos(),"0",6)+ 
				 sys.Util.lPad(myConsult.getStatusEscolhidos(),"0",3)+ sys.Util.formataDataYYYYMMDD(myConsult.getDatReferencia());
				 resultado = chamaTransBRK("050",parteVar,indContinua);
				 AutoInfracaoBean myAt = new AutoInfracaoBean();							
				 if (verErro(myAt,resultado,"Erro Consulta Status por Auto de Infra��o (Transa��o 050).")==false) { 
					 myConsult.setMsgErro(myAt.getMsgErro());
					 return false;					
				 }
				 String linha = "";
				 for (int i=0; i < 140; i++) 
				 {
					 linha = resultado.substring((i*23)+3,(i+1)*23+3);
					 REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					 setPosIni(0);											
					 myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));
					 myAuto.setCodStatus(getPedaco(linha,3," "," "));
					 myAuto.setDatStatus(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					 System.out.println("Auto de Infra��o==>"+myAuto.getNumAutoInfracao());
					 
				 }
				 setPosIni(3+(23*140));
				 indContinua = getPedaco(resultado,12," "," ") ;		
				 if (indContinua.trim().length()<=0) continua=false;
			 }			    
			 stmt.close();
		 }
		 catch (Exception ex) {
			 bOk = false ;
			 throw new DaoException("consultaAutoSemArDaoBroker: "+ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return bOk;
	 }
	
	

	
	/**
	 */
	/******************************************
	 LeResponsavel - Transa��es 051 - 11/11/2005
	 ******************************************/
	public  boolean LeResponsavel (ResponsavelBean myResp,String tpPesq) throws DaoException {
		boolean bOk = true ;
		Connection conn = null ;	
		try {
			String resultado = "";
			if (tpPesq.equals("cnh")) {
				if ((myResp.getIndTipoCnh() == null) || (myResp.getNumCnh() == null)|| (myResp.getCodUfCnh() == null)) {
					bOk = false;
				}
				else {
					conn = serviceloc.getConnection(MYABREVSIST) ;	
					Statement stmt  = conn.createStatement();
					String sCmd     = "";
					ResultSet rs	= null ;
					String numCnh   = sys.Util.lPad(myResp.getNumCnh(),"0",11);
					String codUFCnh = sys.Util.rPad(myResp.getCodUfCnh()," ",2);
					resultado       = chamaTransBRK("051",myResp.getIndTipoCnh()+numCnh+codUFCnh,"");
					AutoInfracaoBean myAt = new AutoInfracaoBean();							
					if (verErro(myAt,resultado,"Erro ao Buscar Condutor (Transa��o 051).")==false) { 
						myResp.setMsgErro(myAt.getMsgErro());
						bOk=false;					
					}
					else { 
						setPosIni(3);
						myResp.setNomResponsavel(getPedaco(resultado,60," "," "));
						myResp.setNumCpfCnpj(sys.Util.lPad(getPedacoNum(resultado,11,0,"."),"0",11));
						myResp.setCodResponsavel(myResp.getNumCpfCnpj());
						myResp.setIndTipoCnh(getPedaco(resultado,1," "," "));
						myResp.setNumCnh(getPedaco(resultado,11," "," "));
						myResp.setCodUfCnh(getPedaco(resultado,2," "," "));
						myResp.setEndereco(new sys.EnderecoBean());					
						myResp.getEndereco().setTxtEndereco(getPedaco(resultado,27," "," "));
						myResp.getEndereco().setNumEndereco(getPedaco(resultado,5," "," "));
						myResp.getEndereco().setTxtComplemento(getPedaco(resultado,11," "," "));
						myResp.getEndereco().setCodCidade(getPedacoNum(resultado,5,0,"."));
						
						sCmd  = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = '"
							+ myResp.getEndereco().getCodCidade()+"'" ;
						rs    = stmt.executeQuery(sCmd) ;
						while (rs.next()) {
							myResp.getEndereco().setNomCidade(rs.getString("nom_municipio"));	  				  	  				  
						}					
						myResp.getEndereco().setNumCEP(getPedaco(resultado,8," "," "));
						myResp.getEndereco().setNomBairro(getPedaco(resultado,20," "," "));
						myResp.getEndereco().setCodUF(getPedaco(resultado,2," "," "));
						myResp.setDatValidade(sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8)));
						myResp.setCategoriaCnh(getPedaco(resultado,4," "," "));
					}
					if (rs!=null) rs.close();
					stmt.close();
				}  // NAO NULOS
			}  // IF CNH
		}  // TRY
		catch (Exception ex) {
			bOk = false ;
			myResp.setCodResponsavel("0") ;
			throw new DaoException(ex.getMessage());
		}
		finally {
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}	
		return bOk;
	}	
	

	/******************************************
	 Consulta Autos a Parcelar - Transa��es 077 SMITWEB - 11/11/2005
	 ******************************************/
	
	/**-----------------------------------------------------------
	 * Transa��o 078 - Parcelamento de Multas SMITWEB -11/11/2005
	 ------------------------------------------------------------- */
	
	
	/******************************************
	 Transa��es 204, 324
	 ******************************************/
	/**
	 *-----------------------------------------------------------
	 * Receb Notificacao - Ajusta Status - Transacao 204 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto204(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
		try {	
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01()) +
			sys.Util.rPad(myHis.getTxtComplemento02(),"0",2) ;									
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myAuto,resultado,"Erro na grava��o do Recebimento da Notifica��o.")==false) return ;
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return ;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Abrir Defesa Previa - 206,326,334 - 11/11/2005
	 * Indeferimento de Plano - 236,356,364 - 11/11/2005
	 * Renuncia de Defesa Previa - 238 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto206(AutoInfracaoBean myAuto,ParamOrgBean myParam,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException 
	{
		Connection conn =null ;
		String resultado ="";
		Statement stmt = null;
		String parteVar = "";
		boolean continua = true;
		int contador = 0;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);	   			
			while(continua && contador < 500)
			{
				buscarReq("206",myAuto,myReq,myHis,myParam,conn);
				
				
				
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
				sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
				sys.Util.rPad(myHis.getNomUserName()," ",20) +	
				sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +							
				sys.Util.rPad(myHis.getTxtComplemento02()," ",02) +
				sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento03()) +							
				sys.Util.rPad(myHis.getTxtComplemento04()," ",60) +
				sys.Util.lPad(myHis.getTxtComplemento05(),"0",1) ;
				if ("1".equals(myHis.getTxtComplemento05())) parteVar+=sys.Util.lPad(myHis.getTxtComplemento06(),"0",14);
				else parteVar+=sys.Util.lPad(myHis.getTxtComplemento06(),"0",14);
				parteVar+=sys.Util.rPad(myHis.getTxtComplemento07()," ", 50)+myHis.getTxtComplemento08()+
				sys.Util.rPad(myUsuario.getLocalTrabalho()," ",10);
				
				resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
				stmt = conn.createStatement();												
				if (verErro(myAuto,resultado,"Erro na grava��o da Defesa/Recurso.",stmt)==false) {
					conn.rollback();
					if(stmt!=null) stmt.close();  
					
					if(resultado.substring(0,3).equals("039")||resultado.substring(0,3).equals("718")) 
						IncrementaProcesso(myAuto,myParam,myReq,myHis,resultado.substring(0,3));    				
					else 
						continua = false;
				}
				else if (resultado.substring(0,3).equals("000")) 
				{
					
					LeAutoInfracao(myAuto,"",myUsuario);
					GravaAutoInfracaoLocal(myAuto, conn);
					/*AtualizaCodigoBarraProcesso(myAuto);*/					
					if (myAuto.getCodAutoInfracao().length()>0) 
					{
						/*Gravar Requerimento*/
						GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						GravarHistorico(myHis,conn);
					}
					conn.commit();
					continua = false;
				}
				if(stmt!=null) stmt.close();
				contador++;
			}
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}

	
	public void processaPenalidadeDER(AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException 
	{
		Connection conn =null ;
		String resultado ="";
		Statement stmt = null;
		String parteVar = "";
		boolean continua = true;
		int contador = 0;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST) ;
			parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
					sys.Util.rPad(myAuto.getNumPlaca()," ",7);
			resultado = chamaTransBRK("417",parteVar,"",myUsuario);
			stmt = conn.createStatement();
			if (verErro(myAuto,resultado,"Erro na grava��o da Autua��o/Penalidade (DER).",stmt)==false) {
				if(stmt!=null) stmt.close();
				return ;
			}
			else
				myAuto.setMsgErro("Opera��o realizada com sucesso.");
			if(stmt!=null) stmt.close();			
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;

	}

	
	
	
	public void BuscarDataCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			Statement stmt = conn.createStatement();
			ResultSet rs   = null;
			String sCmd = "SELECT * FROM TSMI_CONTROLE_PROCESSO " +
			" WHERE NUM_PROCESSO = '" + myAuto.getNumProcesso() + "'";
			rs    = stmt.executeQuery(sCmd) ;
			if (rs.next())
			  myAuto.setDtGerCBProcesso(converteData(rs.getDate("DAT_GERACAO")));
			stmt.close();
			rs.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}

	public void BuscarDataCodigoBarraNotificacao(REC.AutoInfracaoBean myAuto) throws DaoException {
		Connection conn = null;
		try {
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			ResultSet rs   = null;
			/*Pegar Data e Sequencia C�digo de Barra Notificaca��o Autua��o*/
			String sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
				    "NUM_NOTIFICACAO='"+myAuto.getNumNotifAut()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myAuto.setDtGerCBNotificacaoAut(converteData(rs.getDate("DAT_GERACAO_CB_AI")));
				myAuto.setSeqGeracaoCBNotificacaoAut(rs.getInt("SEQ_GERACAO_CB_AI"));
			}
			/*Pegar Data e Sequencia C�digo de Barra Notificaca��o Penalidade*/
			sCmd  = "SELECT * FROM TSMI_CONTROLE_NOTIFICACOES WHERE "+
				    "NUM_NOTIFICACAO='"+myAuto.getNumNotifPen()+"'";
			rs    = stmt.executeQuery(sCmd) ;
			while (rs.next()) {
				myAuto.setDtGerCBNotificacaoPen(converteData(rs.getDate("DAT_GERACAO_CB_AI")));
				myAuto.setSeqGeracaoCBNotificacaoPen(rs.getInt("SEQ_GERACAO_CB_AI"));
			}
			stmt.close();
			rs.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	public void AtualizaCodigoBarraProcesso(REC.AutoInfracaoBean myAuto) throws DaoException {
		Connection conn = null;
		try {
			SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			conn = serviceloc.getConnection(MYABREVSIST);                        
			Statement stmt = conn.createStatement();
			String sql = "";
			
			sql = "UPDATE TSMI_CONTROLE_PROCESSO SET " +
			" SIT_UTILIZADO='S' " +
			" WHERE NUM_PROCESSO = " + myAuto.getNumProcesso();
			stmt.executeUpdate(sql);
			conn.commit();
			stmt.close();
			
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		} finally {
			if (conn != null) {
				try {
					serviceloc.setReleaseConnection(conn);
				} catch (sys.ServiceLocatorException e) {
					throw new DaoException(e.getMessage());
				}
			}
		}		
	}
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Requerimento com Parecer Juridico e grava Historico - Transa��es 207 - 11/11/2005 
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto207(AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
		try {	
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +							
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			sys.Util.rPad(myHis.getTxtComplemento03()," ",01) +
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000) +
			sys.Util.lPad(myHis.getTxtComplemento05(),"0",11) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);															
			if (verErro(myAuto,resultado,"Erro na grava��o de Parecer Juridico.")==false) { 
				
			}
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return ;
	}
	
	
	/**
	 *-----------------------------------------------------------
	 * Atualiza Junta e Relator e grava Historico - Transa��es 208, 328, 335 - 11/11/2005 
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto208(AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
		Connection conn =null ;						
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);	   			
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +							
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			sys.Util.lPad(myHis.getTxtComplemento03(),"0",6) +
			sys.Util.lPad(myHis.getTxtComplemento04(),"0",11) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myAuto,resultado,"Erro na grava��o de Definir/Troca de Relator.")==false) { 
			}
			else
			{
				GravaAutoInfracaoLocal(myAuto, conn);
				if (myAuto.getCodAutoInfracao().length()>0)
				{
					/*Gravar Requerimento*/
					GravarRequerimento(myReq,conn);
					/*Gravar Historico*/
					GravarHistorico(myHis,conn);
				}
				conn.commit();			  
			}
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	/**
	 *---------------------------------------
	 * Atualiza Resultado e grava Historico - 209, 329, 336 - 11/11/2005
	 *---------------------------------------
	 */					
	public void atualizarAuto209(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		Connection conn =null ;								
		try {
			conn = serviceloc.getConnection(MYABREVSIST) ;
			conn.setAutoCommit(false);
			
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +							
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			sys.Util.rPad(myHis.getTxtComplemento03()," ",1) +
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myAuto,resultado,"Erro na grava��o do Resultado.")==false) {
			}
			else
			{
				GravaAutoInfracaoLocal(myAuto, conn);
				if (myAuto.getCodAutoInfracao().length()>0)
				{
					/*Gravar Requerimento*/
					GravarRequerimento(myReq,conn);
					/*Gravar Historico*/
					GravarHistorico(myHis,conn);
				}
				conn.commit();			  
			}
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	/**
	 *---------------------------------------
	 * Atualiza data de Envio para DO e Numero da CI e grava Historico
	 *     Transa��es 211, 331, 338 - NIMGUEM CHAMA
	 *  *---------------------------------------
	 */
	public void atualizarAuto211(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		try {
			String parteVar = sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01()) +	
			sys.Util.rPad(myHis.getTxtComplemento02()," ",20) +	
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +				  
			sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +			
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento03()," ",1) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myAuto,resultado,"Erro na grava��o da data de Envio para DO.")==false) { 
				
			}
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return ;
	}
	
	/**
	 *---------------------------------------
	 * Atualiza Data de envio para DO e grava Historico
	 * Transa��es 213, 333, 340 - NIMGUEM CHAMA
	 *---------------------------------------
	 */
	public void atualizarAuto213(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		try {
			String parteVar = sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01())+	
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +				  
			sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +			
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) ;		
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);			  
			if (verErro(myAuto,resultado,"Erro na grava��o da data de Publicacao no DO.")==false) { 
				
			}
		}
		catch (Exception ex) {
			throw new DaoException(ex.getMessage());
		}
		return ;
	}
	
	
	
	/**
	 *-----------------------------------------------------------
	 * executar Troca de Real Infrator - 214 - 11/11/2005
	 *-----------------------------------------------------------
	 */
	public void atualizarAuto214(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,ParamOrgBean myParam,UsuarioBean myUsuario) throws DaoException 
	{
		Connection conn =null ;
		String resultado ="";
		Statement stmt = null;
		String parteVar = "";
		boolean continua = true;
		int contador = 0;
		try 
		{
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);	   			
			while(continua && contador < 500)
			{
				buscarReq("214",myAuto,myReq,myHis,myParam,conn) ;
				parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
				sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
				sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
				sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
				sys.Util.rPad(myHis.getNomUserName()," ",20) +	
				sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
				sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01()) +
				sys.Util.lPad(myHis.getTxtComplemento02(),"0",11) +
				sys.Util.rPad(myHis.getTxtComplemento03()," ",60) +
				sys.Util.rPad(myHis.getTxtComplemento04()," ",43) +
				sys.Util.rPad(myHis.getTxtComplemento05()," ",20) +
				sys.Util.lPad(myHis.getTxtComplemento06(),"0",05) +
				sys.Util.rPad(myHis.getTxtComplemento07()," ",02) +
				sys.Util.lPad(myHis.getTxtComplemento08(),"0",8) +
				sys.Util.lPad(myHis.getTxtComplemento09(),"0",12) +
				sys.Util.rPad(myHis.getTxtComplemento10()," ",02) +
				"0" + // Indicador Judicial
				sys.Util.rPad(myHis.getTxtComplemento11()," ",30);
				parteVar+=sys.Util.rPad(myHis.getTxtComplemento12()," ", 50)+
				sys.Util.rPad(myUsuario.getLocalTrabalho()," ",10);
				
				resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
				stmt = conn.createStatement();												
				if (verErro(myAuto,resultado,"Erro na grava��o da TRI.",stmt)==false) { 
					conn.rollback();
					if(stmt!=null) stmt.close();  
					
					if(resultado.substring(0,3).equals("039")||resultado.substring(0,3).equals("718")) 
						IncrementaProcesso(myAuto,myParam,myReq,myHis,resultado.substring(0,3)) ;    				
					else 
						continua = false;
				}
				else 
				{
					LeAutoInfracao(myAuto,"",myUsuario);
					GravaAutoInfracaoLocal(myAuto, conn);
					/*AtualizaCodigoBarraProcesso(myAuto);*/					
					if (myAuto.getCodAutoInfracao().length()>0)
					{
						/*Gravar Requerimento*/
						GravarRequerimento(myReq,conn);
						/*Gravar Historico*/
						GravarHistorico(myHis,conn);
					}
					conn.commit();
					continua = false;
				}
				if(stmt!=null) stmt.close();
				contador++;
			}	
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	/**
	 *---------------------------------------
	 * Estorno DP, 1A e 2A - 226 - 350 - 351 
	 * Estorno de Resultado DP, 1A e 2A - 210 - 330 - 337 
	 * Estorno de Troca de Real Infrator - 352 
	 * Estorno de Ren�ncia de DP - 239
	 * Estorno de Indeferimento de plano - 237 
	 *---------------------------------------
	 */
	public void atualizarAuto226(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		Connection conn = null;   					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);	   			
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +							
			sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			sys.Util.rPad(myHis.getTxtComplemento03()," ",50) +
			sys.Util.rPad(myHis.getTxtComplemento04()," ",1000) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);			  												
			if (verErro(myAuto,resultado,"Erro na grava��o do Estorno.")==false) {
			}
			else
			{
				GravaAutoInfracaoLocal(myAuto, conn);
				atualizarStatusReq(myReq,conn);
				if (myAuto.getCodAutoInfracao().length()>0)
				{
					/*Gravar Requerimento*/
					GravarRequerimento(myReq,conn);
					/*Gravar Historico*/
					GravarHistorico(myHis,conn);
				}
				conn.commit();			  
			}
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	/**
	 *---------------------------------------
	 * Revis�o de Requerimento DP, 1A e 2A - 264,384,391 - 15/05/2006
	 *---------------------------------------
	 */

	public void atualizarAuto264(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {
		Connection conn = null;   					
		try {
			conn = serviceloc.getConnection(MYABREVSIST);
			conn.setAutoCommit(false);	   			
			String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			sys.Util.lPad(myAuto.getCodStatus(),"0",3) +			
			sys.Util.lPad(myHis.getCodEvento(),"0",3) +
			sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento01()," ",30) +									
			sys.Util.rPad(myHis.getTxtComplemento02()," ",2) +
			sys.Util.rPad(myHis.getTxtComplemento03()," ",1000) ;
			String resultado = chamaTransBRK("264",parteVar,"",myUsuario);			  												
			if (verErro(myAuto,resultado,"Erro na grava��o do Estorno.")==false) {
			}
			else
			{
				GravaAutoInfracaoLocal(myAuto, conn);
				atualizarStatusReq(myReq,conn);
				if (myAuto.getCodAutoInfracao().length()>0)
				{
					/*Gravar Requerimento*/
					GravarRequerimento(myReq,conn);
					/*Gravar Historico*/
					GravarHistorico(myHis,conn);
				}
				conn.commit();			  
			}
		}
		catch (Exception ex) {
			try {  conn.rollback(); }
			catch (SQLException ey) { }	  
			throw new DaoException(ex.getMessage());
		}
		finally 
		{
			if (conn != null) {
				try { serviceloc.setReleaseConnection(conn); }
				catch (Exception ey) { }
			}
		}
		return ;
	}
	
	
	
	public void atualizarStatusReq(RequerimentoBean myReq,Connection conn) throws sys.DaoException {			
		ResultSet rs = null;
		Statement stmt = null;	
		try{
			String codStatus="";    
			stmt = conn.createStatement();
			String sCmd     =  "SELECT * FROM TSMI_REQUERIMENTO WHERE "+
			"COD_AUTO_INFRACAO='"+myReq.getCodAutoInfracao()+"' and "+              
			"NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
			rs = stmt.executeQuery(sCmd);
			if (rs.next())
				codStatus = rs.getString("COD_STATUS_REQUERIMENTO");
			
			if(codStatus.equals("2"))
				myReq.setCodStatusRequerimento("0"); 
			
			if(codStatus.equals("3"))
				myReq.setCodStatusRequerimento("2");
			
			stmt.close();								  	  
			rs.close();
		}
		catch (Exception ex) {
			throw new sys.DaoException("daoBroker: "+ex.getMessage());	  
		} 
		finally {
			if (conn != null) {
				try {
					if (stmt!=null) stmt.close();
				} catch (Exception ey) { }
			}
		}
	}
	
	
	/**-----------------------------------------------------------
	 * Transa��o 250 - Atualiza data de vencimento do Auto - 11/11/2005
	 ------------------------------------------------------------- */	 
	public void atualizaDataVenc(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,String novadata)throws DaoException {
		try {		 
			String parteVar = 
				sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
				sys.Util.rPad(myAuto.getNumPlaca()," ",7)+
				sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
				sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
				sys.Util.lPad(myAuto.getCodStatus(),"0",3)+
				sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus())+
				"250"+"100"+	 							
				sys.Util.rPad(myUsrLog.getNomUserName()," ",20)+						
				sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6) +
				sys.Util.formataDataYYYYMMDD(novadata);
			
			//Chama a Transacao			
			String resultado = chamaTransBRK("250",parteVar,"",myUsrLog);
			if (verErro(myAuto,resultado,"Erro na altera��o da data de vencimento.")==false) return ;		  
		}    
		catch (Exception ex) {
			throw new DaoException("ERR-DaoBroker.Transacao250: "+ex.getMessage());
		}
	}
	
	
	/**
	 *---------------------------------------
	 * Atualiza data de Envio para Publica��o por Requerimento
	 *     Transa��es 261, 381, 388 - 11/11/2005
	 *  *---------------------------------------
	 */
	public void atualizarAuto261(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {			
		Connection conn = null;
        try {
            conn = serviceloc.getConnection(MYABREVSIST);
            conn.setAutoCommit(false);  
			String parteVar = sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01()) +	
			sys.Util.rPad(myHis.getTxtComplemento02()," ",20) +	
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +				  
			sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +			
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +	
			sys.Util.rPad(myHis.getTxtComplemento03()," ",1) + 
			sys.Util.rPad(myHis.getTxtComplemento04()," ",30) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if(verErro(myAuto,resultado,"Erro no Envio de Requerimento para Publica��o.")){
                RequerimentoBean myReq = new RequerimentoBean();
                myReq.setCodAutoInfracao(myHis.getCodAutoInfracao());
                myReq.setNumRequerimento(myHis.getTxtComplemento04());

                if (myReq.getCodAutoInfracao().length()>0)
                {
                    /*Gravar Requerimento*/
                    LeRequerimentoLocal(myReq,conn);
                    myReq.setCodEnvioDO(myHis.getTxtComplemento02());
                    myReq.setDatEnvioDO(myHis.getTxtComplemento01());
                    GravarRequerimento(myReq,conn);
                }
                conn.commit();            
            }
        }
        catch (Exception ex) {
            try {  conn.rollback(); }
            catch (SQLException ey) { }   
            throw new DaoException(ex.getMessage());
        }
        finally 
        {
            if (conn != null) {
                try { serviceloc.setReleaseConnection(conn); }
                catch (Exception ey) { }
            }
        }
        return ;
    }
	
	/**
	 *---------------------------------------
	 * Atualiza data de Envio para Publica��o por Requerimento
	 *     Transa��es 263, 383, 390 - 11/11/2005
	 *  *---------------------------------------
	 */
	public void atualizarAuto263(HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {					
        Connection conn = null;
        try {
            conn = serviceloc.getConnection(MYABREVSIST);
            conn.setAutoCommit(false);  
			String parteVar = sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento01()) +		
			sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +				  
			sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			sys.Util.rPad(myAuto.getNumPlaca()," ",7) +			
			sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +	 
			sys.Util.rPad(myHis.getTxtComplemento02()," ",30) ;
			String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);
			if (verErro(myAuto,resultado,"Erro na atualiza��o da data de Publica��o do Requerimento.")){
                RequerimentoBean myReq = new RequerimentoBean();
                myReq.setCodAutoInfracao(myHis.getCodAutoInfracao());
                myReq.setNumRequerimento(myHis.getTxtComplemento02());

                if (myReq.getCodAutoInfracao().length()>0)
                {
                    /*Gravar Requerimento*/
                    LeRequerimentoLocal(myReq,conn);
                    myReq.setDatPublPDO(myHis.getTxtComplemento01());
                    GravarRequerimento(myReq,conn);
                }
                conn.commit();
            }
                
		}
        catch (Exception ex) {
            try {  conn.rollback(); }
            catch (SQLException ey) { }   
            throw new DaoException(ex.getMessage());
        }
        finally 
        {
            if (conn != null) {
                try { serviceloc.setReleaseConnection(conn); }
                catch (Exception ey) { }
            }
        }
        return ;
    }
	
	/**
	 *---------------------------------------
	 * Realiza o estorno dos requerimentos com status 4 (Enviados para publica��o).
	 * @param myReq. Objeto referente ao requerimento, possui todas as informa��es do mesmo
	 * @param myHis. Objeto hist�rico do processamento realizado. Somente � utilizado para informar o usu�rio e o codEvento a ser realizado.
	 * @param myAuto. Objeto referente ao auto de infra��o, possui todas as informa��es do mesmo.
	 * @author Sergio Roberto Junior
	 * @since 09/12/2004
	 * @throws DaoException
	 * @version 1.0 
	 * Estorno de envio p/ publica��o - 382, 389 - 11/11/2005
	 *---------------------------------------
	 */public void atualizarAuto382(RequerimentoBean myReq,HistoricoBean myHis,AutoInfracaoBean myAuto,UsuarioBean myUsuario) throws DaoException {			
		 
		 try {
			 String parteVar = sys.Util.formataDataYYYYMMDD(sys.Util.formatedToday().substring(0,10)) +
			 sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			 sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			 sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +																																  
			 sys.Util.rPad(myReq.getNumRequerimento()," ",30);		
			 String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);			  												
			 verErro(myAuto,resultado,"ERRO EM  "+myHis.getTxtComplemento01());
		 }
		 catch (Exception ex) { 
			 throw new DaoException(ex.getMessage());
		 }		
		 return ;
	 }
	 
	 
	 /**
	  *-----------------------------------------------------------
	  * Movimento para Banco - Transa��es 407 - 11/11/2005
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto407(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {					
         Connection conn = null;
         try {	
             
             conn = serviceloc.getConnection(MYABREVSIST);
			 String parteVar =
				 sys.Util.rPad(myHis.getNomUserName()," ",20) +
				 sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +
				 "8"+
				 sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
				 sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
				 sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
				 sys.Util.lPad("0","0",7) +
				 sys.Util.lPad("0","0",3) +
				 sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
				 sys.Util.rPad(myHis.getNomUserName()," ",8) +
				 sys.Util.rPad(" "," ",1) +
				 sys.Util.lPad(myHis.getTxtComplemento01(),"0",1) +
				 sys.Util.rPad(" "," ",51) +
				 sys.Util.rPad("0","0",607);
			 String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);						
			 if (verErro(myAuto,resultado,"Erro na Movimenta��o Banc�ria.")==false) return ;
             else
               /*Gravar Historico*/
               GravarHistorico(myHis,conn);
		 }				
         catch (Exception ex) {
                try {  conn.rollback(); }
                catch (SQLException ey) { }   
                throw new DaoException(ex.getMessage());
            }
            finally 
            {
                if (conn != null) {
                    try { serviceloc.setReleaseConnection(conn); }
                    catch (Exception ey) { }
                }
            }
            return ;
     }
	 
	 /*-----------------------------------------------------------
	  * Atualiza Numero do Processo e grava Historico - 410 - 11/11/2005
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto410(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
		 try {
			 String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			 sys.Util.rPad(myAuto.getNumProcesso()," ",20) +
			 sys.Util.lPad(myAuto.getCodStatus(),"0",3) +
			 sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus()) +
			 sys.Util.lPad(myHis.getCodEvento(),"0",3) +																																			  
			 sys.Util.lPad(myHis.getCodOrigemEvento(),"0",3) +
			 sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			 sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			 sys.Util.rPad(myHis.getTxtComplemento01().trim()," ",20) +
			 sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			 sys.Util.rPad(myHis.getTxtComplemento03()," ",50) +
			 sys.Util.rPad(myHis.getTxtComplemento04()," ",1000) ;
			 String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);						
			 if (verErro(myAuto,resultado,"Erro na grava��o do Processo.")==false) return ;
			 return ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
	 }  
	 
	 /*-----------------------------------------------------------
	  * Atualiza Status e grava Historico - 411 - 11/11/2005
	  *-----------------------------------------------------------
	  */
	 public void atualizarAuto411(AutoInfracaoBean myAuto,HistoricoBean myHis,UsuarioBean myUsuario) throws DaoException {
		 try {
			 String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12) + 
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7) +
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6) +
			 sys.Util.rPad(myHis.getNomUserName()," ",20) +	
			 sys.Util.lPad(myHis.getCodOrgaoLotacao(),"0",6) +	
			 sys.Util.lPad(myHis.getTxtComplemento01().trim(),"0",3) +
			 sys.Util.formataDataYYYYMMDD(myHis.getTxtComplemento02()) +							
			 sys.Util.rPad(myHis.getTxtComplemento03()," ",50) +
			 sys.Util.rPad(myHis.getTxtComplemento04()," ",1000) ;
			 String resultado = chamaTransBRK(myHis.getCodEvento(),parteVar,"",myUsuario);					
			 if (verErro(myAuto,resultado,"Erro na grava��o do Ajuste de Status.")==false) return ;
			 return ;
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
	 } 
	 
	 /*---------------------------------------------
	  * Ajuste Automatico de CR da Venda - 412 - 11/11/2005
	  *-----------------------------------------------
	  */
	 public ArrayList ajustarCrVenda412(AutoInfracaoBean myAuto,ACSS.UsuarioBean myUsrLog)throws DaoException {
		 ArrayList resulTrans412 = new ArrayList();
		 try {
			 String parteVar = sys.Util.rPad(myUsrLog.getNomUserName()," ",20) +
			 sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6) +
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7);			
			 String resultado = chamaTransBRK("412",parteVar,"",myUsrLog);
			 if (verErro(myAuto,resultado,"Erro na grava��o do Ajuste de Automatico (CR).")==false) {
				 myAuto.setMsgErro(resultado) ;
			 }
			 else {	
				 resulTrans412.add(resultado.substring(3,7));
				 // Verifica o numero de autos a serem lidos
				 int tamanho = resultado.trim().length()/12;  
				 String na = "";			   	 
				 for (int i=0; i < tamanho; i++ ){
					 AutoInfracaoBean autoInfracaoBean = new AutoInfracaoBean();
					 na = resultado.substring((i*12)+7,(i*12)+19);
					 if (na.trim().length()>0) {
						 autoInfracaoBean.setNumAutoInfracao(resultado.substring((i*12)+7,(i*12)+19));
						 resulTrans412.add(autoInfracaoBean);
					 }
				 } 	
				 myAuto.setMsgErro("Ajuste Automatico (CR) para placa "+myAuto.getNumPlaca()+" realizado em "+resultado.substring(3,7)+" Autos.") ;		  	
				 myAuto.setMsgOk("S");
			 }
		 }    
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return resulTrans412;
	 }
	 /**-----------------------------------------------------------
	  * Transa��o 413 - Ajuste manual do Responsavel pelos Pontos - 11/11/2005
	  ------------------------------------------------------------- */	 
	 public void ajustarManual(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq,HistoricoBean myHist)throws DaoException {
		 Connection conn =null ;
		 try {		 
			 
			 String sCmd  = "SELECT cod_municipio from TSMI_MUNICIPIO WHERE nom_municipio = '"+ myAuto.getCondutor().getEndereco().getNomCidade()+"'" ;
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();			
			 ResultSet rs    = stmt.executeQuery(sCmd) ;
			 while (rs.next()) {
				 myAuto.getCondutor().getEndereco().setCodCidade(rs.getString("cod_municipio"));	  				  	  				  
			 }
			 String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7)+			 
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
			 sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
			 sys.Util.lPad(myAuto.getCodStatus(),"0",3)+
			 sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus())+
			 "413"+"100"+ sys.Util.rPad(myUsrLog.getNomUserName()," ",20)+
			 sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6) +
			 sys.Util.formataDataYYYYMMDD(myReq.getDatRequerimento())+
			 sys.Util.lPad(myAuto.getCondutor().getNumCpfCnpj(),"0",11)+
			 sys.Util.rPad(myAuto.getCondutor().getNomResponsavel()," ",60)+
			 sys.Util.rPad(myAuto.getCondutor().getEndereco().getTxtEndereco()," ",27)+
			 sys.Util.rPad(myAuto.getCondutor().getEndereco().getNumEndereco()," ",5)+
			 sys.Util.rPad(myAuto.getCondutor().getEndereco().getTxtComplemento()," ",11)+
			 sys.Util.rPad(myAuto.getCondutor().getEndereco().getNomBairro()," ",20)+
			 sys.Util.lPad(myAuto.getCondutor().getEndereco().getCodCidade(),"0",5)+
			 sys.Util.rPad(myAuto.getCondutor().getEndereco().getCodUF(), " ",2)+
			 sys.Util.lPad(myAuto.getCondutor().getEndereco().getNumCEP().trim(),"0",8)+
			 sys.Util.lPad(myAuto.getCondutor().getIndTipoCnh(),"0",1)+
			 sys.Util.lPad(myAuto.getCondutor().getNumCnh(),"0",11)+
			 sys.Util.rPad(myAuto.getCondutor().getCodUfCnh()," ",2)+
             myHist.getTxtComplemento11();		   	
			 //Chama a Transacao			
			 String resultado = chamaTransBRK("413",parteVar,"",myUsrLog);
			 if (verErro(myAuto,resultado,"Erro na grava��o do Ajuste Manual do Respos�vel pelos pontos.")==false) return ;		  
			 
		 }    
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException("ERR-DaoBroker.Transacao413: "+ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try {
					 conn.rollback(); 
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
	 }
	 
	 /**-----------------------------------------------------------
	  * Transa��o 414 - Ajuste CPF - 11/11/2005
	  ------------------------------------------------------------- */	 
	 public void ajustarCPF(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,String indResp)throws DaoException {
		 try {		 
			 String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7)+			 
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
			 sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
			 sys.Util.lPad(myAuto.getCodStatus(),"0",3)+
			 sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus())+
			 "414"+"100"+
			 sys.Util.rPad(myUsrLog.getNomUserName()," ",20)+
			 sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6) +
			 sys.Util.lPad(indResp," ",1)+
			 sys.Util.lPad(myAuto.getProprietario().getIndCpfCnpj()," ",1)+
			 sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",14);									   	
			 //Chama a Transacao			
			 String resultado = chamaTransBRK("414",parteVar,"",myUsrLog);
			 if (verErro(myAuto,resultado,"Erro na grava��o do Ajuste de CPF.")==false) return ;		  
		 }    
		 catch (Exception ex) {
			 throw new DaoException("ERR-DaoBroker.Transacao414: "+ex.getMessage());
		 }
	 }
	 
	 /**-----------------------------------------------------------
	  * Transa��o 415 - Ajuste manual do Responsavel pela Pec�nia - 11/11/2005
	  ------------------------------------------------------------- */	 
	 public void ajustarManualRespPecunia(AutoInfracaoBean myAuto, ACSS.UsuarioBean myUsrLog,RequerimentoBean myReq)throws DaoException {
		 Connection conn =null ; 
		 try {		 
			 
			 String sCmd  = "SELECT cod_municipio from TSMI_MUNICIPIO WHERE nom_municipio = '"+ myAuto.getProprietario().getEndereco().getNomCidade()+"'" ;
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();			
			 ResultSet rs    = stmt.executeQuery(sCmd) ;
			 while (rs.next()) {
				 myAuto.getProprietario().getEndereco().setCodCidade(rs.getString("cod_municipio"));	  				  	  				  
			 }
			 
			 String parteVar = sys.Util.rPad(myAuto.getNumAutoInfracao()," ",12)+
			 sys.Util.rPad(myAuto.getNumPlaca()," ",7)+			 
			 sys.Util.lPad(myAuto.getCodOrgao(),"0",6)+
			 sys.Util.rPad(myAuto.getNumProcesso()," ",20)+
			 sys.Util.lPad(myAuto.getCodStatus(),"0",3)+
			 sys.Util.formataDataYYYYMMDD(myAuto.getDatStatus())+
			 "415"+"100"+ sys.Util.rPad(myUsrLog.getNomUserName()," ",20)+
			 sys.Util.lPad(myUsrLog.getOrgao().getCodOrgao(),"0",6) +
			 sys.Util.formataDataYYYYMMDD(myReq.getDatRequerimento())+
			 sys.Util.lPad(myAuto.getProprietario().getIndCpfCnpj(),"0",1)+
			 sys.Util.lPad(myAuto.getProprietario().getNumCpfCnpj(),"0",14)+
			 sys.Util.rPad(myAuto.getProprietario().getNomResponsavel()," ",60)+
			 sys.Util.rPad(myAuto.getProprietario().getEndereco().getTxtEndereco()," ",27)+
			 sys.Util.rPad(myAuto.getProprietario().getEndereco().getNumEndereco()," ",5)+
			 sys.Util.rPad(myAuto.getProprietario().getEndereco().getTxtComplemento()," ",11)+
			 sys.Util.lPad(myAuto.getProprietario().getEndereco().getNumCEP().trim(),"0",8)+
			 sys.Util.lPad(myAuto.getProprietario().getEndereco().getCodCidade(),"0",5)+
			 sys.Util.rPad(myAuto.getProprietario().getIndTipoCnh(),"0",1)+
			 sys.Util.lPad(myAuto.getProprietario().getNumCnh(),"0",11)+
			 sys.Util.rPad(myAuto.getProprietario().getCodUfCnh()," ",2);		   			
			 
			 //Chama a Transacao			
			 String resultado = chamaTransBRK("415",parteVar,"",myUsrLog);
			 if (verErro(myAuto,resultado,"Erro na grava��o do Ajuste Manual do Respos�vel pela pec�nia.")==false) return ;		  
			 
		 }    
		 catch (Exception ex) {
			 try {  conn.rollback(); }
			 catch (SQLException ey) { }	  
			 throw new DaoException("ERR-DaoBroker.Transacao413: "+ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try {
					 conn.rollback(); 
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
	 }	 
	 
	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
	  * @param conn Conex�o com a base de dados 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	  */

	 
	 public String Transacao086(String codOrgao, String codJunta, String codAcao, 
			 String indContinua,	String nomJunta, String sigJunta, String tpJunta, String txtEnd, 
			 String numEnd, String txtCompl, String nomBairro, String numCep, 
			 String codMun, String numDddTel, String numTel, String numFax, Connection conn) throws DaoException {
		 
		 return Transacao086(codOrgao, codJunta, codAcao, indContinua, nomJunta,
				 sigJunta, tpJunta,txtEnd, numEnd, txtCompl, nomBairro, numCep,
				 codMun, numDddTel, numTel, numFax);
	 }
	 
	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	  */
	 
	 public String Transacao086(String codOrgao, String codJunta, String codAcao, 
			 String indContinua,	String nomJunta, String sigJunta, String tpJunta,String txtEnd, 
			 String numEnd, String txtCompl, String nomBairro, String numCep, 
			 String codMun, String numDddTel, String numTel, String numFax) throws DaoException {
		 String resultado = "";
		 boolean transOK = true;
		 BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		 //Cr�tica do C�digo do �rg�o		
		 if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo do �rg�o n�o preenchido";
		 }
		 if (codOrgao.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo do �rgao inv�lido";
		 }
		 //Cr�tica do C�digo da Junta 		
		 if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta n�o preenchido";
		 }
		 if (codJunta.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo da Junta inv�lido";
		 }
		 //Cr�tica da A��o 		
		 if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo do A��o n�o preenchido";
		 }
		 if ("12345".indexOf(codAcao) < 0){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: C�digo da A��o inv�lido";
		 }
		 //Cr�tica do Indicador de Continuidade 		
		 if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			 indContinua = "      ";
		 }
		 if (indContinua.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao086: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		 }
		 
		 if (transOK) {
			 String parteVar = codOrgao + codJunta + codAcao + indContinua 
			 + sys.Util.rPad(nomJunta," ",20) + sys.Util.rPad(sigJunta," ",10)
			 + sys.Util.rPad(tpJunta,"0",1) + sys.Util.rPad(txtEnd," ",27) 
			 + sys.Util.rPad(numEnd," ",5) + sys.Util.rPad(txtCompl," ",11)
			 + sys.Util.rPad(nomBairro," ",20) + sys.Util.lPad(codMun," ",5)
			 + sys.Util.lPad(numCep," ",8) + sys.Util.rPad(numDddTel," ",3)
			 + sys.Util.rPad(numTel," ",15) + sys.Util.rPad(numFax," ",15);  
			 
			 transBRK.setParteFixa("086");
			 transBRK.setParteVariavel(parteVar);
			 try {
				 resultado = transBRK.getResultado();
			 } catch (Exception e) {
				 throw new DaoException (e.getMessage());
			 }
		 }
		 
		 return resultado;
	 }
	 
	 
	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codJunta C�digo da Junta. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Junta/Relator a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	  */
	 
	 public String Transacao087(String codOrgao, String codJunta, String numCPF, 
			 String codAcao, String indContinua,	String nomRelator) throws DaoException {
		 String resultado = "";
		 boolean transOK = true;
		 BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		 
		 //Cr�tica do C�digo do �rg�o		
		 if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo do �rg�o n�o preenchido";
		 }
		 if (codOrgao.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo do �rgao inv�lido";
		 }
		 //Cr�tica do C�digo da Junta 		
		 if ((codJunta.equals(null)) ||(codJunta.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo da Junta n�o preenchido";
		 }
		 if (codJunta.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo da Junta inv�lido";
		 }
		 //Cr�tica da A��o 		
		 if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo do A��o n�o preenchido";
		 }
		 if ("12345".indexOf(codAcao) < 0){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: C�digo da A��o inv�lido";
		 }
		 //Cr�tica do Indicador de Continuidade 		
		 if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			 indContinua = "                       ";
		 }
		 if (indContinua.length() != 11){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao087: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		 }
		 
		 if (transOK) {
			 String parteVar = codOrgao + codJunta + numCPF + codAcao + indContinua 
			 + sys.Util.rPad(nomRelator," ",40);  
			 
			 transBRK.setParteFixa("087");
			 transBRK.setParteVariavel(parteVar);
			 try {
				 resultado = transBRK.getResultado();
			 } catch (Exception e) {
				 throw new DaoException (e.getMessage());
			 }
		 }
		 
		 return resultado;
	 }
	 
	 
	 /**************************************
	  * 
	  * @param codOrgao C�digo do Orgao. 999999 = Todos
	  * @param codAcao C�digo da a��o desejada
	  * @param indContinua C�digo do Orgao/Respons�vel pelo parecer Jur�dico a partir do qual continuar� a consulta geral 
	  * @return Retorna as 13 posi��es de Retorno do Broker concatenadas em um String - 11/11/2005
	  */
	 
	 public String Transacao088(String codOrgao, String numCPF, 
			 String codAcao, String indContinua,	String nomResponsavel) throws DaoException {
		 String resultado = "";
		 boolean transOK = true;
		 BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		 
		 //Cr�tica do C�digo do �rg�o		
		 if ((codOrgao.equals(null)) ||(codOrgao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: C�digo do �rg�o n�o preenchido";
		 }
		 if (codOrgao.length() != 6){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: C�digo do �rg�o inv�lido";
		 }
		 //Cr�tica do CPF	
		 if ((numCPF.equals(null)) ||(numCPF.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: CPF n�o preenchido";
		 }
		 if (numCPF.length() != 11){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: CPF inv�lido";
		 }
		 //Cr�tica da A��o 		
		 if ((codAcao.equals(null)) ||(codAcao.trim().equals(null))){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: C�digo do A��o n�o preenchido";
		 }
		 if ("12345".indexOf(codAcao) < 0){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: C�digo da A��o inv�lido";
		 }
		 //Cr�tica do Indicador de Continuidade 		
		 if ((indContinua.equals(null)) ||(indContinua.trim().equals(null))){
			 indContinua = "                 ";
		 }
		 if (indContinua.length() != 17){
			 transOK=false;
			 resultado = "ERR-DaoBroker.Transacao088: Indicador de Continuidade inv�lido-> '" + indContinua + "'";
		 }
		 
		 if (transOK) {
			 String parteVar = codOrgao + numCPF + codAcao + indContinua 
			 + sys.Util.rPad(nomResponsavel," ",43);  
			 
			 transBRK.setParteFixa("088");
			 transBRK.setParteVariavel(parteVar);
			 try {
				 resultado = transBRK.getResultado();
			 } catch (Exception e) {
				 throw new DaoException (e.getMessage());
			 }
		 }
		 
		 return resultado;
	 }
	 
	 public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua) 
	 throws DaoException {
		 String resultado = "";
		 BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		 if (opcEscolha.equals(null)) 
			 resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		 else {
			 transBRK.setParteFixa(codTransacao);				
			 transBRK.setParteVariavel(opcEscolha + indContinua);
			 resultado = transBRK.getResultado();				  
		 }
		 return resultado;
	 }

	 
	 public String chamaTransBRK(String codTransacao,String opcEscolha, String indContinua, UsuarioBean myUsuario) 
	 throws DaoException {
		 String resultado = "";
		 BRK.TransacaoBRKR transBRK = new BRK.TransacaoBRKR();
		 if (opcEscolha.equals(null)) 
			 resultado =	"ERR-DaoBroker.Transacao("+codTransacao+"): Par�metros para pesquisa n�o preenchidos";
		 else {
			 transBRK.setCodOrgaoAtuacao(myUsuario.getCodOrgaoAtuacao());
			 transBRK.setCodOrgaoLotacao(myUsuario.getOrgao().getCodOrgao());
			 transBRK.setNomUserName(myUsuario.getNomUserName());
			 
			 transBRK.setParteFixa(codTransacao);				
			 transBRK.setParteVariavel(opcEscolha + indContinua);
			 resultado = transBRK.getResultado();
			 
			 if ((VerificaDt(codTransacao,resultado,transBRK)==false)) resultado="ERR";
		 }
		 return resultado;
	 }
	 
	 
	 
	 public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg,Statement stmt) throws DaoException {
		 boolean bOk = false ;
		 try {
			 if ((resultado.substring(0,3).equals("000")==false) &&
					 (resultado.substring(0,3).equals("037")==false)) {
				 if ( (resultado.substring(0,3).equals("ERR")) || (sys.Util.isNumber(resultado.substring(0,3))==false) ) {
					 myInf.setMsgErro(msg+" Erro: "+resultado) ;		
				 }
				 else {
					 myInf.setMsgErro(msg+" Erro: "+ resultado.substring(0,3)) ;
					 if(myInf.getAutoAntigo().equals("1"))
						 myInf.setMsgErro(msg+" AUTOS ANTERIORES � JULHO DE 2004 E COM PROCESSO ABERTO, S� EST�O DISPON�VEIS PARA CONSULTA.") ;
					 else if(resultado.substring(0,3).equals("060") && "1,2,3".indexOf(myInf.getIdentAutoOrigem())>=0)
						 myInf.setMsgErro(msg+" Erro: ("+resultado.substring(0,3)+") IMPOSS�VEL EFETUAR TRANSA��O - AUTO RENAINF.") ;
						 
					 else
					 {	
						 String sCmd    = "SELECT dsc_erro from TSMI_ERRO_BROKER WHERE cod_erro = '"+resultado.substring(0,3)+"'" ;
						 ResultSet rs   = stmt.executeQuery(sCmd) ;
						 while (rs.next()) {
							 myInf.setMsgErro(msg+" Erro: "+resultado.substring(0,3)+" - "+ rs.getString("dsc_erro")) ;
						 }
						 rs.close();
					 }
				 }
			 }
			 else   bOk = true ;
			 if (resultado.substring(0,3).equals("718"))
				 myInf.setMsgErro(msg+"/"+myInf.getNumRequerimento());
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return bOk;  
	 }
	 
	 public boolean verErro(AutoInfracaoBean myInf,String resultado,String msg) throws DaoException {
		 Connection conn = null ;
		 boolean bOK = false ;	
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);	
			 Statement stmt = conn.createStatement();
			 bOK = verErro(myInf,resultado,msg,stmt);
			 stmt.close();		
		 }
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }	
		 return bOK;
	 }
	 
	 public void acertaComplemento (HistoricoBean myHis) throws DaoException {
		 if ("206,326,334, 211,331,338, 236,356,364".indexOf(myHis.getCodEvento())>=0) {
			 myHis.setTxtComplemento03(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento03()));
		 }
		 if ("200, 204,324, 207,352, 208,328,335, 209,329,336, 210,330,337, 215,226,350,351,352 227, 237,357,365 239 406,410,411".indexOf(myHis.getCodEvento())>=0) {
			 myHis.setTxtComplemento02(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento02()));
		 }
		 if ("214,381,382,383,388,389,".indexOf(myHis.getCodEvento())>=0) {
			 myHis.setTxtComplemento01(sys.Util.formataDataDDMMYYYY(myHis.getTxtComplemento01()));	
		 }
	 }
	 /******************************************
	  Verifica se o Auto ja esta no Oracle
	  ******************************************/
	 public void VerificaAutoOracle (AutoInfracaoBean myAuto,Connection conn) throws DaoException { 
		 try {	
			 boolean existeAuto = false;	  
			 String sCmd  = "SELECT cod_auto_infracao from TSMI_AUTO_INFRACAO "+
			 "WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
			 Statement stmt = conn.createStatement();
			 ResultSet rs    = stmt.executeQuery(sCmd) ;
			 while (rs.next()) {
				 existeAuto = true;	  				  	  				  
				 myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	  			 
			 }
			 if (existeAuto == false) {
				 sCmd =  "insert into tsmi_auto_infracao (cod_auto_infracao, num_auto_infracao, num_placa, dat_infracao) "+
				 " values(seq_tsmi_auto_infracao.nextval,"+
				 " '"+myAuto.getNumAutoInfracao()+ "','"+myAuto.getNumPlaca() + "','"+myAuto.getDatInfracao()+"')";
				 stmt.execute(sCmd) ;
				 sCmd  = "SELECT cod_auto_infracao from TSMI_AUTO_INFRACAO WHERE num_auto_infracao = '"+myAuto.getNumAutoInfracao()+"'" ;
				 rs    = stmt.executeQuery(sCmd) ;
				 while (rs.next()) {
					 myAuto.setCodAutoInfracao(rs.getString("cod_auto_infracao"));	  			 
				 }
			 }
			 rs.close();
			 stmt.close();
		 }			
		 catch (Exception ex) {
			 throw new DaoException("DaoBroker - VerificaAutoOracle: "+ex.getMessage());
		 }
		 return;
	 }

	 
	 public  boolean consultaAutoSemAr (ConsultaAutosSemDigiBean myAutosSemDig,ACSS.UsuarioBean myUsrLog) throws DaoException{
		 boolean bOk = true ;
		 Connection conn =null ;					
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;	
			 Statement stmt = conn.createStatement();
			 String sCmd        = "";
			 ResultSet rs       = null;	  
			 boolean continua   = true;
			 String resultado   = "";
			 String parteVar    = "";
			 
			 String indContinua = sys.Util.rPad(" "," ",27);   
			 ArrayList autos = new ArrayList();
			 int reg=0;			
			 
			 while (continua) {
				 parteVar = sys.Util.lPad(myAutosSemDig.getCodOrgao(),"0",6)+ 
				 sys.Util.lPad(myAutosSemDig.getCodStatus(),"0",3)+ sys.Util.formataDataYYYYMMDD(myAutosSemDig.getDataIni());
				 resultado = chamaTransBRK("043",parteVar,indContinua);
				 AutoInfracaoBean myAt = new AutoInfracaoBean();							
				 if (verErro(myAt,resultado,"Erro em Prepara Guia (Transa��o 049).")==false) { 
					 myAutosSemDig.setMsgErro(myAt.getMsgErro());
					 return false;					
				 }
				 String linha = "";
				 for (int i=0; i < 38; i++) {
					 linha = resultado.substring((i*83)+3,(i+1)*83+3);
					 REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();
					 setPosIni(0);											
					 myAuto.setNumAutoInfracao(getPedaco(linha,12," "," "));
					 myAuto.setCodStatus(getPedaco(linha,3," "," "));
					 myAuto.setDatInfracao(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					 myAuto.getInfracao().setCodInfracao(getPedaco(linha,3," "," "));
					 myAuto.getInfracao().setDscEnquadramento(getPedaco(linha,20," "," "));
					 myAuto.getInfracao().setNumPonto(getPedacoNum(linha,1,0,"."));
					 myAuto.getInfracao().setIndTipo(getPedaco(linha,1," ", " "));
					 myAuto.setCodOrgao(getPedacoNum(linha,6,0,"."));
					 myAuto.getInfracao().setValReal(getPedacoNum(linha,9,2,","));
					 myAuto.setDatVencimento(sys.Util.formataDataDDMMYYYY(getPedacoDt(linha,8)));
					 myAuto.setCodMunicipio(getPedacoNum(linha,4,0,"."));
					 myAuto.setNumPlaca(getPedaco(linha,7," "," "));
					 myAuto.getInfracao().setValRealDesconto(getPedacoNum(linha,9,2,","));
					 if(myAutosSemDig.verificaData(myAutosSemDig.getDataIni(),myAutosSemDig.getDataFim(),myAuto.getDatInfracao())){ 
						 if(myAutosSemDig.getOpcao().equals("AR")){
							 // Verifica se tem AR Digitalizado
							 sCmd  = "SELECT COD_AR_DIGITALIZADO FROM TSMI_AR_DIGITALIZADO "+
							 " WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"' ORDER BY NUM_AUTO_INFRACAO ";
							 
							 rs    = stmt.executeQuery(sCmd) ;
							 if (!(rs.next())) {
								 myAuto.setTemARDigitalizado(false);	
								 autos.add(myAuto) ;		
							 }	
						 }	
						 
						 if(myAutosSemDig.getOpcao().equals("AI")){
							 // Verifica se tem AI Digitalizado
							 sCmd  = "SELECT COD_AI_DIGITALIZADO FROM TSMI_AI_DIGITALIZADO "+
							 " WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"' ORDER BY NUM_AUTO_INFRACAO ";
							 rs    = stmt.executeQuery(sCmd) ;
							 if (!(rs.next())) {
								 myAuto.setTemAIDigitalizado(false);	
								 autos.add(myAuto) ;		
							 }	
						 }		
						 
						 if(myAutosSemDig.getOpcao().equals("FOTO")){
							 // Verifica se tem Foto Digitalizada
							 sCmd  = "SELECT COD_FOTO_DIGITALIZADA FROM TSMI_FOTO_DIGITALIZADA "+
							 " WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"' ORDER BY NUM_AUTO_INFRACAO ";
							 rs    = stmt.executeQuery(sCmd) ;
							 if (!(rs.next())) {
								 myAuto.setTemFotoDigitalizada(false);			
								 autos.add(myAuto) ;
							 }	
						 }
					 }										
				 }
				 setPosIni(3+(38*83));
				 indContinua = getPedaco(resultado,12," "," ") ;		
				 if (indContinua.trim().length()<=0) continua=false;
				 reg++;
			 }			    
			 myAutosSemDig.setAutos(autos);	
			 if (rs != null) rs.close();
			 stmt.close();
		 }
		 catch (Exception ex) {
			 bOk = false ;
			 throw new DaoException("consultaAutoSemArDaoBroker: "+ex.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return bOk;
	 }
	 
	 public void buscarReq(String tran,AutoInfracaoBean myAuto,RequerimentoBean myReq,HistoricoBean myHis,ParamOrgBean myParam,Connection conn) throws DaoException {
		 try {
			 // Pegar o Assunto
			 String nAss = "";
			 if ("1P,1C,2P,2C".indexOf(myReq.getCodTipoSolic())>=0) nAss = myParam.getParamOrgao("COD_ASSUNTO_PENALIDADE","4110","3") ;			
			 else nAss = myParam.getParamOrgao("COD_ASSUNTO_DEF_PREVIA","4110","1")	;
			 
			 // pegar numero de processo e requerimento
			 String sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			 " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
			 Statement stmt = conn.createStatement();			
			 ResultSet rs  = stmt.executeQuery(sCmd) ;
			 int numUltProc = 0;
			 boolean naoExisteNumeracao  = true ;
			 boolean naoExisteNumeracao2 = true ;		
			 while (rs.next()) {
				 numUltProc = rs.getInt("VAL_PARAMETRO");	  			 
				 naoExisteNumeracao	= false;			  
			 }
			 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " +
			 " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";	
			 rs  = stmt.executeQuery(sCmd) ;
			 int numUltReq  = 0;
			 while (rs.next()) {
				 numUltReq  = rs.getInt("VAL_PARAMETRO");
				 naoExisteNumeracao2	= false;			  
			 }
			 if ((naoExisteNumeracao) || (naoExisteNumeracao2))	{
				 //	System.err.println("naoExisteNumeracao: "+naoExisteNumeracao2+" "+naoExisteNumeracao);		
				 throw new DaoException("Parametros para numera��o de processo n�o localizado para o Org�o: "+myAuto.getCodOrgao());
			 }
			 // atualizar NUMERO DE PROCESSO DO Auto de Infracao, se ja nao houver
			 String nProc = myAuto.getNumProcesso() ;
			 if (nProc.length()==0) {
				 numUltProc++;
				 nProc = "000000"+String.valueOf(numUltProc) ;
				 if(nAss.length()>0)
					 nProc = myParam.getParamOrgao("COD_SECRETARIA","SEC","2")+"/"+nProc.substring(nProc.length()-6,nProc.length())+"/"+nAss+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2");
				 else
					 nProc = myParam.getParamOrgao("COD_SECRETARIA","SEC","2")+"/"+nProc.substring(nProc.length()-6,nProc.length())+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2");
				 
				 if(nProc.length()>20) nProc=nProc.substring(0,20);
				 // select para ver se existe o numero de processo na TSMI_AUTO_INFRACAO
				 // se existir avanca o contador ate nao achar - fazer +- 200/1000 vezes
				 // 
				 myAuto.setNumProcesso(nProc) ;
				 //				myAuto.setDatProcesso(myAuto.getDatStatus()) ;
				 myAuto.setDatProcesso(sys.Util.formatedToday().substring(0,10)); 
			 }
			 myHis.setNumProcesso(myAuto.getNumProcesso());
			 // atualizar NUMERO DE PROCESSO DO Auto de Infracao, se ja nao houver
			 sCmd = "UPDATE TSMI_AUTO_INFRACAO set COD_STATUS='"+myAuto.getCodStatus()+"',DAT_STATUS=to_date('"+myAuto.getDatStatus()+"','dd/mm/yyyy'), ";
			 sCmd += " NUM_PROCESSO='"+myAuto.getNumProcesso()+"',DAT_PROCESSO = to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),";
			 sCmd += " TXT_EMAIL='"+myHis.getTxtComplemento12()+"' ";		
			 sCmd += " where cod_auto_infracao='"+myAuto.getCodAutoInfracao()+"'";	
			 
			 stmt.execute(sCmd);
			 // gravar no requerimentos
			 numUltReq++		;
			 String nReq = "000000"+String.valueOf(numUltReq) ;			    
			 //	System.err.println("NumRequerimento:antes "+nReq );				
			 
			 
			 nReq = myParam.getParamOrgao("COD_REQ_ORGAO","REQ","2")+"/"+nReq.substring(nReq.length()-6,nReq.length())+"/"+myParam.getParamOrgao("PROCESSO_ANO","2004","2") ;
			 
			 
			 myReq.setNumRequerimento(nReq) ;
			 //	System.err.println("NumRequerimento: "+nReq );		
			 if ("206".equals(tran)) myHis.setTxtComplemento01(myReq.getNumRequerimento());
			 else myHis.setTxtComplemento11(myReq.getNumRequerimento());
			 // Update na Numeracao			
			 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltProc+"'," +
             " NOM_USERNAME_ALT ='" +myParam.getNomUsrNameAlt()+"' "+			 
			 " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"'";		
			 stmt.execute(sCmd);
			 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numUltReq+"'," +
             " NOM_USERNAME_ALT ='" +myParam.getNomUsrNameAlt()+"' "+			 
			 " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"'";		
			 stmt.execute(sCmd);
			 stmt.close();
			 rs.close();		
			 
		 }	
		 catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }//fim do catch
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 
		 return ;		
	 }
	 
	 
	 /**-----------------------------------------------------------
	  * Inicio da Rotina de incremento autom�tico do 
	  * n� de Processo e do n� de Requerimento
	  * -----------------------------------------------------------
	  * @throws DaoException
	  * @throws ServiceLocatorException
	  * @throws SQLException
	  * @author wellem
	  * */
	 public void  IncrementaProcesso(AutoInfracaoBean myAuto,ParamOrgBean myParam,RequerimentoBean myReq,HistoricoBean myHis,String sErro) throws DaoException, ServiceLocatorException, SQLException {
		 
		 Connection conn =null ;
		 try{
			 String sCmd    = "";				  
			 conn           = serviceloc.getConnection(MYABREVSIST);
			 Statement stmt = conn.createStatement();
			 ResultSet rs   = null;					  
			 conn.setAutoCommit(false);
			 if (sErro.equals("039"))	  {
				 //Select para pegar o �ltimo n�mero de processo
				 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " ;
				 sCmd +=" where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
				 rs    = stmt.executeQuery(sCmd) ;
				 int numProc    = 1;
				 while (rs.next()) {
					 //Nesse momento ocorre o incremento do processo					
					 numProc= Integer.parseInt(rs.getString("VAL_PARAMETRO"))+1;
				 }
				 //Atualizo o �ltimo n�mero de processo j� com o incremento
				 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numProc+"', "+
                        "NOM_USERNAME_ALT = '"+myParam.getNomUsrNameAlt()+"' ";				 
				 sCmd += " where nom_Parametro='NUM_ULT_PROCESSO' and cod_orgao='"+myAuto.getCodOrgao()+"'";
				 stmt.execute(sCmd);
			 }
			 
			 if (sErro.equals("718"))      {
				 //Select para pegar o �ltimo n�mero de Requerimento
				 sCmd = "SELECT VAL_PARAMETRO FROM TCAU_PARAM_ORGAO " ;
				 sCmd +=" where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"' FOR UPDATE";
				 rs    = stmt.executeQuery(sCmd) ;
				 int numReq     = 1;
				 while (rs.next()) {
					 //Nesse momento ocorre o incremento do Requerimento
					 numReq= Integer.parseInt(rs.getString("VAL_PARAMETRO"))+1 ;
				 }
				 //Atualizo o �ltimo n�mero de Requerimento j� com o incremento
				 sCmd = "UPDATE TCAU_PARAM_ORGAO set VAL_PARAMETRO='"+numReq+"', "+
                        "NOM_USERNAME_ALT = '"+myParam.getNomUsrNameAlt()+"' ";				 
				 sCmd += " where nom_Parametro='NUM_ULT_REQUERIMENTO' and cod_orgao='"+myAuto.getCodOrgao()+"'";
				 stmt.execute(sCmd);
			 }					
			 conn.commit();
			 stmt.close();
			 rs.close();				 		
		 }
		 catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }//fim do catch
		 finally {
			 if (conn != null) {
				 try { 
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ;
	 }
	 
	 public void  gravarHistorico(HistoricoBean myHis) throws DaoException, ServiceLocatorException, SQLException {
		 
		 Connection conn =null ;
		 try{
			 conn = serviceloc.getConnection(MYABREVSIST);
			 GravarHistorico(myHis,conn);		 		
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }//fim do catch
		 finally {
			 if (conn != null) {
				 try { 
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return ;
	 }
	
	 
	 
	 public  void GravarHistorico(HistoricoBean myHis, Connection conn) throws DaoException {
		 String sCmd = "";
		 try {
			 sCmd  =  "INSERT INTO TSMI_HISTORICO_AUTO " +
			 "(COD_HISTORICO_AUTO,"+
			 "COD_AUTO_INFRACAO,"+
			 "NUM_PROCESSO,"+
			 "COD_STATUS,"+
			 "DAT_STATUS,"+
			 "COD_EVENTO,"+
			 "COD_ORIGEM_EVENTO,"+ 
			 "NOM_USERNAME,"+
			 "COD_ORGAO_LOTACAO,"+
			 "DAT_PROC,"+
			 "TXT_COMPLEMENTO_01,"+             
			 "TXT_COMPLEMENTO_02,"+
			 "TXT_COMPLEMENTO_03,"+
			 "TXT_COMPLEMENTO_04,"+
			 "TXT_COMPLEMENTO_05,"+
			 "TXT_COMPLEMENTO_06,"+
			 "TXT_COMPLEMENTO_07,"+
			 "TXT_COMPLEMENTO_08,"+
			 "TXT_COMPLEMENTO_09,"+
			 "TXT_COMPLEMENTO_10,"+
			 "TXT_COMPLEMENTO_11,"+
			 "TXT_COMPLEMENTO_12) "+
			 " VALUES (" +			 			             
			 " seq_tsmi_historico_auto.nextval,"+
			 " '"+myHis.getCodAutoInfracao()+"',"+ 
			 " '"+myHis.getNumProcesso()+"',"+
			 " '"+myHis.getCodStatus()+"',"+
			 " to_date('"+myHis.getDatStatus()+"','dd/mm/yyyy'), "+
			 " '"+myHis.getCodEvento()+"',"+
			 " '"+myHis.getCodOrigemEvento()+"',"+
			 " '"+myHis.getNomUserName()+"',"+
			 " '"+myHis.getCodOrgaoLotacao()+"',"+
			 " to_date('"+myHis.getDatProc()+"','dd/mm/yyyy'), "+
			 " '"+myHis.getTxtComplemento01()+"',"+
			 " '"+myHis.getTxtComplemento02()+"',"+
			 " '"+myHis.getTxtComplemento03()+"',"+
			 " '"+myHis.getTxtComplemento04()+"',"+
			 " '"+myHis.getTxtComplemento05()+"',"+
			 " '"+myHis.getTxtComplemento06()+"',"+
			 " '"+myHis.getTxtComplemento07()+"',"+
			 " '"+myHis.getTxtComplemento08()+"',"+
			 " '"+myHis.getTxtComplemento09()+"',"+
			 " '"+myHis.getTxtComplemento10()+"',"+
			 " '"+myHis.getTxtComplemento11()+"',"+
			 " '"+myHis.getTxtComplemento12()+"')";		
			 Statement stmt     = conn.createStatement();
			 //System.err.println("gravarHistorico: "+sCmd );		
			 stmt.execute(sCmd) ;
			 stmt.close();
		 }
		 catch (SQLException e) {
			 //System.out.println("* SQL Inv�lido :"+sCmd);			 
			 throw new DaoException(e.getMessage());
		 }	  
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return ;
	 }
	 
	 public  void GravarRequerimento(RequerimentoBean myReq, Connection conn) throws DaoException {
		 String sCmd = "";
		 try 
		 {
			 Statement stmt  = conn.createStatement();
			 sCmd     =  "SELECT * FROM TSMI_REQUERIMENTO WHERE "+
			 "COD_AUTO_INFRACAO='"+myReq.getCodAutoInfracao()+"' and "+              
			 "NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
			 ResultSet rs    = stmt.executeQuery(sCmd) ;
			 if (rs.next())
			 {
				 sCmd  =  "UPDATE TSMI_REQUERIMENTO SET " +
				 "COD_TIPO_SOLIC='"+myReq.getCodTipoSolic()+"',"+
				 "DAT_REQUERIMENTO=to_date('"+myReq.getDatRequerimento()+"','dd/mm/yyyy'),"+
				 "COD_STATUS_REQUERIMENTO='"+myReq.getCodStatusRequerimento()+"'," +
				 "NOM_USERNAME_DP='"+myReq.getNomUserNameDP()+"'," +
				 "COD_ORGAO_LOTACAO_DP='"+myReq.getCodOrgaoLotacaoDP()+"'," +
				 "NOM_RESP_DP='"+myReq.getNomResponsavelDP()+"',"+			
				 "DAT_PROC_DP=to_date('"+myReq.getDatProcDP()+"','dd/mm/yyyy'),"+
				 "DAT_PJ=to_date('"+myReq.getDatPJ()+"','dd/mm/yyyy'),"+                
				 "COD_PARECER_PJ='"+myReq.getCodParecerPJ()+"'," +
				 "COD_RESP_PJ='"+myReq.getCodRespPJ()+"'," +
				 "TXT_MOTIVO_PJ='"+myReq.getTxtMotivoPJ()+"'," +
				 "NOM_USERNAME_PJ='"+myReq.getNomUserNamePJ()+"'," +
				 "COD_ORGAO_LOTACAO_PJ='"+myReq.getCodOrgaoLotacaoPJ()+"'," +
				 "DAT_PROC_PJ=to_date('"+myReq.getDatProcPJ()+"','dd/mm/yyyy'),"+
				 "DAT_JU=to_date('"+myReq.getDatJU()+"','dd/mm/yyyy'), "+
				 "COD_JUNTA_JU='"+myReq.getCodJuntaJU()+"'," +
				 "COD_RELATOR_JU='"+myReq.getCodRelatorJU()+"'," +
				 "NOM_USERNAME_JU='"+myReq.getNomUserNameJU()+"'," +
				 "COD_ORGAO_LOTACAO_JU='"+myReq.getCodOrgaoLotacaoJU()+"'," +
				 "DAT_PROC_JU=to_date('"+myReq.getDatProcJU()+"','dd/mm/yyyy'),"+
				 "DAT_RS=to_date('"+myReq.getDatRS()+"','dd/mm/yyyy'), " +
				 "COD_RESULT_RS='"+myReq.getCodResultRS()+"'," +
				 "TXT_MOTIVO_RS='"+myReq.getTxtMotivoRS()+"'," +
				 "NOM_USERNAME_RS='"+myReq.getNomUserNameRS()+"'," +
				 "COD_ORGAO_LOTACAO_RS='"+myReq.getCodOrgaoLotacaoRS()+"'," +
				 "DAT_PROC_RS=to_date('"+myReq.getDatProcRS()+"','dd/mm/yyyy')," +
				 "NOM_CONDUTOR_TR='"+myReq.getNomCondutorTR()+"',"+
				 "IND_TIPO_CNH_TR='"+myReq.getIndTipoCNHTR()+"',"+
				 "NUM_CNH_TR='"+myReq.getNumCNHTR()+"',"+
				 "COD_UF_CNH_TR='"+myReq.getCodUfCNHTR()+"',"+
				 "NUM_CPF_TR='"+myReq.getNumCPFTR()+"',"+	
				 "DAT_ATU_TR=to_date('"+myReq.getDatAtuTR()+"','dd/mm/yyyy'),"+
				 "NOM_USERNAME_TR='"+myReq.getNomUserNameTR()+"',"+
				 "COD_ORGAO_LOTACAO_TR="+(myReq.getCodOrgaoLotacaoTR().equals("0")?"null":"'"+myReq.getCodOrgaoLotacaoTR()+"'")+","+
				 "SIT_PROTOCOLO='"+myReq.getSitProtocolo()+"',"+
                 "COD_ENVIO_PUBDO= '"+myReq.getCodEnvioDO()+"',"+
                 "DAT_ENVIO_PUBDO= to_date('"+myReq.getDatEnvioDO()+"','dd/mm/yyyy'),"+
                 "DAT_PUBDO= to_date('"+myReq.getDatPublPDO()+"','dd/mm/yyyy'), " +
                 "COD_MOTIVO_DEFERIDO= '"+ myReq.getCodMotivoDef()+"' "+
				 "WHERE " +
				 "COD_AUTO_INFRACAO='"+myReq.getCodAutoInfracao()+"' and "+
				 "NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
			 }
			 else
			 {
				 sCmd  =  "INSERT INTO TSMI_REQUERIMENTO " +
				 "(COD_REQUERIMENTO," +
				 "COD_AUTO_INFRACAO," +
				 "COD_TIPO_SOLIC," +
				 "NUM_REQUERIMENTO," +
				 "DAT_REQUERIMENTO," +
				 "COD_STATUS_REQUERIMENTO," +
				 "NOM_USERNAME_DP," +
				 "COD_ORGAO_LOTACAO_DP," +
				 "NOM_RESP_DP," +			
				 "DAT_PROC_DP," +
				 "DAT_PJ," +                
				 "COD_PARECER_PJ," +
				 "COD_RESP_PJ," +
				 "TXT_MOTIVO_PJ," +
				 "NOM_USERNAME_PJ," +
				 "COD_ORGAO_LOTACAO_PJ," +
				 "DAT_PROC_PJ," +
				 "DAT_JU," +
				 "COD_JUNTA_JU," +
				 "COD_RELATOR_JU," +
				 "NOM_USERNAME_JU," +
				 "COD_ORGAO_LOTACAO_JU," +
				 "DAT_PROC_JU," +
				 "DAT_RS," +
				 "COD_RESULT_RS," +
				 "TXT_MOTIVO_RS," +
				 "NOM_USERNAME_RS," +
				 "COD_ORGAO_LOTACAO_RS," +
				 "DAT_PROC_RS," +
				 "NOM_CONDUTOR_TR," +
				 "IND_TIPO_CNH_TR," +
				 "NUM_CNH_TR," +
				 "COD_UF_CNH_TR," +
				 "NUM_CPF_TR," +	
				 "DAT_ATU_TR," +
				 "NOM_USERNAME_TR," +
				 "COD_ORGAO_LOTACAO_TR," +
				 "SIT_PROTOCOLO," +
                 "COD_ENVIO_PUBDO," +
                 "DAT_ENVIO_PUBDO," +
                 "DAT_PUBDO," +
                 "COD_MOTIVO_DEFERIDO) "+
				 " VALUES (" +
				 " seq_tsmi_requerimento.nextval," +
				 " '"+myReq.getCodAutoInfracao()+"'," +
				 "'"+myReq.getCodTipoSolic()+"'," +
				 " '"+myReq.getNumRequerimento()+"'," +
				 " to_date('"+myReq.getDatRequerimento()+"','dd/mm/yyyy')," +
				 " '"+myReq.getCodStatusRequerimento()+"'," +
				 " '"+myReq.getNomUserNameDP()+"'," +
				 " '"+myReq.getCodOrgaoLotacaoDP()+"'," +
				 " '"+myReq.getNomResponsavelDP()+"',"+
				 " to_date('"+myReq.getDatProcDP()+"','dd/mm/yyyy'),"+
				 " to_date('"+myReq.getDatPJ()+"','dd/mm/yyyy'),"+                
				 " '"+myReq.getCodParecerPJ()+"'," +
				 " '"+myReq.getCodRespPJ()+"'," +
				 " '"+myReq.getTxtMotivoPJ()+"'," +
				 " '"+myReq.getNomUserNamePJ()+"'," +
				 " '"+myReq.getCodOrgaoLotacaoPJ()+"'," +
				 " to_date('"+myReq.getDatProcPJ()+"','dd/mm/yyyy'),"+
				 " to_date('"+myReq.getDatJU()+"','dd/mm/yyyy'), "+
				 " '"+myReq.getCodJuntaJU()+"'," +
				 " '"+myReq.getCodRelatorJU()+"'," +
				 " '"+myReq.getNomUserNameJU()+"'," +
				 " '"+myReq.getCodOrgaoLotacaoJU()+"'," +
				 " to_date('"+myReq.getDatProcJU()+"','dd/mm/yyyy'),"+
				 " to_date('"+myReq.getDatRS()+"','dd/mm/yyyy'), " +
				 " '"+myReq.getCodResultRS()+"'," +
				 " '"+myReq.getTxtMotivoRS()+"'," +
				 " '"+myReq.getNomUserNameRS()+"'," +
				 " '"+myReq.getCodOrgaoLotacaoRS()+"'," +
				 " to_date('"+myReq.getDatProcRS()+"','dd/mm/yyyy')," +
				 " '"+myReq.getNomCondutorTR()+"',"+
				 " '"+myReq.getIndTipoCNHTR()+"',"+			
				 " '"+myReq.getNumCNHTR()+"',"+
				 "'"+myReq.getCodUfCNHTR()+"',"+
				 " '"+myReq.getNumCPFTR()+"',"+
				 " to_date('"+myReq.getDatAtuTR()+"','dd/mm/yyyy'),"+
				 " '"+myReq.getNomUserNameTR()+"',"+
				 (myReq.getCodOrgaoLotacaoTR().equals("0")?"null":"'"+myReq.getCodOrgaoLotacaoTR()+"'")+","+
				 " '"+myReq.getSitProtocolo()+"'," +
                 " '"+myReq.getCodEnvioDO()+"'," +
                 " to_date('"+myReq.getDatEnvioDO()+"','dd/mm/yyyy'),"+
                 " to_date('"+myReq.getDatPublPDO()+"','dd/mm/yyyy')," +
                 " '"+myReq.getCodMotivoDef()+"')";
			 }
			 stmt.execute(sCmd);
			 stmt.close();
			 rs.close();
		 }
		 catch (SQLException e) {
			 //System.out.println("* SQL Inv�lido :"+sCmd);
			 throw new DaoException(e.getMessage());
		 }	  
		 catch (Exception ex) {
			 throw new DaoException(ex.getMessage());
		 }
		 return ;
	 }
	 
	 public void  GravarHistorico(AutoInfracaoBean myInf) throws DaoException, ServiceLocatorException, SQLException {
		 Connection conn =null ;
		 ResultSet rs = null;
		 String sCmd  = "";
		 conn = serviceloc.getConnection(MYABREVSIST);
		 Statement stmt = conn.createStatement();
		 conn.setAutoCommit(false);
		 try{
			 sCmd="DELETE FROM TSMI_HISTORICO_AUTO WHERE Cod_Auto_Infracao='"+myInf.getCodAutoInfracao()+"'";
			 stmt.executeQuery(sCmd);
			 Iterator it = myInf.getHistoricos().iterator();
			 REC.HistoricoBean myhist = new REC.HistoricoBean();
			 while (it.hasNext())
			 {
				 myhist = (REC.HistoricoBean)it.next();
				 GravarHistorico(myhist,conn);
			 }
			 conn.commit();
			 if (rs != null)rs.close();	
			 if (conn != null) stmt.close();
		 }
		 catch (SQLException sqle) {
			 throw new DaoException(sqle.getMessage());
		 }
		 catch (Exception e) {	
			 throw new DaoException(e.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { 
					 conn.rollback();
					 serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
	 }
	 
	 
	 /****************************************************
	  * M�todos para acessar o Auto de Infra��o no Oracle
	  ****************************************************/
	 public void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto) throws DaoException {
		 Connection conn = null;
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 GravaAutoInfracaoLocal(myAuto, conn);
			 conn.commit();
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try {
					 serviceloc.setReleaseConnection(conn);
				 } catch (Exception e) { 
					 System.out.println(e);
				 }
			 }
		 }
	 }
	 
	 public void GravaAutoInfracaoLocal(AutoInfracaoBean myAuto, Connection conn) throws DaoException,SQLException {
		 String sqlIns     = "";
		 String sqlComando = "";
		 
		 PreparedStatement stmSel = null;
		 PreparedStatement stmIns = null;
		 Statement stmtComando    = null;
		 Statement stmSeq         = null;
		 
		 ResultSet rsSel          = null;
		 ResultSet rsComando      = null;
		 ResultSet rsSeq          = null; 
		 
		 try {
			 // Verifica se o auto existe
			 String sqlSel = "SELECT NUM_AUTO_INFRACAO FROM TSMI_AUTO_INFRACAO WHERE NUM_AUTO_INFRACAO = :NUM_AUTO_INFRACAO";
			 stmSel = conn.prepareStatement(sqlSel);
			 stmSel.setString(1, myAuto.getNumAutoInfracao());
			 rsSel  = stmSel.executeQuery();
			 if (!rsSel.next()) {
				 rsSel.close();
				 // Carrega a sequence do auto
				 String sqlSeq = "SELECT SEQ_TSMI_AUTO_INFRACAO.NEXTVAL CODIGO FROM DUAL";
				 stmSeq = conn.createStatement();
				 rsSeq  = stmSeq.executeQuery(sqlSeq);
				 if (rsSeq.next()) {
					 myAuto.setCodAutoInfracao(rsSeq.getString("CODIGO"));
				 }
				 stmSeq.close();
				 
				 sqlIns  = "INSERT INTO TSMI_AUTO_INFRACAO ";
				 sqlIns += "(COD_AUTO_INFRACAO,";
				 sqlIns += "NUM_AUTO_INFRACAO,";
				 sqlIns += "DAT_INFRACAO,";
				 sqlIns += "VAL_HOR_INFRACAO,";
				 sqlIns += "DSC_LOCAL_INFRACAO,";
				 sqlIns += "NUM_PLACA,";
				 sqlIns += "NUM_PROCESSO,";
				 sqlIns += "DAT_PROCESSO,";
				 sqlIns += "COD_AGENTE,";
				 sqlIns += "COD_ORGAO,";
				 sqlIns += "DSC_LOCAL_APARELHO,";
				 sqlIns += "NUM_CERTIF_AFER_APARELHO,";
				 /*
				 sqlIns += "DAT_ULT_AFER_APARELHO,";
				 */
				 sqlIns += "NUM_IDENT_APARELHO,";
				 sqlIns += "NUM_INMETRO_APARELHO,";
				 sqlIns += "COD_TIPO_DISP_REGISTRADOR,";
				 sqlIns += "DAT_VENCIMENTO,";
				 sqlIns += "COD_MUNICIPIO,";
				 sqlIns += "VAL_VELOC_PERMITIDA,";
				 sqlIns += "VAL_VELOC_AFERIDA,";
				 sqlIns += "VAL_VELOC_CONSIDERADA,";
				 sqlIns += "DSC_SITUACAO,";
				 sqlIns += "IND_CONDUTOR_IDENTIFICADO,";
				 sqlIns += "NUM_LOTE,";
				 sqlIns += "COD_STATUS,";
				 sqlIns += "DAT_STATUS,";
				 sqlIns += "DAT_PUBDO,";
				 sqlIns += "COD_ENVIO_PUBDO,";
				 sqlIns += "DAT_ENVDO,";
				 sqlIns += "IND_TP_CV,";
				 sqlIns += "COD_RESULT_PUBDO,";
				 sqlIns += "COD_BARRA,";
				 sqlIns += "IND_SITUACAO,";
				 sqlIns += "IND_FASE,";
				 sqlIns += "IND_PAGO,";
				 sqlIns += "NUM_IDENT_ORGAO,";
				 sqlIns += "DSC_RESUMO_INFRACAO,";
				 sqlIns += "DSC_MARCA_MODELO_APARELHO,";
				 sqlIns += "DSC_LOCAL_INFRACAO_NOTIF,";
				 sqlIns += "COD_INFRACAO,";
				 sqlIns += "DSC_INFRACAO,";
				 sqlIns += "DSC_ENQUADRAMENTO,";
				 sqlIns += "VAL_REAL,";
				 sqlIns += "NUM_PONTO,";
				 sqlIns += "DSC_COMPETENCIA,";
				 sqlIns += "IND_COND_PROP,";
				 sqlIns += "IND_SUSPENSAO,";
				 sqlIns += "VAL_REAL_DESCONTO,";
				 sqlIns += "COD_MARCA_MODELO,";
				 sqlIns += "COD_ESPECIE,";
				 sqlIns += "COD_CATEGORIA,";
				 sqlIns += "COD_TIPO,";
				 sqlIns += "COD_COR,";
				 sqlIns += "NOM_PROPRIETARIO,";
				 sqlIns += "TIP_DOC_PROPRIETARIO,";
				 sqlIns += "NUM_DOC_PROPRIETARIO,";
				 sqlIns += "TIP_CNH_PROPRIETARIO,";
				 sqlIns += "NUM_CNH_PROPRIETARIO,";
				 sqlIns += "DSC_END_PROPRIETARIO,";
				 sqlIns += "NUM_END_PROPRIETARIO,";
				 sqlIns += "DSC_COMP_PROPRIETARIO,";
				 sqlIns += "COD_MUN_PROPRIETARIO,";
				 sqlIns += "NUM_CEP_PROPRIETARIO,";
				 sqlIns += "UF_PROPRIETARIO,";
				 sqlIns += "NOM_CONDUTOR,";
				 sqlIns += "TIP_DOC_CONDUTOR,";
				 sqlIns += "NUM_DOC_CONDUTOR,";
				 sqlIns += "TIP_CNH_CONDUTOR,";
				 sqlIns += "NUM_CNH_CONDUTOR,";
				 sqlIns += "DSC_END_CONDUTOR,";
				 sqlIns += "NUM_END_CONDUTOR,";
				 sqlIns += "DSC_COMP_CONDUTOR,";
				 sqlIns += "COD_MUN_CONDUTOR,";
				 sqlIns += "NUM_CEP_CONDUTOR,";
				 sqlIns += "UF_CONDUTOR,";
				 sqlIns += "NUM_NOTIF_AUTUACAO,";
				 sqlIns += "DAT_NOTIF_AUTUACAO,";
				 sqlIns += "COD_ENT_AUTUACAO,";
				 sqlIns += "NUM_LOTE_AUTUACAO,";
				 sqlIns += "NUM_NOTIF_PENALIDADE,";
				 sqlIns += "DAT_NOTIF_PENALIDADE,";
				 sqlIns += "COD_ENT_PENALIDADE,";
				 sqlIns += "NUM_LOTE_PENALIDADE,";
				 sqlIns += "DSC_AGENTE,";
				 sqlIns += "DSC_UNIDADE,";
				 sqlIns += "DAT_PAGAMENTO,";
				 sqlIns += "NUM_AUTO_ORIGEM,";
				 sqlIns += "COD_INFRACAO_ORIGEM,";
				 sqlIns += "IND_TIPO_AUTO,";
				 sqlIns += "NUM_AUTO_RENAINF,";
				 sqlIns += "TXT_EMAIL,";
				 sqlIns += "DAT_INCLUSAO,";
				 sqlIns += "DAT_REGISTRO)";
				 sqlIns += " VALUES (:COD_AUTO_INFRACAO,";
				 sqlIns += ":NUM_AUTO_INFRACAO,";
				 sqlIns += ":DAT_INFRACAO,";
				 sqlIns += ":VAL_HOR_INFRACAO,";
				 sqlIns += ":DSC_LOCAL_INFRACAO,";
				 sqlIns += ":NUM_PLACA,";
				 sqlIns += ":NUM_PROCESSO,";
				 sqlIns += ":DAT_PROCESSO,";
				 sqlIns += ":COD_AGENTE,";
				 sqlIns += ":COD_ORGAO,";
				 sqlIns += ":DSC_LOCAL_APARELHO,";
				 sqlIns += ":NUM_CERTIF_AFER_APARELHO,";
				 /*
				 sqlIns += ":DAT_ULT_AFER_APARELHO,";
				 */
				 sqlIns += ":NUM_IDENT_APARELHO,";
				 sqlIns += ":NUM_INMETRO_APARELHO,";
				 sqlIns += ":COD_TIPO_DISP_REGISTRADOR,";
				 sqlIns += ":DAT_VENCIMENTO,";
				 sqlIns += ":COD_MUNICIPIO,";
				 sqlIns += ":VAL_VELOC_PERMITIDA,";
				 sqlIns += ":VAL_VELOC_AFERIDA,";
				 sqlIns += ":VAL_VELOC_CONSIDERADA,";
				 sqlIns += ":DSC_SITUACAO,";
				 sqlIns += ":IND_CONDUTOR_IDENTIFICADO,";
				 sqlIns += ":NUM_LOTE,";
				 sqlIns += ":COD_STATUS,";
				 sqlIns += ":DAT_STATUS,";
				 sqlIns += ":DAT_PUBDO,";
				 sqlIns += ":COD_ENVIO_PUBDO,";
				 sqlIns += ":DAT_ENVDO,";
				 sqlIns += ":IND_TP_CV,";
				 sqlIns += ":COD_RESULT_PUBDO,";
				 sqlIns += ":COD_BARRA,";
				 sqlIns += ":IND_SITUACAO,";
				 sqlIns += ":IND_FASE,";
				 sqlIns += ":IND_PAGO,";
				 sqlIns += ":NUM_IDENT_ORGAO,";
				 sqlIns += ":DSC_RESUMO_INFRACAO,";
				 sqlIns += ":DSC_MARCA_MODELO_APARELHO,";
				 sqlIns += ":DSC_LOCAL_INFRACAO_NOTIF,";
				 sqlIns += ":COD_INFRACAO,";
				 sqlIns += ":DSC_INFRACAO,";
				 sqlIns += ":DSC_ENQUADRAMENTO,";
				 sqlIns += ":VAL_REAL,";
				 sqlIns += ":NUM_PONTO,";
				 sqlIns += ":DSC_COMPETENCIA,";
				 sqlIns += ":IND_COND_PROP,";
				 sqlIns += ":IND_SUSPENSAO,";
				 sqlIns += ":VAL_REAL_DESCONTO,";
				 sqlIns += ":COD_MARCA_MODELO,";
				 sqlIns += ":COD_ESPECIE,";
				 sqlIns += ":COD_CATEGORIA,";
				 sqlIns += ":COD_TIPO,";
				 sqlIns += ":COD_COR,";
				 sqlIns += ":NOM_PROPRIETARIO,";
				 sqlIns += ":TIP_DOC_PROPRIETARIO,";
				 sqlIns += ":NUM_DOC_PROPRIETARIO,";
				 sqlIns += ":TIP_CNH_PROPRIETARIO,";
				 sqlIns += ":NUM_CNH_PROPRIETARIO,";
				 sqlIns += ":DSC_END_PROPRIETARIO,";
				 sqlIns += ":NUM_END_PROPRIETARIO,";
				 sqlIns += ":DSC_COMP_PROPRIETARIO,";
				 sqlIns += ":COD_MUN_PROPRIETARIO,";
				 sqlIns += ":NUM_CEP_PROPRIETARIO,";
				 sqlIns += ":UF_PROPRIETARIO,";
				 sqlIns += ":NOM_CONDUTOR,";
				 sqlIns += ":TIP_DOC_CONDUTOR,";
				 sqlIns += ":NUM_DOC_CONDUTOR,";
				 sqlIns += ":TIP_CNH_CONDUTOR,";
				 sqlIns += ":NUM_CNH_CONDUTOR,";
				 sqlIns += ":DSC_END_CONDUTOR,";
				 sqlIns += ":NUM_END_CONDUTOR,";
				 sqlIns += ":DSC_COMP_CONDUTOR,";
				 sqlIns += ":COD_MUN_CONDUTOR,";
				 sqlIns += ":NUM_CEP_CONDUTOR,";
				 sqlIns += ":UF_CONDUTOR,";
				 sqlIns += ":NUM_NOTIF_AUTUACAO,";
				 sqlIns += ":DAT_NOTIF_AUTUACAO,";
				 sqlIns += ":COD_ENT_AUTUACAO,";
				 sqlIns += ":NUM_LOTE_AUTUACAO,";
				 sqlIns += ":NUM_NOTIF_PENALIDADE,";
				 sqlIns += ":DAT_NOTIF_PENALIDADE,";
				 sqlIns += ":COD_ENT_PENALIDADE,";
				 sqlIns += ":NUM_LOTE_PENALIDADE,";
				 sqlIns += ":DSC_AGENTE,";
				 sqlIns += ":DSC_UNIDADE,";
				 sqlIns += ":DAT_PAGAMENTO,";
				 sqlIns += ":NUM_AUTO_ORIGEM,";
				 sqlIns += ":COD_INFRACAO_ORIGEM,";
				 sqlIns += ":IND_TIPO_AUTO,";
				 sqlIns += ":NUM_AUTO_RENAINF,";
				 sqlIns += ":TXT_EMAIL,";
				 sqlIns += ":DAT_INCLUSAO,";
				 sqlIns += ":DAT_REGISTRO)";
				 
				 stmIns = conn.prepareStatement(sqlIns);
				 converteInt(myAuto.getCodAutoInfracao(), 1, stmIns);				 
				 stmIns.setString(2, myAuto.getNumAutoInfracao());
				 stmIns.setDate(3, converteData(myAuto.getDatInfracao()));
				 stmIns.setString(4, myAuto.getValHorInfracao());
				 stmIns.setString(5, myAuto.getDscLocalInfracao());				 
				 stmIns.setString(6, myAuto.getNumPlaca());
				 stmIns.setString(7, myAuto.getNumProcesso());
				 stmIns.setDate(8, converteData(myAuto.getDatProcesso()));				 				 
				 stmIns.setString(9, myAuto.getCodAgente());
				 converteInt(myAuto.getCodOrgao(), 10, stmIns);				 
				 stmIns.setString(11, myAuto.getDscLocalAparelho());
				 stmIns.setString(12, myAuto.getNumCertAferAparelho());
				 stmIns.setDate(13, converteData(myAuto.getDatUltAferAparelho()));
				 stmIns.setString(14, myAuto.getNumIdentAparelho());
				 stmIns.setString(15, myAuto.getNumInmetroAparelho());
				 stmIns.setString(16, myAuto.getCodTipDispRegistrador());				 
				 stmIns.setDate(17, converteData(myAuto.getDatVencimento()));
				 converteInt(myAuto.getCodMunicipio(), 18, stmIns);
				 converteInt(myAuto.getValVelocPermitida(), 19, stmIns);
				 converteDouble(myAuto.getValVelocAferida(), 20, stmIns);
				 converteDouble(myAuto.getValVelocConsiderada(), 21, stmIns);
				 stmIns.setString(22, myAuto.getDscSituacao());
				 stmIns.setString(23, myAuto.getIndCondutorIdentificado());
				 stmIns.setString(24, myAuto.getCodLote());
				 converteInt(myAuto.getCodStatus(), 25, stmIns);
				 stmIns.setDate(26, converteData(myAuto.getDatStatus()));
				 stmIns.setDate(27, converteData(myAuto.getDatPublPDO()));
				 stmIns.setString(28, myAuto.getCodEnvioDO());
				 stmIns.setDate(29, converteData(myAuto.getDatEnvioDO()));
				 stmIns.setString(30, myAuto.getIndTPCV());
				 stmIns.setString(31, myAuto.getCodResultPubDO());
				 stmIns.setString(32, myAuto.getCodBarra());
				 stmIns.setString(33, myAuto.getIndSituacao());
				 stmIns.setString(34, myAuto.getIndFase());
				 stmIns.setString(35, myAuto.getIndPago());
				 stmIns.setString(36, myAuto.getIdentOrgao());				 
				 stmIns.setString(37, myAuto.getDscResumoInfracao());
				 stmIns.setString(38, myAuto.getDscMarcaModeloAparelho());
				 stmIns.setString(39, myAuto.getDscLocalInfracaoNotificacao());
				 converteInt(myAuto.getInfracao().getCodInfracao(), 40, stmIns);
				 stmIns.setString(41, myAuto.getInfracao().getDscInfracao());
				 stmIns.setString(42, myAuto.getInfracao().getDscEnquadramento());
				 converteDouble(myAuto.getInfracao().getValReal(), 43, stmIns);
				 stmIns.setString(44, myAuto.getInfracao().getNumPonto());
				 stmIns.setString(45, myAuto.getInfracao().getDscCompetencia());
				 stmIns.setString(46, myAuto.getInfracao().getIndTipo());
				 stmIns.setString(47, myAuto.getInfracao().getIndSDD());
				 converteDouble(myAuto.getInfracao().getValRealDesconto(), 48, stmIns);
				 converteInt(myAuto.getVeiculo().getCodMarcaModelo(), 49, stmIns);
				 converteInt(myAuto.getVeiculo().getCodEspecie(), 50, stmIns);
				 converteInt(myAuto.getVeiculo().getCodCategoria(), 51, stmIns);
				 converteInt(myAuto.getVeiculo().getCodTipo(), 52, stmIns);
				 converteInt(myAuto.getVeiculo().getCodCor(), 53, stmIns);
				 stmIns.setString(54, myAuto.getProprietario().getNomResponsavel());				 
				 stmIns.setString(55, myAuto.getProprietario().getIndCpfCnpj());
				 stmIns.setString(56, myAuto.getProprietario().getNumCpfCnpj());
				 stmIns.setString(57, myAuto.getProprietario().getIndTipoCnh());
				 stmIns.setString(58, myAuto.getProprietario().getNumCnh());
				 stmIns.setString(59, myAuto.getProprietario().getEndereco().getTxtEndereco());
				 stmIns.setString(60, myAuto.getProprietario().getEndereco().getNumEndereco());
				 stmIns.setString(61, myAuto.getProprietario().getEndereco().getTxtComplemento());
				 converteInt(myAuto.getProprietario().getEndereco().getCodCidade(), 62, stmIns);
				 stmIns.setString(63, myAuto.getProprietario().getEndereco().getNumCEP());
				 stmIns.setString(64, myAuto.getProprietario().getEndereco().getCodUF()); 				 
				 stmIns.setString(65, myAuto.getCondutor().getNomResponsavel());
				 stmIns.setString(66, myAuto.getCondutor().getIndCpfCnpj());
				 stmIns.setString(67, myAuto.getCondutor().getNumCpfCnpj());
				 stmIns.setString(68, myAuto.getCondutor().getIndTipoCnh());
				 stmIns.setString(69, myAuto.getCondutor().getNumCnh());
				 stmIns.setString(70, myAuto.getCondutor().getEndereco().getTxtEndereco());
				 stmIns.setString(71, myAuto.getCondutor().getEndereco().getNumEndereco());
				 stmIns.setString(72, myAuto.getCondutor().getEndereco().getTxtComplemento());
				 converteInt(myAuto.getCondutor().getEndereco().getCodCidade(), 73, stmIns);
				 stmIns.setString(74, myAuto.getCondutor().getEndereco().getNumCEP());
				 stmIns.setString(75, myAuto.getCondutor().getEndereco().getCodUF());
				 converteInt(myAuto.getNumNotifAut(), 76, stmIns);
				 stmIns.setDate(77, converteData(myAuto.getDatNotifAut()));
				 stmIns.setString(78, myAuto.getCodNotifAut());
				 stmIns.setString(79, myAuto.getNumARNotifAut());
				 converteInt(myAuto.getNumNotifPen(), 80, stmIns);
				 stmIns.setDate(81, converteData(myAuto.getDatNotifPen()));
				 stmIns.setString(82, myAuto.getCodNotifPen());
				 stmIns.setString(83, myAuto.getNumARNotifPen());
				 stmIns.setString(84, myAuto.getDscAgente());
				 stmIns.setString(85, myAuto.getDscUnidade());
				 stmIns.setDate(86, converteData(myAuto.getDatPag()));
				 stmIns.setString(87, myAuto.getNumAutoOrigem());
				 converteInt(myAuto.getNumInfracaoOrigem(), 88, stmIns);				 
				 stmIns.setString(89, myAuto.getIdentAutoOrigem());
				 stmIns.setString(90, myAuto.getNumInfracaoRenainf());
				 stmIns.setString(91, myAuto.getTxtEmail());
				 stmIns.setDate(92, converteData(myAuto.getDatInclusao()));
				 stmIns.setDate(93, converteData(myAuto.getDatRegistro()));				 
				 stmIns.executeUpdate();
				 stmIns.close();
				 
			 } else {
				 stmtComando = conn.createStatement();
				 sqlComando="SELECT * FROM TSMI_INFRACAO WHERE COD_INFRACAO='"+myAuto.getInfracao().getCodInfracao()+"'";
				 rsComando  = stmtComando.executeQuery(sqlComando);
				 if (!rsComando.next()) {
				 	 sqlComando="INSERT INTO TSMI_INFRACAO (COD_INFRACAO,DSC_INFRACAO,NUM_PONTO) VALUES "+
				 	 "('"+myAuto.getInfracao().getCodInfracao()+"','',0)";
					 stmtComando.execute(sqlComando);					 
				 }
				 rsComando.close();
				 rsSel.close();

				 

				 if (myAuto.getDatAlteracao().equals("")) {
					 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					 myAuto.setDatAlteracao(df.format(new Date()));
				 }
				 if (myAuto.getDatProcesso()==null)        myAuto.setDatProcesso("");
				 if (myAuto.getDatUltAferAparelho()==null) myAuto.setDatUltAferAparelho("");
				 if (myAuto.getDatVencimento()==null)      myAuto.setDatVencimento("");
				 if (myAuto.getDatPublPDO()==null)         myAuto.setDatPublPDO("");
				 if (myAuto.getDatEnvioDO()==null)         myAuto.setDatEnvioDO("");
				 if (myAuto.getDatNotifAut()==null)        myAuto.setDatNotifAut("");
				 if (myAuto.getDatNotifPen()==null)        myAuto.setDatNotifPen("");
				 if (myAuto.getDatPag()==null)             myAuto.setDatPag("");
				 double valVelPermitida=0;
				 try {
					 valVelPermitida=Double.parseDouble(myAuto.getValVelocPermitida());
				 }
				 catch (Exception e) {valVelPermitida=0;}
				 double valVelAferida=0;
				 try {
					 valVelAferida=Double.parseDouble(myAuto.getValVelocAferida());
				 }
				 catch (Exception e) {valVelAferida=0;}
				 double valVelConsiderada=0;
				 try {
					 valVelConsiderada=Double.parseDouble(myAuto.getValVelocConsiderada());
				 }
				 catch (Exception e) {valVelConsiderada=0;}
				 double valReal=0;
				 try {
					 valReal=Double.parseDouble(myAuto.getInfracao().getValReal());
				 }
				 catch (Exception e) {valReal=0;}
				 double valRealDesconto=0;
				 try {
					 valRealDesconto=Double.parseDouble(myAuto.getInfracao().getValRealDesconto());
				 }
				 catch (Exception e) {valRealDesconto=0;}

			 
				 sqlComando  = "UPDATE TSMI_AUTO_INFRACAO SET ";
				 sqlComando += "DAT_INFRACAO          = to_date('"+myAuto.getDatInfracao()+"','dd/mm/yyyy'),";
				 sqlComando += "VAL_HOR_INFRACAO      = '"+myAuto.getValHorInfracao()+"',";
				 sqlComando += "DSC_LOCAL_INFRACAO    = '"+myAuto.getDscLocalInfracao()+"',";
				 sqlComando += "NUM_PLACA             = '"+myAuto.getNumPlaca()+"',";
				 sqlComando += "NUM_PROCESSO          = '"+myAuto.getNumProcesso()+"',";
				 sqlComando += "DAT_PROCESSO          = to_date('"+myAuto.getDatProcesso()+"','dd/mm/yyyy'),";
				 sqlComando += "COD_AGENTE            = '"+myAuto.getCodAgente()+"',";
				 sqlComando += "COD_ORGAO             = '"+myAuto.getCodOrgao()+"',";
				 sqlComando += "DSC_LOCAL_APARELHO    = '"+myAuto.getDscLocalAparelho()+"',";
				 sqlComando += "NUM_CERTIF_AFER_APARELHO  = '"+myAuto.getNumCertAferAparelho()+"',";
				 /*
				 sqlComando += "DAT_ULT_AFER_APARELHO = to_date('"+myAuto.getDatUltAferAparelho()+"','dd/mm/yyyy'),";
				 */
				 sqlComando += "NUM_IDENT_APARELHO    = '"+myAuto.getNumIdentAparelho()+"',";
				 sqlComando += "NUM_INMETRO_APARELHO  = '"+myAuto.getNumInmetroAparelho()+"',";
				 sqlComando += "COD_TIPO_DISP_REGISTRADOR = '"+myAuto.getCodTipDispRegistrador()+"',";
				 sqlComando += "DAT_VENCIMENTO        = to_date('"+myAuto.getDatVencimento()+"','dd/mm/yyyy'),";
				 sqlComando += "COD_MUNICIPIO         = '"+myAuto.getCodMunicipio()+"',";
				 sqlComando += "VAL_VELOC_PERMITIDA   =  "+valVelPermitida+",";
				 sqlComando += "VAL_VELOC_AFERIDA     =  "+valVelAferida+",";
				 sqlComando += "VAL_VELOC_CONSIDERADA =  "+valVelConsiderada+",";
				 sqlComando += "DSC_SITUACAO          = '"+myAuto.getDscSituacao()+"',";
				 sqlComando += "IND_CONDUTOR_IDENTIFICADO = '"+myAuto.getIndCondutorIdentificado()+"',";
				 sqlComando += "NUM_LOTE              = '"+myAuto.getCodLote()+"',";
				 sqlComando += "COD_STATUS            = '"+myAuto.getCodStatus()+"',";
				 sqlComando += "DAT_STATUS            = to_date('"+myAuto.getDatStatus()+"','dd/mm/yyyy'),";
				 sqlComando += "DAT_PUBDO             = to_date('"+myAuto.getDatPublPDO()+"','dd/mm/yyyy'),";
				 sqlComando += "COD_ENVIO_PUBDO       = '"+myAuto.getCodEnvioDO()+"',";
				 sqlComando += "DAT_ENVDO             = to_date('"+myAuto.getDatEnvioDO()+"','dd/mm/yyyy'),";
				 sqlComando += "IND_TP_CV             = '"+myAuto.getIndTPCV()+"',";
				 sqlComando += "COD_RESULT_PUBDO      = '"+myAuto.getCodResultPubDO()+"',";
				 sqlComando += "COD_BARRA             = '"+myAuto.getCodBarra()+"',";
				 sqlComando += "IND_SITUACAO          = '"+myAuto.getIndSituacao()+"',";
				 sqlComando += "IND_FASE              = '"+myAuto.getIndFase()+"',";
				 sqlComando += "IND_PAGO              = '"+myAuto.getIndPago()+"',";
				 sqlComando += "NUM_IDENT_ORGAO       = '"+myAuto.getIdentOrgao()+"',";
				 sqlComando += "DSC_RESUMO_INFRACAO   = '"+myAuto.getDscResumoInfracao()+"',";
				 sqlComando += "DSC_MARCA_MODELO_APARELHO = '"+myAuto.getDscMarcaModeloAparelho()+"',";
				 sqlComando += "DSC_LOCAL_INFRACAO_NOTIF  = '"+myAuto.getDscLocalInfracaoNotificacao()+"',";
				 sqlComando += "COD_INFRACAO          = '"+myAuto.getInfracao().getCodInfracao()+"',";
				 sqlComando += "DSC_INFRACAO          = '"+myAuto.getInfracao().getDscInfracao()+"',";
				 sqlComando += "DSC_ENQUADRAMENTO     = '"+myAuto.getInfracao().getDscEnquadramento()+"',";
				 sqlComando += "VAL_REAL              =  "+valReal+",";
				 sqlComando += "NUM_PONTO             = '"+myAuto.getInfracao().getNumPonto()+"',";
				 sqlComando += "DSC_COMPETENCIA       = '"+myAuto.getInfracao().getDscCompetencia()+"',";
				 sqlComando += "IND_COND_PROP         = '"+myAuto.getInfracao().getIndTipo()+"',";
				 sqlComando += "IND_SUSPENSAO         = '"+myAuto.getInfracao().getIndSDD()+"',";
				 sqlComando += "VAL_REAL_DESCONTO     =  "+valRealDesconto+",";
				 sqlComando += "COD_MARCA_MODELO      = '"+myAuto.getVeiculo().getCodMarcaModelo()+"',";
				 sqlComando += "COD_ESPECIE           = '"+myAuto.getVeiculo().getCodEspecie()+"',";
				 sqlComando += "COD_CATEGORIA         = '"+myAuto.getVeiculo().getCodCategoria()+"',";
				 sqlComando += "COD_TIPO              = '"+myAuto.getVeiculo().getCodTipo()+"',";
				 sqlComando += "COD_COR               = '"+myAuto.getVeiculo().getCodCor()+"',";
				 sqlComando += "NOM_PROPRIETARIO      = '"+myAuto.getProprietario().getNomResponsavel()+"',";
				 sqlComando += "TIP_DOC_PROPRIETARIO  = '"+myAuto.getProprietario().getIndCpfCnpj()+"',";
				 sqlComando += "NUM_DOC_PROPRIETARIO  = '"+myAuto.getProprietario().getNumCpfCnpj()+"',";
				 sqlComando += "TIP_CNH_PROPRIETARIO  = '"+myAuto.getProprietario().getIndTipoCnh()+"',";
				 sqlComando += "NUM_CNH_PROPRIETARIO  = '"+myAuto.getProprietario().getNumCnh()+"',";
				 sqlComando += "DSC_END_PROPRIETARIO  = '"+myAuto.getProprietario().getEndereco().getTxtEndereco()+"',";
				 sqlComando += "NUM_END_PROPRIETARIO  = '"+myAuto.getProprietario().getEndereco().getNumEndereco()+"',";
				 sqlComando += "DSC_COMP_PROPRIETARIO = '"+myAuto.getProprietario().getEndereco().getTxtComplemento()+"',";
				 sqlComando += "COD_MUN_PROPRIETARIO  = '"+myAuto.getProprietario().getEndereco().getCodCidade()+"',";
				 sqlComando += "NUM_CEP_PROPRIETARIO  = '"+myAuto.getProprietario().getEndereco().getNumCEP()+"',";
				 sqlComando += "UF_PROPRIETARIO       = '"+myAuto.getProprietario().getEndereco().getCodUF()+"',";
				 sqlComando += "NOM_CONDUTOR          = '"+myAuto.getCondutor().getNomResponsavel()+"',";
				 sqlComando += "TIP_DOC_CONDUTOR      = '"+myAuto.getCondutor().getIndCpfCnpj()+"',";
				 sqlComando += "NUM_DOC_CONDUTOR      = '"+myAuto.getCondutor().getNumCpfCnpj()+"',";
				 sqlComando += "TIP_CNH_CONDUTOR      = '"+myAuto.getCondutor().getIndTipoCnh()+"',";
				 sqlComando += "NUM_CNH_CONDUTOR      = '"+myAuto.getCondutor().getNumCnh()+"',";
				 sqlComando += "DSC_END_CONDUTOR      = '"+myAuto.getCondutor().getEndereco().getTxtEndereco()+"',";
				 sqlComando += "NUM_END_CONDUTOR      = '"+myAuto.getCondutor().getEndereco().getNumEndereco()+"',";
				 sqlComando += "DSC_COMP_CONDUTOR     = '"+myAuto.getCondutor().getEndereco().getTxtComplemento()+"',";
				 sqlComando += "COD_MUN_CONDUTOR      = '"+myAuto.getCondutor().getEndereco().getCodCidade()+"',";
				 sqlComando += "NUM_CEP_CONDUTOR      = '"+myAuto.getCondutor().getEndereco().getNumCEP()+"',";
				 sqlComando += "UF_CONDUTOR           = '"+myAuto.getCondutor().getEndereco().getCodUF()+"',";
				 sqlComando += "NUM_NOTIF_AUTUACAO    = '"+myAuto.getNumNotifAut()+"',";
				 sqlComando += "DAT_NOTIF_AUTUACAO    = to_date('"+myAuto.getDatNotifAut()+"','dd/mm/yyyy'),";
				 sqlComando += "COD_ENT_AUTUACAO      = '"+myAuto.getCodNotifAut()+"',";
				 sqlComando += "NUM_LOTE_AUTUACAO     = '"+myAuto.getNumARNotifAut()+"',";
				 sqlComando += "NUM_NOTIF_PENALIDADE  = '"+myAuto.getNumNotifPen()+"',";
				 sqlComando += "DAT_NOTIF_PENALIDADE  = to_date('"+myAuto.getDatNotifPen()+"','dd/mm/yyyy'),";
				 sqlComando += "COD_ENT_PENALIDADE    = '"+myAuto.getCodNotifPen()+"',";
				 sqlComando += "NUM_LOTE_PENALIDADE   = '"+myAuto.getNumARNotifPen()+"',";
				 sqlComando += "DSC_AGENTE            = '"+myAuto.getDscAgente()+"',";
				 sqlComando += "DSC_UNIDADE           = '"+myAuto.getDscUnidade()+"',";
				 sqlComando += "DAT_PAGAMENTO         = to_date('"+myAuto.getDatPag()+"','dd/mm/yyyy'),";
				 sqlComando += "NUM_AUTO_ORIGEM       = '"+myAuto.getNumAutoOrigem()+"',";
				 sqlComando += "COD_INFRACAO_ORIGEM   = '"+myAuto.getNumInfracaoOrigem()+"',";
				 sqlComando += "IND_TIPO_AUTO         = '"+myAuto.getIdentAutoOrigem()+"',";
				 sqlComando += "NUM_AUTO_RENAINF      = '"+myAuto.getNumInfracaoRenainf()+"',";
				 sqlComando += "TXT_EMAIL             = '"+myAuto.getTxtEmail()+"',";
				 sqlComando += "DAT_INCLUSAO          = to_date('"+myAuto.getDatInclusao()+"','dd/mm/yyyy'),";
				 sqlComando += "DAT_ALTERACAO         = to_date('"+myAuto.getDatAlteracao()+"','dd/mm/yyyy'),";
				 sqlComando += "DAT_REGISTRO          = to_date('"+myAuto.getDatRegistro()+"','dd/mm/yyyy')";
				 sqlComando += " WHERE NUM_AUTO_INFRACAO = '"+myAuto.getNumAutoInfracao()+"'";
				 stmtComando.executeUpdate(sqlComando);
				 stmtComando.close();
			 }
			 stmSel.close();
		 }  
		 catch (SQLException e) {
			 //System.out.println("* SQL Inv�lido : sqlIns:"+sqlIns+" sqlUpd:"+sqlUpd+" sqlComando:"+sqlComando);			 
			 throw new DaoException(e.getMessage());
		 }	  
		 catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 }
		 finally {
			 if (stmSel!=null) stmSel.close();
			 if (stmIns!=null) stmIns.close();
			 if (stmtComando!=null) stmtComando.close();
			 if (stmSeq!=null) stmSeq.close();
			 if (rsSel!=null) rsSel.close();
			 if (rsComando!=null) rsComando.close();
			 if (rsSeq!=null) rsSeq.close(); 
		 }
	 } 
	 
	 public boolean LeAutoInfracaoLocal(AutoInfracaoBean myAuto, String tipo) throws DaoException {		 		 
		 Connection conn = null;
		 boolean retorno = false;
		 
		 if (!tipo.equals("auto") && !tipo.equals("ident")) 
			 throw new DaoException("Tipo de busca inv�lido ! Deve ser informado auto ou nofic.");		 		 
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST);
			 String sql = "SELECT COD_AUTO_INFRACAO,";
			 sql += "NUM_AUTO_INFRACAO,";
			 sql += "DAT_INFRACAO,";
			 sql += "VAL_HOR_INFRACAO,";
			 sql += "DSC_LOCAL_INFRACAO,";
			 sql += "NUM_PLACA,";
			 sql += "NUM_PROCESSO,";
			 sql += "DAT_PROCESSO,";
			 sql += "COD_AGENTE,";
			 sql += "COD_ORGAO,";
			 sql += "DSC_LOCAL_APARELHO,";
			 sql += "NUM_CERTIF_AFER_APARELHO,";
			 sql += "DAT_ULT_AFER_APARELHO,";
			 sql += "NUM_IDENT_APARELHO,";
			 sql += "NUM_INMETRO_APARELHO,";
			 sql += "COD_TIPO_DISP_REGISTRADOR,";
			 sql += "DAT_VENCIMENTO,";
			 sql += "COD_MUNICIPIO,";
			 sql += "VAL_VELOC_PERMITIDA,";
			 sql += "VAL_VELOC_AFERIDA,";
			 sql += "VAL_VELOC_CONSIDERADA,";
			 sql += "DSC_SITUACAO,";
			 sql += "IND_CONDUTOR_IDENTIFICADO,";
			 sql += "NUM_LOTE,";
			 sql += "COD_STATUS,";
			 sql += "DAT_STATUS,";
			 sql += "DAT_PUBDO,";
			 sql += "COD_ENVIO_PUBDO,";
			 sql += "DAT_ENVDO,";
			 sql += "IND_TP_CV,";
			 sql += "COD_RESULT_PUBDO,";
			 sql += "COD_BARRA,";
			 sql += "IND_SITUACAO,";
			 sql += "IND_FASE,";
			 sql += "IND_PAGO,";
			 sql += "NUM_IDENT_ORGAO,";
			 sql += "DSC_RESUMO_INFRACAO,";
			 sql += "DSC_MARCA_MODELO_APARELHO,";
			 sql += "DSC_LOCAL_INFRACAO_NOTIF,";
			 sql += "COD_INFRACAO,";
			 sql += "DSC_INFRACAO,";
			 sql += "DSC_ENQUADRAMENTO,";
			 sql += "VAL_REAL,";
			 sql += "NUM_PONTO,";
			 sql += "DSC_COMPETENCIA,";
			 sql += "IND_COND_PROP,";
			 sql += "IND_SUSPENSAO,";
			 sql += "VAL_REAL_DESCONTO,";
			 sql += "COD_MARCA_MODELO,";
			 sql += "COD_ESPECIE,";
			 sql += "COD_CATEGORIA,";
			 sql += "COD_TIPO,";
			 sql += "COD_COR,";
			 sql += "NOM_PROPRIETARIO,";
			 sql += "TIP_DOC_PROPRIETARIO,";
			 sql += "NUM_DOC_PROPRIETARIO,";
			 sql += "TIP_CNH_PROPRIETARIO,";
			 sql += "NUM_CNH_PROPRIETARIO,";
			 sql += "DSC_END_PROPRIETARIO,";
			 sql += "NUM_END_PROPRIETARIO,";
			 sql += "DSC_COMP_PROPRIETARIO,";
			 sql += "COD_MUN_PROPRIETARIO,";
			 sql += "NUM_CEP_PROPRIETARIO,";
			 sql += "UF_PROPRIETARIO,";
			 sql += "NOM_CONDUTOR,";
			 sql += "TIP_DOC_CONDUTOR,";
			 sql += "NUM_DOC_CONDUTOR,";
			 sql += "TIP_CNH_CONDUTOR,";
			 sql += "NUM_CNH_CONDUTOR,";
			 sql += "DSC_END_CONDUTOR,";
			 sql += "NUM_END_CONDUTOR,";
			 sql += "DSC_COMP_CONDUTOR,";
			 sql += "COD_MUN_CONDUTOR,";
			 sql += "NUM_CEP_CONDUTOR,";
			 sql += "UF_CONDUTOR,";
			 sql += "NUM_NOTIF_AUTUACAO,";
			 sql += "DAT_NOTIF_AUTUACAO,";
			 sql += "COD_ENT_AUTUACAO,";
			 sql += "NUM_LOTE_AUTUACAO,";
			 sql += "NUM_NOTIF_PENALIDADE,";
			 sql += "DAT_NOTIF_PENALIDADE,";
			 sql += "COD_ENT_PENALIDADE,";
			 sql += "NUM_LOTE_PENALIDADE,";
			 sql += "DSC_AGENTE,";
			 sql += "DSC_UNIDADE,";
			 sql += "DAT_PAGAMENTO,";
			 sql += "NUM_AUTO_ORIGEM,";
			 sql += "COD_INFRACAO_ORIGEM,";
			 sql += "IND_TIPO_AUTO,";
			 sql += "NUM_AUTO_RENAINF,";
			 sql += "TXT_EMAIL,";
			 sql += "DAT_INCLUSAO,";
			 sql += "DAT_ALTERACAO,";
			 sql += "DAT_REGISTRO";
			 sql += " FROM TSMI_AUTO_INFRACAO";
			 
			 PreparedStatement stm = null;
			 if (tipo.equals("auto")) {
				 sql += " WHERE NUM_AUTO_INFRACAO = :NUM_AUTO_INFRACAO";
				 stm = conn.prepareStatement(sql);
				 stm.setString(1, myAuto.getNumAutoInfracao());
			 } else {
				 sql += " WHERE NUM_IDENT_ORGAO = :NUM_IDENT_ORGAO";
				 stm = conn.prepareStatement(sql);	 			
				 stm.setString(1, myAuto.getIdentOrgao());	 			
			 }
			 
			 ResultSet rs = stm.executeQuery();			 
			 if (rs.next()) {				 				 
				 
				 myAuto.setCodAutoInfracao(rs.getString("COD_AUTO_INFRACAO"));
				 myAuto.setNumAutoInfracao(rs.getString("NUM_AUTO_INFRACAO"));
				 myAuto.setDatInfracao(converteData(rs.getDate("DAT_INFRACAO")));
				 myAuto.setValHorInfracao(rs.getString("VAL_HOR_INFRACAO"));
				 myAuto.setDscLocalInfracao(rs.getString("DSC_LOCAL_INFRACAO"));
				 myAuto.setNumPlaca(rs.getString("NUM_PLACA"));
				 myAuto.setNumProcesso(rs.getString("NUM_PROCESSO"));
				 myAuto.setDatProcesso(converteData(rs.getDate("DAT_PROCESSO")));
				 myAuto.setCodAgente(rs.getString("COD_AGENTE"));
				 myAuto.setCodOrgao(rs.getString("COD_ORGAO"));
				 myAuto.setDscLocalAparelho(rs.getString("DSC_LOCAL_APARELHO"));
				 myAuto.setNumCertAferAparelho(rs.getString("NUM_CERTIF_AFER_APARELHO"));
				 myAuto.setDatUltAferAparelho(converteData(rs.getDate("DAT_ULT_AFER_APARELHO")));
				 myAuto.setNumIdentAparelho(rs.getString("NUM_IDENT_APARELHO"));
				 myAuto.setNumInmetroAparelho(rs.getString("NUM_INMETRO_APARELHO"));
				 myAuto.setCodTipDispRegistrador(rs.getString("COD_TIPO_DISP_REGISTRADOR"));
				 myAuto.setDatVencimento(converteData(rs.getDate("DAT_VENCIMENTO")));
				 myAuto.setCodMunicipio(rs.getString("COD_MUNICIPIO"));
				 myAuto.setValVelocPermitida(rs.getString("VAL_VELOC_PERMITIDA"));
				 myAuto.setValVelocAferida(rs.getString("VAL_VELOC_AFERIDA"));
				 myAuto.setValVelocConsiderada(rs.getString("VAL_VELOC_CONSIDERADA"));
				 myAuto.setDscSituacao(rs.getString("DSC_SITUACAO"));
				 myAuto.setIndCondutorIdentificado(rs.getString("IND_CONDUTOR_IDENTIFICADO"));
				 myAuto.setCodLote(rs.getString("NUM_LOTE"));
				 myAuto.setCodStatus(rs.getString("COD_STATUS"));
				 myAuto.setDatStatus(converteData(rs.getDate("DAT_STATUS")));
				 myAuto.setDatPublPDO(converteData(rs.getDate("DAT_PUBDO")));
				 myAuto.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
				 myAuto.setDatEnvioDO(converteData(rs.getDate("DAT_ENVDO")));
				 myAuto.setIndTPCV(rs.getString("IND_TP_CV"));
				 myAuto.setCodResultPubDO(rs.getString("COD_RESULT_PUBDO"));
				 myAuto.setCodBarra(rs.getString("COD_BARRA"));
				 myAuto.setIndSituacao(rs.getString("IND_SITUACAO"));
				 myAuto.setIndFase(rs.getString("IND_FASE"));
				 myAuto.setIndPago(rs.getString("IND_PAGO"));
				 myAuto.setIdentOrgao(rs.getString("NUM_IDENT_ORGAO"));
				 myAuto.setDscResumoInfracao(rs.getString("DSC_RESUMO_INFRACAO"));
				 myAuto.setDscMarcaModeloAparelho(rs.getString("DSC_MARCA_MODELO_APARELHO"));
				 myAuto.setDscLocalInfracaoNotificacao(rs.getString("DSC_LOCAL_INFRACAO_NOTIF"));
				 myAuto.getInfracao().setCodInfracao(rs.getString("COD_INFRACAO"));
				 myAuto.getInfracao().setDscInfracao(rs.getString("DSC_INFRACAO"));
				 myAuto.getInfracao().setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));
				 myAuto.getInfracao().setValReal(rs.getString("VAL_REAL"));
				 myAuto.getInfracao().setNumPonto(rs.getString("NUM_PONTO"));
				 myAuto.getInfracao().setDscCompetencia(rs.getString("DSC_COMPETENCIA"));
				 myAuto.getInfracao().setIndTipo(rs.getString("IND_COND_PROP"));
				 myAuto.getInfracao().setIndSDD(rs.getString("IND_SUSPENSAO"));
				 myAuto.getInfracao().setValRealDesconto(rs.getString("VAL_REAL_DESCONTO"));
				 myAuto.getVeiculo().setCodMarcaModelo(rs.getString("COD_MARCA_MODELO"));
				 myAuto.getVeiculo().setCodEspecie(rs.getString("COD_ESPECIE"));
				 myAuto.getVeiculo().setCodCategoria(rs.getString("COD_CATEGORIA"));
				 myAuto.getVeiculo().setCodTipo(rs.getString("COD_TIPO"));
				 myAuto.getVeiculo().setCodCor(rs.getString("COD_COR"));
				 myAuto.getProprietario().setNomResponsavel(rs.getString("NOM_PROPRIETARIO"));				 
				 myAuto.getProprietario().setIndCpfCnpj(rs.getString("TIP_DOC_PROPRIETARIO"));
				 myAuto.getProprietario().setNumCpfCnpj(rs.getString("NUM_DOC_PROPRIETARIO"));
				 myAuto.getProprietario().setIndTipoCnh(rs.getString("TIP_CNH_PROPRIETARIO"));
				 myAuto.getProprietario().setNumCnh(rs.getString("NUM_CNH_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setTxtEndereco(rs.getString("DSC_END_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setNumEndereco(rs.getString("NUM_END_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setTxtComplemento(rs.getString("DSC_COMP_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setCodCidade(rs.getString("COD_MUN_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setNumCEP(rs.getString("NUM_CEP_PROPRIETARIO"));
				 myAuto.getProprietario().getEndereco().setCodUF(rs.getString("UF_PROPRIETARIO"));
				 myAuto.getCondutor().setNomResponsavel(rs.getString("NOM_CONDUTOR"));
				 myAuto.getCondutor().setIndCpfCnpj(rs.getString("TIP_DOC_CONDUTOR"));
				 myAuto.getCondutor().setNumCpfCnpj(rs.getString("NUM_DOC_CONDUTOR"));
				 myAuto.getCondutor().setIndTipoCnh(rs.getString("TIP_CNH_CONDUTOR"));
				 myAuto.getCondutor().setNumCnh(rs.getString("NUM_CNH_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setTxtEndereco(rs.getString("DSC_END_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setNumEndereco(rs.getString("NUM_END_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setTxtComplemento(rs.getString("DSC_COMP_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setCodCidade(rs.getString("COD_MUN_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setNumCEP(rs.getString("NUM_CEP_CONDUTOR"));
				 myAuto.getCondutor().getEndereco().setCodUF(rs.getString("UF_CONDUTOR"));
				 myAuto.setNumNotifAut(rs.getString("NUM_NOTIF_AUTUACAO"));
				 myAuto.setDatNotifAut(converteData(rs.getDate("DAT_NOTIF_AUTUACAO")));
				 myAuto.setCodNotifAut(rs.getString("COD_ENT_AUTUACAO"));
				 myAuto.setNumARNotifAut(rs.getString("NUM_LOTE_AUTUACAO"));
				 myAuto.setNumNotifPen(rs.getString("NUM_NOTIF_PENALIDADE"));
				 myAuto.setDatNotifPen(converteData(rs.getDate("DAT_NOTIF_PENALIDADE")));
				 myAuto.setCodNotifPen(rs.getString("COD_ENT_PENALIDADE"));
				 myAuto.setNumARNotifPen(rs.getString("NUM_LOTE_PENALIDADE"));
				 myAuto.setDscAgente(rs.getString("DSC_AGENTE"));
				 myAuto.setDscUnidade(rs.getString("DSC_UNIDADE"));
				 myAuto.setDatPag(converteData(rs.getDate("DAT_PAGAMENTO")));
				 myAuto.setNumAutoOrigem(rs.getString("NUM_AUTO_ORIGEM"));
				 myAuto.setNumInfracaoOrigem(rs.getString("COD_INFRACAO_ORIGEM"));
				 myAuto.setIdentAutoOrigem(rs.getString("IND_TIPO_AUTO"));
				 myAuto.setNumInfracaoRenainf(rs.getString("NUM_AUTO_RENAINF"));
				 myAuto.setTxtEmail(rs.getString("TXT_EMAIL"));
				 
				 myAuto.setDatInclusao(converteData(rs.getDate("DAT_INCLUSAO")));
				 myAuto.setDatAlteracao(converteData(rs.getDate("DAT_ALTERACAO")));
				 myAuto.setDatRegistro(converteData(rs.getDate("DAT_REGISTRO")));
				 
				 String sqlMun = "SELECT nom_municipio from TSMI_MUNICIPIO WHERE cod_municipio = :cod_municipio";
				 PreparedStatement stmMun = conn.prepareStatement(sqlMun);
				 stmMun.setString(1, myAuto.getCodMunicipio());
				 ResultSet rsMun = stmMun.executeQuery() ;
				 while (rsMun.next()) {
					 myAuto.setNomMunicipio(rsMun.getString("nom_municipio"));	  				  	  				  
				 }
				 stmMun.close();
				 
				 myAuto.getOrgao().Le_Orgao(myAuto.getCodOrgao(),0);
				 myAuto.setSigOrgao(myAuto.getOrgao().getSigOrgao());	  				  	  				  
				 myAuto.setNomOrgao(myAuto.getOrgao().getNomOrgao());
				 
				 retorno = true;
			 }
			 stm.close();
			 
		 } catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 } finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception e) { 
					 System.out.println(e);
				 }
			 }
		 }
		 return retorno;
	 }
	 
	 
	 private java.sql.Date converteData(String data) {
		 java.sql.Date Retorno = null;
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 try {
			 Retorno = new java.sql.Date(df.parse(data).getTime());
		 } catch (Exception e) {
			 Retorno = null;
		 }
		 return Retorno;
	 }
	 private String converteData(java.sql.Date data) {
		 String Retorno = null;
		 SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		 try {
			 Retorno = df.format(data);
		 } catch (Exception e) {
			 Retorno = "";
		 }
		 return Retorno;
	 }	 
	 private void converteInt(String numero, int parametro, PreparedStatement stm) throws DaoException {
		 try {
			 stm.setInt(parametro, Integer.parseInt(numero));
		 } catch (Exception e) {
			 try {
				 stm.setNull(parametro, Types.INTEGER);
			 } catch (Exception e1) {}
		 }
	 }
	 private void converteDouble(String numero, int parametro, PreparedStatement stm) throws DaoException {
		 try {
			 stm.setDouble(parametro, Double.parseDouble(numero));
		 } catch (Exception e) {
			 try {
				 stm.setNull(parametro, Types.DOUBLE);
			 } catch (Exception e1) {}
		 }
	 }
	 
	 public  boolean LeRequerimentoLocal (RequerimentoBean myReq, Connection conn) throws sys.DaoException {
		 boolean bOk = true ;
		 try {
			 Statement stmt  = conn.createStatement();
			 String sCmd     =  "SELECT * FROM TSMI_REQUERIMENTO WHERE "+
			 "COD_AUTO_INFRACAO='"+myReq.getCodAutoInfracao()+"' and "+              
			 "NUM_REQUERIMENTO='"+myReq.getNumRequerimento()+"'";
			 ResultSet rs    = stmt.executeQuery(sCmd) ;			 
			 
			 if (rs.next())
			 {
				 myReq.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				 myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				 myReq.setCodTipoSolic(rs.getString("COD_TIPO_SOLIC"));
				 myReq.setDatRequerimento(converteData(rs.getDate("DAT_REQUERIMENTO")));
				 myReq.setCodStatusRequerimento(rs.getString("COD_STATUS_REQUERIMENTO"));
				 myReq.setNomUserNameDP(rs.getString("NOM_USERNAME_DP"));
				 myReq.setCodOrgaoLotacaoDP(rs.getString("COD_ORGAO_LOTACAO_DP"));
				 myReq.setNomResponsavelDP(rs.getString("NOM_RESP_DP"));			
				 myReq.setDatProcDP(converteData(rs.getDate("DAT_PROC_DP")));
				 myReq.setDatPJ(converteData(rs.getDate("DAT_PJ")));                
				 myReq.setCodParecerPJ(rs.getString("COD_PARECER_PJ"));
				 myReq.setCodRespPJ(rs.getString("COD_RESP_PJ"));
				 myReq.setTxtMotivoPJ(rs.getString("TXT_MOTIVO_PJ"));
				 myReq.setNomUserNamePJ(rs.getString("NOM_USERNAME_PJ"));
				 myReq.setCodOrgaoLotacaoPJ(rs.getString("COD_ORGAO_LOTACAO_PJ"));
				 myReq.setDatProcPJ(converteData(rs.getDate("DAT_PROC_PJ")));
				 myReq.setDatJU(converteData(rs.getDate("DAT_JU")));
				 myReq.setCodJuntaJU(rs.getString("COD_JUNTA_JU"));
				 myReq.setCodRelatorJU(rs.getString("COD_RELATOR_JU"));
				 myReq.setNomUserNameJU(rs.getString("NOM_USERNAME_JU"));
				 myReq.setCodOrgaoLotacaoJU(rs.getString("COD_ORGAO_LOTACAO_JU"));
				 myReq.setDatProcJU(converteData(rs.getDate("DAT_PROC_JU")));
				 myReq.setDatRS(converteData(rs.getDate("DAT_RS")));
				 myReq.setCodResultRS(rs.getString("COD_RESULT_RS"));
				 myReq.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				 myReq.setNomUserNameRS(rs.getString("NOM_USERNAME_RS"));
				 myReq.setCodOrgaoLotacaoRS(rs.getString("COD_ORGAO_LOTACAO_RS"));
				 myReq.setDatProcRS(converteData(rs.getDate("DAT_PROC_RS")));
				 myReq.setNomCondutorTR(rs.getString("NOM_CONDUTOR_TR"));
				 myReq.setIndTipoCNHTR(rs.getString("IND_TIPO_CNH_TR"));
				 myReq.setNumCNHTR(rs.getString("NUM_CNH_TR"));
				 myReq.setCodUfCNHTR(rs.getString("COD_UF_CNH_TR"));
				 myReq.setNumCPFTR(rs.getString("NUM_CPF_TR"));	
				 myReq.setDatAtuTR(converteData(rs.getDate("DAT_ATU_TR")));
				 myReq.setNomUserNameTR(rs.getString("NOM_USERNAME_TR"));
				 myReq.setCodOrgaoLotacaoTR(rs.getString("COD_ORGAO_LOTACAO_TR"));
				 myReq.setSitProtocolo(rs.getString("SIT_PROTOCOLO"));
				 myReq.setDatRecebimentoGuia(converteData(rs.getDate("DAT_RECEB_GUIA")));
				 myReq.setDatEnvioGuia(converteData(rs.getDate("DAT_ENVIO_GUIA")));
				 myReq.setNomUserNameRecebimento(rs.getString("NOM_USERNAME_RECEB_GUIA"));
				 myReq.setNomUserNameEnvio(rs.getString("NOM_USERNAME_ENVIO_GUIA"));
				 myReq.setCodRemessa(rs.getString("COD_REMESSA"));	
                 myReq.setDatEnvioDO(converteData(rs.getDate("DAT_ENVIO_PUBDO")));
                 myReq.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
                 myReq.setDatPublPDO(converteData(rs.getDate("DAT_PUBDO")));
                 myReq.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));
				 bOk = true ;
			 }
			 
			 sCmd =  "SELECT * FROM TSMI_REQUERIMENTO_WEB WHERE "+
			 "COD_REQUERIMENTO='"+myReq.getCodRequerimento()+"'";
			 rs = stmt.executeQuery(sCmd) ;			 
			 if (rs.next())
			 {
				 myReq.setNomRequerente(rs.getString("NOM_REQUERENTE"));
				 myReq.setTxtJustificativa(rs.getString("TXT_MOTIVO"));
				 myReq.setTxtEmail(rs.getString("TXT_EMAIL"));
			 }
			 
			 rs.close();
			 stmt.close();
		 }
		 catch (SQLException e) {
			 bOk = false ;
			 throw new sys.DaoException(e.getMessage());
		 }	  
		 catch (Exception ex) {
			 bOk = false ;
			 throw new sys.DaoException(ex.getMessage());
		 }
		 return bOk;
	 }
	 
	 public  boolean LeRequerimentoLocal (AutoInfracaoBean myInf, Connection conn) throws sys.DaoException {
		 boolean bOk = true ;
		 try {
			 Statement stmt  = conn.createStatement();
			 String sCmd     =  "SELECT * FROM TSMI_REQUERIMENTO WHERE "+
			 "COD_AUTO_INFRACAO='"+myInf.getCodAutoInfracao()+"' ORDER BY DAT_REQUERIMENTO DESC";
			 ResultSet rs    = stmt.executeQuery(sCmd) ;
			 List requerimentos = new ArrayList();			
			 while (rs.next())
			 {
				 RequerimentoBean myReq = new RequerimentoBean();				
				 myReq.setNumRequerimento(rs.getString("NUM_REQUERIMENTO"));
				 myReq.setCodRequerimento(rs.getString("COD_REQUERIMENTO"));
				 myReq.setCodTipoSolic(rs.getString("COD_TIPO_SOLIC"));				 
				 myReq.setDatRequerimento(converteData(rs.getDate("DAT_REQUERIMENTO")));
				 myReq.setCodStatusRequerimento(rs.getString("COD_STATUS_REQUERIMENTO"));
				 myReq.setNomUserNameDP(rs.getString("NOM_USERNAME_DP"));
				 myReq.setCodOrgaoLotacaoDP(rs.getString("COD_ORGAO_LOTACAO_DP"));
				 myReq.setNomResponsavelDP(rs.getString("NOM_RESP_DP"));			
				 myReq.setDatProcDP(converteData(rs.getDate("DAT_PROC_DP")));
				 myReq.setDatPJ(converteData(rs.getDate("DAT_PJ")));                
				 myReq.setCodParecerPJ(rs.getString("COD_PARECER_PJ"));
				 myReq.setCodRespPJ(rs.getString("COD_RESP_PJ"));
				 myReq.setTxtMotivoPJ(rs.getString("TXT_MOTIVO_PJ"));
				 myReq.setNomUserNamePJ(rs.getString("NOM_USERNAME_PJ"));
				 myReq.setCodOrgaoLotacaoPJ(rs.getString("COD_ORGAO_LOTACAO_PJ"));
				 myReq.setDatProcPJ(converteData(rs.getDate("DAT_PROC_PJ")));
				 myReq.setDatJU(converteData(rs.getDate("DAT_JU")));
				 myReq.setCodJuntaJU(rs.getString("COD_JUNTA_JU"));
				 myReq.setCodRelatorJU(rs.getString("COD_RELATOR_JU"));
				 myReq.setNomUserNameJU(rs.getString("NOM_USERNAME_JU"));
				 myReq.setCodOrgaoLotacaoJU(rs.getString("COD_ORGAO_LOTACAO_JU"));
				 myReq.setDatProcJU(converteData(rs.getDate("DAT_PROC_JU")));
				 myReq.setDatRS(converteData(rs.getDate("DAT_RS")));
				 myReq.setCodResultRS(rs.getString("COD_RESULT_RS"));
				 myReq.setTxtMotivoRS(rs.getString("TXT_MOTIVO_RS"));
				 myReq.setNomUserNameRS(rs.getString("NOM_USERNAME_RS"));
				 myReq.setCodOrgaoLotacaoRS(rs.getString("COD_ORGAO_LOTACAO_RS"));
				 myReq.setDatProcRS(converteData(rs.getDate("DAT_PROC_RS")));
				 myReq.setNomCondutorTR(rs.getString("NOM_CONDUTOR_TR"));
				 myReq.setIndTipoCNHTR(rs.getString("IND_TIPO_CNH_TR"));
				 myReq.setNumCNHTR(rs.getString("NUM_CNH_TR"));
				 myReq.setCodUfCNHTR(rs.getString("COD_UF_CNH_TR"));
				 myReq.setNumCPFTR(rs.getString("NUM_CPF_TR"));	
				 myReq.setDatAtuTR(converteData(rs.getDate("DAT_ATU_TR")));
				 myReq.setNomUserNameTR(rs.getString("NOM_USERNAME_TR"));
				 myReq.setCodOrgaoLotacaoTR(rs.getString("COD_ORGAO_LOTACAO_TR"));
				 myReq.setSitProtocolo(rs.getString("SIT_PROTOCOLO"));
				 myReq.setDatRecebimentoGuia(converteData(rs.getDate("DAT_RECEB_GUIA")));
				 myReq.setDatEnvioGuia(converteData(rs.getDate("DAT_ENVIO_GUIA")));
				 myReq.setNomUserNameRecebimento(rs.getString("NOM_USERNAME_RECEB_GUIA"));
				 myReq.setNomUserNameEnvio(rs.getString("NOM_USERNAME_ENVIO_GUIA"));
				 myReq.setCodRemessa(rs.getString("COD_REMESSA"));	
                 myReq.setCodEnvioDO(rs.getString("COD_ENVIO_PUBDO"));
                 myReq.setDatEnvioDO(rs.getString("DAT_ENVIO_PUBDO"));
                 myReq.setDatPublPDO(rs.getString("DAT_PUBDO"));
                 myReq.setCodMotivoDef(rs.getString("COD_MOTIVO_DEFERIDO"));
				 requerimentos.add(myReq) ;
			 }
			 myInf.setRequerimentos(requerimentos);
			 rs.close();
			 stmt.close();
		 }
		 catch (SQLException e) {
			 bOk = false ;
			 throw new sys.DaoException(e.getMessage());
		 }	  
		 catch (Exception ex) {
			 bOk = false ;
			 throw new sys.DaoException(ex.getMessage());
		 }
		 return bOk;
	 }

	 public List getAutosPendentes(String Placa,String Auto ) throws DaoException {
		 Connection conn =null ;	
		 List pendentes = new ArrayList();
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;
			 Statement stmt = conn.createStatement();
			 Statement stmt1 = conn.createStatement();
			 String sCmd = "select a.cod_arquivo,a.cod_ident_arquivo,a.cod_orgao,l.dsc_linha_arq_rec,l.num_auto_infracao,l.cod_linha_arq_rec from tsmi_arquivo_recebido a,tsmi_linha_arquivo_rec l ";
			 sCmd +="where a.cod_arquivo = l.cod_arquivo ";	   
			 if(!"".equals(Auto))
				 sCmd += "and l.NUM_AUTO_INFRACAO ='"+Auto+"' "; 	
			 if(!"".equals(Placa))
				 sCmd += "and l.NUM_PLACA ='"+Placa+"' "; 				   
			 sCmd += "AND l.COD_RETORNO <>'000' ";
			 sCmd += "AND a.cod_ident_arquivo='MR01' ";		
			 ResultSet rs = stmt.executeQuery(sCmd); 
			 while( rs.next() ) {
				 AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	
				 String linha = rs.getString("DSC_LINHA_ARQ_REC");
				 myAuto.setNumAutoInfracao(linha.substring(1,13));
				 myAuto.setNumPlaca(linha.substring(13,20));
				 myAuto.setDscLocalInfracao(linha.substring(27,72));
				 String datinfracao=linha.substring(72,80);
				 if(!"00000000".equals(datinfracao))
					 myAuto.setDatInfracao(linha.substring(72,74)+"/"+linha.substring(74,76)+"/"+linha.substring(76,80));
				 myAuto.setValHorInfracao(linha.substring(80,84));
				 String datvencimento=linha.substring(99,107);
				 if(!"00000000".equals(datvencimento))
					 myAuto.setDatVencimento(linha.substring(99,101)+"/"+linha.substring(101,103)+"/"+linha.substring(103,107));
				 myAuto.getInfracao().setCodInfracao(linha.substring(88,91));
				 if (!"".equals(myAuto.getInfracao().getCodInfracao())){
					 List dsc_infracao =CarregaDescInfracao(myAuto.getInfracao().getCodInfracao(),rs.getString("cod_orgao"), conn);
					 
					 Iterator it = dsc_infracao.iterator() ;
					 REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
					 while (it.hasNext()) {						 
						 not   = (REC.AutoInfracaoBean)it.next() ;			
						 
						 myAuto.getInfracao().setDscInfracao(not.getInfracao().getDscInfracao());
						 myAuto.getInfracao().setDscEnquadramento(not.getInfracao().getDscEnquadramento());
						 myAuto.getInfracao().setValReal(not.getInfracao().getValReal());
						 myAuto.getInfracao().setNumPonto(not.getInfracao().getNumPonto());
						 myAuto.setSigOrgao(not.getSigOrgao());
						 
					 }
				 }
				 
				 pendentes.add(myAuto) ;
			 }    	
			 
			 
		 }catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return pendentes;
		 
	 }	
	 public List getAutosPendentesDol(String Placa,String Auto ) throws DaoException {
		 Connection conn =null ;	
		 List pendentesDol = new ArrayList();
		 try {
			 conn = serviceloc.getConnection(MYABREVSIST) ;					
			 Statement stmt = conn.createStatement();
			 
			 ACSS.ParamSistemaBean ParamSistemaBeanId = new ACSS.ParamSistemaBean();
			 ParamSistemaBeanId.setCodSistema("1"); // M�dulo ACSS
			 ParamSistemaBeanId.PreparaParam();
			 String sHistorico=ParamSistemaBeanId.getParamSist("BD_BKP_UP");
			 if (MYABREVSIST.equals("GER")) sHistorico="S";
			 
			 
			 String  sCmd = "select l.num_auto_infracao,l.dsc_linha_lote,a.cod_orgao  from tsmi_linha_lote l,tsmi_arquivo_dol a ,tsmi_lote_dol d ";
			 sCmd +="where a.cod_arquivo_dol=d.cod_arquivo_dol ";	
			 sCmd += "and d.cod_lote=l.cod_lote ";				   
			 if(!"".equals(Auto))
				 sCmd += "and l.NUM_AUTO_INFRACAO ='"+Auto+"' "; 	
			 if(!"".equals(Placa))
				 sCmd += "and l.NUM_PLACA ='"+Placa+"' "; 
			 sCmd += "and l.cod_retorno <> '000' ";
			 sCmd += "and a.cod_ident_arquivo = 'DE01' ";
			 
			 if (sHistorico.equals("S")) {
				 sCmd += "UNION ";				 
				 sCmd += "select l.num_auto_infracao,l.dsc_linha_lote,a.cod_orgao  from tsmi_linha_lote_BKP@SMITPBKP l,tsmi_arquivo_dol_BKP@SMITPBKP a ,tsmi_lote_dol_BKP@SMITPBKP d ";
				 sCmd +="where a.cod_arquivo_dol=d.cod_arquivo_dol ";	
				 sCmd += "and d.cod_lote=l.cod_lote ";				   
				 if(!"".equals(Auto))
					 sCmd += "and l.NUM_AUTO_INFRACAO ='"+Auto+"' "; 	
				 if(!"".equals(Placa))
					 sCmd += "and l.NUM_PLACA ='"+Placa+"' "; 
				 sCmd += "and l.cod_retorno <> '000' ";
				 sCmd += "and a.cod_ident_arquivo = 'DE01'";
			 }

			 
			 
			 
			 ResultSet rs = stmt.executeQuery(sCmd); 
			 
			 
			 while( rs.next() ) {
				 AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	
				 String linha = rs.getString("dsc_linha_lote");
				 
				 myAuto.setNumAutoInfracao(linha.substring(41,53));
				 myAuto.setNumPlaca(linha.substring(53,60));
				 myAuto.setDscLocalInfracao(linha.substring(67,112));
				 
				 String datinfracao=linha.substring(112,120);
				 if(!"00000000".equals(datinfracao))
					 myAuto.setDatInfracao(linha.substring(112,114)+"/"+linha.substring(114,116)+"/"+linha.substring(116,120));
				 myAuto.setValHorInfracao(linha.substring(120,124));
				 
				 String datvencimento=linha.substring(139,147);
				 if(!"00000000".equals(datvencimento))
					 myAuto.setDatVencimento(linha.substring(139,141)+"/"+linha.substring(141,143)+"/"+linha.substring(143,147));
				 else myAuto.setDatVencimento(datvencimento);
				 
				 myAuto.getInfracao().setCodInfracao(linha.substring(128,131));
				 
				 if (!"".equals(myAuto.getInfracao().getCodInfracao())){
					 List dsc_infracao =CarregaDescInfracao(myAuto.getInfracao().getCodInfracao(),rs.getString("cod_orgao"), conn);
					 
					 Iterator it = dsc_infracao.iterator() ;
					 REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
					 while (it.hasNext()) {						 
						 not   = (REC.AutoInfracaoBean)it.next() ;			
						 
						 myAuto.getInfracao().setDscInfracao(not.getInfracao().getDscInfracao());
						 myAuto.getInfracao().setDscEnquadramento(not.getInfracao().getDscEnquadramento());
						 myAuto.getInfracao().setValReal(not.getInfracao().getValReal());
						 myAuto.getInfracao().setNumPonto(not.getInfracao().getNumPonto());
						 myAuto.setSigOrgao(not.getSigOrgao());
						 
					 }
				 }
				 
				 pendentesDol.add(myAuto) ;
			 }    	
			 
			 rs.close();
			 stmt.close();
		 }
		 catch (Exception e) {
			 throw new DaoException(e.getMessage());
		 }
		 finally {
			 if (conn != null) {
				 try { serviceloc.setReleaseConnection(conn); }
				 catch (Exception ey) { }
			 }
		 }
		 return pendentesDol;
		 
	 }	
	 
	 public List CarregaDescInfracao(String codinfracao,String codorgao,Connection conn) throws DaoException, SQLException, BeanException {
		 List dscinfracao=new ArrayList();
		 
		 Statement stmt = conn.createStatement(); 	
		 Statement stmt1 = conn.createStatement(); 	
		 ResultSet rs1 =null;
		 
		 String sCmd = "select dsc_infracao,dsc_enquadramento,val_real,num_ponto from tsmi_infracao ";		
		 sCmd += "WHERE COD_INFRACAO = '"+codinfracao+"'";		
		 
		 ResultSet rs = stmt.executeQuery(sCmd); 		
		 
		 while( rs.next() ) {
			 AutoInfracaoBean myAuto = new AutoInfracaoBean() ;	
			 myAuto.getInfracao().setDscInfracao(rs.getString("dsc_infracao"));
			 myAuto.getInfracao().setDscEnquadramento(rs.getString("dsc_enquadramento"));
			 myAuto.getInfracao().setValReal(rs.getString("val_real"));
			 myAuto.getInfracao().setNumPonto(rs.getString("num_ponto"));
			 
			 sCmd = "select sig_orgao from tsmi_orgao ";		
			 sCmd += "WHERE COD_orgao = ' "+codorgao+"'";					   
			 
			 rs1 = stmt1.executeQuery(sCmd);
			 while( rs1.next() ) {
				 myAuto.setSigOrgao(rs1.getString("sig_orgao"));			 	   
			 } 
			 dscinfracao.add(myAuto) ;
		 }
		 rs.close();
		 stmt.close();
		 rs1.close();
		 stmt1.close();
		 
		 
		 return dscinfracao;
	 }
	 
     public void LeInfracao(InfracaoBean infracaoBean, String tpChave) throws DaoException {
         Connection conn =null ;    
         List infracoes = new ArrayList();
         try {
             conn = serviceloc.getConnection(MYABREVSIST) ;                 
             Statement stmt = conn.createStatement();
             
             
             String  sCmd = "SELECT * FROM TSMI_INFRACAO ORDER BY ";
             if (tpChave.equals("codigo"))
                 sCmd += " COD_INFRACAO ";
             else if (tpChave.equals("enquadramento"))
                 sCmd += " DSC_ENQUADRAMENTO ";
            
             ResultSet rs = stmt.executeQuery(sCmd); 
             while( rs.next() ) {
                 InfracaoBean InfracaobeanResult = new  InfracaoBean();
                 InfracaobeanResult.setCodInfracao(rs.getString("COD_INFRACAO"));
                 InfracaobeanResult.setDscInfracao(rs.getString("DSC_INFRACAO"));
                 InfracaobeanResult.setDscEnquadramento(rs.getString("DSC_ENQUADRAMENTO"));
                 InfracaobeanResult.setValReal(rs.getString("VAL_REAL"));
                 InfracaobeanResult.setNumPonto(rs.getString("NUM_PONTO"));
                 InfracaobeanResult.setDscCompetencia(rs.getString("DSC_COMPETENCIA"));
                 InfracaobeanResult.setIndTipo(rs.getString("IND_COND_PROP"));
                 InfracaobeanResult.setDscGravidade(rs.getString("DSC_GRAVIDADE"));
                 InfracaobeanResult.setValRealDesconto(rs.getString("VAL_REAL_DESCONTO"));
                 infracoes.add(InfracaobeanResult);
             }      
             infracaoBean.setInfracoes(infracoes);
             rs.close();
             stmt.close();
         }
         catch (Exception e) {
             throw new DaoException(e.getMessage());
         }
         finally {
             if (conn != null) {
                 try { serviceloc.setReleaseConnection(conn); }
                 catch (Exception ey) { }
             }
         }
     }	 
	          
	 public boolean VerificaDt(String codTransacao,String resultado,BRK.TransacaoBRKR transBRK) throws DaoException 
	 {     
		 String codRetorno = resultado.substring(0,3);		 
		 if (codTransacao.equals("041")) 
		 {
			 /*Data Infra��o*/
			 setPosIni(205);
		 	 String sDataInfracao=sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8));
		 	 if ((sDataInfracao.length()>0) && codRetorno.equals("000") ) {
		 		 if (Util.ValidaData(sDataInfracao).length()==0) {
		 			 transBRK.GravarBroker(resultado,codRetorno);
		 			 System.out.println("* Retorno Inv�lido :"+resultado);
		 			 //return false;
		 		 }
		 	 }

		 	 /*Data Aferi��o Aparelho*/
			 setPosIni(275);
		 	 String sDataAfericaoAper=sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8));
		 	 if ((sDataAfericaoAper.length()>0) && codRetorno.equals("000") ) {
		 		 if (Util.ValidaData(sDataAfericaoAper).length()==0) {
		 			 transBRK.GravarBroker(resultado,codRetorno);
		 			 System.out.println("* Retorno Inv�lido :"+resultado);		 			 
		 			 //return false;
		 		 }
		 	 }
			 
		 	 /*Data Vencimento*/
			 setPosIni(332);
		 	 String sDataVencto=sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8));
		 	 if ((sDataVencto.length()>0) && codRetorno.equals("000") ) {
		 		 if (Util.ValidaData(sDataVencto).length()==0) {
		 			 transBRK.GravarBroker(resultado,codRetorno);
		 			 System.out.println("* Retorno Inv�lido :"+resultado);		 			 
		 			 //return false;
		 		 }
		 	 }
			 
		 	 /*Data Status*/
		 	 setPosIni(715);
		 	 String sDataStatus=sys.Util.formataDataDDMMYYYY(getPedacoDt(resultado,8));
		 	 if ((sDataStatus.length()>0) && codRetorno.equals("000") ) {
		 		 if (Util.ValidaData(sDataStatus).length()==0) {
		 			 transBRK.GravarBroker(resultado,codRetorno);
		 			 System.out.println("* Retorno Inv�lido :"+resultado);		 			 
		 			 //return false;
		 		 }
		 	 }
		 }
	 	 return true;
	 }

	@Override
	public void atualizarAuto206(AutoInfracaoBean myAuto, ParamOrgBean myParam,
			RequerimentoBean myReq, HistoricoBean myHis, UsuarioBean myUsuario,
			String ata) throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void atualizarAuto261(RequerimentoBean myReq, HistoricoBean myHis,
			AutoInfracaoBean myAuto, UsuarioBean myUsuario) throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void atualizarAuto263(RequerimentoBean myReq, HistoricoBean myHis,
			AutoInfracaoBean myAuto, UsuarioBean myUsuario) throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ajustarManualRespPecunia(AutoInfracaoBean myAuto,
			UsuarioBean myUsrLog, RequerimentoBean myReq, HistoricoBean myHis)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void atualizarAuto209(RequerimentoBean myReq, HistoricoBean myHis,
			AutoInfracaoBean myAuto, UsuarioBean myUsuario, String txtAta)
			throws DaoException {
		// TODO Auto-generated method stub
		
	}
	 
	 /*	  
	  200         - Registro - Historico
	  203,323     - Trocou para 406  
	  204,324     - Receber Notificacao - Historico
	  206,326,334 - Abrir Recurso - Historico
	  207         - Parecer Juridico - Historico
	  208,328,335 - Troca de Junta/Relator - Historico
	  209,329,336 - Resultado - Historico
	  210,330,337 - Estorno de Resultado - Historico	
	  211,331,338 - Envio para DO - DESCONTINUADO
	  212,332,339 - Excluir Auto do Relatorio enviado para DO - DESCONTINUADO 
	  213,333,340 - Atualiza data de Publicacao DO - DESCONTINUADA
	  214         - TRI - Historico
	  215         - Estorno de Junta/Relator
	  226,350,351 - Estorno de Abrir Recurso - Historico
	  227         - Rejeito
	  352         - Estorno de TRI - Historico
	  236,356,364 - Abrir Recurso - Indeferimento de Plano
	  237,357,365 - Estorno de Abrir Recurso - Indeferimento de Plano
	  238         - Renuncia de Defesa Previa
	  239         - Estorno de Renuncia de Defesa Previa
	  261,381,388 - Envio para DO - Por Requerimento
	  262,382,389 - Excluir Auto do Relatorio enviado para DO - Por Requerimento
	  263,383,390 - Atualiza data de Publicacao DO - Por Requerimento
	  264,384,391 - Revisao do requerimento
	  
	  405         - Cancelamento/Suspensao de Autos
	  406         - Retorno VEX
	  407         - Atualiza Mov Bancario
	  408         - Digitacao via SMIT
	  410         - Atualiza Processo - Historico
	  411         - Ajuste de Status - Historico
	  412         - Ajuste Automatico
	  413         - Ajuste Manual do Responsavel pelos pontos
	  414         - Ajuste de CPF
	  
	  
	  C�DIGO	DESCRI��O
	  041	CONSULTA AUTO DE INFRA��O - DAO BROKER
	  042	CONSULTA QUANTIDADE DE AUTOS
	  043	CONSULTA AUTOS EM DETERMINADOS STATUS
	  044	CONSULTAR HIST�RICO - DAO BROKER
	  045	CONSULTAR REQUERIMENTOS - DAO BROKER
	  046	CONSULTA AUTOS COM DEF PR�VIA ENCERRADA
	  047	CONSULTA RELAT�RIO DE PUBLICA��O NO D.O.
	  048	CONSULTA AUTO DE INFRA��O POR PER�ODO - DAO BROKER
	  049	CONSULTA REQUERIMENTO EM DETERMINADO STATUS
	  
	  051	CONSULTA CONDUTOR - DAO BROKER	
	  
	  081	CONSULTA / ATUALIZACAO TABELA DE INFRACOES (*) - ROBOT TABELAS
	  082	CONSULTA / ATUALIZACAO TABELA DE UNIDADES (*) - ROBOT TABELAS
	  083	CONSULTA / ATUALIZACAO TABELA DE ORGAOS (*) - ROBOT TABELAS
	  084	CONSULTA / ATUALIZACAO TABELA DE AGENTES (*) - ROBOT TABELAS
	  085	CONSULTA / ATUALIZACAO TABELA DE UFS (*) - ROBOT TABELAS
	  086	CONSULTA / ATUALIZACAO  TABELA DE JUNTAS - ROBOT TABELAS
	  087	CONSULTA / ATUALIZACAO TABELA DE RELATORES - ROBOT TABELAS
	  088	CONSULTA / ATUALIZACAO TABELA RESP. (PARC. JUR.) - ROBOT TABELAS
	  089	CONSULTA / ATUALIZACAO TABELA DE PARAMETROS - ROBOT TABELAS
	  090	CONSULTA / ATUALIZACAO TABELA DE STATUS - ROBOT TABELAS
	  091	CONSULTA / ATUALIZACAO TABELA DE C�DIGOS DE RETORNO VEX - ROBOT TABELAS
	  092	CONSULTA / ATUALIZACAO TABELA DE EVENTOS - ROBOT TABELAS
	  093	CONSULTA / ATUALIZACAO TABELA DE ORIGEM DO EVENTO - ROBOT TABELAS
	  094	CONSULTA / ATUALIZACAO TABELA DE STATUS DO REQUERIMENTO - ROBOT TABELAS
	  121	CONSULTA TABELA DE MARCAS - ROBOT TABELAS
	  122	CONSULTA TABELA DE TIPOS - ROBOT TABELAS
	  123	CONSULTA TABELA DE MUNICIPIOS - ROBOT TABELAS
	  124	CONSULTA TABELA DE CORES - ROBOT TABELAS
	  125	CONSULTA TABELA DE ESPECIES - ROBOT TABELAS
	  126	CONSULTA TABELA DE CATEGORIAS - ROBOT TABELAS	  
	  **/
	  
      
      
}