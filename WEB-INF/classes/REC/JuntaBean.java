package REC;

import java.util.List;
import java.util.Vector;

import sys.Util;

/**
* <b>Title:</b>        	SMIT-REC - Bean de Junta <br>
* <b>Description:</b>  	Bean dados das Juntas - Tabela de Junta<br>
* <b>Copyright:</b>    	Copyright (c) 2004<br>
 * <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author  				Luciana Rocha
* @version 				1.0
*/

public class JuntaBean extends sys.HtmlPopupBean { 

	private String codJunta;
	private String sigJunta;
	private String nomJunta;
	private String indTipo;
	
	private String codOrgao;
	
	private String atualizarDependente;
	private Vector relator;
	private Vector relatorGravado;
	private Vector respParecer;
	private Vector respParecerGravado;


	public JuntaBean(){

	  	super();
	  	setTabela("TSMI_JUNTA");	
	  	setPopupWidth(35);
	  	codJunta = "";	  	
		nomJunta = "";
		sigJunta = "";
		indTipo	 = "0";			
		codOrgao = "";
		
		relator = new Vector();
		relatorGravado = new Vector();
		respParecer = new Vector(); 
		respParecerGravado = new Vector(); 
	}


	public void setCodJunta(String codJunta)  {
		this.codJunta=codJunta ;
		if (codJunta==null) 
		 this.codJunta="" ;
	}
	public String getCodJunta()  {
  		return this.codJunta;
  	}
	
	
	public void setSigJunta(String sigJunta){
		this.sigJunta = sigJunta;
		if (sigJunta == null)
		 this.sigJunta = "";
	}
	public String getSigJunta()  {
  		return this.sigJunta;
  	}
	
	
	public void setNomJunta(String nomJunta)  {
		this.nomJunta = nomJunta ;
		if (nomJunta == null)
		 this.nomJunta="" ;
	}  
	public String getNomJunta()  {
  		return this.nomJunta;
  	}
	
  	public void setIndTipo(String indTipo){
  		this.indTipo = indTipo;
  		if (indTipo == null)
  		 this.indTipo = "0";
  	}
  	public String getIndTipo()  {
  		return this.indTipo;
  	}
	
	public void setCodOrgao(String codOrgao)  {
		this.codOrgao=codOrgao ;
		if (codOrgao==null) 
		 this.codOrgao="" ;
	}  
	public String getCodOrgao()  {
  		return this.codOrgao;
  	}
	

  	public void setAtualizarDependente(String atualizarDependente)   {
  		this.atualizarDependente = atualizarDependente ;
  	}
  	public String getAtualizarDependente() {
	    return this.atualizarDependente; 
	}
 
	public void Le_Junta(String tpsist, int pos)  {
     try { 
	       DaoTabelas daoTabela = DaoTabelas.getInstance();
 			if (pos==0) 
			   this.codJunta=tpsist;		 	
			else 
			   this.nomJunta=tpsist;	
    	   if ( daoTabela.JuntaLeBean(this, pos) == false) {
			    this.codJunta	  = "0";
				setPkid("0") ;		
           }
     }// fim do try
     catch (Exception e) {
          	this.codJunta = "0" ;
			setPkid("0") ;		        			
     }
   }

	public String getJunta(){
    	String myJunta = "" ; 
     	try    {
        	DaoTabelas daoTabela = DaoTabelas.getInstance();
        	myJunta = daoTabela.getJunta() ;
			
  		}
   		catch(Exception e){
   		}	
   		return myJunta ;
 	}
	
	public String getJunta(ACSS.UsuarioBean UsrLogado ){
    	String myJunta = "" ; 
     	try    {
        	DaoTabelas daoTabela = DaoTabelas.getInstance();
        	myJunta = daoTabela.getJunta(UsrLogado ) ;
			
  		}
   		catch(Exception e){
   		}	
   		return myJunta ;
 	}
	
	
/*--------------------------------------------------------------
*   C�digo relativo a Relator
*---------------------------------------------------------------
*/

  	public Vector getRelator(int min,int livre)  {
       try  {
 	
  			DaoTabelas daoTabela = DaoTabelas.getInstance();
  			daoTabela.JuntaGetRelator(this);
     		if (this.relator.size()<min-livre) 
  				livre = min-relator.size() ;	  	
    			for (int i=0;i<livre; i++) 	{
    				this.relator.addElement(new RelatorBean()) ;
     		}
       }
       catch (Exception e) {
            setMsgErro("Junta007 - Leitura: " + e.getMessage());
       }
   	  return this.relator ;
    }

  	/*--------------------------------------------------------------
  	*   C�digo relativo a Relator Broker
  	*---------------------------------------------------------------
  	*/

  	  	public Vector getRelatorBroker(int min,int livre)  {
	       	Vector vErro = new Vector();
  	  		try  {
  	  			DaoTabelas daoTabela = DaoTabelas.getInstance();
  	 // 			daoTabela.JuntaGetRelatorBroker(this); 
  	     		if (this.relator.size()<min-livre) 
  	  				livre = min-relator.size() ;	  	
  	    			for (int i=0;i<livre; i++) 	{
  	    				this.relator.addElement(new RelatorBean()) ;
  	     		}
  	       }
  	       catch (Exception e) {
  	            setMsgErro("Junta007 - Leitura: " + e.getMessage());
  	       }
  	       setMsgErro(vErro);
  	   	  return this.relator ;
  	    }
  
  	public Vector getRelator()  {
       return this.relator;
  	}   
  
  	public RelatorBean getRelator(int i)  { 
   	   return (RelatorBean)this.relator.elementAt(i);
    }
 
  	public void setRelator(Vector relator)  {
    	this.relator = relator ;
  	}  
 
	public void setRelatorGravado(Vector relatorGravado)  {
		this.relatorGravado = relatorGravado ;
	}  
	public Vector getRelatorGravado()  {
	   return this.relatorGravado;
	}   
   
 

/*--------------------------------------------------------------
*   C�digo relativo a Responsavel Parecer
*---------------------------------------------------------------
*/

/*  	public Vector getRespParecer(int min,int livre)  {
   		Vector vErro = new Vector();
  	   try  {
  			DaoTabelas daoTabela = DaoTabelas.getInstance();
  			daoTabela.JuntaGetRespParecer(this);
     		if (this.respParecer.size()<min-livre) 
  				livre = min-respParecer.size() ;	  	
    			for (int i=0;i<livre; i++) 	{
    				this.respParecer.addElement(new RespJuridicoBean()) ;
     		}
       }
       catch (Exception e) {
            setMsgErro("Junta007 - Leitura: " + e.getMessage());
       }
       setMsgErro(vErro);
   	  return this.respParecer ;
    }*/
  	
  	/*--------------------------------------------------------------
  	*   C�digo relativo a Responsavel Parecer
  	*---------------------------------------------------------------
  	*/

  	  	public Vector getRespParecerBroker(int min,int livre)  {
  	   		Vector vErro = new Vector();
  	  	   try  {
  	  			DaoTabelas daoTabela = DaoTabelas.getInstance();
  	//  			daoTabela.JuntaGetRespParecerBroker(this,vErro);
  	     		if (this.respParecer.size()<min-livre) 
  	  				livre = min-respParecer.size() ;	  	
  	    			for (int i=0;i<livre; i++) 	{
  	    				this.respParecer.addElement(new RespJuridicoBean()) ;
  	     		}
  	       }
  	       catch (Exception e) {
  	            setMsgErro("Junta007 - Leitura: " + e.getMessage());
  	       }
  	       setMsgErro(vErro);
  	   	  return this.respParecer ;
  	    }
  
  	public Vector getRespParecer()  {
       return this.respParecer;
  	}   
  
  	public RespJuridicoBean getRespParecer(int i)  { 
   	   return (RespJuridicoBean)this.respParecer.elementAt(i);
    }
 
  	public void setRespParecer(Vector respParecer)  {
    		this.respParecer = respParecer ;
  	}  
  	
	public void setRespParecerGravado(Vector respParecerGravado)  {
			this.respParecerGravado = respParecerGravado ;
	}  
	public Vector getRespParecerGravado()  {
	   return this.respParecerGravado;
	}   
  	
    /**-------------------------------------------------------------------
    /*  C�digo Relativo a Junta (em REC)
    /*--------------------------------------------------------------------
    **/
    
	public Vector getJunta(int min,int livre)  {
		Vector myJunta = null;
		try  {
			
			DaoTabelas daoTabela = DaoTabelas.getInstance();

			if( (myJunta = daoTabela.OrgaoGetJunta(codOrgao)) == null ){
				setMsgErro("Junta009 - Erro de Leitura: ");
				myJunta = new Vector();
			}
			if (myJunta.size()<min-livre) 
				livre = min-myJunta.size();	  	
			for (int i=0;i<livre; i++){
				myJunta.addElement(new REC.JuntaBean());
			}
		}
		catch (Exception e){
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta;
	}

	
    /**-------------------------------------------------------------------
    /*  C�digo Relativo a Junta (em REC)
    /*--------------------------------------------------------------------
    **/
    
	public Vector getJunta(String codOrgao)  {
		Vector myJunta = null;
		try  {
			
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if( (myJunta = daoTabela.OrgaoGetJunta(codOrgao)) == null )
				setMsgErro("Junta009 - Erro de Leitura Smit: ");
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
 
	 /**-------------------------------------------------------------------
	  C�digo Relativo a Junta Broker
	  --------------------------------------------------------------------**/

	public List getJuntaBroker()  {
		List myJunta = null;
		try  { 
			DaoTabelas daoTabela = DaoTabelas.getInstance();
			if( (myJunta = daoTabela.OrgaoGetJuntaBroker(codOrgao)) == null )
				setMsgErro("Junta009 - Erro de Leitura Broker: ");
		}
		catch (Exception e) {
			setMsgErro("Junta007 - Leitura: " + e.getMessage());
		}//fim do catch
		return myJunta ;
	}
	
	public boolean compareSmitBroker(List juntaSmit, List juntaBroker,Vector errosPosicoes){
		JuntaBean junta, aux_junta;
		boolean flag = true;
		if( (juntaSmit == null) || (juntaBroker == null) )
			return false;
		
		for(int pos=0;pos<juntaSmit.size();pos++,flag=true){
			junta = (JuntaBean)juntaSmit.get(pos);
			for(int i= 0;i<juntaBroker.size();i++){
				aux_junta = (JuntaBean)juntaBroker.get(i);
				if(Util.lPad(junta.getCodJunta(),"0",6).equals(aux_junta.getCodJunta())){
					if( !(Util.rPad(junta.getNomJunta()," ",20).equals(aux_junta.getNomJunta())) || !(Util.rPad(junta.getSigJunta()," ",10).equals(aux_junta.getSigJunta())) || !(junta.getIndTipo().equals(aux_junta.getIndTipo())) )
						errosPosicoes.add(""+pos);
					if(pos != i){
						junta = (JuntaBean)juntaBroker.set(pos,aux_junta);
						juntaBroker.set(i,junta);
					}
					flag=false;
					break;
				}
			}
			if(flag){
				if(pos>=juntaBroker.size())
					continue;
				junta = (JuntaBean)juntaBroker.set(pos,new JuntaBean());
				juntaBroker.add(junta);
			}
		}
		if( juntaSmit.size() > juntaBroker.size() ){
			for(int pos = juntaBroker.size(); pos < juntaSmit.size(); pos++)
				juntaBroker.add(new JuntaBean());
		}
		if( juntaSmit.size() < juntaBroker.size() ){
			for(int pos = juntaSmit.size(); pos < juntaBroker.size(); pos++)
				juntaSmit.add(new JuntaBean());
		}
		return true;
	}
	
	  public boolean isInsertJuntaBroker(List juntaSmit, List juntaSmit_aux, List juntaBroker, List juntaBroker_aux)   {
	  	boolean retorno = true;
	  	Vector vErro = new Vector() ;
	  	
	  	JuntaBean myJuntaSmit, myJuntaSmit_aux, myJuntaBroker, myJuntaBroker_aux;
	  	// Verificada a valida��o dos dados nos campos
	  	try  {
	  		REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
	  		for(int pos=0; pos < juntaSmit.size();pos++){
	  			myJuntaSmit       = (JuntaBean)juntaSmit.get(pos);
	  			myJuntaSmit_aux   = (JuntaBean)juntaSmit_aux.get(pos);
	  			myJuntaBroker     = (JuntaBean)juntaBroker.get(pos);
	  			myJuntaBroker_aux = (JuntaBean)juntaBroker_aux.get(pos);	  		
	  			if( (myJuntaSmit.getNomJunta().equals(myJuntaSmit_aux.getNomJunta())) && (myJuntaSmit.getIndTipo().equals(myJuntaSmit_aux.getIndTipo())) && (myJuntaBroker.getNomJunta().equals(myJuntaBroker_aux.getNomJunta())) && (myJuntaBroker.getIndTipo().equals(myJuntaBroker_aux.getIndTipo())) )
	  				continue;
	  			 
	  			if( (myJuntaSmit.getCodJunta().equals("")) && (myJuntaSmit.getNomJunta().equals("")) )
	  				continue;
	  			if( (!myJuntaSmit.getCodJunta().equals("")) && (!myJuntaSmit.getNomJunta().equals("")) && (!myJuntaBroker.getCodJunta().equals("")) ){
	  				if( daoTabela.JuntaUpdateBroker(myJuntaBroker) )
	  				{
	  					if( !daoTabela.JuntaUpdate(myJuntaSmit) ){
	  						vErro.add("Erro: 001 | Altera��o no Smit Junta: "+myJuntaSmit.getNomJunta()+ "\n");
	  						daoTabela.JuntaUpdateBroker(myJuntaBroker_aux);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 001 | Altera��o no Detran Junta: "+myJuntaBroker.getNomJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getCodJunta().equals("")) && (!myJuntaSmit.getNomJunta().equals("")) && (!myJuntaBroker.getCodJunta().equals("")) ){
	  				if( daoTabela.JuntaUpdateBroker(myJuntaBroker) )
	  				{
	  					myJuntaSmit.setCodJunta(myJuntaBroker.getCodJunta());
	  					if( !daoTabela.JuntaInsert(myJuntaSmit,false) ){
	  						vErro.add("Erro: 002 | Inclus�o no Smit Junta: "+myJuntaSmit.getNomJunta()+ "\n");
	  						daoTabela.JuntaUpdateBroker(myJuntaBroker_aux);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 002 | Altera��o no Detran Junta: "+myJuntaBroker.getNomJunta()+ "\n");
	  			}
	  			if( (!myJuntaSmit.getCodJunta().equals("")) && (!myJuntaSmit.getNomJunta().equals("")) && (myJuntaBroker.getCodJunta().equals("")) ){
	  				myJuntaBroker.setCodJunta(myJuntaSmit.getCodJunta());
	  				if( daoTabela.JuntaInsertBroker(myJuntaBroker) )
	  				{
	  					if( !daoTabela.JuntaUpdate(myJuntaSmit) ){
	  						vErro.add("Erro: 003 | Altera��o no Smit Junta: "+myJuntaSmit.getNomJunta()+ "\n");
	  						daoTabela.JuntaDeleteBroker(myJuntaBroker);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 003 | Inclus�o no Detran Junta: "+myJuntaBroker.getNomJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getCodJunta().equals("")) && (!myJuntaSmit.getNomJunta().equals("")) && (myJuntaBroker.getCodJunta().equals("")) ){
	  				if( daoTabela.JuntaInsert(myJuntaSmit,true) )
	  				{
	  					myJuntaBroker.setCodJunta(myJuntaSmit.getCodJunta());
	  					if( !daoTabela.JuntaInsertBroker(myJuntaBroker) ){
	  						vErro.add("Erro: 004 | Inclus�o no Detran Junta: "+myJuntaBroker.getNomJunta()+ "\n");
	  						daoTabela.JuntaDelete(myJuntaSmit);
	  					}
	  				}
	  				else
	  					vErro.add("Erro: 004 | Inclus�o no Smit Junta: "+myJuntaBroker.getNomJunta()+ "\n");
	  			}
	  			if( (myJuntaSmit.getNomJunta().equals("")) && (myJuntaBroker.getNomJunta().equals(""))){
	  				boolean sucesso = true;
	  				if( !myJuntaBroker.getCodJunta().equals("") ){
	  					if( !daoTabela.JuntaDeleteBroker(myJuntaBroker) ){
	  						vErro.add("Erro: 005 | Exclus�o no Detran Junta: "+myJuntaBroker_aux.getNomJunta()+ "\n");
	  						sucesso = false;
	  					}
	  				}
	  				if(sucesso){
	  					if( !myJuntaSmit.getCodJunta().equals("") ){
	  						if( !daoTabela.JuntaDelete(myJuntaSmit) ){
	  							vErro.add("Erro: 005 | Exclus�o no Smit Junta: "+myJuntaSmit_aux.getNomJunta()+ "\n");
	  							if( !myJuntaBroker.getCodJunta().equals(""))
	  								daoTabela.JuntaInsertBroker(myJuntaBroker_aux);
	  							sucesso = false;
	  						}
	  					}
	  				}
	  				if(sucesso){
	  					juntaSmit.remove(pos);
	  					juntaSmit_aux.remove(pos);
	  					juntaBroker.remove(pos);
	  					juntaBroker_aux.remove(pos);
	  					pos--;
	  				}
	  			}
	  		}
	  	}
	  	catch (Exception e) {
	  		vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
	  		retorno = false;
	  	}//fim do catch
	  	setMsgErro(vErro);
	  	return retorno;
	   }
	  
	  public boolean isValidJunta(Vector vErro, List myJuntas) {
	  	if("0".equals(this.codOrgao)) 
	  		vErro.addElement("Orgao n�o selecionado. \n") ;
	  	
	  	String sDesc = "";
	  	for (int i = 0 ; i< myJuntas.size(); i++) {
	  		REC.JuntaBean myJunta = (REC.JuntaBean)myJuntas.get(i) ;
	  		sDesc = myJunta.getNomJunta();
	  		if((sDesc.length()==0) ||("".equals(sDesc)))
	  			continue ;
	  		
	  		// verificar duplicata 
	  		for (int m = 0 ; m <myJuntas.size(); m++) {
	  			REC.JuntaBean myJuntaOutra = (REC.JuntaBean)myJuntas.get(m) ;		
	  			if ((m!=i) && (myJuntaOutra.getNomJunta().trim().length()!=0) && (myJuntaOutra.getNomJunta().equals(sDesc))) {
	  				vErro.addElement("Junta em duplicata: (linhas: "+(i+1)+","+(m+1)+") "+sDesc+ " \n") ;		   
	  			}
	  		}
	  	}
	  	return (vErro.size() == 0) ;  
	  }
	  
	  public boolean isInsertJunta(List myJuntas, List myJuntas_aux)   {
	  	boolean retorno = true;
	  	Vector vErro = new Vector() ;
	  	// Verificada a valida��o dos dados nos campos
	  	if( isValidJunta(vErro,myJuntas) )   {
	  		try  {
	  			REC.DaoTabelas daoTabela = REC.DaoTabelas.getInstance();		   
	  			retorno = daoTabela.InsertJunta(vErro,myJuntas,myJuntas_aux);

	  		}
	  		catch (Exception e) {
	  			vErro.addElement("Junta do Org�o "+codOrgao+" n�o atualizados:"+ " \n") ;
	  			vErro.addElement("Erro na inclus�o: "+ e.getMessage() +"\n");
	  			retorno = false;
	  		}//fim do catch
	  	}
	  	else retorno = false;   
	  	setMsgErro(vErro);
	  	return retorno;
	  }
	}
