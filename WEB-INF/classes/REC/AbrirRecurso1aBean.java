package REC;



/**
* <b>Title:</b>        SMIT - Entrada de Recurso - Abrir Recurso Bean<br>
* <b>Description:</b>  Abrir Recurso Bean <br>
* <b>Copyright:</b>    Copyright (c) 2004<br>
* <b>Company:</b>      DECLINK - Tecnologia de Confian�a<br>
* @author Sergio Monteiro
* @version 1.0
*/

public class AbrirRecurso1aBean   extends AbrirRecBean   { 

  public AbrirRecurso1aBean()  throws sys.BeanException {

  super() ;	
  
  }
  
//--------------------------------------------------------------------------  
  public void setEventoOK(ParamOrgBean myParam,REC.AutoInfracaoBean myAuto)  throws sys.BeanException  {
	if ("".equals(myAuto.getNomStatus())) {
		setMsg("Status: "+myAuto.getCodStatus()+" n�o cadastrado na Tabela.");
		setEventoOK("N");	
	}
	else {
  	  setMsg(myAuto.getNomStatus()+" (Data do Status: "+myAuto.getDatStatus()+")");
	  String complMsg = "" ;
	  try {
		  	setEventoOK("S");	  
		  // Status 10,11,12,14,15,16
		  if ( (myAuto.getCodStatus().length()>1 ) && (myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE","10,11,12,14,15,16,82","3").indexOf(myAuto.getCodStatus())<0) )   
			{
		  	setEventoOK("N");
		  	complMsg = " - N�o pode entrar com Recurso de 1a Inst�ncia";
		  }
		     
		  // Status 10,11,12,14,15,16	
		  if ( (myParam.getParamOrgao("STATUS_ABRIR_PENALIDADE","10,11,12,14,15,16,82","3").indexOf(myAuto.getCodStatus())>=0) && (myAuto.getCodStatus().length()>1) ) {
		  	complMsg = " - Entrar com Recurso de 1a.Inst�ncia";
		  	setNFuncao("RECURSO 1a INST�NCIA");
		  	/* Michel
		  	setTpSolDP(getDP("1P",myParam,myAuto));
		  	setTpSolDC(getDC("1C",myParam,myAuto));
		  	*/
            setTpSolDP(getDP("1P",myParam,myAuto));		  	
			if ((!myAuto.getProprietario().getNomResponsavel().trim().equals(myAuto.getCondutor().getNomResponsavel().trim())) && (myAuto.getInfracao().getIndTipo().equals("C")) )
              setTpSolDC(getDC("1C",myParam,myAuto));			
		  }
		  if ( ("".equals(getTpSolDP())) && ("".equals(getTpSolDC())) ) {
			  setEventoOK("N");
			  complMsg = " - Nenhuma solicita��o efetuada.";
		  }
		  if (myAuto.getOrgao().getIntegrada().equals("N"))	  {
		      setEventoOK("N");
		      complMsg = " - Org�o isolado. N�o pode entrar com Recurso de 1a Inst�ncia";
		  }		  
		  setMsg(getMsg() + complMsg) ;
	 }
	 catch (Exception ue) {
       throw new sys.BeanException("AbrirRecurso1aBean: " + ue.getMessage());
	 }
	 
   }
  }   
//--------------------------------------------------------------------------  
}
