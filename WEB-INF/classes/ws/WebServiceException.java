package ws;

public class WebServiceException extends Exception {
    
    public WebServiceException() {
        super();
    }
    
    public WebServiceException(String msg) {  
        super(msg); 
    }
}
