package ws;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.activation.DataHandler;

import org.apache.commons.io.FileUtils;

import sys.BeanException;

import ACSS.DaoException;
import GER.NivelProcessoSistemaBean;
import PNT.NotificacaoBean;
import REC.AutoInfracaoBean;
import REG.NotifControleBean;

public class DigitalizacaoService {
	
	public String carregarAR(String codOrgao, String nomUsuario, String senha, String nomArquivo, String numLote, 
			String numCaixa, DataHandler datArquivo) throws sys.BeanException {
		
		String retorno = "";
		boolean bGeraArqCodRet = false;
		
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitDig = param.getParamSist("PARAR_SMIT_DIG");
			
			//Verifico a hora em que est� sendo enviado as imagens
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			String datHorInicio = param.getParamSist("DAT_HOR_INICIO");
			String datHorFim = param.getParamSist("DAT_HOR_FIM");
			String datHorAtual = sys.Util.getHora().substring(0,5);
			
			//Verifica se par�metro de hor�rio inicio e fim est�o ativo
			boolean flagHorario = false;
			if(datHorInicio.equals("") && datHorFim.equals(""))
			{
				flagHorario = true;
			}
			else if (df.parse(datHorAtual).before(df.parse(datHorInicio))||
		      df.parse(datHorAtual).after(df.parse(datHorFim))) 
			{
				flagHorario = true;
			}
				
			
			//Controla execu��o do SMIT DIG - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if( paraSmitDig.equals("N") && 
			    (flagHorario) )
			{
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);            
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				Date datEnvio = new Date();
				String datEnvioPNT = sys.Util.formatedToday().substring(0,10);
				File arquivo = new File(datArquivo.getName());
				
				String numAuto      = "";
				String numProcesso  = "";
				String codNotificacao = "";
				String numNotif     = "";            
				String codBarra     = nomArquivo.substring(0, nomArquivo.lastIndexOf(".tif"));
				String seqBarra     = "";
				boolean bNotificacaoPNT = false;
				String datEntrega   = "";
				String codRetorno   = "";
				
				if (numLote.length() > 3){
					if (numLote.length() < 15)
						throw new sys.BeanException("22-Data Recebimento-C�d. Retorno-Num. Lote inferior a 15 caracteres(xxxxxxxx-xx-xxx) !");
					numLote        = sys.Util.rPad(numLote," ",15);
					datEntrega     = numLote.substring(6,8) + "/" + numLote.substring(4,6) + "/" + numLote.substring(0,4);
					codRetorno     = numLote.substring(9,11);
					numLote        = numLote.substring(12,15);
					bGeraArqCodRet = true;
				}
				
				
				
				/*Pontua��o*/
				if (codBarra.indexOf("-")>=0) {
				  seqBarra = codBarra.substring(codBarra.lastIndexOf("-")+1,codBarra.length());					
				  codBarra = codBarra.substring(0, codBarra.lastIndexOf("-"));
				  bNotificacaoPNT = true;
				}
				
				//Valida os tamanhos dos nomes dos diretorios e do arquivo
				if (numCaixa.length() > 8)
					throw new sys.BeanException("01-Nome da Caixa superior a 8 caracteres !");
				
				else if (numLote.length() > 3)
					throw new sys.BeanException("02-Nome do Lote superior a 3 caracteres !");
				
				else if ((codBarra.length() != 9) && (codBarra.length() != 10))
					throw new sys.BeanException("03-Nome do Arquivo diferente de 9 e 10 caracteres !");
				
				else if (!sys.Util.isNumber(codBarra))
					throw new sys.BeanException("08-N�mero de Notifica��o inv�lido !");
				
				if (bNotificacaoPNT)
				{
					if (seqBarra.length() != 3) 
						throw new sys.BeanException("20-Nome do Arquivo (Sequ�ncia) diferente de 3 caracteres !");
					
					else if (!sys.Util.isNumber(seqBarra))
						throw new sys.BeanException("21-N�mero da Sequ�ncia da Notifica��o inv�lida !");
					
					PNT.NOT.Dao daoPnt = PNT.NOT.DaoFactory.getInstance();
					numProcesso = daoPnt.BuscaNumProcesso(codBarra,seqBarra);
					if (numProcesso.length()==0)
						throw new sys.BeanException("06-Notifica��o n�o localizada !");

					//Converte a imagem de tiff para gif
					String cmd = "C:\\Program Files\\XnView\\nconvert.exe -out gif \"" + arquivo.getPath() + "\"";                                                                                 
					Process pConv = Runtime.getRuntime().exec(cmd);
					pConv.waitFor();
					pConv = null;
					
					//Cria o bean de AR
					PNT.ARDigitalizadoBean AR = new PNT.ARDigitalizadoBean(codBarra, seqBarra, numProcesso, numLote, numCaixa, datEnvioPNT);
					//Copia a imagem gif para o diretorio final
					String imagOrigem = arquivo.getPath().substring(0, arquivo.getPath().length() - 4) + ".gif";
					FileUtils.copyFile(new File(imagOrigem), new File(AR.getArquivo(param)));
					
					//Verifica se o AR existe na base
					PNT.ARDigitalizadoBean tempAR = null;
					tempAR = PNT.ARDigitalizadoBean.consultaAR(new String[] {"NUM_NOTIFICACAO","SEQ_NOTIFICACAO"}, new String[] {"'" + codBarra + "'", "'" + seqBarra + "'" });
					
					if (tempAR != null)
						AR.setCodARDigitalizado(tempAR.getCodARDigitalizado());
					
					// Atualizando campos relativos a digitaliza��o.
					PNT.NotificacaoBean myNotifCont = new PNT.NotificacaoBean();
					myNotifCont.setNumNotificacao(codBarra);
					myNotifCont.setSeqNotificacao(seqBarra);
					myNotifCont.setDatDigitalizacao(datEnvioPNT);
					myNotifCont.setSitDigitaliza("S");
					
					myNotifCont.atualizaDigitalizacao();
					
					/*
					String operacao = AR.GravaARDigitalizado();
					if (operacao.equals("S"))
						retorno = "00-Atualizado com sucesso !";
					else
						retorno = "00-Incluido com sucesso !";
						
					(new PNT.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, operacao, retorno, nomUsuario)).GravaLogARDigitalizado();
					*/
					
					
					
					//Grava AR pra gerar arquivo de codRet pelo SMIT(PNT)
					if (bGeraArqCodRet){ 
						//Faz de-Para no codRetorno do SISGEN 
						/*codRetorno = tempAR.ConsultaCodRet(codRetorno);*/ 
						PNT.ARDigitalizadoBean ARCodRet = new PNT.ARDigitalizadoBean(codBarra, seqBarra, numProcesso, numLote, numCaixa, datEnvioPNT, datEntrega, codRetorno);
						ARCodRet.GravaARDigitalizadoTmp();
						retorno = "00-Incluido com sucesso !";						
					}
					
					
					//Exclui o arquivo original e o arquivo gif gerado
					arquivo.delete();
					(new File(imagOrigem)).delete();
					
					
				}
				else
				{
					String letra = codBarra.substring(0,1);
					boolean autoAlfa = false;
					if ( (codBarra.length() == 9) && !sys.Util.isNumber(letra) ) { //N�mero do auto alfanum�rico
						autoAlfa = true;
						String numero = String.valueOf(Integer.parseInt(codBarra.substring(1)));
						if (sys.Util.isNumber(letra))
							throw new sys.BeanException("07-N�mero do Auto deve come�ar por uma letra !");
						numAuto = letra + numero;				
					} else { //N�mero da notifica��o
						numNotif = codBarra.substring(0, 9);
						
						//Busca o n�mero do auto no banco de dados
						numAuto = REC.DaoDigit.getInstance().BuscaNumAutoInfracao(numNotif);
						if (numAuto.equals("")) throw new sys.BeanException("06-Notifica��o n�o localizada !");                    
					}
					//Converte a imagem de tiff para gif
					String cmd = "C:\\Program Files\\XnView\\nconvert.exe -out gif \"" + arquivo.getPath() + "\"";                                                                                 
					Process pConv = Runtime.getRuntime().exec(cmd);
					pConv.waitFor();
					pConv = null;
					
					//Cria o bean de AR
					REC.ARDigitalizadoBean AR = new REC.ARDigitalizadoBean(numNotif, numAuto, numLote, numCaixa, datEnvio);
					//Copia a imagem gif para o diretorio final
					String imagOrigem = arquivo.getPath().substring(0, arquivo.getPath().length() - 4) + ".gif";
					FileUtils.copyFile(new File(imagOrigem), new File(AR.getArquivo(param)));
					
					//Verifica se a imagem est� corrompida
					verificaImagem(usuarioBean.getOrgao().getCodOrgao(),AR.getNumAutoInfracao()+".gif", AR.getArquivo(param), "AR");
					
					//Verifica se o AR existe na base
					REC.ARDigitalizadoBean tempAR = null;
					if (autoAlfa) //N�mero do Auto
						tempAR = REC.ARDigitalizadoBean.consultaAR(new String[] {"NUM_AUTO_INFRACAO", 
						"DAT_DIGITALIZACAO" }, new String[] {"'" + numAuto + "'", "'" + sys.Util.fmtData(
								datEnvio) + "'" });
					else //N�mero do AR
						tempAR = REC.ARDigitalizadoBean.consultaAR(new String[] {"NUM_NOTIFICACAO"}, 
								new String[] {numNotif});            
					if (tempAR != null)
						AR.setCodARDigitalizado(tempAR.getCodARDigitalizado());
					
					// Atualizando campos relativos a digitaliza��o.
					NotifControleBean myNotifCont = new NotifControleBean();
					myNotifCont.setNumNotificacao(numNotif);
					myNotifCont.setDatDigitalizacao(datEnvio);
					myNotifCont.setSitDigitaliza("S");
					myNotifCont.atualizaDigitalizacao();
					
					/*
					String operacao = AR.grava();
					if (operacao.equals("S"))
						retorno = "00-Atualizado com sucesso !";
					else
						retorno = "00-Incluido com sucesso !";
					(new REC.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, operacao, retorno, nomUsuario)).grava();
					*/
					
					
					//Grava AR pra gerar arquivo de codRet pelo SMIT
					if (bGeraArqCodRet){ 
						//Faz de-Para no codRetorno do SISGEN 
						/*codRetorno = tempAR.ConsultaCodRet(codRetorno);*/ 
						REC.ARDigitalizadoBean ARCodRet = new REC.ARDigitalizadoBean(numNotif, numAuto, numLote, numCaixa, datEnvio, datEntrega, codRetorno);
						ARCodRet.gravaTmp();
						retorno = "00-Incluido com sucesso !";						
				    }
					
					//Exclui o arquivo original e o arquivo gif gerado
					arquivo.delete();
					(new File(imagOrigem)).delete();
				}
				
			}
			else if(paraSmitDig.equals("S"))
				retorno = "01- O SMIT DIG est� temporariamente bloqueado.";
			else
				retorno = "02- O SMIT DIG est� temporariamente em manuten��o.";


		} catch (sys.BeanException e1) {
			retorno = e1.getMessage();
			try {
				(new REC.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed1) {
				retorno = "ER-" + ed1.getMessage();
			}
			
		} catch (Exception e2) {
			retorno = "ER-" + e2.getMessage();
			try {
				(new REC.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed2) {
				retorno = "ER-" + ed2.getMessage();
			}
		}
		return retorno;
	} 
	
	/**
	 * 
	 * @param codOrgao
	 * @param nomUsuario
	 * @param senha
	 * @param nomArquivo
	 * @param numLote
	 * @param numCaixa
	 * @param datArquivo
	 * @return
	 * @throws sys.BeanException
	 */
	public String carregarAI(String codOrgao, String nomUsuario, String senha, String nomArquivo, String numLote,
			String numCaixa, DataHandler datArquivo) throws sys.BeanException {
		
		String retorno = "";
		try {
            //Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitDig = param.getParamSist("PARAR_SMIT_DIG");
			
			//Verifico a hora em que est� sendo enviado as imagens
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			String datHorInicio = param.getParamSist("DAT_HOR_INICIO");
			String datHorFim = param.getParamSist("DAT_HOR_FIM");
			String datHorAtual = sys.Util.getHora().substring(0,5);
			
			//Verifica se par�metro de hor�rio inicio e fim est�o ativo
			boolean flagHorario = false;
			if(datHorInicio.equals("") && datHorFim.equals(""))
			{
				flagHorario = true;
			}
			else if (df.parse(datHorAtual).before(df.parse(datHorInicio))||
		      df.parse(datHorAtual).after(df.parse(datHorFim))) 
			{
				flagHorario = true;
			}
			
			
			//Controla execu��o do SMIT DIG - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if( paraSmitDig.equals("N") && 
			    (flagHorario) )
			{
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);            
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				Date datEnvio = new Date();
				File arquivo = new File(datArquivo.getName());
				
				String numAuto = nomArquivo.substring(0, nomArquivo.lastIndexOf(".tif"));
				
				//Valida os tamnhados dos nomes dos diretorios e do arquivo
				if (numCaixa.length() > 10)
					throw new sys.BeanException("01-Nome da Caixa superior a 10 caracteres !");
				
				else if (numLote.length() > 11)
					throw new sys.BeanException("02-Nome do Lote superior a 11 caracteres !");
				
				else if (numAuto.length() > 13)
					throw new sys.BeanException("03-Nome do Arquivo superior a 13 caracteres !");
				
				else if (sys.Util.isNumber(numAuto.substring(0,1)))
					throw new sys.BeanException("04-N�mero do Auto deve come�ar por uma letra !");
				
				//Converte a imagem de tiff para gif
				String cmd = "C:\\Program Files\\XnView\\nconvert.exe -out gif \"" + arquivo.getPath() + "\"";                                                                                 
				Process pConv = Runtime.getRuntime().exec(cmd);
				pConv.waitFor();
				pConv = null;
				
				AutoInfracaoBean autoBean = new AutoInfracaoBean();
				autoBean.setIdentOrgaoFoto(numAuto);
				//Verifica se a imagem vem nomeada com o n�mero do auto ou o identificador do �rg�o
				if (!autoBean.getIdentOrgao().equals("")) {
					//Localiza o n�mero do auto a partir da identifica��o do �rg�o
					if (autoBean.LeAutoInfracaoLocal("ident"))
						numAuto = autoBean.getNumAutoInfracao();
					else
						throw new sys.BeanException("05-N�mero de Auto n�o localizado !");
				}
				
				if (!sys.Util.isNumber(numAuto.substring(1)))
					throw new sys.BeanException("09-N�mero de Auto inv�lido !");
				
				//Cria o bean de AI
				REC.AIDigitalizadoBean AI = new REC.AIDigitalizadoBean(numAuto, numLote, null, datEnvio);
				
				//Copia a imagem gif para o diretorio final
				String imagOrigem = arquivo.getPath().substring(0, arquivo.getPath().length() - 4) + ".gif";
				FileUtils.copyFile(new File(imagOrigem), new File(AI.getArquivo(param)));
				
				//Verifica se a imagem est� corrompida
				verificaImagem(usuarioBean.getOrgao().getCodOrgao(),AI.getNumAutoInfracao()+".gif", AI.getArquivo(param), "AI");
				
				//Verifica se cria o bean de caixa                                                                              
				REC.CaixAIDigitalizadoBean[] colCaixa = REC.DaoDigit.getInstance().ConsultaCaixAIDigitalizado(
						new String[] {"NUM_CAIXA"}, new String[] {"'"+numCaixa+"'"});
				REC.CaixAIDigitalizadoBean beanCaixa = null;
				if (colCaixa.length > 0)
					beanCaixa = colCaixa[0];
				else {
					beanCaixa = new REC.CaixAIDigitalizadoBean(numCaixa, "", "", "", "", "");
					beanCaixa.grava();
				}
				AI.setCaixa(beanCaixa);
				
				//Verifica se o num. de auto existe na base
				REC.AIDigitalizadoBean tempAI = REC.DaoDigit.getInstance().ConsultaAIDigitalizado( 
						new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'" + numAuto + "'"});
				if (tempAI != null)
					AI.setCodAIDigitalizado(tempAI.getCodAIDigitalizado());			
				
				// Atualizando campos relativos a digitaliza��o.
				AutoInfracaoBean myAutoInfracao = new AutoInfracaoBean();
				myAutoInfracao.setNumAutoInfracao(numAuto);
				myAutoInfracao.setDatDigitalizacao(datEnvio);
				myAutoInfracao.setSit_digitaliza("S");
				myAutoInfracao.atualizaDigitalizacao();
				
				String operacao = AI.grava();
				if (operacao.equals("S"))
					retorno = "00-Atualizado com sucesso !";
				else
					retorno = "00-Incluido com sucesso !";                
				(new REC.LogAIDigitalizadoBean(nomArquivo, numCaixa, numLote, operacao, retorno, nomUsuario)).grava();
				
				//Exclui o arquivo original e o arquivo gif gerado
				arquivo.delete();
				(new File(imagOrigem)).delete();    
			}
			else if(paraSmitDig.equals("S"))
			{
				throw new sys.BeanException("O SMIT DIG est� temporariamente bloqueado.");
			}
			else
			{
				throw new sys.BeanException("O SMIT DIG est� temporariamente em manuten��o.");
			}
			
		} catch (sys.BeanException e1) {
			retorno = e1.getMessage();
			try {
				(new REC.LogAIDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed1) {
				retorno = "ER-" + ed1.getMessage();
			}
			
		} catch (Exception e2) {
			retorno = "ER-" + e2.getMessage();
			try {
				(new REC.LogAIDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed2) {
				retorno = "ER-" + ed2.getMessage();
			}
		}
		return retorno;
	}
	
	public String carregarFoto(String codOrgao, String nomUsuario, String senha, String nomArquivo, DataHandler
			datArquivo) throws sys.BeanException {
		
		String retorno = "";
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitDig = param.getParamSist("PARAR_SMIT_DIG");
			
			//Verifico a hora em que est� sendo enviado as imagens
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			String datHorInicio = param.getParamSist("DAT_HOR_INICIO");
			String datHorFim = param.getParamSist("DAT_HOR_FIM");
			String datHorAtual = sys.Util.getHora().substring(0,5);
			
			//Verifica se par�metro de hor�rio inicio e fim est�o ativo
			boolean flagHorario = false;
			if(datHorInicio.equals("") && datHorFim.equals(""))
			{
				flagHorario = true;
			}
			else if (df.parse(datHorAtual).before(df.parse(datHorInicio))||
		      df.parse(datHorAtual).after(df.parse(datHorFim))) 
			{
				flagHorario = true;
			}
			
			
			//Controla execu��o do SMIT DIG - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if( paraSmitDig.equals("N") && 
			    (flagHorario) )
			{
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);            
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				Date datEnvio = new Date();
				File arquivo = new File(datArquivo.getName());
				
				String numAuto = nomArquivo.substring(0, nomArquivo.lastIndexOf(".jpg"));
				
				//Valida os tamnhados dos nomes dos diretorios e do arquivo                
				if (numAuto.length() > 13)
					throw new sys.BeanException("03-Nome do Arquivo superior a 13 caracteres !");
				
				else if (sys.Util.isNumber(numAuto.substring(0,1)))
					throw new sys.BeanException("04-N�mero do Auto deve come�ar por uma letra !");								
				
				AutoInfracaoBean autoBean = new AutoInfracaoBean();
				autoBean.setIdentOrgaoFoto(numAuto);
				//Verifica se a imagem vem nomeada com o n�mero do auto ou o identificador do �rg�o
				if (!autoBean.getIdentOrgao().equals("")) {
					//Localiza o n�mero do auto a partir da identifica��o do �rg�o
					if (autoBean.LeAutoInfracaoLocal("ident"))
						numAuto = autoBean.getNumAutoInfracao();
					else
						throw new sys.BeanException("05-N�mero de Auto n�o localizado !");
				}
				
				if (!sys.Util.isNumber(numAuto.substring(1)))
					throw new sys.BeanException("09-N�mero de Auto inv�lido !");			
				
				//Cria o bean de Foto
				REC.FotoDigitalizadaBean Foto = new REC.FotoDigitalizadaBean(numAuto, datEnvio);
				
				//Copia a imagem jpg para o diretorio final
				String imagOrigem = arquivo.getPath();
				FileUtils.copyFile(new File(imagOrigem), new File(Foto.getArquivo(param)));                                       
				
				//Verifica se a imagem est� corrompida
				verificaImagem(usuarioBean.getOrgao().getCodOrgao(),Foto.getNumAutoInfracao()+".jpg", Foto.getArquivo(param), "Foto");
				
				//Verifica se o num. de auto existe na base
				REC.FotoDigitalizadaBean tempFoto = REC.DaoDigit.getInstance().ConsultaFotoDigitalizada( 
						new String[] {"NUM_AUTO_INFRACAO"}, new String[] {"'" + numAuto + "'"});
				if (tempFoto != null)
					Foto.setCodFotoDigitalizada(tempFoto.getCodFotoDigitalizada());
				
				String operacao = Foto.grava();
				if (operacao.equals("S"))
					retorno = "00-Atualizado com sucesso !";
				else
					retorno = "00-Incluido com sucesso !";
				(new REC.LogFotoDigitalizadaBean(nomArquivo, operacao, retorno, nomUsuario)).grava();
				
				//Exclui o arquivo original e o arquivo gif gerado
				arquivo.delete();
				(new File(imagOrigem)).delete();
			}
			else if(paraSmitDig.equals("S"))
			{
				throw new sys.BeanException("O SMIT_DIG est� temporariamente bloqueado.");
			}
			else
			{
				throw new sys.BeanException("O SMIT_DIG est� temporariamente em manuten��o.");
			}
				
		} catch (sys.BeanException e1) {
			retorno = e1.getMessage();
			try {
				(new REC.LogFotoDigitalizadaBean(nomArquivo, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed1) {
				retorno = "ER-" + ed1.getMessage();
			}
			
		} catch (Exception e2) {
			retorno = "ER-" + e2.getMessage();
			try {
				(new REC.LogFotoDigitalizadaBean(nomArquivo, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed2) {
				retorno = "ER-" + ed2.getMessage();
			}
		}
		return retorno;
	}
	
	public String carregarFotoDnf(String codOrgao, String nomUsuario, String senha, String nomArquivo, String numLote, 
			String numCaixa, DataHandler datArquivo) throws sys.BeanException {
		
		String retorno = "";
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitDig = param.getParamSist("PARAR_SMIT_DIG");
			
			//Verifico a hora em que est� sendo enviado as imagens
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			String datHorInicio = param.getParamSist("DAT_HOR_INICIO");
			String datHorFim = param.getParamSist("DAT_HOR_FIM");
			String datHorAtual = sys.Util.getHora().substring(0,5);
			
			//Verifica se par�metro de hor�rio inicio e fim est�o ativo
			boolean flagHorario = false;
			if(datHorInicio.equals("") && datHorFim.equals(""))
			{
				flagHorario = true;
			}
			else if (df.parse(datHorAtual).before(df.parse(datHorInicio))||
		      df.parse(datHorAtual).after(df.parse(datHorFim))) 
			{
				flagHorario = true;
			}
				
			
			//Controla execu��o do SMIT DIG - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if( paraSmitDig.equals("N") && 
			    (flagHorario) )
			{
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);            
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				Date datEnvio = new Date();
				File arquivo = new File(datArquivo.getName());
				
				String numAuto = nomArquivo.substring(0, nomArquivo.lastIndexOf(".jpg"));
				
				//Valida os tamnhados dos nomes dos diretorios e do arquivo                
				if (numAuto.length() > 13)
					throw new sys.BeanException("03-Nome do Arquivo superior a 13 caracteres !");
				
				
				//Cria o bean de Foto
				REC.FotoDanificadaBean Foto = new REC.FotoDanificadaBean(numAuto, datEnvio);
				
				//Copia a imagem jpg para o diretorio final
				String imagOrigem = arquivo.getPath();
				FileUtils.copyFile(new File(imagOrigem), new File(Foto.getArquivo(param)));                                       
				
				
				String operacao = Foto.gravaDnf();
				if (operacao.equals("I"))
					retorno = "00-Incluido com sucesso !";
				
				
				//Exclui o arquivo original e o arquivo gif gerado
				arquivo.delete();
				(new File(imagOrigem)).delete();
			}
			else if(paraSmitDig.equals("S"))
			{
				throw new sys.BeanException("O SMIT_DIG est� temporariamente bloqueado.");
			}
			else
			{
				throw new sys.BeanException("O SMIT_DIG est� temporariamente em manuten��o.");
			}
				
		} catch (sys.BeanException e1) {
			retorno = e1.getMessage();
			
		} catch (Exception e2) {
			retorno = "ER-" + e2.getMessage();
		}
		return retorno;
	} 	
	

	public void verificaImagem(String codOrgao, String nomImagem, String pathImagem, String tipoImagem) throws Exception{
		File arq = new File(pathImagem);
		FileInputStream imagem;
		try {
			imagem = new FileInputStream(arq);
			int statusImage = imagem.read(); //L� a imagem
			
			if (statusImage <= 0) //Se statusImage <= 0 igual a Imagem Corrompida
			{
                //Envia e-mail
				enviaEmailErroImagem(nomImagem, tipoImagem, codOrgao);
				//Informa erro de imagem corrompida na aplica��o
				throw new IOException("Erro: Imagem "+nomImagem+" est� corrompida! Favor, reenviar." );
				
			}
				
		} catch (IOException e) {
			throw new IOException("Erro: "+ e.getMessage());
		} catch (Exception e) {
			throw new Exception ("Erro: "+ e.getMessage());
		} 
	}
	
	public void enviaEmailErroImagem(String nomImagem, String tipoImagem,String codOrgao) throws Exception{
        //Prepara par�metro
        ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
        param.setCodSistema("40"); // M�DULO REGISTRO 
        try {
			param.PreparaParam();
			String emailBroker = param.getParamSist("EMAIL_LOG_BROKER");
			ArrayList<NivelProcessoSistemaBean> ListDestinatarios = new ArrayList<NivelProcessoSistemaBean>();
			ListDestinatarios = ACSS.Dao.getInstance().getDestinatariosPorNivel(param,"CONTROLE_IMAGENS_CORROMPIDAS");
			
			//Monta mensagem do corpo do e-mail
			StringBuffer msg = new StringBuffer();
			msg.append("Prezado(a) Senhor(a), \n\nInformamos que a imagem do(a) "+tipoImagem+" "+
					 nomImagem +" est� corrompida. Favor, reenviar. \n\nAtenciosamente, \nAuditoria SMIT");
			
			//Envia E-mail
			ArrayList destinatarioErro = preparaEmail(ListDestinatarios, msg, codOrgao);
			enviaEmail(destinatarioErro, emailBroker,codOrgao);
       
        }catch (BeanException e) {
        	throw new BeanException("Erro: "+ e.getMessage());
		} catch (DaoException e) {
			throw new DaoException("Erro: "+ e.getMessage());
		} catch (Exception e) {
			throw new Exception("Erro: "+ e.getMessage());
		}
	}
	
	public String carregarARPontuacao(String codOrgao, String nomUsuario, String senha, String nomArquivo, String numLote, 
			String numCaixa, DataHandler datArquivo) throws sys.BeanException {
		
		String retorno = null;
		boolean bGeraArqCodRet = false;
		
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitDig = param.getParamSist("PARAR_SMIT_DIG");
			
			//Verifico a hora em que est� sendo enviado as imagens
			SimpleDateFormat df = new SimpleDateFormat("HH:mm");
			String datHorInicio = param.getParamSist("DAT_HOR_INICIO");
			String datHorFim = param.getParamSist("DAT_HOR_FIM");
			String datHorAtual = sys.Util.getHora().substring(0,5);
			
			//Verifica se par�metro de hor�rio inicio e fim est�o ativo
			boolean flagHorario = false;
			if(datHorInicio.equals("") && datHorFim.equals("")) {
				flagHorario = true;
			} else if (df.parse(datHorAtual).before(df.parse(datHorInicio))|| df.parse(datHorAtual).after(df.parse(datHorFim))){
				flagHorario = true;
			}
				
			
			//Controla execu��o do SMIT DIG - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if(paraSmitDig.equals("N") && (flagHorario)){
				
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);            
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				Date datEnvio = new Date();
				String datEnvioPNT = sys.Util.formatedToday().substring(0,10);
				File arquivo = new File(datArquivo.getName());
				
				String numAuto      = "";
				String numProcesso  = "";
				String codNotificacao = "";
				String numNotif     = "";            
				String codBarra     = nomArquivo.substring(0, nomArquivo.lastIndexOf(".tif"));
				String seqBarra     = "";
				boolean bNotificacaoPNT = true;
				String datEntrega   = "";
				String codRetorno   = "";
				
				if (numLote.length() > 3){
					if (numLote.length() < 15)
						throw new sys.BeanException("22-Data Recebimento-C�d. Retorno-Num. Lote inferior a 15 caracteres(xxxxxxxx-xx-xxx) !");
					numLote        = sys.Util.rPad(numLote," ",15);
					datEntrega     = numLote.substring(6,8) + "/" + numLote.substring(4,6) + "/" + numLote.substring(0,4);
					codRetorno     = numLote.substring(9,11);
					numLote        = numLote.substring(12,15);
					bGeraArqCodRet = true;
				}
				
				//Valida os tamanhos dos nomes dos diretorios e do arquivo
				if (numCaixa.length() > 8)
					throw new sys.BeanException("01-Nome da Caixa superior a 8 caracteres !");
				
				else if (numLote.length() > 3)
					throw new sys.BeanException("02-Nome do Lote superior a 3 caracteres !");
				
				else if ((codBarra.length() != 9) && (codBarra.length() != 10))
					throw new sys.BeanException("03-Nome do Arquivo diferente de 9 e 10 caracteres !");
				
				else if (!sys.Util.isNumber(codBarra))
					throw new sys.BeanException("08-N�mero de Notifica��o inv�lido !");
				
				if (seqBarra.length() != 3){
					throw new sys.BeanException("20-Nome do Arquivo (Sequ�ncia) diferente de 3 caracteres !");
				}else if (!sys.Util.isNumber(seqBarra)){
					throw new sys.BeanException("21-N�mero da Sequ�ncia da Notifica��o inv�lida !");
				}
					
				PNT.NOT.Dao daoPnt = PNT.NOT.DaoFactory.getInstance();
				numProcesso = daoPnt.BuscaNumProcesso(codBarra,seqBarra);
				if (numProcesso.length()==0)
					throw new sys.BeanException("06-Notifica��o n�o localizada !");

				//Converte a imagem de tiff para gif
				String cmd = "C:\\Program Files\\XnView\\nconvert.exe -out gif \"" + arquivo.getPath() + "\"";                                                                                 
				Process pConv = Runtime.getRuntime().exec(cmd);
				pConv.waitFor();
				pConv = null;
					
				//Cria o bean de AR
				PNT.ARDigitalizadoBean AR = new PNT.ARDigitalizadoBean(codBarra, seqBarra, numProcesso, numLote, numCaixa, datEnvioPNT);
				//Copia a imagem gif para o diretorio final
				String imagOrigem = arquivo.getPath().substring(0, arquivo.getPath().length() - 4) + ".gif";
				FileUtils.copyFile(new File(imagOrigem), new File(AR.getArquivo(param)));
					
				//Verifica se o AR existe na base
				PNT.ARDigitalizadoBean tempAR = null;
				tempAR = PNT.ARDigitalizadoBean.consultaAR(new String[] {"NUM_NOTIFICACAO","SEQ_NOTIFICACAO"}, new String[] {"'" + codBarra + "'", "'" + seqBarra + "'" });
					
				if (tempAR != null)
					AR.setCodARDigitalizado(tempAR.getCodARDigitalizado());
					
				// Atualizando campos relativos a digitaliza��o.
				PNT.NotificacaoBean myNotifCont = new PNT.NotificacaoBean();
				myNotifCont.setNumNotificacao(codBarra);
				myNotifCont.setSeqNotificacao(seqBarra);
				myNotifCont.setDatDigitalizacao(datEnvioPNT);
				myNotifCont.setSitDigitaliza("S");
			
				myNotifCont.atualizaDigitalizacao();
					
				/*
				String operacao = AR.GravaARDigitalizado();
				if (operacao.equals("S"))
					retorno = "00-Atualizado com sucesso !";
					else
						retorno = "00-Incluido com sucesso !";
						
					(new PNT.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, operacao, retorno, nomUsuario)).GravaLogARDigitalizado();
					*/
					
				//Grava AR pra gerar arquivo de codRet pelo SMIT(PNT)
				if (bGeraArqCodRet){ 
					//Faz de-Para no codRetorno do SISGEN 
					/*codRetorno = tempAR.ConsultaCodRet(codRetorno);*/ 
					PNT.ARDigitalizadoBean ARCodRet = new PNT.ARDigitalizadoBean(codBarra, seqBarra, numProcesso, numLote, numCaixa, datEnvioPNT, datEntrega, codRetorno);
					ARCodRet.GravaARDigitalizadoTmp();
					retorno = "00-Incluido com sucesso !";						
				}
					
				//Exclui o arquivo original e o arquivo gif gerado
				arquivo.delete();
				(new File(imagOrigem)).delete();
				
			}else if(paraSmitDig.equals("S")){
				retorno = "01- O SMIT DIG est� temporariamente bloqueado.";
			}else{
				retorno = "02- O SMIT DIG est� temporariamente em manuten��o.";
			}


		} catch (sys.BeanException e1) {
			retorno = e1.getMessage();
			try {
				(new REC.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed1) {
				retorno = "ER-" + ed1.getMessage();
			}
			
		} catch (Exception e2) {
			retorno = "ER-" + e2.getMessage();
			try {
				(new REC.LogARDigitalizadoBean(nomArquivo, numCaixa, numLote, "R", retorno, nomUsuario)).grava();
				(new File(datArquivo.getName())).delete();
			} catch (REC.DaoException ed2) {
				retorno = "ER-" + ed2.getMessage();
			}
		}
		
		return retorno;
	}
	

	private ArrayList<NivelProcessoSistemaBean> preparaEmail(ArrayList ListDestinatarios, StringBuffer sConteudoEmail,String codOrgao){
		// Prepara Email    
		ArrayList<NivelProcessoSistemaBean> usuariosBloqueados = new ArrayList<NivelProcessoSistemaBean>();
		if (ListDestinatarios.size()>0)
		{
			Iterator it = ListDestinatarios.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
		     	{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					ArrayList lDestinatarios = new ArrayList();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getCodOrgao().equals(codOrgao)&& nivelProcesso.getTipoMsg().equals("M"))
						{
				  		   destinatario.getMsg().add(sConteudoEmail.toString());
				  		   lDestinatarios.add(destinatario);
						}
					}
					nivelProcesso.setListNivelDestino(lDestinatarios);
					usuariosBloqueados.add(nivelProcesso);
			   }
			}
		  }
          return usuariosBloqueados;
	}
	
	private void enviaEmail(ArrayList usuariosBloqueados, String emailBroker,String codOrgao) throws Exception{	
		if (usuariosBloqueados.size()>0)
		{
			Iterator it = usuariosBloqueados.iterator();
			GER.NivelProcessoSistemaBean nivelProcesso= new GER.NivelProcessoSistemaBean();
			while (it.hasNext())
			{
				nivelProcesso=(GER.NivelProcessoSistemaBean)it.next();
				if (nivelProcesso.getValNivelProcesso().equals("S"))
				{
					Iterator itx = nivelProcesso.getListNivelDestino().iterator();
					GER.CadastraDestinatarioBean destinatario= new GER.CadastraDestinatarioBean();
					while (itx.hasNext())
					{
						destinatario=(GER.CadastraDestinatarioBean)itx.next();
						if(destinatario.getMsg().size()>0)
						{
							String sConteudo="";
							Iterator ity = destinatario.getMsg().iterator();			
							while (ity.hasNext()) 
							  sConteudo+=(String)ity.next();
							
							sys.EmailBean EmailBeanId = new sys.EmailBean();
							EmailBeanId.setDestinatario( destinatario.getTxtEmail() );
							EmailBeanId.setRemetente(emailBroker);
							EmailBeanId.setAssunto( nivelProcesso.getNomNivelProcesso()+" - �RG�O: "+codOrgao);
							EmailBeanId.setConteudoEmail(sConteudo);
							EmailBeanId.sendEmail();
						}
				   }
				}
			}
		}
	}

}