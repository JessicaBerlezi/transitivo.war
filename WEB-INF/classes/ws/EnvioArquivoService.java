package ws;

import java.util.ArrayList;
import java.util.List;

import sys.BeanException;
import REG.ArquivoRecebidoBean;

public class EnvioArquivoService {
		
	public Object[] iniciarArquivo(String codOrgao, String nomUsuario, String senha, String codOrgaoAtuacao, 
			String nomArquivo, String funcArquivo, String header, Integer qtdLinhas) throws BeanException {
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			String paraSmitEva = param.getParamSist("PARAR_SMIT_EVA");
			
			Object[] retorno = new Object[0];
			//Controla execu��o do SMIT EVA - 'S'= SIM (Parar) e 'N'= N�o (Executar) 
			if( paraSmitEva.equals("N") )
			{
				//Valida o usu�rio e a senha passada
				ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
				usuarioBean.setNomUserName(nomUsuario);
				ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();					
				orgaoBean.setCodOrgao(codOrgao);
				usuarioBean.setOrgao(orgaoBean);
				usuarioBean.Le_Usuario(1);		
				if (!usuarioBean.isSenhaValid(senha))
					throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
				
				REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();  
				if (!arqRec.pegaOrgaoAtuacao(codOrgaoAtuacao))
					throw new sys.BeanException("08-�rg�o de Atua��o inv�lido !");
				
				arqRec.setCodOrgaoLotacao(usuarioBean.getOrgao().getCodOrgao());
				arqRec.setSigOrgaoLotacao(usuarioBean.getOrgao().getSigOrgao());		
				arqRec.setNomUsername(nomUsuario);
				arqRec.setDatRecebimento(sys.Util.formatedToday().substring(0,10));                            
				arqRec.setNomArquivo(nomArquivo.toUpperCase());
				arqRec.setArqFuncao(funcArquivo);
				arqRec.carregarHeader(header, usuarioBean);
				arqRec.setQtdRegCalculada(qtdLinhas.intValue());
				
				boolean existe = arqRec.isInsertTemp();
				
				List retornos = new ArrayList();
				if (arqRec.getErroArquivo().size() == 0) {
					retornos.add("00");
					retornos.add(Integer.valueOf(arqRec.getCodArquivo()));
					if (existe) retornos.add(new Integer(arqRec.getUltLinha()));
				} else {
					retornos.add("ERR");
					String[] erros = new String[arqRec.getErroArquivo().size()];
					arqRec.getErroArquivo().toArray(erros);
					retornos.add(erros);
				}
				retorno = new Object[retornos.size()];
				retornos.toArray(retorno);
			}
			else if(paraSmitEva.equals("S"))
			{
				throw new BeanException("O SMIT_EVA est� temporariamente bloqueado.");
			}
			return retorno;
				
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}		
	}
	
	public Object[] gravarLinha(String codOrgao, String nomUsuario, String senha, Integer codArquivo, String linha, 
			Integer numLinha) throws BeanException {
		try {
			//Valida o usu�rio e a senha passada
			ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
			usuarioBean.setNomUserName(nomUsuario);
			ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();					
			orgaoBean.setCodOrgao(codOrgao);
			usuarioBean.setOrgao(orgaoBean);
			usuarioBean.Le_Usuario(1);		
			if (!usuarioBean.isSenhaValid(senha))
				throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
			
			REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			arqRec.carregaArqRecebidoTemp(String.valueOf(codArquivo));
			arqRec.isLinhaInsertTemp(linha, numLinha.intValue());
			
			List retornos = new ArrayList();
			if (arqRec.getErroArquivo().size() == 0) {
				retornos.add("00");
				retornos.add("Linha enviada com sucesso !");
			} else {
				retornos.add("ERR");
				String[] erros = new String[arqRec.getErroArquivo().size()];
				arqRec.getErroArquivo().toArray(erros);
				retornos.add(erros);
			}
			Object[] retorno = new Object[retornos.size()];
			retornos.toArray(retorno);
			return retorno;
			
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}
	}
	
	public Object[] fecharArquivo(String codOrgao, String nomUsuario, String senha, Integer codArquivo, 
			Integer qtdLinhas) throws BeanException {
		try {

			//Valida o usu�rio e a senha passada
			ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
			usuarioBean.setNomUserName(nomUsuario);
			ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();					
			orgaoBean.setCodOrgao(codOrgao);
			usuarioBean.setOrgao(orgaoBean);
			usuarioBean.Le_Usuario(1);		
			if (!usuarioBean.isSenhaValid(senha))
				throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");
			
			//Pega os parametros do org�o
			REC.ParamOrgBean paramOrgao = new REC.ParamOrgBean() ;
			paramOrgao.PreparaParam(usuarioBean);
			
			REG.ArquivoRecebidoBean arqRec = new ArquivoRecebidoBean();
			arqRec.carregaArqRecebidoTemp(String.valueOf(codArquivo));
			arqRec.setQtdRegCalculada(qtdLinhas.intValue());
			
			//Valida a quantidade de linhas enviadas
			if (arqRec.criticaQtdLinhas(qtdLinhas.intValue())) {
				/*
				int iQtdReg=Integer.parseInt(arqRec.getQtdReg())-1;
				arqRec.setQtdReg(String.valueOf(iQtdReg));
				*/
				
				arqRec.carregaLinhasTemp();
				if (arqRec.isIndProcessamento())
					arqRec.setCodStatus("0"); //Ser� processado
				else
					arqRec.setCodStatus("1"); //N�o ser� processado
				arqRec.setNumProtocolo();
				
				arqRec.moveTemp(paramOrgao);
			}
			
			List retornos = new ArrayList();
			if (arqRec.getErroArquivo().size() == 0) {
				retornos.add("00");
				retornos.add("Arquivo enviado com sucesso !");
			} else {
				retornos.add("ERR");
				String[] erros = new String[arqRec.getErroArquivo().size()];
				arqRec.getErroArquivo().toArray(erros);
				retornos.add(erros);
			}
			Object[] retorno = new Object[retornos.size()];
			retornos.toArray(retorno);
			return retorno;
			
		} catch (Exception e) {
			throw new BeanException(e.getMessage());
		}		
	}
	
}