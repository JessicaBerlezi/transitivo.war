package ws;

/**
 * @author glaucio
 */

public class AxisServlet extends org.apache.axis.transport.http.AxisServlet {
    
    public void doGet(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) {
        try {
            super.doGet(request, response);
        } catch (Exception e) {
            System.out.println("AxisServlet doGet: Exception " + e.getMessage());
        }
    }
    
    public void doPost(javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response) {
        try {
            super.doPost(request, response);
        } catch (Exception e) {
            System.out.println("AxisServlet doPost: Exception " + e.getMessage());
        }     
    }
    
}