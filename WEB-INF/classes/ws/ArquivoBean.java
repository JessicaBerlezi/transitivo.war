package ws;

import java.util.Date;

import javax.activation.DataHandler;

/**
 * @author Pedro N�brega
 */

public class ArquivoBean {
	
	private String codArquivo;
	private String nomArquivo;
	private int qtdReg;
	private int qtdFoto;
	private Date datRecebimento;
	private DataHandler arquivo;
	private String numAuto;
	private String erro;
	private Date datErro;
	private boolean autenticacao;
	
	public ArquivoBean()
	{
		codArquivo = "";
		nomArquivo = "";
		qtdReg = 0;
		qtdFoto = 0;
		datRecebimento = null;
		arquivo = null;
		numAuto = "";
		erro = "";
		datErro = null;
		autenticacao = true;
	}
	
	public String getCodArquivo() {
		return codArquivo;
	}
	
	public void setCodArquivo(String codArquivo) {
		this.codArquivo = codArquivo;
	}
	
	public String getNomArquivo() {
		return nomArquivo;
	}
	
	public void setNomArquivo(String nomArquivo) {
		this.nomArquivo = nomArquivo;
	}
	
	public int getQtdReg() {
		return qtdReg;
	}
	
	public void setQtdReg(int qtdReg) {
		this.qtdReg = qtdReg;
	}
	
	public int getQtdFoto() {
		return qtdFoto;
	}
	
	public void setQtdFoto(int qtdFoto) {
		this.qtdFoto = qtdFoto;
	}
	
	public Date getDatRecebimento() {
		return datRecebimento;
	}
	
	public void setDatRecebimento(Date datRecebimento) {
		this.datRecebimento = datRecebimento;
	}
	
	public DataHandler getArquivo() {
		return arquivo;
	}
	
	public void setArquivo(DataHandler arquivo) {
		this.arquivo = arquivo;
	}
	
	public String getNumAuto() {
		return numAuto;
	}
	
	public void setNumAuto(String numAuto) {
		this.numAuto = numAuto;
	}
	
	public String getErro() {
		return erro;
	}
	
	public void setErro(String erro) {
		this.erro = erro;
	}
	
	public Date getDatErro() {
		return datErro;
	}

	public void setDatErro(Date datErro) {
		this.datErro = datErro;
	}

	public boolean isAutenticacao() {
		return autenticacao;
	}
	
	public void setAutenticacao(boolean autenticacao) {
		this.autenticacao = autenticacao;
	}
	
}