package ws;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;

import org.apache.axis.MessageContext;
import org.apache.axis.session.Session;

import REG.LinhaArquivoRec;

/**
 * Altera��o para contemplar o download das imagens para o contrato Perrone, associado ao login
 *  @author Jefferson.trindade
 */

public class CopiaFotoService {
	
	public ArquivoBean[] listarArquivos(String codOrgao, String codOrgaoAtuacao, String nomUsuario,
			String senha, String dataInicial, String dataFinal) throws sys.BeanException 
			{
		ArquivoBean[] arquivosRecebidos = {};
		ArquivoBean beanComErro = new ArquivoBean();
		try{
			//Valida o usu�rio e a senha passada
			ACSS.UsuarioBean usuarioBean = new ACSS.UsuarioBean();
			usuarioBean.setNomUserName(nomUsuario);
			ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
			orgaoBean.setCodOrgao(codOrgao);
			usuarioBean.setCodOrgaoAtuacao(codOrgaoAtuacao);
			usuarioBean.setOrgao(orgaoBean);
			usuarioBean.Le_Usuario(1);            
			if (!usuarioBean.isSenhaValid(senha)){
				beanComErro.setAutenticacao(false);
				throw new sys.BeanException("07-Usu�rio ou senha inv�lida !");	
			}
			REG.ArquivoRecebidoBean arquivoRecebidoBean = new REG.ArquivoRecebidoBean();
			arquivoRecebidoBean.setCodOrgao(codOrgao);
			arquivoRecebidoBean.setCodOrgaoAtuacao(usuarioBean.getCodOrgaoAtuacao());
			if (codOrgao.equals("56"))
				arquivoRecebidoBean.setCodIdentArquivo("EMITEVEX");
			if (codOrgao.equals("58"))
				arquivoRecebidoBean.setCodIdentArquivo("EMITEPER");
			
			arquivosRecebidos = arquivoRecebidoBean.buscarArquivosRecebidos(dataInicial, dataFinal);
		}
		catch (Exception e) {
			beanComErro.setErro(e.getMessage());
			ArquivoBean[] arquivosRecebidosTemp = {beanComErro};
			arquivosRecebidos = arquivosRecebidosTemp;
		}
		return arquivosRecebidos;
	}
	
	/**
	 * 
	 * @param codArquivo
	 * @param autosExistentes
	 * @param autosIgnorados
	 * @return
	 * @throws sys.BeanException
	 */
	public ArquivoBean copiarFotos(String codArquivo, Set autosExistentes, Set autosIgnorados) throws sys.BeanException
	{
		String numAuto = "";
		try {
			//Pega os parametros de Sistema
			ACSS.ParamSistemaBean param = new ACSS.ParamSistemaBean();
			param.setCodSistema("45"); //M�dulo Recurso 
			param.PreparaParam();
			
			Session session = MessageContext.getCurrentContext().getSession();
			REG.ArquivoRecebidoBean arquivoRecebido = (REG.ArquivoRecebidoBean) session.get("Arquivo");
			if (arquivoRecebido == null || !arquivoRecebido.getCodArquivo().equals(codArquivo)) { 
				arquivoRecebido = new REG.ArquivoRecebidoBean();
				arquivoRecebido.setCodArquivo(codArquivo);
				arquivoRecebido.listarLinhasComFoto();
				session.set("Arquivo", arquivoRecebido);
			}
			
			List linhas = arquivoRecebido.getLinhaArquivoRec();
			for (int i = 0; i < linhas.size(); i++) {
				LinhaArquivoRec linhaRec = (LinhaArquivoRec) linhas.get(i);
				numAuto = linhaRec.getNumAutoInfracao();
				if (autosIgnorados.contains(numAuto))
					continue;
				if (autosExistentes.contains(numAuto))
					continue;
				String[] campos = {"num_auto_infracao"};
				String numAutoAux = "'" + numAuto + "'";
				String[] valores = {numAutoAux};
				REC.FotoDigitalizadaBean[] fotos = REC.FotoDigitalizadaBean.consultaFotos(campos, valores); 
				if (fotos.length > 0) {
					File arquivo = new File(fotos[0].getArquivo(param));
					if(!arquivo.exists())
						throw new sys.BeanException("N�o existe foto para o auto " + numAuto);
					
					/*REG.Dao.getInstance().GravaDataDownloadFoto(numAuto,arquivoRecebido.getCodArquivo()); 06/08/2009*/
					
					ArquivoBean autoArquivoBean = new ArquivoBean();
					autoArquivoBean.setNomArquivo(numAuto + ".jpg");
					autoArquivoBean.setArquivo(new DataHandler(new FileDataSource(arquivo)));
					return autoArquivoBean;				
				}
				else if (!autosExistentes.contains(numAuto))
					throw new sys.BeanException("N�o existe foto para o auto " + numAuto);
			}
		}catch(sys.BeanException ex){
			ArquivoBean autoArquivoBean = new ArquivoBean();
			autoArquivoBean.setCodArquivo(codArquivo);
			autoArquivoBean.setNumAuto(numAuto);
			autoArquivoBean.setErro(ex.getMessage());
			autoArquivoBean.setDatErro(new Date());
			return autoArquivoBean;		
		} 
		catch (Exception e) {
			ArquivoBean autoArquivoBean = new ArquivoBean();
			autoArquivoBean.setErro(e.getMessage());
			autoArquivoBean.setDatErro(new Date());
			return autoArquivoBean;
		} 
		return null;
	}
	
}