<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import = "java.util.List" %>
<%@ page import = "java.util.Iterator" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama os demais beans -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:useBean id="NivUsrId" scope="request" class="REC.NivelUsuarioRelatorBean" />
<!-- Chama o Objeto da Tabela de Junta --> 
<jsp:useBean id="JuntaId" scope="request" class="REC.JuntaBean" /> 
<jsp:useBean id="RelatorId" scope="request" class="REC.RelatorBean" /> 
<html>    
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myJunta      = new Array(<%= JuntaId.getJunta(UsuarioBeanId) %>);
cod_junta    = new Array(myJunta.length/4);
cod_juntaOrg = new Array(myJunta.length/4);
sig_junta    = new Array(myJunta.length/4);

for (i=0; i<myJunta.length/4; i++) {
	cod_junta[i]    = myJunta[i*4+0] ;
	cod_juntaOrg[i] = myJunta[i*4+1] ;	
	sig_junta[i]    = myJunta[i*4+3] ;
}


<!-- Carrega e monta a combo-->
myRelat     = new Array(<%= RelatorId.getRelatores(JuntaId) %>);
cod_relator = new Array(myRelat.length/2);
nom_relator = new Array(myRelat.length/2);
for (i=0; i<myRelat.length/2; i++) {
	cod_relator[i] = myRelat[i*2+0] ;
	nom_relator[i] = myRelat[i*2+1] ;
}


mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

myProcesso = new Array(<%= SistId.getProcessos(  SistId.getCodSistema()  )  %>);
cod_processo  = new Array();
nom_processo  = new Array();
for (i=0; i<myProcesso.length/2; i++)
{
	cod_processo[i] = myProcesso[i*2+0];
	nom_processo[i] = myProcesso[i*2+1];
}

myNivel = new Array(<%= NivUsrId.getNiveis(  SistId.getCodProcesso()  )  %>);
cod_nivel  = new Array();
nom_nivel  = new Array();
for (i=0; i<myNivel.length/2; i++)
{
	cod_nivel[i] = myNivel[i*2+0];
	nom_nivel[i] = myNivel[i*2+1];
}


var bProcessando=false;

function valida(opcao,fForm) 
{

	switch (opcao) 
	{
		case 'buscaRelator':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   
				if (fForm.codJunta.value==0) 
				{
					alert("Junta n�o selecionada. \n")
				}
				else 
				{	
					fForm.acao.value=opcao
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				}
			}
			break ; 
	  
		case 'buscaNivel':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   		
				if (fForm.codRelator.value==0) 
				{
					alert("Relator n�o selecionado. \n")
				}
				else 
				{
					carregaDescricao(fForm);	  
					fForm.acao.value=opcao
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
			}
			break ; 	  
	  
		case 'A':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   		
				for (i=0;i<fForm.usuariosPorNivel.length;i++)
					fForm.usuariosPorNivel[i].selected=true;
   
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
				else 
					alert(sErro)
			}
			break ;
	  
		case 'I':
	      	fForm.acao.value=opcao;
		   	fForm.target= "_blank";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
			break ;	  
	  
		case 'voltar':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   		
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	
			}
			break;
			
		case 'R':	
			close();
			break;
		
		case 'O':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
      		else                 
      			document.all["MsgErro"].style.visibility='hidden' ; 
			break;  
		
		case 'V':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='show' ; 
			else                 
				document.all["MsgErro"].style.visibility='visible' ; 
			break;	  
	}
}

function atualiza(opcao,fForm) 
{
	if (veCampos(fForm)==false) 
		return ;

	switch (opcao) 
	{
		case 'IT':
			ApagaSelect(fForm.usuariosPorNivel,true)    	
			CopiaSelect(fForm.tempusuarios,fForm.usuariosPorNivel,true)
			ApagaSelect(fForm.usuarios,true)    	
			break; 
			
	   case 'ET':   
			ApagaSelect(fForm.usuarios,true)    	
			CopiaSelect(fForm.tempusuarios,fForm.usuarios,true)
			ApagaSelect(fForm.usuariosPorNivel,true)    	
			break; 	
		
	   case 'Inclui':
			DeselecionaSelect(fForm.tempusuarios,fForm.tempusuarios,false) 
			SelecionaSelect(fForm.usuariosPorNivel,fForm.tempusuarios,true) 	
			SelecionaSelect(fForm.usuarios,fForm.tempusuarios,false)
			ApagaSelect(fForm.usuariosPorNivel,true)    	
	    	CopiaSelect(fForm.tempusuarios,fForm.usuariosPorNivel,false)	
			ApagaSelect(fForm.usuarios,false)   

	   case 'Exclui':  
			DeselecionaSelect(fForm.tempusuarios,fForm.tempusuarios,false) 
			SelecionaSelect(fForm.usuarios,fForm.tempusuarios,true) 	
			SelecionaSelect(fForm.usuariosPorNivel,fForm.tempusuarios,false) 
			ApagaSelect(fForm.usuarios,true)    							
			CopiaSelect(fForm.tempusuarios,fForm.usuarios,false)	
			ApagaSelect(fForm.usuariosPorNivel,false)
	}	
}

function carregaDescricao(fForm)
{
	with(fForm){
	indice = codRelator.selectedIndex;
	nomRelator.value = codRelator.options[indice].text;
	}
}



function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	if (fForm.codJunta.value==0) {
	   sErro += "Junta n�o selecionada. \n"
	   valid = false;
	}
	if (fForm.codRelator.value==0) {
	   sErro += "Relator n�o selecionado. \n"
	   valid = false;
	}	
	if (valid==false) alert(sErro) ;
	return valid ;
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="parametroForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />

<input name="acao" type="hidden" value=' '>				
<input name="atualizarDependente" type="hidden" value='<%= RelatorId.getAtualizarDependente() %>'> 
<input name="nomRelator" type="hidden" value='<%=RelatorId.getNomRelator() %>'> 

  <div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 78px; left=35px;left: 50px;" > 
    <table border="0" cellpadding="0" cellspacing="1" width=720  align="center">
      <tr>
        <td width="66"  align="left" valign="top"><b>&nbsp;Junta:&nbsp;</b></td>
		<td width="316" align="left" valign="top">
   			<% if (RelatorId.getAtualizarDependente().equals("N")){ %>				  
				<select name="codJunta" onChange="javascript: valida('buscaRelator',this.form);"> 
				   <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				       <script>
					      for (j=0;j<cod_junta.length;j++) {
						    document.write ("<option value="+cod_junta[j]+">"+sig_junta[j])+"</option>"
					      }
				       </script>
				</select>
          <% }
			else { %>
              <input name="codJunta" type="hidden" size="10" maxlength="20"  value="<%=JuntaId.getCodJunta()%>"> 
		      <input disabled name="sigJunta" style="border: 0px none;" type="text" size="50" value="<%= JuntaId.getSigJunta() %>">
	   	<% } %></td>
	    

        <td width="334" align="left" valign="top"><b>&nbsp;Relatores:&nbsp;</b>    		
   			<% 	if (RelatorId.getAtualizarDependente().equals("S")==false){ %>					  
				<select name="codRelator" onChange="javascript: valida('buscaNivel',this.form);"> 
				   <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				       <script>
					      for (j=0;j<cod_relator.length;j++) {
						    document.write ("<option value="+cod_relator[j]+">"+nom_relator[j])+"</option>"
					      }
				       </script>
				</select>
          <% }
			else { %>
              <input name="codRelator" type="hidden" size="10" maxlength="20"  value="<%= RelatorId.getCodRelator() %>"> 
		      <input disabled name="nomRelator" style="border: 0px none;" type="text" size="50" value="<%= RelatorId.getNomRelator() %>">              
	   	<% } %></td>
	  </tr>
	  <tr> 
        <td valign="top" colspan="3" height="5"><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
	  <tr>
		        <td width="66" align="left" valign="top"><b></b> </td>
				<td width="316" align="left" valign="top"> </td>

 
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="400" align="center">
    <TR>
		<% if (RelatorId.getAtualizarDependente().equals("S")){ %>			
	 	   <td align="left" width="33%"  height="15" > 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
     	   </td>
	       <td  align="center" width="34%" > 
                  <button type=button NAME="Imprimir"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('voltar',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
            <%} %>
    </TR>
  </TABLE>  
<!--FIM BOTOES-->  
</div>
 <div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 137px; left: 50px;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">  	   		
    <tr><td colspan=6 height=3></td></tr>		  		
	<tr> 		 	  
	   	 <td width=41% align="center"  bgcolor="#be7272"><font color="#ffffff"><b>USU&Aacute;RIOS</b></font></TD>		 
	   	 <td width=14%></TD>		 
	   	 <td width=45% align="center"  bgcolor="#be7272"><font color="#ffffff"><b>ACESSO &Agrave; GUIA DE DISTRIBUI�&Atilde;O </b></font></TD>		 
	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->
<div style="position:absolute; width:720px; overflow: hidden; z-index: 1; top: 155px; left: 51px; height: 212px;">
  <table border="0" cellpadding="0" cellspacing="0" width=100% height="212" align="center">  	   		
    <tr><td colspan="6" height="3"></td></tr>		  		
	<tr height="48">
		 <td width="41%" align="center"  bgcolor="#faeae5" rowspan="4">
		  <table bgcolor="#faeae5" border="0" cellpadding="0" cellspacing="0" width="100%"  align="left"> 
		  	<tr bgcolor="#faeae5"><td align="center" bgcolor="#faeae5">
<%
   if (RelatorId.getAtualizarDependente().equals("S")== true){
       List usuarios = (List)request.getAttribute("usuarios");
       if (usuarios.size()>0) {
			out.println("<select name=\"usuarios\" multiple size=10 style=\"width: 240px \">");
			Iterator it = usuarios.iterator();
			int cont = 0;
			REC.NivelUsuarioRelatorBean myUsuario = new REC.NivelUsuarioRelatorBean();
				while (it.hasNext()){
					myUsuario = (REC.NivelUsuarioRelatorBean)it.next();
					Iterator itx = NivUsrId.getListNivelDestino().iterator();
	         		REC.NivelUsuarioRelatorBean myNivelUsuario = new REC.NivelUsuarioRelatorBean();
	    			while (itx.hasNext()){
	    				myNivelUsuario = (REC.NivelUsuarioRelatorBean)itx.next();
	    				
	    				if(myUsuario.getCodUsuario().equals(myNivelUsuario.getCodUsuario())){
	    					cont++;
	    					break;
	    				}
	    			}
	    			if(cont == 0)
	    			  out.println("<option value=\""+myUsuario.getCodUsuario()+"\">"+myUsuario.getNomUsuario()+"</option>");
	    			cont = 0;
			}
			out.println("</select>");
		}
   }
		 
%>
			</td>
		  	</tr>
	    </table>		
	    </td>
	   	<td width=14%>
		   <div align="center">
				<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="FimImg"  onClick="javascript: atualiza('IT',this.form);">
				<img alt="Inclui Todas" src="<%= path %>/images/bot_final.gif" width="20" height="19">
				</button>
		   </div></td>				
 	   	 <td width=45% align=center  bgcolor="#faeae5" rowspan=4>
		  <table bgcolor="#faeae5" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
		  	<tr bgcolor="#faeae5"><td align=center bgcolor="#faeae5">

<%          if (RelatorId.getAtualizarDependente().equals("S")== true){ 
         	  if (NivUsrId.getListNivelDestino().size()>0) {
         		out.println("<select name=\"usuariosPorNivel\" multiple size=10 style=\"width: 240px \">");
         		Iterator it = NivUsrId.getListNivelDestino().iterator();
    			REC.NivelUsuarioRelatorBean myNivelUsuario = new REC.NivelUsuarioRelatorBean();
    			while (it.hasNext()){
    				myNivelUsuario = (REC.NivelUsuarioRelatorBean)it.next();
    				out.println("<option value=\""+myNivelUsuario.getCodUsuario()+"\">"+myNivelUsuario.getNomUsuario()+"</option>");
         	    }
    			out.println("</select>");
         	  }
         	  else {%>
         	  <select  name="usuariosPorNivel" multiple size=10  style="width: 240px ">              
                 <option  value=""></option>
              </select>
              
           <% }
           }%>           

			</td></tr>
	    </table>		</td>
	</tr>	
	<tr height="48"><td align="center">
			<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="ProximoImg"  onClick="javascript: atualiza('Inclui',this.form);">
			<img alt="Inclui" src="<%= path %>/images/bot_prox.gif" width="20" height="19">
			</button>
		</td>				
    </tr>
	<tr height="48"><td align="center">
			<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="AnteriorImg"  onClick="javascript: atualiza('Exclui',this.form);">
			<img alt="Exclui" src="<%= path %>/images/bot_ant.gif" width="20" height="19">
			</button>
		</td>				
	</tr>
	<tr height="48"><td align="center">
			<button type="button" style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="InicioImg"  onClick="javascript: atualiza('ET',this.form);"> 
          <img alt="Exclui Todas" src="<%= path %>/images/bot_inicial.gif" width="20" height="19"> 
          </button>
		</td>
	</tr>		
  </table>
<input name="atualizarDependente" type="hidden" size="1" value="<%= RelatorId.getAtualizarDependente() %>"> 
</div>

<!--FIM_CORPO_sistema-->
  
<!--Div Erros-->
<div style="position:absolute; width:100%; overflow: auto; z-index: 1; top: 233px; height=195px; visibility='hidden';left: 20px;">
  <table bgcolor="#faeae5" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
  	<tr bgcolor="#faeae5"><td align=center bgcolor="#faeae5">
      <% 
	if (RelatorId.getAtualizarDependente().equals("S")== true){
	    List usuarios = (List)request.getAttribute("usuarios");
	    if (usuarios.size()>0) {
		   out.println("<select name=\"tempusuarios\" multiple size=15 >");
		   Iterator it = usuarios.iterator();
		   REC.NivelUsuarioRelatorBean myUsuario = new REC.NivelUsuarioRelatorBean();
		   while (it.hasNext()){
			   myUsuario = (REC.NivelUsuarioRelatorBean)it.next();
			   out.println("<option value=\""+myUsuario.getCodUsuario()+"\">"+myUsuario.getNomUsuario()+"</option>");
		   }
		   out.println("</select>");
        }
	}
%>
	</td></tr>
  </table>	
</div>


<jsp:include page="Retornar.jsp" flush="true" />
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= NivUsrId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "100 px" />
  <jsp:param name="msgErroLeft" value= "40 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= JuntaId.getCodJunta() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.parametroForm.codJunta.length;i++){
    if(document.parametroForm.codJunta.options[i].value==njint){
       document.parametroForm.codJunta.selectedIndex=i;	  
    }
  }
}  
nj = '<%= RelatorId.getCodRelator() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.parametroForm.codRelator.length;i++){ 
    if(document.parametroForm.codRelator.options[i].value==njint){ 
       document.parametroForm.codRelator.selectedIndex=i;		
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


