<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="REC.JuntaBean"%> 

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" /> 
<jsp:useBean id="JuntaId" scope="request" class="REC.JuntaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaOrgao':
		if (fForm.codOrgao.value==0) {
		   alert("�rg�o n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else alert(sErro)
	  break ;
   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	  
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
     fForm.acao.value=opcao
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codOrgao.value==0) {
   valid = false
   sErro = sErro + "�rg�o n�o selecionado. \n"
}

<!--valida sigla se for vazia-->
for (k=0;k<fForm.sigJunta.length;k++) {
   var sigla = trim(fForm.sigJunta[k].value);
   var junta = trim(fForm.nomJunta[k].value);
   var tipo = trim(fForm.indTipo[k].value);
   if(sigla=="" && junta!=""){
   	 valid = false
   	 sErro = sErro + "Sigla deve ser preenchida. \nVerifique a seq "+(k+1)+" \n";
   }
  if(junta==""&& sigla!=""){
    valid = false
    sErro = sErro + "O nome da junta deve ser preenchida. \nVerifique a seq "+(k+1)+" \n";
  }
  if(tipo!="0" && tipo!="1" && tipo!="2"){
    valid = false
    sErro = sErro + "Tipo deve ser '0'- Def.Pr�via, '1'- 1a.Instancia ou '2'- 2a.Instancia. \nVerifique a seq "+(k+1)+" \n";
  }

}

	
for (k=0;k<fForm.nomJunta.length;k++) {
   var ne = trim(fForm.nomJunta[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.nomJunta.length;m++) {
	   var dup = trim(fForm.nomJunta[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Junta em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 75px; left:204px;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="400"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>     
	  </tr>
      <tr>
		  <td align="left" width="60%"><b>�rg�o:&nbsp;</b>
		    <input name="atualizarDependente" type="hidden" size="1" value="<%= (String)request.getAttribute("atualizarDependente") %>"> 
     			<% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>				  
				<select name="codOrgao"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_orgao.length;j++) {
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
			<% }
			else { %>
		        <input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
		        <input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
			<% } %>			
		  </td>
   		  <% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>			  
 	         <td align=right width=350>&nbsp;&nbsp;&nbsp;
			  	<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaOrgao',this.form);">	
	    	    <IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19"></button>																			
			 </td>
		<% } %>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
		<% if (((String)request.getAttribute("atualizarDependente")).equals("S")){ %>			
	 	   <td width="33%" align="left"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
     	   </td>
	       <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
		<%} %>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 140px; height: 32px"> 
    <table border="0" cellpadding="0" cellspacing="1" width="720"  align="center">  	   		
	<tr bgcolor="#993300"> 
        <td width=100 align="center" bgcolor="#be7272"><font color="#ffffff"><b>Seq</b></font></TD>		 	  
        <td width=350 align="center" bgcolor="#be7272"><font color="#ffffff"><b>Junta</b></font></TD>		 
        <td width=120  align="center" bgcolor="#be7272"><font color="#ffffff"><b>Sigla</b></font></TD>		 
        <td           align="center" bgcolor="#be7272"><font color="#ffffff"><b>Tipo</b></font></TD>		 
	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 155px; height: 191px;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (((String)request.getAttribute("atualizarDependente")).equals("S")) {				   
  	            	   List myJuntas = (List)request.getAttribute("myJuntas");
					   for (int i=0; i<myJuntas.size(); i++) {	   %>
      <tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
          <input name="seq" type="text" readonly size="10" value="<%= i+1 %>">
		    <input name="codJunta" type="hidden"  value="<%= ((JuntaBean)myJuntas.get(i)).getCodJunta()%>">
        </td>
        <td width=350 align="center"> 
          <input name="nomJunta" type="text" size="20" maxlength="20"  value="<%= ((JuntaBean)myJuntas.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
          <input name="nomJunta_aux" type="hidden" size="20" maxlength="20"  value="<%= ((JuntaBean)myJuntas.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
    	<td width=120  align="center"> 
          <input name="sigJunta" type="text" size="10" maxlength="10"  value="<%= ((JuntaBean)myJuntas.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
          <input name="sigJunta_aux" type="hidden" size="10" maxlength="10"  value="<%= ((JuntaBean)myJuntas.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
    	<td align="center"> 
          <input name="indTipo" type="text" size="5" maxlength="1"  value="<%= ((JuntaBean)myJuntas.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" >
          <input name="indTipo_aux" type="hidden" size="5" maxlength="1"  value="<%= ((JuntaBean)myJuntas.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" >
        </td>
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
          <input name="seq"  readonly  type="text" size="10" value="<%= i+1 %>">
        </td>
	   	<td width=350 align=center> 
          <input name="nomJunta" readonly type="text" size="23" maxlength="20" value="">
        </td>			
    	<td width=120 align=center> 
          <input name="sigJunta" readonly type="text" size="14" maxlength="10" value=""></td>
    	<td align="center"> 
          <input name="indTipo"  readonly type="text" size="5"  maxlength="1"  value="0"></td>
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%=JuntaId.getMsgErro()%>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>


