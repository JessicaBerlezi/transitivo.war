<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{    
		case 'ImprimirRemessa':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
				
				fForm.acao.value = opcao;
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();	 
			}
			break;
		case 'R':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;			

				fForm.acao.value ="R";
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();
		   	}
			break;		  

	   case 'O':  // Esconder os erro
   		  if (document.layers) 
   		  	fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 
	      	document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
		  
	   case 'Novo':  // Esconder os erro
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;		   
				
				fForm.acao.value = opcao;
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();	 
			}
			break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
			break;  
		  
  }
}

function Classificacao(ordem,fForm)  
{
	if(bProcessando)
	{
		alert("Opera��o em Processamento, por favor aguarde...");
		return;
	}
	else
	{
		bProcessando=true;	

		document.all["acao"].value  = "Classifica";
		document.all["ordem"].value = ordem;
		document.all["PreparaGuiaForm"].target = "_self";
		document.all["PreparaGuiaForm"].action = "acessoTool";  
		document.all["PreparaGuiaForm"].submit() ;	 
	}
}


function Mostra(opcoes,fForm,numplaca,numauto) {	
	document.all["acao"].value          = opcoes;
	document.all["mostraplaca"].value   = numplaca;
	document.all["mostranumauto"].value = numauto;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}
function SomaSelec() {
}
function marcaTodos(obj) {
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="PreparaGuiaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"           type="hidden" value=' '>
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#faeae5">
        <td width="10%"></td>		     
        <td width="60%" height=23 align=center><b><%=RemessaId.getNFuncao()%> &nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=RemessaId.getCodRemessa()%>
			&nbsp;&nbsp;-&nbsp;&nbsp;<%=RemessaId.getDatProc()%>
	
        </td>
        <td width="10%" align="center" valign="middle">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimirRemessa',this.form)"> 
              <img src="<%= path %>/images/REC/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">
			</button>
		</td>	
		<td align=center  width="20%">
		  	
		  </td>	
 	 </tr>
     <tr><td colspan=4 height=5></td></tr>
     <tr bgcolor="#faeae5">
     	<td colspan=4>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" >
	     	<tr>
		        <td width="50%" height=20></td>
		        <td width="50%" align=right>&nbsp;</td>        
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=4 height=5></td></tr>     
  </table> 
</div>
<jsp:include page="RemessaCorpo.jsp" flush="true" >  
  <jsp:param name="posTop" value= "185 px" />
  <jsp:param name="posHei" value= "167 px" />
</jsp:include> 

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= RemessaId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= RemessaId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>


