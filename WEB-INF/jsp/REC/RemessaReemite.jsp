<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 
	<jsp:setProperty name="RemessaId" property="j_abrevSist" value="REC" />  
	<jsp:setProperty name="RemessaId" property="colunaValue" value="cod_remessa" />
	<jsp:setProperty name="RemessaId" property="popupNome"   value="numRemessa"  />


<% 
     String cm = "SELECT cod_Remessa,IND_STATUS_REMESSA,' Guia: '||cod_Remessa||' Status: '||decode(IND_status_REMESSA,0,'Envio',1,'Recebimento') numRemessa";     
     cm += " FROM TSMI_REMESSA";    
     
     if("REC0134".equals(UsuarioFuncBeanId.getJ_sigFuncao()))
    	 cm += " WHERE ind_STATUS_remessa =0" ; 
     else if ("REC0202".equals(UsuarioFuncBeanId.getJ_sigFuncao())||"REC0311".equals(UsuarioFuncBeanId.getJ_sigFuncao())
    		 ||"REC0402".equals(UsuarioFuncBeanId.getJ_sigFuncao()))
     	cm += " WHERE ind_STATUS_remessa =0" ;     
     else
    	cm+= " WHERE ind_STATUS_remessa in (0,1)";

     if ( ("REC0202".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0203".equals(UsuarioFuncBeanId.getJ_sigFuncao())) )
       cm+= " and IND_RECURSO=5";
       
     if ( ("REC0311".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0312".equals(UsuarioFuncBeanId.getJ_sigFuncao())) )
       cm+= " and IND_RECURSO=15 ";
       
     if ( ("REC0402".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0403".equals(UsuarioFuncBeanId.getJ_sigFuncao())) )
       cm+= " and IND_RECURSO=35 ";
       
     if ( ("REC0206".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0207".equals(UsuarioFuncBeanId.getJ_sigFuncao())) )
       cm+= " and IND_RECURSO=98";
       
     if ( ("REC0314".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0315".equals(UsuarioFuncBeanId.getJ_sigFuncao())))
       cm+= " and IND_RECURSO=96 ";
       
     if ( ("REC0405".equals(UsuarioFuncBeanId.getJ_sigFuncao())) || ("REC0406".equals(UsuarioFuncBeanId.getJ_sigFuncao())))
       cm+= " and IND_RECURSO=97 ";
     
      
     cm += " AND DAT_PROC BETWEEN TO_DATE('"+RemessaId.getDataIni()+"','DD/MM/YYYY') AND TO_DATE('"+RemessaId.getDataFim()+"','DD/MM/YYYY') "+ 
    	 " ORDER BY COD_REMESSA DESC";
     	
	 RemessaId.setPopupString("numRemessa,"+cm);     
	 
%>   

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'LeRemessa':
		if (vCampos(fForm)) {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'R':
	  fForm.acao.value=opcao
	  fForm.target= "_self";
	  fForm.action = "acessoTool";  
	  fForm.submit();	  		  
      close() ;   
	  break;
   case 'consulta':
      if(vDatas(fForm)){
		  fForm.acao.value=opcao
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	
	  }  		  
	  break;		  
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}
function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.numRemessa,"Guia",0);
    if (valid==false) 
      alert(sErro) ;
    else     
      vDatas(fForm);
	return valid ;
}
function vDatas(fForm){
    valid = true ;
	sErro = "" ;
	verString(fForm.De,"Data Inicial",0);
	verString(fForm.Ate,"Data Final",0);
	if(valid){
		if (comparaDatas(fForm.De.value,fForm.Ate.value)==false){
	       valid = false
	       sErro += "Data final n�o pode ser inferior a data inicial. \n"
		   document.all.Ate.focus();       
	    }
    }
	if (valid==false) alert(sErro) ;
	return valid ;
}
function comparaDatas(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:740px; overflow: visible; z-index: 1; top: 100px; left:50px;" > 
   <table border="0" cellpadding="0" cellspacing="1" width="740"  align="center">    	   
	  <tr><td valign="top" colspan=2 height="10"></td></tr>	
	  <tr>
		  <td width="271" ><b>Data:&nbsp;</b>&nbsp;
		    <input name="De" type="text" id="De" size="12" maxlength="12" value="<%=RemessaId.getDataIni()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);javascript: valida('consulta',this.form);" onfocus="javascript:this.select();">
			  &nbsp;<strong>at�&nbsp;</strong>&nbsp;
			  <input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=RemessaId.getDataFim()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);javascript: valida('consulta',this.form);" onfocus="javascript:this.select();">
		  </td>
      </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="1" width="740"  align="center">    
      <tr><td valign="top" colspan="2" height="15"></td></tr>
      <tr>
		  <td align="left" width="90%"><b>Guia:&nbsp;</b>
				<jsp:getProperty name="RemessaId" property="popupString" />	
		  </td>
          <td align=center  width="10%">&nbsp;&nbsp;&nbsp;
			  	<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('LeRemessa',this.form);">	
	    	    <IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19"></button>																			
		  </td>
      </tr>	
      <tr><td valign="top" colspan=2 height="2"></td></tr>	 
  </table>
</div>

<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= RemessaId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 

</form>
</BODY>
</HTML>


