<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="ParecerJuridicoId"  scope="request" class="REC.ParecerJuridicoBean" /> 
<%			ParecerJuridicoId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="CssImpressao.jsp" flush="true" /> 
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ParecJurForm" method="post" action="">
<jsp:include page="AutoImp.jsp" flush="true" />
<input name="acao"         type="hidden" value=' '>				
<input name="codParecerPJ" type="hidden" value="">				

<!--IN�CIO_CORPO_sistema--> 
<% if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0) { %> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr >
	  	<% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>			 	 
	        <td height=23 valign="middle" >&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
    	    <td align="left" valign="middle"> 
				<%=request.getAttribute("Requerimento") %>
	        </td>
	        <td >Data do Parecer:&nbsp;
			  <%=RequerimentoId.getDatPJ() %>
			</td>
		<% } else { %>
    	    <td height=23 colspan=3 align="left" valign="middle"> 
				<jsp:getProperty name="ParecerJuridicoId" property="msg" />
	        </td>	
		<% } %>
     </tr>
  	 <% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>					 
     <tr><td colspan=3 height=2></td></tr>
     <tr >
        <td  height=20 width="13%" valign="middle">&nbsp;&nbsp;Respons&aacute;vel:&nbsp;&nbsp;</td>
    	<td width="50%">		
				<%=request.getAttribute("Responsavel") %>
	    <td width="37%" valign="middle">Parecer :&nbsp;&nbsp;&nbsp;         	  
			  <%= ("D".equals(RequerimentoId.getCodParecerPJ()) ? "Deferido" : ("I".equals(RequerimentoId.getCodParecerPJ()) ? "Indeferido" : "" )) %>	
		</td> 		
     </tr>
	<% }  %> 
  </table>
  <% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>					    
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan=3 height=2></td></tr>
      <tr >
	        <td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
    	    <td height=25 width="80%" > 			  
				<%=RequerimentoId.getTxtMotivoPJ() %>
	        </td>
    	    <td width="7%"  align="right" valign="middle"> 	        	  
			</td>
      </tr>
  </table>	
  <% } %>  	
<% } %>
<!--FIM_CORPO_sistema--> 
</form>
</body>
</html>