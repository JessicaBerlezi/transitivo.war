<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="EstornoAbrirRecursoId"   scope="request" class="REC.EstornoAbrirRecursoBean" /> 
<%			EstornoAbrirRecursoId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'LeReq':
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	   
		  break;
	   case 'EstornaRecurso':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	      fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.codRequerimento,"Requerimento",0);
	valid = ValDt(fForm.datEST,1) && valid
	verString(fForm.datEST,"Data do Estorno",0);
	verString(fForm.txtMotivoEST,"Motivo",0);	
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"        type="hidden" value=' '>

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;">  
<% if ("S".equals(EstornoAbrirRecursoId.getEventoOK())) { %>			 	 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
	    <td valign="middle" >&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
        <td align="left" valign="middle"> 
			<jsp:getProperty name="EstornoAbrirRecursoId" property="reqs" />
	    </td>
	    <td >Data do Estorno:&nbsp;
		  <input type="text" name="datEST" size="12" maxlength="10" value='<%=RequerimentoId.getDatEST() %>' onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
	    <td></td>		
     </tr>
     <tr><td colspan=3 height=1></td></tr>
     <tr bgcolor="#faeae5">
   
   
         <% if ("REC0810,REC0811,REC0812,REC0820,REC0830".indexOf(UsuarioFuncBeanId.getJ_sigFuncao())>= 0 ) { %>
      
        </tr>
  			</table>
  			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     		<tr><td colspan=3 height=3></td></tr>
      		<tr bgcolor="#faeae5">			
  			<td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
        	<td width="80%" valign="top"> 
		  		<textarea rows=2 style="border-style: outset;" name="txtMotivoEST" cols="105" 
				 onfocus="javascript:this.select();"><%=RequerimentoId.getTxtMotivoEST() %></textarea>
        	</td>
        	<td width="7%"  align="right" valign="middle"> 
		  	<% if ("S".equals(EstornoAbrirRecursoId.getEventoOK())) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('EstornaRecurso',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
			<% } %>
			
			</td>
      	</tr>	  
  	</table>      
        
  		<%}else{%> 
  		<td width="13%">&nbsp;&nbsp;Relator:</td> 
        <td width="50%">
          <input disabled name="nomRelator" type="text" size="60" maxlength="43"  value="<%= RequerimentoId.getNomRelatorJU() %>">		
		</td>
        <td  width="30%" valign="middle">
		</td>
		<td width="7%">
		</td>
     </tr>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan=3 height=3></td></tr>
      <tr bgcolor="#faeae5">
        <td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
        <td width="80%" valign="top"> 
		  <textarea rows=2 style="border-style: outset;" name="txtMotivoEST" cols="105" 
				 onfocus="javascript:this.select();"><%=RequerimentoId.getTxtMotivoEST() %></textarea>
        </td>
        <td width="7%"  align="right" valign="middle"> 
		  	<% if ("S".equals(EstornoAbrirRecursoId.getEventoOK())) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('EstornaRecurso',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
			<% } %>
		</td>
      </tr>	   
  </table>
  		 	
  		<%}%>
<% } else { %>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
    	 <td height=23> 
			<jsp:getProperty name="EstornoAbrirRecursoId" property="msg" />
	     </td>
      </tr>	  
  </table>		
						
<% } %>  
</div>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 

<!--FIM_Div Erros-->
</form>
</body>
</html>
