<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de consulta -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<html>
<head>
<jsp:include page="CssImpressao.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AIForm" method="post" action="">
<input name="acao" type="hidden" value=' '>				
<!--T�TULO DO M�DULO-->
<div align="center">
	  <table width="720" height="23" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td width="12%" align="center"><font color="#000000"><strong>RECURSO&nbsp;&nbsp;<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;&nbsp;Consulta 
          Auto Digitalizado</strong></font></td>
		</tr>
	  </table>
</div>
<!--FIM T�TULO DO M�DULO-->
<!--TELA DE IMPRESSAO RESULTADO-->

<div align="center">
    <table width="720" height="130" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr> 
        <td height="5" colspan="4"></td>
      </tr>
      <tr> 
        <td width="18%" height="25" ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero 
          do Auto</font></strong></td>
        <td width="32%" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getNumAutoInfracao()%> 
        </td>
        <td width="18%" ><strong><font color="#666666">Data de Digitaliza&ccedil;&atilde;o</font></strong></td>
        <td width="32%" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getDataDigitalizacaoString()%> 
        </td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr> 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero 
          do Lote</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getNumLote()%></td>
        <td ><strong><font color="#666666">N&uacute;mero da Caixa</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumCaixa()%></td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr > 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Prateleira</font></strong></td>
        <td><strong><font color="#993300">: </font></strong>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumPrateleira()%></td>
        <td height="25"><strong><font color="#666666">Estante</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumEstante()%> 
        </td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr > 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Bloco</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumBloco()%></td>
        <td height="25"><strong><font color="#666666">Rua</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumRua()%></td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr > 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea</font></strong></td>
        <td colspan="3" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumArea()%></td>
      </tr>
    </table>
</div>
<!--FIM TELA DE IMPRESSAO RESULTADO-->

<!--Chama o dialog de impressao-->
<SCRIPT LANGUAGE="JavaScript1.2">window.print();</script>
 <!--FIM_CORPO_sistema-->
 </form>
</BODY>
</HTML>


