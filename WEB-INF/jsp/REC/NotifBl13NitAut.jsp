<%@ page errorPage="ErrorPage.jsp" %>
<!--parte B2-->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
    <tr> 
    <td align="center" valign="top" class="fontmedia"> 
   </td>
  </tr>
  <tr> 
      
      <td align="center" valign="top"> 
        <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr>   
		<td height="1"></td>
	</tr>
</table>
	
	 
      <table width="100%" border="0" cellpadding=0 cellspacing=1 bordercolor="#000000" class="semborda">
        <tr> 
            <td > 
              <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr> 
                  <td height="10" align="center" bgcolor="#CCCCCC"><strong class="fontmedia">DECLARA&Ccedil;&Atilde;O DE INDICA&Ccedil;&Atilde;O DE REAL INFRATOR - DIRI </strong></td>
              </tr>
            </table>
			</td>
        </tr>
      </table>
      <div align="center"> 
        <table id="espaco" cellpadding=3 cellspacing=1 border="0" width="100%" bordercolor="#ffffff" class="semborda">
          <tr> 
            <td height="5" bgcolor="#CCCCCC">* A presente Notifica&ccedil;&atilde;o da Autua&ccedil;&atilde;o foi emitida em nome do propriet&aacute;rio do ve&iacute;culo cadastrado no DETRAN / RJ e n&atilde;o sendo V. Sa. o autor da infra&ccedil;&atilde;o, ou nos casos em que o ve&iacute;culo esteja em nome de pessoa jur&iacute;dica, proceda da seguinte forma:<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Preencha corretamente a <strong>Declara&ccedil;&atilde;o de Indica&ccedil;&atilde;o de Real Infrator - DIRI</strong>, indicando os dados do verdadeiro autor da infra&ccedil;&atilde;o a que se refere esta notifica&ccedil;&atilde;o, datada e assinada pelo propriet&aacute;rio do ve&iacute;culo e real infrator. Caso isto n&atilde;o seja feito V. Sa. estar&aacute; sujeito a penalidade administrativa.<br>
&nbsp;&nbsp;&nbsp;&nbsp;- Entregue ou envie por correspond�ncia registrada, at� o prazo limite para indetifica��o do condutor infrator, para os Protocolos Avan�ados da SSPTT  localizados na Avenida Visconde do Rio Branco, s/n� - Centro (Terminal Rodovi�rio Jo�o Goulart ), Estrada Francisco da Cruz Nunes 6666 - Piratiniga (Administra��o Regional de Piratininga) e na  Av. Almirante Ari Parreiras, 21 - Icara� (Administra��o Regional de Icara�), todas em Niter�i - RJ, junto com a DIRI, os seguintes documentos do Real Infrator: c�pia da CNH, c�pia do CPF , documento que comprove a assinatura do condutor infrator, quando esta n�o constar na CNH, comprovante de resid�ncia e c�pia do Contrato Social quando Pessoa Jur�dica.<br>
&Eacute; de inteira responsabilidade c&iacute;vel, administrativa e penal, de V.Sa. a veracidade das informa&ccedil;&otilde;es e documentos anexados a DIRI.  </td>
          </tr>
        </table>
        </div>
		<table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">
		<tr>
		<td height="5"></td>
		</tr>
		</table>
		
        <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
          <tr> 
            <td height="1"> </td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding=0 cellspacing=1 bordercolor="#000000" class="semborda">
       <tr> 
          <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="11%" height="32" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>Placa do Ve&iacute;culo </strong></span><br>
&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %> </td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>C&oacute;digo da Infra&ccedil;&atilde;o</strong></span><br>
&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="10%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>Pontos na CNH </strong></span><br>
&nbsp;<%=AutoInfracaoBeanId.getInfracao().getNumPonto() %></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="24%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>Prazo para Identifica&ccedil;&atilde;o do Real Infrator </strong></span><br>
&nbsp;At� 15 dias do  recebimento</td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="15%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>N&ordm; do Auto de Infra&ccedil;&atilde;o<br> 
                  &nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao() %><br>
                </strong></span></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="8%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>N&uacute;mero NIT<br>
                &nbsp;<%=AutoInfracaoBeanId.getIdentOrgaoEdt() %>                </strong></span></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="14%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>Data/Hora da Infra&ccedil;&atilde;o <br>
                &nbsp;<%=AutoInfracaoBeanId.getDatInfracao() %>/<%=AutoInfracaoBeanId.getValHorInfracaoEdt() %>                </strong></span></td>
              </tr>
            </table>
            <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr>
                <td height="2"></td>
              </tr>
            </table>
            <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr>
                <td height="2" bgcolor="#000000"></td>
              </tr>
            </table>
            <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr>
                <td height="2"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="49%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>NOME</strong></span><strong>:<br>
                </strong> </td>
              </tr>
            </table>
            <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr>
                <td height="2"></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr> 
                  <td width="35%" height="25" valign="top" class="tdborda" style="border-right: 0px none"><span class="fontmenor">&nbsp;&nbsp;<strong>N� 
                    CNH / PERM. P/ DIRIGIR</strong></span>:<br></td>
                  
                <td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000">
				
			<!-- linha vertical para separar o campo UF. Caso precise apagar esta linha vertical, remova a table e os styles da td anterior e da pr�xima -->	
				<table width="1" cellpadding="0" cellspacing="0" border="0" class="semborda">
                    <tr>
                      <td bgcolor="#000000" width="1" height="16"></td>
                    </tr>
				</table>
			<!-- Fim linha vertical para separar o campo UF-->	
                </td>
                <td width="5%" valign="top" class="tdborda" style="border-left: 0px none"><span class="fontmenor"><strong>UF: 
                  </strong></span><br></td>
				<td width="1%">&nbsp;</td>
				<td width="26%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>CPF:</strong></span></td>
                <td width="1%">&nbsp;</td>
                <td width="31%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>N&ordm; da Identidade: </strong></span><br>
                </td>
              </tr>
          </table>		  </td>
        </tr>
		<tr> 
          <td height="20">
		  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr> 
                <td height="2"></td>
              </tr>
            </table>
		      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                <tr> 
                  <td width="81%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>ENDERE�O 
                    COMPLETO</strong></span><strong>:<br>
                    </strong> </td>
                  <td width="1%" valign="top" class="semborda">&nbsp;</td>
                  <td width="18%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>CEP</strong></span>:<br>
                  </td>
                </tr>
              </table>
            
		  </td>
        </tr>
        <tr> 
          <td width="25%">
		  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr> 
                <td height="2"></td>
              </tr>
            </table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="37%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>BAIRRO</strong></span><strong>:</strong></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
			 	  <td width="54%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>CIDADE</strong></span>:<br>
                  </td>
			      <td width="1%">&nbsp;</td>
                  <td width="7%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>UF:</strong></span></td>
              </tr>
            </table>
		  </td>
        </tr>
       <tr> 
          <td width="25%" nowrap>
		<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
              <tr> 
                <td height="2"></td>
              </tr>
            </table>		
			
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                <tr>
                  <td width="9%" height="45" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>DATA:</strong></span><strong></strong></td>
                  <td width="1%" valign="top">&nbsp;</td> 
                  <td width="36%" valign="top" nowrap class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>ASSINATURA 
                    DO PROPRIET&Aacute;RIO</strong></span><strong>:<br>
                  </strong> </td>
                  <td width="1%" valign="top" class="semborda">&nbsp;</td>
                  <td width="53%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>ASSINATURA 
                    DO CONDUTOR <br>
                  &nbsp;&nbsp;</strong></span></td>
                </tr>
            </table>
		  </td>
        </tr>
      </table>
	  </td>
  </tr>
</table>

<!--fim parte B2-->
