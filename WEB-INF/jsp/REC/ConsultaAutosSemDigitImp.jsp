<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="consultaAutosSemDigiBean" scope="session" class="REC.ConsultaAutosSemDigiBean" />        
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" />        

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

</script>

<jsp:include page="CssImpressao.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="FormImp" method="post" action="">
<input name="ordem" type="hidden" value="numAuto">
<input name="acao" type="hidden" value="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0;
	int npag = 0;
	int seq = 0;
	String orgao = "";
	boolean passa = true;
	int numVez = 0;
	Iterator it = consultaAutosSemDigiBean.getAutos().iterator() ;
	REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();					
	if (consultaAutosSemDigiBean.getAutos().size()>0) { 
		while (it.hasNext()) {
			myAuto   = (REC.AutoInfracaoBean)it.next() ;
			seq++;
			contLinha++;
			
			if (contLinha%35==1){
				npag++;							
				if (npag!=1){
		%>			 					
         		<jsp:include page="rodape_impConsulta.jsp" flush="true" />
				<div class="quebrapagina"></div>
				<% } %>				
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 

	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
       <tr><td width="88" height=10></td></tr>
       <tr>	 
	   	 <td width="291"  ><font color="#000000"><b>&nbsp;&nbsp;�RG�O: &nbsp;&nbsp;<%= OrgaoBeanId.getSigOrgao() %></b></font></TD>		  	
       </tr>	   	 
	</table>
	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		

 	<tr > 	
         <td width="100" align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></TD>		 	  		 		 	  
	   	 <td width="150" bgcolor="#999999"><font color="#000000"><b>&nbsp;&nbsp;&nbsp;N�AUTO</b></font></TD>	
	   	 <td width="150" bgcolor="#999999"><font color="#000000"><b>&nbsp;&nbsp;&nbsp;N� PLACA</b></font></TD>	
	   	 <td width="200" bgcolor="#999999"><font color="#000000"><b>&nbsp;&nbsp;&nbsp;N� PROCESSO</b></font></TD>	
		 <td width="100" bgcolor="#999999"><font color="#000000"><b>&nbsp;&nbsp;&nbsp;DATA INFRACAO</b></font></TD>
	   	 <td  bgcolor="#999999"><font color="#000000"><b>REQUERIMENTO</b></font></TD>
	 </tr>  	   		
  </table>

<% } %>

    <table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" >							
		<tr> 
		 <td width="100" align="center"><%= seq %></TD>		 		 	  
         <td width="150" >&nbsp;<%= myAuto.getNumAutoInfracao() %> </td>
 		 <td width="150" >&nbsp;<%= myAuto.getNumPlaca()%></TD>	
 		 <td width="200" ><%= myAuto.getNumProcesso()%></TD>	
 		 <td width="100" ><%= myAuto.getDatInfracao()%></td>
		 <td >&nbsp;<%= myAuto.getTpRequerimento()%> </td>
    	</tr>
		<tr><td height=1 bgcolor="#000000" colspan=7></td></tr>
   <% } %>
    
  </table>      

<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>

	<div class="quebrapagina"></div>
	
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>   
	
    <jsp:include page="rodape_impconsulta.jsp" flush="true" /> 
<%} %>
<%if (consultaAutosSemDigiBean.getAutos().size()>0) {  %>
	<jsp:include page="rodape_impconsulta.jsp" flush="true" />
<%} %>

<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= consultaAutosSemDigiBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "220 px" />
  <jsp:param name="msgErroLeft" value= "35 px" />
</jsp:include> 
</form>
</body>
</html>