<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />  
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="AbrirRecursoBeanId" scope="request" class="REC.AbrirRecurso1aBean" /> 
<jsp:useBean id="RevisaoRequerimentoBeanId"   scope="request" class="REC.RevisaoRequerimentoBean" /> 
<%			RevisaoRequerimentoBeanId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'R':
			close() ;
			break;
			
		case 'LeReq':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	   
			}
			break;
		  			
		case 'alterarStatusReq':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
		
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
				}  
			}
		  	break;
		  	
	   case 'O':  // Esconder os erro
   		    if (document.layers) 
   		    	fForm.layers["MsgErro"].visibility='hide' ; 
	        else                 
	        	document.all["MsgErro"].style.visibility='hidden' ; 

			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   
		      	fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	
		   	}
			break;  
			
	   case 'N':  // Esconder os erro
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   
				fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		   	}
			break;  
	}
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.codRequerimento,"Requerimento",0);
	valid = ValDt(fForm.data,1) 
	verString(fForm.data,"Data",0);
	verString(fForm.txtMotivo,"Motivo",0);
    if (comparaDatas(fForm.data.value)==true){
       valid = false
       sErro += "Data do Acerto da Revis�o de Requerimento n�o pode ser posterior a data atual. \n"
	   document.all.data.focus();
    }

	if (valid==false) alert(sErro) 
	return valid ;
}
function validData(fForm) {
 sErro = "" ;
 valid = true ;
 if (comparaDatas(fForm.data.value)){
       sErro += "Data do Acerto da Revis�o de Requerimento n�o pode ser posterior a data atual. \n"
       valid = false
 }
  if(!ValDt(fForm.data,0)){
     valid = true; 
  }  
  
  if (valid==false){
     alert(sErro) 
     setTimeout("doSelection(document.AlteraDatVenc.data)", 0)
  }
  return valid 
}
function comparaDatas(data2) {
	var valid = false;
	var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var today=new Date();
	var difference=today-dataB;//obt�m o resultado em milisegundos.
	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = true;
	return valid;
}  

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AlteraDatVenc" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<jsp:include page="lerAutoPlaca.jsp"      flush="true" />    
<jsp:include page="apresentaInfracao.jsp" flush="true" />   
<div style="position:absolute; left:66px; top:269px; width:686px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:66px; top:274px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
     <tr bgcolor="#faeae5">
   	    <td colspan=3 height="22" valign="middle"><strong>&nbsp;
		<% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
			<%=AutoInfracaoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=AutoInfracaoBeanId.getDatStatus()%>)
		<% } %>
			</strong>
        </td>			
     </tr>
    <tr><td height=2  colspan="3"></td></tr>	
     <%if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0 && AutoInfracaoBeanId.getMsgErro().length()==0){%>
        <tr align="left" height=20 >
           <td align="right"  bgcolor="#faeae5">&nbsp;Requerimento:&nbsp;</td>
          <td  colspan="2" bgcolor="#faeae5">
    	   <% if ("S".equals(RevisaoRequerimentoBeanId.getEventoOK())) { %>		          
			 <jsp:getProperty name="RevisaoRequerimentoBeanId" property="reqs" />
           <%}else{ %>
			 <jsp:getProperty name="RevisaoRequerimentoBeanId" property="msg" />
           <%}%>
          </td>	
        </tr>
        <tr><td height=2 colspan="3" ></td></tr>	
    	<tr height=20 >
		 <td height="15" align="right" bgcolor="#faeae5">&nbsp;Data:&nbsp;</td>
          <td bgcolor="#faeae5" ><input type="text"  tabindex="1" name="data" size="12" maxlength="10" value='<%=RequerimentoId.getDatEST()%>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:validData(this.form);" onfocus="javascript:this.select();">         </td>	
		 <td bgcolor="#faeae5" align="right">
		      <button tabindex="3" type=button style="height: 21px; width: 33px; cursor : hand; border: none; background: transparent;"  onClick="javascript: valida('alterarStatusReq',this.form);">			  
	  	         <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19">
	      </button>	      </td>	
       </tr>  
       <tr><td height=2 colspan="3" ></td></tr>	
       <tr align="left" height=20 >
         <td align="right" bgcolor="#faeae5"> &nbsp;Motivo:&nbsp;</td>
         <td colspan="2" bgcolor="#faeae5"><input type="text"tabindex="2"  name="txtMotivo" size="100" maxlength="100" value='<%=RequerimentoId.getTxtMotivoEST()%>'  onkeypress="javascript:f_end();" onfocus="javascript:this.select();">         </td>	
      </tr>
	<%}%>	  

    </table>
</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0){%>
<%}else{%>    		
   	<script>document.AlteraDatVenc.numAutoInfracao.focus();</script>	        
<%}%>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>