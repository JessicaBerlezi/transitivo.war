<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
	<title>SMIT - Barcode</title>
</head>
<body leftmargin="0" topmargin="0">
<%
	String numCaixa = (String) request.getAttribute("numCaixa");	
%>
<table border="0" width="750" cellpadding="0" cellspacing="0">
<tr>
	<td><strong></td>
</tr>
<tr>
	<td><img src="<%= path %>/images/inv.gif" width="1" height="13" alt="" border="0"></td>
</tr>
</table>
<table border="0" width="750" cellpadding="0" cellspacing="0">

<%
    Iterator itx = ((ArrayList) request.getAttribute("listAuto")).iterator();
    REC.EtiquetaAutoBean etqBean = new REC.EtiquetaAutoBean();
    String numAuto="";
    String numeLote="";
    String numeLoteAtual="";
    String numeLoteProximo="";
    int contLinha=0;
    boolean passou = true;
    

while (itx.hasNext()) {
		contLinha++;
%>	
	<tr>		
	<%//Faz quebra de linha a cada 9 linhas, exceto na primeira linha.
		if (contLinha!=1 && contLinha%9==1) { 
			%>			
			<tr><td colspan=3><DIV class=quebrapagina></DIV></td></tr>
			<tr>
				<td colspan=3>
				<table border="0" width="750" cellpadding="0" cellspacing="0">
				<tr>
					<td></td>
				</tr>
				<tr>
					<td><img src="<%= path %>/images/inv.gif" width="1" height="13" alt="" border="0"></td>
				</tr>
				</table>
				</td>
			</tr>		 
		  <%		  	
		}		
	for (int i = 0; i < 3; i++) {	   	
		if (itx.hasNext()  ) {
		//Tem proximo, entao monta <tr> com 3 colunas
        if (passou==true){	  
		etqBean=(REC.EtiquetaAutoBean)itx.next();			
		}
		numAuto=etqBean.getNumAutoInfracao();
		numeLote=etqBean.getNumLote();
		numeLoteAtual=etqBean.getNumLote();		
		
		if (i <= 2) {
		  	if (!numeLoteAtual.equals(numeLoteProximo)) {%>
		  	  <td width="250" height="90">
					<table width="250" height="90" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td height="20" align="center"><strong>Caixa:</strong> <%=numCaixa%></td>
					</tr>
					<tr>
						<td height="10" align="center"><strong>Lote:</strong> <%=numeLote%></td>
					</tr>
					<tr>
						<td height="10" align="center" valign="bottom"></td>
					</tr>
					</table>				
				</td>						    	      
	          <%numeLoteProximo=etqBean.getNumLote();
	                     
	          	i++;
	          	passou=true;
	        }	       	         
	        if (i  <= 2) {%>
			    <td width="250" height="90">
					<table width="250" height="90" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td height="70"><img src="<%= path %>/codigoBarra?codigo=<%=numAuto%>&tipo=1&altura=70&flag=0" width="250" height="70" alt="" border="0"></td>
					</tr>
					<tr>
						<td height="10"><img src="<%= path %>/images/inv.gif" width="250" height="10" alt="" border="0"></td>
					</tr>
					<tr>
						<td height="10" align="center" valign="bottom"><strong><%=numAuto%></strong></td>
					</tr>
					</table>				
				</td> 
				
			<%
			passou=true;
			}else
			{passou=false;}
		}			
	}
    else{
		//Monta td Vazia%>	
        <td width="250">&nbsp;</td>
    <%}
  }%>  	
    </tr>    
  <%  if(itx.hasNext() && (contLinha%9!=0 || contLinha==1)) {
			//monta a linha de separacao
			out.print("<tr><td colspan=3 height=19>&nbsp;</td></tr>");
		}
    
}%>
</table>
</body>
</html>

