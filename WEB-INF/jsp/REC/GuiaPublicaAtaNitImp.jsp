<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*"%>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 

<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<SCRIPT LANGUAGE="JavaScript1.2">
window.print()
</script>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">
DIV.quebrapagina{page-break-after: always}
td {font-family:Verdana, Arial, Helvetica, sans-serif; font-size: 11px;}
table.comborda td {border: 1px solid #000000}
</style>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="CssIpressao.jsp" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >
<form name="NotificacaoForm1" method="post" action="">
<input name="acao"              type="hidden" value=' '>
  <table width="100%" border="0" cellpadding="3" cellspacing="0">
<%    
	  REC.AutoInfracaoBean myAuto = new REC.AutoInfracaoBean();  
	  REC.GuiaDistribuicaoBean myGuia = new REC.GuiaDistribuicaoBean();
	  REC.RelatorBean myRel = new REC.RelatorBean();
	  String presidente = "";
	  String nomRel = "";
      List guias = GuiaDistribuicaoId.getAutos();
	  String nomRelatorPres = "";
	  String nomRelator = "";
	  for(int x = 0;x < guias.size();x++){
	  	myGuia = (REC.GuiaDistribuicaoBean)guias.get(x);
	  	
	  	//Encontro o Presidente da Junta
	  	myRel.setCodJunta(myGuia.getCodJunta());
	  	List relatores = myRel.getJuntaRelator();
        for(int y = 0;y < relatores.size();y++){	
           REC.RelatorBean nomPresidente = (REC.RelatorBean)relatores.get(y);
           if (nomPresidente.getNomTitulo().equals("PRESIDENTE") && nomRelatorPres.equals(nomPresidente.getNomRelator()))
               presidente = nomPresidente.getNomRelator();
           nomRelatorPres = myGuia.getNomRelator();
        }
        
        //Encontro os Relatores da Junta
        if(!nomRelator.equals(myGuia.getNomRelator())&&
        	(nomRel.indexOf(myGuia.getNomRelator())< 0)){
               nomRel += myGuia.getNomRelator()+ ", ";
        }
        nomRelator = myGuia.getNomRelator();
     }  
	%>
       <tr>
   		 <td width="46%" align="center"><div align="justify">Secretaria Municipal de Servi&ccedil;os P&uacute;blicos, Tr&acirc;nsito e Transportes Junta Administrativa de Recursos de Infra&ccedil;&otilde;es - JARI 01 </div></td>
            <td width="54%"></td>
       </tr>
	   <tr>
	      <td><%=myGuia.getNumSessao()%>� Sess&atilde;o Ordin&aacute;ria da JARI 01, realizada em <%=myGuia.getDatSessao()%>.</td>
	   </tr>
        <tr>
      		<td align="justify">Presid&ecirc;ncia:&nbsp;&nbsp;<%= presidente %>
          . Membros efetivos participantes: &nbsp;&nbsp;
          <%if(!nomRel.equals("")){ %>
              <%=nomRel.substring(0,nomRel.length()-2)%>
          <%} %>    
            </td>
            <td></td>
        </tr>
        <tr>
    		<td align="justify"><div align="justify">1.Instala��o, verifica��o de quorum e abertura da sess�o pelo presidente da JARI 01.</div></td>
            <td></td>
        </tr>
        <tr>
    		<td align="justify"><div align="justify">2.Julgamento de recursos destinados ao cancelamento de multas por infra&ccedil;&otilde;es de tr&acirc;nsito. </div></td>
            <td></td>
       </tr>
       <tr>
    		<td align="justify"><div align="justify">2.1.Vistos, relatados e discutidos, os recursos de multa, abaixo elencados, decidem os membros da JARI 01, em sess&atilde;o plen&aacute;ria: </div></td>
            <td></td>
       </tr>
       <tr> 
         <td valign="top">
<%    	   List autos = null; 
           String nomRelat = "";
           for(int i = 0;i < guias.size();i++){
	        	myGuia = (REC.GuiaDistribuicaoBean)guias.get(i);
	        	autos = myGuia.getAutos();
	            if(!myGuia.getNomRelator().equals(nomRelat)){	
%>         
		   <tr>
    		<td height="10" colspan ="2" rowspan="2" align="justify" valign="bottom">2.1.<%=i+1%>.Relator: <%=myGuia.getNomRelator()%></td>
            <td ></td>
           </tr>
		   <tr>
    		<td></td>
           </tr>
<%				} 
	            nomRelat = myGuia.getNomRelator();
%>           
      </table>
      <table width="100%" border="0" cellpadding="3" cellspacing="0" class="comborda">  
          <tr>
	      		<td width="22%" align="justify">Recurso de multa n� </td>
		  		<td width="24%" align="justify">Decis�o da JARI 01 </td>
		  		<td  height="20" width="54%"></td>
		   </tr> 
<%            String codResultado = "";
              for(int z=0;z<autos.size();z++){
                  myAuto = (REC.AutoInfracaoBean)autos.get(z);
                  if(myAuto.getRequerimentos(0).getCodResultRS().equals("D")){
                	  codResultado = "Deferido";
                  }else if(myAuto.getRequerimentos(0).getCodResultRS().equals("I")){
                	  codResultado = "Indeferido";
                  }
%>       
           	     
           <tr>
              <td align="justify"><%= myAuto.getNumProcesso() %></td>
			  <td align="justify"><%= codResultado %></td>
			  <td></td>
 		   </tr>
 <%           } %>
           <tr>
    		<td height="10" ></td>
            <td></td>
            <td></td>
           </tr>
<%     }%>	
           
   
    </table>     
    <table width="100%" border="0" cellpadding="2" cellspacing="0" > 	   
    <tr>
      		<td colspan="2" align="justify"><div align="justify">Nada mais havendo, encerrada a sess&atilde;o e lavrada e assinada a devida ata. </div></td>
            <td width="54%"></td>
      </tr>
    </table>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= GuiaDistribuicaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>

</body>
</html>
























