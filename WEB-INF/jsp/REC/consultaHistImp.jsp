<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->
<%@ page import="java.util.Iterator"%>
<%String path = request.getContextPath();%>
            
<jsp:useBean id="UsuarioBeanId"      scope="session" class="ACSS.UsuarioBean" />             
<jsp:useBean id="AutoInfracaoBeanId"     scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>

<link href="CssImpressao.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0"
	marginheight="0">

<form name="ConsultaHistImp" method="post" action=""><!-- Inicio Div Detalhes vari�veis impressao -->
<%boolean bPrimeira = false;
            int contLinha = 99;
            int npag = 0;
            int seq = 0;
            int a = AutoInfracaoBeanId.getHistoricos().size() + 1;
            Iterator itx = AutoInfracaoBeanId.getHistoricos().iterator();
            REC.HistoricoBean hist = new REC.HistoricoBean();
            if (itx.hasNext()) {
                while (itx.hasNext()) {
                    a--;
                    hist = (REC.HistoricoBean) itx.next();
                    seq++;
                    contLinha++;
                    //Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
                    if (contLinha > 9) {
                        contLinha = 1;
                        npag++;
                        if ((npag != 1) && (bPrimeira)) {

                        %>
</table>
<%@ include file="rodape_impconsulta_Diretiva.jsp" %>
<div class="quebrapagina"></div>
<%}
                        bPrimeira = true;

                    %> <%@ include file="cabecalho_histconsulta.jsp" %>	
	
<%}

                    %>
<table id="histImp" width="100%" border="0" cellpadding="0" overflow
	: "hidden" cellspacing="0" class="semborda">
	<tr height="8" bgcolor="#C0BDBD">
		<td>&nbsp;&nbsp;<strong><%=a%>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;Evento:&nbsp;</strong></td>
		<td>&nbsp;<strong><%=hist.getNomEvento()%></strong></td>
		<td></td>
		<td>&nbsp;</td>
		<td colspan=2><strong>Origem:&nbsp;<%=hist.getNomOrigemEvento()%></strong>&nbsp;&nbsp;</td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="8">
		<td>&nbsp;&nbsp;<strong>Status:</strong></td>
		<td colspan=3>&nbsp;<%=hist.getNomStatus()%> - <%=hist.getCodStatus()%></td>
		<td><strong>Data Status:</strong></td>
		<td><%=hist.getDatStatus()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="8">
		<td width="106">&nbsp;&nbsp;<strong>Executado por :</strong></td>
		<td width="238">&nbsp;<%=hist.getNomUserName()%></td>
		<td width="83"><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
		<td width="82"><%=hist.getCodOrgaoLotacao()%>-<%=hist.getSigOrgaoLotacao()%></td>
		<td width="127"><strong>Data Proc.:</strong></td>
		<td width="84"><%=hist.getDatProc()%></td>
	</tr>
	<tr>
		<td height="5"></td>
	</tr>
</table>

<%if ("406".equals(hist.getCodEvento()) == true) { %>
<!--============ EVENTO: REGISTRAR AR-REC/N�O REC  =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="301">&nbsp;&nbsp;<strong>C&oacute;d. do Retorno de Entrega
		:</strong></td>
		<td width="419"><%=hist.getTxtComplemento01()%></td>
	</tr>
</table>

<%}else if ("200".equals(hist.getCodEvento())&& !hist.getCodOrigemEvento().equals("200")&& !hist.getTxtComplemento05().equals("")){%>
<!--============ EVENTO: INCLUS�O DE AUTUA��O =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#FFFFFF">
        <td width="106">&nbsp;&nbsp;<strong>Nome Arquivo:</strong></td>
        <td style="text-align: left;">&nbsp;<%=hist.getTxtComplemento05()%></td>
      </tr>
      <tr><td height="2" colspan="2"></td></tr>
      <tr bgcolor="#FFFFFF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Data Receb.:</strong></td>
        <td style="text-align: left;">&nbsp;<%=hist.getTxtComplemento06()%></td>
      </tr>
    </table>    

<%} else if ("053".equals(hist.getCodEvento()) == true) { %> 
<!--============ EVENTO: ENVIO PARA O BANCO  =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="107">&nbsp;&nbsp;<strong>Tipo Opera��o :</strong></td>
		<td width="613"><%=hist.getTxtComplemento06()%></td>
	</tr>
</table>

<%} else if ("204".equals(hist.getCodEvento())
                            || "324".equals(hist.getCodEvento())
                            || "203".equals(hist.getCodEvento())
                            || "323".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: REGISTRAR AR-REC/N�O REC  =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="301">&nbsp;&nbsp;<strong>C&oacute;d. de Retorno:</strong></td>
		<td width="209"><%=hist.getTxtComplemento01()%></td>
		<td width="210"><strong>Data da Notifica��o:</strong>&nbsp;<%=hist.getTxtComplemento02()%></td>
	</tr>
</table>

<%} else if ("903".equals(hist.getCodEvento())) {%> 
<!--============ EVENTO: CANCELAMENTO DE AUTOS =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88">&nbsp;&nbsp;<strong>Processo :</strong></td>
		<td width="630"><%=hist.getNumProcesso()%></td>
	</tr>
	<tr>
		<td width="90">&nbsp;&nbsp;<strong>Motivo Cancel. :</strong></td>
		<td width="630"><%=hist.getTxtComplemento04()%></td>
	</tr>
	<%if (!hist.getTxtComplemento03().equals("")){%>
	<tr>
		<td width="109">&nbsp;&nbsp;<strong>Doc. Gerador :</strong></td>
		<td width="476"><%=hist.getTxtComplemento03()%></td>
	</tr>
	<%}%>
</table>

<%} else if ("206".equals(hist.getCodEvento())
                            || "326".equals(hist.getCodEvento())
                            || "334".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ABRIR DEFESA PR�VIA/TRI =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="111">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
		<td width="401"><%=hist.getTxtComplemento01()%></td>
		<td width="124"><strong>Tipo :</strong></td>
		<td width="84"><%="TR".equals(hist
                                                .getTxtComplemento02()) ? "TRI"
                                        : hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Respons�vel :</strong></td>
		<td><%=hist.getTxtComplemento04()%></td>
		<td><strong>Abertura :</strong></td>
		<td><%=hist.getTxtComplemento03()%></td>
	</tr>
</table>

<%} else if ("207".equals(hist.getCodEvento()) == true) {   %> 
<!--============ EVENTO: PARECER JUR�DICO =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Requerimento :</strong></td>
		<td><%=hist.getTxtComplemento01()%></td>
		<td colspan=2><strong>C&oacute;d. do Respons&aacute;vel :</strong>&nbsp;<%=hist.getTxtComplemento05()%></td>
		<td colspan=2><strong>Parecer :&nbsp;</strong> <%=("D".equals(hist
                                                .getTxtComplemento03()) ? "Deferido"
                                                : ("E".equals(hist
                                                        .getTxtComplemento03()) ? "Estornado"
                                                        : "Indeferido"))%></td>
		<td width="4"></td>
	</tr>
	<tr>
		<td colspan=6 height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
		<td colspan=5><%=hist.getTxtComplemento04()%></td>
	</tr>
	<tr>
		<td colspan=6 height="2"></td>
	</tr>
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>C&oacute;d. Junta : </strong></td>
		<td width="194"><%=hist.getTxtComplemento06()%></td>
		<td width="125"><strong>C&oacute;d. do Relator:</strong></td>
		<td width="84"><%=hist.getTxtComplemento07()%></td>
		<td width="121"><strong>Data:</strong></td>
		<td width="220"><%=hist.getTxtComplemento02()%></td>
	</tr>
</table>

<%} else if ("208".equals(hist.getCodEvento())
                            || "328".equals(hist.getCodEvento())
                            || "335".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: TROCA JUNTA/RELATOR  =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>Requerimento: </strong></td>
		<td width="397"><%=hist.getTxtComplemento01()%></td>
		<td width="127"><strong>Data da Troca : </strong></td>
		<td width="84"><%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td colspan=4 height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>C&oacute;d. da Junta : </strong></td>
		<td><%=hist.getTxtComplemento03()%></td>
		<td colspan=2><strong>C&oacute;d. do Relator:</strong>&nbsp;&nbsp;<%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("209".equals(hist.getCodEvento())
                            || "329".equals(hist.getCodEvento())
                            || "336".equals(hist.getCodEvento()) == true) {

                        %>
<!--============ EVENTO: RESULTADO DE DEFESA PR�VIA =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>Requerimento:</strong></td>
		<td width="193"><%=hist.getTxtComplemento01()%></td>
		<td width="125"><strong>Resultado :&nbsp;</strong>
		<td width="79"><%=("D".equals(hist
                                                .getTxtComplemento03()) ? "Deferido"
                                                : ("E".equals(hist
                                                        .getTxtComplemento03()) ? "Estornado"
                                                        : "Indeferido"))%></td>
		<td width="127"><strong>Data:</strong></td>
		<td width="84"><%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td colspan=5 height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
		<td colspan=4><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("210".equals(hist.getCodEvento())
                            || "330".equals(hist.getCodEvento())
                            || "337".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ESTORNO DE RESULTADO =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>Requerimento:</strong></td>
		<td width="403"><%=hist.getTxtComplemento01()%></td>
		<td width="120"><strong>Data Estorno:</strong></td>
		<td width="89"><%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td colspan=4 height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
		<td colspan=3><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("211".equals(hist.getCodEvento())
                            || "331".equals(hist.getCodEvento())
                            || "338".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ENVIO DEF. PREVIA PARA D.O. =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88">&nbsp;&nbsp;<strong>C&oacute;d. Relat&oacute;rio D.O.:</strong></td>
		<td width="423"><%=hist.getTxtComplemento02()%></td>
		<td width="120"><strong>Data de Envio Publica&ccedil;&atilde;o :</strong></td>
		<td width="89"><%=hist.getTxtComplemento01()%></td>
	</tr>
</table>

<%} else if ("212".equals(hist.getCodEvento())
                            || "332".equals(hist.getCodEvento())
                            || "339".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ENVIO DEF. PREVIA PARA D.O. =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88"></td>
		<td width="421"></td>
		<td width="122"><strong>Data Exclus�o:</strong></td>
		<td width="89"><%=hist.getTxtComplemento01()%></td>
	</tr>
</table>

<%} else if ("213".equals(hist.getCodEvento())
                            || "333".equals(hist.getCodEvento())
                            || "340".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ATUALIZACAO DATA DO D.O. =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>C&oacute;d. Relat&oacute;rio D.O.:</strong></td>
		<td width="159"><%=hist.getTxtComplemento02()%></td>
		<td width="266"><strong>Data de Publica&ccedil;&atilde;o no D.O. :</strong>&nbsp;<%=hist.getTxtComplemento01()%></td>
		<td width="185">&nbsp;</td>
	</tr>
</table>

<%} else if ("214".equals(hist.getCodEvento()) == true) { %> 
<!--=================== EVENTO: EXECUTAR TRI  ==============================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	class="semborda">
	<tr height="16">
		<td width="111">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
		<td width="159"><%=hist.getTxtComplemento11()%></td>
		<td width="86"></td>
		<td width="154"></td>
		<td width="126"><strong>Data Req. :</strong></td>
		<td width="84"><%=hist.getTxtComplemento01()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="16">
		<td><strong>&nbsp;&nbsp;Nome :</strong></td>
		<td><%=hist.getTxtComplemento03()%></td>
		<td><strong>Endere&ccedil;o :</strong></td>
		<td colspan=3><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="110">&nbsp;&nbsp;<strong>Bairro :</strong></td>
		<td width="161"><%=hist.getTxtComplemento05()%></td>
		<td width="95"><strong>Cidade :</strong></td>
		<td width="147"><%=hist.getTxtComplemento06()%></td>
		<td width="73"><strong>UF :</strong></td>
		<td width="17"><%=hist.getTxtComplemento07()%></td>
		<td width="10"></td>
		<td width="39"><strong>CEP :</strong></td>
		<td width="68"><%=hist.getTxtComplemento08()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="110">&nbsp;&nbsp;<strong>CPF : </strong></td>
		<td width="162"><%=hist.getTxtComplemento02()%></td>
		<td width="139"><strong>N� CNH / Perm. p/ Dirigir :</strong></td>
		<td width="102"><%=hist.getTxtComplemento09()%></td>
		<td width="74">
		<p><strong>UF </strong><strong>CNH</strong><strong> :</strong></p>
		</td>
		<td width="133"><%=hist.getTxtComplemento10()%></td>
	</tr>
</table>

<%} else if ("226".equals(hist.getCodEvento())
                            || "350".equals(hist.getCodEvento())
                            || "351".equals(hist.getCodEvento())
                            || "352".equals(hist.getCodEvento())
                            || "215".equals(hist.getCodEvento())) {

                        %> 
<!--============ EVENTO: ESTORNO DE DEFESA PR�VIA/RECURSO =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="112">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
		<td width="159"><%=hist.getTxtComplemento01()%></td>
		<td width="449"><strong>Data do Estorno:</strong>&nbsp;<%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo :&nbsp;</strong></td>
		<td colspan=2><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("410".equals(hist.getCodEvento())) {  %> 
<!--============ EVENTO: AJUSTE DE NUMERO DE PROCESSO ====================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" overflow:
	hidden bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88">&nbsp;&nbsp;<strong>Novo Processo:</strong></td>
		<td width="179"><%=hist.getTxtComplemento01()%></td>
		<td width="453"><strong>Data do Processo:</strong>&nbsp;<%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
		<td colspan=2><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("411".equals(hist.getCodEvento())) {%> 
<!--============ EVENTO: AJUSTE DE STATUS ===================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88">&nbsp;&nbsp;<strong>Novo Status:</strong></td>
		<td width="238"><%=hist.getTxtComplemento01()%></td>
		<td width="453"><strong>Data do Status:</strong>&nbsp;<%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td height="2"></td>
	</tr>
	<tr height="16">
		<td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
		<td colspan=2><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%} else if ("381".equals(hist.getCodEvento())) {%> 
<!--============ EVENTO: ENVIO DO =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="88">&nbsp;&nbsp;<strong>Requerimento&nbsp;:&nbsp;</strong></td>
		<td width="238"><%=hist.getTxtComplemento03()%></td>
		<td width="41">&nbsp;&nbsp;<strong>Data:&nbsp;</strong></td>
		<td width="155"><%=hist.getTxtComplemento01()%></td>
		<td width= "127"><strong>N&ordm ATA:&nbsp</strong><%=hist.getTxtComplemento02()%></td>
	</tr>
	<tr>
		<td colspan=5 height="2"></td>
	</tr>
</table>
<%} else if ("383".equals(hist.getCodEvento())
                            || "388".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ATUALIZA DATA DO =================================================================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="97">&nbsp;&nbsp;<strong>Requerimento&nbsp;:&nbsp;</strong></td>
		<td width="264"><%=hist.getTxtComplemento02()%></td>
		<td width="45">&nbsp;&nbsp;<strong>Data:&nbsp;</strong></td>
		<td width="173"><%=hist.getTxtComplemento01()%></td>
		<td width="141"><strong>N&ordm ATA:&nbsp</strong><%=hist.getTxtComplemento03()%></td>
	</tr>
	<tr>
		<td colspan=5 height="2"></td>
	</tr>
</table>

<%} else if ("264".equals(hist.getCodEvento())
                            || "384".equals(hist.getCodEvento())
                            || "391".equals(hist.getCodEvento())) {

                        %>
<!--============ EVENTO: ACERTO EM REQUERIMENTO PARA REVIS�O ===========================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr height="16">
		<td width="111">&nbsp;&nbsp;<strong>Motivo:</strong></td>
		<td width="609"><%=hist.getTxtComplemento04()%></td>
	</tr>
</table>

<%}else if ("413".equals(hist.getCodEvento())&& hist.getTxtComplemento12().length()>0) { %>
<!--============ EVENTO: AJUSTE MANUAL RESP. PELOS PONTOS ===========================-->
<table width="100%" border="0" cellpadding="0" cellspacing="0"
bgcolor="#FFFFFF" class="semborda">
<tr height="16">
	<td width="111">&nbsp;&nbsp;<strong>Motivo:</strong></td>
	<td width="609"><%=hist.getTxtComplemento12()%></td>
</tr>
</table>
<%}%>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr>
		<td height="8"></td>
	</tr>
</table>

<%}

            %> <%} else {%>
<div id="mensagem"
	style="position:absolute; left:70px; top:85px; width:538px; height:35px; z-index:10; overflow: visible; visibility: hidden;">
<table width="100%" align="center" border="0" cellpadding="0"
	cellspacing="0" class="semborda">
	<tr>
		<td>&nbsp;&nbsp;<strong>AUTO SEM HIST�RICO</strong></td>
	</tr>
</table>
</div>
<%}%> <%@ include file="rodape_impconsulta_Diretiva.jsp" %></form>
</body>
</html>
