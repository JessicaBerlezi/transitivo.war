<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId" scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="UFCNHId" scope="request" class="sys.UFBean" />
<jsp:useBean id="UFId" scope="request" class="sys.UFBean" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<!--M�dulo : REC
	Vers�o : 2.0
	Atualiza��es: 11/03/2008
-->	
<title>:: TRANSITIVO ::</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'ConfirmaAjuste':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AjusteManualForm.dataReq,1) && valid

	if (valid==false) alert(sErro) 
	return valid ;
} 
function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if ( (valor.length != 0)&&(valor.length > 11) ) {     
    if (valor.length > 11) { 
     	valor = Tiraedt(valor,11);
    } 
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 document.AjusteManualForm.cpfpnthidden.value=valor;
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjusteManualForm" method="post" action="">
 <%String cpf = AutoInfracaoBeanId.getProprietario().getNumCpfCnpj(),c_cpf="";    					
  c_cpf=cpf;
  if (cpf.length()>0)
  {
    cpf = cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11);
  }%>


<%@ include file="Cab_Diretiva.jsp" %>

<%
   String isDisabled = "";   
   UFCNHId.setChecked(RequerimentoId.getCodUfCNHTR());
   UFId.setChecked(AutoInfracaoBeanId.getCondutor().getEndereco().getCodUF());
   if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")){isDisabled += "disabled";}
   
%>  
<!-- UF do CNH --> 
<jsp:setProperty name="UFCNHId" property="j_abrevSist" value="REC" />  	 	     	 	  
<jsp:setProperty name="UFCNHId" property="popupExt" value="<%=isDisabled%>"  />
<jsp:setProperty name="UFCNHId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFCNHId" property="popupNome"   value="codUfCNHTR"  />  
<jsp:setProperty name="UFCNHId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 

<!-- UF do endereco -->
<jsp:setProperty name="UFId" property="j_abrevSist" value="REC" /> 
<jsp:setProperty name="UFId" property="popupExt" value="<%=isDisabled%>"  /> 	 	     	 	  
<jsp:setProperty name="UFId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFId" property="popupNome"   value="codUf"  />  
<jsp:setProperty name="UFId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 



<input name="acao"     type="hidden" value=' '>
<input name="verTabela" type="hidden" value=''>
<input name="cpfpnthidden" type="hidden" value='<%=c_cpf%>'>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>  
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:276px; height:45px; z-index:800; overflow: visible; visibility: visible;">
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>			 	 			

<!-- ========================1� bloco========================================== -->   
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	<tr bgcolor="#faeae5">
	  <td width="11%" align="left" >&nbsp;<strong>Resp. Ptos :&nbsp;&nbsp;</strong></td>
		<td width="33%" align="left" >
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomResp" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getNomResponsavel().trim() %>' size="41" maxlength="40" onKeyPress="javascript:f_let();" onChange="this.value=this.value.toUpperCase()"></td>
		<td width="12%" style="text-align:right" ><strong>CPF :</strong></td>
		<td width="18%" align="left">
			
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="numCPF" type="text" size="16" maxlength="14" value='<%=cpf%>' onKeyPress="javascript:Mascaras(this,'999.999.999-99');" onFocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);">		</td>
		<td width="9%" style="text-align:right" ><strong>Data Req :</strong></td>	 
	    <td width="16%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="dataReq" type="text" size="14" maxlength="10" value='<%=RequerimentoId.getDatRequerimento() %>' onKeyPress="javascript:Mascaras(this,'99/99/9999');"></td>
	    <td width="1%">&nbsp;</td>
	</tr>	 
	</table>


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
 

<!-- ========================Fim do 1� bloco========================================== -->    
 
    
<!-- ========================2� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Tipo :</strong></td> 
	    <td width="17%" align="left" ><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>disabled<%}%> name="indTipoCNHPGU" type="checkbox" style=" heigth:11;width:13px" value='1'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getCondutor().getIndTipoCnh(),"1")%> class="input">
      				PGU &nbsp;<input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>disabled<%}%> name="indTipoCNH" type="checkbox" style=" heigth:11;width:13px" value='2'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getCondutor().getIndTipoCnh(),"2")%> class="input">
		CNH</td>
	    <td width="12%" style="text-align:right" ><span><strong>Decis&atilde;o Judic.:</strong></span>        </td>
		<td width="4%" align="left" ><input name="decisaoJur" type="checkbox" class="input" style=" heigth:11;width:13px" value='1' <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%>></td>
		<td width="12%" style="text-align:right" ><strong>N� CNH/PGU :</strong></td>
		<td width="18%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="numCNH" type="text" size="19" maxlength="11" value='<%=AutoInfracaoBeanId.getCondutor().getNumCnh().trim() %>' onChange="this.value=this.value.toUpperCase()"></td>
		<td width="9%" align="right" nowrap><strong >UF CNH :</strong></td>	 
        <td width="7%"><jsp:getProperty name="UFCNHId" property="popupString" /></td>
        <td width="10%" align="right" nowrap>&nbsp;</td>
  </tr>
</table>   


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      
<!-- ========================Fim do 2� bloco========================================== -->        

<!-- ========================3� bloco========================================== --> 
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Endere�o :</strong></td> 
	    <td width="25%" align="left" >
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtEndereco" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtEndereco().trim() %>' size="35" maxlength="27"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">	    </td>
	    <td width="4%" style="text-align:right" ><strong>N�:</strong></td>
	    <td width="8%" align="rigth">&nbsp;<input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtNumEndereco" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getNumEndereco().trim() %>' size="7" maxlength="5"  onKeyPress="javascript:f_num();" ></td>
	    <td width="8%" style="text-align:right">
        
        <strong>Compl.:</strong></td>
		<td width="18%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtComplemento" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtComplemento().trim() %>' size="20" maxlength="11"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" ></td>
		<td width="9%" style="text-align:right"><strong >Bairro :</strong></td>	 
        <td width="17%" align="left" nowrap><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomBairro"   type="text" size="20" maxlength="30" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>
  </tr>
</table> 


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      
<!-- ========================Fim do 3� bloco========================================== -->        
<!-- ========================4� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Cidade :</strong></td> 
	    <td width="33%" align="left" >
   	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomCidade"   type="text" size="32" maxlength="30" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getNomCidade().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">	    </td>
	    <td width="12%" style="text-align:right" ><strong>CEP :</strong></td>
	    <td width="18%">
        <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCEPTR" type="text" size="11" maxlength="8" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt().trim() %>' onKeyPress="javascript:f_num();"></td>
        <td width="9%" style="text-align:right" ><strong>UF : </strong></td>
		<td width="8%"><jsp:getProperty name="UFId" property="popupString" /></td>
			 
		<td width="9%" align="right" nowrap>
	  <%if (!UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>
        	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ConfirmaAjuste',this.form);">	
		   		<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" style="margin-left: 4px; margin-top:0px; margin-bottom: 0px" >		  	</button>
		<%}%>		</td>	 
  </tr>
</table>  

  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      

<!-- ========================Fim do 4� bloco========================================== -->        
    <%}%>
</div>
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>		
<div style="position:absolute; left:50px; right: 15px; bottom:63px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>   	
<%}%>	
   	<script>document.AjusteManualForm.numAutoInfracao.focus();</script>	        
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
 </body>
</html>
