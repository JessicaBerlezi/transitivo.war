<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<% String path=request.getContextPath(); %>

<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<head>
<jsp:include page="CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
</script>
</head>
<body>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<!--CABE�ALHO IMPRESS�O-->
<div id="cabecalhoimp" style="position: relative; z-index:100;">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" >
        <tr> 
		  <td width="33%" align="center" class="td_linha_right"><img src="<%= path %>/images/logos/logoniteroi3.gif" width="222" height="86" /></td>
		
    <%--if (!"".equals(AutoInfracaoBeanId.getNomOrgao())){ --%>	                
    	  <td width="34%" valign="middle" class="td_linha_right"><div align="center">
    	      <p class="fontmaior"><strong>AUTO DE INFRA&Ccedil;&Atilde;O<br />
    	      </strong><strong>DE TR&Acirc;NSITO </strong></p>
   	        </div></td>
          <td width="33%" valign="middle" class="semborda"><div align="center"><span class="fontmaior"><strong>&Oacute;RG&Atilde;O AUTUADOR<br />
  <%= UsuarioBeanId.getCodOrgaoAtuacao()%> </strong></span></div></td>
        </tr>
</table>


       
		  <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr> 
                      
                <td width="33%" height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;N&ordm; AUTO DE INFRA&Ccedil;&Atilde;O  </strong></td>
                <td width="17%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;N&ordm; DETRAN </strong></td>
                <td width="17%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;DATA</strong></td>
                <td width="11%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;HORA</strong></td>
                <td width="22%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;AGENTE </strong></td>
              </tr>
        </table>
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                  <tr>
                    <td width="33%" height="30" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getIdentOrgaoEdt() %></strong></span></td>
                    <td width="17%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong></span></td>
                    <td width="17%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getDatInfracao() %></strong></span></td>
                    <td width="11%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></strong></span></td>
                    <td width="22%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getDscAgente() %></strong></span></td>
                  </tr>
              </table>
			  
			    <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
                  <tr>
                    <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;LOCAL DA INFRA&Ccedil;&Atilde;O </strong></td>
                  </tr>
                </table>
		  
		  
		  
		  
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="30" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getDscLocalInfracaoNotificacao() %></strong></span><br>
                    </tr>
                  </table>                
                
			
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr> 
                  
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;C&Oacute;DIGO DO MUNIC&Iacute;PIO DA INFRA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
			
			
		 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
               <tr> 
                        
                        
                      <td width="32%" height="30" valign="top" class="tdborda"><span class="fontmedia">
                        <label></label>
                      </span>&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getNomMunicipio() %> - <%=AutoInfracaoBeanId.getCodMunicipio() %></strong></span></td>
               </tr>
	    </table>		
			
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td width="50%" height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;PLACA </strong></td>
                <td width="50%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;MUNIC&Iacute;PIO VE&Iacute;CULO/ESTADO </strong></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="50%" height="30" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getNumPlaca() %></strong></span></td>
                      <td width="50%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomCidade()%>&nbsp;&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getProprietario().getEndereco().getCodUF()%></strong></span></td>
                    </tr>
        </table>
				
				
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td width="50%" height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;ESP&Eacute;CIE/MARCA/MODELO</strong></td>
                <td width="17%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;C&Oacute;D. INFRA&Ccedil;&Atilde;O </strong></td>
                <td width="17%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;ARTIGO</strong></td>
                <td width="16%" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;GRAU</strong></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="50%" height="30" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %>&nbsp;&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></strong></span></td>
                <td width="17%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %></strong></span></td>
                <td width="17%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getInfracao().getDscEnquadramento() %></strong></span></td>
                <td width="16%" valign="top" class="tdborda">&nbsp;&nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getInfracao().getDscGravidade() %></strong></span></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;DESCRI&Ccedil;&Atilde;O DA INFRA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="32%" height="30" valign="top" class="tdborda"><span class="fontmedia">
                  <label></label>
                </span><span class="fontmedia"><strong>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscInfracao() %></strong></span></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;&nbsp;COMPLEMENTO DA INFRA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="32%" height="30" valign="top" class="tdborda"><span class="fontmedia">
                  <label></label>
                </span><span class="fontmedia">&nbsp;&nbsp;<strong><%=AutoInfracaoBeanId.getDscResumoInfracao() %></strong></span></td>
              </tr>
            </table>
            
            
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>
<%if(AutoInfracaoBeanId.getFotoDigitalizada().getDatDigitalizacao()!= null){ %>
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
  <tr> 
    <td width="25%" height="20" bgcolor="#b6b6b6" > 
      <strong>3 &#8226; FOTO</strong>
	</td>  
  </tr>
  <tr><td height="10" ></td></tr>
   <tr>
      <td align="center">
	  <img id="imagemFoto" alt="Foto do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7"  src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getFotoDigitalizada().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getFotoDigitalizada().getParametro(ParamSistemaBeanId)%>&header=#" width="540" height="280">	
	  </td>
   </tr>
</table>  
<%}%> 
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="10" ></td>
	</tr>
</table>
</div>
<!--FIM_BLOCO B-->
</body>