<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'CadastraProc':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		        if (veCampos(fForm)==true) 
		        {		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  		
				   	fForm.submit();	 
				} 		  
			}
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;	
	valid = ValDt(fForm.datprocesso,1) && valid
	verString(fForm.datprocesso,"Data do Processo",0);
	if (valid==false) alert(sErro) 
	return valid ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="CadatraProc" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
 
<input name="acao"         type="hidden" value=' '>				

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:66px; top:269px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>   

<div id="Cadastra" style="position:absolute; left:66px; top:273px; width:687px; height:60px; z-index:2; overflow: visible; visibility: visible;"> 
 <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr bgcolor="#faeae5">
   	    <td colspan=3 height="22" valign="middle"><strong>&nbsp;
		<% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
			<%=AutoInfracaoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=AutoInfracaoBeanId.getDatStatus()%>)
		<% } %>
			</strong>
        </td>			
     </tr>
     <tr><td colspan=3 height=2></td></tr>
     <tr bgcolor="#faeae5">
   	    <td width="60%" height="22" valign="middle">&nbsp;	N�mero do Processo :&nbsp;
			  <input type="text" name="numprocesso" size="25" maxlength="20" value='<%=AutoInfracaoBeanId.getNumProcesso() %>'  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase();">	
        </td>			
        <td width="30%">&nbsp;	Data do Processo :&nbsp;
			  <input type="text" name="datprocesso" size="12" maxlength="10" value='<%=AutoInfracaoBeanId.getDatProcesso() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
        <td width="10%"  align="center" valign="middle"></td>
     </tr>
      <tr bgcolor="#faeae5">
	        <td colspan=2>&nbsp;&nbsp;Motivo:&nbsp;&nbsp;
			  <textarea rows=2 style="border-style: outset;" name="txtMotivo" cols="105" 
				 onfocus="javascript:this.select();"><%=request.getAttribute("txtMotivo") %></textarea>
	        </td>
        <td align="center" valign="middle">         
        <% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('CadastraProc',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
		<% } %> 
		 </td>
			
      </tr>	 
  </table>		
</div>
<div style="position:absolute; left:66px; bottom:66px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0){%>
	<script>document.CadatraProc.numprocesso.focus();</script>	        
<%}else{%>    		
   	<script>document.CadatraProc.numAutoInfracao.focus();</script>	        
<%}%>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>