<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.io.File" %>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="top" class="fontmedia"> 
      		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        		<tr> 
		  			<td width="12%" align="center"> 
            			<table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
              				<tr> 
             				 <% File imagem = new File(path + "/images/logos/" + UsuarioBeanId.getSigOrgaoAtuacao() + ".gif");
          				       if (imagem.exists()) {%>
						    <td><img src="<%=path%>/images/logos/<%=UsuarioBeanId.getSigOrgaoAtuacao() %>.gif" width="65" height="60"></td>
							<%} else {%>
			    			<td><img src="<%=path%>/images/logos/inv.gif" width="65" height="60" ></td>  
							<%}%>
							</tr>
			        	</table>
				    </td>
				    <td width="68%" valign="top" class="fontmaior"> 
			          <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
						<tr>
			  				<td class="fontmaior"><strong><%=AutoInfracaoBeanId.getNomOrgao() %> </strong>  </td>
			  			</tr>
	  					<tr>
		 					<td class="fontmedia"><STRONG><%=UsuarioBeanId.getSigOrgaoAtuacao() %></strong> </td>
			  			</tr> 
    					<tr>
		 					<td class="fontmedia"><STRONG><%=NotifBeanId.getDscNotificacao()%></strong>	</td>
						</tr>
					  </table>	 	
		          </td>
        		  <td width="20%" align="center"> 
		            <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0" class="semborda">
            		  <tr> 
		                <td width="30%">N&ordm; Auto:</td>
        		        <td width="70%"><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong></span></td>
					  </tr>
			          <tr> 
				          <td>Placa:</td>
				          <td><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumPlaca() %></strong></span></td>
				      </tr>
				      <tr>
				          <td>N&ordm; AR:</td>
				          <td><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumNotificacao() %></strong></span></td>
				      </tr>
					</table>
		          </td>
			   </tr>
			</table>
		    <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr><td height="1" bgcolor="#000000"></td></tr>
			</table>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
				<tr valign="top"> 
				    <td width="25%" height="75" align="center"></td>
				    <td width="40%" align="center"><font size="2">&nbsp;</font></td>
				    <td width="35%" align="center"><font size="2">Expedi&ccedil;&atilde;o<br>
					    <%if (("3,4,5".indexOf(NotifBeanId.getCodNotificacao()))>=0){
	                 	if (AutoInfracaoBeanId.getDatNotifPen().length()>0){%>
	                 	<%=AutoInfracaoBeanId.getDatNotifPen() %>
	                  <%}else{%>
	                  <%=AutoInfracaoBeanId.getDatStatus() %>
	                  <%}%>
                 <%}else{%>
                 		<%if (AutoInfracaoBeanId.getDatNotifAut().length()>0){%>
	                 		<%=AutoInfracaoBeanId.getDatNotifAut() %>
	                  	<%}else{%>
	                  		<%=AutoInfracaoBeanId.getDatStatus() %>
						 <%}	                  
                  }%>	</font></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td>
				    <td height="35" colspan="2" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td> 
				    <td height="35" colspan="2" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %>
					    <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco().length() > 0){%> ,&nbsp; <%}%>
					    <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %> 
					    <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco().length() > 0){ %>&nbsp;/ &nbsp;
					    <%}%>
					    <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td> 
				    <td height="35" colspan="2" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro() %></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td> 
				    <td height="35" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt() %></td>
				    <td align="left" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomCidade() %>&nbsp;</td>
				</tr>
			</table>
			<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				 <tr><td height="77"></td></tr>
			</table>
		</td>
	</tr>
</table>