<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama os beans -->
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" />
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: auto;">
	
<!--T�TULO DO M�DULO-->
	  <table align="center" width="468" border="0" cellpadding="2" cellspacing="0">
		<tr>
        	<td align="left" width="36%"><font color="#000000"><strong>N&ordm; DO AUTO:</strong>&nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao()%></font></td>
        	<td align="center" width="39%"><font color="#000000"><strong>PROCESSO:</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso()%></font></td>
        	<td align="right" width="25%"><font color="#000000"><strong>PLACA:</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca()%></font></td>
		</tr>
	  </table>
<br>
<!--FIM T�TULO DO M�DULO-->

<!--TELA DE IMPRESSAO RESULTADO-->
<%
  String largura = ParamOrgBeanId.getParamOrgao("LARGURA_AI_DIG", "470", "7");
  if ((largura == null) || largura.equals("")) largura = "470";
  String altura = ParamOrgBeanId.getParamOrgao("ALTURA_AI_DIG", "903", "7");
  if ((altura == null) || altura.equals("")) largura = "903";
%>
<div align="center" style="position:absolute; width: 100%; top: 18px;">
	<img id="imagemAI" alt="Imagem do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getAIDigitalizado().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getAIDigitalizado().getParametro(ParamSistemaBeanId)%>&header=#" width="<%=largura%>" height="<%=altura%>"> 
</div>
<!--FIM TELA DE IMPRESSAO RESULTADO-->
<script>window.print();</script>
 </form>
</BODY>
</HTML>
