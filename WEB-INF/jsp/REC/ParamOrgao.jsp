<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>     

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgId" scope="request" class="ACSS.OrgaoBean" /> 
<jsp:useBean id="paramId" scope="request" class="REC.ParamOrgBean" /> 
<jsp:useBean id="SistemaBeanId"     scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="ParamOrgBeanId"    scope="session" class="REC.ParamOrgBean" /> 
<jsp:useBean id="ParamSistemaBeanId"    scope="session" class="ACSS.ParamSistemaBean" /> 
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<%  String CorLinha = "#EFF5E2" ;
	String CorTitulo = "#DEEBC2";
	String CorMedia = "#F5D9D0";
	String nomeSistema= "RECURSO" ; 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema();
	if ("REG".equals(mySistema)) {
		 CorLinha = "#EFF5E2" ;
		 CorTitulo = "#a8b980";
		 CorMedia = "#E4F2C5";
		 nomeSistema = "REGISTRO"; 		 
	}		
	if ("PNT".equals(mySistema)) {
		 CorLinha = "#d9f4ef";
		 CorTitulo = "#8AAEAE";
		 CorMedia = "#C0E1DF";
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 CorLinha = "#F8EFD3" ;
		 CorTitulo = "#993300";
		 CorMedia = "#E5D9BA";
		 nomeSistema= "GERENCIAL" ;		 		 
	}
	if ("REC".equals(mySistema)) {
		 CorLinha = "#faeae5";
		 CorTitulo = "#BE7272";
		 CorMedia = "#F5D9D0";
		 nomeSistema= "RECURSO" ;		 
	}	
 %>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

myOrg = new Array(<%= OrgId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}


function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'buscaOrgao':
		if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{		
			if (fForm.codOrgao.value==0) 
			{
				alert("�rg�o n�o selecionado. \n")
			}
			else 
			{
				bProcessando=true;
				fForm.acao.value=opcao
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	  		  
			}
		}	
	  		break ; 
		
		case 'A':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				}
				else 
		  			alert(sErro)
			}
	  		break ;
	  
		case 'C':
			document.all["escolha"].style.visibility='visible';
			break;
     
		case 'Copiar':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if (fForm.codOrgaoCp.value==0) 
				{
					alert("�rg�o de destino n�o selecionado. \n")
		 		}
				
				if(fForm.codOrgao.value == fForm.codOrgaoCp.value)
				{
					alert( "�rg�o de destino deve ser diferente do �rg�o selecionado. \n")
				}
				else
				{
					fForm.acao.value=opcao
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  				  
				}
			}
			break ;

		case 'R':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if (fForm.atualizarDependente.value=="S") 
				{
					fForm.atualizarDependente.value="N"	  
					fForm.acao.value=opcao
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  
				}
				else 
				{
					close() ;   
				}
			}
			break;
			
		case 'I':
			temp = fForm.j_cmdFuncao.value
			fForm.j_cmdFuncao.value = "construcao" ;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  		  
			fForm.submit();	
			fForm.j_cmdFuncao.value = temp ;		   
			break;
			
		case 'O':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
			else                 
				document.all["MsgErro"].style.visibility='hidden' ; 
			break;  
			
		case 'V':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='show' ; 
			else                 
				document.all["MsgErro"].style.visibility='visible' ; 
			break;	  
	}
}

function confirma()
{
	if(confirm("Deseja incluir os par�metros existentes no org�o selecionado para outro?"))
	{ 
		return true; 
	} 
	return false; 
} 

function veCampos(fForm) 
{
	valid   = true ;
	sErro = "" ;
	// verifica se o popup foi selecionado (value do popupNome = )
	if (fForm.codOrgao.value==0) 
	{
	   valid = false
	   sErro = sErro + "Orgao n�o selecionado. \n"
	}
	// fim de validar combo

	for (k=0;k<fForm.nomDescricao.length;k++) 
	{	

		var ne = trim(fForm.nomDescricao[k].value)
		var np = trim(fForm.nomParametro[k].value)
		var valor = trim(fForm.valParametro[k].value)
  
//   if( ((ne != "") || (np != "")) && (valor == "") ){
//   		sErro = "O campo Valor deve ser preenchido: (seq: " + (k+1) + ")\n" ;
//		valid = false ;
//		return;			   
//	}
	
	
		if( ((ne != "") || (valor != "")) && (np == "") ){
	   		sErro = "O campo Par�metro deve ser preenchido: (seq: " + (k+1) + ")\n" ;
			valid = false ;
			return;			   
		}
		
		if( ((np != "") || (valor != "")) && (ne == "") ){
	   		sErro = "O campo Descri��o deve ser preenchido: (seq: " + (k+1) + ")\n" ;
			valid = false ;
			return;			   
		}
   
		if (ne.length==0) 
			continue ;
   
		<!-- verificar duplicata --> 
		for (m=0;m<fForm.nomDescricao.length;m++) 
		{
	
			var dup 	 = trim(fForm.nomDescricao[m].value)
			var dupNp = trim(fForm.nomParametro[m].value)
		   
			if ((m!=k) && (dup.length!=0) && (dup==ne)) {
			   sErro += "Descri��o do Par�metro em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
			   valid = false ;
			   return;			   
			}
		   
			if ((m!=k) && (dupNp.length!=0) && (dupNp==np)) 
			{
				sErro += "Parametro em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
				valid = false ;	
				return;		   
			}
		}
	}
	return valid ;
}

function j_ajuda(fForm) 
{
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

function j_info(fForm) 
{
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acao.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="parametroForm" method="post" action="">

<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<!--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o 1.0&nbsp;</font>  -->
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%
		if( (RequisicaoBeanId.getCmdFuncao().compareTo("InformacoesCmd")!= 0 ) && (RequisicaoBeanId.getSigFuncao().compareTo("ACSS750")!= 0) )
			if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
				<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%	} else {%>
				<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<% } %></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>

  </table>
  
<input name="j_id"          type="hidden" value="<%=RequisicaoBeanId.getId()%>">
<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">
</div>  

<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:468px; height:23px; z-index:14; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr><td><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">
		<% } %>
		&nbsp;<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
  </table>
</div>
<input name="acao" type="hidden" value=' '>				

  <div id="WK_SISTEMA" style="position:absolute; right: 15px; overflow: visible; z-index: 1; top: 75px; left:50px;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%">
      <tr> 
        <td valign="top" colspan="3"><img src="images/inv.gif" width="3" height="15"></td>
      </tr>
      <tr>
		<td width="150">&nbsp;</td>  
        <td align="left" valign="top"><b>&Oacute;rg&atilde;o:&nbsp;</b> 
          <input name="atualizarDependente" type="hidden" size="1" value="<%= OrgId.getAtualizarDependente() %>"> 
   			<% if (OrgId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codOrgao"> 
				   <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				       <script>
					      for (j=0;j<cod_orgao.length;j++) {
						    document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					      }
				       </script>
				</select>
          <% }
		  
			else { %>
              <input name="codOrgao" type="hidden" size="10" maxlength="20"  value="<%= OrgId.getCodOrgao() %>"> 
		      <input disabled name="sigOrgao" style="border: 0px none;" type="text" size="30" value="<%= OrgId.getSigOrgao() %>">
		   	<% } %>	
			<% if (OrgId.getAtualizarDependente().equals("S")==false){ %>			  
 	             &nbsp;&nbsp; 
                <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaOrgao',this.form);">	
          <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left"></button>
			 
	    <% } %>		
	    </td>
   		  
      </tr>	
      <tr> 
        <td valign="top" colspan="3"><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <tr>
		<% if (OrgId.getAtualizarDependente().equals("S")){ %>			
	 	   <td style="text-align:right" width="33%"  height="15" > 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
     	   </td>
	       <td style="text-align:center" width="34%" > 
                  <button type=button NAME="Copiar"  style="height: 21px; width: 44px; border: none; background: transparent;" onClick="javascript: valida('C',this.form);">	
                  <IMG src="<%= path %>/images/bot_copiar.gif" width="42" height="19"></button>
    	   </td>
	       <td style="text-align:left" width="33%"> 
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
		<%} %>
    </tr>
  </table>  
<!--FIM BOTOES-->  
</div>

<div id="principal" style="position:absolute; left:50px;right: 15px;z-index:50;overflow: visible;visibility: visible; background: #ffffff; top: 140px; height: 33%;">
<div id="espacotitulos" style="position:relative; left:0px; right: 0px;z-index:50;overflow: visible; visibility: visible;">
    <table cellspacing="1" style="width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;height: 18px;">
      <tr bgcolor="<%=CorTitulo%>"> 
        <td width="035"  align="center" rowspan=2><font color="#ffffff"><b>Seq</b></font></TD>
        <td width="410">          <font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Descri��o</b></font></TD>
        <td width="190" rowspan=2><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Valor</b></font></TD>
        <td             rowspan=2><font color="#ffffff"><b>&nbsp;Ordem</b></font></TD>
	  </tr>
      <tr bgcolor="<%=CorTitulo%>"> 
        <td><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Par�metro</b></font></TD>
      </tr>
    </table>
<!--FIM CABEC DA TABELA-->
</div>
 <div style="position: relative; z-index: 60;width:100%; left: 0px; right: 0px;overflow: auto; visibility: visible; height: 100%; top: 0px;">
  
    <table cellspacing="1" style="width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;height: 18px;">
      <% 
					if (OrgId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = OrgId.getParametros().size() ;
					   if (ocor<=0) ocor=OrgId.getParametros(20,5).size() ;
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="<%= CorLinha%>">
		<td width="35" align="center" rowspan=2> 
          <input name="seq"          type="text" readonly size="3" value="<%= i+1 %>">
		  <input name="codParametro" type="hidden"  value="<%= OrgId.getParametros(i).getCodParametro() %>">
		</td>		
		<td width="410">&nbsp;&nbsp;&nbsp;<input name="nomDescricao" type="text" size="75" maxlength="200"  value="<%= OrgId.getParametros(i).getNomDescricao() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();"onChange="this.value=this.value.toUpperCase()"></td>
		<td width="190" rowspan=2>&nbsp;&nbsp;&nbsp;<input name="valParametro" type="text" size="30" maxlength="500"  value="<%= OrgId.getParametros(i).getValParametro() %>"  onfocus="javascript:this.select();" ></td>
		<td  rowspan=2>&nbsp;&nbsp;&nbsp;<input name="numOrdem" type="text"      size = "7" maxlength = "7" value="<%= OrgId.getParametros(i).getNumOrdem() %>" onKeyPress = "javascript:f_num();"></td>
	</tr>
      <tr bgcolor="<%=CorMedia%>">
        <td>&nbsp;&nbsp;&nbsp;<input name="nomParametro" type="text" size="75" maxlength="100"  value="<%= OrgId.getParametros(i).getNomParam() %>"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
	</tr>
	  <tr> 
        <td height="1" colspan="4"></td>			
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<20; i++) {				   %>					
  	<tr bgcolor="<%=CorLinha%>">
		<td width="35" align="center" rowspan="2" > 
          <input name="seq"  readonly  type="text" size="3" value="<%= i+1 %>">
		  <input name="codParametro"    type="hidden"  value="">
		</td>
		<td width="410">&nbsp;&nbsp;&nbsp;<input name="nomDescricao2" readonly type="text" size="75" maxlength="400" value=""></td >
		<td width="190" rowspan=2>&nbsp;&nbsp;&nbsp;<input name="valParametro" readonly type="text" size="30" maxlength="500"  value=""></td>
		<td rowspan=2>&nbsp;&nbsp;&nbsp;<input name="numOrdem" readonly type="text"  size = "7" maxlength = "7" value=""></td>	
        
	</tr>
  	<tr bgcolor="<%=CorLinha%>">
    	<td>&nbsp;&nbsp;&nbsp;<input name="nomParametro" readonly type="text" size="75" maxlength="100"  value=""></td>
	</tr>
	<tr><td height="1" colspan="4"></td></tr>
					   
<%						}					
					}   %>
  </table>
</div>
</div>
<!-- Rodap�-->
<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>

<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:9; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  <img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!--texto 0800-->
<jsp:include page="../sys/telSuporte.jsp" flush="true" /> 
<!--FIM texto 0800-->
<!-- Fim Rodap� -->
<!-- Combo para escoha de orgao para copia de parametros -->
<div id = "escolha"  style="position:absolute; top: 87px; width: 361px; height: 71px; left: 350px; overflow: visible; z-index: 15; visibility: hidden;" > 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr><td width="70%" align="center"><b>&Oacute;rg&atilde;o&nbsp; de Destino:</b>&nbsp; 
         <select name="codOrgaoCp">
           <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
           <script>
	         for (j=0;j<cod_orgao.length;j++) {
			 	if (parametroForm.codOrgao.value!=cod_orgao[j]) {
		    	  document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
				  }
	         }
	     </script>
         </select>
      </td>
	   <td width="30%">
	     <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('Copiar',this.form);">	
        	<IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left">
		 </button>
       </td>
   	  </tr>
	</table>
  </div>
<!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= OrgId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%= OrgId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.parametroForm.codOrgao.length;i++){
    if(document.parametroForm.codOrgao.options[i].value==njint){
       document.parametroForm.codOrgao.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


