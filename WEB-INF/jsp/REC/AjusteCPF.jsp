<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	 switch (opcao) 
	 {    
	   case 'R':
	    	close() ;
			break;
	   case 'ConfirmaAjuste':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
		   		bProcessando=true;
	   
			    if (veCampos(fForm)==true) 
			    {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
				}
		    } 		  
		    break;
		    
	   case 'O':  // Esconder os erro
   		  if (document.layers) 
   		  	fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 
	      	document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
		  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  	}
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	if ( (fForm.numCPF.value == "") && (fForm.numCNPJ.value == "") ){
		alert("Informe o CPF/CNPJ");
		
	}
	else{
	if (atuRadio(fForm.indRespradio,fForm.indRespPontos)) {
	  valid = false
	  sErro = sErro + "Ajustar CPF/CNPJ n�o selecionado. \n"
	}
	if (atuRadio(fForm.IndCPFradio,fForm.IndCPF)) {
	  valid = false
	  sErro = sErro + "CPF/CNPJ n�o selecionado. \n"
	}
	
	preencheValor(fForm.IndCPFradio)

	if (valid==false) alert(sErro) 
	return valid ;
	}
} 
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}
function Lig_CPF() {
 AjusteCPFForm.numCNPJ.value="";             
 return ;
}

function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if (valor.length != 0) {     
     valor = Tiraedt(valor,11);
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}
function Lig_CNPJ() {
 AjusteCPFForm.numCPF.value="";             
 return ;
}

function veCNPJ(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)
  if (valor.length != 0) {  
     valor = Tiraedt(valor,14);
   <!-- validar se cpf se preenchido-->
      if (dig_cgc(valor) == false) { 
	 	 if (forma==0) {
		     alert("CNPJ com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CNPJ com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,2)+"."+valor.substring(2,5)+"."+valor.substring(5,8)+"/"+valor.substring(8,12)+"-"+valor.substring(12,14)      	 
  }
  return true;
}
function preencheValor(objradio) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {		   
		   document.all["IndCPF"].value = objradio[i].value ;
		   break ;
		   }
	}
	
}


</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjusteCPFForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>  
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;">  
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>	
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan="3" height=8></tr>
      <tr bgcolor="#faeae5">
        <td colspan="3">&nbsp;Ajustar CPF/CNPJ:
   	      <input type="radio" name="indRespradio" value="1" Checked class="sem-borda" style="height: 14px; width: 14px;">Propriet�rio          
       	  <input type="radio" name="indRespradio" value="2"  class="sem-borda" style="height: 14px; width: 14px;">Resp. p/ pontos&nbsp;&nbsp;				  		
		  <input name="indRespPontos" type="hidden" value=""></td>
   		<td></td>		
      </tr>
      <tr><td colspan="3" height=5></tr>
      <tr bgcolor="#faeae5">
        <td width="20%"></td>
        <td width="10%">
   	      <input type="radio" name="IndCPFradio" value="1" <%= sys.Util.isChecked(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj(),"1") %> 
   	        class="sem-borda" style="height: 14px; width: 14px;"
   	        onClick="javascript:Lig_CPF(); "
   	        >CPF
        </td>
        <td width="55%">
        	<input name="numCPF" type="text" size="22" maxlength="14" 
        	   value='<%= ("1".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()) ? AutoInfracaoBeanId.getProprietario().getNumCpfCnpjEdt() : "") %>' 
        	   onkeypress="javascript:Mascaras(this,'999.999.999-99');"
        	   onChange = "javascript:veCPF(this,0);"
        	   >
        </td>
        <td width="15%"></td>        
      </tr>
      <tr><td colspan="3" height=5></tr>      
      <tr bgcolor="#faeae5">
	    <td></td>
        <td>
          <input type="radio" name="IndCPFradio" value="2" <%= sys.Util.isChecked(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj(),"2") %>
			 class="sem-borda" style="height: 14px; width: 14px;"
			 onClick="javascript:Lig_CNPJ();"
			 >CNPJ&nbsp;&nbsp;
		  <input name="IndCPF" type="hidden" value="">			
        </td>
        <td>
        	<input name="numCNPJ" type="text" size="22" maxlength="18" 
        	value='<%= ("2".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()) ? AutoInfracaoBeanId.getProprietario().getNumCpfCnpjEdt() : "") %>'
        	onkeypress="javascript:Mascaras(this,'99.999.999/9999-99');"
        	onChange = "javascript:veCNPJ(this,0);">        	
        </td>
        <td width="15%"><button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ConfirmaAjuste',this.form);">	
		   <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" style="margin-left: 4px; margin-top:0px; margin-bottom: 0px" >
		  </button>
		</td>        
      </tr>
    </table>
<% } %>	
</div>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
 </body>
</html>
