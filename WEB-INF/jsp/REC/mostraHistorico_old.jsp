<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;
		  
	   case 'Imprimehistorico':  
			fForm.acao.value=opcao;
	  		fForm.target= "_blank";
	   		fForm.action = "acessoTool";  
	  		fForm.submit();
       	    break;	  
		    
  }
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="mostraHistForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="numAutoInfracao" value="<%=AutoInfracaoBeanId.getNumAutoInfracao() %>">
<input type="hidden" name="numPlaca" value="<%=AutoInfracaoBeanId.getNumPlaca()%>">
<input type="hidden" name="numProcesso" value="<%=AutoInfracaoBeanId.getNumProcesso()%>">

<!--DIV QUE IDENTIFICA O AUTO -->

<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
      <tr bgcolor="#faeae5" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan="2"><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA HIST�RICOS &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('Imprimehistorico',this.form);"> 
              <img src="<%= path %>/images/REC/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">        
		</button>
        </b></td>        
      </tr>
      <tr><td colspan="4" height="2"></td></tr>	  
      <tr bgcolor="#faeae5" height="18">
        <td width="88"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="150"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="217"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="257"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan="4" height="2"></td></tr>
      <tr bgcolor="#faeae5" height="14">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan="2"><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
				&nbsp;(<%=AutoInfracaoBeanId.getDatStatus() %>)</td>
      </tr>
      <tr><td colspan="4" height="2"></td></tr>
      <tr bgcolor="#faeae5" height="14">
		<td>&nbsp;&nbsp;<strong>Agente:&nbsp;</strong></td>
		<td><%=AutoInfracaoBeanId.getDscAgente() %></td>
        <td><strong>Unidade:&nbsp;</strong><%=AutoInfracaoBeanId.getDscUnidade() %></td>
        <td><strong>Lote:&nbsp;</strong><%=AutoInfracaoBeanId.getDscLote() %></td>
      </tr>
      
      <tr><td colspan="4" height="2"></td></tr>	        
      <tr bgcolor="#faeae5" height="14">
		<td>&nbsp;&nbsp;<strong>Autua��o em:&nbsp;</strong></td>
        <td colspan="2"><%=AutoInfracaoBeanId.getDatNotifAut() %>&nbsp;-&nbsp;
			          <%=AutoInfracaoBeanId.getNomMotivoNotifAut() %>
        </td>
        <td><strong>AR:&nbsp;</strong><%=AutoInfracaoBeanId.getNumNotifAut() %>
        	&nbsp;&nbsp;(Lote:&nbsp;<%=AutoInfracaoBeanId.getNumARNotifAut() %>)
        </td>
      </tr>
      <tr><td colspan="4" height="2"></td></tr>	        
      <tr bgcolor="#faeae5" height="14">
		<td>&nbsp;&nbsp;<strong>Penalidade em:&nbsp;</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getDatNotifPen() %>&nbsp;-&nbsp;
			          <%=AutoInfracaoBeanId.getNomMotivoNotifPen() %>
        </td>
        <td><strong>AR:&nbsp;</strong><%=AutoInfracaoBeanId.getNumNotifPen() %>
        	&nbsp;&nbsp;(Lote:&nbsp;<%=AutoInfracaoBeanId.getNumARNotifPen() %>)
        </td>
      </tr>     
  </table>    
</div>

<!--DIV COM OS HISTORICOS --> 
<div class="divPrincipal" style="top: 200px; height:20%;">
<div id="hist2" style="position:relative; left:0px; right: 0px; height:150px; z-index:60; width: 100%;overflow: auto; visibility: visible;">   
   <%   int a =AutoInfracaoBeanId.getHistoricos().size()+1;
		Iterator itx  = AutoInfracaoBeanId.getHistoricos().iterator() ;
		REC.HistoricoBean hist = new REC.HistoricoBean();
		if 	(itx.hasNext()){
			while (itx.hasNext()) {
				a--;
				hist =(REC.HistoricoBean)itx.next() ;	
	%>  
	<!--dados fixos para todo os eventos-->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr bgcolor="#f5d6cc" height="18"> 
        <td>&nbsp;&nbsp;<strong><%=a%>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;Evento:&nbsp;</strong></td>
        <td>&nbsp;<strong><%= hist.getNomEvento() %></strong></td>
        <td ></td>
        <td><strong>Origem:</strong></td>
        <td colspan="2" ><strong>&nbsp;<%=hist.getNomOrigemEvento() %></strong>&nbsp;&nbsp;</td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr bgcolor="#faeae5" height="14"> 
        <td>&nbsp;&nbsp;<strong>Status:</strong></td>
        <td colspan="3">&nbsp;<%=hist.getNomStatus() %> - <%=hist.getCodStatus() %></td>
        <td><strong>Data Status:</strong></td>
        <td><%=hist.getDatStatus() %></td>
      </tr>
      <tr><td height="2"></td>  </tr>
      <tr bgcolor="#faeae5" height="14"> 
        <td width="88" >&nbsp;&nbsp;<strong>Executado por:</strong></td>
        <td width="271">&nbsp;<%=hist.getNomUserName() %></td>
        <td width="97"><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
        <td width="93"><%=hist.getCodOrgaoLotacao() %></td>
        <td width="65"><strong>Data Proc.:</strong></td>
        <td width="106"><%=hist.getDatProc() %></td>
      </tr>
      <tr><td height="2"></td></tr>
   </table> 
   <!--fim dados fixos par todo os eventos-->

<% if ("053".equals(hist.getCodEvento())== true){ %>
<!--============ EVENTO: ENVIO PARA O BANCO  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>Tipo Opera��o :</strong></td>
        <td width="632"><%=hist.getTxtComplemento06() %></td>
      </tr>
    </table>
    
<%}else if ("200".equals(hist.getCodEvento())&& !hist.getCodOrigemEvento().equals("200")&& !hist.getTxtComplemento05().equals("")){%>
<!--============ EVENTO: INCLUS�O DE AUTUA��O =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5">
        <td width="88">&nbsp;&nbsp;<strong>Nome Arquivo:</strong></td>
        <td style="text-align: left;">&nbsp;<%=hist.getTxtComplemento05()%></td>
      </tr>
      <tr><td height="2" colspan="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Data Receb.:</strong></td>
        <td style="text-align: left;">&nbsp;<%=hist.getTxtComplemento06()%></td>
      </tr>
    </table>    
    
<%}else if ("406".equals(hist.getCodEvento())== true){ %>
<!--============ EVENTO: REGISTRAR AR-REC/N�O REC  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="156">&nbsp;&nbsp;<strong>C&oacute;d. do Retorno de Entrega :</strong></td>
        <td width="564"><%=hist.getTxtComplemento01() %></td>
      </tr>
    </table>


<%}else if ("405".equals(hist.getCodEvento())){%>
<!--============ EVENTO: CANCELAMENTO DE AUTOS =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5">
        <td width="156">&nbsp;&nbsp;<strong>Data Cancelamento :</strong></td>
        <td width="564"><%=hist.getTxtComplemento02()%></td>
      </tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td width="156">&nbsp;&nbsp;<strong>Documento Gerador :</strong></td>
        <td width="564"><%=hist.getTxtComplemento03()%></td>
      </tr>
    </table>


<%}else if ("903".equals(hist.getCodEvento())){%>
<!--============ EVENTO: CANCELAMENTO DE AUTOS =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Processo :</strong></td>
        <td width="630" colspan="2"><%=hist.getNumProcesso()%></td>  
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5">  
        <td width="90" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Motivo Cancel. :</strong></td>
        <td width="630"><%=hist.getTxtComplemento04()%></td>
      </tr>
      <%if (!hist.getTxtComplemento03().equals("")){%>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5">  
        <td width="90" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Doc. Gerador :</strong></td>
        <td width="630"><%=hist.getTxtComplemento03()%></td>
      </tr>
      <%}%>
    </table>

<%}else if ("204".equals(hist.getCodEvento())||"324".equals(hist.getCodEvento())||"203".equals(hist.getCodEvento())||"323".equals(hist.getCodEvento()) ) { %>
<!--============ EVENTO: REGISTRAR AR-REC/N�O REC  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>C&oacute;d. de Retorno:</strong>&nbsp;<%=hist.getTxtComplemento01() %>&nbsp;-&nbsp;<%=hist.getTxtComplemento03() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5"  height="16">        
         <td>&nbsp;&nbsp;<strong>Data da Notifica��o:</strong>&nbsp;<%=hist.getTxtComplemento02() %></td>
      </tr>
    </table>

<%}else if ("206".equals(hist.getCodEvento())||"326".equals(hist.getCodEvento())||"334".equals(hist.getCodEvento()) ) { %>
<!--============ EVENTO: ABRIR DEFESA PR�VIA/TRI =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="461">&nbsp;<%=hist.getTxtComplemento01() %></td>
        <td width="65"><strong>Tipo :</strong></td>
        <td width="106"><%= "TR".equals(hist.getTxtComplemento02()) ? "TRI" : hist.getTxtComplemento02() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Respons�vel :</strong></td>
        <td>&nbsp;<%=hist.getTxtComplemento04() %></td>
        <td><strong>Abertura :</strong></td>
        <td><%=hist.getTxtComplemento03() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
      <td valign="top">&nbsp;&nbsp;<strong>Doc. gerador:</strong></td>
  	    <td colspan="3">&nbsp;&nbsp;<textarea readonly cols="100" rows="3" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" ><%=hist.getTxtComplemento07() %></textarea></td>
      </tr>  
    </table>

<%}else if (  "207".equals(hist.getCodEvento())== true){ %>		
<!--============ EVENTO: PARECER JUR�DICO =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td>&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td><%=hist.getTxtComplemento01() %></td>
        <td colspan=2><strong>C&oacute;d. do Respons&aacute;vel :</strong>&nbsp;<%=hist.getTxtComplemento05() %></td>
        <td colspan=2><strong>Parecer :&nbsp;</strong>
		<% if ("D".equals(hist.getTxtComplemento03())){%>
				Deferido
		<%}else if("E".equals(hist.getTxtComplemento03())){%>
				Estornado 
		<%}else if("I".equals(hist.getTxtComplemento03())){%>		
				Indeferido
				
		<%}else{%>
		<%}%>
		</td>		
      </tr>
      <tr> <td colspan="6" height="2"></td> </tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td colspan=5>
	         <textarea readonly cols="120" rows="3" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
      <tr> <td colspan=6 height="2"></td> </tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>C&oacute;d. da Junta : </strong></td>
        <td width="271"><%=hist.getTxtComplemento06() %></td>
        <td width="97"><strong>C&oacute;d. do Relator:</strong></td>
        <td width="93"><%=hist.getTxtComplemento07() %></td>
        <td width="65"><strong>Data:</strong></td>
        <td width="106"><%=hist.getTxtComplemento02() %></td>
      </tr>
  </table>

<%}else if ( "208".equals(hist.getCodEvento())|| "328".equals(hist.getCodEvento())|| "335".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: TROCA JUNTA/RELATOR  =================================================================--> 
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>Requerimento: </strong></td>
        <td width="461"><%=hist.getTxtComplemento01() %></td>
		<td width="65"><strong>Data da Troca : </strong></td>
        <td width="106"><%=hist.getTxtComplemento02() %></td>
      </tr>
      <tr> <td colspan=4 height="2"></td> </tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>C&oacute;d. da Junta : </strong></td>
        <td><%=hist.getTxtComplemento03() %></td>
        <td colspan="2" ><strong>C&oacute;d. do Relator:</strong>&nbsp;<%=hist.getTxtComplemento04() %></td>
      </tr>
    </table>

<%}else if ("209".equals(hist.getCodEvento())||"329".equals(hist.getCodEvento())|| "336".equals(hist.getCodEvento())== true){ %>
<!--============ EVENTO: RESULTADO DE DEFESA PR�VIA =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>Requerimento:</strong></td>
        <td width="271"><%=hist.getTxtComplemento01() %></td>
        <td width="97"><strong>Resultado :&nbsp;</strong>		
        <td width="93">
		<% if ("D".equals(hist.getTxtComplemento03())){%>
				Deferido
		<%}else if("E".equals(hist.getTxtComplemento03())){%>
				Estornado 
		<%}else if("I".equals(hist.getTxtComplemento03())){%>		
				Indeferido
				
		<%}else{%>
		
		<%}%>
		</td>		

        <td width="65"><strong>Data:</strong></td>
        <td width="106"><%=hist.getTxtComplemento02() %></td>		
      </tr>
      <tr> <td colspan="6" height="2"></td> </tr>
      <tr bgcolor="#faeae5" height="16">
        <td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td colspan=5>
	         <textarea cols="120" rows="3" style="border: 0px none;  text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
    </table>

<%}else if  ("210".equals(hist.getCodEvento())|| "330".equals(hist.getCodEvento())|| "337".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: ESTORNO DE RESULTADO =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>Requerimento:</strong></td>
        <td width="461"><%=hist.getTxtComplemento01() %></td>
        <td width="65"><strong>Data do Estorno:</strong></td>
        <td width="106"><%=hist.getTxtComplemento02() %></td>		
      </tr>
      <tr> <td colspan="4" height="2"></td> </tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td colspan=3>
            <textarea cols="120" rows="3" style="border: 0px none; text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
	</table>

<% }else if  ("211".equals(hist.getCodEvento())|| "331".equals(hist.getCodEvento()) || "338".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: ENVIO DEF. PREVIA PARA D.O. =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>C&oacute;d. Relat&oacute;rio D.O.:</strong></td>
        <td width="461"><%=hist.getTxtComplemento02()%></td>
        <td width="65"><strong>Data de Envio para Publica&ccedil;&atilde;o :</strong></td>
        <td><%=hist.getTxtComplemento01()%></td>
      </tr>
	</table>

<% }else if  ("212".equals(hist.getCodEvento())|| "332".equals(hist.getCodEvento()) || "339".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: ENVIO DEF. PREVIA PARA D.O. =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88"></td>
        <td width="461"></td>
        <td width="65"><strong>Data de Exclus�o:</strong></td>
        <td><%=hist.getTxtComplemento01()%></td>
      </tr>
	</table>

<%}else if ("213".equals(hist.getCodEvento())|| "333".equals(hist.getCodEvento()) || "340".equals(hist.getCodEvento())){  %>
<!--============ EVENTO: ATUALIZACAO DATA DO D.O. =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="110">&nbsp;&nbsp;<strong>C&oacute;d. 
          Relat&oacute;rio D.O.:</strong></td>
        <td width="159"><%=hist.getTxtComplemento02() %></td>
        <td width="266"><strong>Data 
          de Publica&ccedil;&atilde;o no D.O. :</strong>&nbsp;<%=hist.getTxtComplemento01() %></td>
        <td>&nbsp;</td>
      </tr>
	</table>

<%}else if ("214".equals(hist.getCodEvento())== true){  %>
<!--=================== EVENTO: EXECUTAR TRI  ==============================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="271" ><%=hist.getTxtComplemento11()%></td>
        <td width="97" ></td>
        <td width="93" ></td>				
        <td width="65" ><strong>Data Req. :</strong></td>
        <td><%=hist.getTxtComplemento01()%></td>
      </tr>
	 <tr><td height="2"></td></tr>	  
      <tr bgcolor="#faeae5" height="16">
	    <td><strong>&nbsp;&nbsp;Nome :</strong></td>
      	<td><%=hist.getTxtComplemento03()%></td>
      	<td><strong>Endere&ccedil;o :</strong></td>
   	  	<td colspan="3"><%=hist.getTxtComplemento04()%></td>
      </tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>Bairro :</strong></td>
        <td width="156"><%=hist.getTxtComplemento05()%></td>
        <td width="57"><strong>Cidade :</strong></td>
        <td width="172"><%=hist.getTxtComplemento06()%></td>
        <td width="22"><strong>UF :</strong></td>
        <td width="51"><%=hist.getTxtComplemento07()%></td>        
        <td width="80"></td>
        <td width="29"><strong>CEP :</strong></td>
        <td><%=hist.getTxtComplemento08()%></td>
      </tr>
      <tr><td height="2"></td></tr>
    </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="88">&nbsp;&nbsp;<strong>CPF : </strong></td>
        <td width="156" ><%=hist.getTxtComplemento02() %></td>
        <td width="136" ><strong>N� CNH / Perm. p/ Dirigir :</strong></td>
        <td width="94"  ><%=hist.getTxtComplemento09() %></td>
        <td width="47"  ><strong>UF CNH</strong><strong> :</strong></td>
        <td><%=hist.getTxtComplemento10() %></td>
      </tr>
    </table>

<%}else if ( "226".equals(hist.getCodEvento()) || "350".equals(hist.getCodEvento()) || "351".equals(hist.getCodEvento()) ||
			"352".equals(hist.getCodEvento())  || "215".equals(hist.getCodEvento()) ){  
			%>
<!--============ EVENTO: ESTORNO DE DEFESA PR�VIA/RECURSO =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88" >&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="271"><%=hist.getTxtComplemento01() %></td>
        <td><strong>Data do Estorno:</strong>&nbsp;<%=hist.getTxtComplemento02() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
        <td colspan="2" >
            <textarea cols="120" rows="3" style="border: 0px none;  text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
</table>

<%}else if ("381".equals(hist.getCodEvento())){ 
    %>
<!--============ EVENTO: ENVIO DO =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>Requerimento&nbsp;:&nbsp;</strong></td>
        <td width="267"><%=hist.getTxtComplemento03()%></td>
        <td width="41">&nbsp;&nbsp;<strong>Data:&nbsp;</strong></td>
        <td width="155"><%=hist.getTxtComplemento01()%></td>
        <td><strong>N&ordm ATA:&nbsp</strong><%=hist.getTxtComplemento02()%></td>		
      </tr>
      <tr> <td colspan="5" height="2"></td> </tr>
    </table>

<%}else if ("383".equals(hist.getCodEvento()) || "388".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: ATUALIZA DATA DO =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88">&nbsp;&nbsp;<strong>Requerimento&nbsp;:&nbsp;</strong></td>
        <td width="267"><%=hist.getTxtComplemento02()%></td>
        <td width="41">&nbsp;&nbsp;<strong>Data:&nbsp;</strong></td>
        <td width="155"><%=hist.getTxtComplemento01()%></td>
        <td><strong>N&ordm ATA:&nbsp</strong><%=hist.getTxtComplemento03()%></td>		
      </tr>
      <tr> <td colspan="5" height="2"></td> </tr>      
    </table>

<%}else if  ("382".equals(hist.getCodEvento())|| "389".equals(hist.getCodEvento()) ){ %>
<!--============ EVENTO: ESTORNO DO ENVIO P/DO 1 E 2 INST. =================================================================--> 
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="90">&nbsp;&nbsp;<strong>Requerimento&nbsp;:</strong></td>
        <td width="269"><%=hist.getTxtComplemento05() %></td>
        <td width="90"><strong>Data do Estorno:&nbsp;</strong></td>
        <td><%=hist.getTxtComplemento01()%></td>		
      </tr>      
	</table>

<%}else if ("410".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: AJUSTE DE NUMERO DE PROCESSO ====================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88" >&nbsp;&nbsp;<strong>Novo Processo:</strong></td>
        <td width="271"><%=hist.getTxtComplemento01() %></td>
        <td><strong>Data do Processo:</strong>&nbsp;<%=hist.getTxtComplemento02() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
        <td colspan="2" >
            <textarea cols="120" rows="3" style="border: 0px none; text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
</table>

<%}else if ("414".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: AJUSTE DE CPF ====================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16"> 
        <td width="120">&nbsp;&nbsp;<strong>Tipo de Altera��o :&nbsp;</strong></td>
        <td width="170"><%=	(hist.getTxtComplemento01().equals("1") ? "Propriet�rio" : "Respons�vel pelos pontos")  %></td>
        <td width="70">&nbsp;&nbsp;<strong>Novo CPF :&nbsp;</strong></td>
        <td width="130"><%String cpf = "";
        					if (hist.getTxtComplemento02().length()<14){ 
        					cpf +="00000000000";
	  	                    cpf = cpf.substring(0,cpf.length()- hist.getTxtComplemento02().length())+hist.getTxtComplemento02();
	  	                    cpf = cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11) ;
    						}else{ 
									if (hist.getTxtComplemento02().length()<18) {
									cpf += "                    ";
									cpf = hist.getTxtComplemento02().substring(0,2)+"."+hist.getTxtComplemento02().substring(2,5)+"."+hist.getTxtComplemento02().substring(5,8)+"/"+hist.getTxtComplemento02().substring(8,12)+"-"+hist.getTxtComplemento02().substring(12,14);
        							}
        					 }%>	
        
        					<%=cpf%> 
		
		
		</td>
        <td width="90">&nbsp;&nbsp;<strong>CPF Antigo :&nbsp;</strong></td>
        <td><%String cpfAnt = "";
        					if (hist.getTxtComplemento03().length()<14){ 
        					cpfAnt +="00000000000";
	  	                    cpfAnt = cpfAnt.substring(0,cpfAnt.length()- hist.getTxtComplemento03().length())+hist.getTxtComplemento03();
	  	                    cpfAnt = cpfAnt.substring(0,3)+"."+cpfAnt.substring(3,6)+"."+cpfAnt.substring(6,9)+"-"+cpfAnt.substring(9,11) ;
    						}else{ 
									if (hist.getTxtComplemento03().length()<18) {
									cpfAnt += "                    ";
									cpfAnt = hist.getTxtComplemento03().substring(0,2)+"."+hist.getTxtComplemento03().substring(2,5)+"."+hist.getTxtComplemento03().substring(5,8)+"/"+hist.getTxtComplemento03().substring(8,12)+"-"+hist.getTxtComplemento03().substring(12,14);
        							}
        					 }%>	
        
        					<%=cpfAnt%>         
        
       </td>
        
      </tr>
    </table>

<%}else if ("411".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: AJUSTE DE STATUS ===================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="88" >&nbsp;&nbsp;<strong>Novo Status:</strong></td>
        <td width="271"><%=hist.getTxtComplemento01() %></td>
        <td><strong>Data do Status:</strong>&nbsp;<%=hist.getTxtComplemento02() %></td>
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
        <td colspan="2" >
            <textarea cols="120" rows="3" style="border: 0px none; text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
        </td>
      </tr>
</table>

<% }else if ("239".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: AJUSTE DE STATUS ===================================================--> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
	  <tr bgcolor="#faeae5" height="16"> 
	    <td>&nbsp;&nbsp;<strong>Observa��o :</strong></td>
	    <td colspan="2" >
	        <textarea cols="120" rows="3" style="border: 0px none; text-align: left; background: transparent;" ><%=hist.getTxtComplemento04() %></textarea>
	    </td>
	  </tr>
	</table>

<% }else if  ("264".equals(hist.getCodEvento())|| "384".equals(hist.getCodEvento()) || "391".equals(hist.getCodEvento())){ %>
<!--============ EVENTO: ACERTO EM REQUERIMENTO PARA REVIS�O ===========================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5" height="16">
        <td width="92" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td><%=hist.getTxtComplemento04()%></td>
      </tr>
	</table>

<% } else if ("413".equals(hist.getCodEvento())&& hist.getTxtComplemento12().length()>0) { %>
<!--============ EVENTO: AJUSTE MANUAL RESP. PELOS PONTOS ===========================-->
	<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
		<tr height="16">
			<td width="92" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Motivo:</strong></td>
			<td bgcolor="#faeae5"><%=hist.getTxtComplemento12()%></td>
		</tr>
	</table>
<%}%>

<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr> <td height="8"></td>  </tr>
</table>
<% } %>

<%}else{%>
<div id="mensagem" style="position:absolute; left:70px; top:91px; width:538px; height:35px; z-index:10; overflow: visible; visibility: visible;"> 
	<table width="119" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td bgcolor="#faeae5">&nbsp;&nbsp;<strong>AUTO SEM HIST�RICO</strong></td> </tr>	
	</table>
</div>          
<%}%>
</div>
</div>
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %>
<!--FIM_Div Erros-->

</form>
</body>
</html>