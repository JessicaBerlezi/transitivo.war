<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="ParecerJuridicoId"  scope="request" class="REC.ParecerJuridicoBean" /> 
<%	ParecerJuridicoId.setReqs(AutoInfracaoBeanId,RequerimentoId); %>

<jsp:useBean id="RespJuridicoId"  scope="request" class="REC.RespJuridicoBean" /> 
<jsp:setProperty name="RespJuridicoId" property="j_abrevSist" value="REC" />  
<jsp:setProperty name="RespJuridicoId" property="colunaValue" value="num_cpf" />
<jsp:setProperty name="RespJuridicoId" property="popupNome"   value="codRespPJ"  />
<%
   RespJuridicoId.setChecked(RequerimentoId.getCodRespPJ());
   RespJuridicoId.setPopupString("nom_Resp_Parecer,SELECT p.num_cpf,p.nom_Resp_Parecer||' ('||p.NUM_CPF||')' nom_Resp_Parecer "+
    "FROM TSMI_RESP_PARECER p,TSMI_ORGAO o "+
   	"where p.cod_orgao=o.cod_orgao and "+
   	"o.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' "+
   	" ORDER BY nom_Resp_Parecer");
%>   
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
			
	   case 'LeReq':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
					   
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	   
			}
			break;
			
	   case 'ParecerJuridico':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
					   
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	 
				} 
			}		  
		  	break;
		  	
	   case 'O':  // Esconder os erro
   		  if (document.layers) 
   		  	fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 
	      	document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
		  
	   case 'N':  // Esconder os erro
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
			}
			break;  	
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.codRequerimento,"Requerimento",0);
	valid = ValDt(fForm.datPJ,1) && valid
	verString(fForm.datPJ,"Data do Parecer",0);
	verString(fForm.codRespPJ,"Respons�vel pelo Parecer",0);
	if (atuRadio(fForm.codParecerPJradio,fForm.codParecerPJ)) {
	  valid = false
	  sErro = sErro + "Parecer n�o selecionado. \n"
	}
	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ParecJurForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"         type="hidden" value=' '>				
<input name="codParecerPJ" type="hidden" value="">				

<!--IN�CIO_CORPO_sistema--> 
<jsp:include page="lerAutoPlaca.jsp" flush="true" />    
<jsp:include page="apresentaInfracao.jsp" flush="true" />
<div style="position:absolute; left:66px; top:269px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
   
<div id="recurso3" style="position:absolute; left:66px; top:274px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
<% if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0) { %> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr bgcolor="#faeae5">
	  	<% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>			 	 
	        <td height=23 valign="middle" >&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
    	    <td align="left" valign="middle"> 
				<jsp:getProperty name="ParecerJuridicoId" property="reqs" />
	        </td>
	        <td >Data do Parecer:&nbsp;
			  <input type="text" name="datPJ" size="12" maxlength="10" value='<%=RequerimentoId.getDatPJ() %>' 
			  	onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
			</td>
		<% } else { %>
    	    <td height=23 colspan=3 align="left" valign="middle"> 
				<jsp:getProperty name="ParecerJuridicoId" property="msg" />
	        </td>	
		<% } %>
     </tr>
  	 <% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>					 
     <tr><td colspan=3 height=2></td></tr>
     <tr bgcolor="#faeae5">
        <td  height=20 width="13%" valign="middle">&nbsp;&nbsp;Respons&aacute;vel:&nbsp;&nbsp;</td>
    	<td width="50%">		
				<jsp:getProperty name="RespJuridicoId" property="popupString" /> </td>
	    <td width="37%" valign="middle">Parecer :&nbsp;&nbsp;&nbsp; 
        	  <input type="radio" name="codParecerPJradio" value="D" <%= sys.Util.isChecked(RequerimentoId.getCodParecerPJ(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">Deferido 
    	      <input type="radio" name="codParecerPJradio" value="I" <%= sys.Util.isChecked(RequerimentoId.getCodParecerPJ(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">Indeferido&nbsp;&nbsp;
		</td> 		
     </tr>
	<% }  %> 
  </table>
  <% if ("S".equals(ParecerJuridicoId.getEventoOK())) { %>					    
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan=3 height=2></td></tr>
      <tr bgcolor="#faeae5">
	        <td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
    	    <td height=25 width="80%" valign="top"> 
			  <textarea rows=2 style="border-style: outset;" name="txtMotivoPJ" cols="105" 
				
					 onfocus="javascript:this.select();"><%=RequerimentoId.getTxtMotivoPJ() %></textarea>
	        </td>
    	    <td width="7%"  align="right" valign="middle"> 
	        	  <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ParecerJuridico',this.form);">	
	    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
    	    	  </button>
			</td>
      </tr>
  </table>	
  <% } %>  	
<% } %>
</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>

<!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>