<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->
<%@page import="UTIL.TipoRecGeneric"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="REC.ControladorRecGeneric"%>
<%@page import="ACSS.Ata"%>
<%@ page session="true"%>
<%
	String path = request.getContextPath();
%>

<%@ page errorPage="ErrorPage.jsp"%>
<%@ page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ACSS.Ata"%>
<jsp:useBean id="NoticiaId" scope="session" class="ACSS.NoticiaBean" />
<jsp:useBean id="AvisoId" scope="session" class="ACSS.AvisoBean" />
<jsp:useBean id="consultaAutoId" scope="session"
	class="REC.consultaAutoBean" />
<jsp:useBean id="consultaAtaId" scope="session" class="ACSS.Ata" />
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session"
	class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId" scope="request"
	class="sys.RequisicaoBean" />
<jsp:useBean id="ParamOrgBeanId" scope="session"
	class="REC.ParamOrgBean" />

<%
	if (ParamOrgBeanId.getCodOrgao() != null) {
		ControladorRecGeneric.codOrgao = ParamOrgBeanId.getCodOrgao().toString();
		System.out.println("Valor do org�o gestor: " + ParamOrgBeanId.getCodOrgao());
	} else {
		System.out.print("nao");
	}
%>


<%
	int kyz = 0;
	int nyz = 0;
	String mySistema = UsuarioFuncBeanId.getAbrevSistema();
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao();
	String nomeSistema = "CONTROLE DE ACESSO";
	String Cor = "#DFEEF2";
	String Cresult = "#be7272";
	String resultline = "#faeae5";
	if ("REC".equals(mySistema)) {
		Cor = "#faeae5";
		nomeSistema = "RECURSO";
	}
	if ("REG".equals(mySistema)) {
		Cor = "#EFF5E2";
		nomeSistema = "REGISTRO";
	}
	if ("PNT".equals(mySistema)) {
		Cor = "#E9EEFE";
		nomeSistema = "PONTUA��O";
	}
	if ("CID".equals(mySistema)) {
		Cor = "#faeae5";
		nomeSistema = "COMISS�O CIDAD�";
	}
	if ("GER".equals(mySistema)) {
		Cor = "#F8EFD3";
		nomeSistema = "GERENCIAL";
		Cresult = "#B8A47A";
		resultline = "#efeadc";
	}
%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%-- <%@ include file="Css.jsp" %> --%>
<link rel="stylesheet" href="css/transitivo.css">

<style type="text/css">
.linhas td {
	border: 1px solid #ff0000;
}
</style>

<title>DETRAN &#8226;TRANSITIVO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%=path%>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<script type="text/javascript" language="javascript">
function valida_form (opcao, fForm){
	switch (opcao) { 
	case 'IncluirAta':
		if ((trim(fForm.numPlaca.value) =='')){
			alert('Por favor, selecione o tipo da Ata!');
// 			document.getElementById("numero da Ata").focus();
			fForm.acao.value=opcao;
// 			fForm.numPlaca.disabled = false;
			fForm.numAutoInfracao.disabled = false;
			fForm.indPagoradio.disabled = false;
			fForm.qtdeprocesso.disabled = false;
			fForm.De.disabled = false;
// 			fForm.nrAta.disabled = false;
			fForm.LocalizarAutoInfracao.disabled = false;
			ChamPopula(fForm);
			
			return false;
		}
		
		if ((trim(fForm.numAutoInfracao.value) =='')){
			alert('Por favor, preencha o numero da Junta!');
// 			document.getElementById("numero da Ata").focus();
			fForm.numAutoInfracao.focus();
			return false;
		}
		
		if ((trim(fForm.De.value) =='')){
			alert('Por favor, preencha a data da Sess�o!');
// 			document.getElementById("numero da Ata").focus();
			fForm.De.focus();
			
			return false;
		}
		else{
			fForm.acao.value=opcao;
			fForm.codAta.value= "";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool"; 
			fForm.numPlaca.disabled = false;
			fForm.nrAta.disabled = false;
			
		   	fForm.submit();
		}
		  break;
	case 'HabilitaIncluirAta':
		fForm.acao.value=opcao;
		fForm.numPlaca.disabled = false;
		fForm.numAutoInfracao.disabled = false;
		fForm.indPagoradio.disabled = false;
		fForm.qtdeprocesso.disabled = false;
		fForm.De.disabled = false;
		fForm.nrAta.disabled = false;
		fForm.LocalizarAutoInfracao.disabled = false;
		
		break;
	}
}

function callNrAta(fForm, tpAta){
   // fForm.nrAta.value= populaNrAta(fForm);
    var valNrAta = populaNrAta(fForm, tpAta);
    fForm.nrAta.value=  valNrAta;
}

function populaNrAta(fForm, tpAta){
    var x = document.getElementById("indPagoradio"); // tipoAta
    var i = x.selectedIndex;
    
    switch(x.options[i].text){
    case "Def. Previa":
    	  <%Ata ataDp = new Ata();
			ataDp.setTpAta("Def. Previa");
			ataDp.getNextNrAta(ataDp, session.getAttribute("codNatureza").toString());%>
    	  var teste = '<%=ataDp.getNrAta()%>'
    	    	return teste;
    	break;
    case "1� INST�NCIA":
    	 <%Ata ata1P = new Ata();
			ata1P.setTpAta("1� INST�NCIA");
			ata1P.getNextNrAta(ata1P, session.getAttribute("codNatureza").toString());%>
   	  var teste = '<%=ata1P.getNrAta()%>'
   	    	return teste;
    	break;
    case "2� INST�NCIA":
    	 <%Ata ata2P = new Ata();
			ata2P.setTpAta("2� INST�NCIA");
			ata2P.getNextNrAta(ata2P, session.getAttribute("codNatureza").toString());%>
   	  var teste = '<%=ata2P.getNrAta()%>'
   	    	return teste;
    	break;
    case "P.A.E":
		<%
			Ata ataPAE = new Ata();
			ataPAE.setTpAta("PAE");
			ataPAE.getNextNrAta(ataPAE, session.getAttribute("codNatureza").toString());
		%>

		return <%= ataPAE.getNrAta() %>
    case "SELECIONE...":
   	  var teste = '0';
   	    	return teste;
    	break;
	default:
		break;
	}
}


function ChamPopula(fForm){
	 var gerarhtm = document.getElementById('divRecebe');
	    var x = document.getElementById("indPagoradio"); // tipoAta
	    var i = x.selectedIndex;
	    var ano = new Date();
	    var codTipoSolic ;
	    var valNrAta;
	    
	    switch(x.options[i].text){
	    case "Def. Previa":
	    	var tpAta = "Def. Previa";
	        callNrAta(fForm,tpAta);
	        
	    	valNrAta = fForm.nrAta.value;
	    	fForm.qtdeprocesso.value= populaDivRecebValParamDp();
	    	codTipoSolic = 'DP';
	    	var dsAta = valNrAta + "/" + ano.getFullYear() + codTipoSolic;
// 	    	alert(dsAta);
	    	fForm.numPlaca.value= dsAta;
// 	    	document.getElementById("demoAta").innerHTML =	populaDivRecebValParamDp();

	    	break;
	    case "1� INST�NCIA":
	    	var tpAta = "1� INST�NCIA";
	        callNrAta(fForm, tpAta);
	        
	    	valNrAta = fForm.nrAta.value;
	    	fForm.qtdeprocesso.value= populaDivRecebValParam1P();
	    	codTipoSolic = 'PI';
	    	var dsAta = valNrAta + "/" + ano.getFullYear() + codTipoSolic;
	    	fForm.numPlaca.value= dsAta;
// 	    	document.getElementById("demoAta").innerHTML =	populaDivRecebValParam1P();
			
	    	break;
	    case "2� INST�NCIA":
	    	var tpAta = "2� INST�NCIA";
	        callNrAta(fForm, tpAta);
	        
	    	valNrAta = fForm.nrAta.value;
	    	fForm.qtdeprocesso.value= populaDivRecebValParam2P();
	    	codTipoSolic = 'SI';
	    	var dsAta = valNrAta + "/" + ano.getFullYear() + codTipoSolic;
	    	fForm.numPlaca.value= dsAta;
	    	
// 	    	document.getElementById("demoAta").innerHTML =	populaDivRecebValParam2P();
	    	break;
	    case "P.A.E":
	    	var tpAta = "P.A.E";
	    	
	        callNrAta(fForm, tpAta);
	        
	    	valNrAta = fForm.nrAta.value;
	    	fForm.qtdeprocesso.value= populaDivRecebValParam2P();
	    	codTipoSolic = 'PAE';
	    	var dsAta = valNrAta + "/" + ano.getFullYear() + codTipoSolic;
	    	fForm.numPlaca.value= dsAta;
	    	
	    	break;
	    case "SELECIONE...":
	    	fForm.qtdeprocesso.value= "";
	    	fForm.numPlaca.value="";
	    	fForm.nrAta.value = "";
// 	    	document.getElementById("demoAta").innerHTML =	"SELECIONE O TIPO";
	    	break;
	    	default:
	    		break;
	    }
	}

function populaDivRecebValParamDp(){
    
    <%//System.out.println("populaDivRecebValParamDp: daniel" + ParamOrgBeanId.getCodOrgao());  258470
			Ata ataValParamDp = new Ata();
			ataValParamDp.setQtdeAtaProcesso(ataValParamDp.getValParamProcess(ParamOrgBeanId.getCodOrgao(),
					"VAL_GERENCIAMENTO_ATA_DP_1P_2P", "Def. Previa"));%>
    	var teste = '<%=ataValParamDp.getQtdeAtaProcesso()%>'
        return teste;
}

function populaDivRecebValParam1P(){
	
    <%Ata ataValParam1P = new Ata();
			ataValParam1P.setQtdeAtaProcesso(ataValParam1P.getValParamProcess(ParamOrgBeanId.getCodOrgao(),
					"VAL_GERENCIAMENTO_ATA_DP_1P_2P", "1� INST�NCIA"));%>
    	var teste = <%=ataValParam1P.getQtdeAtaProcesso()%>
		return teste;
	}

	function populaDivRecebValParam2P() {
<%Ata ataValParam2P = new Ata();
			ataValParam2P.setQtdeAtaProcesso(ataValParam2P.getValParamProcess(ParamOrgBeanId.getCodOrgao(),
					"VAL_GERENCIAMENTO_ATA_DP_1P_2P", "2� INST�NCIA"));%>
	var teste =
<%=ataValParam2P.getQtdeAtaProcesso()%>
	return teste;
	}
</script>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
																	self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;
function valida(opcao, codAta ,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			
			break;
	   case 'IncluirAta':
	   		if(bProcessando){
	   			alert("Opera��o em Processamento, por favor aguarde..." + opcao );
	   		}else{
		   		bProcessando=true;
// 		   		alert("bla" );
				if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
					fForm.codAta.value= "";
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
			 	}
		 	}
		  break;
	   case 'AlterarAta':
	   		if(bProcessando){
	   			alert("Opera��o em Processamento, por favor aguarde..." + opcao );
	   		}else{
		   		bProcessando=true;
//		   		alert("bla" );
					fForm.acao.value=opcao;
					fForm.codAta.value=codAta;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
		 	}
		  break;
		  
	   case 'ExcluirAta':
	   		if(bProcessando){
	   			alert("Opera��o em Processamento, por favor aguarde..." + opcao );
	   		}else{
		   		bProcessando=true;
//		   		alert("bla" );
					fForm.acao.value=opcao;
					fForm.codAta.value=codAta;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
		 	}
		  break;
      case 'ImprimeAuto':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;		  
	  case 'Novo':
		  fForm.acao.value=opcao;
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;
	  case 'N':
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;		  
      case 'ImprimeNots':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;	
      case 'RecebNotif':
      		if (confirm("As Notifica��es devem estar impressas e assinadas. Confirma?")) {
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	   		
			}
			break;		 		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["ConsultaAuto"].target= "_self";
	document.all["ConsultaAuto"].action = "acessoTool";  
	document.all["ConsultaAuto"].submit();	 
}
function MostraAut(opcoes,fForm,numplaca,numauto,autoAntigo) {	
	document.all["acao"].value=opcoes;
	document.all["mostraplaca"].value=numplaca;
	document.all["mostranumauto"].value=numauto;
	document.all["mostraAutoAntigo"].value=autoAntigo;
   	document.all["ConsultaAuto"].target= "_blank";
    document.all["ConsultaAuto"].action = "acessoTool";  
   	document.all["ConsultaAuto"].submit();	 
}

function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	if ( (trim(fForm.numPlaca.value) =='')
		&& (trim(fForm.numAutoInfracao.value)=='') ) {
			sErro += "Nenhuma op��o selecionada. \n"
			valid = false ;	
	}
	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
// 	if(document.all["Ate"].value==''){
// 		valid=false;
// 		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
// 	}

	if(document.all["numPlaca"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : N� da Ata obrigat�rio " ;
	}
	
	if(document.all["numAutoInfracao"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : N� da Ata obrigat�rio " ;
	}

			
	
















<%if ("REC0680".equals(consultaAutoId.getSigFuncao()) == false) {%>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
		if (atuRadio(fForm.indPagoradio,fForm.indPago)) {
		  valid = false
		  sErro = sErro + "Selecione um tipo (Defesa Previa ou Jari). \n"
		}
		if (atuRadio(fForm.indPagoradio,fForm.indPago)) {
		  valid = false
		}
		
















<%}%>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

















</script>

<style type="text/css">
body {
	overflow: auto;
}
</style>
</head>

<body>
	<div id="head">
		<div class="img-head2">
			<div class="img-head1">
				<div style="position: absolute; right: 6px;">
					<div class="img-head-btn"
						style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
					<div class="img-head-btn"
						style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
					<div class="img-head-btn"
						style="background-image: url('/Transitivo/images/detran_help.png');"></div>
				</div>
			</div>
		</div>
		<div id="bread" class="bread">
			<strong class="text">PROCESSO &gt; GERENCIAR ATA</strong>
		</div>
	</div>

	<div id="body" class="body" style="padding-right: 15px;">
		<form name="ConsultaAuto" method="post" action=""
			style="margin-left: 50px; margin-bottom: 0px;">
			<input name="j_token" type="hidden"
				value="<%=RequisicaoBeanId.getToken()%>"> <input
				name="j_cmdFuncao" type="hidden"
				value="<%=RequisicaoBeanId.getCmdFuncao()%>"> <input
				name="j_sigFuncao" type="hidden"
				value="<%=RequisicaoBeanId.getSigFuncao()%>"> <input
				name="j_jspOrigem" type="hidden"
				value="<%=RequisicaoBeanId.getJspOrigem()%>"> <input
				name="j_abrevSist" type="hidden"
				value="<%=RequisicaoBeanId.getAbrevSist()%>"> <input
				name="acao" type="hidden" value=' '> <input name="codAta"
				type="hidden" value=' '> <input name="dscNot" type="hidden"
				size="10" maxlength="20" value=""> <input name="codOrgao"
				type="hidden" value="" /> <input name="codNatureza"
				id="codNatureza" type="hidden"
				value="<%=session.getAttribute("codNatureza")%>" />

			<%-- <%@ include file="consultaAta.jsp" %> --%>

			<div style="margin-bottom: 20px;">
				<table width="100%" cellspacing="0">
					<tbody>
						<tr>
							<td colspan="6" height="5"></td>
						</tr>
						<tr height="20" bgcolor="#faeae5">
							<td colspan="4" height="20"><strong>NR Ata :</strong> <input
								name="nrAta" type="text" value="" size="9" maxlength="9"
								onfocus="javascript:this.select();"
								onkeyup="this.value=this.value.toUpperCase()"
								disabled="disabled"> <strong>Desc Ata :</strong> <input
								name="numPlaca" type="text" value="" size="9" maxlength="9"
								onfocus="javascript:this.select();"
								onkeyup="this.value=this.value.toUpperCase()"
								disabled="disabled"> <strong>Junta :</strong> <input
								name="numAutoInfracao" type="text" value="" size="14"
								maxlength="1" onfocus="javascript:this.select();"
								onkeyup="this.value=this.value.toUpperCase()"
								disabled="disabled"></td>
							<td>Tipo:</td>
							<td><select disabled="disabled" name="indPagoradio"
								id="indPagoradio" onchange="ChamPopula(this.form)">
									<option value="SELECIONE..." label="SELECIONE...">SELECIONE...</option>
									<option value="Def. Previa">Def. Previa</option>
									<option value="1� INST�NCIA">1� INST�NCIA</option>
									<option value="2� INST�NCIA">2� INST�NCIA</option>
									<option value="PAE">P.A.E</option>
							</select> <input name="indPago" type="hidden" value=""></td>
							<td width="13%" style="text-align: right"><strong>Qtde
									Processo: </strong></td>
							<td width="17%">
								<p id="demoAta"></p> <input name="qtdeprocesso" type="text"
								id="qtdeprocesso" size="3" maxlength="3"
								onkeypress="javascript:f_num()"
								onkeyup="this.value=this.value.toUpperCase()"
								disabled="disabled">
							</td>
						</tr>
						<tr>
							<td colspan="6" height="5"></td>
						</tr>
						<tr>
							<td colspan="6" height="5"></td>
						</tr>
						<tr bgcolor="#faeae5">
							<td width="13%" style="text-align: right"><strong>Dt
									Sess�o: </strong></td>
							<td width="17%"><input name="De" type="text" id="De"
								size="12" maxlength="12" value=""
								onkeypress="javascript:Mascaras(this,'99/99/9999');"
								onchange="javascript:ValDt(this,0);"
								onfocus="javascript:this.select();" disabled="disabled">
							</td>
							<td colspan="2" rowspan="2">
								<button type="button"
									style="height: 21px; width: 32px; border: none; background: transparent;"
									name="LocalizarAutoInfracao"
									onclick="javascript:valida_form('IncluirAta',this.form);">
									<img src="/Transitivo/images/ATA/new.png" alt="Confirmar"
										width="26" height="19" title="Incluir">
								</button>
							</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td style="text-align: right; padding-top: 10px;">
								<b>Listar Atas:</b>
							</td>
							<td style="padding-top: 10px;">
								<select style="width: 117px;"
									onchange="consultarAtas(this);">
										<option value="A">Abertas</option>
										<option value="F">Fechadas</option>
								</select>								
								<span id="spanCarregando" hidden="true" style="position:fixed;">
									<img src="/Transitivo/images/carregando-gif.gif" style="width: 60px; height: 33px;" />
									<span style="position: relative; top: -10px; font-size: 12px;">Carregando...</span>
								</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div id="tbContainerDP" style="height: 200px; overflow: auto;">
			</div>

			<div id="tbContainer1P" style="height: 200px; overflow: auto;">
			</div>

			<div id="tbContainer2P" style="height: 200px; overflow: auto;">
			</div>

			<div id="tbContainerPAE" style="height: 200px; overflow: auto;">
			</div>
		</form>
	</div>

	<!--FIM_CORPO_sistema-->
	<input name="acao" type="hidden" value=' '>
	<input name="codAta" type="hidden" value=' '>
	<input name="mostraplaca" type="hidden" value=" ">
	<input name="mostranumauto" type="hidden" value=" ">
	<input name="mostraAutoAntigo" type="hidden" value=" ">
	<input name="ordem" type="hidden" value="Data">

	<!--Bot�o retornar-->
	<!-- <div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;">  -->
	<!-- 	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table"> -->
	<!-- 		<tr>  -->
	<!-- 		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);">  -->
	<%--           <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td> --%>
	<!-- 		</tr> -->
	<!-- 	</table> -->
	<!-- </div> -->
	<!-- Rodap�-->
	<%-- <%@ include file="Rod_Diretiva.jsp" %> --%>

	<div id="footer">
		<div class="img-footer2">
			<div class="img-footer"></div>
			<div class="img-footer-btn" onclick="location.href='/Transitivo'"
				title="Home" style="cursor: pointer;"></div>
		</div>
	</div>
	<!-- Fim Rodap� -->

	<script>
		document.ConsultaAuto.numPlaca.focus();
	</script>

	<!--Div Erros-->
	<%
		String msgErro = consultaAutoId.getMsgErro();
		String msgOk = consultaAutoId.getMsgOk();
		String msgErroTop = "245 px";
		String msgErroLeft = "80 px";
	%>
	<%-- <%@ include file="EventoErro_Diretiva.jsp" %> --%>
	<!--FIM_Div Erros-->

	<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

	<script type="text/javascript">
		var cdNatureza = <%=session.getAttribute("codNatureza")%>
		var tipoRec = <%=TipoRecGeneric.GERENCIAR_ATA_CONSULTAR_ATAS%>

		$(document).ready(function(){
			consultarAtas({ value: "A" });
		});

		function consultarAtas(e) {
			var statusAta = e.value;
			
			exibirCarregando("Carregando atas...");

			$.ajax({
						url : "/Transitivo/ControladorRecGeneric",
						method : "post",
						data : {
							tipoRec : tipoRec,
							cdNatureza : cdNatureza,
							statusAta : statusAta
						},
						success : function(data) {
							construirListaAtas(data);
							esconderCarregando();
						},
						error : function() {
							alert("Erro ao consultar as atas. Favor tentar novamente.");
							esconderCarregando();
						}
					});
		}

		function construirListaAtas(data) {
			construirDivDP(data);
			construirDiv1P(data);
			construirDiv2P(data);
			construirDivPAE(data);
		}

		function construirDivDP(data) {
			var divContainerDP = document.getElementById("tbContainerDP");

			divContainerDP.innerHTML = null;

			var htmlDP = "<table style='width: 100%; font-size: 14px;'>";

			htmlDP += "<tr style='background-color: #be7272; color: white;'>";
			htmlDP += "<th colspan='9' style='text-align: center; text-size: 40px;'>Defesa Pr�via</th>";
			htmlDP += "</tr>";
			htmlDP += "<tr style='background-color: #be7272; color: white;'>";
			htmlDP += "<th>Descri��o da Ata</th>";
			htmlDP += "<th>Quantidade de Processos</th>";
			htmlDP += "<th>Data de Distribui��o</th>";
			htmlDP += "<th>Data de Aviso</th>";
			htmlDP += "<th>Data de Sess�o</th>";
			htmlDP += "<th>Data de Publica��o</th>";
			htmlDP += "<th></th>";
			htmlDP += "<th></th>";
			htmlDP += "</tr>";

			for (var i = 0; i < data.length; i++) {
				var color = "#fff";
				
				if (data[i].TIPO_ATA == "Def. Previa") {																		
					htmlDP += "<tr style='text-align: center; background-color: #faeae5;'>";
					htmlDP += "<td>" + data[i].DES_ATA + "</td>";
					htmlDP += "<td>" + data[i].QTDE_PROCESSO + "</td>";
					htmlDP += "<td>" + data[i].DT_DISTRIBUICAO + "</td>";
					htmlDP += "<td>" + data[i].DT_AVISO + "</td>";
					htmlDP += "<td>" + data[i].DT_SESSAO + "</td>";
					htmlDP += "<td>" + data[i].DATA_PUBLICACAO + "</td>";
					htmlDP += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Editar' onclick='editarAta(this);' /></td>";
					htmlDP += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Excluir' onclick='excluirAta(this);' /></td>";
					htmlDP += "</tr>";

					
				}
			}

			htmlDP += "</table>";
			divContainerDP.innerHTML = htmlDP;
		}

		function construirDiv1P(data) {
			var divContainer1P = document.getElementById("tbContainer1P");

			divContainer1P.innerHTML = null;

			var html1P = "<table style='width: 100%; font-size: 14px;'>";

			html1P += "<tr style='background-color: #be7272; color: white;'>";
			html1P += "<th colspan='9' style='text-align: center; text-size: 40px;'>1� Inst�ncia</th>";
			html1P += "</tr>";
			html1P += "<tr style='background-color: #be7272; color: white;'>";
			html1P += "<th>Descri��o da Ata</th>";
			html1P += "<th>Quantidade de Processos</th>";
			html1P += "<th>Data de Distribui��o</th>";
			html1P += "<th>Data de Aviso</th>";
			html1P += "<th>Data de Sess�o</th>";
			html1P += "<th>Data de Publica��o</th>";
			html1P += "<th></th>";
			html1P += "<th></th>";
			html1P += "</tr>";

			for (var i = 0; i < data.length; i++) {
				if (data[i].TIPO_ATA == "1� INST�NCIA") {																		
					html1P += "<tr style='text-align: center; background-color: #faeae5;'>";
					html1P += "<td>" + data[i].DES_ATA + "</td>";
					html1P += "<td>" + data[i].QTDE_PROCESSO + "</td>";
					html1P += "<td>" + data[i].DT_DISTRIBUICAO + "</td>";
					html1P += "<td>" + data[i].DT_AVISO + "</td>";
					html1P += "<td>" + data[i].DT_SESSAO + "</td>";
					html1P += "<td>" + data[i].DATA_PUBLICACAO + "</td>";
					html1P += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Editar' onclick='editarAta(this);' /></td>";
					html1P += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Excluir' onclick='excluirAta(this);' /></td>";
					html1P += "</tr>";
				}
			}

			html1P += "</table>";
			divContainer1P.innerHTML = html1P;
		}

		function construirDiv2P(data) {
			var divContainer2P = document.getElementById("tbContainer2P");

			divContainer2P.innerHTML = null;

			var html2P = "<table style='width: 100%; font-size: 14px;'>";

			html2P += "<tr style='background-color: #be7272; color: white;'>";
			html2P += "<th colspan='9' style='text-align: center; text-size: 40px;'>2� Inst�ncia</th>";
			html2P += "</tr>";
			html2P += "<tr style='background-color: #be7272; color: white;'>";
			html2P += "<th>Descri��o da Ata</th>";
			html2P += "<th>Quantidade de Processos</th>";
			html2P += "<th>Data de Distribui��o</th>";
			html2P += "<th>Data de Aviso</th>";
			html2P += "<th>Data de Sess�o</th>";
			html2P += "<th>Data de Publica��o</th>";
			html2P += "<th></th>";
			html2P += "<th></th>";
			html2P += "</tr>";

			for (var i = 0; i < data.length; i++) {
				if (data[i].TIPO_ATA == "2� INST�NCIA") {																		
					html2P += "<tr style='text-align: center; background-color: #faeae5;'>";
					html2P += "<td>" + data[i].DES_ATA + "</td>";
					html2P += "<td>" + data[i].QTDE_PROCESSO + "</td>";
					html2P += "<td>" + data[i].DT_DISTRIBUICAO + "</td>";
					html2P += "<td>" + data[i].DT_AVISO + "</td>";
					html2P += "<td>" + data[i].DT_SESSAO + "</td>";
					html2P += "<td>" + data[i].DATA_PUBLICACAO + "</td>";
					html2P += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Editar' onclick='editarAta(this);' /></td>";
					html2P += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Excluir' onclick='excluirAta(this);' /></td>";
					html2P += "</tr>";
				}
			}

			html2P += "</table>";
			divContainer2P.innerHTML = html2P;
		}

		function construirDivPAE(data) {
			var divContainerPAE = document.getElementById("tbContainerPAE");

			divContainerPAE.innerHTML = null;

			var htmlPAE = "<table style='width: 100%; font-size: 14px;'>";

			htmlPAE += "<tr style='background-color: #be7272; color: white;'>";
			htmlPAE += "<th colspan='9' style='text-align: center; text-size: 40px;'>P.A.E</th>";
			htmlPAE += "</tr>";
			htmlPAE += "<tr style='background-color: #be7272; color: white;'>";
			htmlPAE += "<th>Descri��o da Ata</th>";
			htmlPAE += "<th>Quantidade de Processos</th>";
			htmlPAE += "<th>Data de Distribui��o</th>";
			htmlPAE += "<th>Data de Aviso</th>";
			htmlPAE += "<th>Data de Sess�o</th>";
			htmlPAE += "<th>Data de Publica��o</th>";
			htmlPAE += "<th></th>";
			htmlPAE += "<th></th>";
			htmlPAE += "</tr>";

			for (var i = 0; i < data.length; i++) {
				if (data[i].TIPO_ATA == "PAE") {																		
					htmlPAE += "<tr style='text-align: center; background-color: #faeae5;'>";
					htmlPAE += "<td>" + data[i].DES_ATA + "</td>";
					htmlPAE += "<td>" + data[i].QTDE_PROCESSO + "</td>";
					htmlPAE += "<td>" + data[i].DT_DISTRIBUICAO + "</td>";
					htmlPAE += "<td>" + data[i].DT_AVISO + "</td>";
					htmlPAE += "<td>" + data[i].DT_SESSAO + "</td>";
					htmlPAE += "<td>" + data[i].DATA_PUBLICACAO + "</td>";
					htmlPAE += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Editar' onclick='editarAta(this);' /></td>";
					htmlPAE += "<td><input type='button' id='" + data[i].COD_ATA + "' value='Excluir' onclick='excluirAta(this);' /></td>";
					htmlPAE += "</tr>";
				}  	
			}

			htmlPAE += "</table>";
			divContainerPAE.innerHTML = htmlPAE;
		}

		function editarAta(e) {
			valida("AlterarAta", e.id, e.form);
		}

		function excluirAta(e) {
			valida("ExcluirAta", e.id, e.form);
		}
		
		function exibirCarregando(texto){
			var tdCarregando = document.getElementById("spanCarregando");
			
			tdCarregando.hidden = false;
			tdCarregando.children[1].innerHTML = texto;
		}
		
		function esconderCarregando(){
			var tdCarregando = document.getElementById("spanCarregando");
			
			tdCarregando.hidden = true;
		}
	</script>
</body>
</html>