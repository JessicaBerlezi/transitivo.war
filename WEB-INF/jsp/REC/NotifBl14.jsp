<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %><%@ page import="REC.AutoInfracaoBean" %>    
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr> 
    <td align="center" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top"> 
          <td width="55%" align="center" valign="top" class="fontmedia"> 
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr valign="top"> 
                <td width="25%" height="15" valign="top" class="fontmedia"> 
                 &nbsp;&nbsp;&nbsp;<strong><%=AutoInfracaoBeanId.getNumAutoInfracao() %>&nbsp;&nbsp;&nbsp;�rg�o:&nbsp;<%=AutoInfracaoBeanId.getSigOrgao()%></strong></td>
                <td width="25%" align="center" valign="top" class="fontmedia"><strong>Expedi&ccedil;&atilde;o: 
                 <% 
                 if (("3,4,5".indexOf(NotifBeanId.getCodNotificacao()))>=0){
	                 	if (AutoInfracaoBeanId.getDatNotifPen().length()>0){%>
	                 	<%=AutoInfracaoBeanId.getDatNotifPen() %>
	                  <%}else{
	                  		if (AutoInfracaoBeanId.getDatStatus().length()>0)%>                
	                  		<%=AutoInfracaoBeanId.getDatStatus() %>	
	                  	<%}%>	                 
                 <%}else{%>
                 		<%if (AutoInfracaoBeanId.getDatNotifAut().length()>0){%>
	                 		<%=AutoInfracaoBeanId.getDatNotifAut() %>
	                  	<%}else{%>
	                  		<%=AutoInfracaoBeanId.getDatStatus() %>	
						 <%}	                  
                  }%>	
                 	</strong>&nbsp;
	            </td>
              </tr>
              <tr valign="top"> 
                <td colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
              </tr>
              <tr valign="top"> 
                <td colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %> 
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco().length() > 0){%> ,&nbsp; <%}%>
                 	<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %>
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco().length() > 0){ %> 
      				  &nbsp;/ &nbsp;
   				 <%}%>
                 <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %>
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento().length() > 0){ %> 
      				  &nbsp;- &nbsp;
   				 <%}%>                 
                 <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro() %> 
                </td>
              </tr>
              <tr valign="top"> 
                <td colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt() %>&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomCidade() %>&nbsp;</td>
              </tr>
              <tr valign="top"> 
                <td colspan="2" class="fontmaior"> 
                  <!--simulando o c�digo de barras-->
                  <table width="100%" cellpadding="1" cellspacing="0" class="semborda">
                    <tr> 
                      <td class="fontmedia" height="16"><strong>
<% if(AutoInfracaoBeanId.getNumAutoInfracao().length()>0){%>
						<% 						
						   String codBarra = "";
						   String sDataGeracaoCB=((Integer.parseInt(AutoInfracaoBeanId.getCodStatus())< 10)?AutoInfracaoBeanId.getDtGerCBNotificacaoAut():AutoInfracaoBeanId.getDtGerCBNotificacaoPen());
						   String sNotificacao=((Integer.parseInt(AutoInfracaoBeanId.getCodStatus())< 10)?AutoInfracaoBeanId.getNumNotifAut():AutoInfracaoBeanId.getNumNotifPen());
						   
						   if (sNotificacao.length()==0) sNotificacao="000000000";
						   else
							   sNotificacao=sys.Util.lPad(sNotificacao.trim(),"0",9);
							   
					   %>
					   
					   
					   
					   <%if (sDataGeracaoCB.length()>0) {%> 
					   <img src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getArquivo(ParamSistemaBeanId,"DIR_CODIGO_BARRA")%>&contexto=<%=AutoInfracaoBeanId.getParametro("DIR_CODIGO_BARRA")%>" width="240" height="70" alt="" border="0"></strong>											   
                       <%}else{%>
                       <img src="<%=path%>/codigoBarra?codigo=<%=sNotificacao%>&tipo=1&altura=70" width="240" height="70" alt="" border="0"></strong>						
                       <%}%> 

<%}%>                       
					  </td>
                    </tr>
                    <tr> 
                      <td></td>
                    </tr>
                  </table>
                  <!--Fim simulando o c�digo de barras-->
                </td>
              </tr>
            </table>
          </td>
          
          
          <td width="45%" align="center" valign="top" class="fontmedia"> 
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
		        <td height="15" align="left" valign="top" class="fontmenor"><strong><span class="fontmenor">&nbsp;&nbsp;<strong></strong></span>Comprovante 
                  de Entrega - <%=NotifBeanId.getDscNotificacao()%> </strong></td>
		  </tr>
		  </table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" >
              <tr> 
                <td> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="25" valign="top" class="semborda"><span class="fontmenor">&nbsp;&nbsp;<strong>Nome 
                        do recebedor</strong></span><br> </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="40%" height="25" valign="top" class="table_linha_top" ><span class="fontmenor">&nbsp;&nbsp;<strong>Data</strong></span><br> 
                      </td>
                      <td width="60%" valign="top" class="table_linha_top" style="border-left: 1px solid #000000"><span class="fontmenor">&nbsp;&nbsp;<strong>Grau 
                        de relacionamento</strong></span><br> </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="25" valign="top" class="table_linha_top"><span class="fontmenor">&nbsp;&nbsp;<strong>Identifica&ccedil;&atilde;o</strong></span><br> 
                      </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="25" valign="top" class="table_linha_top"><span class="fontmenor">&nbsp;&nbsp;<strong>Assinatura 
                        do recebedor</strong></span><br> </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
        <tr> 
          <td width="50%" height="20" class="fontmenor"><strong>&nbsp;&nbsp;DESTAQUE 
            AQUI</strong></td>
          <td width="40%" align="right"  class="fontmenor"><strong>2-0630</strong></td>
          <td width="10%" align="right" class="fontmenor"><strong>1</strong>&nbsp;&nbsp;</td>
        </tr>
      </table>
<!--Linha--> 
			  <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
					<tr> 
						<td height="1" bgcolor="#000000"></td>
					</tr>
			  </table>
<!--FIM Linha-->
	  </td>
  </tr>
</table>