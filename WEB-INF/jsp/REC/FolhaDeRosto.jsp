	<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objetos -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" />
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="HistoricoBeanId"    scope="request" class="REC.HistoricoBean" /> 
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" /> 
<jsp:useBean id="AbrirRecBeanId" scope="request" class="REC.AbrirRecBean" /> 
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<%  String ExisteProcesso  = (String)request.getAttribute("ExisteProcesso");
 	String assuntoProcesso = "";
	if (HistoricoBeanId.getCodEvento().equals("214")) {
	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_TRI","Troca de Real Infrator","7");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("RD")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_REN_PREVIA","Renuncia de Defesa Pr�via","1");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("DI")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_DP","Indeferimento de Plano - Def.Pr�via","1");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("1I")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_1A","Indeferimento de Plano - 1a Inst�ncia","3");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("2I")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_2A","Indeferimento de Plano - 2a Inst�ncia","4");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("DP") || HistoricoBeanId.getTxtComplemento02().equals("DC")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_DEF_PREVIA","Defesa Pr�via de Infra��o de Tr�nsito","1");
   	}
   	else if (HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_1INSTANCIA","Recurso de Multa","3");
   	}
   	else {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_2INSTANCIA","Recurso de 2a. Inst�ncia","4");
   	}if (ExisteProcesso==null) ExisteProcesso="N" ;
%>
<html>
<head>
<style type="text/css">
td { font-family:Verdana, Arial, Helvetica, sans-serif; 
     font-size: 8px; color: #000000;
	 border-top: 1px solid #000000;
     padding-left: 3px;
     padding-right: 3px;
     line-height: 12px;
}
td .sem-borda {border: 0px none;}
td .sem-bordaTop {border-top: 0px none;}
td.borda1 {border: 1px solid #000000;}
td.borda2 {border: 2px solid #000000;}
.borda-esquerda {border-left: 1px solid #000000;}
table#semlinha  td.sem-borda {border: 0px none;}
table#fontemaior td.fonte-maior {font-family: Verdana, Arial, Helvetica, sans-serif; 
     font-size: 14px; color: #000000;
	 padding-left: 3px; padding-right: 3px;line-height: 25px;
}

.letramedia {font-family: Verdana, Arial, Helvetica, sans-serif; 
     font-size: 9px; color: #000000;
     font-weight: normal;
     font-variant:small-caps}
.letramaior {font-family: Verdana, Arial, Helvetica, sans-serif; 
     font-size: 13px; color: #000000;
     font-weight: normal;
     font-variant:small-caps}
table td.fonteTabEsq {font-family: Verdana, Arial, Helvetica, sans-serif; 
     font-size: 11px; color: #000000;
     font-weight: normal;
  } 
</style>

</head>
<body>
<table id="semlinha" width="100%"  border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="3" valign="top" class="borda2">    <table width="100%"  border="0" cellpadding="0" cellspacing="0" id="fontemaior">
        <tr>
          <% String codBarra = AutoInfracaoBeanId.getNumProcesso().trim();%>
          <td height="118" colspan="2" align= "right" valign="top" class="sem-borda fonte-maior">
          <% if(!AbrirRecBeanId.getNomeImagem().equals("N")){ %>
             <img src="<%=path%>/images/logos/<%= AbrirRecBeanId.getNomeImagem()%>" width="<%=AbrirRecBeanId.getLarguraImageGra()%>" height="<%= AbrirRecBeanId.getAlturaImageGra() %>" vspace="7">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <% }else{
        	  OrgaoBeanId.Le_Orgao(UsuarioBeanId.getCodOrgaoAtuacao(),0);%>
        	 <%= OrgaoBeanId.getNomOrgao()%>
        <%  }%>   
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <img src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getArquivo(ParamSistemaBeanId,"DIR_CODIGO_BARRA_PROCESSO")%>&contexto=<%=AutoInfracaoBeanId.getParametro("DIR_CODIGO_BARRA_PROCESSO")%>" width="240" height="70" alt="" border="0"></td>
        </tr>
        <tr>
          <td width="23%" class="fonte-maior sem-borda"><strong>Processo</strong>&nbsp;</td>
          <td width="77%" class="fonte-maior sem-borda">:&nbsp;<%=AutoInfracaoBeanId.getNumProcesso()%></td>
        </tr>
        <tr>
          <td class="fonte-maior"><strong>Requerente</strong>&nbsp;
          </td>
          <td class="fonte-maior">:&nbsp;<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %><%=HistoricoBeanId.getTxtComplemento03()%>
		    <% } else { %><%=HistoricoBeanId.getTxtComplemento04()%>
	        <% } %>        
          </td>
        </tr>
        <tr>   
          <td class="fonte-maior"><strong>Placa</strong>&nbsp;</td>
          <td class="fonte-maior">:&nbsp;<span class="letramaior"><%=AutoInfracaoBeanId.getNumPlaca()%></span></td>
        </tr>
        <tr>
          <td class="fonte-maior"><strong>Dt. Entrada </strong>&nbsp;
		  </td>
          <td class="fonte-maior">:&nbsp;<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %><%=HistoricoBeanId.getTxtComplemento01()%>
		     <% } else { %><%=HistoricoBeanId.getTxtComplemento03()%>
	         <% } %>  
        </td>
        </tr>
        <tr valign="top">
          <td height="25" class="fonte-maior"><strong>Assunto</strong>&nbsp;</td>
          <td height="25" class="fonte-maior">:&nbsp;<%=assuntoProcesso%></td>
        </tr>
    </table>
      <table width="100%"  border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td class="sem-borda fonteTabEsq"><strong>Auto:</strong></td>
          <td class="sem-borda fonteTabEsq"><strong>Data:</strong></td>
          <td class="sem-borda fonteTabEsq"><strong>Ident. �rg�o:</strong></td>
          <td class="sem-borda fonteTabEsq"><strong>Situa&ccedil;&atilde;o</strong></td>
        </tr>
        <tr>
          <td height="350" valign="top" nowrap class="sem-borda">&nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao()%></td>
          <td valign="top" class="sem-borda">&nbsp;          
              <%=AutoInfracaoBeanId.getDatInfracao()%>
		  </td>
          <td valign="top" class="sem-borda">&nbsp;<%=AutoInfracaoBeanId.getIdentOrgaoEdt()%></td>
          <td valign="top" class="sem-borda">&nbsp;<%=AutoInfracaoBeanId.getSituacao()%></td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td  colspan="3" height="10" class="sem-borda"></td>
  </tr>
  <TR>
 <td  valign="top" class="borda2">      
    <table width="100%"  border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="50%"  class="sem-borda">
          <span class="sem-borda fonte-maior">
          <% if(!AbrirRecBeanId.getNomeImagem().equals("N")){ %>
            <img src="<%=path%>/images/logos/<%= AbrirRecBeanId.getNomeImagem()%>" width="<%=AbrirRecBeanId.getLarguraImagePeq() %>" height="<%=AbrirRecBeanId.getAlturaImagePeq()%>" vspace="5">
          <%}else{
        	  OrgaoBeanId.Le_Orgao(UsuarioBeanId.getCodOrgaoAtuacao(),0);%>
       <%=	  OrgaoBeanId.getNomOrgao()%>
    <%      }
             %>
           <br>
      PROTOCOLO/
      <% if(!AbrirRecBeanId.getSigProtocolo().equals("N")){ %>
             <%=AbrirRecBeanId.getSigProtocolo()%>
      <% } %>       
    </span></td>
        <td colspan="3" valign="top"  class="sem-borda"><fieldset><legend>N� Processo</legend><br>
         &nbsp;<%=AutoInfracaoBeanId.getNumProcesso()%>
         <br><br>
         <%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				   <%=HistoricoBeanId.getTxtComplemento01()%>
			 <% } else { %>
				   <%=HistoricoBeanId.getTxtComplemento03()%>
		     <% } %>  
		 <br><br>
        </fieldset></td>
      </tr>
	  </table>
	   <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="2"><strong>Nome:</strong>&nbsp;
            <%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				<%=HistoricoBeanId.getTxtComplemento03()%>
		    <% } else { %>
				<%=HistoricoBeanId.getTxtComplemento04()%>
	      <% } %>          </td>
          <td width="63%" colspan="2" ><strong>Assunto:</strong>&nbsp;<%=assuntoProcesso%></td>
        </tr>
		</table>
		<table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="37%" ><strong>Placa:</strong>&nbsp;<span class="letramedia"><%=AutoInfracaoBeanId.getNumPlaca()%></span></td>
          <td colspan="2"><strong>Auto:</strong>&nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao()%></td>
          <td width="28%" colspan="2" ><strong>Data:</strong>&nbsp;<%=AutoInfracaoBeanId.getDatInfracao()%></td>
        </tr>
      </table>
      
      <table width="100%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td colspan="3" ><strong>Outros:</strong>&nbsp;<%=HistoricoBeanId.getTxtComplemento07()%></td>
        </tr>
        <tr>
          <td colspan="3" class="sem-borda">&nbsp;</td>
        </tr>
        <tr>
          <td><div align="center"><strong>Data</strong></div></td>
          <td class="borda-esquerda"><div align="center"><strong>Protocolo</strong></div></td>
          <td class="borda-esquerda"><div align="center"><strong>Observa&ccedil;&otilde;es</strong></div></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
          <td class="borda-esquerda">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="3"><div align="center">AS INFORMA&Ccedil;&Otilde;ES S&Oacute; SER&Atilde;O PRESTADAS COM A APRESENTA&Ccedil;&Atilde;O DESTE PROTOCOLO. A SUA PERDA RETARDAR&Aacute; A LOCALIZA&Ccedil;&Atilde;O DO PROCESSO.<br><br> </div></td>
        </tr>
    </table>
      <table   border="0" cellspacing="0" cellpadding="0">
        <tr>
        <% int i = 0;
           String nomeUsuario = "";
           i = UsuarioBeanId.getNomUsuario().lastIndexOf(" ");
           if (i == -1) i = UsuarioBeanId.getNomUsuario().length();
           nomeUsuario = UsuarioBeanId.getNomUsuario().substring(0,i);%>
          <td width="739" class="fonteTabEsq"><strong >Atendido por: </strong>&nbsp;<%=nomeUsuario%></td>
        </tr>
        <tr>
          <td><div align="justify"><strong>
           <% if(!AbrirRecBeanId.getObservacao().equals("N")){ %>
	          <%= AbrirRecBeanId.getObservacao()%>
           <% } %>
          </strong><br>
          <br>
          </div></td>
        </tr>
    </table></TD>
  </tr>
</table>
<%String codEventoFiltro = (String)request.getAttribute("codEventoFiltro");
if ((HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) 
	&& !codEventoFiltro.equals("N") ){ %>
<div class="quebrapagina"></div>
<jsp:include page="consultaHistImp.jsp" flush="true" />
<%} %>
</body>
</html>