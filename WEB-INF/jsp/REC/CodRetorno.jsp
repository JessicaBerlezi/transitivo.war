<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela Codigo de Retorno --> 
<jsp:useBean id="CodRetornoID" scope="request" class="REC.CodigoRetornoBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   switch (opcao) {
   case 'R':
      close() ;   
	  break ;   
   case 'Atualizar':
		if( valida_input(fForm) == true ){
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
		}		 		  
	  	break ;
   case 'I':
     fForm.acao.value=opcao
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function valida_input (form){
	for(var i = 0; i < form.codCodigoRetorno.length ; i ++ ){
		var codCodigoRetorno = form.codCodigoRetorno[i].value
		var desc_codigo_retorno = form.descRetorno[i].value
		var num_codigo_retorno = form.numRetorno[i].value
		if( (num_codigo_retorno != '') && (desc_codigo_retorno == '') ){
			alert( "Descri��o do retorno em branco. \n" )
			return false
		}
		if( (num_codigo_retorno == '') && (desc_codigo_retorno != '') ){
			alert( "C�digo do retorno em branco.\n" )
			return false
		}
	}
	return true
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="relatorForm" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>		

<div id="WK_SISTEMA" style="position:absolute; width:568px; overflow: visible; z-index: 1; top: 66px; left:51px;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="400"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>     
	  </tr>
      <tr>
		  <td align="right" width="20%"><input name="atualizarDependente" type="hidden" size="1" value="<%= CodRetornoID.getAtualizarDependente() %>"></td>
          <td align=left width=350></td>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
		<% if (CodRetornoID.getAtualizarDependente().equals("S")){ %>			
		   <td align="center"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('Atualizar',this.form);">	
        <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>   	    </td>
     	   <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
    	   <td align="center" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
        <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>        </td>
    	   
     	   
		<%} %>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:570px; overflow: auto; z-index: 1; left: 50px; top: 119px; height: 32px"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<tr bgcolor="#993300"> 
        <td width=100 align="center"><a href="#" onClick="javascript: Classifica(retornos,numRetorno,0,'input_text','#faeae5','#faeae5');"><font color="#ffffff"><b>Cod Retorno</b></font></TD>		 	  
        <td width=350 align="center"><a href="#" onClick="javascript: Classifica(retornos,descRetorno,1,'input_text','#faeae5','#faeae5');"><font color="#ffffff"><b>Descri��o do Retorno</b></font></TD>		 
        <td           align="center"><font color="#ffffff"><b>Recebido</b></font></TD>		 
	</tr>
  </table>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:570px; overflow: auto; z-index: 1; left: 50px; top: 132px; height: 191px;"> 
    <table id="retornos" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (CodRetornoID.getAtualizarDependente().equals("S")) {				   
					   int ocor   = CodRetornoID.getRetorno().size() ;
					   if (ocor<=0) ocor=CodRetornoID.getRetorno(10,5).size() ;
					   for (int i=0; i<ocor; i++) {	   %>
      <tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
		    <input name="numRetorno"    type="text" size="5" maxlength="3"  value="<%= CodRetornoID.getRetorno(i).getNumRetorno() %>" onfocus="javascript:this.select();" onkeypress="javascript:f_num();">
        	<input name="codCodigoRetorno" type="hidden" value="<%= CodRetornoID.getRetorno(i).getCodCodigoRetorno() %>">				
		<td width=350 align="center"> 
          <input name="descRetorno" type="text" size="43" maxlength="43"  value="<%= CodRetornoID.getRetorno(i).getDescRetorno() %>" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"  >
        </td>			
    	<td align="center"> 
          <select name="indEntregue">
            <option value="true" <%if (CodRetornoID.getRetorno(i).getIndEntregue()) {%>selected<%}%>> Sim</option>		  
            <option value="false" <%if (!CodRetornoID.getRetorno(i).getIndEntregue()) {%>selected<%}%>>N&atilde;o</option>
          </select>       
		</td>
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
          <input name="numRetorno"  readonly  type="text" size="10" maxlength="10" value="">
        </td>
	   	<td width=350 align=center> 
          <input name="descRetorno" readonly  type="text" size="43" maxlength="50"  value="">
        </td>			
    	<td align=center>
    		<select name="indEntregue">
            	<option value="true"> Sim</option>		  
            	<option value="false" selected>N&atilde;o</option>
          </select>
		</td>
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= CodRetornoID.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>
