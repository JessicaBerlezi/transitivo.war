<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto da Infracao -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'ProcessaPenalidade':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
			}
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AbrirRecForm.datEntrada,1) && valid
	// validar se nada foi selecionado
	nadaSelec = true ;
	if ((AbrirRecForm.SolicDCBox != null) && (AbrirRecForm.SolicDCBox.checked==true)) nadaSelec = false ;
	if ((AbrirRecForm.SolicDPBox != null) && (AbrirRecForm.SolicDPBox.checked==true)) nadaSelec = false ;	
	if (nadaSelec) {
		sErro += "Nenhuma op��o selecionada. \n"
		valid = false ;
	}	
	if (valid==false) alert(sErro) 
	return valid ;
}
function apaga(defesa) {
	if (defesa=='DC') {
		if (AbrirRecForm.SolicDCBox != null) AbrirRecForm.SolicDCBox.checked=false ;
	}
	else {
		if (AbrirRecForm.SolicDPBox != null) AbrirRecForm.SolicDPBox.checked=false ;	
	}
	return ;
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AbrirRecForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>

<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>
<%@ include file="apresentaInfracao_Diretiva.jsp" %>


<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr><td height="2" colspan="4"></td></tr>
    <tr>
     	<% if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0) { %>
		<td bgcolor="#faeae5" align="right">
		<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ProcessaPenalidade',this.form);">			  
	  	<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19">			</button>
		<% } %>	  </td>	  	
    </tr>	
 
     <tr><td height="2" colspan="4"></td></tr>
  </table>
</div>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<script>document.AbrirRecForm.numAutoInfracao.focus();</script>	        

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->

<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 


<!--FIM_Div Erros-->
</form>
</body>
</html>