<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="REC.GuiaDistribuicaoBean"%>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="guiaArquivoAtaId" scope="request" class="REC.GuiaDistribuicaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
		case 'consultaArq':
			if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		break;	
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'AbreRecurso':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}

function download(nomArquivo,fForm) {	
	fForm.nomArquivo.value=nomArquivo;
   	fForm.target= "_self";
    fForm.action = "download";
    fForm.submit();
}

function registra(opcao,codArquivo,fForm) {	
	fForm.acao.value = opcao
	fForm.codArquivo.value=codArquivo;
   	fForm.target= "_self";
    fForm.action = "acessoTool";
    fForm.submit();
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownloadAta" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />

<input name="acao"     type="hidden" value=' '>
<input name="nomArquivo"  type="hidden" value=''>
<input name="codArquivo"  type="hidden" value=''>
<input name="contexto"  type="hidden" value="DIR_ARQUIVO_ATA">


<!--IN�CIO_CORPO_sistema-->
<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:780px; overflow: visible; z-index: 1; top: 80px; left: 115px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="36%"><b>&nbsp;Data de Envio:&nbsp;</b>&nbsp;<input name="De" type="text" id="De" size="12" maxlength="12" value="<%=(String)request.getAttribute("dataIni")%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  &nbsp;at&eacute;&nbsp;&nbsp;
			  <input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=(String)request.getAttribute("dataFim")%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  </td>		  
	          <td align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consultaArq',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:115px; top:328px; width:200px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;j&aacute; efetuado</td>
        <td width="10" bgcolor="#FFFFFF" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>
        &nbsp;n&atilde;o efetuado</td>
      </tr>
    </table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:115px; top:105px; width:560px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr bgcolor="#BE7272"> 
        <td width="270" height="15" style="color:#FFFFFF"><strong>&nbsp;&nbsp;&nbsp;N&uacute;mero da ATA</strong></td>
		<td width="15" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="90" align="center" style="color:#FFFFFF"><strong>Publicado em</strong></td>
        <td width="15" bgcolor="#FFFFFF">&nbsp;</td>
		<td style="color:#FFFFFF"><strong>&nbsp;&nbsp;&nbsp;Status</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:115px; top:124px; width:560px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
         
     <%
        if(((String)request.getAttribute("statusInteracao")).equals("2")){
      		List vetArquivos = (List) request.getAttribute("vetArquivos"); 
 			String caminho = (String)request.getAttribute("caminho");
    		String corArq;
      		for(int i=0;i<vetArquivos.size();i++) {
      			GuiaDistribuicaoBean arqAtaBean = null;
      			arqAtaBean=(GuiaDistribuicaoBean)vetArquivos.get(i);   		
      			if (arqAtaBean.getCodEvento().equals("0")) {
      				corArq="#FFFFFF";
      			}
      			else {
      				corArq="#FFFFD5";
      			}
      %>
      <tr bgcolor="<%=corArq%>"> 
        <td width="270" height="15">&nbsp;&nbsp;&nbsp;<a href="#" onActivate="javascript: registra('registra','<%=arqAtaBean.getNumGuia()%>',document.frmDownloadAta);" onClick="javascript: download('<%=arqAtaBean.getNomArquivoATA()%>',document.frmDownloadAta);"><strong><%=arqAtaBean.getCodEnvioDO()%></strong></a></td>
        <td width="15" bgcolor="#FFFFFF">&nbsp;</td>
		<td width="90" align="center"><a href="#" onClick="javascript: download('<%=arqAtaBean.getNomArquivoATA()%>',document.frmDownloadAta);" onActivate="javascript: registra('registra','<%=arqAtaBean.getNumGuia()%>',document.frmDownloadAta);"><strong><%=arqAtaBean.getDatPubliPDO()%></strong></a></td>
        <td width="15" bgcolor="#FFFFFF">&nbsp;</td>
		<td>&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: download('<%=arqAtaBean.getNomArquivoATA()%>',document.frmDownloadAta);" onActivate="javascript: registra('registra','<%=arqAtaBean.getNumGuia()%>',document.frmDownloadAta);"><strong><%=arqAtaBean.getNomStatus()%></strong></a></td>
      </tr>
      <tr> 
        <td height="1" colspan="5"  bgcolor="#cccccc"></td>
      </tr>
      
      <%	} 
      	}%>
    </table>
</div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/REC/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= guiaArquivoAtaId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>