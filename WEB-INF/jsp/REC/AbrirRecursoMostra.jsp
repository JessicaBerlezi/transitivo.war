<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>
<%@ page import="REC.AutoInfracaoBean" %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto de Historico -->
<jsp:useBean id="HistoricoBeanId"    scope="request" class="REC.HistoricoBean" /> 
<!-- Chama o Objeto dos Parametros do Orgao de atuacao -->
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
	    	close() ;
			break;
	  case 'recurso':
			fForm.acao.value=' ';
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	
		   	break;  
     case 'Imprimehistorico':
			fForm.acao.value=opcao;
		   	fForm.target= "_blank";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	
		   	break;  
     case 'imprimirInfRaj':
			fForm.acao.value=opcao;
		   	fForm.target= "_blank";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	
		   	break;  	   	
	}			
}

var theTable, theTableBody

function imprimeFrame() {
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();		
}
function escondeFrame() {
	document.all.etiquetaFrame.style.visibility="hidden";
}
function mostraFrame() {
	document.all.etiquetaFrame.style.visibility="visible";
}

function init() {
	theTable = (document.all) ? document.all.myTABLE : document.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function init2() {
	theTable = (document.all) ? window.parent.frame1.myTABLE : window.parent.frame1.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function appendRow(form) {
	insertTableRow(form, -1)
}
function appendRow2(form) {
	insertTableRow2(form, -1)
}

function addRow(form) {
	insertTableRow(form, form.insertIndex.value)
}

function removeRow(form) {
	theTableBody.deleteRow(0)
}

function insertTableRow(form, where) {
	var now = new Date()
	var nowData = [now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()]
	clearBGColors()
	var newCell
	var newRow = theTableBody.insertRow(where);
	for (var i = 0; i < nowData.length; i++) {
		newCell = newRow.insertCell(i)
		newCell.innerHTML = nowData[i]
		newCell.style.backgroundColor = "salmon"
	}
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body onLoad="init2()" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="controls" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />

<div id="etiqueta" style="position:absolute; left:200px; top:75px; width:400px; height:265px; z-index:5; overflow: auto; visibility: visible;"> 
<!--etiqueta-->
<%  String ExisteProcesso  = (String)request.getAttribute("ExisteProcesso");
    String atualiza        = ParamOrgBeanId.getParamOrgao("QUANTAS_ETIQUETAS","2","2");
    String tipRecurso="";
	String assuntoProcesso = "";
	String somaDias = "";
	String diasDef=ParamOrgBeanId.getParamOrgao("DIAS_DEF_PREVIA");
	String diasRec=ParamOrgBeanId.getParamOrgao("DIAS_DEF_PENALIDADE");
	String dataNotif= sys.Util.formatedToday().substring(0,10);	
	
	if (HistoricoBeanId.getCodEvento().equals("214")) {
	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_TRI","Troca de Real Infrator","7");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("RD")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_REN_PREVIA","Renuncia de Defesa Pr�via","1");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("DI")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_DP","Indeferimento de Plano - Def.Pr�via","1");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("1I")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_1A","Indeferimento de Plano - 1a Inst�ncia","3");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("2I")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_2A","Indeferimento de Plano - 2a Inst�ncia","4");
   	}
    else if (HistoricoBeanId.getTxtComplemento02().equals("DP") || HistoricoBeanId.getTxtComplemento02().equals("DC")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_DEF_PREVIA","Defesa Pr�via de Infra��o de Tr�nsito","1");
   	}
   	else if (HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_1INSTANCIA","Recurso de Multa","3");
   	}
   	else {
   	 	assuntoProcesso=ParamOrgBeanId.getParamOrgao("NOME_ASS_2INSTANCIA","Recurso de 2a. Inst�ncia","4");
   	}if (ExisteProcesso==null) ExisteProcesso="N" ;
    int cont=0;%>
    
    
    
    <input name="acao"     type="hidden" value=' '>
     <input type="hidden" name="Orgao" value='<%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA","DETRAN","2")%>'>	 	 
	 
	 
	 <input type="hidden" name="notifData" value="<%=sys.Util.formatedToday().substring(0,10)%>">
	  <input type="hidden" name="NumProcesso" value="<%=AutoInfracaoBeanId.getNumProcesso() %>">
	  <%
					System.out.print("NumProcesso: " + AutoInfracaoBeanId.getNumProcesso());
				%>
	    <input type="hidden" name="NumProcessoRequerimentoRaj" value="<%=HistoricoBeanId.getTxtComplemento01() %>">
	    
	    <%
					System.out.print("NumProcessoRequerimentoRaj: " + HistoricoBeanId.getTxtComplemento01());
				%>
	  
	<%if ("0,1,2".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){		
		tipRecurso="Defesa";
		somaDias = Util.addData(dataNotif,Integer.parseInt(diasDef));
	 }%>
	<%if ("10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0)	{    
		tipRecurso="Recurso";
	    somaDias = Util.addData(dataNotif,Integer.parseInt(diasRec));	
	}%>	
	 <input type="hidden" name="prazoRec" value='<%=somaDias%>'>   	
	 <input type="hidden" name="tipRecurso" value="<%=tipRecurso%>">	 
	 <input type="hidden" name="DatProcesso" value="<%=AutoInfracaoBeanId.getDatProcesso()%>">	 
	 <input type=hidden name=AssuntoProcesso value="<%= assuntoProcesso %>">
	  <input type=hidden name=numProcesso value="<%=AutoInfracaoBeanId.getNumProcesso()%>">
	 
	 <input type=hidden name="numAutoInfracao" value="<%= AutoInfracaoBeanId.getNumAutoInfracao() %>">
	 <input type=hidden name="numPlaca" value="<%= AutoInfracaoBeanId.getNumPlaca() %>">
    <%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>				
				<input type="hidden" name="TxtComplemento03" value="<%=HistoricoBeanId.getTxtComplemento03()%>">
				<%
					System.out.print("HistoricoBeanId.getTxtComplemento03(): " + HistoricoBeanId.getTxtComplemento03());
				%>
			<% } else { %>
				
				<input type="hidden" name="TxtComplemento03" value="<%=HistoricoBeanId.getTxtComplemento04()%>">
				<%
					System.out.print("HistoricoBeanId.getTxtComplemento04(): " + HistoricoBeanId.getTxtComplemento04());
				%>			
			<% } %>     
	<%if (ExisteProcesso.equals("S")) { 
	if(ParamOrgBeanId.getParamOrgao("QUANTAS_ETIQUETAS","2","2").equals("2"))
	{  %>

     

<!--IN�CIO_CORPO_sistema-->	 
	 
<!--Form de Processo-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="titprincipal">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td ><strong><%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA","DETRAN","2") %></strong></td>
			
		</tr>
		</table>
	</td>
</tr>
<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Notificado:&nbsp;</strong></td>

			<td><%=sys.Util.formatedToday().substring(0,10)%></td>
			
			<td align="right"><strong>Prazo at�:&nbsp;</strong></td>
			<td align="right" width="40"><%=somaDias%></td>			
		</tr>
		</table>
	</td>
</tr>
<%}else{%>
<tr>	
<td width="100%">&nbsp;</td>
</tr>
<%}%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>

			<td ><%=AutoInfracaoBeanId.getNumProcesso()%> </td>
			<%
					System.out.print("AutoInfracaoBeanId.getNumProcesso(): " + AutoInfracaoBeanId.getNumProcesso());
				%>
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=AutoInfracaoBeanId.getDatProcesso()%></td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td >
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				<%=HistoricoBeanId.getTxtComplemento03()%>
				
			<% } else { %>
				<%=HistoricoBeanId.getTxtComplemento04()%>
				
			<% } %>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40" valign="top"><strong>Assunto:&nbsp;</strong></td>
			<td  valign="top">			
			<%= assuntoProcesso %>			
			
			</td>
		</tr>
		</table>
	</td>
	
</tr>
</table>








<DIV class="quebrapagina"></DIV>
<!--FIM Form de Processo-->       
<br>
		<%}%>
<!--Segunda Etiqueta para o processo-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="titprincipal">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td><strong><%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA","DETRAN","2")%></strong></td>
		</tr>
		</table>
	</td>
</tr>
<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Notificado:&nbsp;</strong></td>

			<td><%=sys.Util.formatedToday().substring(0,10)%></td>
			
			<td align="right"><strong>Prazo at�:&nbsp;</strong></td>
			<td align="right" width="40"><%=somaDias%></td>			
		</tr>
		</table>
	</td>
</tr>
<%}else{%>
<tr>	
<td width=100%>&nbsp;</td>
</tr>
<%}%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td ><%=AutoInfracaoBeanId.getNumProcesso()%></td>
			
			<%
					System.out.print("AutoInfracaoBeanId.getNumProcesso(): " + AutoInfracaoBeanId.getNumProcesso());
				%>
		
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=AutoInfracaoBeanId.getDatProcesso()%></td>
		
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td >
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				<%=HistoricoBeanId.getTxtComplemento03()%>
			<% } else { %>
				<%=HistoricoBeanId.getTxtComplemento04()%>
			<% } %>
			</td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40" valign="top"><strong>Assunto:&nbsp;</strong></td>
			<td  valign="top">			
				<%= assuntoProcesso %>
			</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<DIV class="quebrapagina"></DIV>
<!--FIM Form de Processo-->

<% } //ExisteProcesso 

	String assunto = ""; //Usada para armazenar os assuntos de cada requerimento
%>


<% if (HistoricoBeanId.getTxtComplemento01().equals("")==false) { 
	%>
<br>	
<!--Form do REQ 1-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="titprincipal">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td><strong><%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA","DETRAN","2")%></strong>
			<input type="hidden" name="Orgao<%=cont%>" value='<%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA","DETRAN","2")%>'></td>
		</tr>
		</table>
	</td>
</tr>
<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Notificado:&nbsp;</strong></td>

			<td><%=sys.Util.formatedToday().substring(0,10)%></td>
			
			<td align="right"><strong>Prazo at�:&nbsp;</strong></td>
			<td align="right" width="40"><%=somaDias%></td>			
		</tr>
		</table>
	</td>
</tr>
<%}else{%>
<tr>	
<td width=100%>&nbsp;</td>
</tr>
<%}%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td ><%=AutoInfracaoBeanId.getNumProcesso() %></td>
			<input type="hidden" name="NumProcesso<%=cont%>" value="<%=AutoInfracaoBeanId.getNumProcesso() %>">
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=AutoInfracaoBeanId.getDatProcesso()%></td>
			<input type="hidden" name="DatProcesso<%=cont%>" value="<%=AutoInfracaoBeanId.getDatProcesso()%>">
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td>
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				<%=HistoricoBeanId.getTxtComplemento03()%>
				<input type="hidden" name="Nome<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento03()%>">
			<% } else { %>
				<%=HistoricoBeanId.getTxtComplemento04()%>
				<input type="hidden" name="Nome<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento04()%>">			
			<% } %>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento11()%></td>
					<input type="hidden" name="Req<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento11()%>">
					<%
					System.out.print("HistoricoBeanId.getTxtComplemento11(): " + HistoricoBeanId.getTxtComplemento11());
				%>
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento01()%></td>
					<input type="hidden" name="Data<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento01()%>">
					<%
					System.out.print("HistoricoBeanId.getTxtComplemento01(): " + HistoricoBeanId.getTxtComplemento01());
				%>
			
			<% } else { %>		
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento01()%></td>
					<input type="hidden" name="Req<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento01()%>">
					<%
					System.out.print("HistoricoBeanId.getTxtComplemento01(): " + HistoricoBeanId.getTxtComplemento01());
				%>
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento03()%></td>
					<input type="hidden" name="Data<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento03()%>">
					<%
					System.out.print("HistoricoBeanId.getTxtComplemento03(): " + HistoricoBeanId.getTxtComplemento03());
				%>
			<% } %>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Assunto:&nbsp;</strong></td>
			<td >
			<%	assunto = "";
				if (HistoricoBeanId.getCodEvento().equals("214")) {
				 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_TRI","Troca de Real Infrator","7");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("RD")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_REN_PREVIA","Renuncia de Defesa Pr�via","1");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("DI")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_DP","Indeferimento de Plano - Def.Pr�via","1");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("1I")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_1A","Indeferimento de Plano - 1a Inst�ncia","3");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("2I")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_2A","Indeferimento de Plano - 2a Inst�ncia","4");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("DP") || HistoricoBeanId.getTxtComplemento02().equals("DC")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_DEF_PREVIA","Defesa Pr�via de Infra��o de Tr�nsito","1");
			   	}
			   	else if (HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_1INSTANCIA","Recurso de Multa","3");
			   	}
			   	else if (HistoricoBeanId.getTxtComplemento02().equals("2P") || HistoricoBeanId.getTxtComplemento02().equals("2C")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_2INSTANCIA","Recurso de 2a. Inst�ncia","4");
			   }
			%>
			<%= assunto %>
			</td>
			<input type="hidden" name="Assunto<%=cont%>" value="<%=assunto%>">
		</tr>
		</table>
	</td>
</tr>

</table>
<!--FIM Form de REQ 1-->

<% 
	cont++;
	} //if REQ 1 
%>

<% if (HistoricoBeanId.getTxtComplemento01().equals("")==false) { 
	%>

<br>
<!--Form do REQ 1 SEGUNDA ETIQUETA-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="titprincipal">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td><strong><%=ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA")%></strong></td>
		</tr>
		</table>
	</td>
</tr>
<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Notificado:&nbsp;</strong></td>

			<td><%=sys.Util.formatedToday().substring(0,10)%></td>
			
			<td align="right"><strong>Prazo at�:&nbsp;</strong></td>
			<td align="right" width="40"><%=somaDias%></td>			
		</tr>
		</table>
	</td>
</tr>
<%}else{%>
<tr>	
<td width=100%>&nbsp;</td>
</tr>
<%}%>


<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td><%=AutoInfracaoBeanId.getNumProcesso() %></td>
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=AutoInfracaoBeanId.getDatProcesso()%></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td >
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>			
				<%=HistoricoBeanId.getTxtComplemento03()%>
			<% } else { %>
				<%=HistoricoBeanId.getTxtComplemento04()%>
			<% } %>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<%	if (HistoricoBeanId.getCodEvento().equals("214")) { %>
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento11()%></td>
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento01()%></td>			
			<% } else { %>		
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento01()%></td>
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento03()%></td>
			<% } %>
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
		<tr>
			<td  width="40"><strong>Assunto:&nbsp;</strong></td>
			<td>
			<%	assunto = "";
				if (HistoricoBeanId.getCodEvento().equals("214")) {
				 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_TRI","Troca de Real Infrator","7");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("RD")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_REN_PREVIA","Renuncia de Defesa Pr�via","1");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("DI")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_DP","Indeferimento de Plano - Def.Pr�via","1");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("1I")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_1A","Indeferimento de Plano - 1a Inst�ncia","3");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("2I")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_INDEF_2A","Indeferimento de Plano - 2a Inst�ncia","4");
			   	}
			    else if (HistoricoBeanId.getTxtComplemento02().equals("DP") || HistoricoBeanId.getTxtComplemento02().equals("DC")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_DEF_PREVIA","Defesa Pr�via de Infra��o de Tr�nsito","1");
			   	}
			   	else if (HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_1INSTANCIA","Recurso de Multa","3");
			   	}
			   	else if (HistoricoBeanId.getTxtComplemento02().equals("2P") || HistoricoBeanId.getTxtComplemento02().equals("2C")) {
			   	 	assunto=ParamOrgBeanId.getParamOrgao("NOME_ASS_2INSTANCIA","Recurso de 2a. Inst�ncia","4");
			   }			   
			%>
			<%= assunto %>
			</td>
		</tr>
		</table>
	</td>
</tr>

</table>

<!--FIM Form de REQ 1 - SEGUNDA ETIQUETA-->

<% 
	
	} //if REQ 1 
%>

<!--fim etiqueta-->
</div> 
<div id="botaoimprimir" style="position:absolute; right: 165px; bottom: 70px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible; left: 424px; top: 336px;">
<table>
<tr>
	<td>
<!-- 		<button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" type="button" onClick="appendRow2(this.form)">  -->
		<button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" type="button" onClick="javascript:valida('imprimirInfRaj',this.form);">  
			<img src="<%=path%>/images/bot_imprimir_det1.gif" width="70" height="26" alt="" border="0">
		</button>
		<input type="hidden" name="printRaj" value="imprimirInfRaj">
	</td>
	<td>
		<button style="border: 0px; background-color: transparent; height: 28px; width: 82px; cursor: hand;" type="button" onClick="javascript:valida('recurso',this.form);"> 
			<img src="<%= path %>/images/bot_prosseguir_det1.gif" width="80" height="26" alt="" border="0">
		</button>	
	</td>
	<% String codEventoFiltro = (String)request.getAttribute("codEventoFiltro"); 
	   if (codEventoFiltro == null) codEventoFiltro = "";
	   if (!codEventoFiltro.equals("N")&& (HistoricoBeanId.getTxtComplemento02().equals("1P") || HistoricoBeanId.getTxtComplemento02().equals("1C")) ){%>
	<td>
		<button style="border: 0px; background-color: transparent; height: 20px; width: 76px; cursor: hand;" type="button" onClick="javascript:valida('Imprimehistorico',this.form);"> 
			<img src="<%= path %>/images/bot_historico_ilt.gif" alt="" width="74" height="19" border="0">
		</button>	
	</td>
	<% } %>
</tr>
</table>
</div>

<div id="etiquetaFrame" style="position:absolute; left:600px; top:50px; width:500px; height:350px; z-index:5; overflow: auto; visibility: visible;"> 
<iframe name="frame1" id="frame1" src="<%= path %>/js/etiquetaFrame.html" frameborder="0" width="365" heigth="350" MARGINWIDTH="0" MARGINHEIGHT"0" SCROLLING="NO">
</iframe>
</div>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
<script>
function insertTableRow2(form, where) {
	console.log(theTableBody);
	while(theTableBody.rows.length>0) {
		removeRow(form); 
	}
	var newCell
	var newRow
	var strPadrao1="<td><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=85 class=texto><strong>"		
	
<% if (ExisteProcesso.equals("S")) { 

if(ParamOrgBeanId.getParamOrgao("QUANTAS_ETIQUETAS","2","2").equals("2"))
	{  %>
	//Primeira etiqueta Processo
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao.value + "</td></tr></table></td>"

<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Notificado em</strong></td><td align=left class=texto><font style=font-size:10px>" + form.notifData.value + "</font></td><td align=right class=texto><strong>&nbsp;Prazo de&nbsp;"+ form.tipRecurso.value +":&nbsp;</strong></td><td align=right width=40 class=texto>" + form.prazoRec.value + "</td></tr></table></td>"
<%}else{%>	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
<%}%>
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.TxtComplemento03.value + "</font></strong></td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.AssuntoProcesso.value + "</td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=46 border=0></td>"
   <%}%>
	//Segunda etiqueta Processo
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao.value + "</td></tr></table></td>"

<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Notificado em</strong></td><td align=left class=texto><font style=font-size:10px>" + form.notifData.value + "</font></td><td align=right class=texto><strong>&nbsp;Prazo de&nbsp;"+ form.tipRecurso.value +":&nbsp;</strong></td><td align=right width=40 class=texto>" + form.prazoRec.value + "</td></tr></table></td>"
<%}else{%>	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
<%}%>
		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.TxtComplemento03.value + "</font></strong></td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.AssuntoProcesso.value + "</td></tr></table></td>"
	
	<% if (cont>0) { %>
		newRow = theTableBody.insertRow(where)		
		newCell = newRow.insertCell(0)	
		newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=30 border=0></td>"
	<% } %>

<% }
	 
	for(int i=0; i<cont; i++) {  %>
	

	//primeira etiqueta requerimento
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao<%=i%>.value + "</td></tr></table></td>"
	
<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Notificado em</strong></td><td align=left class=texto><font style=font-size:10px>" + form.notifData.value + "</font></td><td align=right class=texto><strong>&nbsp;Prazo de&nbsp;"+ form.tipRecurso.value +":&nbsp;</strong></td><td align=right width=40 class=texto>" + form.prazoRec.value + "</td></tr></table></td>"
<%}else{%>	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
<%}%>

		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso<%=i%>.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.Nome<%=i%>.value + "</font></strong></td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Req:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto>" + form.Req<%=i%>.value + "</td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.Data<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.Assunto<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=35 border=0></td>"
	
	//segunda etiqueta requerimento
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao<%=i%>.value + "</td></tr></table></td>"

<%if ("0,1,2,10,11,12".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Notificado em</strong></td><td align=left class=texto><font style=font-size:10px>" + form.notifData.value + "</font></td><td align=right class=texto><strong>&nbsp;Prazo de&nbsp;"+ form.tipRecurso.value +":&nbsp;</strong></td><td align=right width=40 class=texto>" + form.prazoRec.value + "</td></tr></table></td>"
<%}else{%>	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
<%}%>
		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso<%=i%>.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.Nome<%=i%>.value + "</font></strong></td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Req:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto>" + form.Req<%=i%>.value + "</td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.Data<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.Assunto<%=i%>.value + "</td></tr></table></td>"

	<% if (i+1<cont) { %>
		newRow = theTableBody.insertRow(where)		
		newCell = newRow.insertCell(0)	
		newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=25 border=0></td>"
	<% } 
	}
%>


	//Impressao
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();
}
</script>
</html>