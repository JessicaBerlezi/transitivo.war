<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{    
		case 'AtualizaRecebimento':
			if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value = opcao;
				   	fForm.target     = "_blank";
				    fForm.action     = "acessoTool";  
				   	fForm.submit();	 
		  		}
		  	}
		  break;
	   case 'R':
			if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;
				
				fForm.acao.value =opcao;
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();	   	   
		    	close() ;
	    	}
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datRS,1) && valid
	verString(fForm.datRS,"Data do Recebimento",0);
	if (valid==false) alert(sErro) ;
	return valid ;
}

function Classificacao(ordem,fForm)  {
	if(bProcessando)
	{
   		alert("Opera��o em Processamento, por favor aguarde...");
		return;
	}
	else
	{
		bProcessando=true;	
		
		document.all["acao"].value  = "Classifica";
		document.all["ordem"].value = ordem;
		document.all["InformaResultForm"].target = "_self";
		document.all["InformaResultForm"].action = "acessoTool";  
		document.all["InformaResultForm"].submit() ;	 
	}
}

function Mostra(opcoes,fForm,numplaca,numauto) {	
	document.all["acao"].value          = opcoes;
	document.all["mostraplaca"].value   = numplaca;
	document.all["mostranumauto"].value = numauto;
   	document.all["InformaResultForm"].target = "_blank";
    document.all["InformaResultForm"].action = "acessoTool";  
   	document.all["InformaResultForm"].submit() ;	 
}

function SomaSelec() {
  
	n = 0 ;
	
	for (i=0;i<document.all["InformaResultForm"].Selecionado.length;i++) {
		if (document.all["InformaResultForm"].Selecionado[i].checked)
		 n++;
	
	}
	if (document.all["Selecionado"].value=='0'){
		if (document.all["InformaResultForm"].Selecionado.checked) n++;
	}
	document.all["numSelec"].value = n;	
	
}

function marcaTodos(obj) {
	n=0
	for (i=0; i<document.all["InformaResultForm"].Selecionado.length;i++) {
		document.all["InformaResultForm"].Selecionado[i].checked = obj.checked ;
		if (document.all["InformaResultForm"].Selecionado[i].checked) n++;
		
	}
	
	if (document.all["Selecionado"].value=='0'){				
			document.all["InformaResultForm"].Selecionado.checked = obj.checked ;
			if (document.all["InformaResultForm"].Selecionado.checked) n++;
	
	}
	
	document.all["numSelec"].value = n;
	
}


</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResultForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"           type="hidden" value=' '>
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#faeae5">
        <td width="56%" height=23><b>GUIA DE REMESSA &nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=RemessaId.getCodRemessa()%>
			&nbsp;&nbsp;-&nbsp;&nbsp;<%=RemessaId.getDatProc()%>
        </td>
        <td width="30%">Data do Recebimento:&nbsp;
		  <input type="text" name="datRS" size="12" maxlength="10" value='<%=RemessaId.getDatProcRecebimento() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
        <td width="14%" align="center" valign="middle"> 
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AtualizaRecebimento',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
		</td>
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
     <tr bgcolor="#faeae5">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" >
	     	<tr>
		        <td width="50%" height=20></td>
		        <td width="50%" align=right></td>        
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=3 height=5></td></tr>     
  </table>
  <jsp:include page="RemessaTop.jsp" flush="true" />  
</div>
<jsp:include page="RemessaPreparaCorpo.jsp" flush="true" >  
  <jsp:param name="posTop" value= "187 px" />
  <jsp:param name="posHei" value= "166 px" />
</jsp:include> 

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= RemessaId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= RemessaId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>


