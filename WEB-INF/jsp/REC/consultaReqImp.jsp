<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="ParamOrgBeanId" scope ="session" class = "REC.ParamOrgBean"/>
<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="CssImpressao.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">

<form name="ConsultaHistImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
    boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	int a =AutoInfracaoBeanId.getRequerimentos().size()+1;
		Iterator itx  = AutoInfracaoBeanId.getRequerimentos().iterator() ; ;
		REC.RequerimentoBean req = new REC.RequerimentoBean();
		ACSS.OrgaoBean acssOrg = new ACSS.OrgaoBean();	
	if 	(itx.hasNext()){
			while (itx.hasNext()) {
				a--;
				req =(REC.RequerimentoBean)itx.next() ;
		        acssOrg.Le_Orgao(req.getCodOrgaoLotacaoDP(),0);	
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if (contLinha>9){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {			
		%>
				        </table>      
						<jsp:include page="rodape_impconsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
<%if(ParamOrgBeanId.getParamOrgao("LAYOUT_REQUERIMENTO","N","2").indexOf("S")>=0){%>
	<jsp:include page="cabecalho_reqconsulta_Nit.jsp" flush="true" >
    <jsp:param name="nPag" value= "<%= npag %>" />				
    </jsp:include> 
<%}else{%>
    <jsp:include page="cabecalho_reqconsulta.jsp" flush="true" >	
    <jsp:param name="nPag" value= "<%= npag %>" />				
    </jsp:include> 
<%}%>	

		       <% } %>	
   <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
      <tr> <td height="7" colspan=3></td></tr>	  	 
      <tr bgcolor="#C0BDBD"> 
        <td width="324" class="espaco2" ><strong>&nbsp;&nbsp;N&ordm;&nbsp;REQUERIMENTO :</strong>&nbsp;<%=req.getNumRequerimento()%></td>
        <td width="222" class="espaco2" >&nbsp;<strong>&nbsp;<%=req.getNomTipoSolic()%></strong></td>
        <td width="168" align="left" class="espaco2"><strong>Data :</strong>&nbsp;<%=req.getDatRequerimento()%>&nbsp;&nbsp;</td>
      </tr>
      <tr> <td height="5" colspan=3></td></tr>	  
  </table>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr> 
        <td ><strong>&nbsp;&nbsp;</strong><strong>Respons&aacute;vel :</strong></td>
        <td colspan=3><%=  (req.getCodTipoSolic().equals("TR")) ? req.getNomCondutorTR() : req.getNomResponsavelDP() %></td>
        <td width="169" align="right"><strong><%=req.getNomStatusRequerimento()%></strong>&nbsp;&nbsp;</td>      
      </tr>
      <tr><td height="2"></td></tr>
      <tr> 
      	<td width="116" ><strong>&nbsp;&nbsp;Aberto por :</strong>&nbsp;</td>
        <td width="130" ><%=req.getNomUserNameDP() %></td>
        <td width="116" ><strong>Org�o Lota��o :</strong></td>
      	<td width="189" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcDP()%>&nbsp;&nbsp;</td>
      </tr>  
      <% if(req.getNumGuiaDistr().length()>0) { %>
	      <tr><td height="2"></td></tr>
    	  <tr> 
      		<td><strong>&nbsp;&nbsp;Guia Distrib.:</strong>&nbsp;</td>
	        <td><%=req.getNumGuiaDistr() %></td>
    	    <td></td>
      		<td></td>
	      	<td></td>
    	  </tr>  
      <% } %> 
      <% if ((req.getCodRemessa().length()>0)) { %>
	      <tr><td height="2"></td></tr>
    	  <tr> 
      		<td><strong>&nbsp;&nbsp;Guia Remessa:</strong>&nbsp;</td>
	        <td><%=req.getCodRemessa() %></td>
    	    <td width="116"><strong>Data Envio:</strong></td>
      		<td width="189"><%=req.getDatEnvioGuia()%></td>
	      	<td><strong>Enviado por:</strong>&nbsp;&nbsp;<%=req.getNomUserNameEnvio()%></td>
    	  </tr>  
	      <tr><td height="2"></td></tr>
    	  <tr> 
      		<td><strong>&nbsp;&nbsp;Data Recebimento:</strong>&nbsp;</td>
	        <td><%=req.getDatRecebimentoGuia()%></td>
    	    <td width="116"><strong>Recebido por:</strong></td>
      		<td width="189"><%=req.getNomUserNameRecebimento()%></td>
	      	<td></td>
    	  </tr>  
      <% } %> 
  </table>
  <% if ( (req.getCodTipoSolic().equals("TR")) && (req.getNumCNHTR().length()>0) ) {
	  	if (req.getCodOrgaoLotacaoTR().equals("")==false)
			acssOrg.Le_Orgao(req.getCodOrgaoLotacaoTR(),0);
		else acssOrg.setNomOrgao("");
  %>
<!--Requerimento Troca Real Infrator-->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr><td height="5"></td></tr>	
    <tr> 
      <td width="116" valign="top"><strong>&nbsp;&nbsp;Nome :</strong></td>
      <td width="245" valign="top"><%=req.getNomCondutorTR()%></td>
      <td width="76" valign="top"><strong>Endere&ccedil;o :</strong></td>
      <td width="283" valign="top"><%=req.getTxtEnderecoTR()%></td>
    </tr>
    <tr><td height="2"></td></tr>	
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="116" valign="top">&nbsp;&nbsp;<strong>Bairro :</strong></td>
      <td width="130" valign="top"><%=req.getNomBairroTR()%></td>
      <td width="113" valign="top"><strong>Munic&iacute;pio :</strong></td>
      <td width="134" valign="top"><%=req.getNomCidadeTR()%></td>
      <td width="64" valign="top"><strong>UF :</strong></td>
      <td width="31" valign="top"><%=req.getCodUfTR()%></td>
      <td width="14" valign="top"><strong>&nbsp;</strong></td>
      <td width="19" valign="top">&nbsp;</td>
      <td width="34" valign="top"><strong>CEP :</strong></td>
      <td width="65" valign="top"><%=req.getNumCEPTREdt()%></td>
    </tr>
    <tr><td height="2"></td></tr>		
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="116" valign="top" >&nbsp;&nbsp;<strong>CPF :</strong></td>
      <td width="130" valign="top" ><%=req.getNumCPFTREdt()%></td>
      <td width="149" valign="top" ><strong>N� CNH / Perm. p/ Dirigir :</strong></td>
      <td width="99" valign="top" ><%=req.getIndTipoCNHTR() %>&nbsp;-&nbsp;<%=req.getNumCNHTR()%></td>
      <td width="67" valign="top" ><strong>UF CNH</strong><strong> :</strong></td>
      <td width="159" valign="top" ><%=req.getCodUfCNHTR()%></td>
    </tr>
  </table>
  <% } //fim bloco TR 
     if ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodParecerPJ().equals("")==false))  {
	   	if (req.getCodOrgaoLotacaoPJ().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoPJ(),0);
		else	acssOrg.setNomOrgao("");
  %>
	<!--Requerimento Parecer Jur�co-->
  	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5"></td></tr>	
      <tr> 
        <td  colspan=2>&nbsp;&nbsp;<strong>PARECER JUR�DICO:</strong>&nbsp;<%= req.getNomResultPJ() %></td>
        <td  colspan=2><strong>Respons�vel pelo Parecer:&nbsp;</strong><%=req.getNomRespPJ() %></td>
        <td width="172"><strong>Data Parecer:</strong>&nbsp;<%=req.getDatPJ() %>&nbsp;&nbsp;</td>
      </tr>
      <tr> <td height="2"></td> </tr>
    <tr> 
  	  <td width="118" valign="top">&nbsp;&nbsp;<strong>Motivo :</strong></td>
      <td  colspan=4>
    	<%=req.getTxtMotivoPJ() %>
      </td>
    </tr>
    <tr> <td height="2"></td></tr>
      <tr> 
      	<td width="118" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="128" ><%=req.getNomUserNamePJ() %></td>
        <td width="115" ><strong>Org�o Lota��o :</strong></td>
      	<td width="187" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcPJ()%>&nbsp;&nbsp;</td>
      </tr>
  </table>        
  <!--FIM PARECER JURIDICO--> 
  <% } //IF DO PARECER JURIDICO
    if ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodJuntaJU().equals("")==false))  {
	     if (req.getCodOrgaoLotacaoJU().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoJU(),0);
		 else	acssOrg.setNomOrgao("");
  %>  
	<!--Requerimento JUNTA-->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5"></td> </tr>
      <tr> 
        <td colspan=2>&nbsp;&nbsp;<strong>JUNTA:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><%=req.getSigJuntaJU() %></td>
        <td colspan=2 ><strong>Relator:&nbsp;</strong><%=req.getNomRelatorJU() %></td>			
        <td width="170"><strong>Data:</strong>&nbsp;<%=req.getDatJU() %>&nbsp;&nbsp;</td>
      </tr>
	  <tr><td height="2"></td></tr>
      <tr> 
      	<td width="117" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="129" ><%=req.getNomUserNameJU() %></td>
        <td width="115" ><strong>Org�o Lota��o :</strong></td>
      	<td width="189" ><%=acssOrg.getSigOrgao()%></td>
      	<td><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcJU()%>&nbsp;&nbsp;</td>
      </tr>    
   </table> 
  <% } //IF JUNTA
   if  ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodResultRS().equals("")==false))  {
       	if (req.getCodOrgaoLotacaoRS().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoRS(),0);
		else acssOrg.setNomOrgao("");
  %>
	<!--Requerimento RESULTADO-->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5"></td> </tr>
      <tr> 
        <td colspan=3>&nbsp;&nbsp;<strong>RESULTADO:</strong>&nbsp;<%= req.getNomResultRS() %></td>
        <td></td>
        <td width="170"><strong>Data Resultado :</strong>&nbsp;<%=req.getDatRS() %></td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr> 
	     <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
	     <td colspan=3>
		     <%=req.getTxtMotivoRS() %>
	     </td>
      </tr>
     <%if (!req.getCodMotivoDef().equals("")){%>
      <tr><td height="2" colspan=5></td></tr>
	  <tr>    
         <td colspan=5>&nbsp;&nbsp;<strong>Motivo do Deferimento:</strong>&nbsp;<%=req.getNomMotivoDef()%></td>
      </tr>
      <%}%>
      <tr> <td height="2"></td> </tr>	  
      <tr> 
      	<td width="118" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="129" ><%=req.getNomUserNameRS() %></td>
        <td width="114" ><strong>Org�o Lota��o :</strong></td>
      	<td width="189" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcRS()%>&nbsp;&nbsp;</td>
      </tr>    
   </table>
<%			} //IF RESULTADO
		} //Loop do while 
	}	else	{
%>
	<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr><td height="45"></td></tr>
	  <tr><td bgcolor="#f5d6cc" align="center">&nbsp;&nbsp;<strong>AUTO SEM REQUERIMENTO</strong></td> </tr>	
	</table>
<%	}	%>
</div>
<jsp:include page="rodape_impconsulta.jsp" flush="true" />        

</form>
</body>
</html>