<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="StatusId" scope="request" class="TAB.StatusBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'A':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   		
				if (veCampos(fForm)==true) 
				{
					fForm.atualizarDependente.value="S"	  
					fForm.acao.value=opcao;
			   		fForm.target= "_self";
			    	fForm.action = "acessoTool";  
			   		fForm.submit();	  		  
				}
				else 
					alert(sErro)
			}
			break ;

		case 'R':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
					   		
				if (fForm.atualizarDependente.value=="S") 
				{
					fForm.atualizarDependente.value="N"	  
					fForm.acao.value=opcao
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
				else 
				{
					close() ;   
				}
			}
			break;
		
		case 'I':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  		  
			fForm.submit();		   
			break;
		
		case 'O':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
			else                 
	      		document.all["MsgErro"].style.visibility='hidden' ; 
			break;  
		
		case 'V':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='show' ; 
			else                 
				document.all["MsgErro"].style.visibility='visible' ; 
			break;	  
	}
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;


<!--valida sigla se for vazia-->
for (k=0;k<fForm.nomStatus.length;k++) {
   var dscStatus = trim(fForm.nomStatus[k].value);
   var codStatus = trim(fForm.codStatus[k].value);
   if(dscStatus=="" && codStatus!=""){
   	 valid = false
   	 sErro = sErro + "Descri��o deve ser preenchida. \nVerifique a seq "+(k+1)+" \n";
   }
  if(codStatus==""&& dscStatus!=""){
    valid = false
    sErro = sErro + "C�digo deve ser preenchido. \nVerifique a seq "+(k+1)+" \n";
  }

}

	
for (k=0;k<fForm.nomStatus.length;k++) {
   var ne = trim(fForm.nomStatus[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.nomStatus.length;m++) {
	   var dup = trim(fForm.nomStatus[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Descri��o em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 87px; left:204px;" > 
  
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
	 	   <td width="33%" align="left"> 
	 	   <input name="atualizarDependente" type="hidden" size="1" value="<%= StatusId.getAtualizarDependente() %>"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
     	   </td>
	       <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 132px; height: 191px"> 
    <table border="0" cellpadding="0" cellspacing="1" width="720"  align="center">  	   		
	<tr bgcolor="#993300"> 
        <td width=138 align="center" bgcolor="#be7272">
		<a href="#" onClick="javascript: Classifica(fstatus,codStatus,0,'input_int','#faeae5','#faeae5');"><font color="#ffffff"><b>C�digo</b></font></TD>		 	  
        <td width=579 align="center" bgcolor="#be7272">
		<a href="#" onClick="javascript: Classifica(fstatus,nomStatus,1,'input_text','#faeae5','#faeae5');"><font color="#ffffff"><b>Descri��o</b></font></TD>		 
 	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:720px; overflow: auto; z-index: 2; left: 50px; top: 147px; height: 191px;"> 
    <table id="fstatus" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
		   
  	            	   int ocor   = StatusId.getStatus().size() ;
					   if (ocor<=0) ocor=StatusId.getStatus(20,5).size() ;
					   for (int i=0; i<ocor; i++) {	   %>
      <tr bgcolor="#FAEAE5">
		<td width=200 align="center"> 
          <input name="codStatus"          type="text"  size="20"  maxlength="10"value="<%= StatusId.getStatus(i).getCodStatus() %>"onkeypress="javascript:f_num();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">

</td>	
        <td width=691 align="center"> 
             <input name="nomStatus" type="text" size="100" maxlength="40"  value="<%= StatusId.getStatus(i).getNomStatus().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
	</tr>
<% 						}

 %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= StatusId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</BODY>
</HTML>


