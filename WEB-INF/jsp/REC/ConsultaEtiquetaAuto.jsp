<!-- Abre a Sessao -->  
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 

<jsp:useBean id="bean" scope="request" class="REC.EtiquetaAutoBean" /> 
        
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   
   switch (opcao) {    

   case 'buscaAutoEtiq':
   		if(veCampos(fForm) == true){	
 		    fForm.acao.value=opcao 
       		fForm.target= "_self";
	  		fForm.action = "acessoTool";  
	  		fForm.submit();	 
   		}
	  	break;
	  
   case 'retorna':
        fForm.verTable.value="N"	  
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
        
  case 'chamaAutoEtiq':
      fForm.acao.value=opcao 
   	  fForm.target= "_self";
  	  fForm.action = "acessoTool";  
  	  fForm.submit();	
	break;
  
  case 'R':
	 close() ;   
	 break;
	  		
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;        	
		
	if ((fForm.numCaixa.value == "")&&(fForm.numLote.value == "")&&(fForm.numAuto.value == "")) {
	  valid = false
	  sErro = "Insira dados para consulta. \n"
	}

	if (valid==false) alert(sErro) 
	
	return valid;
}

function chamaAuto (numCaixa,numLote,numAuto) {
var fForm=document.form;

	fForm.numCaixaSel.value = numCaixa;
	fForm.numLoteSel.value = numLote;
	fForm.numAutoSel.value = numAuto;
	valida('chamaAutoEtiq',fForm);
}

function changeColorOver(obj) {	
	obj.style.background="#F5D6CC";
	obj.style.textDecoration="none";
}

function changeColorOut(obj) {
	obj.style.background="#FAEAE5";
	obj.style.textDecoration="none";
}

function acaoDigitaLote(obj,fForm) {	
<%if(UsuarioBeanId.getCodOrgaoAtuacao().equals("258650")){ //Niteroi%>
	Mascaras(obj,'99999');
<%}else{%>
	Mascaras(obj,'999-99-9999');
<%}%>
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="form" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="verTable" type="hidden" value=''>
<input name="numCaixaSel" type="hidden" value=''>
<input name="numLoteSel" type="hidden" value=''>
<input name="numAutoSel" type="hidden" value=''>

<!-- Rodap�-->

<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 83px; left:50px; visibility: visible;" id="topo2" > 
<!--INICIO BOTOES-->  
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%" >
    <TR>
    <% if (((String)request.getAttribute("verTable")).compareTo("N")==0){%>
    	<td  width="47"><b>Caixa&nbsp;</b> </td>
		<td width="134" ><strong>:</strong> 
			<input name="numCaixa" type="text" size="20" maxlength="10"value="<%= bean.getNumCaixa() %>" onkeypress="javascript:f_num();">		    
	    </td>
	    <td width="38"><b>Lote&nbsp;</b></td>
		<td width="134" ><strong>:</strong> 
		    <input name="numLote" type="text" size="20" maxlength="11"value="<%= bean.getNumLote() %>" onkeypress="javascript:acaoDigitaLote(this,this.form);">
	  	</td>  
	  	<td width="40"><b>Auto&nbsp;</b></td>
		<td width="220" ><strong>:</strong> 
	        <input name="numAuto" type="text" size="20" maxlength="12"value="<%= bean.getNumAutoInfracao() %>" onkeypress="javascript:f_end();" onKeyUp="this.value=this.value.toUpperCase()">
	  	</td>  
	  	<td align=left width=107>&nbsp;&nbsp;&nbsp;
       		<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaAutoEtiq',this.form);"  >	
    		<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
  	    </td>
	 </tr>
 </table>
</div>

<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 120px; left:50px; visibility: visible;" id="radios" > 
  <TABLE border="0"  cellpadding="0" cellspacing="0" width="100%">
  	<tr> 
   		<td width="48" ><b>Ordem</b></td>
		<td width="672" ><strong>:</strong> 
        	<input name="ordem" type="radio"  value="Caixa"  class="sem-borda"   style="height: 13px;" checked> Caixa &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  			<input name="ordem" type="radio"  value="Lote"  class="sem-borda"   style="height: 13px;"> Lote &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="ordem" type="radio"  value="Auto"  class="sem-borda"   style="height: 13px;"> Auto &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
  	</tr>
  </table>       
</div>
       	<% }
			else { %>
<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 83px; left:50px; visibility: visible;" id="topo" > 
<!--INICIO BOTOES-->  
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
    <tr>
		<td align="center"><b>Caixa&nbsp;</b></td>
		<td><strong>:</strong>
		     <input readonly name="numCaixa" type="text" size="20" maxlength="10"value="<%= bean.getNumCaixa() %>" style="border: 0px none;">
		</td>
		<td ><b>Lote&nbsp;</b></td>
		<td><strong>:</strong>
		     <input readonly name="numLote" type="text" size="20" maxlength="11"value="<%= bean.getNumLote() %>" style="border: 0px none;">
		</td>
		<td ><b>Auto&nbsp;</b></td>
		<td><strong>:</strong>
		     <input readonly name="numAuto" type="text" size="20" maxlength="12"value="<%= bean.getNumAutoInfracao() %>" style="border: 0px none;">
		</td>
		<td ><b>Ordem&nbsp;</b></td>
		<td><strong>:</strong>
		  	<input readonly name="ordem" type="text" size="20" maxlength="11"value="<%= bean.getOrdem() %>" style="border: 0px none;">
		</td>
	</tr>
  </table>
</div>

 
 <div style="position:absolute; left:50px; top:113px; width:720px; height:10px; z-index:1; overflow: visible; visibility: visible;" id="titulos"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr bgcolor="#be7272"> 
        <td width="100"> <div align="center"><font color="#ffffff"><strong>Seq</strong></font></div></td>
		<td width="175"> <div align="center"><font color="#ffffff"><strong>Caixa</strong></font></div></td>
	    <td width="175"> <div align="center"><font color="#ffffff"><strong>Lote</strong></font></div></td>
	    <td> <div align="center"><font color="#ffffff"><strong>Auto</strong></font></div></td>
	</tr>
  </table>
</div>
 	  
 <div style="position:absolute; width:720px; overflow: auto; z-index: 3; top: 130px; left: 50px; height: 193px; visibility: visible;" id="tabConteudo">   
   <table width="100%" border="0" cellpadding="1" cellspacing="1" id="fconsulta"  >  	 
	<%
        Iterator it = bean.getDados().iterator() ;
		int seq = 0;	
		REC.EtiquetaAutoBean dados =  new REC.EtiquetaAutoBean() ;   
        while (it.hasNext()) {
             seq ++;
			// lista os dados		
			dados = (REC.EtiquetaAutoBean)it.next() ;
	%>       
		<tr bgcolor="#FAEAE5" style="cursor:hand" align="center" onClick="javascript: chamaAuto('<%= dados.getNumCaixa()%>','<%= dados.getNumLote()%>','<%= dados.getNumAutoInfracao()%>');" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);"> 
		     <td width=100 align="center"><input name="seq"  readonly  type="text" size="10" value="<%= seq %>" style="border: 0px none; background-color: transparent;"></td>
			 <td align="center" width="175"><%= dados.getNumCaixa()%></td>
			 <td align="center" width="175"><%= dados.getNumLote()%></td>
		     <td ><%= dados.getNumAutoInfracao()%></td>	
 		</tr>	 
 	   <% 

	   } 
	   %>	
       </tr>
   	</table>
  </div>  
 <% } %>
 
<div style="position:absolute; left:50px; top:323px; width:720px; height:36px; z-index:100; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;" id="botRetornar"> 
 	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	
 		<tr>
			<td  align = "center"> 
  		      <button type=button NAME="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;"  onClick="javascript: valida('retorna',this.form);" >
			  <img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >			  </button>
			</td>

		</tr>
 	</table>
</div> 
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= bean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  
</form>
</BODY>
</HTML>
