<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<%
if(posTop==null) posTop ="170px";   
if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" >
	  <%
	   int max = 0,iContadorErro=0;	 

	   if ((GuiaDistribuicaoId.getAutos().size()>0) ) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {

			myAuto = (REC.AutoInfracaoBean)it.next() ;				
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			cor = "#ffffff";
			boolean bSelecionado=true;
			if (myAuto.getCodArquivo().equals("999"))  
			{
				cor = "#FFFFCC"; /*--Mais de um requerimento*/
			}
			if (myAuto.getMsgErro().length()>0)  
			{
				cor = "#CCCCCC"; /*--Com erro-*/
				iContadorErro++;
			}
		%>
		<tr bgcolor='<%=cor%>'> 
	        <td width="60" height="14" align="center">&nbsp;<%= (seq+1) %>&nbsp;
	        <input type="hidden" name="Selecionado" value="<%= seq %>">
	        </td>
	        <td width="200">&nbsp;<%=myAuto.getNumProcesso()%></td>
	        <td width="100">&nbsp;<%=myAuto.getDatProcesso()%></td>	        
	        <td width="120">&nbsp;<%=myAuto.getNumAutoInfracao()%></td>	        
	        <td>&nbsp;<%=myAuto.getNumRequerimento()+" - "+myAuto.getTpRequerimento()%></td>	        
		</tr>
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan="5">&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr><td height="2" colspan="5" bgcolor="#666666"></td></tr>		
		<% seq++; 
		   max++;
		}
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40"></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%=msg%></b></td>
		</tr>		
	<% 
		}
	} 
	%>
	<input type="hidden" name="ProcessosErro" value="<%=iContadorErro%>" >
    </table>      
</div>  
