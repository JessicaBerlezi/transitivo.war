<!--M�dulo : REC
	Vers�o : 1.2
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>

<%
if(posTop==null) posTop ="170px";   
if(posHei==null) posHei ="183px";    
%>
<link href="Css.jsp" rel="stylesheet" type="text/css">

<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda"  >
	 <%  if (GuiaDistribuicaoId.getAutos().size()>0) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
			myAuto = (REC.AutoInfracaoBean)it.next() ;				
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (myAuto.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width="80" height="14" align=center rowspan=2><%=(seq+1)%></td>
	        <td width="85" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getDatInfracao() %></td>
	        <td width="85" align=center onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getSigOrgao() %></td>
			<td width="85" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumAutoInfracao() %></td>
        	<td width="75" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumPlaca() %>       </td>
			<td width="150" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %>   </td>
			<td onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %>    </td>
		</tr>
		<tr bgcolor='<%=cor%>' 
			onclick     = "javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');"> 
	        <td colspan=4>&nbsp;<%=myAuto.getInfracao().getDscEnquadramento() %> </td>
	        <td colspan=2>&nbsp;<%=myAuto.getNumRequerimento() %>&nbsp;&nbsp;<%=myAuto.getTpRequerimento() %> </td>	        
		</tr>	
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan="7">&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr  bgcolor='<%=cor%>'> 
	      <td height="25" colspan="7">&nbsp;&nbsp;<strong>Resultado:</strong>&nbsp;&nbsp; 
    	    <input name="codResultRS<%= seq %>" type="radio" value="D" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"D") %> onclick="javascript: mostaCodMotivoDef('<%=seq%>');" class="sem-borda" style="height: 14px; width: 14px;">
	         DEFERIDO&nbsp;
    	    <input name="codResultRS<%= seq %>" type="radio" value="I" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"I") %> onclick="javascript: escondeCodMotivoDef('<%=seq%>');" class="sem-borda" style="height: 14px; width: 14px;">
        	 INDEFERIDO&nbsp;
        	Motivo:&nbsp;
	        <input name="txtMotivoRS<%= seq %>" type="text" size="67" maxlength="1000"  value="<%= myAuto.getRequerimentos(0).getTxtMotivoRS() %>">		       	
          </td>
	    </tr>
        <tr bgcolor='<%=cor%>' >
          <td colspan=7>
	           <table id ="codMotivoDef<%= seq %>"  style="visibility: hidden;" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda"  >
	            <tr>   
			 	    <td  colspan=7 height=20>&nbsp;&nbsp;<input type="radio" checked="checked" name="codMotivo<%= seq %>" value="1" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
				       <input type="radio" name="codMotivo<%= seq %>" value="2" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
				       <input type="radio" name="codMotivo<%= seq %>" value="3" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
				   <!--<input type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros -->
				    </td>
				<tr>
			   </table>	  
		   </td>  
        </tr>
    	<tr><td height="2" colspan="7" bgcolor="#666666"></td></tr>
		<% seq++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>  
