<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="AutoInfracaoBeanId"     scope="request" class="REC.AutoInfracaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>DETRAN &#8226;TRANSITIVO </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;

     case 'retorna':
        fForm.verTable.value="N"	  
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;   
          
	  case 'ajustaCrVenda':
        if(fForm.numPlaca.value == 0)
           alert("Informe o n�mero da Placa.");
        else{
			fForm.acao.value=opcao
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	  
		}  
		break ;		  
  	  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function changeColorOver(obj) {	
	obj.style.background="#EFF5E2";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#DEEBC2";
	obj.style.textDecoration="none";
}

</script>
<style type="text/css">
<!--
.style2 {color: #FFFFFF}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaAuto" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<!--INICIO_CORPO_sistema--> 
 <div id="dadosTelaAntes" style="position:absolute; left:50px; top:122px; width:722px; height:39px; z-index:13; overflow: visible; visibility: visible;">  
   <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr   height="20" bgcolor="#FAEAE5">
       <td width="30%" height="50" align="right" > &nbsp;<strong>Num Placa :</strong></td>
       <td width="3%"></td> 
       <td width="28%"><input name="numPlaca" type="text"  value="<%=AutoInfracaoBeanId.getNumPlaca() %>" size="14" maxlength="7"
         onkeypress="javascript:Mascaras(this,'AAX9999')"  onKeyUp="this.value=this.value.toUpperCase()"></td>
	   <td width="39%" >
         <button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ajustaCrVenda',this.form);"  >	
    	 <IMG src="<%= path %>/images/REC/bot_ok.gif" align="left" ></button>	
       </td>
     </tr>
 </table>
</div>	 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<script>document.ConsultaAuto.numPlaca.focus();</script>
</form>
</body>
</html>