<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- ChaRenunciaDPma o Objeto do Usuario logado -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="HistoricoBeanId" scope="request" class="REC.HistoricoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'R':
			close() ;
			break;
		
		case 'RenunciaDP':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;			
				
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  		
			   	fForm.submit();	 
			}
			break;

	   case 'O':  // Esconder os erro
   		  if (document.layers) 
   		  	fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 
	      	document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  

	   case 'N':  // Esconder os erro
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;		   
				fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
			}
			break;  
	}
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="RenunciaDP" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
 
<input name="acao"         type="hidden" value=' '>				

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>   
<div id="Cadastra" style="position:absolute; left:50px; right: 15px; top:273px; height:60px; z-index:1; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr bgcolor="#faeae5">
   	    <td colspan=3 height="22" valign="middle"><strong>&nbsp;
		<% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
			<%=AutoInfracaoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=AutoInfracaoBeanId.getDatStatus()%>)
		<% } %>
			</strong>
        </td>			
     </tr>
     <tr><td colspan=2 height=2></td></tr>
      <tr bgcolor="#faeae5">
	  <% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
        <% if ((AutoInfracaoBeanId.getCodStatus().equals("0")) || 
               (AutoInfracaoBeanId.getCodStatus().equals("1")) ||
               (AutoInfracaoBeanId.getCodStatus().equals("2")) ||
               (AutoInfracaoBeanId.getCodStatus().equals("3")) ||
               (AutoInfracaoBeanId.getCodStatus().equals("4")))  
        { %>		
	        <td colspan=2>&nbsp;&nbsp;Confirma Ajuste de Recurso ?&nbsp;&nbsp;
	        </td>
        <td align="center" valign="middle">         
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('RenunciaDP',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
		 </td>
		 
		  <tr><td height="2" colspan="2"></td></tr>
    		<tr>
	  			<td height="16" colspan="3" bgcolor="#faeae5">&nbsp;Documento Gerador:&nbsp; 
	  				<input type="text" name="txtMotivo" size="90" maxlength="50" value='<%=HistoricoBeanId.getTxtComplemento07() %>'onfocus="javascript:this.select();"> </td>
	  		</tr>	 
		<% }else{ %> 

	        <td colspan=2>&nbsp;&nbsp;Status n�o Permite Ajuste de Recurso&nbsp;&nbsp;
	        </td>

		<% } %> 
	  <% } %> 
			
      </tr>	 
      
     
  </table>		     
</div>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>