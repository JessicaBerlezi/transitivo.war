<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'LeNotificacao':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  		  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.numNotificacao,"N�mero da Notifica��o",0);
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaNotificacaoForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="MostraAuto.jsp" %>       
<%@ include file="apresentaInfracao_Diretiva.jsp" %>   
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr><td colspan="4" height="20"></td></tr>
   	    <% if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0) { %>
     <tr bgcolor="#faeae5">
   	    <td colspan="4" valign="middle"> 
		<%= AutoInfracaoBeanId.getNomStatus()+" em "+AutoInfracaoBeanId.getDatStatus() %>	
        </td>			
     </tr>
	<% } %>
     <tr><td colspan="3" height="10"></td></tr>
     <tr bgcolor="#faeae5"> 
	    <td width="40%" height="20"> 
          &nbsp;N�mero da Notifica��o:&nbsp; 
          <input type="text" name="numNotificacao" size="12" maxlength="9" value='<%=AutoInfracaoBeanId.getNumNotificacao() %>'  onkeypress="javascript:f_num();" onFocus="javascript:this.select();">
	    </td>
	    <td width="30%"> </td>
	    <td width="10%" align="center">  
          <button type=button NAME="Limpar"   style="height: 21px; width: 35px;border: none; background: transparent;" onClick="javascript: valida('N',this.form);">	
          <IMG src="<%= path %>/images/REC/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" > 
          </button>
	    </td>	  
        <td width="20%" align="center" >
			<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('LeNotificacao',this.form);">			  
		  	<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19">
			</button>
	    </td>	
    </tr>	
  </table>
</div>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>

<script>document.ConsultaNotificacaoForm.numNotificacao.focus();</script>	        


<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp"%>
<%@ include file="Rod_Diretiva.jsp"%>
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>