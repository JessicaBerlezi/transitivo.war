<!--M�dulo : REC
	Vers�o : 1.2
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>


<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto da Tabela de UF -->
<jsp:useBean id="StatusId" scope="request" class="REC.StatusBean" />
  <jsp:setProperty name="StatusId" property="j_abrevSist" value="REC" />  	 	     	 	  
  <jsp:setProperty name="StatusId" property="colunaValue" value="cod_status" />  
  <jsp:setProperty name="StatusId" property="popupNome"   value="codStatus"  />  
  <jsp:setProperty name="StatusId" property="checked" value="<%=AutoInfracaoBeanId.getCodStatus()%>" />                 	 
<% if ("REC0890".equals(StatusId.getSigFuncao())) { %>   	              	 
<jsp:setProperty name="StatusId" property="popupString" value="nom_status,SELECT cod_status||' - '||nom_status as nom_status,cod_status FROM TSMI_STATUS_AUTO WHERE COD_STATUS like '9%' ORDER BY cod_status asc" />                 	 
<% } else if("REC0880".equals(StatusId.getSigFuncao()))  { %>
<jsp:setProperty name="StatusId" property="popupString" value="nom_status,SELECT cod_status||' - '||nom_status as nom_status,cod_status FROM TSMI_STATUS_AUTO WHERE (COD_STATUS NOT like '9%' or cod_status = '99') ORDER BY cod_status asc" />               
<% } else { %>
<jsp:setProperty name="StatusId" property="popupString" value="nom_status,SELECT cod_status||' - '||nom_status as nom_status,cod_status FROM TSMI_STATUS_AUTO WHERE COD_STATUS NOT like '9%' ORDER BY cod_status asc" />               
<% } %>  

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	 switch (opcao) 
	 {    
	   case 'R':
	    	close() ;
			break;
	  case 'AjustaStatus':
	 		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
			    if (veCampos(fForm)==true) 
			    {		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
			    } 
		    }		  
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	
	valid = ValDt(fForm.Data,1) && valid
	verString(fForm.Data,"Data da Notifica��o",0);
	if (valid==false) alert(sErro) 
	return valid ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjustaStatusForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"         		type="hidden" value=' '>
<input name="statusIteracao"    type="hidden" value=' '>				
				
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<% if( request.getAttribute("statusIteracao") != null) {%>   
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr bgcolor="#faeae5">
   	    <td colspan=3 height="10" valign="middle"><strong>&nbsp;
			<% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
			<%=AutoInfracaoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=AutoInfracaoBeanId.getDatStatus()%>)
			<% } %>

			
			<% 
			  String sMsg20PT="- AUTO PERTENCE PROCESSO 20 PTS";
			  if (AutoInfracaoBeanId.getIndPontuacao().equals("1")) {%>
			  &nbsp;<%=sMsg20PT%>
			<%}%>
			
			
			
			</strong>        </td>			
     </tr>
     <tr><td colspan=3 height=2></td></tr>
     <tr bgcolor="#faeae5">
       <td width="41%" valign="middle" align="center" height=20>
            Nova Data do Status:&nbsp;
            <input type="text" name="Data" size="12" maxlength="10" value='<%=sys.Util.formatedToday().substring(0,10)%>' onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">
       </td>
       <td width="49%">Novo Status:&nbsp;<jsp:getProperty name="StatusId" property="popupString" />        
       <td width="10%"></td>
     </tr>
     <tr><td colspan=3 height=2></td></tr>     
     <tr bgcolor="#faeae5">
        <td colspan=2>&nbsp;&nbsp;Motivo:&nbsp;&nbsp;
			  <textarea rows=2 style="border-style: outset;" name="txtMotivo" cols="105" 
				 onfocus="javascript:this.select();"><%= StatusId.getTxtMotivo() %></textarea>
        </td>
        <td align="center" valign="middle">         
	        <% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AjustaStatus',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
			<% } %> 
		</td>			
      </tr>	 
  </table>		
</div>
<% } %>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>