<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	

<style type="text/css">
table
     {
     font-family: verdana;
     font-size: 8px;
	 border: 1px solid #000000;
	 color: #000000;
     }	 
td
	{
	font-family: verdana;
	font-size: 8px;
	position: static;
	border: 0px solid #000000;
	color: #000000;
	
}
.tdborda
	{
	border: 1px solid #000000;
}
.table_linha_top
	{
	border-top: 0.1em solid #000000;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em none #ffffff;
}	
.td_linha_left
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}

.td_linha_top_left
	{
	border-top: 0.1em solid #000000;	
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}
select
     {
	 font-family: verdana;
	 font-size: 9px;
	 border: 2px solid #000000;
	 }

textarea
     {
	 font-family: verdana;
	 font-size: 9px; 
	 border-color: 1px solid #666666; 
	 background-color: #eeeeee;
	 }

input 
	 {
	font-family: verdana;
	font-size: 9px;
	border: 1px solid #666666;
	background-color: #eeeeee;
	 }
	 
.input_semborda 
	 {
	font-family: verdana;
	font-size: 9px;
	border: 0px none ;
	background-color: #eeeeee;
	}
.semborda
     {
	 border-style: none ; 
	 background-color: #ffffff;
	 }
	 
.linha
	{
	font-family: verdana;
	font-size: 9px;
	height: auto;
	margin: 0px;
	position: static;
	border: thin none #ffffff;
	
}
.fontmaior
    {
	font-family:  verdana;
	font-size: 12px
	}
	
.fontmedia
    {
	font-family:  verdana;
	font-size: 9px
	}
.fontmenor
    {
	font-family:  verdana;
	font-size: 7px
	}
.fontseis
    {
	font-family:  verdana;
	font-size: 6px
	}
</style> 
<%@ page errorPage="ErrorPage.jsp" %>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td align="center" valign="top" class="fontmedia"> 
      <!--Linha-->
      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top: 0px none;">
        <tr>
          <td height="20" align="center" valign="middle" class="fontmaior" style="background-color: #cccccc; font-weight: bold; border-top: 1px solid #000000;border-bottom: 1px solid #000000">NOTIFICA&Ccedil;&Atilde;O DA AUTUA&Ccedil;&Atilde;O POR INFRA&Ccedil;&Atilde;O &Agrave; LEGISLA&Ccedil;&Atilde;O DE TR&Acirc;NSITO </td>
        </tr>
      </table>      
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top">
          <td height="3"></td>
        </tr>
      </table>
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="border: 1px solid #000000">
  <tr valign="top">
    <td width="50%" align="center" valign="top" class="fontmedia">
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="41%" height="32" align="center" valign="top" class="tdborda"><img src="<%=path%>/images/logos/logoniteroi2.gif" width="130" height="30" hspace="2" class="semborda"></td>
                <td width="1%">&nbsp;</td>
                <td width="32%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>&Oacute;rg&atilde;o Autuador</strong></span><br>
<span class="fontmenor">&nbsp;<%= ParamOrgBeanId.getParamOrgao("NOME_SECRETARIA") %></span></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="25%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d. &Oacute;rg&atilde;o Autuador</strong></span><br>
<span class="fontmenor">&nbsp;<%=UsuarioBeanId.getCodOrgaoAtuacao() %></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="100%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Nome do Respons&aacute;vel / Propriet&aacute;rio </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></span></td>
                </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="22%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>CPF/CGC</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getProprietario().getNumCpfCnpjEdt() %></span></td>
                <td width="1%" valign="top" class="semborda"></td>
                <td width="77%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Descri&ccedil;&atilde;o da Marca / Modelo</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></span></td>
                </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="22%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Placa do Ve&iacute;culo </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="25%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Esp&eacute;cie do Ve&iacute;culo</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="38%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Munic&iacute;pio </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNomMunicipio() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="12%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>UF</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getOrgao().getEndereco().getCodUF() %></span></td>
              </tr>
          </table></td>
        </tr>
      </table>      
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
          
          
          
          
          
          
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="22%" height="32" valign="top" class="tdborda">
                <span class="fontseis">&nbsp;<strong>Data de Emiss&atilde;o </strong></span><br>
				<span class="fontmenor">&nbsp;
		        <%if (("3,4,5,11".indexOf(NotifBeanId.getCodNotificacao()))>=0){
	               	if (AutoInfracaoBeanId.getDatExpPen().length()>0){%>
                <%=AutoInfracaoBeanId.getDatExpPen() %>
                <%}else{%>
                <%=AutoInfracaoBeanId.getDatStatus() %>
                <%}%>
                <%}else{%>
                <%if (AutoInfracaoBeanId.getDatExpAut().length()>0){%>
                <%=AutoInfracaoBeanId.getDatExpAut() %>
                <%}else{%>
                <%=AutoInfracaoBeanId.getDatStatus() %>
                <%}
                }%>
                </span>                </td>
                
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="77%" colspan="3" rowspan="3" valign="top" class="tdborda" style="text-align: justify; padding-left: 3px; padding-right: 3px;"><span class="fontseis"><strong>Descri&ccedil;&atilde;o Resumida da Infra&ccedil;&atilde;o</strong></span><br />
<span class="fontmenor"><%=AutoInfracaoBeanId.getDscResumoInfracao() %></span></td>
              </tr>
              <tr>
                <td height="3" valign="top" class="semborda"></td>
                <td valign="top" class="semborda"></td>
                </tr>
              <tr>
                <td height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&uacute;mero do Auto</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao() %></span></td>
                <td valign="top" class="semborda">&nbsp;</td>
                
                
      <!-- -->          
                </tr>
              <tr>
                <td height="2" valign="top" class="semborda"></td>
                <td valign="top" class="semborda"></td>
                <td valign="top" class="semborda"></td>
                <td valign="top" class="semborda"></td>
                <td valign="top" class="semborda"></td>
                </tr>
              <tr>
                <td height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&uacute;mero do AR </strong></span><br />
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumNotificacao() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&ordm; Cert. Aferi&ccedil;&atilde;o</strong></span><br />
                    <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumCertAferAparelho() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Data Aferi&ccedil;&atilde;o</strong></span><br />
                    <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDatUltAferAparelho() %></span></td>
              </tr>
          </table>
          
          
          
          
          
          
          </td>
        </tr>
      </table>      
      </td>
    <td rowspan="2"  align="center" valign="middle" bgcolor="#CCCCCC" class="fontmedia"><img src="<%=path%>/images/logos/auto_de_infracao.gif"></td>
    <td  width="50%" align="center" valign="top" class="fontmedia" rowspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
      <tr>
        <td valign="top">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
            <tr>
              <td width="100%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Descri&ccedil;&atilde;o da Infra&ccedil;&atilde;o</strong></span><br />
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscInfracao() %></span></td>
            </tr>
        </table></td>
      </tr>
    </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="45%" height="50" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Local da Infra&ccedil;&atilde;o </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDscLocalInfracaoNotificacao()%></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="18%" valign="top" class="tdborda">&nbsp;<span class="fontseis"><strong>&oacute;d. Infra&ccedil;&atilde;o</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %></span></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="23%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Enquadramento</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscEnquadramento() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="18%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d. Munic&iacute;pio </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodMunicipio() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="19%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Munic&iacute;pio / UF </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNomMunicipio() %>/<%=AutoInfracaoBeanId.getOrgao().getEndereco().getCodUF() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="18%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&uacute;mero NIT </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getIdentOrgaoEdt() %></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="18%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Pontos na CNH </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getNumPonto() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="23%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Gravidade da Infra&ccedil;&atilde;o </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscGravidade() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="28%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Data/Hora da Infra&ccedil;&atilde;o </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDatInfracao() %>&nbsp;/<%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></span></td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="28%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d./ Agente Resp.</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodAgente()%>&nbsp;/&nbsp;<%=AutoInfracaoBeanId.getDscAgente()%></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="42%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Velocidade Considerada</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocConsiderada() %></span></td>
                <td width="1%">&nbsp;</td>
                <td width="28%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Velocidade Regulamentada</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocPermitida() %></span></td>
                <td width="1%" valign="top" class="semborda">&nbsp;</td>
                <td width="28%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Velocidade Medida</strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocAferida() %></span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="100%" height="32" valign="top" class="tdborda"><span class="fontseis"><strong>&nbsp;<strong></strong>Prazo para Defesa da Autua&ccedil;&atilde;o</strong></span><br>
<span class="fontmenor">&nbsp;At� 15 dias do recebimento</span></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
        <tr>
          <td valign="top">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="45%" height="49" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Equipamento Marca / Modelo </strong></span><br>
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDscMarcaModeloAparelho() %></span></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
  </tr>
  <tr valign="top">
    <td align="center" valign="top" class="fontmedia">
	<table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
      <tr>
        <td valign="top">
          <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
            <tr>
              
              <td width="48%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Identif. do Aparelho</strong></span><br />
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumIdentAparelho() %></span></td>
              <td width="1%"></td>
              <td width="38%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&ordm; Cert. INMETRO / CERTA</strong></span><br />
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumInmetroAparelho() %></span></td>
              <td width="1%" valign="top">&nbsp;</td>
              <td width="12%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Tipo</strong></span><br />
<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodTipDispRegistrador() %></span></td>
            </tr>
        </table></td>
      </tr>
    </table></td>
    <td align="center" valign="top" class="fontmedia">&nbsp;</td>
  </tr>
</table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top">
          <td align="center" valign="top" class="fontmedia">&nbsp;</td>
        </tr>
      </table>      <!--FIM Linha-->    </td>
</tr>
<tr> 
    <td height="167" align="center" valign="top"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
          <% if ( AutoInfracaoBeanId.getTemFotoDigitalizada()) { %>
		  <tr>
            <td height="120" align="center" valign="top"><img src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getFotoDigitalizada().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getFotoDigitalizada().getParametro(ParamSistemaBeanId)%>&header=#"  width="700" height="167"></td>
          <%}else{%>
		  <tr>
  <td height="167" align="center" class="fontmaior"><strong>RESPEITE O C�DIGO DE TR�NSITO BRASILEIRO</strong></td>
  <%}%></tr>
  
    </table></td>
  </tr>
</table>
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr>   
		<td height="2"></td>
	</tr>
</table>

      
      <!--Local da foto-->



<!--Fim Local da foto-->