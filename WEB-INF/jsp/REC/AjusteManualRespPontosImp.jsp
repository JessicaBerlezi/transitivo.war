<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<% String path=request.getContextPath(); %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId" scope="request" class="REC.RequerimentoBean" />
<jsp:useBean id="myHist" scope="request" class="REC.HistoricoBean" />

<jsp:include page="CssImpressao.jsp" flush="true" />
<!--Cabecalho-->
<jsp:include page="../sys/cabecalho_imp.jsp" flush="true" />

<!-- Mudan�a ================================================================= -->
<link href="CssImpressao.jsp" rel="stylesheet" type="text/css">

<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	<tr>
		<td width="29%" align="left" >N&ordm; Auto :&nbsp;&nbsp;<strong class="fontmaior"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong></td>
		<td width="30%" align="center">Placa :&nbsp;&nbsp;<strong  class="fontmaior"><%=AutoInfracaoBeanId.getNumPlaca() %></strong></td>
		<td width="41%" align="right" nowrap>Processo :&nbsp;&nbsp;<strong class="fontmaior"><%= AutoInfracaoBeanId.getNumProcesso() %></strong></td>	 
	</tr>	 
</table>
<div align="center">
	<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#000000"></td></tr>
	</table>
</div>     
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr> 
    <td width="25%" height="20" bgcolor="#b6b6b6" > 
      <strong>1 &#8226; DADOS DO V&Eacute;ICULO</strong>
    <td width="75%" bgcolor="#b6b6b6" align="right" ><strong class="fontmenor">Propriet&aacute;rio :&nbsp;
      	<span class="fontmedia">
      		<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
      		<%= "2".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()) ? " (Jur.)" : "" %>
      	</span></strong>
      </td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr> 
    <td width="29%" valign="bottom"> 
      <div align="left" class="fontmenor"><strong>MARCA/MODELO</strong></div></td>
      	<td width="13%" valign="bottom"> 
      <div align="center" class="fontmenor"><strong>ESP�CIE</strong></div></td>
    	<td width="17%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>CATEGORIA</strong></div></td>
    	<td width="15%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>TIPO</strong></div></td>
    	<td width="25%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>COR PREDOMINANTE</strong></div></td>
  </tr>
</table>
<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1" bgcolor="#cccccc"></td>	</tr>
  </table>
</div>  
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
    <td width="29%"> 
      <div align="left" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></div></td>
    <td width="13%"> 
      <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %></div></td>
    <td width="17%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscCategoria() %></div></td>
    <td width="15%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscTipo() %></div></td>
    <td width="25%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscCor() %></div></td>
  </tr>
</table>  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>	  
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr> 
    <td width="100%"><strong class="fontmenor">ENDERE&Ccedil;O :</strong>&nbsp;&nbsp;
    	<span class="fontmedia">
			<% if ("1".equals(UsuarioFuncBeanId.getIndVisEndereco())) { %>			
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %>&nbsp;
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %>&nbsp;		  
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %>&nbsp;		  
			<% } %>
    	</span>
    </td>
  </tr>
</table>	
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr>          
    <td width="37%" height="20" bgcolor="#B6B6B6"> 
      <strong>2 &#8226; DADOS DA INFRA&Ccedil;&Atilde;O</strong></td>
    <td width="20%" bgcolor="#B6B6B6"> 
      <%=AutoInfracaoBeanId.getSigOrgao() %>&nbsp;&nbsp;</td>
    <td width="43%" bgcolor="#B6B6B6">
	    <b>Notificado em: <%=AutoInfracaoBeanId.getDatNotificacao()%>
    	   &nbsp;&nbsp;
	       <% if (AutoInfracaoBeanId.getNomMotivoNotificacao().length()>0) { %>
    	   (<%=AutoInfracaoBeanId.getNomMotivoNotificacao() %>)
	       <% } %>
    	</b>&nbsp;&nbsp;&nbsp;Vencto:&nbsp;<%=AutoInfracaoBeanId.getDatVencimento() %>   
	</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr><td width="20%" valign="bottom"> 
    		<div align="left"  class="fontmenor"><strong>C�DIGO </strong></div></td>
	    <td width="14%" valign="bottom">
			<div align="center" class="fontmenor"><strong>GRAVIDADE</strong></div></td>      
	    <td width="17%" valign="bottom"> 
    	  	<div align="center" class="fontmenor"><strong>PONTO</strong></div></td>
	    <td width="22%" valign="bottom"> 
    	 	<div align="center" class="fontmenor"><strong>ENQUADRAMENTO</strong></div></td>  
	    <td width="27%" valign="bottom"> 
    	    <div align="center" class="fontmenor"><strong>DATA/HORA</strong></div></td>
  </tr>
</table>  	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
  	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>    
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
      <td width="20%"> <div align="left" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %>
        &nbsp;
	  	<%  if (AutoInfracaoBeanId.getInfracao().getCodInfracao().equals("")==false) { %>
		<%=	(AutoInfracaoBeanId.getInfracao().getIndTipo().equals("C") ? "Condutor" : "Propriet�rio")  %>
		<%  } %></div></td>
      <td width="14%"> <div align="center" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscGravidade() %></div></td>
      <td width="17%"> 
      	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getInfracao().getNumPonto() %></div></td>
        	<td width="22%"> 
    	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getInfracao().getDscEnquadramento() %></div></td>
	       	<td width="27%"> 
    	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getDatInfracao() %>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></div></td>
</tr>
</table>	   
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>  
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
  	<td width=57%><strong  class="fontmenor">DESCRI��O :</strong>&nbsp;&nbsp;<span class="fontmedia"><%=AutoInfracaoBeanId.getInfracao().getDscInfracao() %></span></td>
    <td align=right width=43%><%=AutoInfracaoBeanId.getSituacao() %>&nbsp;</td>
  </tr>
</table>	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
	  <td><strong class="fontmenor">LOCAL :</strong>&nbsp;&nbsp;<span class="fontmedia">
	      <% if (AutoInfracaoBeanId.getDscLocalInfracao().length()+AutoInfracaoBeanId.getNomMunicipio().length()>60) { %>	  
		      <%= (AutoInfracaoBeanId.getDscLocalInfracao()+" "+AutoInfracaoBeanId.getNomMunicipio()).substring(0,61) %>
	      <% } else { %>
    	  	  <%=AutoInfracaoBeanId.getDscLocalInfracao() %>&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getNomMunicipio() %>
		  <% } %>
	  	</span>
	  </td>
  </tr>
</table> 	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>        
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
    <td><strong  class="fontmenor">RESP. PONTOS  :</strong>&nbsp;&nbsp;<span class="fontmedia">
    	<%=AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>
    	<%= "1".equals(AutoInfracaoBeanId.getIndCondutorIdentificado()) ? " (Ident)" : "" %></span>
    </td>
  </tr>
</table>     
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>	  
<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="2" bgcolor="#000000"></td></tr>
</table>
<!--FIM Linha-->


<!-- Fim Mudan�a ============================================================================= -->


<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	<tr>
		<td width="60%" align="left" ><strong>Resp.Pontos :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>
        <%if (myHist.getTxtComplemento12().equals("")){ %>
		 <strong >&nbsp;&nbsp;- DECIS�O JUDICIAL &nbsp;&nbsp;</strong>	 
		<%}%>
        </td>
		<td width="20%" align="center"><strong>N� CNH :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getNumCnh() %></td>
		<td width="20%" align="right" nowrap><strong >UF CNH :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getCodUfCnh()%></td>	 
	</tr>	 
</table>
<div align="center">
	<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#000000"></td></tr>
	</table>
</div>     
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr> 
	    <td width="60%" align="left" ><strong>Endere�o :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtEndereco() %></td>
		<td width="20%" align="center"><strong>Tipo :&nbsp;&nbsp;</strong><%=(AutoInfracaoBeanId.getCondutor().getIndTipoCnh().equals("1") ? "PGU" : "CNH")  %></td>
		<td width="20%" align="right" nowrap><strong >CPF :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getNumCpfCnpjEdt() %></td>	 
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr> 
    	<td width="20%" align="left" ><strong>N�mero :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getNumEndereco() %></td>
		<td width="30%" align="center"><strong>Complemento :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtComplemento()%></td>
		<td width="50%" align="right" nowrap><strong >Bairro :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getNomBairro() %></td>	 
  </tr>
</table>
<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1" bgcolor="#cccccc"></td>	</tr>
  </table>
</div>  
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
  		<td width="40%" align="left" ><strong>Cidade :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getNomCidade() %></td>
		<td width="20%" align="center"><strong>CEP :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getNumCEPEdt() %></td>
		<td width="10%" align="right" nowrap><strong >UF :&nbsp;&nbsp;</strong><%=AutoInfracaoBeanId.getCondutor().getEndereco().getCodUF() %></td>	 
		<td width="30%" align="right" nowrap><strong >Data Req. :&nbsp;&nbsp;</strong><%=RequerimentoId.getDatRequerimento() %></td>	 
  
  </tr>
</table>  
<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="2" bgcolor="#000000"></td></tr>
</table>
<!--FIM Linha-->
<!--Rodap�-->
<jsp:include page="../sys/rodape_imp.jsp" flush="true" />
<!--FIM_BLOCO B-->
