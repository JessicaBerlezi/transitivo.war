<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="AutoInfracaoBeanEspelhoId" scope="session" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId" scope="request" class="REC.RequerimentoBean" />
<jsp:useBean id="RequerimentoEspelhoId" scope="session" class="REC.RequerimentoBean" />
<jsp:useBean id="HistoricoEspelhoId" scope="session" class="REC.HistoricoBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />  
<jsp:useBean id="UFCNHId" scope="request" class="sys.UFBean" />
<jsp:useBean id="UFId" scope="request" class="sys.UFBean" />
<%
   String isDisabled = "";   
   UFCNHId.setChecked(AutoInfracaoBeanId.getProprietario().getCodUfCnh());
   UFId.setChecked(AutoInfracaoBeanId.getProprietario().getEndereco().getCodUF());
   if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")){isDisabled += "disabled";}
   
%>  
<!-- UF do CNH --> 
<jsp:setProperty name="UFCNHId" property="j_abrevSist" value="REC" /> 
<jsp:setProperty name="UFCNHId" property="popupExt" value="<%=isDisabled%>"  /> 	 	     	 	  
<jsp:setProperty name="UFCNHId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFCNHId" property="popupNome"   value="codUfCNHTR"  />  
<jsp:setProperty name="UFCNHId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 
<!-- UF do endereco -->
<jsp:setProperty name="UFId" property="j_abrevSist" value="REC" />
<jsp:setProperty name="UFId" property="popupExt" value="<%=isDisabled%>"  />  	 	     	 	  
<jsp:setProperty name="UFId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFId" property="popupNome"   value="codUf"  />  
<jsp:setProperty name="UFId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {

	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'ConfirmaAjuste':



  var valor = Tiraedt(trim(document.AjusteManualForm.numCPF.value),15);
  var forma =0;
  
  if (valor*1==0)
  {
    alert("CPF/CNPJ invalido.");
	obj.value="";
    document.AjusteManualForm.numCPF.focus();
    return;
  }
  

  if (valor.length != 0) {     
    if(valor.length <= 11){  
      <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
		     alert("CPF com digito verificador invalido.");
		     document.AjusteManualForm.numCPF.value="";
             document.AjusteManualForm.numCPF.focus();

         return ;
	  }
      document.AjusteManualForm.cpfhidden.value=valor;	  
	  document.AjusteManualForm.numCPF.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11);
	}
	if(valor.length == 14 && valor.substring(0,4)!="."){
	    if (dig_cgc(valor) == false) {
         alert("CNPJ com digito verificador invalido. \n");
		 valid = false ;
         document.AjusteManualForm.numCPF.value="";
         document.all["numCPF"].focus();
         return;
	    }
	    else{
	    document.AjusteManualForm.cpfhidden.value=valor;
	    document.AjusteManualForm.numCPF.value = valor.substring(0,2)+"."+valor.substring(2,5)+"."+valor.substring(5,8)+"/"+valor.substring(8,12)+"-"+valor.substring(12,14);
	    }
	}
  }
	   	if (trim(document.AjusteManualForm.numcpfResp.value).length==0)
	   	{
	   		document.AjusteManualForm.numcpfResp.value="00000000000";
	   		document.AjusteManualForm.cpfpnthidden.value="00000000000";
	   	}

	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	  case 'N':  // Esconder os erro
  		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break; 

  }
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AjusteManualForm.dataReq,1) && valid
	if (valid==false) alert(sErro) 
	return valid ;
} 
function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)
  
  if (valor*1==0)
  {
    alert("CPF/CNPJ invalido.");
	obj.value="";
    document.AjusteManualForm.numCPF.focus();
    return false;
  }
  

  if (valor.length != 0) {     
    if(valor.length <= 11){  
      <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido.");
		     obj.value="";
             document.AjusteManualForm.numCPF.focus();
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido/n";
		  	obj.value="";	
		    document.AjusteManualForm.numCPF.focus(); 
		 }
         return false ;  	 
	  }
      document.AjusteManualForm.cpfhidden.value=valor;	  
	  obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11);
	}
	if(valor.length == 14 && valor.substring(0,4)!="."){
	    if (dig_cgc(valor) == false) {
         alert("CNPJ com digito verificador invalido. \n");
		 valid = false ;
         obj.value="";
         document.all["numCPF"].focus();
         return false;
	    }
	    else{
	    document.AjusteManualForm.cpfhidden.value=valor;
	    obj.value = valor.substring(0,2)+"."+valor.substring(2,5)+"."+valor.substring(5,8)+"/"+valor.substring(8,12)+"-"+valor.substring(12,14);
	    }
	}
  }
}

function veCPF2(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if (valor.length != 0) {     
    if(valor.length <= 11){  
      <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido.");
		     obj.value="";
             document.AjusteManualForm.numCPF.focus();
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido/n";
		  	obj.value="";	
		    document.AjusteManualForm.numCPF.focus(); 
		 }
         return false ;  	 
	  }
	  document.AjusteManualForm.cpfpnthidden.value=valor;	  
	  obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11);
   }

 }
}

function dig_cgc(cgcinf) {
  w = Tiraedt(cgcinf,15); 
  
  if (w.length < 15) { return false }   
  var dvcgc = w.substring(0,13);  
  var s1 = 0;
  for (i=0; i<5  ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(6-i)) }
  for (i=5; i<13 ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(14-i)) }  
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1 * 2
  for (i = 0; i < 6 ; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(7-i)) }
  for (i = 6; i < 13; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(15-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(13,15)
  return (oldDV == DV) ;
}

function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;  
}
function apaga(tipCnh) {
	if (tipCnh=='PGU') {
		if (AjusteManualForm.indTipoCNHPGU != null) AjusteManualForm.indTipoCNHPGU.checked=false ;
	}
	else {
		if (AjusteManualForm.indTipoCNH != null) AjusteManualForm.indTipoCNH.checked=false ;	
	}
	return ;
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjusteManualForm" method="post" action="">
<%   
  
  String cpf = AutoInfracaoBeanId.getProprietario().getNumCpfCnpj(),c_cpf="";    					
  c_cpf=cpf;
  if (cpf.length()>0)
  {
    if (cpf.length()== 11) cpf = cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11);
    else cpf = cpf.substring(0,2)+"."+cpf.substring(2,5)+"."+cpf.substring(5,8)+"/"+cpf.substring(8,12)+"-"+cpf.substring(12,14);
  }

  String cpf2 = AutoInfracaoBeanId.getCondutor().getNumCpfCnpj(),c_cpf2="";
  c_cpf2=cpf2;
  if (cpf2.length()==14)
  {
    cpf2 = cpf2.substring(3,14);
    c_cpf2=cpf2;    
    cpf2 = cpf2.substring(0,3)+"."+cpf2.substring(3,6)+"."+cpf2.substring(6,9)+"-"+cpf2.substring(9,11);	  	     	 
  }else if(cpf2.length()== 11) cpf2 = cpf2.substring(0,3)+"."+cpf2.substring(3,6)+"."+cpf2.substring(6,9)+"-"+cpf2.substring(9,11);
%>

<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input name="flagMantemDados"  type="hidden" value='<%=(String)session.getAttribute("flagMantemDados")%>'>
<input name="verTabela" type="hidden" value=''>
<input name="cpfhidden" type="hidden" value='<%=c_cpf%>'>
<input name="cpfpnthidden" type="hidden" value='<%=c_cpf2%>'>
<!--IN�CIO_CORPO_sistema--> 
<jsp:include page="lerAutoPlaca.jsp"      flush="true" />    
<jsp:include page="apresentaInfracao.jsp" flush="true" />   
<div style="position:absolute; left:66px; top:269px; width:686px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>



<div id="recurso3" style="position:absolute; left:66px; top:273px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>			 	 			

<!-- ========================1� bloco========================================== -->   
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	<tr bgcolor="#faeae5">
		<td width="50%" align="left" ><strong>Propriet�rio :&nbsp;&nbsp;</strong><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="nomResp" type="text" value='<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>' size="41" maxlength="40" onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>
		<td width="29%" align="left"><strong>CPF/CNPJ:&nbsp;&nbsp;</strong>
  		 <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCPF" type="text" size="22" maxlength="14" value='<%=cpf%>'  onfocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);">
		</td>
		<td width="21%" align="rigth" nowrap><strong >Data Req:&nbsp;&nbsp;&nbsp;</strong><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="dataReq" type="text" size="14" maxlength="10" value='<%=RequerimentoId.getDatRequerimento() %>' onkeypress="javascript:Mascaras(this,'99/99/9999');"></td>	 
	</tr>	 
	</table>

<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
</div>  

<!-- ========================Fim do 1� bloco========================================== -->    
 
    
<!-- ========================2� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5"> 
  		<td width="35%" align="left" ><strong>CPF Resp.Pontos :&nbsp;</strong>
	    <input name="numcpfResp" type="text" size="19" maxlength="11" value='<%=cpf2%>' onfocus="javascript:this.select();" onChange = "javascript:veCPF2(this,0);">
	    </td>
	    <td width="25%" align="left" ><strong>Tipo :&nbsp;</strong>
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>disabled<%}%> name="indTipoCNHPGU" type="checkbox" style=" heigth:11;" value='1'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getProprietario().getIndTipoCnh(),"1")%> class="input" onclick="javascript:apaga('CNH');">
      				PGU &nbsp;&nbsp;&nbsp;
        <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>disabled<%}%> name="indTipoCNH" type="checkbox" style=" heigth:11;" value='2'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getProprietario().getIndTipoCnh(),"2")%> class="input" onclick="javascript:apaga('PGU');">
      				CNH
	    </td>
		<td width="30%" align="rigth"><strong>&nbsp;&nbsp;N� CNH/PGU:&nbsp;</strong><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCNH" type="text" size="19" maxlength="11" value='<%=AutoInfracaoBeanId.getProprietario().getNumCnh() %>' onChange="this.value=this.value.toUpperCase()"></td>
		<td width="10%" align="rigth" nowrap><strong >UF CNH :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong><jsp:getProperty name="UFCNHId" property="popupString" /></td>	 
  </tr>
</table>   

<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
</div>      
<!-- ========================Fim do 2� bloco========================================== -->        

<!-- ========================3� bloco========================================== --> 
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5"> 
	    <td width="37%" align="left" ><strong>Endere�o :&nbsp;&nbsp;</strong>
	    <input  name="txtEndereco" type="text" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco().trim() %>' size="35" maxlength="27"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">

	    </td>
	    <td width="8%" align="rigth"><strong>N�:&nbsp;</strong><input name="txtNumEndereco" type="text" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco().trim() %>' size="7" maxlength="5"  onKeyPress="javascript:f_num();" ></td>
		<td width="30%" align="center"><strong>Compl.:&nbsp;</strong><input name="txtComplemento" type="text" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento().trim() %>' size="20" maxlength="11"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" ></td>
		<td width="25%" align="left" nowrap><strong >Bairro :&nbsp;&nbsp;&nbsp;</strong><input name="nomBairro"   type="text" size="20" maxlength="30" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>	 
  </tr>
</table> 

<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
</div>       
<!-- ========================Fim do 3� bloco========================================== -->        
<!-- ========================4� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5"> 
	    <td width="32%" align="left" ><strong>Cidade :&nbsp;&nbsp;</strong>
	    	<input  name="nomCidade"   type="text" size="32" maxlength="30" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomCidade().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">      	
	    </td>
        <td width="15%" align="center"><strong>CEP&nbsp; :&nbsp;&nbsp;</strong><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCEPTR" type="text" size="11" maxlength="8" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt() %>' onKeyPress="javascript:f_num();"></td>
        <td width="27%" align="center"><strong>Decis&atilde;o Judicial&nbsp; :&nbsp;&nbsp;</strong>
        <input name="decisaoJur" type="checkbox" class="input" style=" heigth:11;" value='1' <%= sys.Util.isChecked(HistoricoEspelhoId.getTxtComplemento11(),"1")%>></td>
		<td width="21%" align="center"><strong>UF : &nbsp;</strong>
	    <jsp:getProperty name="UFId" property="popupString" /></td>
					 
		<td width="5%" align="right" nowrap>
		<%if (!UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>
        	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ConfirmaAjuste',this.form);">	
		   		<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" style="margin-left: 4px; margin-top:0px; margin-bottom: 0px" >
		  	</button>
		<%}%>
		</td>	 
  </tr>
</table>
<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
</div>       
<!-- ========================Fim do 4� bloco========================================== -->        
    <%}%>
</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px;top:379px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>   		
   	<script>document.AjusteManualForm.numAutoInfracao.focus();</script>	        


<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
 </body>
</html>
