<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Bean AutoInfracao -->
<jsp:useBean id="AutoInfracaoBeanId"      scope="request" class="REC.AutoInfracaoBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaFuncao':
	  if (fForm.codSistema.value==0) {
		alert("Sistema n�o selecionado.") ;
	  }
	  else {	
	    fForm.acao.value=opcao
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 

   case 'buscaDados':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
	  
   case 'ImprimeResultConsulta':
		fForm.acao.value=opcao
   		fForm.target= "_blank";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  	break; 	  
	  		  
   case 'R':
      close() ;   
	  break;
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function Mostra(opcoes,fForm,numplaca,numauto) {	
	document.all["acao"].value=opcoes;
	document.all["mostraplaca"].value=numplaca;
	document.all["mostranumauto"].value=numauto;
   	document.all["AjusteAutoCr"].target= "_blank";
    document.all["AjusteAutoCr"].action = "acessoTool";  
   	document.all["AjusteAutoCr"].submit();	 
}

function changeColorOver(obj) {	
	obj.style.background="#F5D6CC";
	obj.style.textDecoration="none";
}

function changeColorOut(obj) {
	obj.style.background="#FAEAE5";
	obj.style.textDecoration="none";
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codSistema.value==0) {
	   valid = false
	   sErro = "Sistema n�o selecionado. \n"
	}	
	if (fForm.codOperacao.value==0) {
	   valid = false
	   sErro += "Fun��o n�o selecionada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}
 
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AjusteAutoCr" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"           type="hidden" value=' '>				
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<% ArrayList resutTrans412 = (ArrayList)request.getAttribute("resutTrans412");
   String numPlaca = (String)request.getAttribute("numPlaca");  %>
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 90px; left: 50px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
 	    <td>Alterados <b><%=resutTrans412.get(0)%></b> Autos para Placa <b><%= numPlaca%></b><td>
      </tr>	
      <tr><td colspan="2" height="2"></td></tr>	 
  </table>    
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	 
    <tr>
	  <td width="91%"  align = "right"> 
	    <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
		  <img src="<%= path %>/images/bot_retornar.gif" width="52" height="19" >
	    </button>
	  </td>
     </tr> 
  </table>
</div>     


<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 142px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr><td colspan=4 height=3></td></tr>		
 	<tr > 	
	   	 <td width="70" align="center" bgcolor="#be7272"><font color="#ffffff"><b>SEQ</b></font></TD>		 	  		 		 	  
	   	 <td width="647" align="left" bgcolor="#be7272"><font color="#ffffff"> &nbsp;&nbsp;&nbsp;<b>AUTO</b></font></TD>	
 
	</tr> 
   </table>
</div>   

<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 162px; left: 50px; height: 184px; visibility: visible;">   
   <table id=fconsulta border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
  <table id=fconsulta border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator it = resutTrans412.subList(1,resutTrans412.size()).iterator() ;
		int seq = 1;	
		REC.AutoInfracaoBean autoInfra =  new REC.AutoInfracaoBean() ;
        while (it.hasNext()) {
			// lista os dados		
			 autoInfra = (REC.AutoInfracaoBean)it.next();
	%>        
		<tr bgcolor="#faeae5" style="cursor:hand;" onmouseover="javascript:changeColorOver(this);"; onmouseout="javascript:changeColorOut(this);">
    	   	 <td width="70"  align="center"><%= seq %></TD>		 		 	  
             <td width="647"  onclick="javascript:Mostra('MostraAuto',this.form,'<%=numPlaca %>','<%=autoInfra.getNumAutoInfracao() %>');">&nbsp;&nbsp;<%= autoInfra.getNumAutoInfracao() %> </td>
   <% 
	  		seq++;
	   }
	   %>	

   </table>    
</div>

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</BODY>
</HTML>

