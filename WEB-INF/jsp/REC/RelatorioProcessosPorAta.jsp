<%@page import="UTIL.DataUtils"%>
<%@page import="REC.ControladorRecGeneric"%>
<%@page import="REC.RequerimentoBean"%>
<%@page import="java.util.List" %>
<%@page import="java.util.HashMap" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Relat�rio: Ata de Publica��o</title>
		
		<link rel="stylesheet" href="css/transitivo.css" />
		<style>
		</style>
	</head>
	<body style="font-family: verdana;">
		<div class="cab-relatorio" id="head">
			<div style="display: block; font-size: 9px; width: 100%;">
				EMISS�O: <strong><%=sys.Util.formatedToday().substring(0,10)%> - <%=sys.Util.formatedToday().substring(13,18)%></strong>
			</div>
			<div style="width: 10%;">
				<img alt="Prefeitura de Maca�" src="images/prefeitura_macae.png" style="width: 200px; height: 90px;">
			</div>
			<div style="text-align: center; vertical-align: top; width: 70%; margin: auto auto;">
				<p>PREFEITURA MUNICIPAL DE MACA�</p>
						<p>:: TRANSITIVO ::</p>
					<p>RELAT�RIO DE PROCESSOS POR ATA</p>
			</div>
			<div style="float: right; padding: 25px 0px; width: 10%; margin-right: 30px;">
				<img alt="" src="images/im_logosmit.gif" width="98px" height="30px">
			</div>
		</div>
		<div id="body" style="font-size: 12px; padding: 3px;">
			<%
				List<HashMap<String, String>> listaRequerimentos = RequerimentoBean.consultarRequerimentoPorAta(
						request.getAttribute("codAta").toString(),
						request.getAttribute("codRelator").toString(),
						request.getAttribute("codOrgao").toString());
			%>
			<div style="font-weight: bold;">
				Processos da Ata: <%= listaRequerimentos.get(0).get("ata") %>
			</div>
			<div style="font-weight: bold;">
				Data: <%= ControladorRecGeneric.dateToDateBr(listaRequerimentos.get(0).get("dataSessao").toString()) %>
			</div>
			<div style="font-weight: bold; background-color: #b6b6b6; padding: 3px;">
				Dados dos Processos:
			</div>
			<div>
				<table style="width: 100%; font-size: 10px;">
					<thead>
						<tr>
							<th>N� do Auto</th>
							<th>N� do Processo</th>
							<th>Cod Infra��o</th>
							<th>Requerente</th>
							<th>Placa</th>
							<th>Data do Processo</th>
							<th>Relator</th>
							<th>Resultado</th>
						</tr>
					</thead>
					<tbody>
						<%
							for(int i = 0; i < listaRequerimentos.size(); i++){ %>
								<tr style="text-align: center; height: 20px;">
									<td><%= listaRequerimentos.get(i).get("numAuto") %></td>
									<td><%= listaRequerimentos.get(i).get("numProcesso") %></td>
									<td><%= listaRequerimentos.get(i).get("codInfracao") %></td>
									<td><%= listaRequerimentos.get(i).get("nomRequerente") %></td>
									<td>
										<% if(listaRequerimentos.get(i).get("placa") == null){ %>
												N/A
										<% } else { %>
											<%= listaRequerimentos.get(i).get("placa") %>
										<% } %>
									</td>
									<td><%= DataUtils.dateToDateBr(listaRequerimentos.get(i).get("dataProcesso").toString()) %></td>
									<td><%= listaRequerimentos.get(i).get("nomRelator") %></td>
									<td>
										<% if(listaRequerimentos.get(i).get("resultado") == null){ %>
												S/R
										<% } else { %>
											<%= listaRequerimentos.get(i).get("resultado") %>
										<% } %>
									</td>
								</tr>
						<% } %>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="8" style="background-color: #87CEEB; height: 15px;"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		<div id="footer">
		</div>
		
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	</body>
</html>