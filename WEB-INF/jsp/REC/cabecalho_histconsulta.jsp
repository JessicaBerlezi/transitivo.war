<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!--CABE�ALHO IMPRESS�O-->
<link href="CssImpressao.jsp" rel="stylesheet" type="text/css">

<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr> 
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr><td width="25%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
	        </tr>
    	    <tr> <td width="38%" height="40" valign="top"><img src="images/<%=SistemaBeanId.getCodUF()%>/im_logodetran.gif"></td>
        	</tr>
	      </table>
     </td>        
     <td width="60%" align="center" valign="top" class="fontmaior"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
    	  	<STRONG style="font-size: 11px;">:: TRANSITIVO ::</strong> 
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
    	  <span class="fontmedia"><STRONG><%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></span>
     </td>
     <td width="15%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr>   <td height="20" align="right" valign="top">P�gina: <%= npag %> </td>
	        </tr>
	        <tr><td height="40" align="right" valign="top"><img src="images/im_logosmit.gif" width="98" height="30"></td>
    	    </tr>
	      </table>		  
     </td>
  </tr>        
</table> 
<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
</table>
<!--FIM Linha-->
<!--DIV QUE IDENTIFICA O AUTO -->

  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
      <tr height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA HIST�RICOS</b></td>        
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr height="18">
        <td width="119"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="119"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="217"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="257"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr height="14">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan=2><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
				&nbsp;(<%=AutoInfracaoBeanId.getDatStatus() %>)</td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	        
      <tr height="14">
		<td>&nbsp;&nbsp;<strong>Agente:&nbsp;</strong></td>
		<td><%=AutoInfracaoBeanId.getDscAgente() %></td>		
        <td><strong>Unidade:&nbsp;</strong><%=AutoInfracaoBeanId.getDscUnidade() %></td>
        <td><strong>Lote:&nbsp;</strong><%=AutoInfracaoBeanId.getDscLote() %></td>
      </tr>
      
      <tr><td colspan=4 height="2"></td></tr>	        
      <tr height="14">
		<td>&nbsp;&nbsp;<strong>Autua��o em:&nbsp;</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getDatNotifAut() %>&nbsp;-&nbsp;
			          <%=AutoInfracaoBeanId.getNomMotivoNotifAut() %>
        </td>
        <td><strong>AR:&nbsp;</strong><%=AutoInfracaoBeanId.getNumNotifAut() %>
      		&nbsp;&nbsp;(Lote:&nbsp;<%=AutoInfracaoBeanId.getNumARNotifAut() %>)		
      	</td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	        
      <tr height="14">
		<td>&nbsp;&nbsp;<strong>Penalidade em:&nbsp;</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getDatNotifPen() %>&nbsp;-&nbsp;
			          <%=AutoInfracaoBeanId.getNomMotivoNotifPen() %>
        </td>
        <td><strong>AR:&nbsp;</strong><%=AutoInfracaoBeanId.getNumNotifPen() %>
      		&nbsp;&nbsp;(Lote:&nbsp;<%=AutoInfracaoBeanId.getNumARNotifPen() %>)
      	</td>
      </tr>
            
      <tr><td colspan=4 height="2"></td></tr>	        
      <tr height="14">
		<td>&nbsp;&nbsp;<strong>Publica��o em:&nbsp;</strong></td>
        <td><%=AutoInfracaoBeanId.getDatEnvioDO() %></td>
        <td><strong>N&ordm:&nbsp;</strong><%=AutoInfracaoBeanId.getCodEnvioDO() %>
        </td>
        <td><strong>Publicado em:&nbsp;</strong><%=AutoInfracaoBeanId.getDatPublPDO() %>
	        &nbsp;-&nbsp;<strong><%=AutoInfracaoBeanId.getNomResultPubDO() %></strong>
        </td>
      </tr>      
      
  </table>  
  <!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
</table>
<!--FIM Linha-->  