<%@page import="REC.AutoInfracaoBean"%>
<%@page import="UTIL.TipoRecGeneric"%>

<jsp:useBean id="RequisicaoBeanId" scope="request" class="sys.RequisicaoBean" /> 
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	
<!DOCTYPE html>

<html lang="pt-br">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Abrir Processo</title>
		
		<link rel="stylesheet" href="css/transitivo.css" />
		
		<style>
			span {
				font: bold 10px verdana, arial;
			}
			th {
				font: bold 11px verdana, arial;
			}
			
			td {
				font: normal 11px verdana, arial;
				background-color: #FAEAE5;
			}
		</style>
	</head>
	<body>
		<div id="head">
			<div class="img-head2">
				<div class="img-head1">
					<div style="position: absolute; right: 6px;">
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/detran_help.png');"></div>
					</div>
				</div>
			</div>
			<div id="bread" class="bread">
				<strong class="text">ATA &gt; ABRIR PROCESSO</strong>
			</div>
		</div>
	
		<div id="body" class="body" style="height: 690px;">
			<div class="divForm" style="margin-right: 10px;">
				<table id="tableForm" style="width: 100%;">
					<tr>
						<td>
							<label for="placa">Placa:</label>
							<input id="placa" style="width: 80px; margin-right: 5px;" tabindex="1" autofocus="autofocus" />
							<label for="numeroAuto">N� do Auto:</label>
							<input id="numeroAuto" style="width: 120px; margin-right: 5px;" tabindex="2" />
							<label for="numeroProcesso">N� do Processo:</label>
							<input id="numeroProcesso" style="width: 120px; margin-right: 5px;" tabindex="3" />
						</td>
					</tr>
					<tr>
						<td>
							<label for="dataInfracaoInicial">Data da Infra��o de:</label>
							<input id="dataInfracaoInicial" style="width: 80px; margin-right: 5px;" />
							<label for="dataInfracaoFinal">at�:</label>
							<input id="dataInfracaoFinal" style="width: 80px;" />
						</td>
					</tr>
					<tr>
						<td>
							<button style="margin-top: 10px;" onclick="consultarAutos()">Consultar Processos</button>
						</td>
					</tr>
				</table>
	
				<div style="margin-top: 5px;">
					<span id="quantidadeAutos">Auto(s) Localizado(s): 0</span>
					<div id="divAutos" style="overflow: auto; height: 395px; border: solid 1px #862222;">
						
					</div>
				</div>
				
				<div id="divAbrirRequerimento" style="margin-top: 20px;">
					<table style="width: 100%;">
						<tr>
							<td colspan="4">
								<label for="dataEntrada">Data de Entrada:</label>
								<input id="dataEntrada" style="width: 80px;" />
								<label for="nomeRequerente">Nome do Requerente:</label>
								<input id="nomeRequerente" style="width: 300px;" />
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<label>N�mero do Auto: </label>
								<label id="numeroAutoSelecionado"></label>
							</td>
						</tr>						
						<tr style="background-color: #be7272; color: #fff;">
							<th style="width: 150px;">N� do Processo</th>
							<th style="width: 150px;">Tipo de Requerimento</th>
							<th style="width: 150px;">N� da Ata</th>
							<th style="text-align: left;">Nome do Relator</th>
						</tr>
						<tr>
							<td>
								<input id="numeroNovoProcesso" disabled="disabled" style="width: 150px;" />
							</td>
							<td>
								<select id="tipoRequerimento" style="width: 150px;" onchange="consultarAtas();">
									<option value="">SELECIONE</option>
									<option value="DP">Defesa Pr�via</option>
									<option value="1P">1� Inst�ncia</option>
									<option value="2P">2� Inst�ncia</option>
									<option value="PAE">P.A.E</option>
								</select>
							</td>
							<td>
								<select id="listaAtas" style="width: 150px;" onchange="consultarRelatores();">
									<option value="">SELECIONE</option>
								</select>
							</td>
							<td>
								<select id="listaRelatores" style="width: 300px;">
									<option value="">SELECIONE</option>
								</select>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="padding-top: 5px;">
								<button id="btnAbrirRequerimento" style="margin-top: 10px;" disabled="disabled" onclick="abrirRequerimento();">Abrir Requerimento</button>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div style="text-align: center; padding: 16px 0px; border: solid 1px red;">
									<span id="mensagem" style="font-size: 14px; color: red;"></span>
								</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		
		<div id="footer">
			<div class="img-footer2">
				<div class="img-footer"></div>
				<div class="img-footer-btn"></div>
			</div>
		</div>
	
		<script type="text/javascript" src="/Transitivo/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript">		
			var orgao = <%= ParamOrgBeanId.getCodOrgao() %>;
			var codNatureza = <%= session.getAttribute("codNatureza") %>;
			var listaAutos = [];
			var autoSelecionado = {};
			
			$(document).ready(function() {
				document.getElementById("dataEntrada").value = new Date().toLocaleDateString("pt-BR");
				document.getElementById("dataInfracaoInicial").value = "22/01/1998";
				document.getElementById("dataInfracaoFinal").value = new Date().toLocaleDateString("pt-BR");
			});

			function validarConsulta() {
				var placa = document.getElementById("placa").value;
				var numeroAuto = document.getElementById("numeroAuto").value;
				var numeroProcesso = document.getElementById("numeroProcesso").value;

				document.getElementById("mensagem").innerHTML = null;
				
				if(!placa && !numeroAuto && !numeroProcesso) {
					document.getElementById("mensagem")
						.innerHTML = "Para realizar uma consulta de processo, � necess�rio preencher pelomenos um dos campos: (placa/n�mero do auto/n�mero do processo).";

					return false;
				} /*else if(new Date(document.getElementById("dataInfracaoInicial").value) == "Invalid Date") {
					document.getElementById("mensagem").innerHTML = "A data inicial informada � inv�lida.";

					return false;
				} else if(new Date(document.getElementById("dataInfracaoFinal").value) == "Invalid Date") {
					document.getElementById("mensagem").innerHTML = "A data final informada � inv�lida.";

					return false;
				}*/

				return true;
			}

			function consultarAutos() {
				var placa = document.getElementById("placa").value;
				var numeroAuto = document.getElementById("numeroAuto").value;
				var numeroProcesso = document.getElementById("numeroProcesso").value;
				var dataInicial = document.getElementById("dataInfracaoInicial").value;
				var dataFinal = document.getElementById("dataInfracaoFinal").value;
				var tipoRec = <%= TipoRecGeneric.ABRIR_PROCESSO_CONSULTAR_AUTOS %>
				
				document.getElementById("divAutos").innerHTML = null;
				document.getElementById("numeroNovoProcesso").value = null;
				document.getElementById("numeroAutoSelecionado").innerHTML = null;
				document.getElementById("btnAbrirRequerimento").disabled = true;
				document.getElementById("quantidadeAutos").innerHTML = "Auto(s) Localizado(s): 0";
				document.getElementById("nomeRequerente").value = null;
				listaAutos = [];
				autoSelecionado = {};

				if(!validarConsulta())
					return;

				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						orgao: orgao,
						codNatureza: codNatureza,
						placa: placa,
						numeroAuto: numeroAuto,
						numeroProcesso: numeroProcesso,
						dataInicial: dataInicial,
						dataFinal: dataFinal,
						tipoRec: tipoRec
					},
					success: function(data) {
						montarLayoutAutos(data);
					},
					error: function() {
						alert("Erro no Servidor.");
					}
				});
			}

			function consultarAtas() {
				var tipoRec = <%= TipoRecGeneric.CONSULTAR_ATA %>
				var tipoAta = $("#tipoRequerimento option:selected").val();

				if(tipoAta == "DP")
					tipoAta = "Def. Previa";
				else if(tipoAta == "1P")
					tipoAta = "1� INST�NCIA";
				else if(tipoAta == "2P")
					tipoAta = "2� INST�NCIA";
				
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						codOrgao: orgao,
						cdNatureza: codNatureza,
						tipoAta: tipoAta,
						tipoRec: tipoRec
					},
					success: function(data) {
						preencherCboAtas(data);
					},
					error: function() {
						alert("Erro no Servidor.");
					}
				});
			}

			function consultarRelatores() {
				var tipoRec = <%= TipoRecGeneric.MONTAR_RELATOR %>;
				var cboTipoRequerimento = document.getElementById("tipoRequerimento");
				var tipoRequerimento = cboTipoRequerimento.options[cboTipoRequerimento.selectedIndex].value;
				var junta = 0;

				switch(tipoRequerimento) {
					case "DP":
						junta = 1;
						
						break;
					case "1P":
						junta = 2;
						
						break;
					case "2P":
						junta = 3;
						
						break;
					case "PAE":
						junta = 4;
						
						break;
				}

				$(document).ready(function() {
					$.ajax({
						url : "/Transitivo/ControladorRecGeneric",
						method : "post",
						data : {
							tipoRec: tipoRec,
							seletor: "idCpfAtaRelator",
							valor: junta
						},
						success : function(data) {
							document.getElementById("listaRelatores").innerHTML = data;
						},
						error : function() {
							alert("Erro no Servidor.");
						}
					});
				});
			}

			function enviarNovoRequerimento(novoRequerimento) {
				novoRequerimento.orgao = orgao;
				novoRequerimento.codNatureza = codNatureza;
				novoRequerimento.tipoRec = <%= TipoRecGeneric.ABRIR_PROCESSO_ABRIR_REQUERIMENTO %>
				
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: novoRequerimento,
					success: function() {
						document.getElementById("mensagem").innerHTML = "O Requerimento foi aberto com sucesso.";

						limparTela(false);
					},
					error: function(data) {
						document.getElementById("mensagem").innerHTML = data.responseText ? data.responseText : data.statusText;
					}
				});
			}

			function limparTela(limparMensagem) {
				document.getElementById("placa").value = null;
				document.getElementById("numeroAuto").value = null;
				document.getElementById("numeroProcesso").value = null;
				listaAutos = [];
				autoSelecionado = {};
				document.getElementById("quantidadeAutos").innerHTML = null;
				document.getElementById("quantidadeAutos").innerHTML = null;
				document.getElementById("divAutos").innerHTML = null;
				document.getElementById("numeroAutoSelecionado").innerHTML = null;
				document.getElementById("numeroNovoProcesso").value = null;
				document.getElementById("nomeRequerente").value = null;
				document.getElementById("numeroNovoProcesso").disabled = true;
				document.getElementById("tipoRequerimento").options[0].selected = true;
				document.getElementById("listaAtas").options[0].selected = true;
				document.getElementById("listaRelatores").options[0].selected = true;

				if(limparMensagem)
					document.getElementById("mensagem").innerHTML = null;
			}

			function abrirRequerimento() {
				var novoRequerimento = {
						numeroNovoProcesso: document.getElementById("numeroNovoProcesso").value,
						tipoRequerimento: $("#tipoRequerimento option:selected").val(),
						ata: $("#listaAtas option:selected").val(),
						relator: $("#listaRelatores option:selected").val(),
						codAuto: autoSelecionado[0].codAuto,
						dataRequerimento: document.getElementById("dataEntrada").value,
						nomeRequerente: document.getElementById("nomeRequerente").value
				};

				if(!validarNovoRequerimento(novoRequerimento))
					return;

				enviarNovoRequerimento(novoRequerimento);
			}

			function validarNovoRequerimento(novoRequerimento) {
				document.getElementById("mensagem").innerHTML = null;
				
				if(!novoRequerimento.numeroNovoProcesso) {
					document.getElementById("mensagem").innerHTML = "O n�mero do requerimento � obrigat�rio.";
					
					return false;
				} else if(!novoRequerimento.tipoRequerimento || tipoRequerimento.tipoRequerimento == "SELECIONE") {
					document.getElementById("mensagem").innerHTML = "O tipo de requerimento � obrigat�rio.";
					
					return false;
				} else if(!novoRequerimento.nomeRequerente) {
					document.getElementById("mensagem").innerHTML = "O nome do requerente � obrigat�rio.";
					
					return false;
				} else if(autoSelecionado.codSituacao == 90 || autoSelecionado.codSituacao == 96 || autoSelecionado.codSituacao == 97 ||
				autoSelecionado.codSituacao == 98 || autoSelecionado.codSituacao == 99 || autoSelecionado.codSituacao == 9000 ||
				autoSelecionado.codSituacao == 9001 || autoSelecionado.codSituacao == 9002 || autoSelecionado.codSituacao == 9003 ||
				autoSelecionado.codSituacao == 9004) {
					document.getElementById("mensagem").innerHTML = "N�o � poss�vel abrir um processo em um auto com este status.";
					
					return false;
				}

				return true;
			}

			function montarLayoutAutos(data) {				
				listaAutos = JSON.parse(data);

				if(listaAutos.length <= 0)
					return;

				document.getElementById("quantidadeAutos").innerHTML = "Auto(s) Localizado(s): " + listaAutos.length;

				for(var i = 0; i < listaAutos.length; i++) {
					var item = ItemAuto(i + 1, listaAutos[i]);
					
					document.getElementById("divAutos").innerHTML += item;
				}

				if(listaAutos.length == 1) {
					document.getElementById(listaAutos[0].codAuto).checked = true;
					autoChange(listaAutos[0].codAuto);
				}
			}

			function preencherCboAtas(data) {
				var cboAtas = document.getElementById("listaAtas");

				if(!cboAtas)
					return;

				var options = "<option value=''>SELECIONE</option>";

				for(var i = 0; i < data.length; i++) {
					options += "<option value='" + data[i].codAta + "'>" + data[i].desAta + "</option>";
				}

				cboAtas.innerHTML = options;
			}

			function autoChange(codAuto) {
				var codNatureza = <%= session.getAttribute("codNatureza") %>
				
				autoSelecionado = listaAutos.filter(function(e) {
					if(e.codAuto == codAuto)
						return e;
				});

				if(!autoSelecionado[0])
					return;

				document.getElementById("nomeRequerente").value =
						autoSelecionado[0].proprietario ? autoSelecionado[0].proprietario :
						autoSelecionado[0].respPontos ? autoSelecionado[0].respPontos : null;

				document.getElementById("numeroAutoSelecionado").innerHTML = autoSelecionado[0].numeroAuto;
				
				if(!autoSelecionado[0].numeroProcesso || autoSelecionado[0].numeroProcesso == "N/A") {
					if(codNatureza == "1")
						autoSelecionado[0].numeroProcessoSujerido = null;
					
					document.getElementById("numeroNovoProcesso").disabled = false;					
					document.getElementById("numeroNovoProcesso").value = autoSelecionado[0].numeroProcessoSujerido;
				}
				else {
					document.getElementById("numeroNovoProcesso").disabled = true;
					document.getElementById("numeroNovoProcesso").value = autoSelecionado[0].numeroProcesso;
				}

				document.getElementById("btnAbrirRequerimento").disabled = false;
			}

			function Auto(data) {
				return {
					codAuto: data.codAuto,
					sigOrgao: data.sigOrgao,
					numeroAuto: data.numeroAuto,
					placa: data.placa,
					descricaoAta: data.descricaoAta,
					numeroProcesso: data.numeroProcesso,
					situacao: data.situacao,
					dataAuto: data.dataAuto,
					infracao: data.infracao,
					vencimento: data.vencimento,
					local: data.local,
					valor: data.valor,
					enquadramento: data.enquadramento,
					proprietario: data.proprietario,
					respPontos: data.respPontos,
					dataNotificacao: data.dataNotificacao
				};
			}

			function ItemAuto(index, data) {
				var novoAuto = Auto(data);
				var htmlTableAuto = "<table style='width: 100%; margin-bottom: 10px;'>";
				htmlTableAuto += "<tr style='background-color: #be7272; color: #fff;'>";
				htmlTableAuto += "<th style='width: 20px;'></th>";
				htmlTableAuto += "<th style='width: 80px;'>Org�o</th>";
				htmlTableAuto += "<th style='width: 140px;'>N�. Auto</th>";
				htmlTableAuto += "<th style='width: 140px;'>Placa</th>";
				htmlTableAuto += "<th>Processo</th>";
				htmlTableAuto += "<th style='width: 140px;'>Data da Notifica��o</th>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td rowspan='6'>";
				htmlTableAuto += "<input type='radio' id='" + data.codAuto + "' name='rdAuto' onchange='autoChange(this.id);' />";
				htmlTableAuto += "</td>";
				htmlTableAuto += "<td rowspan='2' style='text-align: center;'>" + index + "</td>";
				htmlTableAuto += "<td rowspan='2' style='font-weight: bold; text-align: center;'>"+novoAuto.numeroAuto+"</td>";
				htmlTableAuto += "<td rowspan='2' style='font-weight: bold; text-align: center;'>"+novoAuto.placa+"</td>";
				htmlTableAuto += "<td>Ata: "+novoAuto.descricaoAta+" - N� do Processo: "+novoAuto.numeroProcesso+"</td>";
				htmlTableAuto += "<td rowspan='2' style='text-align: center;'>"+novoAuto.dataNotificacao+"</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td>Situa��o: " + novoAuto.situacao + "</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td rowspan='2' style='text-align: center;'>"+novoAuto.sigOrgao+"</td>";
				htmlTableAuto += "<td colspan='2'>Data: "+novoAuto.dataAuto+"</td>";
				htmlTableAuto += "<td colspan='2'>Infra��o: "+novoAuto.infracao+"</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td colspan='2'>Vencimento: "+novoAuto.vencimento+"</td>";
				htmlTableAuto += "<td colspan='2'>Local: "+novoAuto.local+"</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td rowspan='2'></td>";
				htmlTableAuto += "<td colspan='2'>Valor: "+novoAuto.valor+"</td>";
				htmlTableAuto += "<td colspan='2'>Enquadramento: "+novoAuto.enquadramento+"</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "<tr>";
				htmlTableAuto += "<td colspan='2'>Propriet�rio: "+novoAuto.proprietario+"</td>";
				htmlTableAuto += "<td colspan='2'>Resp. Pontos: " + (novoAuto.respPontos ? novoAuto.respPontos : "Condutor n�o Identificado") + "</td>";
				htmlTableAuto += "</tr>";
				htmlTableAuto += "</table>";

				return htmlTableAuto;
			}
			
		</script>
	</body>
</html>