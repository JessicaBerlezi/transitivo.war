<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!--Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Instrucoes -->
<jsp:useBean id="InstrId" scope="request" class="REC.InstrNotBean" /> 
 
<html>
<head>    
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

myNotificacao      = new Array(<%= InstrId.getNotificacoes(  UsuarioBeanId ) %>);
cod_Notificacao = new Array(myNotificacao.length/2);
dsc_Notificacao = new Array(myNotificacao.length/2);
for (n=0; n<myNotificacao.length/2; n++) {
	cod_Notificacao[n] = myNotificacao[n*2+0] ;
	dsc_Notificacao[n] = myNotificacao[n*2+1] ;
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaInstrucao':
   		if (fForm.codNotificacao.value==0){
			alert("Notifica��o n�o selecionada. \n")
		}
		else {
		 if (fForm.codOrgao.value==0){ 
		   alert("Org�o n�o selecionado. \n")
		   }
		
			else {
			indice = fForm.codNotificacao.selectedIndex;
	        fForm.dscNot.value = fForm.codNotificacao.options[indice].text;
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		
		}
		}
	  break ; 
   case 'A':
   	  if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else alert(sErro)
	  break ;	  
	case 'I':
	fForm.acao.value=opcao;
	fForm.target= "_blank";
	fForm.action = "acessoTool";  
	fForm.submit();	 
	break;	  
	  case 'C':
	    
     document.all["escolha"].style.visibility='visible';
    
     break;
     
     case 'Pergunta':
    
     document.all["tabbotoes"].style.visibility='hidden';
     document.all["pergunta"].style.visibility='visible';     
     
     break;
     
     case 'Sim':
     
     fForm.sobrescreve.value='true';
     document.all["pergunta"].style.visibility='hidden';
     valida('C',fForm);
     break;
     
     case 'Nao':
     
     fForm.sobrescreve.value='false';
     document.all["pergunta"].style.visibility='hidden';
     valida('C',fForm);
     break;
     
  case 'Copiar':
    if (fForm.codOrgaoCp.value==0) {
		alert("�rg�o de destino n�o selecionado. \n")
	 }
	 if(fForm.codOrgao.value == fForm.codOrgaoCp.value){
	    alert( "�rg�o de destino deve ser diferente do �rg�o selecionado. \n")
	 }
	 else{
	   fForm.acao.value=opcao
	   fForm.target= "_self";
	   fForm.action = "acessoTool";  
	   fForm.submit();	  				  
	 }
  	 break ;	  
   case 'R':
  
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	;  
		fForm.acao.value=opcao;
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	     close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codNotificacao.value==0) {
   valid = false
   sErro = sErro + "Notificac�o n�o Selecionada"
}
// fim de validar Input Descricao
for (k=0;k<fForm.txtLinhaNot.length;k++) {
   var ne = trim(fForm.txtLinhaNot[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.txtLinhaNot.length;m++) {
	   var dup = trim(fForm.txtLinhaNot[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Parametro em duplicata !!!: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
 
function carregaDescricao(fForm)
{
	with(fForm){
	indice = codNotificacao.selectedIndex;
	dscNot.value = codNotificacao.options[indice].text;

	}
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="instrucaoForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="sobrescreve" type="hidden" value=''>			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 61px; left=35px;height: 111px; left: 50px;" > 
  <table border="0" cellpadding="0" cellspacing="1" width=720  align="center">    
      <tr> 
        <td valign="top" colspan="3"><img src="images/inv.gif" width="3" height="15"></td>
      </tr>
	  <tr>
	  <td><b>Org&atilde;o:</b>&nbsp;<input disabled name="sigOrgao" type="text" size="30" value="<%= InstrId.getOrgao().getSigOrgao() %>" style= "border: 0px none;" ></td>
	  <td><b>Notifica��o:</b>&nbsp;<input disabled name="dscNotificacao" type="text" size="70" value= "<%= InstrId.getDscNotificacao () %>" style= "border: 0px none;" ></td>
	  <td></td>
	  <td></td>
	  </tr>
      <tr>
		  <td width="40%">
		   
   			<% if (InstrId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codOrgao"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_orgao.length;j++) {
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
			<% }
			else { %>
		        <input name="codOrgao" type="hidden" size="10" maxlength="20"  value="<%= InstrId.getCodOrgao() %>"> 
		        
			<% } %>	    </td>
   		  <td width="46%">
   		    <input name="atualizarDependente" type="hidden" size="1" value="<%= InstrId.getAtualizarDependente() %>"> 
   			<% if (InstrId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codNotificacao" onChange="carregaDescricao(this.form)"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (t=0;t<cod_Notificacao.length;t++) {
						document.write ("<option value="+cod_Notificacao[t]+">"+dsc_Notificacao[t])+"</option>"
					}
				</script>
				</select>
			<% }
			else { %>
		        <input name="codNotificacao" type="hidden" size="10" maxlength="20"  value="<%= InstrId.getCodNotificacao() %>" >
		        
		  <% } %>	 </td>
   		  <% if (InstrId.getAtualizarDependente().equals("S")==false){ %>			  
 	         <td align=left width=7%>
			  	<button type="button" NAME="Ok" style="width: 28px; height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaInstrucao',this.form);">	
	    	    <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left" ></button>
			 </td>
	       	 
		<% } %>
      </tr>	
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES--> 
	<td><tr height="2">
	</tr></td> 
  <TABLE id = "tabbotoes" border="0" cellpadding="0" cellspacing="7" width="300" align="center">
    <TR>
		<% if (InstrId.getAtualizarDependente().equals("S")){ %>			
	 	   <td width=100 align="center"> 
                  <button type=button NAME="Atualizar" align="center" style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align=left  ></button>
     	   </td>
	       <td  width=100 align="center"> 
                  <button type=button NAME="Copiar"  style="height: 21px; width: 44px; border: none; background: transparent;" onClick="javascript: valida('Pergunta',this.form);">	
                  <IMG src="<%= path %>/images/bot_copiar.gif" width="44" height="21"></button>
    	   </td>
    	   <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td width=100 align="center"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align=left  ></button>
           </td>
		<%} %>
    </TR>

  </TABLE>  
<!--FIM BOTOES-->  
</div>
  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 141px; height: 32px;"> 
    <table width="720" border="0"  align="center" cellpadding="0" cellspacing="1">  	   		
	<tr bgcolor="#993300"> 
        <td width="80"  align="center" bgcolor="#be7272"><font color="#ffffff"><b>Linha</b></font></TD>		 	  
        <td width="80" align="center" bgcolor="#be7272"><font color="#ffffff"><b>Tipo</b></font></TD>		 
        <td width="556" align="center" bgcolor="#be7272"><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Descri��o</b></font></TD>		 
      </tr>
   </table>
<!--FIM CABEC DA TABELA-->
</div>

  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 157px; height: 191px;"> 
    <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
      <% 
					if (InstrId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = InstrId.getParametros().size() ;
					   if (ocor<=0) ocor=InstrId.getParametros(10,0).size() ;
					   
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="#faeae5">
		<td width="80" align="center"> 
          <input name="numLinhaNot"          type="text" readonly size="3" value="<%= i+1 %>">
	    <input name="codInstrucaoNot" type="hidden"  value="<%= InstrId.getParametros(i).getCodInstrucaoNot() %>">		</td>
        <td width="80" align="center"> 
          <input name="codTipoLinhaNot" type="text" size="3" maxlength="1"  value="<%= InstrId.getParametros(i).getCodTipoLinhaNot() %>"  onfocus="javascript:this.select() ;" onKeyUp="this.value=this.value.toUpperCase();" >		</td>
        <td align="center"> 
          <input name="txtLinhaNot" type="text" size="98" maxlength="170"  value="<%= InstrId.getParametros(i).getTxtLinhaNot() %>" onfocus="javascript:this.select();" >		</td>
      </tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#f5d6cc">
		<td width="80" align="center" bgcolor="#FAEAE5"> 
          <input name="numLinhaNot"  readonly  type="text" size="3" value="<%= i+1 %>">
        <input name="codInstrucaoNot"    type="hidden"  value=""></td>
	   	<td width="80" align="center" bgcolor="#FAEAE5" > 
        <input name="codTipoLinhaNot" readonly type="text" size="3" maxlength="1"  value=""></td>			
	   	<td align="center"> 
          <input name="txtLinhaNot" readonly type="text" size="98" maxlength="170"  value="">        </td>			
    	
	  </tr>
					   
<%						}					
					}   %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!-- Div para perguntar se sobrescreve ou n�o as linhas existentes -->
<div id = "pergunta"  style="position:absolute; top: 107px; width: 361px; height: 71px; left: 280px; overflow: visible; z-index: 15; visibility: hidden;" > 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	 	<tr>
	 		<td width="50%" align="center"><strong>Substitui as linhas existentes ?</strong>&nbsp; 
         
      		</td>
	   		<td width="10%" align="left">
	     		<button type="button" NAME="Ok" style="width: 33px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('Sim',this.form);">	
        			<IMG src="<%= path %>/images/bot_sim.gif" width="31" height="19" align="left">
				</button>
       		</td>
       		<td width="40%" align="left">
	     		<button type="button" NAME="Ok" style="width: 33px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('Nao',this.form);">	
        			<IMG src="<%= path %>/images/bot_nao.gif" width="31" height="19" align="left">
				</button>
       		</td>
       		
   	  	</tr>
	</table>
</div>


<!-- Combo para escoha de orga para copia de parametros -->
<div id = "escolha"  style="position:absolute; top: 107px; width: 361px; height: 71px; left: 269px; overflow: visible; z-index: 15; visibility: hidden;" > 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr><td width="70%" align="center"><b>&Oacute;rg&atilde;o&nbsp; de Destino:</b>&nbsp; 
         <select name="codOrgaoCp">
           <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
           <script>
	         for (j=0;j<cod_orgao.length;j++) {
			 	if (instrucaoForm.codOrgao.value!=cod_orgao[j]) {
		    	  document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
				  }
	         }
	     </script>
         </select>
      </td>
	   <td width="30%">
	     <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('Copiar',this.form);">	
        	<IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left">
		 </button>
       </td>
   	  </tr>
	</table>
  </div>


  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= InstrId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%= InstrId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.instrucaoForm.codOrgao.length;i++){
    if(document.instrucaoForm.codOrgao.options[i].value==njint){
       document.instrucaoForm.codOrgao.selectedIndex=i;	  
    }
  }
}  

nj = '<%= InstrId.getCodNotificacao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.instrucaoForm.codNotificacao.length;i++){
    if(document.instrucaoForm.codNotificacao.options[i].value==njint){
       document.instrucaoForm.codNotificacao.selectedIndex=i;	  
    }
  }
}  



</script>
<input name="dscNot" type="hidden" size="10" maxlength="20"  value= ""> 
</form>
</BODY>
</HTML>


