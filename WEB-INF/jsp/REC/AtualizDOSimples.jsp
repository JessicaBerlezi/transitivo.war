<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="InformarResultId"   scope="request" class="REC.InformarResultBean" /> 
<%			InformarResultId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'LeReq':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   		
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	   
		   	}
			break;
	   case 'AtualizDO':
	   		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   		
			    if (veCampos(fForm)==true) 
			    {		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
				} 		  
			}
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AtaulizDOForm.datEnvioDO,1) && valid
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AtaulizDOForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp"%>

<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp"%>   
<div style="position:absolute; left:66px; top:269px; width:686px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:66px; top:275px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
<% if ("S".equals(InformarResultId.getEventoOK())) { %>			 	 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
	    <td valign="middle" >&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
        <td align="left" valign="middle" colspan=2> 
			<jsp:getProperty name="InformarResultId" property="reqs" />
	    </td>
	    <td ></td>
	    <td></td>		
     </tr>
     <tr><td colspan=4 height=2></td></tr>
     <tr bgcolor="#faeae5">
        <td width="10%"></td>
        <td width="40%">&nbsp;&nbsp;Data de Envio:
          &nbsp;&nbsp; 
          <input disabled type="text" name="datEnvioDO" size="12" maxlength="10" value='<%=RequerimentoId.getDatEnvioDO() %>' 
           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
		</td>
        <td  width="43%">
	  	  &nbsp;N�mero da ATA:&nbsp;
		 <input disabled type="text" name="codEnvioDO" size="26" maxlength="20" value="<%= RequerimentoId.getCodEnvioDO() %>" class="input" 
           onkeypress="javascript:f_end();" onFocus="javascript:this.select();">	  
		</td>		
		<td width="7%"></td>
     </tr>
     <tr><td colspan=4 height=3></td></tr>     
     <tr bgcolor="#faeae5">
        <td></td>
        <td>
	  		&nbsp;Resultado:&nbsp; 
            <input type="text" disabled name="codResultPubDO" size="12" maxlength="10" 
          	value='<%= RequerimentoId.getNomResultRS() %>'>
		</td>
        <td>
          &nbsp;Data de Atualiza��o:&nbsp; 
          <input type="text" name="datAtualizDO" size="12" maxlength="10" value='<%=RequerimentoId.getDatPublPDO() %>' 
           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">        
        </td>		
		<td>
          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  
          	onClick="javascript: valida('AtualizDO',this.form);">	
   	  	    <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
       	  </button>
		</td>
     </tr>
  </table>

<% } else { %>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
    	 <td height=23> 
			<jsp:getProperty name="InformarResultId" property="msg" />
	     </td>
      </tr>	  
  </table>								
<% } %>  
</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>

<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->

<!--FIM_Div Erros-->
</form>
</body>
</html>