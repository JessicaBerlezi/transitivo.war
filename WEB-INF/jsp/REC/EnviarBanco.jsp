<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
  
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="EnviarBancoId" scope="request" class="REC.EnviarBancoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'EnviarBanco':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(EnviarBancoForm.datEnvioDO,1) && valid
	if (atuRadio(fForm.indMovimentoradio,fForm.indMovimento)) {
	  valid = false
	  sErro = sErro + "Tipo de Movimenta��o n�o selecionado. \n"
	}
	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="EnviarBancoForm" method="post" action="">
<%@include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>

<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%if (((String)request.getAttribute("verBanco")).equals("S")){%> 
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr><td height=7 colspan=3></td></tr>	
    <tr bgcolor="#faeae5"  height="15"> 
      <td colspan=3 class="espaco2">&nbsp;<b><%= EnviarBancoId.getMsg() %></b>&nbsp;</td>
    </tr>
    <tr><td height=7 colspan=3></td></tr>	
    <tr bgcolor="#faeae5"> 
	  <td width="25%" height="15"> 
          &nbsp;Data de Envio:&nbsp; 
          <input type="text" name="datEnvioDO" disabled size="12" maxlength="10" value='<%=EnviarBancoId.getDatEnvioBanco() %>' 
           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
	  </td>
	  <td width="68%">
   	      <input type="radio" name="indMovimentoradio" value="1" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Incluir no Banco&nbsp;&nbsp;&nbsp;
          <input type="radio" name="indMovimentoradio" value="2" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Alterar no Banco&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
       	  <input type="radio" name="indMovimentoradio" value="3" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Excluir no Banco&nbsp;&nbsp;				  		
		  <input name="indMovimento" type="hidden" value="">				
	  </td>
      <td width="7%" align="center">
      </td>
    </tr>
    <tr><td height=7 colspan=3></td></tr>	
    <tr bgcolor="#faeae5"> 
	  <td  height="15">  </td>
	  <td width="68%">
   	      <input type="radio" name="indMovimentoradio" value="4" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"4") %> class="sem-borda" style="height: 14px; width: 14px;">Incluir Suspens�o
          <input type="radio" name="indMovimentoradio" value="5" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"5") %> class="sem-borda" style="height: 14px; width: 14px;">Excluir Suspens�o&nbsp;&nbsp;
       	  <input type="radio" name="indMovimentoradio" value="6" <%= sys.Util.isChecked(EnviarBancoId.getIndMovimento(),"6") %> class="sem-borda" style="height: 14px; width: 14px;">Excluir por Pgto em outro Banco&nbsp;&nbsp;				  		
	  </td>
      <td width="7%" align="center">
  		<% if ("S".equals(EnviarBancoId.getEventoOK())) { %>		        
          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  
          	onClick="javascript: valida('EnviarBanco',this.form);">	
   	  	    <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
       	  </button>
		<% } %>        
      </td>
    </tr>    
  </table>
</div>
<%}%>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%if (AutoInfracaoBeanId.getNumAutoInfracao().length()<=0 ){%>	
   	<script>document.EnviarBancoForm.numAutoInfracao.focus();</script>	        
<%}%>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>

<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->

<%
String msgErro= AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk() ;
String msgErroTop  = "160 px" ;
String msgErroLeft = "80 px" ;
%>
<%@ include file="EventoErro_Diretiva.jsp" %>
 
<!--FIM_Div Erros-->
</form>
</body>
</html>