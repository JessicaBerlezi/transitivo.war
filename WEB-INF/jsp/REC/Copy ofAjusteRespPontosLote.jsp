<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="consultaAutoId" scope="request" class="REC.consultaAutoBean" />

<jsp:useBean id="RequerimentoId" scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="UFCNHId" scope="request" class="sys.UFBean" />
<jsp:useBean id="UFId" scope="request" class="sys.UFBean" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<!--M�dulo : REC
	Vers�o : 2.0
	Atualiza��es: 11/03/2008
-->	
<title>:: TRANSITIVO ::</title>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'ConfirmaAjuste':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AjusteManualForm.dataReq,1) && valid

	if (valid==false) alert(sErro) 
	return valid ;
}


function veCPF(obj,forma) {
	  <!-- editar a celula -->
	  valor = trim(obj.value)
	  
	  if (valor*1==0)
	  {
	    alert("CPF/CNPJ invalido.");
		obj.value="";
	    document.AjusteManualForm.numCPF.focus();
	    return false;
	  }
	  

	  if (valor.length != 0) {     
	    if(valor.length <= 11){  
	      <!-- validar se cpf se preenchido-->
	      if (dig_cpf(valor) == false) { 
		 	 if (forma==0) {
			     alert("CPF com digito verificador invalido.");
			     obj.value="";
	             document.AjusteManualForm.numCPF.focus();
		 	 }
			 else {	
			  	sErro = sErro + "CPF com digito verificador invalido/n";
			  	obj.value="";	
			    document.AjusteManualForm.numCPF.focus(); 
			 }
	         return false ;  	 
		  }
	      document.AjusteManualForm.cpfhidden.value=valor;	  
		  obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11);
		}
		if(valor.length == 14 && valor.substring(0,4)!="."){
		    if (dig_cgc(valor) == false) {
	         alert("CNPJ com digito verificador invalido. \n");
			 valid = false ;
	         obj.value="";
	         document.all["numCPF"].focus();
	         return false;
		    }
		    else{
		    document.AjusteManualForm.cpfhidden.value=valor;
		    obj.value = valor.substring(0,2)+"."+valor.substring(2,5)+"."+valor.substring(5,8)+"/"+valor.substring(8,12)+"-"+valor.substring(12,14);
		    }
		}
	  }
	}

	function veCPF2(obj,forma) {
	  <!-- editar a celula -->
	  valor = trim(obj.value)

	  if (valor.length != 0) {     
	    if(valor.length <= 11){  
	      <!-- validar se cpf se preenchido-->
	      if (dig_cpf(valor) == false) { 
		 	 if (forma==0) {
			     alert("CPF com digito verificador invalido.");
			     obj.value="";
	             document.AjusteManualForm.numCPF.focus();
		 	 }
			 else {	
			  	sErro = sErro + "CPF com digito verificador invalido/n";
			  	obj.value="";	
			    document.AjusteManualForm.numCPF.focus(); 
			 }
	         return false ;  	 
		  }
		  document.AjusteManualForm.cpfpnthidden.value=valor;	  
		  obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11);
	   }

	 }
	}

	function dig_cgc(cgcinf) {
	  w = Tiraedt(cgcinf,15); 
	  
	  if (w.length < 15) { return false }   
	  var dvcgc = w.substring(0,13);  
	  var s1 = 0;
	  for (i=0; i<5  ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(6-i)) }
	  for (i=5; i<13 ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(14-i)) }  
	  r1 = s1 % 11
	  if (r1<2) dv1 = 0
	  else dv1 = 11 - r1  
	  var s2 = dv1 * 2
	  for (i = 0; i < 6 ; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(7-i)) }
	  for (i = 6; i < 13; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(15-i)) }
	  r2 = s2 % 11
	  if (r2<2) dv2 = 0
	  else dv2 = 11 - r2
	  var DV = "" + dv1 + dv2
	  var oldDV = w.substring(13,15)
	  return (oldDV == DV) ;
	}

	function dig_cpf(cpfinf) {
	  w = Tiraedt(cpfinf,11); 
	  if (w.length < 11) { return false }   
	  var dvcpf = w.substring(0,9);    
	  var s1 = 0;
	  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
	  r1 = s1 % 11
	  if (r1<2) dv1 = 0
	  else dv1 = 11 - r1  
	  var s2 = dv1*2 
	  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
	  r2 = s2 % 11
	  if (r2<2) dv2 = 0
	  else dv2 = 11 - r2
	  var DV = "" + dv1 + dv2
	  var oldDV = w.substring(9,11)
	  return (oldDV == DV) ;  
	}






</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjusteManualForm" method="post" action="">
 <%String cpf = AutoInfracaoBeanId.getProprietario().getNumCpfCnpj(),c_cpf="";    					
  c_cpf=cpf;
  if (cpf.length()>0)
  {
    cpf = cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11);
  }
  
  
  String cpf2 = AutoInfracaoBeanId.getCondutor().getNumCpfCnpj(),c_cpf2="";
  c_cpf2=cpf2;
  if (cpf2.length()==14)
  {
    cpf2 = cpf2.substring(3,14);
    c_cpf2=cpf2;    
    cpf2 = cpf2.substring(0,3)+"."+cpf2.substring(3,6)+"."+cpf2.substring(6,9)+"-"+cpf2.substring(9,11);	  	     	 
  }else if(cpf2.length()== 11) cpf2 = cpf2.substring(0,3)+"."+cpf2.substring(3,6)+"."+cpf2.substring(6,9)+"-"+cpf2.substring(9,11);
  %>


<%@ include file="Cab_Diretiva.jsp" %>

<%
   String isDisabled = "";   
   UFCNHId.setChecked(RequerimentoId.getCodUfCNHTR());
   UFId.setChecked(AutoInfracaoBeanId.getCondutor().getEndereco().getCodUF());
   if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")){isDisabled += "disabled";}
   
%>  
<!-- UF do CNH --> 
<jsp:setProperty name="UFCNHId" property="j_abrevSist" value="REC" />  	 	     	 	  
<jsp:setProperty name="UFCNHId" property="popupExt" value="<%=isDisabled%>"  />
<jsp:setProperty name="UFCNHId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFCNHId" property="popupNome"   value="codUfCNHTR"  />  
<jsp:setProperty name="UFCNHId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 

<!-- UF do endereco -->
<jsp:setProperty name="UFId" property="j_abrevSist" value="REC" /> 
<jsp:setProperty name="UFId" property="popupExt" value="<%=isDisabled%>"  /> 	 	     	 	  
<jsp:setProperty name="UFId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFId" property="popupNome"   value="codUf"  />  
<jsp:setProperty name="UFId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 



<input name="acao"     type="hidden" value=' '>
<input name="verTabela" type="hidden" value=''>
<input name="cpfhidden" type="hidden" value='<%=c_cpf%>'>
<input name="cpfpnthidden" type="hidden" value='<%=c_cpf2%>'>




<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_DiretivaPts.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>  
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:276px; height:1px; z-index:800; overflow: visible; visibility: visible;">
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>





<!-- ========================1� bloco========================================== -->

      <table border="0" cellspacing="1" cellpadding="0" width="100%">
      <tr> 
        <td width="60" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Orgao',this.form);" class="branco">�rg�o</a></td>
		<td width="100" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Auto',this.form);" class="branco">N&ordm; Auto</a></td>
        <td width="100" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Placa',this.form);" class="branco">Placa</a></td>
        <td width="350" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Processo',this.form);" class="branco">Processo</a></td>
        <td style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;Dt Notif.</td>
      </tr>
	</table>

   

	

<div class="divCorpo" id="conteudo" style="height:100%;">

    <table width="100%" border="0" cellpadding="0" cellspacing="1" id="autos">
	  <%
	   if (consultaAutoId.getAutos().size()>0) {
		String resultline = "#faeae5";		   
	  	int a =0;
	  	int seq = 0;
		String cor   = (a%2==0) ? "#faeae5" : "#ffffff";
		String fonte="";		
		Iterator it = consultaAutoId.getAutos().iterator() ;
		REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
		    seq++;
			not   = (REC.AutoInfracaoBean)it.next() ;				
			cor   = (a%2==0) ?  resultline  : "#ffffff";
			fonte = (a%2==0) ? "#ffffff" : "#000000";  
		%>


		
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; " name="line<%=a%>" id="line<%=a%>"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');">
			
			
			 
	        <td width="60" rowspan="2" height="14" align="center">&nbsp;<%=seq %> <input type="checkbox" name="Selecionado" value="<%= seq %>" class="input" <%= sys.Util.isChecked(not.getEventoOK(),"1")%> 
			        	onclick="javascript:SomaSelec();"> </td>
			        	
			        	
			<td width="100" rowspan="2">&nbsp;<strong><%=not.getNumAutoInfracao().trim()%></strong></td>
        	<td width="100" rowspan="2">&nbsp;<strong><%=not.getNumPlaca().trim()%></strong></td>
	        <td width="350">&nbsp;<%= not.getNumProcesso().trim()%></td>
	        
	        <input type="hidden" name="SelecionadoPlaca"     value='<%=not.getNumPlaca().trim()%>'> 
	        <input type="hidden" name="SelecionadoAuto"      value='<%=not.getNumAutoInfracao().trim()%>'>    
	        <input type="hidden" name="SelecionadoProcesso"  value='<%=not.getNumProcesso().trim()%>'>
	        
	        
	        
			<td>&nbsp;
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	<%= not.getDatNotificacao().trim()%>
			<% } %>
			
			
			
			
				        
        	</td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');">
			
    	    <td colspan="2">
    	    	&nbsp;Situa��o:&nbsp;<%= not.getSituacao().trim()%>
				<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= not.getNomStatus().trim()%>
				<% } %>
			<%
			 String desc = "";
		    if (not.getIdentAutoOrigem().length()>0 && !not.getIdentAutoOrigem().equals("0") ) {
		    	%>
        	<br>&nbsp;AUTO RENAINF - &nbsp;<%=not.getDescIdentAutoOrigem()%>&nbsp;
	        <%}%>	
	        <% if (not.getNumAutoOrigem().length()>0) { %>	
		          <br>&nbsp;Ft. Gerador:&nbsp;<%=not.getNumAutoOrigem()%>&nbsp;(<%=not.getNumInfracaoOrigem() %>)
			<% } %>		        	        
	        </td>
		</tr>
		
		
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"> 
		
		
		
	        <td height="14" rowspan="2" align="center">&nbsp;
	        <%=not.getSigOrgao() %></td>
	        
	        
	            	    
        	<td colspan="2">
        		<a href="#" onClick="javascript: Classificacao('Data',this.form);">
        			&nbsp;Data:&nbsp;<%= not.getDatInfracao()%>&nbsp;&nbsp;<%=not.getValHorInfracaoEdt() %>
        		</a>
        	</td>
	        <td height="14" colspan="2">&nbsp;Infra��o:&nbsp;<%=not.getInfracao().getCodInfracao().trim() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscInfracao().trim() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');"> 
        	<td colspan="2">&nbsp;Vencimento:&nbsp;<%= not.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan="2">&nbsp;Local:&nbsp;<%=not.getDscLocalInfracao().trim()%>&nbsp;&nbsp;<%= not.getNomMunicipio().trim() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; ">
         <%
        if ( (not.getIdentOrgaoEdt().length()>0) || (not.getNumInfracaoRenainf().length()>0) ){ %>
	        <td height="14" rowspan="3" align="center">&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>    	    	<%}else{%>
	        <td height="14" rowspan="2" align="center">&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>     	    	<%}%>
            
        	<td colspan="2">&nbsp;Valor:&nbsp;<%= not.getInfracao().getValRealEdt().trim()%>&nbsp;</td>											
	        <td height="14" colspan="2">
	          	<a href="#" onClick="javascript: Classificacao('Enquadramento',this.form);">
		        	&nbsp;Enquadramento:&nbsp;<%=not.getInfracao().getDscEnquadramento().trim() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscGravidade().trim() %>&nbsp;-&nbsp;<span style="color: #ff0000"><strong>Pontos:&nbsp;<%= not.getInfracao().getNumPonto().trim() %></strong></span></a></td>
	        	
		</tr>
		

		<%
        if ( (not.getIdentOrgaoEdt().length()>0) || (not.getNumInfracaoRenainf().length()>0) ){ %>			
        
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; "> 
		    <%
		    if (not.getIdentOrgaoEdt().length()>0) {%>
	        <td height="14" colspan="2">&nbsp;Ident. �rg�o:&nbsp;<%= not.getIdentOrgaoEdt().trim()%>&nbsp;&nbsp;</td>    	    
	        <%}else{%>
	        <td height="14" colspan="2">&nbsp;</td>    	    	        
		    <%}%>			        
	        
		    <%
		    if (not.getNumInfracaoRenainf().length()>0) {%>
        	<td colspan="2">&nbsp;Auto Renainf:&nbsp;<%=not.getNumInfracaoRenainf().trim()%>&nbsp;</td>
	        <%}else{%>
        	<td colspan="2">&nbsp;</td>        
		    <%}%>	
		    
		</tr>
		<%}%>		
		
		
		
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; "> 
	        <td height="14" colspan="5">          	  <a href="#" onClick="javascript: Classificacao('Responsavel',this.form);">
	        		&nbsp;Propriet�rio:&nbsp;<%=not.getProprietario().getNomResponsavel().trim() %>
        	</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resp. Pontos :&nbsp;
				<% if (not.getCondutor().getNomResponsavel().length()>0) { %>
					<%=not.getCondutor().getNomResponsavel().trim() %>
				<% } else { %>Condutor n�o Identificado	<% } %>
			</td>
		</tr>		
    	<tr><td height="2" colspan="5" bgcolor="#666666"></td></tr>		
		<% a++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>	
		<tr><td height="40" colspan="5" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align="center" colspan="5"><strong><%= msg %></strong></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
  </div> 




<!-- ========================Fim do 1� bloco========================================== -->    














			 	 			

<!-- ========================1� bloco========================================== -->   
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
    
    
    <tr><td height="2" colspan="6" bgcolor="#be7272"></td></tr>
	
	

    
    
    
    
	
    


	<tr bgcolor="#faeae5">
	  <td width="11%" align="left" >&nbsp;<strong>Propriet�rio:&nbsp;&nbsp;</strong></td>
		<td width="33%" align="left" >
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomResp" type="text" value='<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>' size="41" maxlength="40" onKeyPress="javascript:f_let();" onChange="this.value=this.value.toUpperCase()"></td>
		<td width="12%" style="text-align:right" ><strong>CPF :</strong></td>
		<td width="18%" align="left">
        <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCPF" type="text" size="22" maxlength="14" value='<%=cpf%>'  onfocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);">
		<td width="9%" style="text-align:right" ><strong>&nbsp;</strong></td>	 
	    <td width="16%">&nbsp;</td>
	    <td width="1%">&nbsp;</td>
	</tr>	 
    
    
    
    
    
    
   
	<tr bgcolor="#faeae5">
		<td width="12%" style="text-align:left" ><strong>CPF Resp. Ptos :&nbsp;&nbsp;</strong></td>
		<td width="18%" align="left">
	    <input name="numcpfResp" type="text" size="19" maxlength="11" value='<%=cpf2%>' onfocus="javascript:this.select();" onChange = "javascript:veCPF2(this,0);">
	

		<td width="9%" style="text-align:right" ><strong>Data Req :</strong></td>	 
	    <td width="16%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="dataReq" type="text" size="14" maxlength="10" value='<%=RequerimentoId.getDatRequerimento() %>' onKeyPress="javascript:Mascaras(this,'99/99/9999');"></td>
	    <td width="1%">&nbsp;</td>

	
	    <td width="11%" align="left" >&nbsp;<strong>&nbsp;&nbsp;</strong></td>
		<td width="33%" align="left" >&nbsp;</td>
	    
	    
	</tr>	 
	</table>


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
 

<!-- ========================Fim do 1� bloco========================================== -->    
 
    
<!-- ========================2� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Tipo :</strong></td> 
	    <td width="17%" align="left" ><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>disabled<%}%> name="indTipoCNHPGU" type="checkbox" style=" heigth:11;width:13px" value='1'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getCondutor().getIndTipoCnh(),"1")%> class="input">
      				PGU &nbsp;<input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>disabled<%}%> name="indTipoCNH" type="checkbox" style=" heigth:11;width:13px" value='2'  <%= sys.Util.isChecked(AutoInfracaoBeanId.getCondutor().getIndTipoCnh(),"2")%> class="input">
		CNH</td>
	    <td width="12%" style="text-align:right" ><span><strong>Decis&atilde;o Judic.:</strong></span>        </td>
		<td width="4%" align="left" ><input name="decisaoJur" type="checkbox" class="input" style=" heigth:11;width:13px" value='1' <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%>></td>
		<td width="12%" style="text-align:right" ><strong>N� CNH/PGU :</strong></td>
		<td width="18%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="numCNH" type="text" size="19" maxlength="11" value='<%=AutoInfracaoBeanId.getCondutor().getNumCnh().trim() %>' onChange="this.value=this.value.toUpperCase()"></td>
		<td width="9%" align="right" nowrap><strong >UF CNH :</strong></td>	 
        <td width="7%"><jsp:getProperty name="UFCNHId" property="popupString" /></td>
        <td width="10%" align="right" nowrap>&nbsp;</td>
  </tr>
</table>   


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      
<!-- ========================Fim do 2� bloco========================================== -->        

<!-- ========================3� bloco========================================== --> 
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Endere�o :</strong></td> 
	    <td width="25%" align="left" >
	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtEndereco" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtEndereco().trim() %>' size="35" maxlength="27"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">	    </td>
	    <td width="4%" style="text-align:right" ><strong>N�:</strong></td>
	    <td width="8%" align="rigth">&nbsp;<input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtNumEndereco" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getNumEndereco().trim() %>' size="7" maxlength="5"  onKeyPress="javascript:f_num();" ></td>
	    <td width="8%" style="text-align:right">
        
        <strong>Compl.:</strong></td>
		<td width="18%"><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="txtComplemento" type="text" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getTxtComplemento().trim() %>' size="20" maxlength="11"  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" ></td>
		<td width="9%" style="text-align:right"><strong >Bairro :</strong></td>	 
        <td width="17%" align="left" nowrap><input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomBairro"   type="text" size="20" maxlength="30" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>
  </tr>
</table> 


  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      
<!-- ========================Fim do 3� bloco========================================== -->        
<!-- ========================4� bloco========================================== -->
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr bgcolor="#faeae5">
    <td width="11%" align="left" >&nbsp;<strong>Cidade :</strong></td> 
	    <td width="33%" align="left" >
   	    <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>readonly<%}%> name="nomCidade"   type="text" size="32" maxlength="30" value='<%=AutoInfracaoBeanId.getCondutor().getEndereco().getNomCidade().trim() %>'  onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()">	    </td>
	    <td width="12%" style="text-align:right" ><strong>CEP :</strong></td>
	    <td width="18%">
        <input <%if (UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1120")) {%>readonly<%}%> name="numCEPTR" type="text" size="11" maxlength="8" value='<%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt().trim() %>' onKeyPress="javascript:f_num();"></td>
        <td width="9%" style="text-align:right" ><strong>UF : </strong></td>
		<td width="8%"><jsp:getProperty name="UFId" property="popupString" /></td>
			 
		<td width="9%" align="right" nowrap>
	  <%if (!UsuarioFuncBeanId.getJ_sigFuncao().equals("REC1110")) {%>
        	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ConfirmaAjuste',this.form);">	
		   		<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" style="margin-left: 4px; margin-top:0px; margin-bottom: 0px" >		  	</button>
		<%}%>		</td>	 
  </tr>
</table>  

  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1"></td> </tr>
  </table>
      

<!-- ========================Fim do 4� bloco========================================== -->      

<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
     <tr bgcolor="#faeae5">
   	    <td width="30%" height="22" valign="middle">&nbsp;<strong>N�mero do Processo :</strong>&nbsp;
			  <input type="text" name="numprocesso" size="25" maxlength="20" value='<%=AutoInfracaoBeanId.getNumProcesso() %>'  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase();">	
        </td>			
        <td width="30%">&nbsp;	<strong>Data do Processo :</strong>&nbsp;
			  <input type="text" name="datprocesso" size="12" maxlength="10" value='<%=AutoInfracaoBeanId.getDatProcesso() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
		 <td colspan=2>&nbsp;&nbsp;<strong>Motivo:</strong>&nbsp;&nbsp;
			  <textarea rows=2 style="border-style: outset;" name="txtMotivo" cols="105" 
				 onfocus="javascript:this.select();"></textarea>
	     </td>
     </tr>
  </table>		








  
    <%}%>
</div>
<% if ("S".equals(AutoInfracaoBeanId.getMsgOk())) { %>		
<div style="position:absolute; left:50px; right: 15px; bottom:63px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>   	
<%}%>	
   	<script>document.AjusteManualForm.numAutoInfracao.focus();</script>	        
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
 </body>
</html>
