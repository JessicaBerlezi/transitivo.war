<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
	TELA > AtribuirProcesso
-->	
<!-- Abre a Sessao -->
<%@page import="REC.ParamOrgBean"%>
<%@page import="com.lowagie.text.pdf.PRAcroForm"%>
<%@page import="UTIL.TipoRecGeneric"%>
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@page import="ACSS.Ata"%>
<%String sFuncaoPai="";%>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:setProperty name="SistId" property="j_abrevSist" value="ACSS" />  
<%@page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>
<!-- Chama o Objeto da Tabela de Perfil -->
<jsp:useBean id="PerfBeanId" scope="request" class="ACSS.PerfilBean" />
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 

<html>
<head>
<link rel="stylesheet" href="css/transitivo.css">
<%-- <jsp:include page="Css.jsp" flush="true" /> --%>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<style type="text/css">
body{
	overflow: auto;
}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
function valida(opcao,fForm) {
	var codOrgao = document.getElementById("codOrgao").value;
	switch (opcao) {
 		case 'buscaProcessos':
			var x = document.getElementById("tipoAta");
			var i = x.selectedIndex;
	  
	  		var a = document.getElementById("idNrAtaName");
	  		var b = a.selectedIndex;	  
	  
	  		var gerarhtm = document.getElementById('divRecebe');
	  		var c = document.getElementById("idCpfAtaRelator");
	  		var d = c.selectedIndex;

	  		var j_id = document.getElementById('j_id').value;
	  		var j_token = document.getElementById('j_token').value;
	  		var j_cmdFuncao = document.getElementById('j_cmdFuncao').value;
	  		var j_sigFuncao = document.getElementById('j_sigFuncao').value;
	  		var j_jspOrigem = document.getElementById('j_jspOrigem').value;
	  		var j_abrevSist = document.getElementById('j_abrevSist').value;
			
			var tipoAta = x.options[i].text;
			var cdAta = a.options[b].value;
			var idCpfAtaRelator = c.options[d].value;
			
			exibirCarregando("Carregando processos...");
			
			if (veCamposProcesso(fForm) == true) {
				$.ajax({
						url: "/Transitivo/acessoTool",
						method: "POST",
						data:{
							acao: opcao,
							tpRequerimento: tipoAta,
							idCpfAtaRelator: idCpfAtaRelator,
							cdAta: cdAta,
							codOrgao: codOrgao,
							
							j_id: j_id,
							j_token: j_token,
							j_cmdFuncao: j_cmdFuncao,
							j_sigFuncao: j_sigFuncao,
							j_jspOrigem: j_jspOrigem,
							j_abrevSist: j_abrevSist
						},
						success: function(data){
							MontarListas();
						},
						error: function(){
							alert("erro");
						}
				});
			}
			break;
			
 		 case 'imprimirInf':
 			 
 	
 			var j_id = document.getElementById('j_id').value;
	  		var j_token = document.getElementById('j_token').value;
	  		var j_cmdFuncao = document.getElementById('j_cmdFuncao').value;
	  		var j_sigFuncao = document.getElementById('j_sigFuncao').value;
	  		var j_jspOrigem = document.getElementById('j_jspOrigem').value;
	  		var j_abrevSist = document.getElementById('j_abrevSist').value;
	  		var codAta = document.getElementById("idNrAtaName").value;
 			fForm.acao.value=opcao;
 			fForm.codOrgao.value = document.getElementById("codOrgao").value;
 			fForm.target= "_blank";
 			fForm.j_id.value = j_id;
 			fForm.j_token.value = j_token;
 			fForm.j_cmdFuncao.value = j_cmdFuncao;
 			fForm.j_sigFuncao.value = j_sigFuncao;
 			fForm.j_jspOrigem.value = j_jspOrigem;
 			fForm.j_abrevSist.value = j_abrevSist;
 			fForm.cdAta.value = codAta;
 			fForm.action = "acessoTool";  
 			fForm.submit();	   		
 			break;	
 			
 		case 'V':
			close();
			break;
	}
}

function MontarListas(){
	var tipo = <%=TipoRecGeneric.LISTA_PROCESSOS%>;
	var list = [];
	var codAta = $("#idNrAtaName option:selected").val();
	var codOrgao = document.getElementById("codOrgao").value;
	
	$.ajax({
		url: "/Transitivo/ControladorRecGeneric",
		method: "POST",
		data: {
			tipoRec: tipo,
			codAta: codAta,
			codOrgao: codOrgao
		},
		success: function(date){
			
			document.getElementById("countSemRelator").innerHTML = "(" + date[0].length + ")";
			document.getElementById("countComRelator").innerHTML = "(" + date[1].length + ")";
			
			var headTable = "<tr style='font-size: 12px;'>";
			headTable += "<th></th>";
			headTable += "<th></th>";
			headTable += "<th>N�mero do Processo</th>";
			headTable += "<th>Data do Processo</th>";
			headTable += "<th>Ano</th>";
			headTable += "</tr>";
			
			document.getElementById("processosSemAtrib").innerHTML = headTable;
			
			for (var i = 0; i < date[0].length; i++) {
				var row = "<tr style='text-align: center; font-size: 12px;'>";
				row += "<td>" + (i + 1) + "</td>";
				row += "<td align='center'><input type='checkbox' name='item' value='" + date[0][i].cod_requerimento + "' /></td>";
				row += "<td>" + date[0][i].num_processo + "</td>";
				row += "<td>" + date[0][i].dat_processo + "</td>";
				row += "<td>" + date[0][i].ano + "</td>";
				row += "</tr>";
				
				document.getElementById("processosSemAtrib").innerHTML += row;
			}
			
			var headTable = "<tr style='font-size: 12px;'>";
			headTable += "<th></th>";
			headTable += "<th></th>";
			headTable += "<th>N�mero do Processo</th>";
			headTable += "<th>Data do Processo</th>";
			headTable += "<th>Nome do Relator</th>";
			headTable += "<th>Ano</th>";
			headTable += "</tr>";
			
			document.getElementById("processosComAtrib").innerHTML = headTable;
			
			for (var i = 0; i < date[1].length; i++) {
				var row = "<tr style='text-align: center; font-size: 12px;'>";
				row += "<td>" + (i + 1) + "</td>";
				row += "<td align='center'><input type='checkbox' name='itemC' value='" + date[1][i].cod_requerimento + "' /></td>";
				row += "<td>" + date[1][i].num_processo + "</td>";
				row += "<td>" + date[1][i].dat_processo + "</td>";
				row += "<td>" + date[1][i].nom_relator + "</td>";
				row += "<td>" + date[1][i].ano + "</td>";
				row += "</tr>";
				
				document.getElementById("processosComAtrib").innerHTML += row;
			}
			
			var chksTudo = document.getElementsByName("chkTudo");
			for(var i = 0; i < chksTudo.length; i++){
				chksTudo[i].checked = false;
			}
			
			esconderCarregando();
		},
		error: function(){
			alert("erro");
		}
	});
}

function veCamposProcesso(fForm) {
	valid   = true;
	sErro = "";
	
	
	  var x = document.getElementById("tipoAta");
	  var i = x.selectedIndex;
	  
	  var a = document.getElementById("idNrAtaName");
	  var b = a.selectedIndex;
	  
	  
	  var gerarhtm = document.getElementById('divRecebe');
	  var c = document.getElementById("idCpfAtaRelator");
	  var d = c.selectedIndex;
	  
	  document.all["tpRequerimento"].value= x.options[i].text;
	  document.all["dsAta"].value= a.options[b].text;
	  document.all["dsRelator"].value= c.options[d].text;
	
// 	if (fForm.tpRequerimento.value==0) {
// 	   valid = false
// 	   sErro = "Tipo de requerimento n�o selecionado. \n"
// 	}	
// 	if (fForm.dsAta.value==0) {
// 	   valid = false
// 	   sErro += "Numero da Ata n�o selecionado. \n"
// 	}
	if(a.options[b].value == "0"){
		  valid = false;
		  sErro += " O n�mero da Ata n�o foi selecionada.\n";
	}
	if(c.options[d].value == "SELECIONE"){
		  /* valid = false;
		  sErro += " O Relator n�o foi selecionado.\n"; */
	}
    if (valid==false) alert(sErro)
	return valid ;
}

function atualiza(opcao,fForm) {
	 if (veCampos(fForm)==false) return ;
	 if ((fForm.perfOperacoes==null) || (fForm.perfOperacoes==null) || (fForm.perfOperacoes==null)) {
		 alert("Selecione o Perfil desejado e confirme no 'OK'")
		 return ;
	 }
	 switch (opcao) {
	 
	   case 'IT':
			ApagaSelect(fForm.perfOperacoes,true)    	
			CopiaSelect(fForm.tempOperacoes,fForm.perfOperacoes,true)
			ApagaSelect(fForm.sistOperacoes,true)    	
			break; 
				
	   case 'ET':   
			ApagaSelect(fForm.sistOperacoes,true)    	
			CopiaSelect(fForm.tempOperacoes,fForm.sistOperacoes,true)
			ApagaSelect(fForm.perfOperacoes,true)    	
			break; 	
			
	   case 'Inclui':
				DeselecionaSelect(fForm.tempOperacoes,fForm.tempOperacoes,false) 
				SelecionaSelect(fForm.perfOperacoes,fForm.tempOperacoes,true) 	
				SelecionaSelect(fForm.sistOperacoes,fForm.tempOperacoes,false)
				ApagaSelect(fForm.perfOperacoes,true)    	
		    	CopiaSelect(fForm.tempOperacoes,fForm.perfOperacoes,false)	
				ApagaSelect(fForm.sistOperacoes,false)
			break;
	   case 'Exclui':  
			DeselecionaSelect(fForm.tempOperacoes,fForm.tempOperacoes,false) 
			SelecionaSelect(fForm.sistOperacoes,fForm.tempOperacoes,true) 	
			SelecionaSelect(fForm.perfOperacoes,fForm.tempOperacoes,false) 
			ApagaSelect(fForm.sistOperacoes,true)    							
			CopiaSelect(fForm.tempOperacoes,fForm.sistOperacoes,false)	
			ApagaSelect(fForm.perfOperacoes,false)
			break;
	 }	
	}

function ChamPopula(){
	 var gerarhtm = document.getElementById('divRecebe');
	    var x = document.getElementById("tipoAta");
	    var i = x.selectedIndex;
	    
	    exibirCarregando("Carregando...");
	    
	    switch(x.options[i].text)
	    {
		    case "Def. Previa":
		    	document.getElementById("demoAta").innerHTML = populaDivRecebMaisDp();
		    	break;
		    case "1� INST�NCIA":
		    	document.getElementById("demoAta").innerHTML = populaDivRecebMaisPI();
		    	break;
		    case "2� INST�NCIA":
		    	document.getElementById("demoAta").innerHTML = populaDivRecebMaisSI();
		    	break;
		    case "P.A.E":
		    	document.getElementById("demoAta").innerHTML = populaDivRecebMaisPAE();
		    	break;
		    case "SELECIONE...":
		    	document.getElementById("demoAta").innerHTML = "SELECIONE O TIPO";
		    	break;
	    	default:
	    		break;
	    	
	    }
	    //callSelectRelator();
	}
	
	
function callSelectRelator(){
	document.getElementById("demoAtaRelator").innerHTML =	populaDivRecebRelator();
}

function populaDivRecebRelator(){
 
   
   


<%Ata ataRelator = new Ata();
   ataRelator.setSelectAta(ataRelator.montaSelectSqlRelator("idCpfAtaRelator"));%>
	
   	var teste = '<%=ataRelator.getSelectAta()%>'
   	return teste;
}


function populaDivRecebMaisDp(){   
   
<%Ata ataDp = new Ata();
  ataDp.setSelectAta(ataDp.montaSelectSql("Def. Previa", "idNrAtaName", session.getAttribute("codNatureza").toString()));%>
	
   	var teste = '<%=ataDp.getSelectAta()%>'

   	esconderCarregando();
   	
   	return teste;
}

function populaDivRecebMaisPI(){
  
   
<%Ata ataPI = new Ata();
   ataPI.setSelectAta(ataPI.montaSelectSql("1� INST�NCIA", "idNrAtaName", session.getAttribute("codNatureza").toString()));%>
	
   	var teste = '<%=ataPI.getSelectAta()%>'

   	esconderCarregando();
   	
   	return teste;
}

function populaDivRecebMaisSI(){

   
<%Ata ataSI = new Ata();
   ataSI.setSelectAta(ataSI.montaSelectSql("2� INST�NCIA", "idNrAtaName", session.getAttribute("codNatureza").toString()));%>
	
   	var teste = '<%=ataSI.getSelectAta()%>'

   	esconderCarregando();
   	
   	return teste;
}

function populaDivRecebMaisPAE(){
	<%
		Ata ataPAE = new Ata();
		ataPAE.setSelectAta(ataPAE.montaSelectSql("PAE", "idNrAtaName", session.getAttribute("codNatureza").toString()));
	%>
	
   	var teste = '<%=ataPAE.getSelectAta()%>'

   	esconderCarregando();
   	
   	return teste;
}

function selecionar_tudo(){ 
	   for (i=0;i<document.perfilForm.elements.length;i++) 
	      if(document.perfilForm.elements[i].type == "checkbox")	
	         document.perfilForm.elements[i].checked=1 
	} 
	
	
function deselecionar_tudo(){ 
	   for (i=0;i<document.perfilForm.elements.length;i++) 
	      if(document.perfilForm.elements[i].type == "checkbox")	
	         document.perfilForm.elements[i].checked=0 
	}

function atualizar(opcao,fForm) {
	var codOrgao = document.getElementById("codOrgao").value;
	switch (opcao) {
		case 'Inclui':
			if(VerificarValoresCbos() == false){
				break;
			}
			else{
				if(VerificarRelator() == false){
					break;
				}
				if(VerificarChks('left') == false){
					break;
				}
				
				var ii = null;
				var indices = "";
				var aChk = document.getElementsByName("item");  
				for (var i=0;i<aChk.length;i++){  
					if (aChk[i].checked == true){  
						// CheckBox Marcado... Fa�a alguma coisa... Ex:
						if(indices == ""){
							indices = aChk[i].value;
						}
						else{
							indices += "," + aChk[i].value;
						}
					}
				}

				var objhidden = document.getElementsByName('hdindice');
				var ata = document.getElementById("tipoAta").value;
				var codAta = document.getElementById("idNrAtaName").value;
				var nameAta = $("#tipoAta option:selected").text();
				var codRelator = document.getElementById("idCpfAtaRelator").value;
				var nameRelator = $("#idCpfAtaRelator option:selected").text();

		  		var j_id = document.getElementById('j_id').value;
		  		var j_token = document.getElementById('j_token').value;
		  		var j_cmdFuncao = document.getElementById('j_cmdFuncao').value;
		  		var j_sigFuncao = document.getElementById('j_sigFuncao').value;
		  		var j_jspOrigem = document.getElementById('j_jspOrigem').value;
		  		var j_abrevSist = document.getElementById('j_abrevSist').value;

				$.ajax({
					url: "/Transitivo/acessoTool",
					method: "POST",
					data: {
						acao: opcao,
						hdindice: indices.toString(),
						tpRequerimentoExtra: ata,
						idCpfAtaRelator: codRelator,
						dsRelator: nameRelator,
						cdAta: codAta,
						dsAta: nameAta,
						codOrgao: codOrgao,
						
						j_id: j_id,
						j_token: j_token,
						j_cmdFuncao: j_cmdFuncao,
						j_sigFuncao: j_sigFuncao,
						j_jspOrigem: j_jspOrigem,
						j_abrevSist: j_abrevSist
					},
					success: function(data){
						MontarListas();
					},
					error: function(data){
						alert("erro: " + data);
					}
				});
				break;
			}
	   case 'Exclui':
		   if(VerificarValoresCbos() == false){
			   break;
		   }
		   else{
				if(VerificarChks('right') == false){
					break;
			 	}

		   		var ii = null;
		   		var indicesC = "";
			   	var aChk = document.getElementsByName("itemC");

				for (var i = 0; i < aChk.length; i++){  
					if (aChk[i].checked == true){  
						// CheckBox Marcado... Fa�a alguma coisa... Ex:
						if(indicesC == ""){
								indicesC = aChk[i].value;
						}
						else{
							indicesC += "," + aChk[i].value
						}
					}
				}
			
			var objhiddenC = document.getElementsByName('hdindiceC');

			var ata = document.getElementById("tipoAta").value;
			var codAta = document.getElementById("idNrAtaName").value;
			var nameAta = $("#tipoAta option:selected").text();
			var codRelator = document.getElementById("idCpfAtaRelator").value;
			var nameRelator = $("#idCpfAtaRelator option:selected").text();

	  		var j_id = document.getElementById('j_id').value;
	  		var j_token = document.getElementById('j_token').value;
	  		var j_cmdFuncao = document.getElementById('j_cmdFuncao').value;
	  		var j_sigFuncao = document.getElementById('j_sigFuncao').value;
	  		var j_jspOrigem = document.getElementById('j_jspOrigem').value;
	  		var j_abrevSist = document.getElementById('j_abrevSist').value;

			$.ajax({
				url: "/Transitivo/acessoTool",
				method: "POST",
				data: {
					acao: opcao,
					hdindiceC: indicesC.toString(),
					tpRequerimentoExtra: ata,
					idCpfAtaRelator: codRelator,
					dsRelator: nameRelator,
					cdAta: codAta,
					dsAta: nameAta,
					codOrgao: codOrgao,
					
					j_id: j_id,
					j_token: j_token,
					j_cmdFuncao: j_cmdFuncao,
					j_sigFuncao: j_sigFuncao,
					j_jspOrigem: j_jspOrigem,
					j_abrevSist: j_abrevSist
				},
				success: function(data){
					MontarListas();
				},
				error: function(data){
					alert("erro: " + data);
				}
			});
			
			objhiddenC.value = indicesC.toString();
			break; 
		}
	}
}
</script>
</head>

<body>
<div id="head">
			<div class="img-head2">
				<div class="img-head1">
					<div style="position: absolute; right: 6px;">
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/detran_help.png');"></div>
					</div>
				</div>
			</div>
			<div id="bread" class="bread">
				<strong class="text">PROCESSO &gt; ATRIBUIR RELATOR</strong>
			</div>
		</div>



<div id="body" class="body" style="height: 573px; padding-right: 15px;">


<form name="perfilForm" method="post" action="" style="margin: 0px;">
<%-- <jsp:include page="Cab.jsp" flush="true" /> --%>
<input name="acao" type="hidden" value=' '>	
<input name="cdAta" type="hidden" value=' '>
<input name="dscNot" type="hidden" size="10" maxlength="20"  value= "">
<input name="codOrgao" type="hidden" value="" />

<div id="WK_SISTEMA" style="position:absolute; width:703px; overflow: visible; z-index: 1; top: 75px; left: 52px; " > 
  <!--INICIO CABEC DA TABELA-->
</div>

<!--FIM_CORPO_sistema-->
  
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= PerfBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "350 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<div style="position:absolute; width:100%; overflow: auto; z-index: 1; top: 180px; height=195px; display:none; visibility='hidden';">
  <table bgcolor="#DFEEF2" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
  	<tr bgcolor="#DFEEF2"><td align=center>
	</td></tr>
  </table>	
</div>

<div style="position:absolute; width:100%; overflow: auto; z-index: 1; top: 180px; height=195px; display:none; visibility='hidden';">
  <table bgcolor="#DFEEF2" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
  	<tr bgcolor="#DFEEF2"><td align=center>
	</td></tr>
  </table>	
</div>
 <%-- <% Ata.arListComAtribuicao.clear();
   		   Ata.arListSemAtribuicao.clear();%> --%>

<input name="tpRequerimento"    type="hidden" value="">
<input name="dsAta"             type="hidden" value="">
<input name="dsRelator"         type="hidden" value="">
<input type="hidden" name="hdindice"/>
<input type="hidden" name="hdindiceC"/>
	
					
<!-- Rodap�-->
<%-- <jsp:include page="Retornar.jsp" flush="true" /> --%>

</form>

<%-- <jsp:include page="Rod.jsp" flush="true" /> --%>
<!-- <!-- Fim Rodap� -->
<form name="perfilForm" method="post" action="">
<%-- <jsp:include page="Cab.jsp" flush="true" /> --%>
<input name="acao" type="hidden" value=' '>	
<input name="cdAta" type="hidden" value=' '>

<input name="dscNot" type="hidden" size="10" maxlength="20"  value= "">
<input name="codOrgao" type="hidden" value="" />

<input name="tpRequerimento"    type="hidden" value="">
<input name="dsAta"             type="hidden" value="">
<input name="dsRelator"         type="hidden" value="">
<input type="hidden" name="hdindice"/>
<input type="hidden" name="hdindiceC"/>

<input name="j_id" id="j_id" type="hidden" value="<%=RequisicaoBeanId.getId()%>">
<input name="j_token" id="j_token" type="hidden" value="<%=RequisicaoBeanId.getToken()%>">
<input name="j_cmdFuncao" id="j_cmdFuncao" type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao" id="j_sigFuncao" type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem" id="j_jspOrigem" type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist" id="j_abrevSist" type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">

	<table style="width: 100%; margin-left: 55px" border="0" cellpadding="0" cellspacing="1">
		<tr>
			<td style="width: 15%;">
				<b>Tipo de Ata:</b>
			</td>
			<td style="padding: 2px; text-align: left;">
				<select style="width: 200px;" name="tipoAta" id="tipoAta" onchange="ChamPopula(); verificaQtdeProcAtribuidos();">
		    		<option value="0" label="SELECIONE...">SELECIONE...</option>
  					<option value="1" label="Def. Previa">Def. Previa</option>
 					<option value="2" label="1� INST�NCIA">1� INST�NCIA</option>
 					<option value="3" label="2� INST�NCIA">2� INST�NCIA</option>
 					<option value="4" label="PAE">P.A.E</option>
	  			</select>
			</td>
      </tr>       
      <tr>
		<td>
			<b>N�mero da Ata:</b>
		</td>
		<td style="padding: 2px; text-align: left;">
			<p id="demoAta">
			</p>
		</td>
      </tr>      
       <tr>
			<td>
				<b>Relator:</b>
			</td>
			<td style="padding: 2px; text-align: left;">
				<p id="demoAtaRelator"></p>
			</td>
      </tr>
      <tr>
      	<td>
      		<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('imprimirInf',this.form)"> 
            	<img src="<%= path %>/images/REC/bot_imprimir_ico.gif" alt="Imprimir" title="Imprimir Relat�rio" width="29" height="19">
			</button>
      	</td>
      	<td id="tdCarregando" hidden="true">
			<img src="/Transitivo/images/carregando-gif.gif" style="width: 50px; height: 30px;" />
			<span style="position: relative; top: -10px; font-weight: bold;">Carregando...</span>
		</td>
      </tr> 
  </table>
</form> 
  
  <table border="1" style="width: 95%; margin-left: 55px; height: 350px;">
	<tr style="height: 20px;">
		<td style="text-align: center;">
			<span style="float: left; margin-left: 7px;">
				<input type="checkbox" name="chkTudo" onchange="selecionarTudo(this, 'item')" />
					Marcar Todos
			</span>
			PROCESSOS SEM ATRIBUI��ES
			<span id="countSemRelator" style="color: blue;">(0)</span>
		</td>
		<td>
		</td>
		<td style="text-align: center;">
			<span style="float: left; margin-left: 7px;">
				<input type="checkbox" name="chkTudo" onchange="selecionarTudo(this, 'itemC')" />
					Marcar Todos
			</span>
			PROCESSOS ATRIBUIDOS
			<span id="countComRelator" style="color: blue;">(0)</span>
		</td>
	</tr>
	<tr>
		<td style="width: 50%; background-color: #DAA9A9; vertical-align: top;">
			<div style="height: 330px; overflow: auto; text-align: center;">
				<table id="processosSemAtrib" style="width: 100%;">
				</table>
			</div>
		</td>
		<td style="vertical-align: top;">
			
          	<button style="height: 21px; border: none; background: transparent;" NAME="ProximoImg" onClick="javascript: atualizar('Inclui',this.form);">
				<img alt="Inclui" src="<%= path %>/images/bot_prox.gif" width="20" height="19"/>
			</button>
			<button style="height: 21px; border: none; background: transparent;" NAME="AnteriorImg" onClick="javascript: atualizar('Exclui',this.form);">
				<img alt="Excluir" src="<%= path %>/images/bot_ant.gif" width="20" height="19"/>
			</button>
			
		</td>
		<td style="width: 50%; background-color: #DAA9A9; vertical-align: top;">
			<div style="height: 330px; overflow: auto;">
				<table id="processosComAtrib" style="width: 100%;">
				</table>
			</div>
		</td>
	</tr>
</table>
	<input type="hidden" id="codOrgao" value="<%= ParamOrgBeanId.getCodOrgao() %>" />
</div>

<div id="footer">
			<div class="img-footer2">
				<div class="img-footer"></div>
				<div class="img-footer-btn" onclick="location.href='/Transitivo'" title="Home" style="cursor: pointer;">
				</div>
			</div>
		</div>

<script type="text/javascript" src="<%= path %>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript">
//fun��o para preencher a select do Relator
	function verificaQtdeProcAtribuidos() {
		var junta = document.getElementById("tipoAta");
		var valor = junta.options[junta.selectedIndex].value;
		var tipo = <%=TipoRecGeneric.MONTAR_RELATOR%>;

		$(document).ready(function() {
			$.ajax({
				url : "/Transitivo/ControladorRecGeneric",
				method : "post",
				data : {
					tipoRec : tipo,
					valor : valor,
					seletor : "idCpfAtaRelator"
				},
				success : function(data) {
					$("#demoAtaRelator").html(data);
				},
				error : function() {
					alert("erro na requisi��o!");
				}
			});
		});
	};

	function EscolherRelator(form) {
		limparProcessosComRelator();
		
		var idNomeRelator = document.getElementById("idCpfAtaRelator");
		var nomeRelator = idNomeRelator.options[idNomeRelator.selectedIndex].text;
		var valorRelator = idNomeRelator.options[idNomeRelator.selectedIndex].value;

		$("#idNameRelator").html("Relator: " + nomeRelator);

		if(document.getElementById("nameRelator") != null){
			document.getElementById("nameRelator").value = nomeRelator;
		}

		if(document.getElementById("cod_relator") != null){
			document.getElementById("cod_relator").value = valorRelator;
		}

		if(VerificarRelator() == true){
			valida('buscaProcessos', form);
		}
	}

	function SetCodAta(){
		document.getElementById("codAta").value = "";
	}
	
	function VerificarValoresCbos() {
		var isValid = true;
		var msgErro = "";

		var slcTipoAta = document.getElementById("tipoAta");
		var slcNumAta = document.getElementById("idNrAtaName");
		//var slcRelator = document.getElementById("idCpfAtaRelator");

		if (VerificarCbos() == true) {
			var tipoAta = slcTipoAta.options[slcTipoAta.selectedIndex].value;
			var numAta = slcNumAta.options[slcNumAta.selectedIndex].value;
			//var relator = slcRelator.options[slcRelator.selectedIndex].value;

			if (tipoAta == "0") {
				msgErro = "Escolha um tipo de ata.\n";
				isValid = false;
			} else if (numAta == "0") {
				msgErro += "Escolha um n�mero de ata.\n";
				isValid = false;
			} /* else if (relator == "SELECIONE") {
				msgErro += "Escolha um relator.";
				isValid = false;
			} */

			if (msgErro != "") {
				alert(msgErro);
			}
		} else {
			isValid = false;
		}

		return isValid;
	}

	function VerificarRelator(){
		var isValid = true;

		if(VerificarValoresCbos() == true){
			var slcRelator = document.getElementById("idCpfAtaRelator");

			if(slcRelator == null){
				isValid = false;
				alert("Relator inv�lido!");
			}
			else{
				if(slcRelator.value == "SELECIONE"){
					isValid = false;
					alert("Selecione um Relator.");
				}
			}
		}
		else{
			isValid = false;
		}

		return isValid;
	}

	function VerificarCbos() {
		var isValid = true;
		var msgErro = "";

		if (document.getElementById("tipoAta").value == "0") {
			msgErro = "Escolha um tipo de ata.\n";
			isValid = false;
		} else if (slcNumAta = document.getElementById("idNrAtaName") == null) {
			msgErro += "Escolha um n�mero de ata.\n";
			isValid = false;
		} /* else if (document.getElementById("idCpfAtaRelator") == null) {
			msgErro += "Escolha um relator.";
			isValid = false;
		} */
		if (msgErro != "")
			alert(msgErro);
		
		return isValid;
	}
	
	function VerificarChks(op) {
		var isValid = false;

		var leftChks = document.getElementsByName("item");
		var rightChks = document.getElementsByName("itemC");

		if (op == "left") {
			for (var i = 0; i < leftChks.length; i++) {
				if (leftChks[i].checked == true) {
					isValid = true;
				}
			}
		} else if (op == "right") {
			for (var i = 0; i < rightChks.length; i++) {
				if (rightChks[i].checked == true) {
					isValid = true;
				}
			}
		}

		if(isValid == false){
			alert("Por favor selecione ao menos um processo.");
		}

		return isValid;
	}

	function selecionarTudo(e, elementName){
		var elements = document.getElementsByName(elementName);
		if(elements.length > 0){
			if(e.checked){
				for(var i = 0; i < elements.length; i++){
					elements[i].checked = true;
				}
			}
			else{
				for(var i = 0; i < elements.length; i++){
					elements[i].checked = false;
				}
			}
		}
	}
	
	function limparProcessosSemRelator(){
		document.getElementById("countSemRelator").innerHTML = "(0)";
		document.getElementById("processosSemAtrib").innerHTML = null;
		
		limparProcessosComRelator();
	}
	
	function limparProcessosComRelator(){
		document.getElementById("countComRelator").innerHTML = "(0)";
		document.getElementById("processosComAtrib").innerHTML = null;
	}
	
	function exibirCarregando(texto){
		var tdCarregando = document.getElementById("tdCarregando");
		
		tdCarregando.hidden = false;
		tdCarregando.children[1].innerHTML = texto;
	}
	
	function esconderCarregando(){
		var tdCarregando = document.getElementById("tdCarregando");
		
		tdCarregando.hidden = true;
	}
</script>
</BODY>
</HTML>