<!--M�dulo : REC
	Vers�o : 1.1 - Diretiva
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>

<% 
if(posTop==null) posTop ="170px";   
if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      
      <tr bgcolor="#be7272"> 
        <td width="25" rowspan="2" align="center"><strong><font color="#FFFFFF">Seq</font></strong></td>			
        <td width="150"><strong><a href="#" onClick="javascript: Classificacao('Data');">
			<font color="#FFFFFF">&nbsp;Dt Infra&ccedil;&atilde;o</font></a></strong></td>
        <td width="65" align="center"><strong><a href="#" onClick="javascript: Classificacao('Orgao');">
			<font color="#FFFFFF">&Oacute;rg&atilde;o</font></a></strong></td>
		<td width="95" align="center"><strong><a href="#" onClick="javascript: Classificacao('Auto');">
			<font color="#FFFFFF">N&deg; Auto</font></a></strong></td>
        <td width="60" align="center"><strong><a href="#" onClick="javascript: Classificacao('Placa');">
			<font color="#FFFFFF">Placa</font></a></strong></td>
        <td width="223"><strong><a href="#" onClick="javascript: Classificacao('Processo');">
			<font color="#FFFFFF">&nbsp;Processo</font></a></strong></td>
        <td  align="center"><strong><a href="#" onClick="javascript: Classificacao('DatProcesso');">
			<font color="#FFFFFF">Dt Proc</font></a></strong></td>    

      </tr>
      <tr bgcolor="#be7272">
        <td height="14" colspan="4" ><strong><a href="#" onClick="javascript: Classificacao('Enquadramento');"><font color="#FFFFFF">&nbsp;Enquadramento</font></a></strong></td>     		
        <td colspan="2"><strong><font color="#FFFFFF">&nbsp;Requerimento - Tipo Req</font></strong></td>
      </tr>  
	</table>
    
    
    
    <table width="100%" border="0" cellpadding="1" cellspacing="1">
	  <%
	   if (GuiaDistribuicaoId.getAutos().size()>0) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
			myAuto = (REC.AutoInfracaoBean)it.next() ;				
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (myAuto.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width=25 height="14" align=center rowspan="2">
	        	&nbsp;<%= (seq+1) %></td>
	        <td width=150 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getDatInfracao() %></td>
	        <td width=65 align=center onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getSigOrgao() %></td>
			<td width=95 align="center" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumAutoInfracao() %></td>
        	<td width=60 align="center"onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumPlaca() %></td>
			<td width=223 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %></td>
			<td  onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' onclick 	 = "javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');"> 
	        <td colspan="4" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">&nbsp;<%=myAuto.getInfracao().getDscEnquadramento() %></td>
	        <td colspan="2">&nbsp;<%=myAuto.getNumRequerimento() %>-<%=myAuto.getTpRequerimento() %></td>
	              
		</tr>	
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=8>&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr bgcolor='<%=cor%>'> 
	      <td height="25" colspan=8><strong>&nbsp;&nbsp;&nbsp;Resultado:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input disabled="disabled" name="codResultRS<%= seq %>" type="radio" value="D" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">DEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input disabled name="codResultRS<%= seq %>" type="radio" value="I" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">
        	 INDEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo:&nbsp;<input disabled="disabled" name="txtMotivoRS<%= seq %>" type="text" size="68" maxlength="1000"  value="<%= myAuto.getRequerimentos(0).getTxtMotivoRS() %>"></td>
	    </tr>
	    
	    <% if (myAuto.getRequerimentos(0).getCodResultRS().equals("D")) { %>
	    <tr bgcolor='<%=cor%>' >
             <td  colspan=7 height=20>&nbsp;&nbsp;<input type="radio" checked="checked" disabled="disabled" name="codMotivo<%= seq %>" value="1" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
			       <input disabled="disabled" type="radio" name="codMotivo<%= seq %>" value="2" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
			       <input disabled="disabled" type="radio" name="codMotivo<%= seq %>" value="3" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
			   <!--<input disabled="disabled" type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros -->
			 </td>
        </tr>
        <%}%>
		<% if (myAuto.getRequerimentos(0).getCodRemessa().length()>0) { %>
        <tr bgcolor='<%=cor%>'>
          <td colspan="2"><strong>&nbsp;&nbsp;N&ordm;&nbsp;Guia de Remessa :</strong>&nbsp;<%=myAuto.getRequerimentos(0).getCodRemessa()%></td>        
          <td colspan=6><strong>&nbsp;&nbsp;Data Envio :</strong>&nbsp;<%=myAuto.getRequerimentos(0).getDatEnvioGuia() %></td>
        </tr>
   	    <%}%>   	    
 		<%if (myAuto.getRequerimentos().size()>0){
 			int iJ= myAuto.getRequerimentos().size()-1;	
 			if (myAuto.getRequerimentos(iJ).getTxtJustificativa().length()>0){%>
	   	    <tr bgcolor='<%=cor%>'> 
		      <td height="25" colspan="8" valign="top" ><label for="textexemplo" style="vertical-align:top; display: block; float: left; margin-top: 7px; margin-left: 4px"><strong>Justificativa:&nbsp;</strong></label><textarea cols="100" rows="2" style="background: transparent; " readonly="readonly" class="sem-borda" name="textexemplo" id="textexemplo"><%=myAuto.getRequerimentos(iJ).getTxtJustificativa()%></textarea></td>
		    </tr>
	    <%  }
	    }%>
		<tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>   	    
		<% seq++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>  
