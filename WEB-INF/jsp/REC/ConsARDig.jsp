<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="LogARDigitalizadoBeanId" scope="request" class="REC.LogARDigitalizadoBean" /> 
            
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   
   var edtDataIni = document.forms[0].datInicial;
   var edtDataFim = document.forms[0].datFinal;
   
   diaI = edtDataIni.value.toString().substring(0,2);
   mesI = edtDataIni.value.toString().substring(3,5);
   anoI = edtDataIni.value.toString().substring(6,10);   
   mesI = mesI -1;

   var dtIni = new Date();
   dtIni.setDate(diaI);
   dtIni.setMonth(mesI);
   dtIni.setYear(anoI);   

   diaF = edtDataFim.value.toString().substring(0,2);
   mesF = edtDataFim.value.toString().substring(3,5);
   anoF = edtDataFim.value.toString().substring(6,10);
   mesF = mesF - 1;  
   
   var dtFim = new Date();
   dtFim.setDate(diaF);
   dtFim.setMonth(mesF);
   dtFim.setYear(anoF);   
   
   diff 	= dtFim.getTime() - dtIni.getTime();
   periodo 	= Math.floor(diff/(24*60*60*1000));  

   if ((diaI > diaF)||(mesI > mesF) || (anoI > anoF)){
     alert("Faixa de dias inv�lida. \n");
     return false;
   }
         
   switch (opcao) {    
   
   case 'busca':
   		if(veCampos(fForm) == true){	 
       		fForm.acao.value=opcao;
			fForm.target= "_self";
	  		fForm.action = "acessoTool";  
	  		fForm.submit();	 
   		}
	  	break;
	  
   case 'retorna':
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
 
   case 'R':
	  close() ;   
	  break;	  		
   case 'O':
	  document.all["MsgErro"].style.visibility='hidden'; 
	  break;  
   case 'V':
   	  document.all["MsgErro"].style.visibility='visible'; 
	  break;	  
   }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;        	
	datFinal = fForm.datFinal.value;
	datInicial = fForm.datInicial.value;
	
	if (!ValDt(fForm.datFinal,0)){
	   valid = false;
	   sErro = "Data Inv�lida. \n";
	}
	
	if (!ValDt(fForm.datInicial,0)){
	   valid = false;
	   sErro = "Data Inv�lida. \n";
	}
	
	if ((fForm.datFinal.value == "")&&(fForm.datInicial.value == "")) {
	  valid = false;
	  sErro = "A data de inicio e fim s�o obrigatorias. \n";
	}

	if (valid==false) alert(sErro) 
	
	return valid;
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">

<%
String acao = (String) request.getAttribute("acao");
String datInicial = (String) request.getAttribute("datInicial");
String datFinal = (String) request.getAttribute("datFinal");
%>

<form name="form" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value='<%=acao%>'>
	
<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 80px; left:50px;" id="filtro" > 
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="7%" align="right"  ><b>De : </b></td>
  	    <td width="21%" align="center"><input <%if (!acao.equals("")) {%>readonly<%}%> name="datInicial" type="text" size="14" maxlength="10" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=datInicial%>"></td>
	    <td width="7%" align="right"><strong>At&eacute; : </strong></td>
	    <td width="21%" align="center"><input <%if (!acao.equals("")) {%>readonly<%}%> name="datFinal" type="text" onChange="javascript:ValDt(this,0);" onKeyPress="javascript:Mascaras(this,'99/99/9999');" value="<%=datFinal%>" size="14" maxlength="10"></td>
	    <td width="44%" align="center"><button type="button" name="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);"  >	
    		<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>
	</tr>	
	<tr>
       <td height="1" colspan="9"></td>
	</tr>
   </table>       
 </div>
 <%if (!acao.equals("")) {%>
 <div style="position:absolute; left:50px; top:106px; width:720px; height:10px; z-index:1; overflow: visible;" id="titulos"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr bgcolor="#be7272"> 
		<td width="150" bgcolor="#be7272"> <div align="center"><font color="#ffffff"><strong>Data/Hora</strong></font></div></td>
	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Arquivo</strong></font></div></td>
	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Caixa</strong></font></div></td>
  	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Lote</strong></font></div></td>
        <td> <div align="center"><font color="#ffffff"><strong>Opera&ccedil;&atilde;o</strong></font></div></td>
	</tr>
	<tr bgcolor="#be7272"> 
       <td  colspan="5" align="center"><font color="#ffffff"><strong>Mensagem de Retorno </strong></font></td>	    
	</tr>		  
  </table>
</div>
 	  
 <div style="position:absolute; width:720px; overflow: auto; z-index: 3; top: 136px; left: 50px; height: 180px;" id="conteudo">   
   <table width="100%" border="0" cellpadding="1" cellspacing="1" id="fconsulta"  >  	   		
	<%
        REC.LogARDigitalizadoBean[] logs = LogARDigitalizadoBeanId.get$Logs();
        for (int i = 0; i < logs.length; i++) {
	%>       
		<tr bgcolor="#faeae5"> 
			 <td width="150">&nbsp;<%= logs[i].getDatProcessamentoExt()%></TD>
			 <td width="150">&nbsp;<%= logs[i].getNomArquivo()%></TD>
			 <td width="150">&nbsp;<%= logs[i].getNumCaixa()%></TD>	
 			 <td width="150">&nbsp;<%= logs[i].getNumLote()%></TD>	
		     <td bgcolor    >&nbsp;<%= logs[i].getCodOperacaoExt()%></TD>	
 		</tr>
		<%if (logs[i].getCodOperacao().equals("R")) {%>
			<tr bgcolor="#faeae5"><td colspan="5">&nbsp;<%= logs[i].getDscRetorno()%></font></td></tr>
		<%}%>
		<tr ><td height="2" colspan="5" bgcolor="#be7272"></td></tr>
	
       </tr>
    <%}%>
   	</table>
  </div> 
  
 <div style="position:absolute; left:50px; top:320px; width:720px; height:36px; z-index:1; background-image: url(images/detran_bg4.gif); overflow: visible;" id="retornar"> 
 	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
 		<tr>
			<td  align = "center"> 
	  		      <button type=button name="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('retorna',this.form);" >
					  <img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >
				  </button>
			</td>
		</tr>
 	</table>
 </div>     
<%}%>
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="../sys/DivErro.jsp" flush="true" >
<jsp:param name="msgErro" value= "<%= LogARDigitalizadoBeanId.getMsgErro() %>" />
<jsp:param name="msgErroTop" value= "230 px" />
<jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include>

</form>
</BODY>
</HTML>
