<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objetos -->
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="InformarResultId"   scope="request" class="REC.InformarResultBean" /> 
<jsp:include page="CssImpressao.jsp" flush="true" /> 
<!--Include da Parte Fixa-->
<jsp:include page="AutoImp.jsp" flush="true" />
<% if ("S".equals(InformarResultId.getEventoOK())) { %>			 	 
  <table width="100%" border="0" cellpadding="0" cellspacing="0"  class="semborda">	 
     <tr>
	    <td>&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
        <td><%=	 RequerimentoId.getCodTipoSolic()+" - "+RequerimentoId.getNomResponsavelDP()+" - "+RequerimentoId.getNumRequerimento() %></td>
	    <td >Data do Resultado:&nbsp; <%=RequerimentoId.getDatRS() %></td>
	    <td></td>		
     </tr>
     <tr><td colspan=4 height=3></td></tr>
     <tr >
        <td width="13%">&nbsp;&nbsp;Relator:</td>
        <td width="50%"><%= RequerimentoId.getNomRelatorJU()%></td>
        <td  width="30%" valign="middle">Resultado:&nbsp;&nbsp;&nbsp; 
          <%= ("D".equals(RequerimentoId.getCodResultRS()) ? "Deferido" : ("I".equals(RequerimentoId.getCodResultRS()) ? "Indeferido" : "" )) %>
		</td>
		<td width="7%"></td>
     </tr>
      <tr><td colspan=4 height=3></td></tr>
      <tr >
        <td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
        <td width="80%" colspan=3><%=RequerimentoId.getTxtMotivoRS()%></td>
      </tr>	  
  </table>		
<% } else { %>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr ><td height=23> 
			<jsp:getProperty name="InformarResultId" property="msg" />
	     </td>
      </tr>	  
  </table>		
						
<% } %>  
