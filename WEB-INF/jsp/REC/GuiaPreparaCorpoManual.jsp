<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<%
String posTop = request.getParameter("posTop"); if(posTop==null) posTop ="170px";   
String posHei = request.getParameter("posHei"); if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" >
	  <%
	   int max = 0;	  
	   if ((GuiaDistribuicaoId.getAutos().size()>0) ) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {

			myAuto = (REC.AutoInfracaoBean)it.next() ;				
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (myAuto.getMsgErro().length()>0)  cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'> 
	        <td width=60 height="14" align=center rowspan=2>&nbsp;<%= (seq+1) %>&nbsp;
			        <input type="checkbox" name="Selecionado" value="<%= seq %>" class="input" checked onclick="javascript:SomaSelec();">
	        </td>
	        <td width=80 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getDatInfracao() %></td>
	        <td width=80 align=center onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getSigOrgao() %></td>
			<td width=80 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumAutoInfracao() %></td>
        	<td width=70 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumPlaca() %>       </td>
			<td width=150 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %>   </td>
			<td width=75 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumProcesso()%>','<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %></td>
			<% if  ("REC0336".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %>
			<td align=center> <% if ( myAuto.getTemAIDigitalizado()) { %>							
				  <button style="border: 0px; background-color: transparent; height: 21px; width: 41px; cursor: hand;" type="button" onclick="javascript:MostraAI('MostraAI',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>','1');"> 
	              <img src="<%= path %>/images/REC/bot_Auto_dig_vm_ico.gif" alt="Visualizar Auto Digitalizado" width="39" height="19"></button>
		        <% } else { %>
				  <button style="border: 0px; background-color: transparent; height: 21px; width: 41px; cursor: hand;" type="button" onclick="javascript:MostraAI('MostraAI',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>','0');"> 
	              <img src="<%= path %>/images/REC/bot_Auto_dig_ico.gif" alt="Visualizar Auto Digitalizado" width="39" height="19"></button>
				<% } %></td>
			<%}else{%>			
			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
			<%}%>
		</tr>
		<tr bgcolor='<%=cor%>'> 
	        <td colspan=4>&nbsp;<%=myAuto.getInfracao().getDscEnquadramento() %> </td>
	        <td colspan=4>&nbsp;<%=myAuto.getNumRequerimento() %>&nbsp;&nbsp;<%=myAuto.getTpRequerimento() %> </td>	        
		</tr>	
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=8>&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>		
		<% seq++; 
		   max++;
		}
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    <input type="hidden" name="Selecionado" value="" class="input">	
    </table>      
</div>  
