<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %> 
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsrLogado"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama Beans complementares -->
<jsp:useBean id="StatusId" scope="request" class="REC.StatusBean" />
  <jsp:setProperty name="StatusId" property="j_abrevSist" value="REC" />  	 	     	 	  
  <jsp:setProperty name="StatusId" property="colunaValue" value="cod_status" />  
  <jsp:setProperty name="StatusId" property="popupNome"   value="codStatus"  />  
  <jsp:setProperty name="StatusId" property="popupString" value="nom_status,SELECT cod_status||' - '||nom_status as nom_status,cod_status FROM TSMI_STATUS_AUTO ORDER BY cod_status asc" />                 	 
  
<jsp:useBean id="consultaAutosSemDigiBean" scope="session" class="REC.ConsultaAutosSemDigiBean" />          


<html>
<head>  
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsrLogado  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'MostraConsulta':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'Cancela':
	  fForm.acao.value=opcao
	  fForm.target= "_blank";
	  fForm.action = "acessoTool";  
	  fForm.submit();	  		  
	  break ; 	  
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function checa(fForm) { 
	//validacao de radio buttons sem saber quantos sao 
	marcado = -1 
	for (i=0; i<fForm.opcao.length; i++) { 
		if (fForm.opcao[i].checked) { 
		marcado = i 
		resposta = fForm.opcao[i].value 
		} 
	} 
	
	if (marcado == -1) { 
		return false; 
	} 
	return true; 
} 

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	
	if (fForm.codStatus.value=="") {
	   valid = false
	   sErro += "Status n�o selecionado. \n"
	}
	
	if (fForm.dataIni.value==0) {
	   valid = false
	   sErro += "Data inicio n�o informada. \n"
	}
	
	if (fForm.dataFim.value==0) {
	   valid = false
	   sErro += "Data Fim n�o informada. \n"
	}
	
	if (checa(fForm) == false){
       valid = false
	   sErro += "Informe a op��o de deseja verificar. \n"

    } 
	
    if (valid==false) alert(sErro)
	return valid ;
}

</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>	

<div  style="position:absolute; width:489px; overflow: visible; z-index: 1; top: 105px; left: 207px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<% if (request.getAttribute("quantAutosAlta").equals("N")){%>  
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr >
			  <td width="15%"><b>�rg�o&nbsp;</b></td>
			  <td colspan="3"><b>:</b>&nbsp;&nbsp;&nbsp;
			  		<select name="codOrgao"  > 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select></td>
	  </tr>		
	  <tr><td colspan="4" height="15"></td></tr>	 
	    <tr>
	       <td width="15%" ><b>Status</b></td>
		   <td colspan="3"><b>:</b>&nbsp;&nbsp;&nbsp;&nbsp;<jsp:getProperty name="StatusId" property="popupString" /></td>
	    </tr>
		<tr><td colspan="4" height="15"></td></tr>	
		<tr>
			  <td width="15%"  ><b>Data</b></td>
	  	      <td colspan="3"><strong>:</strong>&nbsp;&nbsp;&nbsp;
		  	    <input name="dataIni" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  	      <b>At�&nbsp;:</b>&nbsp;&nbsp;<input name="dataFim" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="" >		  </td> 	
      </tr>	
    	<tr><td colspan="4" height="15"></td></tr>	
		<tr>
		   <td width="15%"  ><b>Verificar</b></td>
	      <td colspan="2"><input tabindex="8"  type="radio" name="opcao" value="AR" <%= sys.Util.isChecked("opcao","AR")%> class="input"  style="height: 12px; width: 12px">
&nbsp;AR Digitalizado&nbsp;
  <input  tabindex="9"  type="radio" name="opcao" value="AI" <%= sys.Util.isChecked("opcao","AI")%> class="input" style="height: 12px; width: 12px">
&nbsp;AI Digitalizado&nbsp;
<input tabindex="10"  type="radio" name="opcao" value="FOTO" <%= sys.Util.isChecked("opcao","FOTO")%> class="input"  style="height: 12px; width: 12px">  
&nbsp;Foto Digitalizada&nbsp; </td> 
        <td width="17%" align="right">
			  	<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('MostraConsulta',this.form);">	
    	  <IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>
          		
    	</tr>
    </table>
<%}else{%>  
     <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
      <tr>
         <td width="15%" colspan="2">Foram encontrados&nbsp;<%=consultaAutosSemDigiBean.getAutos().size() %></td>
       <tr><td colspan="4" height="15"></td></tr> 	
	  </tr>
	  <tr> 
       <td align="right">
		  <button type="button" NAME="prosseguir"   style="width: 82px;height: 28px;  border: none; background: transparent;"  onClick="javascript: valida('MostraConsulta',this.form);">	
    	  <IMG src="<%= path %>/images/bot_prosseguir_det1.gif" align="left" ></button>
       </td>
	   
       <td  align="right">
		  <button type="button" NAME="cancelar"   style="width: 742px;height: 28px;  border: none; background: transparent;"  onClick="javascript: valida('Cancela',this.form);">	
    	  <IMG src="<%= path %>/images/bot_cancelar_det1.gif" align="left" ></button>
       </td>          		
      </tr> 
  </table>
<%}%>
 
 
    
    
</div>     



<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= consultaAutosSemDigiBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.RelatForm.codOrgao.length;i++){
		if(document.RelatForm.codOrgao.options[i].value==njint){
			document.RelatForm.codOrgao.selectedIndex=i;	  
		}
	}
} 
</script>			  
</form>    
</BODY> 
</HTML>

