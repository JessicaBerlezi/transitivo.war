<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama os beans -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
	case 'R':
		close() ;
		break;
	case 'O':
   		document.all["MsgErro"].style.visibility='hidden';
	  	break;  
   case 'V':
   		document.all["MsgErro"].style.visibility='visible';
	  	break;
	case 'VisFotoDigImagem' :
		fForm.acao.value=opcao
		fForm.target= "_blank";
		fForm.action = "acessoTool";  
		fForm.submit();
		break;		
   }
}


function mostraImagem(mypage, myname, w, h, scroll) {		
	var winl = (screen.width - w) / 2;
	//var wint = (screen.height - h) / 2;
	var wint=0;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
	win = window.open(mypage, myname, winprops)
	if (parseInt(navigator.appVersion) >= 4) { 
		win.window.focus(); 
		win.window.print();
	}
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AIForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
  <input name="acao" type="hidden" value=' '>	
  <input name="numAutoInfracao" type="hidden" value='<%=AutoInfracaoBeanId.getNumAutoInfracao()%>'>
  <input name="numPlaca"        type="hidden" value='<%=AutoInfracaoBeanId.getNumPlaca()%>'>				
  <input name="numProcesso"     type="hidden" value='<%=AutoInfracaoBeanId.getNumProcesso()%>'>
  
  
<% if (!AutoInfracaoBeanId.getFotoDigitalizada().getMsgErro().equals("")) { //se  houver erro, mostra cabecalho e msg de erro.%>  

<!--DIV FIXA-->
<div id="req1" style="position:absolute; left:50px; top:80px; width:738px; height:75px; z-index:5; overflow: auto; visibility: visible;"> 
<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA AUTO DIGITALIZADO</b></td>        
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td width="90"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="108"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="323"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="199"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan=2><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
				&nbsp;(<%=AutoInfracaoBeanId.getDatStatus() %>)</td>
      </tr>
      <tr> <td height="10"></td>  </tr>
  </table>
  </div>
<!--FIM DIV FIXA-->
<div id="req2" style="position:absolute; left:50px; top:138px; width:738px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr><td height="50"></td></tr>
	  <tr><td bgcolor="#f5d6cc" align="center" height="25">&nbsp;&nbsp;<strong><%=AutoInfracaoBeanId.getFotoDigitalizada().getMsgErro()%></strong></td> </tr>	
	</table>
</div>

<%} else  { //se n�o houver erro, mostra imagem%>
  

  
<!--AREA DE RESULTADO DO MODULO - TABELA COM 2 COLUNAS-->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td align="left">	
	<div id="menulateral" style="position:absolute; left:40px; top:90px; width:110px; height:250px;  z-index:1; background-color: transparent; overflow: visible; visibility: visible;"> 
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<%String mostrarLocal = (String) request.getAttribute("mostrarLocal");
 		  if ((mostrarLocal != null) && (mostrarLocal != "N")) {%>
        <tr> 
			<td><strong><font color="#666666">N&uacute;mero do Auto</font></strong></td>
		</tr>
		<tr> 
			<td bgcolor="#faeae5" height="18"><%=AutoInfracaoBeanId.getNumAutoInfracao()%></td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="5"></td>
		</tr>
		<tr> 
			<td><strong><font color="#666666">Data de Digitaliza&ccedil;&atilde;o</font></strong></td>
		</tr>
		<tr> 
			<td bgcolor="#faeae5" height="18"><%=AutoInfracaoBeanId.getFotoDigitalizada().getDataDigitalizacaoString()%></td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="10"></td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="10"></td>
		</tr>
		<%}%>
		<tr> 
			<td>
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>					
					<% if (!AutoInfracaoBeanId.getFotoDigitalizada().getImagem().equals("")) {%>				
					<td align="center">
						<button style="border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript:valida('VisFotoDigImagem',this.form)";>
							<img src="<%= path %>/images/REC/bot_imprimir_img.gif" width="71" height="26" alt="" border="0">
						</button>
					</td>
					<%}%>
				</tr>
				</table>
			</td>
		</tr>		
		</table>	
	</div>	
	</td>
</tr>
</table>
<!--FIM DA AREA DE RESULTADO-->

<div id="imagem" align="center" style="position:absolute; left:164px; top: 85px; width:580px; height: 422px;  z-index:99; background-color: transparent; overflow:auto; visibility: visible;">
<%--    <img id="imagemFoto" alt="Foto do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7"  --%>
<%--    src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getFotoDigitalizada().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getFotoDigitalizada().getParametro(ParamSistemaBeanId)%>&header=#"  --%>
<!--    width="570" height="412">  -->

	<img id="imagemFoto" alt="Foto do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7"  src="<%= path %>/FotoDigitalizada/<%=AutoInfracaoBeanId.getNumAutoInfracao()%>.jpg" width="570" height="412">
	
</div>

<%}//fim do else  %>
 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
 <!--FIM_CORPO_sistema-->
</form>
</BODY>
</HTML>
