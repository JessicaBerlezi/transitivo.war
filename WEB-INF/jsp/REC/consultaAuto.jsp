<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>

<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>

<jsp:useBean id="NoticiaId" scope="session" class="ACSS.NoticiaBean" /> 
<jsp:useBean id="AvisoId"   scope="session" class="ACSS.AvisoBean" /> 
<jsp:useBean id="consultaAutoId"     scope="session" class="REC.consultaAutoBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId"     scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
    int kyz = 0; 
    int nyz=0;
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#be7272";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<style type="text/css">
.linhas td{border: 1px solid #ff0000;}
</style>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			
			break;
	   case 'ConsultaAuto':
	   		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
			 	}
		 	}
		  break;
      case 'ImprimeAuto':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;		  
	  case 'Novo':
		  fForm.acao.value=opcao;
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;
	  case 'N':
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;		  
      case 'ImprimeNots':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;	
      case 'RecebNotif':
      		if (confirm("As Notifica��es devem estar impressas e assinadas. Confirma?")) {
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	   		
			}
			break;		 		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["ConsultaAuto"].target= "_self";
	document.all["ConsultaAuto"].action = "acessoTool";  
	document.all["ConsultaAuto"].submit();	 
}
function MostraAut(opcoes,fForm,numplaca,numauto,autoAntigo) {	
	document.all["acao"].value=opcoes;
	document.all["mostraplaca"].value=numplaca;
	document.all["mostranumauto"].value=numauto;
	document.all["mostraAutoAntigo"].value=autoAntigo;
   	document.all["ConsultaAuto"].target= "_blank";
    document.all["ConsultaAuto"].action = "acessoTool";  
   	document.all["ConsultaAuto"].submit();	 
}

function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	if (   (trim(fForm.numRenavam.value) =='')   && (trim(fForm.numPlaca.value) =='')
		&& (trim(fForm.numCpf.value)=='')        && (trim(fForm.numProcesso.value) =='')
		&& (trim(fForm.numAutoInfracao.value)=='') ) {
			sErro += "Nenhuma op��o selecionada. \n"
			valid = false ;	
	}
	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}
			
	<% if ("REC0680".equals(consultaAutoId.getSigFuncao())==false) { %>	
		if (atuRadio(fForm.indPagoradio,fForm.indPago)) {
		  valid = false
		  sErro = sErro + "Indicador de Pagamento n�o selecionado. \n"
		}
		if (atuRadio(fForm.indFaseradio,fForm.indFase)) {
		  valid = false
		  sErro = sErro + "Indicador de Fase n�o selecionado. \n"
		}
		<% } %>
	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">

<form name="ConsultaAuto" method="post" action="">
<!-- Div de mensagem de Noticia e Avisos  -->

<%@ include file="../sys/MensagemAviso_Diretiva.jsp" %> 

<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0">
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0">
		<%}%>
		</button>
		</td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
	<!--FIM T�TULO DO M�DULO-->

<% if ("REC0680".equals(consultaAutoId.getSigFuncao())) { %>	
	<%@ include file="consultaAutoReceb.jsp" %>
<%} else { %>	
	<%@ include file="consultaAutoCons.jsp" %>
<% } %>

<div id="cons3" style="position:absolute; left:50px; top:180px; right:15px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
 <!--Dados vari�veis "A" para evento 14-->
    <table width="100%" border="0" cellpadding="0" cellspacing="1">
      <tr bgcolor="#FFFFFF"> 
        <td height="14" width="59%">
			 <% if (consultaAutoId.getAutos().size()>0) {	%>	
			<font color="#000000">&nbsp;LOCALIZADO(s)&nbsp;<%= consultaAutoId.getAutos().size()%>&nbsp;AUTO(s)&nbsp;&nbsp;&nbsp;
			<%String msg=(String)request.getAttribute("processoA"); if (msg==null) msg="";
			if (msg.length()>0) {%>
			
			<span style="color:#ff0000"><%= msg %></span>
			
			<% } %>
			</font>
			<% } %>
		</td>
        <td width="41%" align=right>
			<font color="#000000">&nbsp;Ordem:&nbsp;<%= consultaAutoId.getNomOrdem()%>&nbsp;</font>
		</td>		
      </tr>
      </table>
</div>
				
<div class="divPrincipal" style="top: 196px; height:20%;">
<div class="divTitulos" id="titulo">
      <table border="0" cellspacing="1" cellpadding="0" width="100%">
      <tr> 
        <td width="60" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Orgao',this.form);" class="branco">�rg�o</a></td>
		<td width="100" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Auto',this.form);" class="branco">N&ordm; Auto</a></td>
        <td width="100" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Placa',this.form);" class="branco">Placa</a></td>
        <td width="350" style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: Classificacao('Processo',this.form);" class="branco">Processo</a></td>
        <td style="text-align:left; color: #ffffff; height: 18px; background-color: #be7272; font-weight: bold;">&nbsp;&nbsp;&nbsp;Dt Notif.</td>
      </tr>
	</table>
</div>

 
<div class="divCorpo" id="conteudo" style="height:100%;">  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" id="autos">
	  <%
	   if (consultaAutoId.getAutos().size()>0) {
	  	int a =0;
	  	int seq = 0;
		String cor   = (a%2==0) ? "#faeae5" : "#ffffff";
		String fonte="";		
		Iterator it = consultaAutoId.getAutos().iterator() ;
		REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
		    seq++;
			not   = (REC.AutoInfracaoBean)it.next() ;				
			cor   = (a%2==0) ?  resultline  : "#ffffff";
			fonte = (a%2==0) ? "#ffffff" : "#000000";  
		%>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; " name="line<%=a%>" id="line<%=a%>"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');"> 
	        <td width="60" rowspan="2" height="14" align="center">&nbsp;<%=seq %> </td>
			<td width="100" rowspan="2">&nbsp;<strong><%=not.getNumAutoInfracao().trim()%></strong></td>
        	<td width="100" rowspan="2">&nbsp;<strong><%=not.getNumPlaca().trim()%></strong></td>
	        <td width="350">&nbsp; Ata: <%= not.getDsAta() %> / N�mero do Processo: <%= not.getNumProcesso().trim()%></td>
			<td>&nbsp;
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	<%= not.getDatNotificacao().trim()%>
			<% } %>	        
        	</td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');"> 
    	    <td colspan="2">
    	    	&nbsp;Situa��o:&nbsp;<%= not.getSituacao().trim()%>&nbsp;&nbsp;<%=not.getIndParcelamento().trim()%>
    	    	
    	    					<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= not.getNomStatus().trim()%>
				<% } %>
			<%
			 String desc = "";
		    if (not.getIdentAutoOrigem().length()>0 && !not.getIdentAutoOrigem().equals("0") ) {
		    	%>
        	<br>&nbsp;AUTO RENAINF - &nbsp;<%=not.getDescIdentAutoOrigem()%>&nbsp;
	        <%}%>	
	        <% if (not.getNumAutoOrigem().length()>0) { %>	
		          <br>&nbsp;Ft. Gerador:&nbsp;<%=not.getNumAutoOrigem()%>&nbsp;(<%=not.getNumInfracaoOrigem() %>)
			<% } %>		        	        
	        </td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"> 
	        <td height="14" rowspan="2" align="center">&nbsp;<%=not.getSigOrgao() %></td>    	    
        	<td colspan="2">
        		<a href="#" onClick="javascript: Classificacao('Data',this.form);">
        			&nbsp;Data:&nbsp;<%= not.getDatInfracao()%>&nbsp;&nbsp;<%=not.getValHorInfracaoEdt() %>
        		</a>
        	</td>
	        <td height="14" colspan="2">&nbsp;Infra��o:&nbsp;<%=not.getInfracao().getCodInfracao().trim() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscInfracao().trim() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000;"
			onclick="javascript:MostraAut('MostraAuto',this.form,'<%=not.getNumPlaca() %>','<%=not.getNumAutoInfracao() %>','<%=not.getAutoAntigo()%>');"> 
        	<td colspan="2">&nbsp;Vencimento:&nbsp;<%= not.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan="2">&nbsp;Local:&nbsp;<%=not.getDscLocalInfracao().trim()%>&nbsp;&nbsp;<%= not.getNomMunicipio().trim() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; ">
         <%
        if ( (not.getIdentOrgaoEdt().length()>0) || (not.getNumInfracaoRenainf().length()>0) ){ %>
	        <td height="14" rowspan="3" align="center">&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>    	    	<%}else{%>
	        <td height="14" rowspan="2" align="center">&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>     	    	<%}%>
            
        	<td colspan="2">&nbsp;Valor:&nbsp;<%= not.getInfracao().getValRealEdt().trim()%>&nbsp;</td>											
	        <td height="14" colspan="2">
	          	<a href="#" onClick="javascript: Classificacao('Enquadramento',this.form);">
		        	&nbsp;Enquadramento:&nbsp;<%=not.getInfracao().getDscEnquadramento().trim() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscGravidade().trim() %>&nbsp;-&nbsp;<span style="color: #ff0000"><strong>Pontos:&nbsp;<%= not.getInfracao().getNumPonto().trim() %></strong></span></a></td>
	        	
		</tr>
		

		<%
        if ( (not.getIdentOrgaoEdt().length()>0) || (not.getNumInfracaoRenainf().length()>0) ){ %>			
        
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; "> 
		    <%
		    if (not.getIdentOrgaoEdt().length()>0) {%>
	        <td height="14" colspan="2">&nbsp;Ident. �rg�o:&nbsp;<%= not.getIdentOrgaoEdt().trim()%>&nbsp;&nbsp;</td>    	    
	        <%}else{%>
	        <td height="14" colspan="2">&nbsp;</td>    	    	        
		    <%}%>			        
	        
		    <%
		    if (not.getNumInfracaoRenainf().length()>0) {%>
        	<td colspan="2">&nbsp;Auto Renainf:&nbsp;<%=not.getNumInfracaoRenainf().trim()%>&nbsp;</td>
	        <%}else{%>
        	<td colspan="2">&nbsp;</td>        
		    <%}%>	
		    
		</tr>
		<%}%>		
		
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:#000000; "> 
	        <td height="14" colspan="5">          	  <a href="#" onClick="javascript: Classificacao('Responsavel',this.form);">
	        		&nbsp;Propriet�rio:&nbsp;<%=not.getProprietario().getNomResponsavel().trim() %>
        	</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resp. Pontos :&nbsp;
				<% if (not.getCondutor().getNomResponsavel().length()>0) { %>
					<%=not.getCondutor().getNomResponsavel().trim() %>
				<% } else { %>Condutor n�o Identificado	<% } %>
			</td>
		</tr>		
    	<tr><td height="2" colspan="5" bgcolor="#666666"></td></tr>		
		<% a++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>	
		<tr><td height="40" colspan="5" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align="center" colspan="5"><strong><%= msg %></strong></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
  </div> 
  
 </div> 
<!--FIM_CORPO_sistema--> 
<input name="acao"              type="hidden" value=' '>
<input name="mostraplaca"       type="hidden" value=" ">
<input name="mostranumauto"     type="hidden" value=" ">
<input name="mostraAutoAntigo"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">

<!--Bot�o retornar--> 
<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!-- Rodap�-->
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

<script>document.ConsultaAuto.numPlaca.focus();</script>	        

<!--Div Erros-->
<%String msgErro = consultaAutoId.getMsgErro();
  String msgOk   = consultaAutoId.getMsgOk();	
  String msgErroTop    = "245 px";	
  String msgErroLeft   = "80 px";  
%>
<%@ include file="EventoErro_Diretiva.jsp" %>
<!--FIM_Div Erros-->

</form>
</body>
</html>