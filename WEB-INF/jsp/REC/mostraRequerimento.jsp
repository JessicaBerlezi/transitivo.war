<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
	   break;
	   case 'O':  // Esconder os erro
	    	close() ;
	   break;	
	   case 'Imprimerequerimento':  
			 fForm.acao.value=opcao;
	   		 fForm.target= "_blank";
	    	 fForm.action = "acessoTool";  
	   		 fForm.submit();
	   break;
  	}
}
function downloadDocumento(tipoDocumento, fForm){
	fForm.nomArquivo.value = 'DOC_WEB';
	fForm.tipoDocumento.value = tipoDocumento;
	fForm.target= "_blank";
	fForm.action = "download";
	fForm.submit();
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AbrirRecForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="numAutoInfracao" value="<%=AutoInfracaoBeanId.getNumAutoInfracao() %>">
<input type="hidden" name="numPlaca" value="<%=AutoInfracaoBeanId.getNumPlaca()%>">
<input type="hidden" name="numProcesso" value="<%=AutoInfracaoBeanId.getNumProcesso()%>">
<input type="hidden" name="contexto" value="DIR_ARQUIVOS_DIGITALIZADO_">
<input name="codArquivo"  type="hidden" value=''>
<input name="nomArquivo"  type="hidden" value=''>
<input name="extensao"    type="hidden" value=''>
<input name="tipoDocumento"  type="hidden" value=''>

<!--IN�CIO_CORPO_sistema-->
<div id="req1" style="position:absolute; left:50px; right: 15px; top:80px; height:90px; z-index:5; overflow: visible; visibility: visible;">
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
      <tr bgcolor="#faeae5" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA REQUERIMENTOS &nbsp;&nbsp;
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('Imprimerequerimento',this.form);"> 
              <img src="<%= path %>/images/REC/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">        
		</button>

		<%if (AutoInfracaoBeanId.possuiDocumentoRequerimentoWeb()){ %>
			<input value="Documento" type="button" style="text-decoration:underline; border: 0px; background-color: ButtonFace; height: 16px; cursor: pointer;" onClick="javascript:downloadDocumento('1',this.form);"/>
		<%} %>
		
		<%if (AutoInfracaoBeanId.possuiCRLVRequerimentoWeb()){ %>
			<input value="CRLV" type="button" style="text-decoration:underline; border: 0px; background-color: ButtonFace; height: 16px; cursor: pointer;" onClick="javascript:downloadDocumento('2',this.form);"/>
		<%} %>
		
		<%if (AutoInfracaoBeanId.possuiProcuracaoRequerimentoWeb()){ %>
			<input value="Procura��o" type="button" style="text-decoration:underline; border: 0px; background-color: ButtonFace; height: 16px; cursor: pointer;" onClick="javascript:downloadDocumento('3',this.form);"/>
		<%} %>
		
        </b></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#faeae5" height="18">
        <td width="90"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="108"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="323"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="199"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#faeae5" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan=2><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
				&nbsp;(<%=AutoInfracaoBeanId.getDatStatus() %>)</td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#faeae5" height="18">
		<td><strong>&nbsp;&nbsp;Agente :&nbsp;</strong></td>
        <td><%=AutoInfracaoBeanId.getDscAgente() %></td>
        <td><strong>Unidade :&nbsp;</strong><%=AutoInfracaoBeanId.getDscUnidade() %></td>
        <td><strong>Lote :&nbsp;</strong><%=AutoInfracaoBeanId.getDscLote() %></td>
      </tr>      
      <tr> <td height="5"></td>  </tr>
  </table>    
</div>
<!--FIM DIV FIXA-->
<div class="divPrincipal" style="top: 168px; height:20%;">
<div id="req2" style="position:relative; left:0px; right: 0px; height:180px; z-index:60; width: 100%;overflow: auto; visibility: visible;">  
  <%
	Iterator itx  = AutoInfracaoBeanId.getRequerimentos().iterator() ;
	REC.RequerimentoBean req = new REC.RequerimentoBean();	
	ACSS.OrgaoBean acssOrg = new ACSS.OrgaoBean();
	if (!AutoInfracaoBeanId.getCodStatus().equals("90")){
		if 	(itx.hasNext()){	
	  	while (itx.hasNext()) {
			req =(REC.RequerimentoBean)itx.next() ;
			acssOrg.Le_Orgao(req.getCodOrgaoLotacaoDP(),0);	
  %>
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
      <tr> <td height="7" colspan=3></td></tr>	  	 
      <tr bgcolor="#f5d6cc"> 
        <td width="350" class="espaco2" ><strong>&nbsp;&nbsp;N&ordm;&nbsp;REQUERIMENTO :</strong>&nbsp;<%=req.getNumRequerimento()%></td>
        <td width="270" class="espaco2" >&nbsp;<strong>&nbsp;<%=req.getNomTipoSolic()%></strong></td>
        <td align="right" class="espaco2"><strong>Data :</strong>&nbsp;<%=req.getDatRequerimento()%>&nbsp;&nbsp;</td>
      </tr>
      <tr> <td height="2" colspan=3></td>
      </tr>	  
    </table>  
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr bgcolor="#faeae5"> 
        <td ><strong>&nbsp;&nbsp;</strong><strong>Respons&aacute;vel :</strong></td>
        <td colspan=3><%=  (req.getCodTipoSolic().equals("TR")) ? req.getNomCondutorTR() : req.getNomResponsavelDP() %></td>
        <td align="right"><strong><%=req.getNomStatusRequerimento()%></strong>&nbsp;&nbsp;</td>      
      </tr>
      <tr><td height="2"></td></tr>
      <tr bgcolor="#faeae5"> 
      	<td width="90" ><strong>&nbsp;&nbsp;Aberto por :</strong>&nbsp;</td>
        <td width="108" ><%=req.getNomUserNameDP() %></td>
        <td width="82" ><strong>Org�o Lota��o :</strong></td>
      	<td width="241" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcDP()%>&nbsp;&nbsp;</td>
      </tr>
      <!--Informa��es da Guia de Remessa-->
      
      <% if ((req.getCodRemessa().length()>0)) {%>
      <tr> <td height="2" colspan=5></td></tr>	  	 
      <tr bgcolor="#faeae5"> 
        <td width="220" colspan=5><strong>&nbsp;&nbsp;N&ordm;&nbsp;Guia de Remessa :</strong>&nbsp;<%=req.getCodRemessa()%></td>        
      </tr>
      <tr> <td height="1" colspan=5></td></tr>	
      <tr bgcolor="#faeae5">         
        <td colspan=2><strong>&nbsp;&nbsp;Data Envio :</strong>&nbsp;<%=req.getDatEnvioGuia() %></td>
        <td colspan=2 ><strong>&nbsp;Enviado Por :</strong>&nbsp;<%=req.getNomUserNameEnvio()%>&nbsp;&nbsp;</td>
        <td></td>        
      </tr>
      <tr> <td height="2" colspan=5></td></tr>	
      <tr bgcolor="#faeae5">         
        <td colspan=2>&nbsp;<strong>&nbsp;Data Recebimento :</strong>&nbsp;<%=req.getDatRecebimentoGuia() %></td>
        <td colspan=2><strong>&nbsp;Recebido Por :</strong>&nbsp;<%=req.getNomUserNameRecebimento()%>&nbsp;&nbsp;</td>
		<td width="60" colspan=3></td>
      </tr>
      <tr> <td height="2" colspan=5></td></tr>	    
    <%}%>
 <!--Fim das Informa��es da Guia de Remessa-->    
      <% if(req.getNumGuiaDistr().length()>0) { %>
	      <tr><td height="2"></td></tr>
    	  <tr bgcolor="#faeae5"> 
      		<td class="espaco2"><strong>&nbsp;&nbsp;Guia Distrib.:</strong>&nbsp;</td>
	        <td><%=req.getNumGuiaDistr() %></td>
    	    <td></td>
      		<td></td>
	      	<td></td>
    	  </tr>  
      <% } %> 

        
  </table>
  <% if ( (req.getCodTipoSolic().equals("TR")) && (req.getNumCNHTR().length()>0) ) {
	  	if (req.getCodOrgaoLotacaoTR().equals("")==false)
			acssOrg.Le_Orgao(req.getCodOrgaoLotacaoTR(),0);
		else acssOrg.setNomOrgao("");
  %>
<!--Requerimento Troca Real Infrator-->
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr><td height="5"></td></tr>	
    <tr bgcolor="#faeae5"> 
      <td width="59" valign="top"><strong>&nbsp;&nbsp;Nome :</strong></td>
      <td width="300" valign="top"><%=req.getNomCondutorTR()%></td>
      <td width="72" valign="top"><strong>Endere&ccedil;o :</strong></td>
      <td width="289" valign="top"><%=req.getTxtEnderecoTR()%></td>
    </tr>
    <tr><td height="2"></td></tr>	
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr bgcolor="#faeae5"> 
      <td width="59" valign="top">&nbsp;&nbsp;<strong>Bairro :</strong></td>
      <td width="185" valign="top"><%=req.getNomBairroTR()%></td>
      <td width="57" valign="top"><strong>Munic&iacute;pio :</strong></td>
      <td width="172" valign="top"><%=req.getNomCidadeTR()%></td>
      <td width="22" valign="top"><strong>UF :</strong></td>
      <td width="22" valign="top"><%=req.getCodUfTR()%></td>
      <td width="29" valign="top"><strong>&nbsp;</strong></td>
      <td width="80" valign="top">&nbsp;</td>
      <td width="29" valign="top" class="tdborda"><strong>CEP :</strong></td>
      <td width="65" valign="top" class="tdborda"><%=req.getNumCEPTREdt()%></td>
    </tr>
    <tr><td height="2"></td></tr>		
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr bgcolor="#faeae5"> 
      <td width="59" valign="top" bgcolor="#faeae5" >&nbsp;&nbsp;<strong>CPF :</strong></td>
      <td width="185" valign="top" bgcolor="#faeae5" ><%=req.getNumCPFTREdt()%></td>
      <td width="136" valign="top" bgcolor="#faeae5" ><strong>N� CNH / Perm. p/ Dirigir :</strong></td>
      <td width="94" valign="top" bgcolor="#faeae5" ><%=req.getIndTipoCNHTR() %>&nbsp;-&nbsp;<%=req.getNumCNHTR()%></td>
      <td width="47" valign="top" ><strong>UF CNH</strong><strong> :</strong></td>
      <td width="199" valign="top" ><%=req.getCodUfCNHTR()%></td>
    </tr>
  </table>
  <% } //fim bloco TR 
     if ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodParecerPJ().equals("")==false))  {
	   	if (req.getCodOrgaoLotacaoPJ().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoPJ(),0);
		else	acssOrg.setNomOrgao("");
  %>
	<!--Requerimento Parecer Jur�co-->
  	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5"></td></tr>	
      <tr bgcolor="#f5d6cc"> 
        <td  colspan=2>&nbsp;&nbsp;<strong>PARECER JUR�DICO:</strong>&nbsp;<%= req.getNomResultPJ() %></td>
        <td  colspan=2><strong>Respons�vel pelo Parecer:&nbsp;</strong><%=req.getNomRespPJ() %></td>
        <td><strong>Data Parecer:</strong>&nbsp;<%=req.getDatPJ() %>&nbsp;&nbsp;</td>
      </tr>
      <tr> <td height="2"></td> 
      </tr>
    <tr> 
  	  <td width="90" valign="top" bgcolor="#faeae5">&nbsp;&nbsp;<strong>Motivo :</strong></td>
      <td  colspan=4 bgcolor="#faeae5">
    	<textarea cols="120" rows="3" readonly style="border: 0px none;  text-align: left; background: transparent;" ><%=req.getTxtMotivoPJ() %></textarea>
      </td>
    </tr>
    <tr> <td height="2"></td></tr>
      <tr bgcolor="#faeae5"> 
      	<td width="90" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="108" ><%=req.getNomUserNamePJ() %></td>
        <td width="82" ><strong>Org�o Lota��o :</strong></td>
      	<td width="241" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcPJ()%>&nbsp;&nbsp;</td>
      </tr>
  </table>        
  
  
  
  
  <!--FIM PARECER JURIDICO--> 
  <% } //IF DO PARECER JURIDICO
    if ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodJuntaJU().equals("")==false))  {
	     if (req.getCodOrgaoLotacaoJU().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoJU(),0);
		 else	acssOrg.setNomOrgao("");
  %>  
	<!--Requerimento JUNTA-->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5" colspan=5></td> </tr>
      <tr bgcolor="#f5d6cc"> 
        <td colspan=2>&nbsp;&nbsp;<strong>JUNTA:&nbsp;</strong><%=req.getSigJuntaJU() %></td>
        <td colspan=2 ><strong>Relator:&nbsp;</strong><%=req.getNomRelatorJU() %></td>			
        <td><strong>Data:</strong>&nbsp;<%=req.getDatJU() %>&nbsp;&nbsp;</td>
      </tr>
	  <tr><td height="2" colspan=5></td></tr>
      <tr bgcolor="#faeae5"> 
      	<td width="90" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="108" ><%=req.getNomUserNameJU() %></td>
        <td width="82" ><strong>Org�o Lota��o :</strong></td>
      	<td width="241" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcJU()%>&nbsp;&nbsp;</td>
      </tr>    
   </table> 
  <% } //IF JUNTA
   if  ((req.getCodTipoSolic().equals("TR")==false) && (req.getCodResultRS().equals("")==false))  {
       	if (req.getCodOrgaoLotacaoRS().equals("")==false) acssOrg.Le_Orgao(req.getCodOrgaoLotacaoRS(),0);
		else acssOrg.setNomOrgao("");
  %>
	<!--Requerimento RESULTADO-->
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td height="5"></td> </tr>
      <tr bgcolor="#f5d6cc"> 
        <td colspan=3>&nbsp;&nbsp;<strong>RESULTADO:</strong>&nbsp;<%= req.getNomResultRS() %></td>
        <td></td>
        <td width="200"><strong>Data Resultado :</strong>&nbsp;<%=req.getDatRS() %></td>
      </tr>
      <tr> <td height="2"></td>  
      </tr>
      <tr bgcolor="#faeae5"> 
	     <td width="86">&nbsp;&nbsp;<strong>Motivo :</strong></td>
	     <td colspan=4>
		     <textarea cols="120" rows="3" style="border: 0px none; text-align: left; background: transparent;"><%=req.getTxtMotivoRS() %></textarea>
	     </td>
	  </tr>
	  <%if (!req.getCodMotivoDef().equals("")){%>
      <tr><td height="2" colspan=5></td></tr>
	  <tr bgcolor="#faeae5">    
         <td colspan=5>&nbsp;&nbsp;<strong>Motivo do Deferimento:</strong>&nbsp;<%=req.getNomMotivoDef()%></td>
      </tr>
      <%}%>
      <tr> <td height="2" colspan=5></td> </tr>	  
      <tr bgcolor="#faeae5"> 
      	<td width="86" >&nbsp;&nbsp;<strong>Executado por :</strong></td>
        <td width="111" >&nbsp;<%=req.getNomUserNameRS() %></td>
        <td width="84" ><strong>Org�o Lota��o :</strong></td>
      	<td width="239" ><%=acssOrg.getSigOrgao()%></td>
      	<td ><strong>Data de Proc.:</strong>&nbsp;<%=req.getDatProcRS()%>&nbsp;&nbsp;</td>
      </tr>    
   </table>
	<%				} //IF RESULTADO
			} //Loop do while 
		} // If tem requerimento
		else {
			%>
			<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
		      <tr><td height="45"></td></tr>
			  <tr><td bgcolor="#f5d6cc" align="center">&nbsp;&nbsp;<strong>AUTO SEM REQUERIMENTO</strong></td> </tr>	
			</table>
		<%	}	// If do codStatus 90
	}else{
		%>
		<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
	      <tr><td height="45"></td></tr>
		  <tr><td bgcolor="#f5d6cc" align="center">&nbsp;&nbsp;<strong>AUTO CANCELADO</strong></td> </tr>	
		</table>
	<% }	%>
</div>
</div>

<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<jsp:include page="../sys/DivErro.jsp" flush="true">
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 

</form>
</body>
</html>