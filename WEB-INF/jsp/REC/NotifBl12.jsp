<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.io.File" %>

<!-- Chama o Objeto da Infracao -->
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="top" class="fontmedia"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr> 
		  <td width="12%" align="center"> 
            <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
              <tr> 
              <% File imagem = new File(path + "/images/logos/" + UsuarioBeanId.getSigOrgaoAtuacao() + ".gif");
                 if (imagem.exists()) {%>
			    <td><img src="<%=path%>/images/logos/<%=UsuarioBeanId.getSigOrgaoAtuacao() %>.gif" width="65" height="60"></td>
		<%} else {%>
			    <td><img src="<%=path%>/images/logos/inv.gif" width="65" height="60" ></td>  
		<%}%>
			</tr>
        <tr> 
        </table>
    </td>
    <td width="68%" valign="top" class="fontmaior"> 
    	<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
			<tr>
			  <td class="fontmaior"><strong><%=AutoInfracaoBeanId.getNomOrgao() %></strong></td>
			</tr>
			<tr>
			  <td class="fontmedia"><STRONG><%=UsuarioBeanId.getSigOrgaoAtuacao() %></strong></td>
			</tr> 
		    <tr>
			  <td class="fontmedia"><STRONG><%=NotifBeanId.getDscNotificacao()%></strong></td>
			</tr>
			<tr>
			  <td class="fontmedia"><STRONG>2� VIA - SMIT</strong></td>
			</tr>
		</table>	   
     </td>
     <td width="20%" align="center"> 
            <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0" class="semborda">
              <tr> 
                <td width="30%">N&ordm; Auto:</td>
                <td width="70%"><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong></span></td>
   		</tr>
        <tr> 
          <td>Placa:</td>
          <td><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumPlaca() %></strong></span></td>
        </tr>
        <tr>
          <td>N&ordm; AR:</td>
          <td><span class="fontmedia"><strong><%=AutoInfracaoBeanId.getNumNotificacao() %></strong></span></td>
        </tr>
 </table>
      
          </td>
   </tr>
</table>
      <!--Linha-->
      <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 
		<td height="1" bgcolor="#000000"></td>
	</tr>
 </table>
<!--FIM Linha-->

      </td>
</tr>
<tr> 
      <td height="277" align="center" valign="top"> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top"> 
            
            <td width="50%" align="center" valign="top" class="fontmedia">
			
			<table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                      <tr> 
                        <td width="66%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Descri&ccedil;&atilde;o 
                          da Marca / Modelo</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></span></td>
                        <td width="1%" valign="top" class="semborda">&nbsp;</td>
                        <td width="33%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Esp&eacute;cie 
                          do Ve&iacute;culo</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %></span></td>
                      </tr>
                    </table></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                        <td width="34%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Categoria 
                          do Ve&iacute;culo</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscCategoria() %></span></td>
                      <td width="1%">&nbsp;</td>
                        <td width="38%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Tipo 
                          do Ve&iacute;culo</strong></span><br>
                        <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscTipo() %></span></td>
                      <td width="1%" valign="top" class="semborda">&nbsp;</td>
                      <td width="26%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Cor 
                        do Ve&iacute;culo</strong></span><br>
                        <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscCor() %></span></td>
                    </tr>
                  </table>
				  </td>
              </tr>
            </table>			 
              <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr>
         <td valign="top">
		 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
               <tr> 
                        
                        <td width="100%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Nome 
                          do Respons&aacute;vel</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></span></td>
                      
               </tr>
		  </table>
		</td>
	</tr>
</table>			
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> 
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                        <td width="86%" height="44" valign="top" class="tdborda" style="text-align: justify; padding-left: 3px; padding-right: 3px;"><span class="fontseis"><strong>Descri&ccedil;&atilde;o 
                          da Infra&ccedil;&atilde;o</strong></span><br>
                          <span class="fontmenor"><%=AutoInfracaoBeanId.getInfracao().getDscInfracao() %></span></td>
                          <td width="1%">&nbsp;</td>
                        <td width="14%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d. 
                          Infr.</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao().trim() %></span> </td>  
					</tr>
                  </table></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                        <td width="100%" height="32" valign="top" class="tdborda" style="padding-left: 2px;"><span class="fontseis"><strong>Local 
                          da Infra&ccedil;&atilde;o</strong></span><br>
                          <span class="fontmenor"><%=AutoInfracaoBeanId.getDscLocalInfracao() %></span> 
                        </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
			
			<table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                      <tr> 
                        <td width="20%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d. 
                          Munic&iacute;pio</strong></span><br> 
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodMunicipio() %></span> 
                        </td>
                        <td width="1%" valign="top" class="semborda">&nbsp;</td>
                        <td width="79%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Munic&iacute;pio 
                          da Infra&ccedil;&atilde;o</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNomMunicipio() %></span></td>
                      </tr>
                    </table></td>
              </tr>
            </table>
			
            <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                        <td width="45%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Enquadramento</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscEnquadramento() %></span></td>
                      <td width="1%">&nbsp;</td>
                        <td width="42%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Data/Hora 
                          da Infra&ccedil;&atilde;o</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDatInfracao() %>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></span></td>
                        <td width="1%" valign="top" class="semborda">&nbsp;</td>
                        <td width="11%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Pontos</strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getNumPonto() %></span> </td>
                    </tr>
                  </table></td>
              </tr>
            </table>		
			
            
          
                   
            
            </td>
		  <td width="50%" height="175" align="center" valign="top" class="fontmedia"> 
             <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                <tr>                 
                   <% if ( AutoInfracaoBeanId.getTemFotoDigitalizada()) { %>							
						<td align="center" height="252">
						
<%--  						 <img src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getFotoDigitalizada().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getFotoDigitalizada().getParametro(ParamSistemaBeanId)%>&header=#"  width="360" height="252"> --%>
					    
							<img src="<%=path%>/FotoDigitalizada/<%=AutoInfracaoBeanId.getNumAutoInfracao()%>.jpg" width="360" height="252">
	
					    </td>					    
				</tr>	    
				   <%}else{%>	 
	                <td align="center" height="127" class="fontmaior">Este auto de infra��o n�o possui<br>
					fotografia do ve�culo infrator em<br>
					virtude de ter sido originada por<br>
					agente de tr�nsito.</td>
                </tr>

                <tr>
	                <td align="center" height="127" class="fontmedia">&nbsp;</td>
                </tr>         
             
              <%}%>

            </table> 
            </td>
        </tr>
      </table>
      
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr>
      <td>
      <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
                <tr> 
                  <td valign="top"> 
                   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                      <tr> 
                        <td width="26%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Gravidade 
                          da Infra&ccedil;&atilde;o</strong></span><br>
                        <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscGravidade() %></span></td>
                        <td width="4" valign="top" class="semborda">&nbsp;</td>
                        <td width="13%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Veloc. 
                          Considerada </strong></span><br>
                        <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocConsiderada() %></span>                        </td>
                        <td width="4" valign="top" class="semborda">&nbsp;</td>
                        <td width="4%" height="30" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Tipo</strong></span><br>
                              	<span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodTipDispRegistrador() %></span>                              </td>
                        <td width="4" valign="top" class="semborda">&nbsp;</td>
                            <td width="24%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Identif. 
                              do Aparelho</strong></span><br>
							  <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumIdentAparelho()%></span>                            </td>  
                        <td width="4">&nbsp;</td>
                            <td width="9%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&ordm; 
                              Cert. Aferi&ccedil;&atilde;o</strong></span><br>
                              <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumCertAferAparelho()%></span>                            </td>
                        <td width="4">&nbsp;</td>
                        <td width="9%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Data 
                              Aferi&ccedil;&atilde;o</strong> </span><br>
                              <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDatUltAferAparelho()%></span>                        </td>
                        <td width="4">&nbsp;</td>
                        <td width="12%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>C&oacute;d. / Mat. 
                              Agente</strong> </span><br>
                              <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getCodAgente()%>  /  <%=AutoInfracaoBeanId.getDscAgente() %></span></td>
                            </tr>
                    </table></td>
                </tr>
              </table>
              
              
              
              
              
              <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                      <tr> 
                        <td width="21%" height="32" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Veloc. 
                          Regulamentada</strong></span><br>
                        <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocPermitida() %></span></td>
                        <td width="4" valign="top" class="semborda">&nbsp;</td>
                        <td width="13%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Veloc. 
                          Aferida </strong></span><br>
                          <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getValVelocAferida() %></span></td>
                        <td width="4">&nbsp;</td>
                        <td width="9%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Ident. 
                          PRF </strong></span><br>
                        <span class="fontmenor">&nbsp;</span></td>
                        <td width="4">&nbsp;</td>
                        <td width="12%" height="30" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>N&ordm; 
                                Cert. INMETRO</strong></span><br>
                                <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getNumInmetroAparelho() %></span>                        </td>
                        <td width="4">&nbsp;</td>
                        <td width="43%" valign="top" class="tdborda"><span class="fontseis">&nbsp;<strong>Localiza&ccedil;&atilde;o 
                                do Aparelho</strong></span><br>
                                <span class="fontmenor">&nbsp;<%=AutoInfracaoBeanId.getDscLocalAparelho() %></span>                        </td>
                    </tr>
                    </table></td>
              </tr>
            </table>
      </td>
      </tr>
      </table>
      
      
      
     
    </td>
        </tr>
      </table>
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr>   
		<td height="2"></td>
	</tr>
</table>


<!--Local da foto-->



<!--Fim Local da foto-->