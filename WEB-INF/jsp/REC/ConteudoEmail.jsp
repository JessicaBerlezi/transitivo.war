<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<jsp:setProperty name="OrgaoBeanId" property="j_abrevSist" value="ACSS" />  
<jsp:setProperty name="OrgaoBeanId" property="colunaValue" value="cod_orgao" />
<jsp:setProperty name="OrgaoBeanId" property="popupNome"   value="codOrgao"  />
<jsp:setProperty name="OrgaoBeanId" property="popupString" value="sig_orgao,SELECT cod_orgao,sig_orgao FROM TSMI_ORGAO ORDER BY SIG_ORGAO" />
<!-- Chama o Objeto da Tabela Codigo de Retorno --> 
<jsp:useBean id="conteudoEmailId" scope="request" class="REC.ConteudoEmailBean" /> 
<!-- Carrega Eventos -->
<jsp:useBean id="evento" scope="request" class="TAB.EventoBean" /> 
<jsp:setProperty name="evento" property="j_abrevSist" value="REC" />  
<jsp:setProperty name="evento" property="colunaValue" value="cod_evento" />
<jsp:setProperty name="evento" property="popupNome"   value="codEvento"  />
<jsp:setProperty name="evento" property="popupString" value="dsc_evento, SELECT cod_evento,dsc_evento as cod_evento,dsc_evento FROM TSMI_EVENTO ORDER BY DSC_EVENTO" />
  
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {

   switch (opcao) {    
   case 'busca':
   if(fForm.codOrgao.value==0)
     alert("�rg�o n�o selecionado. \n")
   if (fForm.codEvento.value==0)
     alert("Evento n�o selecionado. \n ")
   else{	 
      fForm.acao.value=opcao;
	  fForm.dscEvento.value = fForm.codEvento.options[fForm.codEvento.selectedIndex].text;
	  fForm.target= "_self";
	  fForm.action = "acessoTool";  
	  fForm.submit();	 
   }
	  break;
	  
   case 'retorna':
        fForm.atualizarDependente.value="N"	  
        fForm.codOrgao.value = " "
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
 
   case 'R':
	 close() ;   
	 break;
	  		
   case 'excluir':
	   if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	 
	     }
		  	break ;
  
   case 'alter':
     if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
     }
	  	break ;
	  	
	case 'insert':
		if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
 		}
	  	break ;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;        

	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codEvento.value==0) {
	   valid = false
	   sErro += "Evento n�o selecionado. \n"
	}
	
	if (fForm.nomRemetente.value=='') {
	    valid = false
	    sErro += "Remetente deve ser preenchido. \n"
	 }
	 
	 if (fForm.txtAssunto.value == ''){
	    valid = false
	    sErro += "Assunto do e-mail deve ser preenchido. \n"
	 }
	
	if (valida_email(fForm.nomRemetente)==false) {
		valid = false ;	
		sErro+="Email inv�lido.\n"
	}
	
	if (valid==false) alert(sErro) 
	return valid;
}

function verifica_email(myobj) {
	if (valida_email(myobj)==false) {
		alert("Email inv�lido.")
	}
}
function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}



</script>
<style type="text/css">
</style>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="emailForm" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="dscEvento" type="hidden" value='<%=conteudoEmailId.getDscEvento()%>'>					
<input name="atualizarDependente" type="hidden" value=''>

<!-- Rodap�-->

<div id="WK_SISTEMA" style="position:absolute; width:561px; overflow: visible; z-index: 1; top: 83px; left:165px; visibility: visible;" > 
<!--INICIO BOTOES-->  
  <TABLE width="101%" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
    <% if (((String)request.getAttribute("atualizarDependente")).compareTo("N")==0){%>
      <tr><td height="10"></td></tr>
      <tr>
    	<td  width="97"><b>�rg�o Atua��o:&nbsp;</b></td>
		<td width="412">
       	    <jsp:getProperty name="OrgaoBeanId" property="popupString" />
	    </td>
	    <td align=left width=58>&nbsp;&nbsp;&nbsp;
	      	<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);">	
	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
		 </td>
	  </tr>
	  <tr><td height="10"></td></tr>
	  <tr>  
	    <td ><b>Evento:&nbsp;</b></td>
	    <td>
			 <jsp:getProperty name="evento" property="popupString" />
	    </td>
 	  </TR>	
 	</table>       
 </div>
       	<% }
			else { %>
			    <td align=center width=40%><b>�rg�o Atua��o:&nbsp;</b>
				    <input name="codOrgao" type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
		            <input disabled name="sigOrgao" type="text" size="20" maxlength="10"value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >
		        </td>
			    <td align=center width=40%><b>Evento:&nbsp;</b>
			      <input name="codEvento" type="hidden" size="6" maxlength="2"  value="<%= conteudoEmailId.getCodEvento() %>"> 
		          <input disabled name="dscEvento" type="text" size="60"  maxlength="50" value="<%=conteudoEmailId.getDscEvento()%>" style="border: 0px none;">
			    </td>
	</TR>	
 	</table>       
 </div>
 
  <div style="position:absolute; width:527px; overflow: visible; z-index: 1; top: 109px; left: 191px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  
     <tr>	   				  
      <td>Remetente</td>
	  <td >:&nbsp; 
	  	 <input name="nomRemetente" type="text"  size="60" maxlength="50" value="<%= conteudoEmailId.getRemetente() %>" onfocus="javascript:this.select();" onchange="verifica_email(this);" >
	 </td>  
	 </tr> 
	 <tr>
       <td height="1" colspan="2"></td>
     </tr>
	 <tr> 
	  <td>Assunto</td>
	  <td >:&nbsp; 
	   	 <input name="txtAssunto" type="text"  size="60" maxlength="50" value="<%= conteudoEmailId.getAssunto() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
	  </td>
	 </tr>
	 <tr>
       <td height="1" colspan="2"></td>
     </tr>
	 <tr> 
	  <td>Login</td>
	  <td >:&nbsp; 
	   	 <input name="login" type="text"  size="30" maxlength="20" value="<%= conteudoEmailId.getLogin() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
	  </td>
	  </tr>
	  <tr>
       <td height="1" colspan="2"></td>
     </tr>
     <tr>
	  <td >Senha </td>
	  <td >:&nbsp; 
	   	 <input name="senha" type="password"  size="30" maxlength="20" value="<%=conteudoEmailId.getSenha() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
	  </td>
	 </tr>
	  <tr>
       <td height="5" colspan="2"></td>
   	</table>
  </div> 
      
  <div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 198px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	
	 <tr>   				  
      	<td width="100%" align="center" >
		    <textarea name="textConteudo"  cols="80" rows="7" "dir="ltr" lang="pt"><%=conteudoEmailId.getConteudoEmail()%></textarea>   
     	</td>
     </TR>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	
     <TR>
		<td width="300" align="center" > 
		   <button type=button NAME="Atualizar"  style="height: 26px; width: 71px;border: none; background: transparent;" onClick="javascript: valida('<%=(String)request.getAttribute("acao")%>',this.form);">           
	    <img src="<%= path %>/images/bot_atualizar_det1.gif" width="71" height="26" ></button>        </td>
		<td>&nbsp;</td>
		<td  >
		   <button type=button NAME="Excluir"  style="height: 26px; width: 63px;border: none; background: transparent;" onClick="javascript: valida('excluir',this.form);">	
        <IMG src="<%= path %>/images/bot_excluir_det1.gif" width="63" height="26" ></button>        </td>
        <td >
		   <button type=button NAME="Retornar"  style="height: 26px; width: 71px;border: none; background: transparent;" onClick="javascript: valida('retorna',this.form);">	
        <IMG src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" ></button>        </td>
     </TR>
    </TABLE>  
  </div> 
 <% } %>
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= conteudoEmailId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  

<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.emailForm.codOrgao.length;i++){
		if(document.emailForm.codOrgao.options[i].value==njint){
			document.emailForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>	
</form>
</BODY>
</HTML>
