<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 

<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 

<jsp:useBean id="RelatorId"       scope="request" class="REC.RelatorBean" /> 
<jsp:setProperty name="RelatorId" property="j_abrevSist" value="REC" />  
<jsp:setProperty name="RelatorId" property="colunaValue" value="cod_Relator_Junta" />
<jsp:setProperty name="RelatorId" property="popupNome"   value="cod_Relator_Junta"  />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	if ("Top,Proximo,Anterior,Fim".indexOf(opcao)>=0) 
	{ 
		if(bProcessando)
		{
	   		alert("Opera��o em Processamento, por favor aguarde...");
			return;
		}
		else
		{
			bProcessando=true;
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	  		  
			return ;
		}
	}
		
	switch (opcao) 
	{   	  
		case 'R':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
		
				fForm.acao.value =opcao;
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();	   	   
    			close() ;
    		}
			break;
		
		case 'AtualizaEnvio':
  			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
  
				fForm.acao.value = opcao;
			   	fForm.target     = "_self";
			    fForm.action     = "acessoTool";  
			   	fForm.submit();	 
			}
			break;
		 
		case 'O':  // Esconder os erro
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
			else                 
				document.all["MsgErro"].style.visibility='hidden' ; 
			break;  

		case 'Novo':  // Esconder os erro
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				fForm.acao.value = opcao;
				fForm.target     = "_self";
				fForm.action     = "acessoTool";  
				fForm.submit();	 
				break;  
			}
	}
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.cod_Relator_Junta,"Junta/Relator",0);
	a=fForm.cod_Relator_Junta.value
	if (a.length<18) {
		a="000000000000000000"+a	
		a= a.substring((a.length-18),a.length)
	}
	fForm.codJuntaJU.value = a.substring(0,6) ;
	fForm.codRelatorJU.value = a.substring(7,18) ;	
	if (valid==false) alert(sErro) ;
	return valid ;
}
function MostraAI(opcoes,fForm,numplaca,numauto,temauto) {	
if(temauto=='1'){
	document.all["acao"].value          = opcoes;
	document.all["mostraplaca"].value   = numplaca;
	document.all["mostranumauto"].value = numauto;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
} 	
}

function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["PreparaGuiaForm"].target = "_self";
	document.all["PreparaGuiaForm"].action = "acessoTool";  
	document.all["PreparaGuiaForm"].submit() ;	 
}

function Mostra(opcoes,fForm,numplaca,numauto) {

	document.all["acao"].value          = opcoes;
	document.all["mostraplaca"].value   = numplaca;
	document.all["mostranumauto"].value = numauto;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}
function SomaSelec() {
  
//	n = 0 ;
//	
//	for (i=0;i<document.all["PreparaGuiaForm"].Selecionado.length;i++) {
//		if (document.all["PreparaGuiaForm"].Selecionado[i].checked)
//		 n++;
//	
//	}
	
}

function marcaTodos(obj) {
	n=0
	for (i=0; i<document.all["PreparaGuiaForm"].Selecionado.length;i++) {
		document.all["PreparaGuiaForm"].Selecionado[i].checked = obj.checked ;
		if (document.all["PreparaGuiaForm"].Selecionado[i].checked) n++;
	}
}

</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="PreparaGuiaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"           type="hidden" value=' '>
<input name="codRelatorJU"   type="hidden" value="">				
<input name="codJuntaJU"     type="hidden" value="">	
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=4 height=8></td></tr>
     <tr bgcolor="#faeae5">
        <td width="70%" height=23 align="center"><font size='1'><b>&nbsp;Processos/Requerimentos no Protocolo</b></font></td>			
		<td width="15%" align="center" valign="middle"> 
			<% if (RemessaId.getAutos().size()>0) {        %>
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AtualizaEnvio',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
        	<% } %>
		</td>
        <td width="15%" align="center" valign="middle"> 
	        <button type=button NAME="Limpar"   style="height: 21px; width: 35px;border: none; background: transparent;" onClick="javascript: valida('Novo',this.form);">	
    	    <IMG src="<%= path %>/images/REC/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" > 
        	</button>
		</td>	
     </tr>
     <tr><td colspan=4 height=8></td></tr>     
  </table>
  <jsp:include page="RemessaTop.jsp" flush="true" />
</div>
<jsp:include page="RemessaPreparaCorpo.jsp" flush="true" >  
  <jsp:param name="posTop" value= "170 px" />
  <jsp:param name="posHei" value= "175 px" />
</jsp:include> 

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= RemessaId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= RemessaId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>


