<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela Codigo de Retorno --> 
<jsp:useBean id="motivoCancelBean" scope="request" class="REC.MotivoCancelBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   switch (opcao) {
   case 'R':
      close() ;   
	  break ;   
   case 'Atualizar':
		if( valida_input(fForm) == true ){
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
		}		 		  
	  	break ;
   case 'I':
     fForm.acao.value=opcao
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function valida_input (form){
	for(var i = 0; i < form.codMotivoCancelamento.length ; i ++ ){
		var codMotivoCancelamento = form.codMotivoCancelamento[i].value
		var descMotivoCancelamento = form.descMotivoCancelamento[i].value
		var numMotivoCancelamento = form.numMotivoCancelamento[i].value
		if( (numMotivoCancelamento != '') && (descMotivoCancelamento == '') ){
			alert( "Descri��o do Motivo em branco.\n" )
			return false
		}
		if( (numMotivoCancelamento == '') && (descMotivoCancelamento != '') ){
			alert( "C�digo do Motivo em branco. \n" )
			return false
		}
	}
	return true
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="relatorForm" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>		

<div id="WK_SISTEMA" style="position:absolute; width:568px; overflow: visible; z-index: 1; top: 66px; left:51px;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="400"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>     
	  </tr>
      <tr>
		  <td align="right" width="20%"><input name="atualizarDependente" type="hidden" size="1" value="<%= motivoCancelBean.getAtualizarDependente() %>"></td>
          <td align=left width=350></td>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
		<% if (motivoCancelBean.getAtualizarDependente().equals("S")){ %>			
		   <td align="center"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('Atualizar',this.form);">	
        <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>   	    </td>
     	   <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
    	   <td align="center" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
        <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>        </td>
    	   
     	   
		<%} %>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:570px; overflow: auto; z-index: 1; left: 50px; top: 119px; height: 32px"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<tr bgcolor="#993300"> 
        <td width=100 align="center"><a href="#" onClick="javascript: Classifica(motivoCancel,numMotivoCancelamento,0,'input_text','#faeae5','#faeae5');"><font color="#ffffff"><b>C�d. Motivo</b></font></a></TD>		 	  
        <td width=350 align="center"><a href="#" onClick="javascript: Classifica(motivoCancel,descMotivoCancelamento,1,'input_text','#faeae5','#faeae5');"><font color="#ffffff"><b>Descri��o do Motivo</b></font></a></TD>		 
	</tr>
  </table>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    </div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:570px; overflow: auto; z-index: 1; left: 50px; top: 132px; height: 191px;"> 
    <table id="motivoCancel" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (motivoCancelBean.getAtualizarDependente().equals("S")) {				   
					   int ocor   = motivoCancelBean.getMotivoCancel().size() ;
					   if (ocor<=0) ocor = motivoCancelBean.getMotivoCancel(10,5).size() ;
					   for (int i=0; i<ocor; i++) {	   %>
      <tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
		    <input name="numMotivoCancelamento" type="text" size="5" maxlength="3"  value="<%= motivoCancelBean.getMotivoCancel(i).getNumMotivoCancel().trim() %>" onfocus="javascript:this.select();" onkeypress="javascript:f_num();">
        	<input name="codMotivoCancelamento" type="hidden" value="<%= motivoCancelBean.getMotivoCancel(i).getCodMotivoCancel() %>">		
        </td>			
		<td width=350 align="center"> 
          <input name="descMotivoCancelamento" type="text" size="43" maxlength="43"  value="<%= motivoCancelBean.getMotivoCancel(i).getDescMotivoCancel() %>" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"  >
        </td>			
 	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#FAEAE5">
		<td width=100 align="center"> 
          <input name="numMotivoCancelamento"  readonly  type="text" size="10" maxlength="10" value="">
        </td>
	   	<td width=350 align=center> 
          <input name="descMotivoCancelamento" readonly  type="text" size="43" maxlength="50"  value="">
        </td>			
  	</tr>
					   
<%						}					
					}   %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= motivoCancelBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>
