<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
	AtaDistribui��oImpPorRelator
-->	
<%@page import="REC.ControladorRecGeneric"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ACSS.Ata"%>
<%@ page errorPage="ErrorPage.jsp" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.HashMap"%>
<%@page import="ACSS.Ata"%>

<% String path=request.getContextPath(); %>

<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

	<head>
		<jsp:include page="CssImpressao.jsp" flush="true" />
		<SCRIPT LANGUAGE="JavaScript1.2">
		<!-- Abre a janela com o tamanho maximo disponivel -->
		self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
		</script>
		
		<style>
			td, th { font-size: 10px; }
		</style>
	</head>
	
<body style="font-family: verdana;">
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<!--CABE�ALHO IMPRESS�O-->
<div id="cabecalhoimp" style="position: relative; z-index:100;">
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	
		<td height="1" bgcolor="#000000"></td>	
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr><td width="23%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
        </tr>
        <tr>
        	<td width="38%" height="40" valign="top">
				<img alt="Prefeitura de Maca�" src="images/prefeitura_macae.png" style="width: 200px; height: 90px;">
        	</td>
        
        <%
        	System.out.print("Valor : "  + SistemaBeanId.getCodUF().toString());
        %>
        </tr>
      </table>
    </td>        
    <td width="60%" align="center" valign="top" class="fontmaior"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
      <STRONG style="font-size: 11px;">:: TRANSITIVO ::</strong> 
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
      <span class="fontmedia"><STRONG><%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></span></td>
    <td width="17%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr>   <td height="20" align="right" valign="top"></td></tr>
        <tr><td height="40" align="right" valign="top"><img src="images/im_logosmit.gif" width="98" height="30"></td>
        </tr>
      </table>		  
    </td>
 </tr>        
</table>
<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
</table>
<!--FIM Linha-->

<!--FIM CABE�ALHO IMPRESS�O-->
</div>
<% 
 ArrayList<HashMap<String, String>> list = new ArrayList();
 ArrayList<HashMap<String, String>> listCom = new ArrayList();
 
 
	Ata ata = new Ata();
	
		String codAta = (String) request.getAttribute("codAta");
		String codRelator = (String) request.getAttribute("codRelator");
		String codOrgao = (String)request.getParameter("codOrgao");
		
		listCom = ata.consultarProcessosPorRelator(codAta, codOrgao, codRelator);
// 		list = ata.getAtaDistribuicaoMap(request.getParameter("numAta").trim(),RequisicaoBeanId.getSigFuncao() ); //Defesa previa / 1 instancia / 2 instancia
		
		if(listCom.size()>0){
			%>
			
				<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
				<tr>
					<td width="29%" align="left" >
						<strong>Distribui��o de Processo da Ata N&ordm;:&nbsp;&nbsp;</strong>
						<strong class="fontmaior"><%= listCom.get(0).get("DS_ATA")%></strong>
						<strong>Data: <%= ControladorRecGeneric.dateToDateBr(Ata.consultarAtaPor(codAta).getDataSessao()) %></strong>
					</td>
			<!-- 		<td width="30%" align="center">Placa :&nbsp;&nbsp;<strong  class="fontmaior"></strong></td> -->
			<!-- 		<td width="41%" align="right" nowrap>Processo :&nbsp;&nbsp;<strong class="fontmaior"></strong></td>	 -->
				</tr> 
			</table>
						<%
			%>
			<div id="tabelastopo" style="position: relative; z-index:100;">
			<div align="center">
				<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
					<tr><td height="1" bgcolor="#000000"></td></tr>
				</table>
			</div>     
			<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
			  <tr> 
			    <td width="25%" height="20" bgcolor="#b6b6b6" > 
			      <strong>1 &#8226; DADOS DA DISTRIBUI��O / DADOS DO PROCESSO</strong>
			<!--     <td width="75%" bgcolor="#b6b6b6" align="right" ><strong class="fontmenor">Propriet&aacute;rio :&nbsp; -->
			<!--       	<span class="fontmedia"> -->
			<!--       	</span></strong> -->
			<!--     </td> -->
			  </tr>
			</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
	<tr style="text-align: center;">
		<th></th>
		<th>N� de Processo</th>
<!-- 	  	<th>Tipo de Processo</th> -->
      	<th>N� do Auto</th>
      	<th>Cod Infra��o</th>
      	<th>Requerente</th>
    	<th>Placa</th>
    	<th>Data do Processo</th>
    	<th>Relator</th>
    	<th>Resultado</th>
  </tr>
	<% for(int i=0; i<listCom.size(); i++){ %>
	<tr style="text-align: center; font-size: 10px;">
		<td><%= i + 1 %></td>
	    <td>
			<%= listCom.get(i).get("num_processo")%>
	    </td>
<!-- 		<td> -->
<%-- 			<%= listCom.get(i).get("TIPO_ATA")%> --%>
<!-- 		</td> -->
		<td>
			<%= listCom.get(i).get("NUM_AUTO_INFRACAO")%>
		</td>
		<td>
			<%= listCom.get(i).get("COD_INFRACAO") == null ? "N/A" : listCom.get(i).get("COD_INFRACAO") %>
		</td>
		<td>
			<%= listCom.get(i).get("NOM_PROPRIETARIO")%>
		</td>
    	<td>
			<%= listCom.get(i).get("NUM_PLACA")%>
		</td>
    	<td>
			<%= ControladorRecGeneric.dateToDateBr(listCom.get(i).get("DAT_PROCESSO"))%>
		</td>
		<td>
			<%= listCom.get(i).get("nom_relator") %>
		</td>
    	<td style="width: 61px;">
			<div style="display: inherit; border: solid 1px; width: 15px; height: 18px;"></div>
			<div style="display: inherit;vertical-align: middle;">I</div>
			<div style="display: inherit; border: solid 1px; width: 15px; height: 18px;"></div>
			<div style="display: inherit;vertical-align: middle;">D</div>
		</td>
	</tr>
	<% } %>
</table>

<!--FIM Linha-->
</div>

	<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	  <tr>          
	    <td width="37%" height="5px" bgcolor="#87CEEB"> 
	      <strong></strong></td>
	    <td width="10%" bgcolor="#B6B6B6"> 
	     &nbsp;&nbsp;</td>
	  </tr>
	</table>
			<!--Rodap�-->
			<div id="rodape" style="font: bold 12px verdana; margin-top: 10px;"> 
				Data Recebimento:__/__/___
				
				<table style="width: 100%; border: none; margin-top: 40px; text-align: center;">
					<tr>
						<td>_________________________________________</td>
						<td>_________________________________________</td>
						<td>_________________________________________</td>
					</tr>
					<tr>
						<th style="font-size: 12px;">Presidente</th>
						<th style="font-size: 12px;">Relator</th>
						<th style="font-size: 12px;">Relator</th>
					</tr>
				</table>
			</div>
<!-- 			<div style="font: bold 9px verdana; padding-top: 50px; text-align: center;"> -->
<!-- 				___________________________________________________________________<br> -->
<%-- 				<%= listCom.get(0).get("NOME_RELATOR") %> --%>
<!-- 			</div> -->
		<%
		}else{
			%>
			<hr noshade color="#000000">
			<div id="rodape" style="position:relative; left:0px; width:100%; height:6px; z-index:100; visibility: visible;"> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
				  <tr><td width="31%"  height="20" ><strong><font color="red"><h3>N�o existe ata para distribui��o com a op��o selecionada!</h3></font></strong>&nbsp;<span class="fontmedia"></span></td>
				  </tr> 
				</table>
			</div>
		<%
		}
%>
<!--FIM_BLOCO B-->
</body>