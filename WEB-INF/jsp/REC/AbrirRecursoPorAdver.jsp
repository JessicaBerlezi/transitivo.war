<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@page import="ACSS.Ata"%>
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AbrirRecursoBeanId" scope="request" class="REC.AbrirRecursoDPBean" /> 
<jsp:useBean id="consultaAtaId" scope="session" class="ACSS.Ata" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'AbreRecursoAdver':
		   if(verifica(fForm)){
				if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	
				   	return true; 
			  	} 
		   }
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AbrirRecForm.datEntrada,1) && valid
	// validar se nada foi selecionado
	nadaSelec = true ;
	if ((AbrirRecForm.SolicDCBox != null) && (AbrirRecForm.SolicDCBox.checked==true)) nadaSelec = false ;
	if ((AbrirRecForm.SolicDPBox != null) && (AbrirRecForm.SolicDPBox.checked==true)) nadaSelec = false ;	
	if (nadaSelec) {
		sErro += "Nenhuma opção selecionada. \n"
		valid = false ;
	}	
	if (valid==false) alert(sErro) 
	return valid ;
}


function verifica(fForm) {  
    if (fForm.numProcesso.value=="")  {  
        alert("Por favor, preencha o nº do PROCESSO."); 
        fForm.numProcesso.focus();
        return false;  
    }else{
    	return true;
    }
}

function apaga(defesa) {
	if (defesa=='DC') {
		if (AbrirRecForm.SolicDCBox != null) AbrirRecForm.SolicDCBox.checked=false ;
	}
	else {
		if (AbrirRecForm.SolicDPBox != null) AbrirRecForm.SolicDPBox.checked=false ;	
	}
	return ;
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AbrirRecForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>

<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>
<%@ include file="apresentaInfracao_Diretiva.jsp" %>


<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr> 
      <td colspan="4" class="espaco2">&nbsp;<b><%= AbrirRecursoBeanId.getMsg() %></b>&nbsp;</td>
	  <td></td>
    </tr>
    <tr><td height="2" colspan="4"></td></tr>	
    <tr>
   
     <!-- Raj --> <input type="checkbox" name="teste" value="" class="input" >
	    <td width="14%" height="15" bgcolor="#faeae5"> 
          &nbsp;Data  Entrada:&nbsp;          	  </td>
	    <td width="14%" bgcolor="#faeae5"><input type="text" name="datEntrada" size="12" maxlength="10" value='<%=AbrirRecursoBeanId.getDatEntrada() %>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"></td>
	    <td width="65%" bgcolor="#faeae5">
		    <% if (AbrirRecursoBeanId.getTpSolDP().length()>0) { %>
<%-- 		  <td><%=AbrirRecursoBeanId.getTpSolDP() %></td> --%>
		      <input type="checkbox" name="SolicDPBox" value="<%= AbrirRecursoBeanId.getTpSolDP() %>" class="input" onClick="javascript: apaga('DC')">
		      <font color=red><b><%= AbrirRecursoBeanId.getNFuncao() %></b></font>
		      <% if (AutoInfracaoBeanId.getProprietario().getNomResponsavel().length()>31) { %>
					&nbsp;Proprietário:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel().substring(0,31) %>		  
		      <% } else { %>
					&nbsp;Proprietário:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
		      <% } %>		      	  
			<% } 
			 else out.println(AbrirRecursoBeanId.getMsgDP());			 
			%>	  
		</td>
      <td width="7%" align="center"  bgcolor="#faeae5"></td>
    </tr>
    <tr>
	    <td height="2" colspan="4"></td>
	</tr>
    <tr>
        <td height="17" bgcolor="#faeae5">&nbsp;Email/Telefone:&nbsp; 
          		</td>	
    	<td bgcolor="#faeae5"><input type="text" name="txtEMail" size="30" maxlength="60" value='<%=AbrirRecursoBeanId.getTxtEMail() %>'  onfocus="javascript:this.select();"></td>
    	<td bgcolor="#faeae5" > 
		    <% if (AbrirRecursoBeanId.getTpSolDC().length()>0) { %>
		      <input type="checkbox" name="SolicDCBox" value="<%= AbrirRecursoBeanId.getTpSolDC() %>" class="input" onClick="javascript: apaga('DP')">
		      <font color=red><b><%= AbrirRecursoBeanId.getNFuncao() %></b>&nbsp;</font>
		      <% if (AutoInfracaoBeanId.getCondutor().getNomResponsavel().length()>31) { %>
			      Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel().substring(0,31) %> 		  
		      <% } else { %>
			      Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>		  		      
		      <% } %>
			<% } 
			 else out.println(AbrirRecursoBeanId.getMsgDC());			 
			%>		</td>
			
			<!--Verifica se existe ata para a solicita��o -->
			 <%
				Ata ataa = new Ata();
			  	ataa = ataa.getAtasDaVez(AutoInfracaoBeanId.getNumAutoInfracao());
			  	//colocar para exibir todas caso exista mais que uma
			  	if(ataa != null){
			  		%>
			  		<td bgcolor="#faeae5" align="right">
					  	<% if ("S".equals(AbrirRecursoBeanId.getEventoOK())) { %>
							<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AbreRecursoPorAdve',this.form);">			  
						  	<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19">			</button>
						<% } %>	  
					</td>
			  		<%
			  	}
			 %>
    </tr>	
 
     <tr><td height="2" colspan="4"></td></tr>
    <tr>
	  <td height="5" bgcolor="#faeae5">&nbsp;Doc.Gerador:&nbsp; 
	  	  </td>
          <td colspan="3" bgcolor="#faeae5"><input type="text" name="txtMotivo" size="70" maxlength="50" value='<%=AbrirRecursoBeanId.getTxtMotivo() %>'  onfocus="javascript:this.select();"></td>
	  </tr>   
	  <tr>
	  <td height="5" bgcolor="#faeae5">&nbsp;PROCESSO:&nbsp; 
	  	  </td>
          <td colspan="3" bgcolor="#faeae5"><input name="numProcesso" type="text" value="" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()"></td>
	  </tr>
	  <%
	  	//Verifica a ata da semana de acordo com o tipo DF ou JARI e o seu referido status. O sistema tem que exibir as atas do tipo DP ou JARI que estao abertas para a semana
	  	// Data da atual nao pode ser superior a data da sessao e nem a data da distribuicao
	  	Ata ata = new Ata();
	  	ata = ata.getAtasDaVez(AutoInfracaoBeanId.getNumAutoInfracao());
	  	//colocar para exibir todas caso exista mais que uma
	  	if(ata != null){
	  		if(AutoInfracaoBeanId.getNumAutoInfracao().length()>0){
	  		%>
	  				
	  				<tr>
					  <td height="5" bgcolor="#faeae5">&nbsp;ATA:&nbsp; </td>
				          <td colspan="3" bgcolor="#faeae5">
				          	<input name="dsAta" type="text" value="<%= ata.getDescAta() %>" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
				          <strong>Junta:</strong>    <input name="juntaAta" type="text" value="<%= ata.getJuntaAta()%>" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()" disabled="disabled">
				          <strong> Dt Dist:</strong> <input name="dtDistribuicao" type="text" value="<%= ata.getDataDistribuicao() %>" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()" disabled="disabled">
				          <strong>Dt Aviso:</strong> <input name="dtAviso" type="text" value="<%= ata.getDataAviso() %>" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()" disabled="disabled">
				          <strong>Dt Sess�o:</strong><input name="dtSessao" type="text" value="<%= ata.getDataSessao() %>" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()" disabled="disabled">
				          <input name="cdAta" type="hidden" value="<%= ata.getCodAta() %>"> 
				          </td>
	  				</tr>
	  
	  		<%
	  		}
	  	}else{
	  		%>
	  				<tr>
					  <td height="5" bgcolor="#faeae5" style="color: red; " align="center"><b> N�o existe Ata para a solicita��o! </b></td>
	  				</tr>
	  		<%
	  	}
	  %>
  </table>
</div>

<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0){%>
	<script>document.AbrirRecForm.txtEMail.focus();</script>	        
<%}else{%>    		
   	<script>document.AbrirRecForm.numAutoInfracao.focus();</script>	        
<%}%>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->

<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 


<!--FIM_Div Erros-->
</form>
</body>
</html>