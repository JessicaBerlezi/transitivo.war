<%@page import="REC.ParamOrgBean"%>
<%@page import="UTIL.TipoRecGeneric"%>
<%@page import="REC.ControladorRecGeneric"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<jsp:useBean id="ParamOrgBeanId"    scope="session" class="REC.ParamOrgBean" />
    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="iso-8859-1">
		<title>Ata para Publica��o</title>
		
		<link rel="stylesheet" href="css/transitivo.css" />
	</head>
	<body class="body">
		<div id="head">
			<div class="img-head2">
				<div class="img-head1">
					<div style="position: absolute; right: 6px;">
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/detran_help.png');"></div>
					</div>
				</div>
			</div>
			<div id="bread" class="bread">
				<strong class="text">ATA &gt; ATA PARA PUBLICA��O</strong>
			</div>
		</div>
		<div id="body" style="height: 573px; padding-right: 15px;">
			<div class="divForm">
				<table id="tableForm">
					<tr>
						<td>*Tipo de Ata:</td>
						<td>
							<select id="tipoAta" style="width: 100px; height: 25px;"
									onchange="limparListAtas(); limparCboAta(); abilitaTxts(); $('#txtMonth, #txtYear').val('');">
								<option value="">Selecione...</option>
								<option value="Def. Previa">Def. Pr�via</option>
								<option value="1� INST�NCIA">1� Inst�ncia</option>
								<option value="2� INST�NCIA">2� Inst�ncia</option>
								<option value="PAE">P.A.E</option>
							</select>
						</td>
						<td style="padding-left: 20px;">*M�s/ Ano:</td>
						<td>
							<input type="text" id="txtMonth" style="width: 30px;" maxlength="2" disabled onkeyup="limparCboAta(); requestAtas();" />
							/
							<input type="text" id="txtYear" style="width: 30px;" maxlength="4" disabled onkeyup="limparCboAta(); requestAtas();" />
						</td>
					</tr>
					<tr><td colspan="4" style="height: 10px;"></td></tr>
					<tr>
						<td>N�mero da Ata:</td>
						<td>
							<select id="listNumAta" disabled style="width: 100px; height: 25px;" onchange="abilitaBtnAdd()">
								<option value="">selecione uma op��o</option>
							</select>
						</td>
						<td colspan="2">
							<button class="btn" disabled="disabled" id="btnAdicionar" onclick="adicionarAta()">Adicionar</button>
						</td>
					</tr>
				</table>
			</div>
			<div class="divList">
				<ul id="list" class="list-nostyle">
				</ul>
			</div>
			<div class="divBtn">
				<form method="post" action="acessoTool" target="_blank" id="relatorioForm">
					<input type="hidden" name="acao" value="gerarRelatorio" />
					<input type="hidden" name="listAtas" value="" />
					<input type="hidden" name="tipoAta" value="" />
					<input type="hidden" name="codOrgao" id="codOrgao" value="<%= ParamOrgBeanId.getCodOrgao() %>" />
					<input name="j_id" id="j_id" type="hidden" value="<%=RequisicaoBeanId.getId()%>">
					<input name="j_token" id="j_token" type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
					<input name="j_cmdFuncao" id="j_cmdFuncao" type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
					<input name="j_sigFuncao" id="j_sigFuncao" type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
					<input name="j_jspOrigem" id="j_jspOrigem" type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
					<input name="j_abrevSist" id="j_abrevSist" type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
				</form>
				<button class="btn" style="float: right;" id="btnImprimir" disabled="disabled" onclick="gerarRelatorio();">Imprimir...</button>
			</div>
		</div>
		<div id="footer">
			<div class="img-footer2">
				<div class="img-footer"></div>
				<div class="img-footer-btn"></div>
			</div>
		</div>
		
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		
		<script type="text/javascript">
			var listAtas = [];
			
			$(function(){
			    $('#txtMonth').bind('keydown', soNums);
			    $('#txtYear').bind('keydown', soNums);
			});
			 
			function soNums(e){
			 
			    //teclas adicionais permitidas (tab,delete,backspace,setas direita e esquerda)
			    keyCodesPermitidos = new Array(8,9,37,39,46);
			     
			    //numeros e 0 a 9 do teclado alfanumerico
			    for(x=48;x<=57;x++)
			        keyCodesPermitidos.push(x);
			     
			    //numeros e 0 a 9 do teclado numerico
			    for(x=96;x<=105;x++)
			        keyCodesPermitidos.push(x);
			     
			    //Pega a tecla digitada
			    keyCode = e.which; 
			     
			    //Verifica se a tecla digitada � permitida
			    if ($.inArray(keyCode,keyCodesPermitidos) != -1)
			        return true;
		        
			    return false;
			}
            
			function requestAtas() {
				//var txtData = $("#txtData").val();
				var txtMonth = $("#txtMonth").val();
				var txtYear = $("#txtYear").val();
				var codOrgao = document.getElementById("codOrgao").value;

				/* if (txtData == "")
					return; */

				if (!txtMonth || txtMonth.length < 2)
					return;
				else if(!txtYear || txtYear.length < 4)
					return;

				var year = txtYear; //txtData.split('-')[0];
				var month = txtMonth; //txtData.split('-')[1];
				var tipoAta = $("#tipoAta").val();
				var tipoRec = <%= TipoRecGeneric.ATA_PUBLICACAO %>
				var cdNatureza = <%= session.getAttribute("codNatureza") %>;
				
				$.ajax({
					method: "POST",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						tipoRec: tipoRec,
						year: year,
						month: month,
						tipoAta: tipoAta,
						codOrgao: codOrgao,
						cdNatureza: cdNatureza
					},
					success: function(data){
						populaCboAta(data);
					},
					error: function(){
						alert("erro na requisi��o!");
					}
				});
			}
			
			function populaCboAta(data){
				//limparCboAta();
				
				var options;
				
				for(var op = 0; op < data.length; op++){
					options += "<option value='" + data[op].codAta + "'>" + data[op].descAta + "</option>";
				}

				if(options){
					document.getElementById("listNumAta").innerHTML += options;
					document.getElementById("listNumAta").disabled = false;
				}
				else
					document.getElementById("listNumAta").disabled = true;
			}
			
			function adicionarAta(){
				var op = $("#listNumAta option:selected");
				var ata = {
					codAta: op.val(),
					descAta: op.text()
				};

				for(var i = 0; i < listAtas.length; i++){
					if(listAtas[i].codAta == ata.codAta){
						return;
					}
				}
				
				listAtas.push(ata);
				
				var li = "<li value='" + ata.codAta + "'><div class='list-text'>" + ata.descAta +
						"</div><div title='excluir?' class='list-btn-excluir' onclick='excluirAta(this)'>x</div></li>";
				
				document.getElementById("list").innerHTML += li;

				abilitaBtnImprimir();
			}
			
			function abilitaTxts(){
				var cboTipoAta = $("#tipoAta option:selected").val();

				if(cboTipoAta != ""){
					document.getElementById("txtMonth").disabled = false;
					document.getElementById("txtYear").disabled = false;
				}
				else{
					document.getElementById("txtMonth").disabled = true;
					document.getElementById("txtYear").disabled = true;
				}
			}

			function abilitaBtnAdd(){
				var selectedOp = $("#listNumAta option:selected").val();

				if(selectedOp != "")
					document.getElementById("btnAdicionar").disabled = false;
				else
					document.getElementById("btnAdicionar").disabled = true;
			}

			function abilitaBtnImprimir(){
				if(listAtas.length > 0)
					document.getElementById("btnImprimir").disabled = false;
				else
					document.getElementById("btnImprimir").disabled = true;
			}

			function limparCboAta(){
				document.getElementById("listNumAta").innerHTML = "<option value=''>selecione uma op��o</option>";
				//listAtas = { };
			}

			function gerarRelatorio(){
				var relatorioForm = document.getElementById("relatorioForm");
				
				relatorioForm.tipoAta.value = $("#tipoAta option:selected").text();

				if(listAtas.length > 0)
					relatorioForm.listAtas.value = JSON.stringify(listAtas);
		  		
				relatorioForm.submit();
			}

			function excluirAta(e){
				e.parentNode.remove();
				
				listAtas = listAtas.filter(function(ata){
					if(ata.codAta != e.parentNode.value)
						return ata;
				});
			}

			function limparListAtas(){
				listAtas = [];
				document.getElementById("list").innerHTML = "";
			}
		</script>
	</body>
</html>