<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RemessaId" scope="session" class="REC.RemessaBean" /> 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{    
		case 'R':
			close() ;
			break;
			
	   case 'ExcluirProc':
			if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;
				
				if (veCampos(fForm)==true){
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				   	fForm.action = "acessoTool";  
				   	fForm.submit();
				}
			}
		   	break;

	   case 'O':  // Esconder os erro
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
			else                 
				document.all["MsgErro"].style.visibility='hidden' ; 

	      	if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;
	  
				fForm.acao.value="Novo";
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	
			}
			break; 

	   case 'N':  // Esconder os erro
			if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;
			   
				fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		   	}
			break;  
	}
}

function veCampos(fForm){
    valid = true ;
	sErro = "" ;
	// validar se nada foi selecionado
	nadaSelec = true ;
	if ((fForm.codReq1 != null) && (fForm.codReq1.checked==true)) nadaSelec = false ;
	if ((fForm.codReq2 != null) && (fForm.codReq2.checked==true)) nadaSelec = false ;	
	if (nadaSelec) {
		sErro += "Nenhuma op��o selecionada. \n"
		valid = false ;
	}	
	if (valid==false) alert(sErro) 
	return valid ;  
}
function apaga(codReq) {
	if (codReq == 'DC1') {
		if (form.codReq2!= null) form.codReq2.checked=false ;
	}
	else {
		if (form.codReq1 != null)form.codReq1.checked=false ;	
	}
	return ;
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="form" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"   type="hidden" value=' '>						
<!--IN�CIO_CORPO_sistema--> 
<jsp:include page="lerAutoPlaca.jsp" flush="true" />
<jsp:include page="apresentaInfracao.jsp" flush="true" />
<div style="position:absolute; left:66px; top:269px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
   
<div id="recurso3" style="position:absolute; left:66px; top:274px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
<% if (AutoInfracaoBeanId.getCodAutoInfracao().length()>0) { %> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  	 <tr bgcolor="#faeae5"><td ><img src="<%= path %>/images/inv.gif" width="1" height="5"></td></tr>
	  	<% if ("S".equals(RemessaId.getEventoOK())) { 
	  	  Iterator it = RemessaId.getAutos().iterator();
	  	  REC.RemessaBean myRemessa = new REC.RemessaBean();
	  	  int ind = 0;
	  	  while(it.hasNext()){
	  		myRemessa = (REC.RemessaBean)it.next();
	  		ind ++;
	  	  %>			 	 
         <tr bgcolor="#faeae5">
    	    <td align="left" valign="top" >&nbsp;&nbsp;&nbsp;&nbsp;
				  <input type="checkbox" name="codReq<%=ind%>" value="<%= myRemessa.getNumRequerimento() %>" class="input" onClick="javascript: apaga('DC<%=ind%>')">
				  &nbsp;Requerimento:&nbsp;<%= myRemessa.getNumRequerimento() %>&nbsp;-&nbsp;Guia de Remessa:&nbsp;<%= myRemessa.getCodRemessa()%>
	        </td> 
	     </tr>
	     <tr ><td ><img src="<%= path %>/images/inv.gif" width="1" height="2"></td></tr>
	     <%}%>   
		 </table>

		<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
     	  <tr bgcolor="#faeae5">
		        <td  align="right"> 
			    	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('ExcluirProc',this.form);">	
		    			<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
	       	        </button>
	       	    </td>
			 </tr>
		 </table>
		<% } %>
<% } else { %>   

		<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
	     	 <tr bgcolor="#faeae5"><td ><img src="<%= path %>/images/inv.gif" width="1" height="5"></td></tr>
		 </table>
<%} %>		 


</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>