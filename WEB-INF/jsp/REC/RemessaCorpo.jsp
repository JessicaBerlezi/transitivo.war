<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<%
String posTop = request.getParameter("posTop"); if(posTop==null) posTop ="170px";   
String posHei = request.getParameter("posHei"); if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 140; height: <%= posHei %>; visibility: visible;" >  
    
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      
      <tr bgcolor="#be7272"> 
        <td width="25" rowspan="2" align="center"><strong><font color="#FFFFFF">Seq</font></strong></td>			
        <td width="150"><a href="#" onClick="javascript: Classificacao('Processo');">
			<font color="#FFFFFF">Processo</font></a></strong></td>
        <td width="65" align="center"><strong><a href="#" onClick="javascript: Classificacao('Orgao');">
			<font color="#FFFFFF">&Oacute;rg&atilde;o</font></a></strong></td>
		<td width="95" align="center"><strong><a href="#" onClick="javascript: Classificacao('Auto');">
			<font color="#FFFFFF">N&deg; Auto</font></a></strong></td>
        <td width="60" align="center"><strong><a href="#" onClick="javascript: Classificacao('Placa');">
			<font color="#FFFFFF">Placa</font></a></strong></td>
        <td width="223"><strong><a href="#" onClick="javascript: Classificacao('Processo');">
			<font color="#FFFFFF">Processo</font></a></strong></td>
        <td  align="center"><strong><a href="#" onClick="javascript: Classificacao('DatProcesso');">
			<font color="#FFFFFF">Dt Proc</font></a></strong></td>    

      </tr>
      <tr bgcolor="#be7272">
        <td height="14" colspan="4" ><strong><font color="#FFFFFF">&nbsp;&nbsp;&nbsp;Nome</font></a></strong></td>     		
        <td colspan="2"><strong><font color="#FFFFFF">&nbsp;Requerimento - Tipo Req</font></strong></td>
      </tr>  
	</table>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >	  
	  <%
	   int max = 0;	  
	   if ((RemessaId.getAutos().size()>=0) ) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
	    String tipoSolic = "";
		Iterator it = RemessaId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {

			myAuto = (REC.AutoInfracaoBean)it.next() ;				
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (myAuto.getMsgErro().length()>0)  cor = "#CCCCCC"; 
					Iterator ot = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
			while (ot.hasNext()) {
				myReq = (REC.RequerimentoBean)ot.next() ;
				
				if(myAuto.getNumRequerimento().equals(myReq.getNumRequerimento())){									
					tipoSolic = myReq.getCodTipoSolic();
					break;
				}
			}	
		%>
		<tr bgcolor='<%=cor%>'> 
	        <td width=25 rowspan="2" align="center" >
	        &nbsp;<%= (seq+1) %></td>	        	
	        <td width=150 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
	        	&nbsp;<%=myAuto.getNumProcesso() %></td>
			<td width=65 align=center onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getSigOrgao() %></td>
        	<td width=95 align="center" onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumAutoInfracao() %></td>
			<td width=60 align="center"onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumPlaca() %></td>
			<td width=223 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %></td>
				
			<td  onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %></td>
			
		</tr>
		<tr bgcolor='<%=cor%>'> 
	        <td colspan=4>&nbsp;<%if ("DP,1P,2P".indexOf(tipoSolic) >=0) {%>
                  <%=myAuto.getProprietario().getNomResponsavel() %> 
                  <%} else {%>
                      <%=myAuto.getCondutor().getNomResponsavel() %>
                  <%}%>  </td>
	        <td colspan=2>&nbsp;<%=myAuto.getNumRequerimento() %>&nbsp;&nbsp;<%=myAuto.getTpRequerimento() %> </td>	        
		</tr>	
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=8>&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>		
		<% seq++; 
		   max++;
		}
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>  
