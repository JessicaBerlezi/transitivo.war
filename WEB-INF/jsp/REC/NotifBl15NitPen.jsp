<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<!--parte E2-->
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<script language="JavaScript1.2">

function detalheelements(){
//muda todos os elementos com  id="detalhe" para "detalhe_el"
var detalhe_el=document.all.detalhe
//verifica se ha algum objeto com o  id=detalhe
if (detalhe_el!=''&&detalhe_el.length==null)
detalhe_el.style.fontSize='9px'
else{
//for para cada elemento com  id="detalhe"
for (i=0;i<detalhe_el.length;i++)
detalhe_el[i].style.fontSize='7px'
}
} 

function revertback(){
//muda todos os elementos com  id="detalhe" para "detalhe_el"
var detalhe_el=document.all.detalhe
//verifica se ha algum objeto com o  id=detalhe
if (detalhe_el!=''&&detalhe_el.length==null)
detalhe_el.style.fontSize='9px'
else{
//for para cada elemento com  id="detalhe"
for (i=0;i<detalhe_el.length;i++)
detalhe_el[i].style.fontSize='9px'
}
}
window.onbeforeprint=detalheelements
window.onafterprint=revertback

</script>
	  
<!--Linha-->

<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1"></td></tr>
</table>
<!--FIM Linha-->     
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
		
			<tr>
			  <td height="16" align="center" bgcolor="#CCCCCC"><font size="1"><strong>INSTRU&Ccedil;&Otilde;ES GERAIS </strong></font></td>
			</tr>		
		
		<tr><td id="detalhe" style="font-size: 9px;">O pagamento da multa poder&aacute; ser efetuado com este documento, at&eacute; a data do prazo para pagamento com desconto, em qualquer ag&ecirc;ncia do BANERJ ou ITA&Uacute;. O valor expresso nesta notifica&ccedil;&atilde;o j&aacute; contempla o desconto de 20% do seu valor integral (Art 284 - CTB). N&atilde;o realizado o pagamento da multa no prazo estabelecido, seu valor ser&aacute; integral (&sect; &uacute;nico do Art. 284 do CTB). O n&atilde;o pagamento desta guia impedir&aacute; a renova&ccedil;&atilde;o da Licen&ccedil;a Anual e a Transfer&ecirc;ncia de Propriedade. Aquele que adquirir o ve&iacute;culo, cuja as taxas e multas n&atilde;o tenham sido pagas, fica respons&aacute;vel pelo pagamento das mesmas. Em seu pr&oacute;prio benef&iacute;cio, os propriet&aacute;rios de ve&iacute;culos devem manter seus endere&ccedil;os atualizados no DETRAN / RJ.</td>
		</tr>		
		
			<tr>
			  <td height="16" align="center" bgcolor="#CCCCCC"><font size="1"><strong>INSTRU&Ccedil;&Otilde;ES PARA INTERPOR RECURSO </strong></font></td>
  </tr>
			<tr>
			  <td style="font-size: 9px;">O prazo para interposi��o de recursos, em primeira inst�ncia, expira 30 dias ap�s a data do recebimento. O recurso n�o tem efeito suspensivo. N�o havendo provimento, o pagamento da multa ser� integral, de acordo com a legisla��o vigente. Para recorrer, s�o necess�rios os seguintes procedimentos: Se dirigir aos Protocolos avan�ados da SSPTT, localizados na Avenida Visconde do Rio Branco s/n - Centro (Terminal Rodovi�rio Jo�o Goulart -Recep��o 2), Estrada Francisco da Cruz Nunes, 6666 - Piratininga (Administra��o Regional de Piratininga), e na  Av. Almirante Ari Parreiras, 21 - Icara� (Administra��o Regional de Icara�), todas em Niter�i - RJ, com preenchimento do formul�rio espec�fico, endere�ado a Autoridade de Tr�nsito.<br>
			  No formul�rio, o requerente dever� expor as raz�es de defesa e anexar fotoc�pia da Carteira Nacional de Habilita��o, do Certificado de Registro e Licenciamento de Ve�culo e desta notifica��o da penalidade de multa por infra��o de tr�nsito.
			   </td>
			</tr>
	
</table>
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
        <tr><td height="225"> </td></tr>
</table>	  
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_linha_top">
    <tr> 
      <td align="center" height="30"  class="fontmaior">Endere&ccedil;o 
        para Defesa Pr&eacute;via e Recurso:<br>
		<%= NotifBeanId.getTxtEnderecoRecurso() %></td>
    </tr>
</table>
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>


<!--fim parte E2-->