<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" />  

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) { 
	  case 'estonarGuia':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
			break;			    
	   case 'R':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["InformaResultForm"].target = "_self";
	document.all["InformaResultForm"].action = "acessoTool";  
	document.all["InformaResultForm"].submit() ;	 
}

</script>	
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;" >
<form name="InformaResultForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"             type="hidden" value=' '>	
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">	
<input name="statusInteracao"  type="hidden" value='apresentacao'>
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:73px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#faeae5">
        <td width="10%"></td>		     
        <td width="80%" height=23 align=center><b>GUIA DE DISTRIBUI��O&nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=GuiaDistribuicaoId.getNumGuia()%>
			&nbsp;&nbsp;-&nbsp;&nbsp;<%=GuiaDistribuicaoId.getDatProc()%>
        </td>
        <td width="10%" align="center" valign="middle">
		</td>		
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
     <tr bgcolor="#faeae5">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
	     	<tr>
		        <td width="40%" height=20>&nbsp;Junta:&nbsp;<b><%=GuiaDistribuicaoId.getSigJunta() %></b></td>
		        <td width="50%">Relator:&nbsp;<b><%=GuiaDistribuicaoId.getNomRelator() %></b>&nbsp;</td>        
				<td width="10%" align="center">	    
					<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('estonarGuia',this.form);">	
    	      			<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  		</button>
			    </td>
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=3 height=5></td></tr>     
  </table> 
  
</div>
<%
String posTop = "150 px";
String posHei = "143 px";
%>

<%@ include file="GuiaCorpo.jsp" %>  
  

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>


