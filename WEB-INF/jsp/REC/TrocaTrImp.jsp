<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="ExecutarTRIId"      scope="request" class="REC.ExecutarTRIBean" /> 
<%
	String proxAcao = (String)request.getAttribute("proxAcao") ;
	if (proxAcao==null) proxAcao="InformaTRI" ;
%>   
<jsp:include page="CssImpressao.jsp" flush="true" /> 
<!--Include da Parte Fixa-->
<jsp:include page="AutoImp.jsp" flush="true" />

<!-- Chama o Objeto da Infracao -->
  <%		
	ACSS.OrgaoBean org = new ACSS.OrgaoBean();			
  %>

<!--FIM Detalhe-->   
  <!--Requerimento Troca Real Infrator-->  
  <table width="720" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr> 
 <% if ("S".equals(ExecutarTRIId.getEventoOK())) { %>   
      <td width="59" valign="top"><strong>&nbsp;&nbsp;Nome :</strong></td>
      <td width="300" valign="top"><%=RequerimentoId.getNomCondutorTR()%></td>
      <td width="72" valign="top"><strong>Endere&ccedil;o :</strong></td>
      <td width="289" valign="top"><%=RequerimentoId.getTxtEnderecoTR()%></td>
    </tr>
    <tr><td height="2"></td></tr>	
  </table>
  <table width="720" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr > 
      <td width="59" valign="top">&nbsp;&nbsp;<strong>Bairro :</strong></td>
      <td width="185" valign="top"><%=RequerimentoId.getNomBairroTR()%></td>
      <td width="57" valign="top"><strong>Munic&iacute;pio :</strong></td>
      <td width="172" valign="top"><%=RequerimentoId.getNomCidadeTR()%></td>
      <td width="22" valign="top"><strong>UF :</strong></td>
      <td width="22" valign="top"><%=RequerimentoId.getCodUfTR()%></td>
      <td width="29" valign="top"><strong>&nbsp;</strong></td>
      <td width="80" valign="top">&nbsp;</td>
      <td width="29" valign="top" class="semborda"><strong>CEP :</strong></td>
      <td width="65" valign="top" class="semborda"><%=RequerimentoId.getNumCEPTR()%></td>
    </tr>
    <tr><td height="2"></td></tr>		
  </table>
  <table width="720" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
    <tr > 
      <td width="59" valign="top" >&nbsp;&nbsp;<strong>CPF :</strong></td>
      <td width="185" valign="top"><%=RequerimentoId.getNumCPFTR()%></td>
      <td width="136" valign="top" ><strong>N� CNH / Perm. p/ Dirigir :</strong></td>
      <td width="94" valign="top"  ><%=RequerimentoId.getNumCNHTR()%></td>
      <td width="47" valign="top" ><strong>UF CNH</strong><strong> :</strong></td>
      <td width="199" valign="top" ><%=RequerimentoId.getCodUfCNHTR()%></td>
    </tr>
    <tr><td height="2"></td></tr>			
  </table>
  <table width="720" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr > 
      	<td width="90" ><strong>&nbsp;&nbsp;</strong><strong>Executado por :</strong>&nbsp;</td>
        <td width="108" ><%=RequerimentoId.getNomUserNameTR() %></td>
        <td width="82" ><strong>Org�o Lota��o :</strong></td>
      	<td width="241" ><%=org.getSigOrgao()%></td>
      	<td ><strong>Data de Processamento :</strong>&nbsp;<%=RequerimentoId.getDatProcTR()%>&nbsp;&nbsp;</td>
    </tr> 
    <tr><td height="7"></td></tr>	
    
 <% } else { %>
    	    <td height=23 colspan=3 align="left" valign="middle"> 
				<%=ExecutarTRIId.getMsg()%>
	        </td>	
		<% } %>
    		   
  </table>
    <!--FIM RequerimentoIduerimento Troca Real Infrator--> 

