<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objetos -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RemessaIncluirProcBeanId"  scope="request" class="REC.RemessaIncluirProcBean" /> 
<jsp:useBean id="RemessaId" scope="request" class="REC.RemessaBean" /> 
<%

String codReq = "";

 if ( (String) session.getAttribute("codReq") != null)
  codReq = (String) session.getAttribute("codReq");

RemessaIncluirProcBeanId.setComboReqs(AutoInfracaoBeanId,codReq); 
RemessaIncluirProcBeanId.setComboGuias(AutoInfracaoBeanId); 
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {

	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
			
	   case 'LeReq':
	   		if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
				
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			   	fForm.action = "acessoTool";  
			   	fForm.submit();
		   	}
		   	break;
		   	
	   case 'IncluirProcesso':
	   		if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
				
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			   	fForm.action = "acessoTool";  
			   	fForm.submit();
		   	}
		   	break;

	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break; 

	   case 'N':  // Esconder os erro
	   		if(bProcessando)
			{
		   		alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;	
				
				fForm.acao.value="Novo";
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		   	}
		  	break;  
  	}
}

</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmEtiqueta" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"   type="hidden" value=' '>						
<!--IN�CIO_CORPO_sistema--> 
<jsp:include page="lerAutoPlaca.jsp" flush="true" />
<jsp:include page="apresentaInfracao.jsp" flush="true" />
<div style="position:absolute; left:66px; top:269px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
   
<div id="recurso3" style="position:absolute; left:66px; top:274px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
<% if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0) { %> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF">
  	 <tr bgcolor="#faeae5"><td colspan="5"><img src="<%= path %>/images/inv.gif" width="1" height="5"></td></tr>
	  	<% if ("S".equals(RemessaIncluirProcBeanId.getEventoOK())) { %>			 	 
     <tr bgcolor="#faeae5">
	        <td height=23 valign="top" width="60" >&nbsp;&nbsp;Requerimento:&nbsp;</td>
	        <td align="left" valign="top" colspan="2"> 
			<%= RemessaIncluirProcBeanId.getComboReqs() %>
	        </td>	 
     </tr>
     <tr bgcolor="#faeae5">

       	    <td height=23 valign="top" width="40" >&nbsp;&nbsp;Guia:&nbsp;</td>
    	    <td align="left" valign="top" > 
			<%= RemessaIncluirProcBeanId.getComboGuias() %>
	        </td> 
	        
	        <td align="right" valign="middle" > 
		    	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('IncluirProcesso',this.form);">	
	    			<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
	        	</button>
			</td>
			
		<% } else { %>
    	    <td height=23 colspan=5 align="left" valign="middle" bgcolor="#faeae5"> 
				&nbsp;&nbsp;<jsp:getProperty name="RemessaIncluirProcBeanId" property="msg" />
	        </td>	       
		<% } %>
     </tr>
     <tr bgcolor="#faeae5"><td colspan="5"><img src="<%= path %>/images/inv.gif" width="1" height="5"></td></tr>
  </table>
<% } %>
</div>
<div style="position:absolute; left:66px; bottom:65px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>