<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List"%>
<%@ page import="REC.GuiaDistribuicaoBean"%>
<%@ page import="REC.AutoInfracaoBean"%>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 

<%
String posTop = request.getParameter("posTop"); if(posTop==null) posTop ="170px";   
String posHei = request.getParameter("posHei"); if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
	  <%
		REC.AutoInfracaoBean myAuto  = null;	
		Iterator guias = GuiaDistribuicaoId.getAutos().iterator();
		Iterator it = null;
		while (guias.hasNext()) {
			GuiaDistribuicaoBean guia = (GuiaDistribuicaoBean)guias.next();
		%>
		<tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>
		<tr>
			<td height="5" colspan="3">&nbsp;<strong>Junta: <%=guia.getSigJunta()%></strong></td>
		  <td colspan="2" align="center" bgcolor="#FFFFFF"><strong>Guia: <%=guia.getNumGuia()%></strong></td>
		  <td colspan="2" align="right" bgcolor="#FFFFFF"><strong>Relator: <%=guia.getNomRelator()%></strong></td>
		</tr>
		<%
			List autos = guia.getAutos();
			int seq = 0;
			String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (autos.size()>0) {
				it = autos.iterator();
				while (it.hasNext()) {
					myAuto = (AutoInfracaoBean)it.next() ;		
					cor    = (seq%2==0) ? "#faeae5" : "#f5d6cc";
					if (myAuto.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width=40 height="14" align=center>
	        	&nbsp;<%= (seq+1) %>&nbsp;</td>
	        <td width=80 align="center">
	        	&nbsp;<%=myAuto.getSigOrgao() %></td>
			<td width=80 align="center" >
				&nbsp;<%=myAuto.getNumAutoInfracao() %></td>
			<td width=150>
				&nbsp;&nbsp;<%=myAuto.getNumProcesso() %>   </td>
			<td width=80 align="center">
				&nbsp;<%=myAuto.getDatProcesso() %>    </td>
			<td width=200>&nbsp;&nbsp;<%=myAuto.getNumRequerimento() %></td>			
	        <td>&nbsp;&nbsp;<%=myAuto.getRequerimentos(0).getNomResultRS() %></td>
		</tr>
		<%			if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=7>&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% 			} %>	
    	<tr><td height=1 colspan=7 bgcolor="#666666"></td></tr>
		<% 			seq++; 
				}
			}	
			else {
		 		String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 		if (msg.length()>0) {
	%>	
		<tr bgcolor='#faeae5'> 
	        <td height="14" colspan="7" align=center bgcolor="#CC3300"><b><%= msg %></b></td>
		</tr>	
		<tr><td height=1 colspan=7 bgcolor="#666666"></td></tr>
		<%	
				} 
			}%>
		<tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>
		<tr><td height=7 colspan=7 bgcolor="#ffffff"></td></tr>
	<%	}%>
  </table>      
</div>  
