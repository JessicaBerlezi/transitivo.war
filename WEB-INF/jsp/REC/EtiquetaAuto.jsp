<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Iterator" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<jsp:include page="Css.jsp" flush="true" />
<!--M�dulo : REC
	Vers�o : 2.0
	Atualiza��es: 11/03/2008
-->	
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {

	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	  case 'buscaEtiquetas':
	  		if (veCampos(fForm)) {
				fForm.acao.value='buscaEtiquetas';
		   		fForm.target= "_self";
		    	fForm.action = "acessoTool";  
		   		fForm.submit();
		   	}
		   	else {
		   		alert(sErro);
		   	}
		   	break;
	   case 'retorna':
			fForm.acao.value='retorna';
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();
		   	break;
	   case 'atualizaEtiquetas':
	  		if (veCampos(fForm)) {
			<%if(UsuarioBeanId.getCodOrgaoAtuacao().equals("258650")){ //Niteroi%>
	  			if (fForm.numLote.value.length<4) {
	  				alert('N�mero do lote precisa ter o formato 99999');
			<%}else{%>
	  			if (fForm.numLote.value.length<11) {
	  				alert('N�mero do lote precisa ter o formato 999-99-9999');
			<%}%>
	  				break;
	  			}
				fForm.acao.value='atualizaEtiquetas';
		   		fForm.target= "_self";
		    	fForm.action = "acessoTool";  
		   		fForm.submit();
		   	}
		   	else {
		   		alert(sErro);
		   	}
		   	break; 

	   case 'consultaAutoEtiq':
	       fForm.acao.value=opcao;
		   fForm.target= "_self";
		   fForm.action = "acessoTool";  
		   fForm.submit();
		   break;
		   
	   case 'mostraMultiEtiquetas':
	       fForm.acao.value=opcao;
		   fForm.target= "_self";
		   fForm.action = "acessoTool";  
		   fForm.submit();
		   break; 

	   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
   	   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  	  break;	  

	}			
}
  
function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;

	if (trim(fForm.numLote.value)=="" || trim(fForm.numCaixa.value)=="") {
	   valid = false
	   sErro = "Lote ou Caixa n�o selecionado. \n"
	   return valid;
	}
	return valid;		
}

function teclouEnter(obj,evento) {
	var tecla;
	tecla = evento.keyCode;
	if (tecla == 13)
		return true;
	else
		return false;
}

function acaoDigitaAuto(obj,evento,myform,ind_atual) {	
	
<%if(UsuarioBeanId.getCodOrgaoAtuacao().equals("258650")){ //Niteroi%>
	Mascaras(obj,'NIT999999999');
<%}else{%>
	Mascaras(obj,'A99999999999');
<%}%>
	var nomecampo;
	var campoText;
	if(teclouEnter(obj,evento)) {
		if (ind_atual!=81)
			ind_atual++;
		document.all("numAuto" + ind_atual).focus();
		if(ind_atual>=2) {	
			var ind_ant=ind_atual-1;
		<%if(UsuarioBeanId.getCodOrgaoAtuacao().equals("258650")){ //Niteroi%>
			document.all("numAuto" + ind_atual).value=document.all("numAuto" + ind_ant).value.substring(0,4);
		<%}else{%>
			document.all("numAuto" + ind_atual).value=document.all("numAuto" + ind_ant).value.substring(0,1);
		<%}%>
			document.all("numAuto" + ind_atual).focus();
		}
	}
}

function testebla(obj) {
	obj.value="W";
}

function acaoDigitaCaixa(obj,fForm,evento) {	
	if(teclouEnter(obj,evento)) {
		obj.form.numLote.focus();
	}
}

function acaoDigitaLote(obj,fForm,evento) {	
<%if(UsuarioBeanId.getCodOrgaoAtuacao().equals("258650")){ //Niteroi%>
	Mascaras(obj,'99999');
<%}else{%>
	Mascaras(obj,'999-99-9999');
<%}%>
	if(teclouEnter(obj,evento)) {
		valida('buscaEtiquetas',fForm);
	}
}

function changeOrgao(obj,fForm) {
	if (obj.value!=0)
		fForm.numAutoInfracao.value="";	
}

function guardaValoresCaixaLote (numCaixa,numLote) {
var fForm=document.form;
	fForm.numCaixaBackUp.value = numCaixa;
	fForm.numLoteBackUp.value = numLote;
	valida('atualizaEtiquetas',fForm);
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="etiquetaAutoForm" method="post" action="" >
<input name="acao" type="hidden" value=' '>		
<input name="numCaixaBackUp" type="hidden" value='<%=(String) request.getAttribute("numCaixaBackup")%>'>		
<input name="numLoteBackUp" type="hidden" value='<%= (String) request.getAttribute("numLoteBackup")%>'>	
<jsp:include page="Cab.jsp" flush="true" />

<%
	String numCaixa = (String) request.getAttribute("numCaixa");
	String numLote = (String) request.getAttribute("numLote");
	Iterator itx = ((ArrayList) request.getAttribute("listAuto")).iterator();
%>


<!--IN�CIO_CORPO_sistema-->
<div id="filtro_superior" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 60px; left:185px; visibility: visible;" > 
	<table border="0" cellpadding="0" cellspacing="1" width="400"  align="left" class="titprincipal">    
  	<tr> 
    	<td valign="top" colspan="3"><img src="images/inv.gif" width="3" height="15"></td>     
  	</tr>
	<tr>
		<td align="right" >
		<strong>Caixa:</strong>&nbsp;<input name="numCaixa" type="text" value="<%=numCaixa%>" size="18" maxlength="10" onKeyPress="javascript:acaoDigitaCaixa(this,this.form,event);">&nbsp;&nbsp;&nbsp;<b>Lote:&nbsp;<input name="numLote" type="text" value="<%=numLote%>" size="18" maxlength="11" onKeyPress="javascript:acaoDigitaLote(this,this.form,event);" onFocus="javascript:this.select();" ></b></td>
		<td align="right">
			<%
				if(itx.hasNext() || (!numCaixa.equals("") && !numLote.equals("")) ) {
			%>
		
			<button type=button style="height: 21px; width: 54px; border: none; background: transparent;"  onClick="javascript: valida('retorna',this.form);">			  
		  		<img src="<%= path %>/images/bot_retornar.gif" width="52" height="19">
			</button>
			<button type=button style="height: 21px; width: 54px; border: none; background: transparent;"  onClick="javascript: valida('atualizaEtiquetas',this.form);">			  
		  		<img src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" alt="" border="0">
			</button>
			<%  }
				else { %>
			<button type=button style="height: 21px; width: 28px; border: none; background: transparent;"  onClick="javascript: valida('buscaEtiquetas',this.form);">			  
		  		<img src="<%= path %>/images/bot_ok.gif" width="26" height="19">
			</button>
			<button type=button style="height: 21px; width: 56px; border: none; background: transparent;"  onClick="javascript: valida('consultaAutoEtiq',this.form);">			  
		  		<img src="<%= path %>/images/bot_consulta.gif" width="54" height="19">
			</button>
			<button type=button style="height: 21px; width: 54px; border: none; background: transparent;"  onClick="javascript: valida('mostraMultiEtiquetas',this.form);">			  
		  		<img src="<%= path %>/images/bot_imprimir.gif" width="52" height="19" alt="" border="0">
			</button>			
			<% }%>			
			
		</td>
	</tr>
	<tr> 
    	<td valign="top" colspan="3"><img src="images/inv.gif" width="1" height="12"></td>
    </tr>	 
  	</table>
<!--FIM_CORPO_sistema--> 
</div>

<%
if(itx.hasNext() || (!numCaixa.equals("") && !numLote.equals("")) ) { %>

<div id="etiquetas_digitacao" style="position:absolute; width:420px; height:260px; overflow: auto; z-index: 1; top: 100px; left:204px; visibility: visible;"> 
<%
    REC.EtiquetaAutoBean etqBean = new REC.EtiquetaAutoBean();
    String numAuto="";
    int codEtq=0;
    int cont=0;
%>	
	<table border="0" cellpadding="0" cellspacing="0" align="left" class="titprincipal">
	<% for(int contLinha=0;contLinha<27;contLinha++) { %>
		<tr align="center">
		<td>
			<tr>
				<% 
				cont++;
				if (itx.hasNext()) {
					etqBean=(REC.EtiquetaAutoBean)itx.next();
					numAuto=etqBean.getNumAutoInfracao();
					codEtq=etqBean.getCodEtqAutoInfracao();
				}
				else {
					numAuto="";
					codEtq=0;
				}
				%>
				<td width="100" height="22" align="center" background="<%= path %>/images/REC/bg_etiq.gif"><input name="numAutoInfracao" id="numAuto<%=cont%>" type="text" style="height:14px; font-size:10px; border:1px solid #c7c7c7;" value="<%=numAuto%>" size="16" onKeyPress="javascript:acaoDigitaAuto(this,event,this.form,<%=cont%>);"  onKeyUp="this.value=this.value.toUpperCase()"></td>
				<input type="hidden" name="codEtq" value=<%=codEtq%>>
				<td><img src="<%= path %>/images/inv.gif" width="5" height="22"></td>
				<%
				 cont++;
				 if (itx.hasNext()) {
					etqBean=(REC.EtiquetaAutoBean)itx.next();
					numAuto=etqBean.getNumAutoInfracao();
					codEtq=etqBean.getCodEtqAutoInfracao();
				}
				else {
					numAuto="";
					codEtq=0;
				}
				%>
				<td width="100" height="22" align="center" background="<%= path %>/images/REC/bg_etiq.gif"><input name="numAutoInfracao" id="numAuto<%=cont%>" type="text" style="height:14px; font-size:10px; border:1px solid #c7c7c7;" value="<%=numAuto%>" size="16" onKeyPress="javascript:acaoDigitaAuto(this,event,this.form,<%=cont%>);"  onKeyUp="this.value=this.value.toUpperCase()"></td>
				<input type="hidden" name="codEtq" value=<%=codEtq%>>
				<td><img src="<%= path %>/images/inv.gif" width="5" height="22"></td>
				
				<% 
					cont++;
					if (itx.hasNext()) {
					etqBean=(REC.EtiquetaAutoBean)itx.next();
					numAuto=etqBean.getNumAutoInfracao();
					codEtq=etqBean.getCodEtqAutoInfracao();
				}
				else {
					numAuto="";
					codEtq=0;
				}
				%>
				<td width="100" height="22" align="center" background="<%= path %>/images/REC/bg_etiq.gif"><input name="numAutoInfracao" id="numAuto<%=cont%>" type="text" style="height:14px; font-size:10px; border:1px solid #c7c7c7;" value="<%=numAuto%>" size="16" onKeyPress="javascript:acaoDigitaAuto(this,event,this.form,<%=cont%>);" onKeyUp="this.value=this.value.toUpperCase()"></td>
				<input type="hidden" name="codEtq" value=<%=codEtq%>>
			</tr>		
		</td>
		</tr>
		<tr height="5">
			<td colspan="5" width="310" height="5"><img src="<%= path %>/images/inv.gif" width="5" height="5"></td>
		</tr>		
		<% 
		 if ( (contLinha+1)!=27 && (contLinha+1)%9==0) { 
		 %>
			 <!--LINHA-->
			<tr>
			<td colspan=5>
		  		<table width="100%" border="0" cellpadding="0" cellspacing="0" >	
		    		<tr><td><hr noshade  color="#993300"></td></tr>	
		  		</table>
			</td>
			</tr>
			<!--Fim LINHA-->		
			<tr height="5">
				<td colspan="5" width="310" height="5"><img src="<%= path %>/images/inv.gif" width="5" height="5"></td>
			</tr>		 
		 <%
		 }
		
		} %>		

	</table>
</div>
<% } //fim do if para testar visualizacao da div de etiquetas 
  else {
%>
  <script>document.etiquetaAutoForm.numCaixa.focus();</script>
<% } %>

<!--Bot�o retornar--> 
<div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);">
		  <img src="<%= path %>/images/REC/bot_retornar.gif" alt="Retornar ao Menu" width="25" height="24"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<% String msgErro = (String)request.getAttribute("msgErro"); %>

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=msgErro%>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>