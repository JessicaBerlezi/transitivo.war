<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="ParamOrgBeanId" scope ="session" class = "REC.ParamOrgBean"/>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 



<% 	Iterator nit = GuiaDistribuicaoId.getAutos().iterator() ;
    int seqNit = 0;
    int npag = 0;
    int contLinha = 0;
    String cor = "";
    REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
    if (GuiaDistribuicaoId.getAutos().size()>0) { 
		while (nit.hasNext()) {
			myAuto = (REC.AutoInfracaoBean)nit.next() ;
			cor    = "#ffffff";
			if (myAuto.getMsgErro().length()>0)  {
				cor = "#CCCCCC"; 
			}			
			seqNit++;
			contLinha++;
			npag++;%>

	<jsp:include page="GuiaPreparaCabNitImp.jsp" flush="true" >
    <jsp:param name="nPag" value= "<%= npag %>" />				
    </jsp:include> 
  <table id="autosImp" width="100%" border="0" cellpadding="1" cellspacing="1" >
    <tr bgcolor='<%=cor%>'> 
      <td width=25 height="14" align=center rowspan=2><%= seqNit %></td>
      <td width=150>&nbsp;<%=myAuto.getDatInfracao() %></td>
      <td width=65 rowspan="2" align=center><%=myAuto.getSigOrgao() %></td>
      <td width=95 rowspan="2" align="center"><%=myAuto.getNumAutoInfracao() %></td>
      <td width=60 rowspan="2" align="center"><%=myAuto.getNumPlaca() %></td>
      <td width=223>&nbsp;<%=myAuto.getNumProcesso() %></td>
      <td width=70 rowspan="2"><%=myAuto.getDatProcesso() %></td>
      <!--<td width=56 rowspan="2"></td>-->
    </tr>
    <tr bgcolor='<%=cor%>'> 
      <td>&nbsp;<%=myAuto.getInfracao().getDscEnquadramento() %> 
      </td>
      <td>&nbsp;<%=myAuto.getNumRequerimento() %>-<%=myAuto.getTpRequerimento() %> 
      </td>
    </tr>
    <tr bgcolor='<%=cor%>'>    
      <td height="25" colspan=8><strong>&nbsp;Resultado:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
   	    <input name="codResultRSNit<%= seqNit %>" type="radio" value="D" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">
        DEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	    <input name="codResultRSNit<%= seqNit %>" type="radio" value="I" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">
        INDEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo:
		<% if (myAuto.getRequerimentos(0).getTxtMotivoRS().length()>60) { %>
	        <%= myAuto.getRequerimentos(0).getTxtMotivoRS().substring(0,60) %>      
	    <% } else { %>
	        <%= myAuto.getRequerimentos(0).getTxtMotivoRS() %>      	    
	    <% } %>
      </td>
    </tr>
    <% if (myAuto.getRequerimentos(0).getCodResultRS().equals("D")) { %>
	<tr bgcolor='<%=cor%>' >
	     <td  colspan=7 height=20>&nbsp;&nbsp;<input type="radio" checked="checked"  name="codMotivo<%= seqNit %>" value="1" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
		       <input  type="radio" name="codMotivo<%= seqNit %>" value="2" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
		       <input  type="radio" name="codMotivo<%= seqNit %>" value="3" <%= sys.Util.isChecked(myAuto.getRequerimentos(0).getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
		   <!--<input  type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros -->
		 </td>
    </tr>
    <%}%>
	<% if (myAuto.getRequerimentos(0).getCodRemessa().length()>0) { %>
    <tr bgcolor='<%=cor%>'>
        <td colspan="2"><strong>&nbsp;&nbsp;N&ordm;&nbsp;Guia de Remessa :</strong>&nbsp;<%=myAuto.getRequerimentos(0).getCodRemessa()%></td>        
        <td colspan=6><strong>&nbsp;&nbsp;Data Envio :</strong>&nbsp;<%=myAuto.getRequerimentos(0).getDatEnvioGuia() %></td>
    </tr>
   	<% } %>	 
  	<% if (myAuto.getMsgErro().length()>0) { %>
		<tr bgcolor='<%=cor%>' >
	        <td colspan=8>&nbsp;<%=myAuto.getMsgErro() %> </td>
		</tr>			
	<% } %>	
    
    	<jsp:include page="rodape_impconsulta.jsp" flush="true" />
        <%if (GuiaDistribuicaoId.getAutos().size() != seqNit){%>
		   <div class="quebrapagina"></div>
		<%}%>   
    <%}%>
  <%}%>
