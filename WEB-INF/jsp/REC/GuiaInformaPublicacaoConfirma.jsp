<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head> 
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'AtualizaPublicacao':
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;
	   case 'R':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
	    	close() ;
			break;		
	   case 'Imprime':
	   		fForm.acao.value = opcao;
			fForm.target     = "_blank";
			fForm.action     = "acessoTool";
			fForm.submit();
			break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="EnviarDOForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"           type="hidden" value=' '>		
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:73px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#faeae5">
        <td width="10%"></td>		     
        <td width="80%" height=23 align=center><b>ATA DE PUBLICA&Ccedil;&Atilde;O:&nbsp;<%= GuiaDistribuicaoId.getCodEnvioDO() %>&nbsp;
	Enviada em:&nbsp;<%= GuiaDistribuicaoId.getDatEnvioDO()%>
	<% if (GuiaDistribuicaoId.getDatPubliPDO().length()>0) { %>	
	&nbsp;&nbsp;Publicada em:&nbsp;<%= GuiaDistribuicaoId.getDatPubliPDO()%>
	<% } %>	
        </td>
        <td width="10%" align="center" valign="middle"> 
        <%	String flag = (String)request.getAttribute("flag"); 
        	if( flag == null ){
        	%>
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AtualizaPublicacao',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
        <%	}
        	else if(flag.equals("S")){%>
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick= "javascript: valida('Imprime',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19"> 
        	  </button>
		<%	}
			else{}%>
		</td>
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
  </table>
  <jsp:include page="GuiaAtaTop.jsp" flush="true" />  
</div>
<jsp:include page="GuiaAtaCorpo.jsp" flush="true" >  
  <jsp:param name="posTop" value= "132 px" />
  <jsp:param name="posHei" value= "220 px" />
</jsp:include> 



<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= GuiaDistribuicaoId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= GuiaDistribuicaoId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>


