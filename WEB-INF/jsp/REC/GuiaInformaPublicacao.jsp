<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="REC.GuiaDistribuicaoBean"%>
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'LeATA':
      if (veCampos(fForm)==true) {			
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'R':
	  fForm.acao.value=opcao
	  fForm.target= "_self";
	  fForm.action = "acessoTool";  
	  fForm.submit();	  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	naoSelec = true ;
	for(i=0;i<fForm.atas.length;i++){	
	    if(fForm.atas.options[i].selected){
			naoSelec = false ; 
    	}
	}  
	if (naoSelec)  {
		valid = false ;
		sErro = "ATA n�o selecionada. \n"
	}	
	verString(fForm.datPubliPDO,"Data de publica��o",0);
	if (valid==false) alert(sErro) ;
	return valid ;
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:660px; overflow: visible; z-index: 1; top: 101px; left:83px;" > 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
        <td width="280" height=23>
			<strong>Data de Publica��o:&nbsp;&nbsp;</strong>
	        <strong><input type="text" name="datPubliPDO" size="12" maxlength="10" value='<%=GuiaDistribuicaoId.getDatPubliPDO() %>' 
	           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"></strong>        
        </td>
        <td width="100"></td> 
		<td align="left">
        </td>
		<td height="1" bgcolor="#FFFFFF"></td>
	      <td align="center" valign="middle"> 
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('LeATA',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
		  </td>
     </tr>
     <tr><td colspan=4 height=18></td></tr>     
  </table>
</div>
<div style="position:absolute; width:660px; z-index: 1; top: 159px; left: 81px;">
  <table border="0" cellpadding="0" cellspacing="0" width=100% align="center">
    <tr bgcolor="#ffffff" >
      <td align="center">
	  	<table bgcolor="#faeae5" border="0" cellpadding="0" cellspacing="0" width="100%"  align="left">
          <%List atas = (List)request.getAttribute("atas");%>
            <td id="disponiveis" bgcolor="#faeae5" align="center" height=23>
				  <select name="atas" style="width:520px">
					<%for(int i=0; i<atas.size(); i++){
						out.println("<option value=\"" + ((GuiaDistribuicaoBean)atas.get(i)).getCodEnvioDO() + "\">" + 
								"ATA: " +((GuiaDistribuicaoBean)atas.get(i)).getCodEnvioDO() + 
								" Enviada em: " + ((GuiaDistribuicaoBean)atas.get(i)).getDatEnvioDO() + 
								" Publicada em: " + ((GuiaDistribuicaoBean)atas.get(i)).getDatPubliPDO() +
								 " - Status:  " + ((GuiaDistribuicaoBean)atas.get(i)).getNomStatus() +"</option>");
					}
					out.println("</select>");
			%>
            </td>
      	</table>
	  </td>
    </tr>
  </table>
</div>
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= GuiaDistribuicaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
</form>
</BODY>
</HTML>


