<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="GuiaPreparaImp" method="post" action="">	 
<%
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	String dtReq="";
	String	cor = "#ffffff";	
    String tipoSolic = "";
	Iterator ot = RemessaId.getAutos().iterator() ;
	REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
	if (RemessaId.getAutos().size()>0) { 
		while (ot.hasNext()) {
			myAuto = (REC.AutoInfracaoBean)ot.next() ;
			cor    = "#ffffff";
			if (myAuto.getMsgErro().length()>0)  {
				cor = "#CCCCCC"; 
			}		
			Iterator at = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
			while (at.hasNext()) {
				myReq = (REC.RequerimentoBean)at.next() ;
				
				if(myAuto.getNumRequerimento().equals(myReq.getNumRequerimento())){									
					dtReq=myReq.getDatProcDP();	
					tipoSolic = myReq.getCodTipoSolic();
					break;
				}					
			}			
			seq++;
			contLinha++;
			//Faz quebra de linha a cada 13 linhas, exceto na primeira linha.
			if (contLinha>13){
			    contLinha=1;
				npag++;			
				if (npag!=1){			
		%>
						</table>      
					<jsp:include page="RemessaRod.jsp" flush="true" />
					<jsp:include page="rodape_impconsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
				<% } %>				
	<jsp:include page="RemessaPreparaCabImp.jsp" flush="true" >
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
    
  <table id="autosImp" width="100%" border="0" cellpadding="1" cellspacing="1" >
    <% } %>
    <tr > 
      <td width=50 height="14" align=center rowspan=2>&nbsp;<%= seq %>&nbsp;	           
	        </td>
	        <td width=170 onClick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %>
	        <td width=70 onClick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %></td>
			<td width=220 >&nbsp;<%=myAuto.getNumRequerimento() %>&nbsp;-&nbsp;<%=dtReq %></td>
        	<td width=75 onClick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumAutoInfracao() %>       </td>
			<td width=55 onClick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumPlaca() %>   </td>
			<td>  
				&nbsp;<%=myAuto.getDatInfracao() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>'> 
	        <td colspan=6>&nbsp;<%if ("DP,1P,2P".indexOf(tipoSolic) >=0) {%>
                  <%=myAuto.getProprietario().getNomResponsavel() %> 
                  <%} else {%>
                      <%=myAuto.getCondutor().getNomResponsavel() %>
                  <%}%> </td>
		</tr>	
   
	<% if (myAuto.getMsgErro().length()>0) { %>
		<tr bgcolor='<%=cor%>' >
	        <td colspan=8>&nbsp;<%=myAuto.getMsgErro() %> </td>
		</tr>			
	<% } %>	    
    <tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>
    <%	} %>
  </table>      
 	 
<%} else { 
	String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="RemessaPreparaCabImp.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
	<jsp:include page="RemessaRod.jsp" flush="true" />
	<jsp:include page="rodape_impconsulta.jsp" flush="true" />

<%} %>
<%	if (RemessaId.getAutos().size()>0) { 
		if (contLinha<13){

		} %>
        <jsp:include page="RemessaRod.jsp" flush="true" />
		<jsp:include page="rodape_impconsulta.jsp" flush="true" />

<%} %>



<%String imprimeAI = (String) request.getAttribute("imprimeAI");%>
<%if ((imprimeAI != null) && (imprimeAI != "N")) {%>

<div class="quebrapagina"></div>

<!--FIM T�TULO DO M�DULO-->
<!--TELA DE IMPRESSAO RESULTADO-->


<%  	
	int numpag = 0;		
	Iterator it = RemessaId.getAutos().iterator() ;
	REC.AutoInfracaoBean myAutoInf  = new REC.AutoInfracaoBean();%>	
	
<%	if (RemessaId.getAutos().size()>0) { 
		while (it.hasNext()) {
			myAutoInf = (REC.AutoInfracaoBean)it.next() ;			
			 if ( myAutoInf.getTemAIDigitalizado()) { 
			//Faz quebra de P�gina a cada Auto Digitalizado.			
				numpag++;			
				if (numpag!=1){		%>
						</table>      					
					<div class="quebrapagina"></div>
				<% } %>			

<!--T�TULO DO M�DULO-->
	  <table align="center" width="468" border="0" cellpadding="2" cellspacing="0">
		<tr>
        	<td align="left" width="36%"><font color="#000000"><strong>N&ordm; DO AUTO:</strong>&nbsp;<%=myAutoInf.getNumAutoInfracao()%></font></td>
        	<td align="center" width="39%"><font color="#000000"><strong>PROCESSO:</strong>&nbsp;<%=myAutoInf.getNumProcesso()%></font></td>
        	<td align="right" width="25%"><font color="#000000"><strong>PLACA:</strong>&nbsp;<%=myAutoInf.getNumPlaca()%></font></td>
		</tr>
	  </table>
<br>
<!--FIM T�TULO DO M�DULO-->

<!--TELA DE IMPRESSAO RESULTADO-->
<div align="center" style=" width: 100%;">	
	<img id="imagemAI" alt="Imagem do Auto de Infra��o: <%=myAutoInf.getNumAutoInfracao()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=myAutoInf.getAIDigitalizado().getNomFoto()%>&contexto=<%=myAutoInf.getAIDigitalizado().getParametro(ParamSistemaBeanId)%>&header=#" width="470" height="903"> 
</div>


<!--FIM TELA DE IMPRESSAO RESULTADO-->
<% }
     }
       }
}%>




</form>
</body>
</html>