<%@page import="REC.ControladorRecGeneric"%>
<%@page import="REC.AutoInfracaoBean"%>
<%@page import="java.util.*"%>
<%@page import="REC.GerenciarAta"%>
<%@page import="sys.ControladorServlet"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Relat�rio: Ata de Publica��o</title>
		
		<link rel="stylesheet" href="css/transitivo.css" />
		<style>
		</style>
	</head>
	<body style="font-family: verdana;">
		<div class="cab-relatorio" id="head">
			<div style="display: block; font-size: 9px; width: 100%;">
				EMISS�O: <strong><%=sys.Util.formatedToday().substring(0,10)%> - <%=sys.Util.formatedToday().substring(13,18)%></strong>
			</div>
			<div style="width: 10%;">
				<img alt="Prefeitura de Maca�" src="images/prefeitura_macae.png" style="width: 200px; height: 90px;">
			</div>
			<div style="text-align: center; vertical-align: top; width: 70%; margin: auto auto;">
				<p>PREFEITURA MUNICIPAL DE MACA�</p>
						<p>:: TRANSITIVO ::</p>
					<p>RELAT�RIO DE ATAS DE PUBLICA��O</p>
			</div>
			<div style="float: right; padding: 25px 0px; width: 10%; margin-right: 30px;">
				<img alt="" src="images/im_logosmit.gif" width="98px" height="30px">
			</div>
		</div>
		<div id="body" style="font-size: 12px;">
			<div style="text-align: justify; width: 80%; margin: 10px auto;">
				RESULTADO DOS JULGAMENTOS DE <SPAN STYLE="COLOR: RED;"><%= ControladorRecGeneric.dateToDateBr(GerenciarAta.listProcessos.get(0).get("dataSessao")).substring(3) %></SPAN>, 
				REALIZADOS PELA COMISS�O DE AN�LISE DE RECURSOS DE <SPAN STYLE="COLOR: RED;"> <%= GerenciarAta.tipoAta %></SPAN> DA <%= session.getAttribute("codNatureza").equals("1") ? "JARI" : "JARIT" %> ATRAV�S DAS NOTIFICA��ES DE
					<% 
						String tipoNotificacao = "AUTUA��O";
				
						if(GerenciarAta.tipoAta.equals("1� Inst�ncia"))
							tipoNotificacao = "PENALIDADE";
						else if(GerenciarAta.tipoAta.equals("2� Inst�ncia"))
							tipoNotificacao = "2� INST�NCIA";
						else
							tipoNotificacao = "AUTUA��O";
					%>
					<%= tipoNotificacao %>
				APRESENTADAS NAS ATAS
				<SPAN STYLE="COLOR: red;">
					<%
						ArrayList<HashMap<String, String>> listAtas = new ArrayList<HashMap<String, String>>();
						listAtas = GerenciarAta.listAtas;
						
						for(HashMap<String, String> auto : listAtas){
							%>
							<%=auto.get("desAta") + "(" + ControladorRecGeneric.dateToDateBr(auto.get("dataSessao")) + ") "%>
							<%
						}
					%>
				</SPAN>
			</div>
			<div class="processo">
				<div class="head">Processos Deferidos</div>
				<%
						ArrayList<HashMap<String, String>> listProcessos = new ArrayList<HashMap<String, String>>();
						listProcessos = GerenciarAta.listProcessos;
						
						if(listProcessos.size() > 0){
							for(HashMap<String, String> auto : listProcessos){
								if(auto.get("result").equals("D")){
								%>
									<div class="item-processo-por-ata"><%= auto.get("numProcesso") %></div>
								<%}
							}
						}
					%>
			</div>
			<div class="processo">
				<div class="head">Processos Indeferidos</div>
				<%						
						if(listProcessos.size() > 0){
							for(HashMap<String, String> auto : listProcessos){
								if(auto.get("result").equals("I")){
								%>
									<div class="item-processo-por-ata"><%= auto.get("numProcesso") %></div>
								<%}
							}
						}
					%>
			</div>
		</div>
		<div id="footer">
		</div>
		
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
	</body>
</html>