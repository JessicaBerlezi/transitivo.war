<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="InformarResultId"   scope="request" class="REC.InformarResultBean" /> 
<%			
  InformarResultId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'LeReq':
		   if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   		
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	   
		   	}
		  break;
	   case 'InformaResult':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   		
		      if (veCampos(fForm)==true) {		  
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
			  } 
			}		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.codRequerimento,"Requerimento",0);
	valid = ValDt(fForm.datRS,1) && valid
	verString(fForm.datRS,"Data do Resultado",0);
	if (atuRadio(fForm.codResultRSradio,fForm.codResultRS)) {
	  valid = false
	  sErro = sErro + "Resultado n�o selecionado. \n"
	}

	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}
function mostaCodMotivoDef(){
   document.all["codMotivoDef"].style.visibility='visible' ; 
}
function escondeCodMotivoDef(){
   document.all["codMotivoDef"].style.visibility='hidden' ; 
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"        type="hidden" value=' '>
<input name="codResultRS" type="hidden" value="">				

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:66px; top:269px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2 colspan=4 ><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<% if ("S".equals(InformarResultId.getEventoOK())) { %>			 	 
<div id="input" style="position:absolute; left:66px; top:275px; width:687px; height:20px; z-index:1; overflow: visible; visibility:  visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
	    <td valign="middle" >&nbsp;&nbsp;Requerimento:&nbsp;&nbsp;</td>
        <td align="left" valign="middle"> 
			<jsp:getProperty name="InformarResultId" property="reqs" />
	    </td>
	    <td width="31%" >Data do Resultado:&nbsp;
		  <input type="text" name="datRS" size="12" maxlength="10" value='<%=RequerimentoId.getDatRS() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
      </tr>
     <tr><td colspan=3 height=2></td></tr>
     <tr bgcolor="#faeae5">
        <td width="13%">&nbsp;&nbsp;Relator:</td>
        <td width="56%">
          <input disabled name="nomRelator" type="text" size="60" maxlength="43"  value="<%= RequerimentoId.getNomRelatorJU() %>">		
		</td>
        <td valign="middle">Resultado:&nbsp;&nbsp;&nbsp; 
          <input type="radio" name="codResultRSradio" value="D" <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;" onclick="javascript: mostaCodMotivoDef();">Deferido 
          <input type="radio" name="codResultRSradio" value="I" <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;" onclick="javascript: escondeCodMotivoDef();">Indeferido&nbsp;&nbsp;
		</td>
	  </tr>
  </table>
</div>
<div id="codMotivoDef" style="position:absolute; left:66px; top:346px; width:687px; height:20px; z-index:1; overflow: visible; visibility: hidden;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan=4 height=3></td></tr>
      <tr bgcolor="#faeae5">
         <td colspan=4 height=23>&nbsp;&nbsp;<input type="radio" checked="checked" name="codMotivo" value="1" <%= sys.Util.isChecked(RequerimentoId.getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
          <input type="radio" name="codMotivo" value="2" <%= sys.Util.isChecked(RequerimentoId.getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
          <input type="radio" name="codMotivo" value="3" <%= sys.Util.isChecked(RequerimentoId.getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
          <!--<input type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros -->
         </td>
      </tr>
   </table>
</div>  
<div id="txtMotivo" style="position:absolute; left:66px; top:313px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;">     
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr><td colspan=4 height=3></td></tr>
      <tr bgcolor="#faeae5">
        <td width="13%" >&nbsp;&nbsp;Motivo:&nbsp;&nbsp;</td>
        <td width="79%" valign="top"> 
		  <textarea rows=2 style="border-style: outset;" name="txtMotivoRS" cols="105" 
				 onfocus="javascript:this.select();"><%=RequerimentoId.getTxtMotivoRS() %></textarea></td>
        <td width="8%"  align="right" valign="middle"> 
		  	<% if ("S".equals(InformarResultId.getEventoOK())) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('InformaResult',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
			<% } %>
		</td>
      </tr>	  
  </table>		
</div>
<% } else { %>
<div id="msg" style="position:absolute; left:66px; top:274px; width:687px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
   	    <td height=23> 
			<jsp:getProperty name="InformarResultId" property="msg" />
        </td>
      </tr>	  
  </table>		
</div>						
<% } %>  

<div style="position:absolute; left:66px; bottom:66px; width:687px; height:30px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %>

<!--FIM_Div Erros-->
</form>
</body>
</html>
