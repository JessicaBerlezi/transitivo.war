<%@page import="UTIL.TipoRecGeneric"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />

<!doctype HTML>
<html lang="pt-br">
	<head>
		<title>Processo: Atribuir Ata</title>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/transitivo.css" />
	</head>
	<body>
		<div id="head">
			<div class="img-head2">
				<div class="img-head1">
					<div style="position: absolute; right: 6px;">
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
						<div class="img-head-btn" style="background-image: url('/Transitivo/images/detran_help.png');"></div>
					</div>
				</div>
			</div>
			<div id="bread" class="bread">
				<strong class="text">PROCESSO &gt; ATRIBUIR ATA</strong>
			</div>
		</div>
		
		<div class="body">
			<div class="divForm">
				<table id="tableForm">
					<tbody>
						<tr>
							<td>*Tipo de Ata:</td>
							<td>
								<select id="tipoAta" style="width: 100px; height: 25px;" onchange="limpaCboNumAta(); limparListasProcessos(); consultarAtas(this.value); consultarProcessosSemAta();">
									<option value="">Selecione...</option>
									<option value="Def. Previa">Def. Prévia</option>
									<option value="1ª INSTÂNCIA">1ª Instância</option>
									<option value="2ª INSTÂNCIA">2ª Instância</option>
									<option value="PAE">P.A.E</option>
								</select>
							</td>
							<td>*Filtro por Ano: </td>
							<td>
								<select id="cboAno" style="width: 80px; height: 25px;" onchange="filtrarAtas(value);">
								</select>
							</td>
							<td>*Número da Ata:</td>
							<td>
								<select id="listNumAta" onchange="limparListasProcessos(); abilitaBtnOk();" style="width: 150px; height: 25px;">
									<option value="">selecione uma opção</option>
								</select>
							</td>
							<td colspan="2">
								<button class="btn" disabled id="btnListar" onclick="limparProcessosComAta(); limparListasProcessos(); consultarProcessosComAta();">Listar</button>
							</td>
							<td>
								<input type="hidden" id="codOrgao" value="<%= ParamOrgBeanId.getCodOrgao() %>" />
							</td>
							<td id="tdCarregando" hidden="true">
								<img src="/Transitivo/images/carregando-gif.gif" style="width: 50px; height: 30px;" />
								<span style="position: relative; top: -10px;">Carregando...</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style="height: 50px;"></div>
			<div>
				<table style="border: solid 1px gray; width: 95%; margin-left: 55px; height: 475px; font-size: 9px; font-family: verdana;">
					<tbody>
						<tr style="height: 20px;">
							<td style="text-align: center;">
								<span style="float: left; margin-left: 7px;">
									<input type="checkbox" onchange="marcarCbos(this, 'semAta');">Marcar Todos
								</span>
								PROCESSOS SEM ATA<span id="countSemAta" style="margin-left: 5px; color: blue;"></span>
							</td>
							<td></td>
							<td style="text-align: center;">
								<span style="float: left; margin-left: 7px;">
									<input type="checkbox" onchange="marcarCbos(this, 'comAta');"> Marcar Todos
								</span>
								PROCESSOS COM ATA<span id="countComAta" style="margin-left: 5px; color: blue;"></span>
							</td>
						</tr>
						<tr>
							<td style="width: 50%; background-color: #DAA9A9; vertical-align: top;">
								<div style="height: 457px; overflow: auto;" id="processosSemAta">
								</div>
							</td>
							<td style="vertical-align: top;">
								<button title="atribuir ata" style="border: none; background: transparent;" onclick="alterarAtaProcesso('atribuirAta');">
									<img alt="Incluir" src="/Transitivo/images/bot_prox.gif" width="20" height="19">
								</button>
								<button title="remover ata" style="border: none; background: transparent;" onclick="alterarAtaProcesso('removerAta');">
									<img alt="Excluir" src="/Transitivo/images/bot_ant.gif" width="20" height="19">
								</button>
							</td>
							<td style="width: 50%; background-color: #DAA9A9; vertical-align: top;">
								<div style="height: 457px; overflow: auto;" id="processosComAta">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		
		<div id="footer">
			<div class="img-footer2">
				<div class="img-footer"></div>
				<div class="img-footer-btn"></div>
			</div>
		</div>
		<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
		
		<script type="text/javascript">
			var listaAtas = [];

			var listaSemAta = [];
			var listaComAta = [];

			function consultarAtas(tipoAta){
				var tipoRec = <%= TipoRecGeneric.CONSULTAR_ATA %>;
				var codOrgao = document.getElementById("codOrgao").value;
				var cdNatureza = <%= session.getAttribute("codNatureza") %>;
				
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						tipoRec: tipoRec,
						codOrgao: codOrgao,
						tipoAta: tipoAta,
						cdNatureza: cdNatureza
					},
					success: function(data){
						listaAtas = data;
						preencherCboAno();
						filtrarAtas($("#cboAno option:selected").val());
					},
					error: function(){
						alert("erro na requisição de processos sem ata.");
					}
				});
			}
			
			function preencherCboAno(){
				var options = "";
				var anos = [];
				
				if(listaAtas){
					listaAtas.forEach(function(ata){
						if(anos.length > 0){
							var possui = false;
							
							for(var i = 0; i < anos.length; i++){
								if(ata.dataInclusao == anos[i])
									possui = true;
							}
							
							if(!possui)
								anos.push(ata.dataInclusao);
						}
						else
							anos.push(listaAtas[0].dataInclusao);
					});
					
					anos.sort();
					
					for(var i = 0; i < anos.length; i++){
						var anoAtual = new Date().getFullYear();
						
						if(anoAtual == anos[i])
							options += "<option value='" + anos[i] + "' selected>" + anos[i] + "</option>";
						else
							options += "<option value='" + anos[i] + "'>" + anos[i] + "</option>";
					}
					
					document.getElementById("cboAno").innerHTML = options;
				}
			}			
			
			function filtrarAtas(ano){
				if(listaAtas)
					preencherCboAtas(listaAtas, ano);
			}

			function preencherCboAtas(data, ano) {
				var cboAtas = document.getElementById("listNumAta");

				if(!cboAtas) return;

				var options = "<option value=''>selecione uma opção</option>";

				for(var i = 0; i < data.length; i++){
					if(data[i].dataInclusao == ano)
						options += "<option value='" + data[i].codAta + "'>" + data[i].desAta + "</option>";
				}

				cboAtas.innerHTML = options;
			}
			
			function consultarProcessosSemAta(){
				var tipoRec = <%= TipoRecGeneric.CONSULTAR_PROCESSOS_SEM_ATA %>;
				var tipoAta = $("#tipoAta option:selected").val();
				var codOrgao = document.getElementById("codOrgao").value;
				var cdNatureza = <%= session.getAttribute("codNatureza") %>;
				
				if(!tipoAta) return;
				
				exibirCarregando("Carregando processos de " + tipoAta + "...");
			
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						tipoRec: tipoRec,
						tipoAta: tipoAta,
						codOrgao: codOrgao,
						cdNatureza: cdNatureza
					},
					success: function(data){
						preencherProcessosSemAta(data);
					},
					error: function(){
						alert("erro na requisição de processos sem ata.");
					}
				});
			}
			
			function preencherProcessosSemAta(data){
				//if(data == 0) return;
				
				var divSemAta = document.getElementById("processosSemAta");
				var lista = "";
				listaSemAta = data;

				for(var i = 0; i < data.length; i++){
					lista += "<div style='display: inline-block; width: 100%;'>";
					lista += "<span style='padding-right: 15px;'>" + zeroToLeft((i + 1), 4) + "</span>";
					lista += "<input type='checkbox' name='semAta' id='" + data[i].codRequerimento + "' />";
					lista += "<div style='display: inherit;'>" + data[i].numAuto + "</div>";
					lista += "<div style='display: inherit; margin-left: 20px;'>" + data[i].numProcesso + "</div>";
					lista += "</div>";
				}

				document.getElementById("countSemAta").innerHTML = "(" + data.length + ")";
				divSemAta.innerHTML = lista;
				
				esconderCarregando();
			}

			function zeroToLeft(value, size) {
				var zeros = "";

				while(zeros.toString().length + value.toString().length < size) {
					zeros += "0";
				}

				return zeros + value;
			}
			
			function consultarProcessosComAta(){
				var tipoRec = <%= TipoRecGeneric.CONSULTAR_PROCESSOS_COM_ATA %>;
				var codOrgao = document.getElementById("codOrgao").value;
				var tipoAta = $("#tipoAta option:selected").val();
				var codAta = $("#listNumAta option:selected").val();
				
				if(!codAta) return;
				
				exibirCarregando("Carregando processos da ata " + codAta + "...");
				
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						tipoRec: tipoRec,
						codOrgao: codOrgao,
						tipoAta: tipoAta,
						codAta: codAta
					},
					success: function(data){
						preencherProcessosComAta(data);
					},
					error: function(){
						alert("erro na requisição de processos com ata.");
					}
				});
			}
			
			function preencherProcessosComAta(data){
				//if(data == 0) return;
				
				var divComAta = document.getElementById("processosComAta");
				var lista = "";
				listaComAta = data;

				for(var i = 0; i < data.length; i++){
					lista += "<div style='display: inline-block; width: 100%;'>";
					lista += "<span style='padding: 10px; margin-right: 5px;'>" + (i + 1) + "</span>";
					lista += "<input type='checkbox' name='comAta' id='" + data[i].codRequerimento + "' />";
					lista += "<div style='display: inherit;'>" + data[i].numAuto + "</div>";
					lista += "<div style='display: inherit; margin-left: 20px;'>" + data[i].numProcesso + "</div>";
					lista += "</div>";
				}

				document.getElementById("countComAta").innerHTML = "(" + data.length + ")";
				divComAta.innerHTML = lista;
				
				esconderCarregando();
			}

			function alterarAtaProcesso(tipoAlteracao){				
				var tipoRec = <%= TipoRecGeneric.ALTERAR_ATA_PROCESSO %>;
				var codOrgao = document.getElementById("codOrgao").value;
				var codAta = $("#listNumAta option:selected").val();
				var desAta = $("#listNumAta option:selected").text();
				var processos = [];
				
				if(tipoAlteracao == "atribuirAta"){
					if(!validarCampos()) return;
					
					var _processos = $("input:checkbox[name='semAta']");

					for(var i = 0; i < _processos.length; i++){
						if(_processos[i].checked)
							processos.push({
								codRequerimento: _processos[i].id
							});
					}
				}
				else if(tipoAlteracao == "removerAta"){
					_processos = $("input:checkbox[name='comAta']");

					for(var i = 0; i < _processos.length; i++){
						if(_processos[i].checked)
							processos.push({
								codRequerimento: _processos[i].id
							});
					}
				}

				if(processos.length == 0) return;
				
				$.ajax({
					method: "post",
					url: "/Transitivo/ControladorRecGeneric",
					data: {
						tipoRec: tipoRec,
						codOrgao: codOrgao,
						codAta: codAta,
						desAta: desAta,
						tipoAlteracao: tipoAlteracao,
						processos: JSON.stringify(processos)
					},
					success: function(data){
						consultarProcessosSemAta();
						consultarProcessosComAta();
					},
					error: function(){
						alert("erro na requisição de processos com ata.");
					}
				});
			}
		
			function limpaCboNumAta(){
				document.getElementById("listNumAta").innerHTML = "<option value=''>selecione uma opção</option>";
				abilitaBtnOk();
			}
			
			function abilitaBtnOk(){
				var cboNumAta = $("#listNumAta option:selected").val();
				
				if(!cboNumAta)
					document.getElementById("btnListar").disabled = true;
				else
					document.getElementById("btnListar").disabled = false;
			}
			
			function marcarCbos(e, tipoProcesso){
				elements = document.getElementsByName(tipoProcesso);
				
				if(elements.length == 0) return;
				
				if(e.checked){
					for(var i = 0; i < elements.length; i++){
						elements[i].checked = true;
					}
				}
				else{
					for(var i = 0; i < elements.length; i++){
						elements[i].checked = false;
					}
				}
			}
			
			function limparProcessosComAta(){
				document.getElementById("processosComAta").innerHTML = "";
			}

			function limparListasProcessos(){
				//listaSemAta = [];
				//document.getElementById("processosSemAta").innerHTML = "";
				
				listaComAta = [];
				document.getElementById("processosComAta").innerHTML = "";
				document.getElementById("countComAta").innerHTML = "";
			}

			function validarCampos(){
				var isValid = true;
				var tipoAta = $("#tipoAta option:selected").val();
				var codAta = $("#listNumAta option:selected").val();

				if(!tipoAta || !codAta) isValid = false;

				return isValid;
			}
			
			function exibirCarregando(texto){
				var tdCarregando = document.getElementById("tdCarregando");
				
				tdCarregando.hidden = false;
				tdCarregando.children[1].innerHTML = texto;
			}
			
			function esconderCarregando(){
				var tdCarregando = document.getElementById("tdCarregando");
				
				tdCarregando.hidden = true;
			}

// 			$(document).ready(function(){
// 				var d = document.getElementById("footer");
// 				if(window.innerHeight < 770){
// 					d.hidden = "true";
// 				}
// 				else{
// 					d.hidden = "";
// 				}
// 			});

			
		</script>
	</body>
</html>