<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>

<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="RequerimentoBean" scope="session" class="REC.RequerimentoBean"/>

<head>
<jsp:include page="CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
</script>
</head>
<body>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<!--CABE�ALHO IMPRESS�O-->
<div id="cabecalhoimp" style="position: relative; z-index:100;">
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	
		<td height="1" bgcolor="#000000"></td>	
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr><td width="23%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
        </tr>
        <tr> <td width="38%" height="40" valign="top"><img src="images/<%=SistemaBeanId.getCodUF()%>/im_logodetran.gif"></td>
        
        <%
        	System.out.print("Valor : "  + SistemaBeanId.getCodUF().toString());
        %>
        </tr>
      </table>
    </td>        
    <td width="60%" align="center" valign="top" class="fontmaior"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
      <STRONG style="font-size: 11px;">:: TRANSITIVO ::</strong> 
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
      <span class="fontmedia"><STRONG><%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></span></td>
    <td width="17%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr>   <td height="20" align="right" valign="top"></td></tr>
        <tr><td height="40" align="right" valign="top"><img src="images/im_logosmit.gif" width="98" height="30"></td>
        </tr>
      </table>		  
    </td>
 </tr>        
</table>
<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
</table>
<!--FIM Linha-->

<!--FIM CABE�ALHO IMPRESS�O-->
</div>
<div id="tabelastopo" style="position: relative; z-index:100;">
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	<tr>
		<td width="29%" align="left" >N&ordm; Auto :&nbsp;&nbsp;<strong class="fontmaior"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong></td>
		<td width="30%" align="center">Placa :&nbsp;&nbsp;<strong  class="fontmaior"><%=AutoInfracaoBeanId.getNumPlaca() %></strong></td>
		<td width="41%" align="right" nowrap>Processo :&nbsp;&nbsp;<strong class="fontmaior"><%= AutoInfracaoBeanId.getNumProcesso() %></strong></td>	
	</tr>	 
</table>
<div align="center">
	<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#000000"></td></tr>
	</table>
</div>     
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr> 
    <td width="25%" height="20" bgcolor="#b6b6b6" > 
      <strong>1 &#8226; DADOS DO V&Eacute;ICULO</strong>
    <td width="75%" bgcolor="#b6b6b6" align="right" ><strong class="fontmenor">Propriet&aacute;rio :&nbsp;
      	<span class="fontmedia">
      		<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
      		<%= "2".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()) ? " (Jur.)" : "" %>
      	</span></strong>
    </td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr> 
    <td width="29%" valign="bottom"> 
      <div align="left" class="fontmenor"><strong>MARCA/MODELO</strong></div></td>
      	<td width="13%" valign="bottom"> 
      <div align="center" class="fontmenor"><strong>ESP�CIE</strong></div></td>
    	<td width="17%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>CATEGORIA</strong></div></td>
    	<td width="15%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>TIPO</strong></div></td>
    	<td width="25%" valign="bottom">
	  <div align="center" class="fontmenor"><strong>COR PREDOMINANTE</strong></div></td>
  </tr>
</table>
<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1" bgcolor="#cccccc"></td>	</tr>
  </table>
</div>  
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
    <td width="29%"> 
      <div align="left" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></div></td>
    <td width="13%"> 
      <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %></div></td>
    <td width="17%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscCategoria() %></div></td>
    <td width="15%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscTipo() %></div></td>
    <td width="25%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getVeiculo().getDscCor() %></div></td>
  </tr>
</table>  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>	  
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr> 
    <td width="100%"><strong class="fontmenor">ENDERE&Ccedil;O :</strong>&nbsp;&nbsp;
    	<span class="fontmedia">
			<% if ("1".equals(UsuarioFuncBeanId.getIndVisEndereco())) { %>			
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %>&nbsp;
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %>&nbsp;		  
			  <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %>&nbsp;		  
			<% } %>
    	</span>
    </td>
  </tr>
</table>	
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr>          
    <td width="37%" height="20" bgcolor="#B6B6B6"> 
      <strong>2 &#8226; DADOS DA INFRA&Ccedil;&Atilde;O</strong></td>
    <td width="20%" bgcolor="#B6B6B6"> 
      <%=AutoInfracaoBeanId.getSigOrgao() %>&nbsp;&nbsp;</td>
    <td width="43%" bgcolor="#B6B6B6">
	    <b>Notificado em: <%=AutoInfracaoBeanId.getDatNotificacao()%>
    	   &nbsp;&nbsp;
	       <% if (AutoInfracaoBeanId.getNomMotivoNotificacao().length()>0) { %>
    	   (<%=AutoInfracaoBeanId.getNomMotivoNotificacao() %>)
	       <% } %>
    	</b>&nbsp;&nbsp;&nbsp;Vencto:&nbsp;<%=AutoInfracaoBeanId.getDatVencimento() %>   
	</td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr><td width="20%" valign="bottom"> 
    		<div align="left"  class="fontmenor"><strong>C�DIGO </strong></div></td>
	    <td width="14%" valign="bottom">
			<div align="center" class="fontmenor"><strong>GRAVIDADE</strong></div></td>      
	    <td width="17%" valign="bottom"> 
    	  	<div align="center" class="fontmenor"><strong>PONTO</strong></div></td>
	    <td width="22%" valign="bottom"> 
    	 	<div align="center" class="fontmenor"><strong>ENQUADRAMENTO</strong></div></td>  
	    <td width="27%" valign="bottom"> 
    	    <div align="center" class="fontmenor"><strong>DATA/HORA</strong></div></td>
  </tr>
</table>  	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
  	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>    
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
      <td width="20%"> <div align="left" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %>
        &nbsp;
	  	<%  if (AutoInfracaoBeanId.getInfracao().getCodInfracao().equals("")==false) { %>
		<%=	(AutoInfracaoBeanId.getInfracao().getIndTipo().equals("C") ? "Condutor" : "Propriet�rio")  %>
		<%  } %></div></td>
      <td width="14%"> <div align="center" class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscGravidade() %></div></td>
      <td width="17%"> 
      	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getInfracao().getNumPonto() %></div></td>
        	<td width="22%"> 
    	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getInfracao().getDscEnquadramento() %></div></td>
	       	<td width="27%"> 
    	<div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getDatInfracao() %>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></div></td>
</tr>
</table>	   
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>  
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
  	<td width=57%><strong  class="fontmenor">DESCRI��O :</strong>&nbsp;&nbsp;<span class="fontmedia">
   	  <%if (AutoInfracaoBeanId.getDscResumoInfracao().equals("")){ %>
  	      <%=AutoInfracaoBeanId.getInfracao().getDscInfracao() %>
  	  <%}else{ %>
  	      <%=AutoInfracaoBeanId.getDscResumoInfracao() %>
	  <%}%>
	  </span></td>
    <td align=right width=43%><%=AutoInfracaoBeanId.getSituacao() %>&nbsp;</td>
  </tr>
</table>	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
	  <td><strong class="fontmenor">LOCAL :</strong>&nbsp;&nbsp;<span class="fontmedia">
	      <% if (AutoInfracaoBeanId.getDscLocalInfracao().length()+AutoInfracaoBeanId.getNomMunicipio().length()>60) { %>	  
		      <%= (AutoInfracaoBeanId.getDscLocalInfracao()+" "+AutoInfracaoBeanId.getNomMunicipio()).substring(0,61) %>
	      <% } else { %>
    	  	  <%=AutoInfracaoBeanId.getDscLocalInfracao() %>&nbsp;&nbsp;&nbsp;<%=AutoInfracaoBeanId.getNomMunicipio() %>
		  <% } %>
	  	</span>
	  </td>
  </tr>   
</table> 	  
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>        
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
  <tr>          
    <td ><strong  class="fontmenor">RESP. PONTOS  :</strong>&nbsp;&nbsp;<span class="fontmedia">
    	<%=AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>
    	<%= "1".equals(AutoInfracaoBeanId.getIndCondutorIdentificado()) ? " (Ident)" : "" %></span>
    </td>
  </tr>
</table> 
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>        
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
 <tr>
                <td width="32%" valign="top" ><span class="fontmenor"><strong>&nbsp;VELOCIDADE CONSIDERADA: </strong></span>
&nbsp;<%=AutoInfracaoBeanId.getValVelocConsiderada() %> </td>
                <td width="1%" valign="top" class="semborda"></td>
				
                <td width="34%" valign="top"><span class="fontmenor">&nbsp;<strong>VELOCIDADE REGULAMENTADA: </strong></span>
&nbsp;<%=AutoInfracaoBeanId.getValVelocPermitida() %></td>
                <td width="1%" valign="top" class="semborda"></td>
                <td width="32%" height="32" valign="top">&nbsp;<strong class="fontmenor">VELOCIDADE MEDIDA: </strong>
&nbsp;<%=AutoInfracaoBeanId.getValVelocAferida() %></td>
    </tr>

</table>     
<!--Linha-->

<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="10" ></td>
	</tr>
</table>



<!-- OOO -->
<div align="center">
	<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#000000"></td></tr>
	</table>
</div>     
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr> 
    <td width="25%" height="20" bgcolor="#b6b6b6" > 
      <strong>1 &#8226; DADOS DO REQUERIMENTO</strong>
  </tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="3" class="semborda">
 <tr> 
    <td width="29%" valign="bottom"> 
      <div align="left" class="fontmenor"><strong>REQUERIMENTO</strong></div></td>
      	<td width="13%" valign="bottom"> 
      <div align="center" class="fontmenor"><strong>ASSUNTO</strong></div></td>
  </tr>
</table>  
<div align="center">
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> <td height="1" bgcolor="#cccccc"></td>	</tr>
  </table>
</div>  
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr> 
    <td width="29%"> 
      <div align="center" class="fontmedia"><%=RequerimentoBean.getNumRequerimento()%></div></td>
    <td width="13%"> <div align="center" class="fontmedia"><%=AutoInfracaoBeanId.getDscResumoInfracao()%></div></td>
  </tr>
</table> 
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr><td height="1" bgcolor="#cccccc"></td></tr>
</table>
<!-- OOO -->


<!--FIM Linha-->
</div>
<!--Rodap�-->
<div id="rodape" style="position:relative; left:0px; width:100%; height:6px; z-index:100; visibility: visible;"> 
<hr noshade color="#000000">
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr><td width="31%"  height="20" ><strong>USU&Aacute;RIO :</strong>&nbsp;<span class="fontmedia"><%=UsuarioBeanId.getNomUserName()%></span></td>
      <td width="69%" align="right" > <strong>ORG&Atilde;O 
      LOTA&Ccedil;&Atilde;O :</strong>&nbsp;<%=UsuarioBeanId.getSigOrgaoAtuacao()%><span class="fontmedia"></span> 
    </td>
  </tr> 
</table>
</div>
<!--FIM_BLOCO B-->
</body>