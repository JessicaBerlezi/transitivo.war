<%@page import="javax.mail.Session"%>
<%@page import="UTIL.TipoRecGeneric"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />

<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Consultar Requerimentos por Auto</title>
<link rel="stylesheet" href="css/transitivo.css" />

<style type="text/css">
	.resultContainer {
		margin-top: 10px;
		margin-left: 50px;
		font-family: verdana;
		font-size: 11px;
	}
	
	.row-auto {
		border-top: solid 1px #862222;
		padding-top: 5px;
	}
	
	.row-item {
		background-color: #FAEAE5;
		padding: 2px;
		margin-top: 2px;
	}
	
	.item-display {
		display: inline-block;
		font-weight: bold;
		color: #862222;
		margin-right: 30px;
	}
	
	.item-display > span.value {
		font-weight: normal;
		color: black;
	}
	
	.row-requerimento {
		border-top: solid 1px #862222;
		margin-top: 5px;
		padding-top: 5px;
	}
	
	.row-requerimento .row-head {
		background-color: #862222;
		padding: 2px;
		color: #fff;
		font-weight: bold;
	}
	
	.row-requerimento .row-head .value {
		font-weight: normal;
	}
	
	.span-btn {
		cursor: pointer;
		text-decoration: underline;
	}
</style>
</head>

<body>
	<div id="head">
		<div class="img-head2">
			<div class="img-head1">
				<div style="position: absolute; right: 6px;">
					<div class="img-head-btn" style="background-image: url('/Transitivo/images/bot_info_ico.png');"></div>
					<div class="img-head-btn" style="background-image: url('/Transitivo/images/ico_ciclo.png');"></div>
					<div class="img-head-btn" style="background-image: url('/Transitivo/images/detran_help.png');"></div>
				</div>
			</div>
		</div>
		<div id="bread" class="bread">
			<strong class="text">PROCESSOS &gt; POR AUTO</strong>
		</div>
	</div>

	<div id="body" class="body" style="height: 573px; padding-right: 15px;">
		<div class="divForm">
			<table id="tableForm">
				<tbody>
					<tr>
						<td>N�mero do Auto:</td>
						<td>
							<input type="text" id="numAuto" style="width: 100px;" />
						</td>
					<tr>
					<tr>
						<td>N�mero do Processo:</td>
						<td>
							<input type="text" id="numProcesso" style="width: 100px;" />
						</td>
					<tr>
					<tr>
						<td></td>
						<td>
							<input type="submit" id="btnConsultar" value="Consultar" style="width: 100px;" onclick="consultar();" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="resultContainer">
			<div class="row-auto" id="auto">
				<div class="row-item">
					<div class="item-display">
						N�mero do Auto: <span id="resultNumAuto" class="value"></span>
					</div>
					<div class="item-display">
						N�mero do Processo: <span id="resultNumProcesso" class="value"></span>
					</div>
					<div class="item-display">
						Data de Infra��o: <span id="resultDataInfracao" class="value"></span>
					</div>
					<div class="item-display">
						Propriet�rio: <span id="resultProprietario" class="value"></span>
					</div>
				</div>
				<div class="row-item">
					<div class="item-display">
						C�digo de Infra��o: <span id="resultCodInfracao" class="value"></span>
					</div>
					<div class="item-display">
						Placa: <span id="resultPlaca" class="value"></span>
					</div>
					<div class="item-display">
						Local: <span id="resultLocal" class="value"></span>
					</div>
					<div class="item-display">
						Valor: <span id="resultValor" class="value"></span>
					</div>
					<div class="item-display">
						Enquadramento: <span id="resultEnquadramento" class="value"></span>
					</div>
				</div>
			</div>
			
			<div id="divRequerimentos">			
			</div>
		</div>
	</div>

	<div id="footer">
		<div class="img-footer2">
			<div class="img-footer"></div>
			<div class="img-footer-btn"></div>
		</div>
	</div>
</body>
</html>

<script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>

<script type="text/javascript">
	function consultar(){
		var numAuto = document.getElementById("numAuto").value;
		var numProcesso = document.getElementById("numProcesso").value;
		var tipoRec = <%= TipoRecGeneric.REQUERIMENTO_POR_AUTO %>;
		var cdNatureza = <%= session.getAttribute("codNatureza") %>;
		
		if(!validarConsulta())
			return;
		
		$.ajax({
			method: "post",
			url: "/Transitivo/ControladorRecGeneric",
			data: {
				tipoRec: tipoRec,
				numAuto: numAuto,
				numProcesso: numProcesso,
				cdNatureza: cdNatureza
			},
			success: function(data){
				manipularResultado(data);
			},
			error: function(data){
				alert("ERRO" + data);
			}
		});
	}
	
	function manipularResultado(data) {
		var auto = Auto(data[0]);
		
		montarRowAuto(auto);
		
		var divRequerimentos = document.getElementById("divRequerimentos");
		divRequerimentos.innerHTML = "";
		
		var itemDP = "";
		var item1P = "";
		var item2P = "";
		var itemPAE = "";
		
		for (var i = 0; i < data.length; i++) {
			var requerimento = Requerimento(data[i]);
			var itemAtual = "";
			var tipoRequerimento = descricaoTipoRequerimento(requerimento.resultTipoRequerimento);
			var resultado = "";
			
			if(requerimento.resultResultado == "I")
				resultado = "INDEFERIDO"
			else if(requerimento.resultResultado == "D")
				resultado = "DEFERIDO"
			else if(requerimento.resultResultado == "A")
				resultado = "ARQUIVADO"
			else if(requerimento.resultResultado == "S")
				resultado = "SUSPENSO"
			else
				resultado = "S/R"
			
			itemAtual = "<div class='row-requerimento' id='divRequerimento" + requerimento.resultCodRequerimento + "'>";
			itemAtual += "<div class='row-head'>";
			itemAtual += "Tipo de Requerimento: <span class='value' id='resultTipoRequerimento'>" + tipoRequerimento + "</span>";
			itemAtual += "<input type='button' value='Excluir Processo' style='margin-left: 10px;' onclick='deletarProcesso(" + JSON.stringify(requerimento) + ")' />";
			itemAtual += "</div>";
			itemAtual += "<div class='row-item'>";
			itemAtual += "<div class='item-display'>";
			itemAtual += "N�mero do Requerimento: <span id='resultAta' class='value' style='margin-right: 20px;'>" + requerimento.resultNumRequerimento + "</span>";
			itemAtual += "Data de Entrada: <span id='" + requerimento.resultCodRequerimento + "' class='value span-btn' title='Alterar Data' onclick='popupAlterarDataRequerimento("
				+ JSON.stringify(requerimento) + ");'>" + requerimento.resultDataRequerimento + "</span>";
			itemAtual += "</div>";
			itemAtual += "</div>";
			itemAtual += "<div class='row-item'>";
			itemAtual += "<div class='item-display'>";
			
			if(requerimento.resultAta)
				itemAtual += "Ata: <span id='resultAta' class='value'>" + requerimento.resultAta + "</span>";
			else
				itemAtual += "Ata: <span id='resultAta' class='value'>N/A</span>";
				
			itemAtual += "</div>";
			itemAtual += "</div>";
			itemAtual += "<div class='row-item'>";
			itemAtual += "<div class='item-display'>";
			
			if(requerimento.resultRelator)
				itemAtual += "Relator: <span id='resultRelator' class='value'>" + requerimento.resultRelator + "</span>";
			else
				itemAtual += "Relator: <span id='resultRelator' class='value'>N/A</span>";
				
			itemAtual += "</div>";
			itemAtual += "</div>";
			itemAtual += "<div class='row-item'>";
			itemAtual += "<div class='item-display'>";
			itemAtual += "Resultado: <span id='resultRelator' class='value'>" + resultado + "</span>";
				
			itemAtual += "</div>";
			itemAtual += "</div>";
			itemAtual += "</div>";

			if (requerimento.resultTipoRequerimento == "DP") {
				itemDP = itemAtual;
			} else if (requerimento.resultTipoRequerimento == "1P") {
				item1P = itemAtual;
			} else if (requerimento.resultTipoRequerimento == "2P") {
				item2P = itemAtual;
			} else if (requerimento.resultTipoRequerimento == "PAE") {
				itemPAE = itemAtual;
			}
		}
		
		if(itemDP)
			divRequerimentos.innerHTML += itemDP;

		if(item1P)
			divRequerimentos.innerHTML += item1P;

		if(item2P)
			divRequerimentos.innerHTML += item2P;

		if(itemPAE)
			divRequerimentos.innerHTML += itemPAE;
	}

	function montarRowAuto(auto) {
		document.getElementById("resultNumAuto").innerHTML = auto.resultNumAuto;
		document.getElementById("resultNumProcesso").innerHTML = auto.resultNumProcesso;
		document.getElementById("resultDataInfracao").innerHTML = auto.resultDataInfracao;
		document.getElementById("resultProprietario").innerHTML = auto.resultProprietario;
		document.getElementById("resultCodInfracao").innerHTML = auto.resultCodInfracao;
		document.getElementById("resultPlaca").innerHTML = auto.resultPlaca;
		document.getElementById("resultLocal").innerHTML = auto.resultLocal;
		document.getElementById("resultValor").innerHTML = "R$ " + auto.resultValor;
		document.getElementById("resultEnquadramento").innerHTML = auto.resultEnquadramento;
	}

	function validarConsulta() {
		var numAuto = document.getElementById("numAuto").value;
		var numProcesso = document.getElementById("numProcesso").value;

		if (!numAuto && !numProcesso) {
			alert("Preencha o n�mero do Auto ou do Processo.");

			return false;
		} else
			return true;
	}

	function Auto(data) {
		return {
			resultNumAuto : data.numAuto,
			resultNumProcesso : data.numProcesso,
			resultDataInfracao : data.dataInfracao,
			resultProprietario : data.nomProprietario,
			resultCodInfracao : data.codInfracao,
			resultPlaca : data.placa,
			resultLocal : data.localInfracao,
			resultValor : data.valor,
			resultEnquadramento : data.enquadramento
		};
	}

	function Requerimento(data) {
		return {
			resultCodRequerimento : data.codRequerimento,
			resultTipoRequerimento : data.tipoRequerimento,
			resultAta : data.ata,
			resultResultado : data.resultado,
			resultRelator : data.relator,
			resultNumRequerimento : data.numRequerimento,
			resultDataRequerimento: data.dataRequerimento
		};
	}

	function descricaoTipoRequerimento(tipoRequerimento) {
		if(tipoRequerimento == "DP"){
			return "Defesa Pr�via"
		} else if(tipoRequerimento == "1P"){
			return "1� Inst�ncia"
		} else if(tipoRequerimento == "2P"){
			return "2� Inst�ncia"
		} else if(tipoRequerimento == "PAE"){
			return "P.A.E"
		} else{
			return "ERROR"
		}
	}

	function popupAlterarDataRequerimento(requerimento) {
		var novaDataRequerimento = prompt("Digite uma nova data de entrada para "
				+ descricaoTipoRequerimento(requerimento.resultTipoRequerimento),
				requerimento.resultDataRequerimento);

		if(novaDataRequerimento && requerimento.resultDataRequerimento != novaDataRequerimento)
			alterarDataRequerimento(requerimento.resultCodRequerimento, novaDataRequerimento);
	}

	function alterarDataRequerimento(codRequerimento, novaData) {
		var tipoRequerimento = <%= TipoRecGeneric.ALTERAR_DATA_REQUERIMENTO %>;
		
		$.ajax({
			method: "post",
			url: "/Transitivo/ControladorRecGeneric",
			data: {
				tipoRec: tipoRequerimento,
				codRequerimento: codRequerimento,
				novaData: novaData
			},
			success: function() {
				document.getElementById(codRequerimento).innerHTML = novaData;
			},
			error: function(data) {
				if(data.status == 1)
					alert("ERRO: data de entrada inv�lida.");
				else if(data.status == 2)
						alert("ERRO: requerimento inv�lido.");
			}
		});
	}

	function deletarProcesso(requerimento) {
		if(confirm("Tem certeza que deseja deletar o requerimento de " + 
				descricaoTipoRequerimento(requerimento.resultTipoRequerimento) + "?") == false)
			return;
		
		var tipoRequerimento = <%= TipoRecGeneric.DELETAR_REQUERIMENTO %>;
		var codNatureza = <%= session.getAttribute("codNatureza") %>;

		$.ajax({
			url: "/Transitivo/ControladorRecGeneric",
			method: "post",
			data: {
				tipoRec: tipoRequerimento,
				codRequerimento: requerimento.resultCodRequerimento,
				codNatureza: codNatureza
			},
			success: function() {
				document.getElementById("divRequerimento" + requerimento.resultCodRequerimento).innerHTML = null;
			},
			error: function(data) {
				if(data.status == 1)
					alert("ERRO: requerimento inv�lida.");
				else if(data.status == 2)
						alert("ERRO: requerimentos com resultado n�o podem ser deletados.");
			}
		});
	}
</script>