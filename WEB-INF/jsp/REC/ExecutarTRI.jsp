<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@page import="REC.ExecutarTRIBean"%>
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<!-- Chama o Objetos -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="RequerimentoId"     scope="request" class="REC.RequerimentoBean" /> 
<jsp:useBean id="ExecutarTRIId"      scope="request" class="REC.ExecutarTRIBean" /> 
<%
	String proxAcao = (String)request.getAttribute("proxAcao") ;
	if (proxAcao==null) proxAcao="InformaTRI" ;
%>   

<%String cpf = RequerimentoId.getNumCPFTR(),c_cpf="";    					
  c_cpf=cpf;
  if (cpf.length()>0)
  {
    cpf = cpf.substring(0,3)+"."+cpf.substring(3,6)+"."+cpf.substring(6,9)+"-"+cpf.substring(9,11);
  }%>
               	 
<jsp:useBean id="UFCNHId" scope="request" class="sys.UFBean" />
<%
   UFCNHId.setChecked(RequerimentoId.getCodUfCNHTR());
%>   
<jsp:setProperty name="UFCNHId" property="j_abrevSist" value="REC" />  	 	     	 	  
<jsp:setProperty name="UFCNHId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFCNHId" property="popupNome"   value="codUfCNHTR"  />  
<jsp:setProperty name="UFCNHId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	 switch (opcao) 
	 {    
	   case 'R':
	    	close() ;
			break;
	   case 'InformaTRI':
	   		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
	   
	     		if (veCampos(fForm)==true) 
	     		{		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
			  } 
		   }
		   break;
	   case 'ExecutaTRI':
	   	  if (veCamposTRI(fForm)==true) {	
			fForm.acao.value=opcao;  			
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();
		  } 
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datEntrada,1) && valid
	verString(fForm.numCNHTR,"N� CNH",0);
	verString(fForm.codUfCNHTR,"UF CNH",0);
	if (valid==false) alert(sErro) 
	return valid ;
}
function veCamposTRI(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.numCPFTR,"CPF",0);
	if (valid==false) alert(sErro) 
	return valid ;
}
function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if ( (valor.length != 0)&&(valor.length > 11) ) {     
    if (valor.length > 11) { 
     	valor = Tiraedt(valor,11);
    } 
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 document.ExecutarTRIForm.cpfpnthidden.value=valor;
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}

</script>

</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ExecutarTRIForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>
<input name="cpfpnthidden" type="hidden" value='<%=c_cpf%>'>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:50px; right: 15px; top:274px; height:5px; z-index:21; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:275px; height:45px; z-index:20; overflow: visible; visibility: visible;"> 
<% if ("ExecutaTRI".equals(proxAcao)==false) { %>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#faeae5">
    	    <td align="left" valign="middle"><strong> 
				&nbsp;<jsp:getProperty name="ExecutarTRIId" property="msg" /></strong>
	        </td>
	  </tr>
	</table>			
<% } %>
<% if ("S".equals(ExecutarTRIId.getEventoOK())) { %>			 	 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
	  <tr bgcolor="#faeae5"> 
        <td colspan=3>&nbsp;Data de Entrada:&nbsp; 
          <input <%= ("ExecutaTRI".equals(proxAcao)) ? "readonly" : "" %> type="text" name="datEntrada" size="12" maxlength="10" value='<%=RequerimentoId.getDatAtuTR() %>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="indTipoCNHTR" type="checkbox" style=" heigth:13;" value='1'  <%= sys.Util.isChecked(RequerimentoId.getIndTipoCNHTR(),"1")%> class="input">PGU
		</td>            		
        <td colspan=2>&nbsp;N� CNH/PGU:&nbsp;    
	        <input <%= ("ExecutaTRI".equals(proxAcao)) ? "readonly" : "" %> name="numCNHTR" type="text" size="19" maxlength="11" value='<%=RequerimentoId.getNumCNHTR().trim() %>'></td>
	    <td colspan=2 >&nbsp;UF CNH:&nbsp;
			<% if ("ExecutaTRI".equals(proxAcao)) { %>
				<input readonly name="codUfCNHTR" type="text" size="4" maxlength="2" value='<%=RequerimentoId.getCodUfCNHTR() %>'></td>			
			<% } else { %>
			    <jsp:getProperty name="UFCNHId" property="popupString" />
			<% } %>
		</td> 
        <td  align="right"> 
		  	<% if ( ("S".equals(ExecutarTRIId.getEventoOK())) && ("ExecutaTRI".equals(proxAcao)==false) ){ %>		
          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  title="Confirmar" onClick="javascript: valida('ExecutaTRI',this.form);">	
          	<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
       	  </button>
			<% } %>
		</td>
      </tr>
      <tr><td colspan=7 height=2></td></tr>
<%-- <% if ("ExecutaTRI".equals(proxAcao)) { %> --%>
<% if (!"ExecutaTRI".equals(proxAcao)) { %>
      <tr bgcolor="#faeae5">
        <td>&nbsp;Nome:</td> 
        <td><input name="nomCondutorTR" type="text" value='<%=RequerimentoId.getNomCondutorTR() %>' size="42" maxlength="45">
        <input readonly name="codResponsavelDP" type="hidden" value='<%=RequerimentoId.getCodResponsavelDP() %>' size="10" maxlength="10">	
<%-- 			<input readonly name="codResponsavelDP" type="hidden" value='<%=RequerimentoId.getCodResponsavelDP() %>' size="10" maxlength="10">		 --%>
		</td>
		<td  colspan=2>&nbsp;Categoria:
			<input  name="txtCategoriaTR" type="text" value='<%=RequerimentoId.getTxtCategoriaTR() %>' size="8" maxlength="8">	
			&nbsp;&nbsp;Valid.:&nbsp;
			<input name="datValidTR" type="text" value='<%=RequerimentoId.getDatValidTR() %>' size="12" maxlength="10">
		</td>					
		<td>&nbsp;CPF:&nbsp;</td>		
		<td colspan=2><input name="numCPFTR" type="text" size="16" maxlength="14" value='<%=cpf%>' onKeyPress="javascript:Mascaras(this,'999.999.999-99');" onFocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);"></td>			
      </tr>
	  <tr><td colspan=8 height=2></td></tr>
      <tr bgcolor="#faeae5"> 
	  	<td width="8%">&nbsp;End.:</td>
<% if (RequerimentoId.getTxtEnderecoTR().trim().length()==0) {  %>	  	
        <td width="27%"><input name="txtEnderecoTR" type="text" value='<%=RequerimentoId.getTxtEnderecoTR() %>' size="42" maxlength="45"  onkeypress="javascript:f_end();"></td>
		<td width="7%">&nbsp;Munic:</td>
        <td width="37%"><input name="nomCidadeTR"   type="text"   size="32" maxlength="30" value='<%=RequerimentoId.getNomCidadeTR() %>' onKeyPress="javascript:f_end();">&nbsp;&nbsp;
						&nbsp;<input name="codUFTR" type="text"   size="4"  maxlength="2"  value='<%= RequerimentoId.getCodUfTR() %>'    onkeypress="javascript:f_let();">
						<input name="nomBairroTR"   type="hidden" size="20" maxlength="30" value='<%=RequerimentoId.getNomBairroTR() %>'>
						<input name="codCidadeTR"   type="hidden" size="32" maxlength="30" value='<%=RequerimentoId.getCodCidadeTR() %>'>
		</td>
		<td width="5%">&nbsp;CEP:&nbsp;</td>
        <td width="9%" class="tdborda"><input name="numCEPTR" type="text" size="11" maxlength="9" value='<%=RequerimentoId.getNumCEPTREdt() %>' onKeyPress="javascript:f_num();"></td>
<% } else { %>
        <td width="27%"><input readonly name="txtEnderecoTR" type="text" value='<%=RequerimentoId.getTxtEnderecoTR() %>' size="42" maxlength="45"></td>
		<td width="7%">&nbsp;Munic:</td>
        <td width="37%"><input readonly name="nomCidadeTR" type="text" size="32" maxlength="30" value='<%=RequerimentoId.getNomCidadeTR() %>'>&nbsp;&nbsp;
						&nbsp;<input readonly name="codUFTR" type="text" size="4" maxlength="2" value='<%= RequerimentoId.getCodUfTR() %>'>
						<input name="nomBairroTR" type="hidden" size="20" maxlength="30" value='<%=RequerimentoId.getNomBairroTR() %>'>
						<input name="codCidadeTR" type="hidden" size="32" maxlength="30" value='<%=RequerimentoId.getCodCidadeTR() %>'>
		</td>
		<td width="5%">&nbsp;CEP:&nbsp;</td>
        <td width="9%" class="tdborda"><input readonly name="numCEPTR" type="text" size="11" maxlength="9" value='<%=RequerimentoId.getNumCEPTREdt() %>'></td>
<% }  %>
<!-- 		<td width="7%"> -->
<%--           <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('<%= proxAcao %>',this.form);">	 --%>
<%--           	<img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19">  --%>
<!--        	  </button>		 -->
<!-- 		</td> -->
      </tr>
      
      <tr><td height="2" colspan="7"></td></tr>
   	 <tr>
	  <td height="16" colspan="8" bgcolor="#faeae5">&nbsp;Documento Gerador:&nbsp; 
	  <input type="text" name="txtMotivo" size="90" maxlength="50" value='<%=ExecutarTRIId.getTxtMotivo() %>'onfocus="javascript:this.select();"> </td>
	  </tr>
	  <tr>
	 		<td height="5" bgcolor="#faeae5">&nbsp;PROCESSO:&nbsp;</td>
            <td colspan="3" bgcolor="#faeae5">
            	<!-- Verifica se j� existe algum processo(DP, 1P OU 2P) para o referido auto e caso exista informa o mesmo senao o usuario informa na hora -->
            	<%
            	 ExecutarTRIBean execTRI =  new ExecutarTRIBean();
            		String numProcesso = execTRI.getNumProcesso(AutoInfracaoBeanId.getNumAutoInfracao());
            		if(numProcesso.length() > 0){ %>
            			   <input name="numProcesso" type="text" value= <%=numProcesso %> size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
            		<%}else{
            	%>
            			 	<input name="numProcesso" type="text" value= "" size="25"  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
    	        		<%}
	            	%>
            </td>
	  </tr>
	  
	   
<% } %>
		
    
	  
  </table>
<% } %>			
</div>
<!--FIM_CORPO_sistema--> 
<% if ("S".equals(ExecutarTRIId.getEventoOK())) { %>			
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<%}%>
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>