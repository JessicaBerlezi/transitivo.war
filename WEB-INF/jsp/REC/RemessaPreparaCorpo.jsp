<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<jsp:useBean id="RemessaId"   scope="session" class="REC.RemessaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<%
String posTop = request.getParameter("posTop"); if(posTop==null) posTop ="170px";   
String posHei = request.getParameter("posHei"); if(posHei==null) posHei ="183px";    
int seq = 0;
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" >
	  <%
	   int max = 0;
       int contaReq=0;
	   String dtReq="";
	   String tipoSolic = "";
	   if ((RemessaId.getAutos().size()>0) ) {
		String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
		Iterator it = RemessaId.getAutos().iterator() ;
		REC.AutoInfracaoBean myAuto  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {

			myAuto = (REC.AutoInfracaoBean)it.next() ;	
			Iterator ot = myAuto.getRequerimentos().iterator() ;
			REC.RequerimentoBean myReq  = new REC.RequerimentoBean();
			while (ot.hasNext()) {
				myReq = (REC.RequerimentoBean)ot.next() ;
				
				if(myAuto.getNumRequerimento().equals(myReq.getNumRequerimento())){									
					dtReq=myReq.getDatProcDP();	
					dtReq=myReq.getDatRequerimento();
					tipoSolic = myReq.getCodTipoSolic();
					
					break;
				}	
				contaReq++;
			}
			
			cor    = (seq%2==0) ? "#faeae5" : "#ffffff";
			if (myAuto.getMsgErro().length()>0)  cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'> 
	        <td width=50 height="14" align=center rowspan=2>&nbsp;<%= (seq+1) %>&nbsp;
	           <% if (RemessaId.getCodRemessa().length()==0 || "REC0127".equals(UsuarioFuncBeanId.getJ_sigFuncao()) || "REC0128".equals(UsuarioFuncBeanId.getJ_sigFuncao()) || "REC0129".equals(UsuarioFuncBeanId.getJ_sigFuncao()) ) { %>   
			        <input type="checkbox" name="Selecionado" value="<%= seq %>" class="input" <%= sys.Util.isChecked(myAuto.getEventoOK(),"1")%> 
			        	onclick="javascript:SomaSelec();">
		         <% } else {%>        
		         <input type="hidden" name="Selecionado" value="1">
		         <%}%>
	        </td>
	        <td width=170 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumProcesso() %>
	        <td width=70 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getDatProcesso() %></td>
			<td width=220 >&nbsp;<%=myAuto.getNumRequerimento() %>&nbsp;-&nbsp;<%=dtReq %></td>
        	<td width=75 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
        		&nbsp;<%=myAuto.getNumAutoInfracao() %>       </td>
			<td width=55 onclick="javascript:Mostra('MostraAuto',this.form,'<%=myAuto.getNumPlaca() %>','<%=myAuto.getNumAutoInfracao() %>');">
				&nbsp;<%=myAuto.getNumPlaca() %>   </td>
			<td>  
				&nbsp;<%=myAuto.getDatInfracao() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>'> 
	        <td colspan=6>&nbsp;<%if ("DP,1P,2P".indexOf(tipoSolic) >=0) {%>
                  <%=myAuto.getProprietario().getNomResponsavel() %> 
                  <%} else {%>
                      <%=myAuto.getCondutor().getNomResponsavel() %>
                  <%}%> 
             </td>
		</tr>	
		<% if (myAuto.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=7>&nbsp;<%=myAuto.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>		
		<% seq++; 
		   max++;
		}
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>  

<% if (seq>0) {%>
<input type="hidden" name="Selecionado" value="<%=seq%>"
<%}%>