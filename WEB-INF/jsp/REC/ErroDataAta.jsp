<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@page import="ACSS.Ata"%>
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="NoticiaId" scope="session" class="ACSS.NoticiaBean" /> 
<jsp:useBean id="AvisoId"   scope="session" class="ACSS.AvisoBean" /> 
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId"     scope="session" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto dos Parametros do Orgao de atuacao -->
<jsp:useBean id="ParamOrgBeanId"    scope="session" class="REC.ParamOrgBean" /> 
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = "Alterar Senha" ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String cor = "#DFEEF2" ;
    if ("REC".equals(mySistema)) {
		 cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;		 		 
	}	
    if ("PNT".equals(mySistema)) {
		 cor = "#d9f4ef" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;		 		 
	}	
%>

<!-- Chama o Objeto da Tabela de Usuario -->
<jsp:useBean id="UsrBeanId" scope="request" class="ACSS.UsuarioBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: Troca Senha :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'alterar':
// 	    if (veCampos(opcao,fForm)==false) return ;
// 		fForm.j_cmdFuncao.value="trocaSenhaCommand" ;
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  		
		break;	 
	   case 'R':
	    close() ;
		break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;	 
  }
}
function veCampos(opcao,fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.codSenha,"Senha",4);
	verString(fForm.novaSenha,"Nova Senha",4);	
	verString(fForm.digSenha,"Redigite a Senha",4);		
	if (fForm.digSenha.value != fForm.novaSenha.value) {
		sErro += "Nova Senha n�o confere com a Senha redigitada. \n"
		valid = false
	}
	if (valid==false) alert(sErro) 
	return valid ;
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" OnLoad="document.UsrForm.codSenha.focus();" style="overflow: hidden;">
<form name="UsrForm" method="post" action="">
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<!--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o 1.0&nbsp;</font>  -->
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>
  
<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:468px; height:23px; z-index:14; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr><td><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">
		<% } %>
		&nbsp;<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
  </table>
</div>
<!--FIM T�TULO DO M�DULO-->
		<!--IN�CIO_CORPO_sistema--> 
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 94px; left: 50px;" > 
	<!--Botoes padr�o-->
	<table width="100%" border='0' cellpadding="0" cellspacing="0">
    	 <tr>
    	 <td style="font-style: normal;color: red; size: 20dp" >
    	 <b>	O Dia informado n�o � um dia util! </b>
			 </td> 
	     </tr>
	</table>			  
	<!--FIM_Botoes padr�o-->
</div>	

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= UsrBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "130 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<!-- Rodap�-->
<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:9; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  <img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
  <!--texto 0800-->
<jsp:include page="../sys/telSuporte.jsp" flush="true" /> 
<!--FIM texto 0800-->
</div>
<!-- Fim Rodap� -->
</form>
</body>
</html>

