<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>


<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="CancelamentoId" scope="request" class="REC.CancelamentoBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"> 
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	 switch (opcao) 
	 {    
	   case 'R':
	    	close() ;
			break;
	  case 'CancelaAuto':
	   		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
	      		if (veCampos(fForm)==true) 
	      		{		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
		  		} 
		    }		  
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	
	valid = ValDt(fForm.Data,1) && valid
	
	if (atuRadio(fForm.indOperradio,fForm.IndicOper)) {
	  valid = false
	  sErro = sErro + "Tipo de Invalida��o n�o selecionado. \n"
	}
	
	verString(fForm.codMotivo,"C�digo do Motivo",0);
	verString(fForm.ProcInv,"N�mero do processo",0);
	verString(fForm.Data,"Data",0);
	if (valid==false) alert(sErro) 
	return valid ;
}

function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="CancelamentoForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"         		type="hidden" value=' '>
<input name="IndicOper"type="hidden" value=' '>				
				
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="lerAutoPlaca_Diretiva.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<% if (AutoInfracaoBeanId.getNomStatus().length()>0) {%>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;">
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr bgcolor="#faeae5">
   	    <td colspan="3" height="10" valign="middle"><strong>&nbsp;
			<% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>
			<%=AutoInfracaoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=AutoInfracaoBeanId.getDatStatus()%>)
			<% } %>			
			</strong>        </td>			
     </tr>
     <tr><td colspan="3" height="2"></td></tr>
     <tr bgcolor="#faeae5">
       <td width="41%" valign="middle" align="center" height=17>
            Data da Invalida��o:&nbsp;
        <input type="text" name="Data" size="12" maxlength="10" value='<%=sys.Util.formatedToday().substring(0,10)%>' onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">       </td>
       <td width="49%">&nbsp;Tipo Invalida��o:&nbsp;<input type="radio" name="indOperradio" value="1" class="sem-borda" style="height: 14px; width: 14px;">Cancelamento &nbsp;&nbsp;
          <input type="radio" name="indOperradio" value="2" class="sem-borda" style="height: 14px; width: 14px;">Suspens�o&nbsp;&nbsp;        
       <td width="10%"></td>
     </tr>
     <tr><td colspan=3 height=2></td></tr>     
     <tr bgcolor="#faeae5">
        <td width="29%" valign="middle" align="center" >Cod.Motivo:&nbsp;&nbsp;<input type="text" name="codMotivo" size="3" maxlength="3" value= "" onKeyPress="javascript:f_num();" onFocus="javascript:this.select();"></td>
        <td width="30%" align="left" valign="middle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Num.Processo:&nbsp;&nbsp;<input type="text" name="ProcInv" size="25" maxlength="20" value=""  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase();"></td>  
		<td width="30%" align="center" valign="middle">      
	        <% if (AutoInfracaoBeanId.getNomStatus().length()>0) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('CancelaAuto',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
			<% } %> 
		</td>
      </tr>
      <tr><td colspan="3" height="2"></td></tr>	 
      <tr>
	     <td height="5" colspan="4" bgcolor="#faeae5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Documento Gerador:&nbsp; 
	       <input type="text" name="txtMotivo" size="70" maxlength="50" value='<%=CancelamentoId.getTxtMotivo() %>'  onfocus="javascript:this.select();">
	     </td>
	  </tr>	    
  </table>		
</div>
<% } %>
<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height="2"><td bgcolor="#993300" ></td></tr>
  </table>
</div>

<%if (AutoInfracaoBeanId.getNumAutoInfracao().length()>0){%>
	<script>document.CancelamentoForm.codMotivo.focus();</script>	        
<%}else{%>    			
   	<script>document.CancelamentoForm.numAutoInfracao.focus();</script>	        
<%}%>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>