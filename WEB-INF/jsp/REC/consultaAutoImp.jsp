<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<%@ page session="true" %>
<% String path=request.getContextPath(); %>

<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="consultaAutoId"     scope="session" class="REC.consultaAutoBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaAutoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=99;		
	int npag = 0;
	String nPag="";
	int seq = 0;
	Iterator ot = consultaAutoId.getAutos().iterator() ;
	REC.AutoInfracaoBean autoImp  = new REC.AutoInfracaoBean();		
	if (consultaAutoId.getAutos().size()>0) { 
		while (ot.hasNext()) {
			autoImp   = (REC.AutoInfracaoBean)ot.next() ;
			seq++;
			contLinha++;
			//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
			if (contLinha>9){
			    contLinha=1;
				npag++;	
				nPag=String.valueOf(npag);		
				if (npag!=1){			
		%>
						</table>  
						<%@ include file="rodape_impconsulta_Diretiva.jsp" %>		    					
					<div class="quebrapagina"></div>
				<% } %>	
				<%@ include file="cabecalho_impconsulta_Diretiva.jsp" %>		
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">							
			<% } %>	
							
		<tr> 
	        <td width=60 height="14" rowspan=2 align=center>&nbsp;<%= seq %> </td>
			<td width=80 rowspan=2>&nbsp;<b><%=autoImp.getNumAutoInfracao() %></b> </td>
        	<td width=80 rowspan=2>&nbsp;<b><%=autoImp.getNumPlaca() %></b></td>
	        <td width=360>&nbsp;<%= autoImp.getNumProcesso()%></td>
			<td align=right>&nbsp;
				<%  if (sys.Util.DifereDias("16/07/2004",autoImp.getDatInfracao())>=0) { %>
	    	    	<%= autoImp.getDatNotificacao()%>
				<% } %>	        
        	</td>
		</tr>
		<tr> 
    	    <td colspan=2>&nbsp;Situa��o:&nbsp;<%= autoImp.getSituacao()%>
			<%  if (sys.Util.DifereDias("16/07/2004",autoImp.getDatInfracao())>=0) { %>
	        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= autoImp.getNomStatus()%>
			<% } %>
			<% String desc = autoImp.getDescIdentAutoOrigem();
			if (desc.length()>0) {%>
        	<br>&nbsp;AUTO RENAINF - &nbsp;<%=desc%>&nbsp;	    
        	<%
			}
			%>
	        <% if (autoImp.getNumAutoOrigem().length()>0) { %>	
		          <br>&nbsp;Ft. Gerador:&nbsp;<%=autoImp.getNumAutoOrigem()%>&nbsp;(<%=autoImp.getNumInfracaoOrigem() %>)
			<% } %>	
			</td>			
		</tr>
		<tr> 
	        <td height="14" rowspan=4 align=center>&nbsp;<%=autoImp.getSigOrgao() %> </td>    	    				
        	<td colspan=2>&nbsp;Data:&nbsp;<%= autoImp.getDatInfracao()%>&nbsp;&nbsp;<%=autoImp.getValHorInfracaoEdt() %></td>
	        <td height="14" colspan=2>&nbsp;Infra��o:&nbsp;<%=autoImp.getInfracao().getCodInfracao() %>&nbsp;-&nbsp;<%= autoImp.getInfracao().getDscInfracao() %></td>			
		</tr>
		<tr> 
        	<td colspan=2>&nbsp;Vencimento:&nbsp;<%= autoImp.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan=2>&nbsp;Local:&nbsp;<%=autoImp.getDscLocalInfracao() %>&nbsp;&nbsp;<%= autoImp.getNomMunicipio() %></td>
		</tr>
		<tr> 
        	<td colspan=2>&nbsp;Valor:&nbsp;<%= autoImp.getInfracao().getValRealEdt()%>&nbsp;</td>											
	        <td height="14" colspan=2>&nbsp;Enquadramento:&nbsp;<%=autoImp.getInfracao().getDscEnquadramento() %>&nbsp;-&nbsp;<%= autoImp.getInfracao().getDscGravidade() %>&nbsp;-&nbsp;<font color=red><b>Pontos:&nbsp;<%= autoImp.getInfracao().getNumPonto() %></b></font></td>
		</tr>
		
		
		<%
        if ( (autoImp.getIdentOrgaoEdt().length()>0) || (autoImp.getNumInfracaoRenainf().length()>0) ){ %>			
        
		<tr> 
		    <%
		    if (autoImp.getIdentOrgaoEdt().length()>0) {%>
	        <td height="14" colspan=2>&nbsp;Ident. �rg�o:&nbsp;<%= autoImp.getIdentOrgaoEdt()%>&nbsp;&nbsp;</td>    	    
	        <%}else{%>
	        <td height="14" colspan=2>&nbsp;</td>    	    	        
		    <%}%>			        
	        
		    <%
		    if (autoImp.getNumInfracaoRenainf().length()>0) {%>
        	<td colspan=2>&nbsp;Auto Renainf:&nbsp;<%=autoImp.getNumInfracaoRenainf()%>&nbsp;</td>
	        <%}else{%>
        	<td colspan=2>&nbsp;</td>        
		    <%}%>	
		    
		</tr>
		<%}%>		
		
		
		
		
		<tr> 
	        <td height="14" colspan=4>&nbsp;Propriet�rio:&nbsp;<%=autoImp.getProprietario().getNomResponsavel() %>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Resp. Pontos :&nbsp;
			<% if (autoImp.getCondutor().getNomResponsavel().length()>0) { %>
				<%=autoImp.getCondutor().getNomResponsavel() %>
			<% } else { %>
			    Condutor n�o Identificado
			<% } %>
			</td>
		</tr>
		<tr><td height=1 bgcolor="#000000" colspan=6></td></tr>
	<%	} %>
	</table>      
 	 
<%} else { 
	String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<%@ include file="cabecalho_impconsulta_Diretiva.jsp" %>			
	<% nPag = "1";%>	
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
	<%@ include file="rodape_impconsulta_Diretiva.jsp" %>	
<%} %>
<%	if (consultaAutoId.getAutos().size()>0) { 
		if (contLinha<9){

		} %>
		<%@ include file="rodape_impconsulta_Diretiva.jsp" %>			
<%} %>
</form>
</body>
</html>