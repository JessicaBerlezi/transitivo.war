<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->
<!-- Abre a Sessao -->
<%@ page session="true"%>
<%String path = request.getContextPath();

            %>
<%@ page errorPage="ErrorPage.jsp"%>
<%@ page import="java.util.List"%>
<%@ page import="REC.RelatorBean"%>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<!-- Chama o Objeto da Tabela de Junta -->
<jsp:useBean id="JuntaId" scope="request" class="REC.JuntaBean" />
<!-- Chama o Objeto da Tabela de Relator -->
<jsp:useBean id="RelatorId" scope="request" class="REC.RelatorBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);

for (i=0; i<myOrg.length/2; i++) 
{
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

myJunta      = new Array(<%= JuntaId.getJunta() %>);
cod_junta    = new Array(myJunta.length/4);
cod_juntaOrg = new Array(myJunta.length/4);
sig_junta    = new Array(myJunta.length/4);

for (i=0; i<myJunta.length/4; i++) 
{
	cod_junta[i]    = myJunta[i*4+0] ;
	cod_juntaOrg[i] = myJunta[i*4+1] ;	
	sig_junta[i]    = myJunta[i*4+3] ;
}

function setaJunta(objComboEscolha,objComboMontar) 
{
	var codOrgao =  objComboEscolha.value
	// esvazia combo de grupos
	objComboMontar.length = 1
	objComboMontar.options[0] = new Option(" ")
	k=1	
	for(i=0;i<myJunta.length;i++){
	    if(cod_juntaOrg[i]== codOrgao){		
		   objComboMontar.options[k] = new Option(sig_junta[i],cod_junta[i])
	       k++	
	    }
	}
}

<!-- Fim de preenchimento da combo -->

var bProcessando=false;
function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'buscaRelator':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if ((fForm.codOrgao.value==0)||(fForm.codJunta.value == 0)) {
					alert("�rg�o ou Junta n�o selecionado. \n")
				}
				else 
				{
					fForm.acao.value=opcao
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
			}
			break ; 
			
		case 'A':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;		
				
				if (veCampos(fForm)==true) 
				{
					fForm.acao.value=opcao;
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
				else 
					alert(sErro)
			}
			break ;
		
		case 'R':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;	
					
				if (fForm.atualizarDependente.value=="S") 
				{
					fForm.atualizarDependente.value="N"	  
					fForm.acao.value=opcao;
					fForm.target= "_self";
					fForm.action = "acessoTool";  
					fForm.submit();	  		  
				}
				else 
				{
					close() ;   
				}
			}
			break;
		
		case 'I':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;	
				
				fForm.acao.value=opcao;
				fForm.target= "_blank";
				fForm.action = "acessoTool";  		  
				fForm.submit();		   
			}
			break;	 
			 
		case 'O':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='hide' ; 
			else                 
				document.all["MsgErro"].style.visibility='hidden' ; 
			break;  
			
		case 'V':
			if (document.layers) 
				fForm.layers["MsgErro"].visibility='show' ; 
			else                 
				document.all["MsgErro"].style.visibility='visible' ; 
			break;	  
	}
}

function veCampos(fForm) 
{
	valid   = true ;
	sErro = "" ;

	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codJunta.value==0) {
	   valid = false
	   sErro += "Junta n�o selecionado. \n"
	}	
	   
<!--Verifica se o CPF n�o preenchido-->	
   for (k=0;k<fForm.numCpf.length;k++) 
   {
       var cpf = trim(fForm.numCpf[k].value);
	   var relator = trim(fForm.nomRelator[k].value);
	   if((cpf.length==0)&&(relator.length!=0)){
   		  valid = false
   		  sErro = sErro + "O CPF deve ser preenchido. \nVerifique a seq "+(k+1)+" \n";
       }
      if (cpf.length != 0){
	     if(veCPF(fForm.numCpf[k], 1)== true){
		   valid = true
		 }
		 else{
		   valid = false
		 }
      } 
	}
<!--Verifica se o nome n�o preenchido-->	
   for (k=0;k<fForm.nomRelator.length;k++) {
       var cpf = trim(fForm.numCpf[k].value);
	   var relator = trim(fForm.nomRelator[k].value);
	   if((cpf.length!=0)&&(relator.length==0)){
	     valid = false
   		 sErro = sErro + "O nome do Relator deve ser preenchido. \nVerifique a seq "+(k+1)+" \n";
		 
       }
   }
   for (w=0;w<fForm.indAtivo.length;w++) {
	   var indAtivo = trim(fForm.indAtivo[w].value)
	   var cpf = trim(fForm.numCpf[w].value);
	   var relator = trim(fForm.nomRelator[w].value);

	   if((cpf.length!=0)&&(relator.length!=0)&& ((indAtivo != 'S' && indAtivo != 'N')&& indAtivo.length != 0)){
	       sErro += "O campo Ativo s� deve receber ' S ' - Sim ou ' N ' - N�o. Verifique a seq: "+(w+1)+"\n" ;
		   valid = false ;			   
	   }
	   if((cpf.length!=0)&&(relator.length!=0)&& indAtivo.length == 0){
	       sErro += "O campo Ativo deve ser preenchido. Verifique a seq: " +(w+1)+"\n" ;
	       valid = false;
	   }
    }
	
   return valid ;
}

function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if (valor.length != 0) {     
     valor = Tiraedt(valor,11);
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}

</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;">
<form name="relatorForm" method="post" action=""><jsp:include
	page="Cab.jsp" flush="true" /> <input name="acao" type="hidden"
	value=' '>
<div id="WK_SISTEMA"
	style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 75px; left:204px;">
<table border="0" cellpadding="0" cellspacing="1" width="400"
	align="center">
	<tr>
		<td valign="top" colspan="2"><img src="images/inv.gif" width="3"
			height="15"></td>
	</tr>
	<tr>
		<td align="right" width="20%"><b>�rg�o:&nbsp;</b> <input
			name="atualizarDependente" type="hidden" size="1"
			value="<%= (String)request.getAttribute("atualizarDependente") %>"> <%if (((String) request.getAttribute("atualizarDependente"))
                    .equals("S") == false) {

            %>
		<select name="codOrgao"
			onChange="javascript: setaJunta(this,document.relatorForm.codJunta)">
			<option value="0">&nbsp;&nbsp;</option>
			<script>
					for (j=0;j<cod_orgao.length;j++) {
					document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
		</select>
		<td align=center width=40%><b>Junta:&nbsp;</b> <select name="codJunta">
			<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
		</select> <%} else {

                %> <input name="codOrgao" type="hidden" size="6"
			maxlength="20" value="<%= OrgaoId.getCodOrgao() %>"> <input disabled
			name="sigOrgao" type="text" size="10"
			value="<%= OrgaoId.getSigOrgao() %>">
		<td align=center width=40%><b>Junta:&nbsp;</b> <input name="codJunta"
			type="hidden" size="6" maxlength="20"
			value="<%= JuntaId.getCodJunta() %>"> <input disabled name="sigJunta"
			type="text" size="10" value="<%= JuntaId.getSigJunta() %>"></td>
		<%}

            %>
		</td>
		</td>
		<td align=left width=350>&nbsp;&nbsp;&nbsp; <%if (((String) request.getAttribute("atualizarDependente"))
                    .equals("S") == false) {

                %>
		<button type="button" NAME="Ok"
			style="width: 28px; height: 21px;  border: none; background: transparent;"
			onClick="javascript: valida('buscaRelator',this.form);"><IMG
			src="<%= path %>/images/bot_ok.gif" align="left"></button>
		<%}

            %></td>
	</tr>
	<tr>
		<td valign="top" colspan=2><img src="images/inv.gif" width="3"
			height="2"></td>
	</tr>
</table>
<!--INICIO BOTOES-->
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%"
	align="center">
	<TR>
		<%if (((String) request.getAttribute("atualizarDependente"))
                    .equals("S")) {

                %>
		<td width="33%" align="left">
		<button type=button NAME="Atualizar"
			style="height: 21px; width: 54px;border: none; background: transparent;"
			onClick="javascript: valida('A',this.form);"><IMG
			src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
		</td>
		<td width="34%" align="center">
		<button type=button NAME="imprimir"
			style="height: 21px; width: 54px;border: none; background: transparent;"
			onClick="javascript: valida('I',this.form);"><IMG
			src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		</td>
		<td width="34%" align="right">
		<button type=button NAME="retornar"
			style="height: 21px; width: 54px;border: none; background: transparent;"
			onClick="javascript: valida('R',this.form);"><IMG
			src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
		</td>
		<td align="right" width="33%"></td>
		<%}

            %>
	</TR>

</TABLE>
</div>
<!--FIM BOTOES-->

<div
	style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 52px; top: 143px; height: 32px">
<table border="0" cellpadding="0" cellspacing="1" width="720"
	align="center">
	<tr bgcolor="#993300">
		<td width=80 align="center" bgcolor="#be7272"><font color="#ffffff"><b>Seq</b></font></TD>
		<td width=250 align="center" bgcolor="#be7272"><font color="#ffffff"><b>Relator</b></font></TD>
		<td width=120 align="center" bgcolor="#be7272"><font color="#ffffff"><b>CPF</b></font></TD>
		<td width="180" align="center" bgcolor="#be7272"><font color="#ffffff"><b>Titulo</b></font></TD>
		<td align="center" bgcolor="#be7272"><font color="#ffffff"><b>Ativo</b></font></TD>
	</tr>
</table>
</div>
<!--FIM CABEC DA TABELA-->

<div
	style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 52px; top: 157px; height: 191px;">
<table border="0" cellpadding="0" cellspacing="1" width="100%"
	align="center">
	<%if (((String) request.getAttribute("atualizarDependente"))
                    .equals("S")) {
                List relatores = (List) request.getAttribute("relatores");
                for (int i = 0; i < relatores.size(); i++) {

                    %>
		<tr bgcolor="#FAEAE5">
		<td width=80 align="center"><input name="seq" readonly type="text"
			size="10" value="<%= i+1 %>"> <input name="codRelator" type="hidden"
			value="<%= ((RelatorBean)relatores.get(i)).getCodRelator() %>">
		<td width=250 align="center"><input name="nomRelator" type="text"
			size="43" maxlength="43"
			value="<%= ((RelatorBean)relatores.get(i)).getNomRelator().trim() %>"
			onfocus="javascript:this.select();"
			onChange="this.value=this.value.toUpperCase()"> <input
			name="nomRelator_aux" type="hidden" size="43" maxlength="43"
			value="<%= ((RelatorBean)relatores.get(i)).getNomRelator().trim() %>"
			onfocus="javascript:this.select();"
			onChange="this.value=this.value.toUpperCase()"></td>
		<td width=120 align="center"><input name="numCpf" type="text"
			size="19" maxlength="14"
			value="<%= ((RelatorBean)relatores.get(i)).getNumCpfEdt() %>"
			onkeypress="javascript:Mascaras(this,'999.999.999-99');"
			onfocus="javascript:this.select();"
			onChange="javascript:veCPF(this,0);"> <input name="numCpf_aux"
			type="hidden" size="19" maxlength="14"
			value="<%= ((RelatorBean)relatores.get(i)).getNumCpf() %>"></td>
		<td width=180 align="center"><input name="nomTitulo" type="text"
			size="30" maxlength="35"
			value="<%= ((RelatorBean)relatores.get(i)).getNomTitulo().trim() %>"
			onkeypress="javascript:f_let();" onfocus="javascript:this.select();"
			onChange="this.value=this.value.toUpperCase()"> <input
			name="nomTitulo_aux" type="hidden" size="30" maxlength="35"
			value="<%= ((RelatorBean)relatores.get(i)).getNomTitulo().trim() %>"
			onkeypress="javascript:f_let();" onfocus="javascript:this.select();">
		</td>
		<td align=center><input name="indAtivo" type="text" size="1"
			maxlength="1" value="<%= ((RelatorBean)relatores.get(i)).getIndAtivo().trim() %>" 
			onChange="this.value=this.value.toUpperCase();" onkeypress="javascript:f_let();">
			
		</td>
	</tr>
	<%}
			} else {
				for (int i = 0; i < 10; i++) {

					%>
	<tr bgcolor="#FAEAE5">
		<td width=80 align="center"><input name="seq" readonly type="text"
			size="10" value="<%= i+1 %>"></td>
		<td width=250 align=center><input name="nomRelator" readonly
			type="text" size="43" maxlength="50" value=""></td>
		<td width=120 align=center><input name="numCpf" readonly type="text"
			size="19" maxlength="14" value=""></td>
		<td width=180 align=center><input name="nomTitulo" readonly
			type="text" size="30" maxlength="50" value=""></td>
		<td align=center><input name="indAtivo" readonly type="text"
			size="1" maxlength="1" value="" ></td>
	</tr>

	<%}
            }

        %>
</table>
</div>
<!-- Rodap�--> <jsp:include page="Retornar.jsp" flush="true" /> <jsp:include
	page="Rod.jsp" flush="true" /> <!-- Fim Rodap� --> <!--FIM_CORPO_sistema-->
<!--Div Erros--> <jsp:include page="../sys/DivErro.jsp" flush="true">
	<jsp:param name="msgErro" value="<%= RelatorId.getMsgErro() %>" />
	<jsp:param name="msgErroTop" value="230 px" />
	<jsp:param name="msgErroLeft" value="80 px" />
</jsp:include> <!--FIM_Div Erros--></form>
</BODY>
</HTML>
