<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<% String path=request.getContextPath(); %>   
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="ParamOrgaoId" scope="session" class="REC.ParamOrgBean" /> 
<jsp:useBean id="AutoInfracaoBeanId" scope="session" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="AbrirRecursoBeanDPId" scope="session" class="REC.AbrirRecursoDPBean" /> 
<jsp:useBean id="AbrirRecursoBean1aId" scope="session" class="REC.AbrirRecurso1aBean" /> 
<jsp:useBean id="AbrirRecursoBean2aId" scope="session" class="REC.AbrirRecurso2aBean" /> 
<jsp:useBean id="RequerimentoId" scope="session" class="REC.RequerimentoBean" /> 
<jsp:useBean id="OficioBeanId" scope="request" class="REC.OficioBean" /> 
<jsp:useBean id="DefinirRelatorId" scope="request" class="REC.DefinirRelatorBean" />
<jsp:useBean id="InformarResultId"   scope="request" class="REC.InformarResultBean" />
<%	DefinirRelatorId.setReqs(AutoInfracaoBeanId,RequerimentoId); 
    InformarResultId.setReqs(AutoInfracaoBeanId,RequerimentoId);    
%> 
<jsp:useBean id="RelatorId"       scope="request" class="REC.RelatorBean" /> 
<jsp:setProperty name="RelatorId" property="j_abrevSist" value="REC" />  
<jsp:setProperty name="RelatorId" property="colunaValue" value="cod_Relator_Junta" />
<jsp:setProperty name="RelatorId" property="popupNome"   value="cod_Relator_Junta"  />
<%
   String j = "000000"+RequerimentoId.getCodJuntaJU() ;
   String r = "00000000000"+RequerimentoId.getCodRelatorJU()   ;
   RelatorId.setChecked(j.substring(j.length()-6,j.length())+"-"+r.substring(r.length()-11,r.length()));
   RelatorId.setPopupString("nom_Relator_Junta,SELECT LPAD(j.cod_junta,6,0)||'-'||lpad(r.num_cpf,11,'0') cod_Relator_Junta,"+
     "rpad(j.sig_junta,10,' ')||'-'||r.nom_relator nom_Relator_Junta "+
     "FROM TSMI_RELATOR r,TSMI_JUNTA j "+
     "where r.cod_junta=j.cod_junta and j.ind_tipo='"+DefinirRelatorId.getTipoJunta()+"' AND "+
     "j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' "+
     " order by nom_relator_junta");
%>  
<% String readOnly = "";
   if (!OficioBeanId.getEtapaAuto().equals("PROC")){
     readOnly = "readOnly";
   }
%>
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{ 
		case 'R':
			close() ;
			break;   
				  
		case 'LeReq':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				fForm.acao.value=opcao;
				fForm.target = "_self";
				fForm.action = "acessoTool";  
				fForm.submit();
			}
			break;	 
				
		case 'MostraAuto':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
						 
				fForm.acao.value=opcao;
				fForm.target = "_blank";
				fForm.action = "acessoTool";  
		   		fForm.submit();
		   	}
			break;

		case 'LeAI': 
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if (validaPreenchimentoAuto(fForm)==true) 
				{  
					fForm.acao.value=opcao;
					fForm.target = "_self";
					fForm.action = "acessoTool";  
					fForm.submit();
			   	}
		   	}
			break;		
			
		case 'executarAutos1a2a': 
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
						
				if (validaPreenchimento1a2a(fForm)==true) 
				{  
					fForm.acao.value=opcao;
					fForm.target = "_self";
					fForm.action = "acessoTool";  
					fForm.submit();
				}
	   		}
			break;
			
		case 'executarAutosDP':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
				
				if (validaPreenchimentoDP(fForm)==true) 
				{   
					fForm.acao.value=opcao;
					fForm.target = "_self";
					fForm.action = "acessoTool";  
					fForm.submit();
				}
	   	    }
			break;

	   case 'Limpar':
			if(bProcessando)
			{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
				bProcessando=true;
					    
				fForm.acao.value=opcao;
				fForm.target = "_self";
				fForm.action = "acessoTool";  
				fForm.submit();
			}
			break;
			
	   case 'O':  // Esconder os erro
	   		if (document.layers) 
	   			fForm.layers["MsgErro"].visibility='hide' ; 
		    else                 
		    	document.all["MsgErro"].style.visibility='hidden' ; 
            fForm.numAutoInfracao.value="";
            fForm.numPlaca.value="";
            fForm.numAutoInfracao.focus();
			break;  				
		
  }    
}

function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}

function checa(fForm) { 
	//validacao de radio buttons sem saber quantos sao 
	marcado = -1 
	for (i=0; i<fForm.codResultRS.length; i++) { 
		if (fForm.codResultRS[i].checked) { 
		marcado = i 
		resposta = fForm.codResultRS[i].value 
		} 
	} 
	
	if (marcado == -1) { 
		return false; 
	} 
	return true; 
} 

function validaPreenchimentoAuto(fForm) {
	valid = true ;
	sErro = "" ;   
	
	if (fForm.numAutoInfracao.value == 0){
       valid = false
	   sErro += "Informe o n�mero do Auto. \n"
	   document.all.numAutoInfracao.focus();
    }
    if (fForm.numPlaca.value == 0){
       valid = false
	   sErro += "Informe o n�mero da Placa. \n"
	   document.all.numPlaca.focus();
    }

    
	if (valid==false) alert(sErro) 
	return valid ;
}   

function validaPreenchimentoDP(fForm) {
	valid = true ;
	sErro = "" ;   
	
	if((fForm.numAutoInfracao.value == 0)&&(fForm.numPlaca.value == 0)&&
	   (fForm.numprocesso.value == 0)&&(fForm.datprocesso.value == 0)&&
	   (fForm.txtMotivo.value == 0)){
	   valid = false
	   sErro += "Todos os campos devem ser preenchidos. \n"
	   document.all.numAutoInfracao.focus();
	   
	 } else{
			validaPreenchimentoAuto(fForm);
			//verString(fForm.codRequerimento,"Requerimento",0);
			valid = ValDt(fForm.datJU,1) && valid
			verString(fForm.datJU,"Data de Defini��o do Relator",0);
			verString(fForm.cod_Relator_Junta,"Junta/Relator",0);
			a=fForm.cod_Relator_Junta.value
			if (a.length<18) {
				a="000000000000000000"+a	
				a= a.substring((a.length-18),a.length)
			}
		    fForm.codJuntaJU.value = a.substring(0,6) ;
		    fForm.codRelatorJU.value = a.substring(7,18) ;	
            <% if ("PROC".indexOf(OficioBeanId.getEtapaAuto())>=0){%>
			    if (fForm.numprocesso.value == 0){
			       valid = false
				   sErro += "Informe o n�mero do Processo. \n"
				   document.all.numprocesso.focus();
			    }
					
			    if (fForm.datprocesso.value == 0){
			       valid = false
				   sErro += "Informe a data do Processo. \n"
				   document.all.datprocesso.focus();      
			    }  
			    
			    if (comparaDatas(fForm.datprocesso.value)==false){
	               valid = false
	               sErro += "Data do Processo n�o pode ser anterior a data de hoje. \n"
	               document.all.datprocesso.focus();      
	            }
	            
			    if (fForm.txtMotivo.value == 0 && fForm.numprocesso.value == 0){
			       valid = false
				   sErro += "Informe Motivo relativo ao Processo. \n"
				   document.all.txtMotivo.focus();
			    } 
            <%}%>
			    if (fForm.datEntrada.value == 0){
		           valid = false
			       sErro += "Informe a data do Recurso. \n"
			       document.all.datEntrada.focus();
			    }
		
				if (comparaDatas(fForm.datEntrada.value)==false){
			       valid = false
			       sErro += "Data do Recurso n�o pode ser anterior a data de hoje. \n"
				   document.all.datEntrada.focus();       
			    }    
			
				if (fForm.txtEMail.value != 0){		    
					 if (valida_email(fForm.txtEMail.value)== false) {
					     valid = false ;	
					     sErro+="Email inv�lido.\n"
					     document.all.txtEMail.focus();
				     }
				 }
				 
				 if (comparaDatas(fForm.datEntrada.value)==false){
			       valid = false
			       sErro += "Data do Recurso n�o pode ser anterior a data de hoje. \n"
				   document.all.datEntrada.focus();
			     }
			//verifica qual bean usar dependendo do status
			<%if ("0,1,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>	 
			   <%if(AbrirRecursoBeanDPId.getMsgDP().equals("")){%>
				   <%if (AbrirRecursoBeanDPId.getTpSolDC().length()>0) {%>
					  if(fForm.SolicDPBox.checked == false && fForm.SolicDCBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%} else { %>
					   if(fForm.SolicDPBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%}%>   
			   <%}else if(AbrirRecursoBeanDPId.getMsgDC().equals("") && AbrirRecursoBeanDPId.getTpSolDP().length()>0){%>
				   if (fForm.SolicDCBox.checked == false){
				      valid = false
					  sErro += "Informe o tipo do Recurso. \n"
				   }
			   <%}%>
			<%}%>
			<% if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1) ){%>
			   <%if(AbrirRecursoBean1aId.getMsgDP().equals("")){%>
				 <%if (AbrirRecursoBean1aId.getTpSolDC().length()>0) {%>
					  if(fForm.SolicDPBox.checked == false && fForm.SolicDCBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%} else { %>
					   if(fForm.SolicDPBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%}%>
			   <%}else if(AbrirRecursoBean1aId.getMsgDC().equals("")&& AbrirRecursoBean1aId.getTpSolDP().length()>0){%>
				   if (fForm.SolicDCBox.checked == false){
				      valid = false
					  sErro += "Informe o tipo do Recurso. \n"
				   }
			   <%}%>
			<%}%>
			<% if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1) ){%>
			   <%if(AbrirRecursoBean2aId.getMsgDP().equals("")){%>
				  <%if (AbrirRecursoBean2aId.getTpSolDC().length()>0) {%>
					  if(fForm.SolicDPBox.checked == false && fForm.SolicDCBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%} else { %>
					   if(fForm.SolicDPBox.checked == false){
					      valid = false
						  sErro += "Informe o tipo do Recurso. \n"
					   }
					<%}%>
			   <%}else if(AbrirRecursoBean2aId.getMsgDC().equals("")&& AbrirRecursoBean2aId.getTpSolDP().length()>0){%>
				   if (fForm.SolicDCBox.checked == false){
				      valid = false
					  sErro += "Informe o tipo do Recurso. \n"
				   }
			   <%}%>
			<%}%>
        <% if ("PROC,0,2,3".indexOf(OficioBeanId.getEtapaAuto())>=0){%>
		    if (comparaDatas(fForm.datJU.value)==false){
		       valid = false
		       sErro += "Data Junta/Relator n�o pode ser anterior a data de hoje. \n"
			   document.all.datJU.focus();       
		    }
		    

//		    if (comparaDatasEvento(fForm.datJU.value,fForm.datStatus.value)==false){
//		       valid = false
//		       sErro += "Data n�o pode ser anterior a data do Status: "+fForm.datStatus.value+". \n"
//			   document.all.datJU.focus();       
//		    }
		    
		    if (comparaDatasEvento(fForm.datEntrada.value,fForm.datJU.value)==false){
		       valid = false
		       sErro += "Data Junta/Relator n�o pode ser inferior a data do Recurso. \n"
			   document.all.datJU.focus();       
		    }
		    
		    if (comparaDatas(fForm.datRS.value)==false){
		       valid = false
		       sErro += "Data Resultado n�o pode ser anterior a data de hoje. \n"
		   	   document.all.datRS.focus();    
		    }
	    	if (fForm.txtMotivoRS.value == 0){
		       valid = false
			   sErro += "Informe o motivo relativo ao Resultado. \n"
			   document.all.txtMotivoRS.focus();
		    }  
		    
		    if (checa(fForm) == false){
		       valid = false
			   sErro += "Informe o Resultado. \n"
		    } 
		    if (comparaDatasEvento(fForm.datJU.value,fForm.datRS.value)==false){
		       valid = false
		       sErro += "Data Resultado n�o pode ser inferior a Data Junta/Relator. \n"
			   document.all.datRS.focus();       
		    }
	   <%}%>
	}
    <% if ("0,1,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
		if (valid==false) alert(sErro) 
		return valid ;
    <%}%>	
}  



function validaPreenchimento1a2a(fForm) {
	valid = true ;
	sErro = "" ;   

   validaPreenchimentoDP(fForm);
    if (fForm.codEnvioDO.value == 0){
       valid = false
	   sErro += "Informe o n�mero da Ata. \n"
	   document.all.codEnvioDO.focus();
    }
    
    if (fForm.datEnvioDO.value == 0){
       valid = false
	   sErro += "Informe a data do Envio. \n"
	   document.all.datEnvioDO.focus();
    }
    
    if (comparaDatas(fForm.datEnvioDO.value)==false){
       valid = false
       sErro += "Data de Envio n�o pode ser anterior a Infra��o. \n"
	   document.all.datEnvioDO.focus();
    }
    
    if (comparaDatasEvento(fForm.datRS.value,fForm.datEnvioDO.value)==false){
       valid = false
       sErro += "Data de Envio n�o pode ser inferior a Data do Resultado. \n"
	   document.all.datEnvioDO.focus();       
    }
   
	
   if (fForm.datAtualizDO.value == 0){
       valid = false
	   sErro += "Informe a data da Publica��o. \n"
	   document.all.datAtualizDO.focus();
    }
    
    if (comparaDatas(fForm.datInfracao.value,fForm.datAtualizDO.value)==false){
       valid = false
       sErro += "Data da Publica��o n�o pode ser anterior a Infra��o. \n"
	   document.all.datAtualizDO.focus();
    }
    
    if (comparaDatasEvento(fForm.datEnvioDO.value,fForm.datAtualizDO.value)==false){
       valid = false
       sErro += "Data da Publica��o n�o pode ser inferior a Data de Envio. \n"
	   document.all.datAtualizDO.focus();       
    }

	if (valid==false) alert(sErro) 
	return valid ;
}             

function comparaDatas(data2) {
	var valid = true;
	var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var today=new Date();
	var difference=today-dataB;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = false;
	return valid;
}   

function comparaDatasEvento(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores
// Exemplo: Data de Definir Junta/relator n�o pode ser menor que a data do recurso

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}   
function apaga(defesa) {
	if (defesa=='DC') {
		if (PreparaGuiaForm.SolicDCBox != null) PreparaGuiaForm.SolicDCBox.checked=false ;
	}
	else {
		if (PreparaGuiaForm.SolicDPBox != null) PreparaGuiaForm.SolicDPBox.checked=false ;	
	}
	return ;
}


</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="PreparaGuiaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>
<input name="codRelatorJU" type="hidden" value="">				
<input name="codJuntaJU" type="hidden" value="">
<input name="etapa" type="hidden" value="">
<input name="datStatus" type="hidden" value="<%=AutoInfracaoBeanId.getDatStatus()%>">	
<input name="datInfracao" type="hidden" value="<%=AutoInfracaoBeanId.getDatInfracao()%>">	
<input name="dataRec" type="hidden" value="<%=RequerimentoId.getDatRequerimento()%>">		
<input name="tipoRecurso" type="hidden" value="<%=request.getAttribute("tipoRecurso")%>">

<div id="recurso1" style="position:absolute; left:50px; top:79px; width:720px; z-index:1; visibility: visible;"> 
  <table width="687" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr bgcolor="#faeae5"> 
      <td width="121" height=15 align=right bgcolor="#faeae5">N&ordm; AUTO</td>
      <td width="179">:
      <input   name="numAutoInfracao" type="text" onFocus="javascript:this.select();"    onKeyUp="this.value=this.value.toUpperCase()"  onkeypress="javascript:Mascaras(this,'A99999999999')"  value="<%=AutoInfracaoBeanId.getNumAutoInfracao()%>" size="15" maxlength="12"></td>
      <td width="89" align="right" bgcolor="#faeae5">PLACA</td>
      <td width="198">:
        <input  name="numPlaca" type="text"  value="<%=AutoInfracaoBeanId.getNumPlaca()%>" size="13" maxlength="10" onKeyPress="javascript:Mascaras(this,'AAA9999')" onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()"></td>
      <% if (!request.getAttribute("mostraResultado").equals("S")) {%>
      <td width="44" bgcolor="#faeae5"><button  type="button" name="Ok" style="width: 35px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('LeAI',this.form);"> <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" align="left" hspace="7"></button></td>
      <script>document.PreparaGuiaForm.numAutoInfracao.focus();</script>	   
      <% }%>
      <td width="44" align="left" bgcolor="#faeae5"><button  type="button" name="limpar" style="width: 40x; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('Limpar',this.form);"> <img src="<%= path %>/images/REC/bot_limpar.gif" width="29" height="19" align="left" ></button></td>
	  <% if (request.getAttribute("mostraResultado").equals("S")) {%>
	  <td width="57" align="left" bgcolor="#faeae5"><button  type="button" name="consultaAuto" style="width: 44px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('MostraAuto',this.form);"> <img src="<%= path %>/images/REC/bot_consulta_ico.gif" width="26" height="19" align="left" ></button></td>
	  <% }%>
    </tr>  
  </table>  
</div>

<!--======================================== CADASTRAR PROCESSO ===========================-->
<div id="recurso2" style="position:absolute; left:50px; top:103px; width:720px; z-index:5; overflow: auto; height: 250px; visibility: visible;"> 
   <%   int a =AutoInfracaoBeanId.getHistoricos().size()+1;
		String motivo = "";
		Iterator it  = AutoInfracaoBeanId.getHistoricos().iterator() ;
		REC.HistoricoBean hist = new REC.HistoricoBean();
		if 	(it.hasNext()){
			while (it.hasNext()) {
				a--;
				hist =(REC.HistoricoBean)it.next() ;
				if ("410".equals(hist.getCodEvento())== true){ 
					motivo = hist.getTxtComplemento04(); 
					break;
				}	
			 }
		}
	%>

<% if (request.getAttribute("mostraResultado").equals("S")) {%>
  <table width="687" border="0" cellpadding="1" cellspacing="0">
    <tr bgcolor="#f5d6cc">
      <td colspan="6" align="right" >
        <table width="100%" border="0" cellpadding="0" cellspacing="0" >
          <tr>
            <td width="30%"  rowspan="2" valign="top" bgcolor="#f5d6cc" class="espaco2"><strong>&nbsp;&nbsp;1&#8226; CADASTRAR N&ordm; PROCESSO </strong></td>
            <td width=70% bgcolor="#f5d6cc">
				&nbsp;Propriet&aacute;rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel()%>
				<%= "2".equals(AutoInfracaoBeanId.getProprietario().getIndCpfCnpj()) ? " (Jur.)" : "" %>
			</td>
		   </tr>
		   <tr>
			<td >&nbsp;Condutor: &nbsp;<%=AutoInfracaoBeanId.getCondutor().getNomResponsavel() %> <%= "1".equals(AutoInfracaoBeanId.getIndCondutorIdentificado()) ? " (Ident)" : "" %></td>
          </tr>
      </table></td>
    </tr>   
    <tr >
     <td width="108" align="right" bgcolor="#faeae5">&nbsp;PROCESSO </td>
      <td width="374" bgcolor="#faeae5">:
          <input name="numprocesso" <%=readOnly%> type="text" onFocus="javascript:this.select();"   onKeyUp="this.value=this.value.toUpperCase()" value="<%=AutoInfracaoBeanId.getNumProcesso()%>" size="30" maxlength="22"></td>
      <td width="108"  bgcolor="#faeae5">DATA</td>
      <td colspan="3" align="left" bgcolor="#faeae5">:&nbsp;
        <input  name="datprocesso" <%=readOnly%> type="text"  value="<%=AutoInfracaoBeanId.getDatProcesso() %>" size="12" maxlength="10" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onKeyUp="this.value=this.value.toUpperCase()"></td>
      </tr>
	<tr><td colspan=6 height=2></td></tr>
    <tr>
      <td width="108" align="right" bgcolor="#faeae5">MOTIVO</td>      
      <td  bgcolor="#faeae5">:            
        <input type="text" <%=readOnly%>  onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()" value="<%=motivo%>" size="60" maxlength="70"  name="txtMotivo"></td>
      <td  bgcolor="#faeae5">RECEBIDO EM</td>
	  <td bgcolor="#faeae5" width="89">:&nbsp;<%=AutoInfracaoBeanId.getDatNotificacao()%>&nbsp;&nbsp;			       <% if (AutoInfracaoBeanId.getNomMotivoNotificacao().length()>45) { %>
			       (<%=AutoInfracaoBeanId.getNomMotivoNotificacao().substring(0,46) %>)
                   <% } else if (AutoInfracaoBeanId.getNomMotivoNotificacao().length()>0) { %>
                   (<%=AutoInfracaoBeanId.getNomMotivoNotificacao()%>)
                   <% } %>
                   <b>&nbsp;</b>
	  </td>
      
    </tr>
	 <tr>
      <td colspan=6 height="2"></td>
    </tr>
	<tr>
	<td width="108" align="right" bgcolor="#faeae5">&nbsp;SITUA��O&nbsp;</td>      
      <td colspan="3"  bgcolor="#faeae5">:&nbsp;&nbsp;<%= AutoInfracaoBeanId.getSituacao()%></td>
      </tr>
    <tr>
      <td colspan=6 height="2"></td>
    </tr>
  </table>
 <!--======================================== ABRIR RECURSO ===========================-->
  <table width="687" border="0" cellpadding="1" cellspacing="0">
    <tr> 
	<td height=2 colspan=7></td> 
    </tr>
    <tr bgcolor="#f5d6cc"> 
	    <td colspan=8>
 		 <table width="100%" border="0" cellpadding="0" cellspacing="0" >
 		   <tr> 
	  		<td width=26% class="espaco2">&nbsp;&nbsp;<strong>2&#8226; ABRIR RECURSO </strong> </td>
      	   </tr>	
		 </table>					
      </td>
    </tr>
    <tr   bgcolor="#faeae5">
      <td colspan="7">
          <!--- Verifca se � Defesa Pr�via -->
          <%if ("0,1,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
		         <% if (AbrirRecursoBeanDPId.getTpSolDP().length()>0) { %>
				      &nbsp;&nbsp;<input type="radio"  name="SolicDPBox" value="<%= AbrirRecursoBeanDPId.getTpSolDP() %>" class="input" onClick="javascript: apaga('DC')" style="height: 12px; width: 12px" >
				      <font color=red><b><%= AbrirRecursoBeanDPId.getNFuncao() %></b></font>
				 <%   if (AutoInfracaoBeanId.getProprietario().getNomResponsavel().length()>31) { %>
					  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel().substring(0,31) %>		  
				 <%   } else { %>
					  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
				
				 <%   }%>
		         <% }  else{ 
		               out.println(AbrirRecursoBeanDPId.getMsgDP());%>     
		               <input name="SolicDPBox2"     type="hidden" value="<%=AbrirRecursoBeanDPId.getMsgDP()%>">
		         <%}%>      
		         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		         <% if (AbrirRecursoBeanDPId.getTpSolDC().length()>0) { %>
				      <input type="radio"  name="SolicDCBox" value="<%= AbrirRecursoBeanDPId.getTpSolDC() %>"  class="input" onClick="javascript: apaga('DP')" style="height: 12px; width: 12px">
				      <font color=red><b><%= AbrirRecursoBeanDPId.getNFuncao() %></b>&nbsp;</font>
			      	  <% if (AutoInfracaoBeanId.getCondutor().getNomResponsavel().length()>31) { %>
				      	Resp.Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel().substring(0,31) %> 		  
			      	  <% } else { %>
				      	Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>		  		      
			          <% } %>
				  <% }  else{ 
		               out.println(AbrirRecursoBeanDPId.getMsgDC());%>     
		               <input name="SolicDCBox2"  type="hidden" value="<%=AbrirRecursoBeanDPId.getMsgDC()%>">
		         <%}%>
			<%}%>   
			<!--- Verifca se � 1a instancia -->
			<% if(AutoInfracaoBeanId.getCodStatus().length()>1){ %>
	            <% if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1) ){%>
			         <% if (AbrirRecursoBean1aId.getTpSolDP().length()>0) { %>
					      &nbsp;&nbsp;<input type="radio"  name="SolicDPBox" value="<%= AbrirRecursoBean1aId.getTpSolDP() %>"  class="input" onClick="javascript: apaga('DC')" style="height: 12px; width: 12px" >
					      <font color=red><b><%= AbrirRecursoBean1aId.getNFuncao() %></b></font>
					 <%   if (AutoInfracaoBeanId.getProprietario().getNomResponsavel().length()>31) { %>
						  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel().substring(0,31) %>		  
					 <%   } else { %>
						  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
					
					 <%   }%>
			         <% }else{ 
			               out.println(AbrirRecursoBean1aId.getMsgDP());%>     
			               <input name="SolicDPBox2"  type="hidden" value="<%=AbrirRecursoBean1aId.getMsgDP()%>">
			          <%}%>      
			         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			         <% if (AbrirRecursoBean1aId.getTpSolDC().length()>0) { %>
					      <input type="radio" name="SolicDCBox" value="<%= AbrirRecursoBean1aId.getTpSolDC() %>"  class="input" onClick="javascript: apaga('DP')" style="height: 12px; width: 12px">
					      <font color=red><b><%= AbrirRecursoBean1aId.getNFuncao() %></b>&nbsp;</font>
				      	  <% if (AutoInfracaoBeanId.getCondutor().getNomResponsavel().length()>31) { %>
					      	Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel().substring(0,31) %> 		  
				      	  <% } else { %>
					      	Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>		  		      
				          <% } %>
					  <% }else{ 
			               out.println(AbrirRecursoBean1aId.getMsgDC());%>     
			               <input name="SolicDCBox2"  type="hidden" value="<%=AbrirRecursoBean1aId.getMsgDC()%>">
			           <%}%>
				    <%}%>   	
				<!--- Verifca se � 2a instancia -->	               	  
				   <% if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1) ){%>
			         <% if (AbrirRecursoBean2aId.getTpSolDP().length()>0) { %>
					      &nbsp;&nbsp;<input type="radio" name="SolicDPBox" value="<%= AbrirRecursoBean2aId.getTpSolDP() %>" class="input" onClick="javascript: apaga('DC')" style="height: 12px; width: 12px" >
					      <font color=red><b><%= AbrirRecursoBean2aId.getNFuncao() %></b></font>
					 <%   if (AutoInfracaoBeanId.getProprietario().getNomResponsavel().length()>31) { %>
						  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel().substring(0,31) %>		  
					 <%   } else { %>
						  &nbsp;Propriet�rio:&nbsp;<%= AutoInfracaoBeanId.getProprietario().getNomResponsavel() %>
					
					 <%   }%>
			         <% }else{ 
			               out.println(AbrirRecursoBean2aId.getMsgDP());%>     
			               <input name="SolicDPBox2"     type="hidden" value="<%=AbrirRecursoBean2aId.getMsgDP()%>">
			          <%}%>      
			         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			         <% if (AbrirRecursoBean2aId.getTpSolDC().length()>0) { %>
					      <input type="radio" name="SolicDCBox" value="<%= AbrirRecursoBean2aId.getTpSolDC() %>" class="input" onClick="javascript: apaga('DP')" style="height: 12px; width: 12px">
					      <font color=red><b><%= AbrirRecursoBean2aId.getNFuncao() %></b>&nbsp;</font>
				      	  <% if (AutoInfracaoBeanId.getCondutor().getNomResponsavel().length()>31) { %>
					      	Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel().substring(0,31) %> 		  
				      	  <% }else { %>
					      	Resp. Pontos:&nbsp;<%= AutoInfracaoBeanId.getCondutor().getNomResponsavel() %>		  		      
				          <% } %>
					  <% }else{ 
			               out.println(AbrirRecursoBean2aId.getMsgDC());%>     
			               <input name="SolicDCBox2"  type="hidden" value="<%=AbrirRecursoBean2aId.getMsgDC()%>">
			           <%}%>
				      <%}%>
			<%}%>  
      </td>
      </tr>
	  <tr><td colspan=7 height=2></td></tr>
      <tr bgcolor="#faeae5">
      <td width="130" height=15 align=right>DATA</td> 
      <td width="131">:
        <input  name="datEntrada"  type="text"  value="<%= RequerimentoId.getDatRequerimento()%>" size="12" maxlength="10"  onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" ></td>
      <td width="89" align="right">EMAIL</td>
      <td width="329">:
        <input name="txtEMail"     type="text"  value="<%=(String)request.getAttribute("txtEmailRecurso")%>" size="30" maxlength="50"  onFocus="javascript:this.select();" ></td>
    </tr>
    <tr><td colspan=7 height=2></td></tr>
  </table>
  <table width="687" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr>
      <td height=2 colspan="6"></td>
    </tr>
<!--======================================== DEFINIR JUNTA / RELATOR  ===========================-->
    <tr bgcolor="#f5d6cc">
      <td colspan="6">
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
          <tr>
            <td><strong>&nbsp;&nbsp;3&#8226; DEFINIR JUNTA / RELATOR </strong> </td>
          </tr>
      </table>
	  </td>
    </tr>
    <tr bgcolor="#faeae5">
    <% if ("S".equals(DefinirRelatorId.getEventoOK())) { %>		
      <%if ("0,1,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0){%>
	    <% if ((!AbrirRecursoBeanDPId.getMsgDP().equals("") || !AbrirRecursoBeanDPId.getMsgDC().equals(""))&& ("2,3".indexOf(OficioBeanId.getEtapaAuto())>=0) ) {%>
	      <td width="130" align="right" >REQUERIMENTO</td>
		  <td colspan="3">:&nbsp;<jsp:getProperty name="DefinirRelatorId" property="reqs" /></td>
		<%}%>
      <%}%>
      <% if (("10,11,12,14,15,16".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1)){%>
         <% if ((!AbrirRecursoBean1aId.getMsgDP().equals("") || !AbrirRecursoBean1aId.getMsgDC().equals(""))&& ("2,3".indexOf(OficioBeanId.getEtapaAuto())>=0)) {%>
      <td width="130" align="right" >REQUERIMENTO</td>
	  <td colspan="3">:&nbsp;<jsp:getProperty name="DefinirRelatorId" property="reqs" /></td>
         <%}%>
      <%}%>
      <% if(("30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) && (AutoInfracaoBeanId.getCodStatus().length()>1)){%>
         <% if ((!AbrirRecursoBean2aId.getMsgDP().equals("") || !AbrirRecursoBean2aId.getMsgDC().equals(""))&& ("2,3".indexOf(OficioBeanId.getEtapaAuto())>=0)) {%>
      <td width="130" align="right" >REQUERIMENTO</td>
	  <td colspan="3">:&nbsp;<jsp:getProperty name="DefinirRelatorId" property="reqs" /></td>
	     <%}%>
	  <%}%>
    <%}%>
	</tr>
	<tr bgcolor="#faeae5">
      <td width="130" align="right">JUNTA/RELATOR</td>
      <td width="320" >:&nbsp;<jsp:getProperty name="RelatorId" property="popupString" /></td>
      <td width="320" align="right">DATA</td>
	  <td width="140"> :&nbsp;<input  type="text" name="datJU" size="12" maxlength="10" value="<%=RequerimentoId.getDatJU() %>" onChange="javascript:ValDt(this,0);"  onFocus="javascript:this.select();"  onKeyPress="javascript:Mascaras(this,'99/99/9999');">		</td>
	</tr>

    <tr>
      <td height=2 colspan="4"></td>
    </tr>
  </table>
  <table width="687" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr>
      <td colspan=6 height=2></td>
    </tr>
<!--======================================== INFORMAR RESULTADO  ===========================-->    
    <tr bgcolor="#f5d6cc">
      <td colspan=6 bgcolor="#f5d6cc">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
          <tr>
            <td width=26% bgcolor="#f5d6cc" class="espaco2"><strong>&nbsp;&nbsp;4&#8226; INFORMAR RESULTADO </strong></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td width="235" bgcolor="#faeae5">&nbsp;&nbsp;<input name="codResultRS"  type="radio" value="D" <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"D") %>class="sem-borda" style="height: 12px; width: 12px;">
      DEFERIDO&nbsp;
      <input name="codResultRS" type="radio" value="I"  <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"I") %> class="sem-borda" style="height: 12px; width: 12px;">
      INDEFERIDO </td>
      <td width="135" bgcolor="#faeae5" >&nbsp;DATA:&nbsp;
        <input type="text"  name="datRS" size="12" maxlength="10"  value='<%=RequerimentoId.getDatRS() %>' onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"  onKeyPress="javascript:Mascaras(this,'99/99/9999');">      </td>
      <td width="63" align="right" bgcolor="#faeae5">MOTIVO</td>
      <td width="246" align="left" bgcolor="#faeae5">:
        <input name="txtMotivoRS"  type="text" onFocus="javascript:this.select();"    onKeyUp="this.value=this.value.toUpperCase()" value="<%=RequerimentoId.getTxtMotivoRS() %>" size="35" maxlength="40"></td>
    </tr>
    <tr>
      <td colspan=4 height="2"></td>
    </tr>  
    <% if ("0,2,4,5".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) {%> 
	<tr align="right">
        <td colspan="4"  bgcolor="#faeae5"> 
	    <button  type="button" name="Ok" style="width: 44px; height: 21px;  border: none; background: transparent;"   onClick="javascript: valida('executarAutosDP',this.form);"><img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" align="left" hspace="7" usemap="#passo" border="0"></button></td>
      </tr>
    <%}%>
  </table>
  <% if(AutoInfracaoBeanId.getCodStatus().length()>1){ %>
    <%  if ("10,11,12,14,15,16,30,35,82".indexOf(AutoInfracaoBeanId.getCodStatus())>=0) {%>   
<!--================================= ENVIO P/ D.O.  ===============================-->     
  <table width="687" border="0" cellpadding="1" cellspacing="0" bgcolor="#f5d6cc" class="table">
    <tr>
      <td colspan=5 height=2></td>
    </tr>
    <tr bgcolor="#f5d6cc">
      <td colspan=5 bgcolor="#f5d6cc"> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
          <tr>
            <td width=26% bgcolor="#f5d6cc" class="espaco2"><strong>&nbsp;&nbsp;5&#8226; ENVIAR PARA PUBLICA&Ccedil;&Atilde;O</strong></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td width="128" align="right" bgcolor="#faeae5">&nbsp; ATA</td>
      <td width="190" bgcolor="#faeae5"> :
        <input  name="codEnvioDO" type="text"   value="<%= request.getAttribute("codEnvioDO") %>" size="30" maxlength="20"  onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()"></td>
      <td width="145" align="right" bgcolor="#faeae5">DATA </td>
      <td width="169" align="left" bgcolor="#faeae5">:
        <input  name="datEnvioDO"   type="text" onFocus="javascript:this.select();"  onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" value="<%= request.getAttribute("datEnvioDO") %>" size="12" maxlength="10"></td>
    </tr>
  </table>

<!--============================ INFORMA DATA DE PUBLICA��O ============================-->  
<a name="passo6"></a>  
  <table width="687" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr> 
	   <td height=2 colspan=4></td> 
	 </tr>
	 <td colspan=4 bgcolor="#f5d6cc"> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
          <tr>
            <td width=26% bgcolor="#f5d6cc" class="espaco2"><strong>&nbsp;&nbsp;6&#8226; INFORMAR DATA PUBLICA&Ccedil;&Atilde;O</strong></td>
          </tr>
        </table>
	  </td>  
     <tr><td colspan=4 height=2></td></tr>
     <tr bgcolor="#faeae5">
       <td width="343">
          &nbsp;DATA ATUALIZA&Ccedil;&Atilde;O:&nbsp; 
          <input type="text" name="datAtualizDO" size="12" maxlength="10" value='<%=AutoInfracaoBeanId.getDatPublPDO() %>' 
           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">        
        </td>	
         <td width="62"  bgcolor="#faeae5">
            <button  type="button" name="Ok" style="width: 44px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('executarAutos1a2a',this.form);"><img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19" align="left" hspace="7"></button></td>	
	  </tr>
  </table>	 
     <%}%>
  <%}%>
<%}%>   
</div>

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= AutoInfracaoBeanId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "120 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include>  
<!--FIM_Div Erros-->

<map name="passo">
<area shape="rect" coods="2,2,24,17" href="#passo6">
</map>



</form>
 
</body>
</html>


