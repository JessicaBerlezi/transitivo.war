<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>


<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<jsp:useBean id="RelatorId"       scope="request" class="REC.RelatorBean" /> 
<jsp:setProperty name="RelatorId" property="j_abrevSist" value="REC" />  
<jsp:setProperty name="RelatorId" property="colunaValue" value="cod_Relator_Junta" />
<jsp:setProperty name="RelatorId" property="popupNome"   value="cod_Relator_Junta"  />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'TrocaRelator':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value = opcao;
		   	fForm.target     = "_blank";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  }
		  break;
	   case 'R':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
	    	close() ;
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.cod_Relator_Junta,"Junta/Relator",0);
	a=fForm.cod_Relator_Junta.value
	if (a.length<18) {
		a="000000000000000000"+a	
		a= a.substring((a.length-18),a.length)
	}
	fForm.codJuntaJU.value = a.substring(0,6) ;
	fForm.codRelatorJU.value = a.substring(7,18) ;	
	if (valid==false) alert(sErro) ;
	return valid ;
}
function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["InformaResultForm"].target = "_self";
	document.all["InformaResultForm"].action = "acessoTool";  
	document.all["InformaResultForm"].submit() ;	 
}
function Mostra(opcoes,fForm,numplaca,numauto) {	
	document.all["acao"].value          = opcoes;
	document.all["mostraplaca"].value   = numplaca;
	document.all["mostranumauto"].value = numauto;
   	document.all["InformaResultForm"].target = "_blank";
    document.all["InformaResultForm"].action = "acessoTool";  
   	document.all["InformaResultForm"].submit() ;	 
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResultForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>

<%
   RelatorId.setPopupString("nom_Relator_Junta,SELECT LPAD(j.cod_junta,6,0)||'-'||lpad(r.num_cpf,11,'0') cod_Relator_Junta,"+
     "rpad(j.sig_junta,10,' ')||'-'||r.nom_relator nom_Relator_Junta "+
     "FROM TSMI_RELATOR r,TSMI_JUNTA j "+
     "where r.cod_junta=j.cod_junta and j.ind_tipo='"+GuiaDistribuicaoId.getTipoJunta()+"' and "+
     "j.cod_Orgao='"+UsuarioBeanId.getCodOrgaoAtuacao()+"' and r.ind_ativo = 'S' "+
     " order by nom_relator_junta");
%>   




<input name="acao"           type="hidden" value=' '>
<input name="codRelatorJU"   type="hidden" value="">				
<input name="codJuntaJU"     type="hidden" value="">	
<input name="mostraplaca"    type="hidden" value=" ">
<input name="mostranumauto"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:73px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#faeae5">
        <td width="10%"></td>		     
        <td width="80%" height=20 align=center><b>GUIA DE DISTRIBUI��O&nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=GuiaDistribuicaoId.getNumGuia()%>
		&nbsp;&nbsp;-&nbsp;&nbsp;<%=GuiaDistribuicaoId.getDatProc()%>        </td>
        <td width="10%" align="center" valign="middle">
		</td>		
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
     <tr bgcolor="#faeae5">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal" >
	     	<tr>
	          <td width="50%" height=10>&nbsp;Junta:&nbsp;<b><%=GuiaDistribuicaoId.getSigJunta() %></b></td>
		        <td width="50%" height="20" align=right>Relator:&nbsp;<b><%=GuiaDistribuicaoId.getNomRelator() %></b>&nbsp;</td>        
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=3 height=5></td></tr>     
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#faeae5">
        <td width="90%" height=20>&nbsp;Nova Junta/Relator:&nbsp;
			<jsp:getProperty name="RelatorId" property="popupString" /></td>
        <td width="10%" align="center" valign="middle"> 
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('TrocaRelator',this.form);">	
    	      <img src="<%= path %>/images/REC/bot_ok.gif" width="26" height="19"> 
        	  </button>
		</td>
     </tr>
     <tr><td colspan=2 height=8></td></tr>     
  </table>
</div>


<%
String posTop = "155 px";
String posHei = "175 px";
%>


<%@ include file="GuiaCorpo.jsp" %>  



<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp"%>
<%@ include file="Rod_Diretiva.jsp" %>

<!--Div Erros-->
<jsp:include page="EventoErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= GuiaDistribuicaoId.getMsgErro() %>" />
  <jsp:param name="msgOk"       value= "<%= GuiaDistribuicaoId.getMsgOk() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!--Div Erros-->
<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->



</form>
</body>
</html>


