<%@ page errorPage="ErrorPage.jsp" %>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="top" class="fontmedia"> 
      		<table width="100%" border="0" cellpadding="0" cellspacing="0" style="border-top: 0px none;">
              <tr>
                <td height="20" align="center" valign="middle" class="fontmaior" style="background-color: #cccccc; font-weight: bold; border-top: 1px solid #000000;border-right: 1px solid #000000">NOTIFICA&Ccedil;&Atilde;O DA AUTUA&Ccedil;&Atilde;O POR INFRA&Ccedil;&Atilde;O &Agrave; LEGISLA&Ccedil;&Atilde;O DE TR&Acirc;NSITO </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="border-top: 1px solid #000000"><table width="80%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="14%" height="32" align="right" valign="middle"><img src="<%=path%>/images/logos/logoniteroi1.gif" width="693" height="88"></td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td align="center" valign="middle" height="5"></td>
              </tr>
            </table>
      		<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr><td height="1" bgcolor="#000000"></td></tr>
			</table>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
				<tr valign="top">
				  <td height="35" class="fontmaior">N&ordm; Auto:&nbsp;<strong><%=AutoInfracaoBeanId.getNumAutoInfracao() %></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Placa:&nbsp;<font size="2"><strong><%=AutoInfracaoBeanId.getNumPlaca() %></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N&ordm; AR:&nbsp;<strong><%=AutoInfracaoBeanId.getNumNotifAut() %></strong></font></td> 
				    <td width="35%" align="center"><font size="2">EXPEDI&Ccedil;&Atilde;O<br>
				    </font></td>
				</tr>
				<tr valign="top">
				  <td height="35"><font size="2"><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></font></td>
			      <td height="35" align="center"><font size="2">
			        <%if (("3,4,5".indexOf(NotifBeanId.getCodNotificacao()))>=0){
	                 	if (AutoInfracaoBeanId.getDatNotifPen().length()>0){%>
	                 	<%=AutoInfracaoBeanId.getDatNotifPen() %>
	                  <%}else{%>
	                  <%=AutoInfracaoBeanId.getDatStatus() %>
	                  <%}%>
                 <%}else{%>
                 		<%if (AutoInfracaoBeanId.getDatNotifAut().length()>0){%>
	                 		<%=AutoInfracaoBeanId.getDatNotifAut() %>
	                  	<%}else{%>
	                  		<%=AutoInfracaoBeanId.getDatStatus() %>
						 <%}	                 
                  }%>
                  </font></td>
				</tr>
				<tr valign="top">
				  <td height="35" colspan="2"><font size="2"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %>
				      <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco().length() > 0){%>
				      ,&nbsp;				      <%}%>
				      <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %> 
				      <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco().length() > 0){ %>				      &nbsp;/ &nbsp;
				      <%}%>
			      <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %></font></td>
			    </tr>
				<tr valign="top">
				  <td height="35" colspan="2"><font size="2"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro() %></font></td>
			    </tr>
				<tr valign="top">
				  <td height="35"><font size="2"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumCEPEdt() %></font></td>
				    <td align="left" class="fontmaior"><%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomCidade() %>&nbsp;</td>
				</tr>
			</table>
			<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
			  <tr>
                <td height="20" valign="middle" bgcolor="#CCCCCC" style="border: 1px solid #000000; border-bottom:0px none;padding: 3px 3px 3px 3px"><div align="center"><strong class="fontmaior">AVISO</strong></div></td>
		      </tr>
			  <tr>
                <td height="32" valign="middle" bgcolor="#CCCCCC" style="border: 1px solid #000000; border-top:0px none;padding: 3px 3px 3px 3px"><strong class="fontmaior">Em cumprimento ao disposto no inciso II, par&aacute;grafo &uacute;nico, do Artigo 281 da Lei N&ordm; 9503 de 23 de Julho de 1997, alterado pela Lei N&ordm; 9602 de 21 de Janeiro de 1998 (C&oacute;digo de Tr&acirc;nsito Brasileiro) e pela Resolu&ccedil;&atilde;o n&ordm; 149 do CONTRAN de 19 de Setembro de 2003, notificamos V.Sa. da lavratura de Auto de Infra&ccedil;&atilde;o &agrave; Legisla&ccedil;&atilde;o de Tr&acirc;nsito para ve&iacute;culo de vossa propriedade, conforme os dados contidos nesta Notifica&ccedil;&atilde;o da Autua&ccedil;&atilde;o. <br>
    A Notifica&ccedil;&atilde;o de Penalidade ser&aacute; emitida automaticamente ap&oacute;s o vencimento do prazo estabelecido para a apresenta&ccedil;&atilde;o da Defesa da Autua&ccedil;&atilde;o.</strong></td>
		      </tr>
	</table>	
	</td>
  </tr>
</table>
