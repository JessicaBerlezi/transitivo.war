<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.io.File" %>
<jsp:useBean id="ParamOrgBeanId"     scope="session" class="REC.ParamOrgBean" /> 
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<jsp:useBean id="NotifBeanId" scope="request" class="REC.InstrNotBean" /> 
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 

<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<style type="text/css">
table
     {
     font-family: verdana;
     font-size: 8px;
	 border: 1px solid #000000;
	 color: #000000;
     }	 
td
	{
	font-family: verdana;
	font-size: 8px;
	position: static;
	border: 0px solid #000000;
	color: #000000;
	
}
.tdborda
	{
	border: 1px solid #000000;
}
.table_linha_top
	{
	border-top: 0.1em solid #000000;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em none #ffffff;
}	
.td_linha_left
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}
.td_linha_right
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em solid #000000;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em none #ffffff;
}
.td_linha_left_top
	{
	border-top:  0.1em solid #000000;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}

select
     {
	 font-family: verdana;
	 font-size: 9px;
	 border: 2px solid #000000;
	 }

textarea
     {
	 font-family: verdana;
	 font-size: 9px; 
	 border-color: 1px solid #666666; 
	 background-color: #eeeeee;
	 }

input 
	 {
	font-family: verdana;
	font-size: 9px;
	border: 1px solid #666666;
	background-color: #eeeeee;
	 }
	 
.input_semborda 
	 {
	font-family: verdana;
	font-size: 9px;
	border: 0px none ;
	background-color: #eeeeee;
	}
.semborda
     {
	 border-style: none ; 
	 background-color: #ffffff;
	 }
	 
.linha
	{
	font-family: verdana;
	font-size: 9px;
	height: auto;
	margin: 0px;
	position: static;
	border: thin none #ffffff;
	
}
.fontmaior
    {
	font-family:  verdana;
	font-size: 14px
	}
	
.fontmedia
    {
	font-family:  verdana;
	font-size: 9px
	}
.fontmenor
    {
	font-family:  verdana;
	font-size: 7px
	}
.fontseis
    {
	font-family:  verdana;
	font-size: 6px
	}
</style> 
<table width="60%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="top" class="fontmedia"> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" >
        <tr> 
		  <td width="47%" align="center" class="td_linha_right"><img src="<%= path %>/images/REC/BRASAO_RJ.jpg" width="198" height="65" /></td>
		
    <%--if (!"".equals(AutoInfracaoBeanId.getNomOrgao())){ --%>	                
    	  <td width="53%" valign="top" class="semborda"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
            <tr>
              <td colspan="2" class="fontmedia"><table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
                  <tr>
                    <td height="19" style="background-color: #cccccc; border-bottom:0.1em solid #000000;" class="td_linha_left"><strong>&nbsp;1 - IDENTIFICA&Ccedil;&Atilde;O DA INFRA&Ccedil;&Atilde;O </strong></td>
                  </tr>
              </table></td>
            </tr>
            <tr>
              <td width="47%" height="15" class="td_linha_left_top" style="border-right:0.1em solid #000000; border-bottom:0.1em solid #000000;" ><span class="fontmenor">&nbsp;<strong>C&oacute;d. &Oacute;rg&atilde;o Autuador </strong></span></td>
              <td width="53%" rowspan="2" valign="top" class="td_linha_left_top"><span class="fontmenor"><br/><br/>&nbsp;&nbsp;<strong>N&ordm;</strong></span><br />
                &nbsp;<span class="fontmaior"><strong><%=AutoInfracaoBeanId.getNumAutoInfracao()%></strong></span><br /></td>
            </tr>
            <tr>
              <td height="30" class="td_linha_left_top" style="border-right:0.1em solid #000000;"><span class="fontmaior">&nbsp;<strong><%=AutoInfracaoBeanId.getCodOrgao()%></strong></span></td>
            </tr>
          </table></td>
        </tr>
</table>

    </td>
</tr>
<tr> 
      <td height="277" align="center" valign="top"> 
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top"> 
		
<td width="50%" height="35" align="center" valign="top" class="fontmedia"> 
		  <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr> 
                      
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;2 -IDENTIFICA&Ccedil;&Atilde;O DO VE&Iacute;CULO </strong></td>
              </tr>
            </table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
            <tr>
              <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                  <tr>
                    <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>PLACA</strong></span><br />
                        <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getNumPlaca()%> </td>
                    <td width="26%" valign="top" class="semborda"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                        <tr>
                          <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>UF</strong></span><br />
                              <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getOrgao().getEndereco().getCodUF()%></td>
                        </tr>
                    </table></td>
                  </tr>
              </table></td>
            </tr>
          </table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr> 
                <td valign="top"> 
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                       <td width="100%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>MUNIC&Iacute;PIO</strong></span><br>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="padding-top: 0px;">
                        <tr>
                        <td height="30">&nbsp;<span class="fontmedia"><%=AutoInfracaoBeanId.getNomMunicipio()%></span></td>  
                      </tr>
                    </table>					
                    </tr>
                  </table>                
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="52%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>MARCA/MODELO</strong></span><br />
                        <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getVeiculo().getDscMarcaModelo() %></td>
                      <td width="48%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>ESP&Eacute;CIE</strong></span><br />
                      &nbsp;<%=AutoInfracaoBeanId.getVeiculo().getDscEspecie() %></td>
                    </tr>
                  </table></td>
              </tr>
            </table>
			
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr> 
                  
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;3 - IDENTIFICA&Ccedil;&Atilde;O DO CONDUTOR/INFRATOR </strong></td>
              </tr>
            </table>
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr>
         <td valign="top">
		 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
               <tr> 
                        
                        
                      <td width="32%" height="30" valign="top" class="tdborda"><span class="fontmedia">
                        <label>
                        <input type="checkbox" name="checkbox" value="checkbox" class="semborda" />
                        </label>
                        &nbsp;<strong>CONDUTOR</strong></span><br>
                      <span class="fontmedia">
                      <input type="checkbox" name="checkbox2" value="checkbox" class="semborda" />
                      &nbsp;<strong>INFRATOR</strong></span></td>
                      <td width="68%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>NOME</strong></span><br>
                      <span class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></span> </td>
               </tr>
		  </table>		</td>
	</tr>
</table>
			
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr>
                <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>N&ordm; DO REGISTRO DA CNH OU DA PERMISS&Atilde;O </strong></span><br />
                          <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getProprietario().getNumCnh()%> </td>
                      <td width="26%" valign="top" class="semborda">
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                          <tr>
                            <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>CPF/CGC</strong></span><br />
                                <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getProprietario().getNumCpfCnpjEdt()%></td>
                          </tr>
                      </table>
                      </td>
                    </tr>
                </table></td>
              </tr>
            </table>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                      <tr> 
                        
                      <td width="30%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>ENDERE&Ccedil;O</strong></span><br>
                        <span class="fontmedia">&nbsp;<%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco() %> 
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtEndereco().length() > 0){%> ,&nbsp; <%}%>
                 <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco() %>
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getNumEndereco().length() > 0){ %> 
      				  &nbsp;/ &nbsp;
   				 <%}%>
                 <%=AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento() %>
                 <%if (AutoInfracaoBeanId.getProprietario().getEndereco().getTxtComplemento().length() > 0){ %> 
      				  &nbsp;- &nbsp;
   				 <%}%>    
   				 <%=AutoInfracaoBeanId.getProprietario().getEndereco().getNomBairro() %>                     
                        
                        </span> </td>
                      </tr>
                    </table></td>
              </tr>
            </table>
			
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>ASSINATURA</strong></span><br>
                        <span class="fontmedia">&nbsp;</span>                      </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
			
			
			
            <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;4 - IDENTIFICA&Ccedil;&Atilde;O DO LOCAL DE COMETIMENTO DA INFRA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr>
                <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="30%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>LOCAL</strong></span><br />
                          <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getDscLocalInfracao()%></td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr> 
                <td valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                        
                      <td width="34%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;</span><br>
                      <span class="fontmedia">&nbsp;</span></td>
                      <td width="66%" valign="top" ><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                        <tr>
                          <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>DATA</strong></span><br />
                              <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getDatInfracao() %></td>
                          <td width="26%" valign="top" class="semborda"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                              <tr>
                                <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>HORA</strong></span><br />
                                    <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getValHorInfracaoEdt() %></td>
                              </tr>
                          </table></td>
                        </tr>
                      </table></td>
                    </tr>
                </table>			    </td>
              </tr>
            </table>		  
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
              <tr>
                <td valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>MUNIC&Iacute;PIO</strong></span><br />
                      <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getNomMunicipio()%></td>
                      <td width="26%" valign="top" class="semborda">
                      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                        <tr>
                          <td width="74%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>C&Oacute;DIGO</strong></span><br />
                              <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getCodMunicipio() %></td>
                        </tr>
                      </table></td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;5 - TIPIFICA&Ccedil;&Atilde;O DA INFRA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="31%" height="30" valign="top" class="tdborda">
                <table width="92%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="25%" valign="top" class="semborda"><span class="fontmenor">&nbsp;<strong>C&Oacute;DIGO</strong></span></td>
                    </tr>
                    <tr>
                      <td height="30" valign="top" class="semborda">&nbsp;<%=AutoInfracaoBeanId.getInfracao().getCodInfracao() %></td>
                    </tr>
                </table></td>
                <td width="69%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>DESCRI&Ccedil;&Atilde;O</strong></span><br />
                &nbsp;<%=AutoInfracaoBeanId.getInfracao().getDscInfracao()%></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="56%" height="30" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>EQUIPAMENTO/INSTRUMENTO DE AFERI&Ccedil;&Atilde;O UTILIZADO </strong></span><br />
                    <span class="fontmenor">&nbsp;</span><%=AutoInfracaoBeanId.getDscMarcaModeloAparelho() %></td>
                <td width="24%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>MEDI&Ccedil;&Atilde;O REALIZADA </strong></span><br />
                  &nbsp;<%=AutoInfracaoBeanId.getValVelocAferida() %></td>
                <td width="20%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;<strong>LIMITE PERMITIDO </strong></span><br />
&nbsp;<%=AutoInfracaoBeanId.getValVelocPermitida() %></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;6 - OBSERVA&Ccedil;&Atilde;O </strong></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              
              <tr>
                <td width="38%" height="20" class="fontmedia"><span class="fontmenor">&nbsp;<strong>C&oacute;d. &Oacute;rg&atilde;o Autuador </strong></span></td>
              </tr>
              <tr>
                <td height="20" class="table_linha_top">&nbsp;<%=ParamOrgBeanId.getParamOrgao("OBSERVACAO_AUTO_ADM_LIN_1","","10")%></td>
              </tr>
              <tr>
                <td height="20" class="table_linha_top">&nbsp;<%=ParamOrgBeanId.getParamOrgao("OBSERVACAO_AUTO_ADM_LIN_2","","10")%></td>
              </tr>
              <tr>
                <td height="20" class="table_linha_top">&nbsp;<%=ParamOrgBeanId.getParamOrgao("OBSERVACAO_AUTO_ADM_LIN_3","","10")%></td>
              </tr>
            </table>
            <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
              <tr>
                <td height="19" bgcolor="#CCCCCC" class="tdborda"><strong>&nbsp;7 - IDENTIFICA&Ccedil;&Atilde;O DO AGENTE </strong></td>
              </tr>
            </table>
            
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
                <td width="54%" height="30" valign="top" class="tdborda">
                <table width="92%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="25%" valign="top" class="semborda"><span class="fontmenor">&nbsp;<strong>N&ordm; DO AGENTE </strong></span></td>
                    </tr>
                    <tr>
                      <td height="30" valign="top" class="semborda">&nbsp;<%=AutoInfracaoBeanId.getDscAgente()%></td>
                    </tr>
                </table>
                </td>
                <td width="54%" height="30" valign="top" class="tdborda">
                <table width="92%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr>
                      <td width="25%" valign="top" class="semborda"><span class="fontmenor">&nbsp;<strong>ASSINATURA</strong></span></td>
                    </tr>
                    <tr>
                      <td height="30" valign="top" class="semborda">&nbsp;<%=AutoInfracaoBeanId.getNomAgente() %></td>
                    </tr>
                </table>
                </td>
              </tr>
            </table>
            </td>		
          </tr>
      </table>
    </td>
  </tr>
</table>

