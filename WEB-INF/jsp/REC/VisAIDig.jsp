<!--M�dulo : REC
	Vers�o : 1.2
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama os beans -->
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" />
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" />
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) 
{
	switch (opcao) 
	{
		case 'R':
			close() ;
			break;
		case 'O':
	   		document.all["MsgErro"].style.visibility='hidden';
		  	break;  
		case 'V':
	   		document.all["MsgErro"].style.visibility='visible';
		  	break;	  	

		case 'buscaAuto':
			if(bProcessando)
			{
				alert("Opera��o em Processamento, por favor aguarde...");
				return;
			}
			else
			{
				bProcessando=true;   
				if (fForm.numAutoInfracao.value=="") 
				{
				   alert("N�mero de auto n�o selecionado.\n")
				}
				else 
				{
					fForm.acao.value=opcao
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				}
			}
			break ;
			
		case 'chamaImpressao' :
			fForm.acao.value=opcao
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();
			break;		
			
		case 'chamaImagem' :
			fForm.acao.value=opcao
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();
			break;		
	}
}

function mostraImagem(mypage, myname, w, h, scroll) {		
	var winl = (screen.width - w) / 2;
	//var wint = (screen.height - h) / 2;
	var wint=0;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
	win = window.open(mypage, myname, winprops)
	if (parseInt(navigator.appVersion) >= 4) { 
		win.window.focus(); 
		win.window.print();
	}
}

function print_div(div_id) {

	if (div_id =="D") {
		window.onbeforeprint=printDados
		window.onafterprint=voltaNormal
		window.print();	
	}
	else if (div_id =="I"){
		//window.onbeforeprint=printImagem
		//window.onafterprint=voltaNormal
	<%	String pathImageAuto = "";
	    if (AutoInfracaoBeanId.getAIDigitalizado().getMsgErro().equals("")){
	    	pathImageAuto = AutoInfracaoBeanId.getAIDigitalizado().getImagem();
		}%>
		
		mostraImagem('<%=path + pathImageAuto %>','AI','625',screen.height-60,'yes');
	}
	
} 

function printDados() {
	document.all.imagem.style.visibility="hidden";
	document.all.menulateral.style.visibility="hidden";
	document.all.tabelaDados.style.visibility="visible";
}
function voltaNormal() {
	document.all.imagem.style.visibility="visible";
	document.all.menulateral.style.visibility="visible";
	document.all.tabelaDados.style.visibility="hidden";	
}
function printImagem() {
	document.all.imagem.style.visibility="visible";
	document.all.menulateral.style.visibility="hidden";
	document.all.tabelaDados.style.visibility="hidden";
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AIForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
  <input name="acao" type="hidden" value=' '>	
  <input name="numAutoInfracao" type="hidden" value='<%=AutoInfracaoBeanId.getNumAutoInfracao()%>'>
  <input name="numPlaca"        type="hidden" value='<%=AutoInfracaoBeanId.getNumPlaca()%>'>				
  <input name="numProcesso"     type="hidden" value='<%=AutoInfracaoBeanId.getNumProcesso()%>'>
  
  
<%if (!AutoInfracaoBeanId.getAIDigitalizado().getMsgErro().equals("")) { //se  houver erro, mostra cabecalho e msg de erro.%>  

<!--DIV FIXA-->
<div id="req1" style="position:absolute; left:50px; top:80px; width:738px; height:75px; z-index:5; overflow: auto; visibility: visible;"> 
<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA AUTO DIGITALIZADO</b></td>        
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td width="90"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="108"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="323"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="199"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan=2><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
				&nbsp;(<%=AutoInfracaoBeanId.getDatStatus() %>)</td>
      </tr>
      <tr> <td height="10"></td>  </tr>
  </table>
  </div>
<!--FIM DIV FIXA-->
<div id="req2" style="position:absolute; left:50px; top:138px; width:738px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr><td height="50"></td></tr>
	  <tr><td bgcolor="#f5d6cc" align="center" height="25">&nbsp;&nbsp;<strong><%=AutoInfracaoBeanId.getAIDigitalizado().getMsgErro()%></strong></td> </tr>	
	</table>
</div>

<%} else  { //se n�o houver erro, mostra imagem%>
  
<!--AREA DE RESULTADO DO MODULO - TABELA COM 2 COLUNAS-->
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
	<td align="left">	
	<div id="menulateral" style="position:absolute; left:40px; top:90px; width:160px; height:250px;  z-index:1; background-color: transparent; overflow: visible; visibility: visible;"> 
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<%String mostrarLocal = (String) request.getAttribute("mostrarLocal");
 		  if ((mostrarLocal != null) && (mostrarLocal != "N")) {%>
        <tr> 
			<td><strong><font color="#666666">N&uacute;mero do Auto</font></strong></td>
		</tr>
		<tr> 
			<td bgcolor="#faeae5" height="18"><%=AutoInfracaoBeanId.getNumAutoInfracao()%></td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="5"></td>
		</tr>
		<tr> 
			<td><strong><font color="#666666">Data de Digitaliza&ccedil;&atilde;o</font></strong></td>
		</tr>
		<tr> 
			<td bgcolor="#faeae5" height="18"><%=AutoInfracaoBeanId.getAIDigitalizado().getDataDigitalizacaoString()%></td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="10"></td>
		</tr>
		<tr>
			<td>
				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
					<td width="90" align="left">
						<strong><font color="#666666">N&ordm; Lote</font></strong>
					</td>
					<td align="left">
						<strong><font color="#666666">N&ordm; Caixa</font></strong>
					</td>
				</tr>
				  <tr bgcolor="#faeae5"> 
                    <td width="90" align="left" height="18"><%=AutoInfracaoBeanId.getAIDigitalizado().getNumLote()%> 
                    </td>
					<td width="70" align="left"> <%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumCaixa()%>                    </td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="5"></td>
		</tr>
		<tr> 
			<td>
				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
					<td width="90" align="left">
						<strong><font color="#666666">Prateleira</font></strong>
					</td>
					<td align="left">
						<strong><font color="#666666">Estante</font></strong>
					</td>
				</tr>
				  <tr bgcolor="#faeae5"> 
                    <td width="90" align="left" height="18"> <%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumPrateleira()%> 
                    </td>
					<td width="70" align="left"> <%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumEstante()%>                    </td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			  <td><img src="<%= path %>/images/inv.gif" width="1" height="5"></td>
		</tr>
		<tr> 
			<td>
				<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                  <tr>
					<td width="90" align="left">
						<strong><font color="#666666">Bloco</font></strong>
					</td>
					<td align="left">
						<strong><font color="#666666">Rua</font></strong>
					</td>
				</tr>
				  <tr bgcolor="#faeae5"> 
                    <td width="90" align="left" height="18"><%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumBloco()%> 
                    </td>
					<td width="70" align="left"><%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumRua()%>                    </td>
				</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="5"></td>
		</tr>
		<tr> 
			<td><strong><font color="#666666">&Aacute;rea</font></strong></td>
		</tr>
		<tr> 
			<td width="160" height="18" bgcolor="#faeae5"><%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumArea()%>              </td>
		</tr>
		<tr>
			<td><img src="<%= path %>/images/inv.gif" width="1" height="10"></td>
		</tr>
		<%}%>
		<tr> 
			<td>
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>					
					<% if (!AutoInfracaoBeanId.getAIDigitalizado().getImagem().equals("")) {%>				
					<td align="center">
						<button style="border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript:valida('chamaImagem',this.form)";>
							<img src="<%= path %>/images/REC/bot_imprimir_img.gif" width="71" height="26" alt="" border="0">
						</button>
					</td>
					<td align="center">
						<%if ((mostrarLocal != null) && (mostrarLocal != "N")) {%>					
						<button style="border: 0px; background-color: transparent; height: 28px; width: 88px; cursor: hand;" type="button" onClick="javascript:valida('chamaImpressao',this.form)";>
							<img src="<%= path %>/images/REC/bot_imprimir_localiz.gif" width="86" height="26" alt="" border="0">
						</button>						
						<%}%>						
					</td>
					<%}%>
				</tr>
				</table>
			</td>
		</tr>		
		</table>	
	</div>	
	</td>
</tr>
</table>
<!--FIM DA AREA DE RESULTADO-->

<%
  String largura = ParamOrgBeanId.getParamOrgao("LARGURA_AI_DIG", "470", "7");
  if ((largura == null) || largura.equals("")) largura = "470";
  String altura = ParamOrgBeanId.getParamOrgao("ALTURA_AI_DIG", "903", "7");
  if ((altura == null) || altura.equals("")) largura = "903";  
  String larguraDiv = String.valueOf(Integer.parseInt(largura) + 18);
%>
<div id="imagem" align="center" style="position:absolute; left:200px; top: 0px; width:<%=larguraDiv%>px; height: 100%;  z-index:99; background-color: transparent; overflow:auto; visibility: visible;">
   <img id="imagemAI" alt="Imagem do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getAIDigitalizado().getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getAIDigitalizado().getParametro(ParamSistemaBeanId)%>&header=#" width="<%=largura%>" height="<%=altura%>"> 
</div>

<!--TELA DE IMPRESSAO RESULTADO-->
  <div id="tabelaDados" style="position:absolute; overflow: visible; z-index:-5; left:50px; top: 111px; width:720px; height:130px; visibility: hidden;" > 
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr> 
        <td height="5" colspan="4"></td>
      </tr>
      <tr bgcolor="#FAEAE5"> 
        <td width="18%" height="25" bgcolor="#FAEAE5"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero 
          do Auto</font></strong></td>
        <td width="32%" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getNumAutoInfracao()%> 
        </td>
        <td width="18%" ><strong><font color="#666666">Data de Digitaliza&ccedil;&atilde;o</font></strong></td>
        <td width="32%" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getDataDigitalizacaoString()%> 
        </td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr bgcolor="#FAEAE5"> 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;N&uacute;mero 
          do Lote</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getNumLote()%></td>
        <td ><strong><font color="#666666">N&uacute;mero da Caixa</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumCaixa()%></td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr bgcolor="#FAEAE5"> 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Prateleira</font></strong></td>
        <td><strong><font color="#993300">: </font></strong>&nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumPrateleira()%></td>
        <td height="25"><strong><font color="#666666">Estante</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumEstante()%> 
        </td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr bgcolor="#FAEAE5"> 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Bloco</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumBloco()%></td>
        <td height="25"><strong><font color="#666666">Rua</font></strong></td>
        <td ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumRua()%></td>
      </tr>
      <tr> 
        <td height="1" colspan="4"></td>
      </tr>
      <tr bgcolor="#FAEAE5"> 
        <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;&Aacute;rea</font></strong></td>
        <td colspan="3" ><strong><font color="#993300">:</font></strong> &nbsp;&nbsp;<%=AutoInfracaoBeanId.getAIDigitalizado().getCaixa().getNumArea()%></td>
      </tr>
    </table>
</div>
<!--FIM TELA DE IMPRESSAO RESULTADO-->

<%}//fim do else  %>

<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
 <!--FIM_CORPO_sistema-->
</form>
</BODY>
</HTML>