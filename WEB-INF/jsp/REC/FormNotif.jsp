<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">

function abre(janela)
        {
        window.open(janela,"","toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,copyhistory=no,width=760,height=360,left")
        }
		//
		
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,notif,fForm) {
   // Mostra tela N00
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	  
	   case 'FormNotifBranco':		  
		  fForm.acao.value=opcao;	 
		  fForm.codnotif.value=fForm.NotificacoesRadios[notif].value;		
		  fForm.target= "_blank";
		  fForm.action = "acessoTool";  		  
		  fForm.submit();	  
	  	   	break;
	}
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="SegViaNotifForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
  
<input name="acao"              type="hidden" value=' '>				
<input name="codnotif"          type="hidden" value=' '>				
<!--IN�CIO_CORPO_sistema--> 
<div id="NotifDiv" style="position:absolute; left:66px; top:93px; width:687px; height:200px; z-index:5; overflow: visible; visibility: visible;"> 
			
	  <table width="100%" border="0" class="titprincipal" cellpadding="0" cellspacing="0" class="table">
		<tr><td height=10></td></tr>
		<tr height=2><td bgcolor="#993300" ></td></tr>
	  </table>			 
			  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
				<tr> 
				  <td colspan=5> 
					 <table width="100%" border="0" class="titprincipal"  cellpadding="0" cellspacing="0" class="table">
					   <tr><td height=20></td></tr>
					   <tr> 
						<td width=19%></td>
						<td width=67% >
						  <p>
						      <label>
                            <input type="radio" class="input " name="NotificacoesRadios" value="1" onclick="javascript:valida('FormNotifBranco','0',this.form);">
                              <strong>NOTIFICA��O DE AUTUA��O</strong></label>
					          <strong><br>
					          <label>
					        <input type="radio" class="input" name="NotificacoesRadios" value="2" onclick="javascript:valida('FormNotifBranco','1',this.form);">
					        AVISO DE AUTUA��O</label>
					          <br>
					          <label>
					        <input type="radio" class="input" name="NotificacoesRadios" value="3" onclick="javascript:valida('FormNotifBranco','2',this.form);">
					        NOTIFICA��O DE PENALIDADE</label>
					          <br>
					          <label>
					        <input type="radio" class="input" name="NotificacoesRadios"  value="4" onclick="javascript:valida('FormNotifBranco','3',this.form);">
      						AVISO DE PENALIDADE</label>
					          <br>
					          <label>
					        <input type="radio" class="input" name="NotificacoesRadios"  value="10" onclick="javascript:valida('FormNotifBranco','4',this.form);">
					        NOTIFICA��O DE AUTUA��O ADMIMINISTRATIVA</label>
					          <br>
					          <label>
					        <input type="radio" class="input" name="NotificacoesRadios"  value="11" onclick="javascript:valida('FormNotifBranco','5',this.form);">
					        NOTIFICA��O DE PENALIDADE ADMINISTRATIVA</label>
							</strong></p>
						  </form></td>
						<td width=14%></td>
					   </tr>	
					</table>					
				  </td>
				</tr>			
								
	<tr> 
	</td>
	</tr>
	</table>
</div>
<div style="position:absolute; left:66px; bottom:98px; width:687px; height:30px; z-index:2; overflow: visible; visibility: visible;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr height=2><td bgcolor="#993300" ></td></tr>
	</table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>