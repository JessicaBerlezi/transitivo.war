<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page errorPage="ErrorPage.jsp" %>
<!--parte E2-->
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<script language="JavaScript1.2">

function detalheelements(){
//muda todos os elementos com  id="detalhe" para "detalhe_el"
var detalhe_el=document.all.detalhe
//verifica se ha algum objeto com o  id=detalhe
if (detalhe_el!=''&&detalhe_el.length==null)
detalhe_el.style.fontSize='9px'
else{
//for para cada elemento com  id="detalhe"
for (i=0;i<detalhe_el.length;i++)
detalhe_el[i].style.fontSize='7px'
}
} 

function revertback(){
//muda todos os elementos com  id="detalhe" para "detalhe_el"
var detalhe_el=document.all.detalhe
//verifica se ha algum objeto com o  id=detalhe
if (detalhe_el!=''&&detalhe_el.length==null)
detalhe_el.style.fontSize='9px'
else{
//for para cada elemento com  id="detalhe"
for (i=0;i<detalhe_el.length;i++)
detalhe_el[i].style.fontSize='9px'
}
}
window.onbeforeprint=detalheelements
window.onafterprint=revertback

</script>
	  
<!--Linha-->

<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1"></td></tr>
</table>
<!--FIM Linha-->     
<table width="100%" border="0" cellpadding="0" cellspacing="3" bgcolor="#CCCCCC" class="semborda">
		
			<tr>
			  <td height="16" align="center" bgcolor="#CCCCCC"><strong class="fontmaior">OBSERVA&Ccedil;&Otilde;ES IMPORTANTES PARA A DEFESA DA AUTUA&Ccedil;&Atilde;O </strong></td>
  </tr>
			<tr>
			  <td style="font-size: 9px;">O prazo limite para protocolar a Defesa da Autua��o � at� 15 (quinze) dias a partir do recebimento desta notifica��o. Entregue ou envie esta DEFESA por correspond�ncia registrada, at� o prazo limite citado , para os Protocolos avan�ados da SSPTT, localizados na Avenida Visconde do Rio Branco s/n - Centro (Terminal Rodovi�rio Jo�o Goulart -Recep��o2),  Estrada Francisco da Cruz Nunes, 6666 - Piratininga, (Administra��o Regional de Piratininga) e na Av. Almirante Ari Parreiras, 21 - Icara�  (Administra��o Regional de Icara�), todas em Niter�i - RJ.<br>
			    <br><strong>1. DOCUMENTOS NECESS&Aacute;RIOS</strong> <br>
C&oacute;pia desta Notifica&ccedil;&atilde;o, C&oacute;pia do documento de identidade, C&oacute;pia do Certificado de Registro e Licenciamento de Ve&iacute;culo, C&oacute;pia do Contrato Social (Pessoa Jur&iacute;dica) e outros documentos que julgar necess&aacute;rio para sua Defesa da Autua&ccedil;&atilde;o.<br>
<strong><span class="textored"><strong></strong></span><span class="textored"><strong></strong></span><span class="textored"><strong></strong></span>2. ATUALIZA&Ccedil;&Atilde;O DE ENDERE&Ccedil;O</strong><br>
Mantenha atualizados seus dados cadastrais. A Notifica&ccedil;&atilde;o devolvida pela desatualiza&ccedil;&atilde;o do endere&ccedil;o do propriet&aacute;rio do ve&iacute;culo ser&aacute; considerada v&aacute;lida para todos efeitos legais (Lei 9503/97 - Artigo 282 - Par&aacute;grafo 1&ordm; - CTB).</td></tr>
	
</table>
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
        <tr><td height="225"></td></tr>
</table>	  
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_linha_top">
    <tr> 
      <td align="center" height="30"  class="fontmaior">Endere&ccedil;o 
        para Defesa Pr&eacute;via e Recurso:<br>
		<%= NotifBeanId.getTxtEnderecoRecurso() %></td>
    </tr>
</table>
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1" bgcolor="#000000"></td></tr>
</table>

<!--fim parte E2-->