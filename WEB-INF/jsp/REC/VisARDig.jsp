<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<!-- Chama o Objeto da Tabela de consulta -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
 	case 'R':
	    	close() ;
			break;
	case 'VisARDigImagem' :
		fForm.acao.value=opcao
		fForm.target= "_blank";
		fForm.action = "acessoTool";  
		fForm.submit();
		break;	
 }
}

function mostraImagem(mypage, myname, w, h, scroll) {		
	var winl = (screen.width - w) / 2;
	//var wint = (screen.height - h) / 2;
	var wint=0;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
	win = window.open(mypage, myname, winprops)
	if (parseInt(navigator.appVersion) >= 4) { 
		win.window.focus(); 
		win.window.print();
	}
}

function changeColorOver(obj) {
	obj.style.background="#f0dbdb";
}

function changeColorOut(obj) {
	obj.style.background="#faeae5";
}

function callDetalhe(numAR) {
	var fForm = document.VisARDig;			
	fForm.numNotificacao.value=numAR;
	valida('VisARDigImagem',fForm);
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="VisARDig" method="post" action="">
<input name="acao" type="hidden" value=' '>	
<input name="numAutoInfracao" type="hidden" value='<%=AutoInfracaoBeanId.getNumAutoInfracao() %>'>	
<input name="numPlaca" type="hidden" value='<%=AutoInfracaoBeanId.getNumPlaca() %>'>	
<input name="numProcesso" type="hidden" value='<%=AutoInfracaoBeanId.getNumProcesso() %>'>	
<input name="numNotificacao" type="hidden" value=''>	

<jsp:include page="Cab.jsp" flush="true" />

<!--IN�CIO_CORPO_sistema-->

<!--DIV FIXA-->
<div id="req1" style="position:absolute; left:50px; top:80px; width:738px; height:75px; z-index:5; overflow: auto; visibility: visible;"> 
<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Propriet&aacute;rio:</strong></td>
        <td colspan=2><%=AutoInfracaoBeanId.getProprietario().getNomResponsavel() %></td>
        <td><b>CONSULTA AR DIGITALIZADO</b></td>        
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td width="90"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Auto:</strong></td>
        <td width="108"><%=AutoInfracaoBeanId.getNumAutoInfracao() %></td>
        <td width="323"><strong>Placa :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumPlaca() %></td>
        <td width="199"><strong>Processo :</strong>&nbsp;<%=AutoInfracaoBeanId.getNumProcesso() %></td>
      </tr>
      <tr><td colspan=4 height="2"></td></tr>	  
      <tr bgcolor="#f5d6cc" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Data Infra&ccedil;&atilde;o :</strong></td>
        <td><%=AutoInfracaoBeanId.getDatInfracao() %></td>
        <td colspan=2><strong>Status do Auto :</strong>&nbsp;<%=AutoInfracaoBeanId.getNomStatus() %>
			&nbsp;<%=(!AutoInfracaoBeanId.getNomStatus().equals("") ? "("+AutoInfracaoBeanId.getDatStatus()+")" : "")%>
		</td>
      </tr>
      <tr> <td height="10"></td>  </tr>
  </table>    
</div>
<!--FIM DIV FIXA-->
<%   int tam   = AutoInfracaoBeanId.getARDigitalizado().length;
	
	if(tam<=0) { %>
	<div id="req2" style="position:absolute; left:50px; top:138px; width:738px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="240" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr><td height="45"></td></tr>
	  <tr><td bgcolor="#f5d6cc" align="center">&nbsp;&nbsp;<strong>AUTO DE INFRA��O SEM AR</strong></td> </tr>	
	</table>
	</div>	
	<%} else { %>

<div id="req_cab" style="position:absolute; left:50px; top:146px; width:738px; height:20px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
	<tr bgcolor="#f5d6cc">
		<td align="left" width="150"><strong>&nbsp;&nbsp;</strong><strong>Caixa</strong></td>
		<td align="left" width="150"><strong>&nbsp;&nbsp;</strong><strong>Lote</strong></td>		
		<td align="left" width="200"><strong>&nbsp;&nbsp;</strong><strong>N�m. Notifica��o</strong></td>		
		<td align="left"><strong>&nbsp;&nbsp;</strong><strong>Data Digitaliza��o</strong></td>
	</tr>
	<tr> 
    	<td height="2"></td>
    </tr>
    </table>
</div>
<div id="req2" style="position:absolute; left:50px; top:160px; width:938px; height:410px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
<%
	 for (int i=0; i<tam; i++) { %>
	 <tr bgcolor="#faeae5"  style="cursor:hand" name="line<%=i%>" id="line<%=i%>" onmouseover="javascript:changeColorOver(line<%=i%>);" onmouseout="javascript:changeColorOut(line<%=i%>);" onclick="javascript:callDetalhe('<%=AutoInfracaoBeanId.getARDigitalizado(i).getNumNotificacao()%>');">
		 <td align="left" width="150"><strong>&nbsp;</strong>
		 	<%=AutoInfracaoBeanId.getARDigitalizado(i).getNumCaixa()%>
		 </td>	 
		 <td align="left" width="150"><strong>&nbsp;</strong>
		 	<%=AutoInfracaoBeanId.getARDigitalizado(i).getNumLote()%>
		 </td>
		 <td align="left" width="200"><strong>&nbsp;</strong>
		 	<%=AutoInfracaoBeanId.getARDigitalizado(i).getNumNotificacao()%>
		 </td>
		 <td align="left"><strong>&nbsp;</strong>
		 	<%=AutoInfracaoBeanId.getARDigitalizado(i).getDataDigitalizacaoString()%>
		 </td>
	 </tr>
	 <tr> 
      	<td height="2"></td>
    </tr>
    <%if(AutoInfracaoBeanId.getARDigitalizado(i) != null){%>
    <tr>
    <div id="imagem" align="center" style="position:absolute; left:164px; top: 49px; width:580px; height: 422px;  z-index:99; background-color: transparent; overflow:auto; visibility: visible;">
<%--   	 <img id="imagemFoto" alt="Foto do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=AutoInfracaoBeanId.getARDigitalizado(i).getNomFoto()%>&contexto=<%=AutoInfracaoBeanId.getARDigitalizado(i).getParametro(ParamSistemaBeanId)%>&header=#" width="570" height="412"> 	 --%>
	 <img id="imagemFoto" alt="Foto do Auto de Infra��o: <%=AutoInfracaoBeanId.getNumAutoInfracao()%>" style="background-color:#E7E7E7"  src="<%= path %>/FotoDigitalizada/<%=AutoInfracaoBeanId.getNumAutoInfracao()%>.jpg" width="570" height="412">
	
	</div>
    </tr>
 	<%}%>  
  <%} //for %>
  
	</table>	
</div>
<%}//fim do else  %>

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= AutoInfracaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>