<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!--Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de Instrucoes -->
<jsp:useBean id="FeriadoId" scope="request" class="REC.FeriadoBean" />
 
<jsp:useBean id="MunicipioId" scope="request" class="TAB.MunicipioBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<% String UF = ParamSistemaBeanId.getParamSist("UF_FERIADO");
	String select = "nom_municipio,SELECT cod_municipio,nom_municipio FROM TSMI_MUNICIPIO where cod_uf = '"+UF+"' ORDER BY nom_municipio";%>

<!-- Carrega e monta a combo-->
<jsp:setProperty name="MunicipioId" property="j_abrevSist" value="REC" />  	 	     	 	  
<jsp:setProperty name="MunicipioId" property="colunaValue" value="cod_municipio" />  
<jsp:setProperty name="MunicipioId" property="popupNome"   value="codMunicipio"  />  
<jsp:setProperty name="MunicipioId" property="popupString" value="<%=select%>"/>                 	 

<html>
<head>    
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

var bProcessando=false;

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaMunicipio':
	    if(bProcessando)
   		{
   			alert("Opera��o em Processamento, por favor aguarde...");
   			return;
   		}
   		else
   		{
	   		bProcessando=true;
	   		
	        fForm.dscMun.value = fForm.codMunicipio.options[fForm.codMunicipio.selectedIndex].text;   		
	   		fForm.codigomunicipio.value = fForm.codMunicipio.value;

	   		if (fForm.codMunicipio.value==0)
	   		{
				alert("Municipio n�o selecionado. \n")
			}
			else 
			{	
				fForm.acao.value=opcao
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
			}		
		}
	  	break ; 
   case 'A':  
   		if(bProcessando)
   		{
   			alert("Opera��o em Processamento, por favor aguarde...");
   		}
   		else
   		{
	   		bProcessando=true;
	   		
	   		if (veCampos(fForm)==true) 
	   		{ 	 
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	  	
		   	}
		   	else alert(sErro);	   
	   	}
	 	break ;	  
	
	case 'I': 
		if(bProcessando)
   		{
   			alert("Opera��o em Processamento, por favor aguarde...");
   		}
   		else
   		{
	   		bProcessando=true; 	 
	   		
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		} 	   
	 	break ;	  
     case 'R':
     	document.all.func.value = '<%=UsuarioFuncBeanId.getJ_sigFuncao()%>'

		if (document.all.func.value !="REC0994")
		{
			close();
			break;
		}
     
	    if (fForm.atualizarDependente.value=="S") 
	    {
			fForm.atualizarDependente.value="N"	; 
			fForm.dscMun.value =''; 
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	  		  
		}
	    else
	    {			  
		    close() ;  
		}		
		break;
   
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;   
  }
}
function veCampos(fForm) {
valid   = true ;
sErro = "" ;
fForm.func.value = '<%=UsuarioFuncBeanId.getJ_sigFuncao()%>'

if (fForm.func.value =="REC0994"){
	// verifica se o popup foi selecionado (value do popupNome = )
		if (fForm.dscMun.value==0) {
		   valid = false
		   sErro = sErro + "Munic�pio n�o Selecionado"
		}
}	
// fim de validar Input Descricao
	
for (k=0;k<fForm.txtFeriado.length;k++) {
   var ne = trim(fForm.txtFeriado[k].value)
   var de = trim(fForm.datFeriado[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.txtFeriado.length;m++) {
	   var dup = trim(fForm.txtFeriado[m].value)
	   var dupdat = trim(fForm.datFeriado[m].value)
	   if ((m!=k) && (dup.length!=0) && ((dup==ne) || (dupdat==de)) ) {
		   sErro += "Feriado em duplicata !!!: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;		   
		   fForm.datFeriado[m].focus();
		   break;			   
	   }
	}
}

// fim de validar campo em branco
for (k=0;k<fForm.txtFeriado.length;k++) {
   var tf = trim(fForm.txtFeriado[k].value)
   var df = trim(fForm.datFeriado[k].value)     
   if ( ( (tf.length==0) && (df.length!=0) )||( (tf.length!=0) && (df.length==0) ) )
   {
   	sErro +="Campo obrigat�rio em branco";
   	valid = false ;
   	if((tf.length==0)){
   		fForm.txtFeriado[k].focus();
   	}
   	else{
   		fForm.datFeriado[k].focus();
   	}   	
   }
}

return valid ;
}

function ValDtFer(obj,forma,indice) {  
  if (trim(obj.value).length == 0) return true ;
  var sdia = "00" ;
  var smes = "00" ;
  var sano = "0000" ; 
  if ( (obj.value.indexOf("/")>=0) || (obj.value.indexOf("-")>=0) || (obj.value.indexOf(":")>=0) || (obj.value.indexOf(" ")>=0) ){  
	  sdia = SeparaDt(obj,0) ;
	  smes = SeparaDt(obj,sdia.length+1) ;
	  sano = SeparaDt(obj,sdia.length+1+smes.length+1) ;  
  }
  else {
	  a = trim(obj.value) ;
	  if (trim(obj.value).length==4) {
	  	sdia = "0"+a.substring(0,1)
	  	smes = "0"+a.substring(1,2)
	  	sano = a.substring(2,4)				
	  }    
	  if (trim(obj.value).length==6) {
	  	sdia = a.substring(0,2)
	  	smes = a.substring(2,4)
	  	sano = a.substring(4,6)				
	  }    
	  if (trim(obj.value).length==8) {
	  	sdia = a.substring(0,2)
	  	smes = a.substring(2,4)
	  	sano = a.substring(4,8)				
	  }    	  
  }
  if (sdia.length > 2)  sdia = sdia.substring(0,2) ;
  if (sdia.length < 2)  sdia = "0"+sdia ;
  if (smes.length > 2)  smes = smes.substring(0,2) ;
  if (smes.length < 2)  smes = "0"+smes ;
  if (sano.length > 4)  sano = sano.substring(0,4) ;
  if (sano.length == 3) sano = "0"+sano ;
  if (sano.length == 1)  sano = "0"+sano ;   
  if (sano.length == 2) {
  	if (sano < 20) sano = "20"+sano ;
    else           sano = "19"+sano ;
  }
  var semErro = true;
  // validar datas
  if ((smes < 1) || (smes > 12)) semErro = false;
  if (((sano%4) == 0) && (smes == 02) && ((sdia < 01) || (sdia > 29))) semErro = false;
  if ((sano%4) != 0 && (smes == 02) && ((sdia < 01) || (sdia > 28)))   semErro = false;
  if (((smes == 04) || (smes == 06) || (smes == 09) || (smes == 11)) && ((sdia < 01) || (sdia > 30))) semErro = false;
  if (sdia > 31) semErro = false; 
  if (sano < 1900) semErro = false;   
  if (semErro) {
      obj.value = sdia+'/'+smes+'/'+sano ; 
  }
  else   {
  	  if (forma==0) {
	  	  alert("Data invalida: "+obj.value)
	  	  setTimeout("do_Selection(document." + obj.form.name + ". " + obj.name +"[" + (indice) + "])", 0)	  		 
  	  }
	  else {
	  	sErro = sErro + "Data invalida: "+obj.value+" \n"
	  }
  }
  return semErro ;
}

function do_Selection(fld) {	
	fld.value=""
	fld.select()
    return;
}

function SeparaDt(obj,ini) {
  spedaco = "" ; 
  x = obj.value
  for (i=ini; i < x.length; i++) {
      ch = x.charAt(i);
      if (ch == '/' || ch == ':' || ch == ' ' || ch == '-')  break
	  spedaco += ch	 
  }
  return spedaco
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="FeriadoForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="func" type="hidden" value=' '>	
<input name="sobrescreve" type="hidden" value=''>			
<input name="guardaMunicipio" type="hidden" value='<%=FeriadoId.getCodMunicipio()%>'>
<input name="atualizarDependente" type="hidden" size="1" value="<%= FeriadoId.getAtualizarDependente() %>"> 
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 61px; left=35px;height: 111px; left: 50px;" > 
  
  <table border="0" cellpadding="0" cellspacing="1" width=720  align="center">    
      <tr> 
       	<td valign="top" colspan="3"><img src="images/inv.gif" width="3" height="15">
       	</td>
      </tr>
	  <tr> 
	  	<td>	 
		  <% if  ("REC0994".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %> 	  
		  <b>Munic�pio:</b>&nbsp;<input disabled name="codMun" type="text" size="30" value="<%= FeriadoId.getNomMunicipio() %>" style= "border: 0px none;" >	  
	  	</td>
	  	  <%}%>
		<td></td>
		<td></td> 
		<td></td>
	  </tr>     
      <tr>
		  <td width="40%">
		   <% if  ("REC0994".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ 
		    
   			  if (FeriadoId.getAtualizarDependente().equals("S")==false){ %>				  				
				<jsp:getProperty name="MunicipioId" property="popupString" />     
			<% }
			else { %>
				
		        <input name="nommunicipio" type="hidden" size="10" maxlength="20"  value="<%= FeriadoId.getNomMunicipio() %>"> 
		        
			<% } 
			}%>	   
		 </td>
   		  <td width="6%">
   		    
   		  </td>
   		  
   		  <%  if ("REC0994".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ 
   		  			if (FeriadoId.getAtualizarDependente().equals("S")==false){ %>			  
 	         			<td align=left >
					  		<button type="button" NAME="Ok" style="width: 28px; height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaMunicipio',this.form);">	
			    	    	<IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left" ></button>
					 	</td>
	       	 
			 <%}%> 
			       <%}%>
      </tr>	
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
  
<!--INICIO BOTOES--> 
	<td><tr height="2">
	</tr></td> 	
  <TABLE id = "tabbotoes" border="0" cellpadding="0" cellspacing="7" width="300" align="center">
    <TR>
		<% if (FeriadoId.getAtualizarDependente().equals("S")){ %>			
	 	   <td width=100 align="center"> 
                  <button type=button NAME="Atualizar" align="center" style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align=left  ></button>
     	   </td>    
     	   <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td> 	   
	       <td  width=100 align="center"> 
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('R',this.form);">	
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align=left  ></button>
    	   </td>
	       
           </td>
		<%} %>
    </TR>

  </TABLE>  
<!--FIM BOTOES-->  
</div>
  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 141px; height: 32px;"> 
    <table width="720" border="0"  align="center" cellpadding="0" cellspacing="1">  	   		
	<tr bgcolor="#993300"> 
        <td width="40"  align="center" bgcolor="#be7272"><font color="#ffffff"><b>Linha</b></font></TD>		 	  
        <td width="120" align="center" bgcolor="#be7272"><font color="#ffffff"><b>Data</b></font></TD>		 
        <td width="556" align="center" bgcolor="#be7272"><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Descri��o</b></font></TD>		 
      </tr>
   </table>
<!--FIM CABEC DA TABELA-->
</div>
  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 157px; height: 191px;"> 
    <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
      <% 
					if (FeriadoId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = FeriadoId.getFeriados().size() ;
					   if (ocor<=0) ocor=FeriadoId.getFeriados(10,5).size() ;
					   
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="#faeae5">
        <td width="40" align="center"> 
          <input name="seqLinha" type="text" readonly size="3" value="<%= i+1 %>">		
		  <input name="codmunFeriado" type="hidden" size="8" value="<%=FeriadoId.getFeriados(i).getCodMunFeriado()%>">	        
        </td>  
	    <td width="120" align="center">           
       	  <input type="text" name="datFeriado" size="13" maxlength="10" value="<%=FeriadoId.getFeriados(i).getDatFeriado()%>"  onChange="javascript:ValDtFer(this,0,<%=i%>);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">			  
		  
		</td>
        <td align="center"> 
          <input name="txtFeriado" type="text" size="98" maxlength="170"  value="<%=FeriadoId.getFeriados(i).getTxtFeriado()%>" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase();" >		</td>
      </tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	
  		<% if  (!"REC0994".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %>
  		
  		<tr bgcolor="#f5d6cc">  	  		
			<td width="40" align="center" bgcolor="#FAEAE5"> 
	          <input name="seqlinha"  readonly  type="text" size="3" value="<%= i+1 %>">       
		      <input name="codmunFeriado" type="hidden" size="8" value="">	        
		   	</td>
		   	<td width="110" align="center" bgcolor="#FAEAE5" > 
	        	<input name="datFeriado"  type="text" size="13" maxlength="10"  value="" onChange="javascript:ValDtFer(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">
	        </td>				   	
		   	<td align="center"> 
	          <input name="txtFeriado"  type="text" size="90" maxlength="170"  value="" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase();"> 
            </td>                 	
	    </tr>	    
	   <% }else{%>   		
  		<tr bgcolor="#f5d6cc">   	
			<td width="40" align="center" bgcolor="#FAEAE5"> 
	          <input name="seqlinha"  readonly  type="text" size="3" value="<%= i+1 %>">       
		      <input name="codmunFeriado" type="hidden" size="8" value="">	        
		   	<td width="120" align="center" bgcolor="#FAEAE5" > 
	        <input name="datFeriado" readonly type="text" size="13" maxlength="1"  value=""></td>				   	
		   	<td align="center"> 
	          <input name="txtFeriado" readonly type="text" size="98" maxlength="170"  value="">        </td>			    	
	    </tr>	  				   
<%			}
		}					
					}   %>
  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= FeriadoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<input name="dscMun" type="hidden" size="10" maxlength="20"  value= "<%=FeriadoId.getNomMunicipio()%>">
<input name="codigomunicipio" type="hidden" size="10" maxlength="40"  value= "<%=FeriadoId.getCodMunicipio()%>"> 
<input name="tipoFeriado" type="hidden" size="10" maxlength="40"  value='M'> 
</form>
</BODY> 
</HTML>


   