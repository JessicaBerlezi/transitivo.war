<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<html>
<head>

<!--Folha de estilo-->
<jsp:include page="Css.jsp" flush="true" />
<!--FIM Folha de estilo-->

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight);

<!-- Valida os campos do form -->
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;

  <%if (request.getParameter("j_sigFuncao").equals("REC0745")) {%>	
	verString(fForm.numAutoInfracao, "Auto de Infra��o", 0);	
  <%} else if (request.getParameter("j_sigFuncao").equals("REC0750")) {%>	
	verString(fForm.numAutoInfracao, "Auto de Infra��o", 0);	
  <%} else if (request.getParameter("j_sigFuncao").equals("REC0755")) {%>
	if ((trim(fForm.numAutoInfracao.value) == '') && (trim(fForm.numNotificacao.value) == '')) {
		sErro += "Nenhuma op��o de busca informada. \n"
		valid = false;	
	}
  <%}%>
		
	if (valid == false) alert(sErro)
	return valid ;
}

<!-- Executa o comando do bot�o -->
function valida(Opcao, fForm) {
	switch (Opcao) {
	case 'R': //Fechar
		close();
		break;
	case 'V': //Mostar os erros
		document.all["MsgErro"].style.visibility='hidden';
		break;				
	case 'O': //Esconder os erros
		document.all["MsgErro"].style.visibility='visible';
		break;		
		
	case 'ARDigitalizado': //Visualizar AR
		if (veCampos(fForm) == true) {	
			fForm.acao.value = Opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";
			fForm.submit();
		}
		break;
		
	case 'AIDigitalizado': //Visualizar AI
		if (veCampos(fForm) == true) {	
			fForm.acao.value = Opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";
			fForm.submit();
		}
		break;
		
	case 'FotoDigitalizada': //Visualizar Foto
		if (veCampos(fForm) == true) {	
			fForm.acao.value = Opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";
			fForm.submit();
		}
		break;
	}		
}

</SCRIPT>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">

<form name="Form" method="post">
	<input name="acao" type="hidden" value="">
	
<!--Cabe�alho-->
<jsp:include page="Cab.jsp" flush="true" />
<!--FIM Cabe�alho-->

<!--Campos de entrada-->
<div id="recurso1" style="position:absolute; left:204px; top:130px; width:550px; height:120px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr> 
      <td width="25%" height=60 align=right>N&ordm; DO AUTO&nbsp;:&nbsp;&nbsp;
      </td>
      <td width="25%">
        <input name="numAutoInfracao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:Mascaras(this,'A99999999999')"  onKeyUp="this.value=this.value.toUpperCase()" size="20" maxlength="12" width="100%">
      </td>
      <%if (request.getParameter("j_sigFuncao").equals("REC0755")) {%>
      <td width="25%" height=60 align=right>N&ordm; DA NOTIFICA��O&nbsp;:&nbsp;&nbsp;
      </td>
      <td width="25%">
        <input name="numNotificacao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:Mascaras(this,'999999999')"  onKeyUp="this.value=this.value.toUpperCase()" size="20" maxlength="9" width="100%">
      </td>
      <%}%>
    </tr>
  </table>
  <table width="100%"  border="0" cellpadding="0" cellspacing="0" id="espaco" >
  <tr><td height="10">
  </td></tr>
  </table>  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr>
    <%if (request.getParameter("j_sigFuncao").equals("REC0745")) {%>
      <td align="center"><button style="border: 0px; background-color: transparent; height: 28px; width: 78px; cursor: hand;" type="button" onClick="javascript: valida('FotoDigitalizada',this.form);">
        <img src="<%= path %>/images/bot_Foto.gif" alt="Visualizar Foto Digitalizada" width="77" height="26">
      </button></td>
    <%} else if (request.getParameter("j_sigFuncao").equals("REC0750")) {%>
      <td align="center"><button style="border: 0px; background-color: transparent; height: 21px; width: 88px; cursor: hand;" type="button" onClick="javascript: valida('AIDigitalizado',this.form);">
        <img src="<%= path %>/images/bot_Auto_dig.gif" alt="Visualizar Auto Digitalizado" width="87" height="19">
      </button></td>
    <%} else if (request.getParameter("j_sigFuncao").equals("REC0755")) {%>
      <td align="center"><button style="border: 0px; background-color: transparent; height: 21px; width: 88px; cursor: hand;" type="button" onClick="javascript: valida('ARDigitalizado',this.form);">
        <img src="<%= path %>/images/bot_AR_dig.gif" alt="Visualizar AR Digitalizado" width="87" height="19">
      </button></td>	  
    <%}%>
     </tr>
  </table>
</div>
<!--FIM Campos de entrada-->


<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</BODY>
</HTML>
