<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.List"%>
<%@ page import="REC.AutoInfracaoBean"%>
<%@ page import="REC.GuiaDistribuicaoBean"%>
<%@ page import="sys.Util"%>
<% String path=request.getContextPath(); %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="REC.GuiaDistribuicaoBean" /> 
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="myOrg"   scope="request" class="ACSS.OrgaoBean" /> 
<jsp:useBean id="paramOrg"   scope="request" class="REC.ParamOrgBean" /> 
<% paramOrg.PreparaParam(UsuarioBeanId);%>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<SCRIPT LANGUAGE="JavaScript1.2">
window.print()

</script>

<html>
<head> 
<jsp:include page="Css.jsp" flush="true" />
<title>Publica&ccedil&atilde;o de Ata</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
}
-->
</style>
<body>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
  	<tr><td height=24 colspan="3"></td></tr> 
  	<% String layOutCabAta = (String)request.getAttribute("layOutCabAta");
  	  if(layOutCabAta.equals("S")){ %>
	<tr>
	  <td width="185"></td>
  	  <td width="565" align="center"><strong><span class="style3"><%= myOrg.getNomOrgao()%></strong></span></td>
	  <td width="200"></td>
  	</tr>	
  	<% }else { %> 
  	<tr>
	  <td width="185"></td>
  	  <td width="565" align="center"><strong><span class="style3"><%= paramOrg.getParamOrgao("NOME_EMPRESA_ATA","","3")%></strong></span></td>
	  <td width="200"></td>
  	</tr>	
  	<% } %>
	<tr>
	  <td width="185"></td>
	  <td align="center"><span class="style3"><strong><%=paramOrg.getParamOrgao("NOME_DEPARTAMENTO","","3")%></strong></span></td>
	  <td width="200"></td>
	</tr>
	<tr>
	  <td width="185"></td>
	  <td align="center"><span class="style3"><strong><%=paramOrg.getParamOrgao("NOME_JARI","","3")%></strong></span></td>
	  <td width="200"></td>
	</tr>
  </table>
  	<%	List guias = GuiaDistribuicaoId.getAutos();
		List autos = null;
		AutoInfracaoBean myAuto = null;
		GuiaDistribuicaoBean myGuia = null;
		int contador = 0,npag = 2;
		String cor = null;
		String msg = null;
		String sAtaAnt="",sGuiaAnt="";
		String datResultado = "";
		String junta = "";
		String tituloRelator = "";
		String singPlural = null;
		for(int x=0;x<guias.size();x++){
			myGuia = (GuiaDistribuicaoBean)guias.get(x);
			autos = myGuia.getAutos();
			singPlural = "";
			if ( (sAtaAnt.equals(myGuia.getSigJunta())==false) || (sGuiaAnt.equals(myGuia.getNumGuia())==false)){
			  sAtaAnt=myGuia.getSigJunta();
			  sGuiaAnt=myGuia.getNumGuia();
			  if(!junta.equals(myGuia.getSigJunta())){
%>
	 <table>
	 <tr>
		<td width="200"></td>
	 <%				junta = myGuia.getSigJunta();	 
	 				// Loop criado para verificar a necessidade de se acrescentar a letra 's' ap�s a palavra ATA.
	 				for(int i=x+1;(i<guias.size())&&(junta.equals(((GuiaDistribuicaoBean)guias.get(i)).getSigJunta()));i++){
	 					if( (!((GuiaDistribuicaoBean)guias.get(x)).getDatRS().equals(((GuiaDistribuicaoBean)guias.get(i)).getDatRS())) || (!((GuiaDistribuicaoBean)guias.get(x)).getNomTituloRelator().equals(((GuiaDistribuicaoBean)guias.get(i)).getNomTituloRelator())) )
	 						singPlural = "S";
	 				}
	 				String number = "";
	 				if(myGuia.getSigJunta().indexOf(' ')>0)
	 				   number = myGuia.getSigJunta().substring(0,myGuia.getSigJunta().indexOf(' '));
	 				else   
	 				   number = myGuia.getSigJunta().substring(0,myGuia.getSigJunta().length());
	 				if(Util.isNumber(number)){%>
		<td align="center" width="565"><span class="style3"><strong>ATA<%=singPlural%> DA <%=number + "�" +myGuia.getSigJunta().substring(myGuia.getSigJunta().indexOf(' '),myGuia.getSigJunta().length())%></strong></span></td>
		<%			}else{%>
			<td align="center" width="567"><span class="style3"><strong>ATA<%=singPlural%> DA <%=myGuia.getSigJunta()%></strong></span></td>
		<%			}%>
		<td width="200"></td>
	 </tr>
	 </table>
	 <table>
	 <%				datResultado = "";	
	 				tituloRelator = "";
	 			}
	 			if( (!datResultado.equals(myGuia.getDatRS())) || (!myGuia.getNomTituloRelator().equals(tituloRelator)) ){
	 				singPlural = "";
	 	 			// Acrescenta a letra 's' ao cabe�alho caso exista mais de um processo na ATA.
	 				if(autos.size()>1)
	 					singPlural = "s";
	 				if(guias.size()>x+1){
	 					if( (((GuiaDistribuicaoBean)guias.get(x+1)).getDatRS().equals(myGuia.getDatRS())) && (myGuia.getNomTituloRelator().equals(((GuiaDistribuicaoBean)guias.get(x+1)).getNomTituloRelator())) )
	 						singPlural = "s";
	 				}
	 %>
     <tr>   
	 	<td width="166"></td>     
 		<td colspan="7"><span class="style3">Ata julgada em <%=myGuia.getDatRS().substring(0,2)%>.<%=myGuia.getDatRS().substring(3,5)%>.<%=myGuia.getDatRS().substring(6,10)%>, atrav&eacute;s da CI n&ordm;&nbsp;<%=GuiaDistribuicaoId.getCodEnvioDO()%>.</span></td>
 	 </tr>

	<tr>
	  <td width="166"></td>
	  <td colspan="7"><span class="style3">Processo<%=singPlural%> distribu&iacute;do<%=singPlural%> ao Sr. <%=myGuia.getNomTituloRelator()%>:</span></td>
	</tr>
	<tr> 
	  <td width="170"></td>
      <td colspan="2" width="204" align="center"><span class="style3">Processo<%=singPlural%> n�<%=singPlural%></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      <td colspan="2" width="164" align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="style3">Requerimento<%=singPlural%> n�<%=singPlural%></span></td>
	  <td width="18"><span class="style3">Resultado<%=singPlural%></span></td>
	  <td width="212"></td>
	</tr>
	<%				datResultado = myGuia.getDatRS();
					tituloRelator = myGuia.getNomTituloRelator();
			   	}
			}
			for(int y=0;y<autos.size();y++){
				myAuto = (AutoInfracaoBean)autos.get(y);
				msg = myAuto.getMsgErro();
				if(msg.length()==0)
					cor = "#FFFFFF";
				else
					cor = "#CCCCCC";
	%>
  <tr bgcolor='<%=cor%>'>
  	<td width="170" style="line-height:12px "></td>
   	<%	if(myAuto.getNumProcesso()!=null){%>
	   	<%	if(myAuto.getNumProcesso().substring(0,3).equals("E09")){%>
		<td width="200" style="line-height:12px "><span class="style3">E-<%=myAuto.getNumProcesso().substring(1)%></span></td>
		<%	}%>
	<%}else{%>	
  	<td width="200" style="line-height:12px "><span class="style3"><%=myAuto.getNumProcesso()%></span></td>
	<%	}%>
	<td width="4" style="line-height:12px "></td>
 	<td width="138" style="line-height:12px "><span class="style3"><%=myAuto.getNumRequerimento()%></span></td>
	<td width="26" style="line-height:12px "></td>
 	<td width="18" style="line-height:12px "><span class="style3"><%=myAuto.getRequerimentos(0).getNomResultRS()%></span></td>
	<td width="212" style="line-height:12px "></td>
	<%			if(msg.length()!=0){
					contador++;      %>
	<tr bgcolor='<%=cor%>'> 
		<td colspan="8"><b><span class="style3"><%= msg %></span></b></td>
	</tr>
	<%}%>
  </tr>
<%			}
		}
	%>
</table>

<!--fim cab impress�o-->
</body>
</html>