<!--M�dulo : SYS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath();%>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />

<html>
<head>
<title>.    Transitivo    .</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--CSS-->
<jsp:include page="Css.jsp" flush="true" />		
<!--FIM_CSS-->
<script LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></script>
<script LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight);

function fechar()
{
 var acao=self.screenTop; 
 if (acao > 9000)
  { valida('ES',loginForm); }
 else { return; }
}
window.onunload=fechar;

function valida(opcao,fForm) {
  loginForm.j_sigFuncao.value=""	;	
  loginForm.j_jspOrigem.value="/sys/login.jsp" ;  
  switch (opcao) {
   case 'F':
    loginForm.target= "_self";
	loginForm.j_cmdFuncao.value="encerrar";
    loginForm.action = "acessoTool";
   	loginForm.submit();
	break;
   case 'ES':
    loginForm.target= "_blank";
	loginForm.j_cmdFuncao.value="encerrar";
    loginForm.action = "acessoTool";  
   	loginForm.submit();
	break;
  } 
}

function MudaEstado(estado,fForm) {
  loginForm.j_sigFuncao.value="" ;	
  loginForm.j_jspOrigem.value="/sys/login.jsp" ;  
  loginForm.target= "_self";
  loginForm.action = "acessoTool";
  loginForm.codUf.value = estado;
  loginForm.submit();
}

function j_ajuda(fForm) {
	fForm.j_jspOrigem.value="/sys/login.jsp"	;	
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
}
</script>

</head>
<form name= "loginForm" method="post">
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td align="left" background="images/APRES_BACK1.png" valign="top"><img src="images/login_fundo.png" width="800" height="1024"></td>
  </tr>
</table>
<!--Fim Fundo da tela-->
<!--Cabe�alho do login-->
<div id="Cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:66px; z-index:12;"> 
  <table width="100%" height="100" border="0" cellpadding="0" cellspacing="0">
    <tr> 
      <td width="560" height="40" valign="middle"><img src="images/inv.gif" width="560" height="3"></td>
      <td width="157" valign="middle"><font color="#FFFFFF"> <%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%>	
        </font></td>
      <td valign="middle" class="table"> <div align="right"><font color="#FFFFFF">&nbsp;</font></div></td>
      <td width="46">
	    <div align="left"> 
          <button type="button" style="height: 35px; width: 36px; border: none; background: transparent; cursor: hand;" name="j_Ajuda"  onClick="javascript: j_ajuda(this.form);" > 
          <img src="<%= path %>/images/detran_help_menu.png" alt="Ajuda do sistema" width="34" height="32" border="0"></button>
        </div>
	  </td>
    </tr>
    <tr>
      <td height="17" valign="middle">&nbsp;</td>
      <td valign="middle">&nbsp;</td>
      <td valign="middle" class="table">&nbsp;</td>
      <td>
	    <div align="left"> 
          <!--button type="button" style="height: 35px; width: 36px; border: none; background: transparent; cursor: hand;" name="j_Ajuda"  onClick="javascript: j_ajuda(this.form);" --> 
          <A HREF="<%= path %>/apresentacao/sys/inicial_apr.jsp"><img src="<%= path %>/images/bot_apresent_ico.png" alt="Apresenta��o Transitivo" width="34" height="32" border="0"></A><!--/button-->
        </div>      
	  </td>
    </tr>
  </table>
</div>
<!--FIM Cabe�alho do login -->


<!--texto mapa -->
<div id="icones" style="position:absolute; left:453px; top:295px; right: 0px; width: 134px; height:80px; z-index:50; overflow: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr> 
      <td height="60" align="center" valign="top" style="font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px;"><div align="left">Escolha um <br>
        estado no mapa<br> 
        ao lado, clicando<br> 
      na UF correspondente: </div></td>
    </tr>
  </table>
</div>
<!--FIM texto mapa -->

<!-- mapa brasil-->
<div id="mapaBrasil" style="position:absolute; left:449px; top:107px; right: 0px; width: 330px; height:80px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table">
    <tr>
      <td width="330" height="35" colspan=2> <strong><font color="#CC3300"><img src="images/im_mapabrasil.gif" width="335" height="305" border="0" usemap="#mapaBrasil" ></font></strong>
        <map name="mapaBrasil" id="mapaBrasil">
          <area shape="rect" coords="61,68,92,84" href="#" alt="Amazonas" >
          <area shape="rect" coords="15,115,43,131" href="#" alt="Acre" >
          <area shape="rect" coords="67,131,95,146" href="#" alt="Rond�nia" >
          <area shape="rect" coords="89,18,116,36" href="#" alt="Roraima" >
          <area shape="rect" coords="169,25,195,42" href="#" alt="Amap�" >
          <area shape="rect" coords="225,219,251,234" href="#" alt="Rio de Janeiro" onClick="javascript: MudaEstado('RJ',this.form);">
          <area shape="rect" coords="184,116,213,132" href="#" alt="Tocantins" >
          <area shape="rect" coords="208,75,237,90" href="#" alt="Maranh�o" >
          <area shape="rect" coords="255,69,280,84" href="#" alt="Cear�" >
          <area shape="rect" coords="285,76,307,89" href="#" alt="Rio Grande do Norte" >
          <area shape="rect" coords="302,92,326,103" href="#" alt="Para�ba" >
          <area shape="rect" coords="300,108,323,118" href="#" alt="Pernambuco" >
          <area shape="rect" coords="291,122,311,133" href="#" alt="Alagoas" >
          <area shape="rect" coords="280,137,299,148" href="#" alt="Sergipe" onClick="javascript: MudaEstado('SE',this.form);">
          <area shape="rect" coords="200,157,220,170" href="#" alt="Distrito Federal" >
          <area shape="rect" coords="237,137,262,151" href="#" alt="Bahia" >
          <area shape="rect" coords="250,196,272,209" href="#" alt="Esp�rito Santo" >
          <area shape="rect" coords="149,74,176,88" href="#" alt="Par�" >
          <area shape="rect" coords="128,140,157,156" href="#" alt="Mato Grosso" >
          <area shape="rect" coords="167,167,196,183" href="#" alt="Goi�s" >
          <area shape="rect" coords="137,191,165,206" href="#" alt="Mato Grosso do Sul" >
          <area shape="rect" coords="175,205,203,221" href="#" alt="S�o Paulo" >
          <area shape="rect" coords="210,185,241,200" href="#" alt="Minas Gerais" >
          <area shape="rect" coords="155,232,182,246" href="#" alt="Paran�" >
          <area shape="rect" coords="174,253,200,268" href="#" alt="Santa Catarina" >
          <area shape="rect" coords="145,266,170,284" href="#" alt="Rio Grande do Sul" >
          <area shape="rect" coords="230,99,253,114" href="#" alt="Piau�" >
        </map> </td>
	</tr>
  </table>
</div>
<!-- fim mapa brasil-->


<!-- Link Controle de Acesso -->
<div id="controleAcesso" style="position:absolute; left:241px; top:405px; right: 0px; width: 280px; height:30px; z-index:10; overflow: visible;"> 
<input name="acao"          type="hidden" value="">
<input name="codUf"         type="hidden" value="">
<input name="j_cmdFuncao"   type="hidden" value="<%=UsuarioFuncBeanId.getJ_cmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=UsuarioFuncBeanId.getJ_sigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=UsuarioFuncBeanId.getJ_jspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="ACSS">	
<input name="sigOrgaoAtuacao"   type="hidden" value="">					
<input name = "quantErro" type="hidden" value="<%= (String)request.getAttribute("quantErro") %>">
<input name = "loginBloq" type="hidden" value="<%= (String)request.getAttribute("loginBloq") %>">
<input name="tempo_sessao" type="hidden" value="<%=(String)request.getAttribute("tempo_sessao")%>">					
</div>
<!--FIM Link Controle de Acesso -->

<!--Bot�o encerrar sistema--> 
	<div id="encerrar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	  <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
			<tr><td height="26">
			  <button style="border: 0px; background-color: transparent; height: 25px; width: 29px; cursor: hand;" type="button" onClick="javascript: valida('F',this.form);">
			  <img src="images/sys/bot_encerrar_br.gif" alt="Encerrar o Sistema Transitivo" width="24" height="24"></button></td>
			</tr>
	  </table>
	</div>
<!--FIM Bot�o encerrar sistema-->

</body>
</form>
</html>
