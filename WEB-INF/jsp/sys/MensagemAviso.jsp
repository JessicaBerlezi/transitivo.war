<!--M�dulo : SYS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="NoticiaId" scope="session" class="ACSS.NoticiaBean" /> 
<jsp:useBean id="AvisoId"   scope="session" class="ACSS.AvisoBean" /> 

<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 

<%	
    String str="";
    str = UsuarioBeanId.prioridade( UsuarioFuncBeanId );

    ACSS.NoticiaBean[] noticias;
    noticias = UsuarioBeanId.getNoticias();

    ACSS.AvisoBean[] avisos;
    avisos = UsuarioBeanId.getAvisos();
%>

<script language="JavaScript1.2">

function valida_cab(opcao,codigo_cab) {
 switch (opcao) {
   case 'mostraNoticia':
	    document.forms[0].j_cmdFuncao.value = "InformacoesCmd";
	    document.forms[0].acaoNA.value = "mostraNoticia";
	    document.forms[0].codigo_cab.value = codigo_cab;
	    document.forms[0].target= "_blank";
	    document.forms[0].action = "acessoTool";
	    document.forms[0].submit();
	  break ;
   case 'mostraAviso':
		document.forms[0].j_cmdFuncao.value = "InformacoesCmd";
	    document.forms[0].acaoNA.value = "mostraAviso";
	    document.forms[0].codigo_cab.value=codigo_cab;
		document.forms[0].target= "_blank";
	    document.forms[0].action = "acessoTool";
	   	document.forms[0].submit();
	  break ;
   }
}

function mostra(LAYER) {
   if (document.layers)  document.layers[LAYER].visibility='show' ;
   else document.all[LAYER].style.visibility='visible';
 return;
}
function esconde(LAYER) {
   if (document.layers) document.layers[LAYER].visibility='hide';
   else  document.all[LAYER].style.visibility='hidden';
 return;
}

</script>

<input name="codigo_cab"    type="hidden" value="">
<input name="acaoNA"        type="hidden" value="">

<!--------inicio do bloco---------->
<div id="POPUP" style="position: absolute; width: 510px; height: 136px; overflow: visible; border-style: none; background-color: #ffffff; z-index: 100; left: 183px; top: 300px; visibility: hidden;">      	
<table width="100%" height="136" border="1" cellpadding=0 cellspacing=0 >
 <tr bgcolor="#e7e7e7"> 
  	<td>      
	<font color="#CC0000"><b><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>A T E N &Ccedil; &Atilde; O ! ! !&nbsp;&nbsp;&nbsp;&nbsp;H &Aacute;&nbsp;&nbsp;&nbsp;&nbsp;A V I S O S&nbsp;&nbsp;&nbsp;&nbsp;E&nbsp;&nbsp;&nbsp;&nbsp;N O T &Iacute; C I A S&nbsp;&nbsp;&nbsp;&nbsp;U R G E N T E S ! </b></font></td>
  </tr>
  <tr bgcolor="#e7e7e7"><td></td>
  </tr>
  <tr bgcolor="#e7e7e7"> 
    <td height="68">
      <div align="center">
        <div id="Layer1" style="position: relative;  width:510; height:80px; z-index:1; overflow: auto;"> 
	      <table width="100%" height="90%" border=0 align="center" cellpadding=0 cellspacing=0 bordercolor="#000000" bgcolor="#e7e7e7">

			<%   if(avisos!=null) {%>
            <tr bgcolor="#e7e7e7">
              <td bgcolor="#CCCCCC"><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A&nbsp; v&nbsp; i&nbsp; s&nbsp; o&nbsp; s </strong>&nbsp;&nbsp;&nbsp;( Para ler um aviso � s� clicar num item abaixo! ) </td>
            </tr>
			<%   for(int i=0;i<avisos.length;i++) {	%>
			         <tr bgcolor="#e7e7e7" style="cursor: hand;" onClick="javascript:valida_cab('mostraAviso','<%=avisos[i].getCodAviso()%>');" >
			           <td style="font-family: verdana; font-size: 10px; font-weight:  color: #000000; letter-spacing: 1px; " onMouseOver="this.style.color='#cc0000';" onMouseOut="this.style.color='#000000';">&nbsp;&nbsp;&nbsp;&nbsp;<%=avisos[i].getDscAviso()%></td>
			         </tr>
			<%   }
			   }
			%>
			<tr bgcolor="#e7e7e7">
			    <td align=center bgcolor="#e7e7e7"></td>
            </tr>
			<%if(noticias!=null) { %>
            <tr bgcolor="#e7e7e7">
              <td bgcolor="#CCCCCC"><strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;N&nbsp; o&nbsp; t&nbsp; &iacute;&nbsp; c&nbsp; i&nbsp; a&nbsp; s </strong> &nbsp;&nbsp;&nbsp;( Para ler uma not�cia � s� clicar num item abaixo! )</td>
            </tr>
			<%   for(int i=0;i<noticias.length;i++) { %>
					<tr bgcolor="#e7e7e7" valign="top" style="cursor: hand;" onClick="javascript:valida_cab('mostraNoticia','<%=noticias[i].getCodNoticia()%>');"> 
			   			<td style="font-family: verdana; font-size: 10px; font-weight: color: #000000; letter-spacing: 1px; " onMouseOver="this.style.color='#cc0000';" onMouseOut="this.style.color='#000000';">&nbsp;&nbsp;&nbsp;&nbsp;<%=noticias[i].getDscNoticia()%> </td>
					</tr>
			<%   }
			   }
			%>
			<tr bgcolor="#e7e7e7">
			   <td align=center bgcolor="#e7e7e7"></td>
			</tr>
          </table>
        </div>
    </div></td>
  </tr>
  <tr bgcolor="#e7e7e7"> 
    <td>             
	<table width="100%" border=0  bordercolor="#000000" cellpadding=0 cellspacing=0>
         <tr bgcolor="#e7e7e7">
		    <td height="20" align="center" bgcolor="#e7e7e7">
		        <button type=button NAME="Ok"   style="height: 23px; width: 44px;border: none; background: transparent;" onClick="esconde('POPUP');">		      		&nbsp;<IMG src="<%= path %>/images/ok_fundo_cinza.gif"  width="26" height="19"></button>
            </td>
   		 </tr> 
    </table>	
	</td>
  </tr>
</table> 
</div>
<!-----------fim do bloco---------->

<!--inicio do bloco-->
		<%if( RequisicaoBeanId.getCmdFuncao().compareTo("InformacoesCmd")!= 0 )
		   	if ( (str.compareTo("A")==0) ) {	%>
		       	<script> mostra('POPUP'); </script>
		<%	} else {%>
		      	<script> esconde('POPUP'); </script>
		<%	}%>
<!--fim do bloco-->