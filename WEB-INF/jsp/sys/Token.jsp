<!--M�dulo : SYS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>  
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistemaBeanId"      scope="session" class="ACSS.SistemaBean" />  

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
 
   case 'ok':
		fForm.acao.value="continuar";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();
}

}
  
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">

<% if (SistemaBeanId.getNomAbrev().equals("ACSS")){ %>	
	<jsp:include page="../ACSS/Cab.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("REC")){ %>
	<jsp:include page="../REC/Cab.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("REG")){ %>
	<jsp:include page="../REG/Cab.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("sys")){ %>
	<jsp:include page="../sys/Cab.jsp" flush="true" />
<% }

else if (SistemaBeanId.getNomAbrev().equals("TAB")){ %>
	<jsp:include page="../TAB/Cab.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("TRA")){ %>
	<jsp:include page="../TRA/Cab.jsp" flush="true" />
<% }
else { %>
	<jsp:include page="../ACSS/Cab.jsp" flush="true" />
<% } %>

<input name="acao"          type="hidden" value=' '>

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 150px; left: 35px; right: 0px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td height="2"></td></tr>
      <tr>
      		
      		<td width="100%" align="center" style="font-size: 12px;" ><p><strong>A opera��o ainda est� em execu��o.</p><p>Pressione o bot�o para continuar.</p></td></tr>
      <tr>
      <td height="25"></td>
      </tr>
      <tr>
      		
      		<td width="100%" align="center">
			  	<button type="button" NAME="Continua"   style="width: 59px;height: 19px;  border: none; background: transparent;"  onClick="javascript: valida('ok',this.form);">	
                <img src="<%= path %>/images/bot_continuar.gif" align="left" > </button>
			</td> 
      </tr>
	  
    </table>    
	<TABLE border="0" cellpadding="0" cellspacing="0" align = "center" width="100%">	 
    </table>
</div>
<!-- Rodap�-->
<% if (SistemaBeanId.getNomAbrev().equals("ACSS")){ %>	
	<jsp:include page="../ACSS/Rod.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("REC")){ %>
	<jsp:include page="../REC/Rod.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("REG")){ %>
	<jsp:include page="../REG/Rod.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("sys")){ %>
	<jsp:include page="../sys/Rod.jsp" flush="true" />
<% }

else if (SistemaBeanId.getNomAbrev().equals("TAB")){ %>
	<jsp:include page="../TAB/Rod.jsp" flush="true" />
<% }
else if (SistemaBeanId.getNomAbrev().equals("TRA")){ %>
	<jsp:include page="../TRA/Rod.jsp" flush="true" />
<% }
else { %>
	<jsp:include page="../ACSS/Rod.jsp" flush="true" />
<% } %>

<!-- Fim Rodap� -->	  
</form>
</BODY>
</HTML>

