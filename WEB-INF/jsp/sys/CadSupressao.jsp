<!--M�dulo : SYS
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 

<jsp:useBean id="supressaoBean" scope="request" class="sys.SupressaoBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">


function valida(opcao,fForm) {
 switch (opcao) {
   case 'atualizar':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
	   	close() ;   
	  break;
   case 'I':
	 fForm.acao.value=opcao;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();		   
	 break;

   case 'O':  // Esconder os erro
   		if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	    else                 document.all["MsgErro"].style.visibility='hidden' ; 
		 break;  
	case 'V':  // Ver os erros
   		if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	    else                 document.all["MsgErro"].style.visibility='visible' ; 
		 break; 
  }
} 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />

<input name="acao" type="hidden" value="">
  
<!--INICIO BOTOES-->  
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1;" > 
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
	 	   <td  width="33%" align="center"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('atualizar',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
     	   </td>

    </TR>

  </TABLE>
<!--FIM BOTOES-->  

<div style="top: 120px;width:400px; left:200px;" class="divtitulos"> 
  <table cellspacing="0" class="titulos">  	   		
	<tr> 
	    <td width="50">Reg.</td>
		<td>Supress�o</td>		 
 	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->

<div style="top: 138px;width:400px; left:200px;" class="divcorpo">
  <table cellspacing="0" class="corpo"> 
      <%	List supressoes = supressaoBean.getBeans();
      		int i;
      		for(i=0;i<supressoes.size();i++){
      
%>
      <tr>   
      	<input name="id" type="hidden"  maxlength="80" value="<%=((sys.SupressaoBean)supressoes.get(i)).getId()%>">
        <input name="descricao_aux" type="hidden" size="60" maxlength="50"  value="<%=((sys.SupressaoBean)supressoes.get(i)).getDesSupressao()%>" onChange="this.value=this.value.toUpperCase()">
        <td width="50">
        	<%=i+1
%>
        </td>
        <td>        
             <input name="descricao" type="text" size="60" maxlength="50"  value="<%=((sys.SupressaoBean)supressoes.get(i)).getDesSupressao()%>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
	</tr>
<% 	
		   }
		   if(i < 9){
		   	for(;i <= 9;i++){
 %>	
 			   <tr>   
      			<input name="id" type="hidden"  maxlength="80" value="">
        		<input name="descricao_aux" type="hidden" size="60" maxlength="50"  value="" onChange="this.value=this.value.toUpperCase()">
        		<td width="50">
        			<%=i+1%>
        		</td>
        		<td>        
             		<input name="descricao" type="text" size="60" maxlength="50"  value="" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        		</td>			
			  </tr>	
<%
			}
		   }
		   else{
		    int numLinhas = i + 5;
		   	for(;i < numLinhas;i++){
%>
 			   <tr> 
      			<input name="id" type="hidden"  maxlength="80" value="">
        		<input name="descricao_aux" type="hidden" size="60" maxlength="50"  value="" onChange="this.value=this.value.toUpperCase()">
        		<td width="50">
        			<%=i+1%>
        		</td> 
        		<td>        
             		<input name="descricao" type="text" size="60" maxlength="50"  value="" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        		</td>			
			  </tr>	
<%
			}
		   }
%>

  </table>
</div>
</div>
 <!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= supressaoBean.getMensagem() %>" />
  <jsp:param name="msgErroTop" value= "160 px" />
  <jsp:param name="msgErroLeft" value= "30 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="../ACSS/Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</BODY>
</HTML>


