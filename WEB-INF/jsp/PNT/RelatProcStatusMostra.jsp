<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsrLogado"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Perfil/Orgao/Atuacao -->
<jsp:useBean id="RelatProcStatusBean" scope="session" class="PNT.RelatProcStatusBean" /> 
<!-- Chama o Objeto da Tabela de STATUS -->
<jsp:useBean id="StatusId" scope="request" class="PNT.TAB.StatusBean" />
<jsp:setProperty name="StatusId" property="j_abrevSist" value="PNT" />  
<jsp:setProperty name="StatusId" property="colunaValue" value="COD_STATUS_AUTO" />  
<jsp:setProperty name="StatusId" property="popupNome"   value="codStatus"  />  
<jsp:setProperty name="StatusId" property="checked" value="<%=RelatProcStatusBean.getCodStatus()%>" />                 	 
<jsp:setProperty name="StatusId" property="popupString" value="NOM_STATUS,SELECT COD_STATUS_AUTO||' - '||NOM_STATUS_AUTO as NOM_STATUS,COD_STATUS_AUTO FROM TPNT_STATUS_PROCESSO ORDER BY COD_STATUS_AUTO ASC" />                 	 
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm,cod,nomStatus) {
 switch (opcao) {
   case 'ImprimeRelatorio':
	    fForm.acao.value=opcao
		fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ; 
   case 'detalhe':
	    document.RelatForm.nomStatus.value = nomStatus;
	    document.RelatForm.codStatus.value=cod
	    document.RelatForm.acao.value=opcao
		document.RelatForm.target= "_blank";
	    document.RelatForm.action = "acessoTool";  
	   	document.RelatForm .submit();	  		  
	  break ; 
   case 'GeraExcel':
	    document.RelatForm.nomStatus.value = nomStatus;
	    document.RelatForm.codStatus.value=cod
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "GeraXlsRelatProcStatus";  
		fForm.submit();  		  
	  break ;
	  
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<%@ include file="Cab.jsp" %> 

<input name="acao" type="hidden" value=' '>	
<input name="nomArquivo"  type="hidden" value='RelatProcStatus.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>
<input name="TodosStatus" type="hidden" value='<%= RelatProcStatusBean.getTodosStatus()%>'>	
<input name="dataIni" type="hidden" value='<%= RelatProcStatusBean.getDataIni()%>'>	
<input name="dataFim" type="hidden" value='<%= RelatProcStatusBean.getDataFim()%>'>	
<input name="codStatus" type="hidden" value='<%= RelatProcStatusBean.getCodStatus()%>'>	
<input name="nomStatus" type="hidden" value='<%= RelatProcStatusBean.getNomStatus() %>'>	


<div id="WK_SISTEMA" style="position:absolute; width:475px; overflow: visible; z-index: 1; top: 76px; left:205px;" >  
<!--INICIO CABEC DA TABELA-->

<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
	if 	(RelatProcStatusBean.getDados().size()>0){%>      	
  <table width="100%" cellpadding="0" cellspacing="1">
	<tr>
        <td align="left"><strong>Status :</strong>&nbsp; <%= "N".equals(RelatProcStatusBean.getTodosStatus()) ? RelatProcStatusBean.getNomStatus() : "todos" %></td>
	</tr>
	<tr>

        <td ><strong>Per�odo de :</strong>&nbsp;<%= RelatProcStatusBean.getDataIni()%>&nbsp; <strong>at�</strong> &nbsp;<%= RelatProcStatusBean.getDataFim()%></td>
        <td style="text-align: right">
        <button type="button" name="imprimir" style="width: 56px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimeRelatorio',this.form);"> <img src="<%= path %>/images/bot_imprimir_ico.gif" align="left" ></button>
        <button type="button" name="retornar" style="width: 56px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('R',this.form);"> <img src="<%= path %>/images/bot_retornar_ico.gif" align="left" ></button>
        </td>
    </tr>
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>


<div id="relat1" style="position:absolute; left:35px; width:745px; top:125px; height:55px; z-index:1; overflow:visible; visibility: visible;" class="divTitulos"> 
 <table width="100%" border="0" cellpadding="0" cellspacing="1" class="titulos">
    <tr> 
      <td width="85"  height="13" bgcolor="#c0bdbd" style="text-align: center">C�d.Status </td>
      <td width="410" bgcolor="#c0bdbd">Status</td>      
      <td width="100" bgcolor="#c0bdbd" style="padding-right: 42px; ; text-align: right">Total</td>
      <td bgcolor="#c0bdbd" style="padding-right: 42px; ; text-align: right">&nbsp;</td>
  </tr>
  </table>
</div>   

<div id="relat2" style="position:absolute; left:35px; width:745px; top:145px; height:185px; z-index:1; overflow: auto; visibility: visible;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
<%		 for (int i=0;i<RelatProcStatusBean.getDados().size();i++) {					
%>


<!--DADOS-->
    <tr> 
      <td height="25" width="93" style="text-align: center">     
      <a href="#"  onClick="javascript: valida('detalhe',this.form,'<%=RelatProcStatusBean.getDados().get(i).getCodStatus()%>','<%=RelatProcStatusBean.getDados().get(i).getNomStatus()%>');">
      <strong><font color="#000000"><%=RelatProcStatusBean.getDados().get(i).getCodStatus()%></font></strong></a>
      </td>
      
      
      <td width="350" style="padding-left: 70px"><a href="#"  onClick="javascript: valida('detalhe',this.form,'<%=RelatProcStatusBean.getDados().get(i).getCodStatus()%>','<%=RelatProcStatusBean.getDados().get(i).getNomStatus()%>');"><%=RelatProcStatusBean.getDados().get(i).getNomStatus()%></a></td>
      <td width="100" style="padding-right: 26px; text-align: right"><a href="#"  onClick="javascript: valida('detalhe',this.form,'<%=RelatProcStatusBean.getDados().get(i).getCodStatus()%>','<%=RelatProcStatusBean.getDados().get(i).getNomStatus()%>');"><%=RelatProcStatusBean.getDados().get(i).getQtdProcStatus()%></a></td>	
      <td style="padding-right: 26px; text-align: right">
			<button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form,'<%=RelatProcStatusBean.getDados().get(i).getCodStatus()%>','<%=RelatProcStatusBean.getDados().get(i).getNomStatus()%>');"> 	
				<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
			</button>
      </td>	
    </tr>
	  <tr> 
		<td bgcolor="#CCCCCC" height="1" colspan="3"></td>
	  </tr>

<!--FIM DADOS-->

<!--c�digos de retorno-->
 <!--fim dados fixos par todo os eventos-->
<%}
}%>
	</table>
</div>   

<!--Div Erros-->
<%
String msgErro = RelatProcStatusBean.getMsgErro();
String msgOk = "";
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 


<!--FIM_Div Erros-->
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->  
</form>
</BODY>
</HTML>

