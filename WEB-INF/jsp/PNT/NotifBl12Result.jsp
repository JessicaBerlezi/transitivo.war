<!--Bloco 12ResultDefesa - PNT
-->
<% String nomeFase = ( ("1".equals(notificacao.getTipoNotificacao())) ? "Defesa" : "Recurso"); %>
<table class="semborda" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td height="10"></td> 
</tr>
</table>	 
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
  <td height="565" align="center" valign="top" style="padding-left: 20px; padding-right:20px;"><p class="style1" style="margin-top: 10px;">AVISO DE RESULTADO DE <%= nomeFase.toUpperCase() %> </p>
    <p align="left" class="fontmaior" style="text-indent: 50px; line-height:50px;text-align:justify">Fica V.S.&ordf; Notificada de que a justificativa de <%= nomeFase %>, apresentada para o Processo de Suspens&atilde;o do Exerc�cio do Direito de Dirigir n&ordm; <%= ProcessoBeanId.getNumProcesso()%>, foi considerada &quot;DEFERIDA&quot; pela Junta Administrativa de Recursos de Infra&ccedil;&otilde;es, JARI, ficando V.S.&ordf; livre de todas as penalidades previstas nesse Processo, conforme Ata publicada em <%= notificacao.getDatPublDO() %> no Di&aacute;rio Oficial.<br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br />
&nbsp;<br />
&nbsp;<br />
&nbsp;</p>
    <p align="left" class="fontmaior" style="text-indent: 50px;">    </td>
</tr>
</table>
