<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>


<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 

<jsp:useBean id="StatusId" scope="request" class="PNT.TAB.StatusBean" />
<jsp:setProperty name="StatusId" property="j_abrevSist" value="PNT.AJT" />  	 	     	 	  
<jsp:setProperty name="StatusId" property="colunaValue" value="COD_STATUS_AUTO" />  
<jsp:setProperty name="StatusId" property="popupNome"   value="codStatus"  />  
<jsp:setProperty name="StatusId" property="checked" value="<%=ProcessoBeanId.getCodStatus()%>" />                 	 
<jsp:setProperty name="StatusId" property="popupString" value="NOM_STATUS,SELECT COD_STATUS_AUTO||' - '||NOM_STATUS_AUTO as NOM_STATUS,COD_STATUS_AUTO FROM TPNT_STATUS_PROCESSO ORDER BY COD_STATUS_AUTO ASC" />                 	 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	  case 'CancSuspProcesso':
	      		if (veCampos(fForm)==true) 
	      		{		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
		  		} 
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	
	valid = ValDt(fForm.Data,1) && valid
	
	if (atuRadio(fForm.indOperradio,fForm.IndicOper)) {
	  valid = false
	  sErro = sErro + "Tipo de Invalida��o n�o selecionado. \n"
	}
	
	verString(fForm.codMotivo,"C�digo do Motivo",0);
	verString(fForm.ProcInv,"N�mero do processo",0);
	verString(fForm.Data,"Data",0);
	if (valid==false) alert(sErro) 
	return valid ;
}


function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}


</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AjustaStatusForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao"     type="hidden" value=''>
<input name="IndicOper"type="hidden" value=' '>	

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
    	<td height="18" colspan="3"><strong><%= ProcessoBeanId.getMsgOk() %></strong></td>
      	<td>
    		<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('N',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>            
        </td>
	</tr>         
    <tr><td height="4" colspan="4"></td></tr>	
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>    
    <tr bgcolor="#d9f4ef"> 
       <td width="41%" valign="middle" align="left" height=17>
            Data da Invalida��o:&nbsp;
        <input type="text" name="Data" size="12" maxlength="10" value='<%=sys.Util.formatedToday().substring(0,10)%>' onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">       </td>
       <td width="49%">&nbsp;Tipo Invalida��o:&nbsp;<input type="radio" name="indOperradio" value="1" class="sem-borda" style="height: 14px; width: 14px;">Cancelamento &nbsp;&nbsp;
          <input type="radio" name="indOperradio" value="2" class="sem-borda" style="height: 14px; width: 14px;">Suspens�o&nbsp;&nbsp;        
       <td width="10%"></td>
	  	<td></td>
    </tr>
    <tr><td height="2" colspan="4" style="background-color: #ffffff"></td></tr>
    <tr bgcolor="#d9f4ef">
        <td width="59%" colspan=2 valign="middle" align="left" >Motivo:&nbsp;&nbsp;<input type="text" name="dscMotivo" size="80" maxlength="80" value= "" onFocus="javascript:this.select();"></td>
		<td width="30%" align="center" valign="middle"></td>
		<td></td>
	</tr>	     
    <tr><td height="2" colspan="4" style="background-color: #ffffff"></td></tr>
    <tr bgcolor="#d9f4ef">
        <td width="29%" valign="middle" align="left" >Cod.Motivo:&nbsp;&nbsp;<input type="text" name="codMotivo" size="3" maxlength="3" value= "" onKeyPress="javascript:f_num();" onFocus="javascript:this.select();"></td>
        <td width="30%" align="left" valign="middle"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Num.Processo:&nbsp;&nbsp;<input type="text" name="ProcInv" size="25" maxlength="20" value=""  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase();"></td>  
		<td width="30%" align="center" valign="middle"></td>
		<td>      
	    	<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('CancSuspProcesso',this.form);">			  
		  	<img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19">
			</button>
	  	</td>
	</tr>	     
		<% } %>
  </table>
</div>
<% if("S".equals(ProcessoBeanId.getEventoOK())) { %> 
	<script>document.AjustaStatusForm.Data.focus();</script>	        
<% } %>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro       = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</body>
</html>