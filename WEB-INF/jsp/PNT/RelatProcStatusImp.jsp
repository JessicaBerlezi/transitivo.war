<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath();
   String tituloConsulta = (String)request.getAttribute("tituloConsulta");
  	if(tituloConsulta==null) tituloConsulta="";    %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="RelatProcStatusBean" scope="session" class="PNT.RelatProcStatusBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<%@ include file="CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	if 	(RelatProcStatusBean.getDados().size()>0){      	
		 for (int i=0;i<RelatProcStatusBean.getDados().size();i++) {					
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if (contLinha>50){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {		%>
				       
						<%@ include file="rodape_imp.jsp" %>
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
	<%@ include file="Cab_imp.jsp" %>	    

 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="400"  height="18" class="fontmedia" style="color:#000000; font-weight: bold; text-align: left; vertical-align: top">&nbsp;&nbsp;<strong>Status :</strong>&nbsp; <%= "N".equals(RelatProcStatusBean.getTodosStatus()) ? RelatProcStatusBean.getNomStatus() : "todos" %></td>
  </tr>
  </table>
 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="94"  height="13" bgcolor="#c0bdbd" class="fontmedia table_linha_top" style="color:#ffffff; font-weight: bold; text-align: center">C�d.Status &nbsp;</td>
      <td width="450" valign="bottom" bgcolor="#c0bdbd" class="fontmedia td_linha_top_left" style="color:#ffffff; font-weight: bold;padding-left: 70px">Status</td>      
      <td bgcolor="#c0bdbd" class="fontmedia td_linha_top_left" style="color:#ffffff; font-weight: bold; padding-right: 26px; ; text-align: right">T o t a l</td>
  </tr>

		       <% } %>	 
<!--DADOS-->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td class="table_linha_top" height="25" width="94" style="text-align: center"><strong><%=RelatProcStatusBean.getDados().get(i).getCodStatus()%></strong> &nbsp;</td>     
      <td class="td_linha_top_left" width="450" style="padding-left: 70px"><%=RelatProcStatusBean.getDados().get(i).getNomStatus()%></td>
      <td class="fontmedia td_linha_top_left" style="padding-right: 26px; text-align: right"><%=RelatProcStatusBean.getDados().get(i).getQtdProcStatus()%></td>	
    </tr>
	</table>

<!--FIM DADOS-->

<!--c�digos de retorno-->
 <!--fim dados fixos par todo os eventos-->
<%}
}%>
<%@ include file="rodape_imp.jsp" %>
</form>
</body>
</html>