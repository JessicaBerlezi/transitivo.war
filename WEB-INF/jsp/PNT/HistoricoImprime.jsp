<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >

<form name="ConsultaHistImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
    boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	if 	(ProcessoBeanId.getHistoricos().size()>0){   		
      	 int a =ProcessoBeanId.getHistoricos().size()+1;     
		 for (int i=0;i<ProcessoBeanId.getHistoricos().size();i++) {
				a--;	
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if (contLinha>12){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {			
		%>
				        </table>      
						<%@ include file="rodape_imp.jsp" %>
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
	<%@ include file="HistoricoCabecalhoImp.jsp" %>	    
		       <% } %>	
    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr bgcolor="#c0bdbd"  height="18"> 
        <td >&nbsp;&nbsp;<strong><%=a%>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;Evento :&nbsp;<%= ProcessoBeanId.getHistoricos().get(i).getCodEvento() %></strong></td>
        <td >&nbsp;<strong><%= ProcessoBeanId.getHistoricos().get(i).getNomEvento() %></strong></td>
        <td ><strong>Origem:</strong></td>
        <td colspan="4" ><strong><%=ProcessoBeanId.getHistoricos().get(i).getNomOrigemEvento() %></strong>&nbsp;&nbsp;</td>
        
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr height="14"> 
        <td>
        	<strong>&nbsp;&nbsp;Status :</strong></td>
        <td colspan="3" >
        	&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getCodStatus() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getNomStatus() %>
        </td>
        <td ><strong>Data Status:</strong></td>
        <td ><%=ProcessoBeanId.getHistoricos().get(i).getDatStatus() %></td>
      </tr>
      <tr><td height="2"></td>  </tr>
      <tr  height="14"> 
        <td width="92" >&nbsp;&nbsp;<strong>Executado por :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getNomUserName() %></td>
        <td width="97" ><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
        <td width="93" ><%=ProcessoBeanId.getHistoricos().get(i).getSigOrgaoLotacao() %></td>
        <td width="85" ><strong>Data Proc.:</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getDatProc() %></td>
      </tr>
      <tr><td height="2"></td></tr>
   </table> 
   <!--fim dados fixos par todo os eventos-->
   
  

<% if ("800".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 800 - Instaura��o Processo Pontua��o  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Nome Arquivo :</strong></td>
        <td width="440"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="85"><strong>Data Arquivo :</strong></td>
      	<td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
    </table>   
<%} else if ("801".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 801 - Emiss�o de Notifica��o de Autua��o  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>Qtd. Seq�encias :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td width="85" ><strong>Data Gera��o :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>N� Processo :</strong></td>
        <td colspan="5">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>    
      </tr>
    </table>   
<%}else if ("803".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>Tipo de Ata :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="190"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td colspan="3"><strong>C�d. Envio D.O. :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      	
      </tr>
    </table>     
<%}else if ("804".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr height="16"> 
        <td width="190" colspan=4><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      	
      </tr>
    </table>      
<%}else if ("834".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr height="16"> 
        <td width="190" colspan=4><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
    </table>   
<%}else if ("894".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr height="16"> 
        <td width="190" colspan=4><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
    </table>      
<%}else if ("808".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 808 - Registrar AR ? REC/N�o REC  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>C�d.Retorno.:&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></strong></td>
        <td width="93"><strong>Entregue.:&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></strong></td> 
        <td width="85"><strong>Data Retorno :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>    	
      </tr>     
    </table>   
<%}else if ("810".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 810 - Abrir Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="440">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="85"><strong>Data :</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Respons�vel :</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>    
      	<td><strong>CPF:</strong></td>
      	<td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>E-mail :&nbsp;</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento12() %></td>   
      	<td><strong>Tipo:</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
         <td colspan="3">&nbsp;<textarea cols="85" rows="1" style="border: 0px none; overflow: visible; text-align: left; background: transparent;" readonly="readonly"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento07()%></textarea>
	     </td>
      </tr>
    </table>    
    --
    
<%}else if ("680".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 810 - Abrir Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo&nbsp;:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim()%> - <%if (ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim().equals("510")){%>Cancelamento por Decis�o Judicial<%}%> <%if (ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim().equals("512")){%>Cancelamento por Revis�o de Ato Administrativo<%}%>
	     </td>
      </tr>
      <tr   height="16"> 
      <td>&nbsp;&nbsp;<strong>Processo&nbsp;:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02()%></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Justificativa&nbsp;:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04()%></textarea>
	     </td>
      </tr>
      
      
    </table>    
    
    
    
    
    
    
    
    
<%}else if ("811".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 811 - Informar junta/Relator - Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td colspan="4"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>        
      </tr>      
      <tr   height="16"> 
		<td width="92" >&nbsp;&nbsp;<strong>Junta :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>
        <td width="97"><strong>C�d. Relator :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento05() %></td>
        <td width="85" ><strong>CPF Relator :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %></td>            
      </tr>
    </table> 
<%}else if ("812".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())||"842".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())|| "872".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ EVENTO: RESULTADO DE DEFESA PR�VIA =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr height="16">
      
        <td width="92" >&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97" ><strong>Resultado :</strong></td>
        <td width="93" >&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02()%></td>
        <td width="85" ><strong>Data :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>      
      </tr>
      <tr> <td colspan=6 height="2"></td> </tr>
      <tr height="16">
        <td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td colspan=5>
	         &nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %>
        </td>
      </tr>
      <tr height="16">
        <td>&nbsp;&nbsp;<strong>Penalidade:</strong></td>
        <td colspan=5>
	         &nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %>
        </td>
      </tr>

      
      
    </table>     
          
<%}else if ("815".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 815 - Cria��o do Processo F�sico  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="440" align="left">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="85"><strong>Tipo:</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>     
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Data :</strong></td>
        <td align="left">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td><strong>Data Proc F�sico:</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento08() %></td>
      </tr>     
    </table>  
<%}else if ("820".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 820 - Penalidade Registrada  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>Qtd. Seq�encias :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td width="85" ><strong>Ult. Dt. Rec :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr   height="16"> 
        <td>&nbsp;&nbsp;<strong>Penalidade :</strong></td>
        <td colspan = "5">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento05() %></td>          	
      </tr>
    </table> 
<%}else if ("911".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr   height="16"> 

        <td width="92" >&nbsp;&nbsp;<strong>Novo Status :</strong></td>
        <td width="250"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td colspan="3"><strong>Motivo:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04()%></td>
      </tr>
    </table> 
        
<% } %>

<table width="100%" class="semborda" ><tr><td height="2"></td></tr></table>
<% } %>
<%}else{%>  
<div id="mensagem" style="position:absolute; left:70px; top:85px; width:538px; height:35px; z-index:10; overflow: visible; visibility: visible;"> 
	<table width="119" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td bgcolor="#FFFFFF">&nbsp;&nbsp;<strong>PROCESSO SEM HIST�RICO</strong></td> </tr>	
	</table>
</div>          
<%}%>
</div>
<%@ include file="rodape_imp.jsp" %>
</form>
</body>
</html>