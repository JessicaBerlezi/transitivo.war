<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">

<form name="consultaReqImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	

 
<%
    boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;	
	if 	(ProcessoBeanId.getRequerimentos().size()>0){	
	  for (int i=0;i<ProcessoBeanId.getRequerimentos().size();i++) {				    
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if (contLinha>9){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {			
		%>
				        </table>      
						<%@ include file="rodape_imp.jsp" %>
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
	<%@ include file="RequerimentoCabecalhoImp.jsp" %>	    
		       <% }%>
		       
	<%
	if ((ProcessoBeanId.getRequerimentos().get(i).getCodTipoSolic().equals("")==false))  {  %>
 
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	  <tr><td height="5"></td></tr>
      <tr bgcolor="#cccccc" height="18"> 
        <td width="300" bgcolor="#cccccc"><strong>&nbsp;N&ordm;&nbsp;REQUERIMENTO :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNumRequerimento()%></td>
        <td width="270" bgcolor="#cccccc">&nbsp;<strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomTipoSolic()%></strong></td>
        <td align="right" bgcolor="#cccccc"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatRequerimento()%>&nbsp;&nbsp;</td>
      </tr>
      <tr><td height="2"></td></tr>	  
    </table>
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">      
      <tr><td height="2"></td></tr>
      <tr  height="8"> 
      	<td width="80"><strong>&nbsp;Aberto por :&nbsp;</strong></td>
      	<td width="156">&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameDP() %></td>
        <td><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoDP()%></td>
		<td align="right"><strong><%=ProcessoBeanId.getRequerimentos().get(i).getNomStatusRequerimento()%></strong></td>
      </tr>
	    
      <% String guia="";
      	if(guia.length()>0) { %>
	      <tr ><td height="2"></td></tr>
    	  <tr height="8"> 
      		<td ><strong>&nbsp;Guia Distrib.:</strong>&nbsp;</td>
	        <td ><%=guia%></td>
    	    <td ></td>
			<td ></td>
      	  </tr>  
      <% } %> 
	  
  </table> 
	<% 
   } if ((ProcessoBeanId.getRequerimentos().get(i).getCodRelatorJU().equals("")==false)) {	     
  %>  
	<!--Requerimento JUNTA-->
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	  <tr><td height="2" colspan=3></td></tr>
      <tr  height="8">       
        <td width="70">&nbsp;<strong>Junta :&nbsp;</strong></td>
		<td width="100" ALIGN="left">&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getSigJuntaJU()%></td>
        <td width="238"><strong>Relator :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomRelatorJU()%></td>			
        <td><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatJU() %>&nbsp;&nbsp;</td>
      </tr>
	  <tr><td height="2"></td></tr>
      <tr  height="8"> 
      	<td width="80"><strong>&nbsp;Executado por :</strong>&nbsp;</td>
        <td width="138">&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameJU() %></td>
        <td width="93"><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoJU()%></td>
      	<td><strong>Data de Proc.:</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatProcJU()%>&nbsp;&nbsp;</td>
      </tr>
	      
   </table> 
  <% } //IF JUNTA
   if  ((ProcessoBeanId.getRequerimentos().get(i).getCodResultRS().equals("")==false))  {
       
  %>
	<!--Requerimento RESULTADO-->
    <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
	  <tr><td height="2"></td></tr>
      <tr  height="8"> 
        <td width="300">&nbsp;<strong>Resultado :&nbsp;</strong></td>
        <td width="138"><%= ProcessoBeanId.getRequerimentos().get(i).getNomResultRS() %></td>
        <td width="200"><strong>Data Resultado :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatRS() %></td>
      </tr>
      <tr  height="8"> 
        <td>&nbsp;<strong>C�d.Penalidade :</strong></td>
        <td colspan="2"></td>
        <td><strong>Prazo de Suspens�o :</strong>&nbsp;</td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr  height="8"> 
	     <td>&nbsp;<strong>Motivo :</strong></td>
	     <td colspan="3">
		     <textarea cols="100" rows="1" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" readonly="readonly"><%=ProcessoBeanId.getRequerimentos().get(i).getTxtMotivoRS() %></textarea>
	     </td>
      </tr>
      <tr><td height="2"></td></tr>	  
      <tr  height="8"> 
      	<td width="100">&nbsp;<strong>Executado por :</strong>&nbsp;</td>
        <td width="138" ><%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameRS() %></td>
        <td width="93" ><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoRS()%></td>
      	<td><strong>Data de Proc.:</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatProcRS()%>&nbsp;&nbsp;</td>
      </tr>    
   </table>

<%			} //IF RESULTADO
} //Loop do while 
	}else	{
%>
	<table width="240" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr><td height="45"></td></tr>
	  <tr><td bgcolor="#c0bdbd" align="center">&nbsp;&nbsp;<strong>AUTO SEM REQUERIMENTO</strong></td> </tr>	
	</table>
<%	}	%>
</table>      
</div>
<%@ include file="rodape_imp.jsp" %>
</form>
</body>
</html>