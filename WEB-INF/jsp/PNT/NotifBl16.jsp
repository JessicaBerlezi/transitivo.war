<!--Bloco 16 - PNT
-->	
<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
        <tr><td height="7"></td></tr>
</table>	  
 
<table width="100%" border="0" cellpadding="2" cellspacing="0">
  <tr>
    <td height="45" align="center" valign="top" class="fontmedia"> 
      		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        		<tr> 
		  			<td width="9%" align="center"> 
            			<table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
              				<tr>              				 
			    			<td><img src="<%= path %>/images/logo.gif" width="65" height="60" ></td>  							
							</tr>
			        	</table>
				    </td>
				    <td width="66%" valign="top" class="fontmaior"> 
			          <table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
						<tr>
			  				<td class="fontmaior"><strong>DEPARTAMENTO DE TR�NSITO DO ESTADO DO RIO DE JANEIRO </strong>  </td>
			  			</tr>
	  					<tr>
		 					<td class="fontmedia"><STRONG>DETRAN-RJ</strong></td>
			  			</tr> 
    					<tr>
		 					<td class="fontmedia"><STRONG><%= notificacao.getTipoNotificacaoDescNotif().toUpperCase()  %></strong>	</td>
						</tr>
					  </table>	 	
		          </td>
        		  <td width="25%" align="center"> 
		            <table width="100%" height="60" border="0" cellpadding="1" cellspacing="0" class="semborda">
            		  <tr> 
		                <td width="35%">N&ordm; Processo:</td>
        		        <td width="65%"><span class="fontmedia"><strong><%=ProcessoBeanId.getNumProcesso()%></strong></span></td>
					  </tr>
			          <tr> 
				          <td><%=ProcessoBeanId.getTipCNHDesc()%>:</td>
				          <td><span class="fontmedia"><strong><%=ProcessoBeanId.getNumCNH()%>/<%=ProcessoBeanId.getUfCNH()%></strong></span></td>
				      </tr>
				      <tr>
				          <td> </td>
				          <td><span class="fontmedia"><strong></strong></span></td>
				      </tr>
					</table>
		          </td>
			   </tr>
			</table>
		    <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr><td height="1" bgcolor="#000000"></td></tr>
			</table>
			<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
				<tr valign="top"> 
				    <td width="25%" height="30" align="center"></td>
				    <td width="40%" align="center"><font size="2">&nbsp;</font></td>
				    <td width="35%" align="center"><font size="2">Expedi&ccedil;&atilde;o<br>					    
	                  <%=notificacao.getDatEmissao()%>                
                 	</font></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td>
				    <td height="35" colspan="2" class="fontmaior"><%=ProcessoBeanId.getNomResponsavel()%></td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td> 
				    <td height="35" colspan="2" class="fontmaior"><%=ProcessoBeanId.getEndereco()%>        
					    </td>
				</tr>
				<tr valign="top">
				    <td width="33%" height="35"></td> 
				    <td height="35" class="fontmaior"><%=ProcessoBeanId.getCepEdt()%></td>
				    <td align="left" class="fontmaior"><%=ProcessoBeanId.getMunicipio()%>&nbsp;/&nbsp;<%=ProcessoBeanId.getUf()%></td>
				</tr>
			</table>
			<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				 <tr><td height="125"></td></tr>
			</table>
	  </td>
	</tr>
</table> 
