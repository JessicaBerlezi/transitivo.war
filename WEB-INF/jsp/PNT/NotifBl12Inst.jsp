<!--Bloco 12Inst - PNT
-->



<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
  <td height="565" align="center" valign="top" style="padding-left: 20px; padding-right:20px;">

 <div class="fontmedia" style="margin-bottom: 0px; margin-top: 0px; text-align:left;text-indent: 0px; line-height: 17px; font-weight:bold">Notifica&ccedil;&atilde;o n&ordm; <%=notificacao.getNumNotificacao() %>-<%=notificacao.getSeqNotificacao() %></div>
  
 <div class="fontmedia" style="margin-bottom: 10px; margin-top: 10px; text-align:center;text-indent: 0px; line-height: 11px; font-weight:bold">
  <img src="<%= path %>/images/logo.gif" width="65" height="60" align="top" ><br />

  GOVERNO DO RIO DE JANEIRO<br />
SECRETARIA DE ESTADO DA CASA CIVIL<br />
DEPARTAMENTO DE TRÂNSITO DO ESTADO DO RIO DE JANEIRO
  </div> 
  
  <div style="font-size: 11px;margin-bottom: 30px; margin-top: 30px; text-align:center;text-indent: 0px; line-height: 16px; font-weight:bold">INSTAURA&Ccedil;&Atilde;O E  NOTIFICA&Ccedil;&Atilde;O DE PROCESSO TENDENTE &Agrave; SUSPENS&Atilde;O DO EXERC&Iacute;CIO DO DIREITO DE DIRIGIR</div>
  
    <div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">O <strong>Departamento  de Tr&acirc;nsito do Estado do Rio de Janeiro &ndash; DETRAN/RJ</strong>, em conformidade com as  compet&ecirc;ncias estabelecidas pela Lei Federal n&ordm;. 9503/97 (C&oacute;digo de Tr&acirc;nsito  Brasileiro &ndash; CTB);</div>
	
    <div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">CONSIDERANDO o artigo 261, &sect; 1&deg; do  CTB e a Resolu&ccedil;&atilde;o CONTRAN N&deg; 182, de 09  de setembro de 2005, que disp&otilde;em sobre a aplica&ccedil;&atilde;o da penalidade  de suspens&atilde;o do exerc&iacute;cio do direito de dirigir ao infrator que atingir a  contagem de vinte pontos, no per&iacute;odo de doze meses;</div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">CONSIDERANDO  que os artigos 7&ordm;  e 9&ordm; da Resolu&ccedil;&atilde;o  CONTRAN N&deg; 182/05  determinam a instaura&ccedil;&atilde;o de Processo Administrativo tendente &agrave; aplica&ccedil;&atilde;o da  penalidade de suspens&atilde;o do direito de dirigir quando a soma dos pontos  relativos &agrave;s infra&ccedil;&otilde;es de tr&acirc;nsito cometidas, atingir no per&iacute;odo de doze meses,  vinte pontos.</div>
    
    <div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
    	<strong>INSTAURA e  NOTIFICA </strong>o  condutor <strong><%=ProcessoBeanId.getNomResponsavel() %>, </strong>CNH <strong><%=ProcessoBeanId.getTipCNHDesc() %> <%=ProcessoBeanId.getNumCNH() %>/<%=ProcessoBeanId.getUfCNH() %></strong>para dar ci&ecirc;ncia da instaura&ccedil;&atilde;o de Processo  Administrativo n&deg; <strong><%=ProcessoBeanId.getNumProcesso() %></strong>, visando &agrave; aplica&ccedil;&atilde;o da <strong>penalidade  de suspens&atilde;o do exerc&iacute;cio do direito de dirigir</strong>, em face de haver em seu  prontu&aacute;rio <strong><%=ProcessoBeanId.getTotPontos() %></strong> <strong>pontos</strong> em infra&ccedil;&otilde;es de tr&acirc;nsito  ocorridas <strong>no per&iacute;odo de 12 meses</strong>, conforme detalhado  adiante.
    	<!--<strong>infra&ccedil;&otilde;es de tr&acirc;nsito ocorridas <strong>no per&iacute;odo de 12 meses</strong>.-->
    </div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">Fica  assegurado ao condutor o exerc&iacute;cio do direito fundamental ao contradit&oacute;rio e &agrave;  ampla defesa, extra&iacute;do do artigo 5 &deg;, inciso LV&nbsp;  da CRFB, podendo V.S&ordf; apresentar suas Raz&otilde;es de Defesa,&nbsp; por escrito, <strong>no prazo ininterrupto e  improrrog&aacute;vel de quinze dias, contados da data de recebimento desta Notifica&ccedil;&atilde;o</strong>,  no Protocolo Geral do Edif&iacute;cio Sede do Detran-RJ ou remetida via correio, em  carta registrada ao Detran-RJ, com endere&ccedil;o na Avenida Presidente Vargas n.  817, sobreloja - Centro &ndash; Rio de Janeiro, Cep. 20071-004, podendo ainda, ser  entregue nas CIRETRANS.</div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
		Segue a  notifica&ccedil;&atilde;o n&ordm; <strong><%= notificacao.getNumNotificacao() %>-001</strong> com modelo  de requerimento de defesa pr&eacute;via &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Resolu&ccedil;&atilde;o  CONTRAN N&ordm; 182/2005 &ndash; artigo 11). 
	</div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">As defesas e os recursos n&atilde;o ser&atilde;o conhecidos quando  interpostos fora do prazo ou por quem n&atilde;o seja parte leg&iacute;tima de acordo com o  artigo 25 da Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182/2005.</div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">Finalmente,  comunica-se ao condutor que, no caso do n&atilde;o acolhimento de suas raz&otilde;es de  defesa pr&eacute;via ou de seu exerc&iacute;cio fora do prazo legal, ser&aacute; aplicada a  penalidade de suspens&atilde;o do exerc&iacute;cio do direito de dirigir, prevista no artigo  261, do C&oacute;digo de Tr&acirc;nsito Brasileiro e artigo 3&ordm;, inciso I da Res. CONTRAN N&ordm;  182/05, cabendo da decis&atilde;o condenat&oacute;ria, recurso administrativo a uma das JARIs  &ndash; Junta Administrativa de Recursos de Infra&ccedil;&otilde;es &ndash; do DETRAN/RJ, e, da decis&atilde;o  da JARI, caso desfavor&aacute;vel ao condutor, recurso ao CETRAN.</div>
	
	<div class="fontmaior" style="margin-bottom: 10px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px"><%=Util.dataPorExtenso()%></div>
	
	<div style="margin-bottom: 0px; margin-top: 0px; text-align:center"><img src="<%= path %>/images/assinaturadigital.JPG" width="180" height="73" ></div>
	
	<div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px; text-align: center"> <strong>&nbsp;</strong><br />
      <span class="fontmedia" style="font-weight:bold">Presidente - DETRAN/RJ</span></div>
	  <br />
	  <br />
	
  </td>
</tr>
</table>
