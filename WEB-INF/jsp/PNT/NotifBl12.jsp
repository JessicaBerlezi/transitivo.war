<!--Bloco 12 - PNT
-->
<table class="semborda" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
<td height="10"></td>
</tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
<td height="150" align="center" valign="top"><p class="style1" style="margin-top: 10px;">NOTIFICA&Ccedil;&Atilde;O DE INSTAURA&Ccedil;&Atilde;O DE PROCESSO TENDENTE &Agrave; SUSPENS&Atilde;O DO EXERC&Iacute;CIO  DO DIREITO DE DIRIGIR</p>
<p align="left" class="fontmaior" style="text-indent: 50px;">

Fica  V.S&ordf; notificada da instaura&ccedil;&atilde;o do Processo Administrativo <strong><%= ProcessoBeanId.getNumProcesso()%></strong> de Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir,  em face do cometimento da(s) infra&ccedil;&atilde;o (&otilde;es) abaixo descrita(s), conforme previsto  no art. 261 do C&oacute;digo de Tr&acirc;nsito Brasileiro &ndash; CTB e Resolu&ccedil;&atilde;o CONTRAN N&ordm;  182/2005.
</p>

<br />
<p align="left" class="fontmaior" style="text-indent: 50px; margin-top=0px;" >
	V.S&ordf;  poder&aacute; apresentar defesa, de acordo com o art. 265 do C&oacute;digo de Tr&acirc;nsito  Brasileiro, neste formul&aacute;rio, no quadro &ldquo;Justificativa / Descri&ccedil;&atilde;o dos Fatos&rdquo;, com  a exposi&ccedil;&atilde;o dos fatos, fundamenta&ccedil;&atilde;o legal do pedido e documentos que comprovem  a alega&ccedil;&atilde;o, at&eacute; o prazo abaixo determinado, que n&atilde;o poder&aacute; ser inferior a  quinze dias do recebimento desta.  
</p>

<% if ( (lProcAnt.size()>0) && ("0".equals(procAnt.getTotPntProcessoAnt())==false) &&
("001".equals(notificacao.getSeqNotificacao())) ) { %>
<p align="left" class="fontmedia" style="text-indent: 50px;">Processos reincidentes:
<% } else { %>
<p align="left" class="fontmedia" style="text-indent: 50px;">&nbsp;
<% } %>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
<%
for (int i=0;i<lProcAnt.size();i++) {
procAnt = lProcAnt.get(i) ;
// Se seq notificacaonão é 001 colocamos apenas duas linhas de Proc Anteriores
if ( ("001".equals(notificacao.getSeqNotificacao())==false) && (i==lProcAnt.size()-2) ) {
break;
}
if ("0".equals(procAnt.getTotPntProcessoAnt())==false) {
%>
<tr>
<td><span class="fontmedia">N&ordm; Processo: <span style="text-indent: 50px; margin-bottom: 0px;"><%=procAnt.getNumProcessoAnt()%></span></span></td>
<td><span class="fontmedia">Data: <%=procAnt.getCodProcessoAnt()%></span></td>
<td><span class="fontmedia">Pontua&ccedil;&atilde;o: <%=procAnt.getTotPntProcessoAnt()%></span></td>
<td><span class="fontmedia"><%=procAnt.getSitProcessoAnt()%></span></td>
</tr>
<%} else { %>
<tr>
<td><span class="fontmedia">&nbsp;<span style="text-indent: 50px; margin-bottom: 0px;">&nbsp;</span></span></td>
<td><span class="fontmedia">&nbsp;</span></td>
<td><span class="fontmedia">&nbsp;</span></td>
<td><span class="fontmedia">&nbsp;</span></td>
</tr>

<% }
}
%>
</table>
</td>
</tr>
<tr><td height="5">&nbsp;</td></tr>
<tr>
<td height="25" align="center" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-bottom: 10px">
<tr>
<td height="25" valign="top" class="tdborda" colspan="5"><span class="fontmenor">&nbsp;&nbsp;<strong>NOME</strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNomResponsavel()%></span></td>
<td width="1%" valign="top" class="semborda">&nbsp;</td>
<td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE MULTAS</strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getQtdMultas()%></span></td>
<td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
<td width="21%" rowspan="3" valign="top" class="tdborda"><span class="fontmenor"><strong><strong>&nbsp;&nbsp;N&ordf; PROCESSO </strong></strong></span><br>
<br>
<span class="fontmaior">&nbsp;<strong><%=ProcessoBeanId.getNumProcesso()%></strong></span></td>
<td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
<td width="17%" valign="top" rowspan="3" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>PRAZO P/ DEFESA</strong></span><br>
<br>
<span class="fontmaior">&nbsp;<strong><%=ProcessoBeanId.getUltDatDefesa()%></strong></span></td>
</tr>
<tr>
<td colspan="11" height="2"></td>
</tr>
<tr>
<td width="17%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%></strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCNH()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getUfCNH()%></span></td>
<td width="1%" valign="top">&nbsp;</td>
<td width="11%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>EXPEDI&Ccedil;&Atilde;O</strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getDatEmissaoCNH()%></span></td>
<td width="1%" valign="top" class="semborda">&nbsp;</td>
<td width="15%" valign="top" class="tdborda" ><span class="fontmenor">&nbsp;&nbsp;<strong>CPF</strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCPFEdt()%></span></td>
<td width="1%" valign="top" class="semborda">&nbsp;</td>
<td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE PONTOS</strong></span><br>
<span class="fontmaior">&nbsp;<%=ProcessoBeanId.getTotPontos()%></span></td>
<td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
<td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
</tr>
</table></td>
</tr>
</table>