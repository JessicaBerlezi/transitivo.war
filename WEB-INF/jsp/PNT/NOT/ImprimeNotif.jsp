<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="ArquivoBeanID" scope="session" class="PNT.NOT.ArquivoBean" />
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>
	
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>	
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,codigo,tipImpressao) 
			{
			 switch (opcao) {
			   case 'ImprimirNotificacao':
				  document.forms[0].codArquivo.value=codigo;			   
				  document.forms[0].tipImpressao.value=tipImpressao;			   					  
				  document.forms[0].acao.value=opcao;			   					  					  
				  document.forms[0].target= "_self";
				  document.forms[0].action = "acessoTool";  
				  document.forms[0].submit();	  		  
				  break ;
			   case 'ConsultaCEP':
				  document.forms[0].codArquivo.value=codigo;			   
				  document.forms[0].tipImpressao.value=tipImpressao;			   					  
				  document.forms[0].acao.value=opcao;			   					  					  
				  document.forms[0].target= "_self";
				  document.forms[0].action = "acessoTool";  
				  document.forms[0].submit();	  		  
				  break ;
			   case 'R':
				  close() ;   
				  break;
			}
			}
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="RelatArqNotifForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		<input type="hidden" name="codArquivo" value="">
		<input type="hidden" name="tipImpressao" value="">		
		<input type="hidden" name="ordem" value="1">		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:75px; height:260px;">
		<div class="divTitulos" id="titStatus" style=" height:40px;">
			<table cellspacing="1" class="titulos" >  	   		
			<tr> 
      			<td rowspan="2" width="214">Arquivo</td>
      			<td rowspan="2" width="78">Data<br />Gera��o</td>		 	  
      			<td colspan="3">CEPs</td>					
      			<td colspan="3">Notifica��es</td>					
 			</tr>
			<tr> 
   				<td width="38">Total</td>					
  				<td width="66">Impressos</td>					
   				<td width="78">N�o Impressos</td>					
   				<td width="38">Total</td>					
   				<td width="66">Impressos</td>					
   				<td>N�o Impressos</td>					
 			</tr>
		    </table>
		</div>
		
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
        <div class="divCorpo" id="corpoPenalidade" style="height: 220px;" >
    	<table id="fevento" cellspacing="1" style="width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;">
        <%
    	PNT.NOT.ArquivoBean myArq = new PNT.NOT.ArquivoBean();
	    String cor = "";
	    for (int seq=0;seq<ArquivoBeanID.getListaArquivos().size();seq++) {
			myArq = ArquivoBeanID.getListaArquivos(seq);
			cor   = (seq%2==0) ?    "#ffffff" : "#d9f4ef" ;			
				%>
				<tr bgcolor="<%= cor %>"  style="cursor:hand">				
      				<td width="214"><%=myArq.getNomArquivo()%></td>		 	  
      				<td width="78" align="center"><%=myArq.getDatGeracao()%></td>					
      				<td width="38" align="center"><%=sys.Util.formataIntero(myArq.getTotCep())%></td>					
      				<td width="66" align="center" onClick="javascript:valida('ConsultaCEP','<%= myArq.getCodArquivo()%>','R');"><%=sys.Util.formataIntero(myArq.getTotCepImpressos())%></td>	
      				<td width="78" align="center" onClick="javascript:valida('ConsultaCEP','<%= myArq.getCodArquivo()%>','I');"><%=sys.Util.formataIntero(myArq.getTotCepNaoImpressos())%></td>					
      				<td width="38" align="center"><%= sys.Util.formataIntero(myArq.getTotNotificacoes())%> </td>					
      				<td width="66" align="center" onClick="javascript:valida('ImprimirNotificacao','<%= myArq.getCodArquivo()%>','R');"><%=sys.Util.formataIntero(myArq.getTotNotificacoesImpressas())%></td>					
      				<td align="right" style="padding-right: 25px;" onClick="javascript:valida('ImprimirNotificacao','<%= myArq.getCodArquivo()%>','I');" ><%=sys.Util.formataIntero(myArq.getTotNotificacoesNaoImpressas())%></td>
 				</tr>
		<% 	}	%>
		</table>
		</div>
        </div>

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	</form>
</BODY>
</HTML>


	







	