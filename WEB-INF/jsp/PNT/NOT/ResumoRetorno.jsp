<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ResumoRetornoBeanId"     scope="session" class="PNT.NOT.ResumoRetornoBean"/> 
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'consultarAno':
   		if(fForm.numAno.value == "")
   		{
   		    alert("Ano n�o preenchido.");
            document.all.numAno.focus();
   			return;
		}
   		if(fForm.dtReferencia.value == "")
   		{
   		    alert("Data de refer�ncia n�o preenchida.");
            setTimeout("doSelection(document."+fForm.name+".dtReferencia)", 0)   		
   			return;
		}
		fForm.acao.value=opcao;
   		fForm.target= "_self";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  break;
   case 'R':
	    close() ;   
	  break;
  }
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="resumoRetorno" method="post" action="" >
<%@ include file="../Cab.jsp" %>
<input name="acao" type="hidden" value="">	
<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:475px; overflow: visible; z-index: 1; top: 150px; left:200px;" >  
  <table width="100%" cellpadding="0" cellspacing="1">
	<tr>
       <td><strong>Ano :</strong></td>
        <td align="left">
           &nbsp;<jsp:getProperty name="ResumoRetornoBeanId" property="popupString" /></td>
      </td>
      <td> &nbsp;&nbsp;<strong>Posi��o em :</strong>
          <input name="dtReferencia" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=ResumoRetornoBeanId.getDatReferencia()%>">
      </td>
      <td style="text-align: right"><button type="button" name="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consultarAno',this.form);"> <img src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>
    </tr>
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>
 
<!--Div Erros-->
<%
String msgErro       = ResumoRetornoBeanId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->

</form>
</body>
</html>