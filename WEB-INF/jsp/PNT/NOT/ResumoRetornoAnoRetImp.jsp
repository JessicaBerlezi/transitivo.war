<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<jsp:useBean id="SistemaBeanId"       scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"       scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId"   scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="ResumoRetornoBeanId" scope="session" class="PNT.NOT.ResumoRetornoBean"/> 

<html>   
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">
.relat {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 text-align: right;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #000000;
	 border-bottom: 1px solid #000000;
	 border-left: 0px solid #999999;
     word-spacing: 0px;
}
</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ResumoRetornoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
int npag       = 1;
String tituloConsulta = (String)request.getAttribute("tituloConsulta");
if(tituloConsulta==null) tituloConsulta=""; 
%>
<%@ include file="../Cab_imp.jsp" %>			  

<!--DADOS-->

  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="15%"  height="13" align="right" bgcolor="#999999" class="relat"><strong>Quantidade/M&ecirc;s</strong> &nbsp; </td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JAN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">FEV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">ABR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAI</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUL</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">AGO</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">SET</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">OUT</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">NOV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">DEZ</td>
      <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 13px; border-right: 0px none;">T o t a l</td>
    </tr>
    <tr> 
      <td align="right" class="relat"><strong>Retornada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%>&nbsp;&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%></td>
    </tr>
  </table>

<!--FIM DADOS-->				  

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr>
      <td colspan="14" bgcolor="#999999" class="relat" style="text-align: left; border-right: 0px none;"><strong>&nbsp;C&Oacute;DIGOS DE RETORNO</strong></td>
    </tr>
</table>



<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">


    <%for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) {
    %>
    <tr> 
    <td width="15%" align="right" class="relat"><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getDescRetorno()%>&nbsp;<strong><br>
    <%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()%>&nbsp;</strong></td>
      <%for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++) {
      %>
      <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada())%>&nbsp;&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())%></td>
    </tr>
    <%
    }
    %>
  </table>
  <%@ include file="../rodape_imp.jsp" %>


</form>
</body>
</html>