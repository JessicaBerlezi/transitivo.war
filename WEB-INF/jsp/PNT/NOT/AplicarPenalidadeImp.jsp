<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<jsp:useBean id="ProcessoImpBeanID" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<% String tituloConsulta = (String)request.getAttribute("tituloConsulta");
  if(tituloConsulta==null) tituloConsulta=""; 
%> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >

<form name="AplicarPenalidadeImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
    boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	if 	(ProcessoImpBeanID.getProcessos().size()>0){         	   
		 for (int i=0;i<ProcessoImpBeanID.getProcessos().size();i++) {			
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 25 linhas, exceto na primeira linha.
				if (contLinha>22){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {			
		%>
				        </table>      
						<%@ include file="../rodape_imp.jsp" %>
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
	<%@ include file="../Cab_imp.jsp" %>	
		    <table cellpadding=1 cellspacing=1 border="0" width="100%" valign="top" class="semborda">
				<tr>
					<td><strong>PENALIDADE:&nbsp;&nbsp;<font size="1"><b><%=ProcessoImpBeanID.getProcessos(i).getDscPenalidade()%></b></font></strong></td>
				</tr>
			</table>
			<table border="0" cellpadding="0" cellspacing="1" width="100%" align="center" class="semborda">
					<tr>
						<td width="60" rowspan="2" style="background-color:#CCCCCC; text-align:  center;"><strong>Seq          </strong></td>
						<td width="120" rowspan="2" style="background-color:#CCCCCC; text-align: center;"><strong>Processo    </strong></td>
						<td width="70" rowspan="2" style="background-color:#CCCCCC; text-align:  center;"> <strong>Data Proc.  </strong></td>
						<td colspan="2" height="18" style="background-color:#CCCCCC; text-align: left;"><strong>&nbsp;&nbsp;Nome        </strong></td>
						<td style="background-color:#CCCCCC; text-align: center;">                        <strong>Total Pontos</strong></td>
					</tr>					
					<tr>
						<td width="170" height="18" style="background-color:#CCCCCC; text-align: center;"><strong>CPF         </strong></td>
						<td width="170" style="background-color:#CCCCCC; text-align: center;">            <strong>CNH/PGU     </strong></td>
						<td style="background-color:#CCCCCC; text-align: center;">                        <strong>Total Multas</strong></td>
					</tr>						
			</table>
			
		       <% } %>	
    <!--inicia linha-->           
			<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr>
					<td height="1" bgcolor="#000000"></td>
				</tr>
			</table>
			<table id="espaco" cellpadding="0" cellspacing="0" border="0" width="100%" bordercolor="#000000" class="semborda">
				<tr>
					<td width="60"  rowspan="2" style="text-align: center;">&nbsp;&nbsp;<%=seq%></td>
					<td width="120" rowspan="2" style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getNumProcesso()%></td>
					<td width="70"  rowspan="2" style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getDatProcesso()%></td>
					<td colspan="2" height="18" style="text-align: left;">&nbsp;&nbsp;&nbsp;&nbsp;<%=ProcessoImpBeanID.getProcessos(i).getNomResponsavel()%></td>
					<td                         style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getTotPontos()%></td>
				</tr>
				<tr>
					<td width="170" height="18" style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getNumCPF()%></td>
					<td width="170"             style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getNumCNH()%> - <%=ProcessoImpBeanID.getProcessos(i).getUfCNH()%></td>
					<td                         style="text-align: center;"><%=ProcessoImpBeanID.getProcessos(i).getQtdMultas()%></td>
				</tr>
				<tr><td colspan="6" height="1" bgcolor="#000000"></td></tr>
			</table>
			
			
<% } %>
<table width="100%" class="semborda" ><tr><td height="2"></td></tr></table>
<%}%>  
</div>
<%@ include file="../rodape_imp.jsp" %>
</form>
</body>
</html>