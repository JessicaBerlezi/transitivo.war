<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>
<jsp:useBean id="ArquivoBeanId" scope="request" class="PNT.NOT.ArquivoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>		
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
  switch (opcao) {
   case 'R':
    close() ;
	break;
   case 'O':  // Esconder os erro
	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
	
  }
}

var theTable, theTableBody
var texto0 = '';
var texto1 = '';
var texto2 = '';
var texto3 = '';
var texto4 = '';
function callPrint(fForm) {
	
	while(theTableBody.rows.length>0) {
		removeRow(fForm); 
	}
	
	if (texto0!='') {
		appendRow(fForm,texto0)
	}
	if (texto1!='') {
		//alert(texto1)
		appendRow(fForm,texto1)
	}
	if (texto2!='') {
		//alert(texto2)
		appendRow(fForm,texto2)
	}
	if (texto3!='') {
		//alert(texto3)
		appendRow(fForm,texto3)
	}
	if (texto4!='') {
		//alert(texto4)
		appendRow(fForm,texto4)
	}

//Impressao
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();
}

function escondeFrame() {
	document.all.printFrame.style.visibility="hidden";
}
function mostraFrame() {
	document.all.printFrame.style.visibility="visible";
}

function init() {
	theTable = (document.all) ? window.parent.frame1.myTABLE : window.parent.frame1.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function appendRow(form,texto) {
	insertTableRow(form,texto, -1)
}

function addRow(form) {
	insertTableRow(form, form.insertIndex.value)
}

function removeRow(form) {
	theTableBody.deleteRow(0)
}

function insertTableRow(form,texto, where) {
	var newCell
	var newRow
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table>" + texto + "</table></td>"
		
}

</script>
</head>

<body onLoad="init()" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ProtoRecForm" method="post" action="">
<%@ include file="../Cab.jsp"%>
<input name="acao" type="hidden" value=' '>

<!--IN�CIO_CORPO_sistema-->
<div id="dadosProtocolo" style="position:absolute; left:50px; top:80px; width:720px; height:170px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#c0e1df"> 
        <td colspan=5 height="23">&nbsp;&nbsp;<strong>Protocolo de Recebimento&nbsp;<%=ArquivoBeanId.getNumProtocolo() %>&nbsp; 
          do Arquivo&nbsp;&nbsp;&nbsp;<%=ArquivoBeanId.getNomArquivo()%>&nbsp;</strong></td>
        <td><button id="btnimp" style="border: 0px; background-color: transparent; height: 21px; width: 28px; cursor: hand;" type="button" onClick="callPrint(this.form)"><img src="images/PNT/bot_imprimir_escuro_ico.gif" width="26" height="19"></button></td>
      </tr>
      <tr> <td height="6" colspan=6></td></tr>
      <tr bgcolor="#d9f4ef"> 
        <td height="16">&nbsp;&nbsp;<strong>C�digo do Arquivo:</strong></td>
        <td><%=ArquivoBeanId.getCodIdentArquivo() %></td>
        <td><strong>Data Gera&ccedil;&atilde;o:</strong></td>
        <td><%=ArquivoBeanId.getDatGeracao()%></td>
        <td><strong>&Oacute;rg&atilde;o:</strong></td>
        <td><%=ArquivoBeanId.getCodOrgao()%> - <%=ArquivoBeanId.getSigOrgao() %></td>
      </tr>
      <tr bgcolor="#d9f4ef"> 
        <td>&nbsp;&nbsp;<strong>Sequencia:</strong></td>
        <td><%=ArquivoBeanId.getNumControleArq() %></td>
        <td height="16"><strong>Data Envio:</strong></td>
        <td><%=ArquivoBeanId.getDatRecebimento()%></td>
        <td><strong>N&ordm; Registros:</strong></td>
        <td><%=ArquivoBeanId.getQtdLinha() %></td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr bgcolor="#d9f4ef"> 
        <td width="105" height="16">&nbsp;&nbsp;<strong>Recebido por:</strong></td>
        <td width="136"><%=ArquivoBeanId.getNomUsername() %></td>
        <td width="081"><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
        <td colspan="3"><%=ArquivoBeanId.getSigOrgaoLotacao()%></td>
      </tr>
    </table>
</div>
<div id="dadosProtocoloPrint" style="position:absolute; left:50px; top:80px; width:720px; height:170px; z-index:-20; overflow: auto; visibility: hidden;"> 
    <table id="table1" width="720" border="0" cellpadding="0" cellspacing="0" class="table">
    <TBODY>
      <tr><td height="6" colspan=6></td></tr>
      <tr bgcolor="#d9f4ef"> 
        <td height="16">&nbsp;&nbsp;<strong>C�digo do Arquivo:</strong></td>
        <td><%=ArquivoBeanId.getCodIdentArquivo() %></td>
        <td><strong>Data Gera&ccedil;&atilde;o:</strong></td>
        <td><%=ArquivoBeanId.getDatGeracao()%></td>
        <td><strong>&Oacute;rg&atilde;o:</strong></td>
        <td><%=ArquivoBeanId.getCodOrgao()%> - <%=ArquivoBeanId.getSigOrgao() %></td>
      </tr>
      <tr bgcolor="#d9f4ef"> 
        <td>&nbsp;&nbsp;<strong>Sequencia:</strong></td>
        <td><%=ArquivoBeanId.getNumControleArq() %></td>
        <td height="16"><strong>Data Envio:</strong></td>
        <td><%=ArquivoBeanId.getDatRecebimento()%></td>
        <td><strong>N&ordm; Registros:</strong></td>
        <td><%=ArquivoBeanId.getQtdLinha() %></td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr bgcolor="#d9f4ef"> 
        <td width="105" height="16">&nbsp;&nbsp;<strong>Recebido por:</strong></td>
        <td width="136"><%=ArquivoBeanId.getNomUsername() %></td>
        <td width="081"><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
        <td colspan="3"><%=ArquivoBeanId.getSigOrgaoLotacao()%></td>
      </tr>
    </table>
</div>
<!--FIM DIV FIXA-->
<script>
function initTable1() {
	var theTable1
	var theTableBody1	
	theTable1 = (document.all) ? document.all.table1 : document.getElementById("table1")
	theTableBody1 = theTable1.tBodies[0]
	texto0="<tr><td><strong>Protocolo de Recebimento <%=ArquivoBeanId.getNumProtocolo() %> do Arquivo <%=ArquivoBeanId.getNomArquivo()%></strong></td></tr>"
	texto1=theTableBody1.innerHTML
}
      

initTable1()

</script>
<%
	if (ArquivoBeanId.getErrosArquivo().size() > 0) {
%>	
<!--ARQUIVO REJEITADO--> 
<div id="arqRejeitado" style="position:absolute; left:50px; top:165px; width:720px; height:30px; z-index:5;  visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#c0e1df" > 
        <td height="23" class="espaco" colspan=3>
			<p>&nbsp;&nbsp;<strong><font color="#CC3300">ARQUIVO REJEITADO </font></strong></p></td>
      </tr>
	  <tr bgcolor="#d9f4ef"> 
       	<td width="56" align=center><strong>Registro</strong></td>
      	<td width="44" align=center><strong>Erro</strong></td>
      	<td width="610">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Motivo</strong></td>
      </tr>
	 </table>
</div>
<div id="msgErro" style="position:absolute; left:50px; top:202px; width:720px; height:142px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
	<%
	if (ArquivoBeanId.getErrosArquivo().size() > 0) {
		String l = "";
		for ( int t=0; t<ArquivoBeanId.getErrosArquivo().size() ;t++) {
			l = (String) ArquivoBeanId.getErrosArquivo().get(t) ;
	%>
	  		<tr><td colspan="3" height="1px"></tr>
	  		<tr bgcolor="#d9f4ef"> 
       			<td width="56" align=center><%=l.substring(0,5)%></td>
       			<td width="44" align=center><%=l.substring(6,9)%></td>
      			<td><%=l.substring(10,l.length())%></td>
     		</tr>
	<%
		} 
	} 
	%>
	</table>
</div>

<div id="msgErroPrint" style="position:absolute; left:50px; top:208px; width:720px; height:142px; z-index:-20; overflow: auto; visibility: hidden;"> 
    <table id="table2" width="720" border="0" cellpadding="0" cellspacing="0" class="table">
    <TBODY>
   	<tr bgcolor="#deebc2" > 
        <td height="23" class="espaco" colspan=3>
			<p>&nbsp;&nbsp;<strong><font color="#CC3300">ARQUIVO REJEITADO </font></strong></p></td>
     </tr>
	 <tr bgcolor="#d9f4ef"> 
       	<td width="56" align=center><strong>Registro</strong></td>
      	<td width="44" align=center><strong>Erro</strong></td>
      	<td>&nbsp;&nbsp;&nbsp;&nbsp;<strong>Motivo</strong></td>
     </tr>
   	
	<%
	if (ArquivoBeanId.getErrosArquivo().size() > 0) {
		String l = "";
		for ( int t=0; t<ArquivoBeanId.getErrosArquivo().size() ;t++) {
			l = (String) ArquivoBeanId.getErrosArquivo().get(t) ;
	%>
	  		<tr bgcolor="#EFF5E2"> 
       			<td width="56" align=center><%=l.substring(0,5)%></td>
       			<td width="44" align=center><%=l.substring(6,9)%></td>
      			<td><%=l.substring(10,l.length())%></td>
     		</tr>
	<%
		} 
	} 
	%>
	</table>
</div>
<script>
function initTable2() {
	var theTable1
	var theTableBody1	
	theTable1 = (document.all) ? document.all.table2 : document.getElementById("table2")
	theTableBody1 = theTable1.tBodies[0]
	texto2=theTableBody1.innerHTML
}
initTable2()
</script>

<!--FIM DIV ARQUIVO REJEITADO-->
<% } else { %>

<!--INSTRUCOES--> 
<div id="instrucoes" style="position:absolute; left:50px; top:180px; width:720px; height:136px; z-index:5; overflow: visible; visibility: visible;"> 
    <table id="table3" width="720" border="0" cellpadding="0" cellspacing="0" class="table">
    <TBODY>
      <tr  bgcolor="#c0e1df"> 
        <td width="209" height="23"><p>&nbsp;&nbsp;<strong>Instru&ccedil;&otilde;es</strong></p></td>
      </tr>
      <tr><td height="6"></td> </tr>
    </table>
    <table id="table4" width="720" border="0" cellpadding="4" cellspacing="0" class="table">
      <tr bgcolor="#d9f4ef">
        <td height="18" valign="top" colspan=2>1 - ARQUIVO FOI ENVIADO 
          E RECEBIDO COM SUCESSO. AGUARDE PROCESSAMENTO.</td>
      </tr>
      <tr bgcolor="#d9f4ef"> 
        <td height="18" valign="top" colspan=2>2 - ACOMPANHE O 
          STATUS ATRAV&Eacute;S DA OP��O:&nbsp;&nbsp;&nbsp;<font color=red>CONSULTAR ARQUIVO.</font></td>
      </tr>
      <tr bgcolor="#d9f4ef"> 
        <td height="18" valign="top" colspan=2>3 - QUANDO O ARQUIVO 
          FOR PROCESSADO, O RESULTADO ESTAR&Aacute; DISPON&Iacute;VEL PARA CONSULTA E DOWNLOAD.</td>
      </tr>      
    </table>
</div>
<script>
function initTable3() {
	var theTable1
	var theTableBody1	
	theTable1 = (document.all) ? document.all.table3 : document.getElementById("table3")
	theTableBody1 = theTable1.tBodies[0]
	texto3=theTableBody1.innerHTML
	
	theTable1 = (document.all) ? document.all.table4 : document.getElementById("table4")
	theTableBody1 = theTable1.tBodies[0]
	texto4=theTableBody1.innerHTML
}
initTable3()
</script>
<!--FIM DIV INSTRUCOES--> 
<% }  %>

<div id="printFrame" style="position:absolute; left:600px; top:50px; width:500px; height:350px; z-index:5; overflow: auto; visibility: hidden;"> 
<iframe name="frame1" id="frame1" src="<%=path%>/js/printFrame.jsp" frameborder="0" width="365" heigth="350" MARGINWIDTH="0" MARGINHEIGHT"0" SCROLLING="NO">
</iframe>
</div>
 
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->

</form>
</body>
</html>