<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<jsp:useBean id="RetornoNotificacaoBeanID" scope="session" class="PNT.NOT.RetornoNotificacaoBean" />

<!-- Chama o Objeto da Tabela de Retornos -->
<jsp:useBean id="CodigoRetornoBeanID" scope="request" class="REC.CodigoRetornoBean" />
  <jsp:setProperty name="CodigoRetornoBeanID" property="checked"     value="<%=RetornoNotificacaoBeanID.getRetorno().getNumRetorno()%>"  />
  <jsp:setProperty name="CodigoRetornoBeanID" property="j_abrevSist" value="REC" />  	 	     	 	  
  <jsp:setProperty name="CodigoRetornoBeanID" property="colunaValue" value="NUM_CODIGO_RETORNO" /> 
  <jsp:setProperty name="CodigoRetornoBeanID" property="popupNome"   value="codRetorno"  />  
  <jsp:setProperty name="CodigoRetornoBeanID" property="popupString" value="DSC_CODIGO_RETORNO,SELECT * FROM TSMI_CODIGO_RETORNO ORDER BY NUM_CODIGO_RETORNO" />      


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script>var bProcessa=true;</script>
<html>
	<head>
		<%@ include file="../Css.jsp" %>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

		<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {   	  
	   case 'R':
		  close() ;   
		  break;
	   case 'GravarRetorno':
	      if (bProcessa==false)
	      { 
	        alert("Retorno de Notifica��es em Processamento...");
	        return;
	      }
	      if (veCampos(fForm)==true) {		  
	        bProcessa=false;
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}


function MascarasNotif(fForm,Objeto, Masc,k)
{ 
  //  'X' Qualquer Caracter da tabela ASCII Ex: L 9 , . / $ +
  //  'A' S? Letras
  //  '9' S? N?meros
  //  'D' Delimitador  Ex: ! @ # $ % ? & * ( ) _ - _ -

  Masc = Masc.toLocaleUpperCase();

  var Tecla = event.keyCode;         //pega a tecla
  VetMasc = new Array(Masc.length);   //cria o vetor da mascara

  SizeObjeto = Objeto.value.length;   //pega o tamanho da text

  //tamanho Objt >= tamanho mascara
  if( SizeObjeto >= Masc.length ) {
      event.returnValue = false;
      return;
  }

  //preenche o vetor com a mascara  '9','A','. / ( ) S V','X'
  for( i=0; i < Masc.length; i++ ) { VetMasc[i] = Masc.substring( i,i+1);}

  switch( VetMasc[SizeObjeto] )
  {
         case '9': 
                   event.returnValue = ProcNumero(Tecla)
                   break
         case 'A': 
                   event.returnValue = ProcLetra(Tecla)
                   break
         case 'X': 
                   event.returnValue = true;
                   break
         default:  
                   if( Tecla == VetMasc[SizeObjeto].charCodeAt() )
                       event.returnValue = true;
                   else 
                   { 
                       while(SizeObjeto < Masc.length - 1)
                       {
                             SizeObjeto = Objeto.value.length;   //pega o tamanho da text
                             if( ((VetMasc[SizeObjeto] != '9' && VetMasc[SizeObjeto] != 'A') && VetMasc[SizeObjeto] != 'X') )
                                   Objeto.value += VetMasc[SizeObjeto];
                             else
                                  break
                       }
                       // se a masc for "N-----N" e eu clicar em um letra 
                       // nao mostra a letra clicada so 9----- este bloco trata isso
                       switch( VetMasc[SizeObjeto] )
                       {
                               case '9':
                                         event.returnValue = ProcNumero(Tecla)
                                         break
                               case 'A':
                                         event.returnValue = ProcLetra(Tecla)
                                         break
                               case 'X': 
                                         event.returnValue = true;
                               break
                               default:  
                                         event.returnValue = false;
                      }

                   }
  }
  k=k+1;
  if ( (SizeObjeto==12) && (k<fForm.qtdNotif.value) )
  {
      setTimeout("doSelection(document."+fForm.name+".numNotificacao["+k+"])", 0)            
  }

}


function ValNumNotificacao(k,numNotif,fForm) {
/**
* NumProcesso  -> nnnnnnnnn-nnn
*  variavel global valid = true   - sem erro
*  variavel global valid = false  - com erro
*  variavel global sErro = mensagens de erro, se existir
*  retorno = numero da notificacao editado - se sem erro
*  retorno = numero da notificacao original - se com erro
*/
    valid = true ;
	sErro = "" ;
	var today   = new Date();
	var ano     = today.getYear();
	var primPos = "";
	var notif   = numNotif.value;
	// Verifico notificacao esta preenchida
	if (notif.length==0) {
		  sErro += "Notifica��o n�o informada. \n"
		  valid = false ;
	}
	else 
	{
		// separando os blocos
		var p1="";
		var p2="";
		var p4="";

		var posB1=-1;
		posB1=notif.indexOf('-');
		if (posB1>0) posB2=notif.length;
		
		if (posB1<0) {
		  sErro += "Formato da notifica��o invalida. Tente '000000001-001'"
		  valid = false ;
		}
		if (valid==true) {
			//Monto os peda?os e conto as barras			
    	    p1 = notif.substring(0,posB1);
       	    p1 = "000000000"+p1;
       	    p1 = p1.substring(p1.length-9,p1.length);
       	    var p1Num = p1*1; 
       	    if (isNaN(p1Num)) {
				sErro += "Numero da Notifica��o n�o numerico.Tente '000000001-001' \n";
				valid = false ;	
			}
    	    
            p2 = notif.substring(posB1+1,notif.length);   	    
       	    p2 = "000"+p2;
       	    p2 = p2.substring(p2.length-3,p2.length);
       	    var p2Num = p2*1; 
       	    
       	    if (isNaN(p2Num)) {
				sErro += "Numero da Notifica��o n�o numerico.Tente '000000001-001' \n";
				valid = false ;	
			}
		}				
	}		
	if (valid==false)
	{
	  alert(sErro) ;
      setTimeout("doSelection(document."+fForm.name+".numNotificacao["+k+"])", 0)            
	}
	else 
	{
		numNotif.value = p1+"-"+p2;
	}
	return valid ;	
}

function comparaDatas(data2) {
	var valid = false;
	var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var today=new Date();
	var difference=today-dataB;//obt�m o resultado em milisegundos.
	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = true;
	return valid;
}  


function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	valid = ValDt(RetornoForm.datRetorno,1) && valid	
	if ( (RetornoForm.qtdNotif.length==0) || (RetornoForm.qtdNotif.value=='0') )
	{
	  sErro+="Quantidade de Notifica��es n�o preenchida.\n";
	  valid=false;
	}
	if ( (trim(RetornoForm.codRetorno.value).length==0) || (RetornoForm.codRetorno.value=='0') )
	{
	  sErro+="Tipo de Retorno n�o preenchido.\n";
	  valid=false;
	}
    if (comparaDatas(fForm.datRetorno.value)==true){
       valid = false
       sErro += "Data do Retorno das Notifica��es n�o pode ser posterior a data atual. \n"
    }
	if (valid==false) alert(sErro) ;
	return valid ;
}

var theTable, theTableBody , qNotif=<%=RetornoNotificacaoBeanID.getTotalNotificacoes()%>

function imprimeFrame() {
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();		
}
function escondeFrame() {
	document.all.etiquetaFrame.style.visibility="hidden";
}
function mostraFrame() {
	document.all.etiquetaFrame.style.visibility="visible";
}

function init() {
	theTable = (document.all) ? document.all.myTABLE : document.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0];
    appendRow2(document.all,qNotif);
}

function changeQtdNotif(form,qNotif) {
	if ( (form.qtdNotif.length==0) || (form.qtdNotif.value=='0') )
	{
	  alert("Quantidade de Notifica��es n�o preenchida.");
      setTimeout("doSelection(document."+form.name+".qtdNotif)", 0)            
	  return;
	}
  appendRow2(form,qNotif);
  //setTimeout("doSelection(document."+form.name+".numNotificacao[0])", 0)            
}


function init2() {
	theTable = (document.all) ? window.parent.frame1.myTABLE : window.parent.frame1.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function appendRow(form) {
	insertTableRow(form, -1)
}
function appendRow2(form,qNotif) {
	insertTableRow2(form, -1,qNotif)
}

function addRow(form) {
	insertTableRow(form, form.insertIndex.value)
}

function removeRow(form) {
	theTableBody.deleteRow(0)
}

function insertTableRow(form, where) {
	var now = new Date()
	var nowData = [now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()]
	clearBGColors()
	var newCell
	var newRow = theTableBody.insertRow(where);
	for (var i = 0; i < nowData.length; i++) {
		newCell = newRow.insertCell(i)
		newCell.innerHTML = nowData[i]
		newCell.style.backgroundColor = "salmon"
	}
}



</script>
	</head>
	<body onLoad="init()" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
		<form name="RetornoForm" method="post" action="">
			
     		<%@ include file="../Cab.jsp" %>
			<input name="acao" type="hidden" value=''>
			<!--IN�CIO_CORPO_sistema-->
			
<div id="recurso3" style="position:absolute; left:50px; top:80px; width:720px; height:20px; z-index:2; overflow: visible; visibility: visible;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
					
					
					<tr bgcolor="#d9f4ef">
						<td width="15%" align="right">
							&nbsp;&nbsp;<strong>Retornos :&nbsp;&nbsp;</strong></td>
						<td width="45%">
                        <jsp:getProperty name="CodigoRetornoBeanID" property="popupString" />
                        </td>
									
															
						<td width="35%">
                          <strong>Quantidade de Notifica��es :&nbsp;<input type="text" name="qtdNotif" size="5" maxlength="3" value='<%=RetornoNotificacaoBeanID.getTotalNotificacoes()%>'  onkeypress="javascript:Mascaras(this,'999');" onFocus="javascript:this.select();" onChange="javascript:changeQtdNotif(this.form,this.value)">
						</td>
						<td align="right" >
							&nbsp;&nbsp;<strong>Data :&nbsp;&nbsp;</strong> </td>
						<td>
                        <input type="text" name="datRetorno" size="12" maxlength="10" value='<%=sys.Util.formatedToday().substring(0,10)%>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
                        </td>
									
						<td width="2%"></td>									
						<td>
	    	            <button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('GravarRetorno',this.form);">			  
		  	            <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19">
			            </button>
						</td>
					</tr>
					
					
					
				</table>
				</div>
				
				<div id="titulo" style="position:absolute; left:50px; top:110px; width:720px; height:35px; z-index:2; overflow: visible; visibility: visible;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
					<tr>
					<td height="17" style="background-color:#c0e1df; text-align: center;"><strong>Notifica&ccedil;&otilde;es</strong>
					</td>
					</tr>
				</table>
				</div>
								
<div id="recurso4" style="position:absolute; left:50px; top:128px; width:720px; height:205px; z-index:2; overflow: auto; "> 
			 <table ID="myTABLE" width="100%" border="0" cellpadding="0" cellspacing="1" class="table" bgcolor='#d9f4ef'>      
		     <%
		       if (RetornoNotificacaoBeanID.getListaNotificacoes().size()>0)
		       {
	              int resto =   RetornoNotificacaoBeanID.getListaNotificacoes().size()%4 ;
                  if (resto>0) {
                    String myNotif  = "";
                    for (int i=0; i<4-resto;i++) {
                            	RetornoNotificacaoBeanID.getListaNotificacoes().add(myNotif);
				  }
			    }
				for (int i = 0; i < RetornoNotificacaoBeanID.getListaNotificacoes().size(); i=i+4) {
		        %>			  	  
		        
				<tr bgcolor="#d9f4ef"> 
					 <td width="185" style="text-align: right; height: 20px;">&nbsp;<%=sys.Util.lPad(String.valueOf(i+1),"0",3)%>.&nbsp;<input   name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="<%= RetornoNotificacaoBeanID.getNotificacao(i)   %>" size="26"   maxlength="20" onChange='javascript:ValNumNotificacao(<%=i%>,this,this.form);'></TD>
					 <td width="185" style="text-align: right;">              &nbsp;<%=sys.Util.lPad(String.valueOf(i+2),"0",3)%>.&nbsp;<input   name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="<%= RetornoNotificacaoBeanID.getNotificacao(i+1) %>" size="26"   maxlength="20" onChange='javascript:ValNumNotificacao(<%=i+1%>,this,this.form);'></TD>
				     <td width="185" style="text-align: right;">              &nbsp;<%=sys.Util.lPad(String.valueOf(i+3),"0",3)%>.&nbsp;<input   name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="<%= RetornoNotificacaoBeanID.getNotificacao(i+2) %>" size="26"   maxlength="20" onChange='javascript:ValNumNotificacao(<%=i+2%>,this,this.form);'></TD>
					 <td style="text-align: right;">                          &nbsp;<%=sys.Util.lPad(String.valueOf(i+4),"0",3)%>.&nbsp;<input   name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="<%= RetornoNotificacaoBeanID.getNotificacao(i+3) %>" size="26"   maxlength="20" onChange='javascript:ValNumNotificacao(<%=i+3%>,this,this.form);'></TD>
				</tr>		
				<tr><td height="1" colspan="5" bgcolor="#8AAEAE"></td></tr>       
		    	 <%	}    		
				for (int i = RetornoNotificacaoBeanID.getListaNotificacoes().size(); i < Integer.parseInt(RetornoNotificacaoBeanID.getTotalNotificacoes()); i=i+4) {
		        %>			  	  
				<tr bgcolor="#d9f4ef"> 
					 <td width="185" style="text-align: right; height: 20px;">&nbsp;<%=sys.Util.lPad(String.valueOf(i+1),"0",3)%>.&nbsp;<input name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="" size="26" maxlength="20"   onChange='javascript:ValNumNotificacao(<%=i%>,this,this.form);'></TD>
					 <td width="185" style="text-align: right;">              &nbsp;<%=sys.Util.lPad(String.valueOf(i+2),"0",3)%>.&nbsp;<input name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="" size="26" maxlength="20"   onChange='javascript:ValNumNotificacao(<%=i+1%>,this,this.form);'></TD>
				       <td width="185" style="text-align: right;">            &nbsp;<%=sys.Util.lPad(String.valueOf(i+3),"0",3)%>.&nbsp;<input name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="" size="26" maxlength="20"   onChange='javascript:ValNumNotificacao(<%=i+2%>,this,this.form);'></TD>
					 <td style="text-align: right;">                          &nbsp;<%=sys.Util.lPad(String.valueOf(i+4),"0",3)%>.&nbsp;<input name="numNotificacao" onkeypress="javascript:MascarasNotif(this.form,this,'999999999-999');" type="text" value="" size="26" maxlength="20"   onChange='javascript:ValNumNotificacao(<%=i+3%>,this,this.form);'></TD>
				</tr>		
				<tr ><td height="1" colspan="5" bgcolor="#8AAEAE"></td></tr>       
		    	 <%	}  }
	   		 %>		    
		     </table>	
</div>		
</div>		
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>


<%  	
String msgErro     = RetornoNotificacaoBeanID.getMsgErro();
String msgOk       = RetornoNotificacaoBeanID.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>


</form>
</body>

<script>
function insertTableRow2(form, where,qNotif) {
<% if (RetornoNotificacaoBeanID.getListaNotificacoes().size()==0)
   {
%>
	while(theTableBody.rows.length>0) {
		removeRow(form); 
	}
	var newCell
	var newRow
	var strPadrao1="<tr bgcolor='#d9f4ef'>";
	var strPadrao2="<td width='185' style='text-align: right; height: 30px;' bgcolor='#8AAEAE'>";
	var strPadrao3="</tr>";


   var controle=0;
   newRow = theTableBody.insertRow(where)		
   newCell = newRow.insertCell(0)	
   newCell.innerHTML = strPadrao1;
   
   var conteudo="";
   for (i=0; i<qNotif; i++) 
   {  
     newCell = newRow.insertCell(controle);
     var indice=(i+1)+"";
     for (j=indice.length;j<3;j++)
     {
       indice="0"+indice;
     }
	 newCell.innerHTML = strPadrao2+"&nbsp;"+(indice)+".&nbsp;<input name='numNotificacao' onkeypress=javascript:MascarasNotif(this.form,this,'999999999-999',"+i+"); type='text' value='' size='20' maxlength='13' onChange='javascript:ValNumNotificacao("+i+",this,this.form)' ></td>"; 	 
	 controle++;	 
   	 if (controle==4)
   	 {
   	   newRow = theTableBody.insertRow(where);
	   controle=0;
	 }
   }
   newRow = theTableBody.insertRow(where);
   
<%
   }
%>

//document.all.codRetorno.focus();


	
}

</script>
	
	
</html>


