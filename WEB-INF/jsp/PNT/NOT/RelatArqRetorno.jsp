<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>



<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ResumoRetornoBeanID"     scope="session" class="PNT.NOT.ResumoRetornoBean"/> 
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'consultarAno':
   		if(fForm.ano.value == "")
   			alert("Digite Ano.");
   		else{
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  	}
	  break;
   case 'retornarAcompanhamento':
		fForm.acao.value="";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;   
   case 'R':
	    close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function apagaOrgao() {
	document.acompanhamentoEntrega.codOrgao.selectedIndex=0;	  
}
function apagaTodos() {
	document.acompanhamentoEntrega.todosOrgaos.checked=false;	  
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="acompanhamentoEntrega" method="post" action="" >
<%@ include file="../Cab.jsp" %>

<input name="acao" type="hidden" value=' '>	
<input name="codStatusAuto" type="hidden" value='<%=(String)request.getAttribute("statusAuto")%>'>
<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 80px; left:50px;" >  
  <table width="100%" cellpadding="0" cellspacing="1" >

        <button type="button" name="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consultarAno',this.form);"> <img src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>    
    
    <tr>
      <td></td>
      <td> &nbsp;&nbsp;<strong>Ano:</strong>
      
        <td align="left">
           &nbsp;<jsp:getProperty name="ResumoRetornoBeanID" property="popupString" /></td>
      
      

      </td>
      <td> &nbsp;&nbsp;<strong>Posi��o em:</strong>
          <input name="dtProc" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=AcompanhamentoEntregaBeanID.getDatProc()%>" >
      </td>
      
    </tr>
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>
 
<!--Div Erros-->
<%
String msgErro     = AcompanhamentoEntregaBeanID.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %><!-- Fim Rodap� -->

</form>
</body>
</html>