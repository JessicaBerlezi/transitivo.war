<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<jsp:useBean id="ProcessoBeanID" scope="session" class="PNT.ProcessoBean" />

<!-- Chama o Objeto da Tabela de Retornos -->
<jsp:useBean id="PenalidadeBeanID" scope="request" class="PNT.TAB.PenalidadeBean" />
  <jsp:setProperty name="PenalidadeBeanID" property="j_abrevSist" value="PNT" />  	 	     	 	  
  <jsp:setProperty name="PenalidadeBeanID" property="colunaValue" value="COD_PENALIDADE" /> 
  <jsp:setProperty name="PenalidadeBeanID" property="popupNome"   value="codPenalidade"  />  
  <jsp:setProperty name="PenalidadeBeanID" property="popupString" value="DSC_PENALIDADE,SELECT * FROM TPNT_PENALIDADE ORDER BY COD_PENALIDADE" />      


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script>var bProcessa=true;</script>
<html>
	<head>
		<%@ include file="../Css.jsp" %>
		<style type="text/css">
		   .linhas td{border: 1px solid #ff0000}
		   
		
		
		</style>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

		<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {   	  
	   case 'R':
		  close() ;   
		  break;
	   case 'AplicarPenalidade':
	      if (bProcessa==false)
	      { 
	        alert("Aplicando Penalidade em Processamento...");
	        return;
	      }
	      if (veCampos(fForm)==true) {		  
	        bProcessa=false;
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}


function comparaDatas(data2) {
	var valid = false;
	var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var today=new Date();
	var difference=today-dataB;//obt�m o resultado em milisegundos.
	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = true;
	return valid;
}  

function SomaProcesso() {
	n = 0 ;
	for (i=0;i<document.AplicarPenalidadeForm.processoPenalidade.length;i++) {
		if (document.AplicarPenalidadeForm.processoPenalidade[i].checked) n++;
	}
	document.all["qtdProcessoPenalidade"].value = n;
}

function marcaTodos(obj) {
	n=0
	for (i=0; i<document.all["AplicarPenalidadeForm"].processoPenalidade.length;i++) {
		document.all["AplicarPenalidadeForm"].processoPenalidade[i].checked = obj.checked ;
		if (document.all["AplicarPenalidadeForm"].processoPenalidade[i].checked) n++;
	}
	document.all["qtdProcessoPenalidade"].value = n;
}



function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	valid = ValDt(AplicarPenalidadeForm.datPenalidade,1) && valid	
	if ( (AplicarPenalidadeForm.qtdProcessoPenalidade.length==0) || (AplicarPenalidadeForm.qtdProcessoPenalidade.value=='0') )
	{
	  sErro+="Quantidade de Processos n�o selecionada.\n";
	  valid=false;
	}
	if ( (trim(AplicarPenalidadeForm.codPenalidade.value).length==0) || (AplicarPenalidadeForm.codPenalidade.value=='0') )
	{
	  sErro+="Penalidade n�o preenchida.\n";
	  valid=false;
	}
    if (comparaDatas(fForm.datPenalidade.value)==true){
       valid = false
       sErro += "Data n�o pode ser posterior a data atual. \n"
    }
	if (valid==false) alert(sErro) ;
	return valid ;
}


</script>
	</head>
	<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
		<form name="AplicarPenalidadeForm" method="post" action="">
		   <%@ include file="../Cab.jsp" %>
			<input name="acao" type="hidden" value=''>
			<!--IN�CIO_CORPO_sistema-->
			
<div id="recurso3" style="position:absolute; left:50px; top:80px; width:720px; height:20px; z-index:2; overflow: visible; visibility: visible;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
					<tr>
						<td width="15%" align="right">
							<strong>Penalidades :&nbsp;</strong></td>
						<td colspan="2" width="70%">
                        <jsp:getProperty name="PenalidadeBeanID" property="popupString" />
                        </td>
						<td width="2%"></td>									
						<td>
	    	            <button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AplicarPenalidade',this.form);">			  
		  	            <img src="<%= path %>/images/bot_ok.gif" width="26" height="19">
			            </button>
			            &nbsp;&nbsp;&nbsp;
			            <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
				  		<img src="<%= path %>/images/bot_retornar.gif" width="52" height="19" >
			  			</button>
						</td>
					</tr> 
					<tr>
					<td><input type="checkbox" name="todos" value="0" class="input" onClick="javascript: marcaTodos(this);" ><strong>Todos</strong></td>
					<td width="30%"><strong>N� de Processos :&nbsp;<input type="text" name="qtdProcessoPenalidade" size="5" maxlength="3" value='<%=0%>'  onkeypress="javascript:Mascaras(this,'999');" onFocus="javascript:this.select();" onChange="javascript:changeQtdNotif(this.form,this.value)"></td>
					<td width="30%"><strong>Data :&nbsp;&nbsp;</strong><input type="text" name="datPenalidade" size="12" maxlength="10" value='<%=sys.Util.formatedToday().substring(0,10)%>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"></td>
					<td></td>
					<td></td>
			    	</tr>
				</table>
				</div>
				
				<div id="titulo" style="position:absolute; left:50px; top:130px; width:720px; height:35px; z-index:2; overflow: visible; visibility: visible;">
					<table border="0" cellpadding="0" cellspacing="1" width="100%" align="center">
					<tr>
					<td width="60" rowspan="2" style="background-color:#c0e1df; text-align: center;"><strong>Seq</strong></td>
					<td width="120" rowspan="2" style="background-color:#c0e1df; text-align: center;"><strong>Processo</strong></td>
					<td width="70" rowspan="2" style="background-color:#c0e1df; text-align: center;"><strong>Data Proc.</strong></td>
					<td colspan="2" height="18" style="background-color:#c0e1df; text-align: center;"><strong>Nome</strong></td>
					<td style="background-color:#c0e1df; text-align: center;"><strong>Total Pontos</strong></td>
					</tr>
					
					<tr>
					<td width="170" height="18" style="background-color:#c0e1df; text-align: center;"><strong>CPF</strong></td>
					<td width="170" style="background-color:#c0e1df; text-align: center;"><strong>CNH/PGU</strong></td>
					<td style="background-color:#c0e1df; text-align: center;"><strong>Total Multas</strong></td>
					</tr>
					
				</table>
				</div>
								
				<div id="corpo" style="position:absolute; left:50px; top:168px; width:720px; height:165px; z-index:2; overflow: auto;">
				<table border="0" cellpadding="0" cellspacing="1" width="100%" align="center">
				<%
				int seq=0;
				String cor   = (seq%2==0) ? "#faeae5" : "#ffffff";
				for (int i = 0; i < ProcessoBeanID.getProcessos().size(); i++) {
					cor    = (seq%2==0) ? "#d9f4ef" : "#ffffff";					
				%>
					<tr>
						<td width="60" rowspan="2" style="background-color:<%=cor%>; text-align: center;"><input type="checkbox" name="processoPenalidade" value="<%=seq%>" class="sem-borda" style="height: 12px" onclick="javascript:SomaProcesso();" />&nbsp;&nbsp;<%=seq+1%></td>
						<td width="120" rowspan="2" style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getNumProcesso()%></td>
						<td width="70" rowspan="2" style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getDatProcesso()%></td>
						<td colspan="2" height="18" style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getNomResponsavel()%></td>
						<td style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getTotPontos()%></td>
					</tr>
					<tr>
						<td width="170" height="18" style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getNumCPF()%></td>
						<td width="170" style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getNumCNH()%> - <%=ProcessoBeanID.getProcessos(i).getUfCNH()%></td>
						<td style="background-color:<%=cor%>; text-align: center;"><%=ProcessoBeanID.getProcessos(i).getQtdMultas()%></td>
					</tr>
					<tr><td colspan="6" height="2" bgcolor="#8AAEAE"></td></tr>
				<%seq++;
				}
		        %>			  	  
				</table>
</div>		
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>


<%  	
String msgErro     = ProcessoBeanID.getMsgErro();
String msgOk       = ProcessoBeanID.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>


</form>
</body>

	
</html>


