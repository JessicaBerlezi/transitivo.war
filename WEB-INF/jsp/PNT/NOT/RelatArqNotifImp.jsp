<!--M�dulo : PNT.NOT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="ArquivoBeanID" scope="session" class="PNT.NOT.ArquivoBean" />
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<% String tituloConsulta = (String)request.getAttribute("tituloConsulta");
  if(tituloConsulta==null) tituloConsulta=""; 
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="RelatArqNitifImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha  = 999;
	int npag       = 0;
	int seq        = 0;	
	
	Iterator it = (ArquivoBeanID.getListaProcessos().iterator()) ;
	PNT.ProcessoBean   processo  = new PNT.ProcessoBean();					
	if (ArquivoBeanID.getListaProcessos().size()>0) { 
	
		while (it.hasNext()) {
			processo = (PNT.ProcessoBean)it.next() ;
			seq++;
			contLinha++;
			if (contLinha>23) {
				contLinha = 1;
				npag++;	
				if (npag!=1){
		%>			 					
				<%@ include file="../rodape_imp.jsp" %>
				<div class="quebrapagina"></div>
				<% } %>						
			<%@ include file="../Cab_imp.jsp" %>				
			<!--INICIO CABEC DA TABELA-->
		<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center" class="semborda">
			<tr>
				 <td width="5%" rowspan="3" align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="19%" align="center" bgcolor="#999999" class="td_linha_left" style="line-height:20px;"><font color="#000000"><b>PROCESSO</b></font></td>		 
				 <td width="12%" align="center" bgcolor="#999999" class="td_linha_left"><font color="#000000"><b>DATA PROC. </b></font></td>
				 <td width="30%" align="center" bgcolor="#999999" class="td_linha_left"><font color="#000000"><b>NOME</b></font></td>
				 <td width="16%" align="center" bgcolor="#999999" class="td_linha_left"><font color="#000000"><b>CPF</b></font></td>
		         <td align="center" bgcolor="#999999" class="td_linha_left" style="line-height:20px;"><font color="#000000"><b>CNH</b></font></td>
		    </tr>
		</table>
			<% } %>	
			
			
			
					  


	<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center" class="semborda">			 		
		
			
		<tr> 
			<td width="5%" rowspan="2" align="center" style="padding-right: 2px"><%= seq %></td>		 			
			<td colspan="2"  style="line-height:17px; padding-left: 2px"><b><%=processo.getNumProcesso() %></b></td>
			<td width="12%" align="center"  style="padding-right: 2px"><%=processo.getDatProcesso() %></td>
			<td width="30%"  style="padding-left: 2px"><%=processo.getNomResponsavel() %></td>
			<td width="16%"  style="padding-right: 2px;padding-left: 2px" ><%=processo.getNumCPFEdt() %></td>
            <td style="padding-right: 2px"><%=processo.getTipCNHDesc()%>&nbsp;<%=processo.getNumCNH() %>&nbsp;<%=processo.getUfCNH() %></td>
		</tr>
		
		
		
		<tr> 
			<td width="10%" style="line-height:20px;padding-left: 2px"><%=processo.getTipProcessoDesc()%></td>		 			
			<td width="9%" align="center"  style="line-height:17px;padding-right: 2px">QTD. NOTIF.:&nbsp;<%=processo.getTotNotificacoes()%></td>
			<td align="center"  style="padding-right: 2px">QTD. INFR.:&nbsp;<%=processo.getQtdMultas()%></td>
			<td style="padding-left: 2px">TOTAL PONTOS :&nbsp;<%=processo.getTotPontos() %></td>
			<td style="padding-right: 2px;padding-left: 2px" >PRAZO REC.:&nbsp;<%=processo.getUltDatDefesa()%></td>
            <td style="padding-right: 2px">PRAZO ENTR. CNH :&nbsp;<%=processo.getUltDatEntregaCNH()%></td>
		</tr>

		<tr><td colspan="7" height="2" bgcolor="#000000"></td></tr>
		
	</table>   	
  <%
    } 
  }else { 
%>
	<div class="quebrapagina"></div>
	<%npag=1;%>
	<%@ include file="../Cab_imp.jsp" %>		
   <tr><td colspan="4" height="35"></td></tr>	 	  
		 <tr>
			  <td width="30"></td>
			  <td width="30"></td>			  			  			  
			  <td width="150"  align = "center">Nenhum processo localizado</td>
			  <td></td>
   </tr>
   </table>
    <%@ include file ="../rodape_imp.jsp" %>
<%} %>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center" class="table_linha_top semborda">
	    <tr>	
		  <td width="36%" height="17">&nbsp;</td>				  			  			  
		  <td style="padding-left: 2pt"><b>TOTAL DE NOTIFICA��ES :&nbsp;&nbsp;<%=sys.Util.formataIntero(String.valueOf(ArquivoBeanID.getTotNotificacoes()))%></b></td>
	   </tr>		   			     
   </table>
  <%@ include file="../rodape_imp.jsp" %>
</form>
</body>
</html>
