<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="ArquivoBeanID" scope="session" class="PNT.NOT.ArquivoBean" />
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>
	
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>	
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,codigo,narq,fForm) {
			 switch (opcao) {
			   case 'ImprimeArquivo':
					if (atuRadio(fForm.indOrdem,fForm.ordem)) {
					  alert("Ordem do Relat�rio n�o selecionada. \n")
					}
					else {
						fForm.acao.value=opcao;
					   	fForm.target= "_self";
					    fForm.action = "acessoTool";  
					   	fForm.submit();	  		  
					}
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'O':
					  document.forms[0].codArquivo.value="";			   
					  document.forms[0].nomArqEsc.value="";			   					  
			   		  if (document.layers) fForm.layers["Ordem"].visibility='hide' ; 
				      else                 document.all["Ordem"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
					  document.forms[0].codArquivo.value=codigo;
					  document.forms[0].nomArqEsc.value=narq;					  
			   		  if (document.layers) fForm.layers["Ordem"].visibility='show' ; 
				      else                 document.all["Ordem"].style.visibility='visible' ; 
				  break;	  
			  }
			}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="RelatArqNotifForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		<input type="hidden" name="codArquivo" value="">
		<input type="hidden" name="ordem" value="">		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:75px; height:260px;">
		<div class="divTitulos" id="titStatus" style=" height:40px;">
			<table cellspacing="1" class="titulos" >  	   		
			<tr> 
      			<td rowspan="2" width="208">Arquivo</td>	 	  
      			<td rowspan="2" width="40">Num<br/>Prot.</td>					
      			<td colspan="2">Datas</td>					
      			<td rowspan="2" width="38">Cod.<br/>Stat.</td>
      			<td colspan="2">Infra��es</td>					
      			<td colspan="2">Qtd. Reg.</td>
 			</tr>

			<tr> 
   				<td width="74">Gera��o</td>					
   				<td width="74">Proc.</td>					
   									
   				<td width="74">De</td>					
   				<td width="74">At�</td>					
   				<td width="44">Lidos</td>					
   				<td>Canc.</td>
 			</tr>
		  </table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoPenalidade" style="height: 220px;" >
    		<table id="fevento" cellspacing="1" style="width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;">
     			<%	
	PNT.NOT.ArquivoBean myArq = new PNT.NOT.ArquivoBean();
	String cor = "";
	for (int seq=0;seq<ArquivoBeanID.getListaArquivos().size();seq++) {
			myArq = ArquivoBeanID.getListaArquivos(seq);
			cor   = (seq%2==0) ?    "#ffffff" : "#d9f4ef" ;			
				%>
				<tr bgcolor="<%= cor %>" style="cursor:hand" onClick="javascript: valida('V','<%= myArq.getCodArquivo() %>','<%= myArq.getNomArquivo() %>',this.form);">				
      				<td width="208"><%= myArq.getNomArquivo() %></td>		 	  
      				<td width="40" align="right"><%= myArq.getNumProtocolo() %></td>					
      				<td width="74" align="center"><%= myArq.getDatGeracao() %></td>					
      				<td width="74" align="center"><%= myArq.getDatProcessamento() %></td>					
      				<td width="38" align="center"><%= myArq.getCodStatus() %></td>						
      				<td width="74" align="center"><%= myArq.getDatIniPnt() %></td>					
      				<td width="74" align="center" ><%= myArq.getDatFimPnt() %></td>					
      				<td width="44" align="right"><%= sys.Util.formataIntero(String.valueOf(myArq.getQtdLinha())) %></td>					
      				<td align="right"><%= sys.Util.formataIntero(String.valueOf(myArq.getQtdLinhaCancelada())) %></td>
 				</tr>
		<% 	}	%>
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  
<div id="Ordem" style="background-color:#ffffff; border:1px solid #000000; padding:10px; position: absolute; top:75px; left:200px; width:400px; height: 220px; visibility:hidden; z-index: 999">      
			<table cellspacing="0" width="100%" >  	   		
			<tr> 
      			<td colspan=4 style="font-size:11px; text-align:center;" >RELAT�RIO DE PROCESSOS DO ARQUIVO		 	  
				</td>				
 			</tr>
			<tr> 
      			<td colspan=4 height="10px">				</td>				
 			</tr>
			<tr> 
      			<td colspan=4 style="font-size:11px; text-align:center;" >
					<input type="text" name="nomArqEsc" size="30" readonly value="">		
				</td>				
 			</tr>
			<tr> 
      			<td colspan=4 height="20px">				</td>				
 			</tr>
			<tr> 
				<td width="15%"></td>							
				<td width="35%"><input type="radio" name="indOrdem" value="1"  class="sem-borda" style="height: 14px; width: 14px;">Processo</td>
				<td width="35%"><input type="radio" name="indOrdem" value="2"  class="sem-borda" style="height: 14px; width: 14px;">CNH</td>				
				<td width="15%"></td>											
 			</tr>
			<tr> 
      			<td colspan=4 height="5px">				</td>				
 			</tr>
			<tr> 
				<td></td>														
				<td><input type="radio" name="indOrdem" value="3"  class="sem-borda" style="height: 14px; width: 14px;">CEP</td>
				<td><input type="radio" name="indOrdem" value="4"  class="sem-borda" style="height: 14px; width: 14px;">CPF</td>				
				<td></td>											
 			</tr>
			<tr> 													
      			<td colspan=4 height="5px">				</td>				
 			</tr>
			<tr> 
				<td></td>														
				<td><input type="radio" name="indOrdem" value="5" Checked class="sem-borda" style="height: 14px; width: 14px;">Alfabetica</td>
				<td></td>				
				<td></td>															
 			</tr>

			<tr> 
      			<td colspan=4 height="20px">				</td>				
 			</tr>

	        <tr> 
	          <td colspan=2 align="center">
			  	<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimeArquivo',' ',' ',this.form);">	
		    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>
			 </td>	
	          <td colspan=2 align="left">
			  	<button type="button" NAME="Ok"   style="width: 55px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('O',' ',' ',this.form);">	
		    	<IMG src="<%= path %>/images/bot_retornar.gif" align="left" ></button></td>
			 </td>	
 			</tr>
 			</table>
</div>

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	</form>
</BODY>
</HTML>


	