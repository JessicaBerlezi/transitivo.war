<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="ArquivoBeanID" scope="session" class="PNT.NOT.ArquivoBean" />
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>
	
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>	
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'ImprimeCEP':
			   	if (atuRadio(fForm.selCep)) {
					  alert("Nenhum registro selecionado. \n")
					}
					else {
						fForm.acao.value=opcao;
					   	fForm.target= "_self";
					    fForm.action = "acessoTool";  
					   	fForm.submit();	  		  
					}
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'O':
					  document.forms[0].codArquivo.value="";			   
					  document.forms[0].nomArqEsc.value="";			   					  
			   		  if (document.layers) fForm.layers["Ordem"].visibility='hide' ; 
				      else                 document.all["Ordem"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
					  document.forms[0].codArquivo.value=codigo;
					  document.forms[0].nomArqEsc.value=narq;					  
			   		  if (document.layers) fForm.layers["Ordem"].visibility='show' ; 
				      else                 document.all["Ordem"].style.visibility='visible' ; 
				  break;	  
			  }
			}
function atuRadio(objsel) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	
	if(<%=ArquivoBeanID.getListaProcessos().size()%> > 1) {
		for (i=0; i<objsel.length; i++) {
	    	if (objsel[i].checked) {
			   naochq = false ;
	<!--	   objind.value = objradio[i].value;-->
			   break ;
			   }
			   
		}
	}else{	
		if (objsel.checked) {
			   naochq = false ;
		}		
	}	
	return naochq ;
}

function SomaNotificacoes(fForm) {
	somaCeps=0;
	somaNotificacoes = 0;
	somaNotificacoesAtual=0;
	
	if(<%=ArquivoBeanID.getListaProcessos().size()%> > 1) {
		 for (i=0;i<fForm.selCep.length;i++)
	     {
	         if(fForm.selCep[i].checked==true){
	          somaNotificacoesAtual=fForm.numNotifs[i].value
	          somaCeps +=1;
	          somaNotificacoes=(parseInt(somaNotificacoes)) + (parseInt(somaNotificacoesAtual));
	
	          }
	     } 
	     fForm.numNotificacoes.value=somaCeps +" / "+somaNotificacoes;
	}else{
		somaNotificacoesAtual=fForm.numNotifs.value
	    somaCeps +=1;
	    somaNotificacoes=(parseInt(somaNotificacoes)) + (parseInt(somaNotificacoesAtual));
		fForm.numNotificacoes.value=somaCeps +" / "+somaNotificacoes;	
	}
}


		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="RelatArqNotifCepForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao"   value="">
		<input type="hidden" name="codCep" value="">
		<input type="hidden" name="ordem"  value="3">		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:100px; height:160px;left:150px;right: 95px;">
        
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin-bottom: 3px">
        <tr>
        <td colspan="3" style="font-family: verdana; font-size: 9px; font-weight: bold; letter-spacing: 2px;">
        &nbsp;Arquivo:&nbsp;&nbsp;<%=ArquivoBeanID.getNomArquivo()%></td>
        
        <td><button type="button" name="Ok" style="width: 30px; height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimeCEP',this.form);"> <img src="/SMIT/images/bot_ok.gif" align="left" ></button>
        </td>
        </tr>
        
        
        <tr>
       <td>&nbsp;Total Notifica��es n�o impressas:&nbsp;&nbsp;<strong><%=ArquivoBeanID.getTotNotificacoes()%></strong>&nbsp;&nbsp;&nbsp;</td>
        <td style="text-align: left">Total de CEPs / Notifica��es selecionados:&nbsp;&nbsp;<input readonly="readonly"  maxlength="10" size="10" type="text" name="numNotificacoes" value="0" class="sem-borda" style="background-color:transparent; height:15px; font-weight: bold"></td>
        
        
        </tr>
        
        </table>
        
		<div class="divTitulos" id="titStatus">
			<table cellspacing="1" class="titulos" >  	   		
			<tr> 
   				<td width="30">Seq.</td>		 	  
   				<td width="150">CEP</td>					
  				<td>Total Notifica��es</td>					
   				
 			</tr>
		  </table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoPenalidade" style="height: 180px;" >
    		<table id="fevento" cellspacing="1" style="width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;">
     			<%	
	PNT.ProcessoBean myCep = new PNT.ProcessoBean();
	String cor = "";
	for (int seq=0;seq<ArquivoBeanID.getListaProcessos().size();seq++) {
		    myCep = ArquivoBeanID.getListaProcessos().get(seq);
			cor   = (seq%2==0) ?    "#ffffff" : "#d9f4ef" ;			
				%>
				<tr bgcolor="<%= cor %>">				
      				<td width="30" align="center"><input type="checkbox" class="sem-borda" name="selCep" value="<%=seq%>" onClick=javascript:SomaNotificacoes(this.form)></td>		 	  
      				<td width="150" align="center"><input type="hidden" name="numCep" value="<%=myCep.getCep()%>"><%=myCep.getCep()%></td>					
      				<td align="center"><input type="hidden" name="numNotifs" value="<%=myCep.getTotNotificacoes()%>"><%= myCep.getTotNotificacoes()%></td>					
 				</tr>
		<% 	}	%>
			</table>
		</div>
</div>

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	</form>
</BODY>
</HTML>


	