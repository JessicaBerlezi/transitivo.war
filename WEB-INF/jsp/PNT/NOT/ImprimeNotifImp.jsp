<!--M�dulo : PNT.NOT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.List" %>
<%@page import="sys.Util"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="PNT.NotificacaoBean" %>
<%@ page import="PNT.ProcessoBean" %>
<%@ page import="PNT.MultasBean" %>
<%@ page import="PNT.ProcessoAntBean" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="ArquivoBeanID" scope="session" class="PNT.NOT.ArquivoBean" />
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="NotificacaoForm1" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<% 

boolean Impressa  = false;
boolean  primPag = true;
ProcessoBean ProcessoBeanId  = new ProcessoBean();					
NotificacaoBean notificacao  = new NotificacaoBean();
ProcessoAntBean procAnt      = new ProcessoAntBean();									
List<ProcessoAntBean> lProcAnt = new ArrayList<ProcessoAntBean>();
MultasBean multa  = new MultasBean();	
List<MultasBean> lMultas = new ArrayList<MultasBean>();
String ultSeqListMulta = "";

for (int p=0;p<ArquivoBeanID.getListaProcessos().size();p++) { 
	
	ProcessoBeanId = ArquivoBeanID.getListaProcessos().get(p);		
	
	lProcAnt = ProcessoBeanId.getProcAnteriores();
	for (int i = lProcAnt.size();i<3;i++) {
		procAnt= new ProcessoAntBean();
		lProcAnt.add(procAnt);
	}
	if (lProcAnt.size()>3) {
		lProcAnt = new ArrayList<ProcessoAntBean>();
		for (int i = 0;i<3;i++) {
			lProcAnt.add(ProcessoBeanId.getProcAnteriores().get(i));
		}
	}	
	for (int n=0;n<ProcessoBeanId.getNotificacoes().size();n++) { 
		notificacao  = ProcessoBeanId.getNotificacoes().get(n);
		// buscar multas da notificacao
		lMultas = new ArrayList<MultasBean>();
		ultSeqListMulta = "";
		for (int i = 0;i<ProcessoBeanId.getMultas().size();i++) {
			multa = new MultasBean();	
			multa = ProcessoBeanId.getMultas().get(i);
			if (multa.getSeqNotificacao().equals(notificacao.getSeqNotificacao())) {
				lMultas.add(multa);
				ultSeqListMulta = multa.getSeqMultaNotificacao();
				}
		}
		// analisar o tamanho

		if ("001".equals(notificacao.getSeqNotificacao())) {     // Maximo 8			
			for (int i = lMultas.size();i<7;i++) {
				multa = new MultasBean();
				lMultas.add(multa);
			}
		}
		else {
			if ("000".equals(notificacao.getSeqNotificacao())==false) {     // Maximo 18
				for (int i = lMultas.size();i<18;i++) {
					multa = new MultasBean();
					lMultas.add(multa);
				}
			}	
		}
	    Impressa=true;
		if ("0".equals(notificacao.getTipoNotificacao())) {
			if (("000".equals(notificacao.getSeqNotificacao())) ){
	    %>
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<% if ("S".equals(ProcessoBeanId.getTipProcesso()) || "M".equals(ProcessoBeanId.getTipProcesso())) { %>
					<%@ include file="../NotifBl12InstSdd.jsp" %> 
				<% } else { %>
					<%@ include file="../NotifBl12Inst.jsp" %> 
				<% } %>	
				<!--N�O POSSUIA BLOCO C-->
				<%@ include file="../NotifBl13.jsp" %>
								

				<DIV class="quebrapagina"></DIV>

				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Inst.jsp" %> 
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			

			<%}else if (("001".equals(notificacao.getSeqNotificacao())) ) {%> 
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	

			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl13.jsp" %> 

				<DIV class="quebrapagina"></DIV>


			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15.jsp" %> 
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %>

				 				
			<%}else { 	%>
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	

			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl13Anexo.jsp" %> 

				<DIV class="quebrapagina"></DIV>

			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Anexo.jsp" %>
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 				 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %>												

			 <% }    %>   
		<%}%> 
		<%
		if ("1".equals(notificacao.getTipoNotificacao())){%> 
			<%--@ include file="NotificacaoAvisoDefDefesa.jsp" --%>   
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12Result.jsp" %> 
				<!--N�O POSSUI BLOCO C-->				
				<%@ include file="../NotifBl13Result.jsp" %> 
				<DIV class="quebrapagina"></DIV>
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Vazio.jsp" %> 
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			
		<%}%>
		<%	
		if ("2".equals(notificacao.getTipoNotificacao())) { %> 
			<%--@ include file="NotificacaoPenalidade.jsp" --%> 
            <%
			if (("000".equals(notificacao.getSeqNotificacao())) ){
	        %>
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<% if ("S".equals(ProcessoBeanId.getTipProcesso()) || "M".equals(ProcessoBeanId.getTipProcesso())) { %>
					<%@ include file="../NotifBl12InstPenalSdd.jsp" %> 
				<% } else { %>
					<%@ include file="../NotifBl12InstPenal.jsp" %> 
				<% } %>	
				<!--N�O POSSUI BLOCO C-->				

				<DIV class="quebrapagina"></DIV>
			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Penal.jsp" %>
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			
			<%}else if (("001".equals(notificacao.getSeqNotificacao())) ) {%> 
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12Penal.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl13.jsp" %> 

				<DIV class="quebrapagina"></DIV>

			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Penal.jsp" %>
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			
			<%}else { 	%>
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12Penal.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl13Anexo.jsp" %> 
				<DIV class="quebrapagina"></DIV>
			 	<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Penal.jsp" %>
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 							
			 <% }    %>   
		<%}%>
		<%
		if ("3".equals(notificacao.getTipoNotificacao())) { %> 
			<%--@ include file="NotificacaoAvisoRecurso.jsp" --%>		
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12Result.jsp" %> 
				<!--N�O POSSUIA BLOCO C-->				
				<%@ include file="../NotifBl13Result.jsp" %> 
				<DIV class="quebrapagina"></DIV>
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Vazio.jsp" %> 
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			
		<%}%>	
		<%
		if ("4".equals(notificacao.getTipoNotificacao())) { %> 
			<%--@ include file="NotificacaoEntrega.jsp" --%>		
	    		<% if (primPag==false) { %>
				<DIV class="quebrapagina"></DIV>
				<% } else {
					primPag=false;
				}
				%>	
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl11.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl12Apreensao.jsp" %> 
				<!--N�O POSSUIA BLOCO C-->				
				<%@ include file="../NotifBl13Result.jsp" %> 				
				<DIV class="quebrapagina"></DIV>
				<!--IN�CIO BLOCO A-->
				<%@ include file="../NotifBl14.jsp" %>  
				<!--IN�CIO BLOCO B-->
				<%@ include file="../NotifBl15Vazio.jsp" %> 
				<!--IN�CIO BLOCO B2-->
				<%@ include file="../NotifBl15B.jsp" %> 
				<!--IN�CIO BLOCO C-->
				<%@ include file="../NotifBl16.jsp" %> 			
		<%}%>			    		
    <%}%>		    
<%}%>		    
<% if (Impressa==false) {
%>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center" class="semborda">			 		<tr> 
	<tr heigth="100">
	  <td colspan=4></td>
   </tr>

	<tr>
	  <td width="30"></td>
	  <td width="30"></td>			  			  			  
	  <td width="150" class="fontmaior" align = "center">Nenhum processo localizado</td>
	  <td></td>
   </tr>
   </table>
<%
}
%>
</form>

</body>
</html>
