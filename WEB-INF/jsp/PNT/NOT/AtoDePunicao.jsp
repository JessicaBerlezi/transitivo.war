<!--Módulo : PNT.NOT
	Versão : 1.0
	Atualizações:
-->	
<!-- Abre a Sessao -->
<%@ page import="java.util.Date"%>
<%@ page session="true" %>
<%@ page import="sys.Util"%>
<%@ page import="PNT.ProcessoBean" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>


<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="NotificacaoForm1" method="post" action="">
<!-- Inicio Div Detalhes variáveis impressao -->	 

<!-- Quebra de página -->
	<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda" >
	     <tr> 
	       <td height="30" align="center" valign="middle" class="fontmedia">&nbsp;</td>
	     </tr>
	</table>
	<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" style="border: 0px none; border-top: 1px dotted #000000;">
		  <tr> 
			 <td height="1"></td>
		  </tr>
	</table>
	<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
	  <tr>
	    <td width="50%" height="20" class="fontmenor"><strong>&nbsp;&nbsp;DESTAQUE 
	      AQUI</strong></td>
	  </tr>
	</table>
<!-- Fim quebra de página -->

<!-- Cabeçalho -->
	
	<div class="fontmedia" style="margin-bottom: 10px; margin-top: 10px; text-align:center;text-indent: 0px; line-height: 11px; font-weight:bold">
	<img src="<%= path %>/images/logo.gif" width="65" height="60" align="top" ><br />
	
	GOVERNO DO RIO DE JANEIRO<br />
	SECRETARIA DE ESTADO DA CASA CIVIL<br />
	DEPARTAMENTO DE TR&Acirc;NSITO DO ESTADO DO RIO DE JANEIRO<br /><br />
	ATO DO PRESIDENTE DO DETRAN/RJ
	</div>
	
<!-- Fim cabeçalho -->

<!-- Início texto -->
<div style="font-size: 12px;margin: 30px; text-align:justify;text-indent: 0px; line-height: 16px;">
	Em face da Instaura&ccedil;&atilde;o do Processo de Suspens&atilde;o do Direito de Dirigir e em virtude do 
	<strong>condutor n&atilde;o ter apresentado Defesa Pr&eacute;via</strong>  
	deixando de exercer seu direito fundamental ao contradit&oacute;rio e  &agrave; ampla defesa (artigo 5&ordm;, inciso LV da CRFB c/c artigo 15 da Resolu&ccedil;&atilde;o CONTRAN n.&ordm; 182/2005), 
	<strong>RESOLVO</strong>:
</div>

<div style="font-size: 12px;margin: 30px; margin-left: 60px; text-align:justify;text-indent: 0px; line-height: 16px;">
	1- <span style="font-weight:bold;">Aplicar a Penalidade de Suspens&atilde;o do Direito de Dirigir, conforme Processo Administrativo 
		n.&ordm; 
		<%=ProcessoBeanId.getNumProcesso() %>,
		</span> ao motorista abaixo relacionado, pelo prazo de <strong>
		<%=ProcessoBeanId.getDscPenalidadeRes() %> 
		+ Curso de Reciclagem - CRCI,</strong> conforme determina no artigo 261 do C&oacute;digo 
	  	de Tr&acirc;nsito Brasileiro (Lei n.&ordm; 9.503/97) combinado com o disposto no artigo 3&ordm; da Resolu&ccedil;&atilde;o 
	  	CONTRAN n.&ordm; 182 de 09 de setembro de 2005.
</div>

<div style="font-size: 12px;margin: 30px; text-align:justify;text-indent: 0px; line-height: 20px; font-weight:bold;">
	<strong>CONDUTOR(A): </strong><%=ProcessoBeanId.getNomResponsavel() %>
	<br />
	<strong>CPF n.&ordm;: </strong><%=ProcessoBeanId.getNumCPFEdt() %>
</div>

<div style="font-size: 12px;margin: 30px; margin-left: 60px; text-align:justify;text-indent: 0px; line-height: 16px;">
2- Dar ci&ecirc;ncia ao motorista que:
</div>

<div style="font-size: 12px;margin: 30px; margin-left: 60px; text-align:justify;text-indent: 0px; line-height: 16px;">
	a)	<strong>O condutor poder&aacute; dar in&iacute;cio ao cumprimento das Penalidades</strong>, mediante <strong>ENTREGA</strong> da 
		<strong>Carteira Nacional de Habilita&ccedil;&atilde;o </strong> &ndash; CNH, acautelando-a junto ao N&uacute;cleo de Documentos Apreendidos
		 &ndash; NUDA, da Diretoria de Habilita&ccedil;&atilde;o deste Departamento de Tr&acirc;nsito, ou nos Postos de Habilita&ccedil;&atilde;o 
		 <strong>ou ainda apresentar recurso</strong> contra penalidade ora imposta, &agrave; Junta Administrativa de Recurso de 
		 Infra&ccedil;&otilde;es &ndash; JARI, que funciona junto a este &oacute;rg&atilde;o, conforme previsto nos artigos 282, 
		par&aacute;grafo 4&ordm; e 285, do CTB c/c o artigo 17 da Resolu&ccedil;&atilde;o do CONTRAN 182 de 09 de setembro de 2005;
		<br />
	b)	<strong>Caso ocorra o n&atilde;o provimento do recurso</strong> pela Junta Administrativa de Recursos de Infra&ccedil;&otilde;es &ndash; 
		JARI, o condutor poder&aacute; apresentar recurso contra a penalidade imposta junto ao <strong>Conselho Estadual de Tr&acirc;nsito &ndash;
		CETRAN</strong>, conforme previsto no artigo 289, II, do CTB;
		<br />
	c)	<strong>Esgotadas as Inst&acirc;ncias Administrativas de Julgamento</strong>, dever&aacute; <strong>ENTREGAR</strong> sua Carteira de 
		Habilita&ccedil;&atilde;o ao DETRAN/RJ e <strong>iniciar</strong> o cumprimento da puni&ccedil;&atilde;o imposta;
		<br />
	d)	<strong>Conduzir ve&iacute;culo automotor de qualquer esp&eacute;cie violando a suspens&atilde;o</strong> constitui infra&ccedil;&atilde;o 
		de tr&acirc;nsito ao artigo 162, II sujeitando o infrator &agrave; penalidade de multa, al&eacute;m da Cassa&ccedil;&atilde;o de 
		Habilita&ccedil;&atilde;o &ndash; art. 263, inciso I, do CTB e ainda poder&aacute; constituir, 
		em tese, crime de tr&acirc;nsito &ndash; art. 307, todos do CTB;
</div>

<div style="font-size: 12px;margin: 30px; margin-bottom:10px; margin-left: 60px; text-align:justify;text-indent: 0px; line-height: 16px;">
	3-	<strong>Condicionar a libera&ccedil;&atilde;o da Carteira Nacional de Habilita&ccedil;&atilde;o,</strong> ap&oacute;s o cumprimento 
	da penalidade de Suspens&atilde;o, &agrave; apresenta&ccedil;&atilde;o do comprovante de aprova&ccedil;&atilde;o em <strong>Curso de 
	Reciclagem &ndash; CRCI</strong>, conforme previsto no &sect 2&ordm; do art. 261, c/c inciso II do art. 268 do CTB.
</div>

<div style="text-align:center;">
	<img src="<%= path %>/images/assinaturadigital.JPG" width="220" height="138" align="top" ><br />
</div>

<div style="font-size: 12px;margin: 30px; text-align:center;text-indent: 0px; line-height: 16px;">
	<%=Util.dataPorExtenso() %>
	
</div>



<!-- Fim texto -->

</form>
</body>
</html>
