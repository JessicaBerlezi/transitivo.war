<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<table class="semborda" width="100%" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td height="10"></td> 
</tr>
</table>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
  <td height="565" align="center" valign="top" style="padding-left: 20px; padding-right:20px;"><p class="style1" style="margin-top: 10px;">
  NOTIFICA&Ccedil;&Atilde;O DE APREENS&Atilde;O DE HABILITA&Ccedil;&Atilde;O </p>
    <div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 50px; line-height: 30px">
    Fica V.S.&ordf; notificada para entregar sua Carteira Nacional de Habilita&ccedil;&atilde;o,
     CNH, at&eacute; <strong><%= ProcessoBeanId.getUltDatEntregaCNH() %></strong>,
      conforme estabelecido no processo administrativo n&ordm; <%= ProcessoBeanId.getNumProcesso() %>
     de Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir, em face de ter
     <% if ("P".equals(ProcessoBeanId.getTipProcesso())) { %>
      alcan&ccedil;ado o limite de 20 pontos determinado no &sect; 1&ordm;, 
      do art. 261 do C&oacute;digo de Tr&acirc;nsito Brasileiro, CTB.
	<% } else { %>
	transgredido as normas estabelecidas no C&oacute;digo de Tr&acirc;nsito Brasileiro, CTB, 
    cujas infra&ccedil;&otilde;es prev&ecirc;em, de forma espec&iacute;fica, a 
    penalidade de &quot;Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir&quot;.
	<% } %>      
    </div>
   	<% if (ProcessoBeanId.getDatApresentaRecurso().length()>0) { %>
	    <div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 50px; line-height: 30px">
		<em style="letter-spacing:-1px;">
		O Recurso apresentado em <%= ProcessoBeanId.getDatApresentaRecurso() %> foi considerado improcedente
		 pela Junta Administrativa de Recursos de Infra&ccedil;&otilde;es, JARI.</em>.
		</div>
   <% } %>

<div style="margin-bottom: 10px; margin-top: 10px;">
	<table width="100%" style="border: 2px solid #000000;margin-left: -1px; margin-right: -1px">
	  <tr>
		  <td style="padding-left: 3px;padding-right: 4px;font-size:11px"><strong>PENALIDADES:</strong></td>
		  <td style="text-align:justify">
			  	<span class="fontmaior"><%= ProcessoBeanId.getDscPenalidade() %></span>
		  </td>
	  </tr>
  	</table>		  
</div>	
	
<div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 50px; line-height: 30px">
V.S.&ordf; dever&aacute; entregar sua Carteira Nacional de Habilita&ccedil;&atilde;o,
 CNH, ao Detran-RJ, Se��o de Documentos Apreendidos - SDA, na Avenida Presidente Vargas, 817 / Sobreloja - Centro - RJ, podendo ainda ser entregue nos Postos de Habilita&ccedil;&atilde;o ou CIRETRANs, at&eacute; <strong><%= ProcessoBeanId.getUltDatEntregaCNH() %></strong>. 
  Excedendo este prazo e sendo o infrator flagrado conduzindo ve&iacute;culo, ser&aacute; 
  instaurado Processo Administrativo de Cassa&ccedil;&atilde;o do Direito de Dirigir
   nos termos do art. 263, I do CTB</span>.
</div>
<div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 50px; line-height: 30px">
A CNH ficar&aacute; apreendida e ser&aacute; devolvida ao infrator depois de cumprido
 o prazo de Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir e comprovada a
  realiza&ccedil;&atilde;o do curso de reciclagem.
</div>
  </td>
</tr>
</table>
