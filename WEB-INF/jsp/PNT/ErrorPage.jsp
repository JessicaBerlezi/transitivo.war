<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page isErrorPage="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.*, java.io.*" %>
<HTML>
<head><title>SISTEMAS - Pagina de Erro</title></head>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    close() ;
		break;
  }
}
</script>
<BODY>
<jsp:include page="Cab.jsp" flush="true" />
   <div id="ACESSOErro" style="LEFT: 35px; POSITION: absolute; TOP: 95px; WIDTH: 100%; Z-INDEX: 0">      
     <H1>        Pagina de Erro</H1>
     <b><%= exception %></b><BR>
     <br><a href="#" onClick="javascript: valida('R',this.form);">Voltar</a>
	 <%=makeErrorReport(request, exception) %>
   </div>
</BODY>
</HTML>
<%! 
  public String makeErrorReport (HttpServletRequest req, Throwable e) {
    StringBuffer buffer = new StringBuffer();
    reportException(buffer, e);
    reportRequest(buffer, req);
    reportParameters(buffer, req);
    reportHeaders(buffer, req);
    //reportCookies(buffer, req);
    return buffer.toString();
  }
  
  public void reportException (StringBuffer buffer, Throwable e) {
    StringWriter writer = new StringWriter();
    e.printStackTrace(new PrintWriter(writer));
    buffer.append(writer.getBuffer());
    buffer.append('\n');
  }

  public void reportRequest (StringBuffer buffer, HttpServletRequest req) {
    buffer.append("Request: ");
    buffer.append(req.getMethod());
    buffer.append(' ');
    buffer.append(req.getRequestURL());
    String queryString = req.getQueryString();
    if (queryString != null) {
      buffer.append('?');
      buffer.append(queryString);
    }
    buffer.append("\nSession ID: ");
    String sessionId = req.getRequestedSessionId();
    if (sessionId == null) {
      buffer.append("none");
    } else if (req.isRequestedSessionIdValid()) {
      buffer.append(sessionId);
      buffer.append(" (from ");
      if (req.isRequestedSessionIdFromCookie())
        buffer.append("cookie)\n");
      else if (req.isRequestedSessionIdFromURL())
        buffer.append("url)\n");
      else
        buffer.append("unknown)\n");
    } else {
        buffer.append("invalid\n");
    }
  }

  public void reportParameters (StringBuffer buffer, HttpServletRequest req) {
    Enumeration names = req.getParameterNames();
    if (names.hasMoreElements()) {
      buffer.append("Parameters:\n");
      while (names.hasMoreElements()) {
        String name = (String) names.nextElement();
        String[] values = req.getParameterValues(name);
        for (int i = 0; i < values.length; ++i) {
          buffer.append("    ");
          buffer.append(name);
          buffer.append(" = ");
          buffer.append(values[i]);
          buffer.append('\n');
        }
      }
    }
  }

  public void reportHeaders (StringBuffer buffer, HttpServletRequest req) {
    Enumeration names = req.getHeaderNames();
    if (names.hasMoreElements()) {
      buffer.append("Headers:\n");
      while (names.hasMoreElements()) {
        String name = (String) names.nextElement();
        String value = (String) req.getHeader(name);
        buffer.append("    ");
        buffer.append(name);
        buffer.append(": ");
        buffer.append(value);
        buffer.append('\n');
      }
    }
  }

  public void reportCookies (StringBuffer buffer, HttpServletRequest req) {
    Cookie[] cookies = req.getCookies();
    int l = cookies.length;
    if (l > 0) {
      buffer.append("Cookies:\n");
      for (int i = 0; i < l; ++i) {
        Cookie cookie = cookies[i];
        buffer.append("    ");
        buffer.append(cookie.getName());
        buffer.append(" = ");
	buffer.append(cookie.getValue());
	buffer.append('\n');
      }
    }
  } %>
