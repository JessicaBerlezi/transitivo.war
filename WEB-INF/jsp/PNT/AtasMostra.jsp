<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>   
<%@ include file="Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;
		  
	   case 'ataImprime':  
				fForm.acao.value=opcao;
	   			fForm.target= "_blank";
	    		fForm.action = "acessoTool";  
	   			fForm.submit();
	   		
	   break;	  
		    
  }
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="mostraAtaForm" method="post" action="">
<% String nomTela=" CONSULTA ATAS ";%>
<%@ include file="Cab.jsp" %> 
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="numProcesso" value="<%=ProcessoBeanId.getNumProcesso()%>">
<input type="hidden" name="numCPF" value="<%=ProcessoBeanId.getNumCPFEdt()%>">

<!--DIV QUE IDENTIFICA O PROCESSO -->
<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: auto;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr bgcolor="#D9F4EF" height="18">
        <td width="16%" bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Processo:</strong></td>
        <td width="13%" bgcolor="#d9f4ef"><%=ProcessoBeanId.getNumProcesso() %></td>
        <td width="25%" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%> :</strong>&nbsp;<%=ProcessoBeanId.getNumCNH() %>&nbsp;-&nbsp<%=ProcessoBeanId.getUfCNH()%></td>
		<td colspan="2" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>CPF:</strong>&nbsp;<%=ProcessoBeanId.getNumCPFEdt() %></td>        
		<td width="5%"><%   if(ProcessoBeanId.getAtasDO().size()>0){%>
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('ataImprime',this.form);"> 
              <img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="26" height="19">        
		</button>
		<%}%>
		</td>
      </tr>
      <tr><td colspan=6 height="2"></td></tr>	  
      <tr bgcolor="#C0E1DF" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Condutor:</strong></td>
        <td colspan="5"><%=ProcessoBeanId.getNomResponsavel() %></td>
             
      </tr>  
      <tr><td colspan=5 height="2"></td></tr>
      <tr bgcolor="#D9F4EF" height="14">
        <td bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>Data do Processo:</strong></td>
        <td bgcolor="#d9f4ef"><%=ProcessoBeanId.getDatProcesso() %></td>
        <td colspan=4 bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>Status do Processo :</strong>&nbsp;<%=ProcessoBeanId.getCodStatus()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getNomStatus()%>
		&nbsp;(<%=ProcessoBeanId.getDatStatus() %>)</td>
      </tr>      
  </table>    
</div>

<!--DIV COM AS ATAS --> 
<div class="divPrincipal" style="top:155px; height:40%;">
<div  class="divCorpo" id="hist2" style="height:100%;">   
   <%  if 	(ProcessoBeanId.getAtasDO().size()>0){   		
      	 int a =0;     
		 for (int i=0;i<ProcessoBeanId.getAtasDO().size();i++) {
				a++;%>	
	<!--dados fixos para todos os eventos-->
	<table width="100%" border="0" cellpadding="0" cellspacing="0" >
      <tr bgcolor="#c0e1df"  height="18"> 
        <td width="100">&nbsp;&nbsp;<strong><%=a%>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;Ata :&nbsp;</strong></td>
        <td width="150">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getCodEnvioDO() %></td>       
        <td width="300"><strong>Tipo :</strong>&nbsp;<%=ProcessoBeanId.getAtasDO().get(i).getTipAtaDODesc() %>&nbsp;</td>
        <td style="text-align: right;"><strong>&nbsp;Data Envio:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatEDO()%>&nbsp;</td>
      </tr>      
      <tr><td height="2" colspan="6"></td></tr>
   </table> 
   
   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
      <tr bgcolor="#d9f4ef"  height="18"> 
        <td width="100">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aberto por:&nbsp;</strong></td>
        <td width="150" align="left">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getNomUserNameEDO()%></td>       
        <td width="100"><strong>�rg�o :</strong>&nbsp;<%=ProcessoBeanId.getAtasDO().get(i).getCodOrgaoLotacaoEDO() %>&nbsp;</td>
        <td style="text-align: right;"><strong>&nbsp;Data Proc.Envio:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatProcEDO()%>&nbsp;</td>
      </tr>      
      <tr><td height="2" colspan="6"></td></tr>
   </table> 
   
   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
      <tr bgcolor="#d9f4ef"  height="18"> 
        <td width="100">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Publ.:&nbsp;</strong></td>
        <td width="150" align="left">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getDatPublAta()%></td>       
        <td width="150"><strong>Aberto Por:&nbsp;&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getNomUserNamePDO() %>&nbsp;</td>
        <td width="50"><strong>�rg�o:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getCodOrgaoLotacaoPDO() %>&nbsp;</td>
        <td style="text-align: right;"><strong>&nbsp;Data Proc.Publ.:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatProcPDO()%>&nbsp;</td>
      </tr>      
      <tr><td height="2" colspan="6"></td></tr>
   </table> 
<!--=================== Fim Dados vari�veis para evento 411  ============================================================-->
<% } %>
<table width="100%" ><tr><td height="2"></td></tr></table>
<%}else{%>
<div id="mensagem" style="position:absolute; left:70px; top:85px; right: 25px; height:35px; z-index:10; overflow: visible; text-align: center;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sem-borda">
	  <tr><td bgcolor="#D9F4EF">&nbsp;&nbsp;<strong>PROCESSO SEM ATAS</strong></td> </tr>	
	</table>
</div>
         
<%}%>
</div>
</div>
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->

</form>
</body>
</html>