<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de consulta -->
<jsp:useBean id="ProcessoBeanId"      scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="ParamSistemaBeanId"  scope="session" class="ACSS.ParamSistemaBean" />
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
 	case 'R':
	    	close() ;
			break;
	case 'VisARDigImagem' :
		fForm.acao.value=opcao
		fForm.target= "_blank";
		fForm.action = "acessoTool";  
		fForm.submit();
		break;	
 }
}

function mostraImagem(mypage, myname, w, h, scroll) {		
	var winl = (screen.width - w) / 2;
	//var wint = (screen.height - h) / 2;
	var wint=0;
	winprops = 'height='+h+',width='+w+',top='+wint+',left='+winl+',scrollbars='+scroll+',resizable'
	win = window.open(mypage, myname, winprops)
	if (parseInt(navigator.appVersion) >= 4) { 
		win.window.focus(); 
		win.window.print();
	}
}

function changeColorOver(obj) {
	obj.style.background="#C0E1DF";
}

function changeColorOut(obj) {
	obj.style.background="#D9F4EF";
}

function callDetalhe(numAR) {
	var fForm = document.VisARDig;			
	fForm.codARDigitalizado.value=numAR;
	valida('VisARDigImagem',fForm);
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="VisARDig" method="post" action="">
<input name="acao" type="hidden" value=' '>
<input name="codARDigitalizado" type="hidden" value=''>		
<%@ include file="Cab.jsp" %> 

<!--IN�CIO_CORPO_sistema-->

<!--DIV QUE IDENTIFICA O PROCESSO -->
<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: auto;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr bgcolor="#D9F4EF" height="18">
        <td width="16%" bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Processo:</strong></td>
        <td width="13%" bgcolor="#d9f4ef"><%=ProcessoBeanId.getNumProcesso() %></td>
        <td width="25%" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%> :</strong>&nbsp;<%=ProcessoBeanId.getNumCNH() %>&nbsp;-&nbsp<%=ProcessoBeanId.getUfCNH()%></td>
		<td bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>CPF:</strong>&nbsp;<%=ProcessoBeanId.getNumCPFEdt() %></td>        
		<td align="right"><b>CONSULTA AR DIGITALIZADO&nbsp;&nbsp;</td>
		
      </tr>
      <tr><td colspan=6 height="2"></td></tr>	  
      <tr bgcolor="#C0E1DF" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Condutor:</strong></td>
        <td colspan="5"><%=ProcessoBeanId.getNomResponsavel() %></td>       
      </tr>  
      <tr><td colspan=5 height="2"></td></tr>
      <tr bgcolor="#D9F4EF" height="14">
        <td bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>Data do Processo:</strong></td>
        <td bgcolor="#d9f4ef"><%=ProcessoBeanId.getDatProcesso() %></td>
        <td colspan=4 bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>Status do  Processo:</strong>&nbsp;<%=ProcessoBeanId.getCodStatus()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getNomStatus()%>
		&nbsp;(<%=ProcessoBeanId.getDatStatus() %>)</td>
      </tr>      
  </table>    
</div>
<!--FIM DIV FIXA-->
<%  	
	if(ProcessoBeanId.getARDigitalizado().size()==0) { %>
	<div id="req2" style="position:absolute; left:50px; top:138px; width:738px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="240" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
      <tr><td height="45"></td></tr>
	  <tr><td bgcolor="#D9F4EF" align="center">&nbsp;&nbsp;<strong>PROCESSO SEM AR</strong></td> </tr>	
	</table>
	</div>	
	<%} else { %>

<div id="req_cab" style="position:absolute; left:50px; top:146px; width:738px; height:20px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
	<tr bgcolor="#D9F4EF">
		<td align="left" width="150"><strong>&nbsp;&nbsp;</strong><strong>Caixa</strong></td>
		<td align="left" width="150"><strong>&nbsp;&nbsp;</strong><strong>Lote</strong></td>		
		<td align="left" width="200"><strong>&nbsp;&nbsp;</strong><strong>N�m. Notifica��o</strong></td>		
		<td align="left"><strong>&nbsp;&nbsp;</strong><strong>Data Digitaliza��o</strong></td>
	</tr>
	<tr> 
    	<td height="2"></td>
    </tr>
    </table>
</div>
<div id="req2" style="position:absolute; left:50px; top:160px; width:738px; height:210px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
<%
	 for (int i=0; i<ProcessoBeanId.getARDigitalizado().size(); i++) { %>
	 <tr bgcolor="#d9f4ef"  style="cursor:hand" name="line<%=i%>" id="line<%=i%>" onmouseover="javascript:changeColorOver(line<%=i%>);" onmouseout="javascript:changeColorOut(line<%=i%>);" onclick="javascript:callDetalhe('<%=ProcessoBeanId.getARDigitalizado().get(i).getCodARDigitalizado()%>');">
		 <td align="left" width="150"><strong>&nbsp;</strong>
		 	<%=ProcessoBeanId.getARDigitalizado().get(i).getNumCaixa()%>
		 </td>	 
		 <td align="left" width="150"><strong>&nbsp;</strong>
		 	<%=ProcessoBeanId.getARDigitalizado().get(i).getNumLote()%>
		 </td>
		 <td align="left" width="200"><strong>&nbsp;</strong>
		    <%=ProcessoBeanId.getARDigitalizado().get(i).getNumNotificacao()%>-<%=ProcessoBeanId.getARDigitalizado().get(i).getSeqNotificacao()%>
		 </td>
		 <td align="left"><strong>&nbsp;</strong>
		 	<%=ProcessoBeanId.getARDigitalizado().get(i).getDatDigitalizacao()%>
		 </td>
	 </tr>
	 <tr> 
      	<td height="2"></td>
    </tr>   
    <%if(ProcessoBeanId.getARDigitalizado().size() >0 ){%>
    <tr>
    <div id="imagem" align="center" style="position:absolute; left:164px; top: 33px; width:580px; height: 422px;  z-index:99; background-color: transparent; overflow:auto; visibility: visible;">
  	 <img id="imagemFoto" alt="Foto do Processo: <%=ProcessoBeanId.getARDigitalizado().get(i).getNumProcesso()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=ProcessoBeanId.getARDigitalizado().get(i).getNomFoto()%>&contexto=<%=ProcessoBeanId.getARDigitalizado().get(i).getParametro()%>&header=#" width="570" height="412"> 	
	</div>
    </tr>
 	<%}%>  
  <%} //for %>
  
	</table>	
</div>
<%}//fim do else  %>


<!-- Rodap�-->
<%@ include file="Retornar.jsp"%>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
</form>
</body>
</html>