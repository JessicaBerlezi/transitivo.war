<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>
<jsp:useBean id="PaginacaoBeanId" scope="session" class="PNT.PaginacaoBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="CssImpressao.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: Auto;">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 

		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;	
			boolean passa = true;	
		    int max = PaginacaoBeanId.getDados().size() ;
			if (max>0) { 
				for(int i=0;i<max;i++){
				
					PNT.ProcessoBean myCont = (PNT.ProcessoBean) PaginacaoBeanId.getDados().get(i);
					seq++;
					contLinha++;
					
					if (contLinha%35==1){
						npag++;							
						if (npag!=1){
						passa = true;
				%>			 					
						<jsp:include page="rodape_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
						<% } %>				
					<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
					<jsp:param name="nPag" value= "<%=npag%>" />				
					</jsp:include> 			
		         <% } %>
			<%if(passa){%>
			<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center" class="semborda">		
			    <tr>
			      <td colspan="5" style="text-align: left; line-height:17px;"><strong style="display: block; float: left; margin-bottom: -2px">C�d. Status:&nbsp;<%=PaginacaoBeanId.getDscTransacao() %></strong></td>
			      </tr>
			    <tr>
			        <td style="text-align: left; line-height:17px"><strong>Per�odo de :</strong>&nbsp;
					<%= PaginacaoBeanId.getDatInicio()%>&nbsp; <strong>at�</strong> &nbsp;<%= PaginacaoBeanId.getDatFim()%></td>
				</tr>	
				<tr>	
			        <td style="text-align: left; line-height:17px"><strong>Registro(s) :</strong>&nbsp;<%=PaginacaoBeanId.getQtdRegs() %> - <%=PaginacaoBeanId.getQtdParcialReg()%> / <%=PaginacaoBeanId.getTotalRegistros()%></td>
				</tr>	
				
		  	</table>
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center" class="semborda">
		  		<tr>
				      <td width="30" height="18" bgcolor="#999999"><font color="#000000">&nbsp;&nbsp;<strong>SEQ</strong></font></td>
				      <td width="330" height="18" bgcolor="#999999"><font color="#000000">&nbsp;&nbsp;<strong>PROCESSO</strong></font></td>
					  <td width="80" height="18" bgcolor="#999999"><font color="#000000">&nbsp;&nbsp;<strong>DAT PROC.</strong></font></td>
					  <td width="90" height="18" bgcolor="#999999"><font color="#000000">&nbsp;&nbsp;<strong>CPF</strong></font></td>
					  <td bgcolor="#999999"><font color="#999999"><font color="#000000">&nbsp;&nbsp;<strong>CNH</strong></font></td>
		 		</tr>
			</table>
		  	<% } %>	
		    <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1" class="semborda">
			   <tr>
			     <td width="30">&nbsp;&nbsp;<%=i+1 %></td>
			     <td width="330">&nbsp;&nbsp;<%= myCont.getNumProcesso() %> </td>
			     <td width="80">&nbsp;&nbsp;<%= myCont.getDatProcesso() %></td>
			     <td width="90">&nbsp;&nbsp;<%= myCont.getNumCPFEdt() %></td>
			     <td >&nbsp;&nbsp;<%=myCont.getTipCNHDesc() %>&nbsp;<%= myCont.getNumCNH() %>&nbsp;<%= myCont.getUfCNH() %></td>
			   </tr>
				<tr>
			      <td width="30">&nbsp;&nbsp;</td>
			      <td colspan="4" >&nbsp;&nbsp;<strong>NOME CONDUTOR :&nbsp;</strong><%= myCont.getNomResponsavel() %></td>
				</tr>
				<tr>
					<td bgcolor="#00000" height="1" colspan="6"></td>
				</tr>
		    </table>      

		<% passa=false;} %>
		<% }else{%>	
		<table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">									
				<tr><td height="40" ></td></tr>		
				<tr bgcolor='#CCCCCC'> 
			   	    <td height="35" align=center><b>N�o existem dados a serem retornados </b></td>
				</tr>	
		</table>      	
		
		<%}%>
		<%	if (PaginacaoBeanId.getDados().size()>0) { 
				if (contLinha<50){
		
				} %>
				<jsp:include page="rodape_impConsulta.jsp" flush="true" />
		<%} %>
		
</form>
</body>
</html>