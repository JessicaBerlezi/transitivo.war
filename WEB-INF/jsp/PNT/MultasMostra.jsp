<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>   
<%@ include file="Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;
		  
	   case 'multaImprime':  
				fForm.acao.value=opcao;
	   			fForm.target= "_blank";
	    		fForm.action = "acessoTool";  
	   			fForm.submit();
	   		
	   break;	  
		    
  }
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="mostraHistForm" method="post" action="">
<% String nomTela=" CONSULTA HIST�RICO ";%>
<%@ include file="Cab.jsp" %> 
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="numProcesso" value="<%=ProcessoBeanId.getNumProcesso()%>">
<input type="hidden" name="numCPF" value="<%=ProcessoBeanId.getNumCPFEdt()%>">

<!--DIV QUE IDENTIFICA O PROCESSO -->
<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: auto;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr bgcolor="#D9F4EF" height="18">
        <td width="16%" bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Processo:</strong></td>
        <td width="13%" bgcolor="#d9f4ef"><%=ProcessoBeanId.getNumProcesso() %></td>
        <td width="25%" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%> :</strong>&nbsp;<%=ProcessoBeanId.getNumCNH() %>&nbsp;-&nbsp<%=ProcessoBeanId.getUfCNH()%></td>
		<td colspan="2" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>CPF:</strong>&nbsp;<%=ProcessoBeanId.getNumCPFEdt() %></td>        
		<td width="5%"><%   if(ProcessoBeanId.getMultas().size()>0){%>
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('multaImprime',this.form);"> 
              <img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="26" height="19">        
		</button>
		<%}%>
		</td>
      </tr>
      <tr><td colspan=6 height="2"></td></tr>	  
      <tr bgcolor="#C0E1DF" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Condutor:</strong></td>
        <td colspan="5"><%=ProcessoBeanId.getNomResponsavel() %></td>
             
      </tr>  
      <tr><td colspan=5 height="2"></td></tr>
      <tr bgcolor="#D9F4EF" height="14">
        <td bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>Data do Processo:</strong></td>
        <td bgcolor="#d9f4ef"><%=ProcessoBeanId.getDatProcesso() %></td>
        <td colspan=4 bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>Status do Processo :</strong>&nbsp;<%=ProcessoBeanId.getCodStatus()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getNomStatus()%>
		&nbsp;(<%=ProcessoBeanId.getDatStatus() %>)</td>
      </tr>      
  </table>    
</div>

<!--DIV COM AS Multas --> 
<div class="divPrincipal" style="top:155px; height:40%;">
<div  class="divCorpo" id="hist2" style="height:100%;">   
	<table id="espaco" cellpadding="0" cellspacing="0" border="0" width="100%" bordercolor="#000000" class="semborda">
	    <tr>
	       <td bgcolor="#CCCCCC" style="padding-left: 10px;" width="5%" >Seq.</td>
	       <td height="15" bgcolor="#CCCCCC"  style="padding-left: 10px;" class="td_linha_left">Descri&ccedil;&atilde;o da Infra&ccedil;&atilde;o</td>
	       <td width="27%" height="15" align="center" bgcolor="#CCCCCC" class="td_linha_left" >Enquadramento</td>
	       <td width="18%" height="15" align="right"  bgcolor="#CCCCCC" class="td_linha_left" style="padding-right: 10px">Pontua&ccedil;&atilde;o</td>
	    </tr>
	</table>    								       
	<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	   <tr><td height="1" bgcolor="#000000"></td></tr>
	</table>        
	<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#000000" class="semborda">
         <tr>
           <td width="13%" height="15" bgcolor="#CCCCCC" style="padding-left: 10px">Data / Hora </td>
           <td height="15" style="padding-left: 10px" bgcolor="#CCCCCC" class="td_linha_left">Local</td>
           <td width="13%" height="15" align="center" bgcolor="#CCCCCC" class="td_linha_left">Placa </td>
           <td width="13%" height="15" align="center" bgcolor="#CCCCCC" class="td_linha_left">N&ordm; Auto Infra&ccedil;&atilde;o </td>
           <td width="13%" height="15" align="right" bgcolor="#CCCCCC" class="td_linha_left" style="padding-right: 10px">Org&atilde;o</td>
         </tr>
     </table>	
   <%  if 	(ProcessoBeanId.getMultas().size()>0){  
   		 String cor = "";      	    
		 for (int i=0;i<ProcessoBeanId.getMultas().size();i++) {
		 cor   = (i%2==0) ?    "#ffffff" : "#d9f4ef" ;%>	
	<!--inicia linha-->
			<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr>
					<td height="2" bgcolor="#000000"></td>
				</tr>
			</table>
			<table id="espaco" cellpadding="0" cellspacing="0" border="0" width="100%" bordercolor="#000000" class="semborda">
				<tr bgcolor="<%= cor %>" >
					<td style="padding-left: 10px;" width="5%">
						<%=ProcessoBeanId.getMultas().get(i).getSeqMultaNotificacao()%>&nbsp;
					</td>
					<td height="12" style="padding-left: 10px" class="td_linha_left">
						<%=ProcessoBeanId.getMultas().get(i).getDscInfracao()%>&nbsp;
					</td>
					<td width="27%" align="center" class="td_linha_left">
						<%=ProcessoBeanId.getMultas().get(i).getDscEnquadramento()%>&nbsp;
					</td>
					<td width="18%" align="right" class="td_linha_left" style="padding-right: 10px">
						<%=ProcessoBeanId.getMultas().get(i).getNumPonto()%>&nbsp;
					</td>
				</tr>
			</table>
			<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
				<tr>
					<td height="1" bgcolor="#000000"></td>
				</tr>
			</table>
			<table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#000000" class="semborda">
				<tr bgcolor="<%= cor %>" >
					<td width="13%" height="12" style="padding-left: 10px">
						<%=ProcessoBeanId.getMultas().get(i).getDatInfracao()%>&nbsp;&nbsp;
						<%=ProcessoBeanId.getMultas().get(i).getValHorInfracaoEdt()%>&nbsp;
					</td>
					<td class="td_linha_left" style="padding-left: 10px">
						<%=ProcessoBeanId.getMultas().get(i).getDscLocalInfracao()%>&nbsp;
					</td>
					<td width="13%" align="center" class="td_linha_left">
						<%=ProcessoBeanId.getMultas().get(i).getNumPlaca()%>&nbsp;
					</td>
					<td width="13%" align="center" class="td_linha_left">
						<%=ProcessoBeanId.getMultas().get(i).getNumAutoInfracao()%>&nbsp;
					</td>
					<td width="13%" align="right" class="td_linha_left" style="padding-right: 10px">
						<%=ProcessoBeanId.getMultas().get(i).getSigOrgao()%>&nbsp;
					</td>
				</tr>
			</table>
<!--=================== Fim Dados vari�veis ============================================================-->
		<%}%>
<%}else{%>
<div id="mensagem" style="position:absolute; left:70px; top:85px; right: 25px; height:35px; z-index:10; overflow: visible; text-align: center;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sem-borda">
	  <tr><td bgcolor="#D9F4EF">&nbsp;&nbsp;<strong>PROCESSO SEM HIST�RICO</strong></td> </tr>	
	</table>
</div>         
<%}%>
</div>
</div>
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->

</form>
</body>
</html>