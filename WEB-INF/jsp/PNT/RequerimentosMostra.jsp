<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>  

<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
	    	close() ;
			break;	
			
	   case 'requerimentoImprime':	
			 fForm.acao.value=opcao;
	   		 fForm.target= "_blank";
	    	 fForm.action = "acessoTool";  
	   		 fForm.submit();
	   		
	   break;	  				
  }
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="requerimentoMostraForm" method="post" action="">
<% String nomTela=" CONSULTA REQUERIMENTO ";%>
<%@ include file="Cab.jsp" %>
<input name="acao"     type="hidden" value=''>
<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: auto;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr bgcolor="#D9F4EF" height="18">
        <td width="16%" bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Processo:</strong></td>
        <td width="13%" bgcolor="#d9f4ef"><%=ProcessoBeanId.getNumProcesso() %></td>
        <td width="25%" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%> :</strong>&nbsp;<%=ProcessoBeanId.getNumCNH() %>&nbsp;-&nbsp<%=ProcessoBeanId.getUfCNH()%></td>
		<td colspan="2" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>CPF:</strong>&nbsp;<%=ProcessoBeanId.getNumCPFEdt() %></td>        
		<td width="5%"><%   if(ProcessoBeanId.getRequerimentos().size()>0){%>
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('requerimentoImprime',this.form);"> 
              <img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="26" height="19">        
		</button>
		<%}%>
		</td>
      </tr>
      <tr><td colspan=6 height="2"></td></tr>	  
      <tr bgcolor="#C0E1DF" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Condutor:</strong></td>
        <td colspan="5"><%=ProcessoBeanId.getNomResponsavel() %></td>
             
      </tr>  
      <tr><td colspan=5 height="2"></td></tr>
      <tr bgcolor="#D9F4EF" height="18">
        <td bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>Data do Processo:</strong></td>
        <td bgcolor="#d9f4ef"><%=ProcessoBeanId.getDatProcesso() %></td>
        <td colspan=4 bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>Status do Processo :</strong>&nbsp;<%=ProcessoBeanId.getCodStatus()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getNomStatus()%>
		&nbsp;(<%=ProcessoBeanId.getDatStatus() %>)</td>
      </tr>      
  </table>    
</div>
<!--FIM DIV FIXA-->
<div class="divPrincipal" style="top:160px; height:40%;">
<div  class="divCorpo" id="req2" style="height:100%;">
  <%	
	if 	(ProcessoBeanId.getRequerimentos().size()>0){	 
		 for (int i=0;i<ProcessoBeanId.getRequerimentos().size();i++) {		
  %>
  
  <%
	if ((ProcessoBeanId.getRequerimentos().get(i).getCodTipoSolic().equals("")==false))  {  %>
    
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
	  <tr><td height="5"></td></tr>
      <tr bgcolor="#c0e1df" height="18"> 
        <td width="350" bgcolor="#c0e1df"><strong>&nbsp;N&ordm;&nbsp;REQUERIMENTO :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNumRequerimento()%></td>
        <td width="270" bgcolor="#c0e1df">&nbsp;<strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomTipoSolic()%></strong></td>
        <td align="right" bgcolor="#c0e1df"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatRequerimento()%>&nbsp;&nbsp;</td>
      </tr>
      <tr><td height="2"></td></tr>	  
    </table>
    <table width="100%" border="0" cellpadding="2" cellspacing="0">     
      <tr bgcolor="#d9f4ef" height="18"> 
      	<td width="100"><strong>&nbsp;Aberto por :&nbsp;&nbsp;</strong></td>
      	<td width="138"><%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameDP() %></td>
        <td><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoDP()%></td>
		<td align="right"><strong><%=ProcessoBeanId.getRequerimentos().get(i).getNomStatusRequerimento()%></strong></td>
      </tr>
	    
      <% String guia="";
      	if(guia.length()>0) { %>
	      <tr ><td height="2"></td></tr>
    	  <tr bgcolor="#d9f4ef" height="18"> 
      		<td ><strong>&nbsp;Guia Distrib.:</strong>&nbsp;</td>
	        <td ><%=guia%></td>
    	    <td ></td>
			<td ></td>
      	  </tr>  
      <% } %> 
	  
  </table>   
	<% 	
   } 
   if ((ProcessoBeanId.getRequerimentos().get(i).getCodRelatorJU().equals("")==false)) {	     
  %>  
	<!--Requerimento JUNTA-->
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
	  <tr><td height="2" colspan=3></td></tr>
      <tr bgcolor="#d9f4ef" height="18">       
        <td>&nbsp;<strong>Junta :</strong></td>
		<td><%=ProcessoBeanId.getRequerimentos().get(i).getSigJuntaJU()%></td>
        <td><strong>Relator :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getNomRelatorJU()%></td>			
        <td><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatJU() %>&nbsp;&nbsp;</td>
      </tr>
	  <tr><td height="2"></td></tr>
      <tr bgcolor="#d9f4ef" height="18"> 
      	<td width="100"><strong>&nbsp;Executado por :</strong>&nbsp;</td>
        <td width="138"><%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameJU() %></td>
        <td width="293"><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoJU()%></td>
      	<td><strong>Data de Proc.:</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatProcJU()%>&nbsp;&nbsp;</td>
      </tr>
	      
   </table> 
  <% } //IF JUNTA 
     
   if  (ProcessoBeanId.getRequerimentos().get(i).getCodResultRS().equals("")==false)  {
       
  %>
	<!--Requerimento RESULTADO-->
    <table width="100%" border="0" cellpadding="2" cellspacing="0">
	  <tr><td height="2"></td></tr>
      <tr bgcolor="#d9f4ef" height="18"> 
        <td>&nbsp;<strong>Resultado :</strong></td>
        <td colspan="2"><%= ProcessoBeanId.getRequerimentos().get(i).getNomResultRS() %></td>
        <td><strong>Data Resultado :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getDatRS() %></td>
      </tr>
      <tr bgcolor="#d9f4ef" height="18"> 
        <td>&nbsp;<strong>Penalidade :</strong></td>
        <td colspan="3"><%=ProcessoBeanId.getRequerimentos().get(i).getDscPenalidade()%></td>
      </tr>
      <tr> <td height="2"></td>  </tr>
      <tr bgcolor="#d9f4ef" height="18"> 
	     <td>&nbsp;<strong>Motivo :</strong></td>
	     <td colspan="3">
		     <textarea cols="100" rows="1" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" readonly="readonly"><%=ProcessoBeanId.getRequerimentos().get(i).getTxtMotivo()%></textarea>
	     </td>
      </tr>
      <tr><td height="2"></td></tr>	  
      <tr bgcolor="#d9f4ef" height="18"> 
      	<td width="100">&nbsp;<strong>Executado por :</strong>&nbsp;</td>
        <td width="138" ><%=ProcessoBeanId.getRequerimentos().get(i).getNomUserNameRS() %></td>
        <td width="293" ><strong>Org�o Lota��o :</strong>&nbsp;<%=ProcessoBeanId.getRequerimentos().get(i).getCodOrgaoLotacaoRS()%></td>
      	<td><strong>Data de Proc.:</strong>&nbsp; <%=ProcessoBeanId.getRequerimentos().get(i).getDatProcRS()%>&nbsp;&nbsp;</td>
      </tr>    
   </table>
<%			} //IF RESULTADO
		} //Loop do while 
	}	else	{
%>
	<table width="240" align="center" border="0" cellpadding="2" cellspacing="0" class="semborda">
      <tr><td height="45"></td></tr>
	  <tr><td bgcolor="#c0e1df" align="center">&nbsp;&nbsp;<strong>PROCESSO SEM REQUERIMENTO</strong></td> </tr>	
	</table>
<%	}	%>
</div>
</div>
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>

</form>
</body>
</html>