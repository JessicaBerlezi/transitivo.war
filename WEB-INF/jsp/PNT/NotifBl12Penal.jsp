<!--Bloco 12 - PNT
-->	
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
  <td height="130" align="center" valign="top"><p style="margin-top: 0px; font-size: 11px; font-weight:bold;">NOTIFICA&Ccedil;&Atilde;O DE APLICA&Ccedil;&Atilde;O DA PENALIDADE DE SUSPENS&Atilde;O  DO EXERC&Iacute;CIO DO DIREITO DE DIRIGIR</p>
  
    <div style="font-size:11px; font-family:Verdana, Arial, Helvetica, sans-serif; margin-bottom:1px; margin-top:-10px; text-align:justify; text-indent: 50px;">
    	Fica  V.S&ordf; notificada da aplica&ccedil;&atilde;o da Penalidade estabelecida pelo Processo  Administrativo <strong><%=ProcessoBeanId.getNumProcesso() %></strong> de  Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir, 
    	em face do cometimento da(s)  infra&ccedil;&atilde;o (&otilde;es) abaixo descrita(s), 
    	conforme previsto no art. 261 do C&oacute;digo de Tr&acirc;nsito  Brasileiro &ndash; CTB e Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182/2005.
    </div>
		
	<div style="font-size:11px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align:left; margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 50px;">
		V.S&ordf;&nbsp; poder&aacute; entregar sua Carteira Nacional de  Habilita&ccedil;&atilde;o &ndash; CNH, ao Detran- RJ, junto ao N&uacute;cleo de Documentos Apreendidos &ndash;  NUDA, na Avenida Presidente Vargas, 817 &ndash; sobreloja, Centro-RJ, ou em qualquer  Posto de Habilita&ccedil;&atilde;o.&nbsp;
	</div>

	<div style="font-size:11px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align:left; margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 18px;">
		V.S&ordf; poder&aacute;  apresentar Recurso, conforme o art. 17 da Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182/2005, neste  formul&aacute;rio, no quadro &ldquo;Justificativa / Descri&ccedil;&atilde;o dos Fatos&rdquo;, com a exposi&ccedil;&atilde;o  dos fatos, fundamenta&ccedil;&atilde;o legal do pedido e documentos que comprovem a alega&ccedil;&atilde;o,  at&eacute; o prazo abaixo determinado.
	</div>

    <div style="font-size:11px; font-family:Verdana, Arial, Helvetica, sans-serif; text-align:left; margin-bottom: -15px; margin-top: 0px; text-align:justify;text-indent: 50px;">
		Vale  ressaltar que, se o condutor for flagrado conduzindo ve&iacute;culo, instaurar-se-&aacute;  Processo Administrativo tendente &agrave; Cassa&ccedil;&atilde;o do Direito de Dirigir, nos termos  do artigo 263, I, do C&oacute;digo de Tr&acirc;nsito Brasileiro - CTB.
	</div>
	</td>
</tr>
   <tr><td height="20"></td>
   </tr>
    
<tr><td height="25" align="center" valign="top">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-bottom: 10px">
        <tr><td colspan="11" height="25" class="semborda">
		  	<table width="100%" cellpadding="0" cellspacing="0" style="border: 2px solid #000000;margin-left: -1px; margin-right: 1px">
			  <tr>
				  <td width="14%" style="padding: 2px; padding-left: 4px; font-size:10px; font-family:Verdana, Arial, Helvetica, sans-serif;"><strong>PENALIDADES:</strong></td>
				  <td width="86%" style="text-align:justify;padding-right: 4px;">
				  	<span style="font-size:10px; font-family:Verdana, Arial, Helvetica, sans-serif;"><%=ProcessoBeanId.getDscPenalidade()%></td>
			  </tr>
		  	</table>		  
		  	</td>
        </tr>
        <tr><td colspan="11" height="5"></td></tr>
        <tr>
          <td height="25" valign="top" class="tdborda" colspan="5"><span class="fontmenor">&nbsp;&nbsp;<strong>NOME</strong></span><br />
              <span class="fontmaior">&nbsp;<%= ProcessoBeanId.getNomResponsavel() %></span></td>
          <td width="1%" valign="top" class="semborda">&nbsp;</td>
          <td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE MULTAS</strong></span><br />
              <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getQtdMultas()%></span></td>
          <td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
          <td width="21%" rowspan="3" valign="top" class="tdborda"><span class="fontmenor"><strong>&nbsp;&nbsp;N&ordf; PROCESSO </strong></span><br />
              <br />
              <span class="fontmaior">&nbsp;<strong><%=ProcessoBeanId.getNumProcesso()%></strong></span></td>
          <td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
          <td width="17%" rowspan="3" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>PRAZO P/ RECURSO</strong></span><br />
              <br />
              <span class="fontmaior">&nbsp;<strong><%=ProcessoBeanId.getUltDatRecurso()%></strong></span></td>
        </tr>
        <tr><td colspan="11" height="2"></td></tr>
        <tr>
           <td width="17%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;</span><span class="fontmenor"><strong><%=ProcessoBeanId.getTipCNHDesc()%></strong></span><br />
        	     <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCNH()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getUfCNH()%></span></td>
           <td width="1%" valign="top">&nbsp;</td>
           <td width="11%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>EXPEDI&Ccedil;&Atilde;O</strong></span><br />
              <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getDatEmissaoCNH()%></span></td>
           <td width="1%" valign="top" class="semborda">&nbsp;</td>
           <td width="15%" valign="top" class="tdborda" ><span class="fontmenor">&nbsp;&nbsp;<strong>CPF</strong></span><br />
              <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCPFEdt()%></span></td>
           <td width="1%" valign="top" class="semborda">&nbsp;</td>
           <td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE PONTOS</strong></span><br />
              <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getTotPontos()%></span></td>
           <td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
           <td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
        </tr>
      </table>
      </td>
    </tr>
</table>