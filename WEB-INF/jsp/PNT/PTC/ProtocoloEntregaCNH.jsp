<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<% String path=request.getContextPath(); 
   String npag="1";   
   String tituloConsulta = (String)session.getAttribute("tituloConsulta");
  	if(tituloConsulta==null) tituloConsulta="";   
%> 
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId"     scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../CssImpressao.jsp" %>
<style type="text/css">
.tabprotocolo{ margin-top: -1px; margin-bottom: -1px; width: 90%; padding: 0; border: 0px none; border-left: 1px solid #000000 }
.tabprotocolo td{font-weight: bold; font-size:8pt; border: 1px solid #000000; border-left: 0px none; padding: 1px 3px 1px 4px; height: 40px; vertical-align: top;}
.tabprotocolo .normal_noborder{font-weight: normal; font-size:7pt; border: 0px none;}
.tabprotocolo .textomaior{font-weight: normal; font-size:9pt; display:block; margin-top: 3px}
.noborder_left {border-left: 0px none; margin-top: -5px;}
</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >


<!--CABE�ALHO IMPRESS�O-->


<form name="frmDownload" method="post" action="">
  <table cellpadding=0 cellspacing=0 border="0" width="90%" bordercolor="#ffffff" class="semborda" style="margin-left: 25pt">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr> 
  </table>
  <table width="90%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-left: 25pt">
  <tr><td width="25%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
	        </tr>
    	    <tr> <td width="38%" height="40" valign="top"><img src="images/<%=SistemaBeanId.getCodUF()%>/im_logodetran.gif"></td>
        	</tr>
	      </table>
     </td>        
     <td width="60%" align="center" valign="top" class="fontmedia"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
    	  	<strong>SISTEMA DE MONITORAMENTO DE INFRA&Ccedil;&Otilde;ES DE TR&Acirc;NSITO</strong> 
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
    	  <span class="fontmedia"><strong>PROTOCOLO DE ENTREGA DE CNH N&ordm; <%=ProcessoBeanId.getNumProtocoloEntregaCNH()%></strong></span>
     </td>
     <td width="15%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr>   <td height="20" align="right" valign="top">&nbsp;</td>
	        </tr>
	        <tr><td height="40" align="right" valign="top"><img src="images/im_logosmit.gif" width="98" height="30"></td>
    	    </tr>
	      </table>		  
     </td>
  </tr>        
  </table> 
<!--Linha-->
  <table cellpadding=0 cellspacing=0 border="0" width="90%" bordercolor="#ffffff" class="semborda" style="margin-left: 25pt">
	<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
  </table>
<!--FIM Linha-->


<!--VIA DETRAN -->

  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="width: 25%">N&ordm; do Processo<br />
        <span class="textomaior"><%=ProcessoBeanId.getNumProcesso()%></span></td>
        <td style="width: 45%">Carteira Nacional de Habilita��o N&ordm;<br />
        <span class="textomaior"><%=ProcessoBeanId.getTipCNHDesc()%>&nbsp;&nbsp;<%=ProcessoBeanId.getNumCNH()%>-<%=ProcessoBeanId.getUfCNH()%></span></td>
        <td>Data de Expedi&ccedil;&atilde;o<br />
        <span class="textomaior"><%=ProcessoBeanId.getDatEmissaoCNH()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td>Nome do motorista<br />
        <span class="textomaior"><%=ProcessoBeanId.getNomResponsavel()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td>CPF<br />
        <span class="textomaior"><%=ProcessoBeanId.getNumCPFEdt()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td>Endere&ccedil;o<br />
        <span class="textomaior"><%=ProcessoBeanId.getEndereco()%></span></td>
      </tr>
  </table> 
   
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="width: 55%">Cidade<br />
        <span class="textomaior"><%=ProcessoBeanId.getMunicipio()%></span></td>
        <td style="width: 15%">UF<br />
        <span class="textomaior"><%=ProcessoBeanId.getUf()%></span></td>
        <td>CEP<br />
        <span class="textomaior"><%=ProcessoBeanId.getCepEdt()%></span></td>
      </tr>
  </table>  
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td>Local de entrega<br />
        <span class="textomaior"><%=UsuarioBeanId.getOrgao().getNomOrgao()%></span></td>
      </tr>
  </table> 
 
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="width: 35%; height: 50px;">Data de entrega da CNH<br />
        <span class="textomaior"><%=ProcessoBeanId.getDtEntregaCNH()%></span></td>
        <td>Recebida pelo funcion&aacute;rio<br />
        <span class="textomaior"><%=UsuarioBeanId.getNomResp()%></span></td>
      </tr>
  </table> 
  <table cellpadding=0 cellspacing=0 width="90%" class="semborda" style="margin-left: 25pt">
    <tr><td height="2"style="border-top: 1px solid #000000">&nbsp;</td></tr>
  </table> 
  <table cellspacing="0" class="tabprotocolo noborder_left" style="margin-left: 25pt">
      <tr>
        <td width="50%" class="normal_noborder">Login:&nbsp;&nbsp;<%=UsuarioBeanId.getNomUserName()%></td>
        <td style="text-align: right;" class="normal_noborder">Via Detran</td>
      </tr>
  </table> 
<!--FIM VIA DETRAN --> 
  
<!--Linha-->
  <table cellpadding=0 cellspacing=0 width="90%" class="semborda" style="margin-left: 25pt">
    <tr><td height="60"style="vertical-align: bottom; text-align:right">CORTAR AQUI</td></tr>
	<tr><td height="60" style="border-top: 3px dotted #000000">&nbsp;</td></tr>
    
 </table>
<!--FIM Linha-->  

<!--VIA CONDUTOR -->
  <table cellpadding=0 cellspacing=0 border="0" width="90%" bordercolor="#ffffff" class="semborda" style="margin-left: 25pt">
	<tr>
      <td height="1" bgcolor="#000000"></td>
    </tr> 
  </table>

  <table width="90%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-left: 25pt">
  <tr><td width="25%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
	        </tr>
    	    <tr> <td width="38%" height="40" valign="top"><img src="images/<%=SistemaBeanId.getCodUF()%>/im_logodetran.gif"></td>
        	</tr>
	      </table>
     </td>        
     <td width="60%" align="center" valign="top" class="fontmedia"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
    	  	<STRONG>SISTEMA DE MONITORAMENTO DE INFRA&Ccedil;&Otilde;ES DE TR&Acirc;NSITO</strong> 
	      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
    	  <span class="fontmedia"><strong>PROTOCOLO DE ENTREGA DE CNH N&ordm; <%=ProcessoBeanId.getNumProtocoloEntregaCNH()%></strong></span>
     </td>
     <td width="15%" align="center"> 
	      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
    	    <tr><td height="20" align="right" valign="top">&nbsp;</td>
	        </tr>
	        <tr><td height="40" align="right" valign="top"><img src="images/im_logosmit.gif" width="98" height="30"></td>
    	    </tr>
	      </table>		  
     </td>
  </tr>        
  </table> 

  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="width: 25%">Processo N&ordm;<br />
        <span class="textomaior"><%=ProcessoBeanId.getNumProcesso()%></span></td>
        <td style="width: 45%">Carteira Nacional de Habilita��o N&ordm;<br />
        <span class="textomaior"><%=ProcessoBeanId.getTipCNHDesc()%>&nbsp;&nbsp;<%=ProcessoBeanId.getNumCNH()%>-<%=ProcessoBeanId.getUfCNH()%></span></td>
        <td>Data de Expedi&ccedil;&atilde;o<br />
        <span class="textomaior"><%=ProcessoBeanId.getDatEmissaoCNH()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td>Nome do motorista<br />
        <span class="textomaior"><%=ProcessoBeanId.getNomResponsavel()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="width: 55%">Local de entrega<br />
        <span class="textomaior"><%=UsuarioBeanId.getOrgao().getNomOrgao()%></span></td>
        <td>Data de Entrega da CNH<br />
        <span class="textomaior"><%=ProcessoBeanId.getDtEntregaCNH()%></span></td>
      </tr>
  </table> 
  
  <table cellspacing="0" class="tabprotocolo" style="margin-left: 25pt">
      <tr>
        <td style="height: 55px;">Assinatura e matr�cula do funcion�rio respons�vel pelo setor</td>
      </tr>
  </table>
  <table cellspacing="0" class="tabprotocolo noborder_left" style="margin-left: 25pt; margin-top: 5px;">
      <tr>
        <td width="50%" class="normal_noborder">Login:&nbsp;&nbsp;<%=UsuarioBeanId.getNomUserName()%></td>
        <td style="text-align: right;" class="normal_noborder">Via Condutor</td>
      </tr>
  </table>  
<!--FIM VIA CONDUTOR -->
</form>
</body>
</html>