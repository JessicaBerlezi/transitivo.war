<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<%String path = request.getContextPath();

			%>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>
<%@ page import="PNT.HistoricoBean" %>
<%@ page import="ACSS.ParamSistemaBean" %>
<jsp:useBean id="ProcessoBeanId"  scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="HistoricoBeanId" scope="request" class="PNT.HistoricoBean" /> 
<jsp:useBean id="ParamSistemaBeanId"  scope="session" class="ACSS.ParamSistemaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
	    	close() ;
			break;
	  case 'Novo':
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	
		   	break;  
	}			
}

var theTable, theTableBody

function imprimeFrame() {
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();		
}
function escondeFrame() {
	document.all.etiquetaFrame.style.visibility="hidden";
}
function mostraFrame() {
	document.all.etiquetaFrame.style.visibility="visible";
}

function init() {
	theTable = (document.all) ? document.all.myTABLE : document.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function init2() {
	theTable = (document.all) ? window.parent.frame1.myTABLE : window.parent.frame1.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function appendRow(form) {
	insertTableRow(form, -1)
}
function appendRow2(form) {
	insertTableRow2(form, -1)
}

function addRow(form) {
	insertTableRow(form, form.insertIndex.value)
}

function removeRow(form) {
	theTableBody.deleteRow(0)
}

function insertTableRow(form, where) {
	var now = new Date()
	var nowData = [now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()]
	clearBGColors()
	var newCell
	var newRow = theTableBody.insertRow(where);
	for (var i = 0; i < nowData.length; i++) {
		newCell = newRow.insertCell(i)
		newCell.innerHTML = nowData[i]
		newCell.style.backgroundColor = "salmon"
	}
}
</script>
<link href="../Css.jsp" rel="stylesheet" type="text/css">
</head>

<body onLoad="init2()" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="controls" method="post" action="">

<%@ include file="../Cab.jsp" %>

<div id="etiqueta" style="position:absolute; left:200px; top:75px; width:400px; height:265px; z-index:5; overflow: auto; visibility: visible;"> 
<!--etiqueta-->
<%String atualiza = ParamSistemaBeanId.getParamSist("QUANTAS_ETIQUETAS_PNT");
			String assuntoProcesso = "";
			if (HistoricoBeanId.getTxtComplemento02().equals("DP")) {
				assuntoProcesso = ParamSistemaBeanId.getParamSist("NOME_ASS_DEFESA_PNT");
			} else {
				assuntoProcesso = ParamSistemaBeanId.getParamSist("NOME_ASS_RECURSO_PNT");
			}
			int cont = 0;%>
    <input name="acao"     type="hidden" value=''>
     <input type="hidden" name="Orgao" value='<%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%>'>  
	 <input type="hidden" name="NumProcesso" value="<%=ProcessoBeanId.getNumProcesso() %>">
	 <input type="hidden" name="DatProcesso" value="<%=ProcessoBeanId.getDatProcesso()%>">	 
	 <input type=hidden name=AssuntoProcesso value="<%= assuntoProcesso %>">
	 <input type=hidden name="numAuto" value="<%= ProcessoBeanId.getTipProcessoDesc() %>">
	 <input type=hidden name="numCPF" value="<%= ProcessoBeanId.getNumCPFEdt() %>">	
	 <input type="hidden" name="TxtComplemento03" value="<%=HistoricoBeanId.getTxtComplemento04()%>">			
	<%if (ParamSistemaBeanId.getParamSist("QUANTAS_ETIQUETAS_PNT").equals("2")) {
	%>
		<table width="360" border="1" cellpadding="5" cellspacing="0" class="table">
			<tr><td width="100%">
					<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
						<tr><td>
							<strong><%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%></strong>
							</td></tr>
					</table>
				</td>
			</tr>
			<tr><td width="100%">
					<table width="100% border=" 0" cellpadding="0" cellspacing="0" class="table">
						<tr><td width="40"><strong>Processo:&nbsp;</strong></td>
							<td><%=ProcessoBeanId.getNumProcesso()%></td>
							<td align="right"><strong>Data:&nbsp;</strong></td>
							<td align="right" width="40"><%=ProcessoBeanId.getDatProcesso()%></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td width="100%">
					<table width="100% border=" 0" cellpadding="0" cellspacing="0" class="table">
						<tr><td width="40"><strong>Nome:&nbsp;</strong></td>
							<td><%=HistoricoBeanId.getTxtComplemento04()%></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td width="100%">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
					<tr><td width="40" valign="top"><strong>Assunto:&nbsp;</strong></td>
						<td valign="top"><%=assuntoProcesso%></td>
					</tr>
				</table>
				</td>
			</tr>
		</table>
		<DIV class="quebrapagina"></DIV>
		
<br>
		<%}%>
<!--Segunda Etiqueta para o processo-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="table">
<tr><td width="100%">
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr><td><strong><%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%></strong></td>
		</tr>
	</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td ><%=ProcessoBeanId.getNumProcesso()%></td>
		
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=ProcessoBeanId.getDatProcesso()%></td>
		
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td ><%=HistoricoBeanId.getTxtComplemento04()%>	</td>			
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40" valign="top"><strong>Assunto:&nbsp;</strong></td>
			<td  valign="top">	<%=assuntoProcesso%>	</td>
		</tr>
		</table>
	</td>
</tr>
</table>
<DIV class="quebrapagina"></DIV>
<!--FIM Form de Processo-->

<%String assunto = ""; //Usada para armazenar os assuntos de cada requerimento

			%>


<%if (HistoricoBeanId.getTxtComplemento01().equals("") == false) {	%>
<br>	
<!--Form do REQ 1-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="table">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td><strong><%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%></strong>
			<input type="hidden" name="Orgao<%=cont%>" value='<%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%>'></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td ><%=ProcessoBeanId.getNumProcesso()%></td>
			<input type="hidden" name="NumProcesso<%=cont%>" value="<%=ProcessoBeanId.getNumProcesso() %>">
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=ProcessoBeanId.getDatProcesso()%></td>
			<input type="hidden" name="DatProcesso<%=cont%>" value="<%=ProcessoBeanId.getDatProcesso()%>">
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td >
				<%=HistoricoBeanId.getTxtComplemento04()%>
				<input type="hidden" name="Nome<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento04()%>">			
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento01()%></td>
					<input type="hidden" name="Req<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento01()%>">
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento03()%></td>
					<input type="hidden" name="Data<%=cont%>" value="<%=HistoricoBeanId.getTxtComplemento03()%>">
		</tr>
		</table>
	</td>
</tr>

<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Assunto:&nbsp;</strong></td>
			<td >
			<%assunto = "";
				if (HistoricoBeanId.getTxtComplemento02().equals("DP")) {
					assunto = ParamSistemaBeanId.getParamSist("NOME_ASS_DEFESA_PNT");
				} else {
					assunto = ParamSistemaBeanId.getParamSist("NOME_ASS_RECURSO_PNT");
				}

				%>
			<%=assunto%>
			</td>
			<input type="hidden" name="Assunto<%=cont%>" value="<%=assunto%>">
		</tr>
		</table>
	</td>
</tr>

</table>
<!--FIM Form de REQ 1-->

<%cont++;
			} //if REQ 1 

			%>

<%if (HistoricoBeanId.getTxtComplemento01().equals("") == false) {

				%>

<br>
<!--Form do REQ 1 SEGUNDA ETIQUETA-->
<table width="360" border="1" cellpadding="5" cellspacing="0" class="table">
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td><strong><%=ParamSistemaBeanId.getParamSist("NOME_SECRETARIA_PNT")%></strong></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Processo:&nbsp;</strong></td>
			<td><%=ProcessoBeanId.getNumProcesso()%></td>
			<td align="right"><strong>Data:&nbsp;</strong></td>
			<td align="right" width="40"><%=ProcessoBeanId.getDatProcesso()%></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Nome:&nbsp;</strong></td>
			<td >
				<%=HistoricoBeanId.getTxtComplemento04()%>
			</td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%" >
		<table width="100% border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
				<td  width="40"><strong>Req:&nbsp;</strong></td>
				<td ><%=HistoricoBeanId.getTxtComplemento01()%></td>
				<td align="right"><strong>Data:&nbsp;</strong></td>
				<td align="right" width="40"><%=HistoricoBeanId.getTxtComplemento03()%></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td width="100%">
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr>
			<td  width="40"><strong>Assunto:&nbsp;</strong></td>
			<td>
			<%
				if (HistoricoBeanId.getTxtComplemento02().equals("DP")) {
					assunto = ParamSistemaBeanId.getParamSist("NOME_ASS_DEFESA_PNT");
				} else {
					assunto = ParamSistemaBeanId.getParamSist("NOME_ASS_RECURSO_PNT");
				}
			%>
			<%=assunto%>
			</td>
		</tr>
		</table>
	</td>
</tr>

</table>

<!--FIM Form de REQ 1 - SEGUNDA ETIQUETA-->

<%} //if REQ 1 
	%>

<!--fim etiqueta-->
</div> 
<div id="botaoimprimir" style="position:absolute; right: 35px; bottom: 58px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible; left: 520px;">

<table>
<tr>
	<td>
		<button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" type="button" onClick="appendRow2(this.form,this)"> 
			<img src="<%=path%>/images/bot_imprimir_det1.gif" width="70" height="26" alt="" border="0">
		</button>
	</td>
	<td>
		<button style="border: 0px; background-color: transparent; height: 28px; width: 82px; cursor: hand;" type="button" onClick="javascript:valida('Novo',this.form);"> 
			<img src="<%= path %>/images/bot_prosseguir_det1.gif" width="80" height="26" alt="" border="0">
		</button>	
	</td>
</tr>
</table>
</div>
</form>

<div id="etiquetaFrame" style="position:absolute; left:600px; top:50px; width:500px; height:350px; z-index:5; overflow: auto; visibility: visible;"> 
<iframe name="frame1" id="frame1" src="<%= path %>/js/etiquetaFrame.html" frameborder="0" width="365" heigth="350" MARGINWIDTH="0" MARGINHEIGHT"0" SCROLLING="NO">
</iframe>
</div>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%String msgErro = ProcessoBeanId.getMsgErro();
			String msgOk = ProcessoBeanId.getMsgOk();
			String msgErroTop = "160 px";
			String msgErroLeft = "80 px";
			String mostraMsg = "hidden";

			%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</body>
<script>
function insertTableRow2(form, where) {
	
	while(theTableBody.rows.length>0) {
		removeRow(form); 
	}
	var newCell
	var newRow
	var strPadrao1="<td><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 class=texto><strong>"		
	
<% 
if(ParamSistemaBeanId.getParamSist("QUANTAS_ETIQUETAS_PNT").equals("2"))
	{  %>
	//Primeira etiqueta Processo
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao.value + "</td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.TxtComplemento03.value + "</font></strong></td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.AssuntoProcesso.value + "</td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=46 border=0></td>"
   <%}%>
	//Segunda etiqueta Processo
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao.value + "</td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.TxtComplemento03.value + "</font></strong></td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.AssuntoProcesso.value + "</td></tr></table></td>"
	
	<% if (cont>0) { %>
		newRow = theTableBody.insertRow(where)		
		newCell = newRow.insertCell(0)	
		newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=30 border=0></td>"
	<% } %>

<% for(int i=0; i<cont; i++) {  %>
	//primeira etiqueta requerimento
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso<%=i%>.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.Nome<%=i%>.value + "</font></strong></td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Req:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto>" + form.Req<%=i%>.value + "</td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.Data<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.Assunto<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=35 border=0></td>"
	
	//segunda etiqueta requerimento
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "</strong></td><td align=left class=texto>" + form.Orgao<%=i%>.value + "</td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%>&nbsp;</td>"
		
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Processo:&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.NumProcesso<%=i%>.value + "</font></strong></td><td align=right class=texto><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.DatProcesso<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Nome:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto><strong><font style=font-size:10px>" + form.Nome<%=i%>.value + "</font></strong></td></tr></table></td>"

	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = strPadrao1 + "Req:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td><td align=left class=texto>" + form.Req<%=i%>.value + "</td><td align=right class=texto><strong>&nbsp;Data:&nbsp;</strong></td><td align=right width=40 class=texto>" + form.Data<%=i%>.value + "</td></tr></table></td>"
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = "<td width=100%><table width=100% border=0 cellpadding=0 cellspacing=0><tr><td align=left width=40 valign=top class=texto><strong>Assunto:&nbsp;&nbsp;</strong></td><td align=left valign=top class=texto>" + form.Assunto<%=i%>.value + "</td></tr></table></td>"

	<% if (i+1<cont) { %>
		newRow = theTableBody.insertRow(where)		
		newCell = newRow.insertCell(0)	
		newCell.innerHTML = "<td width=100%><img src=../images/inv.gif width=1 height=25 border=0></td>"
	<% } 
	}
%>

	//Impressao
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();
}
</script>
</html>
