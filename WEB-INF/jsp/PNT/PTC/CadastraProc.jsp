<!--M�dulo : REC
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="ProcessoBeanId" scope="request" class="PNT.ProcessoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'CadastraProc':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   			return;
	   		}
	   		else
	   		{
		   		bProcessando=true;
		        if (veCampos(fForm)==true) 
		        {		  
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  		
				   	fForm.submit();	 
				} 		  
			}
		    break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;	
	valid = ValDt(fForm.datprocesso,1) && valid
	verString(fForm.datprocesso,"Data do Processo",0);
	if (valid==false) alert(sErro) 
	return valid ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="CadatraProc" method="post" action="">
<%@ include file="../Cab.jsp" %>
 
<input name="acao"               type="hidden" value=' '>	
<input name="numProcAnt"         type="hidden" value='<%=ProcessoBeanId.getNumProcesso()%> '>
<input name="codProcAnt"         type="hidden" value='<%=ProcessoBeanId.getCodProcesso()%> '>			

<!--IN�CIO_CORPO_sistema--> 
<%@ include file="../ProcessoApresentaTop.jsp" %>    
<%@ include file="../ProcessoApresenta.jsp" %>

<div id="Cadastra" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
 <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
  <% if( request.getAttribute("statusIteracao") != null) {%>   
     <tr bgcolor="#d9f4ef">
   	    <td colspan=3 height="22" valign="middle"><strong>&nbsp;
		<% if (ProcessoBeanId.getNomStatus().length()>0) { %>
			<%=ProcessoBeanId.getNomStatus()%>&nbsp;&nbsp;(<%=ProcessoBeanId.getDatStatus()%>)
		<% } %>
			</strong>
        </td>			
     </tr>
     <tr><td colspan=3 height=2></td></tr>
     <tr bgcolor="#d9f4ef">
   	    <td width="60%" height="22" valign="middle">&nbsp;	N�mero do Processo :&nbsp;
			  <input type="text" name="numprocesso" size="25" maxlength="20" value='<%=ProcessoBeanId.getNumProcesso() %>'  onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase();">	
        </td>			
        <td width="30%">&nbsp;	Data do Processo :&nbsp;
			  <input type="text" name="datprocesso" size="12" maxlength="10" value='<%=ProcessoBeanId.getDatProcesso() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
        <td width="10%"  align="center" valign="middle"></td>
     </tr>
      <tr bgcolor="#d9f4ef">
	        <td colspan=2>&nbsp;&nbsp;Motivo:&nbsp;&nbsp;
			  <textarea rows=2 style="border-style: outset;" name="txtMotivo" cols="105" 
				 onfocus="javascript:this.select();"><%=request.getAttribute("txtMotivo") %></textarea>
	        </td>
        <td align="center" valign="middle">         
        <% if (ProcessoBeanId.getNomStatus().length()>0) { %>		
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('CadastraProc',this.form);">	
    	      <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
        	  </button>
		<% } %> 
		 </td>
			
      </tr>	 
     <% } %>  
  </table>		
</div>

<%if (ProcessoBeanId.getNumProcesso().length()>0){%>
	<script>document.CadatraProc.numprocesso.focus();</script>
<%}%>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro       = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop    = "160 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>