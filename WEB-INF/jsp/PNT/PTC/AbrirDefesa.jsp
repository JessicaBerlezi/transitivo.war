<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>

<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="AbrirRecBeanId" scope="request" class="PNT.PTC.AbrirRecBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	   case 'AbreRecurso':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	// validar as datas
	valid = ValDt(AbrirRecForm.datEntrada,1) && valid	
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AbrirRecForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao"     type="hidden" value=''>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
    	<td height="18" colspan="3"><strong><%= ProcessoBeanId.getMsgOk() %></strong></td>
      	<td>
    		<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('N',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>            
        </td>
	</tr>         
    <tr><td height="4" colspan="4"></td></tr>	
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>    
    <tr bgcolor="#d9f4ef"> 
	    <td width="180" height="12"><strong>Data de Entrada:</strong>&nbsp;
			<input type="text" name="datEntrada" size="12" maxlength="10" value='<%=AbrirRecBeanId.getDatEntrada() %>'  onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
		</td>
	    <td align="right"><strong>E-Mail/Telefone:&nbsp;</strong></td>
    	<td width="340" align="left">
    		<input type="text" name="txtEMail" size="61" maxlength="60" value='<%=AbrirRecBeanId.getTxtEMail() %>'  onfocus="javascript:this.select();">
    	</td>
    	<td></td>
    </tr>
    <tr><td height="2" colspan="4" style="background-color: #ffffff"></td></tr>
    <tr bgcolor="#d9f4ef">
	    <td></td>
		<td align="right"><strong>Documento Gerador:&nbsp;</strong></td>
		<td  align="left">
	  		<input type="text" name="txtMotivo" size="61" maxlength="50" value='<%=AbrirRecBeanId.getTxtMotivo() %>'  onfocus="javascript:this.select();">
	  	</td>
      	<td>
	    	<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('AbreRecurso',this.form);">			  
		  	<img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19">
			</button>
	  	</td>
	</tr>	     
		<% } %>
  </table>
</div>
<% if("S".equals(ProcessoBeanId.getEventoOK())) { %> 
	<script>document.AbrirRecForm.datEntrada.focus();</script>	        
<% } %>
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro       = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</body>
</html>