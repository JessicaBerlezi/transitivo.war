<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	   case 'Etiquetas':
			if (fForm.codRequerimento.value=="") {
				alert("Selecione um Requerimento.");
			}
			else {
				fForm.acao.value=opcao;
		   		fForm.target= "_self";
		    	fForm.action = "acessoTool";  
		   		fForm.submit();
		   	}
		   	break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmEtiqueta" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao"   type="hidden" value=''>						
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
    	<td height="18" colspan="4">
    		<strong><%= ProcessoBeanId.getMsgOk() %></strong>
    	</td>
    </tr>
    <tr><td height="6" colspan="4"></td></tr>	
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>
	    <tr bgcolor="#d9f4ef">
	        <td height=23 valign="top" width="60" >&nbsp;&nbsp;Requerimento:&nbsp;</td>
    	    <td align="left" valign="top"> <%= ProcessoBeanId.getReqs() %>       </td>
	        <td valign="middle" width="200" align="right"></td>
    	  	<td>
		    	<button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('Etiquetas',this.form);">	
	    			<img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
	        	</button>
		  	</td>
		</tr>	     
	<% } %>
  </table>
</div>
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro       = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop    = "160 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</body>
</html>