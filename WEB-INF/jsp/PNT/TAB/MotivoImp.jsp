<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="MotivoId" scope="request" class="PNT.TAB.MotivoBean" />
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="ConsultaLogAcessoImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Iterator it = ((List)MotivoId.getMotivos()).iterator() ;
			PNT.TAB.MotivoBean motivo  = new PNT.TAB.MotivoBean();					
			if (((List)MotivoId.getMotivos()).size()>0) { 
				while (it.hasNext()) {
					motivo   = (PNT.TAB.MotivoBean)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%63 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
							<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>
					  <%}%>				
						<%@ include file="MotivoCabImp.jsp" %>	
						
				  <%}%>	
				
		<div id ="nomes" width="720">											
		    <table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">							
				<tr>  		 	  
		         <td width="90">&nbsp;&nbsp;<%= motivo.getCodMotivo() %> </td>
		 		 <td>&nbsp;&nbsp;<%= motivo.getDescricao()%></TD>							
				</tr>
				<tr><td width="90" height=1 bgcolor="#000000" colspan=7></td>
			<%	} %>
			</table>
		</div>	      
		<%} else { %>
					<%@ include file="EventoCabImp.jsp" %>						
		
					<table class="semborda">
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
		<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>