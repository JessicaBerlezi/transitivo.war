<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="OrigemEventoId" scope="request" class="PNT.TAB.OrigemEventoBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'A':
			      if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'I':
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();			   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
			
				// <!--valida sigla se for vazia-->
				for (k=0;k<fForm.dscOrigemEv.length;k++) {
				
					var codigo 		 = trim(fForm.codOrigemEv[k].value);
		     		var descricao 	 = trim(fForm.dscOrigemEv[k].value);				   	
				   
				   	if( (codigo!="" && descricao=="")){
				   	 	valid = false
				   	 	sErro = sErro + "O campo Descri��o deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   		fForm.dscOrigemEv[k].focus();
				   	}				   	
				   	if( codigo=="" && descricao!="" ){
				     	valid = false
				     	sErro = sErro + "O campo C�digo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   		fForm.codOrigemEv[k].focus();
				   }
				}
	
				for (k=0;k<fForm.codOrigemEv.length;k++) {
				
					var cod  = trim(fForm.codOrigemEv[k].value)
					var desc = trim(fForm.dscOrigemEv[k].value)
					
				   	if (cod.length==0) continue ;
				   	
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.codOrigemEv.length;m++) {
					
						var codDup  = trim(fForm.codOrigemEv[m].value)
						var descDup = trim(fForm.dscOrigemEv[m].value)
						
					   	if ((m!=k) && (codDup.length!=0) && (codDup==cod)) {
						   	sErro += "O C�digo " + cod + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.codOrigemEv[m].focus();
						   	return;			   
					   	}
					   	
					   	if ((m!=k) && (descDup.length!=0) && (descDup==desc)) {
						   	sErro += "A descri��o " + desc + " foi duplicada: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.dscOrigemEv[m].focus();
						   	return;			   
					   	}
					}
				}
				return valid ;
			}			
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="juntaForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		
<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 87px; left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:114px; height:43%;"> 
<div class="divTitulos" id="titPenalidade" style=" height:20px;">   
    		<table cellspacing="1" class="titulos">  	   		
				<tr bgcolor="#993300"> 
					<td width="30" align="center" bgcolor="#8AAEAE">						
							<font color="#ffffff">
								<b>Seq</b>
							</font>
					</td>		
      				<td width="111" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>C�digo</b>
							</font>
					</td>		 	  
      				<td width="" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Descri��o</b>
							</font>
					</TD>					
 				</tr>
   			</table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
		
<div class="divCorpo" id="corpoPenalidade" style="height: 100%; top: 0px;" >
    		<table id="fevento" cellspacing="1"  class="corpo"> 
     			<%	
     				if(OrigemEventoId.getOrigensEventos().get(0).getCodOrigEvento().equals(-1))	
     					OrigemEventoId.setMsgErro("N�o existem origens de eventos cadastradas.\nPara cadastrar uma nova origem de evento preencha os campos abaixo e clique em atualizar.");
     				
     				int codOrigem;	
					for (int i=0; i<OrigemEventoId.getOrigensEventos().size(); i++) {						
						codOrigem = OrigemEventoId.getOrigensEventos().get(i).getCodOrigEvento();						   
				%>
      					<tr bgcolor="#d9f4ef">
      						<td width="30" align="center">        					
             					<input name="seq" type="text" size="3" maxlength="3" readonly="readonly" value="<%=i+1%>" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td>
							<td width="111" align="center"> 
							<%	if(codOrigem != -1){ %>
          							<input name="codOrigemEv" type="text" size="18" maxlength="03"  value="<%=codOrigem%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
							<%	}else{%>
        							<input name="codOrigemEv" type="text" size="18" maxlength="03"  value="" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">	
        					<%	}%>
							</td>	
        					<td width="" align="center">        					
             					<input name="dscOrigemEv" type="text" size="107" maxlength="50"  value="<%=OrigemEventoId.getOrigensEventos().get(i).getDescricao().trim() %>" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td>        								
						</tr>
				<% 	}%>
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	  	
	  	<!--Div Erros -->

<%  	
String msgErro       = OrigemEventoId.getMsgErro();
String msgErroTop    = "230 px" ;
String msgErroLeft   = "130 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>		
		
		
		
		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>


	