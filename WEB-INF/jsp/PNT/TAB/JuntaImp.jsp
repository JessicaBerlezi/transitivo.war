<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto -->
<jsp:useBean id="JuntaId" scope="request" class="PNT.TAB.JuntaBean" />
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="JuntaImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Integer codJunta =0;
			Iterator it = ((List)JuntaId.getJuntas()).iterator() ;
			PNT.TAB.JuntaBean junta  = new PNT.TAB.JuntaBean();					
			if (((List)JuntaId.getJuntas()).size()>0) { 
				while (it.hasNext()) {
					junta   = (PNT.TAB.JuntaBean)it.next() ;					
					seq++;
					contLinha++;
					codJunta = Integer.parseInt(junta.getCodJunta());												   
					
					if (contLinha%66 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
									<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>
					  <%}%>					  				
								    <%@ include file="JuntaCabImp.jsp" %>	
			<table id="fjunta" cellspacing="1"  class="corpo" width="100%">			
				<tr bgcolor="#C0BDBD"> 
					<td width="35" >							
								&nbsp;<b>Seq</b>							
					</td>      				
      				<td width="325" >						
								&nbsp;<b>Nome</b>
					</td>	
					<td width="140"  >						
								&nbsp;<b>Sigla</b>						
					</td>					
					<td >					
								&nbsp;<b>Tipo</b>	     
        			</td>     						
 				</tr>
 				
			 </table>
					  <%}%>	
				
											
		    <table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">							
				<tr >
      						<td width="35" >        					
             					&nbsp;&nbsp;<%=seq%>
        					</td>							
        					<td width="325" >        					
             					&nbsp;&nbsp;&nbsp;<%=junta.getNomeJunta()%>
        					</td>							
							<td width="140" >        					
             					&nbsp;&nbsp;&nbsp;<%=junta.getSigJunta().trim() %>
        					</td>							
        					<td >        					
        						&nbsp;&nbsp;&nbsp;&nbsp;<%=junta.getTipoJunta()%>        				            
        					</td>     								
						</tr>
						<tr><td height=1 bgcolor="#000000" colspan=5></td></tr>
			<%	} %>
			</table>
	      
		<%} else { %>
					<%@ include file="JuntaCabImp.jsp" %>	
					
					<table class="semborda">
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
				<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>