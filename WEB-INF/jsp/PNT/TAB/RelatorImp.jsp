<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:    
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto  -->
<jsp:useBean id="RelatorBeanId" scope="request" class="PNT.TAB.RelatorBean" />
<jsp:useBean id="JuntaBeanId" scope="request" class="PNT.TAB.JuntaBean" />
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="RelatorImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Iterator it = ((List)RelatorBeanId.getRelatores()).iterator() ;
			PNT.TAB.RelatorBean relator  = new PNT.TAB.RelatorBean();					
			if (((List)RelatorBeanId.getRelatores()).size()>0) { 
				while (it.hasNext()) {
					relator   = (PNT.TAB.RelatorBean)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%63 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
									<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>
					  <%}%>					  				
								    <%@ include file="RelatorCabImp.jsp" %>	
						
						<div id="nomes" width="720px">
							<table id="TabVar1"width="100%" border="0" cellpadding="0" class="semborda" cellspacing="1">
      							<tr><td colspan="6"><strong><font color="#000000">
      									Nome da Junta:&nbsp;&nbsp;</font></strong><%=JuntaBeanId.getNomeJunta()%>
      								</td>
      							</tr>
      							<tr bgcolor="#C0BDBD"> 
        							<td width="50" height="14"><strong><font color="#000000">&nbsp;
        								C�digo</font></strong></td>
        							<td width="270" height="14"><strong><font color="#000000">&nbsp;
        								Nome</font></strong></td>
        							<td width="100" height="14"><strong><font color="#000000">&nbsp;
        								CPF</font></strong></td>
        							<td width="220" height="14"><strong><font color="#000000">&nbsp;
        								T�tulo</font></strong></td>
        							<td ><strong><font color="#000000">&nbsp;
        								Ativo</font></strong></td>
      							</tr>
							</table>
						</div>
				  <%}%>	
				
		<div id ="nomes" width="720">											
		    <table id="RelatorImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">							
				<tr>  		 	  
		        	<td width="50" height="14" >&nbsp;&nbsp;<%=relator.getCodRelator() %> </td>
		 		 	<td width="270" height="14">&nbsp;&nbsp;<%=relator.getNomRelator()%></td>
		 		 	<td width="100" height="14">&nbsp;&nbsp;<%=relator.getNumCpf()%></td>
		 		 	<td width="220" height="14">&nbsp;&nbsp;<%=relator.getNomTitulo()%></td>
		 		 	<td >&nbsp;                             <%=relator.getIndAtivo()%></td>
				</tr>
				<tr><td height=1 bgcolor="#000000" colspan=7></td></tr>
			<%	} %>
			</table>
		</div>	      
		<%} else { %>
					<%@ include file="RelatorCabImp.jsp" %>	
					
					<table class="semborda">
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="RelatorImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
				<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>