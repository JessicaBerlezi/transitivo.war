<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="PNT.TAB.RelatorBean" %>  

<!-- Chama o Objeto do Usuario logado -->

<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="JuntaId" scope="request" class="PNT.TAB.JuntaBean" />
<jsp:useBean id="RelatorId" scope="request" class="PNT.TAB.RelatorBean" />
<!-- Chama o Objeto da Tabela de Orgaos -->

<html>
<head>
<%@ include file="../Css.jsp" %>
<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="relatorForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
var	sErro = "" ;
<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

myJunta      = new Array(<%= JuntaId.getJunta() %>);
cod_junta    = new Array(myJunta.length/4);
cod_juntaOrg = new Array(myJunta.length/4);
sig_junta    = new Array(myJunta.length/4);

for (i=0; i<myJunta.length/4; i++) {
	cod_junta[i]    = myJunta[i*4+0] ;
	cod_juntaOrg[i] = myJunta[i*4+1] ;	
	sig_junta[i]    = myJunta[i*4+3] ;
}

function setaJunta(objComboEscolha,objComboMontar) {
	var codOrgao =  objComboEscolha.value
	// esvazia combo de grupos
	objComboMontar.length = 1
	objComboMontar.options[0] = new Option(" ")
	k=1	
	for(i=0;i<myJunta.length;i++){
	    if(cod_juntaOrg[i]== codOrgao){		
		   objComboMontar.options[k] = new Option(sig_junta[i],cod_junta[i])
	       k++	
	    }
	}
}

<!-- Fim de preenchimento da combo -->


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaRelator':
		if ((fForm.codOrgao.value==0)||(fForm.codJunta.value == 0)) {
		   alert("�rg�o ou Junta n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else{
		fForm.textoErro.innerText = sErro;
		document.all["MsgErroCliente"].style.visibility = 'visible'
	  } 
	  break ;
   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	  
		fForm.acao.value=opcao;
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	
   case '1':
   		  if (document.layers) fForm.layers["MsgErroCliente"].visibility='hide' ; 
	      else                 document.all["MsgErroCliente"].style.visibility='hidden' ; 
	  break;      
  }
}

function veCampos(fForm) {
	valid   = true ;
	sErro   = "";
	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codJunta.value==0) {
	   valid = false
	   sErro += "Junta n�o selecionado. \n"
	}	
	for(var i=0;i<fForm.nomRelator_aux.length;i++){
		if( (document.all["nomRelator"+i].value != document.all["nomRelatorBroker"+i].value) || (document.all["numCpf"+i].value != document.all["numCpfBroker"+i].value) ){
			sErro = sErro + "Erro na linha: " + fForm.seq[i].value + " | Relator do Smit difere ao do Detran \n"
			valid = false
		}
	}
   return valid ;
}

function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if (valor.length != 0) {     
     valor = Tiraedt(valor,11);
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
		 sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" \n"		 
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}

function moveNome(obj1,obj2) {
	obj1.value = obj1.value.toUpperCase()
	obj2.value = obj1.value
}

function moveCpf(obj1, obj2) {
	obj2.value = obj1.value 
	veCPF(obj1,0)
}

</script>




<input name="acao" type="hidden" value=' '>	
<input name="atualizarDependente" type="hidden" value="<%= (String)request.getAttribute("atualizarDependente") %>">				
<div id="WK_SISTEMA" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 70px; left:204px; visibility: visible;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="400"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>     
	  </tr>
      <tr>
		  <td align="right" width="20%"><b>�rg�o:&nbsp;</b>
   			<% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>				  
		    	<select name="codOrgao" onChange="javascript: setaJunta(this,document.relatorForm.codJunta)" > 
				<option value="0">&nbsp;&nbsp;</option>
			    <script>
					for (j=0;j<cod_orgao.length;j++) {
					document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
		<td align=center width=40%><b>Junta:&nbsp;</b>
			<select name="codJunta"> 
				<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			</select>
           	<% }
			else { %>
		        <input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
		        <input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
	    <td align=center width=40%><b>Junta:&nbsp;</b>
			      <input name="codJunta" type="hidden" size="6" maxlength="20"  value="<%= JuntaId.getCodJunta() %>"> 
		          <input disabled name="sigJunta" type="text" size="10" value="<%= JuntaId.getSigJunta() %>">
	    </td>	
			 <% } %>			
			  </td>		  
		    </td>
		     <td align=left width=350>&nbsp;&nbsp;&nbsp;
	    		<% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>			
        		  	<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaRelator',this.form);">	
	         	    <IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
				<%} %>
			 </td>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
		<% if (((String)request.getAttribute("atualizarDependente")).equals("S")){ %>			
	 	   <td width="33%" align="left"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
     	   </td>
	       <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
		<%} %>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 134px; height: 32px; visibility: visible;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<tr bgcolor="#FFFFFF">
	<td width=60></td>
	<td width=10></td>
	<td colspan="2"><font color="#000000">Listagem SMIT</font></td>
	<td width=10 bgcolor="#FFFFFF"></td>
	<td colspan="2"><font color="#000000">Listagem DETRAN</font></td>
	</tr>
	<tr bgcolor="#8AAEAE"> 
        <td width=60 align="center"><font color="#ffffff"><b>Seq</b></font></TD>		 	  
        <td width=10 bgcolor="#FFFFFF"></td> 
        <td width=190 align="center"><font color="#ffffff"><b>Relator</b></font></TD>		 
        <td width=120  align="center"><font color="#ffffff"><b>CPF</b></font></TD>	
		<td width=10 bgcolor="#FFFFFF"></td> 
        <td width=190 align="center"><font color="#ffffff"><b>Relator</b></font></TD>		 
        <td align="center"><font color="#ffffff"><b>CPF</b></font></TD>	

	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 160px; height: 191px; visibility: visible;"> 
    <table id="tabela" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (((String)request.getAttribute("atualizarDependente")).equals("S")) {				   
  	            	   List relatorSmit = (List)request.getAttribute("RelatorSmitId");
  	            	   List relatorBroker = (List)request.getAttribute("RelatorBrokerId");
					   for (int i=0; i<relatorSmit.size(); i++) {	   %>
      <tr bgcolor="#d9f4ef">
		<td width=60 align="center"> 
          <input name="seq" type="text" size="4" value="<%= i+1 %>">
		  <input name="codRelator"    type="hidden"  value="<%= ((RelatorBean)relatorSmit.get(i)).getCodRelator() %>">
		</td>
		<td width=10 bgcolor="#FFFFFF" ></td>
        <td width=190 align="center"> 
          <input name="nomRelator<%=i%>"     type="text" size="30" maxlength="43"  value="<%= ((RelatorBean)relatorSmit.get(i)).getNomRelator().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="moveNome(nomRelator<%=i%>,nomRelatorBroker<%=i%>)">
          <input name="nomRelator_aux"     type="hidden" size="30" maxlength="43"  value="<%= ((RelatorBean)relatorSmit.get(i)).getNomRelator().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" >
        </td>			
    	<td width=120  align="center"> 
	      <input name="numCpf<%=i%>"  type="text" size="17" maxlength="14"  value="<%= ((RelatorBean)relatorSmit.get(i)).getNumCpfEdt()%>" onkeypress="javascript:Mascaras(this,'999.999.999-99');" onfocus="javascript:this.select();" onChange = "moveCpf(numCpf<%=i%>,numCpfBroker<%=i%>)">	
	      <input name="numCpf_aux"  type="hidden" size="17" maxlength="14"  value="<%= ((RelatorBean)relatorSmit.get(i)).getNumCpf()%>" >	
		</td>
		<td width=10 bgcolor="#FFFFFF" ></td>
		<td width=190  align="center"> 
          <input name="nomRelatorBroker<%=i%>"     type="text" size="30" maxlength="43"  value="<%= ((RelatorBean)relatorBroker.get(i)).getNomRelator().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
          <input name="nomRelatorBroker_aux"     type="hidden" size="30" maxlength="43"  value="<%= ((RelatorBean)relatorBroker.get(i)).getNomRelator().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
          <input name="codRelatorBroker"    type="hidden"  value="<%= ((RelatorBean)relatorBroker.get(i)).getNumCpf() %>">
        </td>
		<td align="center"> 
	      <input name="numCpfBroker<%=i%>"  type="text" size="17" maxlength="14"  value="<%= ((RelatorBean)relatorBroker.get(i)).getNumCpfEdt()%>" onkeypress="javascript:Mascaras(this,'999.999.999-99');" onfocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);">	
	      <input name="numCpfBroker_aux"  type="hidden" size="17" maxlength="14"  value="<%= ((RelatorBean)relatorBroker.get(i)).getNumCpf()%>" >	
		</td>
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#d9f4ef">
		<td width=60 align="center"> 
          <input name="seq" type="text" size="4" value="">
        </td>
        <td width=10 bgcolor="#FFFFFF" ></td>
	   	<td width=190 align="center"> 
          <input name="nomRelator" readonly type="text" size="30" maxlength="10" value="">
		</td>		
    	<td width=120 align="center"> 
          <input name="numCpf"  readonly type="text" size="17"  maxlength="10"  value="">
		</td>
	    <td width=10 bgcolor="#FFFFFF" ></td>
		 <td width=190 align="center"> 
          <input name="nomRelatorBroker" readonly type="text" size="30" maxlength="10" value="">
		</td>
		<td align="center"> 
          <input name="numCpfBroker"  readonly type="text" size="17"  maxlength="10"  value="">
		</td>
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>

<!-- Div de apresenta��o de mensagem de Erro-->
<div id="MsgErroCliente" style="POSITION: absolute; width:720; z-index: 999; left: 55px; top: 86px; visibility: hidden;">      
  <TABLE  border='1' cellpadding="0" cellspacing="0" width=80% align=center bgcolor="#e7e7e7">
    <tr><td colspan=3 height=18 bgcolor="#e7e7e7"></td></tr>
    <tr> 
	<td width=8% bgcolor="#e7e7e7"></td> 	
	<td width=84%> 
           <table border='0' cellpadding="0" cellspacing="0" width=100% bgcolor="#e7e7e7">
		    <tr><td align=center bgcolor="#e7e7e7">
				<textarea name="textoErro" rows="13" cols=85 align="center"></textarea></td></tr>			
		    <tr>	
			   <td align=center><button type=button NAME="Ok"   style="height: 23px; width: 44px;border: none; background: transparent;" onClick="javascript: valida('1',this.form);">	
					<IMG src="<%= path %>/images/ok_fundo_cinza.gif"  align=left ></button></td>
		    </tr>	
		  </table>
	 </td>
    <td width=8% bgcolor="#e7e7e7"></td> 			 
    </tr>
    <tr><td colspan=3 height=8 bgcolor="#e7e7e7"></td></tr>	
 </table>	
</div> 

<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  	  	<!--Div Erros -->
	  	
	  	<%  	
String msgErro       = RelatorId.getMsgErro();
String msgErroTop    = "230 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</BODY>
</HTML>


