<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="AgravoId" scope="request" class="PNT.TAB.AgravoBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'A':
			      if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'I':
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();			   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
			
				
				for (k=0;k<fForm.codInfracao.length;k++) {
				
					var cod  = trim(fForm.codInfracao[k].value)					
					
				   	if (cod.length==0) continue ;
				   	
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.codInfracao.length;m++) {
					
						var codDup  = trim(fForm.codInfracao[m].value)
						var descDup = trim(fForm.Agravo[m].value)
						
					   	if ((m!=k) && (codDup.length!=0) && (codDup==cod)) {
						   	sErro += "O C�digo Infra��o " + cod + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.codInfracao[m].focus();
						   	return;			   
					   	}					   
					}
				}
				return valid ;
			}			
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="juntaForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		
		<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 75px; left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="left: 200px; top:108px; height:43%; width:440px">
<div class="divTitulos" id="titStatus" style=" height:20px;">
<table cellspacing="1" class="titulos">	   		
				<tr bgcolor="#993300"> 
					<td width="50" align="center" bgcolor="#8AAEAE">						
							<font color="#ffffff">
								<b>Seq</b>
							</font>
					</td>
      				<td width="180" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>C�digo Infra��o</b>
							</font>					
					</td>		 	  
      				<td align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Fator de Agravamento</b>
							</font>
					</TD>							
 				</tr>
   			</table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoPenalidade" style="height: 100%; top: 0px;" >
    		<table id="fevento" cellspacing="1"  class="corpo">
     			<%	
     				if(AgravoId.getAgravos().get(0).getCodInfracao().equals(""))	
     					AgravoId.setMsgErro("N�o existem Agravos cadastrados.\nPara cadastrar um novo status de auto preencha os campos abaixo e clique em atualizar.");
     			
     				String codInfracao;
					for (int i=0; i<AgravoId.getAgravos().size(); i++) {		
						codInfracao = AgravoId.getAgravos().get(i).getCodInfracao();												   
				%>
      					<tr bgcolor="#d9f4ef">
      						<td width="50" align="center">        					
             					<input name="seq" type="text" size="3" maxlength="3"  value="<%=i+1%>" readonly="readonly" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();">
        					</td>
							<td width="180" align="center">
								<% if(! codInfracao.equals("-1")){%> 
									<input name="codInfracao" type="text" size="6"  maxlength="3"  value="<%=codInfracao%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
								<%}else{%>
									<input name="codInfracao" type="text" size="6"  maxlength="3"  value=""  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
								<%}%>
							</td>	
        					<td align="center">        					
             					<input name="Agravo" type="text" size="6" maxlength="2"  value="<%=AgravoId.getAgravos().get(i).getAgravo().trim() %>" 		onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td>  				
						</tr>
				<% 	}%>
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	  	
	  	<!--Div Erros -->
	  	
	  	<%  	
String msgErro       = AgravoId.getMsgErro();
String msgErroTop    = "200 px" ;
String msgErroLeft   = "-15 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>


	