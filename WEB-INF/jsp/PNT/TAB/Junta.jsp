<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto -->
<jsp:useBean id="JuntaId" scope="request" class="PNT.TAB.JuntaBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'A':
			      if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'I':
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();			   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
			
				// <!--valida sigla se for vazia-->
				for (k=0;k<fForm.dscJunta.length;k++) {					
		     		var codigo           = trim(fForm .codJunta[k].value);
		     		var sigla 	         = trim(fForm .dscJunta[k].value);	
		     		var nome 	         = trim(fForm.nomeJunta[k].value);
		     		var tipJunta 	     =           fForm.tipo[k].value;	
		     		var qtdRelatores 	 =   fForm.qtdRelatores[k].value;	    		
		     		
		     		if( (qtdRelatores!="") && (codigo!="-1" && nome=="" && sigla=="" && tipJunta=="")) {
					   
					   	 valid = false
					   	 sErro = sErro + "H� "+ qtdRelatores +" Relator(es) na junta \n Delete os relatores para depois deletar a junta. \n  Verifique a Seq "+(k+1)+" \n\n";
					   	 fForm.nomeJunta[k].focus();
					   	 break;
				    }		     		
				   if( (nome=="") && (codigo!="-1" || sigla!="" || tipJunta!="") ) {				   
				   	 valid = false
				   	 sErro = sErro + "- O campo Nome deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.nomeJunta[k].focus();
				   }		
				   
				   if( (sigla=="") && (codigo!="-1" || nome!="" || tipJunta!="") ) {
				   	 valid = false
				   	 sErro = sErro + "- O campo Sigla deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.dscJunta[k].focus();
				   }		
				   
				   if( (tipJunta=="") && (codigo!="-1" || nome!="" || sigla!="") ) {
				   	 valid = false
				   	 sErro = sErro + "- O campo Tipo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.tipo[k].focus();
				   }	
				   
				}
	
				for (k=0;k<fForm.codJunta.length;k++) {
				
					var nome  = trim(fForm.nomeJunta[k].value)
					var desc = trim(fForm.dscJunta[k].value)
					
				   	if (nome.length==0) continue ;
				   	
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.nomeJunta.length;m++) {
					
						var nomeDup  = trim(fForm.nomeJunta[m].value)
						var descDup = trim(fForm.dscJunta[m].value)
						
					   	if ((m!=k) && (nomeDup.length!=0) && (nomeDup==nome)) {
						   	sErro += "O Nome " + nome + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.nomeJunta[m].focus();
						   	return;			   
					   	}
					   	
					   	if ((m!=k) && (descDup.length!=0) && (descDup==desc)) {
						   	sErro += "A Sigla " + desc + " foi duplicada: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.dscJunta[m].focus();
						   	return;			   
					   	}
					}
				}
				return valid ;
			}			
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="juntaForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">		
		<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 75px; left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:108px; height:43%;">
<div class="divTitulos" id="titStatus" style=" height:20px;">
<table cellspacing="1" class="titulos">	   		
				<tr> 
					<td width="35" align="center" bgcolor="#8AAEAE">						
							<font color="#ffffff">
								<b>Seq</b>
							</font>
					</td>      				
      				<td width="325" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Nome</b>
							</font>
					</td>	
					<td width="140" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Sigla</b>
							</font>
					</td>		
					
					<td align="center" bgcolor="#8AAEAE">   
					<font color="#ffffff">
								<b>Tipo</b>
							</font>
							      
        			</td>     						
 				</tr>
   			</table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoJunta" style="height: 100%;" >
    		<table id="fevento" cellspacing="1"  class="corpo">
     			<%	
     				if(JuntaId.getJuntas().get(0).getCodJunta().equals(""))	
     					JuntaId.setMsgErro("N�o existem Juntas  cadastradas.\nPara cadastrar uma nova junta preencha os campos abaixo e clique em atualizar.");
     				String edita="";
     				Integer codJunta;
					for (int i=0; i<JuntaId.getJuntas().size(); i++) {		
						codJunta = Integer.parseInt(JuntaId.getJuntas().get(i).getCodJunta());															   
				%>
      					<tr bgcolor="#d9f4ef">
      						<td width="35" align="center">        					
             					<input name="seq" type="text" size="3" disabled="disabled" maxlength="3"  value="<%=i+1%>" readonly="readonly" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();">
        						<input type="hidden" name="qtdRelatores" value="<%=JuntaId.getJuntas().get(i).getQtdRelatores()%>">
        					</td>
							<td width="325" align="center">								      					
             					<input name="nomeJunta" type="text" size="50" maxlength="20"  value="<%=JuntaId.getJuntas().get(i).getNomeJunta()%>" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        						<input name="codJunta"  type="hidden" value="<%=codJunta%>" >
        					</td>							
							<td width="140" align="center">        					
             					<input name="dscJunta" type="text" size="12" maxlength="10"  value="<%=JuntaId.getJuntas().get(i).getSigJunta().trim() %>" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td> 
							
        					<td align="center">         					
        					&nbsp;<select name="tipo" style='border-style: outset;' onChange="this.value=this.value.toUpperCase()">
        					<option value="" <%= sys.Util.isSelected("",JuntaId.getJuntas().get(i).getIndTipo()) %>></option>        					
        					<option value="1" <%= sys.Util.isSelected("1",JuntaId.getJuntas().get(i).getIndTipo()) %>>Recurso de pontua��o</option>        					
        					<option value="0" <%= sys.Util.isSelected("0",JuntaId.getJuntas().get(i).getIndTipo()) %>>Defesa de Pontua��o</option>
        					<option value="2" <%= sys.Util.isSelected("2",JuntaId.getJuntas().get(i).getIndTipo()) %>>2� Inst. de pontua��o</option>        					
        					
        				</select>                
        					</td>     								
						</tr>
				<% 	}%>
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	  	
	  	<!--Div Erros -->
	  	
	  	<%  	
String msgErro       = JuntaId.getMsgErro();
String msgErroTop    = "200 px" ;
String msgErroLeft   = "-15 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>


	