<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<jsp:useBean id="PenalidadeId" scope="request" class="PNT.TAB.PenalidadeBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<style type="text/Css">
		.linhas td{border: 1px solid #ff0000;}
		</style>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="ConsultaLogAcessoImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Iterator it = ((List)PenalidadeId.getPenalidades()).iterator() ;
			PNT.TAB.PenalidadeBean penalidade  = new PNT.TAB.PenalidadeBean();					
			if (((List)PenalidadeId.getPenalidades()).size()>0) { 
				while (it.hasNext()) {
					penalidade  = (PNT.TAB.PenalidadeBean)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%60 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
							<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>
					  <%}%>				
						<%@ include file="PenalidadeCabImp.jsp" %>	
					
				  <%}%>	
				
													
		    <table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >							
				<tr>  					
		         <td rowspan="2" width="5%">&nbsp;&nbsp;<%= penalidade.getCodPenalidade() %> </td>
		 		 
		 		 <td width="45%">&nbsp;&nbsp;&nbsp;<%= penalidade.getEnquadramento()%></td>	
		 		 <td width="10%" align="left">&nbsp;&nbsp;&nbsp;
		 		 <%	if(penalidade.getCodFatorAgravamento() != "-1"){ %>
        			<%=penalidade.getCodFatorAgravamento()%>
        		 <%}%>        		
				 </td>			 		 
		 		 <td width="10%" align="left" style="padding-left: 8px"> 		 
		 		 &nbsp;At�:
		 		 <%	if(penalidade.getQtdInfracaoAte() != -1){%> 
		 		 		<%if(String.valueOf(penalidade.getQtdInfracaoAte()).length()==1){%> 
		 		 		 0<%= penalidade.getQtdInfracaoAte()%>
		 		 		<%}else{%>
		 		 		 <%= penalidade.getQtdInfracaoAte()%>
		 		 		 <%}%>		 		 		 
		 		 	<%}%>		 	
		 		 </td>			 		 
		 		 <td width="10%">
		 		 &nbsp;&nbsp;&nbsp;<%	if(penalidade.getNumPrazo() != -1){%> 
		 		 		<%if(String.valueOf(penalidade.getNumPrazo()).length()==1){%> 
		 		 		 0<%= penalidade.getNumPrazo()%>
		 		 		<%}else{%>
		 		 		 <%= penalidade.getNumPrazo()%>
		 		 		 <%}%>
		 		 		 &nbsp;Meses
		 		 	<%}%>	 
		 		 </td>			 		 		 		 
		 		 
		 		 <td  width="20%" >&nbsp;&nbsp;&nbsp;&nbsp;<%= penalidade.getReincidente()%></td>	
				</tr>
				<tr>
				<td colspan="3">&nbsp;&nbsp;&nbsp;Descri��o:&nbsp;<%= penalidade.getDscPenalidade()%></td>
				<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;Descri��o Resumida:&nbsp;<%= penalidade.getDscResumida()%></td>
				</tr>
				<tr><td height="1" bgcolor="#000000" colspan="6"></td></tr>
			<%	} %>
			</table>
			      
		<%} else { %>
					<%@ include file="EventoCabImp.jsp" %>	
					
					<table class="semborda">
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
		<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>