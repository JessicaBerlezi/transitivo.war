<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es: 
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path = request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="PenalidadeId" scope="request" class="PNT.TAB.PenalidadeBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>				
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'A':
			      if (veCampos(fForm)==true) {			      
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':
				  close() ; 			  
				  break;
			   case 'I':
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();			   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
			
				// <!--valida sigla se for vazia-->
				for (k=0;k<fForm.dscPenalidade.length;k++) {
				
					var codigo 		  = trim(fForm.codPenalidade[k].value);
		     		var descricao 	  = trim(fForm.dscPenalidade[k].value);
				   	var enquadramento = trim(fForm.Enquadramento[k].value);
				   	var prazo         = trim(fForm.prazo[k].value);				   
				   	var reincid       = fForm.reincidente[k].selectedIndex;
				 
				   	if( (codigo!="" && descricao=="")){
				   	 	valid = false
				   	 	fForm.codPenalidade[k].focus();
				   	 	sErro = sErro + "O campo Descri��o deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	}
				   	if( (codigo!="" && enquadramento=="") ){
				   	 	valid = false
				   	 	fForm.Enquadramento[k].focus();
				   	 	sErro = sErro + "O campo Enquadramento deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	}
				   	if( (codigo!="" && prazo=="") ){
				   	 	valid = false
				   	 	fForm.prazo[k].focus();
				   	 	sErro = sErro + "O campo Prazo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	}
				   
				   	if( (codigo!="" && reincid==0) ){
				   	 	valid = false
				   	 	fForm.prazo[k].focus();
				   	 	sErro = sErro + "O campo Reincid�ncia deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	}				   	
				   	if( codigo=="" && (descricao!="" || enquadramento!="" || prazo!="" ) ){
				     	valid = false
				     	fForm.codPenalidade[k].focus();
				     	sErro = sErro + "O campo C�digo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   }
				}
	
				for (k=0;k<fForm.codPenalidade.length;k++) {
				
					var cod  = trim(fForm.codPenalidade[k].value)
					var desc = trim(fForm.dscPenalidade[k].value)
					
				   	if (cod.length==0) continue ;
				   	
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.codPenalidade.length;m++) {
					
						var codDup  = trim(fForm.codPenalidade[m].value)						
						
					   	if ((m!=k) && (codDup.length!=0) && (codDup==cod)) {
						   	sErro += "O C�digo " + cod + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.codPenalidade[m].focus();
						   	return;			   
					   	}					   
					}
				}
				return valid ;
			}			
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="juntaForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		
		<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 75px; left:50px;"> 
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		  
		<!--INICIO CABEC DA TABELA-->
<div class="divPrincipal" style="top:108px; height:42%;">
<div class="divTitulos" id="titPenalidade">   
    		<table cellspacing="1" class="titulos">    					
				<tr> 
					<td width="40" align="center" bgcolor="#8AAEAE" rowspan="2">						
							<font color="#ffffff">
								<b>Seq</b>							</font>										</td>
      				<td width="50" align="center" bgcolor="#8AAEAE" rowspan="2">
							<font color="#ffffff">
								<b>C�digo</b>							</font>					</td>		 	  
      				<td colspan="3" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Descri��o</b>							</font>					</td>		
					<td colspan="2" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Enquadramento</b>							</font>				</td>
				</tr>
				<tr>
				    <td width="160" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Descri��o Resumida</b>							</font>			</td>
				
				    <td width="130" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Fator de Agravamento</b>							</font>			</td>
					<td width="90" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Qtd. Infra��es</b>							</font>			</td>
					
					
					
					<td width="100" align="center" bgcolor="#8AAEAE"><font color="#ffffff">
								<b>Penalidade(meses)</b>							</font></td>
					<td align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Reincid�ncia</b>							</font>					</td> 
 				
 				
 				</tr>
   			</table>
	</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoPenalidade" style="height: 100%;" >
    		<table id="fevento" cellspacing="1"  class="corpo">
     			<%	
     				if(PenalidadeId.getPenalidades().get(0).getCodPenalidade().equals("-1") && (PenalidadeId.getMsgErro().equals("")) )	
     					PenalidadeId.setMsgErro("N�o existem penalidades cadastradas.\nPara cadastrar uma nova penalidade preencha os campos abaixo e clique em atualizar.");
     				
     				int codPenalidade;	
					for (int i=0; i<PenalidadeId.getPenalidades().size(); i++) {						
						codPenalidade = Integer.parseInt(PenalidadeId.getPenalidades().get(i).getCodPenalidade());						   
				%>
      					<tr>
      						<td width="40" align="center" rowspan="2">        					
             					<input name="seq" type="text" size="3" maxlength="3"  value="<%=i+1%>" readonly="readonly" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">        					</td>
							<td width="50" align="center" rowspan="2"> 							
							<%	if(codPenalidade != -1){ %>
          							<input name="codPenalidade" type="text" size="4" maxlength="3"  value="<%=codPenalidade%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
							<%	}else{%>
        							<input name="codPenalidade" type="text" size="4" maxlength="3"  value="" 		onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">	
        					<%	}%>							</td>	
        					<td colspan="3" align="center">        					
             					<input name="dscPenalidade" type="text" size="70" maxlength="120"  value="<%=PenalidadeId.getPenalidades().get(i).getDscPenalidade().trim() %>" 	onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">        					</td>
        					<td colspan="2" > 
             					<input name="Enquadramento" type="text" size="28" maxlength="20"  value="<%=PenalidadeId.getPenalidades().get(i).getEnquadramento().trim() %>" 	onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">        					</td>        					
        				</tr>
						<tr>
						
						<td width="160">&nbsp;
        				<input name="dscResumida" type="text" size="26" maxlength="20"  value="<%=PenalidadeId.getPenalidades().get(i).getDscResumida()%>"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        				</td>
						
        					<td width="130" style="text-align: center">&nbsp;
        					<%	if(PenalidadeId.getPenalidades().get(i).getCodFatorAgravamento() != "-1"){ %>
        							<input name="codFatorAgravamento" type="text" size="4" maxlength="1"  value="<%=PenalidadeId.getPenalidades().get(i).getCodFatorAgravamento()%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        						<%	}else{%>
        							<input name="codFatorAgravamento" type="text" size="4" maxlength="1"  value=""  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					<%	}%>	
        					</td>
							
             				<td width="90" style="text-align: center">
             				<!--
             				De&nbsp;             				
        					<%--	if(PenalidadeId.getPenalidades().get(i).getQtdInfracaoDe() != -1){ %>
        							<input name="qtdInfracaoDe" type="text" size="4" maxlength="3"  value="<%=PenalidadeId.getPenalidades().get(i).getQtdInfracaoDe()%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        						<%	}else{%>
        							<input name="qtdInfracaoDe" type="text" size="4" maxlength="3"  value=""  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					<%	}--%>	
        					-->
        					
        					&nbsp;at�&nbsp;        					
	        					<%	if(PenalidadeId.getPenalidades().get(i).getQtdInfracaoAte() != -1){ %>
             						<input name="qtdInfracaoAte" type="text" size="4" maxlength="3"  value="<%=PenalidadeId.getPenalidades().get(i).getQtdInfracaoAte()%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
             					<%	}else{%>
        							<input name="qtdInfracaoAte" type="text" size="4" maxlength="3"  value=""  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					    <%	}%>	
             				</td>
        					
        					<td width="100" style="text-align:left">&nbsp;
	        					<%	if(PenalidadeId.getPenalidades().get(i).getNumPrazo() != -1){ %>
             						&nbsp;&nbsp;&nbsp;<input name="prazo" type="text" size="4" maxlength="3"  value="<%=PenalidadeId.getPenalidades().get(i).getNumPrazo()%>" 	onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" style="text-align: right">
             					<%	}else{%>
        							&nbsp;<input name="prazo" type="text" size="4" maxlength="3"  value="" 	onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" style="text-align: right">
        					    <%	}%>	
             					</td>
        					<td  style="text-align:center">
	        					
								<select name="reincidente" style="width: 50px">
		        					<option value="">&nbsp;</option>								
		        					<option value="N" <%if(PenalidadeId.getPenalidades().get(i).getReincidente().equals("N")){%>selected<%}%> >N&Atilde;O&nbsp;</option>
		        					<option value="S" <%if(PenalidadeId.getPenalidades().get(i).getReincidente().equals("S")){%>selected<%}%> >SIM&nbsp;</option>
								</select></td>
								
											
						</tr>
						<tr><td colspan="7" style="height: 2px; background:#8aaeae;"></td></tr>
				<% 	}%>
			</table>
	</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp"%>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	  	
	  	<!--Div Erros -->
<%  	
String msgErro       = PenalidadeId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
	  	<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
		
		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>