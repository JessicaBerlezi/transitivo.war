<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos  -->
<jsp:useBean id="MotivoId" scope="request" class="PNT.TAB.MotivoBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'A':
			      if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':
				  close() ;   
				  break;
			   case 'I':
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();			   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
			
				// <!--valida sigla se for vazia-->
				for (k=0;k<fForm.dscMotivo.length;k++) {
				
					var codigo 		 = trim(fForm.codMotivo[k].value);
		     		var descricao 	 = trim(fForm.dscMotivo[k].value);	
		     		var tipMotivo 	 = fForm.tipo[k].value;			   	
				   
				   	if( (codigo!="" && descricao=="")){
				   	 	valid = false
				   	 	sErro = sErro + "O campo Descri��o deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   		fForm.dscMotivo[k].focus();
				   	}				   	
				   	if( codigo=="" && descricao!="" ){
				     	valid = false
				     	sErro = sErro + "O campo C�digo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   		fForm.codMotivo[k].focus();
				   }
				   
				   if( codigo!="" && descricao!="" && tipMotivo==" " ){
				     	valid = false
				     	sErro = sErro + "Selecione o Tipo de cancelamento. \n  Verifique a Seq "+(k+1)+" \n\n";
				   		fForm.tipo[k].focus();
				   }
				}
	
				for (k=0;k<fForm.codMotivo.length;k++) {
				
					var cod  = trim(fForm.codMotivo[k].value)
					var desc = trim(fForm.dscMotivo[k].value)
					
				   	if (cod.length==0) continue ;
				   	
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.codMotivo.length;m++) {
					
						var codDup  = trim(fForm.codMotivo[m].value)
						var descDup = trim(fForm.dscMotivo[m].value)
						
					   	if ((m!=k) && (codDup.length!=0) && (codDup==cod)) {
						   	sErro += "O C�digo " + cod + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.codMotivo[m].focus();
						   	return;			   
					   	}
					   	
					   	if ((m!=k) && (descDup.length!=0) && (descDup==desc)) {
						   	sErro += "A descri��o " + desc + " foi duplicada: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.dscMotivo[m].focus();
						   	return;			   
					   	}
					}
				}
				return valid ;
			}			
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="juntaForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		
		<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 75px; left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		
		<!--INICIO CABEC DA TABELA-->  
		<div class="divPrincipal" style="top:108px; height:43%;">
<div class="divTitulos" id="titStatus" style=" height:20px;">
<table cellspacing="1" class="titulos">	   		
				<tr bgcolor="#993300"> 
					<td width="30" align="center" bgcolor="#8AAEAE">						
							<font color="#ffffff">
								<b>Seq</b>
							</font>
					</td>
      				<td width="80" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>C�digo</b>
							</font>					
					</td>		 	  
      				<td align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Descri&ccedil;&atilde;o</b>
							</font>
					</TD>							
 				</tr>
   			</table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
<div class="divCorpo" id="corpoPenalidade" style="height: 100%; top: 0px;" >
    		<table id="fevento" cellspacing="1"  class="corpo">
     			<%	
     				if(MotivoId.getMotivos().get(0).getCodMotivo().equals(""))	
     					MotivoId.setMsgErro("N�o existem Motivos de Cancelamento cadastrados.\nPara cadastrar um novo status de auto preencha os campos abaixo e clique em atualizar.");
     			
     				Integer codMotivo;
					for (int i=0; i<MotivoId.getMotivos().size(); i++) {		
						codMotivo = MotivoId.getMotivos().get(i).getCodMotivo();												   
				%>
      					<tr bgcolor="#d9f4ef">
      						<td width="30" align="center">        					
             					<input name="seq" type="text" size="3" maxlength="3"  value="<%=i+1%>" readonly="readonly" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();">
        					</td>
							<td width="80" align="center">
								<% if(! codMotivo.equals(-1)){%> 
									<input name="codMotivo" type="text" size="8"  maxlength="03"  value="<%=codMotivo%>"  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
								<%}else{%>
									<input name="codMotivo" type="text" size="8"  maxlength="03"  value=""  onkeypress="javascript:f_num();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
								<%}%>
							</td>	
        					<td align="center">        					
             					<input name="dscMotivo" type="text" size="90" maxlength="50"  value="<%=MotivoId.getMotivos().get(i).getDescricao().trim() %>" 		onkeypress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td>  				
						</tr>
				<% 	}%>
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  

		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->
	  	
	  	<!--Div Erros -->
	  	
	  	<%  	
String msgErro       = MotivoId.getMsgErro();
String msgErroTop    = "200 px" ;
String msgErroLeft   = "-15 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>


	