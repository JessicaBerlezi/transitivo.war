<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<jsp:useBean id="AgravoId" scope="request" class="PNT.TAB.AgravoBean" />

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="ConsultaLogAcessoImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Iterator it = ((List)AgravoId.getAgravos()).iterator() ;
			PNT.TAB.AgravoBean agravo  = new PNT.TAB.AgravoBean();					
			if (((List)AgravoId.getAgravos()).size()>0) { 
				while (it.hasNext()) {
					agravo  = (PNT.TAB.AgravoBean)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%60 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
							<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>
					  <%}%>				
						<%@ include file="AgravoCabImp.jsp" %>	
					
				  <%}%>	
				
		<div id ="nomes">											
		    <table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >							
				<tr>  					
		         <td width="80">&nbsp;&nbsp;<%= agravo.getCodInfracao() %> </td>
		 		 <td >&nbsp;&nbsp;&nbsp;
		 		 <%	if(agravo.getAgravo() != "-1"){ %>
        			<%=agravo.getAgravo()%>
        		 <%}%>        		
				 </td>		 		 
				</tr>
				<tr><td height=1 bgcolor="#000000" colspan=2></td></tr>
			<%	} %>
			</table>
		</div>	      
		<%} else { %>
					<%@ include file="AgravoCabImp.jsp" %>	
					
					<table class="semborda">
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
		<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>