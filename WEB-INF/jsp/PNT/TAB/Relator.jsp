<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto -->
<jsp:useBean id="RelatorBeanId" scope="request" class="PNT.TAB.RelatorBean" /> 
<jsp:useBean id="JuntaBeanId" scope="request" class="PNT.TAB.JuntaBean" /> 

<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="../Css.jsp" %>		
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
		
		<SCRIPT LANGUAGE="JavaScript1.2">
		
			<!-- Abre a janela com o tamanho maximo disponivel -->
			self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

			function valida(opcao,fForm) {
			 switch (opcao) {
			   case 'buscarRelatores':
			   	  fForm.acao.value=opcao;
				  fForm.target= "_self";
				  fForm.action = "acessoTool";  
				  fForm.submit();	
				  break;			   
			   case 'A':
			      if (veCampos(fForm)==true) {			      	
			      	for (k=0;k<fForm.codRelator.length;k++) {
			      	 	if (fForm.indAtivo[k].checked){
            				fForm.sIndAtivo.value = fForm.sIndAtivo.value + "1";
          				}else{
            				fForm.sIndAtivo.value = fForm.sIndAtivo.value + "0";
          				}          				
          			}
					fForm.acao.value=opcao;
				  	fForm.target= "_self";
				  	fForm.action = "acessoTool";  
				  	fForm.submit();  		  
				  }
				  else alert(sErro)
				  break ;
			   case 'R':			
			   	  close() ;  				 
				  break;
			   case 'I':
			   if (checaCombo()==true) {
				 fForm.acao.value=opcao;
				 fForm.target= "_blank";
				 fForm.action = "acessoTool";  		  
				 fForm.submit();
				}else alert (sErro)	   
				 break;
			   case 'O':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
				      else                 document.all["MsgErro"].style.visibility='hidden' ; 
				  break;  
			   case 'V':
			   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
				      else                 document.all["MsgErro"].style.visibility='visible' ; 
				  break;	  
			  }
			}

			function veCampos(fForm) {
				valid   = true ;
				sErro = "" ;
	 if (checaCombo()==true) {		
			// <!--valida nomRelator se for vazia-->
				for (k=0;k<fForm.nomRelator.length;k++) {				
				   var nome = trim(fForm.nomRelator[k].value);
				   var codigo = trim(fForm.codRelator[k].value);
				   var cpf = trim(fForm.numCpf[k].value);
				   var titulo = trim(fForm.nomTitulo[k].value);	
				
				   if( (nome=="") && (cpf!="" || titulo!="" )  ){
				   	 valid = false
				   	 sErro = sErro + "- O campo Nome deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.nomRelator[k].focus();
				   }
				   if( (cpf=="") && (nome!="" || titulo!="" ) ) {
				   	 valid = false
				   	 sErro = sErro + "- O campo CPF deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.numCpf[k].focus();
				   }	
				   
				   if( (titulo=="") && (nome!="" || cpf!="" ) ) {
				   	 valid = false
				   	 sErro = sErro + "- O campo T�tulo deve ser preenchido. \n  Verifique a Seq "+(k+1)+" \n\n";
				   	 fForm.nomTitulo[k].focus();
				   }			   	 		   	   
				   		   	   
				}	
				for (k=0;k<fForm.codRelator.length;k++) {				
				   var nome   = trim(fForm.nomRelator[k].value);
				   var codigo = trim(fForm.codRelator[k].value);
				   var cpf    = trim(fForm.numCpf[k].value);				   
				   
				   if (codigo.length==0) continue ;
				   
					<!-- verificar duplicata --> 
					for (m=0;m<fForm.codRelator.length;m++) {
					
					   var codDup  = trim(fForm.codRelator[m].value)
					   var nomDup  = trim(fForm.nomRelator[m].value)
					   var cpfDup  = trim(fForm.numCpf[m].value)					   
					   
					   if ((m!=k) && (codDup.length!=0) && (codDup==codigo)) {
						   sErro += "O C�digo " + codigo + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   valid = false ;
						   fForm.codRelator[m].focus();
						   return;				   
					   }
					   
					   if ((m!=k) && (nomDup.length!=0) && (nomDup==nome)) {
						   	sErro += "O Nome " + nome + " foi duplicada: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.nomRelator[m].focus();
						   	return;			   
					   	}
					   	
					   	if ((m!=k) && (cpfDup.length!=0) && (cpfDup==cpf)) {
						   	sErro += "O CPF " + cpf + " foi duplicado: (Seq: " +(k+1) + " e " + (m+1) + ")\n" ;
						   	valid = false ;
						   	fForm.numCpf[m].focus();
						   	return;			   
					   	}				   
					}
				}
			}	
				return valid ;
			}				
			
			function Mostra(valor){
			alert(valor);
			
			}
			
function checaCombo() {
	valid   = true ;
	sErro = "" ;
	if (<%=RelatorBeanId.getRelatores().size()%> == 0){			
		valid = false;
		sErro = sErro + " Selecione uma junta e tente novamente.";
	}
			
	return valid ;	
}
					
		</script>
	</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
	<form name="relatorForm" method="post" action="">
		<%@ include file="../Cab.jsp" %>
		
		<!-- CAMPO HIDDEN -->
		<input type="hidden" name="acao" value="">
		<input type="hidden" name="sIndAtivo" value="">
		
		<div id="WK_SISTEMA" style="position:absolute; width:200px; overflow: visible; z-index: 1; top: 87px; left:204px;" > 
			<TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<TR>
		    		<td width="" align="left"> 			 	   		
		                <strong>Junta:</strong>
		     	   	</td>
			       	<td  width="" align="center"> 
		                  <SELECT name="cmbJunta" onChange="javascript: valida('buscarRelatores',this.form);">
		                  	<OPTION></OPTION>
		                  	<% String selected="";
		                  		for(int i=0; i<JuntaBeanId.getJuntas().size(); i++){ 
		                  			if( RelatorBeanId.getCodJunta().equals(JuntaBeanId.getJuntas().get(i).getCodJunta()) )	
		                  				selected = "selected";
		                  			else
		                  				selected = "";
		                  	%>		                  		
		                  		<OPTION <%=selected%> value="<%=JuntaBeanId.getJuntas().get(i).getCodJunta()%>"><%=JuntaBeanId.getJuntas().get(i).getNomeJunta()%></OPTION>
		                  	<%}%>
		                  </SELECT>
		    	   	</td>
		    	</TR>
		    </table>
		</div>
		
		
		<!--INICIO BOTOES-->  
		<div id="WK_SISTEMA" style="position:absolute; right:15px; overflow: visible; z-index: 1; top: 120px; left:50px;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
		    	<tr>
			 		<td width="50%" style="text-align:center"> 			 	   		
		                <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
		                <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
		     	   	</td>
			       	<td  width="50%" style="text-align:center"> 
		                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
		                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
		    	   	</td>
		    	</tr>		
		  	</table>  
		</div>
		<!--FIM BOTOES-->
		
		<!--INICIO CABEC DA TABELA-->  

		<div class="divPrincipal" style="top:150px; height:36%;">
<div class="divTitulos" id="titRelator" style=" height:20px;">   
    		<table cellspacing="1" class="titulos">  
				<tr bgcolor="#993300"> 
					<td width="40" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Seq</b>
							</font>						
					</td>      				
      				<td width="260" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Nome</b>
							</font>
					</Td>
					<td width="90" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>CPF</b>
							</font>						
					</Td>	
					<td width="200" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Titulo</b>
							</font>
					</Td>
					<td width="" align="center" bgcolor="#8AAEAE">
							<font color="#ffffff">
								<b>Ativo</b>
							</font>
					</Td> 
 				</tr>
   			</table>
		</div>
		<!--FIM CABEC DA TABELA-->  
		
		<!--INICIO DO CORPO DA TABELA --> 
		
<div class="divCorpo" id="corpoRelator" style="height: 100%; top: 0px;" >
    		<table id="fevento" cellspacing="1"  class="corpo">
      			<%	for (int i=0; i<RelatorBeanId.getRelatores().size(); i++) {	%>
      					<tr bgcolor="#d9f4ef">
      						<td width="40" align="center"> 							
          						<input name="seq" type="text" size="3" maxlength="03"  value="<%=i+1%>">
								<input name="codRelator" type="hidden" size="6"  value="<%=((PNT.TAB.RelatorBean) RelatorBeanId.getRelatores().get(i)).getCodRelator().trim()%>">
							</td>							
        					<td width="260" align="center">        					
             					<input name="nomRelator" type="text" size="45" maxlength="50" value="<%=((PNT.TAB.RelatorBean) RelatorBeanId.getRelatores().get(i)).getNomRelator().trim()%>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" >
        					</td> 
        					<td width="90" align="center">        					
             					<input name="numCpf" type="text" size="14" maxlength="11" value="<%=((PNT.TAB.RelatorBean) RelatorBeanId.getRelatores().get(i)).getNumCpf().trim()%>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" >
        					</td> 
        					<td width="200" align="center">        					
             					<input name="nomTitulo" type="text" size="34" maxlength="50" value="<%=((PNT.TAB.RelatorBean) RelatorBeanId.getRelatores().get(i)).getNomTitulo().trim()%>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        					</td>
        					<td width="" align="center">     
        					<%
        						String checked="";
        						if(	(((PNT.TAB.RelatorBean) RelatorBeanId.getRelatores().get(i)).getIndAtivo().trim()).equals("S") )
        							checked = "checked";
        						else
        							checked ="";
        					%>   					
             					<input name="indAtivo" type="checkbox" size="10" maxlength="50" <%=checked%> onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" class="sem-borda">
        					</td>        													
						</tr>
				<% }%>	
			</table>
		</div>
</div>
 		<!--FIM DO CORPO DA TABELA-->  
			
		<!-- Rodap�-->
		<%@ include file="../Retornar.jsp" %>
		<%@ include file="../Rod.jsp" %>
		<!-- Fim Rodap� -->
		
	  	<!--FIM_CORPO_sistema-->

<%  	
String msgErro       = RelatorBeanId.getMsgErro();
String msgErroTop    = "230 px" ;
String msgErroLeft   = "130 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

 
		<!--FIM_Div Erros -->
	</form>
</BODY>
</HTML>
