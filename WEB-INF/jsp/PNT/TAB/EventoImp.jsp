<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator,java.util.List" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="EventoId" scope="request" class="PNT.TAB.EventoBean" />
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<%@ include file="CssImpressao.jsp" %>
		<!-- insere quebras de p�gina (somente para fins de impress�o)-->
		<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>../../js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
	<form name="ConsultaLogAcessoImp" method="post" action="">

		<!-- Inicio Div Detalhes vari�veis impressao --> 	 		 
		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;
			Iterator it = ((List)EventoId.getEventos()).iterator() ;
			PNT.TAB.EventoBean evento  = new PNT.TAB.EventoBean();					
			if (((List)EventoId.getEventos()).size()>0) { 
				while (it.hasNext()) {
					evento   = (PNT.TAB.EventoBean)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%56 == 1){
						npag++;							
						if (npag!=1){
		%>			 					
							<%@ include file="rodape_impconsulta.jsp" %>
							<div class="quebrapagina"></div>							
					  <%}%>				
						<%@ include file="EventoCabImp.jsp" %>	
						
				  <%}%>	
				
		<div id ="nomes" width="720">											
		    <table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda" >							
				<tr>  		 	  
		         <td width="90">&nbsp;&nbsp;<%= evento.getCodEvento() %> </td>
		 		 <td>&nbsp;&nbsp;<%= evento.getDscEvento()%></TD>	
				</tr>
				<tr><td height=1 bgcolor="#000000" colspan=7></td></tr>
			<%	} %>
			</table>
		</div>	      
		<%} else { %>
					<%@ include file="EventoCabImp.jsp" %>	
					
					<table>
						<td></td>
						<td><strong>N�O EXISTEM REGISTROS.</strong></td>
						<td></td>
					</table>
		
		<%}%>
		<table id="JuntaImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda">	
		<tr>
		<td><STRONG>Total de Registros: <%=seq %></STRONG></td>
		</tr>
		</table>	
		<%@ include file="rodape_impconsulta.jsp" %>
	</form>
</body>
</html>