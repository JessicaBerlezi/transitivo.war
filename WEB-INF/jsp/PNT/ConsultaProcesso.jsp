<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>

<jsp:useBean id="ProcessoBeanId"     scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<style>.borda td{border: 1px solid #ff0000;}</style>

<title>DETRAN &#8226;Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;
	   case 'ListaProcessos':	
			if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		  break;
	  case 'Novo':
		  fForm.acao.value=opcao;
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;
  }
}
function Mostra(opcoes,fForm,numproc) {
	document.all["acao"].value=opcoes;
	document.all["processoMostra"].value=numproc;
   	document.all["ConsultaProcesso"].target= "_blank";
    document.all["ConsultaProcesso"].action = "acessoTool";  
   	document.all["ConsultaProcesso"].submit();	 
}
function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	if ( (trim(fForm.numCpf.value)=='')
		&& (trim(fForm.numProcesso.value) =='')
		&& (trim(fForm.nomes.value)=='') ) {
			sErro += "Nenhuma op��o selecionada. \n"
			valid = false ;	
	}
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>
<body>
<form name="ConsultaProcesso" method="post" action="">

<%@ include file="Cab.jsp" %>


<div id="cons1" style="position:absolute; left:50px; right: 15px; top:75px; height:50px; z-index:3; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr height="20" bgcolor='#d9f4ef'> 
        <td width="8%" align="left"><strong>&nbsp;Processo:</strong></td>
        <td width="17%">
        	<input name="numProcesso" type="text"  value="<%=ProcessoBeanId.getNumProcesso() %>" size="26" maxlength="20" onfocus="javascript:this.select();">

        </td>
        <td width="5%"></td>
        <td width="9%"></td>
  		<td width="28%"></td>
		<td align="right"><strong>CPF:</strong>&nbsp;	
			<input name="numCpf" type="text" value="<%=ProcessoBeanId.getNumCPFEdt() %>" size="17" maxlength="14" 
		    onkeypress="javascript:Mascaras(this,'999.999.999-99');" onFocus="javascript:this.select();" onChange = "javascript:ValCPF(this,0);">
		    &nbsp;&nbsp;
		</td>
      </tr>
      <tr><td colspan=6 height="5"></td></tr>	  
	  <tr bgcolor='#d9f4ef'  height="20">
        <td><strong>&nbsp;Nome:</strong></td>
        <td colspan=3><input name="nomes" type="text" value="<%=ProcessoBeanId.getNomResponsavel() %>" size="65" maxlength="45" 
		    onkeypress="javascript:f_end()" onFocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
        </td>	  
        <td align="right" colspan="2">
			<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Localizarprocesso"  onClick="javascript: valida('ListaProcessos',this.form);">	
			  		<img src="<%= path %>/images/PNT/bot_ok.gif" alt="Confirmar" width="26" height="19">
			</button>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('Novo',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>
 		    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
      </tr>
    </table>
</div>
	

<div class="divPrincipal" style="top:125px; height:24%;">
<div class="divTitulos" id="cons3"> 
    <table width="100%" border="0" cellpadding="2" cellspacing="1">
      <tr bgcolor="#FFFFFF"> 
        <td height="14" colspan=5>
			<% String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";	 %> 			 		
			&nbsp;&nbsp;&nbsp;<%= msg %>&nbsp;
		</td>	
      </tr>
	</table>	  
	<table width="100%" border="0" cellpadding="2" cellspacing="1">	  
      <tr bgcolor="#8AAEAE" > 
        <td width="29" height="18" align="center"><strong>Seq</strong></td>
		<td width="140"><strong>&nbsp;N&ordm; Processo</strong></td>
        <td width="100"><strong>&nbsp;CPF</strong></td>
        <td width="300"><strong>&nbsp;Nome</strong></td>
        <td>&nbsp;<strong>CNH/PGU</strong></td>
      </tr>
	</table>
</div>
<div class="divCorpo" id="cons4" style="height: 100%; top: 0px;"> 
    <table id="autos" width="100%" border="0" cellpadding="2" cellspacing="1">
	  <%	  
       ProcessoBean proc  = new ProcessoBean();	
	   String cor   = "#d9f4ef";	       
	   for (int seq=0; seq<ProcessoBeanId.getProcessos().size(); seq++)  {
			proc   = ProcessoBeanId.getProcessos().get(seq) ;				
			cor   = (seq%2==0) ? "#ffffff" : "#d9f4ef";
		%>
		<tr bgcolor='<%=cor%>' style="cursor:hand;"
			onclick="javascript:Mostra('processoMostra',this.form,'<%=proc.getNumProcesso() %>');">
	        <td width="29"  height="14" align=center>&nbsp;<%=seq+1 %> </td>
			<td width="140" >&nbsp;<strong><%=proc.getNumProcesso() %></strong></td>
        	<td width="100" >&nbsp;<strong><%=proc.getNumCPFEdt() %></strong></td>
	        <td width="300">&nbsp;<strong><%= proc.getNomResponsavel() %></strong></td>
			<td>&nbsp;<strong><%= proc.getNumCNH()%>&nbsp;&nbsp;-&nbsp;<%= proc.getUf()%></strong></td>			
		</tr>		
		<tr bgcolor='<%=cor%>' style="cursor:hand;"
			onclick="javascript:Mostra('processoMostra',this.form,'<%=proc.getNumProcesso() %>');">
 	  		<td></td>
			<td>            &nbsp;Data Proc.:&nbsp;<%=proc.getDatProcesso() %></td>        	
	        <td colspan="2">&nbsp;Status:&nbsp;<%=proc.getCodStatus() %>&nbsp;-&nbsp;<%= proc.getNomStatus() %></td>
			<td>            &nbsp;Tipo:&nbsp;<%= proc.getTipProcessoDesc() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; " 
			onclick="javascript:Mostra('processoMostra',this.form,'<%=proc.getNumProcesso() %>');">
			<td></td>
			<td>&nbsp;Total Pontos:&nbsp;<%=proc.getTotPontos() %></td>
        	<td>&nbsp;Total Multas:&nbsp;<%=proc.getQtdMultas() %></td>
	        <td>&nbsp;Total Notifs:&nbsp;<%=proc.getTotNotificacoes() %></td>
			<td>&nbsp;Total Proc Ant.:&nbsp;<%=proc.getQtdProcAnteriores() %></td>			
		</tr>			
		<tr><td height="1" bgcolor="#000000" colspan=5></td></tr>			
	<% 
	} 
	%>
    </table>      
  </div>
</div>  
<!--FIM_CORPO_sistema--> 
<input name="acao"            type="hidden" value=''>
<input name="processoMostra"  type="hidden" value=" ">
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%String msgErro = ProcessoBeanId.getMsgErro();
  String msgOk   = ProcessoBeanId.getMsgOk();	
  String msgErroTop    = "245 px";	
  String msgErroLeft   = "80 px";  
%>
<%@ include file="EventoErro_Diretiva.jsp" %>
<!--FIM_Div Erros-->

<script>document.ConsultaProcesso.numProcesso.focus();</script>	        

</form>
</body>
</html>