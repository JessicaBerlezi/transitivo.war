<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>

<!-- Chama o Objeto da Tabela -->
<jsp:useBean id="PaginacaoBeanId" scope="session" class="PNT.PaginacaoBean" /> 

<html>
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.linhas td{border: 1px solid #ff0000;}
</style>
<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">

<form name="Form" method="post" action="">
<%@ include file="Cab.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao, fForm, numNotif, numAuto, seqNotif) {
 switch (opcao) {
   case 'Prox':
		document.Form.acao.value=opcao
		document.Form.target= "_self";
		document.Form.action = "acessoTool";  
		document.Form.submit();	  		  
	  break ; 
   case 'Ant':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'Imprimir':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'GravaTXT':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'ARDigitalizado':
		document.Form.numNotificacao.value=numNotif;
		document.Form.numAutoInfracao.value=numAuto;
		document.Form.seqNotif.value=seqNotif;
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
	case 'R':  
	  close() ;   
      break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
  }
}


function download(nomArquivo,fForm) {
	fForm.nomArquivo.value = nomArquivo;
   	fForm.target = "_self";
    fForm.action = "download";
    fForm.submit();
}


</script>

<input name="acao" type="hidden" value=' '>
				
<%@ include file="Paginacao_Cab.jsp" %>

<!-- cabe�alho da transa��o -->
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 115px; left: 50px; height: 18px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center" class="titulos">  	   		
    <tr>
      <td width="40" height="18">&nbsp;&nbsp;<strong>Seq.</strong></td>
      <td width="200" height="18" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Processo</strong></td>
	  <td width="120" height="18">&nbsp;&nbsp;<strong>Dat. Proc.</strong></td>
	  <td width="130" height="18">&nbsp;&nbsp;<strong>CPF</strong></td>
	  <td bgcolor="#a8b980">&nbsp;&nbsp;<strong>CNH</strong></td>
	</tr>
	
  </table>
</div>   
<!-- fim  cabe�alho da transa��o -->

<!-- Registros c/ barra de scroll . Aqui v�o entrar os includes-->
<div style="position:absolute; width:720px; overflow: auto; z-index: 100; top: 134px; left: 50px; height: 181px; visibility: visible;">   
	 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
	<%
	if (PaginacaoBeanId.getDados().size() > 0){	
		int i = 0;
        Iterator itOrg = PaginacaoBeanId.getDados().iterator() ;
		PNT.ProcessoBean myCont =  new PNT.ProcessoBean() ;
        
      while (itOrg.hasNext()) {
			i++;
			myCont = (PNT.ProcessoBean)itOrg.next() ;
		
		
	%>       
	   <tr>
	     <td width="40" style="text-align: center">&nbsp;&nbsp;<%=i %></td>
	     <td width="200" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= myCont.getNumProcesso() %> </td>
	     <td width="120" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= myCont.getDatProcesso() %></td>
	     <td width="130" style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= myCont.getNumCPFEdt() %></td>
	     <td style="text-align: center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=myCont.getTipCNHDesc() %>&nbsp;<%= myCont.getNumCNH() %>&nbsp;<%= myCont.getUfCNH() %></td>
	   </tr>
	   <tr>
	      <td width="30">&nbsp;&nbsp;</td>
	      <td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>NOME CONDUTOR :&nbsp;</strong><%= myCont.getNomResponsavel() %></td>
	   </tr>
	   <tr> 
		 <td bgcolor="#00000" height="2" colspan="5"></td>
	   </tr>

      <%}%>

<%}else {%>

	   <tr>
	     <td width="40">&nbsp;&nbsp;</td>
	     <td width="200">&nbsp;&nbsp;</td>
	     <td width="120">&nbsp;&nbsp;</td>
	     <td width="130">&nbsp;&nbsp;</td>
	     <td>&nbsp;&nbsp;</td>
	   </tr>
	   <tr> 
		 <td bgcolor="#00000" height="2" colspan="5"></td>
	   </tr>
<%}%>

	 </table>
 
 </div>



<%if (PaginacaoBeanId.getDados().size() > 0){%>	
    <%@ include file="Paginacao_Nav.jsp" %>
<%}%>

<%@ include file="Retornar.jsp" %>
<!-- Rodap�-->
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgOk         = "";
String msgErro       = PaginacaoBeanId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>


