<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsrLogado"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Perfil/Orgao/Atuacao -->
<jsp:useBean id="RelatProcStatusBean" scope="session" class="PNT.RelatProcStatusBean" /> 
<!-- Chama o Objeto da Tabela de STATUS -->
<jsp:useBean id="StatusId" scope="request" class="PNT.TAB.StatusBean" />
<jsp:setProperty name="StatusId" property="j_abrevSist" value="PNT" />  
<jsp:setProperty name="StatusId" property="colunaValue" value="COD_STATUS_AUTO" />  
<jsp:setProperty name="StatusId" property="popupNome"   value="codStatus"  />  
<jsp:setProperty name="StatusId" property="checked" value="<%=RelatProcStatusBean.getCodStatus()%>" />                 	 
<jsp:setProperty name="StatusId" property="popupString" value="NOM_STATUS,SELECT COD_STATUS_AUTO||' - '||NOM_STATUS_AUTO as NOM_STATUS,COD_STATUS_AUTO FROM TPNT_STATUS_PROCESSO ORDER BY COD_STATUS_AUTO ASC" />                 	 
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'consultarProc':
	    if (veCampos(fForm)){
		    fForm.nomStatus.value = fForm.codStatus.options[fForm.codStatus.selectedIndex].text;
		    fForm.acao.value=opcao
			fForm.target= "_blank";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  	
	   	}	  
	  break ; 
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	
	if (document.all["codStatus"].selectedIndex<=0 && document.all["TodosStatus"].checked==false )
	{  valid = false
	   sErro += "Selecione um �rg�o ou Todos \n"
	   
	}	
	if (document.all["codStatus"].selectedIndex>0 && document.all["TodosStatus"].checked==true )
	{  valid = false
	   sErro += "Escolha somente uma das op��es(Um Status ou todos...  \n"
	   
	}	
	if (fForm.dataIni.value==0) {
	   valid = false
	   sErro += "Data inicial n�o informada. \n"
	}
	if (fForm.dataFim.value==0) {
	   valid = false
	   sErro += "Data final n�o informada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}


</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<%@ include file="Cab.jsp" %> 

<input name="acao"              type="hidden" value=' '>	
<input name="nomStatus"         type="hidden" value=' '>	


<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 80px; left: 150px; z-index:1; visibility: visible;" > 
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
	  	<tr> 
    	  <td width="100%" height="30">Status:&nbsp; 
   	          <jsp:getProperty name="StatusId" property="popupString" />
				&nbsp;&nbsp;&nbsp;&nbsp;
		        <input type="checkbox" name="TodosStatus" value="S" tabindex="0" class="input" >&nbsp;Todos  
	      </td>
    	</tr>  
    	<tr><td  height=8></td></tr> 
		<tr>  
	      <td>Da Data:&nbsp;
	  	    <input name="dataIni" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=RelatProcStatusBean.getDataIni()%>" >	      	
	      	&nbsp;&nbsp;
	      	At� a Data:&nbsp;
	  	    <input name="dataFim" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=RelatProcStatusBean.getDataFim()%>" >
	      	
			<IMG src="<%= path %>/images/inv.gif" width="26" height="30" >
            <button type="button" name="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consultarProc',this.form);"> 
                 <img src="<%= path %>/images/bot_ok.gif" align="left" >
            </button>

		  </td>
		</tr>	
    </table>    
</div>   

<!--Div Erros-->
<%
String msgErro = RelatProcStatusBean.getMsgErro();
String msgOk = "";
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 


<!--FIM_Div Erros-->
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->  
</form>
</BODY>
</HTML>

