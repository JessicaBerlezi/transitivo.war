<!--Bloco 14 - PNT
-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr> 
    <td align="center" valign="top"> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr valign="top"> 
          <td width="55%" align="center" valign="top" class="fontmedia"> 
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr valign="top"> 
                <td width="35%" height="25" valign="top" class="fontmedia"> 
                 &nbsp;&nbsp;&nbsp;<strong><%=ProcessoBeanId.getNumProcesso()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
                <td align="center" valign="top" class="fontmedia">&nbsp;<strong>Expedi&ccedil;&atilde;o: 
                 
	                  <%=notificacao.getDatEmissao()%>
	                  
                 	
                 	</strong>&nbsp;	            </td>
              </tr>
              <tr valign="top"> 
                <td height="20" colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getNomResponsavel()%></td>
              </tr>
              <tr valign="top"> 
                <td height="20" colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getEndereco()%>                </td>
              </tr>
              <tr valign="top"> 
                <td height="20" colspan="2" valign="middle" class="fontmedia">&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getCepEdt()%>&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getMunicipio()%>&nbsp;</td>
              </tr>
              <tr valign="top"> 
                <td colspan="2" class="fontmaior"> 
                  <!--c�digo de barras-->
                  <table width="100%" cellpadding="1" cellspacing="0" class="semborda">
                    <tr> 
                      <td class="fontmedia" height="16"><strong>
                      <% if (!notificacao.getDatGeracaoCB().equals("")) { %>
						<img src="<%=path%>/download?nomArquivo=<%=notificacao.getArquivoCB()%>&contexto=DIR_CODIGO_BARRA_<%= notificacao.getDatGeracaoCB().substring(6,10) %>" 
						width="240" height="60" alt="" border="0"></strong>
						<% } %>
					  </td>
                    </tr>
                    <tr> 
                      <td></td>
                    </tr>
                  </table>
                  <!--Fim c�digo de barras-->
                </td>
              </tr>
            </table>
          </td>
          <td width="45%" align="center" valign="top" class="fontmedia"> 
            <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
              <tr>
		        <td height="15" align="left" valign="top" class="fontmenor">
		          &nbsp;&nbsp;<strong>Comprovante de Entrega - 
		          <%= notificacao.getTipoNotificacaoDescNotif().toUpperCase()  %></strong>
		        </td>
		  	   </tr>
		  	</table>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0" >
              <tr> 
                <td> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="38" valign="top" class="semborda"><span class="fontmenor">&nbsp;&nbsp;<strong>Nome 
                      do recebedor</strong></span><br> </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="40%" height="38" valign="top" class="table_linha_top" ><span class="fontmenor">&nbsp;&nbsp;<strong>Data</strong></span><br> 
                      </td>
                      <td width="60%" valign="top" class="table_linha_top" style="border-left: 1px solid #000000"><span class="fontmenor">&nbsp;&nbsp;<strong>Grau 
                        de relacionamento</strong></span><br> </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="38" valign="top" class="table_linha_top"><span class="fontmenor">&nbsp;&nbsp;<strong>Identifica&ccedil;&atilde;o</strong></span><br> 
                      </td>
                    </tr>
                  </table>
                  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
                    <tr> 
                      <td width="100%" height="38" valign="top" class="table_linha_top"><span class="fontmenor">&nbsp;&nbsp;<strong>Assinatura 
                      do recebedor</strong></span><br> </td>
                    </tr>
                  </table></td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

      <table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
        <tr> 
          <td width="50%" height="20" class="fontmenor"><strong>&nbsp;&nbsp;DESTAQUE 
            AQUI</strong></td>
          <td width="40%" align="right"  class="fontmenor"><strong>2-0630</strong></td>
          <td width="10%" align="right" class="fontmenor"><strong>1</strong>&nbsp;&nbsp;</td>
        </tr>
      </table>
<!--Linha--> 
			  <table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
					<tr> 
						<td height="1" bgcolor="#000000"></td>
					</tr>
			  </table>
<!--FIM Linha-->
    </td>
  </tr>
</table> 
