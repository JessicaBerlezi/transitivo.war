<%@ page import="ACSS.OperacaoBean" %>
<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Chama o Objeto do Sistema logado -->
<%	
 // Se voc� quer usar a barra de menu no topo da p�gina utilize o valor 1 - Valores: 1 || 0
 String usebar   = request.getParameter("usebar"); if(usebar==null)  usebar  ="0";   
 //A cor da barra do Menu  
 String barcolor   = request.getParameter("barcolor"); if(barcolor==null)  barcolor  ="#CCCCCC";   
 
 // "136" - A largura da sua barra de menu.  
 // String barwidth   = request.getParameter("barwidth"); if(barwidth==null)  barwidth  ="136";   
	String barwidth    =  "250";

 //"23" - A altura da barra de menu. 
 //String barheight   = request.getParameter("barheight"); if(barheight==null)  barheight  ="23";   
 String barheight   =  "17" ;
 
 //A posi��o a esquerda da barra de menu. 
 String barx   = request.getParameter("barx"); if(barx==null)  barx  ="0";   
 //A posi��o da barra em rela��o ao todo 
 String bary   = request.getParameter("bary"); if(bary==null)  bary  ="0";   
 //Atribua o valor 1 se voc� quiser que a barra tenha o mesma borda como os menus de topo
 String barinheritborder   = request.getParameter("barinheritborder"); if(barinheritborder==null)  barinheritborder  ="0";   

 //Propriedades de coloca��o
 //Isto controla como ser� apresentado seu menu, ou  seja em filas ou colunas. Atribua 0 para colunas e 1 para filas - Valores 0 || 1
 // String rows   = request.getParameter("rows"); if(rows==null)  rows  ="0";   
 String rows        =  "0"  ;

 //Este � a posi��o a esquerda do menu
 //String fromleft   = request.getParameter("fromleft"); if(fromleft==null)  fromleft  ="150";   
 String fromleft    =  "203";
 
 //Esta � a posi��o do menu em rela��o ao topo
 //String fromtop   = request.getParameter("fromtop"); if(fromtop==null)  fromtop  ="150";   
 String fromtop     =  "70" ;
 
 //Quanto espa�o voc� quer entre cada item do topo. 
 //String pxbetween   = request.getParameter("pxbetween"); if(pxbetween==null)  pxbetween  ="1";   
 String pxbetween   =  "8"  ;
 
 ACSS.OperacaoBean myOper = new OperacaoBean();
 
%>

<!-- Floating menu da tela menu.jsp-->
<script>
/*****************************************************************************
Checagem do browser
******************************************************************************/

function lib_bwcheck(){ //Browsercheck (needed)
	this.ver=navigator.appVersion; this.agent=navigator.userAgent
	this.dom=document.getElementById?1:0
	this.ie5=(this.ver.indexOf("MSIE 5")>-1 && this.dom)?1:0;
	this.ie6=(this.ver.indexOf("MSIE 6")>-1 && this.dom)?1:0;
	this.ie7=(this.ver.indexOf("MSIE 7")>-1 && this.dom)?1:0;
	this.ie8=(this.ver.indexOf("MSIE 8")>-1 && this.dom)?1:0;
	this.ie9=(this.ver.indexOf("MSIE 9")>-1 && this.dom)?1:0;

	this.ie4=(document.all && !this.dom)?1:0;
	this.ie=this.ie4 || this.ie5 || this.ie6 || this.ie7 || this.ie8 || this.ie9;
	this.mac=this.agent.indexOf("Mac")>-1
	this.opera5=this.agent.indexOf("Opera 5")>-1
	this.ns6=(this.dom && parseInt(this.ver) >= 5) ?1:0;
	this.ns4=(document.layers && !this.dom)?1:0;
	this.bw=(this.ie9 || this.ie8 ||this.ie7 || this.ie6 || this.ie5 || this.ie4 || this.ns4 ||
	this.ns6 || this.opera5 || this.dom)
	return this
}




var bw=new lib_bwcheck() //Fazendo o objeto browsercheck


var mDebugging=2 //Variavel debugging. Atribua 0 para nenhum debugging, 1 para alertas ou 2 para o status do debugging.

oCMenu=new makeCoolMenu("oCMenu") //Fazendo o menu object. Argumento: menuname
oCMenu.useframes=0 //Voc� quer usar os menus como o coolframemenu ou n�o? (com frames ou n�o) - Valor: 0 || 1
oCMenu.frame='' //O nome do seu frame principal (onde os menus dever�o aparecer). Deixe vazio se voc� n�o esta usando frames

oCMenu.useclick=0 //Se voc� quer que o menu fique ativado ou desativado com um clique, apenas atribua o valor 1. - Value: 0 || 1

/*Se voc� atribuir 1 voc� ter� um cursor com a forma de "m�o" quando se mover sobre os links no NS4. 
NOTE: Isto n�o se aplica aos submenus se voc� est� usando frames devido a alguns problemas com o NS4*/
oCMenu.useNS4links=1  

oCMenu.NS4padding=2 

oCMenu.checkselect=0 //Valor 0 (o select fica por tr�s do menu); Valor 1( por cima do menu)/

oCMenu.pagecheck=1 
oCMenu.checkscroll=0 //Valor 0 (permite scroll, o menu muda de posi��o); valor 1 (menu fica fixo, independente do scroll)
oCMenu.resizecheck=1 
oCMenu.wait=500 

//Propriedades da barra do Menu
oCMenu.usebar=<%= usebar %>        //Se voc� quer usar a barra de menu no topo da p�gina utilize o valor 1 - Valores: 1 || 0
oCMenu.barcolor="<%= barcolor %>"  //A cor da barra do Menu 
oCMenu.barwidth=<%= barwidth %>    // "136" - A largura da sua barra de menu. 
oCMenu.barheight="<%= barheight%>" //A altura da barra de menu. 
oCMenu.barx="<%= barx %>"          //A posi��o a esquerda da barra de menu. 
oCMenu.bary="<%= bary %>"          //A posi��o da barra em rela��o ao todo
oCMenu.barinheritborder=<%= barinheritborder %> //Atribua o valor 1 se voc� quiser que a barra tenha o mesma borda como os menus de topo

//Propriedades de coloca��o
oCMenu.rows=<%= rows %>           //Isto controla como ser� apresentado seu menu, ou  seja em filas ou colunas. Atribua 0 para colunas e 1 para filas - Valores 0 || 1
oCMenu.fromleft=<%= fromleft %>   //Este � a posi��o a esquerda d
oCMenu.fromtop=<%= fromtop %>     //Esta � a posi��o do menu em rela��o ao topo
oCMenu.pxbetween=<%= pxbetween %> //Quanto espa�o voc� quer entre cada item do topo. 

/*Voc� tem varios caminhos diferentes para colocar os itens do topo. 

oCMenu.menuplacement=0

Voc� pode deixa-los alinhados para um dos lados - usamos esta condi��o principalmente quando n�o trabalhamos com frames, mas pode ser usado em ambas as condi��es
Valores: (Se voc� obtiver resultados estranhos check as variaveis fromleft,fromtop e pxbetween acima)
Para menus que est�o colocados em colunas (alinhamento=left ou alinhamento=right (ver abaixo)) voc� pode alinha-los como "right" ou "center"
Para menus que est�o colocados em linhas (alinhamento=top ou alinhamento=bottom (ver abaixo)) voc� pode alinha-los como "bottom", "center" ou "bottomcenter"
oCMenu.menuplacement="left"

Voce tambem pode atribui-los diretamente em pixels
oCMenu.menuplacement=new Array(10,200,400,600)

Ou voc� pode colocar em porcentagem: (lembre-se de usar o ' ' entre os numeros)


Escolha uma dessas op��es para obter o resultado desejado.
*/
oCMenu.menuplacement="left"

/*
Agora n�s j� deixamos pronto as propriedades de cada nivel. 
level[0] = top items
level[1] = sub items
level[2] = sub2 items
level[3] = sub3 items and so on....
Todos os menus herdar�o as propriedades, e todas as propriedades somente TER�O de ser especificadas no nivel do topo. 
Se um nivel n�o tem as propriedades especificadas, ele ira parecer com o ultimo nivel especificado, 
se isto ainda n�o existe, ele ir� pegar as propriedades do nivel[0]
*/

//TOP LEVEL PROPRIEDADES - ALL OF THESE MUST BE SPECIFIED FOR LEVEL[0]
oCMenu.level[0]=new Array() //Adicione isto para cada novo nivel
oCMenu.level[0].width=136 //A largura default para cada nivel[0] (top) itens. Voc� pode sobrepor isto em cada item especificando a largura quando fizer o item. - Valores: px || "%"
oCMenu.level[0].height=23 //A altura default para cada nivel[0] (top) itens. Voc� pode sobrepor isto em cada item especificando a altura quando fizer o item. - Valores: px || "%"
oCMenu.level[0].bgcoloroff="" //A cor de fundo default para cada nivel[0] (top) items. Voc� pode sobrepor isto em cada item especificando a cor de fundo quando fizer o item. - Valor: "cor"
oCMenu.level[0].bgcoloron="" //A cor de fundo "on"  default "on" para cada nivel[0] (top) itens. Voc� pode sobrepor isto em cada item especificando o "on" da cor de fundo quando fizer o item. - Valor: "cor"
oCMenu.level[0].textcolor="#000000" //A cor do texto default para cada nivel[0] (top) items. Voc� pode sobrepor isto em cada item especificando a cor do texto quando fizer o item. - Valor: "cor"
oCMenu.level[0].hovercolor="#000000" //A cor do texto "on" default para cada nivel[0] (top) itens. Voc� pode sobrepor isto em cada item especificando o "on" a da do texto quando fizer o item. - Valor: "cor"
oCMenu.level[0].style="padding:2px; font-family:verdana; font-size:10px;" //O estilo para todos os niveis[0] (top) items. - Valor: "style_settings"
oCMenu.level[0].border=0 //O tamanho das bordas para todos os niveis[0] (top) items. - Valor: px
oCMenu.level[0].bordercolor="#999999" //A cor da borda para todos os niveis[0] (top) items. - Valor: "cor"
oCMenu.level[0].offsetX=0 //O X offset dos submenus deste item. 
oCMenu.level[0].offsetY=0 //O Y offset dos submenus deste item. 
oCMenu.level[0].NS4font="verdana"
oCMenu.level[0].NS4fontSize="2"

/*Novo: Adicionando anima��o que pode ser controlado em cada nivel.*/
oCMenu.level[0].clip=0 //Atribua o valor 1 se voc� quiser que os submenus deste nivel abram "deslizando" como um efeito de clip animado. - Valor: 0 || 1
oCMenu.level[0].clippx=0 //Se voc� tem um clip especifico, voce pode atribuir quantos pixels isto ir� aparecer de cada vez para controlar a velocidade da anima��o. - Valor: px 
oCMenu.level[0].cliptim=0 //Esta � a velocidade do timer para cada efeito do clip.  - Valor: milionesimosdesegundos
//Filters - Isto pode ser usado pra obter algum efeito legal como fade, slide, stars e assim por diante. EXPLORER5.5+ SOMENTE - 
oCMenu.level[0].filter=0 //VALOR: 0 || "filter specs"

/*alinhamento das variaveis.

Isto especifica como os submenus deste nivel ser�o apresentados. 
Valores:
"bottom": Os sub menus deste nivel sairam no topo deste item
"top": Os sub menus deste nivel sairam no fundo deste item
"left": Os sub menus deste nivel sairam a direita deste item
"right": Os sub menus deste nivel sairam a esquerda deste item

Em geral "left" e "right" trabalhada melhor para menus em colunas e "top" e "bottom" trabalham melhor para menus em filas. 

*/
oCMenu.level[0].align="right" //Valores: "top" || "bottom" || "left" || "right" 

//EXEMPLO DE SUB NIVEL[1] PROPRIEDADES - Voc� tem que espeficiar as propriedas que voc� quer diferentes do NIVEL[0] - Se voc� quer que todos os itens se pare�am iguais, somente remova isto
oCMenu.level[1]=new Array() //Adicione isto para cada novo nivel (adicione um para o numero)
oCMenu.level[1].width=200
oCMenu.level[1].height=20
oCMenu.level[1].bgcoloroff="#F8F8F8"
oCMenu.level[1].bgcoloron="#B8EDDF"
oCMenu.level[1].style="padding:2px; font-family:verdana; font-size:10px; color:#000000"
oCMenu.level[1].align="right" 
oCMenu.level[1].offsetX=0
oCMenu.level[1].offsetY=0
oCMenu.level[1].border=1 
oCMenu.level[1].bordercolor="#339999"

//EXEMPLO DE SUB NIVEL[2] PROPRIEDADES - Voc� tem que espeficiar as propriedades que voc� quer diferentes do NIVEL[1] OU NIVEL[0] - Se voc� quer que todos os itens se pare�am iguais, somente remova isto
oCMenu.level[2]=new Array() //Adicione isto para cada novo nivel (adicione um para o numero)
oCMenu.level[2].width=200
oCMenu.level[2].height=20
oCMenu.level[2].bgcoloroff="#F8F8F8"
oCMenu.level[2].bgcoloron="#B8EDDF"
oCMenu.level[2].style="padding:2px; font-family:verdana; font-size:10px; color:#000000"
oCMenu.level[2].align="right" 
oCMenu.level[2].offsetX=0
oCMenu.level[2].offsetY=0
oCMenu.level[2].border=1 
oCMenu.level[2].bordercolor="#339999"
oCMenu.level[2].NS4font="verdana"
oCMenu.level[2].NS4fontSize="1"

/*Variaveis para cada item do menu: (** elas tem que ser especificadas!)
name: O nome de cada item. Isto precisa ser unico para cada item. N�o use espa�os ou caracteres estranhos em cada um! **
parent: O nome dos itens de menu que voc� quer para se conectar. Isto vai ser um submenu do item que tera o nome que voc� colocar aqui. ** para todos os outros sera os topitems
text: O texto que voc� quer no item. ** (exceto se voc� usa imagens) 
link: O endere�o da pagina que voc� quer linkar com este item.
target: A janela alvo ou frame que voc� quer linkar 
width: A largura do elemento. 
height: A altura do elemento.
img1: Use "off" imagem para este elemento, se voc� deseja usar imagens.
img2: A imagem que ir� aparecer usando onmouseover (se voc� estiver usando imagens).
bgcoloroff: A cor de fundo para este item. 
bgcoloron: A corde de fundo para este item no comando "on". 
textcolor: A cor do texto para este item. 
hovercolor: A cor do texto no comando "on". 
onclick: Se voc� quer que aconte�a alguma coisa quando clicar sobre o elemento, especifique aqui.
onmouseover: Isto vai acontecer quando passar o mouse sobre o elemento. 
onmouseout: Isto vai acontecer quanto tirar o mouse do elemento.

Lembre-se que voc� pode ter muitos niveis/subniveis como voc� quiser. 
Apenas, procure especificar corretamente o "parent" para cada item.
Para atribuir estilos para cada n�vel, veja acima.
*/


<%  
// makeMenu(name,parent,text,link,target,width,height,img1,img2,bgcoloroff,bgcoloron,textcolor,hovercolor,onclick,onmouseover,onmouseout){
for (int i=0; i < UsuarioFuncBeanId.getFuncoes().size(); i++)   {
	 myOper = (ACSS.OperacaoBean)UsuarioFuncBeanId.getFuncoes().elementAt(i);
	if ("".equals(myOper.getCodOperacaoFk())) //sem n�vel pai
	{
		if ("".equals(myOper.getNomExecuta())) // nao executa
		{		
%>
		oCMenu.makeMenu('<%=myOper.getCodOperacao()%>','','<div align=left><b><%=myOper.getNomOperacao()%></b></div>',
						'','','<%= 8+8*myOper.getNomOperacao().length() %>','<%=barheight %>','','','','','#666666','#339999','')
<%
		}
		else {	
%>
		oCMenu.makeMenu('<%=myOper.getCodOperacao()%>','','<div align=left><b><%=myOper.getNomOperacao()%></b></div>',
						'','','<%= 8+8*myOper.getNomOperacao().length() %>','<%=barheight %>','','','','','#666666','#339999','valida("OK","<%=myOper.getSigFuncao()%>")')
<%
		}
	}
	else //tem ao menos um n�vel pai
	{
	
		if ("".equals(myOper.getNomExecuta())) // nao executa
		{			
%>	
		oCMenu.makeMenu('<%=myOper.getCodOperacao()%>','<%=myOper.getCodOperacaoFk()%>','<%=myOper.getNomOperacao()%>',
						'','','<%= barwidth %>','<%=barheight %>','','','','','#339999','#000000','')
<%
		}
		else {	
%>
		oCMenu.makeMenu('<%=myOper.getCodOperacao()%>','<%=myOper.getCodOperacaoFk()%>','<%=myOper.getNomOperacao()%>',
						'','','<%= barwidth %>','<%=barheight %>','','','','','#339999','#000000','valida("OK","<%=myOper.getSigFuncao()%>")')
<%
		}
	}
}
%>

//Deixe estas duas linhas! 
oCMenu.makeStyle(); 
oCMenu.construct();
</script>
