<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>   
<%@ include file="Css.jsp" %>
<style>
.linhas td{border: 1px solid #ff0000}

</style>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;
		  
	   case 'historicoImprime':  
				fForm.acao.value=opcao;
	   			fForm.target= "_blank";
	    		fForm.action = "acessoTool";  
	   			fForm.submit();
	   		
	   break;	  
		    
  }
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="mostraHistForm" method="post" action="">
<% String nomTela=" CONSULTA HIST�RICO ";%>
<%@ include file="Cab.jsp" %> 
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="numProcesso" value="<%=ProcessoBeanId.getNumProcesso()%>">
<input type="hidden" name="numCPF" value="<%=ProcessoBeanId.getNumCPFEdt()%>">

<!--DIV QUE IDENTIFICA O PROCESSO -->
<div id="hist1" style="position:absolute; left:50px; right: 15px; top:80px; height:125px; z-index:5; overflow: auto;"> 
  <table width="100%" border="0" cellpadding="1" cellspacing="0">
      <tr bgcolor="#D9F4EF" height="18">
        <td width="16%" bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Processo:</strong></td>
        <td width="13%" bgcolor="#d9f4ef"><%=ProcessoBeanId.getNumProcesso() %></td>
        <td width="25%" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%> :</strong>&nbsp;<%=ProcessoBeanId.getNumCNH() %>&nbsp;-&nbsp<%=ProcessoBeanId.getUfCNH()%></td>
		<td colspan="2" bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>CPF:</strong>&nbsp;<%=ProcessoBeanId.getNumCPFEdt() %></td>        
		<td width="5%"><%   if(ProcessoBeanId.getHistoricos().size()>0){%>
        <button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick="javascript:valida('historicoImprime',this.form);"> 
              <img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="26" height="19">        
		</button>
		<%}%>
		</td>
      </tr>
      <tr><td colspan=6 height="2"></td></tr>	  
      <tr bgcolor="#C0E1DF" height="18">
        <td><strong>&nbsp;&nbsp;</strong><strong>Condutor:</strong></td>
        <td colspan="5"><%=ProcessoBeanId.getNomResponsavel() %></td>
             
      </tr>  
      <tr><td colspan=5 height="2"></td></tr>
      <tr bgcolor="#D9F4EF" height="14">
        <td bgcolor="#d9f4ef"><strong>&nbsp;&nbsp;</strong><strong>Data do Processo:</strong></td>
        <td bgcolor="#d9f4ef"><%=ProcessoBeanId.getDatProcesso() %></td>
        <td colspan=4 bgcolor="#d9f4ef">&nbsp;&nbsp;<strong>Status do Processo :</strong>&nbsp;<%=ProcessoBeanId.getCodStatus()%>&nbsp;-&nbsp;<%=ProcessoBeanId.getNomStatus()%>
		&nbsp;(<%=ProcessoBeanId.getDatStatus() %>)</td>
      </tr>      
  </table>    
</div>

<!--DIV COM OS HISTORICOS --> 
<div class="divPrincipal" style="top:155px; height:40%;">
<div  class="divCorpo" id="hist2" style="height:100%;">   
   <%  if 	(ProcessoBeanId.getHistoricos().size()>0){   		
      	 int a =ProcessoBeanId.getHistoricos().size()+1;     
		 for (int i=0;i<ProcessoBeanId.getHistoricos().size();i++) {
				a--;%>	
	<!--dados fixos para todos os eventos-->
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr bgcolor="#c0e1df"  height="18"> 
        <td>&nbsp;&nbsp;<strong><%=a%>&nbsp;&#8226;&nbsp;Evento :&nbsp;<%= ProcessoBeanId.getHistoricos().get(i).getCodEvento()%></strong></td>
        <td colspan="2">&nbsp;<strong><%= ProcessoBeanId.getHistoricos().get(i).getNomEvento() %></strong></td>
        <td style="text-align: right;">&nbsp;</td>
        <td colspan="2"><strong>Origem :&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getNomOrigemEvento() %></strong>&nbsp;</td>
      </tr>
      <tr><td height="2" colspan="6"></td></tr>
      <tr bgcolor="#D9F4EF" height="14"> 
        <td>
        	&nbsp;&nbsp;<strong>Status :</strong>
        </td>
        <td colspan="3" >
        	&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getCodStatus() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getNomStatus() %>
        </td>
        <td>
        	<strong>Data Status :</strong>
        </td>
        <td>
        	<%=ProcessoBeanId.getHistoricos().get(i).getDatStatus() %>
        </td>
      </tr>
      <tr><td height="2" colspan="6"></td></tr>
      <tr bgcolor="#D9F4EF" height="14"> 
        <td width="92" >&nbsp;&nbsp;<strong>Executado por :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getNomUserName() %></td>
        <td width="97" ><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o :</strong></td>
        <td width="93" ><%=ProcessoBeanId.getHistoricos().get(i).getSigOrgaoLotacao() %></td>
        <td width="85" ><strong>Data Proc. :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getDatProc() %></td>
      </tr>
      <tr><td height="2" colspan="6"></td></tr>

   </table> 
   <!--fim dados fixos para todos os eventos-->   
<% if ("800".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 800 - Instaura��o Processo Pontua��o  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>Nome Arquivo :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td colspan="3"><strong>Data Arquivo :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
    </table>   
<%} else if ("801".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 801 - Emiss�o de Notifica��o de Autua��o  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>Qtd. Seq�encias :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td width="85" ><strong>Data Gera��o :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>N� Processo :</strong></td>
        <td colspan="5">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>    
      </tr>
    </table>   
 <%}else if ("803".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>Tipo de Ata :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="190"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td colspan="3"><strong>C�d. Envio D.O. :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      	
      </tr>
    </table>      
 <%}else if ("804".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td colspan=3><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td><strong>&nbsp;&nbsp;N�mero Ata:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>
    </table>      
<%}else if ("834".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td colspan=3><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td><strong>&nbsp;&nbsp;N�mero Ata:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>
    </table>      
<%}else if ("894".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 803 - Enviar Notifica��o de Autua��o para D.O  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td colspan=3><strong>&nbsp;&nbsp;Data D.O.:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td><strong>&nbsp;&nbsp;N�mero Ata:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>
    </table>      
<%}else if ("808".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 808 - Registrar AR ? REC/N�o REC  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>C�d.Retorno.:&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></strong></td>
        <td width="93"><strong>Entregue.:&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></strong></td> 
        <td width="85"><strong>Data Retorno :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>    	
      </tr>     
    </table>   
    
    


<%}else if ("680".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 810 - Abrir Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td><td colspan="3">&nbsp;<textarea cols="90" rows="1" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" readonly="readonly">
         <%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim()%>-<%if (ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim().equals("510")) {%>Cancelamento por decis�o Judicial<%}%> <%if (ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01().trim().equals("512")){%>Cancelamento por Revis�o de Ato Administrativo<%}%>
         <%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02()%>-
         <%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03()%>-
         <%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04()%></textarea>
	     </td>
      </tr>
    </table>    
<%}else if ("811".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>

    
    
<%}else if ("810".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 810 - Abrir Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="440">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="85"><strong>Data :</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Respons�vel :</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>    
      	<td><strong>CPF:</strong></td>
      	<td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %></td>
      </tr>
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>E-mail :&nbsp;</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento12() %></td>   
      	<td><strong>Tipo:</strong></td>
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Motivo :</strong></td>
         <td colspan="3">&nbsp;<textarea cols="90" rows="1" style="border: 0px none; overflow: auto; text-align: left; background: transparent;" readonly="readonly"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento07()%></textarea>
	     </td>
      </tr>
    </table>    
<%}else if ("811".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 811 - Informar junta/Relator - Defesa  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td colspan="4"><strong>Data :</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>        
      </tr>      
      <tr bgcolor="#D9F4EF" height="16"> 
		<td width="92" >&nbsp;&nbsp;<strong>Junta :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></td>
        <td width="97"><strong>C�d. Relator :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento05() %></td>
        <td width="85" >&nbsp;<strong>CPF Relator :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %></td>            
      </tr>
    </table>   
    
<%}else if ("812".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())||"842".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())|| "872".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ EVENTO: RESULTADO DE DEFESA PR�VIA =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
      <tr bgcolor="#D9F4EF" height="16">
      
        <td width="92" >&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97" ><strong>Resultado :</strong></td>
        <td width="93" ><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02()%></td>
        <td width="85" ><strong>Data :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>      
      </tr>
      <tr> <td colspan=6 height="2"></td> </tr>
      <tr bgcolor="#D9F4EF" height="16">
        <td>&nbsp;&nbsp;<strong>Motivo:</strong></td>
        <td colspan=5>
	         &nbsp;<textarea cols="110" rows="3" style="border: 0px none;  text-align: left; background: transparent; overflow: auto" ><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %></textarea>
        </td>
      </tr>
      <tr> <td colspan=6 height="2"></td> </tr>
      <tr bgcolor="#D9F4EF" height="16">
        <td>&nbsp;&nbsp;<strong>Penalidade:</strong></td>
        <td colspan=5>
	         &nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento06() %>
        </td>
      </tr>
      
      
      
      
    </table>    
    
    
    
        
<%}else if ("815".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 815 - Cria��o do Processo F�sico  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92">&nbsp;&nbsp;<strong>Requerimento :</strong></td>
        <td width="440" align="left">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="85"><strong>Tipo:</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
      </tr>     
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Data :</strong></td>
        <td align="left">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
        <td><strong>Data Proc F�sico:</strong></td>        
        <td>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento08() %></td>
      </tr>     
    </table>  
<%}else if ("820".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>
<!--============ Dados vari�veis para evento 820 - Penalidade Registrada  =================================================================--> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>N� Notif.:</strong></td>
        <td width="250">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento01() %></td>
        <td width="97"><strong>Qtd. Seq�encias :</strong></td>
        <td width="93"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td width="85" ><strong>Ult. Dt. Rec :</strong></td>
        <td><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento03() %></td>
      </tr>
      <tr bgcolor="#D9F4EF" height="16"> 
        <td>&nbsp;&nbsp;<strong>Penalidade :</strong></td>
        <td colspan = "5">&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04() %>&nbsp;-&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento05() %></td>          	
      </tr>
    </table>            
<%}else if ("911".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>Novo Status :</strong></td>
        <td width="250"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td colspan="3"><strong>Motivo:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento04()%></td>
      </tr>
    </table>   
<%}else if ("960".equals(ProcessoBeanId.getHistoricos().get(i).getCodEvento())== true){ %>    
    <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="sem-borda">
      <tr bgcolor="#D9F4EF" height="16"> 
        <td width="92" >&nbsp;&nbsp;<strong>Novo Status :</strong></td>
        <td width="250"><%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento02() %></td>
        <td colspan="3"><strong>Motivo:</strong>&nbsp;<%=ProcessoBeanId.getHistoricos().get(i).getTxtComplemento07()%></td>
      </tr>
    </table>   
    
    
    
<% } %>
<table width="100%" ><tr><td height="2"></td></tr></table>
<% } %>
<%}else{%>  

<div id="mensagem" style="position:absolute; left:70px; top:85px; right: 25px; height:35px; z-index:10; overflow: visible; text-align: center;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="0" class="sem-borda">
	  <tr><td bgcolor="#D9F4EF">&nbsp;&nbsp;<strong>PROCESSO SEM HIST�RICO</strong></td> </tr>	
	</table>
</div>         
<%}%>
</div>
</div>
<!-- Rodap�-->
<%@ include file="Retornar.jsp" %>
<%@ include file="Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->

</form>
</body>
</html>