<!--Bloco 13ResultDefesa - PNT
-->	
<table width="100%" border="0" cellpadding="2" cellspacing="0" class="semborda">
  <tr> 
      <td align="center" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-bottom: 10px">
          <tr>
            <td height="25" valign="top" class="tdborda" colspan="5"><span class="fontmenor">&nbsp;&nbsp;<strong>NOME</strong></span><br>
                <span class="fontmaior">&nbsp;<%= ProcessoBeanId.getNomResponsavel() %> </span></td>
            <td width="1%" valign="top" class="semborda">&nbsp;</td>
            <td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE MULTAS</strong></span><br>
                <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getQtdMultas()%></span></td>
            <td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
            <td width="21%" rowspan="3" valign="top" class="tdborda"><span class="fontmenor"><strong><strong>&nbsp;&nbsp;N&ordf; PROCESSO </strong></strong></span><br>
                <br>
                <span class="fontmaior">&nbsp;<strong><%= ProcessoBeanId.getNumProcesso()%></strong></span></td>
            <td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
      <% if ("4".equals(notificacao.getTipoNotificacao())) { %> 
            <td width="17%" valign="top" rowspan="3" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>DATA P/ ENTREGA CNH</strong></span><br>
                <br>
                <span class="fontmaior">&nbsp;<strong><%= ProcessoBeanId.getUltDatEntregaCNH() %></strong></span></td>
      <% } else { %>
            <td width="17%" valign="top" rowspan="3" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>DATA PUBLICA&Ccedil;&Atilde;O D.O. </strong></span><br>
                <br>
                <span class="fontmaior">&nbsp;<strong><%= notificacao.getDatPublDO() %></strong></span></td>
      <% }        %>
          </tr>
          <tr>
            <td colspan="11" height="2"></td>
          </tr>
          <tr>
            <td width="17%" height="25" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong><%=ProcessoBeanId.getTipCNHDesc()%></strong></span><br>
            <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCNH()%> - <%=ProcessoBeanId.getUfCNH()%></span></td>
            <td width="1%" valign="top">&nbsp;</td>
            <td width="11%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>EXPEDI&Ccedil;&Atilde;O</strong></span><br>
                <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getDatEmissaoCNH()%></span></td>
            <td width="1%" valign="top" class="semborda">&nbsp;</td>
            <td width="15%" valign="top" class="tdborda" ><span class="fontmenor">&nbsp;&nbsp;<strong>CPF</strong></span><br>
                <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCPFEdt()%></span></td>
            <td width="1%" valign="top" class="semborda">&nbsp;</td>
            <td width="12%" valign="top" class="tdborda"><span class="fontmenor">&nbsp;&nbsp;<strong>TOTAL DE PONTOS</strong></span><br>
                <span class="fontmaior">&nbsp;<%=ProcessoBeanId.getTotPontos()%></span></td>
            <td width="1%" valign="bottom" class="semborda" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
            <td width="1%" valign="bottom" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000"></td>
          </tr>
        </table>

	  </td>
  </tr>
</table>
