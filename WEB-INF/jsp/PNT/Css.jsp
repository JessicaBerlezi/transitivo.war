<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es: 07/06/06 - 23/06/06
-->	
<head>
	<link rel="icon" href="./images/sys/favicon.ico" type="image/x-icon">
</head>
<style type="text/css">

DIV.quebrapagina{	page-break-after: always }

input,
select,
textarea  {			font-family: verdana; font-size: 10px;
					border: 1px solid #999999;
}
						
.input {			font-family: verdana; font-size: 9px;
					border: 0px;
}
button{				font-family: verdana; font-size: 9px;
					border: 1px solid; font-variant: normal;
					color: #000000; background-color: #D4D4D4;
					margin: 0px; padding: 0px;
}
table	{			FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 				font-size: 9px; letter-spacing: normal;
	 				font-variant: normal; text-transform: none;
	 				text-decoration: none;
}
.sem-borda  {		border: 0px none;
}
td {				FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
  					font-size: 9px; color: #000000;
					border: 0px;
}
A {					color: black; font-family: Verdana, Helvetica, Arial, sans-serif; text-decoration: none;
}
A:link {			color: #000000; font-family: Verdana, Arial, Helvetica;
}
A:visited {			color: #000000; font-family: Verdana, Arial, Helvetica;
}
A:hover {			color: #003366; font-family: Verdana, Arial, Helvetica; text-decoration: underline;
}
.branco {			color: white; font-family: Verdana, Helvetica, Arial, sans-serif; text-decoration: none;
}
.branco:link {		color: #ffffff; font-family: Verdana, Arial, Helvetica;
}
.branco:visited {	color: #e7e7e7; font-family: Verdana, Arial, Helvetica;
}
.branco:hover {		color: #D9F4EF; font-family: Verdana, Arial, Helvetica; text-decoration: underline;
}

.linhas td{border: 1px solid #ff0000;}
.apresenta  td{padding-left: 5px; padding-right: 5px;}
.tit td {padding-left: 5px; padding-right: 5px; background-color:#c0e1df; height:16px;}
.apresentaTit {color:#006666; font-weight: bold; letter-spacing:0px;}
.apresentaDestaque {color:#000000; font-weight: bold; letter-spacing:0px;}
.apresentaCor td{background: #d9f4ef; height: 13px; }
/********************************/	

.divPrincipal {position:absolute; left:50px;right: 15px;z-index:50;overflow: visible;visibility: visible; background: #ffffff;}
.divTitulos {position:relative; left:0px; right: 0px;z-index:50;overflow: visible; visibility: visible;}
.divCorpo {position: relative; z-index: 60;width:100%; left: 0px; right: 0px;overflow: auto; visibility: visible; }
.divGenerico {position: absolute; z-index: 2; left: 50px; right: 15px;overflow: visible; visibility: visible; }

.titulos td{color: #ffffff; height: 18px; background-color: #8AAEAE; font-weight: bold; text-align: center;}
.titulos {width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;}
.corpo td{color: #000000; height: 18px; background-color: #D9F4EF; font-weight: normal; text-align: center;}
.corpo {width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;}
.generico td{color: #000000; background-color: #D9F4EF; font-weight: bold; text-align: left; padding-right: 4px; padding-left: 4px;}
.divGenerico table.generico td.genericotab {text-align: right;}
.generico { width: 100%; border: 0 0 0 0;}
.cormedia td{color: #000000; height: 18px; background-color: #C0E1DF; font-weight: normal;}
.cormedia {width: 100%; border: 0 0 0 0; padding-right: 2px; padding-left: 2px;}

.vermelho 
{color: #ff0000;}

.fundobranco td{background-color: #ffffff;}


/********************************/	

body
    {/* "scrollbar" cria estilos para as barras de scroll dos sistemas, mas n�o � suportado em alguns browsers */ /*verificar compatibilidade com browsers atuais, visto que as novas vers�es, como Internet Explorer, t�m varia��es gr�ficas de scroll, o que pode reduzir o impacto visual aplicado*/
					scrollbar-face-color: #cccccc;
					scrollbar-shadow-color: #666666;
					scrollbar-highlight-color: #cccccc;
					scrollbar-3dlight-color: #666666;
					scrollbar-darkshadow-color: #cccccc;
					scrollbar-track-color: #e7e7e7;
					scrollbar-arrow-color: #00000;
/*---------------------------------------------*/	
					background: #fff url(images/PNT/detran_bg1.gif) repeat-y top left;
					font-size: small; font-family: Verdana, Arial, Helvetica, sans-serif;
					color: #000000; margin: 0;
					overflow: hidden;
}

/*Posi��o dos textos e bot�es do cabe�alho */ 
#divTextosCabec {	position:absolute; left:205px;
					top:0px; right:10px;
					height:60px; z-index:120;
					visibility: visible;
}
/*-----------------------------*/
#textosCabec {		float: left; margin: 25px 0 0 0;    
					padding: 10px 0px; color:#999999;
					list-style: none; font-size: 55%;
}
#botaoAjuda {		float: right; margin: -10px 0 0 5px;
					padding: 10px 0px; list-style: none;
}
#botaoAuto {		float: right; margin: -10px 0 0 5px;
					padding: 10px 0px; list-style: none;
}
#botaoInfo {		float: right; margin: -10px 0 0 5px;
					padding: 10px 0px; list-style: none; 
}

#botaoInfo button{	height: 33px; width: 27px; 
					border: 0px none; background: transparent; 
					cursor: hand;
}
#cabecalho {		position:absolute; left:0px;
					top:0px; width:100%;
					height:108px; z-index:1;
					background: #fff url(images/detran_bg_cab.gif) repeat-x top left;
					visibility: visible;
}
/*Nome da tela*/ /* O mesmo nome usado nos menus - t�tulos das telas*/
#nomeTela 	{ 		position:absolute; left: 204px;
					top: 55px; right:10px;
					height:23px; z-index:11;
					overflow: visible; visibility: visible;
					font-size: 65%; font-weight: bold;
					letter-spacing: 2px; } 
/*-----------------------------*/
#rodape 	{		position:absolute; left:0px;
					bottom:-1px; width:100%; 
					height:94px; z-index:1;
					background: #fff url(images/PNT/detran_bg3.gif) repeat-x top left;
					visibility: visible;
}
#imgMenu { 			position:absolute; right: 0px;
					bottom: 94px; width:329px;
					height:294px; z-index:15;
					overflow: visible;
}

#retornar {			position:absolute; right: 10px;
					bottom: 7px; width:26px;
					height:25px; z-index:10;
					background-color: transparent; overflow: visible;
					visibility: visible;
}
#retornar button {	border: 0px; background-color: transparent;
					height: 25px; width: 25px;
					cursor: hand;
}

/*-------0800---------*//*Esta � a posi��o do n�mero de suporte do 0800, no rodap� das p�ginas*/
#telSuporte {		position:absolute; left:35px;
					bottom: 0px; width:550px;
					height:15px; z-index:10; 
					background-color: transparent; overflow: visible;
					color: #FFFFFF; font-family: verdana;
					font-size: 55%;	font-weight: bold;
					letter-spacing: 1px;
}
/********************************/
/*espa�o entre caracteres*/
.espaco {			letter-spacing: 2px;
}
.espaco2{			letter-spacing: 1px;
}

/*Mensagem Aviso*/ /*Este jsp � localizado no "sys"*/
#POPUP {			position: absolute;	width: 510px; 
					height: 20px; overflow: visible;
					border: 1px solid #000000; background-color: #e7e7e7;
					z-index: 200; left: 163px;
					top: 251px; visibility: hidden;
					padding-left: 0px; color: #cc0000; 
					font-weight: bold; letter-spacing: 3px;
					vertical-align: middle;	font-size: 65%
}
		
#POPUP li { 		list-style: none; padding-top: 3px;
}

.msgtexto { 		font-size: 10px; font-weight:normal; 
					color: #000000; letter-spacing: 1px; 
					height:17px  
}
.msgtit { 			font-size: 10px; font-weight:bold; 
					color: #000000; letter-spacing: 1px;
					background:#e7e7e7; display: block; 
					width:470px; padding-left: 5px; 
					padding-right: 5px 
}

#layerpopup { 		border: 1px solid #000000; background: #cccccc;
					position: absolute; top: 270px; 
					left: 163px;  width:510px; 
					height:80px; z-index:200; 
					overflow: auto; padding-left: 0px; 
					visibility: hidden; 
}

#layerpopup li { 	list-style: none; font-size: 75%; 
					vertical-align:middle 
}

#layerbutton { 		border: 1px solid #000000; position: absolute;
					width:510px; height:30px;
					top: 349px; left: 163px;
					z-index:200; overflow: visible;
					text-align: center; padding-top: 5px;
					background: #e7e7e7; visibility: hidden;
}

#layerbutton button{height: 23px; width: 44px;
					border: none; background: transparent; }

/*************************/
.Depuralinhas td{border: 1px solid #ff0000}


</style>