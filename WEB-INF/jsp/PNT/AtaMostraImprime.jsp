<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >

<form name="ConsultaAtaImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
    boolean bPrimeira=false;
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	if 	(ProcessoBeanId.getAtasDO().size()>0){   		
      	 int a =ProcessoBeanId.getAtasDO().size()+1;     
		 for (int i=0;i<ProcessoBeanId.getAtasDO().size();i++) {
				a--;	
				seq++;
				contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if (contLinha>9){
				    contLinha=1;
					npag++;			
					if ( (npag!=1) && (bPrimeira)) {			
		%>
				        </table>      
						<%@ include file="rodape_imp.jsp" %>
						<div class="quebrapagina"></div>
		           <% }
		           bPrimeira=true;
		           %>
	<%@ include file="AtaCabecalhoImp.jsp" %>	    
		       <% } %>	
		    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
		      <tr bgcolor="#c0bdbd"  height="18"> 
		        <td width="100">&nbsp;&nbsp;<strong><%=a%>&nbsp;&nbsp;&#8226;&nbsp;&nbsp;Ata :&nbsp;</strong></td>
		        <td width="150">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getCodEnvioDO() %></td>       
		        <td width="300"><strong>Tipo :</strong>&nbsp;<%=ProcessoBeanId.getAtasDO().get(i).getTipAtaDODesc() %>&nbsp;</td>
		        <td style="text-align: right;"><strong>&nbsp;Data Envio:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatEDO()%>&nbsp;</td>
		      </tr>      
		      <tr><td height="2" colspan="6"></td></tr>
		   </table> 
		   
		   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
		      <tr height="18"> 
		        <td width="100">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aberto por:&nbsp;</strong></td>
		        <td width="150" align="left">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getNomUserNameEDO()%></td>       
		        <td width="100"><strong>�rg�o :</strong>&nbsp;<%=ProcessoBeanId.getAtasDO().get(i).getCodOrgaoLotacaoEDO() %>&nbsp;</td>
		        <td style="text-align: right;"><strong>&nbsp;Data Proc.Envio:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatProcEDO()%>&nbsp;</td>
		      </tr>      
		      <tr><td height="2" colspan="6"></td></tr>
		   </table> 
		   
		   <table width="100%" border="0" cellpadding="0" cellspacing="0" >
		      <tr   height="18"> 
		        <td width="100">&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data Publ.:&nbsp;</strong></td>
		        <td width="150" align="left">&nbsp;<%= ProcessoBeanId.getAtasDO().get(i).getDatPublAta()%></td>       
		        <td width="150"><strong>Aberto Por:&nbsp;&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getNomUserNamePDO() %>&nbsp;</td>
		        <td width="50"><strong>�rg�o:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getCodOrgaoLotacaoPDO() %>&nbsp;</td>
		        <td style="text-align: right;"><strong>&nbsp;Data Proc.Publ.:&nbsp;</strong><%=ProcessoBeanId.getAtasDO().get(i).getDatProcPDO()%>&nbsp;</td>
		      </tr>      
		      <tr><td height="2" colspan="6"></td></tr>
		   </table> 
   <!--fim dados fixos par todo os eventos-->
     <%}%>
<%}else{%>
<div id="mensagem" style="position:absolute; left:70px; top:85px; width:538px; height:35px; z-index:10; overflow: visible; visibility: visible;"> 
	<table width="119" align="center" border="0" cellpadding="0" cellspacing="0" class="semborda">
	  <tr><td bgcolor="#FFFFFF">&nbsp;&nbsp;<strong>PROCESSO SEM HIST�RICO</strong></td> </tr>	
	</table>
</div>          
<%}%>
</div>
<%@ include file="rodape_imp.jsp" %>
</form>
</body>
</html>