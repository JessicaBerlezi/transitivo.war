<!--M�dulo : PNT
	Vers�o : 1.1
	Atualiza��es:
-->	
<% String path=request.getContextPath(); 
   String npag="1";   
   String tituloConsulta = (String)session.getAttribute("tituloConsulta");
  	if(tituloConsulta==null) tituloConsulta="";   
%> 
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="CssImpressao.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" >

<form name="processoImprimeform" method="post" action="">
<%@ include file="Cab_imp.jsp" %>

  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda" >
    <tr>
      <td height="17" width="50%" style="padding-left: 4px;">Processo :<strong class="fontmaior">&nbsp;<%= ProcessoBeanId.getNumProcesso() %></strong></td>
	  <td width="25%"><strong class="fontmaior"><%=ProcessoBeanId.getTipProcessoDesc()%></strong></td>
      <td colspan="2" align="right" style="padding-right: 4px;">CPF :<strong class="fontmaior">&nbsp;<%=ProcessoBeanId.getNumCPFEdt()%></strong ></td>
    </tr>
  </table>   


  <table width="100%" border="0" cellpadding="1" cellspacing="0" class="semborda">
	<tr><td colspan="4" height="2"></td></tr>
	<tr > 
      <td style="padding-left: 4px;" height="17" width="25%">Data do Processo :&nbsp;<%=ProcessoBeanId.getDatProcesso()%></td>	  
      <td width="25%">Prazo Defesa :&nbsp;<%=ProcessoBeanId.getUltDatDefesa() %></td>	  
	  <td width="25%">Prazo Recurso :&nbsp;<%=ProcessoBeanId.getUltDatRecurso() %></td>
      <td style="padding-right: 4px;" align="right">Prazo Entrega CNH :&nbsp;<%=ProcessoBeanId.getUltDatEntregaCNH() %></td>	 
    </tr>
  </table>   


 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-top: 5px">
   <tr bgcolor="#c0bdbd"> 
     	<td height="17" style="padding-left: 4px;" >Condutor :&nbsp;<span style="font-weight: bold"><%= ProcessoBeanId.getNomResponsavel()%></span>
     	</td>    
   </tr>
   	
</table>
		
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" >
	<tr><td colspan="3" height="2"></td></tr>
    <tr> 
      <td height="17" style="padding-left: 4px;">Endere�o :&nbsp;<%=ProcessoBeanId.getEndereco()%>&nbsp;&nbsp;&nbsp;<%=ProcessoBeanId.getMunicipio()%></td>	  
      <td colspan="3" style="text-align: right; padding-right: 4px;">CEP : &nbsp;<%=ProcessoBeanId.getCepEdt()%></td>
    </tr>
</table>
 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-bottom: 5px">
    <tr><td colspan="4" height=2></td></tr>
	<tr>
	 <td height="17" style="padding-left: 4px;"> 
        <%=ProcessoBeanId.getTipCNHDesc() %>&nbsp;:&nbsp;<%= ProcessoBeanId.getNumCNH() %>/&nbsp;<%= ProcessoBeanId.getUfCNH() %>
     </td>         
   	 <td>Expedi&ccedil;&atilde;o : <%=ProcessoBeanId.getDatEmissaoCNH()%></td>
   	 <td>Validade : <%=ProcessoBeanId.getDatValidadeCNH() %></td>
   	 <td style="padding-right: 4px;" align="right">Categoria : <%=ProcessoBeanId.getCategoriaCNH() %></td>
    </tr>
   	<tr><td colspan="4" height="2"></td></tr>
   	<tr> 
      <td style="padding-left: 4px;" height="17">Total Pontos :&nbsp;<%=ProcessoBeanId.getTotPontos() %></td>
      <td>Total Multas :&nbsp;<%=ProcessoBeanId.getQtdMultas() %></td>
	  <td>Total Notifs :&nbsp;<%=ProcessoBeanId.getTotNotificacoes() %></td>
	  <td style="padding-right: 4px;" align="right">Total Proc Ant. :&nbsp;<%=ProcessoBeanId.getQtdProcAnteriores() %></td>	
    </tr>
    <tr><td height="2" colspan="4"></td></tr>
   	<tr> 
      <td height="17" colspan="2" style="padding-left: 4px;">Status :&nbsp;<strong><%=ProcessoBeanId.getCodStatus() %>&nbsp;-&nbsp;<%= ProcessoBeanId.getNomStatus() %></strong></td>
	  <td colspan="3" align="right" style="padding-right: 4px;">Situa��o :&nbsp;<strong><%=ProcessoBeanId.getSitProcesso()%></strong></td>
    </tr>	
    <tr><td height="2" colspan="4"></td></tr>
</table>
<%@ include file="rodape_imp.jsp" %>




</form>
</body>
</html>