<!--Bloco 15 - PNT
-->	
<%  String nomeFase15 = "Autua��o";
	String nomeDef15  = "Defesa";
	String nomeSeq15a  = "000";
	String nomeSeq15b  = "001";
	if ("2".equals(notificacao.getTipoNotificacao())) {  %>
		String nomeFase15 = "Penalidade";
		String nomeDef15  = "Recurso";
		nomeSeq15a        = "001";
		nomeSeq15b        = "001";
<% } %>

<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1"></td></tr>
</table>

<div id="observacao" style="position:relative; vertical-align:top; left: 0px; top: 0px; width: 100%; height: 272px;">    
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr>
    <td height="16" align="center" bgcolor="#CCCCCC"><span class="style1"><strong>OBSERVA&Ccedil;&Otilde;ES</strong></span></td>
  </tr>
  <tr>
    <td  class="fontmaior" style="text-align:justify; padding: 20px">
	  <br />
	  ESTE DOCUMENTO COMPLEMENTA A NOTIFICA&Ccedil;&Atilde;O DE <%= nomeFase15.toUpperCase() %> <%=notificacao.getNumNotificacao()%>-<%= nomeSeq15a %> DO PROCESSO <%=ProcessoBeanId.getNumProcesso()%>.<br />
	    <br />
O FORMUL�RIO PARA INTERPOR <%= nomeDef15.toUpperCase() %> ENCONTRA-SE NA NOTIFICA��O DE <%= nomeFase15.toUpperCase() %> <%=notificacao.getNumNotificacao()%>-<%= nomeSeq15b %> OU PODE SER OBTIDO NO ENDERE�O ABAIXO.    </td>
  </tr>
</table>
</div>
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_linha_top">
    <tr> 
      <td align="center" height="30"  class="fontmaior" style="font-weight: bold">Endere&ccedil;o 
        para Defesa e Recurso:<br>
	  Av. Pres. Vargas, 817/Protocolo Geral (Sobreloja) - Centro - RJ - CEP 21071-004</td>
    </tr>
</table>