<!--Bloco 15 Penal - PNT 
-->	
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr><td height="1"></td></tr>
</table>

<div id="observacao" style="position:relative; vertical-align:top; left: 0px; top: 0px; width: 100%; height: 272px;">    
<table width="100%" border="0" cellpadding="0" cellspacing="3" class="semborda">
  <tr>
    <td height="16" align="center" bgcolor="#CCCCCC"><span class="style1"><strong>INSTRU&Ccedil;&Otilde;ES PARA INTERPOR RECURSO </strong></span></td>
  </tr>
   <tr>
    <td style="font-family:Arial, Helvetica, sans-serif; line-height:9pt; font-size: 8px; text-align:justify; padding: 20px; padding-right: 15px">
	  <br />
	  - O RECURSO DEVER&Aacute; SER INTERPOSTO POR ESCRITO, NESTE PR&Oacute;PRIO DOCUMENTO, QUADRO &quot;JUSTIFICATIVA / DESCRI&Ccedil;&Atilde;O DOS FATOS&quot;, NO PRAZO ESTABELECIDO, CONTENDO:
      <ul style="margin-top: 2px; margin-bottom: 2px">
	  <li>EXPOSI&Ccedil;&Atilde;O DOS FATOS, FUNDAMENTA&Ccedil;&Atilde;O LEGAL DO PEDIDO E DOCUMENTOS QUE COMPROVEM A ALEGA&Ccedil;&Atilde;O,</li>
      <li>FOTOC&Oacute;PIA LEG&Iacute;VEL DA PERMISS&Atilde;O OU DA CNH E, NA FALTA DESTE, DOCUMENTO DE IDENTIDADE QUE COMPROVE A ASSINATURA DO INFRATOR;,</li>
      <li>DATA E ASSINATURA DO REQUERENTE OU DO SEU REPRESENTANTE LEGAL;</li>
	  </ul>
 
          - CASO V.S&ordf;. N&Atilde;O SE MANIFESTE NO PRAZO ESTABELECIDO, INCORRER&Aacute; NA(S) PENALIDADE(S) PREVISTA(S) NA LEGISLA&Ccedil;&Atilde;O DE TR&Acirc;NSITO;
          <br />
          - O INFRATOR PODER&Aacute; SER REPRESENTADO POR PROCURADOR LEGALMENTE HABILITADO, MEDIANTE APRESENTA&Ccedil;&Atilde;O DE PROCURA&Ccedil;&Atilde;O, NA FORMA DA LEI, SOB PENA DE N&Atilde;O RECONHECIMENTO DO RECURSO;
          <br>
          - ESTE PROCEDIMENTO N&Atilde;O SE PRESTA PARA AVALIAR RECURSO CONTRA O M&Eacute;RITO DAS MULTAS RELACIONADAS NESTA NOTIFICA&Ccedil;&Atilde;O;
          <br />
          - SER&Aacute; CONSIDERADO INV&Aacute;LIDO O RECURSO PREENCHIDO DE FORMA INCOMPLETA, DEFICIENTE, SEM ASSINATURA OU FALTANDO ALGUNS DOS DOCUMENTOS REQUERIDOS;
          <br />
          - CABE AO RESPONS&Aacute;VEL PELA CNH ATUALIZAR O SEU ENDERE&Ccedil;O JUNTO AO DETRAN-RJ, CONFORME PREVISTO NO &sect; 1&ordm;, ART. 282 DO C&Oacute;DIGO DE TR&Acirc;NSITO BRASILEIRO. ESGOTADOS TODOS OS MEIOS PREVISTOS PARA NOTIFICAR O INFRATOR, A NOTIFICA&Ccedil;&Atilde;O DAR-SE-&Aacute; POR EDITAL, NA FORMA DA LEI.
	      <br />
	      - O RECURSO DEVER&Aacute; SER APRESENTADO NO DETRAN-RJ - PROTOCOLO GERAL, NO ENDERE&Ccedil;O ABAIXO OU NAS CIRETRANS. 
	      <br />
	      - CASO O ESPA&Ccedil;O RESERVADO &Agrave; JUSTIFICATIVA SEJA INSUFICIENTE, PROCEDER O RECURSO EM DOCUMENTOS ANEXOS.
		  </td>
  </tr>
</table>
</div>
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="table_linha_top">
    <tr> 
      <td align="center" height="30"  class="fontmaior" style="font-weight: bold">Endere&ccedil;o 
        para Defesa e Recurso:<br>
	  Av. Pres. Vargas, 817/Protocolo Geral (Sobreloja) - Centro - RJ - CEP 21071-004</td>
    </tr>
</table>

