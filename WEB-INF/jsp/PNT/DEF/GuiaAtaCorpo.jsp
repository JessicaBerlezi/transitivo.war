<!--M�dulo : PNT.DEF
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<%@ page import="java.util.List"%>
<%@ page import="PNT.DEF.GuiaDistribuicaoBean"%>
<%@ page import="PNT.ProcessoBean"%>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="funcionalidadesBeanId"   scope="session" class="PNT.DEF.FuncionalidadesBean" /> 
<%
String posTop = request.getParameter("posTop"); if(posTop==null) posTop ="170px";   
String posHei = request.getParameter("posHei"); if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
	  <%
	  	PNT.ProcessoBean myProcesso  = null;	
		Iterator guias = GuiaDistribuicaoId.getRelatGuias().iterator();
		Iterator it = null;
		while (guias.hasNext()) {
			GuiaDistribuicaoBean guia = (GuiaDistribuicaoBean)guias.next();
		%>
		<tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>
		<tr>
			<td height="5" colspan="3">&nbsp;<strong>Junta: <%=guia.getSigJunta()%></strong></td>
		  <td colspan="2" align="center" bgcolor="#FFFFFF"><strong>Guia: <%=guia.getNumGuia()%></strong></td>
		  <td colspan="2" align="right" bgcolor="#FFFFFF"><strong>Relator: <%=guia.getNomRelator()%></strong></td>
		</tr>
		<%
			//List processos = guia.getProcessosComRecursoIndeferidos();
			List processos = guia.getProcessos(funcionalidadesBeanId.getTipoRequerimento());
			int seq = 0;
			String cor   = (seq%2==0) ? "#d9f4ef" : "#ffffff";
			if (processos.size()>0) {
				it = processos.iterator();
				while (it.hasNext()) {
					myProcesso = (ProcessoBean)it.next() ;		
					cor    = (seq%2==0) ? "#d9f4ef" : "#B5EAE0";
					if (myProcesso.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width=40 height="14" align=center>
	        	&nbsp;<%= (seq+1) %>&nbsp;</td>
	        <td width=80 align="center">
	        	&nbsp;<%=myProcesso.getSigOrgao() %></td>
			<td width=150>
				&nbsp;&nbsp;<%=myProcesso.getNumProcesso() %>   </td>
			<td width=80 align="center">
				&nbsp;<%=myProcesso.getDatProcesso() %>    </td>
			<td width=200>&nbsp;&nbsp;<%=myProcesso.getNumRequerimento() %></td>			
	        <td>&nbsp;&nbsp;<%=myProcesso.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBeanId.getTipoRequerimento()).getNomResultRS() %></td>
		</tr>
		<%			if (myProcesso.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=7>&nbsp;<%=myProcesso.getMsgErro() %> </td>
			</tr>			
		<% 			} %>	
    	<tr><td height=1 colspan=7 bgcolor="#666666"></td></tr>
		<% 			seq++; 
				}
			}	
			else {
		 		String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
		 		if (msg.length()>0) {
	%>	
		<tr bgcolor='#d9f4ef'> 
	        <td height="14" colspan="7" align=center bgcolor="#CC3300"><b><%= msg %></b></td>
		</tr>	
		<tr><td height=1 colspan=7 bgcolor="#666666"></td></tr>
		<%	
				} 
			}%>
		<tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>
		<tr><td height=7 colspan=7 bgcolor="#ffffff"></td></tr>
	<%	}%>
  </table>      
</div>  
