<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="funcionalidadesBean"   scope="session" class="PNT.DEF.FuncionalidadesBean" /> 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'InformaResult':
	      if (veCampos(fForm)==true) {		   
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  }
		  break;
	   case 'R':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
	    	close() ;
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function comparaDatas(data1,data2) {
	var valid = true;
	var dataA = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var dataB = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var difference=dataB-dataA;//obt�m o resultado em milisegundos.
	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = false;
	return valid;
}   

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datRS,1) && valid
	verString(fForm.datRS,"Data do Resultado",0);
    <%if(GuiaDistribuicaoId.getIndSessao().equals("S")){%>
	valid = ValDt(fForm.datSessao,1) && valid
	verString(fForm.datSessao,"Data da Sess�o",0);
	
/*	if (comparaDatas('<%=GuiaDistribuicaoId.getDatProc()%>',fForm.datSessao.value)==false)
	{
	  sErro+="Data da Sess�o inferior a data da Guia";
	  valid=false;
	}*/
	
	if (comparaDatas(fForm.datSessao.value,fForm.datRS.value)==false)
	{
	  sErro+="Data da Sess�o superior a data do Resultado";
	  valid=false;
	}
	<%}%>
	if (valid==false) alert(sErro) ;
	return valid ;
}
function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["InformaResultForm"].target = "_self";
	document.all["InformaResultForm"].action = "acessoTool";  
	document.all["InformaResultForm"].submit() ;	 
}
function Mostra(opcoes,fForm,numplaca,numprocesso) {	
	document.all["acao"].value          = opcoes;
	document.all["processoMostra"].value = numprocesso;
   	document.all["InformaResultForm"].target = "_blank";
    document.all["InformaResultForm"].action = "acessoTool";  
   	document.all["InformaResultForm"].submit() ;	 
}
function mostaCodMotivoDef(seq){
   document.all["codMotivoDef"+seq].style.visibility='visible' ; 
}
function escondeCodMotivoDef(seq){
   document.all["codMotivoDef"+seq].style.visibility='hidden' ; 
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResultForm" method="post" action="">

<//%@ include file="Cab_Diretiva.jsp" %>

<%@ include file="../Cab.jsp"%>

<input name="acao"           type="hidden" value=' '>
<input name="processoMostra"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=3></td></tr>
     <tr bgcolor="#8AAEAE">
        <td width="41%"><b>GUIA DE DISTRIBUI��O&nbsp;&nbsp;N&ordm;&nbsp;<%=GuiaDistribuicaoId.getNumGuia()%>
			&nbsp;&nbsp;-&nbsp;&nbsp;<%=GuiaDistribuicaoId.getDatProc()%></b>
        </td>        
        <td width="54%">
		  Data do Resultado:&nbsp;
		  <input type="text" name="datRS" size="12" maxlength="10" value='<%=GuiaDistribuicaoId.getDatRS() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
        <td width="5%" align="center" valign="middle"> 
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('InformaResult',this.form);">	
    	      <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
        	  </button>
		</td>
 	 </tr>
     <tr><td colspan=3 height=4></td>
     </tr>
     <tr bgcolor="#8AAEAE">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
	     	<tr>
		        <td width="50%" height=20>&nbsp;Junta:&nbsp;<b><%=GuiaDistribuicaoId.getSigJunta() %></b></td>
		        <td width="50%" align=right >Relator:&nbsp;<b><%=GuiaDistribuicaoId.getNomRelator() %></b>&nbsp;</td>        
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=3 height=4></td>
    </tr>     
  </table>
  <%@ include file="GuiaInformaResultTop.jsp" %>  
</div>
<%
  String posTop = "179 px";
  String posHei = "166 px";  
%>
<%@ include file="GuiaInformaResultCorpo.jsp" %> 

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
String mostraMsg   = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp"  %>  
<!--FIM_Div Erros-->
</form>
</body>
</html>