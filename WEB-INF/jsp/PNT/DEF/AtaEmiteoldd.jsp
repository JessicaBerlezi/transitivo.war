<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>
<%@ page import="sys.Util" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="PNT.AtaDOBean"%>

<jsp:useBean id="AtaDOBeanId"   scope="session" class="PNT.AtaDOBean" /> 
<%
List listaAtas = (List) request.getAttribute("listaAtas");	
if(listaAtas==null) 	listaAtas= new ArrayList();

List<AtaDOBean> listaAtasList = new ArrayList<AtaDOBean>();
String CodAtaDOSel = "";
%>





<html>
<head>
<%@ include file="../Css.jsp" %>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'ImprimirAta':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'GeraArquivo':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
	  case 'GeraExcel':
	  if (vCampos(fForm)==true){
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "GeraXlsAtaDO";  
		fForm.submit();  		  
	  }
	  break ;
   case 'R':  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function mostraNumEdital(fForm){
  fForm.codEdital.value=fForm.codEnvioDO.value;
}

function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.codEdital,"Edital n�",0);
    if (valid==false) 
      alert(sErro) ;
	return valid ;
}


</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AtaForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao" type="hidden" value=' '>	
<input name="confirmaExclusao" type="hidden" value="N">	
<input name="nomArquivo"  type="hidden" value='AtaEmite.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>
<div id="WK_SISTEMA" style="position:absolute; width:470px; overflow: visible; z-index: 1; top: 100px; left:200px;" > 
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
	  <tr><td valign="top" colspan=2 height="10"></td></tr>	
	  <tr>
	      <%if (request.getParameter("j_sigFuncao").equals("PNT0730")) { %>
		  <td width="50"><b>Apreens�o:</b></td>
		  <% } else {%>
		  <td width="50"><b>Edital N�:</b></td>		  
		  <% } %>
		  <td>
		    &nbsp;&nbsp;&nbsp;<input name="codEdital" style="margin-left: 0px;" type="text"  size="10" maxlength="6" value="" onkeypress="javascript:Mascaras(this,'999999');" readonly>
      </tr>
	  <tr>
        <td colspan="2" height="5" ></td>
      </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
      <tr>
        
        <%if (request.getParameter("j_sigFuncao").equals("PNT0730")) { %>
		<td width="50"><b>Arquivos&nbsp;&nbsp;&nbsp;:</b></td>
		<% } else {%>
		<td width="50"><b>Atas :</b></td>		  
		<% } %>
        
        <td align="left">
        <select id="comboAtas" name="codEnvioDO" >
        	<%
        		
        	if (listaAtas.size()>0){  			
				for (int t=0;t<listaAtas.size();t++) {
					PNT.AtaDOBean myAtas = (PNT.AtaDOBean) listaAtas.get(t);%>								
					<script>
						document.write("<option value='<%=myAtas.getCodEnvioDO()%>'>ATA:  <%=myAtas.getCodEnvioDO()%>     Enviada: <%=myAtas.getDatEDO()%>    Emitida: <%=myAtas.getDatEmissaoEDO()%>     Processos:  <%=myAtas.getQtdProc()%></option>");
					</script>
				<%}
			}%>
		</select>		
           
           
           
           
           
           
           </td>
        <td width="45" align="right" ><button type="button" NAME="Ok" style="width: 29px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimirAta',this.form);"><IMG src="<%= path %>/images/bot_imprimir_ico.gif" align="left" width="26" height="19" alt="Imprimir Layout da Ata"></button></td>
        
        <!-- 
        <td align="left">&nbsp;&nbsp;
			<button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form);"> 	
				<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
			</button>
		</td>

		 -->
        <td align="left">&nbsp;&nbsp;
			<button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraArquivo',this.form);"> 	
				<IMG src="<%= path %>/images/bot_incluir_ico.gif" align="left" alt="Gerar Arquivo Rtf">
			</button>
		</td>
      </tr>
      <tr>
        <td colspan="4" height="5" ></td>
      </tr>
   </table>

</div>

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<%  	
String msgErro     = AtaDOBeanId.getMsgErro();
String msgOk       = AtaDOBeanId.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

<% 	String flag = (String)request.getAttribute("confirmaExclusao");
	if(flag != null){
		if(flag.equals("S")){
%>
<script>
	if(confirm("Existe arquivo gerado para a ATA: <%=AtaDOBeanId.getCodEnvioDO()%> \nDeseja Alterar o Arquivo Existente?")==1){
		AtaForm.confirmaExclusao.value = "S"
		valida('GravarAta',AtaForm)
	}
</script>

<%		}
	}	
%>	

</form>
</body>
</HTML>


