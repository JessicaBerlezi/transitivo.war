<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	

<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ArquivoAtaId" scope="request" class="PNT.AtaDOBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
		case 'consultaArq':
			if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		break;	
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'AbreRecurso':
	      if (veCampos(fForm)==true) {
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}

function download(nomArquivo,fForm) {	
	fForm.nomArquivo.value=nomArquivo;
   	fForm.target= "_self";
    fForm.action = "download";
    fForm.submit();
}

function registra(opcao,codArquivo,fForm) {	
	fForm.acao.value = opcao
	fForm.codArquivo.value=codArquivo;
   	fForm.target= "_self";
    fForm.action = "acessoTool";
    fForm.submit();
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownloadAta" method="post" action="">

<%@ include file="../Cab.jsp" %>

<input name="acao"     type="hidden" value=' '>
<input name="nomArquivo"  type="hidden" value=''>
<input name="codArquivo"  type="hidden" value=''>
<input name="contexto"  type="hidden" value="DIR_ARQUIVO_ATA">


<!--IN�CIO_CORPO_sistema-->
<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:580px; overflow: visible; z-index: 1; top: 80px; left: 205px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="55%"><b>&nbsp;Data de Envio:&nbsp;</b>&nbsp;<input name="De" type="text" id="De" size="12" maxlength="12" value="<%=(String)request.getAttribute("dataIni")%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  &nbsp;at&eacute;&nbsp;&nbsp;
			  <input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=(String)request.getAttribute("dataFim")%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  </td>		  
	          <td align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consultaArq',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:205px; top:328px; width:580px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;j&aacute; efetuado</td>
        <td width="10" bgcolor="#FFFFFF" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>
        &nbsp;n&atilde;o efetuado</td>
      </tr>
    </table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:205px; top:105px; width:400px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="titulos">
      <tr> 
        <td style="text-align: left" width="150" height="15"><strong>&nbsp;&nbsp;&nbsp;N&uacute;mero da ATA</strong></td>
		<td align="center"><strong>Enviada em</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:205px; top:124px; width:400px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0" >
         
     <%
        if(((String)request.getAttribute("statusInteracao")).equals("2")){
      		List vetArquivos = (List) request.getAttribute("vetArquivos"); 
 			String caminho = (String)request.getAttribute("caminho");
    		String corArq;
      		for(int i=0;i<vetArquivos.size();i++) {
      			PNT.AtaDOBean arqAtaBean = null;
      			arqAtaBean=(PNT.AtaDOBean)vetArquivos.get(i);   		
      			if (arqAtaBean.getCodEventoDownload().equals("0")) {
      				corArq="#FFFFFF";
      			}
      			else {
      				corArq="#FFFFD5";
      			}
      %>
      <tr bgcolor="<%=corArq%>"> 
        <td width="150" height="15">&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onActivate="javascript: registra('registra','<%=arqAtaBean.getCodArquivoDownload()%>',document.frmDownloadAta);" onClick="javascript: download('<%=arqAtaBean.getNomArquivoDownload()%>',document.frmDownloadAta);"><strong><%=arqAtaBean.getCodEnvioDO()%></strong></a></td>
        <td align="center"><a href="#" onClick="javascript: download('<%=arqAtaBean.getNomArquivoDownload()%>',document.frmDownloadAta);" onActivate="javascript: registra('registra','<%=arqAtaBean.getCodArquivoDownload()%>',document.frmDownloadAta);"><strong><%=arqAtaBean.getDatPublAta()%></strong></a></td>
      </tr>
      <tr> 
        <td height="1" colspan="2"  bgcolor="#cccccc"></td>
      </tr>
      
      <%	} 
      	}%>
    </table>
</div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/PNT/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<%  	
String msgErro       = ArquivoAtaId.getMsgErro();
String msgOk         = ArquivoAtaId.getMsgOk();
String msgErroTop    = "160 px" ;
String msgErroLeft   = "60 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</body>
</HTML>


