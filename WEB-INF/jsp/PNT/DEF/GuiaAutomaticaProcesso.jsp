<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	

<%@ page session="true"%>
<% String path = request.getContextPath();%>
<%@ page errorPage="../ErrorPage.jsp"%>
<%@ page import="sys.Util"%>

<!-- Chama o Objeto do Usuario logado -->

<jsp:useBean id="GuiaDistribuicaoId" scope="session" class="PNT.DEF.GuiaDistribuicaoBean"/>
<jsp:useBean id="RelatorBeanId" scope="session" class="PNT.TAB.RelatorBean" />
<jsp:useBean id="funcionalidadesBean" scope="session" class="PNT.DEF.FuncionalidadesBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script>var bProcessa=true;</script>
<html>
	<head>
    <%@ include file="../Css.jsp"%>		
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

		<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

		<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {   	  
	   case 'Retornar':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
			break;
	   case 'Distribuir':
	      if (bProcessa==false)
	      { 
	        alert("Guia de Distribui��o em Processamento...");
	        return;
	      }
	      if (veCampos(fForm)==true) {		  
	        bProcessa=false;
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function SomaRelatores(fForm) {
	somaRelator = 0;
	 for (i=0;i<fForm.relatores.length;i++)
     {
         if(fForm.relatores[i].selected==true)
          somaRelator +=1
     } 
     fForm.numRelatores.value=somaRelator;
}




function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	somaRelator = 0;
	somaProc = 0;
	 for (i=0;i<fForm.relatores.length;i++)
     {
         if(fForm.relatores[i].selected==true)
          somaRelator +=1
     } 
     if (somaRelator <= 1){
     //   sErro+="Selecione os relatores.\n"
       // valid = false
     }
     if (fForm.ProcessosErro.value>0) {
        sErro+="Existem Processos com Erro!\n"
        valid = false
     }
     
     if (fForm.totalProcessos.value==1) {
        sErro+="S�o necess�rios pelo menos 2 Processos para a distribui��o!\n"
        valid = false
     }
     
	if (valid==false) alert(sErro) ;
	return valid ;
}

</script>
	</head>
	<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
		<form name="PreparaGuiaForm" method="post" action="">
			
			<%@ include file="../Cab.jsp" %>
			<input name="acao" type="hidden" value=' '>
			<input name="totalProcessos" type="hidden" value='<%=GuiaDistribuicaoId.getTotalProcessos()%>'>
			
			<!--IN�CIO_CORPO_sistema-->
			<div id="recurso3" style="position:absolute; left:50px; top:80px; width:720px; height:20px; z-index:2; overflow: visible; visibility: visible;">
				<table border="0" cellpadding="0" cellspacing="0" width=100% align="center">
					<tr>
						<td colspan="5"></td>
					</tr>
					<tr>
						<td width="10%" rowspan="4" align="left" bgcolor="#d9f4ef">
							&nbsp;&nbsp;<strong>Relatores:&nbsp;&nbsp;</strong> </td>
						<td width="44%" rowspan="4" align="left" bgcolor="#d9f4ef">
							<table bgcolor="#faeae5" border="0" cellpadding="0" cellspacing="0" width="100%" align="left">
								<tr bgcolor="#faeae5">
									<td align="left" bgcolor="#d9f4ef">

				<% if (RelatorBeanId.getRelatores().size() > 0) {
				out.println("<select name=\"relatores\" multiple size=5 onChange=javascript:SomaRelatores(this.form) style=\"width: 300px\">");
				for (int i = 0; i < RelatorBeanId.getRelatores().size(); i++) {
				  out.println("<option value=\""+RelatorBeanId.getRelatores(i).getCodJunta()
				  + RelatorBeanId.getRelatores(i).getNumCpf()
			      + Util.rPad(RelatorBeanId.getRelatores(i).getSigJunta(), " ", 10)
				  + Util.rPad(RelatorBeanId.getRelatores(i).getNomRelator(), " ", 30) + "\">"
				  + RelatorBeanId.getRelatores(i).getSigJunta()
				  + " - "
				  + RelatorBeanId.getRelatores(i).getNomRelator()+ " ("+ RelatorBeanId.getRelatores(i).getQtdProcDistrib()+") "
				  + "</option>");
				}
				out.println("</select>");
			  }
			%>									</td>
								</tr>
							</table>						</td>
						<td width="28%" rowspan=4  bgcolor="#d9f4ef">
                          <strong>&nbsp;&nbsp;Relatores selecionados :</strong><br>
                          <strong>&nbsp;&nbsp;Processos a serem distribu�dos :</strong><br>
                          <strong>&nbsp;&nbsp;Requerimentos a serem distribu�dos :</strong><br>
                          <strong>&nbsp;&nbsp;Processos exclu�dos por erro :</strong><br>
						</td>
						<td width="6%" rowspan=4  bgcolor="#d9f4ef">
						<input readonly="readonly" type="text" name="numRelatores" value="0" class="sem-borda" style="background-color:transparent; height:16px; width:10px; margin-left:-1px"><br/>
						<%=GuiaDistribuicaoId.getTotalProcessos()%>(<%=GuiaDistribuicaoId.getTotalRequerimentos()%>)<br />
						<%=GuiaDistribuicaoId.getTotalRequerimentos()%><br />
						<%=GuiaDistribuicaoId.getToTalProcessosErro()%>
						&nbsp;</td>
						<td width="12%" align="center" valign="middle" bgcolor="#d9f4ef"><br /><br />
						
						<button type="button" style="height: 21px; width: 54px; border: none; background: transparent;"  onClick="javascript: valida('Distribuir',this.form);"> <img src="<%= path %>/images/PNT/bot_distribuir_txt.gif" width="52" height="19"></button>                        </td>
					</tr>
					<tr>
						<td align="center" valign="middle" bgcolor="#d9f4ef">&nbsp;						</td>
					</tr>
					<tr>
						<td align="center" valign="middle" bgcolor="#d9f4ef">
						<button type=button name="Limpar" style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('Retornar',this.form);">
						<img src="<%= path %>/images/PNT/bot_retornar_txt.gif" width="52" height="19" align="left" alt="Retornar"></button>						</td>
					</tr>
					<tr>
					  <td align="center" valign="middle" bgcolor="#d9f4ef">&nbsp;					  </td>
					</tr>
			</table>
				<%@ include file="GuiaTopAutomatica.jsp" %>
			</div>
			
			
			<%
					String posTop="205 px";
					String posHei="170 px";
			%>	
			
			<%@ include file="GuiaPreparaCorpoAutomatica.jsp" %>
					
			

			<!-- Rodap�-->
			<%@ include file="../Retornar.jsp" %>
			<%@ include file="../Rod.jsp" %>
			
			<!-- Fim Rodap� -->
			<!--Div Erros-->
			<%
					String msgErro= GuiaDistribuicaoId.getMsgErro();
					String msgOk= GuiaDistribuicaoId.getMsgOk();
					String msgErroTop=  "160 px";
					String msgErroLeft= "80 px";
					String mostraMsg     = "hidden";
			%>	
			<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
				
			<!--FIM_Div Erros-->

		</form>
	</body>
</html>


