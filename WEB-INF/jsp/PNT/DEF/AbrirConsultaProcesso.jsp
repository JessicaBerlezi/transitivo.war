<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Infracao -->
<jsp:useBean id="AutoInfracaoBeanId" scope="request" class="REC.AutoInfracaoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="AbrirRecForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>
<!--IN�CIO_CORPO_sistema--> 
<%@ include file="MostraAuto.jsp" %>    
<%@ include file="apresentaInfracao_Diretiva.jsp" %>   
<div style="position:absolute; left:50px; right: 15px; top:274px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>
<div id="recurso3" style="position:absolute; left:50px; right: 15px; top:273px; height:45px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
     <tr><td colspan=3 height=20></td></tr>
     <tr bgcolor="#faeae5">
   	    <td width="70%" valign="middle"> 
		<%= AutoInfracaoBeanId.getNomStatus()+" em "+AutoInfracaoBeanId.getDatStatus() %>	
		<%  if (AutoInfracaoBeanId.getIdentAutoOrigem().length()>0 && !AutoInfracaoBeanId.getIdentAutoOrigem().equals("0") ) {%>
		&nbsp;-&nbsp;<b>RENAINF</b>
		<%}%>
        </td>			
     </tr>
     <tr><td colspan="3" height=20></td></tr>
  </table>		
</div>

<div style="position:absolute; left:50px; right: 15px; bottom:65px; height:30px; z-index:20; overflow: visible; visibility: visible;">  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">
    <tr height=2><td bgcolor="#993300" ></td></tr>
  </table>
</div>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgErro = AutoInfracaoBeanId.getMsgErro();
String msgOk = AutoInfracaoBeanId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</body>
</html>