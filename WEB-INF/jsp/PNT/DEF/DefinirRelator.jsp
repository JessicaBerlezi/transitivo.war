<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="PNT.ProcessoBean" %>
<jsp:useBean id="ProcessoBeanId"  scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="RequerimentoId"  scope="request" class="PNT.RequerimentoBean" /> 
<jsp:useBean id="RelatorId"       scope="request" class="PNT.TAB.RelatorBean" /> 
<jsp:setProperty name="RelatorId" property="j_abrevSist" value="PNT" />  
<jsp:setProperty name="RelatorId" property="colunaValue" value="cod_Relator_Junta" />
<jsp:setProperty name="RelatorId" property="popupNome"   value="cod_Relator_Junta"  />
<%
   String r = RequerimentoId.getCodRelatorJU();
   RelatorId.setChecked(r);
   RelatorId.setPopupString("nom_Relator_Junta,SELECT cod_relator cod_Relator_Junta,"+
     "rpad(j.sig_junta,10,' ')||'-'||r.nom_relator nom_Relator_Junta "+
     "FROM TPNT_RELATOR r,TPNT_JUNTA j "+
     "where r.cod_junta=j.cod_junta and j.ind_tipo='"+request.getAttribute("TipoJunta")+"' AND "+
     " r.ind_ativo='S' order by nom_relator_junta");     
%>   

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N': 
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
	   case 'DefineRelator':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datJU,1) && valid
	verString(fForm.datJU,"Data de Defini��o do Relator",0);
	verString(fForm.cod_Relator_Junta,"Junta/Relator",0);	
	fForm.codRelatorJU.value = fForm.cod_Relator_Junta.value ;	
	if (valid==false) alert(sErro) 
	return valid ;
}
</script>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="InformaResForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao"             type="hidden" value=''>
<input name="codRelatorJU"     type="hidden" value="">				
<input name="codRequerimento"  type="hidden" value="<%= RequerimentoId.getCodRequerimento() %>">	
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
       	<td height="18" colspan="3"><strong><%= ProcessoBeanId.getMsgOk() %></strong></td>
      	<td>
    		<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('N',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>            
        </td>
   	</tr>    
    <tr><td height="4" colspan="4"></td></tr>	
    <tr bgcolor="#d9f4ef"><td height="18" colspan="4"><strong>
    	Requerimento: &nbsp;<%= RequerimentoId.getNumRequerimento() %>&nbsp;(<%= RequerimentoId.getCodTipoSolic() %>)&nbsp;&nbsp;&nbsp;&nbsp;
    	Data: &nbsp;<%= RequerimentoId.getDatRequerimento() %>&nbsp;&nbsp;&nbsp;&nbsp;    	
    	&nbsp;&nbsp;&nbsp;&nbsp;<%= RequerimentoId.getNomStatusRequerimento() %>
    	</strong> 	</td>
    </tr>
    <tr><td height="4" colspan="4" style="background-color: #ffffff"></td></tr>
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>
    <tr bgcolor="#d9f4ef">
	    <td width="180" height="12"><strong>Data:</strong>&nbsp;
		    <input type="text" name="datJU" size="12" maxlength="10" value='<%=RequerimentoId.getDatJU() %>'        onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
	    <td align="right"><strong>Junta/Relator:&nbsp;</strong></td>
    	<td width="340" align="left">
    		<jsp:getProperty name="RelatorId" property="popupString" />
    	</td>
      	<td>
	    	<button type=button style="height: 21px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('DefineRelator',this.form);">			  
		  	<img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19">
			</button>
	  	</td>
	</tr>	     
	<% } %>
  </table>
</div>
<% if("S".equals(ProcessoBeanId.getEventoOK())) { %> 
	<script>document.InformaResForm.datJU.focus();</script>	        
<% } %>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro     = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop  = "140 px" ;
String msgErroLeft = "80 px" ;
String mostraMsg   = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</body>
</html>
