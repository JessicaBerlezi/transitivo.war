<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<jsp:useBean id="AtaDOBeanId"         scope="session" class="PNT.AtaDOBean"/> 
<jsp:useBean id="ParamSistemaBeanId"  scope="session" class="ACSS.ParamSistemaBean" /> 


<SCRIPT LANGUAGE="JavaScript1.2">
window.print()

</script>

<html>
<head> 

<title>Publica&ccedil&atilde;o de Ata</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">

<!--
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 13px;	
}
body {margin-left: 60px}
.tdcomborda td{
border-left: 1px solid #000000; border-top: 0px none; border-bottom: 1px solid #000000; border-right: 0px none;
padding: 3px; padding-left: 3px; padding-right: 3px;
font-family: Arial, Helvetica, sans-serif;
font-size: 9px; line-height: 11px; text-align: left;}
.tdcomborda td.alinhadir {text-align: right; border-right: 1px solid #000000;}
.tdcomborda td.linhatop {border-top: 1px solid #000000;}
.tdcomborda td.alinhadir2 {text-align: right;}
-->
</style>
<body>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
  	<tr><td height=24 colspan="3"></td></tr> 
  	<tr>
	  <td width="185"></td>
  	  <td width="565" align="center"><strong><span class="style3"><%=ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_01")%></strong></span></td>
	  <td width="200"></td>
  	</tr>	
	<tr>
	  <td width="185"></td>
	  <td align="center"><span class="style3"><strong><%=ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_02")%></strong></span></td>
	  <td width="200"></td>
	</tr>
	<tr>
	  <td width="185"></td>
	  <td align="center"><span class="style3"><strong><%=ParamSistemaBeanId.getParamSist("NOM_ATA_TIT_03")%></strong></span></td>
	  <td width="200"></td>
	</tr>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">

  <tr>
	  <td style="text-align: center; padding-top: 15px; padding-bottom: 15px; font-family: verdana;">
	  	<strong>MINUTA DA ATA - &Agrave; REVELIA</strong> </td>
	  <td width="4%"></td>
  </tr>


  <tr>
	  <td style="text-align: center; padding-top: 15px; font-family: verdana;">
	  	<strong>EDITAL N�<%=AtaDOBeanId.getCodEnvioDO()%>, DE <%=AtaDOBeanId.getDiaDatEdo()%> DE <%=AtaDOBeanId.getMesDatEdo()%> DE <%=AtaDOBeanId.getAnoDatEdo()%></strong>
	  </td>
	  <td width="4%"></td>
  </tr>
  <tr>
  <td style="font-family: verdana;"><strong>1.&nbsp;<%=AtaDOBeanId.getTipAtaEditalDODesc()%></strong><br /><br />
  </td>
  <td width="4%"></td>
  </tr>
  <tr>
	  <td style="font-family: verdana; font-size: 12px; text-align: justify;">
		  <%=AtaDOBeanId.getTxtParagrago_01()%>
	  </td>
	  <td width="4%"></td>
  </tr>
  <tr><td colspan="2" height="15"></td></tr>
  <tr>
	  <td style="font-family: verdana; font-size: 12px; text-align: justify;">
		  <%=AtaDOBeanId.getTxtParagrago_02()%>
	  </td>
	  <td width="4%"></td>
  </tr>
  <tr><td colspan="2" height="17"></td></tr>
 </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda tdcomborda" > 
				  <tr>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td></td>
				  </tr>
 				</table>
<%
  int contLinha=0;
  int npag=1;
  int maxLinha=4;
  if (AtaDOBeanId.getProcessos().size()>0)
  {
    int resto = AtaDOBeanId.getProcessos().size()%3;
    if (resto>0) 
    {
      for (int i=0; i<3-resto;i++){
    	  PNT.ProcessoBean processoEmBranco = new PNT.ProcessoBean();
    	  processoEmBranco.setNumCPFEdt("0");
    	  processoEmBranco.setDscPenalidadeRes("-");
    	  processoEmBranco.setCodArtigo("-");
    	  AtaDOBeanId.getProcessos().add(processoEmBranco);
	  }
    }
  }
  for (int i=0;i<AtaDOBeanId.getProcessos().size();i=i+3){
	  %>
	  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda tdcomborda" >
	  <tr>
	  <td width="10%"><%=AtaDOBeanId.getProcessos().get(i).getNumCPFEdt() %></td>
  	  <td width="4%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i).getTotPontos() %></td>
  	  <td width="5%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i).getCodArtigo() %> </td>
	  <td width="13%" class="alinhadir"><%=AtaDOBeanId.getProcessos().get(i).getDscPenalidadeRes() %></td>
	  <td width="1%"></td>
	  <td width="10%"><%=AtaDOBeanId.getProcessos().get(i+1).getNumCPFEdt() %></td>
  	  <td width="4%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i+1).getTotPontos() %></td>
  	  <td width="5%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i+1).getCodArtigo() %> </td>
	  <td width="13%" class="alinhadir"><%=AtaDOBeanId.getProcessos().get(i+1).getDscPenalidadeRes() %></td>
	  <td width="1%"></td>
	  <td width="10%"><%=AtaDOBeanId.getProcessos().get(i+2).getNumCPFEdt() %></td>
  	  <td width="4%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i+2).getTotPontos() %></td>
  	  <td width="5%" class="alinhadir2"><%=AtaDOBeanId.getProcessos().get(i+2).getCodArtigo() %> </td>
	  <td width="13%" class="alinhadir"><%=AtaDOBeanId.getProcessos().get(i+2).getDscPenalidadeRes() %></td>
	  <td width="1%"></td>
	  <td></td>
  	  </tr>
  	<%
  	  			contLinha++;
				//Faz quebra de linha a cada 8 linhas, exceto na primeira linha.
				if(npag>1)
					maxLinha=6;
				
				if (contLinha>maxLinha ){
				    contLinha=0;					
					%>					
				 </table>  	
				 
				 <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">   
  				 	<tr>
				  	  <td style="text-align:right;" class="style3"><br> 
  						</td>
  					  <td width="4%"></td>
  					</tr>
  				 </table>			
						<div class="quebrapagina"></div>	
						
						<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda" style="margin-top: 70px">   
							<tr><td colspan="2"></td></tr> 
							<tr>
								<td style="font-family: verdana;font-size: 10px;"><strong>1.&nbsp;<%=AtaDOBeanId.getTipAtaEditalDODesc()%>&nbsp;&nbsp; (Continua��o)</strong><br /><br />								
								</td>
								<td width="4%"></td>
							</tr>
						</table>			
						
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda tdcomborda"> 
				  <tr><td colspan="11" style="border-top: 1px solid #000000"></td></tr> 
				  <tr>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td width="10%" bgcolor="#cccccc" class="linhatop"><strong>CPF</strong></td>
					  <td width="4%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Pts</strong></td> 
					  <td width="5%" bgcolor="#cccccc" class="alinhadir2 linhatop"><strong>Artigo</strong></td>
					  <td width="13%" bgcolor="#cccccc" class="alinhadir linhatop"><strong>Penalidade</strong></td> 
					  <td width="1%"></td>
					  <td></td>
				  </tr>
 				</table>		
		       <% npag++;	} %>	
	  <%
  }
  %>
  </table>
<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">   
  <tr>
  <td style="text-align:center;" class="style3" width="85%">
  <br />
  <br />
  Rio de Janeiro, <%=AtaDOBeanId.getDiaDatEdo()%> DE <%=AtaDOBeanId.getMesDatEdo()%> DE <%=AtaDOBeanId.getAnoDatEdo()%>.</td>
  <td></td>
  <td width="4%"></td>
  </tr>
  <tr><td colspan="2" style="text-align:right;" class="style3"><br> 
  						P�gina:  <%=npag %>
  </td>
  <td width="4%"></td>
  </tr>
  </table>

<!--fim cab impress�o-->
</body>
</html>