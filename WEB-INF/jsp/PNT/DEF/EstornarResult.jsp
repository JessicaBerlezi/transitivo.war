<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="RequerimentoId" scope="request" class="PNT.RequerimentoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	   case 'EstornarResult':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datEST,1) && valid
	verString(fForm.datEST,"Data do Estorno",0);	
	if (valid==false) alert(sErro) 
	return valid ;
}


</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden; ">
<form name="InformaResForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
		
<input name="acao"             type="hidden" value=''>		
<input name="codRequerimento"  type="hidden" value="<%= RequerimentoId.getCodRequerimento() %>">	
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
       	<td height="18" colspan="4"><strong><%= ProcessoBeanId.getMsgOk() %></strong></td>
      	<td>
    		<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('N',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>            
        </td>
   	</tr>    
    <tr><td height="4" colspan="5"></td></tr>	
    <tr bgcolor="#d9f4ef"><td height="18" colspan="5"><strong>
    	Requerimento: &nbsp;<%= RequerimentoId.getNumRequerimento() %>&nbsp;(<%= RequerimentoId.getCodTipoSolic() %>)&nbsp;&nbsp;&nbsp;&nbsp;
    	Data: &nbsp;<%= RequerimentoId.getDatRequerimento() %>&nbsp;&nbsp;&nbsp;&nbsp;    	
    	&nbsp;&nbsp;&nbsp;&nbsp;<%= RequerimentoId.getNomStatusRequerimento() %>
    	</strong> 	</td>
    </tr>
    <tr><td height="4" colspan="5"></td></tr>
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>
    <tr bgcolor="#d9f4ef">
	    <td  style="text-align: right" width="70" height="12"><strong>Data:</strong>&nbsp;	</td>
	    <td  width="60">
	     <input type="text" readonly name="datEST" size="12" maxlength="10" value='<%=RequerimentoId.getDatEST() %>' onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
		<td  width="230" valign="middle">
			<strong>Resultado:</strong>&nbsp;&nbsp;&nbsp;<%=RequerimentoId.getNomResultRS() %>
		</td>
	    <td  width="80" align="right">
     		<input disabled name="nomRelator" type="text" size="42" value="(<%= RequerimentoId.getSigJuntaJU() %>)&nbsp;<%= RequerimentoId.getNomRelatorJU() %>">		
    	</td>
    	<td></td>
	</tr>	  
	<tr><td colspan="5" height=4></td></tr>      	
	</tr>	     
    <tr bgcolor="#D9F4EF">
        <td style="text-align: right"><strong>Motivo:</strong>&nbsp;</td>
        <td style="text-align: left;" colspan=3>
        	<textarea rows="2" name="txtMotivoRS" cols="105" onFocus="javascript:this.select();" onkeypress="javascript:MaxTextarea(this,100);">
        	<%=RequerimentoId.getTxtMotivoRS() %>
        	</textarea>
        </td>
		<td >
			<button type="button"  name="okay" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('EstornarResult',this.form);">	
    	      <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
        	</button>
        	&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>	   
    <tr><td height="4" colspan="5"></td></tr>    
      <tr bgcolor="#D9F4EF">
		<td style="text-align: right"><strong>Penalidade:</strong>&nbsp;</td>
        <td colspan=3><%= ProcessoBeanId.getDscPenalidade() %>
        </td>
       	<td>&nbsp;</td>
      </tr>	         
    <tr bgcolor="#D9F4EF"><td height="4" colspan="5"></td></tr>      	
	<% } %>
  </table>
</div>
<% if("S".equals(ProcessoBeanId.getEventoOK())) { %> 
	<script>document.InformaResForm.datEST.focus();</script>	        
<% } %>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro     = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop  = "140 px" ;
String msgErroLeft = "80 px" ;
String mostraMsg   = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</body>
</html>
