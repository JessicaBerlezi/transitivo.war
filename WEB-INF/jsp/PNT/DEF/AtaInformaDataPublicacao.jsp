<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath();
StringBuilder html = new StringBuilder();	 %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>
<%@ page import="java.util.Iterator"%>
<%@ page import="sys.Util" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="PNT.AtaDOBean" %>


<jsp:useBean id="AtaDOBeanId"   scope="session" class="PNT.AtaDOBean" /> 
<%
List listaAtas = (List) request.getAttribute("listaAtas");	
if(listaAtas==null) 	listaAtas= new ArrayList();

List<AtaDOBean> listaAtasList = new ArrayList<AtaDOBean>();
String CodAtaDOSel = "";
%>

<html>
<head>
<%@ include file="../Css.jsp" %>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'InformaDataPublicacao':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'R':  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.dataPublicacao,"Data de Publica��o",0);
    if (comparaDatas(fForm.dataPublicacao.value)==true){
       valid = false
       sErro += "Data de Publica��o do D.O. n�o pode ser posterior a data atual. \n"
    }
    if (valid==false) 
      alert(sErro) ;
	return valid ;
}

function comparaDatas(data2) {
	var valid = false;
	var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var today=new Date();
	var difference=today-dataB;//obt�m o resultado em milisegundos.
	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference < 0) valid = true;
	return valid;
}  


</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AtaForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao" type="hidden" value=' '>	
<input name="confirmaExclusao" type="hidden" value="N">	
<div id="WK_SISTEMA" style="position:absolute; width:470px; overflow: visible; z-index: 1; top: 100px; left:200px;" > 
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
	  <tr><td valign="top" colspan=2 height="10"></td></tr>	
	  <tr>
		  <td width="90"><b>Data Publica��o :</b></td>
		  <td>
		    &nbsp;&nbsp;&nbsp;<input name="dataPublicacao" style="margin-left: 1px;" type="text" id="De" size="12" maxlength="12" value="<%=sys.Util.addData(sys.Util.formatedToday().substring(0, 10),-60)%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0)" onfocus="javascript:this.select();">
      </tr>
	  <tr>
        <td colspan="2" height="5" ></td>
      </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
      <tr>
        <td width="90"><b>Atas :</b></td>
        <td align="left">
        <select id="comboAtas" name="codEnvioDO" >
        	<%
        		
        	if (listaAtas.size()>0){  			
				for (int t=0;t<listaAtas.size();t++) {
					PNT.AtaDOBean myAtas = (PNT.AtaDOBean) listaAtas.get(t);%>								
					<script>
						document.write("<option value='<%=myAtas.getCodEnvioDO()%>'>ATA:  <%=myAtas.getCodEnvioDO()%>     Enviada: <%=myAtas.getDatEDO()%>    Emitida: <%=myAtas.getDatEmissaoEDO()%>     Processos:  <%=myAtas.getQtdProc()%></option>");
					</script>
				<%}
			}%>
		</select>		
        </td>
        <td width="75" align="right" ><button type="button" NAME="Ok" style="width: 29px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('InformaDataPublicacao',this.form);"><IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19" alt="Informar Data de Publica��o"></button></td>
      </tr>
      <tr>
        <td colspan="3" height="5" ></td>
      </tr>
   </table>

</div>

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<%  	
String msgErro     = AtaDOBeanId.getMsgErro();
String msgOk       = AtaDOBeanId.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</body>
</HTML>


