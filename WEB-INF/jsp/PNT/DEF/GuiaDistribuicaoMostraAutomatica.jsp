<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>
<%@ page import = "java.util.*"%>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'ImprimirGuia':
			fForm.acao.value = opcao;
		   	fForm.target     = "_blank";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;
	   case 'R':
			fForm.acao.value ="R";
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'Novo':  // Esconder os erro
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
		  
  }
}
function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["PreparaGuiaForm"].target = "_self";
	document.all["PreparaGuiaForm"].action = "acessoTool";  
	document.all["PreparaGuiaForm"].submit() ;	 
}
function Mostra(opcoes,fForm,numprocesso) {	
	document.all["acao"].value          = opcoes;
	document.all["mostranumprocesso"].value = numprocesso;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}
function SomaSelec() {
}
function marcaTodos(obj) {
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="PreparaGuiaForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao"           type="hidden" value=' '>
<input name="mostranumprocesso"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=2 height=3></td></tr>
     <tr bgcolor="#d9f4ef">
        <td height="23" align="center"><b>N&ordm;&nbsp;DE GUIA DE DISTRIBUI��O GERADAS&nbsp;
		&nbsp;&nbsp;-&nbsp;&nbsp;<%=GuiaDistribuicaoId.getRelatGuias().size()%>        </td>		     
        <td width="10%" align="center" valign="middle">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimirGuia',this.form)"> 
              <img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">
			</button>
		</td>	
		<!--  
		<td align=center  width="20%">
		  	<input type="checkbox" name="todos" value="S" class="input" onClick="javascript: marcaTodos(this);" ><b>Autos Digitalizados</b>
		  </td>
		  -->	
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
<%
	  if (GuiaDistribuicaoId.getRelatGuias().size()>0) {
		Iterator it = GuiaDistribuicaoId.getRelatGuias().iterator() ;
		PNT.DEF.GuiaDistribuicaoBean myGuia  = new PNT.DEF.GuiaDistribuicaoBean();		
		while (it.hasNext()) {
			myGuia = (PNT.DEF.GuiaDistribuicaoBean)it.next() ;				
%>
     <tr bgcolor="#d9f4ef">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" >
	     	<tr>
		        <td width="16%" height=20>&nbsp;Junta:&nbsp;<b><%=myGuia.getSigJunta() %></b></td>
		        <td width="33%" align=left>Relator:&nbsp;<b><%=myGuia.getNomRelator() %></b>&nbsp;</td> 
		        <td width="51%" align=left>Guia:&nbsp;<b><%=myGuia.getNumGuia()%></b>&nbsp;</td>         
			</tr>
			</table>			
		</td>
	</tr>
    <tr><td colspan=3 height=5></td></tr>     
<%    }
     }   %>  
  </table> 
</div>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->

<%
String msgErro= GuiaDistribuicaoId.getMsgErro();
String msgOk= GuiaDistribuicaoId.getMsgOk();
String msgErroTop=  "160 px";
String msgErroLeft= "80 px";
String mostraMsg     = "hidden";
%>	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

<!--FIM_Div Erros-->
</form>
</body>
</html>


