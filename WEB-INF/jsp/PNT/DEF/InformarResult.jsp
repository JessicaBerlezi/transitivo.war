<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="ProcessoBeanId" scope="session" class="PNT.ProcessoBean" /> 
<jsp:useBean id="RequerimentoId" scope="request" class="PNT.RequerimentoBean" /> 

<jsp:useBean id="PenalidadeId"  scope="request" class="PNT.TAB.PenalidadeBean" /> 
<jsp:setProperty name="PenalidadeId" property="j_abrevSist" value="PNT.TAB" />  
<jsp:setProperty name="PenalidadeId" property="colunaValue" value="COD_PENALIDADE" />
<jsp:setProperty name="PenalidadeId" property="popupNome"   value="COD_PENALIDADE"  />
<jsp:setProperty name="PenalidadeId" property="popupExt"    value="onChange=\"javascript: valida('LePenal',this.form);\"" />                 	              	 
<%
   String r = ProcessoBeanId.getCodPenalidade();
   PenalidadeId.setChecked(r);
   PenalidadeId.setPopupString("DSC_PENALIDADE,SELECT COD_PENALIDADE,DSC_PENALIDADE "+
     "FROM TPNT_PENALIDADE ORDER BY DSC_PENALIDADE");
%>   

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();	   
			break;
	   case 'InformaResult':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'N':  
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	valid = ValDt(fForm.datRS,1) && valid
	verString(fForm.datRS,"Data do Resultado",0);	
	if (atuRadio(fForm.codResultRSradio,fForm.codResultRS)) {
	  valid = false
	  sErro = sErro + "Resultado n�o selecionado. \n"
	}
	if (fForm.codResultRS.value == 'I'){
	    verString(fForm.COD_PENALIDADE,"Penalidade",0);
	}
	if (valid==false) alert(sErro) 
	return valid ;
}

function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}


</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden; ">
<form name="InformaResForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
		
<input name="acao"             type="hidden" value=''>		
<input name="codRequerimento"  type="hidden" value="<%= RequerimentoId.getCodRequerimento() %>">	
<%@ include file="../ProcessoApresentaTop.jsp" %> 
<%@ include file="../ProcessoApresenta.jsp" %>
			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:235px; right: 15px; height:45px; z-index:100; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="apresenta">
    <tr bgcolor="#D9F4EF">
       	<td height="18" colspan="4"><strong><%= ProcessoBeanId.getMsgOk() %></strong></td>
      	<td>
    		<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="Limparprocesso"  onClick="javascript: valida('N',this.form);">	
			    <IMG src="<%= path %>/images/PNT/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>            
        </td>
   	</tr>    
    <tr><td height="4" colspan="5"></td></tr>	
    <tr bgcolor="#d9f4ef"><td height="18" colspan="5"><strong>
    	Requerimento: &nbsp;<%= RequerimentoId.getNumRequerimento() %>&nbsp;(<%= RequerimentoId.getCodTipoSolic() %>)&nbsp;&nbsp;&nbsp;&nbsp;
    	Data: &nbsp;<%= RequerimentoId.getDatRequerimento() %>&nbsp;&nbsp;&nbsp;&nbsp;    	
    	&nbsp;&nbsp;&nbsp;&nbsp;<%= RequerimentoId.getNomStatusRequerimento() %>
    	</strong> 	</td>
    </tr>
    <tr><td height="4" colspan="5"></td></tr>
   	<% if("S".equals(ProcessoBeanId.getEventoOK())) { %>
    <tr bgcolor="#d9f4ef">
	    <td  align="right" width="70" height="12"><strong>Data:</strong></td>
	    <td  width="60">
		    <input type="text" name="datRS" size="12" maxlength="10" value='<%=RequerimentoId.getDatRS() %>'        onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"  onkeypress="javascript:Mascaras(this,'99/99/9999');">	
		</td>
		<td  width="230" valign="middle"><strong>Resultado:</strong>&nbsp;&nbsp;&nbsp; 
          <input type="radio" name="codResultRSradio" value="D" <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">Deferido 
          <input type="radio" name="codResultRSradio" value="I" <%= sys.Util.isChecked(RequerimentoId.getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">Indeferido&nbsp;&nbsp;
		  <input name="codResultRS" type="hidden" value="">
		</td>
	    <td  width="120" align="right">
     		<input disabled name="nomRelator" type="text" size="42" value="(<%= RequerimentoId.getSigJuntaJU() %>)&nbsp;<%= RequerimentoId.getNomRelatorJU() %>)">		
    	</td>
    	<td>&nbsp;</td>
	</tr>	  
	<tr><td colspan="5" height=4></td></tr>      	
	</tr>	     
    <tr bgcolor="#D9F4EF">
        <td style="text-align: right;"><strong>Motivo:</strong></td>
        <td style="text-align: left;" colspan=3>
        	<textarea rows="2" name="txtMotivoRS" cols="82" onFocus="javascript:this.select();" onkeypress="javascript:MaxTextarea(this,1500);">
        	<%=RequerimentoId.getTxtMotivoRS() %>
        	</textarea>        
        </td>
		<td >
			<button type="button"  name="okay" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('InformaResult',this.form);">	
    	      <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
        	</button>
        	&nbsp;&nbsp;&nbsp;&nbsp;
        </td>
    </tr>	  
    <tr><td height="4" colspan="5"></td></tr>
     <tr bgcolor="#D9F4EF">
		<td style="text-align: right"><strong>Penalidade:</strong></td>
        <td colspan=3>
			<jsp:getProperty name="PenalidadeId" property="popupString" />
        </td>
       	<td>&nbsp;</td>
      </tr>	         	
    <tr bgcolor="#D9F4EF"><td height="4" colspan="5"></td></tr>
	<% } %>
  </table>
</div>
<% if("S".equals(ProcessoBeanId.getEventoOK())) { %> 
	<script>document.InformaResForm.datRS.focus();</script>	        
<% } %>

<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<%  	
String msgErro     = ProcessoBeanId.getMsgErro();
String msgOk       = ProcessoBeanId.getMsgOk();
String msgErroTop  = "140 px" ;
String msgErroLeft = "80 px" ;
String mostraMsg   = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</body>
</html>
