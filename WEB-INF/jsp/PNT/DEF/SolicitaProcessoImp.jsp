<%@page import="PNT.ProcessoBean"%>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>
<jsp:useBean id="ParamOrgBeanId" scope ="session" class = "REC.ParamOrgBean"/>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="../CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="GuiaPreparaImp" method="post" action="">	 
<%
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	String	cor = "#ffffff";	
	Iterator ot = GuiaDistribuicaoId.getProcessos().iterator() ;
	ProcessoBean myProcesso  = new ProcessoBean();		
	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		while (ot.hasNext()) {
			myProcesso = (ProcessoBean)ot.next() ;
			cor    = "#ffffff";
			if (myProcesso.getMsgErro().length()>0)  {
				cor = "#CCCCCC"; 
			}		
			seq++;
			contLinha++;
			//Faz quebra de linha a cada 13 linhas, exceto na primeira linha.
			if (contLinha>13){
			    contLinha=1;
				npag++;			
				if (npag!=1){			
		%>
						</table>      
					<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
			<% } %>				
	<jsp:include page="SolicitaProcessoCabImp.jsp" flush="true" >
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
    
  <table id="autosImp" width="100%" border="0" cellpadding="1" cellspacing="1" >
        <% } %>
    <tr bgcolor='<%=cor%>'> 
      <td width="10%" align="center" bgcolor="#FFFFFF"></td> 
      <td width="40%" align="center" ><%=myProcesso.getNumProcesso() %></td>
      <td width="10%"  colspan="3" align="center" bgcolor="#FFFFFF"></td> 
    </tr>
	<% if (myProcesso.getMsgErro().length()>0) { %>
		<tr bgcolor='<%=cor%>' >
	        <td colspan=8>&nbsp;<%=myProcesso.getMsgErro() %> </td>
		</tr>			
	<% } %>	    
    <tr>
       <td width="10%" align="center" bgcolor="#FFFFFF"></td> 
       <td width="80%" height=2 colspan=2 bgcolor="#666666"></td>
       <td width="10%"  colspan="3" align="center" bgcolor="#FFFFFF"></td>
    </tr>
   <%} %>
  </table>      
<%} else { 
	String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="SolicitaProcessoCabImp.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
	<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
<%} %>
<%	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		if (contLinha<13){

		} %>
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr> 
	   	    <td height="35"><strong>TOTAL DE PROCESSOS:</strong>&nbsp;&nbsp;<%= GuiaDistribuicaoId.getProcessos().size() %></td>
		</tr>	
	</table>      			

		<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
<%} %>

<div class="quebrapagina"></div>
</form>
</body>
</html>