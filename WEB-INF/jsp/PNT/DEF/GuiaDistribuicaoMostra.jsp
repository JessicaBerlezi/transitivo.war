<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="funcionalidadesBean"   scope="session" class="PNT.DEF.FuncionalidadesBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="../Css.jsp" %>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	 	case 'RemoveProcesso':
			fForm.acao.value = opcao;
			fForm.target     = "_self";
		 	fForm.action     = "acessoTool";  
		 	fForm.submit();
		  break;
	   case 'ImprimirGuia':
			fForm.acao.value = opcao;
		   	fForm.target     = "_blank";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;
	   case 'R':
			fForm.acao.value ="R";
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
			break;		  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'Novo':  // Esconder os erro
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;  
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
		  
  }
}
function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["PreparaGuiaForm"].target = "_self";
	document.all["PreparaGuiaForm"].action = "acessoTool";  
	document.all["PreparaGuiaForm"].submit() ;	 
}
function Mostra(opcoes,fForm,numprocesso) {	
	document.all["acao"].value          = opcoes;
	document.all["processoMostra"].value = numprocesso;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}
function SomaSelec() {
}
function marcaTodos(obj) {
}
</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="PreparaGuiaForm" method="post" action="">
<!-- %@ include file="Cab_Diretiva.jsp" %>-->
<%@ include file="../Cab.jsp" %>

<input name="acao"           type="hidden" value=' '>
<input name="processoMostra"  type="hidden" value=" ">
<input name="ordem"          type="hidden" value="Data">			
<!--IN�CIO_CORPO_sistema--> 
<div id="recurso3" style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=2 height=3></td></tr>
     <tr bgcolor="#8AAEAE">
        <td width="55%" height="23">&nbsp;&nbsp;<b>GUIA DE DISTRIBUI��O&nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=GuiaDistribuicaoId.getNumGuia()%> &nbsp;&nbsp;-&nbsp;&nbsp;<%=GuiaDistribuicaoId.getDatProc()%>
	      <% 	if (GuiaDistribuicaoId.getCodEnvioDO().length()>0) { %>
&nbsp;&nbsp;-&nbsp;&nbsp;Publica��o CI:&nbsp;<%= GuiaDistribuicaoId.getCodEnvioDO() %>&nbsp;
	<% 	} 
		if (GuiaDistribuicaoId.getDatEnvioDO().length()>0) {%>
	Enviado:&nbsp;<%= GuiaDistribuicaoId.getDatEnvioDO()%>
	<% 	} 
		if (GuiaDistribuicaoId.getDatPubliPDO().length()>0) { %>
&nbsp;&nbsp;Publicado:&nbsp;<%= GuiaDistribuicaoId.getDatPubliPDO()%>
	 <% } %>	
	  <%if(GuiaDistribuicaoId.getIndSessao().equals("S")){%>
     <br>N� Sess&atilde;o:&nbsp;<%=GuiaDistribuicaoId.getNumSessao()%> &nbsp;&nbsp;-&nbsp;&nbsp;Data Sess&atilde;o:&nbsp;<%=GuiaDistribuicaoId.getDatSessao()%>
   	  <%}%>       
	    </td>		
	  <%if(GuiaDistribuicaoId.getIndSessao().equals("S")&& GuiaDistribuicaoId.getTipoReqValidos().equals("DP,DC")
			  && GuiaDistribuicaoId.getCodStatus().equals("3") ){%>
		          <td width="20%" align="left"><div align="right"><strong>Layout :</strong>
              <input name="layout" type="radio" value="ata" class="input" style="height: 13px" checked>
            Ata
            <input name="layout" type="radio" value="guia" class="input" style="height: 13px">
            Guia</div></td>

   	  <%}%>   		    
        <td width="6%" align="center" valign="middle">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimirGuia',this.form)"> 
              	<img src="<%= path %>/images/PNT/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">
			</button>
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button"  onClick ="javascript: valida('RemoveProcesso', this.form)">
				<img src="<%= path %>/images/PNT/bot_ok.gif" alt="Imprimir" width="29" height="19">
			</button>
		</td>	
 	 </tr>
     <tr><td colspan=3 height=5></td></tr>
     <tr bgcolor="#8AAEAE">
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" class="titprincipal">
	     	<tr>
		        <td width="50%" height=20>&nbsp;Junta:&nbsp;<b><%=GuiaDistribuicaoId.getSigJunta() %></b></td>
		        <td width="50%" align=right>Relator:&nbsp;<b><%=GuiaDistribuicaoId.getNomRelator() %></b>&nbsp;</td>        
			</tr>
			</table>			
		</td>
		<%if(GuiaDistribuicaoId.getIndSessao().equals("S")){%>
		   <td></td>
		<%}%>
	</tr>
    <tr><td colspan=3 height=5></td></tr>     
  </table> 
</div>
<%
String posTop=  "150 px";
String posHei= "167 px";
%>
<%@ include file="GuiaCorpoSelect.jsp" %>
<!--FIM_CORPO_sistema--> 

<!-- Rodap�
%@ include file="Retornar_Diretiva.jsp" %>
%@ include file="Rod_Diretiva.jsp" %>
-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->

<%
String msgErro= GuiaDistribuicaoId.getMsgErro();
String msgOk= GuiaDistribuicaoId.getMsgOk();
String msgErroTop=  "160 px";
String msgErroLeft= "80 px";
String mostraMsg     = "hidden";
%>	
<!--  %@ include file="EventoErro_Diretiva.jsp"%>-->
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>


<!--FIM_Div Erros-->
</form>
</body>
</html>


