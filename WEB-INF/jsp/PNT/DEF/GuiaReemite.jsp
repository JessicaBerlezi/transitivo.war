<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->

<!-- Abre a Sessao -->
<%@ page session="true"%>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp"%>

<jsp:useBean id="GuiaDistribuicaoId" scope="session" class="PNT.DEF.GuiaDistribuicaoBean" />
<jsp:useBean id="FuncionalidadesId" scope="session" class="PNT.DEF.FuncionalidadesBean" />
<jsp:setProperty name="GuiaDistribuicaoId" property="j_abrevSist" value="PNT" />
<jsp:setProperty name="GuiaDistribuicaoId" property="colunaValue" value="COD_LOTE_RELATOR" />
<jsp:setProperty name="GuiaDistribuicaoId" property="popupNome"   value="numGuia" />
<html>
<head>
<%@ include file="../Css.jsp" %>


<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'LeGuia':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	    break ; 
	  
   case 'R':
	  fForm.acao.value=opcao
	  fForm.target= "_self";
	  fForm.action = "acessoTool";  
	  fForm.submit();	  		  
      close() ;   
	  break;
	  
    case 'consulta':
    if(ValidaDatas(fForm)){      
      if(vDatas(fForm)){
		  fForm.acao.value=opcao
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	
	  }  	
	}	  
	  break;	  
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.numGuia,"Guia",0);
    if (valid==false) 
      alert(sErro) ;
    else     
      vDatas(fForm);
	return valid ;
}


function ValidaDatas(fForm){
	valid = true ;
	sErro = "" ;
	if (!ValDt(fForm.De,0)){	   
	     fForm.De.value='';	
	      valid=false;	     
    }  
    if(valid)    {
	     if (!ValDt(fForm.Ate,0)){
		      valid=false ;
		      fForm.Ate.value='';		    
	     }  
    }    
	return valid ;
}

function vDatas(fForm){
    valid = true ;
	sErro = "" ;
	verString(fForm.De,"Data Inicial",0);
	verString(fForm.Ate,"Data Final",0);
	if(valid){
		if (comparaDatas(fForm.De.value,fForm.Ate.value)==false){
	       valid = false
	       sErro += "Data final n�o pode ser inferior a data inicial. \n"
		   document.all.Ate.focus();       
	    }
    }
	if (valid==false) alert(sErro) ;
	return valid ;
}
function comparaDatas(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<%@ include file="../Cab.jsp"%>
<%
	 FuncionalidadesId.setSigFuncao(request.getParameter("j_sigFuncao"));
     String cm = "SELECT L.cod_lote_relator,"+
     "' Guia: '||lpad(L.cod_lote_relator,6,'0')||'  '||rpad(j.sig_junta,10,' ')||'-'||r.nom_relator||"+
     "' Status: '||decode(L.cod_status,2,'Def. Junta/Relator',3,'Resultado',4,'Enviado p/ Publica��o',8,'Estornado','Publicado')  numGuia "+
     "FROM SMIT_PNT.TPNT_LOTE_RELATOR L,TPNT_RELATOR R,TPNT_JUNTA J ";
  
     cm += "where L.COD_JUNTA=J.COD_JUNTA  AND "+
     "L.NUM_CPF=R.NUM_CPF     AND L.COD_JUNTA=R.COD_JUNTA  ";
/*     if( !(RequisicaoBeanId.getSigFuncao().equals("REC0237")) 
    	&& !(RequisicaoBeanId.getSigFuncao().equals("REC0337"))
    	&& !(RequisicaoBeanId.getSigFuncao().equals("REC0437")))*/
           cm += "AND L.DT_PROC BETWEEN TO_DATE('"+GuiaDistribuicaoId.getDataIni()+"','DD/MM/YYYY') AND TO_DATE('"+GuiaDistribuicaoId.getDataFim()+"','DD/MM/YYYY') ";
     cm += "AND J.ind_tipo='"+FuncionalidadesId.getTipoJunta()+"'" ;
     
     // foi comentado para permitir reemiss�o de guias estornadas" AND COD_STATUS <> 8";
     /*
    if(!(RequisicaoBeanId.getSigFuncao().equals("PNT0434")) && !(RequisicaoBeanId.getSigFuncao().equals("PNT0534")))
      {    
      	cm += " AND COD_STATUS = '" + GuiaDistribuicaoId.getCodStatus() + "'";
     }
     */
    /**
    if(RequisicaoBeanId.getSigFuncao().equals("REC0286")){
         cm += "AND TIPO_GUIA = '"+ GuiaDistribuicaoId.getTipoGuia()+"'";
      }
    **/
     
     //Controla a exibicao das guias de relatores ativos e inativos
     //Deve aparecer as guias com relatores inativos apenas ao reemitir guia
     //e trocar o relator
     if( !(RequisicaoBeanId.getSigFuncao().equals("PNT0432")) 
    	&& !(RequisicaoBeanId.getSigFuncao().equals("PNT0434"))
    	&& !(RequisicaoBeanId.getSigFuncao().equals("PNT0532"))
    	&& !(RequisicaoBeanId.getSigFuncao().equals("PNT0534")) 
     ){
        cm+= " AND R.IND_ATIVO = 'S' ";
     }
    
    if ( (RequisicaoBeanId.getSigFuncao().equals("PNT0432")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0532"))   
    	|| (RequisicaoBeanId.getSigFuncao().equals("PNT0435")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0535"))
    	|| (RequisicaoBeanId.getSigFuncao().equals("PNT0442")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0542"))
    		){
    	cm+= "AND L.COD_STATUS = '2'";
    }
    /**
	*Estorno de Guia Restulado
    *
    **/
    if ( (RequisicaoBeanId.getSigFuncao().equals("PNT0443")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0543"))	){
        	cm+= "AND L.COD_STATUS = '3'";
    }
    /**
	*Estorno de Guia Envio P/ Publica��o
    *
    **/
    if ( (RequisicaoBeanId.getSigFuncao().equals("PNT0444")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0544"))	){
        	cm+= "AND L.COD_STATUS = '4'";
    }
    
    /**
	*Estorno de Guia Data de Publica��o
    *
    **/
    if ( (RequisicaoBeanId.getSigFuncao().equals("PNT0445")) || (RequisicaoBeanId.getSigFuncao().equals("PNT0545"))	){
        	cm+= "AND L.COD_STATUS = '9'";
    }
     
     /* MEDIDA DE SEGURAN�A - Verifica se h� Controle de Guia 
     if (GuiaDistribuicaoId.getControleGuia().equals("S"))
    	 cm += "AND R.COD_RELATOR = C.COD_RELATOR AND C.COD_USUARIO = "+UsuarioBeanId.getCodUsuario();
     */
     cm+= " ORDER BY NUMGUIA DESC";
	GuiaDistribuicaoId.setPopupString("numGuia,"+cm);    
%>
<input name="acao" type="hidden" value=' '>
<div id="WK_SISTEMA"
	style="position:absolute; width:740px; overflow: visible; z-index: 1; top: 100px; left:50px;">

<table border="0" cellpadding="0" cellspacing="1" width="740"
	align="center">
	<tr>
		<td valign="top" colspan=2 height="10"></td>
	</tr>
	<tr>
		<td width="271"><b>Data:&nbsp;</b>&nbsp; <input name="De" type="text"
			id="De" size="12" maxlength="12"
			value="<%=GuiaDistribuicaoId.getDataIni()%>"
			onkeypress="javascript:Mascaras(this,'99/99/9999');"
			onChange="javascript: valida('consulta',this.form);"
			onfocus="javascript:this.select();"> &nbsp;<strong>at�&nbsp;</strong>&nbsp;
		<input name="Ate" type="text" id="Ate" size="12" maxlength="12"
			value="<%=GuiaDistribuicaoId.getDataFim()%>"
			onkeypress="javascript:Mascaras(this,'99/99/9999');"
			onChange="javascript: valida('consulta',this.form);"
			onfocus="javascript:this.select();"></td>
	</tr>
</table>


<table border="0" cellpadding="0" cellspacing="1" width="740"
	align="center">
	<tr>
		<td valign="top" colspan="2" height="15"></td>
	</tr>
	<tr>
		<td align="left" width="90%"><b>Guia:&nbsp;</b> <jsp:getProperty
			name="GuiaDistribuicaoId" property="popupString" /></td>
		<td align=center width="10%">&nbsp;&nbsp;&nbsp;
		<button type="button" NAME="Ok"
			style="width: 28px; height: 21px;  border: none; background: transparent;"
			onClick="javascript: valida('LeGuia',this.form);"><IMG
			src="<%= path %>/images/bot_ok.gif" align="left" width="26"
			height="19"></button>
		</td>
	</tr>
	<tr>
		<td valign="top" colspan=2 height="2"></td>
	</tr>
</table>
</div>
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
String mostraMsg   = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp"  %> 


</form>
</BODY>
</HTML>


