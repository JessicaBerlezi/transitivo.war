<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->
<%@ page session="true"%>
<%String path = request.getContextPath();

            %>
<%@ page errorPage="../ErrorPage.jsp"%>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />

<jsp:useBean id="FuncionalidadeBeanId" scope="session" class="PNT.DEF.FuncionalidadesBean" />

<jsp:useBean id="ProcessoManualBeanId" scope="request"
	class="PNT.ProcessoBean" />
<jsp:useBean id="GuiaIncluirProcessoBeanId" scope="request"
	class="PNT.DEF.GuiaIncluirProcessoBean" />
	
	
	
	
	
	
<%String codReq = "";

            if ((String) session.getAttribute("codReq") != null)
                codReq = (String) session.getAttribute("codReq");

            GuiaIncluirProcessoBeanId.setComboReqsManual(ProcessoManualBeanId, codReq, FuncionalidadeBeanId.getTipoRequerimento());

            %>

<jsp:useBean id="GuiaDistribuicaoId" scope="session"
	class="PNT.DEF.GuiaDistribuicaoBean" />

<jsp:useBean id="RelatorId" scope="request" class="PNT.TAB.RelatorBean" />
<jsp:setProperty name="RelatorId" property="j_abrevSist" value="PNT" />
<jsp:setProperty name="RelatorId" property="colunaValue"
	value="cod_Relator_Junta" />
<jsp:setProperty name="RelatorId" property="popupNome" 	value="cod_Relator_Junta" />
<%RelatorId.setPopupString("nom_Relator_Junta,SELECT LPAD(j.cod_junta,6,0)||'-'||lpad(r.num_cpf,11,'0') cod_Relator_Junta,"
                            + "rpad(j.sig_junta,10,' ')||'-'||r.nom_relator nom_Relator_Junta "
                            + "FROM TPNT_RELATOR r,TPNT_JUNTA j "
                            + "where r.cod_junta=j.cod_junta and j.ind_tipo='"
                            + FuncionalidadeBeanId.getTipoJunta()
                            + "' and "
                            +"r.ind_ativo = 'S' "
                            + " order by nom_relator_junta");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="../Css.jsp" flush="true" />


<title>DETRAN &#8226; Sistema de Monitoramento de
Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	if ("Top,Proximo,Anterior,Fim".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
		fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	    return ;
	 }
	 switch (opcao) {   	  
	   case 'R':
			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
	    	close() ;
			break;
	   case 'processoLe':
	      if (veCamposRequerimento(fForm)==true) 
	      {		  	   
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		   	fForm.action = "acessoTool";  
		   	fForm.submit();
	   	  }
		  break;
	   case 'LeReq':
			fForm.acao.value=opcao;
		   	fForm.target= "_self";
		   	fForm.action = "acessoTool";  
		   	fForm.submit();
		   	break;
	   case 'AtualizaGuia':
	      if (veCampos(fForm)==true) {		  
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  } 		  
		  break;
		 
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].display='none'; 
	      else                 document.all["MsgErro"].style.display='none'; 
		  break;  
	   case 'Novo':  // Esconder os erro
			fForm.acao.value = opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	 
		  break;  
  }
}

function veCamposRequerimento(fForm) {
	a=fForm.cod_Relator_Junta.value
	if (a.length<18) {
		a="000000000000000000"+a	
		a= a.substring((a.length-18),a.length)
	}
	fForm.codJuntaJU.value = a.substring(0,6) ;
	fForm.codRelatorJU.value = a.substring(7,18) ;	
	valid = true ;
	sErro = "" ;
	verString(fForm.numProcesso,"N�Processo",0);
	

	
	if (valid==false) alert(sErro) ;
	return valid ;
}


function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	verString(fForm.cod_Relator_Junta,"Junta/Relator",0);
	a=fForm.cod_Relator_Junta.value
	if (a.length<18) {
		a="000000000000000000"+a	
		a= a.substring((a.length-18),a.length)
	}
	fForm.codJuntaJU.value = a.substring(0,6) ;
	fForm.codRelatorJU.value = a.substring(7,18) ;	
	<%if(GuiaDistribuicaoId.getIndSessao().equals("S")){%>
      verString(fForm.numSessao,"N� Sess�o",0);
   <%}%>
	if (valid==false) alert(sErro) ;
	return valid ;
}
function MostraProcesso(opcoes,fForm,numProcesso) {	
	document.all["acao"].value          = opcoes;
	document.all["mostranumProcesso"].value = numProcesso;
   	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}

function Classificacao(ordem,fForm)  {
	document.all["acao"].value  = "Classifica";
	document.all["ordem"].value = ordem;
	document.all["PreparaGuiaForm"].target = "_self";
	document.all["PreparaGuiaForm"].action = "acessoTool";  
	document.all["PreparaGuiaForm"].submit() ;	 
}

function Mostra(opcoes,fForm,numprocesso) {

	document.all["acao"].value          = opcoes;
	document.all["processoMostra"].value   = numprocesso;
	document.all["PreparaGuiaForm"].target = "_blank";
    document.all["PreparaGuiaForm"].action = "acessoTool";  
   	document.all["PreparaGuiaForm"].submit() ;	 
}
function SomaSelec() {
	n = 0 ;
	for (i=0;i<document.PreparaGuiaForm.Selecionado.length;i++) {
		if (document.PreparaGuiaForm.Selecionado[i].checked) n++;
	}
	document.all["numSelec"].value = n;
}

function marcaTodos(obj) {
	n=0
	for (i=0; i<document.all["PreparaGuiaForm"].Selecionado.length;i++) {
		document.all["PreparaGuiaForm"].Selecionado[i].checked = obj.checked ;
		if (document.all["PreparaGuiaForm"].Selecionado[i].checked) n++;
	}
	document.all["numSelec"].value = n;
}

</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;" onload="SomaSelec();">
<form name="PreparaGuiaForm" method="post" action=""><jsp:include
	page="../Cab.jsp" flush="true" /> <input name="acao" type="hidden"
	value=' '> <input name="codRelatorJU" type="hidden"
	value="<%=GuiaDistribuicaoId.getNumCPFRelator()%>"> <input
	name="codJuntaJU" type="hidden"
	value="<%=GuiaDistribuicaoId.getCodJunta()%>"> <input
	name="processoMostra" type="hidden" value=" "> <input
	type="hidden" value=" "> <input name="ordem" type="hidden" value="Data">
<!--IN�CIO_CORPO_sistema-->
<div id="recurso3"
	style="position:absolute; left:50px; top:78px; width:720px; height:20px; z-index:1; overflow: visible; visibility: visible;">
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr>
		<td colspan=7 height=8></td>
	</tr>
	<tr bgcolor="#8AAEAE">
		<td width="74" height=23>&nbsp;Relator:</td>
		<td width="132"><jsp:getProperty name="RelatorId"
			property="popupString" /></td>
		<%if (GuiaDistribuicaoId.getIndSessao().equals("S")) {%>
		<td width="178" align="left">&nbsp;N� da Sess�o: <input type="text"
			name="numSessao" size="8" maxlength="6"
			onkeypress="javascript:f_num();"
			value='<%=GuiaDistribuicaoId.getNumSessao()%>'
			onfocus="javascript:this.select();"></td>
		<%} else {%>
		<td width="288" align="center" valign="middle"><%}%></td>
		<td width="33" align="left" valign="middle">
		<button type="button"
			style="height: 23px; width: 33px; border: none; background: transparent;"
			onClick="javascript: valida('AtualizaGuia',this.form);"><img
			src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"></button>
		</td>
		<td width="11"></td>
		<td width="4"></td>
	</tr>

	<tr>
		<td colspan=7 height=8></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0"
	bgcolor="#FFFFFF" class="semborda">
	<tr bgcolor="#8AAEAE">
		<td width="74" height=23>&nbsp;Processo:</td>
		<td width="208" height="23"><input name="numProcesso" type="text"
			value="<%=ProcessoManualBeanId.getNumProcesso()%>" size="26"
			maxlength="20" onfocus="javascript:this.select();"
			onKeyUp="this.value=this.value.toUpperCase()">
		<button type="button"
			style="height: 21px; width: 64px; border: none; background: transparent;"
			onClick="javascript: valida('processoLe',this.form);"><img
			src="<%= path %>/images/REC/bot_consulta_ico.gif" align="left"></button>
		</td>
		<td width="109" height="23" align="left" valign="middle">
		���Requerimento:&nbsp;</td>
		<%if (GuiaIncluirProcessoBeanId.getComboReqs().length() > 0) {

                %>
		<td align="left" valign="top" colspan="2"><%=GuiaIncluirProcessoBeanId.getComboReqs()%>
		</td>
		<%}

            %>

		<td width="4"></td>
	</tr>
	<tr>
		<td colspan=6 height=8></td>
	</tr>
	<%if (!GuiaIncluirProcessoBeanId.getMsg().equals("")) {%>
	<tr bgcolor="#8AAEAE">
		<td colspan=6 height=20 align="left" valign="top"><%=GuiaIncluirProcessoBeanId.getMsg()%>
		</td>
	</tr>
	<tr>
		<td colspan=6 height=8></td>
	</tr>
	<%}%>

</table>
<jsp:include page="GuiaTopManual.jsp" flush="true" /></div>
<jsp:include page="GuiaPreparaCorpoManual.jsp" flush="true">
	<jsp:param name="posTop" value="200 px" />
	<jsp:param name="posHei" value="175 px" />
</jsp:include> <!-- Rodap�--> 
<jsp:include page="Retornar.jsp" flush="true" /> 
<jsp:include page="Rod.jsp" flush="true" /> <!-- Fim Rodap� -->
<!--DIV ERROS--> 
<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgErro();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
String mostraMsg = "hidden";
%>
<!-- @ include file="EventoErro_Diretiva.jsp"  %>--> 
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
<!--FIM_Div Erros-->
 <script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= GuiaDistribuicaoId.getCodJunta()+"-"+GuiaDistribuicaoId.getNumCPFRelator()%>'
if (nj.length>0)
{
	for(i=0;i<document.PreparaGuiaForm.cod_Relator_Junta.length;i++)
	{
		if(document.PreparaGuiaForm.cod_Relator_Junta.options[i].value==nj)
		{
			document.PreparaGuiaForm.cod_Relator_Junta.selectedIndex=i;	  
		}
	}
}
</script>
</form>
</body>
</html>


