<!--M�dulo : PNT
	Vers�o : 1.0 - Diretiva
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% 
if(posTop==null) posTop ="170px";   
if(posHei==null) posHei ="183px";    
%>
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      
      <tr bgcolor="#8AAEAE"> 
        <td width="25" rowspan="2" align="center"><strong><font color="#FFFFFF">Seq</font></strong></td>
        <td width="65" align="center"><strong><a href="#" onClick="javascript: Classificacao('Orgao');">
			<font color="#FFFFFF">&Oacute;rg&atilde;o</font></a></strong></td>
        <td width="223"><strong><a href="#" onClick="javascript: Classificacao('Processo');">
			<font color="#FFFFFF">&nbsp;Processo</font></a></strong></td>
        <td  align="center"><strong><a href="#" onClick="javascript: Classificacao('DatProcesso');">
			<font color="#FFFFFF">Dt Proc</font></a></strong></td>    

      </tr>
      <tr bgcolor="#8AAEAE">     		
        <td colspan="2"><strong><font color="#FFFFFF">&nbsp;Requerimento - Tipo Req</font></strong></td>
        <td colspan="2"><strong><font color="#FFFFFF">&nbsp;Status Processo</font></strong></td>
      </tr>  
	</table>
    
    
    
    <table width="100%" border="0" cellpadding="1" cellspacing="1">
	  <%
	   if (GuiaDistribuicaoId.getProcessos().size()>0) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#d9f4ef" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getProcessos().iterator() ;
		PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();		
		while (it.hasNext()) {
			myProcesso = (PNT.ProcessoBean)it.next() ;				
			cor    = (seq%2==0) ? "#d9f4ef" : "#ffffff";
			if (myProcesso.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width=25 height="14" align=center rowspan="2">
	        	&nbsp;<%= (seq+1) %></td>
	        <td width=65 align=center onclick="javascript:Mostra('processoMostra',this.form,'<%=myProcesso.getNumProcesso() %>');">
	        	&nbsp;<%=myProcesso.getSigOrgao() %></td>
			<td width=223 onclick="javascript:Mostra('processoMostra',this.form,'<%=myProcesso.getNumProcesso() %>');">
				&nbsp;<%=myProcesso.getNumProcesso() %></td>
			<td  onclick="javascript:Mostra('processoMostra',this.form,'<%=myProcesso.getNumProcesso() %>');">
				&nbsp;<%=myProcesso.getDatProcesso() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' onclick 	 = "javascript:Mostra('processoMostra',this.form,'<%=myProcesso.getNumProcesso() %>');"> 
	        <td colspan="2">&nbsp;<%=myProcesso.getNumRequerimento() %>-<%=funcionalidadesBean.getTipoRequerimento() %></td>
	        <td colspan="2">&nbsp;<%=myProcesso.getCodStatus() %>-<%=myProcesso.getNomStatus() %></td>
		</tr>	
		<% if (myProcesso.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan=8>&nbsp;<%=myProcesso.getMsgErro() %> </td>
			</tr>			
		<% } %>	
    	<% 
    		Iterator itProc = myProcesso.getRequerimentos().iterator();
			PNT.RequerimentoBean myReq  = new PNT.RequerimentoBean();
			
			myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento());
			if (!"".equals(myReq.getNumRequerimento())){%>
				<tr bgcolor='<%=cor%>'>
			      <td height="25" colspan=8>
			      	<strong>&nbsp;&nbsp;&nbsp;Resultado:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      	<input disabled="disabled" name="codResultRS<%= seq %>" type="radio" value="D" <%= sys.Util.isChecked(myReq.getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">DEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			      	<input disabled name="codResultRS<%= seq %>" type="radio" value="I" <%= sys.Util.isChecked(myReq.getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;"> INDEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo:&nbsp;
			      	<input disabled="disabled" name="txtMotivoRS<%= seq %>" type="text" size="68" maxlength="1000"  value="<%= myReq.getTxtMotivo() %>"></td>
			    </tr>
			    <tr bgcolor='<%=cor%>'>
			    	<td height="25" colspan=8>
				      	<strong>&nbsp;&nbsp;&nbsp;Penalidade:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				      	<%= myReq.getDscPenalidade() %>
			      	</td>
			    </tr>
		<%}%>
    	
	    <!-- campo CodMotivoDef n�o existe no requerimento de pontua��o.
	    % if (myProcesso.getRequerimentos(0).getCodResultRS().equals("D")) { %>
	    <tr bgcolor='%=cor%>' >
             <td  colspan=7 height=20>&nbsp;&nbsp;
             	   <input type="radio" checked="checked" disabled="disabled" name="codMotivo%= seq %>" value="1" %= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
			       <input disabled="disabled" type="radio" name="codMotivo %= seq %>" value="2" %= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
			       <input disabled="disabled" type="radio" name="codMotivo %= seq %>" value="3" %= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
			   <!--<input disabled="disabled" type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros -->
			 </td>
        </tr>
       <!-- %}%> -->
		<!--  AINDA N�O TEM REMESSA NA PONTUA��O
		% if (myProcesso.getRequerimentos(0).getCodRemessa().length()>0) { %>
        <tr bgcolor='%=cor%>'>
          <td colspan="2"><strong>&nbsp;&nbsp;N&ordm;&nbsp;Guia de Remessa :</strong>&nbsp;%=myProcesso.getRequerimentos(0).getCodRemessa()%></td>        
          <td colspan=6><strong>&nbsp;&nbsp;Data Envio :</strong>&nbsp;%=myProcesso.getRequerimentos(0).getDatEnvioGuia() %></td>
        </tr>
   	    %}%>
   	    -->
		<tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>   	    
		<% seq++; }
	} else {
		 String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>  
