<%@page import="PNT.ProcessoBean"%>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>
<jsp:useBean id="ParamOrgBeanId" scope ="session" class = "REC.ParamOrgBean"/>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="funcionalidadesBean"  scope="session" class="PNT.DEF.FuncionalidadesBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="../CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="GuiaPreparaImp" method="post" action="">	 
<%
  	int contLinha=99;		
	int npag = 0;
	int seq = 0;
	int quebra=13;
	int quantLinhas=0;
	int quantLinhasProx=0;
	int linhas=0;
	int pagAux=0;	
	String	cor = "#ffffff";		
	PNT.ProcessoBean myProcesso      = new PNT.ProcessoBean();		
	PNT.ProcessoBean myProcessoProx  = new PNT.ProcessoBean();
	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		for(int i =0;i<GuiaDistribuicaoId.getProcessos().size();i++){
			myProcesso = GuiaDistribuicaoId.getProcessos(i);
			
			pagAux ++;
			if(pagAux < GuiaDistribuicaoId.getProcessos().size()){
				myProcessoProx  = GuiaDistribuicaoId.getProcessos(i+1);
				quantLinhasProx= myProcessoProx.getRequerimentos(0).getTxtMotivo().length()/102;
			}else
				quantLinhasProx=0;
					
			cor    = "#ffffff";
			if (myProcesso.getMsgErro().length()>0)  {
				cor = "#CCCCCC"; 
			}			
			seq++;
			contLinha++;
			//Faz quebra de linha a cada 13 linhas, exceto na primeira linha.
			if (contLinha>quebra){
			    contLinha=1;			    		   	   
				npag++;						
				if (npag!=1){	
				quebra=13;
					
		%>
						</table>      
					<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
			<% } 
			%>				
	<jsp:include page="GuiaPreparaCabImp.jsp" flush="true" >
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
    
  <table id="autosImp" width="100%" border="0" cellpadding="1" cellspacing="1" class="semborda" >
        <% } %>        
    <tr bgcolor='<%=cor%>'> 
      <td width="5%" height="14" align=center rowspan=4 class="table_linha_top"><%= seq %></td>
      <td width="10%" align=center class="td_linha_top_left"><%=myProcesso.getSigOrgao() %></td>
      <td width="25%" class="td_linha_top_left">&nbsp;<%=myProcesso.getNumProcesso() %></td>
      <td class="td_linha_top_left">&nbsp;<%=myProcesso.getDatProcesso()%></td>
    </tr>
    <tr bgcolor='<%=cor%>'> 
      <td colspan="2"  class="td_linha_top_left" class="semborda">&nbsp;<%=myProcesso.getNumRequerimento() %>-<%=funcionalidadesBean.getTipoRequerimento() %>
      </td> 
    </tr>
    <% 
    		Iterator itProc = myProcesso.getRequerimentos().iterator();
			PNT.RequerimentoBean myReq  = new PNT.RequerimentoBean();
			myReq = myProcesso.getRequerimentoPorStatusETipoRequerimento("3", funcionalidadesBean.getTipoRequerimento());
			if (!"".equals(myReq.getNumRequerimento())){%>
				 <tr bgcolor='<%=cor%>'> 
				      <td style="text-align:right" width="10" class="td_linha_top_left"><strong>Resultado:</strong></td> 
				      <td height="25" colspan="5"  class="table_linha_top">&nbsp; 
				   	    <input name="codResultRS<%= seq %>" type="radio" disabled="disabled" class="semborda" value="D" <%= sys.Util.isChecked(myReq.getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">
				        DEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				   	    <input name="codResultRS<%= seq %>" type="radio" disabled="disabled" class="semborda" value="I" <%= sys.Util.isChecked(myReq.getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">
				        INDEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo: </strong>
					        <%= myReq.getTxtMotivo() %>      	    
				      </td>
				    </tr>
				    <tr bgcolor='<%=cor%>'>
				    	<td style="text-align:right" class="td_linha_top_left"><strong>Penalidade:</strong></td> 
				      	<td height="25" colspan="5"  class="table_linha_top">&nbsp;
					        <%= myReq.getDscPenalidade() %>     	    
				      </td>
				    </tr>
			<% }else{ %>
    			<tr><td></td><td></td></tr>
    		<% }	%>
    <!-- 
    <//% if (myProcesso.getRequerimentos(0).getCodResultRS().equals("D")) { %>
	  <tr bgcolor='<//%=cor%>' >
             <td  colspan="6" height=20  class="td_linha_top_left">&nbsp;&nbsp;<input type="radio" checked="checked" name="codMotivo<//%= seq %>" value="1" <//%= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Inconsist&ecirc;ncia do AI&nbsp;&nbsp; 
			       <input type="radio" name="codMotivo<//%= seq %>" value="2" <//%= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Notifica&ccedil;&atilde;o&nbsp;&nbsp;
			       <input type="radio" name="codMotivo<//%= seq %>" value="3" <//%= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodMotivoDef(),"3") %> class="sem-borda" style="height: 14px; width: 14px;">Alega&ccedil;&atilde;o de Defesa&nbsp;&nbsp;
			       <input disabled="disabled" type="radio" name="codMotivo" value="4" class="sem-borda" style="height: 14px; width: 14px;">Outros
			 </td>
      </tr>
    <//%}%> 
    <//%
    if (myProcesso.getRequerimentos(0).getCodRemessa().length()>0) { %>
    <tr bgcolor='<//%=cor%>'>
        <td colspan="4"  class="td_linha_top_left" class="semborda"><strong>&nbsp;&nbsp;N&ordm;&nbsp;Guia de Remessa :</strong>&nbsp;<//%=myProcesso.getRequerimentos(0).getCodRemessa()%></td>        
        <td colspan="2"  class="td_linha_top_left" class="semborda"><strong>&nbsp;&nbsp;Data Envio :</strong>&nbsp;<//%=myProcesso.getRequerimentos(0).getDatEnvioGuia() %></td>
    </tr>
   	<//%} %>	    -->
   	
   	
	<% if (myProcesso.getMsgErro().length()>0) { %>
		<tr bgcolor='<%=cor%>' >
	        <td colspan=7>&nbsp;<%=myProcesso.getMsgErro() %> </td>
		</tr>			
	<% } %>	    
	
    <tr><td height=2 colspan=7 bgcolor="#666666"></td></tr>
   <%} %>
  </table>      
<%} else { 
	String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="GuiaPreparaCabImp.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
   	<jsp:include page="../rodape_impConsulta.jsp" flush="true" />

<%} %>
<%	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		if (contLinha<13){

		} %>
		


	<jsp:include page="../rodape_impConsulta.jsp" flush="true" />


<%String imprimeAI = (String) request.getAttribute("imprimeAI");%>
<%if ((imprimeAI != null) && (imprimeAI != "N")) {%>

<div class="quebrapagina"></div>

<!--FIM T�TULO DO M�DULO-->
<!--TELA DE IMPRESSAO RESULTADO-->

<%  	
	int numpag = 0;		
	Iterator it = GuiaDistribuicaoId.getProcessos().iterator() ;
	PNT.ProcessoBean myProcessoInf  = new PNT.ProcessoBean();%>	

<!--  	
<//%	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		while (it.hasNext()) {
			myProcessoInf = (ProcessoBean)it.next() ;			
			 	if ( myProcessoInf.getTemAIDigitalizado()) { 
				Faz quebra de P�gina a cada Auto Digitalizado.			
				numpag++;			
				if (numpag!=1){		%>
						</table>      					
					<div class="quebrapagina"></div>
				<//% } %>			
-->

<!--T�TULO DO M�DULO-->
	  <table align="center" width="468" border="0" cellpadding="2" cellspacing="0">
		<tr>
        	<!--  <td align="left" width="36%"><font color="#000000"><strong>N&ordm; DO AUTO:</strong>&nbsp;<//%=myProcessoInf.getNumAutoInfracao()%></font></td> -->
        	<td align="center" width="39%"><font color="#000000"><strong>PROCESSO:</strong>&nbsp;<%=myProcessoInf.getNumProcesso()%></font></td>
        	<!--<td align="right" width="25%"><font color="#000000"><strong>PLACA:</strong>&nbsp;<//%=myProcessoInf.getNumPlaca()%></font></td> -->
		</tr>
	  </table>
<br>
<!--FIM T�TULO DO M�DULO-->

<!--TELA DE IMPRESSAO RESULTADO
<div align="center" style=" width: 100%;">	
	<img id="imagemAI" alt="Imagem do Auto de Infra��o: <//%=myProcessoInf.getNumProcesso()%>" style="background-color:#E7E7E7" 
		src="<//%=path%>/download?nomArquivo=<//%=myProcessoInf.getAIDigitalizado().getNomFoto()%>&contexto=<//%=myProcessoInf.getAIDigitalizado().getParametro(ParamSistemaBeanId)%>&header=#" width="470" height="903"> 
</div>
-->

<!--FIM TELA DE IMPRESSAO RESULTADO -->
<//% }
     }
       }
<% }} %>


</form>
</body>
</html>