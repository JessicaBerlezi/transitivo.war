<%@page import="PNT.ProcessoBean"%>
<%@page import="PNT.DEF.GuiaDistribuicaoBean"%>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>
<jsp:useBean id="ParamOrgBeanId" scope ="session" class = "REC.ParamOrgBean"/>
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="funcionalidadesBean" scope="session" class="PNT.DEF.FuncionalidadesBean" />


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="../CssImpressao.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="GuiaPreparaImp" method="post" action="">	 
<%
  	int contLinha=99;		   
	int npag = 0;
	int seq = 0;
	String	cor = "#ffffff";	
	Iterator ot = GuiaDistribuicaoId.getRelatGuias().iterator() ;
	GuiaDistribuicaoBean myGuia  = new GuiaDistribuicaoBean();		
	if (GuiaDistribuicaoId.getRelatGuias().size()>0) { 
		while (ot.hasNext()) {
			myGuia = (GuiaDistribuicaoBean)ot.next() ;
			cor    = "#ffffff";
			if (myGuia.getMsgErro().length()>0)  {
				cor = "#CCCCCC"; 
			}			
			contLinha++;
			//Faz quebra de linha a cada 13 linhas, exceto na primeira linha.
			if ((myGuia.getProcessos().size()< 13)|| contLinha > 13){
			    contLinha=1;
				npag++;			
				if (npag!=1){			
		%>
						</table>      
					<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
			<% } %>		
         <% } %>				
	<jsp:include page="GuiaPreparaCabImpAutomatica.jsp" flush="true" >
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
    
     <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr><td colspan=3 height=2></td></tr>
     <tr>        
      <td width="20%"></td>		     
      
        <td width="80%" height=23 align=center><b>GUIA DE DISTRIBUI��O&nbsp;&nbsp;&nbsp;&nbsp;N&ordm;&nbsp;<%=myGuia.getNumGuia()%>
			&nbsp;&nbsp;-&nbsp;&nbsp;<%=myGuia.getDatProc()%>
        </td>
      
      <td width="10%"></td>		
 	 </tr>
     <tr>
     	<td colspan=3>
	     	<table  width="100%" border="0" cellpadding="0" cellspacing="0" >
        		<tr>
		        	<td width="50%" height=16>&nbsp;Junta:&nbsp;<b><%=myGuia.getSigJunta() %></b></td>
		        	<td width="50%" align=right>&nbsp;Relator:&nbsp;<b><%=myGuia.getNomRelator() %></b></td>        
				</tr>
			</table>
		</td>
	</tr>   
  </table>

  <table id="TabFixa" width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
    <tr> 
    <td height="14" colspan=5>Ordem:&nbsp;<%= myGuia.getNomOrdem() %> 
    </td>
    <td colspan=3 align=right> Selecionados&nbsp;&nbsp;<%= myGuia.getProcessos().size() %>&nbsp;&nbsp;Processo(s) 
    </td>
  </tr>
  <tr bgcolor="#C0BDBD"> 
    <td width="25" height="14" align="center" rowspan=2><strong>Seq</strong></td>
      <td width="65" rowspan="2" align="center"><strong>�rg�o</strong></td>
      <td width="223"><strong>&nbsp;Processo</strong></td>
      <td width="70" rowspan="2"             align="center"><strong>Dt Proc</strong></td>
      <td width="56" rowspan="2"                        align="center"><strong>Dt Req.</strong></td>
  </tr>
  <tr bgcolor="#C0BDBD"> 
 
      <td><strong>&nbsp;Requerimento - Tipo Req</strong></td>
  </tr>
</table>
  <table id="autosImp" width="100%" border="0" cellpadding="1" cellspacing="1" >
<%  
	Iterator it = myGuia.getProcessos().iterator() ;
	ProcessoBean myProcesso  = new ProcessoBean();		
	if (myGuia.getProcessos().size()>0) { 
		while (it.hasNext()) {
			myProcesso = (ProcessoBean)it.next() ;
			seq++;
%>
    <tr bgcolor='<%=cor%>'> 
      <td width=25 height="14" align=center rowspan=2><%= seq %></td>
      <td width=223>&nbsp;<%=myProcesso.getNumProcesso() %></td>
      <td width=70 rowspan="2"><%=myProcesso.getDatProcesso() %></td>
      <td width=56 rowspan="2"></td>
    </tr>
    <tr bgcolor='<%=cor%>'> 
     
      <td>&nbsp;<%=myProcesso.getNumRequerimento() %>-<%=funcionalidadesBean.getTipoRequerimento() %> </td>
    </tr>
    <tr bgcolor='<%=cor%>'>    
      <td height="25" colspan=8><strong>&nbsp;Resultado:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
   	    <input name="codResultRS<%= seq %>" type="radio" value="D" <%= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodResultRS(),"D") %> class="sem-borda" style="height: 14px; width: 14px;">
        DEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   	    <input name="codResultRS<%= seq %>" type="radio" value="I" <%= sys.Util.isChecked(myProcesso.getRequerimentos(0).getCodResultRS(),"I") %> class="sem-borda" style="height: 14px; width: 14px;">
        INDEFERIDO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Motivo: </strong>
		<% if (myProcesso.getRequerimentos(0).getTxtMotivoRS().length()>60) { %>
	        <%= myProcesso.getRequerimentos(0).getTxtMotivoRS().substring(0,60) %>      
	    <% } else { %>
	        <%= myProcesso.getRequerimentos(0).getTxtMotivoRS() %>      	    
	    <% } %>
      </td>
    </tr>
	<% if (myProcesso.getMsgErro().length()>0) { %>
		<tr bgcolor='<%=cor%>' >
	        <td colspan=8>&nbsp;<%=myProcesso.getMsgErro() %> </td>
		</tr>			
	<% } %>	    
    <tr><td height=2 colspan=8 bgcolor="#666666"></td></tr>
   <%} %>
  </table>      
<% } else { 
	String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="GuiaPreparaCabImp.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
	<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
<%  
  }
} %>
<%	if (GuiaDistribuicaoId.getProcessos().size()>0) { 
		if (contLinha<13){

		} %>
		<jsp:include page="../rodape_impConsulta.jsp" flush="true" />
<%} %>

<%} %>

<%String imprimeAI = (String) request.getAttribute("imprimeProcesso");%>
<%if ((imprimeAI != null) && (imprimeAI != "N")) {%>

<div class="quebrapagina"></div>

<%  	
	int numpag = 0;		
	Iterator it = GuiaDistribuicaoId.getProcessos().iterator() ;
	ProcessoBean myAutoInf  = new ProcessoBean();%>	
	

	  <table align="center" width="468" border="0" cellpadding="2" cellspacing="0">
		<tr>
        	
        	<td align="center" width="39%"><font color="#000000"><strong>PROCESSO:</strong>&nbsp;<%=myAutoInf.getNumProcesso()%></font></td>
        	
		</tr>
	  </table>
<br>

<% }
    // }
      // }
//}%>


</form>
</body>
</html>