<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@page import="PNT.TAB.PenalidadeBean"%>
<%@ page import="java.util.Iterator" %>


<%
if(posTop==null) posTop ="170px";   
if(posHei==null) posHei ="183px";    
%>
<link href="../Css.jsp" rel="stylesheet" type="text/css">

<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: <%= posTop %>; height: <%= posHei %>; visibility: visible;" >  
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="semborda"  >
	 <%  if (GuiaDistribuicaoId.getProcessos().size()>0) {
	  	int seq = 0;
		String cor   = (seq%2==0) ? "#d9f4ef" : "#ffffff";
		Iterator it = GuiaDistribuicaoId.getProcessos().iterator() ;
		PNT.ProcessoBean myProc  = new PNT.ProcessoBean();		
		while (it.hasNext()) {
			myProc = (PNT.ProcessoBean)it.next() ;				
			cor    = (seq%2==0) ? "#d9f4ef" : "#ffffff";
			if (myProc.getMsgErro().length()>0)	cor = "#CCCCCC"; 
		%>
		<tr bgcolor='<%=cor%>'>	
	        <td width="80" height="14" align=center rowspan=2><%=(seq+1)%></td>
	        <td width="85" align=center onclick="javascript:Mostra('MostraProcesso',this.form,'<%=myProc.getNumProcesso() %>');">
	        	&nbsp;<%=myProc.getSigOrgao() %></td>
			<td width="150" onclick="javascript:Mostra('MostraProcesso',this.form,'<%=myProc.getNumProcesso() %>');">
				&nbsp;<%=myProc.getNumProcesso() %>   </td>
			<td onclick="javascript:Mostra('MostraProcesso',this.form,'<%=myProc.getNumProcesso() %>');">
				&nbsp;<%=myProc.getDatProcesso() %>    </td>
		</tr>
		<tr bgcolor='<%=cor%>' 
			onclick     = "javascript:Mostra('MostraProcesso',this.form,'<%=myProc.getNumProcesso() %>');" >
	        <td colspan=6>&nbsp;<%=myProc.getNumRequerimento() %>&nbsp;&nbsp;<%=funcionalidadesBean.getTipoRequerimento() %> </td>	        
		</tr>	
		<% if (myProc.getMsgErro().length()>0) { %>
			<tr bgcolor='<%=cor%>' >
		        <td colspan="7">&nbsp;<%=myProc.getMsgErro() %> </td>
			</tr>			
		<% } %>
    	
    	<tr  bgcolor='<%=cor%>'> 
	      <td height="25" colspan="7">&nbsp;&nbsp;<strong>Resultado:</strong>&nbsp;&nbsp; 
    	    <input name="codResultRS<%= seq %>" type="radio" value="D" <%= sys.Util.isChecked(myProc.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBean.getTipoRequerimento()).getCodResultRS(),"D") %> onclick="javascript: mostaCodMotivoDef('<%=seq%>');" class="sem-borda" style="height: 14px; width: 14px;">
	         DEFERIDO&nbsp;
    	    <input name="codResultRS<%= seq %>" type="radio" value="I" <%= sys.Util.isChecked(myProc.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBean.getTipoRequerimento()).getCodResultRS(),"I") %> onclick="javascript: escondeCodMotivoDef('<%=seq%>');" class="sem-borda" style="height: 14px; width: 14px;">
        	 INDEFERIDO&nbsp;
        	Motivo:&nbsp;
	        <input name="txtMotivoRS<%= seq %>" type="text" size="67" maxlength="1000"  value="<%= myProc.getRequerimentoPorStatusETipoRequerimento("3",funcionalidadesBean.getTipoRequerimento()).getTxtMotivoRS() %>">		       	
          </td>
	    </tr>
    	
    	<!-- In�cio do select de penalidade -->
    	<tr bgcolor="<%=cor%>" >
		<td colspan="1" style="text-align: right"><strong>Penalidade:</strong></td>
        <td colspan=6>
    		<jsp:useBean id="PenalidadeId"  scope="request" class="PNT.TAB.PenalidadeBean" />
			<jsp:setProperty name="PenalidadeId" property="j_abrevSist" value="PNT.TAB" />  
			<jsp:setProperty name="PenalidadeId" property="colunaValue" value="COD_PENALIDADE" />
			<% PenalidadeId.setPopupNome("codPenalidade"+seq); %>
			<jsp:setProperty name="PenalidadeId" property="popupExt"    value="onChange=\"javascript: valida('LePenal',this.form);\"" />                 	              	 
			<%
			   String r = myProc.getCodPenalidade();
			   PenalidadeId.setChecked(r);
			   PenalidadeId.setPopupString("DSC_PENALIDADE,SELECT COD_PENALIDADE,DSC_PENALIDADE "+
			     "FROM TPNT_PENALIDADE ORDER BY DSC_PENALIDADE");
			%>
			<jsp:getProperty name="PenalidadeId" property="popupString" />
        </td>
      	</tr>
      	<!-- Fim do select de penalidade  -->
      	
      	<tr><td height="2" colspan="7" bgcolor="#666666"></td></tr>
		<% seq++; }
	} else {
		 String msg=(String)request.getAttribute("semProcesso"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>		
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#faeae5'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	<% 
		}
	} 
	%>
    </table>      
</div>