<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.List"%>
<%@ page import="PNT.ProcessoBean"%>
<%@ page import="PNT.DEF.GuiaDistribuicaoBean"%>
<%@ page import="PNT.DEF.FuncionalidadesBean"%>
<%@ page import="sys.Util"%>
<% String path=request.getContextPath(); %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="funcionalidadesBeanId"   scope="session" class="PNT.DEF.FuncionalidadesBean" /> 

<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="myOrg"   scope="request" class="ACSS.OrgaoBean" /> 
<jsp:useBean id="paramOrg"   scope="request" class="REC.ParamOrgBean" /> 
<% paramOrg.PreparaParam(UsuarioBeanId);%>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0";  %>
<SCRIPT LANGUAGE="JavaScript1.2">
	window.print()
</script>
<html>
<head> 
<jsp:include page="../Css.jsp" flush="true" />
<title>Publica&ccedil&atilde;o de Ata</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.style3 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
}
-->
</style>
<body>
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">
  	<tr><td height=24 colspan="3"></td></tr>
  	<tr>
	  <td width="185"></td>
  	  <td width="565" align="center"><strong><span class="style3">GOVERNO DO RIO DE JANEIRO</strong></span></td>
	  <td width="200"></td>
  	</tr>  	<tr>
	  <td width="185"></td>
  	  <td width="565" align="center"><strong><span class="style3"><%= paramOrg.getParamOrgao("NOME_EMPRESA_ATA","","3")%></strong></span></td>
	  <td width="200"></td>
  	</tr>	
	<tr>
	  <td width="185"></td>
	  <td align="center"><span class="style3"><strong>DEPARTAMENTO DE TR�NSITO DO ESTADO DO RIO DE JANEIRO<br/><br/></strong></span></td>
	  <td width="200"></td>
	</tr>
  </table>
  	<%	List guias = GuiaDistribuicaoId.getRelatGuias();
		List processos = null;
		ProcessoBean myProcesso = null;
		GuiaDistribuicaoBean myGuia = null;
		int contador = 0,npag = 2;
		String cor = null;
		String msg = null;
		String sAtaAnt="",sGuiaAnt="";
		String datResultado = "";
		String junta = "";
		String tituloRelator = "";
		String singPlural = null;
		for(int x=0;x<guias.size();x++){
			myGuia = (GuiaDistribuicaoBean)guias.get(x);
			//processos = myGuia.getProcessosComRecursoIndeferidos();
			processos = myGuia.getProcessos(funcionalidadesBeanId.getTipoRequerimento());
			singPlural = "";
			if ( (sAtaAnt.equals(myGuia.getSigJunta())==false) || (sGuiaAnt.equals(myGuia.getNumGuia())==false)){
			  sAtaAnt=myGuia.getSigJunta();
			  sGuiaAnt=myGuia.getNumGuia();
			  if(!junta.equals(myGuia.getSigJunta())){
%>
	 <table>
	 <!-- RETIRADO -->
	 </table>
	 <table>
	 <%				datResultado = "";	
	 				tituloRelator = "";
	 			}
	 			if( (!datResultado.equals(myGuia.getDatRS())) || (!myGuia.getNomTituloRelator().equals(tituloRelator)) ){
	 				singPlural = "";
	 	 			// Acrescenta a letra 's' ao cabe�alho caso exista mais de um processo na ATA.
	 				if(processos.size()>1)
	 					singPlural = "s";
	 				if(guias.size()>x+1){
	 					if( (((GuiaDistribuicaoBean)guias.get(x+1)).getDatRS().equals(myGuia.getDatRS())) && (myGuia.getNomTituloRelator().equals(((GuiaDistribuicaoBean)guias.get(x+1)).getNomTituloRelator())) )
	 						singPlural = "s";
	 				}
	 %>
     <tr>   
	 	<td width="166"></td>     
 		<td colspan="7"><span class="style3"><strong><%=GuiaDistribuicaoId.getTituloAta()%><br/><br/></strong></span></td>
 	 </tr>
 	 <tr>   
	 	<td width="166"></td>     
 		<td colspan="7"><span class="style3"><strong>EDITAL N� <%=GuiaDistribuicaoId.getCodEnvioDO()%>, DE <%=GuiaDistribuicaoId.getDatEnvioDO().substring(0,2)%> DE <%=	Util.mesPorExtenso(GuiaDistribuicaoId.getDatEnvioDO().substring(3,5))%> DE <%=GuiaDistribuicaoId.getDatEnvioDO().substring(6,10)%><br/><br/></strong></span></td>
 	 </tr>
 	 
	<tr>
	  <td width="166"></td>
	  <td colspan="7"><span class="style3"><strong><%=GuiaDistribuicaoId.getSubTituloAta()%><br/><br/></strong></span></td>
	</tr>
	<tr>
	  <td width="50"></td>
	  <td colspan="7"><span class="style3"><%=GuiaDistribuicaoId.getParagrafo1()%><br/><br/></span></td>
	</tr>
	<tr>
	  <td width="50"></td>
	  <td colspan="7"><span class="style3"><%=GuiaDistribuicaoId.getParagrafo2()%><br/><br/></span></td>
	</tr>
	<tr> 
	  <td width="170"></td>
      <td colspan="2" width="114" ><span class="style3">CPF</span></td>
      <td colspan="2" width="64" ><span class="style3">Pts</span></td>
	  <td width="302"><span class="style3">Penalidade</span></td>
	  <td width="18"></td>
	</tr>
	<%				datResultado = myGuia.getDatRS();
					tituloRelator = myGuia.getNomTituloRelator();
			   	}
			}
			for(int y=0;y<processos.size();y++){
				myProcesso = (ProcessoBean)processos.get(y);
				msg = myProcesso.getMsgErro();
				if(msg.length()==0)
					cor = "#FFFFFF";
				else
					cor = "#CCCCCC";
	%>
  <tr bgcolor='<%=cor%>'>
  	<td width="170" style="line-height:12px "></td>
  	<td width="110" style="line-height:12px "><span class="style3"><%=myProcesso.getNumCPFEdt()%></span></td>
	<td width="4" style="line-height:12px "></td>
 	<td width="38" style="line-height:12px "><span class="style3"><%=myProcesso.getTotPontos()%></span></td>
	<td width="26" style="line-height:12px "></td>
 	<td width="302" style="line-height:12px "><span class="style3"><%=myProcesso.getRequerimentoPorStatusETipoRequerimento("3", "DP").getDscPenalidade()%></span></td>
	<td width="18" style="line-height:12px "></td>
	<%			if(msg.length()!=0){
					contador++;      %>
	<tr bgcolor='<%=cor%>'> 
		<td colspan="8"><b><span class="style3"><%= msg %></span></b></td>
	</tr>
	<%}%>
  </tr>
<%			}
		}
	%>
</table>

<!--fim cab impress�o-->
</body>
</html>