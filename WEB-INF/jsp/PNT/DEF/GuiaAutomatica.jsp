<%@ page session="true"%>
<%@ page import="java.util.Iterator" %>
<%String path = request.getContextPath();%>
<%@ page errorPage="../ErrorPage.jsp"%>


<jsp:useBean id="GuiaDistribuicaoId" scope="session" class="PNT.DEF.GuiaDistribuicaoBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<%@ include file="../Css.jsp" %>
		<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function Soma(fForm) {
	n = 0 ;

	for (i=0;i<document.all["numProcesso"].length;i++) {
		if (document.all["numProcesso"][i].value.length > 0) n++;
		
	document.all["numProcesso"][i].value = document.all["numProcesso"][i].value.toUpperCase();	
	
	}

	document.all["numSelec"].value = n;
	return n ;
}

function LimpaCampos(fForm) {
	if (veCampos(fForm)==true) { 	
		for (i=0;i<document.all["numProcesso"].length;i++) {
			if (document.all["numProcesso"][i].value.length > 0) n++;
			
		document.all["numProcesso"][i].value = "";		
		
		}
		
	}else alert(sErro);	 
}

function valida(opcao,fForm) {	
	 switch (opcao) {   	  
	   case 'R':

			fForm.acao.value =opcao;
		   	fForm.target     = "_self";
		    fForm.action     = "acessoTool";  
		   	fForm.submit();	   	   
	    	close();
			break;
	   case 'EnviarProcesso':	   
	   	     if (veCampos(fForm)== true) { 		   	     
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				   	fForm.action = "acessoTool";  
			   		fForm.submit();				  
			 }
			 else{alert(sErro)}
				   
		    break;	  
  }
}



function veCampos(fForm){
valid   = true ;
sErro = "" ;


for (k=0;k<fForm.numProcesso.length;k++) {

   var ne = trim(fForm.numProcesso[k].value)  
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.numProcesso.length;m++) {
	   var dup = trim(fForm.numProcesso[m].value)	  
	   if ((m!=k) && (dup.length!=0) && ((dup==ne))) {
		   sErro += "Processo em duplicata !!!: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;		   
		   fForm.numProcesso[k].focus();
		   fForm.numProcesso[k].select();
		   break;			   
	   }
	}
}

 if (document.all["numSelec"].value==0) {  
	sErro += alert("Nenhum processo digitado!");
 }
return valid ;
}

</script>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
	</head>
	<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;" onLoad="javascript:Soma(this);">
		<form name="GuiaAutomaticaForm" method="post" action="">			
			<%@ include file="../Cab.jsp"%>
			<input name="acao" type="hidden" value=' '>					
			<!--IN�CIO_CORPO_sistema-->

<div id="recurso5" style="position:absolute; left:51px; top:80px; width:718px; height:10px; z-index:10; overflow: visible; "> 
			<table width="100%" border="0" cellpadding="0" cellspacing="0" class="table">      		    
		      <tr bgcolor="#8AAEAE">        
		        <td width="90%" align="center" bgcolor="#d9f4ef" style="height:23px;text-align: left;"><strong><strong>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Processos Informados :</strong>
				&nbsp;
				<input name="numSelec" type="text" value="" size="4" readonly >			  </td>
			  <td align="center" width="5%">
				<button style="border: 0px; background-color: transparent; height: 21px; width: 29px; cursor: hand;" onClick="javascript: valida('EnviarProcesso',this.form);">
				<img src="images/PNT/bot_enviar_fundoescuro_ico.gif" width="27" height="19" alt="Enviar Processos"></button></td>
		      <td align="center" width="5%">
				<button style="border: 0px; background-color: transparent; height: 21px; width: 29px; cursor: hand;" onClick="javascript: LimpaCampos(this.form);">
				<img src="images/PNT/bot_limpar_fundoescuro_ico.gif" width="27" height="19" alt="Limpar Campos"></button>
		      </td>
		      </tr> 		     
	</table>
</div>
<div id="recurso4" style="position:absolute; left:50px; top:105px; width:720px; height:240px; z-index:2; overflow: auto; "> 
			
			 <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">      
		      <%
		      

		      
	           int resto =   GuiaDistribuicaoId.getProcessosSelect().size()%4 ;
               if (resto>0) 
               {
            	   PNT.ProcessoBean myProcesso  = new PNT.ProcessoBean();
                     for (int i=0; i<4-resto;i++) {
                             GuiaDistribuicaoId.getProcessosSelect().add(myProcesso);
				      }
				}
				for (int i = 0; i < GuiaDistribuicaoId.getProcessosSelect().size(); i=i+4) {
		        %>			  	  
				<tr bgcolor="#d9f4ef"> 
					 <td width="185" style="text-align: right; height: 20px;">&nbsp;<%=i+1%>.&nbsp;<input   name="numProcesso" type="text" value="<%= GuiaDistribuicaoId.getProcessosSelect(i).getNumProcesso()  %>" size="26"     maxlength="20" onBlur = "javascript:Soma(this.form);"></TD>
					 <td width="185" style="text-align: right;">              &nbsp;<%=i+2%>.&nbsp;<input   name="numProcesso" type="text" value="<%= GuiaDistribuicaoId.getProcessosSelect(i+1).getNumProcesso()%>" size="26"   maxlength="20" onBlur = "javascript:Soma(this.form);"></TD>
				     <td width="185" style="text-align: right;">              &nbsp;<%=i+3%>.&nbsp;<input   name="numProcesso" type="text" value="<%= GuiaDistribuicaoId.getProcessosSelect(i+2).getNumProcesso()%>" size="26"   maxlength="20" onBlur = "javascript:Soma(this.form);"></TD>
					 <td style="text-align: right;">                          &nbsp;<%=i+4%>.&nbsp;<input   name="numProcesso" type="text" value="<%= GuiaDistribuicaoId.getProcessosSelect(i+3).getNumProcesso()%>" size="26"   maxlength="20" onBlur = "javascript:Soma(this.form);"></TD>
				</tr>		
				<tr><td height="1" colspan="5" bgcolor="#8AAEAE"></td></tr>       
		    	 <%	}    		
				for (int i = GuiaDistribuicaoId.getProcessosSelect().size(); i < 99; i=i+4) {
		        %>			  	  
				<tr bgcolor="#d9f4ef"> 
					 <td width="185" style="text-align: right; height: 20px;">&nbsp;<%=i+1%>.&nbsp;<input   name="numProcesso" type="text" value="" size="26" maxlength="20"   onBlur = "javascript:Soma(this.form);"></TD>
					 <td width="185" style="text-align: right;">              &nbsp;<%=i+2%>.&nbsp;<input   name="numProcesso" type="text" value="" size="26" maxlength="20"   onBlur = "javascript:Soma(this.form);"></TD>
				       <td width="185" style="text-align: right;">            &nbsp;<%=i+3%>.&nbsp;<input   name="numProcesso" type="text" value="" size="26" maxlength="20"   onBlur = "javascript:Soma(this.form);"></TD>
					 <td style="text-align: right;">                          &nbsp;<%=i+4%>.&nbsp;<input   name="numProcesso" type="text" value="" size="26" maxlength="20"   onBlur = "javascript:Soma(this.form);"></TD>
				</tr>		
				<tr ><td height="1" colspan="5" bgcolor="#8AAEAE"></td></tr>       
		    	 <%	}    		
	   		 %>		    

		     </table>	
</div>		
			<!-- FIM -->	
			
<!-- Rodap�-->
<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String  msgErro= GuiaDistribuicaoId.getMsgErro();
String  msgOk= GuiaDistribuicaoId.getMsgOk();
String  msgErroTop="160 px";
String  msgErroLeft="80 px";
String mostraMsg   = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
 
<!--FIM_Div Erros-->				
		</form>
	</body>
</html>


