<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<jsp:useBean id="AtaDOBeanId"   scope="session" class="PNT.AtaDOBean" /> 


<html>
<head>
<%@ include file="../Css.jsp" %>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'EstornaAta':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'R':  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.codEnvioDO,"Ata de Publica��o",0);
    if (valid==false) 
      alert(sErro) ;
	return valid ;
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AtaForm" method="post" action="">
<%@ include file="../Cab.jsp" %>
<input name="acao" type="hidden" value=' '>	
<input name="confirmaExclusao" type="hidden" value="N">	
<div id="WK_SISTEMA" style="position:absolute; width:470px; overflow: visible; z-index: 1; top: 100px; left:200px;" > 
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
	  <tr><td valign="top" colspan=2 height="10"></td></tr>	
	  <tr>
        <td colspan="2" height="5" ></td>
      </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
      <tr>
        <td width="90"><b>Atas :</b></td>
	  <td valign="top"><select name="codEnvioDO"> 
					<option selected >&nbsp;</option>
	<%
        int tot = AtaDOBeanId.getEventos().size() ;
        for(int i=0; i<tot; i++) {
			// lista os eventos			
			PNT.AtaDOBean myEvt = AtaDOBeanId.getEventos(i);
			if (AtaDOBeanId.getCodAtaDO().equals(myEvt.getCodAtaDO())){
	%>       
					<option selected value="<%=myEvt.getCodEnvioDO() %>">ATA: <%=myEvt.getCodEnvioDO()%> DATA: <%=myEvt.getDatEDO() %>  PROCESSOS: <%=myEvt.getQtdProc() %> </option>
	<%      }else{%>       
					<option value="<%=myEvt.getCodEnvioDO() %>">ATA: <%=myEvt.getCodEnvioDO()%> DATA: <%=myEvt.getDatEDO() %>  PROCESSOS: <%=myEvt.getQtdProc() %> </option>
	<%      }%>       
	<%}%>       
		         </select>
		</td>
           
           
        <td width="75" align="right" ><button type="button" NAME="Ok" style="width: 29px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('EstornaAta',this.form);"><IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19" alt="Informar Data de Publica��o"></button></td>
      </tr>
      <tr>
        <td colspan="3" height="5" ></td>
      </tr>
   </table>

</div>

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>

<%  	
String msgErro     = AtaDOBeanId.getMsgErro();
String msgOk       = AtaDOBeanId.getMsgOk();
String msgErroTop    = "190 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	  	
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>

</form>
</body>
</HTML>


