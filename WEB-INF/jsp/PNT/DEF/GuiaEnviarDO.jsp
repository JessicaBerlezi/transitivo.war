<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="PNT.DEF.GuiaDistribuicaoBean"%>
<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 
<jsp:useBean id="ParamOrgBeanId" scope="session" class="REC.ParamOrgBean" /> 
<% String numDias = ParamOrgBeanId.getParamOrgao("DIAS_PARA_INFORMAR_RESULTADO");
   if (numDias.equals("")) numDias = "0";
   int dias = Integer.parseInt(numDias);
   if (dias ==0) dias = 120;%>
<html>
<head>
<jsp:include page="../Css.jsp" flush="true" />
<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'LeGuia':    			
                  
       
	      if (veCampos(fForm)==true) {	
	      	if (comparaDatas(document.all.datEnvioDO.value)){ 		
					fForm.acao.value=opcao
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	  		  
	  	  	}
	  	  }
	   	  
	  break ; 
   case 'R':
	  fForm.acao.value=opcao
	  fForm.target= "_self";
	  fForm.action = "acessoTool";  
	  fForm.submit();	  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	naoSelec = true ;
	for(i=0;i<fForm.guia.length;i++){	
	    if(fForm.guia.options[i].selected){
			naoSelec = false ; 
    	}
	}  
	if (naoSelec)  {
		valid = false ;
		sErro = "Guia n�o selecionada. \n"
	}
	verString(fForm.codEnvioDO,"ATA de Publica��o",0);
	valid = ValDt(fForm.datEnvioDO,1) && valid
	verString(fForm.datEnvioDO,"Data de julgamento",0);

	if (valid==false) alert(sErro) ;
	return valid ;
}
function comparaDatas(data2) 
{
valid = true;
var dataB= new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
var today=new Date();
var difference=today-dataB;//obt�m o resultado em milisegundos.
formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
	if (formatdifference > <%=dias%>){
	concorda = confirm("J� se passaram " + formatdifference + " " +"dias, confirma a opera��o ?");
		if (concorda==false){
			valid = false;
		}
	}
return valid;
}



</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="../Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:660px; overflow: visible; z-index: 1; top: 101px; left:83px;" > 
  <table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="semborda">	 
     <tr bgcolor="#d9f4ef">
        <td width="280" height=23><strong>&nbsp;N&ordm; da ATA:</strong> &nbsp;&nbsp;
        	<input type="text" name="codEnvioDO" size="26" maxlength="19" max="19" value="<%=GuiaDistribuicaoId.getCodEnvioDO() %>"
        	onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase();" >
        </td>
        <td width="100"></td> 
		<td align="left"><strong>Data de Julgamento:&nbsp;&nbsp;</strong>
	        <strong><input type="text" name="datEnvioDO" size="12" maxlength="10" value='<%=GuiaDistribuicaoId.getDatEnvioDO() %>' 
	           onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"></strong>
        </td>
		<td height="1" bgcolor="#FFFFFF"></td>
	      <td align="center" valign="middle"> 
	          <button type="button" style="height: 23px; width: 33px; border: none; background: transparent;"  onClick="javascript: valida('LeGuia',this.form);">	
    	      <img src="<%= path %>/images/PNT/bot_ok.gif" width="26" height="19"> 
        	  </button>
		  </td>
     </tr>
     <tr><td colspan=4 height=18></td></tr>     
  </table>
</div>


<div style="position:absolute; width:660px; z-index: 1; top: 159px; left: 81px;">
  <table border="0" cellpadding="0" cellspacing="0" width=100% align="center">
    <tr bgcolor="#d9f4ef" >
      <td align="center">
	  	<table bgcolor="#d9f4ef" border="0" cellpadding="0" cellspacing="0" width="100%"  align="left">
          <%List guias = (List)request.getAttribute("guias");%>
            <td id="disponiveis" bgcolor="#d9f4ef" align="center">
				  <select name="guia" multiple size=15 style="width:520px">
					<%for(int i=0; i<guias.size(); i++){
						out.println("<option value=\"" + ((GuiaDistribuicaoBean)guias.get(i)).getNumGuia() + "\">" + "Guia: " +((GuiaDistribuicaoBean)guias.get(i)).getNumGuia() + " - " + ((GuiaDistribuicaoBean)guias.get(i)).getSigJunta() + " - " + ((GuiaDistribuicaoBean)guias.get(i)).getNomRelator() + " - Status:  " + ((GuiaDistribuicaoBean)guias.get(i)).getNomStatus() +"</option>");
					}
					out.println("</select>");
			%>
            </td>

      	</table>
	  </td>
    </tr>
  </table>
</div>

<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />

<!--Div Erros-->
<%  	
String msgErro     = ""; //GuiaDistribuicaoId.getMsgErro();
String msgOk       = ""; //GuiaDistribuicaoId.getMsgOk();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
String mostraMsg     = "hidden";
%>	 
<%@ include file="../../sys/DivErro_Diretiva.jsp" %>
</form>
</BODY>
</HTML>


