<!--M�dulo : PNT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="GuiaDistribuicaoId"   scope="session" class="PNT.DEF.GuiaDistribuicaoBean" /> 

<html>
<head>
<%@ include file="../Css.jsp" %>
<title>:: Intranet EMP :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'ImprimirAta':
		if (vCampos(fForm)){
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
	case 'GravarAta':
		if (vCampos(fForm)) {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
    case 'consulta':
      if(vDatas(fForm)){
		  fForm.acao.value=opcao
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	
	  }  		  
	  break;	  
   case 'R':  		  
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}
function vCampos(fForm){
	valid = true ;
	sErro = "" ;
	verString(fForm.codEnvioDO,"Ata",0);
    if (valid==false) 
      alert(sErro) ;
    else     
      vDatas(fForm);
	return valid ;
}
function vDatas(fForm){
    valid = true ;
	sErro = "" ;
	verString(fForm.De,"Data Inicial",0);
	verString(fForm.Ate,"Data Final",0);
	if(valid){
		if (comparaDatas(fForm.De.value,fForm.Ate.value)==false){
	       valid = false
	       sErro += "Data final n�o pode ser inferior a data inicial. \n"
		   document.all.Ate.focus();       
	    }
    }
	if (valid==false) alert(sErro) ;
	return valid ;
}
function comparaDatas(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}
</script>
<%@ include file="../Css.jsp" %>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="AtaForm" method="post" action="">
<%@ include file="../Cab.jsp"%>
<input name="acao" type="hidden" value=' '>	
<input name="confirmaExclusao" type="hidden" value="N">	
<div id="WK_SISTEMA" style="position:absolute; width:727px; overflow: visible; z-index: 1; top: 100px; left:50px;" > 
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
	  <tr><td valign="top" colspan=2 height="10"></td></tr>	
	  <tr>
		  <td width="67" align="right"  ><b>Data&nbsp;:</b></td>
		  <td width="660">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		    <input name="De" type="text" id="De" size="12" maxlength="12" value="<%=GuiaDistribuicaoId.getDataIni()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);javascript: valida('consulta',this.form);" onfocus="javascript:this.select();">
			  &nbsp;<strong>at�&nbsp;</strong>&nbsp;
	    <input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=GuiaDistribuicaoId.getDataFim()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);javascript: valida('consulta',this.form);" onfocus="javascript:this.select();"></td>
      </tr>
	  <tr>
        <td colspan="2" height="5" ></td>
      </tr>
  </table>
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">    
      <tr>
        <td width="68" align="right"><b>ATA :</b></td>
        <td align="left" width="584">
            <div align="right">
              <jsp:getProperty name="GuiaDistribuicaoId" property="popupString" />
        </div></td>
        <td width="75" align="right" ><button type="button" NAME="Ok" style="width: 29px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('GravarAta',this.form);"><IMG src="<%= path %>/images/bot_incluir_ico.gif" align="left" width="26" height="19" alt="Gravar Ata"></button></td>
      </tr>
      <tr>
        <td colspan="3" height="5" ></td>
      </tr>
      <tr>
		  <td align="left"><div align="right"></div></td>
        <td align="right" ><button type="button" NAME="Ok" style="width: 29px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimirAta',this.form);"><IMG src="<%= path %>/images/bot_imprimir_ico.gif" align="left" width="26" height="19" alt="Imprimir Layout da Ata"></button></td>
      </tr>	
      <tr><td valign="top" colspan=3 height="2"></td></tr>	 
  </table>

</div>

<%@ include file="../Retornar.jsp" %>
<%@ include file="../Rod.jsp" %>
<%
String msgErro = GuiaDistribuicaoId.getMsgErro();
String msgOk = GuiaDistribuicaoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
String mostraMsg   = "hidden";
%>
<%@ include file="../../sys/DivErro_Diretiva.jsp"  %>
 
<% 	String flag = (String)request.getAttribute("confirmaExclusao");
	if(flag != null){
		if(flag.equals("S")){
%>
<script>
	if(confirm("Existe arquivo gerado para a ATA: <%=GuiaDistribuicaoId.getCodEnvioDO()%> \nDeseja Alterar o Arquivo Existente?")==1){
		AtaForm.confirmaExclusao.value = "S"
		valida('GravarAta',AtaForm)
	}
</script>

<%		}
	}	
%>			
</form>
</BODY>
</HTML>


