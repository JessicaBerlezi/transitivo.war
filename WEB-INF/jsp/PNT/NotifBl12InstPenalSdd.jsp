<!--Bloco 12InstSDD - PNT
-->
<%@page import="sys.Util"%>
<table width="100%" border="0" cellpadding="2" cellspacing="0">
<tr>
  <td height="565" align="center" valign="top" style="padding-left: 20px; padding-right:20px;">

 <div class="fontmedia" style="margin-bottom: 0px; margin-top: 0px; text-align:left;text-indent: 0px; line-height: 15px; font-weight:bold">Notifica��o n� <%=notificacao.getNumNotificacao() %>-<%=notificacao.getSeqNotificacao() %></div>
  
 <div class="fontmedia" style="margin-bottom: 10px; margin-top: 0px; text-align:center;text-indent: 0px; line-height: 11px; font-weight:bold">
  <img src="<%= path %>/images/logo.gif" width="65" height="60" align="top" ><br />

  GOVERNO DO RIO DE JANEIRO<br />
SECRETARIA DE ESTADO DA CASA CIVIL<br />
DEPARTAMENTO DE TR�NSITO DO ESTADO DO RIO DE JANEIRO
  </div> 
  
  <div style="font-size: 11px;margin-bottom: 20px; margin-top: 20px; text-align:center;text-indent: 0px; line-height: 16px; font-weight:bold">NOTIFICA&Ccedil;&Atilde;O DE PENALIDADE  DE SUSPENS&Atilde;O DO EXERC&Iacute;CIO DO DIREITO DE DIRIGIR</div>
  
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 14px">O <strong>Departamento  de Tr&acirc;nsito do Estado do Rio de Janeiro &ndash; DETRAN/RJ</strong>, em conformidade com as  compet&ecirc;ncias estabelecidas pela Lei Federal n&ordm;. 9503/97 (C&oacute;digo de Tr&acirc;nsito  Brasileiro &ndash; CTB),</div>
	
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 14px">CONSIDERANDO  o artigo 261, &sect;1&ordm; do CTB e a Resolu&ccedil;&atilde;o CONTRAN n&deg; 182, de 09 de setembro de 2005, que disp&otilde;em sobre a aplica&ccedil;&atilde;o de penalidade de suspens&atilde;o do exerc&iacute;cio do direito de dirigir ao  infrator que transgredir as normas estabelecidas no CTB, cujas infra&ccedil;&otilde;es  prev&ecirc;em, de forma direta espec&iacute;fica, a penalidade de suspens&atilde;o do direito de  dirigir,</div>
	
	<div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 14px">CONSIDERANDO que os artigos 8&ordm; e 9&ordm; da Resolu&ccedil;&atilde;o CONTRAN n&deg; 182/05 determinam a instaura&ccedil;&atilde;o de Processo Administrativo  tendente &agrave; aplica&ccedil;&atilde;o da penalidade de suspens&atilde;o do exerc&iacute;cio do direito de  dirigir quando esgotados todos os meios administrativos de defesa quanto &agrave;  penalidade da infra&ccedil;&atilde;o na esfera administrativa,</div>
	
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 14px">
    		<strong>APLICA as Penalidades  de Suspens&atilde;o do Exerc&iacute;cio do Direito de Dirigir pelo per&iacute;odo de <%=ProcessoBeanId.getDscPenalidadeRes() %> </strong>ao condutor <strong><%=ProcessoBeanId.getNomResponsavel() %>, </strong>CNH <strong><%=ProcessoBeanId.getNumCNH() %>/<%=ProcessoBeanId.getUfCNH() %> e obrigatoriedade de freq&uuml;&ecirc;ncia em Curso de Reciclagem para  Condutores Infratores (CRCI)</strong>,<strong> </strong>em<strong> </strong>raz&atilde;o do<strong> INDEFERIMENTO</strong> da Defesa  Pr&eacute;via apresentada no Processo Administrativo n&ordm; <strong>&nbsp;<%=ProcessoBeanId.getNumProcesso() %></strong>, em face do cometimento da infra&ccedil;&atilde;o de tr&acirc;nsito <strong>n&ordm; <%=ProcessoBeanId.getMultas(0).getNumAutoInfracao() %></strong>, artigo <strong><em><%=ProcessoBeanId.getMultas(0).getDscEnquadramento() %></em></strong>,  ocorrida em <strong><%=ProcessoBeanId.getMultas(0).getDatInfracao() %></strong>, correspondendo  a <strong><%=ProcessoBeanId.getTotPontos() %> </strong>pontos, conforme detalhado na Notifica&ccedil;&atilde;o  de aplica&ccedil;&atilde;o de penalidade &ndash; em anexo, em raz&atilde;o do que disp&otilde;em os artigos 15 e  17 da Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182, de 9 de setembro de 2005.
    </div>
	
	<div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
		Fica  assegurado ao condutor o exerc&iacute;cio do direito fundamental ao contradit&oacute;rio e &agrave;  ampla defesa, extra&iacute;do do artigo 5&ordm;, inciso LV da CRFB, podendo V.S&ordf; apresentar <strong>RECURSO</strong>, por escrito, <strong>no prazo  ininterrupto e improrrog&aacute;vel de trinta dias, contados da data de recebimento  desta Notifica&ccedil;&atilde;o</strong>, &agrave; JARI &ndash; Junta Administrativa de Recurso de Infra&ccedil;&otilde;es do  DETRAN/RJ, junto ao Protocolo Geral do Edif&iacute;cio Sede do Detran-RJ,&nbsp; via Correio, por meio de carta registrada ao  Detran-RJ, com endere&ccedil;o na Avenida Presidente Vargas n. 817, sobreloja - Centro  &ndash; Rio de Janeiro, Cep. 20071-004, ou nas CIRETRANS.
	</div>
    
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
    	O condutor  poder&aacute; ainda dar in&iacute;cio ao cumprimento das Penalidades, mediante <strong>ENTREGA</strong> da Carteira Nacional de  Habilita&ccedil;&atilde;o &ndash; CNH, acautelando-a junto ao N&uacute;cleo de Documentos Apreendidos &ndash; NUDA,  da Diretoria de Habilita&ccedil;&atilde;o deste Departamento de Tr&acirc;nsito, ou nos Postos de  Habilita&ccedil;&atilde;o.
    </div>
	
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
    	Segue a  notifica&ccedil;&atilde;o n&ordm; <strong><%= notificacao.getNumNotificacao() %>-001</strong> com modelo  de requerimento de defesa pr&eacute;via&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  (Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182/2005 &ndash; artigo 11). 
    </div>
	
	<div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
		Os recursos  n&atilde;o ser&atilde;o conhecidos quando interpostos fora do prazo ou por quem n&atilde;o seja  parte leg&iacute;tima, de acordo com o artigo 25 da Resolu&ccedil;&atilde;o CONTRAN N&ordm; 182/2005.
	</div>
    
	<div class="fontmaior" style="margin-bottom: 9px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 15px">
		Finalmente,  comunica-se ao condutor que, se inconformado com a decis&atilde;o da JARI, poder&aacute;  ainda ser interposto recurso ao CETRAN &ndash; Conselho Estadual de Tr&acirc;nsito.
	</div>
    
    <div class="fontmaior" style="margin-bottom: 9px; margin-top: 15px; text-align:justify;text-indent: 0px; line-height: 15px"><%=Util.dataPorExtenso()%></div>
	
	<div style="margin-bottom: 0px; margin-top: 0px; text-align:center"><img src="<%= path %>/images/assinaturadigital.JPG" width="180" height="73" ></div>
	
	<div class="fontmaior" style="margin-bottom: 1px; margin-top: 0px; text-align:justify;text-indent: 0px; line-height: 14px; text-align: center"><span class="fontmedia" style="font-weight:bold">Diretora de Habilita��o  - DETRAN/RJ</span></div>
	  <br />
	  <br />
	
  </td>
</tr>
</table>
