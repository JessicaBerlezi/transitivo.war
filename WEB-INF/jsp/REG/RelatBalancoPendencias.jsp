<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="BalancoPendenciaARBeanId" scope="session" class="REG.BalancoPendenciaARBean"/> 
<html>   
<head>
<%@ include file="Css.jsp" %>
<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	

<style type="text/css">
.relat {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 text-align: right;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #000000;
	 border-bottom: 1px solid #000000;
	 border-left: 0px solid #999999;
     word-spacing: 0px;
	 line-height: 23px;
}
</style>


<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ResumoRetornoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
int npag       = 1;
String tituloConsulta = (String)request.getAttribute("tituloConsulta");
if(tituloConsulta==null) tituloConsulta=""; 
%>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 

<!--DADOS-->
 
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="20%"  height="13" align="right" bgcolor="#999999" class="relat"><strong>Quantidade/M&ecirc;s&nbsp;</strong> &nbsp; </td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JAN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">FEV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">ABR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAI</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUL</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">AGO</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">SET</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">OUT</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">NOV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">DEZ</td>
      <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 6px; border-right: 0px none;">T 
        o t a l</td>
    </tr>
  </table>
  
    
<!--FIM DADOS-->


 
 <!--saldo anterior--> 
 
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
	
	<tr>
      <td width="20%"  height="13" align="right"  bgcolor="#cccccc" class="relat"><strong>Saldo Anterior</strong> &nbsp;</td>
      <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalAnterior())%>&nbsp;</td>
      <%
      }
      %>
      <td class="relat" bgcolor="#cccccc">&nbsp;</td>
    </tr>
   <tr>
    <td width="20%"  height="10" align="right" bgcolor="#ffffff" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px">&nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#ffffff" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
  </table>
<!--fim saldo anterior-->  



<!--enviadas no m�s-->   
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">

  <tr>
    <td width="20%"  height="13" align="right"  bgcolor="#cccccc" class="relat"><strong>Enviadas no M&ecirc;s</strong> &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"  bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalEnviada())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalEnviada())%>&nbsp;&nbsp;</td>      
  </tr>

</table>
<!--fim enviadas no m�s-->

<!--Retornadas no m�s-->  
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
<tr>
    <td width="20%"  height="10" align="right" bgcolor="#ffffff" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px">&nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#ffffff" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
  <tr>
    <td width="20%"  height="10" align="right" bgcolor="#cccccc" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px"><strong>Retornadas no M&ecirc;s</strong> &nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#cccccc" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
  <tr>
    <td width="20%"  height="10" align="right" bgcolor="#e7e7e7" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px"><strong>Primeiro Retorno</strong> &nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#e7e7e7" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
  
  <!--primeiro retorno: FATURAR-->
   <tr>
    <td  height="13" align="right" class="relat">FATURAR &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalFaturar())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalFaturar())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--FIM primeiro retorno: FATURAR-->
  
  <!--primeiro retorno: CREDITO-->
   <tr>
    <td  height="13" align="right" class="relat">CR&Eacute;DITO &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalCredito())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalCredito())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--FIM primeiro retorno: CREDITO-->
  
  <!--primeiro retorno: J� RECUPERADA-->
  <tr>
    <td  height="13" align="right" class="relat" style="padding-bottom: 6px; padding-top: 6px;"><span style="line-height:11px;">J&Aacute; RECUPERADA &nbsp;<br />  
      POR SERVI&Ccedil;O &nbsp;</span></td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalRecuperadoServico())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalRecuperadoServico())%>&nbsp;&nbsp;</td>      
   </tr>
 <!--FIM primeiro retorno: J� RECUPERADA-->
 
 <!--TOTAL primeiro retorno-->
  <tr>
    <td align="right" class="relat">TOTAL 1&ordm; RETORNO &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalPrimeiroRetorno())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalPrimeiroRetorno())%>&nbsp;&nbsp;</td>      
  </tr>
 <!--FIM TOTAL primeiro retorno--> 
 <!--retorno em duplicata: J� RECUPERADA--> 
   <tr>
    <td width="20%"  height="10" align="right" bgcolor="#e7e7e7" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px"><strong>Retorno em Duplicata</strong> &nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#e7e7e7" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
 <!--retorno em duplicata: RECUPERADA-->   
   <tr>
    <td  height="13" align="right" class="relat">RECUPERADA POR ENTREGA &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalRecuperadoEntrega())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalRecuperadoEntrega())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--fim retorno em duplicata: RECUPERADA-->
   <!--retorno em duplicata: DUPLICATA-->   
  <tr>
    <td  height="13" align="right" class="relat">DUPLICATA &nbsp;</td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalDuplicata())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalDuplicata())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--FIM retorno em duplicata: DUPLICATA-->
  
  <!--TOTAL RETORNO EM DUPLICATA-->
  <tr>
    <td align="right" class="relat"  style="padding-bottom: 6px; padding-top: 6px;"><span style="line-height:11px;">TOTAL RETORNO &nbsp;<br />EM DUPLICATA &nbsp;</span></td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalRetornoDuplicata())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalRetornoDuplicata())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--FIM TOTAL RETORNO EM DUPLICATA-->
  
  <!--TOTAL RETORNADAS-->
  <tr>
    <td align="right" class="relat" style="padding-bottom: 6px; padding-top: 6px;"><span style="line-height:11px;"><strong>TOTAL RETORNADAS &nbsp;<br />
      NO M&Ecirc;S</strong> &nbsp;</span></td>
    <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
    <td width="6%" class="relat"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdTotalRetorna())%>&nbsp;</td>
    <%
      }
      %>
      <td class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getQtdTotalRetorna())%>&nbsp;&nbsp;</td>      
  </tr>
  <!--fim TOTAL RETORNADAS-->
  
  <tr>
    <td width="20%"  height="10" align="right" bgcolor="#ffffff" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px">&nbsp;</td>
    <td  height="10" colspan="13" align="right" bgcolor="#ffffff" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
  </tr>
</table>


<!--Saldo a retornar-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr>
      <td width="20%"  height="13" align="right"  bgcolor="#cccccc" class="relat"><strong>Saldo a Retornar</strong> &nbsp;</td>
      <%for (int i=0;i<BalancoPendenciaARBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat" bgcolor="#cccccc"><%=sys.Util.formataIntero(BalancoPendenciaARBeanId.getRetornos(i).getQtdSaldoRetornar())%>&nbsp;</td>
      <%
      }
      %>
      <td class="relat" bgcolor="#cccccc">&nbsp;</td>      
    </tr>
</table>


<!--fim Saldo a retornar-->
  
  
  
<jsp:include page="Rod_impConsulta.jsp" flush="true" />


</form>
</body>
</html>