<!--M�dulo : REG
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<jsp:useBean id="ArquivoDolBeanId" scope="request" class="REG.ArquivoDolBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'Voltar':
            fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   	
			break;
      case 'ImprimeLote':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
      case 'mostraAuto':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}
 

function changeColorOver(obj) {
	obj.style.background="#EDF4DF";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#ffffff";
	obj.style.textDecoration="none";
}

function callAcertaAuto(opcao,numAuto,fForm) {
	fForm=document.frmDownload;	
	fForm.codLinhaLote.value=numAuto;
	valida(opcao,fForm);
}

</script>
</head>
 
<%
 	Vector vetConsulta = (Vector) ConsultaArquivoDOLBeanId.getVetConsulta(); 
%>


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<%@ include file="Cab_Diretiva.jsp" %>

<input name="acao"     type="hidden" value=' '>
<input name="codLinhaLote"     type="hidden" value=' '>
<!--hidden impressao-->
<input name="nomArquivoSel"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomArquivo()%>'>
<input name="datRecebimento"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getDatRecebimento()%>'>
<input name="statusArquivo"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomStatus()%>'>
<input name="nomConsulta"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomConsulta()%>'>
<% 
	REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();      		
	String codLote="";
	if (vetConsulta.size()>=1) {
		LinhaBean=(REG.LinhaLoteDolBean)vetConsulta.get(0);
		LinhaBean.setInd_P59(ArquivoDolBeanId.getDscPortaria59());     		
		codLote=LinhaBean.getCodLote();
	}
%>
<input name="codLote" type="hidden" value='<%=codLote%>'>


<!--IN�CIO_CORPO_sistema-->

<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 45px; z-index:1;" > 

<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="30%" align="left"><b>Arquivo:&nbsp;</b>
				<%=ConsultaArquivoDOLBeanId.getNomArquivo()%>&nbsp;
			  </td>
			  <td width="25%"><b>Data de Envio:&nbsp;</b>&nbsp;<%=ConsultaArquivoDOLBeanId.getDatRecebimento()%>
			  </td>
			  <td width="26%"><b>Status do arquivo:&nbsp;</b>&nbsp;<%=ConsultaArquivoDOLBeanId.getNomStatus()%>
			  </td>
			  <td width="20%"><b><%=ConsultaArquivoDOLBeanId.getNomConsulta()%></b>&nbsp;
			  </td>		  		  		  		  
	          <td align="right">
					<button type="button" NAME="Imprimir"   style="width: 28px;height: 21px;  border: none; background: transparent; cursor: hand;"  onClick="javascript:valida('ImprimeLote',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->
<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:40px; top:110px; width:740px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#deebc2"> 
        <td width="35" height="15"  align="center" bgcolor="#deebc2"><strong>Seq</strong></td>
        <td width="150" bgcolor="#EDF4DF" align="center"><strong>Auto</strong></td>
        <td width="70" align="center" bgcolor="#deebc2"><strong>Placa</strong></td>
        <td width="90" bgcolor="#EDF4DF" align="center"><strong>Data Infra��o</strong></td>
        <td width="90" bgcolor="#deebc2" align="center"><strong>Hora Infra��o</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>Munic�pio</strong></td>
        <td bgcolor="#deebc2" align="center"><strong>C�d. Infra��o</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:40px; top:130px; width:740px; height:207px; z-index:5; overflow: auto; visibility: visible;"> 
      <table width="720" border="0" cellpadding="0" cellspacing="0">
         
     <%
    	int tam=vetConsulta.size();
    	int linha=0;
    	
      	for(int i=0;i<tam;i++) {      		
      		LinhaBean=(REG.LinhaLoteDolBean)vetConsulta.get(i); 
		    LinhaBean.setInd_P59(ArquivoDolBeanId.getDscPortaria59());     		
      	%>
       	<tr name="line<%=i%>" id="line<%=i%>" style="cursor:hand" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="callAcertaAuto('mostraAuto',<%=LinhaBean.getCodLinhaLote()%>,this.form);">       
	       	<td>
		       	<table width="720" border="0" cellpadding="1" cellspacing="1">
		       	<tr>
			        <td width="35" height="15" align="center"><%=i+1%></td>
			        <td width="150"><%=LinhaBean.getNumAuto()%></td>
			        <td width="70" align="center"><%=LinhaBean.getNumPlaca()%></td>
			        <td width="90" align="center"><%=LinhaBean.getDataInfracao()%></td>
			        <td width="90" align="center"><%=LinhaBean.getHoraInfracao()%></td>
			        <td width="60" align="center"><%=LinhaBean.getCodMunicipio()%></td>
			        <td align="center"><%=LinhaBean.getCodInfracao()%></td>
			    </tr>
			    <% if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("REJEITADOS")) {%>
		      	<tr>      
		      		<td colspan="7" style="padding-left:15px";>Erro:&nbsp;<%=LinhaBean.getDscErro()%></td>
		      	</tr>
		      	<% }else if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("CANCELADOS")) {%>
		      	<tr>      
		      		<td colspan="7" style="padding-left:15px";>Motivo:&nbsp;<%=LinhaBean.getMotivoCancelamento()%></td>
		      	</tr>
		      	<%}%>
		      	</table>
			</td>
		</tr>
      	<tr>
        	<td height="1" bgcolor="#cccccc"></td>
      	</tr>
      
      <%} //loop
     %>
    </table>
  
  </div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('Voltar',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.png" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->


<!--Div Erros-->
<%
String msgErro = ConsultaArquivoDOLBeanId.getMsgErro();
String msgOk = "";
String msgErroTop = "160 px";
String msgErroLeft = "130 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 

<!--FIM_Div Erros-->

</form>
</body>
</html>