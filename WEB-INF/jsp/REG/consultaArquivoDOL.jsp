<!--M�dulo : REG
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">
.vermelho 
{color: #ff0000;}
</style>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	 	case 'consulta':
		 	if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
				if (veCampos(fForm)==true) {
					fForm.acao.value=opcao;
				   	fForm.target= "_self";
				    fForm.action = "acessoTool";  
				   	fForm.submit();	 
			 	}
			 }
			 break;		  
		  case 'ImprimeDetalhe':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
	            if(document.all["nomArquivo"].value=='0')
	            {
	            	alert('Selecione um Arquivo...')            
		            document.all["opcao"][1].checked=false;
				}
				else
				{	
					fForm.acao.value=opcao;
					fForm.target= "_blank";
					fForm.action = "acessoTool";  
					fForm.submit();	
				}			   		
			}
			break;
			 case 'F':
	  document.all["Quiz"].style.visibility='hidden';
	  
	  break;		
	  case 'Imprime':
			if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
				fForm.acao.value=opcao;
				fForm.target= "_blank";
				fForm.action = "acessoTool";  
				fForm.submit();
				document.all["Quiz"].style.visibility='hidden';
	            document.all["opcao"].selected=false;   		
	        }
			break;
			
	  case 'Pergunta':
	        
			document.all["Quiz"].style.visibility='visible' ; 
			break;							  
     
	  case 'ENVIADOS':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
		  		fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";
				fForm.submit();	   		
			}
			break;	      
	  case 'GRAVADOS':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
		  		fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";
				fForm.submit();	
			}   		
			break;	  		  		
	  case 'REJEITADOS':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=false;
		   	
		  		fForm.acao.value=opcao;
				fForm.target= "_blank";
				fForm.action = "acessoTool";
				fForm.submit();	
			}   		
			break;
			
	  case 'CANCELADOS':
		    if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
		   	
		  		fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";
				fForm.submit();	 
			}  		
			break;		
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}

<%
 	Vector vetArquivos = (Vector) ConsultaArquivoDOLBeanId.getVetArquivos(); 
%>

myIdent = new Array ();
<% 	
 	for (int i=0;i<vetArquivos.size();i++) {
 		%> 		
 		myIdent[<%=i%>]= new Array("<%=vetArquivos.get(i)%>","<%=vetArquivos.get(i)%>");
 	<%}
%>

function changeColorOver(obj) {	
	obj.style.background="#EDF4DF";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#ffffff";
	obj.style.textDecoration="none";
}

function callMostraLinha(opcao,qtd,nomArquivo, datRecebimento, statusArquivo, codLote,fForm) {
	if (qtd==0) {
		alert("N�o existem registros para o item selecionado.");
		return false;
	}
	fForm=document.frmDownload;	
	fForm.codLote.value=codLote;
	fForm.nomArquivoSel.value=nomArquivo;
	fForm.datRecebimento.value=datRecebimento;
	fForm.statusArquivo.value=statusArquivo;
	valida(opcao,fForm);
}

</script>
</head>
 
<%
 	Vector vetConsulta = (Vector) ConsultaArquivoDOLBeanId.getVetConsulta(); 
%>


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<%@ include file="Cab_Diretiva.jsp" %>

<input name="acao"     type="hidden" value=' '>
<input name="codLote"  type="hidden" value=''>
<input name="nomArquivoSel"  type="hidden" value=''>
<input name="datRecebimento"  type="hidden" value=''>
<input name="statusArquivo"  type="hidden" value=''>
<


<!--IN�CIO_CORPO_sistema-->

<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:730px; overflow: visible; z-index: 1; top: 75px; left: 45px; z-index:1;" > 

<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="30%" align="left"><b>Arquivo:&nbsp;</b>
				<select name="nomArquivo"> 
					<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						var sel="";
						for (j=0;j<myIdent.length;j++) {
							if (myIdent[j][0]=="<%=ConsultaArquivoDOLBeanId.getNomArquivo()%>")
								sel=" selected";
							else
								sel="";
							document.write ( "<option value=" + myIdent[j][0]+  sel + ">" + myIdent[j][1]) + "</option>"
						}
					</script>
				</select>&nbsp;
			  </td>
			  <td width="40%"><b>Data de Envio:&nbsp;</b>&nbsp;<input name="De" type="text" id="De" size="12" maxlength="12" value="<%=ConsultaArquivoDOLBeanId.getDataIni()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  &nbsp;at�&nbsp;&nbsp;<input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=ConsultaArquivoDOLBeanId.getDataFim()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  </td>		  
			  <td width="15%" align="right">
			  
			  </td>
	          <td align="right">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent; cursor: hand;"  onClick="javascript: valida('consulta',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button type="button" NAME="Imprimir"   style="width: 28px;height: 21px;  border: none; background: transparent; cursor: hand;"  onClick="javascript:valida('Pergunta',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left" >

					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:50px; top:120px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="10" bgcolor="#FFFFFF"  style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido<br>&nbsp;em processamento</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFB0B0" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Rejeitado</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#C6EAB5"style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Processado</td>
        <td><img src="images/inv.gif" width=170 height=2></td>
      </tr>
    </table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:40px; top:158px; width:740px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#deebc2"> 
        <td width="180" height="15"  align="center" bgcolor="#deebc2"><strong>Arquivo  (n� lotes)</strong></td>
        <td width="95" bgcolor="#EDF4DF" align="center"><strong>Enviado em</strong></td>
        <td width="95" bgcolor="#EDF4DF" align="center"><strong>Processado em</strong></td>
        <td width="80" height="15"  align="center" bgcolor="#deebc2"><strong>Lote</strong></td>
        <td width="61" bgcolor="#EDF4DF" align="center"><strong>Reg</strong></td>
        <td width="61" height="15"  align="center"><strong>Grv</strong></td>
        <td width="61" bgcolor="#EDF4DF" align="center"><strong>Rej</strong></td>
        <td width="61" bgcolor="#EDF4DF" align="center"><strong>Canc</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:40px; top:178px; width:740px; height:162px; z-index:5; overflow: auto; visibility: visible;"> 
      <table width="720" border="0" cellpadding="1" cellspacing="1" class="table">
         
     <%
    	int tam=vetConsulta.size();
    	String nomArquivo, nomArquivoAnt, qtdLote, status, datRecebimento, nomArquivoPrint;
    	String corArq, datProcessamento;
    	
    	int qtdReg, qtdLinhaErro,qtdCanc,qtdGrav;
    	int linha=0;
    	
      	for(int i=0;i<tam;i++) {
      		REG.ArquivoDolBean ArqDolBean = new REG.ArquivoDolBean();      		
      		ArqDolBean=(REG.ArquivoDolBean)vetConsulta.get(i);
      		
      		Iterator itx  = ArqDolBean.getLote().iterator() ;
			REG.LoteDolBean myLote = new REG.LoteDolBean();
			
			int cont=0;
			while (itx.hasNext()) {
			cont++;

			myLote =(REG.LoteDolBean)itx.next() ;	//Lote	
      		
      		qtdLote=ArqDolBean.getQtdLote();
      		
      		datRecebimento=ArqDolBean.getDatRecebimento();
      		
      		datProcessamento = ArqDolBean.getDatProcessamentoDetran();
      		     		
      		status=ArqDolBean.getCodStatus();
      		try {
      			qtdReg=Integer.parseInt(myLote.getQtdReg());
      		}
      		catch (Exception e) {
      			qtdReg=0;
      		}
      		try {
      			qtdLinhaErro=Integer.parseInt(myLote.getQtdLinhaErro());
      		}
      		catch (Exception e) {
      			qtdLinhaErro=0;
      		}
      		try {
      			qtdCanc=Integer.parseInt(myLote.getQtdCanc());
      		}
      		catch (Exception e) {
      			qtdCanc=0;
      		}
      		try {
      			qtdGrav=Integer.parseInt(myLote.getQtdGrav());
      		}
      		catch (Exception e) {
      			qtdGrav=0;
      		}
      		if (status.equals("9")) {
      			corArq="#FFB0B0";
      		}
      		else if (status.equals("8") || status.equals("7") || status.equals("1")) {
      			corArq="#FFFFD5";
      		}
      		else if (status.equals("2")) {
      			corArq="#C6EAB5";
      		}
      		else {
      			corArq="#FFFFFF";
      		}
      		
	      	nomArquivo=ArqDolBean.getNomArquivo();
      		
      		
      		
      		if (cont>=2) {
      		
      			nomArquivoPrint="";
      			datRecebimento="";
      			corArq="#FFFFFF";
      		}
      		else {       		
      			nomArquivoPrint=nomArquivo + " (" + qtdLote + ")";
      		}
			linha++;
		
		//temp
		//qtdLinhaErro=1;
		
		
      %>
       	<tr name="line<%=linha%>" id="line<%=linha%>">
        <td width="180" height="15" bgcolor="<%=corArq%>"><%=nomArquivoPrint%></td>
        <td width="95" align="center"><%=datRecebimento%></td>
        <td width="95" align="center"><%=datProcessamento%></td>
        <td width="80" height="15" align="center"><%=myLote.getNumLote()%></td>
        <td style="cursor:hand" name="line1<%=linha%>" id="line1<%=linha%>" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="callMostraLinha('ENVIADOS',<%=qtdReg + ",'" + nomArquivo + "','" + ArqDolBean.getDatRecebimento() + "'," + status + "," + myLote.getCodLote()%>,this.form);" width="61" align="center"><%=qtdReg%></td>
        <td style="cursor:hand" name="line2<%=linha%>" id="line2<%=linha%>" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="callMostraLinha('GRAVADOS',<%=qtdGrav + ",'" + nomArquivo + "','" + ArqDolBean.getDatRecebimento() + "'," + status + "," + myLote.getCodLote()%>,this.form);" width="61" height="15" align="center"><%=qtdGrav%></td>
        <td style="cursor:hand" name="line3<%=linha%>" id="line3<%=linha%>" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="callMostraLinha('REJEITADOS',<%=qtdLinhaErro + ",'" + nomArquivo + "','" + ArqDolBean.getDatRecebimento() + "'," + status + "," + myLote.getCodLote()%>,this.form);" width="61" align="center"> <%=qtdLinhaErro%></td>
        <td style="cursor:hand" name="line3<%=linha%>" id="line3<%=linha%>" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="callMostraLinha('CANCELADOS',<%=qtdCanc + ",'" + nomArquivo + "','" + ArqDolBean.getDatRecebimento() + "'," + status + "," + myLote.getCodLote()%>,this.form);" width="61" align="center"> <%=qtdCanc%></td>
      </tr>
      <tr> 
        <td height="1" colspan="8"  bgcolor="#cccccc"></td>
      </tr>
      
      <%} //loop de lote
      
    } //loop de ArqDol%>
    </table>
  
  </div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.png" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<div id="Quiz" style="POSITION: absolute; TOP:55; left:535; width:240; visibility:hidden; z-index: 999; border:none">      
  <TABLE  border='0' cellpadding="0" cellspacing="0" width=100% align=center bgcolor="#ffffff">
    <tr><td colspan=3 height=30 align=center style="color:#000000; font-weight: bold; ">Escolha o tipo de Relat�rio</td></tr>
    <tr> 
	<td width=90% align=center style="cursor:hand border:none"><input  tabindex="9"  type="radio" name="opcao" value="003"  class="input" style="height: 12px; width: 12px" onClick="javascript: valida('Imprime',this.form);">
							&nbsp;Resumido&nbsp;
	   					  <input tabindex="10"  type="radio" name="opcao" value="004" class="input"  style="height: 12px; width: 12px" onClick="javascript: valida('ImprimeDetalhe',this.form);">&nbsp;Detalhado&nbsp;&nbsp; 
	   					  <button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('F',this.form);"> 
          					<img src="<%= path %>/images/REG/bot_encerrar_peq_ico.gif" alt="Fecha as op��es de relat�rios"></button>
	   					  </td> </tr>	
        
  </table>
	
</div>




<!--Div Erros-->
<%
String msgErro = ConsultaArquivoDOLBeanId.getMsgErro();
String msgOk = "";
String msgErroTop = "160 px";
String msgErroLeft = "130 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->

</form>
</body>
</html>