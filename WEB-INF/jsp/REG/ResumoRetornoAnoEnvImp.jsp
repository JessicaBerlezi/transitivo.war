<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="ResumoRetornoBeanId" scope="session" class="REG.ResumoRetornoBean"/> 

<html>   
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.relat {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 text-align: right;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #000000;
	 border-bottom: 1px solid #000000;
	 border-left: 0px solid #999999;
     word-spacing: 0px;
	 line-height: 23px;
}
</style>


<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ResumoRetornoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
int npag       = 1;
String tituloConsulta = (String)request.getAttribute("tituloConsulta");
if(tituloConsulta==null) tituloConsulta=""; 
%>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 

<!--DADOS-->
 
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="20%"  height="13" align="right" bgcolor="#999999" class="relat"><strong>Quantidade/M&ecirc;s</strong> &nbsp; </td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JAN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">FEV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">ABR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAI</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUL</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">AGO</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">SET</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">OUT</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">NOV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">DEZ</td>
      <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 6px; border-right: 0px none;">T 
        o t a l</td>
    </tr>
    <tr>
      <td  height="13" align="right" class="relat"><strong>Enviada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalEnviada())%></td>
    </tr>
    <tr> 
      <td align="right" class="relat"><strong>N&atilde;o Retornada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalnaoRetornada())%>&nbsp;<br/><%=ResumoRetornoBeanId.getRetornos(i).getPercnaoRetornada()%>%&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalnaoRetornada())%><br />100.00%</td>
    </tr>
    <tr> 
      <td align="right" class="relat"><strong>Retornada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%>&nbsp;<br /><%=ResumoRetornoBeanId.getRetornos(i).getPercRetornada()%>%&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%><br />100.00%</td>
    </tr>
  </table>
  
    
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
 <tr>
	  <td class="relat" style="font-weight: normal; border-right: 0px none; border-bottom: 0px none; padding-right: 6px;">N�o retornados de anos anteriores :&nbsp;&nbsp;<%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalnaoRetornadaAnt())%></td>
      
 </tr>
 </table>



<!--FIM DADOS-->				  

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-top: 10px;">
    <tr>
	  <td width="20%" bgcolor="#999999" class="relat" style="border-right: 0px none;"><strong>&nbsp;C&Oacute;DIGOS DE RETORNO</strong> &nbsp;</td>
      <td colspan="14" bgcolor="#999999" class="relat" style="text-align: left; border-right: 0px none;">&nbsp;</td>
    </tr>
</table>



<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">


    <%for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) {
    if (ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada().equals("0")) continue;    	      	    	
    %>
    <tr> 
    <td width="20%" align="right" class="relat"><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getDescRetorno()%>&nbsp;<strong><br>
    <%=(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N")%>&nbsp;</strong></td>
      <%for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++) {
      %>
      <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada())%>&nbsp;<br/><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getPercRetornada()%>%&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())%><br /><%=ResumoRetornoBeanId.getCodigoRetornos(i).getPercRetornada()%>%</td>
    </tr>
    <%
    }
    %>
  </table>
  
<% if (1==2) { %>  
 <!--faturamento--> 
 
 <!--TIT-->
 <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-top: 30px">
    <tr>
	  <td width="20%"  height="13" align="right" bgcolor="#999999" class="relat"><strong>&nbsp;FATURAMENTO</strong> &nbsp;</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JAN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">FEV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">ABR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">MAI</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">JUL</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">AGO</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">SET</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">OUT</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">NOV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold">DEZ</td>
      <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 6px; border-right: 0px none;">T 
        o t a l</td>
    </tr>
</table>
<!--FIM TIT-->

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
<!--LINHA "ENVIADA"-->
    <tr>
      <td  width="20%" height="13" align="right" class="relat">ENVIADA &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalEnviada())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalEnviada())%></td>
    </tr>
<!--FIM LINHA "ENVIADA"-->	

<!--LINHA "CRED. DO MES"-->	
	<tr>
	  <td  height="10" align="right" bgcolor="#cccccc" class="relat" style="text-align: right; border-right: 0px none;line-height: 12px"><strong>Cr&eacute;ditos</strong> &nbsp;</td>
      <td  height="10" colspan="13" align="right" bgcolor="#cccccc" class="relat" style="text-align: left; border-right: 0px none;line-height: 12px">&nbsp;</td>
    </tr>
	<tr>
      <td  height="13" align="right" class="relat">DO M&Ecirc;S &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalCSS())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalCSS())%></td>
    </tr>
<!--FIM LINHA "CRED. DO MES"-->		

<!--LINHA "CRED. DE MESES ANT"-->		
	<tr>
      <td  height="13" align="right" class="relat">
        DE MESES ANTERIORES &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalCSS_Ant())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalCSS_Ant())%></td>
    </tr>
<!--FIM LINHA "CRED. DE MESES ANT"-->
	
<!--LINHA "CRED. RECUP. DE MESES ANT"-->	
	<tr>
      <td  height="13" align="right" class="relat" style="line-height: 12px">
        RECUPERADOS
        &nbsp;<br>
        DE MESES ANTERIORES
        &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalNCSS())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalNCSS())%></td>
    </tr>
<!--FIM LINHA "CRED. RECUP. DE MESES ANT"-->	
<!--TOTAL-->
     <tr> 
      <td align="right" class="relat"><strong>TOTAL DO FATURAMENTO</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdFaturamento())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdFaturamento())%></td>
    </tr>
  </table>
<!--FIM TOTAL-->

<!--fim faturamento-->
  
  
<%}%>  
  
<jsp:include page="Rod_impConsulta.jsp" flush="true" />


</form>
</body>
</html>