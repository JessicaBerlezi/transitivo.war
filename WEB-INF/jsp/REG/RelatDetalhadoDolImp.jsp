<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<jsp:include page="Head.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: Auto;">
<form name="frmConsTalao" method="post" action="">

<!--IN�CIO_CORPO_sistema-->
<!-- Inicio Div Detalhes vari�veis impressao -->	 

<%
  	int contLinha=0;		
	int npag = 0;	
	String LoteAnt="";
	if (ConsultaArquivoDOLBeanId.getVetConsulta().size()>0) { 	
	 int ocor   = ConsultaArquivoDOLBeanId.getVetConsulta().size() ;	 
		    for (int i=0; i<ocor; i++) {		    	
		    	int seq	=0;				    
			    REG.LoteDolBean myLote = new REG.LoteDolBean();
			    myLote=(REG.LoteDolBean)ConsultaArquivoDOLBeanId.getVetConsulta().get(i);  
			    
			    Vector vetLinha = (Vector) myLote.getLinhaDol(); 
			    int ocorAuto=myLote.getLinhaDol().size();			   
			    for (int j=0; j<ocorAuto; j++) {			    	
			    	REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();      		
		      		LinhaBean=(REG.LinhaLoteDolBean)vetLinha.get(j);
		    	contLinha++;
				seq++; %>
				
			<%//Faz quebra de linha a cada x=8 linhas, exceto na primeira linha.
			
			if (contLinha%25==1||(!myLote.getCodLote().equals(LoteAnt))){
				npag++;			
				if (npag!=1){			
		%>
		
					</table>      
					<jsp:include page="rodape_consutaimp.jsp" flush="true" />					
					<div class="quebrapagina"></div>
				<% } %>				
	<jsp:include page="cabecalhoRelatDetDOL.jsp" flush="true" >	
		<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
	<!--Informa��es Fixas-->

	<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
      <tr> 
        <td  height="15"  align="center"><strong>LOTE&nbsp;&nbsp;</strong></td>        
      </tr>
    </table>
	<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="3"></td></tr>
	      <tr>
			  <td  width="15%"><b>&Oacute;rg&atilde;o :&nbsp;<%=UsuarioBeanId.getCodOrgaoAtuacao() %></b>
					
			  </td>
			  <td width="12%" align="left"><b>LOTE:&nbsp;<%=myLote.getNumLote()%></b>					
			  </td>
				  <td width="20%" align="left"><b> Qtd.Autos:&nbsp;</b>&nbsp;<%=myLote.getQtdReg() %></td>		  
	      		  <td width="15%" align="left" ><b>Unidade:&nbsp;</b>&nbsp;<%=myLote.getCodUnidade() %></td>		 
	      		  <td width="18%" align="left" ><b>Exercicio:&nbsp;</b>&nbsp;<%=myLote.getNumAnoExercicio() %></td>	
	      		  <td width="0%" align="left" ></td>		 
	      		  <td width="20%" align="left" ><b>Data Lote:</b>&nbsp;<%=myLote.getDatLote() %></td>		 	 
      		  
	      </tr>	
      	<tr><td colspan="3" height="1"></td></tr>	 
    </table>
    
    <!--Linha-->
	<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
		<tr> 	<td height="1" bgcolor="#000000"></td>	</tr>
	</table>
<!--FIM Linha-->  
	<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	      <tr> 
	        <td  height="15"  align="center"><strong>AUTOS&nbsp;&nbsp;</strong></td>        
	      </tr>
	</table> 
		<!--t�tulos das colunas da tabela-->

    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#CCCCCC"> 
        <td width="35" height="15"  align="center"><strong>Seq</strong></td>
        <td width="150" align="center"><strong>Auto</strong></td>
        <td width="70" align="center"><strong>Placa</strong></td>
        <td width="90" align="center"><strong>Data Infra��o</strong></td>
        <td width="90" align="center"><strong>Hora Infra��o</strong></td>
        <td width="60" align="center"><strong>Munic�pio</strong></td>
        <td align="center"><strong>C�d. Infra��o</strong></td>
      </tr>
    </table>

<!--FIM t�tulos das colunas da tabela-->

	      <% 
	     } 
%>	
	<table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr>       
	       	<td>
		       	<table width="100%" border="0" cellpadding="1" cellspacing="1">
		       	<tr>
			        <td width="35" height="15" align="center"><%=j+1%></td>
			        <td width="150"><%=LinhaBean.getNumAuto() %></td>
			        <td width="70" align="center"><%=LinhaBean.getNumPlaca()%></td>
			        <td width="90" align="center"><%=LinhaBean.getDataInfracao()%></td>
			        <td width="90" align="center"><%=LinhaBean.getHoraInfracao()%></td>
			        <td width="60" align="center"><%=LinhaBean.getCodMunicipio()%></td>
			        <td align="center"><%=LinhaBean.getCodInfracao()%></td>
			    </tr>
			    <% if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("REJEITADOS")) {%>
		      	<tr>       
		      		<td colspan="7" style="padding-left:15px";>Erro:&nbsp;<%=LinhaBean.getDscErro()%></td>
		      	</tr>
		      	<% }%>
		      	</table>
			</td>
		</tr>
      	<tr>
        	<td height="1" bgcolor="#cccccc"></td>
      	</tr>      
		
		<%
		LoteAnt=myLote.getCodLote();
     }
			    

		    }    
		    
		    
		    %>
    </table>
<%}else{ 
      if (ConsultaArquivoDOLBeanId.getMsgErro().length()>0) { %>
		<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">	
			<tr><td height="40" ></td></tr>		
		     	<tr bgcolor='#cccccc'> 
	        		<td height="35" align=center><b><%= ConsultaArquivoDOLBeanId.getMsgErro() %></b></td>
			</tr>		
		</table>	
			
<%    }
   }%>
<jsp:include page="rodape_consutaimp.jsp" flush="true" />
<!--FIM conte�do da tabela-->
</form>
</body>
</html>