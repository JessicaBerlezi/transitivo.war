<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"       scope="request" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="AuditoriaBeanId"      scope="session" class="GER.AuditoriaBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="EventoBeanId"      scope="request" class="REC.EventoBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ParamSistId"     scope="session" class="ACSS.ParamSistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<style>
.linhas td{border: 1px solid #ff0000;}

</style>

<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="Form" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrgAtu     = new Array(<%= OrgaoBeanId.getOrgaosAtuacao(  UsuarioBeanId,ParamSistId  ) %>);
cod_orgao_atu = new Array(myOrgAtu.length/2);
sig_orgao_atu = new Array(myOrgAtu.length/2);
for (i=0; i<myOrgAtu.length/2; i++) {
	cod_orgao_atu[i] = myOrgAtu[i*2+0] ;
	sig_orgao_atu[i] = myOrgAtu[i*2+1] ;
}


myOrg     = new Array(<%= OrgaoBeanId.getOrgaosACSS(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}


myUsr        = new Array(<%= UsrBeanId.getUsuarios() %>);
cod_usuario  = new Array(myUsr.length/3);
nom_username = new Array(myUsr.length/3);
for (i=0; i<myUsr.length/3; i++) {
	cod_usuario[i] = myUsr[i*3+0] ;
	nom_username[i] = myUsr[i*3+1] ;
}
var bProcessando=false;
function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ; 
   case 'B':
   if(bProcessando)
	{
		alert("Opera��o em Processamento, por favor aguarde...");
	}
	else
	{
		   		bProcessando=true;
      preencheIN(fForm);
      if (veCampos(fForm)==true) { 
        selecionaDscCampos(fForm);
		fForm.acao.value=opcao;
	   	fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
      }	
      
    }  
      break ;
   case 'P':
      if (veCampos(fForm)==true) { 
        selecionaCodEventoRef(fForm);
		fForm.acao.value=opcao;
	   	fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
  case 'C':
     document.all["escolha"].style.visibility='visible';
     break;
     
  case 'G':
     if (fForm.codSistemaCp.value==0) {
		alert("Sistema de destino n�o selecionado. \n")
	 }
	 if(fForm.codSistema.value == fForm.codSistemaCp.value){
	    alert( "Sistema de destino deve ser diferente do sistema selecionado. \n")
	 }
	 else{
	   fForm.acao.value=opcao
	   fForm.target= "_self";
	   fForm.action = "acessoTool";  
	   fForm.submit();	  				  
	 }
  	 break ;

   case 'voltar':
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	  
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  break;
	case 'R':  
	  close() ;   
      break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


function veCampos(fForm) {

    if (fForm.codEvento.value==0) {
      
      alert("Evento n�o selecionado. \n");
      return false;
    }
    

    if (fForm.datIni.value=='') {
      alert("Data inicio em branco. \n");
      return false;
    }

    if (fForm.datFim.value=='') {
      alert("Data fim em branco. \n");
      return false;
    }


    return Compara_Data(fForm.datIni.value, fForm.datFim.value);
    
}

function selecionaDscCampos(fForm)
{

	fForm.dscEvento.value = fForm.codEvento.options[fForm.codEvento.selectedIndex].text;
	fForm.sigOrgao.value = fForm.codOrgao.options[fForm.codOrgao.selectedIndex].text;
	fForm.nomUserName.value = fForm.codUsuario.options[fForm.codUsuario.selectedIndex].text;
	
}



function Compara_Data(dataini, datafim)
{
    
	dia = dataini.substr(0,2);
	mes = dataini.substr(3,2);
	ano = dataini.substr(6,4);
	
	dataini = new Date(ano,mes - 1,dia);
	dia = datafim.substr(0,2);
	mes = datafim.substr(3,2);
	ano = datafim.substr(6,4);

	datafim = new Date(ano,mes - 1,dia);
	
	if (dataini > datafim)
	{
		alert('A Data de T�mino deve ser maior ou igual � Data de In�cio!');
		return false;
	}
	return true;
 }

function preencheIN(fForm)
{
      prim = true ;
      fForm.sIN.value = fForm.codOrgaoAtu.options[fForm.codOrgaoAtu.selectedIndex].value;
      
      if (fForm.sIN.value == '0'){
      
           fForm.sIN.value = "";
      
           for (m=0; m < fForm.codOrgaoAtu.length; m++) 
           {      
              if (fForm.codOrgaoAtu[m].value != "0"){
                     if (prim){
                       fForm.sIN.value = fForm.codOrgaoAtu[m].value;
                       prim = false; 
                     }else{
                         fForm.sIN.value = fForm.sIN.value + "," + fForm.codOrgaoAtu[m].value; 
                     }
              }
           }
        
      
      }
 }



function mascaraData(objeto, evento) {
  var tecla = evento.keyCode;
 
    if (objeto.value.length == 10){
      return;
    }


  if (((tecla < 48) || (tecla > 57)) && (((tecla < 96) || (tecla > 105))) && (tecla != 8) && (tecla != 9) && (tecla != 35) && (tecla != 36) && (tecla != 37) && (tecla != 38) && (tecla != 39) && (tecla != 40)) {
    objeto.value = objeto.value.substr(0,objeto.value.length-1);
  }
  
  if (tecla != 8) {
    //joga a primeira barra da data, assim que o usu�rio digitar 2 caracteres
    if (objeto.value.length == 2) { 
		  objeto.value+="/";
	  }
	  //joga a segunda barra da data, assim que o usu�rio digitar 5 caracteres
	  if (objeto.value.length == 5) { 
  	  objeto.value+="/";
    }
  }
  else {
    if ((objeto.value.length == 2) || (objeto.value.length == 5)) {
      objeto.value = objeto.value.substr(0,objeto.value.length-1);
    }
  }
}

 
</script>

<input name="acao" type="hidden" value=' '>				
<input name="codEventoRef" type="hidden" value=' '>				
<input name="dscEvento" type="hidden" value=' '>				
<input name="nomUserName" type="hidden" value=' '>				
<input name="sigOrgao" type="hidden" value=' '>				
<input name="sIN" type="hidden" value=' '>				

  <div id="WK_SISTEMA" style="position:absolute; width:500px; overflow: visible; z-index: 1; top: 95px; left: 192px;" > 
   <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center" style="margin-bottom: -1px"> 
    	  <tr>
      <td style="text-align:right" height="25" valign="top" width="90"><strong>&nbsp;Evento:&nbsp;</strong></td>
	  <td valign="top"><select name="codEvento"> 
					<option selected >&nbsp;</option>
	<%
        int tot = AuditoriaBeanId.getEventos().size() ;
        for(int i=0; i<tot; i++) {
			// lista os eventos			
			REC.EventoBean myEvt = AuditoriaBeanId.getEventos(i);
			if (EventoBeanId.getCodEvento().equals(myEvt.getCodEvento())){
	%>       
					<option selected value="<%=myEvt.getCodEvento() %>"><%=myEvt.getDscEvento() %></option>
	<%      }else{%>       
					<option value="<%=myEvt.getCodEvento() %>"><%=myEvt.getDscEvento() %></option>
	<%      }%>       
	<%}%>       
		         </select>
		</td>
 	    
      </tr>	
    </table>
    
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
     

	  
      <tr>
        <td width="90" style="text-align:right" valign="top" height="25"><strong>Da data:&nbsp;</strong></td>
		<td width="120" valign="top"><input type="text" name="datIni" size="12" maxlength="10" value='<%=AuditoriaBeanId.getDatInicio() %>' onchange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" onkeypress="javascript:f_hora(),mascaraData(this,event);"></td>
		<td width="90" style="text-align:right" valign="top" ><strong>At� a data:&nbsp;</strong></td>
		<td valign="top"><input type="text" name="datFim" size="12" maxlength="10" value='<%=AuditoriaBeanId.getDatFim()%>' onchange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" onkeypress="javascript:f_hora(),mascaraData(this,event);"></td>
 	   
      </tr>	
	  
	  <tr>
        <td style="text-align:right" valign="top" height="25"><strong>�rg�o Lota��o:&nbsp;</strong></td>
		<td colspan="3" valign="top"><select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
					<option value="0"></option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select>
		</td>
		
	  </tr>
	  
	  
	  <tr>
        
		<td style="text-align:right" valign="top" height="25"><strong>&nbsp;Usu�rio:&nbsp;</strong></td>
      <td colspan="3" valign="top"><select name="codUsuario"> 
					<option value="0" selected></option>
					<script>
						for (j=0;j<cod_usuario.length;j++) {
							document.write ("<option value="+cod_usuario[j]+">"+nom_username[j])+"</option>"
						}
					</script>
				</select></td>
	  </tr>
	  
	  
      <tr>
      <td style="text-align:right"  valign="top" height="25"><strong>&nbsp;�rg�o Atua��o:&nbsp;</strong></td>
		<td valign="top" colspan="3"><select name="codOrgaoAtu"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (h=0;h<cod_orgao_atu.length;h++) {
							document.write ("<option value="+cod_orgao_atu[h]+">"+sig_orgao_atu[h])+"</option>"
						}
					</script>
				</select></td>
				
      </tr>
      
      <tr>
      <td></td>
      <td colspan="3" valign="bottom" height="30"><button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('B',this.form);">	
          <img src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left"></button>
	  </td>
      </tr>
     
	
  </table>
</div>

<jsp:include page="Retornar.jsp" flush="true" />
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= AuditoriaBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.Form.codOrgao.length;i++){
		if(document.Form.codOrgao.options[i].value==njint){
			document.Form.codOrgao.selectedIndex=i;	  
		}
	}
}


// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= AuditoriaBeanId.getCodOrgaoAtuacao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.Form.codOrgaoAtu.length;i++){
		if(document.Form.codOrgaoAtu.options[i].value==njint){
			document.Form.codOrgaoAtu.selectedIndex=i;	  
		}
	}
}


</script>
</form>
</BODY>
</HTML>

