<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="RelatLotesProcBean" scope="session"
	class="REG.RelatLotesProcBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}table.comborda td {border: 1px solid #000000}
</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<jsp:useBean id="OrgaoBeanId" scope="session" class="ACSS.OrgaoBean" />
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int npag = 0,seq = 0, contLinha = 0;	
	Iterator it = RelatLotesProcBean.getDadosLote().iterator();
	REG.LinhaLoteDolBean relatorioImp  = new REG.LinhaLoteDolBean();
	int numMaxLinha = 0; 
	if( RelatLotesProcBean.getDados().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.LinhaLoteDolBean)it.next() ;
			   seq++; 
			   contLinha++;
			   if(RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS"))
			       numMaxLinha = 33;
			   else 
			       numMaxLinha = 55;
		       if (contLinha%numMaxLinha==1){
					npag++;							
					if (npag!=1){
%> 
		  		  <jsp:include page="rodape_impConsulta.jsp" flush="true" />
		  		  <div class="quebrapagina"></div>
<%					} %>		  		  
		  		  		  
		  		  <jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			        <jsp:param name="nPag" value= "<%=npag%>" />				
			      </jsp:include> 
		      	   <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="0" >  	   		
					  <tr><td height="5"></td></tr>
					  <tr>
					      <td width="6%" >&nbsp;&nbsp;<strong>&Oacute;rg&atilde;o:</strong>&nbsp;&nbsp;<%=OrgaoBeanId.getSigOrgao()%></TD>
					  </tr>   
					  <tr><td height="5"></td></tr>
  		         </table>	 
     
			      <table width="100%" border="0"  class="comborda" align="center" cellpadding="0" cellspacing="0" >  	   		
					  <tr> 	
						 <td width="80"  height="15" align="center" bgcolor="#999999"><strong>Seq</strong></td>
						 <td width="150" bgcolor="#999999" align="center"><strong>Auto</strong></td>
						 <td width="90"  bgcolor="#999999" align="center"><strong>Placa</strong></td>
						 <td width="100" bgcolor="#999999" align="center"><strong>Data Infra��o</strong></td>
						 <td width="100" bgcolor="#999999" align="center"><strong>Hora Infra��o</strong></td>
						 <td width="100" bgcolor="#999999" align="center"><strong>Munic�pio</strong></td>
						 <td bgcolor="#999999" align="center"><strong>C�d. Infra��o</strong></td>
		 		      </tr>
            	  </table>    
			  <%}%>


		    <table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">							
  				<tr> 
	 			    <td width="80"  align="center" style="line-height:15px;"><%=seq%></TD>
					<td width="150" align="center"><%=relatorioImp.getNumAuto()%></td>
					<td width="90"  align="center"><%=relatorioImp.getNumPlaca()%></td>
					<td width="100" align="center"><%=relatorioImp.getDataInfracao()%></td>
					<td width="100" align="center"><%=relatorioImp.getHoraInfracao()%></td>
					<td width="100" align="center"><%=relatorioImp.getCodMunicipio()%></td>
					<td align="center"><%=relatorioImp.getCodInfracao()%></td>
				</tr>
				<%if (RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS")) {%>
				<tr>
					<td colspan="7" style="padding-left:15px";>Erro:&nbsp;<%=relatorioImp.getDscErro()%></td>
				</tr>
				<%}%>
				<tr>
					<td colspan="7" bgcolor="#999999" style="padding-left:1px";></td>
				</tr>
				<tr>
					<td colspan="7" style="padding-left:20px";></td>
				</tr>
	        </table>
  <%  } %>
<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="0" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
   	      <td height="35" align=center bgcolor="#CCCCCC"><b><%= msg %></b></td>
		</tr>	
	</table>      			
<%} %>    

  <jsp:include page="rodape_consutaimp.jsp" flush="true" />

</form>
</body>
</html>