<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="EstatisticaBean" scope="request" class="REG.EstatisticaDigitalizaBean" />
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'visualizacao':
			fForm.acao.value=opcao;
	   		fForm.target= "_blank";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  break ;
   case 'R':
	      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


</script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifRelatorio" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 100px; left:50px;" > 
  <table width="100%" cellpadding="0" cellspacing="1" >
	 <script>
	<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
		for (var l=0;l<cod_orgao.length;l++) {
			document.write("<input name='todosOrgaosCod' type='hidden' value='"+cod_orgao[l]+"'>");
		}
	  </script>	  
      <tr>
	  	  <td width="143"></td>
	  	  <td width="208" valign="top"><strong>Fase dos Autos :</strong></td>
		  <td colspan="2" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<strong>&nbsp;&Oacute;rg&atilde;o :</strong>
			<select name="codOrgao"> 
			<script>
			<!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) {
				<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=UsuarioBeanId.getCodOrgaoAtuacao()%>"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
			</script>
			</select>									
		</td>
		<td width="2"></td>
		<td width="166" rowspan="3">        
			<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('visualizacao',this.form);">	
	        	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>						
		</td>
	</tr>
	<tr>	  	  
		<td width="143"></td>
		<td valign="top" width="208">
			<input name="statusAuto" type="radio" value="autuacao" class="sem-borda">Autua&ccedil;&atilde;o
			<input name="statusAuto" type="radio" value="penalidade" class="sem-borda">Penalidade
			<input name="statusAuto" type="radio" value="ambas" checked class="sem-borda">Ambas
		</td>
		<td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  <input type="checkbox" name="todosOrgaos" value="selecionado"  class="sem-borda"> 
		Todos os &Oacute;rg&atilde;os
		</td>
		<td></td>
	</tr>
	<tr><td  colspan="5" height="10"></td></tr>
	<tr>
		<td></td>
		<td colspan="5">  <b> De<strong>:</strong></b>
���        <input name="mesAnoInicio" type="text" size="12" maxlength="12" onkeypress="javascript:Mascaras(this,'99/99/9999')" onChange = "javascript:ValDt(this,0);" onFocus="javascript:this.select();" value="<%=EstatisticaBean.getDatIncio()%>" >&nbsp;&nbsp;&nbsp;<b>At� <strong>:</strong></b>
���        <input name="mesAnoFim" type="text" size="12" maxlength="12" onkeypress="javascript:Mascaras(this,'99/99/9999')" onChange = "javascript:ValDt(this,0);" onFocus="javascript:this.select();" value="<%=EstatisticaBean.getDatFim()%>" >&nbsp;&nbsp;&nbsp;		
        </td>
	  </tr>			
	<!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>




<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=EstatisticaBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>