<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="consultaBroker041"     scope="request" class="REG.ConsultaBrokerTrans041Bean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>DETRAN &#8226;Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;

     case 'retorna':
        fForm.verTable.value="N"	  
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;   
          
	  case 'N':
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;		  
  	  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function changeColorOver(obj) {	
	obj.style.background="#EFF5E2";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#DEEBC2";
	obj.style.textDecoration="none";
}

</script>
<style type="text/css">
<!--
.style2 {color: #FFFFFF}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaAuto" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>

 <div id="dadosTelaAntes" style="position:absolute; left:50px; top:75px; width:722px; height:48px; z-index:13; overflow: visible; visibility: visible;">  
   <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr   height="20" bgcolor="#EFF5E2">
       <td width="8%" > &nbsp;<strong>Dat Proc :</strong></td>
       <td width="23%"><input readonly name="datProc" type="text"  value="<%=consultaBroker041.getDatProc() %>" size="25" maxlength="1"
        style="border: 0px none; background-color:transparent;">
       </td>
	   <td width="12%" >&nbsp;<strong>Transa��o :</strong></td>
       <td width="57%"><input readonly name="codTransacao" type="text"  value="<%=consultaBroker041.getCodTransacaoSel() %>" size="6" maxlength="3" 
         style="border: 0px none; background-color:transparent;">
         -&nbsp;&nbsp;
         <input name="dscTransacao" type="text"  value="<%=consultaBroker041.getDscTransacaoSel() %>" size="58" maxlength="50" 
         style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
 </table>
</div>	 
<div id="parteFixaTitulo" style="position:absolute; left:50px; top:100px; width:356px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" height="10" bgcolor="#a8b980"><strong class="espaco2"><font color="#ffffff">&nbsp;&nbsp;&nbsp;PARTE FIXA</font></strong></td>
    </tr>
 </table>
</div>
<div id="parteFixa" style="position:absolute; left:49px; top:117px; width:357px; height:40px; z-index:13; overflow: auto; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr height="20" bgcolor="#EFF5E2">
       <td width="172" height="10"><strong>&nbsp;&nbsp;Seq Transa��o </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
         <input  readonly name="seqTransacao" type="text" value="<%=consultaBroker041.getSeqTransacao()%>" size="10" maxlength="6" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;C�digo Transa��o </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
        <input readonly name="codTransacao" type="text" value="<%=consultaBroker041.getCodTransacao()%>" size="6" maxlength="3" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;Usuario/Origem/tamanho </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
        <input readonly name="usrOrigTam" type="text"  value="<%=consultaBroker041.getUsrOrigTam()%>" size="30" maxlength="25"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;Dia Juliano</strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
        <input readonly name="diaJuliano" type="text"  value="<%=consultaBroker041.getDiaJuliano()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="5"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td  width="171"></td>
       <td  width="182"></td>
     </tr>
 </table>
</div>
<div id="parteVariavelTitulo" style="position:absolute; left:415px; top:99px; width:356px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" height="10" bgcolor="#a8b980"><strong class="espaco2 style2">&nbsp;&nbsp;&nbsp;PARTE VARI�VEL</strong></td>
    </tr>
 </table>
</div>
<div id="parteVariavel" style="position:absolute; left:415px; top:116px; width:356px; height:40px; z-index:13; overflow: auto; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
   <tr height="20" bgcolor="#EFF5E2">
       <td width="11%" height="10"><strong>&nbsp;&nbsp;Num Auto </strong></td>
       <td width="21%" height="10"><strong>:</strong>&nbsp;
         <input  readonly name="numAuto" type="text" value="<%=consultaBroker041.getNumAuto()%>" size="20" maxlength="12" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="15%" height="10"><strong>&nbsp;&nbsp;Num Placa </strong></td>
       <td width="26%" height="10"><strong>:</strong>&nbsp;
         <input readonly name="codTransacao" type="text" value="<%=consultaBroker041.getNumPlaca()%>" size="15" maxlength="7" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
      <td width="13%" height="10"><strong>&nbsp;&nbsp;C�digo �rg�o </strong></td>
       <td width="14%" height="10"><strong>:</strong>&nbsp;
         <input readonly name="codOrgao" type="text"  value="<%=consultaBroker041.getCodOrgao()%>" size="15" maxlength="6"
			style="border: 0px none; background-color:transparent;"></td>
    </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="11%" height="10"><strong>&nbsp;&nbsp;Num Processo</strong></td>
       <td width="21%" height="10"><strong>:</strong>&nbsp;
         <input readonly name="numProcesso" type="text"  value="<%=consultaBroker041.getNumProcesso()%>" size="25" maxlength="20"
			style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="15%" height="10"><strong>&nbsp;&nbsp;Num Notifica��o</strong></td>
       <td width="26%" height="10"><strong>:</strong>&nbsp;
         <input readonly name="numNotificacao" type="text"  value="<%=consultaBroker041.getNumNotificacao()%>" size="30" maxlength="20"
			style="border: 0px none; background-color:transparent;"></td>
     </tr>
   </table>
 </div>
<div id="resultadoTitulo" style="position:absolute; left:49px; top:165px; width:722px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" bgcolor="#a8b980"><strong class="espaco2 style2">&nbsp;&nbsp;&nbsp;RESULTADO</strong></td>
    </tr>
 </table>
</div>

<div id="resultado" style="position:absolute; left:48px; top:184px; width:722px; height:160px; z-index:13; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
   <tr height="20" bgcolor="#EFF5E2">
       <td width="172" height="10"><strong>&nbsp;C�digo Retorno </strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
         <input  readonly name="codRetorno" type="text" value="<%=consultaBroker041.getCodRetorno()%>" size="6" maxlength="3" 
		    style="border: 0px none; background-color:transparent;"></td>
   </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>&nbsp;Num Auto </strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="numAuto" type="text" value="<%=consultaBroker041.getnumAutoResult()%>" size="24" maxlength="12" 
		    style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
      <td width="170" height="10"><strong>C�digo Infra��o</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="codInfracao" type="text"  value="<%=consultaBroker041.getCodInfracao()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">
       </td>
    </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Dsc Infra��o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="dscInfracao" type="text"  value="<%=consultaBroker041.getDscIntracao()%>" size="75" maxlength="60"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Enquadramento</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="enquadramento" type="text"  value="<%=consultaBroker041.getEnquadramento()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2">       
       <td width="170" height="10"><strong>Pontos</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="pontos" type="text"  value="<%=consultaBroker041.getPontos()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Tipo</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="tipo" type="text"  value="<%=consultaBroker041.getTipo()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2">        
       <td width="170" height="10"><strong>Suspens�o Dirigir</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="supenDirigir" type="text"  value="<%=consultaBroker041.getSuspensaoDirigir()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Local Infra��o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="localInfracao" type="text"  value="<%=consultaBroker041.getLocalInfracao()%>" size="50" maxlength="45"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Dat Infra��o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="datInfracao" type="text"  value="<%=consultaBroker041.getDatInfracao()%>" size="16" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Hora Infra��o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="supenDirigir" type="text"  value="<%=consultaBroker041.getSuspensaoDirigir()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
      </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>C�digo Agente</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codAgente" type="text"  value="<%=consultaBroker041.getCodAgente()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>C�digo �rg�o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codOrgao" type="text"  value="<%=consultaBroker041.getCodOrgao()%>" size="12" maxlength="6"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Local Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="localAparelho" type="text"  value="<%=consultaBroker041.getLocalAparelho()%>" size="50" maxlength="40"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num Cert. Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCertAparelho" type="text"  value="<%=consultaBroker041.getNumCertAparelho()%>" size="20" maxlength="10"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2">        
       <td width="170" height="10"><strong>Data Ult. Afer. Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="dataUltAparelho" type="text"  value="<%=consultaBroker041.getDatUltAferAparelho()%>" size="16" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num Ident. Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numIdentAparelho" type="text"  value="<%=consultaBroker041.getNumIdentAparelho()%>" size="20" maxlength="15"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num Inmetro Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numInmetro" type="text"  value="<%=consultaBroker041.getNumInmetroAparelho()%>" size="15" maxlength="9"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Tipo Disp. Reg. Aparelho</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="tipoDispRegAparelho" type="text"  value="<%=consultaBroker041.getTipoDispRegAparelho()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Valor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="valor" type="text"  value="<%=consultaBroker041.getValor()%>" size="15" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Data Vencimento</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="datVencimento" type="text"  value="<%=consultaBroker041.getDatVencimento()%>" size="16" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>C�digo Munic�pio</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codMunicipio" type="text"  value="<%=consultaBroker041.getCodMunicipio()%>" size="8" maxlength="4"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Veloc. Permitida</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="veloPermitida" type="text"  value="<%=consultaBroker041.getVeloPermitida()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Veloc. Aferida</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="veloAferida" type="text"  value="<%=consultaBroker041.getVeloAferida()%>" size="8" maxlength="4"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Veloc. Considerada</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="veloConsiderada" type="text"  value="<%=consultaBroker041.getVeloConsiderada()%>" size="8" maxlength="4"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Situa��o</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="situacao" type="text"  value="<%=consultaBroker041.getSituacao()%>" size="30" maxlength="26"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="5"><strong>Placa Ve�culo</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="placaVeiculo" type="text"  value="<%=consultaBroker041.getNumPlaca()%>" size="10" maxlength="7"
			style="border: 0px none; background-color:transparent;">       </td>
      </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>C�digo Marca</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codMarca" type="text"  value="<%=consultaBroker041.getCodMarca()%>" size="10" maxlength="7"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>C�digo Esp�cie</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codEspecie" type="text"  value="<%=consultaBroker041.getCodEspecie()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>C�digo Categoria</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codCategoria" type="text"  value="<%=consultaBroker041.getCodCategoria()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>C�digo Tipo</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codTipo" type="text"  value="<%=consultaBroker041.getCodTipo()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>C�digo Cor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codCor" type="text"  value="<%=consultaBroker041.getCodCor()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Nome Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="nomResponsavel" type="text"  value="<%=consultaBroker041.getNomResp()%>" size="45" maxlength="40"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Tipo DOC Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="tipoDocResp" type="text"  value="<%=consultaBroker041.getTipoDocResp()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num CPF/CNPJ Resons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCpf" type="text"  value="<%=consultaBroker041.getNumCpfResp()%>" size="20" maxlength="14"
			style="border: 0px none; background-color:transparent;">       </td>
      </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Tipo CNH Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="tipoCnh" type="text"  value="<%=consultaBroker041.getTipoCNHResp()%>" size="50" maxlength="45"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num CHN Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCHNResp" type="text"  value="<%=consultaBroker041.getNumCNHResp()%>" size="13" maxlength="11"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Endere�o Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="endResp" type="text"  value="<%=consultaBroker041.getEndResp()%>" size="30" maxlength="27"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Complemento Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="compleResp" type="text"  value="<%=consultaBroker041.getComplementoResp()%>" size="14" maxlength="11"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Cod Munic�pio Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codMuniResp" type="text"  value="<%=consultaBroker041.getCodMunicipioResp()%>" size="6" maxlength="4"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>CEP Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="cepResp" type="text"  value="<%=consultaBroker041.getCepResp()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>UF Respons�vel</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="ufResp" type="text"  value="<%=consultaBroker041.getUfResp()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Nome Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="nomCondutor" type="text"  value="<%=consultaBroker041.getNomeCondutor()%>" size="45" maxlength="40"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Num CPF/CNPJ Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCpfCondutor" type="text"  value="<%=consultaBroker041.getNumCpfCondutor()%>" size="19" maxlength="11"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Tipo CNH Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="tipoCnhCondutor" type="text"  value="<%=consultaBroker041.getTipoCNHCondutor()%>" size="4" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Num CNH Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCnhCondutor" type="text"  value="<%=consultaBroker041.getNumCNHCondutor()%>" size="14" maxlength="11"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>End Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="endCondutor" type="text"  value="<%=consultaBroker041.getEndCondutor()%>" size="35" maxlength="27"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Num Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="numCondutor" type="text"  value="<%=consultaBroker041.getNumCondutor()%>" size="9" maxlength="5"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Complemento Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="cepResp" type="text"  value="<%=consultaBroker041.getComplementoCondutor()%>" size="15" maxlength="11"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Cod Munic�pio Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codMuniCondutor" type="text"  value="<%=consultaBroker041.getCodMunicipioCondutor()%>" size="6" maxlength="4"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>CEP Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="cepCondutor" type="text"  value="<%=consultaBroker041.getCepCondutor()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>UF Condutor</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="ufCondutor" type="text"  value="<%=consultaBroker041.getUfCondutor()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Cod Status</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codStatus" type="text"  value="<%=consultaBroker041.getCodStatus()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Data Status</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="datStatus" type="text"  value="<%=consultaBroker041.getDatStatus()%>" size="15" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Data Envio DO</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="datEnvioDo" type="text"  value="<%=consultaBroker041.getDatEnvioDo()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Cod Relatorio DO</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codRelatDO" type="text"  value="<%=consultaBroker041.getCodRelatorioDo()%>" size="25" maxlength="20"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Data Publi. DO</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="datPubliDO" type="text"  value="<%=consultaBroker041.getDatPublDo()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>IND_TP_CV</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="indTpCv" type="text"  value="<%=consultaBroker041.getInTpCv()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Num Processo</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="numProcesso" type="text"  value="<%=consultaBroker041.getNumProcessoResult()%>" size="24" maxlength="20"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
         <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Data Processo</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="datProcesso" type="text"  value="<%=consultaBroker041.getDatProcesso()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Valor Desc.</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="valDesc" type="text"  value="<%=consultaBroker041.getValorDesc()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
         <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Cod Result. Publi. DO</strong></td>
       <td width="76%"><strong>:</strong><input readonly name="codResultDO" type="text"  value="<%=consultaBroker041.getCodResultPubliDo()%>" size="3" maxlength="1"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Cod Barra</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="codBarra" type="text"  value="<%=consultaBroker041.getCodBarra()%>" size="65" maxlength="48"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
         <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Data Notif. Aut.</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
         <input readonly name="datNotifAut" type="text"  value="<%=consultaBroker041.getDatNotifAut()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Cod Ent. Atu.</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codEntAtu" type="text"  value="<%=consultaBroker041.getCodEntAut()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
         <tr height="20" bgcolor="#EFF5E2"> 
       <td width="170" height="10"><strong>Data Notif. Pen.</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
         <input readonly name="datNotifPen" type="text"  value="<%=consultaBroker041.getDatNotifPen()%>" size="14" maxlength="8"
			style="border: 0px none; background-color:transparent;">       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170" height="10"><strong>Cod Ent. Pen.</strong></td>
       <td width="76%" height="10"><strong>:</strong>&nbsp;&nbsp;&nbsp;
        <input readonly name="codEntPen" type="text"  value="<%=consultaBroker041.getCodEntPen()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
   </table>
</div>
 <!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<!--Bot�o retornar--> 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= consultaBroker041.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>