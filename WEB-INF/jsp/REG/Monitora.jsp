<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->  
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 

<!-- Carrega Arquivos -->
<jsp:useBean id="monitorId" scope="request" class="REG.MonitoraBean" /> 
<jsp:useBean id="monitor" scope="request" class="sys.MonitorLog" /> 
<jsp:setProperty name="monitor" property="j_abrevSist" value="sys" />  
<jsp:setProperty name="monitor" property="colunaValue" value="nom_robo"/>
<jsp:setProperty name="monitor" property="popupNome"   value="codMonitor"/>
<jsp:setProperty name="monitor" property="popupString" value="nom_robo, select distinct nom_robo FROM TSMI_Gerencia_robo ORDER BY nom_robo" />

<!-- Carrega Orgaos -->
<jsp:useBean id="orgaoId"     scope="request" class="ACSS.OrgaoBean" /> 
          
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
myOrg = new Array(<%= orgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}
function valida(opcao,fForm) {
   
   var edtDataIni = document.forms[0].edtDe;
   var edtDataFim = document.forms[0].edtAte;
   
   diaI = edtDataIni.value.toString().substring(0,2);
   mesI = edtDataIni.value.toString().substring(3,5);
   anoI = edtDataIni.value.toString().substring(6,10);   
   mesI = mesI -1;

   var dtIni = new Date();
   dtIni.setDate(diaI);
   dtIni.setMonth(mesI);
   dtIni.setYear(anoI);   

   diaF = edtDataFim.value.toString().substring(0,2);
   mesF = edtDataFim.value.toString().substring(3,5);
   anoF = edtDataFim.value.toString().substring(6,10);
   mesF = mesF - 1;  
   
   var dtFim = new Date();
   dtFim.setDate(diaF);
   dtFim.setMonth(mesF);
   dtFim.setYear(anoF);   
   
   diff 	= dtFim.getTime() - dtIni.getTime();
   periodo 	= Math.floor(diff/(24*60*60*1000));  

   if ((diaI > diaF)||(mesI > mesF) || (anoI > anoF)){
     alert("Faixa de dias inv�lida. \n");
     return false;
   }
      
   
   switch (opcao) {    
   
   case 'busca':
   		if(veCampos(fForm) == true){	 
       		fForm.acao.value=opcao;
	  		if (fForm.codMonitor.options[fForm.codMonitor.selectedIndex].value == 0){
				fForm.nomProcesso.value = "";
			}
			else{
			  fForm.nomProcesso.value = fForm.codMonitor.options[fForm.codMonitor.selectedIndex].text;
			}		
		
			fForm.target= "_self";
	  		fForm.action = "acessoTool";  
	  		fForm.submit();	 
   		}
	  	break;
	  
   case 'retorna':
        fForm.atualizarDependente.value="N"	  
        fForm.codOrgao.value = ""
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
 
   case 'R':
	 close() ;   
	 break;
	  		
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;        	
	edtAte = fForm.edtAte.value;
	edtDe = fForm.edtDe.value;
	
	if (!ValDt(fForm.edtAte,0)){
	   valid = false;
	   sErro = "Data Inv�lida. \n";
	}
	
	if (!ValDt(fForm.edtDe,0)){
	   valid = false;
	   sErro = "Data Inv�lida. \n";
	}
	
	if ((fForm.edtAte.value == "")&&(fForm.edtDe.value == "")) {
	  valid = false;
	  sErro = "A data de inicio e fim s�o obrigatorias. \n";
	}
	
	if (fForm.codOrgao.value ==0){
	    valid = false;
	    sErro = "Selecione o �rg�o.\n";
	 }
	 
	 if (fForm.codMonitor.value == 0){
	    valid = false;
	    sErro = "Selecione o robo.\n";
	 }

	if (valid==false) alert(sErro) 
	
	return valid;
}

</script>
<style type="text/css">
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 10px;
}
</style>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="emailForm" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="nomProcesso" type="hidden" value=''> 		
<input name="nomObjeto" type="hidden" value=''> 		
<input name="sigOrgao" type="hidden" value=''>
	

<input name="atualizarDependente" type="hidden" value=''>

<!-- Rodap�-->

<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 83px; left:50px; visibility: visible;" id="topo2" > 
<!--INICIO BOTOES-->  
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%" >
    <TR>
    <% if (((String)request.getAttribute("atualizarDependente")).compareTo("N")==0){%>
    	<td  width="93"><b>�rg�o Atua��o&nbsp;</b> </td>
		<td width="225" ><strong>:</strong> 
			<select name="codOrgao"> 
			  <script>
			    <!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) {
				<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=UsuarioBeanId.getCodOrgaoAtuacao()%>"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
			  </script>
			</select>	
	    </td>
	    <td width="57"><b>Robot&nbsp;</b></td>
		<td width="291" ><strong>:</strong> 
	        <jsp:getProperty name="monitor" property="popupString" />
	  	</td>  
	    <td align=left width=54>&nbsp;&nbsp;&nbsp;
       		<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);"  >	
    		<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
  	    </td>
	 </tr>
	 <tr>
         <td height="1" colspan="6"></td>
     </tr>
	 <tr>
	  	 <td><b> De</b></td>
	  	 <td ><strong>:</strong> 
		  	<input name="edtDe" type="text" size="14" maxlength="10" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=monitorId.getDatIni()%>" >
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	<b>At�</b>
	    	<strong>:</strong> 
	      	<input name="edtAte" type="text" size="14" maxlength="10" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=monitorId.getDatFim()%>">
	  	 </td>
  	 </TR>	
  	 <tr>
       <td height="1" colspan="5"></td>
	 </tr>
  </table>
</div>

<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 131px; left:50px; visibility: visible;" id="radios" > 
  <TABLE border="0"  cellpadding="0" cellspacing="0" width="100%">
  	<tr> 
   		<td width="93" ><b>Ordem</b></td>
		<td  ><strong>:</strong> 
        	<input name="ordem" type="radio"  value="O"  class="sem-borda"   style="height: 13px;"> Org�o &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  			<input name="ordem" type="radio"  value="A"  class="sem-borda"   style="height: 13px;"> Arquivo &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="ordem" type="radio"  value="R"  class="sem-borda"   style="height: 13px;"> Robot &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input name="ordem" type="radio"  class="sem-borda"   style="height: 13px;"  value="D" checked> Data &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
  	</tr>
  </table>       
</div>
       	<% }
			else { %>
<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 83px; left:50px; visibility: visible;" id="topo" > 
<!--INICIO BOTOES-->  
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
    <tr>
		<td ><b>�rg�o Atua��o&nbsp;</b></td>
		<td><strong>:</strong>
		    <input name="codOrgao" type="hidden" size="10" maxlength="6"  value="<%= orgaoId.getCodOrgao() %>"> 
		    <input disabled name="sigOrgao" type="text" size="20" maxlength="10"value="<%= orgaoId.getSigOrgao() %>" style="border: 0px none;" >
		</td>
		<td ><b>Robot&nbsp;</b></td>
		<td><strong>:</strong>
		  	<input name="codMonitor" type="hidden" size="6" maxlength="2"  value="<%= monitorId.getCodMonitor() %>"> 
		    <input disabled name="nomProcesso" type="text" size="60"  maxlength="50" value="<%=monitorId.getNomProcesso()%>" style="border: 0px none;">
		</td>
	</tr>
  </table>
</div>
<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 108px; left:50px; visibility: visible;" id="respostas" > 
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td  ><b>De</b></td>
	  	 <td><strong>:</strong> 
		  	<input disabled name="edtDe" type="text" size="14" maxlength="8" onchange="javascript:ValDt(this,0);" onkeypress="javascript:f_hora();" value="<%=monitorId.getDatIni()%>"  style="border: 0px none;">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        	<b>At�</b>
	    	<strong>:</strong> 
      	<input disabled name="edtAte" type="text" size="14" maxlength="8" onchange="javascript:ValDt(this,0);" onkeypress="javascript:f_hora();" value="<%=monitorId.getDatFim()%>"  style="border: 0px none;">  	    </td>
	</tr>	
	<tr>
       <td height="1" colspan="6"></td>
	</tr>
   </table>       
 </div>
 <div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 131px; left:50px; visibility: visible;" id="radios" > 
  <TABLE border="0"  cellpadding="0" cellspacing="0" width="100%">
  	<tr> 
   		<td width="93" ><b>Ordem:</b></td>
		<td ><strong>:</strong> 
        	<input name="ordem" disabled type="text"  value="<%=monitorId.getOrdem() %>"  style="border: 0px none;" >
 		</td>
  	</tr>
  </table>       
</div>
 
 <div style="position:absolute; left:50px; top:155px; width:720px; height:10px; z-index:1; overflow: visible; visibility: visible;" id="titulos"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr bgcolor="#a8b980"> 
		<td width="150"> <div align="center"><font color="#ffffff"><strong>Robot</strong></font></div></td>
	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Objeto Monitorado</strong></font></div></td>
	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Org�o</strong></font></div></td>
  	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Data/Hora</strong></font></div></td>
        <td > <div align="center"><font color="#ffffff"><strong>Tipo</strong></font></div></td>
	</tr>
	<tr bgcolor="#a8b980"> 
       <td  colspan="5" align="center"><font color="#ffffff"><strong class="espaco2">MENSAGEM DE EXECU��O</strong></font></td>	    
	</tr>		  
  </table>
</div>
 	  
 <div style="position:absolute; width:720px; overflow: auto; z-index: 3; top: 186px; left: 50px; height: 132px; visibility: visible;" id="tabConteudo">   
   <table width="100%" border="0" cellpadding="1" cellspacing="1" id="fconsulta"  >  	   		
	<%
        Iterator it = monitorId.getDados().iterator() ;
		int seq = 1;	
		REG.MonitoraBean dados =  new REG.MonitoraBean() ;   
        while (it.hasNext()) {
			// lista os dados		
			dados = (REG.MonitoraBean)it.next() ;
	%>       
		<tr bgcolor="#DEEBC2"> 
			 <td width="150">&nbsp;<%= dados.getNomProcesso()%></TD>
			 <td width="150">&nbsp;<%= dados.getNomObjeto()%></TD>
			 <td width="150">&nbsp;<%= dados.getSigOrgao()%></TD>	
 			 <td width="150">&nbsp;<%= dados.getDatLog()%></TD>	
		     <td    >&nbsp;<%= dados.getCodTipoMensagem()%></TD>	
 		</tr>	 
		<tr bgcolor="#EFF5E2">
			<td  colspan="5">&nbsp;<font color="<%= dados.getCorMsg()%>"><%= dados.getTxtMensagem()%></font></TD>	
		</tr>
		<tr ><td height="2" colspan="5" bgcolor="#A8B980"></td></tr>
	
 	   <% 

	   } 
	   %>	
       </tr>
   	</table>
  </div> 
 <div style="position:absolute; left:50px; top:323px; width:720px; height:36px; z-index:1; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;" id="botRetornar"> 
 	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	
 		<tr>
			<td  align = "center"> 
	  		      <button type=button NAME="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('retorna',this.form);" >
					  <img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >
				  </button>
			</td>
		</tr>
 	</table>
 </div>     
 
 <% } %>
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= monitorId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  

<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= orgaoId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.emailForm.codOrgao.length;i++){
		if(document.emailForm.codOrgao.options[i].value==njint){
			document.emailForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>	
</form>
</BODY>
</HTML>
