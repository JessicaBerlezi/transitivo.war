<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<jsp:useBean id="ArquivoDolBeanId" scope="request" class="REG.ArquivoDolBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
</script>
</head>
 
<%
 	Vector vetConsulta = (Vector) ConsultaArquivoDOLBeanId.getVetConsulta(); 
%>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">
<!--IN�CIO_CORPO_sistema-->
<!--conte�do da tabela-->
     <%
    	int tam=vetConsulta.size();
    	int linha=0;
    	int contLinha=0;		
		int npag = 0;
    	
      	for(int i=0;i<tam;i++) {
      		REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();      		
      		LinhaBean=(REG.LinhaLoteDolBean)vetConsulta.get(i);
      		LinhaBean.setInd_P59(ArquivoDolBeanId.getDscPortaria59());     		
      		
      		contLinha++;
			//Faz quebra de linha a cada x=30 linhas, exceto na primeira linha.
			if (contLinha%30==1){
				npag++;			   
				if (npag!=1){			
		%>
					<jsp:include page="rodape_impconsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
				<% } %>				
	<jsp:include page="cabecalhoLoteDOL.jsp" flush="true" >	
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
	<!--conte�do da tabela-->
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >     
    <% } %>	
       	<tr>       
	       	<td>
		       	<table width="100%" border="0" cellpadding="1" cellspacing="1">
		       	<tr>
			        <td width="35" height="15" align="center"><%=i+1%></td>
			        <td width="150"><%=LinhaBean.getNumAuto()%></td>
			        <td width="70" align="center"><%=LinhaBean.getNumPlaca()%></td>
			        <td width="90" align="center"><%=LinhaBean.getDataInfracao()%></td>
			        <td width="90" align="center"><%=LinhaBean.getHoraInfracao()%></td>
			        <td width="60" align="center"><%=LinhaBean.getCodMunicipio()%></td>
			        <td align="center"><%=LinhaBean.getCodInfracao()%></td>
			    </tr>
			    <% if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("REJEITADOS")) {%>
		      	<tr>       
		      		<td colspan="7" style="padding-left:15px";>Erro:&nbsp;<%=LinhaBean.getDscErro()%></td>
		      	</tr>
		      	<% }else if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("CANCELADOS")) {%>
		      	<tr>      
		      		<td colspan="7" style="padding-left:15px";>Motivo:&nbsp;<%=LinhaBean.getMotivoCancelamento()%></td>
		      	</tr>
		      	<%}%>
		      	</table>
			</td>
		</tr>
      	<tr>
        	<td height="1" bgcolor="#cccccc"></td>
      	</tr>      
      <%} //loop
     %>
    </table>
</form>
</body>
</html>