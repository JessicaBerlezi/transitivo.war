<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.text.DecimalFormat"%>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'visualizacao':
   		if(fForm.prazo.value == "")
   			alert("Digite um N�mero");
   		else{
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  	}
	  break ;
   case 'retornarFiltro':
		fForm.acao.value="";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;   
   case 'detalhe':
		fForm.acao.value=opcao;
		fForm.totalMesAno.value=totalMesAno;
	   	fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function funcDetalhe(opcao,fForm,selecao,quantidade){
	if(quantidade == '0'){
		alert("Status n�o possui notifica��es a serem apresentadas !")
		return
	}
	document.all["detalhe"].value = selecao
	valida(opcao,fForm)
}

function setaTituloData(tituloData)
{
	document.all["tituloData"].innerHTML = tituloData;
}


</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifRelatorio" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="detalhe" type="hidden" value=' '>	
<% String statusInteracao = (String)request.getAttribute("statusInteracao"); %>
<input name="statusInteracao" type="hidden" value='<%=statusInteracao%>'>	
<input name="codStatusAuto" type="hidden" value='<%=(String)request.getAttribute("statusAuto")%>'>
<input name="sitEntrega" type="hidden" value='<%=(String)request.getAttribute("sitEntrega")%>'>

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="top: 75px; left:50px; width:720px; position:absolute; overflow: visible; z-index: 1; " > 
  <table width="100%" cellpadding="0" cellspacing="1" >
	<script>
	<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
	for (var l=0;l<cod_orgao.length;l++) 
	{
		document.write("<input name='todosOrgaosCod' type='hidden' value='"+cod_orgao[l]+"'>");
	}
	</script>	      

	<tr>
		<td width="150"></td>
		<td width="120"> <strong>&Oacute;rg&atilde;o: </strong></td>
		<td width="80" align="left">
			<input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
			<input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left"> <strong> Fase dos Autos:</strong> </td>
		<td>
			<%
			String statusAuto = (String) request.getAttribute("statusAuto");
			if(statusAuto.equalsIgnoreCase("autuacao"))
				statusAuto = "Autua��o";
			%>
			<input disabled name="statusAuto" type="text" size="20" value="<%=statusAuto.toUpperCase()%>">
		</td>
	</tr>
	<tr>
		<td></td>
		<td align="left"> <strong>Prazo de Entrega: </strong></td>
		<td align="left">
			<input disabled name="txtPrazoDigitado" size="5" value="<%=(String)request.getAttribute("prazo")%>">
		</td>
	</tr>
  </table>
</div>

<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
<input type="hidden" name="prazoDigitado" value="<%=(String)request.getAttribute("prazo")%>">
<div id="div01" style="left:200px; top:155px; width:400px; height:50px; position:absolute; z-index:1; overflow: auto; visibility: visible;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		<tr bgcolor="#deebc2"> 
			<td width="150" height="15" align="center"><strong>M&ecirc;s</strong></td>
			<td width="100" align="center"><strong>Ano</strong></td>
			<td width="100" align="center"><strong>Total</strong></td>
			<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		</tr>
	</table>
</div>

<div id="div13" style="position:absolute; left:200px; top:170px; width:400px; height:155px; z-index:5; overflow: auto; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
	<%
	Map meses = (Map) request.getAttribute("meses");
	List totalAnosMeses = (List) request.getAttribute("totalAnosMeses");
	String valorFormatado = "";
	DecimalFormat formato = new DecimalFormat(",##0");
	for (Iterator iter = totalAnosMeses.iterator(); iter.hasNext();) 
	{
		String totalMesAno = (String) iter.next();
		valorFormatado = formato.format(Integer.parseInt(totalMesAno.substring(8).trim()));
	%>
		<tr bgcolor="#EDF4DF">
			<td width="150" align="center"><%=meses.get(totalMesAno.substring(0, 2))%></td>
			<td width="100" align="center"><%=totalMesAno.substring(3, 7)%></td>
			<td width="100" align="center"><%=valorFormatado%></td>
			<td align="center">        
				<button type="button" NAME="check" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('detalhe',this.form,'<%=totalMesAno%>');">	
				<IMG src="<%= path %>/images/bot_detalhe_ico.gif" alt="Clique aqui para ver detalhamento" align="left" ></button>						
			</td>
		</tr>
	<%
	}
	%>
  </table>    
  <input type="hidden" name="totalMesAno" value="">  
</div>

<div id="div13" style="position:absolute; left:200px; top:330px; width:400px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
  <table width="100%"cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td align="center">
			<button  style=" top: 350px; border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript: valida('retornarFiltro',this.form);">
				<img src="<%=path%>/images/bot_retornar_det1.gif"  width="71px" height="26px" style="cursor: hand;" > </button>
		</td>
	</tr>
  </table>
</div>


<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=OrgaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>