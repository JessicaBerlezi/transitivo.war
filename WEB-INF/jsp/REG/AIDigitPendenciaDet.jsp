<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %> 
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.util.Map"%>
<%@ page import="REC.AutoInfracaoBean" %>
<%@ page import="sys.Util" %>

<jsp:useBean id="autoInfracaoId"  scope="request" class="REC.AutoInfracaoBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html> 
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
	 switch (opcao) {  
	 	 case 'GeraExcel':
			fForm.acao.value=opcao
			fForm.target= "_self";
			fForm.action = "geraXlsAIDigitPendencia";  
			fForm.submit();  		  
	        break;	   
	   case 'R':
			close();
			break;
  	   case 'visualizacao':
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  		break ; 		  
	  break ;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function validaDetalhe(opcao,fForm) {
	document.all["acao"].value="detalhe";
	document.all["sentido"].value=opcao;
	document.all["linhaForm"].target= "_self";
	document.all["linhaForm"].action = "acessoTool";  
	document.all["linhaForm"].submit();
}
</script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
 
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="linhaForm" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="codOrgao" value="<%=(String)request.getAttribute("codOrgao")%>">
<input type="hidden" name="statusAuto" value="<%=(String)request.getAttribute("statusAuto")%>">
<input type="hidden" name="prazo" value="<%=(String)request.getAttribute("prazo")%>">
<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
<input type="hidden" name="totalMesAno" value="<%=(String)request.getAttribute("totalMesAno")%>">
<input type="hidden" name="prazoDigitado" value="<%=(String)request.getAttribute("prazoDigitado")%>">
<input type="hidden" name="primeiroAnterior" value="<%=(String)request.getAttribute("primeiroAnterior")%>">
<input type="hidden" name="numPag" value="<%=(String)request.getAttribute("numPag")%>">
<input type="hidden" name="sentido" value="">
<input type="hidden" name="seq" value="<%=(String) request.getAttribute("seq")%>">
<input name="nomArquivo"  type="hidden" value='Pendencia_digitalizacao_Auto.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>

<%
String totalMesAno = (String)request.getAttribute("totalMesAno");
%>
<div id="div13" style="position:absolute; left:200px; top:85px; width:578px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="100%"cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <%
	    Map meses = (Map) request.getAttribute("meses");
	    String valorFormatado = "";
	    DecimalFormat formato = new DecimalFormat(",##0");
	    valorFormatado = formato.format(Integer.parseInt(totalMesAno.substring(8).trim()));
	    %>
	   	<td width="53%"><strong>
	   			&nbsp;&nbsp;M&ecirc;s:&nbsp;<%=meses.get(totalMesAno.substring(0, 2))%>
				&nbsp;&nbsp;Ano:&nbsp;<%=totalMesAno.substring(3, 7)%>
				&nbsp;&nbsp;Total:&nbsp;<%=valorFormatado%>&nbsp;&nbsp;</strong>
        </td>  
	   <%
	   	Iterator it = autoInfracaoId.getAutos().iterator();
	   %>
        <td width="11%"  align=center><br>
        	<button type="button" NAME="ok" style="width: 28px; height: 21px; border: none; background: transparent;" onClick="javascript: validaDetalhe('Imprimir',this.form);">
				<img src="<%= path %>/images/bot_imprimir_ico.gif" align="left" alt="Imprimir">
			</button>			
		</td>
        <td width="36%"   align=left><br>
		   <button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form);"> 	
				<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
	    </button>	    </td>
	</table>
</div>
<div id="div01" style="position:absolute; left:40px; top:121px; width:740px; z-index:1; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="1">
		<tr bgcolor="#deebc2"> 
  		    <td width="30" height="15" align="center"><strong>Seq</strong></td>
         	<td width="80" align="center"><strong>Auto</strong></td>
        	<td width="80" align="center"><strong>&Oacute;rg&atilde;o</strong></td>
        	<td width="70" align="center"><strong>Placa</strong></td>
        	<td width="80" align="center"><strong>Infra&ccedil;&atilde;o</strong></td>
        	<td width="80" align="center"><strong>Registro</strong></td>
        	<td align="center"><strong>Status</strong></td>
		</tr>
    </table>
</div>

<%
	AutoInfracaoBean autoInfracao = new AutoInfracaoBean();
	int i = 0;
	int cont = Integer.parseInt((String)request.getAttribute("seq"));
%>
<div id="div13" style="position:absolute; left:40px; top:136px; width:740px; height:196px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="100%" border="0" cellpadding="0" cellspacing="1">
<%
		while (it.hasNext()) 
		{     		
  			autoInfracao =(AutoInfracaoBean)it.next();
  			if (i == 0)
  			{
%>
   				<input type="hidden" name="primeiroRegistro" value="<%=autoInfracao.getNumAutoInfracao()%>">
<%   				
  			}
  			if (i == (autoInfracaoId.getAutos().size() - 1))
  			{
%>      				
  				<input type="hidden" name="ultimoRegistro" value="<%=autoInfracao.getNumAutoInfracao()%>">
<%      				
			}
%>
	   		<tr bgcolor="#EDF4DF">
	   			<td width="30" height="18" align="center">
					<%=cont%>
				</td>
			    <td width="80" align="center">
			   		<%=autoInfracao.getNumAutoInfracao()%>
			    </td>
			    <td width="80" align="center">
			   		<%=autoInfracao.getOrgao().getSigOrgao()%>
			   	</td>
			    <td width="70" align="center">
			    	<%=autoInfracao.getNumPlaca()%>
			    </td>
			<%
			if(autoInfracao.getDatInfracao() == null)
			{
			%>
				<td width="80" align="center"></td>
			<%			
			}
			else
			{
			%>
			    <td width="80" align="center">
			    	<%=autoInfracao.getDatInfracao()%>
		    	</td>
			<%			
			}
			%>

			<%
			if(autoInfracao.getDatRegistro() == null)
			{
			%>
				<td width="80" align="center"></td>
			<%			
			}
			else
			{
			%>
			    <td width="80" align="center">
			    	<%=autoInfracao.getDatRegistro()%>
		    	</td>
			<%			
			}
			%>
			    <td align="left">&nbsp;&nbsp;
			    	<%=Util.lPad(autoInfracao.getCodStatus(), "0", 3)%> - <%=autoInfracao.getNomStatus()%>
			    </td>
	  </tr>
<%      		
			i++;
			cont++;
		}
%>
<%
      	if(autoInfracaoId.getAutos().size()==0)
      	{
%>
		<div id="div14" style="position:absolute; left:45px; top:140px; width:740px; height:207px; z-index:5; overflow: auto; visibility: visible;"> 
		  <table width="100%" border="0" cellpadding="0" cellspacing="1">     
			<tr bgcolor="#EDF4DF">
				<td><strong>Nenhum notifica��o existente para o status</strong></td>
			</tr>
		  </table>
		</div>	
<%
		}
%>
	</table>
</div>

<div id="div14" style="position:absolute; left:40px; top:336px; width:740px; z-index:5; overflow: auto; visibility: visible;"> 
    	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		  <tr bgcolor="#EDF4DF">
			   <td colspan="6" align=center>
			     <% if( cont <= Integer.parseInt(totalMesAno.substring(8).trim())){%> 
			     <a href="#" onclick="javascript: validaDetalhe('Prox',this.form);"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>Pr&oacute;xima P&aacute;gina </font></a>&nbsp;&nbsp;&nbsp;
			     <% } %>
			     <% String pag = (String)request.getAttribute("numPag");
			        if (!pag.equals("1")){ %>
			     <a href="#" onclick="javascript: validaDetalhe('Ant',this.form) ;"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>P&aacute;gina Anterior</font></a>&nbsp;&nbsp;&nbsp;	  
			     <% } %>
		    	 <a href="#" onclick="javascript: valida('R',this.form);  "><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>Retornar       </font></a>	  
	      	   </td>
	      </tr>
	   </table>
</div>
<!--FIM conte�do da tabela-->

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</body>
</html>