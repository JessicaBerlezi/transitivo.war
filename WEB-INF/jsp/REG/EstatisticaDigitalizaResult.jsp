<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator"%>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="EstatisticaBean" scope="request" class="REG.EstatisticaDigitalizaBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'R':
        close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


</script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifRelatorio" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
 <div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 89px; left:50px; visibility: visible;" > 
   <table width="100%" cellpadding="0" cellspacing="1" >
		<tr>
			<td width="62"></td>
			<td width="92">
				<strong>&Oacute;rg&atilde;o </strong>
		    </td>
			<td width="560">:
				<input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
				<input disabled class="sem-borda" name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
			</td>
		</tr>
		<tr>
			<td width="62"></td>
		</tr>
		<tr>
			<td width="62"></td>
			<td><strong>Fase dos Autos</strong></td>
			<td >:&nbsp;
				<%
				String statusAuto = (String) request.getAttribute("statusAuto");
				if(statusAuto.equalsIgnoreCase("autuacao"))
					statusAuto = "Autua��o";
				%>
				<input disabled  class="sem-borda" name="statusAuto" type="text" size="20" value="<%=statusAuto.toUpperCase()%>">
			</td>
		</tr>
		<tr><td  colspan="5" height="10"></td></tr>
		<tr>
			<td width="62"></td>
			<td ><b>De</b></td>
			<td>:&nbsp; <input disabled class="sem-borda" name="mesAnoInicio" type="text" size="12" maxlength="12" value="<%=EstatisticaBean.getDatIncio()%>" >&nbsp;&nbsp;&nbsp;<b>At� <strong>:</strong></b>
���           <input disabled class="sem-borda" name="mesAnoFim" type="text" size="12" maxlength="12"  value="<%=EstatisticaBean.getDatFim()%>" >&nbsp;&nbsp;&nbsp;		
            </td>
		</tr>
	</table>
 </div>
 <div id="div01" style="position:absolute; left:50px; top:183px; width:720px; z-index:1; overflow: auto; visibility: visible;"> 
 	<table width="100%" border="0" cellpadding="0" cellspacing="1">
         <tr  bgcolor="#EDF4DF">
		     	<td colspan="2" align="center"><strong>REGISTRADOS</strong></td>
    			<td colspan="2" align="center"><strong>EMITIDOS</strong></td>
				<td colspan="2" align="center"><strong>DIGITALIZADOS</strong></td>
				<td colspan="2" align="center"><strong>AR EMITIDOS </strong></td>
				<td colspan="2" align="center"><strong>AR DIGITALIZADOS </strong></td>
    	</tr>
		<tr bgcolor="#deebc2"> 
     				<td width="72" align="center" style="font-size: 7pt">MR01</td>
     				<td width="72" align="center" style="font-size: 7pt">DOL</td>
					<td width="72" align="center" style="font-size: 7pt">Emitidos</td>
     				<td width="72" align="center" style="font-size: 7pt">Pend&ecirc;ncias</td>
					<td width="72" align="center" style="font-size: 7pt">Digitalizados</td>
     				<td width="72" align="center" style="font-size: 7pt">N&atilde;o Digitalizados </td>
					<td width="72" align="center" style="font-size: 7pt">AR Entregue </td>
     				<td width="72" align="center" style="font-size: 7pt">AR N&atilde;o Entregue </td>
					<td width="72" align="center" style="font-size: 7pt">AR Digitalizados </td>
     				<td width="72" align="center" style="font-size: 7pt">AR N&atilde;o Digitalizados </td>

		</tr>
		</tr>
	</table>
 </div>
 <div id="div13" style="position:absolute; left:50px; top:219px; width:720px; z-index:5; overflow: auto; visibility: visible;"> 
  	  <table width="100%" border="0" cellpadding="0" cellspacing="1">
	    <tr>
        	<td height="1"  colspan="10" bgcolor="#cccccc"></td>
	    </tr>
        <tr>
			<td width="72" bgcolor="#EDF4DF" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdMR01()%></td>
			<td width="72" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdDOL()%></td>
			<td width="72" bgcolor="#EDF4DF" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdEmitidos()%></td>
			<td width="72" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdPendencia()%></td>
			<td width="72" bgcolor="#EDF4DF" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdDigitalizados()%></td>
			<td width="72" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdNaoDigitalizados()%></td>
			<td width="72" bgcolor="#EDF4DF" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdArRecebido()%></td>
			<td width="72" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdArNaoRecebido()%></td>
			<td width="72" bgcolor="#EDF4DF" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdArDigitalizados()%></td>
			<td width="72" align="center" style="font-size: 7pt"><%=EstatisticaBean.getQtdArNaoDigitalizados()%></td>
		</tr>
		<tr>
		  <td height="1"  colspan="10" bgcolor="#cccccc"></td>
		</tr>
	  </table>
  </div>
  <div id="div13" style="position:absolute; left:50px; top:273px; width:720px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
		<table width="100%"cellspacing="0" cellpadding="0" border="0">
		      <tr>
		       <td align="center">
				   <button  style=" top: 350px; border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript: valida('R',this.form);">
				   <img src="<%=path%>/images/bot_retornar_det1.gif"  width="71px" height="26px" style="cursor: hand;" > </button>
			   </td>
		     </tr>
 		</table>
  </div>

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=OrgaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>