<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:useBean id="AuditoriaBeanId" scope="session" class="GER.AuditoriaBean" /> 
<jsp:useBean id="AuditoriaBean_050Id" scope="request" class="GER.AuditoriaBean_050" /> 

<html>
<head>
<%@ include file="Css.jsp" %>


<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="Form" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

function valida(opcao,fForm,posicao) {
 switch (opcao) {
   case 'Prox':
		document.Form.acao.value=opcao
		document.Form.target= "_self";
		document.Form.action = "acessoTool";  
		document.Form.submit();	  		  
	  break ; 
   case 'Ant':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
	case 'R':  
	  close() ;   
      break;      
      case 'Imprimir':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;            
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
	 case 'Informacoes':
	 document.Form.acao.value=opcao;
	 document.Form.posicao.value=posicao;	 
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	 	   
	 break;
  }
}


function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}
// fim de validar combo
for (k=0;k<fForm.nomDescricao.length;k++) {
   var ne = trim(fForm.nomDescricao[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.nomDescricao.length;m++) {
	   var dup = trim(fForm.nomDescricao[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Processo em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
 
</script>
<input name="acao" type="hidden" value=' '>	
<input name="posicao" type="hidden" value=' '>			
<% if (AuditoriaBeanId.getDados().size() > 0) {%>
<%@ include file="AuditoriaLista.jsp" %>



<!-- cabe�alho da transa��o -->
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 115px; left: 50px; height: 18px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>
	  <td width="92" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>Data Proc. </strong></font></td>
      <td width="135" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; DO AUTO </strong></font></td>
	  <td width="85" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; DA PLACA </strong></font></td>
	  <td width="138" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>C&Oacute;D. ORG&Atilde;O LOTA&Ccedil;&Atilde;O </strong></font></td>
      <td width="100" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>Num.Notifica��o</strong></font></td>
      <td width="70" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>C�d.Baixa</strong></font></td>
      <td            bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>Data Receb. </strong></font></td>
    </tr>
  </table>
</div>   
<!-- fim  cabe�alho da transa��o -->
				  
<!-- Registros c/ barra de scroll . Aqui v�o entrar os includes-->
<div style="position:absolute; width:720px; overflow: auto; z-index: 100; top: 134px; left: 50px; height: 181px; visibility: visible;">   
	<%
		
		
        int max = AuditoriaBeanId.getDados().size() ;
		for(int i=0;i<max;i++){
		
		   AuditoriaBean_050Id.setPropriedades(AuditoriaBeanId.getDadosArray(i));
		
		
	%>       

	 <!-- Registros c/ barra de scroll -->

 
 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
   <tr>
     <td width="92">&nbsp;&nbsp;<strong><%=AuditoriaBeanId.getDatProcRef()%></strong></td>
     <td width="135">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumAutoInfracao()%></strong></td>
     <td width="85">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumPlaca()%></strong></td>
     <td width="138">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getSigOrgao()%></strong></td>
     <td width="100">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumNotificacao()%></strong></td>
     <td width="70">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getCodBaixa()%></strong></td>
     <td width="70">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getDataRecebimento()%></strong></td>
    
   </tr>
 </table>
 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1"> 
 <tr> 	
 <td  style="text-align: left; line-height:17px">&nbsp;&nbsp;Desc. Erro :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=AuditoriaBean_050Id.getDscErro()%></td> 
 </tr>
 <tr>
     <td bgcolor="#00000" height="2" ></td>
  </tr>
 </table>
<!-- fim Registros c/ barra de scroll --> 
	<%
        }		
	%>       
 
</div>
<!-- fim Registros c/ barra de scroll -->


<!-- Navega��o-->
<%@ include file="Auditoria_Nav.jsp" %>
<!-- Fim Navega��o-->
<%} else{%>

<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 115px; left: 50px; height: 18px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>
      <td width="100%" height="38" align="center" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>N�O EXISTEM DADOS A SEREM EXIBIDOS </strong></font></td>
	  
	</tr>
  </table>
</div>   

<%}%>
<%@ include file="Retornar_Diretiva.jsp"%>
<!-- Rodap�-->
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
</form>
</BODY>
</HTML>


