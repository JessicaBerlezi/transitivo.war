<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	

<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<!-- Chama o Objeto da Tabela de Parametros de Sistema -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'visualizacao':
   		if(fForm.prazo.value == "")
   			alert("Digite um prazo de entrega.");
   		else{
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  	}
	  break ;
   case 'retornarFiltro':
		fForm.acao.value="";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;   
   case 'detalhe':
		fForm.acao.value=opcao;
		fForm.totalMesAno.value=totalMesAno;
	   	fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
	  if (fForm.statusInteracao.value!="1") {
		history.back()
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function funcDetalhe(opcao,fForm,selecao,quantidade){
	if(quantidade == '0')
	{
		alert("Status n�o possui notifica��es a serem apresentadas!")
		return
	}
	document.all["detalhe"].value = selecao
	valida(opcao,fForm)
}

function setaTituloData(tituloData)
{
	document.all["tituloData"].innerHTML = tituloData;
}


</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifRelatorio" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="detalhe" type="hidden" value=' '>	
<% String statusInteracao = (String)request.getAttribute("statusInteracao"); %>
<input name="statusInteracao" type="hidden" value='<%=statusInteracao%>'>	
<input name="codStatusAuto" type="hidden" value='<%=(String)request.getAttribute("statusAuto")%>'>

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 100px; left:50px;" > 
  <table width="100%" cellpadding="0" cellspacing="1" >
	<script>
	<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
	for (var l=0;l<cod_orgao.length;l++) 
	{
		document.write("<input name='todosOrgaosCod' type='hidden' value='"+cod_orgao[l]+"'>");
	}
	</script>	      
<%
if(statusInteracao.equals("1"))
{
%>
	<tr>
		<td width="148"></td>
		<td width="250" valign="top">&nbsp;&nbsp;<strong>Fase dos Autos:</strong></td>
		<td width="100" align="right"><strong>&nbsp;&Oacute;rg&atilde;o:&nbsp;</strong></td>
		<td width="100" valign="top">
			<select name="codOrgao" onChange="javascript:if(this.value==0){document.forms[0].todosOrgaos.checked=true;}else{document.forms[0].todosOrgaos.checked=false;}"> 
				<option>&nbsp;</option>
			<%			
			String checkedTodosOrgaos = "";
			String selTodosOrgaos = (String)request.getParameter("todosOrgaos");
			
			if (selTodosOrgaos==null)
				checkedTodosOrgaos = "checked";
			else if (selTodosOrgaos.equalsIgnoreCase("selecionado"))
				checkedTodosOrgaos = "checked";

			String codOrgaoEscolhido = request.getParameter("codOrgao");
			if (codOrgaoEscolhido !=null && checkedTodosOrgaos.equalsIgnoreCase(""))
				UsuarioBeanId.setCodOrgaoAtuacao(codOrgaoEscolhido);
			%>
			   
			<script>
			<!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) 
				{
				<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=UsuarioBeanId.getCodOrgaoAtuacao()%>")
					{
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else
					{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
			</script>
			</select>									
		</td>
		<td rowspan="2" align="left">        
			<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('visualizacao',this.form);">	
				<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>						
		</td>
	</tr>
	<tr>	  	  
		<td></td>
		<td valign="top">
		<%
			String checkedAutuacao = "";
			String checkedPenalidade = "";
			String checkedAmbas = "";			
			
			String codStatusAuto = (String)request.getParameter("codStatusAuto");
			if (codStatusAuto==null)
				checkedAmbas = "checked";
			else if (codStatusAuto.equalsIgnoreCase("autuacao"))
				checkedAutuacao = "checked";
			else if (codStatusAuto.equalsIgnoreCase("penalidade"))
				checkedPenalidade = "checked";
			else if (codStatusAuto.equalsIgnoreCase("ambas"))
				checkedAmbas = "checked";
		%>
			<input name="statusAuto" type="radio" value="autuacao" <%=checkedAutuacao%> class="sem-borda">Autua&ccedil;&atilde;o
			<input name="statusAuto" type="radio" value="penalidade" <%=checkedPenalidade%> class="sem-borda">Penalidade
			<input name="statusAuto" type="radio" value="ambas" <%=checkedAmbas%> class="sem-borda">Ambas
		</td>
		<td></td>
		<td>
			<input type="checkbox" name="todosOrgaos" 
				value="selecionado"  class="sem-borda" style="width: 13px" onClick="if(this.checked){ document.forms[0].codOrgao.value=0;}else{document.forms[0].codOrgao.selectedIndex=1;}"> 
			Todos os &Oacute;rg&atilde;os
		</td>		
	</tr>
	<tr><td colspan="5">&nbsp;</td></tr>
	<tr>
		<td></td>
		<td colspan="3" valign="top">&nbsp;
			<strong>Prazo de Entrega:</strong>		
			<input name="prazo" type="text" size="5" maxlength="3"
    	   			value="<%=request.getParameter("prazoDigitado")!=null ? request.getParameter("prazoDigitado") : ParamSistemaBeanId.getParamSist("PRAZO_REL_NOTIFICACOES")%>"
	            	onkeypress="javascript:f_num();" >			&nbsp;Dias </td>
		<td></td>
	</tr>
	<tr>	  	  
		<td width="148"></td>
		<td valign="top" width="250">&nbsp;
		</td>
		<td align="left"></td>
		<td align="left"></td>
		<td align="left"></td>
	</tr>
	<!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
<%
}
%>
  </table>
</div>


<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=OrgaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>