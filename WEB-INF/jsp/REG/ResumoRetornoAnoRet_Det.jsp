<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>

<!-- Chama o Objeto da Tabela -->
<jsp:useBean id="ResumoRetornoPagBeanId" scope="session" class="REG.ResumoRetornoPagBean" /> 

<html>
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.linhas td{border: 1px solid #ff0000;}
</style>
<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">

<form name="Form" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao, fForm, numNotif, numAuto, seqNotif) {
 switch (opcao) {
   case 'Prox':
		document.Form.acao.value=opcao
		document.Form.target= "_self";
		document.Form.action = "acessoTool";  
		document.Form.submit();	  		  
	  break ; 
   case 'Ant':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'Imprimir':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'GravaTXT':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'ARDigitalizado':
		document.Form.numNotificacao.value=numNotif;
		document.Form.numAutoInfracao.value=numAuto;
		document.Form.seqNotif.value=seqNotif;
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
	case 'R':  
	  close() ;   
      break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
  }
}


function download(nomArquivo,fForm) {
	fForm.nomArquivo.value = nomArquivo;
   	fForm.target = "_self";
    fForm.action = "download";
    fForm.submit();
}


</script>

<input name="acao" type="hidden" value=' '>
<input name="codArquivo"  type="hidden" value=''>
<input name="nomArquivo"  type="hidden" value=''>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_RECEBIDO'>
<input name="extensao"    type="hidden" value=''>
<input name="numNotificacao"    type="hidden" value=''>
<input name="numAutoInfracao"    type="hidden" value=''>
<input name="seqNotif"    type="hidden" value=''>
				
<%@ include file="ResumoRetornoAnoRet_Cab.jsp" %>

<!-- cabe�alho da transa��o -->
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 115px; left: 50px; height: 18px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>
      <td width="30" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>SEQ</strong></font></td>
      <td width="110" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; NOTIFICA��O</strong></font></td>
	  <td width="100" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; AUTO</strong></font></td>
	  <td width="100" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>DATA RETORNO </strong></font></td>
	<% if ((ResumoRetornoPagBeanId.getAcao().equals("detalheResumo") && ResumoRetornoPagBeanId.getDscTransacao().equals("RECUPERADO P/ ENTREGA")) || ResumoRetornoPagBeanId.getDscTransacao().equals("CR�DITO")) { %>	
	  <td width="200" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>COD. RETORNO </strong></font></td>
	<% } %>	
	  <td width="100" height="18" bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>TIPO </strong></font></td>
	  <td bgcolor="#a8b980"><font color="#ffffff">&nbsp;&nbsp;<strong>AR Dig. </strong></font></td>
	</tr>
  </table>
</div>   
<!-- fim  cabe�alho da transa��o -->

<!-- Registros c/ barra de scroll . Aqui v�o entrar os includes-->
<div style="position:absolute; width:720px; overflow: auto; z-index: 100; top: 134px; left: 50px; height: 181px; visibility: visible;">   
	 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
	<%
	if (ResumoRetornoPagBeanId.getDados().size() > 0){	
		int i = 0;
        Iterator itOrg = ResumoRetornoPagBeanId.getDados().iterator() ;
		REG.ControleVexBean myCont =  new REG.ControleVexBean() ;
        
      while (itOrg.hasNext()) {
			i++;
			myCont = (REG.ControleVexBean)itOrg.next() ;
		
		
	%>       
	   <tr>
	     <td width="30">&nbsp;&nbsp;<%=i %></td>
	     <td width="110">&nbsp;&nbsp;<%= myCont.getNumNotificacao() %> <%= "".equals(myCont.getNumAutoInfracao()) ? " - " + Util.lPad(myCont.getSeqNotificacao(),"0",3) : "" %> </td>
	     <td width="100">&nbsp;&nbsp;<%= myCont.getNumAutoInfracao() %></td>
	     <td width="100">&nbsp;&nbsp;<%= myCont.getDatRetorno() %></td>
	<% if ((ResumoRetornoPagBeanId.getAcao().equals("detalheResumo") && ResumoRetornoPagBeanId.getDscTransacao().equals("RECUPERADO P/ ENTREGA")) || ResumoRetornoPagBeanId.getDscTransacao().equals("CR�DITO")) { %>	
	     <td width="200">&nbsp;&nbsp;<%= myCont.getCodRetorno() %> - <%=myCont.getDscCodRetorno()%></td>
	<% } %>	
	     
	     <td width="100">&nbsp;&nbsp;<%= "1".equals(myCont.getTipNotificacao()) ? "AUTUA��O" : "PENALIDADE" %></td>
	     <td >&nbsp;&nbsp;
			    <% if ( myCont.getTemARDigitalizado()) { %>					
					  <button style="border: 0px; background-color: transparent; height: 21px; width: 35px; cursor: hand;" type="button" onClick="javascript:valida('ARDigitalizado',this.form,<%= myCont.getNumNotificacao() %>,'<%= myCont.getNumAutoInfracao() %>','<%= myCont.getSeqNotificacao() %>');"> 
		              <img src="<%= path %>/images/REC/bot_AR_dig_vm_ico.gif" alt="Visualizar AR Digitalizado" width="33" height="19"></button>
		        <% } else { %>
					  <button style="border: 0px; background-color: transparent; height: 21px; width: 35px; cursor: hand;" type="button"> 
		              <img src="<%= path %>/images/REC/bot_AR_dig_ico.gif" alt="Visualizar AR Digitalizado" width="33" height="19"></button>
				<% } %>
	     
	     </td>
	   </tr>
	   <tr> 
		 <td bgcolor="#00000" height="2" colspan="6"></td>
	   </tr>

      <%}%>

<%}else {%>

	   <tr>
	     <td width="30">&nbsp;&nbsp;</td>
	     <td width="110">&nbsp;&nbsp;</td>
	     <td width="100">&nbsp;&nbsp;</td>
	     <td width="100">&nbsp;&nbsp;</td>
	<% if ((ResumoRetornoPagBeanId.getAcao().equals("detalheResumo") && ResumoRetornoPagBeanId.getDscTransacao().equals("RECUPERADO P/ ENTREGA")) || ResumoRetornoPagBeanId.getDscTransacao().equals("CR�DITO")) { %>	
	     <td width="200">&nbsp;&nbsp;</td>
	<% } %>	
	     <td width="100">&nbsp;&nbsp;</td>
	     <td >&nbsp;&nbsp;</td>
	   </tr>
	   <tr> 
		 <td bgcolor="#00000" height="2" colspan="6"></td>
	   </tr>
<%}%>

	 </table>
 
 </div>



<%if (ResumoRetornoPagBeanId.getDados().size() > 0){%>	
    <%@ include file="ResumoRetornoAnoRet_Nav.jsp" %>
<%}%>

<%@ include file="Retornar_Diretiva.jsp" %>
<!-- Rodap�-->
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
String msgOk         = "";
String msgErro       = ResumoRetornoPagBeanId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>


