<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="sys.Util" %>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0"; 
   String tituloConsulta = (String)request.getAttribute("tituloConsulta");%>

<jsp:useBean id="acompPendEmitevexBeanId"  scope="request" class="REG.acompPendEmitevexBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0,npag = 0,seq = 0;	
  	String valorFormatado1="0",valorFormatado2="0",valorFormatado3="0",valorFormatado4="0",valorFormatado5="0",valorFormatado6="0";
	String valor1="0",valor2="0",valor3="0",valor4="0",strqtdpend = "0",strqtdpendZero = "0",valor5="0",valor6="0";
  	String totEnv="";	
	String totRet="";
	String dataAtual="";
	String mesAtual="";
	String mesAnterior="";
	String ano="";	
	 long qtdPendZero 			=0;
	 long qtdEnvZero  			=0;
	 long qtdRetZero  			=0;
	 long qtdPend 				=0;
	 long qtdEnv  				=0;
	 long qtdRet  				=0;
	 long pend  				=0;	
	 
	 long totalPendencia=0;  
	 int quantArqPendZero=0; 
	 DecimalFormat formato = new DecimalFormat(",##0") ;
		
	
	Iterator it = acompPendEmitevexBeanId.getListaArqs().iterator();
	REG.acompPendEmitevexBean relatorioImp  = new REG.acompPendEmitevexBean();					
	if( acompPendEmitevexBeanId.getListaArqs().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.acompPendEmitevexBean)it.next() ;
			   dataAtual = relatorioImp.getDatRecebimento();	
			   mesAtual=relatorioImp.getDatRecebimento().substring(3,5);
			   ano=relatorioImp.getDatRecebimento().substring(6,10);
			   contLinha++;	  
			   
			   qtdPend= Integer.parseInt(relatorioImp.getQtdEnviada()) - Integer.parseInt(relatorioImp.getQtdRetornada());
			   strqtdpend =  String.valueOf(qtdPend)	   ;
			  try {  
					 	   valor1 = relatorioImp.getQtdEnviada();
					 	   if( (valor1 != null)||(valor1.equals("")) )  {						   		
						   		valorFormatado1 = formato.format(Long.parseLong(valor1)); 
						   }else valorFormatado1 = "0";				   
						  
					 	   valor2 = relatorioImp.getQtdRetornada();
					 	   if( (valor2 != null)||(valor2.equals("")) )  {						   		
						   		valorFormatado2 = formato.format(Long.parseLong(valor2)); 
						   }else valorFormatado2 = "0";
						   
						   valor3 = strqtdpend;
					 	   if( (valor3 != null)||(valor3.equals("")) )  {						   		
						   		valorFormatado3 = formato.format(Long.parseLong(valor3)); 
						   }else valorFormatado3 = "0";
						   
						   
						   
					}
					catch(Exception e){};		   
			  
			  
			  
			  
			  
			  
			   if (contLinha%35==1){
					npag++;
					if (npag!=1){
%>
						<jsp:include page="Rod_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
				<% } %>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
			<tr > 	
				 <td width="40px"   align="center"  bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="290px"  align="center"  bgcolor="#999999"><font color="#000000"><b>ARQUIVO</b></font></td>
				 <td width="94px"   align="center"   bgcolor="#999999"><font color="#000000"><b>DATA ENVIO</b></font></td>		 
				 <td width="72px"   align="center"   bgcolor="#999999"><font color="#000000"><b>QTD ENV.</b></font></td>					 
				 <td width="72px"   align="center"   bgcolor="#999999"><font color="#000000"><b>QTD RET.</b></font></td>
				 <td                align="center"  bgcolor="#999999"><font color="#000000"><b>QTD PEND.</b></font></td>
			 </tr>
		  </table>     
         <% } %>
		<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand" width="100%" border="0" style="border-style: none">										   
			  <% if(mesAnterior.equals(mesAtual) == false  && seq>1 ){	%>		
					
					
				<tr>
			      <td height="20" colspan="3" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Total de Arquivos sem Pend�ncia : <%=quantArqPendZero%></strong>
			      </td>
				  <td width="72"  align="center" bgcolor='#cccccc'     style="line-height:20px;"><%=formato.format(qtdEnvZero)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRetZero)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(qtdPendZero)%></td>	
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>
								
				<tr>
			      <td height="20" colspan="3" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Totais&nbsp;<%=Util.mesPorExtenso(mesAnterior)%>&nbsp;-&nbsp;<%=ano%>&nbsp;:&nbsp;&nbsp;</strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'  style="line-height:20px;"><%=formato.format(qtdEnv)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRet)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(pend)%></td>				
				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>	
				
				<tr>
			      <td height="5" colspan="6" bgcolor="#ffffff">   </td>
				
				</tr>			
				
					
<%				
				qtdEnv  =0;
	 			qtdRet  =0;
	 			pend    =0;
	 			qtdEnvZero    =0;
	 			qtdRetZero    =0;
	 			qtdPendZero   =0;
	 			quantArqPendZero =0;
				
				%>	
						
				<tr>
			      <td height="20" colspan="6"  bgcolor='#cccccc'>
			        <strong>&nbsp;&nbsp;<%=Util.mesPorExtenso(mesAtual)%>&nbsp;-&nbsp;<%=ano%></strong>
			      </td>				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>						
				<%}%>	
							
				<%if (contLinha==1){%>				
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>	
				<tr >
			      <td height="20" colspan="6"  bgcolor='#cccccc'>
			        <strong>&nbsp;&nbsp;<%=Util.mesPorExtenso(mesAtual)%>&nbsp;-&nbsp;<%=ano%></strong>
			      </td>
				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>							
				<%}%>
				<%
				qtdEnv = qtdEnv + Long.parseLong(relatorioImp.getQtdEnviada());				
			    qtdRet = qtdRet + Long.parseLong(relatorioImp.getQtdRetornada());	
			    pend = pend + qtdPend;
			    
			    if (qtdPend == 0){
			    	qtdEnvZero    = qtdEnvZero+Long.parseLong(relatorioImp.getQtdEnviada());
	 				qtdRetZero    = qtdRetZero+Long.parseLong(relatorioImp.getQtdRetornada());
	 				qtdPendZero   = qtdPendZero + (Long.parseLong(relatorioImp.getQtdEnviada())-Long.parseLong(relatorioImp.getQtdRetornada()));
			    	quantArqPendZero++;
			    }
			    
			    try {  
					 	   valor4 = relatorioImp.getQtdEnviada();
					 	   if( (valor4 != null)||(valor4.equals("")) )  {						   		
						   		valorFormatado4 = formato.format(Long.parseLong(valor4)); 
						   }else valorFormatado4 = "0";				   
						  
					 	   valor5 = relatorioImp.getQtdRetornada();
					 	   if( (valor5 != null)||(valor5.equals("")) )  {						   		
						   		valorFormatado5 = formato.format(Long.parseLong(valor5)); 
						   }else valorFormatado5 = "0";
						   
						   valor6 = String.valueOf(qtdPendZero);
					 	   if( (valor6 != null)||(valor6.equals("")) )  {						   		
						   		valorFormatado6 = formato.format(Long.parseLong(valor6)); 
						   }else valorFormatado6 = "0";
						   
						   
						   
					}
					catch(Exception e){};			    
			    
			   %>
		
		<% if (!"0".equals(valorFormatado3) ){	seq++;%>
			<tr> 
				<td width="40"  align="center"    style="line-height:20px;"><%= seq %></TD>		 		 	  
				<td width="290" align="center"    style="line-height:20px;"><%= relatorioImp.getNomArquivo()%></td>
				<td width="94"  align="center"    style="line-height:20px;"><b><%= relatorioImp.getDatRecebimento()%></b></TD>	
				<td width="72"  align="center"    style="line-height:20px;"><%=valorFormatado1%></TD>					
				<td width="72"  align="center"    style="line-height:20px;"><%= valorFormatado2%></td>				
				<td               align="center"    style="line-height:20px;"><%=valorFormatado3%></td>				
				<input name="codArquivo" type="hidden" value='<%=relatorioImp.getCodArquivo()%>'>	
			</tr>		
			
			<tr><td height="1" bgcolor="#000000" colspan="6"></td></tr>
	  
	  <%}%>  
	  	<% 
	  	mesAnterior=mesAtual;				
				  totalPendencia = pend;
				  %>	
	   <% } %>
	   
	   
		   <tr>
			      <td height="20" colspan="3" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Total de Arquivos sem Pend�ncia : <%=quantArqPendZero%></strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'     style="line-height:20px;"><%=formato.format(qtdEnvZero)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRetZero)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(qtdPendZero)%></td>	
				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>		
	   
	   
				<tr>
			      <td height="20" colspan="3" bgcolor='#cccccc'>
			       <strong><font color="#000000">&nbsp;&nbsp;Totais&nbsp;<%=Util.mesPorExtenso(mesAtual)%>&nbsp;-&nbsp;<%=ano%>&nbsp;:&nbsp;&nbsp;</font></strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'  style="line-height:20px;"><%=formato.format(qtdEnv)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRet)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(pend)%></td>				
				
				</tr>	
				
				
				
				
				<tr>
			      <td height="5" colspan="6" bgcolor="#ffffff">   </td>
				
				</tr>			
	   
	   
	  </table>     	 
	  
<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%}%>	
		<jsp:include page="Rod_impConsulta.jsp" flush="true" />

</form>
</body>
</html>