<!--M�dulo : GER
	Vers�o : 1.1
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="AuditoriaBeanId" scope="session" class="GER.AuditoriaBean" /> 
<jsp:useBean id="AuditoriaBean_050Id" scope="request" class="GER.AuditoriaBean_050" /> 
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<%@ include file="Css.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 

<%
  	int contLinha=0;
	int npag = 0;
	int seq = 0;	
	boolean passa = true;	
    int max = AuditoriaBeanId.getDados().size() ;%>
    
	<%if (max>0) { 
		for(int i=0;i<max;i++){
		
		    AuditoriaBean_050Id.setPropriedades(AuditoriaBeanId.getDadosArray(i));
			seq++;
			contLinha++;
			
			if (contLinha%15==1){
				npag++;							
				if (npag!=1){
				passa = true;
		%>			 					
				<%@ include file="rodape_impConsulta_Diretiva.jsp" %>
				<div class="quebrapagina"></div>
				<% } %>				
			<%@ include file="Cab_impConsulta_Diretiva.jsp" %>				
         <% } %>
	<%if(passa){%>
		<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">		
	    	<tr>
	      		<td colspan="4" style="text-align: left; line-height:17px;"><strong style="display: block; float: left; margin-bottom: -2px">Registros de Auditoria do Evento <%=AuditoriaBeanId.getCodTransacao() %> - <%=AuditoriaBeanId.getDscTransacao() %></strong></td>
	      	</tr>
	    	<tr>
	        	<td width="6%" style="text-align: left; line-height:17px"><strong>Per&iacute;odo:&nbsp;</strong></td>
				<td width="18%">de <%=AuditoriaBeanId.getDatInicio() %> a<strong> &nbsp;</strong><%=AuditoriaBeanId.getDatFim() %></td>            
	        		
	     		
<% if(!AuditoriaBeanId.getNomUserName().equals("")){ %>
			<td width="5%" style="text-align: left; line-height:17px"><strong>Usu�rio:&nbsp;</strong></td>
			<td width="27%"><%=AuditoriaBeanId.getNomUserName() %>&nbsp;/&nbsp;<%=AuditoriaBeanId.getSigOrgao() %></td>	 
<% }else{ %>
			<td width="5%" style="text-align: left; line-height:17px"><strong>Usu�rio:&nbsp;</strong></td>
			<td width="27%">&nbsp;Todos</td>	 
<% } %>
	        		
			</tr>	
	  	</table>
	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	    <tr>
		  <td width="92" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>Data Proc. </strong></font></td>
	      <td width="135" height="18" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; DO AUTO </strong></font></td>
		  <td width="85" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>N&ordm; DA PLACA </strong></font></td>
		  <td width="138" height="18" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>C&Oacute;D. ORG&Atilde;O LOTA&Ccedil;&Atilde;O </strong></font></td>
	      <td width="100" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>Num.Notifica��o</strong></font></td>
	      <td width="70" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>C�d.Baixa</strong></font></td>
	      <td width="70" bgcolor="#cccccc"><font color="#ffffff">&nbsp;&nbsp;<strong>Data Receb. </strong></font></td>
	     
	    </tr>
    </table>
  	<% } %>	
 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
 <tr>
    <td width="92">&nbsp;&nbsp;<strong><%=AuditoriaBeanId.getDatProcRef()%></strong></td>
     <td width="135">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumAutoInfracao()%></strong></td>
     <td width="85">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumPlaca()%></strong></td>
     <td width="138">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getSigOrgao()%></strong></td>
     <td width="100">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getNumNotificacao()%></strong></td>
     <td width="70">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getCodBaixa()%></strong></td>
     <td width="70">&nbsp;&nbsp;<strong><%=AuditoriaBean_050Id.getDataRecebimento()%></strong></td>
    
   </tr>
 </table>
 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1"> 
 <tr> 	
 <td  style="text-align: left; line-height:17px">&nbsp;&nbsp;Desc. Erro :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=AuditoriaBean_050Id.getDscErro()%></td> 
 </tr>
 <tr>
     <td bgcolor="#00000" height="2" ></td>
  </tr>
 </table>
<% passa=false;} %>

<% }else{%>	
<table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b>N�o existem dados a serem retornados </b></td>
		</tr>	
</table>      	

<%}%>
<%	if (AuditoriaBeanId.getDados().size()>0) { 
		if (contLinha<54){

		} %>
		<%@ include file="rodape_impConsulta_Diretiva.jsp" %>
<%} %>
</form>
</body>
</html>