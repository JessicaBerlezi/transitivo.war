<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="REG.NotifControleBean"%>

<jsp:useBean id="NotifControleId"  scope="request" class="REG.NotifControleBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>
<title>:: TRANSITIVO ::</title>
<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
	DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->
<%
  	int contLinha=0,npag = 0,seq = 0;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	Iterator it = NotifControleId.getListaNotifs().iterator();
	NotifControleBean notifControle = new NotifControleBean();
	
	if (NotifControleId.getListaNotifs().size() > 0) {				
		while (it.hasNext()) {
			   notifControle =(NotifControleBean)it.next();
			   seq++; contLinha++;
			   if (contLinha%54==1){
					npag++;
					if (npag!=1){
%>
						<jsp:include page="Rod_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
				<% } %>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
			<tr>
				 <td width="4%"  align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="11%" align="center" bgcolor="#999999"><font color="#000000"><b>NOTIFICA&Ccedil;&Atilde;O</b></font></td>
				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>AUTO</b></font></td>		 
				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>PLACA</b></font></td>					 
				 <td width="28%" align="center" bgcolor="#999999"><font color="#000000"><b>ARQ. RECEBIDO</b></font></td>
 				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>EMISS&Atilde;O</b></font></td>
				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>ENVIO</b></font></td>
				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>ENTREGA</b></font></td>
				 <td  align="center" bgcolor="#999999"><font color="#000000"><b>USU&Aacute;RIO</b></font></td>
			</tr>
		</table>
		 <% } %>
	<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
		<tr> 
			<td width="4%"  align="center" style="line-height:20px;"><%= seq %></TD>		 		 	  
			<td width="11%" align="center" style="line-height:20px;"><b><%= notifControle.getNumNotificacao()%></b></td>
			<td width="9%"  align="center" style="line-height:20px;"><%= notifControle.getNumAutoInfracao()%></TD>
			<td width="9%" align="center"  style="line-height:20px;"><%= notifControle.getNumPlaca()%></td>
			<td width="28%"  align="center" style="line-height:20px;"><%=notifControle.getNomArquivo(notifControle.getCodArquivoEnvio())%></TD>

			<%	if(notifControle.getDatEmissao() == null){%>
				<td width="9%"  align="center" style="line-height:20px;"></TD>
			<%	}else{%>
			    <td width="9%"  align="center" style="line-height:20px;"><%=sdf.format(notifControle.getDatEmissao())%></TD>
			<%	}%>
		

			<%	if(notifControle.getDatEnvio() == null){%>
				<td width="9%"  align="center" style="line-height:20px;"></TD>
			<%	}else{%>
			    <td width="9%"  align="center" style="line-height:20px;"><%=sdf.format(notifControle.getDatEnvio())%></TD>
			<%	}%>	 		 	  

			<%	if(notifControle.getDatEntrega() == null){%>
				<td width="9%"  align="center" style="line-height:20px;"></TD>
			<%	}else{%>
			    <td width="9%"  align="center" style="line-height:20px;"><%=sdf.format(notifControle.getDatEntrega())%></TD>
			<%	}%>	 		 	  

			<td align="center"   style="line-height:20px;"><%= notifControle.getNomUsuarioEnvio()%>/<%= notifControle.getSigCodOrgao(notifControle.getCodOrgaoEnvio())%></td>
		</tr>			
		<tr><td height=1 bgcolor="#000000" colspan=10></td></tr>
		<%	} %>
	</table>

<%} else { 
	String msg=(String)request.getAttribute("semLog"); if (msg==null) msg="Nenhuma notifica&ccedil;&atilde;o existente";
%>

<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
<%	if (NotifControleId.getListaNotifs().size() > 0) { 
		if (contLinha<54){

		} %>
		<jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
</form>
</body>
</html>