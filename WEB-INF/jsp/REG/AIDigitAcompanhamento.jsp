<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="${path}/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%=OrgaoId.getOrgaos(UsuarioBeanId)%>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) 
{
 switch (opcao) {
   case 'ImprimeRelatorio':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'GeraExcel':
	  if (veCampos(fForm)==true){
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "geraXlsAIDigitAcompanhamento";  
		fForm.submit();  		  
	  }
	  break ;
   case 'R':
			close();
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.dataIni.value==0) {  valid = false
	   sErro += "Data in�cio n�o informada. \n"
	}
	if (fForm.dataFim.value==0) {  valid = false
	   sErro += "Data fim n�o informada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="nomArquivo"  type="hidden" value='AIDigitAcompanhamento.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>

<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 144px; left: 50px; z-index:1; visibility: visible;" > 
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="75%" align="center">
	<tr>
		<td align="right"><b>Org&atilde;o<strong>:</strong></b></td>
	  	<td> 
	  		&nbsp;
			<select name="codOrgao"> 
			<script>
			for (var j=0;j<cod_orgao.length;j++) {
					<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="${UsuarioBeanId.codOrgaoAtuacao}"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}   
					else{
						<%	String orgaosDigi = ParamSistemaBeanId.getParamSist("ORGAOS_RESP_DIGITALIZACAO");
						 	int qtdOrgaos = Integer.parseInt(orgaosDigi.substring(0,1));
							int cont = 2;
							int codOrgao = 0;
							for(int i = 0; i< qtdOrgaos; i++)
							{
								codOrgao = Integer.parseInt(orgaosDigi.substring(cont,cont+6));
                         %>  
							
						if(cod_orgao[j].indexOf("<%= codOrgao%>") >= 0)
					    {
						   document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
						<%
							  cont += 7;
							}
						%>	
					}
				}

			</script>
			</select>
		</td>
		<td align="right"><b>Data Inicial<strong>:</strong></b></td>
	  	<td> 
			&nbsp;&nbsp;<input name="dataIni" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<fmt:formatDate value="${AIDigitBeanId.datInicial}" pattern="dd/MM/yyyy"/>" >
		</td>
		<td align="right"><b>Data Final<strong>:</strong></b></td>
	  	<td> 
			&nbsp;&nbsp;<input name="dataFim" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<fmt:formatDate value="${AIDigitBeanId.datFinal}" pattern="dd/MM/yyyy"/>" >
		</td> 	
		<td align="left">&nbsp;&nbsp;
			<button type="button" NAME="ok" style="width: 28px; height: 21px; border: none; background: transparent;" onClick="javascript: valida('ImprimeRelatorio',this.form);">
				<img src="${path}/images/bot_imprimir_ico.gif" align="left" >
			</button>	
		</td>
		<td align="left">&nbsp;&nbsp;
				<button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form);"> 	
					<IMG src="${path}/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
				</button>
		</td>
	</tr>
	
</table>
</div>     

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "${AIDigitBeanId.msgErro}" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
</form>
</body>
</html>