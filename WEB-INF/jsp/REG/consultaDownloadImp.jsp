<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaDownloadBeanId" scope="request" class="REG.ConsultaDownloadBean" /> 
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<script>
<%
 	String arrIdentArquivos[][] = (String[][])request.getAttribute("arrIdentArquivos");
%>
myIdent = new Array ();
<% 	
 	for (int i=0;i<arrIdentArquivos.length;i++) {
 		%> 		
 		myIdent[<%=i%>]= new Array("<%=arrIdentArquivos[i][0]%>","<%=arrIdentArquivos[i][1]%>");
 	<%}
%>


myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

</script>
</head>

<%
 	Vector vetArquivos = (Vector) ConsultaDownloadBeanId.getVetArquivos(); 

%>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<!--IN�CIO_CORPO_sistema-->

<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0;		
	int npag = 0;
	Iterator ot = ConsultaDownloadBeanId.getVetArquivos().iterator() ;
	REG.ArquivoRecebidoBean ArqRecBean = new REG.ArquivoRecebidoBean();
	if (ConsultaDownloadBeanId.getVetArquivos().size()>0) { 
		while (ot.hasNext()) {
			ArqRecBean   = (REG.ArquivoRecebidoBean)ot.next() ;
			contLinha++;
			//Faz quebra de linha a cada x=8 linhas, exceto na primeira linha.
			if (contLinha%30==1){
				npag++;			
				if (npag!=1){			
		%>
						</table>      
					<jsp:include page="rodape_impconsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
				<% } %>				
	<jsp:include page="cabecalho_impconsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
<!--conte�do da tabela-->
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >     
 <% } %>	
 
     <%
    	String nomArquivo, nomArquivoGrav, nomArquivoRej, status, codArquivo, datGrav, datRej;
    	String corArq, corGrav, corRej;
    	
    	int qtdReg, qtdLinhaErro;
      		nomArquivo=ArqRecBean.getNomArquivo();
      		codArquivo=ArqRecBean.getCodArquivo();
      		status=ArqRecBean.getCodStatus();
      		try {
      			qtdReg=Integer.parseInt(ArqRecBean.getQtdRegRecebido());
      		}
      		catch (Exception e) {
      			qtdReg=0;
      		}
      		try {
      			qtdLinhaErro=Integer.parseInt(ArqRecBean.getQtdLinhaErro());
      		}
      		catch (Exception e) {
      			qtdLinhaErro=0;
      		}
      		
      		if (status.equals("9")) {
      			corArq="Rejeitado";
      		}
      		else if (status.equals("8") || status.equals("7") || status.equals("1")) {
      			corArq="Em processamento";
      		}
      		else if (status.equals("2")) {
      			corArq="Processado";
      		}
      		else {
      			corArq="Recebido";
      		}
      		
      		
      		nomArquivoGrav="";
      		nomArquivoRej="";
      		datGrav="";
      		datRej="";
      		
      		if (status.equals("2")) {
      			if (qtdReg>qtdLinhaErro && !ArqRecBean.getSufixoRetornoGrv().equals("") ) {
      				nomArquivoGrav=ArqRecBean.getCodIdentRetornoGrv() + "..." + " (" + (qtdReg-qtdLinhaErro) + " Reg)";
      				if (ArqRecBean.getDatDownloadGravado()!=null && !ArqRecBean.getDatDownloadGravado().equals("")) {
	      				datGrav=ArqRecBean.getDatDownloadGravado();

      				}
      			}
      			if (qtdLinhaErro>0) {
      				nomArquivoRej=ArqRecBean.getCodIdentRetornoRej() + "..." + " (" + qtdLinhaErro + " Reg)";
      				if (ArqRecBean.getDatDownloadErro()!=null && !ArqRecBean.getDatDownloadErro().equals("")) {
	      				datRej=ArqRecBean.getDatDownloadErro();

      				}
      			}
      		}
      %>
      <tr> 
        <td width="213" height="15"><%=nomArquivo + " (" + qtdReg + " Reg)"%><br><%=corArq%></td>
        <td width="57" align="center"><%=ArqRecBean.getDatRecebimento()%></td>
        <td width="2"></td>
        <td width="145" height="15"><%=nomArquivoGrav%></td>
        <td width="61" align="center"><%=datGrav%></td>
        <td width="2"></td>
        <td width="145" height="15"><%=nomArquivoRej%></td>
        <td align="center"><%=datRej%></td>
      </tr>
      <tr> 
        <td height="1" colspan="8"  bgcolor="#cccccc"></td>
      </tr>
      
      <%} %>
    </table>
<%} %>
<!--FIM conte�do da tabela-->

</form>
</body>
</html>