<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="LinhaArquivoRecId" scope="request" class="REG.LinhaArquivoRec" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'Voltar':
            fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   	
			break;
      case 'ImprimeDetalheArq':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
		case 'retornar':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}


</script>
</head>
 


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />

<input name="acao"     type="hidden" value=' '>
<input name="codArquivoRec" 	type="hidden" value="<%=(String)request.getAttribute("codArquivoRec") %>">
<input name="nmArquivo" 		type="hidden" value="<%=(String)request.getAttribute("nmArquivo") %>">
<input name="codOrgao" 		type="hidden" value="<%=(String)request.getAttribute("codOrgao") %>">


<!--IN�CIO_CORPO_sistema-->

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 45px; z-index:1;" > 

<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="50%" align="left"><b>Arquivo:&nbsp;</b>
				<%=(String)request.getAttribute("nmArquivo")%>&nbsp;
			  </td>
			  <td width="25%"><b>&Oacute;rg&atilde;o Atua&ccedil;&atilde;o:&nbsp;</b>&nbsp;<%=(String)request.getAttribute("sigOrgaoAtu")%>
			  </td>	  		  		  		  
	          <td align="right">
					<button type="button" NAME="Imprimir"   style="width: 28px;height: 21px;  border: none; background: transparent; cursor: hand;"  onClick="javascript:valida('ImprimeDetalheArq',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM CABEC DA TABELA-->

<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:40px; top:110px; width:740px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#deebc2"> 
        <td width="35" height="15"  align="center" bgcolor="#deebc2"><strong>Seq</strong></td>
        <td width="150" bgcolor="#EDF4DF" align="center"><strong>Auto</strong></td>
        <td width="70" align="center" bgcolor="#deebc2"><strong>Placa</strong></td>
        <td width="90" bgcolor="#EDF4DF" align="center"><strong>Data Infra&ccedil;&atilde;o</strong></td>
        <td width="90" bgcolor="#deebc2" align="center"><strong>Hora Infra&ccedil;&atilde;o</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>Munic&iacute;pio</strong></td>
        <td bgcolor="#deebc2" align="center"><strong>C&oacute;d. Infra&ccedil;&atilde;o</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:40px; top:130px; width:740px; height:207px; z-index:5; overflow: auto; visibility: visible;"> 
      <table width="720" border="0" cellpadding="0" cellspacing="0">
         
     <%
     	REG.LinhaArquivoRec linhaArquivo = new REG.LinhaArquivoRec();
     
     	Iterator it = LinhaArquivoRecId.getListaArquivos().iterator();
     	int i = 0;
    	
     	while (it.hasNext()) {     		
     		linhaArquivo =(REG.LinhaArquivoRec)it.next();	
      	%>
       	<tr bgcolor="#EDF4DF">       
	       	<td>
		       	<table width="720" border="0" cellpadding="1" cellspacing="1">
		       	<tr>
			        <td width="35" height="15" align="center"><%=i+1%></td>
			        <td width="150"><%=linhaArquivo.getNumAutoInfracao()%></td>
			        <td width="70" align="center"><%=linhaArquivo.getNumPlaca()%></td>
			        <% if (linhaArquivo.getDataInfracao() != null) {%>
			        	<td width="90" align="center"><%=linhaArquivo.getDataInfracao()%></td>
			        	<td width="90" align="center"><%=linhaArquivo.getHoraInfracao()%></td>
			        <% } else { %>
			        	<td width="90" align="center">-</td>
			        	<td width="90" align="center">-</td>
			        <% } %>
			        <td width="60" align="center"><%=linhaArquivo.getCodMunicipio()%></td>
			        <td align="center"><%=linhaArquivo.getCodInfracao()%></td>
			    </tr>
		      	</table>
			</td>
		</tr>
      	<tr>
        	<td height="1" bgcolor="#cccccc"></td>
      	</tr>
      
      <%
      	i++;
     	} //loop
     	if (LinhaArquivoRecId.getListaArquivos().size() == 0){
     	%>
     		<tr bgcolor="#EDF4DF">
      	   			<td><strong>Nenhum registro existente para o arquivo</strong></td>
      	   	</tr>	
      	<%	}%>
	  </table>
  </div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('retornar',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.png" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>