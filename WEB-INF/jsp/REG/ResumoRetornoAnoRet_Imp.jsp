<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="sys.Util" %>
<jsp:useBean id="ResumoRetornoPagBeanId" scope="session" class="REG.ResumoRetornoPagBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 

		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;	
			boolean passa = true;	
		    int max = ResumoRetornoPagBeanId.getDados().size() ;
			if (max>0) { 
				for(int i=0;i<max;i++){
				
					REG.ControleVexBean myCont = (REG.ControleVexBean) ResumoRetornoPagBeanId.getDados().get(i);
					seq++;
					contLinha++;
					
					if (contLinha%50==1){
						npag++;							
						if (npag!=1){
						passa = true;
				%>			 					
						<jsp:include page="rodape_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
						<% } %>				
					<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
					<jsp:param name="nPag" value= "<%=npag%>" />				
					</jsp:include> 			
		         <% } %>
			<%if(passa){%>
			<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">		
		    	<tr>
		      		<td colspan="4" style="text-align: left; line-height:17px;"><strong style="display: block; float: left; margin-bottom: -2px"><%= "detalheResumo".equals(ResumoRetornoPagBeanId.getAcao()) ? "Resumo : " : "Retorno : " %> <%=ResumoRetornoPagBeanId.getDscTransacao() %> &nbsp;&nbsp;&nbsp;&nbsp;Registro(s) <%=ResumoRetornoPagBeanId.getQtdRegs() %> - <%=ResumoRetornoPagBeanId.getQtdParcialReg()%> / <%=ResumoRetornoPagBeanId.getTotalRegistros()%></strong></td>
		      	</tr>
		    	<tr>
		        	<td width="2%" style="text-align: left; line-height:17px"><strong>M�s/Ano:&nbsp;</strong></td>
					<td width="22%"><strong><%=ResumoRetornoPagBeanId.getMes() %>/<%=ResumoRetornoPagBeanId.getAno() %></strong></td>            
		        		
				</tr>	
		  	</table>
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
		  		<tr>
		    		<td width="5%"  bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;SEQ.</strong></font></td>
		    		<td width="14%"  bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;N&ordm; NOTIFICA��O</strong></font></td>
		    		<td width="14%"  bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;N&ordm; AUTO</strong></font></td>
		    		<td width="14%"  bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;DATA RETORNO </strong></font></td>
	        <% if ((ResumoRetornoPagBeanId.getAcao().equals("detalheResumo") && ResumoRetornoPagBeanId.getDscTransacao().equals("RECUPERADO P/ ENTREGA")) || ResumoRetornoPagBeanId.getDscTransacao().equals("CR�DITO")) { %>	
		    		<td width="30%"  bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;COD. RETORNO </strong></font></td>
		    		<% } %>
		    		<td bgcolor="#999999"><font color="#000000"><strong>&nbsp;&nbsp;TIPO </strong></font></td>
		 		</tr>
			</table>
		  	<% } %>	
		    <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
		  <tr>
		    <td width="5%">&nbsp;&nbsp;<%=i+1  %></td>
		    <td width="14%">&nbsp;&nbsp;<%= myCont.getNumNotificacao() %> <%= "".equals(myCont.getNumAutoInfracao()) ? " - " + Util.lPad(myCont.getSeqNotificacao(),"0",3) : "" %></td>
		    <td width="14%">&nbsp;&nbsp;<%= myCont.getNumAutoInfracao() %> </td>
		    <td width="14%">&nbsp;&nbsp;<%= myCont.getDatRetorno() %></td>
	        <% if ((ResumoRetornoPagBeanId.getAcao().equals("detalheResumo") && ResumoRetornoPagBeanId.getDscTransacao().equals("RECUPERADO P/ ENTREGA")) || ResumoRetornoPagBeanId.getDscTransacao().equals("CR�DITO")) { %>	
		    <td width="30%">&nbsp;&nbsp;<%= myCont.getCodRetorno() %> - <%=myCont.getDscCodRetorno()%></td>
		    <% } %>
		    <td >&nbsp;&nbsp;<%= "1".equals(myCont.getTipNotificacao()) ? "AUTUA��O" : "PENALIDADE" %></td>
		  </tr>
		  <tr>
			<td bgcolor="#00000" height="2" colspan="6"></td>
		  </tr>
		  <%-- } --%>
		</table>      

		<% passa=false;} %>
		<% }else{%>	
		<table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
				<tr><td height="40" ></td></tr>		
				<tr bgcolor='#CCCCCC'> 
			   	    <td height="35" align=center><b>N�o existem dados a serem retornados </b></td>
				</tr>	
		</table>      	
		
		<%}%>
		<%	if (ResumoRetornoPagBeanId.getDados().size()>0) { 
				if (contLinha<50){
		
				} %>
				<jsp:include page="rodape_impConsulta.jsp" flush="true" />
		<%} %>
		
</form>
</body>
</html>