<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.text.DecimalFormat"%>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<!-- Chama o Objeto da Tabela de Parametros de Sistema -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaosAtuacao(  UsuarioBeanId,ParamSistemaBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm, totalMesAno) {
 switch (opcao) {
   case 'visualizacao':
   		if(fForm.prazo.value == "")
   			alert("Digite um N�mero");
   		else{
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  	}
	  break ;
   case 'retornarFiltro':
		fForm.acao.value="";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;   
   case 'detalhe':
		fForm.acao.value=opcao;
		fForm.totalMesAno.value=totalMesAno;
	   	fForm.target= "_blank";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
	    close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function funcDetalhe(opcao,fForm,selecao,quantidade){
	if(quantidade == '0'){
		alert("Status n�o possui notifica��es a serem apresentadas !")
		return
	}
	document.all["detalhe"].value = selecao
	valida(opcao,fForm)
}
function apagaOrgao() {
	document.notifRelatorio.codOrgao.selectedIndex=0;	  
}
function apagaTodos() {
	document.notifRelatorio.todosOrgaos.checked=false;	  
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifRelatorio" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="detalhe" type="hidden" value=' '>	
<% String statusInteracao = (String)request.getAttribute("statusInteracao"); %>
<input name="statusInteracao" type="hidden" value='<%=statusInteracao%>'>	
<input name="codStatusAuto" type="hidden" value='<%=(String)request.getAttribute("statusAuto")%>'>

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 80px; left:50px;" >  
  <table width="100%" cellpadding="0" cellspacing="1" >
    <script>
	<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
		for (var l=0;l<cod_orgao.length;l++) {
			document.write("<input name='todosOrgaosCod' type='hidden' value='"+cod_orgao[l]+"'>");
		}
	</script>
    <%	if(statusInteracao.equals("1")){%>
    <tr>
      <td width="148"></td>
      <td width="250" valign="top">&nbsp;&nbsp;<strong>Fase dos Autos :</strong></td>
      <td width="200" valign="top"> <strong>&nbsp;&Oacute;rg&atilde;o :</strong>
          <select name="codOrgao" onclick="javascript:apagaTodos();">
            <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
            <script>
			<!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) {
				<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=UsuarioBeanId.getCodOrgaoAtuacao()%>"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
			</script>
          </select>
      </td>
      <td rowspan="2">
        <button type="button" name="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('visualizacao',this.form);"> <img src="<%= path %>/images/bot_ok.gif" align="left" ></button></td>
    </tr>
    <tr>
      <td width="148 "></td>
      <td valign="top" width="250">
        <input name="statusAuto" type="radio" value="autuacao" class="sem-borda">
        Autua&ccedil;&atilde;o
        <input name="statusAuto" type="radio" value="penalidade" class="sem-borda">
        Penalidade
        <input name="statusAuto" type="radio" value="ambas" checked class="sem-borda">
        Ambas </td>
      <td width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <input type="checkbox" name="todosOrgaos" value="selecionado"  class="sem-borda" onClick="javascript:apagaOrgao();">
      Todos os &Oacute;rg&atilde;os</td>
      <td></td>
    </tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr></tr>
    <tr>
      <td></td>
      <td> &nbsp;&nbsp;Prazo de Envio:
          <input name="prazo" type="text" value="<%=ParamSistemaBeanId.getParamSist("PRAZO_REL_NOTIFICACOES")%>" size="5">
      Dias </td>
    </tr>
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
    <%	}
		else if(statusInteracao.equals("2")){
	%>
    <tr>
      <td width="62"></td>
      <td> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &Oacute;rg&atilde;o :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong>
          <input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>">
          <input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
      </td>
    </tr>
    <%	}
		else if(statusInteracao.equals("3")){%>
    <tr>
      <td width="62"></td>
      <td> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Todos os &Oacute;rg&atilde;os</strong> </td>
    </tr>
    <%
		}
%>
    <%		if( (statusInteracao.equals("2")) || (statusInteracao.equals("3")) ){
%>
    <tr>
      <td width="62"></td>
      <td> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fase dos Autos :</strong>
          <%
				String statusAuto = (String) request.getAttribute("statusAuto");
				if(statusAuto.equalsIgnoreCase("autuacao"))
					statusAuto = "Autua��o";
				%>
          <input disabled name="statusAuto" type="text" size="20" value="<%=statusAuto.toUpperCase()%>">
      </td>
    </tr>
    <tr>
      <td width="62"></td>
      <td> <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prazo de Envio :&nbsp;
            <input disabled name="txtPrazoDigitado" size="5" value="<%=(String)request.getAttribute("prazo")%>">
      </strong> </td>
    </tr>
    <%		}
%>
  </table>
</div>
<%
		if( (statusInteracao.equals("2")) || (statusInteracao.equals("3")) ){
%>
			<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
			<input type="hidden" name="prazoDigitado" value="<%=(String)request.getAttribute("prazo")%>">
			<div id="div01" style="position:absolute; left:200px; top:145px; width:400px; height:50px; z-index:1; overflow: auto; visibility: visible;"> 
    			<table width="100%" border="0" cellpadding="0" cellspacing="1">
					<tr bgcolor="#deebc2"> 
        				<td width="150" height="15" align="center"><strong>M&ecirc;s</strong></td>
        				<td width="100" align="center"><strong>Ano</strong></td>
        				<td width="100" align="center"><strong>Total</strong></td>
        				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					</tr>
				</table>
  			</div>
			<div id="div13" style="position:absolute; left:200px; top:165px; width:400px; height:150px; z-index:5; overflow: auto; visibility: visible;"> 
    			<table width="100%" border="0" cellpadding="0" cellspacing="1">
<%					Map meses = (Map) request.getAttribute("meses");
					List totalAnosMeses = (List) request.getAttribute("totalAnosMeses");
					String valorFormatado = "";
					DecimalFormat formato = new DecimalFormat(",##0");
					for (Iterator iter = totalAnosMeses.iterator(); iter.hasNext();) {
						String totalMesAno = (String) iter.next();
						valorFormatado = formato.format(Integer.parseInt(totalMesAno.substring(8).trim()));
%>
						<tr bgcolor="#EDF4DF">
							<td width="150" align="center"><%=meses.get(totalMesAno.substring(0, 2))%></td>
			    			<td width="100" align="center"><%=totalMesAno.substring(3, 7)%></td>
			    			<td width="100" align="center"><%=valorFormatado%></td>
							<td align="center">        
								<button type="button" NAME="check" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('detalhe',this.form,'<%=totalMesAno%>');">	
	        					<IMG src="<%= path %>/images/bot_detalhe_ico.gif" alt="Clique aqui para ver detalhamento" align="left" ></button>						
							</td>
			    		</tr>
<%
					}
%>
					<input type="hidden" name="totalMesAno" value="">
				</table>
  
  
  </div>
  <div id="div13" style="position:absolute; left:200px; top:317px; width:400px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
				<table width="100%"cellspacing="0" cellpadding="0" border="0">
			      <tr>
			       <td align="center">
					   <button  style=" top: 350px; border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript: valida('retornarFiltro',this.form);">
					   <img src="<%=path%>/images/bot_retornar_det1.gif"  width="71px" height="26px" style="cursor: hand;" > </button>
				   </td>
			     </tr>
  			</table>
  </div>
<%
		}
%> 


<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=OrgaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "150 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>