<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="UsuarioBeanId"       scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="ArquivoRecebidoBeanId" scope="request" class="REG.ArquivoRecebidoBean" /> 

<% 
	   String nomArq = (String) session.getAttribute("nomArq");
%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>DETRAN � Sistema de Monitoramento de Multas de Tr�nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%=path %>/sys/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight);

mySetor     = new Array(<%= ArquivoRecebidoBeanId.getSetor(  UsuarioBeanId, nomArq  ) %>);
cod_setor = new Array(mySetor.length/2);
sig_setor = new Array(mySetor.length/2);
for (i=0; i<mySetor.length/2; i++) {
	cod_setor[i] = mySetor[i*2+0] ;
	sig_setor[i] = mySetor[i*2+1] ;
}


 
function valida(opcao,fForm) {
  switch (opcao) {
   case 'OK':
   
    if (document.UsrForm.arqUpload.value.length==0) alert("Nenhum arquivo selecionado.") ;
	else {

<% 		if (ArquivoRecebidoBeanId.getTamNumControle()==0)  { %>
			document.UsrForm.acao.value="Upload"	;
			document.UsrForm.target= "_self";
    		document.UsrForm.action = "acessoTool";
	   		document.UsrForm.submit();
<% 		} else { %>
			 if ((document.UsrForm.arqUpload.value).indexOf(document.UsrForm.proximoArquivo.value)<0) 
				alert("Arquivo selecionado n�o � "+document.UsrForm.proximoArquivo.value+".") ;
	 		else {
			document.UsrForm.acao.value="Upload"	;
			document.UsrForm.target= "_self";
    		document.UsrForm.action = "acessoTool";
	   		document.UsrForm.submit();
			}
<% 		} %>
			
	}
	break;
   case 'R':
    close() ;
	break;
	case 'O':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	  else  document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
    case 'V':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	  else  document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  
  }
}

function seleciona(fForm) {
	fForm.proximoArquivo.value= fForm.Setor.options[fForm.Setor.selectedIndex].value;
}



</script>
</head>
<form enctype="multipart/form-data" name="UsrForm" method="post" action="" >
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<!-- Cabe�alho -->
<jsp:include page="Cab.jsp" flush="true" />
<!--Upload de Arquivo-->	
<div id="Upload" style="position:absolute; left:50px; top:87px; width:720px; height:200px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table">
   <tr bgcolor="#EFF5E2"> 
	  <td ></td>
      <td colspan=2>SELECIONE O SETOR</td>
      <td colspan=2>&nbsp;: 
 		<select name="Setor" onClick="javascript: seleciona(this.form);"> 
			<option value="" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			<script>
			for (j=0;j<cod_setor.length;j++) {
				document.write ("<option value="+cod_setor[j]+">"+sig_setor[j])+"</option>"
			}
			</script>
		 </select>
      </td>
    </tr>
	<% if (ArquivoRecebidoBeanId.getTamNumControle()>0) { %>
	<tr>
		<td width="3%"></td>
		<td height="12" width="8%"></td>
		<td width="9%"></td>
		<td width="40%"></td>
		<td width="44%"></td>
	</tr>
   <tr bgcolor="#EFF5E2"> 
	  <td ></td>
      <td colspan=2>ARQUIVO ESPERADO</td>
      <td colspan=2>&nbsp;: 
		<input name="proximoArquivo"  type="text" readonly value='' size="40" maxlength="20">
      </td>
    </tr>
	<% } %> 
	
	<tr>
		<td width="3%"></td>
		<td height="12" width="8%"></td>
		<td width="9%"></td>
		<td width="40%"></td>
		<td width="44%"></td>
	</tr>
    <tr bgcolor="#EFF5E2">
	  <td ></td>
      <td >ARQUIVO</td>
      <td colspan=3>&nbsp;: 
        <input name="arqUpload"    type="file" size="90" maxlength="90" onKeyUp="this.value=this.value.toUpperCase()">
      </td>
    </tr>
	<tr><td height="10" colspan=5></td></tr>
    <tr><td align="center" colspan=5>
		<button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" onClick="javascript: valida('OK',this.form);">
		<img src="images/bot_enviar.gif" width="41" height="19"></button></td>
    </tr>
  </table>
</div>
<!--Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!--FIM Rodap�-->
<input name="acao"            type="hidden" value="Upload">
</body>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= ArquivoRecebidoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</html>
