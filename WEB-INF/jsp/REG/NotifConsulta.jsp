<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %> 
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@	page import="java.util.List"%>
<%@	page import="REG.NotifControleBean"%>
<%@page import="java.text.SimpleDateFormat"%>

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="NotifControleId" scope="request" class="REG.NotifControleBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
 switch (opcao) {
   case 'consulta':
   		if(!validaCampos(fForm)){
   			alert("Campo do Auto ou da Notifica��o n�o preenchido !")
   			return
   		}	
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
	  if (fForm.statusInteracao.value=='2') {
		history.back()
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function validaCampos(fForm){
	if( (fForm.auto.value == "") && (fForm.notificacao.value == "") ){
		return false
	}
	return true
}
</script>


</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifPendencia" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="statusInteracao" type="hidden" value='<%=(String)request.getAttribute("statusInteracao")%>'>	

<div  id="div01" style="position:absolute; left:55px; top:105px; width:720px; height:195px; z-index:1; overflow: auto; visibility: visible;"> 
<%	String statusInteracao = (String)request.getAttribute("statusInteracao");
	if(statusInteracao.equals("1")){%>
		<table width="100%" cellpadding="0" cellspacing="1" >    
			<tr>
				<td width="65"></td>
				<td width="100"><strong>Auto de Infra&ccedil;&atilde;o :</strong></td>
				<td width="113"><input name="auto" type="text" size="12" maxlength="12"  value="<%=NotifControleId.getNumAutoInfracao()%>" onChange="this.value=this.value.toUpperCase()"></td>
				<td width="70"><strong>Notifica&ccedil;&atilde;o :</strong></td>
				<td width="185"><input name="notificacao" type="" size="12" maxlength="12"  value="<%=NotifControleId.getNumNotificacao()%>"  onKeyPress="javascript: f_num();"></td>
				<td>
					<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('consulta',this.form);">	
	        		<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>	
				</td>
			</tr>
		</table>
<%	}
	else{
		List notificacoes = (List)request.getAttribute("notificacoes");
		NotifControleBean notificacao = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		for(int i=0;i<notificacoes.size();i++){
			notificacao = (NotifControleBean)notificacoes.get(i);
			if(i>0){%>
		<table width="100%" cellpadding="0" cellspacing="0" >
			<tr bgcolor="#000000">
				<td height="2"></td>
			</tr>
			<tr>
       		 	<td height="5"></td>
      		</tr>
		</table>
<%			}%>
		<table width="100%" cellpadding="0" cellspacing="0" >  
     		<tr bgcolor="#deebc2">
        		<td colspan="6" class="espaco2">&nbsp;&nbsp;<strong>DADOS DA NOTIFICA��O</strong></td>
      		</tr>
      		<tr bgcolor="#EFF5E2">
        		<td width="160" align="right"><strong><font color="#666666">Notifica&ccedil;&atilde;o :&nbsp;</font></strong></td>
        		<td width="100"><%=notificacao.getNumNotificacao()%></td>
        		<td width="155" align="right"><font color="#666666"><strong>Auto de infra&ccedil;&atilde;o :&nbsp;</strong></font></td>
        		<td width="70"><%=notificacao.getNumAutoInfracao()%></td>
        		<td width="130" align="right"><strong><font color="#666666">N&uacute;mero da Placa :&nbsp;</font></strong></td>
        		<td><%=notificacao.getNumPlaca()%></td>
	  		</tr>
	  		<tr>
        		<td height="1"></td> 
      		</tr>
      		<tr bgcolor="#EFF5E2">
      		    <td width="160" align="right"><strong><font color="#666666">Status da Notifica&ccedil;&atilde;o :</font>&nbsp;</strong></td>
        		<td colspan="5"><%=notificacao.getCodStatusNotif()%>-<%=notificacao.getNomStatusNotificacao()%></td>
	  		</tr>
	  		<tr>
        		<td height="1"></td>
      		</tr>
	      	<tr bgcolor="#EFF5E2">
	      		<td width="160" align="right"><strong><font color="#666666">Status do Auto de Infra&ccedil;&atilde;o :&nbsp;</font></strong></td>
        		<td colspan="5"><%=notificacao.getCodStatusAuto()%>-<%=notificacao.getNomStatusAuto()%></td>
	      	</tr>  		
	  		<tr>
       		 	<td height="5" colspan="5"></td>
      		</tr>
	  	</table>
		
	  	<table width="100%" cellpadding="0" cellspacing="0" >  
	  		<tr bgcolor="#deebc2">
        		<td colspan="5">&nbsp;&nbsp;<strong>DADOS DO ENVIO</strong></td>
      		</tr>
      		<tr bgcolor="#EFF5E2">
        		<td width="125" align="right"><strong><font color="#666666">Arquivo de Envio :&nbsp;</font></strong></td>
          		<td width="180"><%=notificacao.getNomArquivo(notificacao.getCodArquivoEnvio())%></td>          	       		
        		<%	if( (notificacao.getCodArquivoEnvioOrig() == null) || (notificacao.getCodArquivoEnvioOrig().equals("")) ){%>
        		<td width="170"></td>
        		<td></td>
        		<%	}else{%>
        		<td width="170" align="right"><font color="#666666"><strong>Arquivo Original de Envio :&nbsp;</strong></font></td>
        		<td><%=notificacao.getNomArquivo(notificacao.getCodArquivoEnvioOrig())%></td>
        		<%	}%>
	  		</tr>
	  		<tr>
        		<td height="1"></td>
      		</tr>
	  		<tr bgcolor="#EFF5E2">
	  			<td width="125" align="right"><strong><font color="#666666">Data de Envio :&nbsp;</font></strong></td>
        		<%	if(notificacao.getDatEnvio() == null){%>
        		<td width="180"></td>  
        		<%	}else{%>
        		<td width="180"><%=sdf.format(notificacao.getDatEnvio())%></td>  
        		<%	}
        			if(notificacao.getDatEnvioOrig() == null){%>
				<td width="170"></td> 
				<td></td>
				<%	}else{%> 
        		<td width="170" align="right"><strong><font color="#666666">Data Original de Envio :&nbsp;</font></strong></td>
        		<td><%=sdf.format(notificacao.getDatEnvioOrig())%></td> 
        		<%	}%>	
	  		</tr>
	  		<tr>
        		<td height="1"></td>
      		</tr>
	  		<tr bgcolor="#EFF5E2">
        		<td width="125" align="right"><strong><font color="#666666">Usu&aacute;rio de Envio :&nbsp;</font></strong></td>
        		<td width="180"><%=notificacao.getNomUsuarioEnvio()%></td>
        		<%	if( (notificacao.getNomUsuarioEnvioOrig() == null) || (notificacao.getNomUsuarioEnvioOrig().equals("")) ){%>
        		<td width="170"></td>
        		<td></td>
        		<%	}else{%>
        		<td width="170" align="right"><font color="#666666"><strong>Usu&aacute;rio Original de Envio :&nbsp;</strong></font></td>
        		<td><%=notificacao.getNomUsuarioEnvioOrig()%></td>
        		<%	}%>
	  		</tr>
	  		<tr>
        		<td height="1"></td>
      		</tr>
	  		<tr bgcolor="#EFF5E2">
        		<td width="125" align="right"><strong><font color="#666666">�rg�o de Envio :&nbsp;</font></strong></td>
        		<td width="180"><%=notificacao.getSigCodOrgao(notificacao.getCodOrgaoEnvio())%></td>
        		<%	if( (notificacao.getCodOrgaoEnvioOrig() == null) || (notificacao.getCodOrgaoEnvioOrig().equals("")) ){%>
        		<td width="170"></td>
        		<td></td>
        		<%	}else{%>
        		<td width="170" align="right"><font color="#666666"><strong>�rg�o Original de Envio :&nbsp;</strong></font></td>
        		<td><%=notificacao.getSigCodOrgao(notificacao.getCodOrgaoEnvioOrig())%></td>
	  	        <%	}%>
	  		</tr>
	  		<tr>
       		 	<td height="5" colspan="4"></td>
      		</tr>
	  	</table>
		
	  	<table width="100%" cellpadding="0" cellspacing="0" >  
	  		<tr bgcolor="#deebc2">
        		<td colspan="6">&nbsp;&nbsp;<strong>DADOS DO RECEBIMENTO</strong></td>
      		</tr>
      		<tr bgcolor="#EFF5E2">
        		<td width="125" align="right"><font color="#666666"><strong>Data do Recebimento :&nbsp;</strong></font></td>
        		<%	if(notificacao.getDatReceb() == null){%>
				<td width="90"></td>  
				<%	}else{%>
        		<td width="90"><%=sdf.format(notificacao.getDatReceb())%></td>     	
        		<%	}%>	
        		<td width="175" align="right"><font color="#666666"><strong>Usu&aacute;rio do Recebimento :&nbsp;</strong></font></td>
        		<td width="90"><%=notificacao.getNomUsuarioReceb()%></td>    				
        		<td width="140" align="right"><strong><font color="#666666">�rg�o de Recebimento :&nbsp;</font></strong></td>
        		<td><%=notificacao.getSigCodOrgao(notificacao.getCodOrgaoReceb())%></td>
	  		</tr>
	  		<tr>
       		 	<td height="5" colspan="6"></td>
      		</tr>
		</table>
		
		<table width="100%" cellpadding="0" cellspacing="0" >  
	  		<tr bgcolor="#deebc2">
        		<td colspan="4" class="espaco2">&nbsp;&nbsp;<strong>DADOS DO RETORNO</strong></td>
      		</tr>
      		<tr bgcolor="#EFF5E2">
      			<td width="125" align="right"><strong><font color="#666666">Arquivo de Retorno :</font>&nbsp;</strong></td>
      		    <td width="180"><%=notificacao.getNomArquivo(notificacao.getCodArquivoRetorno())%></td>
	  			<td width="170" align="right"><strong><font color="#666666">Data de Retorno :&nbsp;</font></strong></td>
        		<%	if(notificacao.getDatRetorno() == null){%>
				<td></td> 
				<%	}else{%>
        		<td><%=sdf.format(notificacao.getDatRetorno())%></td>  
        		<%	}%>
      		</tr>
      		<tr>
        		<td height="1"></td>
      		</tr>
      		<tr bgcolor="#EFF5E2">
        		<td width="125" align="right"><strong><font color="#666666">Usu&aacute;rio de Retorno :</font>&nbsp;</strong></td>
        		<td width="180" ><%=notificacao.getNomUsuarioRetorno()%></td>
        		<td width="170" align="right"><strong><font color="#666666">�rg�o de Retorno :</font>&nbsp;</strong></td>
        		<td><%=notificacao.getSigCodOrgao(notificacao.getCodOrgaoRetorno())%></td>
	  		</tr>
	  		<tr>
       		 	<td height="5" colspan="4"></td>
      		</tr>
		</table>
<%		}
	}%>
</div>
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=NotifControleId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>
