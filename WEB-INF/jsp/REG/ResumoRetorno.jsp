<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>

<%@ page errorPage="ErrorPage.jsp" %>


<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ResumoRetornoBeanId"     scope="session" class="REG.ResumoRetornoBean"/> 
<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
 switch (opcao) {
   case 'consultarAnoVex':
   		if(fForm.numAno.value == "")
   		{
   		    alert("Ano n�o preenchido.");
            document.all.numAno.focus();
   			return;
		}
		fForm.acao.value=opcao;
   		fForm.target= "_self";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  break;
   case 'consultarAnoPerrone':
  		if(fForm.numAno.value == "")
  		{
  		    alert("Ano n�o preenchido.");
           document.all.numAno.focus();
  			return;
		}
		fForm.acao.value=opcao;
  		fForm.target= "_self";
   	fForm.action = "acessoTool";  
  		fForm.submit();	  		  
	  break;
   case 'R':
	    close() ;   
	  break;
  }
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="resumoRetorno" method="post" action="" >
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao" type="hidden" value="">	
<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:475px; overflow: visible; z-index: 1; top: 150px; left:200px;" >  
  <table width="100%" cellpadding="0" cellspacing="1">
	<tr>
       <td width="110"><strong>&nbsp;&nbsp;Ano :</strong></td>
        <td align="left">
   		<%int numAno = Integer.parseInt(sys.Util.formatedToday().substring(6,10));%>
   		<select name="numAno" > 
   		<%
		  for (int j=0;j<=2;j++) {					
	      %>
			<option value="<%=numAno%>"><%=numAno%></option>
			<%numAno--;
		  }
		%>


   		</select>
   		</td>
        <td>&nbsp;</td>
        <td style="text-align: center">
        	<button type="button" name="Ok" style="width: 40px; height: 30px;  border: none; background: transparent;"  onClick="javascript: valida('consultarAnoVex',this.form);"> 
        		<img src="<%= path %>/images/bot_ok.gif">Vex
        	</button>
        </td>
        <td style="text-align: center">
        	<button type="button" name="Ok" style="width: 40px; height: 30px;  border: none; background: transparent;"  onClick="javascript: valida('consultarAnoPerrone',this.form);"> 
        		<img src="<%= path %>/images/bot_ok.gif" >Perrone
        	</button>
        </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    </tr>
    
    
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>
 
<!--Div Erros-->
<%
String msgOk         = "";
String msgErro       = ResumoRetornoBeanId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

</form>
</body>
</html>