<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<jsp:useBean id="consultaAutoId"     scope="request" class="REG.ConsultaBrokerBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>DETRAN &#8226;Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;
	   case 'ConsultaAuto':
			if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		  break;
      
     case 'abrirJanela':
    	fForm.acao.value=opcao
		fForm.target= "_blank";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;  
         
     case 'retorna':
        fForm.verTable.value="N"	  
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;   
          
	  case 'Novo':
		  fForm.acao.value=opcao;
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;
	  case 'N':
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;		  
  	  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}



function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	if (fForm.De.value != fForm.Ate.value){
		if (   (trim(fForm.numRenavam.value) =='')   && (trim(fForm.numPlaca.value) =='')
			&& (trim(fForm.numCpf.value)=='')        && (trim(fForm.numProcesso.value) =='')
			&& (trim(fForm.numAutoInfracao.value)=='') && (trim(fForm.codTransacao.value)=='') ) {
				sErro += "Nenhuma op��o selecionada. \n"
				valid = false ;	
		}
	}
	// validar as datas
	if(fForm.De.value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio. \n";
	}
	if(fForm.Ate.value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio. \n";
	}		

	if (valid==false) alert(sErro) 
	return valid ;
}

function changeColorOver(obj) {	
	obj.style.background="#EFF5E2";
	obj.style.textDecoration="none";
}

function changeColorOut(obj) {
	obj.style.background="#DEEBC2";
	obj.style.textDecoration="none";
}

function DetalheResult (datProc,resultado, parteVariavel, codTransacao, parteFixa) {
  var fForm=document.ConsultaAuto;
  fForm.result.value = resultado;
  fForm.variavel.value = parteVariavel;
  fForm.transacao.value = codTransacao;
  fForm.fixa.value = parteFixa;
  fForm.datProc.value = datProc;
  valida('abrirJanela',fForm);
}


</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaAuto" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="verTable" type="hidden" value=''>
<input name="datproc" type="hidden" value=''>
<input name="result" type="hidden" value=''>
<input name="fixa" type="hidden" value=''>
<input name="variavel" type="hidden" value=''>
<input name="transacao" type="hidden" value=''>

<% if (((String)request.getAttribute("verTable")).compareTo("N")==0){%>
 <div id="cons1" style="position:absolute; left:50px; top:80px; width:720px; height:125px; z-index:13; overflow: visible; visibility: visible;">  
   <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr   height="20" bgcolor="#EFF5E2">
       <td width="7%" > &nbsp;<strong>Placa :</strong></td>
       <td width="17%"><input name="numPlaca" type="text"  value="<%=consultaAutoId.getNumPlaca() %>" size="9" maxlength="7"
		   onKeyPress="javascript:Mascaras(this,'AAX9999')" onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">
       </td>
	   <td width="17%" >&nbsp;<strong>Num. Auto :</strong></td>
       <td width="23%"><input name="numAutoInfracao" type="text"  value="<%=consultaAutoId.getNumAutoInfracao() %>" size="14" maxlength="12"
		    onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">
       </td>
	   <td width="12%" >&nbsp;<strong>Renavam :</strong></td>
       <td width="24%"><input name="numRenavam" type="text"value="<%=consultaAutoId.getNumRenavam() %>" size="11" maxlength="9" 
		  	onKeyPress="javascript:f_num()" onFocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="5"></td>
     </tr>
	 <script>document.ConsultaAuto.numPlaca.focus();</script>	 
 </table>	 
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr height="20" bgcolor="#EFF5E2">
       <td width="7%"><strong>&nbsp;CPF :</strong></td>
       <td width="17%"><input name="numCpf" type="text" value="<%=consultaAutoId.getNumCPFEdt() %>" size="20" maxlength="14" 
		    onKeyPress="javascript:Mascaras(this,'999.999.999-99');" onFocus="javascript:this.select();" onChange = "javascript:ValCPF(this,0);">
       </td>
       <td width="17%"><strong>C�digo da Transa��o :</strong></td>
       <td width="23%"><input name="codTransacao" type="text"  value="<%=consultaAutoId.getCodTransacao() %>" size="10" maxlength="3"
			onKeyPress="javascript:f_num()" onFocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
       </td>
       <td width="12%"><strong>Processo :</strong></td>
       <td width="24%"><input name="numProcesso" type="text"  value="<%=consultaAutoId.getNumProcesso() %>" size="26" maxlength="20"
			onFocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="5"></td>
     </tr>
 </table>
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr bgcolor= "#EFF5E2">
       <td width="15%"><strong>&nbsp;Dt Infra��o:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; De</strong></td>
       <td width="26%"><input name="De" type="text" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoDe() %>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"></td>
       <td width="59%" colspan=2 rowspan=2>&nbsp;&nbsp;&nbsp;
           <button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  name="LocalizarAutoInfracao"  onClick="javascript: valida('ConsultaAuto',this.form);">
           <div align="left"><img src="<%= path %>/images/REG/bot_ok.gif" width="26" height="19"> </div>
           </button>
     ���������
           <button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  name="LimparAutoInfracao"  onClick="javascript: valida('Novo',this.form);"> <img src="<%= path %>/images/REG/bot_limpar.gif" width="29" height="19"  align="left"  > </button>
     ��������� </td>
     </tr>
     <tr bgcolor="#EFF5E2">
       <td align=right><strong>&nbsp;At&eacute;&nbsp;</strong></td>
       <td><input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoAte() %>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();"></td>
     </tr>
   </table>
 </div>
<% }
			else { %>
<div id="cons2" style="position:absolute; left:50px; top:80px; width:720px; height:114px; z-index:13; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr   height="20" bgcolor="#EFF5E2">
       <td width="8%" > &nbsp;<strong>Placa :</strong></td>
       <td width="19%"><input readonly name="numPlaca" type="text"  value="<%=consultaAutoId.getNumPlaca() %>" size="9" maxlength="7" style="border: 0px none; background-color:transparent;"></td>
       <td width="17%" >&nbsp;<strong>Num. Auto :</strong></td>	
       <td width="23%"><input readonly name="numAutoInfracao" type="text"  value="<%=consultaAutoId.getNumAutoInfracao() %>" size="14" maxlength="12" style="border: 0px none; background-color:transparent;"></td>
       <td width="11%" >&nbsp;<strong>Renavam :</strong></td>
       <td width="22%"><input readonly name="numRenavam" type="text"value="<%=consultaAutoId.getNumRenavam() %>" size="11" maxlength="9" style="border: 0px none; background-color:transparent;"></td>
	</tr>
      <tr><td colspan=6 height="5"></td></tr>
</table>	 
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr height="20" bgcolor="#EFF5E2">
        <td width="8%"><strong>&nbsp;CPF :</strong></td>
        <td width="19%"><input readonly name="numCpf" type="text" value="<%=consultaAutoId.getNumCPFEdt() %>" size="20" maxlength="14" style="border: 0px none; background-color:transparent;"></td>
        <td width="17%"><strong>C�digo da Transa��o :</strong></td>
        <td width="23%"><input readonly name="codTransacao" type="text"  value="<%=consultaAutoId.getCodTransacao() %>" size="26" maxlength="20" style="border: 0px none; background-color:transparent;"></td>
        <td width="11%"><strong>Processo :</strong></td>
        <td width="22%"><input readonly name="numProcesso" type="text"  value="<%=consultaAutoId.getNumProcesso() %>" size="26" maxlength="20" style="border: 0px none; background-color:transparent;"></td>
	  </tr>			  
      <tr><td colspan=6 height="5"></td></tr>	  
  </table>
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr bgcolor= "#EFF5E2">
        <td width="16%"><strong>&nbsp;Dt Infra��o:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; De</strong></td>
        <td width="10%"><input readonly name="De" type="text" id="De" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoDe() %>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" style="border: 0px none; background-color:transparent;"></td>
        <td width="5%" align=left><strong>&nbsp;At&eacute;&nbsp;</strong></td>
	  	<td width="69%"><input readonly name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoAte() %>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" style="border: 0px none; background-color:transparent;"></td>
      </tr>	  
    </table>
</div>
<div style="position:absolute; left:50px; top:152px; width:720px; height:35px; z-index:1; overflow: visible; visibility: visible;" id="mensagem"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr > 
	<td width="81%" >&nbsp;&nbsp;LOCALIZADO(S):&nbsp;&nbsp;<%=consultaAutoId.getQuantidadeReg()%>&nbsp;&nbsp;&nbsp;REGISTRO(S) </td>
    <td width="19%" align="center">&nbsp;&nbsp;&nbsp;
   	   <button type=button style="height: 21px; width: 54px;border: none; background: transparent;"  name="retornar"  onClick="javascript: valida('retorna',this.form);">
   		 <div align="left"><img src="<%= path %>/images/bot_retornar.png" alt="Retornar" width="52" height="19"> </div>
   	   </button>
    </td>
	</tr>
  </table>
</div>			

 <div style="position:absolute; left:50px; top:175px; width:720px; height:10px; z-index:1; overflow: visible; visibility: visible;" id="titulos"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr bgcolor="#a8b980"> 
		<td width="150"> <div align="center"><font color="#ffffff"><strong>Dat Proc</strong></font></div></td>
	    <td width="89"> <div align="center"><font color="#ffffff"><strong>Cod Transa��o</strong></font></div></td>
	    <td width="113"> <div align="center"><font color="#ffffff"><strong>Parte Fixa</strong></font></div></td>
  	    <td width="124"> <div align="center"><font color="#ffffff"><strong>Parte Vari�vel</strong></font></div></td>
        <td > <div align="center"><font color="#ffffff"><strong>Resultado</strong></font></div></td>
	</tr>
  </table>
</div>

<div style="position:absolute; width:720px; overflow: auto; z-index: 3; top: 192px; left: 50px; height: 165px; visibility: visible;" id="tabConteudo">   
   <table width="100%" border="0" cellpadding="1" cellspacing="1" id="fconsulta"  >  	   		
	<%
        Iterator it = consultaAutoId.getDados().iterator() ;
		REG.ConsultaBrokerBean dados =  new REG.ConsultaBrokerBean() ;   
        while (it.hasNext()) {
			// lista os dados		
			dados = (REG.ConsultaBrokerBean)it.next() ;    
	%>       
		<tr bgcolor="#DEEBC2" style="cursor:hand;"  onclick = "javascript:DetalheResult('<%= dados.getDatProc()%>','<%= dados.getResultado()%>','<%= dados.getParteVariavel()%>','<%= dados.getCodTransacao()%>', '<%= dados.getParteFixa()%>' );"
		   onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);"> 
		  <td width="150">
		     <input readonly name="datProc" type="text"  value="<%= dados.getDatProc()%>" 
		        size="25" maxlength="19" style="border: 0px none; background-color:transparent;cursor:hand;"></TD>
		  <td width="89" align="center">
		     <input readonly name="codTransacao" type="text"  value="<%= dados.getCodTransacao()%>" 
		        size="6" maxlength="3" style="border: 0px none; background-color:transparent;cursor:hand;"></TD>
		  <td width="113">
		     <input readonly name="parteFixa" type="text"  value="<%= dados.getParteFixa()%>" 
		        size="20" maxlength="15" style="border: 0px none; background-color:transparent;cursor:hand;"></TD>
		  <td width="124" align="center">
		     <input readonly name="parteVariavel" type="text"  value="<%= dados.getParteVariavel()%>" 
		        size="20" maxlength="15" style="border: 0px none; background-color:transparent;cursor:hand;"></TD>	
	      <td  >
	         <input readonly name="resultado" type="text"  value="<%= dados.getResultado()%>" 
	            size="40" maxlength="35" style="border: 0px none; background-color:transparent;cursor:hand;"></TD>
 		</tr>	 
 	<% } %>
   	</table>  
</div> 
 	  
  <% } %>

<!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<!--Bot�o retornar--> 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= consultaAutoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>