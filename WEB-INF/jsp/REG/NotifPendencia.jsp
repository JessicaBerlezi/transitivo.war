<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="REG.ArquivoRecebidoBean"%>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta a combo-->
myOrg = new Array(<%= OrgaoId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'selecaoOrgao':
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  break ; 
   case 'selecaoArquivo':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ;
   case 'R':
	  if (fForm.statusInteracao.value!="1") {
		fForm.acao.value="";
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  	break ; 
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function changeColorOver(obj,cor) {		
	obj.style.background="#deebc2";
	obj.style.color=cor;
}

function changeColorOut(obj,cor) {
	 a = obj.style.background ;
	obj.style.background= cor;
}

function selecaoArquivo(fForm,codArquivo,nomArquivo){
	fForm.codArquivo.value = codArquivo
	fForm.nomArquivo.value = nomArquivo
	valida('selecaoArquivo',fForm)
}
function apagaOrgao() {
	document.notifPendencia.codOrgao.selectedIndex=0;	  
}
function apagaTodos() {
	document.notifPendencia.todosOrgaos.checked=false;	  
}
</script>


</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifPendencia" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="codArquivo" type="hidden" value=' '>	
<input name="nomArquivo" type="hidden" value=' '>	
<% String statusInteracao = (String)request.getAttribute("statusInteracao"); %>
<input name="statusInteracao" type="hidden" value='<%=statusInteracao%>'>	

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 100px; left:50px;" > 
  <table width="100%" border="0" cellpadding="0" cellspacing="1" >    
	<script>
	<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
		for (var l=0;l<cod_orgao.length;l++) {
			document.write("<input name='todosOrgaosCod' type='hidden' value='"+cod_orgao[l]+"'>");
		}
	</script>  
      <tr>
	  	  <td width="85"></td>
		  <td width="480"><strong>&nbsp;&Oacute;rg&atilde;o<%if(request.getParameter("j_sigFuncao").equals("REG0520")){%> de Envio<%}%> :&nbsp;</strong>
		  					
				<!-- Verifica se est� na etapa de intera��o para sele��o do �rg�o -->
				<%	if(statusInteracao.equals("1")){%>
				<select name="codOrgao" onClick="javascript:apagaTodos();"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
				<!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) {

					<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=UsuarioBeanId.getCodOrgaoAtuacao()%>"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
				</script>
				</select>
				<%if(request.getParameter("j_sigFuncao").equals("REG0510")){%>
					&nbsp;&nbsp;&nbsp;<input type="checkbox" name="todosOrgaos" value="selecionado" class="sem-borda" onClick="javascript:apagaOrgao();">&nbsp;Todos os &Oacute;rg&atilde;os
				<%}%>
			</td>
			<td align=left width="350">&nbsp;		
        		<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('selecaoOrgao',this.form);">	
	        	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
			</td>
					
					<!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
				<%	}
					else if(statusInteracao.equals("2")){
				%>
			 			<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
		        		<input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
		        		<input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">

			<td align=left width=350>&nbsp;</td>
				<%	}%>
	  </tr>
  </table>
</div>
<%		if(statusInteracao.equals("2")){%>
<div id="cons3" style="position:absolute; left:50px; top:138px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
	  <tr>
	  	<td><strong>&nbsp;Arquivo(s) com pend&ecirc;ncia de envio </strong></td>
	  </tr>
      <tr bgcolor="a8b980"> 
        <td width="279" height="14" align="center"><strong>
			<font color="#FFFFFF">Arquivo</font></strong></td>
		<td width="135"            align="center"><strong>
			<font color="#FFFFFF">Total de Linhas</font></strong></td>
        <td width="135"             align="center"><strong>
			<font color="#FFFFFF">Total de Pend�ncias</font></strong></td>
        <td align="center"><strong>
			<font color="#FFFFFF">Data do Recebimento</font></strong></td>
      </tr>
	</table>
</div>
  
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 168px; height: 91px; visibility: visible;" > 
    <table id="arquivos" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">      


<!-- Loop usado para apresentar todos os arquivos que possuem pend�ncias associadas. 
	 Cada arquivo � apresentado em uma linha espec�fica podendo ser referenciado 
	 atr�ves de um link.
	 -->
<%		int a = 0;
		List arquivos = (List)request.getAttribute("arquivos");
		ArquivoRecebidoBean arquivo = null;
		if(arquivos.size() == 0){%>
	 <tr>
	    <td height="70"></td>
	 	<td align="center"><strong><font color="#000000" size="-1">�rg�o n�o possui arquivo com erro</font></strong></td>
	 </tr>
<%		}
		else{
			for(int i=0;i<arquivos.size();i++){
				arquivo = (ArquivoRecebidoBean)arquivos.get(i);
				String cor   = (a%2==0) ? "#eff5e2" : "#ffffff";
%>
		<tr bgcolor='<%=cor%>' style="cursor:hand; " name="line<%=a%>" id="line<%=a%>" onmouseover="javascript:changeColorOver(line<%=a%>,'<%=cor%>');" onmouseout="javascript:changeColorOut(line<%=a%>,'<%=cor%>');" onClick="javascript: selecaoArquivo(notifPendencia,'<%=arquivo.getCodArquivo()%>','<%=arquivo.getNomArquivo()%>');"> 
	        <td width="279" height="14" align="center"><%=arquivo.getNomArquivo()%> </td>
			<td width="135"   align="center"><%=arquivo.getQtdReg()%>  </td>
        	<td width="135"  align="center"><%=arquivo.getQtdLinhaPendente()%> </td>
	        <td align="center"><%=arquivo.getDatRecebimento()%> </td>
		</tr>
<% 				a++;
			}
		}
	}
%>  
    </table>      
  </div>  
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=OrgaoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>