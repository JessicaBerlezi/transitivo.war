<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama os beans -->
<jsp:useBean id="paramSistREC" scope="request" class="ACSS.ParamSistemaBean" /> 
<jsp:useBean id="ProcessoBeanId" scope="request" class="PNT.ProcessoBean" /> 
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	   case 'O':  // Esconder os erro
	    	close() ;
			break;
	}			
  }

var theTable, theTableBody

function imprimeFrame() {
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();		
}
function escondeFrame() {
	document.all.ARFrame.style.visibility="hidden";
}
function mostraFrame() {
	document.all.ARFrame.style.visibility="visible";
}

function init() {
	theTable = (document.all) ? document.all.myTABLE : document.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function init2() {
	theTable = (document.all) ? window.parent.frame1.myTABLE : window.parent.frame1.getElementById("myTABLE")
	theTableBody = theTable.tBodies[0]
}
function appendRow(form) {
	insertTableRow(form, -1)
}
function appendRow2(form) {
	insertTableRow2(form, -1)
}

function addRow(form) {
	insertTableRow(form, form.insertIndex.value)
}

function insertTableRow(form, where) {
	var now = new Date()
	var nowData = [now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds()]
	clearBGColors()
	var newCell
	var newRow = theTableBody.insertRow(where);
	for (var i = 0; i < nowData.length; i++) {
		newCell = newRow.insertCell(i)
		newCell.innerHTML = nowData[i]
		newCell.style.backgroundColor = "salmon"
	}
}
</script>
</head>

<body onLoad="init2()" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="VisARDig" method="post" action="">
<input name="acao" type="hidden" value=' '>	
<!--IN�CIO_CORPO_sistema-->

<!--DIV FIXA-->
  <div id="req1" style="position:absolute; left:50px; top:10px; width:738px; height:67px; z-index:5; overflow: auto; visibility: visible;"> 
  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
    <tr> 
      <td height="2"></td>
    </tr>
  </table>
    <table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr bgcolor="#edf4df">
        <td width="88"><strong>&nbsp;&nbsp;</strong><strong>N&ordm; Notifica��o :</strong></td>
        <td ><%=ProcessoBeanId.getARDigitalizado().get(0).getNumNotificacao() %>-<%=ProcessoBeanId.getARDigitalizado().get(0).getSeqNotificacao() %></td>
      </tr>
    </table>  
</div>
<!--FIM DIV FIXA-->


<div id="req2" style="position:absolute; left:50px; top:45px; width:738px; height:370px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="720" border="0" cellpadding="1" cellspacing="0" class="table">
	  	<tr>
			<td align="center"><img id="imagemAR" alt="Imagem do AR da Notificacao: <%=ProcessoBeanId.getARDigitalizado().get(0).getNumNotificacao()%>" style="background-color:#E7E7E7" src="<%=path%>/download?nomArquivo=<%=ProcessoBeanId.getARDigitalizado().get(0).getNomFoto()%>&contexto=<%=ProcessoBeanId.getARDigitalizado().get(0).getParametro()%>&header=#" width="700" height="226" ></td>		
		</tr>
	</table>
</div>

<div id="botaoimprimir" style="position:absolute; right: 50px; bottom: 0px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;">
<table>
<tr>
	<td>
		<button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" type="button" onClick="appendRow2(this.form)"> 
			<img src="<%=path%>/images/bot_imprimir_det1.gif" width="70" height="26" alt="" border="0">
		</button>
	</td>
	<td>
		<button style="border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" type="button" onClick="javascript:valida('R',this.form);"> 
			<img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" alt="" border="0">
		</button>	
	</td>
</tr>
</table>
</div>

<div id="ARFrame" style="position:absolute; left:600px; top:50px; width:500px; height:350px; z-index:5; overflow: auto; visibility: hidden;"> 
<iframe name="frame1" id="frame1" src="<%= path %>/js/etiquetaFrame.html" frameborder="0" width="365" heigth="350" MARGINWIDTH="0" MARGINHEIGHT"0" SCROLLING="NO">
</iframe>
</div>
<!--Script para impressao-->
<script>
function insertTableRow2(form, where) {
	var newCell
	var newRow	
	
	newRow = theTableBody.insertRow(where)		
	newCell = newRow.insertCell(0)	
	newCell.innerHTML = '<td align=center><img id=imagemAR src="<%=path%>/download?nomArquivo=<%=ProcessoBeanId.getARDigitalizado().get(0).getNomFoto()%>&contexto=<%=ProcessoBeanId.getARDigitalizado().get(0).getParametro()%>&header=#" width=700 height=730 alt=AR></td>'
		

	//Impressao
	mostraFrame();		
	window.onafterprint=escondeFrame;
	window.parent.frame1.focus();		
	window.print();
}
</script>

</form>
</body>
</html>