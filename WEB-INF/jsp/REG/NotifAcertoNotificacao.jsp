<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="linhaRecId" scope="request" class="REG.LinhaArquivoRec" /> 
 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<jsp:include page="Css.jsp" flush="true" />

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value="selecaoArquivo";
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
      case 'atualizar':
	  		if(veCampos(fForm)){
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	
			}   		
			break;
	  case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function veCampos(fForm){		
	if(document.all["j_sigFuncao"].value == "REG0510"){
		var erro = new Array(17)
		var retorno = true	
		if(fForm.numNotificacao.value == ''){
			erro[0] = 'Notifica��o'
			retorno = false
		}
		else if(fForm.numAuto.value == ''){
			erro[1] = 'Auto de Infra��o'
			retorno = false
		}
		else if(fForm.orgAtuador.value == ''){
			erro[2] = '�rg�o Atuador'
			retorno = false
		}
		else if(fForm.nomMunicipio.value == ''){
			erro[3] = 'Munic�pio'
			retorno = false
		}
		else if(fForm.dataInfracao.value == ''){
			erro[4] = 'Data da Infra��o'
			retorno = false
		}
		else if(fForm.horaInfracao.value == ''){
			erro[5] = 'Hora da Infra��o'
			retorno = false
		}
		else if(fForm.tipoReemissao.value == ''){
			erro[6] = 'Tipo de Reemiss�o'
			retorno = false
		}
		else if(fForm.indSuspensao.value == ''){
			erro[7] = 'Indicador de Suspens�o'
			retorno = false
		}
		else if(fForm.tipoNotificacao.value == ''){
			erro[8] = 'Tipo de Notifica��o'
			retorno = false
		}
		else if(fForm.vencimentoMulta.value == ''){
			erro[9] = 'Vencimento da Multa'
			retorno = false
		}
		else if(fForm.valorMulta.value == ''){
			erro[10] = 'Valor da Multa'
			retorno = false
		}
		else if(fForm.numPlaca.value == ''){
			erro[11] = 'Placa'
			retorno = false
		}
		else if(fForm.especie.value == ''){
			erro[12] = 'Esp�cie do Ve�culo'
			retorno = false
		}
		else if(fForm.marcaModelo.value == ''){
			erro[13] = 'Marca/Modelo do Ve�culo'
			retorno = false
		}
		else if(fForm.categoriaVeiculo.value == ''){
			erro[14] = 'Categoria do Ve�culo'
			retorno = false
		}
		else if(fForm.tipoVeiculo.value == ''){
			erro[15] = 'Tipo do Ve�culo'
			retorno = false
		}
		else if(fForm.corVeiculo.value == ''){
			erro[16] = 'Cor do Ve�culo'
			retorno = false
		}
	}
	else{
		var erro = new Array(7)
		var retorno = true	
		if(fForm.numNotificacao.value == ''){
			erro[0] = 'Notifica��o'
			retorno = false
		}
		else if(fForm.dataRecebimento.value == ''){
			erro[1] = 'Data de Recebimento'
			retorno = false
		}
		else if(fForm.codBaixa.value == ''){
			erro[2] = 'C�digo da Baixa'
			retorno = false
		}
		else if(fForm.codCaixa.value == ''){
			erro[3] = 'C�digo da Caixa'
			retorno = false
		}
		else if(fForm.codLote.value == ''){
			erro[4] = 'C�digo do Lote'
			retorno = false
		}
		else if(fForm.identRecebedor.value == ''){
			erro[5] = 'Identidade do Recebedor'
			retorno = false
		}
		else if(fForm.nomRecebedor.value == ''){
			erro[6] = 'Nome do Recebedor'
			retorno = false
		}
	
	}
	var msg = ""
	for(i=0;i<erro.length;i++){
		if(erro[i] != null){
			msg += "Campo "+erro[i]+" n�o preenchido \n"
		}
	}
	if(msg!=""){
		alert(msg)
	}
	return retorno
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">

<form name="form" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>
<input name="statusInteracao" type="hidden" value='<%=(String)request.getAttribute("statusInteracao")%>'>
<input name="codLinha" type="hidden" value='<%=(String)request.getAttribute("codLinha")%>'>
<input name="codArquivo" type="hidden" value='<%=linhaRecId.getCodArquivo()%>'>
<input name="codOrgao" type="hidden" value='<%=(String)request.getAttribute("codOrgao")%>'>
<input name="nomArquivo" type="hidden" value='<%=(String)request.getAttribute("nomArquivo")%>'>

<%if(request.getParameter("j_sigFuncao").equals("REG0510")) {%>
<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
<%String[] todosOrgaosCod = (String[]) request.getAttribute("todosOrgaosCod");
  for(int i = 0; i < todosOrgaosCod.length; i++) {%>
	<input name='todosOrgaosCod' type='hidden' value='<%=todosOrgaosCod[i]%>'>
<%}
}%>

<%	String apresNotificacao="", apresOutrosCampos ="", apresOrgao="", apresTipoNotificacao="";
	if("N02,N03,N04".indexOf(linhaRecId.getCodRetorno())>=0){
		apresNotificacao = "disabled";
		apresOutrosCampos = "disabled";
	}
	if( "N04".equals(linhaRecId.getCodRetorno()) )
		apresOrgao = "disabled";
	if( "N03".equals(linhaRecId.getCodRetorno()) )
		apresTipoNotificacao = "disabled";%>

<!--IN�CIO_CORPO_sistema-->
  <div id="dadosAutoCondutor" style="position:absolute; left:50px; top:80px; width:720px; height:170px; z-index:1; overflow: visible; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <%	if(request.getParameter("j_sigFuncao").equals("REG0510")){     			
				String notificacao="", numAuto="", orgAtuador="", nomMunicipio="", dataInfracao="", horaInfracao="", tipoReemissao="";
				String indSuspensao="", tipoNotificacao="", vencimentoMulta="", valorMulta="", numPlaca="", especie="";
				String marcaModelo="", categoriaVeiculo="", tipoVeiculo="", corVeiculo="";
				if(linhaRecId.getDscLinhaArqRec().length()>15)
					notificacao = linhaRecId.getDscLinhaArqRec().substring(6,15);
				if(linhaRecId.getDscLinhaArqRec().length()>292)
					numAuto = linhaRecId.getDscLinhaArqRec().substring(280,292);
				if(linhaRecId.getDscLinhaArqRec().length()>493)
					orgAtuador = linhaRecId.getDscLinhaArqRec().substring(487,493);
				if(linhaRecId.getDscLinhaArqRec().length()>212)
					nomMunicipio = linhaRecId.getDscLinhaArqRec().substring(175,212);
				if(linhaRecId.getDscLinhaArqRec().length()>170)
					dataInfracao = linhaRecId.getDscLinhaArqRec().substring(162,170);
				if(linhaRecId.getDscLinhaArqRec().length()>175)
					horaInfracao = linhaRecId.getDscLinhaArqRec().substring(170,175);
				if(linhaRecId.getDscLinhaArqRec().length()>476)
					tipoReemissao = linhaRecId.getDscLinhaArqRec().substring(475,476);
				if(linhaRecId.getDscLinhaArqRec().length()>479)
					indSuspensao = linhaRecId.getDscLinhaArqRec().substring(478,479);
				if(linhaRecId.getDscLinhaArqRec().length()>480)
					tipoNotificacao = linhaRecId.getDscLinhaArqRec().substring(479,480);
				if(linhaRecId.getDscLinhaArqRec().length()>78)
					vencimentoMulta = linhaRecId.getDscLinhaArqRec().substring(70,78);
				if(linhaRecId.getDscLinhaArqRec().length()>85)
					valorMulta = linhaRecId.getDscLinhaArqRec().substring(78,85);
				if(linhaRecId.getDscLinhaArqRec().length()>117)
					numPlaca = linhaRecId.getDscLinhaArqRec().substring(110,117);
				if(linhaRecId.getDscLinhaArqRec().length()>110)
					marcaModelo = linhaRecId.getDscLinhaArqRec().substring(85,110);
				if(linhaRecId.getDscLinhaArqRec().length()>641)
					especie = linhaRecId.getDscLinhaArqRec().substring(631,641);
				if(linhaRecId.getDscLinhaArqRec().length()>653)
					categoriaVeiculo = linhaRecId.getDscLinhaArqRec().substring(641,653);
				if(linhaRecId.getDscLinhaArqRec().length()>671){
					tipoVeiculo = linhaRecId.getDscLinhaArqRec().substring(653,671);
					corVeiculo = linhaRecId.getDscLinhaArqRec().substring(671,linhaRecId.getDscLinhaArqRec().length());
				}%>

      <tr bgcolor="#deebc2">
        <td colspan=10 class="espaco2">&nbsp;&nbsp;<strong>DADOS DO AUTO</strong></td>
      </tr>
      <tr>
        <td height="3" colspan=10></td>
      </tr>
      <tr bgcolor="#EFF5E2">
        <td width="125" align="right"><strong><font color="#666666">Notifica&ccedil;&atilde;o :&nbsp;</font></strong></td>
        <td width="153"><input <%=apresNotificacao%> name="numNotificacao" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=notificacao%>' size="11" maxlength="9"></td>
        <td height="16" align="right"><font color="#666666"><strong>Auto da infra&ccedil;&atilde;o :&nbsp;</strong></font></td>
        <td><input <%=apresOutrosCampos%> name="numAuto" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onKeyPress="javascript:Mascaras(this,'H999999999999')" value='<%=numAuto%>' size="13" maxlength="12"></td>
        <td align="right"><strong><font color="#666666">Org&atilde;o atuador :&nbsp;</font></strong></td>
        <td><input <%=apresOrgao%> name="orgAtuador" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=orgAtuador%>' size="7" maxlength="6"></td>
	  </tr>
	  <tr>
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2">
	    <td width="125" align="right"><strong><font color="#666666"> Munic&iacute;pio da infra&ccedil;&atilde;o :&nbsp;</font></strong></td>
        <td><input <%=apresOutrosCampos%> name="nomMunicipio" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase();" value='<%=nomMunicipio%>' size="30" maxlength="40"></td>
        <td width="157" height="16" align="right">&nbsp;<strong><font color="#666666">&nbsp;Data da infra&ccedil;&atilde;o :</font>&nbsp;</strong></td>
        <td width="63"><input <%=apresOutrosCampos%> name="dataInfracao" type="text" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=dataInfracao%>' size="10" maxlength="10"></td>
        <td width="146" align="right"><strong><font color="#666666">Hora da infra&ccedil;&atilde;o :</font>&nbsp;</strong></td>
        <td width="76"><input <%=apresOutrosCampos%> name="horaInfracao" type="text"  onFocus="javascript:this.select();" onKeyPress="javascript:Mascaras(this,'99:99')" onChange="this.value=this.value.toUpperCase()" value='<%=horaInfracao%>' size="5" maxlength="5"></td>
	  </tr>
      <tr>
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2">
        <td width="125" height="16" align="right">&nbsp;<strong><font color="#666666">&nbsp;Tipo de reemiss&atilde;o :</font>&nbsp;</strong></td>
        <td width="153"><input <%=apresOutrosCampos%> name="tipoReemissao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=tipoReemissao%>' size="1" maxlength="1"></td>
        <td width="157" align="right"><strong><font color="#666666">Indicador de suspens&atilde;o :</font>&nbsp;</strong></td>
        <td width="63"><input <%=apresOutrosCampos%> name="indSuspensao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=indSuspensao%>' size="1" maxlength="1"></td>
        <td align="right"><strong><font color="#666666">Tipo de notifica&ccedil;&atilde;o :</font>&nbsp;</strong></td>
        <td width="76"><input <%=apresTipoNotificacao%> name="tipoNotificacao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=tipoNotificacao%>' size="1" maxlength="1"></td>
      </tr>
	  <tr>
        <td height="2"></td>
      </tr>
	  <tr bgcolor="#EFF5E2">
	  	<td height="16" align="right" ><font color="#666666"><strong> Vencimento da multa :</strong></font>&nbsp;</td>
        <td><input <%=apresOutrosCampos%> name="vencimentoMulta" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=vencimentoMulta%>' size="8" ></td>
        <td width="157" height="16" align="right">&nbsp;<strong><font color="#666666">Valor da multa :</font>&nbsp;</strong></td>
        <td colspan="3"><input <%=apresOutrosCampos%> name="valorMulta" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=valorMulta%>' size="8" maxlength="7"></td>
	  </tr>
      <tr>
        <td height="6" colspan=10></td>
      </tr>
    </table>
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#deebc2"> 
        <td colspan=6>&nbsp;&nbsp;<strong>DADOS DO VE&Iacute;CULO</strong></td>
      </tr>
      <tr> 
        <td height="3" colspan=6></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td align="right" height="10"><strong><font color="#666666">Placa :</font>&nbsp;</strong></td>
        <td><input <%=apresOutrosCampos%> name="numPlaca" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:Mascaras(this,'AAA9999')" value='<%=numPlaca%>' size="9" maxlength="7"></td> 
        <td width="80" align="right">&nbsp;<font color="#666666">&nbsp;<strong><font color="#666666">Esp&eacute;cie</font>:</strong></font><strong>&nbsp;</strong></td>
        <td width="145"><input <%=apresOutrosCampos%> name="especie" type="text" value='<%=especie%>' size="12" maxlength="10"></td>
		<td width="119" align="right"><strong><font color="#666666">Marca/Modelo :</font>&nbsp;</strong></td>
        <td ><input <%=apresOutrosCampos%> name="marcaModelo" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=marcaModelo%>' size="25" maxlength="25"></td>	  
	  </tr>
      <tr> 
        <td height="2" colspan="6"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td width="95" align="right"><strong><font color="#666666">Categoria :</font>&nbsp;</strong></td>
        <td width="132"><input <%=apresOutrosCampos%> name="categoriaVeiculo" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=categoriaVeiculo%>' size="13" maxlength="12"></td>
        <td width="80" align="right"><strong><font color="#666666">Tipo :</font>&nbsp;</strong></td>
        <td width="145"><input <%=apresOutrosCampos%> name="tipoVeiculo" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=tipoVeiculo%>' size="19" maxlength="18"></td>
        <td  align="right"><strong><font color="#666666">Cor :</font>&nbsp;</strong></td> 
        <td ><input <%=apresOutrosCampos%> name="corVeiculo" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=corVeiculo%>' size="11" maxlength="10"></td>
      </tr>
      <tr> 
        <td height="5"></td>
      </tr>    
	  <tr>  
	  	<td colspan="6" align="center" style="padding-left:15px";><strong><font color="#FF0000" size="-1"><%=linhaRecId.getNomStatusErro(request.getParameter("j_sigFuncao"))%></font></strong></td>
	  </tr>
      <%	}else{
      			String dataRecebimento="", codBaixa="", codCaixa="", codLote="", identRecebedor="", nomRecebedor="";
				if(linhaRecId.getDscLinhaArqRec().length()>45)
					dataRecebimento = linhaRecId.getDscLinhaArqRec().substring(37,45);
				if(linhaRecId.getDscLinhaArqRec().length()>47)
					codBaixa = linhaRecId.getDscLinhaArqRec().substring(45,47);
				if(linhaRecId.getDscLinhaArqRec().length()>51)
					codCaixa = linhaRecId.getDscLinhaArqRec().substring(47,51);
				if(linhaRecId.getDscLinhaArqRec().length()>55)
					codLote = linhaRecId.getDscLinhaArqRec().substring(51,55);
				if(linhaRecId.getDscLinhaArqRec().length()>107)
					identRecebedor = linhaRecId.getDscLinhaArqRec().substring(95,107);
				if(linhaRecId.getDscLinhaArqRec().length()>95)
					nomRecebedor = linhaRecId.getDscLinhaArqRec().substring(55,95);%>

      <tr bgcolor="#deebc2">
        <td colspan=8 class="espaco2">&nbsp;&nbsp;<strong>DADOS DA NOTIFICA��O</strong></td>
      </tr>
      <tr>
        <td height="3" colspan=10></td>
      </tr>
      <tr bgcolor="#EFF5E2">
        <td width="171" align="right"><strong><font color="#666666">Notifica&ccedil;&atilde;o :&nbsp;</font></strong></td>
        <td width="214"><input <%=apresNotificacao%> name="numNotificacao" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=linhaRecId.getNumNotificacao()%>' size="11" maxlength="9"></td>
   	  	<td width="1" bgcolor="#FFFFFF"></td>
	    <td height="16" align="right"><font color="#666666"><strong>Data do Recebimento :&nbsp;</strong></font></td>
        <td><input <%=apresOutrosCampos%> name="dataRecebimento" type="text"  onFocus="javascript:this.select();" value='<%=dataRecebimento%>' size="9" maxlength="8"></td>
	  </tr>
	  <tr>
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2">
	    <td width="171" align="right"><strong><font color="#666666"> C&oacute;digo da Baixa:&nbsp;</font></strong></td>
        <td><input <%=apresOutrosCampos%> name="codBaixa" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase();" value='<%=codBaixa%>' size="2" maxlength="2"></td>
   	  	<td width="1" bgcolor="#FFFFFF"></td>
		<td width="160" height="16" align="right"><strong><font color="#666666">C&oacute;digo da Caixa  :</font>&nbsp;</strong></td>
        <td width="174"><input <%=apresOutrosCampos%> name="codCaixa" type="text" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=codCaixa%>' size="4" maxlength="4"></td>
	  </tr>
      <tr>
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2">
        <td width="171" height="16" align="right">&nbsp;<strong><font color="#666666">&nbsp;C&oacute;digo do Lote:</font>&nbsp;</strong></td>
        <td width="214"><input <%=apresOutrosCampos%> name="codLote" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=codLote%>' size="4" maxlength="4"></td>
   	  	<td width="1" bgcolor="#FFFFFF"></td>
		<td height="16" align="right"><font color="#666666"><strong>Identidade do Recebedor:&nbsp;</strong></font></td>
        <td><input <%=apresOutrosCampos%> name="identRecebedor" type="text"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=identRecebedor%>' size="15" maxlength="12" ></td>
      </tr>
	  <tr>
        <td height="2"></td>
      </tr>
	  <tr bgcolor="#EFF5E2">
        <td width="171" align="right"><strong><font color="#666666">Nome do Recebedor  :</font>&nbsp;</strong></td>
        <td colspan="4"><input <%=apresOutrosCampos%> name="nomRecebedor" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=nomRecebedor%>' size="40" maxlength="40"></td>
      </tr>
      <tr> 
        <td height="5"></td>
      </tr>    
	  <tr>      
	  	<td colspan="5" align="center" style="padding-left:15px";><strong><font color="#FF0000" size="-1"><%=linhaRecId.getNomStatusErro(request.getParameter("j_sigFuncao"))%></font></strong></td>
	  </tr>
	  <%	}%>
    </table>
  </div>
  
  <!--Bot�es Atualizar / Retornar-->
  <div id="atualizaEretorna" style="position:absolute; left: 359px; top: 321px; width:26px; height:25px; z-index:1; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
            <td align="right" nowrap> <button style="border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" onClick="javascript: valida('atualizar',this.form);"><img src="<%= path %>/images/bot_atualizar_det1.gif" width="71" height="26"></button></td>
            <td align="left" nowrap> <button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" onClick="javascript:  valida('R',this.form);" ><img src="<%= path %>/images/bot_retornar_det1.gif" width="70" height="26"></button></td>
        </tr>
	</table>
</div>
<!--FIM Bot�es Atualizar / Retornar-->
  
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=linhaRecId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "178 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->


</form>
</body>
</html>
