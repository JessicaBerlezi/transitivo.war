<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %> 
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Bean ArquivoRecebidoBean-->
<jsp:useBean id="prioridadeBean"       scope="request" class="REG.DefinirPrioridadeBean" /> 

<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'busca':
     	if (fForm.codOrgao.value==0) {
	       valid = false;
	       sErro = "�rg�o n�o selecionado. \n";
	    }	
        else { 
		   fForm.acao.value=opcao;
	       fForm.target= "_self";
	       fForm.action = "acessoTool";  
	   	   fForm.submit();	  		  
	    }
	    break ; 
   case 'atualizar':
        if (veCampos(fForm)==true) {
		  fForm.acao.value=opcao;
	   	  fForm.target= "_self";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    }
	    break ;
   case 'R':
        close() ;   
	    break;
   case 'V':
	    fForm.acao.value=opcao
    	fForm.target= "_self";
        fForm.action = "acessoTool";  
   	    fForm.submit();	  		  
	    break;
   case 'O':
   		if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	    else                 document.all["MsgErro"].style.visibility='hidden' ; 
	    break;  
   case 'V':
   		if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	    else                 document.all["MsgErro"].style.visibility='visible' ; 
	    break;
  }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codOrgao.value==0) {
	   valid = false;
	   sErro = "�rg�o n�o selecionado. \n";
	}	
    if (valid==false) alert(sErro)
	return valid ;
}
 
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>				

<div id="WK_SISTEMA" style="position:absolute; width:568px; overflow: visible; z-index: 1; top: 75px; left: 204px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
   		<% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td  width="17%"><b>�rg�o Lota��o:&nbsp;</b> </td>
			  <td width="46%" >
					<select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select>
			  </td>
			 <td width="37%">
			  		<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
	    </td>
	  </tr>
    </table>  
          <% }
			else { %>
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
		   <tr>
			  <td width="17%"><b>Org�o Lota��o:&nbsp;</b></td>
			  <td width="83%" >
	            <input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
			    <input disabled name="sigOrgao" type="text"  size="15" maxlength="10" value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >			  </td>
		  </tr>
     <% } %>			  
      </tr>	
      <tr><td colspan="2" height="10"></td></tr>	 
    </table>    
</div>
<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>	
<div style="position:absolute; left:50px; top:118px; width:720px; height:10px; z-index:1; overflow: visible; visibility: visible;" id="titulos"> 
  <TABLE border="0" cellpadding="1" cellspacing="1" width="100%">	
    <tr bgcolor="#a8b980"> 
		<td width="363"> <div align="center"><font color="#ffffff"><strong>Nome Arquivo</strong></font></div></td>
	    <td width="150"> <div align="center"><font color="#ffffff"><strong>Status</strong></font></div></td>
	    <td > <div align="center"><font color="#ffffff"><strong>Prioridade</strong></font></div></td>
    </tr>
</table>
</div>
	  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 134px; left: 50px; visibility: visible; height: 151px;">   
   <table border="0" cellpadding="1" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator it = prioridadeBean.getDados().iterator() ;
        while (it.hasNext()) {
			// lista os dados		
			REG.DefinirPrioridadeBean dados =  new REG.DefinirPrioridadeBean() ;   			
			dados = (REG.DefinirPrioridadeBean)it.next() ;
	%>
		<tr height="20"> 
		   	 <td width="363"align="center" bgcolor="#DEEBC2">
		   	   <input readonly tabindex="-1" name="nomArquivo" type="text"  size="60" maxlength="50" value="<%= dados.getNomArquivo() %>" style="border: 0px none; background-color: transparent;" ></td>		 
	         <td width="150"  align="center" bgcolor="#DEEBC2">
	            <input readonly tabindex="-1" name="codStatus" type="text"  size="24" maxlength="16"  value="<%= dados.getCodStatusExtenso() %>" style="border: 0px none; background-color:transparent;" ></td>    
	         <td  align="center" bgcolor="#DEEBC2">
	            <input name="indPrioridade" tabindex="1" type="text" size="4" maxlength="2" value="<%= dados.getIndPrioridade()%>" onkeypress="javascript:f_num();" style="border: 0px none;" >
	         </td>   	
	    </tr>
 	   <% 

	   }
	   %>	
   </table>    
</div>
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 305px; left: 51px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
 		<td  align="right">
		   <button type="button" NAME="Desbloquear"  style="height: 28px; width: 76px;border: none; background: transparent;" onClick="javascript: valida('atualizar',this.form);">
			  <IMG src="<%= path %>/images/bot_atualizar_det1.gif" width="71" height="26" align="left"  >
	      </button>		</td>	
  	  	<td width="20%"> </td>
		<td> 
	  	  <button type=button NAME="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
			<img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >
		  </button>
		</td>
    </table>
 </div>
    
  <% } %>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= prioridadeBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "150 px" />
  <jsp:param name="msgErroLeft" value= "40 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codOrgao.length;i++){
		if(document.UsrAtuForm.codOrgao.options[i].value==njint){
			document.UsrAtuForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>			  
</form>
</BODY>
</HTML>

