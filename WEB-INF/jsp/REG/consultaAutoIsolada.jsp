<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<jsp:useBean id="consultaAutoId"     scope="session" class="REC.consultaAutoBean" /> 
<%  String Cor = "#EFF5E2" ; 
List pendentes = (List)request.getAttribute("AutosPendentes"); 
  if (pendentes==null) pendentes = new ArrayList();
  
  List pendentesDol = (List)request.getAttribute("AutosPendentesDol"); 
  if (pendentesDol==null) pendentesDol = new ArrayList();
  %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>
<title>DETRAN &#8226;Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">


<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;
	   case 'ConsultaAuto':
			if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		  break;
      case 'ImprimeAuto':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;		  
	  case 'Novo':
		  fForm.acao.value=opcao;
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}
function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["ConsultaAuto"].target= "_self";
	document.all["ConsultaAuto"].action = "acessoTool";  
	document.all["ConsultaAuto"].submit();	 
}

function veCampos(fForm) {
    valid = true ;
	sErro = "" ;
	if (   (trim(fForm.numRenavam.value) =='')   && (trim(fForm.numPlaca.value) =='')
		&& (trim(fForm.numCpf.value)=='')        && (trim(fForm.numProcesso.value) =='')
		&& (trim(fForm.numAutoInfracao.value)=='') ) {
			sErro += "Nenhuma op��o selecionada. \n"
			valid = false ;	
	}
	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}		
	
	if (valid==false) alert(sErro) 
	return valid ;
}
function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaAuto" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>

<script>
function apagaTodos() {
	ConsultaAuto.Sit9.checked=false ;
	return ;
}
</script>
<!--DIV FIXA -->
  <div id="cons1" style="position:absolute; left:50px; top:80px; width:720px; height:125px; z-index:13; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
      <tr   height="20" bgcolor="<%= Cor %>"> 
      	<td colspan=4 height="20">
      		&nbsp;<strong>Placa :</strong>&nbsp;&nbsp;			
          <input name="numPlaca" type="text"  value="<%=consultaAutoId.getNumPlaca() %>" size="9" maxlength="7"
		   onkeypress="javascript:Mascaras(this,'AAX9999')" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">
			&nbsp;&nbsp;
      		&nbsp;<strong>Num. Auto :</strong>			
          <input name="numAutoInfracao" type="text"  value="<%=consultaAutoId.getNumAutoInfracao() %>" size="14" maxlength="12"
		    onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">      		
      		&nbsp;&nbsp;
			&nbsp;<strong>Renavam :</strong>&nbsp;&nbsp;
          <input name="numRenavam" type="text"value="<%=consultaAutoId.getNumRenavam() %>" size="11" maxlength="9" 
		  	onkeypress="javascript:f_num()" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()">
      	</td>
		<td>&nbsp;&nbsp;&nbsp;Qto Pagto:</td>
		<td>
   	      <input type="radio" name="indPagoradio" value="1" <%= sys.Util.isChecked(consultaAutoId.getIndPago(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Pago
          <input type="radio" name="indPagoradio" value="2" <%= sys.Util.isChecked(consultaAutoId.getIndPago(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">N�o Pago&nbsp;&nbsp;
       	  <input type="radio" name="indPagoradio" value="9" <%= sys.Util.isChecked(consultaAutoId.getIndPago(),"9") %> class="sem-borda" style="height: 14px; width: 14px;">Todas&nbsp;&nbsp;				  		
		  <input name="indPago" type="hidden" value="">				
		</td>		
      </tr>
      <tr><td colspan=6 height="5"></td></tr>
      <tr height="20" bgcolor="<%= Cor %>"> 
        <td width="13%"><strong>&nbsp;CPF :</strong></td>
        <td width="17%"><input name="numCpf" type="text" value="<%=consultaAutoId.getNumCPFEdt() %>" size="20" maxlength="14" 
		    onkeypress="javascript:Mascaras(this,'999.999.999-99');" onFocus="javascript:this.select();" onChange = "javascript:ValCPF(this,0);">
        </td>
        <td width="9%"><strong>&nbsp;Processo :</strong></td>
        <td width="18%"><input name="numProcesso" type="text"  value="<%=consultaAutoId.getNumProcesso() %>" size="26" maxlength="20"
			onfocus="javascript:this.select();"  onKeyUp="this.value=this.value.toUpperCase()">
        </td>
		<td width=12%>&nbsp;&nbsp;&nbsp;Qto Fase:</td>
		<td width=30%>
          <input type="radio" name="indFaseradio" value="1" <%= sys.Util.isChecked(consultaAutoId.getIndFase(),"1") %> class="sem-borda" style="height: 14px; width: 14px;">Autua��o
   	      <input type="radio" name="indFaseradio" value="2" <%= sys.Util.isChecked(consultaAutoId.getIndFase(),"2") %> class="sem-borda" style="height: 14px; width: 14px;">Penalidade&nbsp;&nbsp;
       	  <input type="radio" name="indFaseradio" value="9" <%= sys.Util.isChecked(consultaAutoId.getIndFase(),"9") %> class="sem-borda" style="height: 14px; width: 14px;">Todas&nbsp;&nbsp;				  		
		  <input name="indFase" type="hidden" value="">				       	  
		</td>		
	  </tr>			  
      <tr><td colspan=6 height="5"></td></tr>	  
      <tr bgcolor="<%= Cor %>"> 
        <td><strong>&nbsp;Dt Infra��o: De</strong></td>
        <td><input name="De" type="text" id="De" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoDe() %>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"></td>
        <td colspan=2 rowspan=2>&nbsp;&nbsp;&nbsp;
			<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="LocalizarAutoInfracao"  onClick="javascript: valida('ConsultaAuto',this.form);">	
			  		<img src="<%= path %>/images/REG/bot_ok.gif" alt="Confirmar" width="26" height="19">
			</button>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button type=button style="height: 21px; width: 32px;border: none; background: transparent;"  NAME="LimparAutoInfracao"  onClick="javascript: valida('Novo',this.form);">	
			    <IMG src="<%= path %>/images/REG/bot_limpar.gif" width="29" height="19"  align="left" alt="Limpar Campos" >
			</button>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimeAuto',this.form)"> 
              <img src="<%= path %>/images/REG/bot_imprimir2_ico.gif" alt="Imprimir" width="29" height="19">
			</button>
 		    &nbsp;&nbsp;&nbsp;
		</td>
		<td rowspan=2>&nbsp;&nbsp;&nbsp;Qto Situa��o:</td>
        <td>
		      <input type="checkbox" name="Sit1" value="1" class="input" <%= ((consultaAutoId.getIndSituacao().indexOf("1")>=0) ? "checked" : "") %> onClick="javascript: apagaTodos()">Ativo&nbsp;
		      <input type="checkbox" name="Sit2" value="2" class="input" <%= ((consultaAutoId.getIndSituacao().indexOf("2")>=0) ? "checked" : "") %> onClick="javascript: apagaTodos()">&nbsp;Em Recurso&nbsp;
		      <input type="checkbox" name="Sit9" value="9" class="input" <%= ((consultaAutoId.getIndSituacao().indexOf("9")>=0) ? "checked" : "") %> >&nbsp;Todos
		</td>
      </tr>
      <tr bgcolor="<%= Cor %>"> 
        <td align=right><strong>&nbsp;At&eacute;&nbsp;&nbsp;</strong></td>
	  	<td><input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=consultaAutoId.getDatInfracaoAte() %>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();"></td>
        <td>
		      <input type="checkbox" name="Sit3" value="3" class="input" <%= ((consultaAutoId.getIndSituacao().indexOf("3")>=0) ? "checked" : "") %> onClick="javascript: apagaTodos()">Suspensos&nbsp;		  
				&nbsp;&nbsp;&nbsp;
		      <input type="checkbox" name="Sit4" value="4" class="input" <%= ((consultaAutoId.getIndSituacao().indexOf("4")>=0) ? "checked" : "") %> onClick="javascript: apagaTodos()">&nbsp;Cancelados
			  <input name="indSituacao" type="hidden" value="">				
		</td>
      </tr>	  
      <tr><td height=2 colspan=5 bgcolor="#666666"></td></tr>		      
    </table>
</div>
	
<div id="cons3" style="position:absolute; left:50px; top:180px; width:720px; height:44px; z-index:5; overflow: visible; visibility: visible;"> 
    <!--Dados vari�veis "A" para evento 14-->
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
      <tr bgcolor="#ffffff"> 
        <td height="14" colspan=3>
			 <% if (consultaAutoId.getAutos().size()>0) {	%>	
			<font color="#000000">&nbsp;&nbsp;&nbsp;LOCALIZADO(s)&nbsp;<%= consultaAutoId.getAutos().size()%>&nbsp;AUTO(s)</font>
			<% } %>
		</td>
        <td colspan=2 align=right>
			<font color="#000000">&nbsp;Ordem:&nbsp;<%= consultaAutoId.getNomOrdem()%>&nbsp;</font>
		</td>		
      </tr>
      <tr bgcolor="a8b980"> 
        <td width="60" height="14"><strong><a href="#" onClick="javascript: Classificacao('Orgao',this.form);">
			<font color="#FFFFFF">&nbsp;&nbsp;&nbsp;�rg�o</font></a></strong></td>
		<td width="80"><strong><a href="#" onClick="javascript: Classificacao('Auto',this.form);">
			<font color="#FFFFFF">&nbsp;&nbsp;&nbsp;N&ordm; Auto</font></a></strong></td>
        <td width="80"><strong><a href="#" onClick="javascript: Classificacao('Placa',this.form);">
			<font color="#FFFFFF">&nbsp;&nbsp;&nbsp;Placa</font></a></strong></td>
        <td width="400"><strong><a href="#" onClick="javascript: Classificacao('Processo',this.form);">
        	<font color="#FFFFFF">&nbsp;&nbsp;&nbsp;Processo</font></a></strong></td>
        <td><strong><font color="#FFFFFF">&nbsp;&nbsp;&nbsp;Dt Notif.</font></strong></td>
      </tr>
	</table>
</div>
  
<div id="cons4" style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 211px; height: 143px; visibility: visible;" > 
    <table id="autos" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
	  <%int seq = 0;
	   if (consultaAutoId.getAutos().size()>0) {
	  	int a =0;
	  	
		String cor   = (a%2==0) ? "#eff5e2" : "#ffffff";
		String fonte="";		
		Iterator it = consultaAutoId.getAutos().iterator() ;
		REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
		    seq++;
			not   = (REC.AutoInfracaoBean)it.next() ;				
			cor   = (a%2==0) ? "#EFF5E2" : "#ffffff";
			fonte = (a%2==0) ? "#ffffff" : "#000000";  
		%>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td width=60 rowspan=2 height="14" align=center>&nbsp;<%=seq %> </td>
			<td width=80 rowspan=2>&nbsp;<b><%=not.getNumAutoInfracao() %></b> </td>
        	<td width=80 rowspan=2>&nbsp;<b><%=not.getNumPlaca() %></b></td>
	        <td width=383>&nbsp;<%= not.getNumProcesso()%></td>
			<td>&nbsp;
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	<%= not.getDatNotificacao()%>
			<% } %>	        
        	</td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
    	    <td colspan=2>&nbsp;Situa��o:&nbsp;<%= not.getSituacao()%>
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= not.getNomStatus()%>
			<% } %>
			</td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%=not.getSigOrgao() %> </td>         	    
        	<td colspan=2>
        		<a href="#" onClick="javascript: Classificacao('Data',this.form);">        	
	        		&nbsp;Data:&nbsp;<%= not.getDatInfracao()%>&nbsp;&nbsp;<%=not.getValHorInfracaoEdt() %>
	        	</a>
	        </td>
	        <td height="14" colspan=2>&nbsp;Infra��o:&nbsp;<%=not.getInfracao().getCodInfracao() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscInfracao() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
        	<td colspan=2>&nbsp;Vencimento:&nbsp;<%= not.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan=2>&nbsp;Local:&nbsp;<%=not.getDscLocalInfracao() %>&nbsp;&nbsp;<%= not.getNomMunicipio() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>    	    
        	<td colspan=2>&nbsp;Valor:&nbsp;<%= not.getInfracao().getValRealEdt()%>&nbsp;</td>											
	        <td height="14" colspan=2>
	          	<a href="#" onClick="javascript: Classificacao('Enquadramento',this.form);">	        
		        	&nbsp;Enquadramento:&nbsp;<%=not.getInfracao().getDscEnquadramento() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscGravidade() %>&nbsp;-&nbsp;<font color=red><b>Pontos:&nbsp;<%= not.getInfracao().getNumPonto() %></b></font>
				</a>
		    </td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" colspan=4>
	          	<a href="#" onClick="javascript: Classificacao('Responsavel',this.form);">	        
		        	&nbsp;Propriet�rio:&nbsp;<%=not.getProprietario().getNomResponsavel() %>
		        </a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Condutor:&nbsp;
			<% if (not.getCondutor().getNomResponsavel().length()>0) { %>
				<%=not.getCondutor().getNomResponsavel() %>
			<% } else { %>
			    Condutor n�o Identificado
			<% } %>
			</td>
		</tr>		
		<% a++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if ((msg.length()>0 && pendentes.size()==0 && pendentesDol.size()==0)) {
	%>	
	
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='<%= Cor %>'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	
	<% 
		}
	} 
	%>
	
	
	<%
	
	 if (pendentes.size()>=0) {
	  	int a =0;
	  	
		String cor   = (a%2==0) ? "#eff5e2" : "#ffffff";
		String fonte="";		
		Iterator it = pendentes.iterator() ;
		REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
		    seq++;
			not   = (REC.AutoInfracaoBean)it.next() ;				
			cor   = (a%2==0) ? "#EFF5E2" : "#ffffff";
			fonte = (a%2==0) ? "#ffffff" : "#000000";  
		%>
		
			<tr bgcolor='#deebc2' style="cursor:hand; color:'#FFFFFF'; "> 
		        <td colspan=8 height="20" align="center" style="letter-spacing:1px;"><strong>&nbsp;AUTOS PENDENTES DE REGISTRO - MR</strong></td>
		    </tr>
	    
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td width=60 rowspan=2 height="14" align=center>&nbsp;<%=seq %> </td>
			<td width=80 rowspan=2>&nbsp;<b><%=not.getNumAutoInfracao() %></b> </td>
        	<td width=80 rowspan=2>&nbsp;<b><%=not.getNumPlaca() %></b></td>
	        <td width=383>&nbsp;<%= not.getNumProcesso()%></td>
			<td>&nbsp;
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	<%= not.getDatNotificacao()%>
			<% } %>	        
        	</td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
    	    <td colspan=2>
			</td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%=not.getSigOrgao() %> </td>         	    
        	<td colspan=2>
        		<a href="#" onClick="javascript: Classificacao('Data',this.form);">        	
	        		&nbsp;Data:&nbsp;<%= not.getDatInfracao()%>&nbsp;&nbsp;<%=not.getValHorInfracaoEdt() %>
	        	</a>
	        </td>
	        <td height="14" colspan=2>&nbsp;Infra��o:&nbsp;<%=not.getInfracao().getCodInfracao() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscInfracao() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
        	<td colspan=2>&nbsp;Vencimento:&nbsp;<%= not.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan=2>&nbsp;Local:&nbsp;<%=not.getDscLocalInfracao() %>&nbsp;&nbsp;<%= not.getNomMunicipio() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>    	    
        	<td colspan=2>&nbsp;Valor:&nbsp;<%= not.getInfracao().getValRealEdt()%>&nbsp;</td>											
	        <td height="14" colspan=2>
	          	<a href="#" onClick="javascript: Classificacao('Enquadramento',this.form);">	        
		        	&nbsp;Enquadramento:&nbsp;<%=not.getInfracao().getDscEnquadramento() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscGravidade() %>&nbsp;-&nbsp;<font color=red><b>Pontos:&nbsp;<%= not.getInfracao().getNumPonto() %></b></font>
				</a>
		    </td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" colspan=4>
	         &nbsp;Condutor:&nbsp;
			<% if (not.getCondutor().getNomResponsavel().length()>0) { %>
				<%=not.getCondutor().getNomResponsavel() %>
			<% } else { %>
			    Condutor n�o Identificado
			<% } %>
			</td>
		</tr>		
		<% a++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>	
	
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='<%= Cor %>'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	
	<% 
		}
	} 
	%>
	
	<%
	
	 if (pendentesDol.size()>=0) {
	  	int a =0;
	  	
		String cor   = (a%2==0) ? "#eff5e2" : "#ffffff";
		String fonte="";		
		Iterator it = pendentesDol.iterator() ;
		REC.AutoInfracaoBean not  = new REC.AutoInfracaoBean();		
		while (it.hasNext()) {
		    seq++;
			not   = (REC.AutoInfracaoBean)it.next() ;				
			cor   = (a%2==0) ? "#EFF5E2" : "#ffffff";
			fonte = (a%2==0) ? "#ffffff" : "#000000";  
		%>
		
			<tr bgcolor='#deebc2' style="cursor:hand; color:'#FFFFFF'; "> 
		        <td colspan=8 height="20" align="center" style="letter-spacing:1px;"><strong>&nbsp;AUTOS PENDENTES DE REGISTRO - DOL</strong></td>
		    </tr>
	    
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td width=60 rowspan=2 height="14" align=center>&nbsp;<%=seq %> </td>
			<td width=80 rowspan=2>&nbsp;<b><%=not.getNumAutoInfracao() %></b> </td>
        	<td width=80 rowspan=2>&nbsp;<b><%=not.getNumPlaca() %></b></td>
	        <td width=383>&nbsp;<%= not.getNumProcesso()%></td>
			<td>&nbsp;
			<%  if (sys.Util.DifereDias("16/07/2004",not.getDatInfracao())>=0) { %>
	        	<%= not.getDatNotificacao()%>
			<% } %>	        
        	</td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
    	    <td colspan=2>
			</td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%=not.getSigOrgao() %> </td>         	    
        	<td colspan=2>
        		<a href="#" onClick="javascript: Classificacao('Data',this.form);">        	
	        		&nbsp;Data:&nbsp;<%= not.getDatInfracao()%>&nbsp;&nbsp;<%=not.getValHorInfracaoEdt() %>
	        	</a>
	        </td>
	        <td height="14" colspan=2>&nbsp;Infra��o:&nbsp;<%=not.getInfracao().getCodInfracao() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscInfracao() %></td>			
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
        	<td colspan=2>&nbsp;Vencimento:&nbsp;<%= not.getDatVencimento()%>&nbsp;</td>											
	        <td height="14" colspan=2>&nbsp;Local:&nbsp;<%=not.getDscLocalInfracao() %>&nbsp;&nbsp;<%= not.getNomMunicipio() %></td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" rowspan=2 align=center>&nbsp;<%= (not.getAutoAntigo().equals("1") ? "(ANT.)" : "") %> </td>    	    
        	<td colspan=2>&nbsp;Valor:&nbsp;<%= not.getInfracao().getValRealEdt()%>&nbsp;</td>											
	        <td height="14" colspan=2>
	          	<a href="#" onClick="javascript: Classificacao('Enquadramento',this.form);">	        
		        	&nbsp;Enquadramento:&nbsp;<%=not.getInfracao().getDscEnquadramento() %>&nbsp;-&nbsp;<%= not.getInfracao().getDscGravidade() %>&nbsp;-&nbsp;<font color=red><b>Pontos:&nbsp;<%= not.getInfracao().getNumPonto() %></b></font>
				</a>
		    </td>
		</tr>
		<tr bgcolor='<%=cor%>' style="cursor:hand; color:'<%=fonte%>'; "> 
	        <td height="14" colspan=4>
	         &nbsp;Condutor:&nbsp;
			<% if (not.getCondutor().getNomResponsavel().length()>0) { %>
				<%=not.getCondutor().getNomResponsavel() %>
			<% } else { %>
			    Condutor n�o Identificado
			<% } %>
			</td>
		</tr>		
		<% a++; }
	} else {
		 String msg=(String)request.getAttribute("semAuto"); if (msg==null) msg="";
		 if (msg.length()>0) {
	%>	
	
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='<%= Cor %>'> 
	        <td height="35" align=center><b><%= msg %></b></td>
		</tr>		
	
	<% 
		}
	} 
	%>
	
	
    </table>      
  </div>  
<!--FIM_CORPO_sistema--> 
<input name="acao"           type="hidden" value=' '>
<input name="ordem"          type="hidden" value="Data">
<!-- Rodap�-->
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

<!--Div Erros-->
<%
String msgErro = consultaAutoId.getMsgErro();
String msgOk = consultaAutoId.getMsgOk();
String msgErroTop = "160 px";
String msgErroLeft = "80 px";
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->

</form>
</body>
</html>