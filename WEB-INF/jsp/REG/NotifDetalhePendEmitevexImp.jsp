<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="REG.NotifControleBean"%>

<jsp:useBean id="NotifControleId"  scope="request" class="REG.NotifControleBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
	DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->
<%
  	int contLinha=0,npag = 0,seq = 0;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	Iterator it = NotifControleId.getListaNotifs().iterator();
	NotifControleBean notifControle = new NotifControleBean();
	
	if (NotifControleId.getListaNotifs().size() > 0) {				
		while (it.hasNext()) {
			   notifControle =(NotifControleBean)it.next();
			   seq++; contLinha++;
			   if (contLinha%35==1){
					npag++;
					if (npag!=1){
%>
						<jsp:include page="Rod_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
				<% } %>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
			<tr>
				<td width="30"  height="15" align="center" bgcolor="#999999"><font color="#000000"><b>Seq</b></td>
			   	<td width="80" height="15" align="center" bgcolor="#999999"><font color="#000000"><b>Notifica&ccedil;&atilde;o</b></td>
	        	<td width="80" align="center" bgcolor="#999999"><font color="#000000"><b>Auto</b></td>
	        	<td width="70" align="center" bgcolor="#999999"><font color="#000000"><b>Placa</b></td>
	        	<td width="80" align="center" bgcolor="#999999"><font color="#000000"><b>Emiss&atilde;o</b></td>
	        	<td align="center" bgcolor="#999999"><font color="#000000"><b>Arquivo Recebido</b></td>				
			</tr>
		</table>
		 <% } %>
	<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
		<tr> 
			
			<td width="30" height="18" align="center" style="line-height:20px;"><%=seq%></td>
				<td width="80" height="18" align="center" style="line-height:20px;"><%=notifControle.getNumNotificacao()%></td>
			    <td width="80" align="center" style="line-height:20px;"><%=notifControle.getNumAutoInfracao()%></td>
			    <td width="70" align="center" style="line-height:20px;"><%=notifControle.getNumPlaca()%></td>
		<%			if(notifControle.getDatEmissao() == null){%>
				<td width="80" align="center" style="line-height:20px;"></td>
		<%			}else{%>
			    <td width="80" align="center" style="line-height:20px;"><%=sdf.format(notifControle.getDatEmissao())%></td>
		<%			}%>
			    <td align="center" style="line-height:20px;"><%=notifControle.getNomArquivo()%></td>
		</tr>			
		<tr><td height=1 bgcolor="#000000" colspan=6></td></tr>
		<%	} %>
	</table>

<%} else { 
	String msg=(String)request.getAttribute("semLog"); if (msg==null) msg="Nenhuma notifica&ccedil;&atilde;o existente";
%>

<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
<%	if (NotifControleId.getListaNotifs().size() > 0) { 
		if (contLinha<54){

		} %>
		<jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
</form>
</body>
</html>