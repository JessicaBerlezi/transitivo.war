<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path1=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="RelatArDigitTMPBeanId" scope="session" class="REG.RelatArDigitTMPBean" /> 

<html>
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.linhas td{border: 1px solid #ff0000;}
</style>
<title>:: SMIT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path1 %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="Form" method="post" action="">
<%@ include file="Cab.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 



function valida(opcao,fForm,numNotif) {
 
 switch (opcao) {
   case 'Prox':
		document.Form.acao.value=opcao
		document.Form.target= "_self";
		document.Form.action = "acessoTool";  
		document.Form.submit();	  		  
	  break ; 
   case 'Ant':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_self";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
   case 'Imprimir':
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
	  
   case 'buscaArDigitTMP':
        document.Form.numNotif.value=numNotif
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
	  break ;
	case 'R':  
	  close() ;   
      break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
  }
}



</script>

<input name="acao" type="hidden" value=' '>				
<input name="numNotif" type="hidden" value=' '>	

<%@ include file="RelatArDigitTMP_Lista.jsp" %>


<!-- cabe�alho da transa��o -->
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 115px; left: 50px; height: 18px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>
      <td width="85" height="18" bgcolor="#deebc2">&nbsp;&nbsp;<strong>N&ordm; Notifica&ccedil;&atilde;o</strong></td>
     <td width="85" height="18" bgcolor="#deebc2">&nbsp;&nbsp;<strong>N&ordm; do Auto</strong></td>
	  <td width="80" height="18" bgcolor="#deebc2">&nbsp;&nbsp;<strong>N&ordm; do Lote</strong></td>
	  <td width="80" height="18" bgcolor="#deebc2">&nbsp;&nbsp;<strong>N&ordm; da Caixa</strong></td>
	  <td width="80" height="18" bgcolor="#deebc2">&nbsp;&nbsp;<strong>C&oacute;d. Retorno</strong></td>
	  <td bgcolor="#deebc2">&nbsp;&nbsp;<strong>Dsc. Retorno</strong></font></td>
	</tr>
  </table>
</div>   
<!-- fim  cabe�alho da transa��o -->
				  
<!-- Registros c/ barra de scroll . Aqui v�o entrar os includes-->
<div style="position:absolute; width:720px; overflow: auto; z-index: 100; top: 134px; left: 50px; height: 181px; visibility: visible;">   
	<%
		
		
        int max = RelatArDigitTMPBeanId.getDados().size() ;
		if (max>0) { 
		for(int i=0;i<max;i++){
		
		   String dados = RelatArDigitTMPBeanId.getDadosArray(i);
		
		
	%>       

	 <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
	   <tr>
	     
        <td width="85">
    	<a href="#"  class="preto"  onClick="javascript: valida('buscaArDigitTMP',this.form,'<%=dados.substring(0,9)%>')">
        <%= dados.substring(0,9)  %></a></td>
	     <td width="85">&nbsp;&nbsp;<%= dados.substring(9,21) %></td>
	     <td width="80">&nbsp;&nbsp;<%= dados.substring(21,24) %></td>
	     <td width="80">&nbsp;&nbsp;<%= dados.substring(24,32) %></td>
	     <td width="80">&nbsp;&nbsp;<%= dados.substring(32,35) %></td>
	     <td>&nbsp;&nbsp;<%= dados.substring(35,135) %></td>
	   </tr>
	   <tr> 
		 <td bgcolor="#cccccc" height="1" colspan="6"></td>
	   </tr>
	 </table>
	<%
        }		
	%>       
 
 </div>

<!-- fim Registros c/ barra de scroll -->

<!-- Navega��o-->
<%@ include file="RelatArDigitTMP_Nav.jsp" %>
<!-- Fim Navega��o-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',document.frmDownload);"> 
          <img src="<%= path1 %>/images/REG/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
  </div>	
		<% }else{%>	
		 <!--Bot�o retornar-->
		<table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
				<tr><td height="40" ></td></tr>		
				<tr bgcolor='#CCCCCC'> 
			   	    <td height="35" align=center><b>N�o existem dados a serem retornados </b></td>
				</tr>	
		</table>      	
        </div>
		  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
		    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
				<tr> 
				  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',document.frmDownload);"> 
		          <img src="<%= path1 %>/images/REG/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
				</tr>
			</table>
		  </div>	
 		<% }%>	
	
	
	

<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</BODY>
</HTML>


