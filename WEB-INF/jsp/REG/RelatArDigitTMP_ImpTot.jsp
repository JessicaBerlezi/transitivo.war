<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="sys.Util" %>
<jsp:useBean id="RelatArDigitTMPBeanId" scope="session" class="REG.RelatArDigitTMPBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 


		<%
		  	int contLinha=0;
			int npag = 0;
			int seq = 0;	
			boolean passa = true;	
		    int max = RelatArDigitTMPBeanId.getDados().size() ;
		    
	        Iterator it = RelatArDigitTMPBeanId.getEventos().iterator();
	        REG.RelatArDigitTMPBean_Cont relatorioImp  = new REG.RelatArDigitTMPBean_Cont();					
	        if( RelatArDigitTMPBeanId.getEventos().size()>0 ) { 
		      while (it.hasNext()) {
			   relatorioImp = (REG.RelatArDigitTMPBean_Cont)it.next() ;
					seq++;
					contLinha++;
					
					if (contLinha%50==1){
						npag++;							
						if (npag!=1){
						passa = true;
				%>			 					
						<jsp:include page="rodape_impconsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
						<% } %>				
					<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
					<jsp:param name="nPag" value= "<%=npag%>" />				
					</jsp:include> 			
		         <% } %>
			<%if(passa){%>
			<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">		
		    	<tr>
				  <td width="36%">&nbsp;&nbsp<strong>Data de Envio:&nbsp;</strong>&nbsp;<input name="datIni" type="text" id="datIni" size="12" maxlength="12" readonly="readonly" value="<%=RelatArDigitTMPBeanId.getDatInicio()%>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();" style="border: 0px none;">
				  &nbsp;at�&nbsp;<input name="datFim" type="text" id="datFim" size="12" maxlength="12" readonly="readonly" value="<%=RelatArDigitTMPBeanId.getDatFim()%>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();" style="border: 0px none;">
				  </td>		  
				</tr>
				<tr><td height="5"></td></tr>	
		  	</table>
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
		  		<tr bgcolor="#cccccc">
			        <td width="12%" height="15"  align="center" ><strong>Dia</strong></td>
			        <td width="12%" style="text-align: right"><strong>Retornados</strong>&nbsp;&nbsp;&nbsp;</td>
			        
			        <td width="12%" height="15"  style="text-align: right" ><strong>Apropriados</strong>&nbsp;&nbsp;&nbsp;</td>
			        <td width="12%" style="text-align: right"><strong>Desprezados</strong>&nbsp;&nbsp;&nbsp;</td>
			        <td width="12%" style="text-align: right"><strong>N�o Processados</strong>&nbsp;&nbsp;&nbsp;</td>
			       
			        <td   style="text-align: right"><strong>Avisos</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		 		</tr>
			</table>
		  	<% } %>	
		    <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="1">
		  <tr>
		        <td width="12%" height="15" style="text-align: center"><%=relatorioImp.getDatEnvio()%></td>		
		        <td width="12%" style="text-align: right">
		        <%= Util.formataIntero(relatorioImp.getQtdRetornados())  %>&nbsp;&nbsp;&nbsp;</a></td>
		        <td width="12%" style="text-align: right">
		        <%= Util.formataIntero(relatorioImp.getQtdApropriado())%>&nbsp;&nbsp;&nbsp;</a></td>
		        <td width="12%" style="text-align: right">
		        <%= Util.formataIntero(relatorioImp.getQtdDesprezados())%>&nbsp;&nbsp;&nbsp;</a></td>
		        <td width="12%" style="text-align: right">
		        <%= Util.formataIntero(relatorioImp.getQtdNaoProcessados()) %>&nbsp;&nbsp;&nbsp;</a></td>
		        <td height="12%" style="text-align: right">
		        <%= Util.formataIntero(relatorioImp.getQtdAvisos()) %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
		  </tr>
		  
			<td bgcolor="#00000" height="1" colspan="7"></td>
		  </tr>
		</table>      

		<% passa=false;} %>
		
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
<tr> 
        <td width="12%" height="15"  bgcolor="#CCCCCC"  style="text-align: right"><strong>TOTAL :</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>		
        <td width="12%"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdRetornadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="12%"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdApropriadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="12%"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdDesprezadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="12%"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdNoaProcessTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdAvisoTot()) %></strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
</table>
		
		
		
		<% }else{%>	
		<table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
				<tr><td height="40" ></td></tr>		
				<tr bgcolor='#CCCCCC'> 
			   	    <td height="35" align=center><b>N�o existem dados a serem retornados </b></td>
				</tr>	
		</table>      	
		
		<%}%>
		<%	if (RelatArDigitTMPBeanId.getDados().size()>0) { 
				if (contLinha<51){
		
				} %>
				<jsp:include page="rodape_impconsulta.jsp" flush="true" />
		<%} %>

</script>		





</form>
</body>
</html>