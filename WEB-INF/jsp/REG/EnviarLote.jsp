<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="ArquivoDolId" scope="request" class="REG.ArquivoDolBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />  
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>DETRAN � Sistema de Monitoramento de Multas de Tr�nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%=path %>/sys/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

var bProcessando=false;
function valida(opcao,fForm) {
  switch (opcao) {
   case 'OK':
   if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;
			    if (document.UsrForm.arqUpload.value.length==0) alert("Nenhum arquivo selecionado.") ;
				else {
				var funcao = '<%=UsuarioFuncBeanId.getJ_sigFuncao()%>';		
					 if  (funcao!='REG0120'){
						 	if ((document.UsrForm.arqUpload.value).indexOf(document.UsrForm.proximoArquivo.value)<0) {
								alert("Arquivo selecionado n�o � "+document.UsrForm.proximoArquivo.value+".") ;
						 		break;
						 	}
					 }
							document.UsrForm.acao.value="Upload"	;
							document.UsrForm.target= "_self";
				    		document.UsrForm.action = "acessoTool";
					   		document.UsrForm.submit();	   
				}	
  }
	break;
   case 'R':
    close() ;
	break;
  }
}
</script>
</head>
<form enctype="multipart/form-data" name="UsrForm" method="post" action="" >
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<!-- Cabe�alho -->
<jsp:include page="Cab.jsp" flush="true" />

<!--Upload de Arquivo-->

<% if  (!"REG0120".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %>
   
	<div id="Upload" style="position:absolute; left:50px; top:87px; width:720px; height:200px; z-index:1; visibility: visible;"> 
 <%}else{ %>
	 <div id="Upload" style="position:absolute; left:50px; top:87px; width:720px; height:200px; z-index:1; visibility: hidden;"> 
  <%} %>
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table">
  
  
   <tr bgcolor="#EFF5E2"> 	
	  <td ></td>
      <td colspan=2>ARQUIVO ESPERADO</td>
      <td colspan=2>&nbsp;: 
		<input name="proximoArquivo"  type="text" readonly value='<%=request.getAttribute("proximoArquivo")%>' size="26" maxlength="19">				
      </td>
    </tr>
	
	<tr>
		<td width="3%"></td>
		<td height="12" width="8%"></td>
		<td width="9%"></td>
		<td width="40%"></td>
		<td width="44%"></td>
	</tr>
</table>
</div>
<div id="Upload2" style="position:absolute; left:50px; top:116px; width:720px; height:200px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="2" class="table">	
    <tr bgcolor="#EFF5E2"> 
	  <td ></td>
      <td >ARQUIVO</td>
      <td colspan=3>&nbsp;: 
        <input name="arqUpload"    type="file" size="80" maxlength="60" onKeyUp="this.value=this.value.toUpperCase()">
      </td>
    </tr>
	<tr><td height="10" colspan=5></td></tr>
    <tr><td align="center" colspan=5>
		<button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" onClick="javascript: valida('OK',this.form);">
		<img src="images/bot_enviar.gif" width="41" height="19"></button></td>
    </tr>
  </table>
</div>
<!--Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!--FIM Rodap�-->
<input name="acao"            type="hidden" value="Upload">
</body>
</form>
</html>
