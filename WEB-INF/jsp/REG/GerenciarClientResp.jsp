<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->  
<%@ page session="true" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Carrega Arquivos -->
<jsp:useBean id="GerenciarClientBean" scope="request" class="REG.GerenciarClientBean" />
            
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   switch (opcao) {    
 
   case 'V':
        fForm.atualizarDependente.value="N"	  
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
 
   case 'liberar':
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;

   case 'parar':
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break; 
   case 'R':
	 close() ;   
	 break;
	  		
   case 'O':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	  else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
	  
   case 'V':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	  else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}


</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="form" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="valClient" type="hidden" value='<%=GerenciarClientBean.getValClient()%>'>

<div id="topo" style="position:absolute; width:720px; overflow: visible; z-index: 2; top: 95px; left: 50px; visibility: visible;" > 

  <TABLE width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#EFF5E2" >
    <tr>
      <td width="137">&nbsp;</td>
	  <td width="93" height="40"><b>Client&nbsp;</b></td>
	  <td colspan="4"><strong>:</strong>
			&nbsp;&nbsp;
			<input readonly name="nomClient" type="text" size="60"  maxlength="50" value="<%=GerenciarClientBean.getNomClient()%>" style="border: 0px none; background-color:transparent;">
    </tr>
    <tr>
        <td height="2" colspan="4"></td>
    </tr>
	<tr>
	   <td>&nbsp;</td>
	   <td height="40" ><b>Situa��o do Client&nbsp;</b></td>
	   <td colspan="4"><strong>:</strong>
			   &nbsp;&nbsp;<input readonly type="text" name="situacao" value="<%=GerenciarClientBean.getSituacaoClient() %>" style="border: 0px none; background-color:transparent; "  >
	   </td>
    </tr>
	 <tr>
        <td height="4" colspan="4"></td>
	  <td width="23">&nbsp;</td>
	  <td width="401">
	  <% if (GerenciarClientBean.getStatusClient().equals("1")){%>
	    <img src="<%= path %>/images/REG/im_cadeado_fechado.gif" width="59" height="51">
     <%}%>
     <% if (GerenciarClientBean.getStatusClient().equals("0")){%>
	    <img src="<%= path %>/images/REG/im_cadeado_aberto.gif" width="59" height="51">
	 <%}%>   
	  </td>
	</tr>
	<tr><td colspan="4" height="15"></td></tr>
  </table>
</div>

 <div style="position:absolute; left:50px; top:297px; width:720px; height:36px; z-index:1; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;" id="botRetornar"> 
 	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	
 		<tr>
		 <td  align="center">
 		 <% if (GerenciarClientBean.getStatusClient().equals("0")){%>
				  <button type="button" NAME="parar"  style="height: 28px; width: 70px;border: none; background: transparent;" onClick="javascript: valida('parar',this.form);">
					<IMG src="<%= path %>/images/bot_parar_det1.gif" width="57" height="26" align="left"  >
		          </button>	
		 <%}%>
        <% if (GerenciarClientBean.getStatusClient().equals("1")){%>
				  <button type="button" NAME="liberar"  style="height: 28px; width: 70px;border: none; background: transparent;" onClick="javascript: valida('liberar',this.form);">
					<IMG src="<%= path %>/images/bot_liberar_det1.gif" width="64" height="26" align="left"  >
		          </button>		
		 <%}%>
		</td>	
		</tr>
 	</table>
 </div>     
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= GerenciarClientBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "190 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  

</form>
</BODY>
</HTML>
