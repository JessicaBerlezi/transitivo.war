<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->  
<%@ page session="true" %>
<% String path=request.getContextPath(); %>       
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Carrega Arquivos -->
<jsp:useBean id="paraRobot" scope="request" class="REG.GerenciarRobotBean" />
<jsp:setProperty name="paraRobot" property="j_abrevSist" value="REG" />  
<jsp:setProperty name="paraRobot" property="colunaValue" value="NOM_ROBO"/>
<jsp:setProperty name="paraRobot" property="popupNome"   value="codMonitor"/>
<jsp:setProperty name="paraRobot" property="popupString" value="nom_robo, select distinct nom_robo FROM TSMI_GERENCIA_ROBO WHERE cod_tipo_Registro = 1 ORDER BY nom_robo" />
            
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
   switch (opcao) {    
             
   case 'busca':
       	if(veCampos(fForm) == true){
       		if (fForm.codMonitor.options[fForm.codMonitor.selectedIndex].value == 0){
			  fForm.nomRobo.value = "";
			}
			else{
			  fForm.nomRobo.value = fForm.codMonitor.options[fForm.codMonitor.selectedIndex].text;
			}		 		
			fForm.target= "_self";
			fForm.acao.value=opcao;
	  		fForm.action = "acessoTool";  
	  		fForm.submit();	 
   	    }
	  	break;
	  
   case 'V':
        fForm.atualizarDependente.value="N"	  
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;
 
   case 'liberar':
        fForm.atualizarDependente.value="N"	  
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;

   case 'parar':
        fForm.atualizarDependente.value="N"	  
     	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break; 
   case 'R':
	 close() ;   
	 break;
	  		
   case 'O':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	  else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
	  
   case 'V':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	  else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
   }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;        	
    
    if(fForm.codMonitor == 0){
	  valid = false
	  sErro = "Selecione o Robot. \n"
	}

	if (valid==false) alert(sErro) 
	
	return valid;
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="form" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>
<input name="nomRobo" type="hidden" value='<%= paraRobot.getNomProcesso() %>'> 		
<input name="atualizarDependente" type="hidden" value=''>
<input name="liberarBotao" type="hidden" value=''>
<input name="pararBotao" type="hidden" value=''>
<input name="situacao" type="hidden" value='<%= (String)request.getAttribute("situacao")%>'>


<%if (((String)request.getAttribute("atualizarDependente")).compareTo("N")==0){%>
 <div id="div1" style="position:absolute; width:700px; overflow: visible; z-index: 1; top: 83px; left:204px; visibility: visible;"  > 
  <TABLE border="0" align="center" cellpadding="0" cellspacing="0" width="100%" >
    <TR>
	    <td width="61"><b>Robot&nbsp;</b></td>
		<td width="308" ><strong>:</strong> 
	        <jsp:getProperty name="paraRobot" property="popupString" />
	  	</td>  
	    <td align=left width=351>&nbsp;&nbsp;&nbsp;
       		<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);"  >	
    		<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
  	    </td>
	 </tr>
	 <tr>
         <td height="1" colspan="6"></td>
     </tr>
  </table>
</div>

<% }
			else { %>
<div id="topo" style="position:absolute; width:720px; overflow: visible; z-index: 2; top: 95px; left: 50px; visibility: visible;" > 

  <TABLE width="100%" border="0" align="center" cellpadding="1" cellspacing="0" bgcolor="#EFF5E2" >
    <tr>
      <td width="152">&nbsp;</td>
	  <td width="133" height="40"><b>Robot&nbsp;</b></td>
	  <td colspan="2"><strong>:</strong>
			&nbsp;&nbsp;
			<input readonly name="nomRobo" type="text" size="60"  maxlength="50" value="<%=paraRobot.getNomProcesso()%>" style="border: 0px none; background-color:transparent;">
		<input name="codMonitor" type="hidden" size="6" maxlength="2"  value="<%= paraRobot.getCodMonitor() %>"></td>
     </tr>
	 <tr>
        <td height="2" colspan="4"></td>
     </tr>
	 <tr>
	   <td>&nbsp;</td>
	   <td height="40" ><b>Situa��o de Execu��o&nbsp;</b></td>
	   <td colspan="2"><strong>:</strong>
			   &nbsp;&nbsp;<input readonly type="text" name="situacao" value="<%=paraRobot.getSituacaoExecucao() %>" style="border: 0px none; background-color:transparent; "  >
	   </td>
    </tr>
    <tr>
	   <td>&nbsp;</td>
	   <td height="40" ><b>Data da &Uacute;ltima  Execu��o&nbsp;</b></td>
	   <td colspan="2"><strong>:</strong>
			   &nbsp;&nbsp;<input readonly type="text" name="datUltimaExec" value="<%=paraRobot.getDatUltimaExec()%>" style="border: 0px none; background-color:transparent; "  >
	   </td>
     </tr>
	 <tr>
        <td height="2" colspan="4"></td>
     </tr>	
	<tr>
	  <td width="152">&nbsp;</td>
	  <td width="133" height="40"><b>Situa��o de Bloqueio&nbsp;</b></td>
	  <td width="141"><strong>:</strong>
			   &nbsp;&nbsp;
			   <input readonly type="text" name="situacao" value="<%=paraRobot.getSituacaoBloqueio() %>" style="border: 0px none; background-color:transparent;"  ></td>
	  <td width="286">
	  <% if (((String)request.getAttribute("pararBotao")).compareTo("N")==0){%>
	    <img src="<%= path %>/images/REG/im_cadeado_fechado.gif" width="59" height="51">
     <%}%>
     <% if (((String)request.getAttribute("liberarBotao")).compareTo("N")==0){%>
	    <img src="<%= path %>/images/REG/im_cadeado_aberto.gif" width="59" height="51">
	 <%}%>   
	  </td>
	</tr>
	<tr><td colspan="4" height="15"></td></tr>
  </table>
</div>

 <div style="position:absolute; left:50px; top:297px; width:720px; height:36px; z-index:1; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;" id="botRetornar"> 
 	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	
 		<tr>
		 <td  align="right">
 		 <% if (((String)request.getAttribute("pararBotao")).compareTo("S")==0){%>
				  <button type="button" NAME="parar"  style="height: 28px; width: 70px;border: none; background: transparent;" onClick="javascript: valida('parar',this.form);">
					<IMG src="<%= path %>/images/bot_parar_det1.gif" width="57" height="26" align="left"  >
		          </button>	
		 <%}%>
        <% if (((String)request.getAttribute("liberarBotao")).compareTo("S")==0){%>
				  <button type="button" NAME="liberar"  style="height: 28px; width: 70px;border: none; background: transparent;" onClick="javascript: valida('liberar',this.form);">
					<IMG src="<%= path %>/images/bot_liberar_det1.gif" width="64" height="26" align="left"  >
		          </button>		
		 <%}%>
		</td>	
		<td width="20%"> </td>
			<td > 
				  <button type=button NAME="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
					  <img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >
				  </button>
			</td>
		</tr>
 	</table>
 </div>     
 
 <% } %>
 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--FIM_CORPO_sistema-->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= paraRobot.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "190 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  

</form>
</BODY>
</HTML>
