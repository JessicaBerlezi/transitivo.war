<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="REG.ArquivoRecebidoBean"%>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<!-- Chama o Objeto de Arquivos Recebido -->
<jsp:useBean id="ArquivoId" scope="request" class="REG.ArquivoRecebidoBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Carrega e monta as combos-->

<%String identArquivos[][] = (String[][])request.getAttribute("identArquivos");%>
myIdent = new Array ();
<%for (int i=0;i<identArquivos.length;i++) {%> 		
	myIdent[<%=i%>]= new Array("<%=identArquivos[i][0]%>","<%=identArquivos[i][1]%>");
<%}%>

myOrg = new Array(<%= OrgaoId.getOrgaos(UsuarioBeanId)%>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm,codArquivo) {
 switch (opcao) {
   case 'selecaoOrgao':
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  break ; 
   case 'selecaoArquivo':
		fForm.acao.value=opcao;
		fForm.codArquivo.value=codArquivo
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();
	  break ;
   case 'R':
		close();
	  break ; 
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide';
	      else document.all["MsgErro"].style.visibility='hidden';
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show';
	      else document.all["MsgErro"].style.visibility='visible';
	  break;	  
  }
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="notifPendencia" method="post" action="" >
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="codArquivo" type="hidden" value=''>

<!-- A div � utilizado para definir o posicionamento da combol de �rg�o -->			
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 100px; left:50px;" >
  <table width="100%" border="0" cellpadding="0" cellspacing="1" >    
      <tr>
	  	  <td width="85"></td>
		  <td width="480"><strong>&nbsp;&Oacute;rg&atilde;o:&nbsp;</strong>
		  					
				<select name="codOrgao">
				<script>
				<!-- Loop utilizado para apresentar todos os �rg�os -->
				for (var j=0;j<cod_orgao.length;j++) {

					<!-- Condicional usada para tornar como selecionado o �rg�o de atua��o do usu�rio logado no SMIT. -->
					if(cod_orgao[j]=="<%=request.getAttribute("codOrgao")%>"){
						document.write ("<option selected value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
					else{
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				}
				</script>
				</select>
			</td>
			<td width="27%" align="left"><b>Tipo de Arquivo:&nbsp;</b>							
				<select name="codIdentArquivo"> 
					<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						var sel="";
						for (j=0;j<myIdent.length;j++) {
							if (myIdent[j][0]=="<%=request.getAttribute("codIdentArquivo")%>")
								sel=" selected";
							else
								sel="";
							document.write ( "<option value=" + myIdent[j][0]+  sel + ">" + myIdent[j][1]) + "</option>"
						}
					</script>
				</select>				
				
			</td>
			<td align=left width=350>&nbsp;		
        		<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('selecaoOrgao',this.form,'');">	
	        	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																			
			</td>					
	  </tr>
  </table>
</div>

<div style="position:absolute; left:50px; top:138px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="1" class="table">
      <tr bgcolor="a8b980"> 
		<td width="30" height="20" align="center">&nbsp;</td>            
        <td width="279" align="center"><strong>
			<font color="#FFFFFF">Arquivo</font></strong></td>
		<td width="135"            align="center"><strong>
			<font color="#FFFFFF">Total de Linhas</font></strong></td>
        <td width="135"             align="center"><strong>
			<font color="#FFFFFF">Linhas Enviadas</font></strong></td>
        <td align="center"><strong>
			<font color="#FFFFFF">Dat. Recebimento</font></strong></td>
      </tr>
	</table>
</div>
  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 160px; height: 150px; visibility: visible;" > 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">      

<%int a = 0;
  List arquivos = (List) request.getAttribute("arquivos");
  if ((arquivos != null) && (arquivos.size() == 0)) {%>
	 <tr>
	    <td height="70"></td>
	 	<td align="center"><strong><font color="#000000" size="-1">�rg�o n�o possui arquivo sendo enviado</font></strong></td>
	 </tr>
<%}
  else if (arquivos != null) {
  	for(int i=0;i<arquivos.size();i++){
		ArquivoRecebidoBean arquivo = (ArquivoRecebidoBean)arquivos.get(i);
		String cor   = (a%2==0) ? "#eff5e2" : "#ffffff";
%>
		<tr bgcolor='<%=cor%>'>
	        <td width="30" height="20" align="center">
        		<button type="button" NAME="Ok" style="border: none; background: transparent;" onClick="javascript: valida('selecaoArquivo',this.form,'<%=arquivo.getCodArquivo()%>');">
	        	<img src="<%= path %>/images/REG/bot_check_ico.gif" align="left" ></button>
			</td>		
	        <td width="279" align="center"><%=arquivo.getNomArquivo()%> </td>
			<td width="135" align="center"><%=arquivo.getQtdReg()%>  </td>
        	<td width="135" align="center"><%=arquivo.getQtdEnviada()%> </td>
	        <td align="center"><%=arquivo.getDatRecebimento()%> </td>	
		</tr>
<% 		a++;
	}
}%>  
    </table>      
  </div>
  
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=ArquivoId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>