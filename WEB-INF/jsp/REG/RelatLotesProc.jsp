<!--M�dulo : GER
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />  
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="RelatLotesProcBean" scope="session"
	class="REG.RelatLotesProcBean" />
<jsp:useBean id="OrgaoBeanId" scope="session" class="ACSS.OrgaoBean" />
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
myOrg = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i< myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}
function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	if (document.all["codOrgao"].selectedIndex<=0  )
	{  valid = false
	   sErro += "Selecione um �rg�o \n"
	      
	}	
	
    if (comparaDatas(fForm.datInicio.value,fForm.datFim.value)==false){
       valid = false
       sErro += "Data final n�o pode ser inferior a data inicial. \n"
	   document.all.datFim.focus();       
    }
		     
	if (valid==false) alert(sErro) 
	return valid ;
}

function valida(opcao,fForm) {  
	 switch (opcao) {  
	   case 'R':
	    	close() ;
			break;	 
	   case  'geraRelatorio':   
	       if(veCampos(fForm)){       
			fForm.acao.value=opcao;
		   	fForm.target= "_blank";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		   	}
		  break;
	   case 'N':  // Esconder os erro
			fForm.acao.value="Novo";
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	 
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;
  }
}


function comparaDatas(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores
// Exemplo: Data de Definir Junta/relator n�o pode ser menor que a data do recurso

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=''>
       
<%
String sOrgao=request.getParameter("codOrgao");
%>
<div id="dadosEntrada" style="position:absolute; width:490px; overflow: visible; z-index: 35; top: 108px; left: 108px;" > 
    <table width="650" border="0" cellpadding="0" cellspacing="0">	  	  	 
	  	<tr> 
    	  <td width="16%" height="30" align="right"><strong><b>&Oacute;rg&atilde;o:</strong>&nbsp;</td>
		  <td colspan="5">&nbsp;&nbsp;&nbsp;
   	          <select name="codOrgao" > 
				   <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				       <script>

					      for (j=0;j<cod_orgao.length;j++) {

				document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"

					      }
				       </script>
			 </select>				
	      </td>
	</tr>   
    <tr><td height="20" colspan="5"></td></tr>
	<tr>		
		<td width="16%" align="right"  ><b>De<strong>:</strong></b></td>
		<td width="22%">&nbsp;&nbsp;&nbsp;
		  <input name="datInicio" type="text" size="12" maxlength="10" onKeyPress="javascript:Mascaras(this,'99/99/9999');"onChange="javascript:ValDt(this,0); "onFocus="javascript:this.select();" value="<%=RelatLotesProcBean.getDataIni()%>" >
		</td> 	
		<td colspan="2"  ><b>At&eacute;<strong>:</strong></b>&nbsp;&nbsp;&nbsp;
	    <input name="datFim" type="text" size="12" maxlength="10" onKeyPress="javascript:Mascaras(this,'99/99/9999');"onChange="javascript:ValDt(this,0); "onFocus="javascript:this.select();" value="<%=RelatLotesProcBean.getDataFim()%>" >		</td>
		<td width="35%" colspan="2" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('geraRelatorio',this.form)"> 	
					<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
	    </button>	    </td>
	  </tr>
	<tr><td height="20" colspan="5"></td></tr>

</table>
</div>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= OrgaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />  
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<script>
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%=sOrgao%>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.all["codOrgao"].length;i++){
    if(document.all["codOrgao"].options[i].value==njint){
       document.all["codOrgao"].selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>
