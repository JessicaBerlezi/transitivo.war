<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="REG.LinhaArquivoRec"%>
<%@ page import="REG.LinhaArquivoRec"%>
<%@ page import="java.text.SimpleDateFormat"%>

<!-- Chama o Objeto da Consulta --> 
<jsp:useBean id="arquivoRecId" scope="request" class="REG.ArquivoRecebidoBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html> 
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
var selecionados = 0

function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value="selecaoOrgao";
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
      case 'selecaoLinha':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
	   case 'cancelarLinha':
	   		if(fForm.numSelec.value == 0){
				alert("Nenhuma linha selecionada!")
			}
	   		else if(confirm("Deseja cancelar a(s) linha(s)")){
	   			fForm.acao.value=opcao;
	   			fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();  	
			}	
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function selecaoLinha(opcao,posicao,fForm){
	fForm.posicaoLinha.value=posicao;
	valida(opcao,fForm)
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}
 

function changeColorOver(obj) {
	obj.style.background="#EDF4DF";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#ffffff";
	obj.style.textDecoration="none";
}

function SomaSelec(posicao) {
	if(document.all["linhaForm"].linhas.length > 1){
		if(document.all["linhaForm"].linhas[posicao].checked){
			selecionados++
		}
		else{
			selecionados--
		}
	}
	else{
		if(document.all["linhaForm"].linhas.checked){
			selecionados++
		}
		else{
			selecionados--
		}
	}
	document.all["numSelec"].value = selecionados;
}

function marcaTodos(obj) {
	n=0
	for (i=0; i<document.all["linhaForm"].linhas.length;i++) {
		document.all["linhaForm"].linhas[i].checked = obj.checked ;
		if (document.all["linhaForm"].linhas[i].checked) n++;
	}
	document.all["numSelec"].value = n;
}

</script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
 
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="linhaForm" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input name="posicaoLinha"     type="hidden" value=' '>
<input name="codArquivo" type="hidden" value='<%=arquivoRecId.getCodArquivo()%>'>
<input name="codOrgao" type="hidden" value='<%=(String)request.getAttribute("codOrgao")%>'>
<input name="nomArquivo" type="hidden" value='<%=(String)request.getAttribute("nomArquivo")%>'>

<%if(request.getParameter("j_sigFuncao").equals("REG0510")) {%>
<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
<!-- Loop utilizado para montar campos hidden com todos os �rg�os -->
<%String[] todosOrgaosCod = (String[]) request.getAttribute("todosOrgaosCod");
  for(int i = 0; i < todosOrgaosCod.length; i++) {%>
	<input name='todosOrgaosCod' type='hidden' value='<%=todosOrgaosCod[i]%>'>
<%}
}%>

<!--IN�CIO_CORPO_sistema-->

<!--FILTROS--> 
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 45px; z-index:1;" > 

<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="30%" align="left"><b>Arquivo:&nbsp;</b>
				<%=arquivoRecId.getNomArquivo()%>&nbsp;
			  </td> 		  		  
	          <td align="right">
					<button type="button" NAME="cancelar"   style="width: 55px;height: 21px;  border: none; background: transparent; cursor: hand;"  onClick="javascript:valida('cancelarLinha',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_cancelar_txt.gif" width="52" height="19" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
	<%	List linhas = arquivoRecId.getLinhaArquivoRec();
    	LinhaArquivoRec linha = null; 	
	%>
  <div id="div1" style="position:absolute; left:40px; top:100px; width:780px; height:50px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="740" border="0" cellpadding="1" cellspacing="1" class="table">
	<tr>
		<td colspan = "8">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr bgcolor="#FFFFFF"> 
				<% if (linhas.size()>1) { %>   
        			<td width="70" height="14">    
	         			<input type="checkbox" name="todos" value="0" class="input" onClick="javascript: marcaTodos(this);" ><b>TODOS</b>			
        			</td>
				<% } %>
        			<td>
						<font color="#000000">&nbsp;&nbsp;Localizados&nbsp;<%= linhas.size() %>&nbsp;Processo(s)</font>
					</td>
        			<td align=right>
						<font color="#000000">&nbsp;&nbsp;Selecionados&nbsp;
						<input readonly type=text name=numSelec value="0" size=4>&nbsp;Processo(s)&nbsp;
						</font>
					</td>
      			</tr>
      		</table>
		</td>
	</tr>
	<tr bgcolor="#deebc2"> 
		<%	if(request.getParameter("j_sigFuncao").equals("REG0510")){%>
        <td width="35" height="15"  align="center" bgcolor="#deebc2"><strong>Seq</strong></td>
		<td width="70" bgcolor="#EDF4DF" align="center"><strong>&Oacute;rg&atilde;o</strong></td>
        <td width="90" bgcolor="#EDF4DF" align="center"><strong>Auto</strong></td>
        <td width="69" align="center" bgcolor="#deebc2"><strong>Placa</strong></td>
        <td width="76" align="center" bgcolor="#EDF4DF"><strong>Notifica&ccedil;&atilde;o</strong></td>
        <td width="99" bgcolor="#deebc2" align="center"><strong>Data Infra&ccedil;&atilde;o</strong></td>
        <td width="80" bgcolor="#EDF4DF" align="center"><strong>Hora Infra&ccedil;&atilde;o</strong></td>
        <td width="130" bgcolor="#deebc2" align="center"><strong>Munic&iacute;pio</strong></td>
        <td align="center" bgcolor="#EDF4DF"><strong>C&oacute;d. Infra&ccedil;&atilde;o</strong></td>
      	<%	}else{%>
        <td width="60" height="15"  align="center" bgcolor="#deebc2"><strong>Seq</strong></td>
        <td width="130" bgcolor="#EDF4DF" align="center"><strong>Notifica&ccedil;&atilde;o</strong></td>
        <td width="150" align="center" bgcolor="#deebc2"><strong>Data de Recebimento</strong></td>
        <td width="120" align="center" bgcolor="#EDF4DF"><strong>C&oacute;digo da Baixa</strong></td>
        <td width="120" bgcolor="#deebc2" align="center"><strong>C&oacute;digo da Caixa</strong></td>
        <td bgcolor="#EDF4DF" align="center"><strong>C&oacute;digo da Lote</strong></td>
		<%	}%>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:40px; top:153px; width:740px; height:207px; z-index:5; overflow: auto; visibility: visible;"> 
      <table width="720" border="0" cellpadding="0" cellspacing="0">
         
	<%	if( linhas.size() == 0){	%>
	 <tr>
	    <td height="70"></td>
	 	<td align="center"><strong><font color="#000000" size="-1">Arquivo n&atilde;o possui linha com erro</font></strong></td>
	 </tr>
	 <%
      	}
		else{
			SimpleDateFormat sdfOld = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat sdfNew = new SimpleDateFormat("dd/MM/yyyy");	
			for(int i=0;i<linhas.size();i++) {      		
      			linha =(LinhaArquivoRec)linhas.get(i);      
      			ACSS.OrgaoBean orgaoBean = new ACSS.OrgaoBean();
				orgaoBean.Le_Orgao(linha.getCodOrgao(),0);
      	%>
       	<tr name="line" style="cursor:hand" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);">       
	       	<td>
		       	<table width="720" border="0" cellpadding="1" cellspacing="1"> 
		       	<tr>
		       		<%	if(request.getParameter("j_sigFuncao").equals("REG0510")){%>
			        <td width="35"  height="15" align="center"><%=i+1%><input name="linhas" type="checkbox" value="<%=linha.getCodLinhaArqReq()%>" style="border:0 " onClick="javascript:SomaSelec(<%=i%>)"></td>
				  <td width="70"  align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=orgaoBean.getSigOrgao()%></td>
			        <td width="90"  align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(280,292).trim()%></td>
			        <td width="69"  align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(110,117)%></td>
			        <td width="76"  align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(6,15)%></td>
			        <td width="99"  align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%= sys.Util.formataDataDDMMYYYY(linha.getDscLinhaArqRec().substring(162,170))%></td>
		          <td width="90" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(170,175)%></td>
			        <td width="130" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(175,214)%></td>
			        <td align="center"><%=linha.getDscLinhaArqRec().substring(214,219)%></td>
			    	<%	}else{%>
			        <td width="60"  height="15" align="center"><%=i+1%><input name="linhas" type="checkbox" value="<%=linha.getCodLinhaArqReq()%>" style="border:0 " onClick="javascript:SomaSelec(<%=i%>)"></td>
			        <td width="130" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getNumNotificacao()%></td>
			        <td width="150" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=sdfNew.format(sdfOld.parse(linha.getDscLinhaArqRec().substring(37,45)))%></td>
			        <td width="120" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(45,47)%></td>
			        <td width="120" align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(47,51)%></td>
			        <td align="center" onclick="selecaoLinha('selecaoLinha',<%=i%>,linhaForm);"><%=linha.getDscLinhaArqRec().substring(51,55)%></td>					
					<%}%>
			    </tr>
		      	<tr>      
		      		<td colspan="8" style="padding-left:15px";>Erro:&nbsp;&nbsp;<%=linha.getNomStatusErro(request.getParameter("j_sigFuncao"))%></td>
		      	</tr>
		      	</table>
			</td>
		</tr>
      	<tr>
        	<td height="1" bgcolor="#cccccc"></td>
      	</tr>
      
      <%	}
	  			} //loop
     %>
    </table>
  </div>
<!--FIM conte�do da tabela-->

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=arquivoRecId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>