<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat"%>

<jsp:useBean id="RelArqDownlBeanId"  scope="request" class="REG.RelatArqNotificacaoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0,npag = 0,seq = 0;	
	String valorFormatado1="",valorFormatado2="",valorFormatado3="",valorFormatado4="";
	String valor1="",valor2="",valor3="",valor4="";
	
	Iterator it = RelArqDownlBeanId.getListaArqs().iterator();
	REG.RelatArqNotificacaoBean relatorioImp  = new REG.RelatArqNotificacaoBean();					
	if( RelArqDownlBeanId.getListaArqs().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.RelatArqNotificacaoBean)it.next() ;
			   seq++; contLinha++;
			   if (contLinha%54==1){
					npag++;
					if (npag!=1){
%>
						<jsp:include page="Rod_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
				<% } %>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
			<tr > 	
				 <td width="4%"  align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="28%" align="center" bgcolor="#999999"><font color="#000000"><b>ARQUIVO</b></font></td>
				 <td width="9%"  align="center" bgcolor="#999999"><font color="#000000"><b>DATA ENVIO</b></font></td>		 
				 <td width="6%"  align="center" bgcolor="#999999"><font color="#000000"><b>QTD ENV.</b></font></td>					 
				 <td width="13%" align="center" bgcolor="#999999"><font color="#000000"><b>USU&Aacute;RIO ENVIO</b></font></td>
				 <td width="9%"  align="center" bgcolor="#999999"><font color="#000000"><b>DATA DOWNLOAD</b></font></td>
				 <td width="13%" align="center" bgcolor="#999999"><font color="#000000"><b>USU&Aacute;RIO DOWNLOAD</b></font></td>
				 <td width="6%"  align="center" bgcolor="#999999"><font color="#000000"><b>QTD PEND.</b></font></td>
				 <td width="6%"  align="center" bgcolor="#999999"><font color="#000000"><b>QTD CANC.</b></font></td>
				 <td width="6%"  align="center" bgcolor="#999999"><font color="#000000"><b>QTD RECEB.</b></font></td>
			 </tr>
		  </table>     
         <% } %>
		<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" align="center">							
			<tr> 
				<td width="4%"  align="center" style="line-height:20px;"><%= seq %></TD>		 		 	  
				<td width="28%" align="left"   style="line-height:20px;"><%= relatorioImp.getNomArquivo()%>&nbsp;(<%= relatorioImp.getCodStatus()%>)</td>
				<td width="9%"  align="center" style="line-height:20px;"><b><%= relatorioImp.getDatRecebimento()%></b></TD>	

				<%
					try {  
					 	   valor1 = relatorioImp.getQtdReg();
					 	   if( (valor1 != null)||(valor1.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado1 = formato.format(Integer.parseInt(valor1)); 
						   }else valorFormatado1 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%"   align="right"  style="line-height:20px;"><%=valorFormatado1%></TD>					
				<td width="13%"  align="center" style="line-height:20px;"><%= relatorioImp.getTarNomUsername()%>/<%= relatorioImp.getTarCodOrgaoLotacao()%></td>
				<td width="9%"   align="center" style="line-height:20px;"><%= relatorioImp.getDatDownload() %></TD>	
				<td width="13%"  align="center" style="line-height:20px;"><%= relatorioImp.getNomUsername()%>/<%= relatorioImp.getCodOrgaoLotacao()%></td>
				<%
					try {  
					 	   valor3 = relatorioImp.getQtdLinhaPendente();
					 	   if( (valor3 != null)||(valor3.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado3 = formato.format(Integer.parseInt(valor3)); 
						   }else valorFormatado3 = "0";
					}
					catch(Exception e){};
				%>				
				<td width="6%"  align="right" style="line-height:20px;"><%= valorFormatado3 %></td>
				<%
					try {  
					 	   valor4 = relatorioImp.getQtdLinhaCancelada();
					 	   if( (valor4 != null)||(valor4.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado4 = formato.format(Integer.parseInt(valor4)); 
						   }else valorFormatado4 = "0";
					}
					catch(Exception e){};
				%>				
				<td width="6%"  align="right" style="line-height:20px;"><%= valorFormatado4%>&nbsp;&nbsp;</td>				
				<%
					try {  
					 	   valor2 = relatorioImp.getQtdRecebido();
					 	   if( (valor2 != null)||(valor2.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado2 = formato.format(Integer.parseInt(valor2)); 
						   }else valorFormatado2 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%" align="right" style="line-height:20px;"><%= valorFormatado2 %></td>
			</tr>
			
			<tr><td height=1 bgcolor="#000000" colspan=10></td></tr>
	   <% } %>
	  </table>      
<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%}
	if (RelArqDownlBeanId.getListaArqs().size()>0) { 
		if (contLinha<54){}
%>
		<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" >							
			<tr> 
				<td width="4%"  align="center"  style="line-height:40px;font-weight: bold" >Total:</TD>		 		 	  
				<td width="28%" align="center"></td>
				<td width="9%"  align="center"></td>
				<%
					try {  
					 	   valor1 = RelArqDownlBeanId.getTotalQtdReg();
					 	   if( (valor1 != null)||(valor1.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado1  = formato.format(Integer.parseInt(valor1)); 
						   }else valorFormatado1 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%"  align="right" style="font-weight: bold"><%=valorFormatado1%></TD>	
				<td width="13%" align="center"></td>
				<td width="9%"  align="center"></td>
				<td width="13%" align="center"></td>					
				<%
					try {  
					 	   valor2 = RelArqDownlBeanId.getTotalQtdLinhaPendente();
					 	   if( (valor2 != null)||(valor2.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado2  = formato.format(Integer.parseInt(valor2)); 
						   }else valorFormatado2 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%" align="right" style="font-weight: bold"><%=valorFormatado2%></td>
				<%
					try {  
					 	   valor4 = RelArqDownlBeanId.getTotalQtdLinhaCancelada();
					 	   if( (valor4 != null)||(valor4.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado4  = formato.format(Integer.parseInt(valor4)); 
						   }else valorFormatado4 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%" align="right" style="font-weight: bold"><%=valorFormatado4%>&nbsp;&nbsp;</td>				
				<%
					try {  
					 	   valor3 = RelArqDownlBeanId.getTotalQtdRecebido();
					 	   if( (valor3 != null)||(valor3.equals("")) )  {
						   		DecimalFormat formato = new DecimalFormat(",##0") ;
						   		valorFormatado3  = formato.format(Integer.parseInt(valor3)); 
						   }else valorFormatado3 = "0";
					}
					catch(Exception e){};
				%>
				<td width="6%" align="right" style="font-weight: bold"><%=valorFormatado3%></td>
			</tr>
  </table>


		<jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
<table>
<tr>
<td>P - Processado /</td>
<td>R - Recebido /</td>
<td>E - Em Processamento</td>
</tr>
</table>
</form>
</body>
</html>