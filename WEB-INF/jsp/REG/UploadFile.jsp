<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="ArquivoRecebidoBeanId" scope="request" class="REG.ArquivoRecebidoBean" /> 

<html>
<head>
<title>DETRAN � Sistema de Monitoramento de Multas de Tr�nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="UsrForm" method="post" action="">
<!-- Cabe�alho -->
<jsp:include page="Cab.jsp" flush="true" />
<!-- Fim Cabe�alho -->
<!--Corpo do sistema-->
<!-- Resultado do Upload do Arquivo -->	
!--Corpo do sistema-->
<div id="UploadDiv" style="position:absolute; left:86px; top:74px; width:687px; height:160px; z-index:3; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;"> 

  <table width="100%" border="0" cellspacing="1" cellpadding="3">
    <tr> 
      <td width="22%"> <div align="left" class="espaco"><strong>ENVIO DE LOTE</strong></div></td>
    </tr>
  </table>
  
  <table width="100%" border="0" cellspacing="1" cellpadding="2" class="table">
    <tr bgcolor="#EFF5E2"> 
      <td width="29%" bgcolor="#EFF5E2">
	  <div align="center" >ARQUIVO:&nbsp;&nbsp;&nbsp;<%=ArquivoRecebidoBeanId.getNomArquivo()%></div> 
	  </td>
    </tr>
  </table>
  
  <table width="687" border="0" cellpadding="2" cellspacing="2" class="table">
    <tr bgcolor="#DEEBC2"> 
	  <td width="80"> <div align="left"><font color="#666666"><strong>LINHA</strong></font></div></td>
      <td > <div align="left"><font color="#666666"><strong>MENSAGEM</strong></font></div></td>
     </tr>
  </table>
  
</div>

<!--Inicio da Table do detalhe das informa��es retornadas do banco-->
<div id="ResultDiv" style="position:absolute; left:86px; top:140px; width:687px; height:160px; z-index:3; background-image: url(images/detran_bg4.gif); overflow: auto; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="2"  class="table3">
    <tr bgcolor="#DEEBC2"> 
      
<%
	if (ArquivoRecebidoBeanId.getErroArquivo().size() > 0) {
		for (int t=0; t<ArquivoRecebidoBeanId.getErroArquivo().size();t++) {
			out.println("<tr bgcolor=\"#EFF5E2\"><td width=\"80\" height=\"23\">"); 
			out.println(((String) ArquivoRecebidoBeanId.getErroArquivo().elementAt(t)).substring(0,5) + "</td>");
			out.println("<td height=\"23\">");			
			out.println(((String) ArquivoRecebidoBeanId.getErroArquivo().elementAt(t)).substring(10) + "</td>");
//			out.println((String) ArquivoRecebidoBeanId.getErroArquivo().elementAt(t) + "</td>");
			out.println("</tr>");
	  	}  
	}
	else {
			out.println("<tr bgcolor=\"#EFF5E2\"><td width=\"80\" height=\"23\">"); 
			out.println("  " + "</td>");
			out.println("<td height=\"23\">");			
			out.println("Arquivo recebido com sucesso!</td>");
			out.println("</tr>");
	}
%>
    </tr>
  </table>
 
</div>

<div id="botoesconsulta" style="position: absolute; left: 86px; top: 330px; overflow: visible; width:687px;"> 
  <table border="0" align="center" cellpadding="0" cellspacing="0" class="BordaTopo">
    <tr>
      <td align="right"><img src="images/bot_ant.gif" width="20" height="19"></td>
      <td align="right"><img src="images/inv.gif" width="10" height="10"></td>
      <td align="right"><img src="images/bot_prox.gif" width="20" height="19"></td>
      <td align="center"><img src="images/inv.gif" width="10" height="10"></td>
      <td align="left"><button style="border: 0px; background-color: transparent; height: 19px; width: 52px; cursor: hand;" onClick="javascript: window.close();"><img src="images/retornar.gif" width="52" height="19" ></button></td>
    </tr>
  </table>
</div>
<!--Fim Envio Lote-->

<!--Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!--FIM Rodap�-->
</form>		
</body>

</html>


