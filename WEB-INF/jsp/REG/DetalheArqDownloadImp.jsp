<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="REG.LinhaArquivoRec"%>

<jsp:useBean id="LinhaArquivoRecId" scope="request" class="REG.LinhaArquivoRec" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
	DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->
<%
  	int contLinha=0,npag = 0,seq = 0;
	
	Iterator it = LinhaArquivoRecId.getListaArquivos().iterator();
	LinhaArquivoRec listaArquivo = new LinhaArquivoRec();
	
	if (LinhaArquivoRecId.getListaArquivos().size() > 0) {				
		while (it.hasNext()) {
			listaArquivo =(LinhaArquivoRec)it.next();
			   seq++; contLinha++;
			   if (contLinha%54==1){
					npag++;
					if (npag!=1){
%>
						<jsp:include page="Rod_impConsulta.jsp" flush="true" />
						<div class="quebrapagina"></div>
				<% } %>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
			<tr>
				 <td width="4%"  align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="9%" align="center" bgcolor="#999999"><font color="#000000"><b>AUTO</b></font></td>		 
				 <td width="11%" align="center" bgcolor="#999999"><font color="#000000"><b>PLACA</b></font></td>					 
				 <td width="15%" align="center" bgcolor="#999999"><font color="#000000"><b>DATA INFRA&Ccedil;&Atilde;O</b></font></td>
				 <td width=15%" align="center" bgcolor="#999999"><font color="#000000"><b>HORA INFRA&Ccedil;&Atilde;O</b></font></td>
				 <td width="15%" align="center" bgcolor="#999999"><font color="#000000"><b>MUNIC&Iacute;PIO</b></font></td>
				 <td align="center" bgcolor="#999999"><font color="#000000"><b>C&Oacute;D. INFRA&Ccedil;&Atilde;O</b></font></td>
			</tr>
		</table>
		 <% } %>
	<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" align="center">
		<tr> 
			<td width="4%"  align="center" style="line-height:20px;"><%= seq %></TD>		 		 	  
			<td width="9%" align="left"   style="line-height:20px;"><b><%= listaArquivo.getNumAutoInfracao()%></b></td>
			<td width="11%" align="center"   style="line-height:20px;"><%= listaArquivo.getNumPlaca()%></td>
			<% if (listaArquivo.getDataInfracao() == null) {%>
				<td width="15%"  align="center" style="line-height:20px;">-</TD>
		<%			}else{%>
			    <td width="15%"  align="center" style="line-height:20px;"><%=listaArquivo.getDataInfracao()%></TD>
		<%			}%>
		
			<%	if(listaArquivo.getDataInfracao() == null){%>
				<td width="15%"  align="center" style="line-height:20px;">-</TD>
		<%			}else{%>
			    <td width="15%"  align="center" style="line-height:20px;"><%=listaArquivo.getHoraInfracao()%></TD>
		<%			}%>	 		 	  
			<td width="15%" align="center"   style="line-height:20px;"><%= listaArquivo.getCodMunicipio()%></td>
			<td align="center" style="line-height:20px;"><%= listaArquivo.getCodInfracao()%></TD>	
		</tr>			
		<tr><td height=1 bgcolor="#000000" colspan=10></td></tr>
		<%	} %>
	</table>

<%} else { 
	String msg=(String)request.getAttribute("semLog"); if (msg==null) msg="Nenhum registro existente para o arquivo";
%>

<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
<%	if (LinhaArquivoRecId.getListaArquivos().size() > 0) { 
		if (contLinha<54){

		} %>
		<jsp:include page="Rod_impConsulta.jsp" flush="true" />
<%} %>
</form>
</body>
</html>