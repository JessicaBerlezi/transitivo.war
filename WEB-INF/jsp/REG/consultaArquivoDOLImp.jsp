<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.*" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<style type="text/css">
.vermelho 
{color: #ff0000;}
</style>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

<%
 	Vector vetArquivos = (Vector) ConsultaArquivoDOLBeanId.getVetArquivos(); 
%>

myIdent = new Array ();
<% 	
 	for (int i=0;i<vetArquivos.size();i++) {
 		%> 		
 		myIdent[<%=i%>]= new Array("<%=vetArquivos.get(i)%>","<%=vetArquivos.get(i)%>");
 		
 	<%}
%>

function changeColorOver(obj) {	
	obj.style.background="#EDF4DF";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#ffffff";
	obj.style.textDecoration="none";
}

function callMostraLinha(opcao,qtd,nomArquivo, datRecebimento, statusArquivo, codLote,fForm) {
	if (qtd==0) {
		alert("N�o existem registros para o item selecionado.");
		return false;
	}
	fForm=document.frmDownload;	
	fForm.codLote.value=codLote;
	fForm.nomArquivoSel.value=nomArquivo;
	fForm.datRecebimento.value=datRecebimento;
	fForm.statusArquivo.value=statusArquivo;
	valida(opcao,fForm);
}

</script>
</head>
 
<%
 	Vector vetConsulta = (Vector) ConsultaArquivoDOLBeanId.getVetConsulta(); 
%>


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="frmDownload" method="post" action="">
<!--IN�CIO_CORPO_sistema-->

<!-- Inicio Div Detalhes vari�veis impressao -->	 
         
     <%
	    int contLinha=0;		
		int npag = 0;%>    	
    	<%int tam=vetConsulta.size();
    	String nomArquivo, nomArquivoAnt, qtdLote, status, datRecebimento, nomArquivoPrint;
    	String corArq,datProcessamento;
    	
    	int qtdReg, qtdLinhaErro,qtdCanc,qtdGrav;
    	int linha=0;
    	
      	for(int i=0;i<tam;i++) {
      		REG.ArquivoDolBean ArqDolBean = new REG.ArquivoDolBean();      		
      		      		
      		ArqDolBean=(REG.ArquivoDolBean)vetConsulta.get(i);
      		
      		Iterator itx  = ArqDolBean.getLote().iterator() ;
			REG.LoteDolBean myLote = new REG.LoteDolBean();
			
			int cont=0;
			while (itx.hasNext()) {
			cont++;
			
			contLinha++;
			//Faz quebra de linha a cada x=30 linhas, exceto na primeira linha.
			if (contLinha%30==1){
				npag++;			
				if (npag!=1){
				%>
					</table> 
					<jsp:include page="rodape_impconsulta.jsp" flush="true" />
					<div class="quebrapagina"></div>
				<% } %>				
	<jsp:include page="cabecalhoDOL.jsp" flush="true" >	
	<jsp:param name="nPag" value= "<%= npag %>" />				
	</jsp:include> 
<!--conte�do da tabela-->
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >     
 <% } 
			myLote =(REG.LoteDolBean)itx.next() ;	//Lote	
      		
      		qtdLote=ArqDolBean.getQtdLote();
      		
      		datRecebimento=ArqDolBean.getDatRecebimento();
      		
      		datProcessamento = ArqDolBean.getDatProcessamentoDetran();
      		     		
      		status=ArqDolBean.getCodStatus();
      		try {
      			qtdReg=Integer.parseInt(myLote.getQtdReg());
      		}
      		catch (Exception e) {
      			qtdReg=0;
      		}
      		try {
      			qtdCanc=Integer.parseInt(myLote.getQtdCanc());
      		}
      		catch (Exception e) {
      			qtdCanc=0;
      		}   
      		try {
      			qtdGrav=Integer.parseInt(myLote.getQtdGrav());
      		}
      		catch (Exception e) {
      			qtdGrav=0;
      		}      		
      		try {
      			qtdLinhaErro=Integer.parseInt(myLote.getQtdLinhaErro());
      		}
      		catch (Exception e) {
      			qtdLinhaErro=0;
      		}
      		
      		if (status.equals("9")) {
      			corArq="Rejeitado";
      		}
      		else if (status.equals("8") || status.equals("7") || status.equals("1")) {
      			corArq="Em processamento";
      		}
      		else if (status.equals("2")) {
      			corArq="Processado";
      		}
      		else {
      			corArq="Recebido";
      		}
      		
	      	nomArquivo=ArqDolBean.getNomArquivo();
      		
      		
      		
      		if (cont>=2) {
      		
      			nomArquivoPrint="";
      			datRecebimento="";
      			corArq="";
      		}
      		else {       		
      			nomArquivoPrint=nomArquivo + " (" + qtdLote + ")";
      		}
			linha++;
		
		//temp
	//	qtdLinhaErro=1;
		
		
      %>
       	<tr name="line<%=linha%>" id="line<%=linha%>">
        <td width="200" height="15"><%=nomArquivoPrint%><br><%=corArq%></td>
        <td width="100" align="center"><%=datRecebimento%></td>
        <td width="100" align="center"><%=datProcessamento%></td>
        <td width="85" height="15" align="center"><%=myLote.getNumLote()%></td>
        <td width="61" align="center"><%=qtdReg%></td>
        <td width="61" height="15" align="center"><%=qtdGrav%></td>
        <td width="61" align="center"> <%=qtdLinhaErro%></td>
        <td width="61" align="center"><%=qtdCanc%></td>
      </tr>
      <tr> 
        <td height="1" colspan="8"  bgcolor="#cccccc"></td>
      </tr>
      
      <%} //loop de lote
      
    } //loop de ArqDol%>
    </table>
  

</form>
</body>
</html>