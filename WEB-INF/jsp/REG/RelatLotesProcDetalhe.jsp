<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->
<%@ page session="true"%>
<%String path = request.getContextPath();

            %>
<%@ page errorPage="ErrorPage.jsp"%>
<%@ page import="java.util.*"%>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="RelatLotesProcBean" scope="session"
	class="REG.RelatLotesProcBean" />
<jsp:useBean id="OrgaoBeanId" scope="session" class="ACSS.OrgaoBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>DETRAN &#8226; Sistema de Monitoramento de
Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'Voltar':
            close() ;
			break;	 
      case 'imprimirRelDetalhe':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
 	  case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}



function callAcertaAuto(opcao,numAuto,fForm) {   
	fForm=document.frmDownload;	
	fForm.codLinhaLote.value=numAuto;
	valida(opcao,fForm);
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;">
<form name="frmDownload" method="post" action=""><jsp:include
	page="Cab.jsp" flush="true" /> <input name="acao" type="hidden"
	value=' '> 
<!--IN�CIO_CORPO_sistema-->

<!--FILTROS-->
<div id="WK_SISTEMA"
	style="position:absolute; width:568px; overflow: visible; z-index: 1; top: 75px; left: 180px; z-index:1; visibility: visible;">
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"
	align="center">
	<tr>
		<td width="9%" align="right"><b>Org�o:&nbsp;</b></td>
		<td colspan="4">&nbsp;&nbsp;&nbsp; <input disabled name="sigOrgao"
			type="text" size="15" maxlength="10"
			value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;"></td>
	</tr>
	<tr>
		<td colspan="4" height="10"></td>
	</tr>
	<tr>
		<td align="right"><b>De<strong>:</strong></b>&nbsp;</td>
		<td width="20%">&nbsp;&nbsp;&nbsp; <input disabled name="datInicio"
			type="text" size="12" maxlength="10" style="border: 0px none;"
			value="<%=RelatLotesProcBean.getDataIni()%>"></td>
		<td colspan="2"><b>At&eacute;<strong>:</strong></b>&nbsp;&nbsp;&nbsp;		  <input disabled name="datFim"
			type="text" size="12" maxlength="10" style="border: 0px none;"
			value="<%=RelatLotesProcBean.getDataFim()%>"></td>
	  </tr>
	<tr>
		<td colspan="4" height="10"></td>
	</tr>
    <tr>
		<td height="10" align="right"><b>Consulta:</b>&nbsp;</td>
         <td>&nbsp;&nbsp;&nbsp;
	    <input disabled name="tipoConsulta"
			type="text" size="15" maxlength="15" style="border: 0px none;"
			value="<%=RelatLotesProcBean.getTipoConsulta() %>">		</td>
		<td width="57%" align="right" >
		<button
			style="border: 0px; background-color: transparent; height: 21px; width: 28px; cursor: hand;"
			type="button"
			onClick="javascript: valida('imprimirRelDetalhe',this.form)"><img
			src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir"
			width="26" height="19"></button>
		</td>
        <td width="14%"></td>
	</tr>
</table>
</div>
<!-- FIM FILTROS--> <!--t�tulos das colunas da tabela-->
<div id="div1"
	style="position:absolute; left:40px; top:163px; width:740px; height:50px; z-index:5; overflow: auto; visibility: visible;">
<table width="720" border="0" cellpadding="1" cellspacing="1"
	class="table">
	<tr bgcolor="#deebc2">
		<td width="35" height="15" align="center" bgcolor="#deebc2"><strong>Seq</strong></td>
		<td width="150" bgcolor="#EDF4DF" align="center"><strong>Auto</strong></td>
		<td width="70" align="center" bgcolor="#deebc2"><strong>Placa</strong></td>
		<td width="90" bgcolor="#EDF4DF" align="center"><strong>Data Infra��o</strong></td>
		<td width="90" bgcolor="#deebc2" align="center"><strong>Hora Infra��o</strong></td>
		<td width="60" bgcolor="#EDF4DF" align="center"><strong>Munic�pio</strong></td>
		<td bgcolor="#deebc2" align="center"><strong>C�d. Infra��o</strong></td>
	</tr>
</table>
</div>
<!--FIM t�tulos das colunas da tabela--> <!--conte�do da tabela-->
<div id="div2"
	style="position:absolute; left:40px; top:183px; width:740px; height:194px; z-index:5; overflow: auto; visibility: visible;">
<table width="720" border="0" cellpadding="0" cellspacing="0">

	<% Iterator it = RelatLotesProcBean.getDadosLote().iterator();
   	   REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();
   	   int i =0;
   	   while (it.hasNext()){
	
                LinhaBean = (REG.LinhaLoteDolBean) it.next();

                %>
	<tr name="line<%=i%>" id="line<%=i%>" 
	>
		<td>
		<table width="720" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td width="35" height="15" align="center"><%=i + 1%></td>
				<td width="150"><%=LinhaBean.getNumAuto()%></td>
				<td width="70" align="center"><%=LinhaBean.getNumPlaca()%></td>
				<td width="90" align="center"><%=LinhaBean.getDataInfracao()%></td>
				<td width="90" align="center"><%=LinhaBean.getHoraInfracao()%></td>
				<td width="60" align="center"><%=LinhaBean.getCodMunicipio()%></td>
				<td align="center"><%=LinhaBean.getCodInfracao()%></td>
			</tr>
			<%if (RelatLotesProcBean.getTipoConsulta().equals("REJEITADOS")) {%>
			<tr>
				<td colspan="7" style="padding-left:15px";>Erro:&nbsp;<%=LinhaBean.getDscErro()%></td>
			</tr>
			<%}%>
		</table>
		</td>
	</tr>
	<tr>
		<td height="1" bgcolor="#cccccc"></td>
	</tr>

	<%    i++;
	   } //loop

            %>  
</table>
</div>
<!--FIM conte�do da tabela--> <!--FIM_CORPO_sistema--> <!--Bot�o retornar-->
 <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('Voltar',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar--> <!-- Rodap�--> <jsp:include page="Rod.jsp"
	flush="true" /> <!-- Fim Rodap� --> <!--Div Erros--> <jsp:include
	page="../sys/DivErro.jsp" flush="true">
	<jsp:param name="msgErro"
		value="<%=RelatLotesProcBean.getMsgErro() %>" />
	<jsp:param name="msgErroTop" value="160 px" />
	<jsp:param name="msgErroLeft" value="130 px" />
</jsp:include> <!--FIM_Div Erros--></form>
</body>
</html>
