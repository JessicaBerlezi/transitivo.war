<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->
<!-- Abre a Sessao -->
<%@ page session="true"%>
<%@ page import="java.util.*"%>
<%String path = request.getContextPath();

            %>
<%@ page errorPage="ErrorPage.jsp"%>
<jsp:useBean id="RelatLotesProcBean" scope="session"
	class="REG.RelatLotesProcBean" />
<jsp:useBean id="OrgaoBeanId" scope="session" class="ACSS.OrgaoBean" />

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT:: - Gerencial</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {

 switch (opcao) {

    case 'imprimirRelatorio':
		  fForm.acao.value=opcao;
	   	  fForm.target= "_blank";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    break ;
   case 'PROCESSADOS':
		  fForm.acao.value=opcao;
	   	  fForm.target= "_blank";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    break ;	    
	case 'GRAVADOS':
		  fForm.acao.value=opcao;
	   	  fForm.target= "_blank";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    break ;	
   case 'REJEITADOS':
		  fForm.acao.value=opcao;
	   	  fForm.target= "_blank";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    break ;	   
   case 'CANCELADOS':
		  fForm.acao.value=opcao;
	   	  fForm.target= "_blank";
	      fForm.action = "acessoTool";  
	      fForm.submit();	  		  
	    break ;	    	         
   case 'R':
        close() ;   
	    break;
   case 'O':
   		if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	    else                 document.all["MsgErro"].style.visibility='hidden' ; 
	    break;  
   case 'V':
   		if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	    else                 document.all["MsgErro"].style.visibility='visible' ; 
	    break;
  }
}
function changeColorOver(obj) {	
	obj.style.background="#deebc2";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#FFFFFF";
	obj.style.textDecoration="none";
}
function detalhe(opcao,data,fForm) {
  	fForm.acao.value=opcao;
	fForm.data.value=data;
  	fForm.target= "_blank";
  	fForm.action = "acessoTool";  
  	fForm.submit();	  		  
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" /> 
	<input name="acao" type="hidden" value=' '> 
	<input name="data" type="hidden" value=' '> 
	<%if (RelatLotesProcBean.getDados().size() > 0) {  %>
<div id="WK_SISTEMA"
	style="position:absolute; width:568px; overflow: visible; z-index: 1; top: 75px; left: 180px; z-index:1; visibility: visible;">
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"
	align="center">
	<tr>
		<td width="9%" align="right"><b>Org�o:&nbsp;</b></td>
		<td colspan="4">&nbsp;&nbsp;&nbsp; <input disabled name="sigOrgao"
			type="text" size="15" maxlength="10"
			value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;"></td>
	</tr>
	<tr>
		<td colspan="4" height="10"></td>
	</tr>
	<tr>
		<td align="right"><b>De<strong>:</strong></b>&nbsp;</td>
		<td width="20%">&nbsp;&nbsp;&nbsp; <input disabled name="datInicio"
			type="text" size="12" maxlength="10" style="border: 0px none;"
			value="<%=RelatLotesProcBean.getDataIni()%>"></td>
		<td width="7%" align="right"><b>At&eacute;<strong>:</strong></b></td>
		<td width="64%">&nbsp;&nbsp;&nbsp; <input disabled name="datFim"
			type="text" size="12" maxlength="10" style="border: 0px none;"
			value="<%=RelatLotesProcBean.getDataFim()%>"></td>
	</tr>
	<tr>
		<td colspan="4" height="10"></td>
	</tr>
	<tr align="right">
		<td colspan="4">
		<button
			style="border: 0px; background-color: transparent; height: 21px; width: 28px; cursor: hand;"
			type="button"
			onClick="javascript: valida('imprimirRelatorio',this.form)"><img
			src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir"
			width="26" height="19"></button>
		</td>
	</tr>
	<tr>
		<td colspan="4" height="10"></td>
	</tr>
</table>
</div>
<div
	style="position:absolute; left:50px; top:157px; width:720px; height:10px; z-index:1; overflow: visible; visibility: visible;"
	id="titulos">
<TABLE border="0" cellpadding="1" cellspacing="1" width="100%">
	<tr>
		<td width="80" align="center" bgcolor="#deebc2"><font color="#000000"><b>SEQ</b></font></TD>
		<td width="80" bgcolor="#EDF4DF"><font color="#000000">&nbsp;&nbsp;<b>DATA
		LOTE</b></font></TD>
		<td width="140" bgcolor="#deebc2"><font color="#000000">&nbsp;&nbsp;<b>PROCESSADOS</b></font></TD>
		<td width="140" bgcolor="#EDF4DF"><font color="#000000">&nbsp;&nbsp;<b>GRAVADOS</b></font></TD>
		<td width="140" bgcolor="#deebc2"><font color="#000000">&nbsp;&nbsp;<b>REJEITADOS</b></font></TD>
		<td bgcolor="#EDF4DF"><font color="#000000">&nbsp;&nbsp;<b>CANCELADOS</b></font></TD>
	</tr>
</table>
</div>

<div       
	style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 172px; left: 50px; visibility: visible; height: 151px;">
<table border="0" cellpadding="1" cellspacing="1" width="100%"
	align="center">
	<%Iterator it = RelatLotesProcBean.getDados().iterator();
                int seq = 0;
                while (it.hasNext()) {
                    // lista os dados		
                    seq++;
                    REG.RelatLotesProcBean dados = new REG.RelatLotesProcBean();
                    dados = (REG.RelatLotesProcBean) it.next();

                    %>
	<tr height="20">
		<td width="80" align="center" ><%=seq%></td>
		<td width="80" align="center" ><%=dados.getDataLote()%></td>
		<td width="140" align="right"  style="cursor:hand"
			onmouseover="javascript:changeColorOver(this);"
			onmouseout="javascript:changeColorOut(this);" 
			onClick="javascript: detalhe('PROCESSADOS','<%=dados.getDataLote()%>',document.UsrAtuForm)"
		><%=dados.getQtdReg()%>&nbsp;</td>
		<td width="140" align="right"  style="cursor:hand"
			onmouseover="javascript:changeColorOver(this);"
			onmouseout="javascript:changeColorOut(this);"
			onClick="javascript: detalhe('GRAVADOS','<%=dados.getDataLote()%>',document.UsrAtuForm)"
			><%=dados.getQtdGrav()%>&nbsp;</td>
		<td width="140" align="right"  style="cursor:hand"
			onmouseover="javascript:changeColorOver(this);"
			onmouseout="javascript:changeColorOut(this);"
			onClick="javascript: detalhe('REJEITADOS','<%=dados.getDataLote()%>',document.UsrAtuForm);"
			><%=dados.getQtdRej()%>&nbsp;</td>
		<td align="right" style="cursor:hand"
			onmouseover="javascript:changeColorOver(this);"
			onmouseout="javascript:changeColorOut(this);"
			onClick="javascript: detalhe('CANCELADOS','<%=dados.getDataLote()%>',document.UsrAtuForm);"
			><%=dados.getQtdCancel()%>&nbsp;</td>
	</tr>
	<tr>
		<td height="1"  colspan="6" bgcolor="#cccccc"></td>
	</tr>
	<%}%>
</table>
</div>
<%} else { %>
<div
	style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 171px; left: 50px; visibility: visible; height: 151px;">
<table border="0" cellpadding="1" cellspacing="1" width="100%"
	align="center">
	<tr>
		<td align="center"><b><%=RelatLotesProcBean.getMsgErro()%></b></td>
	</tr>
</table>
</div>

<%}%> 
<!-- Rodap�--> <jsp:include page="Retornar.jsp" flush="true" /> <jsp:include
	page="Rod.jsp" flush="true" /> <!-- Fim Rodap� -->
</form>
</BODY>
</HTML>

