<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaArquivoDOLBeanId" scope="request" class="REG.ConsultaArquivoDOLBean" /> 
<jsp:useBean id="ArquivoDolBeanId" scope="request" class="REG.ArquivoDolBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<head>
<jsp:include page="Css.jsp" flush="true" />

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
var bProcessando=false;
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
	    	fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   	
			break;
      case 'ImprimeDownload':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
      case 'acertarAuto':
      		
      		if(bProcessando)
	   		{
	   			alert("Opera��o em Processamento, por favor aguarde...");
	   		}
	   		else
	   		{
		   		bProcessando=true;     
      
				fForm.acao.value=opcao;
				fForm.target= "_self";
				fForm.action = "acessoTool";  
				fForm.submit();	 
			
			}  		
			break;
	 case 'atualizaMarca':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}


comboUF = new Array(<%= ConsultaArquivoDOLBeanId.getComboUF()%>);


</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">

<%
	REG.LinhaLoteDolBean LinhaBean = new REG.LinhaLoteDolBean();
   	LinhaBean=(REG.LinhaLoteDolBean)ConsultaArquivoDOLBeanId.getVetConsulta().get(0); 
   	LinhaBean.setInd_P59(ArquivoDolBeanId.getDscPortaria59());     		
%>

<form name="form" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input name="codLinhaLote" type="hidden" value='<%=LinhaBean.getCodLinhaLote()%>'>
<input name="codLote" type="hidden" value='<%=LinhaBean.getCodLote()%>'>
<input name="nomConsulta"       type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomConsulta()%>'>
<input name="nomArquivoSel"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomArquivo()%>'>
<input name="datRecebimento"    type="hidden" value='<%=ConsultaArquivoDOLBeanId.getDatRecebimento()%>'>
<input name="statusArquivo"     type="hidden" value='<%=ConsultaArquivoDOLBeanId.getNomStatus()%>'>
<!--IN�CIO_CORPO_sistema-->
  <div id="dadosAutoCondutor" style="position:absolute; left:50px; top:75px; width:720px; height:170px; z-index:1; overflow: visible; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#deebc2"> 
        <td colspan=10 class="espaco2">&nbsp;&nbsp;<strong>DADOS DO AUTO</strong></td>
      </tr>
      <tr> 
        <td height="3" colspan=10></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td height="16" align="right">&nbsp;<font color="#666666">&nbsp;<strong>N&ordm; 
          do Auto :</strong></font><strong>&nbsp;</strong></td>
        <td><input name="numAuto" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:Mascaras(this,'9 - 99999999')" value='<%=LinhaBean.getNumAuto()%>' size="18" ></td>
        <td align="right"><strong><font color="#666666">Placa :</font>&nbsp;</strong></td>
        <td><input name="numPlaca" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:Mascaras(this,'AAA9999')" value='<%=LinhaBean.getNumPlaca()%>' size="10" maxlength="8"></td>        
        
        <td align="right"><strong><font color="#666666">Marca/Modelo :</font>&nbsp;</strong></td>
        <td><input name="codMarca" type="text"  value='<%=(ConsultaArquivoDOLBeanId.isIndMarcaBroker()? ConsultaArquivoDOLBeanId.getCodMarcaBroker():LinhaBean.getCodMarca()) %>' size="10" maxlength=""></td>                
        <% if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("REJEITADOS")) {%>
             <% if (LinhaBean.getCodOrgao().equals("258650")){%>
                 <td >&nbsp;&nbsp;<button style="border: 0px; background-color: transparent; height: 21px; width: 22px; cursor: hand;" onClick="javascript:valida('atualizaMarca',this.form);"><img src="<%= path %>/images/REG/bot_atualizar_ico.gif" width="20" height="19" alt="Atualizar Marca/Modelo via Broker"></button></td>  		
             <% }else{%>
                 <td colspan="4">&nbsp;&nbsp;<button style="border: 0px; background-color: transparent; height: 21px; width: 22px; cursor: hand;" onClick="javascript:valida('atualizaMarca',this.form);"><img src="<%= path %>/images/REG/bot_atualizar_ico.gif" width="20" height="19" alt="Atualizar Marca/Modelo via Broker"></button></td>  		
             <% }%>
      	<%}%>
      	
<% if (LinhaBean.getCodOrgao().equals("258650")){%>
        <td align="right"><strong><font color="#666666">N� Nit :</font>&nbsp;</strong></td>
        <td colspan="3"><input name="identPRF" type="text"  value='<%=LinhaBean.getIdentPRFEdt()%>' size="15" maxlength="8"></td>        
<% }%>
      </tr>
      <tr> 
        <td height="2"></td>
      </tr>
      
<% if (LinhaBean.getCodOrgao().equals("258650")){%>
      <tr bgcolor="#EFF5E2"> 
        <td align="right"><strong>&nbsp;&nbsp;<font color="#666666">Local    :</font>&nbsp;</strong></td>
        <td height="16" colspan="9"><input name="localInfracao" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getLocalInfracao().trim()%>' size="50" maxlength="45">
                                    &nbsp;<strong><font color="#666666">Local Det.:</font>&nbsp;</strong>          
                                    <input name="localInfracaoDet" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getLocalInfrDet().trim() %>' size="50" maxlength="100"></td>
      </tr>
      <tr> 
        <td height="2"></td>   
      </tr>
<%}else{%>
      <tr bgcolor="#EFF5E2"> 
        <td align="right"><strong>&nbsp;&nbsp;<font color="#666666">Local    :</font>&nbsp;</strong></td>

        <%if (LinhaBean.getInd_P59().equals("P59")){%>
                <td height="16" colspan="9"><input name="localInfracao" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getLocalInfracao()%>' size="115" maxlength="80">
        <%}else{%>
                <td height="16" colspan="9"><input name="localInfracao" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getLocalInfracao()%>' size="115" maxlength="45">
        <%}%>

                                    <input name="localInfracaoDet" type="hidden"  value='<%=LinhaBean.getLocalInfrDet() %>' ></td>
      </tr>

<%}%>
      <tr> 
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td width="87" height="16" align="right">&nbsp;<strong><font color="#666666">&nbsp;Data 
          :</font>&nbsp;</strong></td>
        <td width="98"><input name="dataInfracao" type="text" onFocus="javascript:this.select();" onKeyPress="javascript:Mascaras(this,'99/99/9999')" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getDataInfracao()%>' size="12" maxlength="10"></td>
        <td width="54" align="right"><strong><font color="#666666">Hora :</font>&nbsp;</strong></td>
        <td width="73"><input name="horaInfracao" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:Mascaras(this,'99:99')" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getHoraInfracao()%>' size="10" maxlength="5"></td>
        <td width="86" align="right"><strong><font color="#666666">C&oacute;d. 
          Munic&iacute;pio :</font>&nbsp;</strong></td>
        <%if (LinhaBean.getInd_P59().equals("P59")){%>
               <td width="63"><input name="codMunicipio" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getCodMunicipio()%>' size="7" maxlength="5"></td>
        <%}else{%>
               <td width="63"><input name="codMunicipio" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getCodMunicipio()%>' size="7" maxlength="4"></td>
        <%}%>
        
        
        <td width="85" align="right"><strong><font color="#666666">C&oacute;d. 
          Infra&ccedil;&atilde;o :</font>&nbsp;</strong></td>
        <td colspan="3"> 
        <%if (LinhaBean.getInd_P59().equals("P59")){%>
            <input name="codInfracao" type="text" size="6" maxlength="5" value='<%=LinhaBean.getCodInfracao()%>' onkeypress="javascript:f_num();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        <%}else{%>
            <input name="codInfracao" type="text" size="5" maxlength="4" value='<%=LinhaBean.getCodInfracao()%>' onkeypress="javascript:f_num();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
         <%}%>
        </td>
      </tr>
      <tr> 
        <td height="2"></td>   
      </tr>
<% if (LinhaBean.getCodOrgao().equals("258650")){%>
      <tr bgcolor="#EFF5E2"> 
        <td align="right"><strong>&nbsp;&nbsp;<font color="#666666">Infr.Res:</font>&nbsp;</strong></td>
        <td height="16" colspan="9"><input name="desInfracaoRes" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getDesInfracaoRes().trim() %>' size="115" maxlength="250"></td>
      </tr>

      <tr> 
        <td height="2"></td>   
      </tr>
<%}else{%>
      <tr bgcolor="#EFF5E2"> 
        <td align="right"><strong>&nbsp;&nbsp;<font color="#666666">Infr.Res:</font>&nbsp;</strong></td>
        <td height="16" colspan="9"><input name="desInfracaoRes" type="hidden"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getDesInfracaoRes().trim() %>' size="115" maxlength="250"></td>
      </tr>
<%}%>

      <tr bgcolor="#EFF5E2">    
        <td width="87" height="16" align="right">&nbsp;<strong><font color="#666666">&nbsp;Agente 
          :</font>&nbsp;</strong></td>   
        <td width="98"><input name="codAgente" type="text" onFocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getCodAgente()%>' size="12" maxlength="2"></td>
        <td width="54" align="right"><strong><font color="#666666">Esp&eacute;cie 
          :</font>&nbsp;</strong></td>     										
        <td width="73"><input name="especieVeiculo" type="text" onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=(ConsultaArquivoDOLBeanId.isIndEspecieVeiculo()? ConsultaArquivoDOLBeanId.getEspecieVeiculo():LinhaBean.getEspecieVeiculo())%>' size="10" maxlength="2"></td>
        <td width="86" align="right"><strong><font color="#666666">Categoria :</font>&nbsp;</strong></td>
        <td width="63"><input name="categoriaVeiculo" type="text" onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=(ConsultaArquivoDOLBeanId.isIndCategoriaVeiculo()? ConsultaArquivoDOLBeanId.getCategoriaVeiculo():LinhaBean.getCategoriaVeiculo())%>' size="7" maxlength="2"></td>
        <td width="85" align="right"><strong><font color="#666666">Tipo :</font>&nbsp;</strong></td>
        <td width="47"><input name="tipoVeiculo" type="text" onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=(ConsultaArquivoDOLBeanId.isIndTipoVeiculo()? ConsultaArquivoDOLBeanId.getTipoVeiculo():LinhaBean.getTipoVeiculo())%>' size="5" maxlength="3"></td>
        <td width="42" align="right"><strong><font color="#666666">Cor :</font>&nbsp;</strong></td>
        <td width="85"><input name="corVeiculo" type="text" onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=(ConsultaArquivoDOLBeanId.isIndCorVeiculo()? ConsultaArquivoDOLBeanId.getCorVeiculo():LinhaBean.getCorVeiculo())%>' size="5" maxlength="3"></td>
      </tr>
      <tr> 
        <td height="6" colspan=10></td>
      </tr>
    </table>
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#deebc2"> 
        <td colspan=6 class="espaco2">&nbsp;&nbsp;<strong>DADOS DO CONDUTOR</strong></td>
      </tr>
      <tr> 
        <td height="3" colspan=6></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td height="16" align="right">&nbsp;<font color="#666666">&nbsp;<strong>Nome 
          :</strong></font><strong>&nbsp;</strong></td>
        <td colspan="5">
        <%if (LinhaBean.getInd_P59().equals("P59")){%>
             <input name="nomeCondutor" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getNomeCondutor()%>' size="115" maxlength="60" >
        <%}else{%>
             <input name="nomeCondutor" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getNomeCondutor()%>' size="115" maxlength="45" >
        <%}%>
        </td>
      </tr>
      <tr> 
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td width="87" height="16" align="right">&nbsp;<font color="#666666">&nbsp;<strong>CPF 
          :</strong></font><strong>&nbsp;</strong></td>
        <td width="154"><input name="numCpfCondutor" type="text" value='<%=LinhaBean.getNumCpfCondutor()%>' size="20" maxlength="11"></td>
        <td width="136" align="right"><strong><font color="#666666">CNH/PGU :</font>&nbsp;</strong></td>
        <td width="113"><input name="numCnhCondutor" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:Mascaras(this,'99999999999')" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getNumCnhCondutor()%>' size="20" maxlength="11"></td>
        <td width="112" align="right"><strong><font color="#666666">UF da CNH/PGU 
          :</font>&nbsp;</strong></td>
        <td width="118">
        <select name="uFCnhCondutor">
        			<option value="  " selected>&nbsp;&nbsp;&nbsp;</option> 					
					<script>
						var sel="";
						for (j=0;j<comboUF.length;j++) {
							if (comboUF[j]=="<%=LinhaBean.getUFCnhCondutor()%>")
								sel=" selected";
							else
								sel="";
							document.write ( "<option value=" + comboUF[j] +  sel + ">" + comboUF[j]) + "</option>"
						}
					</script>
				</select>
        </td>
      </tr>
      <tr> 
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td align="right">&nbsp;<font color="#666666">&nbsp;<strong>Endere&ccedil;o 
          :</strong></font><strong>&nbsp;</strong></td>
        <td height="16" colspan="3"><input name="enderecoCondutor" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getEnderecoCondutor()%>' size="78" maxlength="25"></td>
        <td align="right"><strong><font color="#666666">N&uacute;mero :</font>&nbsp;</strong></td>
        <td><input name="numEnderecoCondutor" type="text" id="numEnderecoCondutor" onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getNumEnderecoCondutor()%>' size="12" maxlength="5"></td>
      </tr>
      <tr> 
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td width="87" height="16" align="right"><font color="#666666"><strong>Complemento 
          :</strong></font><strong>&nbsp;</strong></td>
        <td width="154"><input name="complementoEnderecoCondutor" type="text"  onFocus="javascript:this.select();" onkeypress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getComplementoEnderecoCondutor()%>' size="20" maxlength="11"></td>
        <td width="136" align="right"><strong><font color="#666666">C&oacute;d. 
          Munic&iacute;pio :</font>&nbsp;</strong></td>
        <%if (LinhaBean.getInd_P59().equals("P59")){%>
            <td width="113"><input name="municipioCondutor" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getMunicipioCondutor()%>' size="20" maxlength="5"></td>
        <%}else{%>
            <td width="113"><input name="municipioCondutor" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getMunicipioCondutor()%>' size="20" maxlength="4"></td>
        <%}%>
        
        <td width="112" align="right"><strong><font color="#666666">CEP :</font>&nbsp;</strong></td>
        <td width="118"><input name="cepEndereco" type="text"  onfocus="javascript:this.select();" onkeypress="javascript:f_num();" onChange="this.value=this.value.toUpperCase()" value='<%=LinhaBean.getCepEndereco()%>' size="12" maxlength="8"></td>
      </tr>
      <tr> 
        <td height="2"></td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td height="16" align="right" valign="top">&nbsp;<font color="#666666">&nbsp;<strong>Obs 
          :</strong></font><strong>&nbsp;</strong></td>
<% if (LinhaBean.getCodOrgao().equals("258650")){%>
        <td colspan="5"><input name="observacao" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getObservacao().trim()%><%=LinhaBean.getComplObservacao().trim()%>' size="114" maxlength="151">
<%}else{%>
        <td colspan="5"><input name="observacao" type="text"  onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()" onkeypress="javascript:f_end();" value='<%=LinhaBean.getObservacao().trim()%>' size="114" maxlength="51">
<%}%>
    </table>
  </div>
  
  <!--Bot�es Atualizar / Retornar-->
  <div id="atualizaEretorna" style="position:absolute; left: 359px; top: 311px; width:26px; height:35px; z-index:1; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> <% if (ConsultaArquivoDOLBeanId.getNomConsulta().equals("REJEITADOS")) {%>
              <td align="right" nowrap> <button style="border: 0px; background-color: transparent; height: 28px; width: 73px; cursor: hand;" onClick="javascript:valida('acertarAuto',this.form);"><img src="<%= path %>/images/bot_atualizar_det1.gif" width="71" height="26"></button></td>
              <%} %>
              <td align="center"><img src="<%= path %>/images/inv.gif" width="10" height="10"></td>
              <td align="left" nowrap> <button style="border: 0px; background-color: transparent; height: 28px; width: 72px; cursor: hand;" onClick="javascript:  valida('R',this.form);" ><img src="<%= path %>/images/bot_retornar_det1.gif" width="70" height="26"></button></td>
            </tr>
	</table>
</div>
<!--FIM Bot�es Atualizar / Retornar-->
  
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=ConsultaArquivoDOLBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "115 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->


</form>
</body>
</html>
