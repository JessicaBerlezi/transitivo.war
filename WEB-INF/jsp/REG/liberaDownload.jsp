<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath();  
%>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Vector" %>
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaDownloadBeanId" scope="request" class="REG.ConsultaDownloadBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />  

<% 
String funcao=(String)request.getAttribute("sigFuncao"); if (funcao==null) funcao="";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
    switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
		case 'buscaDownloads':
			if (veCampos(fForm)==true) {				
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
			break;
		case 'liberaDownloads':
			if (validaCheckBox(fForm)==true) {				
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}else{
		 		alert("Nenhum arquivo selecionado.")
		 	}
			break;
	   case 'O':  // Esconder os erro
	      	document.all["MsgErro"].style.visibility='hidden' ; 
		  	break;  
    }
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	
	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}

function download(nomArquivo,fForm) {

	fForm.nomArquivo.value = nomArquivo;
   	fForm.target = "_self";
    fForm.action = "download";
    fForm.submit();
}

function registra(fForm,codArquivo,nomArquivo,extensao,funcao) {	
	fForm.codArquivo.value = codArquivo;
	fForm.nomArquivo.value = nomArquivo;	
	fForm.extensao.value = extensao;
	fForm.funcao.value = funcao;
   	fForm.target= "_self";
    fForm.action = "arquivo"; 
   	fForm.submit();
}

function validaCheckBox(fForm){
	valido = false;
	for(i=0; i<fForm.length; i++){
		if (fForm.elements[i].type=="checkbox"){
			if(fForm.elements[i].checked){
				valido = true;
			}
		}
	}
	return valido;
}

</script>
</head>

<%
 	Vector vetArquivos = (Vector) request.getAttribute("vetArquivos"); 
 %>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />

<input name="acao"        type="hidden" value=' '>

<input name="codArquivo"  type="hidden" value=''>
<input name="nomArquivo"  type="hidden" value=''>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_RECEBIDO'>
<input name="extensao"    type="hidden" value=''>
<input name="funcao"      type="hidden" value=''>

<!--IN�CIO_CORPO_sistema-->
<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:730px; overflow: visible; z-index: 1; top: 80px; left: 45px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="36%"><b>Data de Envio:&nbsp;</b>&nbsp;<input name="De" type="text" id="De" size="12" maxlength="12" value="<%=ConsultaDownloadBeanId.getDataIni()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  &nbsp;at�&nbsp;<input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=ConsultaDownloadBeanId.getDataFim()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  </td>		  
	          <td align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaDownloads',document.frmDownload);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:50px; top:328px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="10" bgcolor="#FFFFFF"  style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido<br>&nbsp;em processamento</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFB0B0" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Rejeitado</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#C6EAB5"style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Processado</td>
        <td ><img src="images/inv.gif" width=170 height=2></td>
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;efetuado</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFFFFF" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;n�o efetuado</td>

      </tr>
    </table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:40px; top:105px; width:740px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#deebc2"> 
        <td width="225" height="15"  align="center" bgcolor="#deebc2"><strong>Arquivo Recebido (Arq.Origem)</strong></td>
        <td width="57" bgcolor="#EDF4DF" align="center"><strong>em</strong></td>
        <td width="2" bgcolor="#FFFFFF"></td>
        <td width="145" height="15"  align="center" bgcolor="#deebc2"><strong>Gravado</strong></td>
        <td width="61" bgcolor="#EDF4DF" align="center"><strong>Download</strong></td>
        <td width="2" bgcolor="#FFFFFF"></td>
        <td width="145" height="15"  align="center"><strong>Liberar</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:40px; top:124px; width:740px; height:200px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
         
     <%
    	int tam=vetArquivos.size();
    	String nomArquivo, nomArquivoGrav, nomArquivoRej, status, codArquivo, datGrav, datRej;
    	String corArq, corGrav, corRej;    	
    	int qtdReg, qtdLinhaErro;
    	
    	
      	for(int i=0;i<tam;i++) {
      		REG.ArquivoRecebidoBean ArqRecBean = new REG.ArquivoRecebidoBean();
      		ArqRecBean=(REG.ArquivoRecebidoBean)vetArquivos.get(i);
      		nomArquivo=ArqRecBean.getNomArquivo();
      		codArquivo=ArqRecBean.getCodArquivo();
      		status=ArqRecBean.getCodStatus();
      		try {
      			qtdReg=Integer.parseInt(ArqRecBean.getQtdRegRecebido());
      		}
      		catch (Exception e) {
      			qtdReg=0;
      		}
      		try {
      			qtdLinhaErro=Integer.parseInt(ArqRecBean.getQtdLinhaErro());
      		}
      		catch (Exception e) {
      			qtdLinhaErro=0;
      		}
      		
      		if (status.equals("9")) {
      			corArq="#FFB0B0";
      		}
      		else if (status.equals("8") || status.equals("7") || status.equals("1")) {
      			corArq="#FFFFD5";
      		}
      		else if (status.equals("2")||"REG2066".equals(UsuarioFuncBeanId.getJ_sigFuncao())) {
      			corArq="#C6EAB5";
      		}
      		else {
      			corArq="#FFFFFF";
      		}
      		            		
      		nomArquivoGrav="";
      		nomArquivoRej="";
      		datGrav="";
      		datRej="";
      		corGrav="#FFFFFF";
      		corRej="#FFFFFF";
      		
      		if (status.equals("2")) {
      		    if (ArqRecBean.getSufixoRetornoRej().equals("")) {
      				nomArquivoGrav="<table width=\"100%\"><tr><td "+ArqRecBean.geraStyleEEventoOnClickGrv(funcao)+">" + ArqRecBean.getCodIdentRetornoGrv() + "..." + " (" + qtdReg + " Reg)" + "</td></tr></table>";
      				if (ArqRecBean.getDatDownloadGravado()!=null && !ArqRecBean.getDatDownloadGravado().equals("")) {
	      				datGrav=ArqRecBean.getDatDownloadGravado();
      					corGrav="#FFFFD5";
      				}
      			}
      			else if (ArqRecBean.getSufixoRetornoGrv().equals("")) {
      				nomArquivoRej="<table width=\"100%\"><tr><td "+ArqRecBean.geraStyleEEventoOnClickRej(funcao)+" >" + ArqRecBean.getCodIdentRetornoRej() + "..." + " (" + qtdLinhaErro + " Reg)" + "</td></tr></table>";
      				if (ArqRecBean.getDatDownloadErro()!=null && !ArqRecBean.getDatDownloadErro().equals("")) {
	      				datRej=ArqRecBean.getDatDownloadErro();
      					corRej="#FFFFD5";
      				}
      			}
      			else {
      				if (qtdReg>qtdLinhaErro) {
      					nomArquivoGrav="<table width=\"100%\"><tr><td "+ArqRecBean.geraStyleEEventoOnClickGrv(funcao)+">" + ArqRecBean.getCodIdentRetornoGrv() + "..." + " (" + (qtdReg-qtdLinhaErro) + " Reg)" + "</td></tr></table>";
      					if (ArqRecBean.getDatDownloadGravado()!=null && !ArqRecBean.getDatDownloadGravado().equals("")) {
	      					datGrav=ArqRecBean.getDatDownloadGravado();
	      					corGrav="#FFFFD5";
    	  				}
      				}
      				if (qtdLinhaErro>0) {
      					nomArquivoRej="<table width=\"100%\"><tr><td "+ArqRecBean.geraStyleEEventoOnClickRej(funcao)+">"+ ArqRecBean.getCodIdentRetornoRej() + "..." + " (" + qtdLinhaErro + " Reg)" + "</td></tr></table>";
      					if (ArqRecBean.getDatDownloadErro()!=null && !ArqRecBean.getDatDownloadErro().equals("")) {
	      					datRej=ArqRecBean.getDatDownloadErro();
	      					corRej="#FFFFD5";
    	  				}
      				}
      			}
      		}
      		
      		
      	String NomArqOrig="";
    	NomArqOrig=ArqRecBean.getNomArquivoDir();
    	
    	if(ArqRecBean.getNomArquivoDir().lastIndexOf(".")== - 1){
    		if  (!"REG2066".equals(UsuarioFuncBeanId.getJ_sigFuncao()))
    			NomArqOrig = NomArqOrig + ".txt";
    		else
    			
    			NomArqOrig = NomArqOrig + ".bkp";
		}else{
			if  (!"REG2066".equals(UsuarioFuncBeanId.getJ_sigFuncao()))
				NomArqOrig = NomArqOrig.substring(0,ArqRecBean.getNomArquivoDir().lastIndexOf(".")) + ".txt";
			else
				NomArqOrig = NomArqOrig.substring(0,ArqRecBean.getNomArquivoDir().lastIndexOf(".")) + ".bkp";
		}
      		
      %>
      <tr> 
      
      	<%if (corArq=="#FFB0B0" ||"REG2066".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %>      		
			
				<td width="225" height="15" bgcolor="<%=corArq%>"><%=nomArquivo + " (" + qtdReg + " Reg)"%></td>			
		
		<%}else{
			if ("EMITEPER,EMITEPONTOPER,EMITEVEX".indexOf(ArqRecBean.getCodIdentArquivo()) >= 0){
		%>		
				<td width="225" height="15" bgcolor="<%=corArq%>"><%=nomArquivo + " (" + qtdReg + " Reg)"%></td>
        <%} else { %>
        		<td width="225" height="15" bgcolor="<%=corArq%>" style = " cursor: hand;" onClick="javascript:download('<%=NomArqOrig%>', document.frmDownload)";><%=nomArquivo + " (" + qtdReg + " Reg)"%></td>
        <%  }
        }%>
        <td width="57" align="center"><%=ArqRecBean.getDatRecebimento()%></td>
        <td width="2"></td>
        <td width="145" height="15" bgcolor="<%=corGrav%>"><%=nomArquivoGrav%></td>
        <td width="61" align="center"><%=datGrav%></td>
        <td width="2"></td>
        <td width="145" height="15"><center><%if("2".equals(status)) {%><input type="checkbox" name="codArquivos" value="<%=codArquivo%>" /><%} %></center></td>
      </tr>
      <tr> 
        <td height="1" colspan="8"  bgcolor="#cccccc"></td>
      </tr>
      
      <%} %>
    </table>
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
    	<tr>
    		<td width="80%"></td>
    		<td width="20%" align="center" ><input type="button" onClick="javascript: valida('liberaDownloads',document.frmDownload);" value="Liberar"/>
    	</tr>
    </table>
</div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',document.frmDownload);"> 
          <img src="<%= path %>/images/REG/bot_retornar.png" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= ConsultaDownloadBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>