<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<head>
	<link rel="icon" href="./images/sys/favicon.ico" type="image/x-icon">
</head>
<style type=text/css>

input,select,textarea 
    {
	font-family: verdana;
	font-size: 10px;
	border: 1px solid #999999;
}

.input 
	 {
	 font-family: verdana;
	 font-size: 9px;
	 border: 0px;
}

button
    {
	font-family: verdana;
	font-size: 9px;
	border: 1px solid;
	font-variant: normal;
	color: #000000;
	background-color: #D4D4D4;
	margin: 0px;
	padding: 0px;
}

table
     {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 font-size: 9px;
	 letter-spacing: normal;
	 font-variant: normal;
	 text-transform: none;
	 text-decoration: none;
	 	 
}

.td2 {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #000000;
	 border-bottom: 1px solid #000000;
	 border-left: 0px solid #999999;
     word-spacing: 10px;
}

.texto {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 color: #d0d0d0;
	 font-size: 9px;
	 font-weight: bold;
	 letter-spacing: normal;
	 font-variant: small-caps;
	 text-transform: none;
	 text-decoration: none;
}

.texto2 
	{
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 color: #666666;
	 font-size: 9px;
	 letter-spacing: normal;
	 font-variant: normal;
	 text-transform: none;
	 text-decoration: none;
}

.td 
	{
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 border-top: 1px solid #999999;
	 border-right: 1px solid #999999;
	 border-bottom: 1px solid #999999;
	 border-left: 0px solid #999999;
     word-spacing: 10px;
}

.sem-borda  
    {
	border: 0px none;
}

td {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
  	font-size: 9px;
	color: #000000;
	border: 0px;
	
}
A {
	 COLOR: black; FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif; TEXT-DECORATION: none
}
A:link {
	 COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica
}
A:visited {
	 COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica
}
A:hover {
	 COLOR: #003366; FONT-FAMILY: Verdana, Arial, Helvetica; TEXT-DECORATION: underline
}

.branco {
	 COLOR: white; FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif; TEXT-DECORATION: none
}
.branco:link {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco:visited {
	 COLOR: #e7e7e7; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco:hover {
	 COLOR: #99ccff; FONT-FAMILY: Verdana, Arial, Helvetica; TEXT-DECORATION: underline
}

body
    {
	scrollbar-face-color: #cccccc;
	scrollbar-shadow-color: #666666;
	scrollbar-highlight-color: #cccccc;
	scrollbar-3dlight-color: #666666;
	scrollbar-darkshadow-color: #cccccc;
	scrollbar-track-color: #e7e7e7;
	scrollbar-arrow-color: #00000;
	}
.readonly
{
background-color: #EFF5E2;
}
.transp
{
background-color: transparent;
border-style: none;
border-width: 0px;
color: #ffffff;
font-weight: bold;
}
.espaco
{
letter-spacing: 2px;
}
.espaco2
{
letter-spacing: 1px;
}
.BordaTopo  
    {
	margin-top: 5px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
}
/*-------0800---------*//*Esta � a posi��o do n�mero de suporte do 0800, no rodap� das p�ginas*/
#telSuporte {		position:absolute; left:35px;
			bottom: 0px; width:550px;
			height:15px; z-index:10; 
			background-color: transparent; overflow: visible;
			color: #FFFFFF; font-family: verdana;
			font-size: 55%;	font-weight: bold;
			letter-spacing: 1px;
}
/********************************/
</style>

