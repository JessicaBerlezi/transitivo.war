<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="RelatLotesProcBean" scope="session"
	class="REG.RelatLotesProcBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}table.comborda td {border: 1px solid #000000}
</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int npag = 0,seq = 0, contLinha = 0;	
	Iterator it = RelatLotesProcBean.getDados().iterator();
	REG.RelatLotesProcBean relatorioImp  = new REG.RelatLotesProcBean();	
	if( RelatLotesProcBean.getDados().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.RelatLotesProcBean)it.next() ;
			   seq++; 
			   contLinha++;
		       if (contLinha%54==1){
					npag++;							
					if (npag!=1){
%> 
		  		  <jsp:include page="rodape_impConsulta.jsp" flush="true" />
		  		  <div class="quebrapagina"></div>
<%					} %>		  		  
		  		  		  
		  		  <jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			        <jsp:param name="nPag" value= "<%=npag%>" />				
			      </jsp:include> 
		      	   <table width="100%" border="0"  align="center" cellpadding="0" cellspacing="0" >  	   		
					  <tr><td height="5"></td></tr>
					  <tr>
					      <td width="6%" >&nbsp;&nbsp;<strong>&Oacute;rg&atilde;o:</strong><%=relatorioImp.getSigOrgao()%></TD>
					  </tr>   
					  <tr><td height="5"></td></tr>
  		         </table>	 
     
			      <table width="100%" border="0"  class="comborda" align="center" cellpadding="0" cellspacing="0" >  	   		
					  <tr> 	
						 <td width="80"   align="center"  bgcolor="#999999"><strong><font color="#000000">SEQ</font></strong></TD>
						 <td width="100"  align="center"  bgcolor="#999999"><font color="#000000"><strong>DATA LOTE</strong></font></TD>
						 <td width="140"  align="center"  bgcolor="#999999"><strong><font color="#000000">PROCESSADOS</font></strong></TD>
						 <td width="140"  align="center"  bgcolor="#999999"><strong><font color="#000000">GRAVADOS</font></strong></TD>
 						 <td width="140"  align="center"  bgcolor="#999999"><strong><font color="#000000">REJEITADOS</font></strong></TD>
  						 <td align="center"  bgcolor="#999999"><strong><font color="#000000">CANCELADOS</font></strong></TD>
		 		      </tr>
            	  </table>    
			  <%}%>


		    <table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">							
  				<tr> 
				  <td width="80"    align="center" style="line-height:15px;"><%=seq%></TD>
				  <td width="100"   align="center"><font color="#000000"><%=relatorioImp.getDataLote()%></font></TD>
	 		      <td width="140"   align="right"><font color="#000000"><%=relatorioImp.getQtdReg()%>&nbsp;&nbsp;</font></TD>
				  <td width="140"   align="right"><font color="#000000"><%=relatorioImp.getQtdGrav()%>&nbsp;&nbsp;</font></TD>
  				  <td width="140"   align="right"><font color="#000000"><%=relatorioImp.getQtdRej()%>&nbsp;&nbsp;</font></TD>
  				  <td align="right"><font color="#000000"><%=relatorioImp.getQtdCancel()%>&nbsp;&nbsp;</font></TD>
				</tr>
				<tr>
					<td colspan="7" bgcolor="#999999" style="padding-left:1px";></td>
				</tr>
		     </table>


		     
  <%  } %>
<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="0" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
   	      <td height="35" align=center bgcolor="#CCCCCC"><b><%= msg %></b></td>
		</tr>	
	</table>      			
<%} %>    

  <jsp:include page="rodape_consutaimp.jsp" flush="true" />

</form>
</body>
</html>