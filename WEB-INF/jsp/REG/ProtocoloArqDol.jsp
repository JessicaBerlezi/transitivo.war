<!--M�dulo : REG
	Vers�o : 1.1
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="ArquivoDolId" scope="request" class="REG.ArquivoDolBean" />
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<%@ include file="Css.jsp" %>

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
  switch (opcao) {
   case 'R':
    close() ;
	break;
   case 'O':  // Esconder os erro
	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
	
  }
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ProtoRecForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao"     type="hidden" value=' '>

<!--Cabe�alho-->
<div id="dadosProtocolo" style="position:absolute; left:50px; top:80px; width:720px; height:111px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="0" class="table">
      <tr bgcolor="#deebc2"> 
        <td colspan=4 height="23">&nbsp;&nbsp;<strong>Protocolo de Recebimento&nbsp;<%=ArquivoDolId.getNumProtocolo() %>&nbsp; 
          do Arquivo&nbsp;&nbsp;&nbsp;<%=ArquivoDolId.getNomArquivo()%>&nbsp;</strong></td>
      </tr>
      <tr> <td height="6" colspan=4></td></tr>
      <tr bgcolor="#EFF5E2"> 
        <td width="188" >&nbsp;&nbsp;<strong>C�digo do Arquivo:</strong></td>
        <td width="215"><%=ArquivoDolId.getCodIdentArquivo() %></td>
        <td><strong>Exerc&iacute;cio:</strong></td>
        <td><%=ArquivoDolId.getNumAnoExercicio() %></td>
      </tr>
      <tr> <td height="2" colspan=4></td>  </tr>
      <tr bgcolor="#EFF5E2"> 
        <td>&nbsp;&nbsp;<strong>Sequencia:</strong></td>
        <td><%=ArquivoDolId.getNumControleArq() %></td>
        <td height="16"><strong>Data de Envio:</strong></td>
        <td><%=ArquivoDolId.getDatRecebimento() %></td>
      </tr>
      <tr> <td height="2" colspan=4></td>  </tr>
      <tr bgcolor="#EFF5E2"> 
        <td>&nbsp;&nbsp;<strong>N&ordm; Registros:</strong></td>
        <td><%=ArquivoDolId.getQtdReg() %></td>
        <td width="149"><strong>&Oacute;rg&atilde;o Lota&ccedil;&atilde;o:</strong></td>
        <td width="168"><%=ArquivoDolId.getSigOrgaoLotacao()%></td>
      </tr>
    </table>
</div>

<%
	if (ArquivoDolId.getErroArquivo().size() > 0) {
%>	
<!--ARQUIVO REJEITADO--> 
<div id="arqRejeitado" style="position:absolute; left:50px; top:165px; width:720px; height:195px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="720" border="0" cellpadding="0" cellspacing="2" class="table">
      <tr bgcolor="#deebc2" > 
        <td height="23" class="espaco" colspan=3>
			<p>&nbsp;&nbsp;<strong><font color="#CC3300">ARQUIVO REJEITADO </font></strong></p></td>
      </tr>
	  <tr bgcolor="#EFF5E2"> 
       	<td width="56" align=center><strong>Registro</strong></td>
      	<td width="44" align=center><strong>Erro</strong></td>
      	<td width="610">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Motivo</strong></td>
      </tr>
	  	<%
	if (ArquivoDolId.getErroArquivo().size() > 0) {
		String l = "";
		for ( int t=0; t<ArquivoDolId.getErroArquivo().size() ;t++) {
			l = (String) ArquivoDolId.getErroArquivo().elementAt(t) ;
	%>
	  		<tr bgcolor="#EFF5E2"> 
       			<td width="56" align=center><%=l.substring(0,5)%></td>
       			<td width="44" align=center><%=l.substring(6,9)%></td>
      			<td width="610"><%=l.substring(10,l.length())%></td>
     		</tr>
	<%
		} 
	} 
	%>
    </table>
</div>

<!--FIM DIV ARQUIVO REJEITADO-->
<% } else { %>

<!--INSTRUCOES--> 
<div id="instrucoes" style="position:absolute; left:50px; top:180px; width:720px; height:136px; z-index:5; overflow: visible; visibility: visible;"> 
    <table id="table3" width="720" border="0" cellpadding="0" cellspacing="0" class="table">
    <TBODY>
      <tr  bgcolor="#deebc2"> 
        <td width="209" height="23"><p>&nbsp;&nbsp;<strong>Instru&ccedil;&otilde;es</strong></p></td>
      </tr>
      <tr><td height="6"></td> </tr>
    </table>
    <table id="table4" width="720" border="0" cellpadding="4" cellspacing="0" class="table">
      
      <%if (!"REG0120".equals(UsuarioFuncBeanId.getJ_sigFuncao())){ %>
      <tr bgcolor="#EFF5E2">
        <td height="18" valign="top" colspan=2>1 - ARQUIVO FOI ENVIADO 
          E RECEBIDO COM SUCESSO. AGUARDE PROCESSAMENTO.</td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td height="18" valign="top" colspan=2>2 - ACOMPANHE O 
          STATUS ATRAV&Eacute;S DA OP��O:&nbsp;&nbsp;&nbsp;<font color=red>CONSULTA LOTE.</font></td>
      </tr>
<%}else{%>
      <tr bgcolor="#EFF5E2">
        <td height="18" valign="top" colspan=2>1 - SEU ARQUIVO FOI ENVIADO 
          E RECEBIDO COM SUCESSO.</td>
      </tr>
      <tr bgcolor="#EFF5E2"> 
        <td height="18" valign="top" colspan=2>2 - O ARQUIVO PODER� SER RECUPERADO 
          ATRAV&Eacute;S DA OP��O:&nbsp;&nbsp;&nbsp;<font color=red>RETORNAR ARQUIVO - BACKUP SMIT_DOL .</font></td>
      </tr>
 <%}%>     
    </table>
</div>
<!--FIM DIV INSTRUCOES--> 
<% }  %>
 
<!--FIM_CORPO_sistema--> 
<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

</form>
</body>
</html>



<!--   		document.ProtoRecForm.txtErroArq.value=""; -->
<!--		mostra("arqRejeitado"); -->
<!--		-->
