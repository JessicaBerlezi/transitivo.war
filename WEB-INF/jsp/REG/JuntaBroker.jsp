<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List" %>
<%@ page import="REC.JuntaBean" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="OrgaoId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="JuntaId" scope="request" class="REC.JuntaBean" />
<!-- Chama o Objeto da Tabela de Orgaos -->

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaOrgao':
		if (fForm.codOrgao.value==0) {
		   alert("�rg�o n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else{
	  	//fForm.textoErro.innerHTML = sErro
		fForm.textoErro.innerText = sErro;
		document.all["MsgErroCliente"].style.visibility = 'visible'
	  } 
	  break ;
   case 'R':
   	  fForm.atualizarDependente.value = "<%=(String)request.getAttribute("atualizarDependente")%>"
	  if (fForm.atualizarDependente.value=="S") { 
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	
	case '1':
   		  if (document.layers) fForm.layers["MsgErroCliente"].visibility='hide' ; 
	      else                 document.all["MsgErroCliente"].style.visibility='hidden' ; 
	  break;    
  }
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codOrgao.value==0) {
   valid = false
   sErro = sErro + "Org�o n�o selecionado."
}
else{
	for(var i=0;i<fForm.nomJunta_aux.length;i++){
		if( (document.all["nomJunta"+i].value != document.all["nomJuntaBroker"+i].value) || (document.all["sigJunta"+i].value != document.all["sigJuntaBroker"+i].value) || (document.all["indTipo"+i].value != document.all["indTipoBroker"+i].value) ){
			sErro = sErro + "Erro na linha: " + fForm.seq[i].value + " | Relator do Smit difere ao do Detran \n"
			valid = false
		}
	}
}
return valid ;
}
 
function moveCampos(nomJunta, nomJuntaBroker, sigJunta, sigJuntaBroker, indTipo, indTipoBroker) {
	nomJunta.value = nomJunta.value.toUpperCase()
	sigJunta.value = sigJunta.value.toUpperCase()
	indTipo.value = indTipo.value.toUpperCase()
	nomJuntaBroker.value = nomJunta.value
	sigJuntaBroker.value = sigJunta.value
	indTipoBroker.value = indTipo.value
}
 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<jsp:include page="../REG/Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="atualizarDependente" type="hidden" value=' '>				
<div id="WK_SISTEMA" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 70px; left:204px; visibility: visible;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="400"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>     
	  </tr>
      <tr>
		  <td align="left" width="60%"><b>�rg�o:&nbsp;</b>
   			<% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>				  
				<jsp:setProperty name="OrgaoId" property="j_abrevSist" value="ACSS" />  	 	     	 	  
				<jsp:setProperty name="OrgaoId" property="colunaValue" value="cod_orgao" />  
				<jsp:setProperty name="OrgaoId" property="popupNome"   value="codOrgao"  />  
				<jsp:setProperty name="OrgaoId" property="popupString" value="sig_orgao,SELECT cod_orgao,sig_orgao FROM TSMI_ORGAO order by nom_orgao asc" />                 	 
				<jsp:getProperty name="OrgaoId" property="popupString" />
			<% }
			else { %>
		        <input name="codOrgao" type="hidden" size="6" maxlength="20"  value="<%= OrgaoId.getCodOrgao() %>"> 
		        <input disabled name="sigOrgao" type="text" size="10" value="<%= OrgaoId.getSigOrgao() %>">
			<% } %>			
		  </td>
   		  <% if (((String)request.getAttribute("atualizarDependente")).equals("S")==false){ %>			  
 	         <td align=right width=350>&nbsp;&nbsp;&nbsp;
			  	<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaOrgao',this.form);">	
	    	    <IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19"></button>																			
			 </td>
		<% } %>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
		<% if (((String)request.getAttribute("atualizarDependente")).equals("S")){ %>			
	 	   <td width="33%" align="left"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" ></button>
     	   </td>
	       <td  width="34%" align="center"> 
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.png" width="52" height="19"></button>
           </td>
		<%} %>
    </TR>

  </TABLE>  
</div>
<!--FIM BOTOES-->  

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 134px; height: 32px; visibility: visible;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<tr bgcolor="#FFFFFF">
	<td width=60></td>
	<td width=10></td>
	<td colspan="3"><font color="#000000">Listagem SMIT</font></td>
	<td width=10 bgcolor="#FFFFFF"></td>
	<td colspan="3"><font color="#000000">Listagem DETRAN</font></td>
	</tr>
	<tr bgcolor="#A8B980"> 
	    <td width=60 align="center"><font color="#ffffff"><b>Seq</b></font></TD>	
		<td width=10 bgcolor="#FFFFFF"></td> 	
        <td width=150 align="center"><font color="#ffffff"><b>Junta</b></font></TD>		 	  
        <td width=90 align="center"><font color="#ffffff"><b>Sigla</b></font></TD>		 
        <td width=70  align="center"><font color="#ffffff"><b>Tipo</b></font></TD>	
		<td width=10 bgcolor="#FFFFFF"></td> 
        <td width=150 align="center"><font color="#ffffff"><b>Junta</b></font></TD>		 	  
        <td width=90 align="center"><font color="#ffffff"><b>Sigla</b></font></TD>		 
        <td align="center"><font color="#ffffff"><b>Tipo</b></font></TD>	

	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 160px; height: 191px; visibility: visible;"> 
    <table id="tabela" border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (((String)request.getAttribute("atualizarDependente")).equals("S")) {				   
  	            	   List juntaSmit = (List)request.getAttribute("JuntaSmitId");
  	            	   List juntaBroker = (List)request.getAttribute("JuntaBrokerId");
  	            	   List errosPosicoes = (List)request.getAttribute("errosPosicao"); 
					   for (int i=0; i<juntaSmit.size(); i++) {	   %>
      <tr bgcolor="#EFF5E2">
	  	<td width=60 align="center"> 
          <input name="seq"  type="text"  size="4" value="<%= i+1 %>">
		</td>
		<td width=10 bgcolor="#FFFFFF" ></td>
		<td width=150 align="center"> 
          <input name="nomJunta<%=i%>" type="text" size="20" maxlength="20"  value="<%= ((JuntaBean)juntaSmit.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="moveCampos(nomJunta<%=i%>,nomJuntaBroker<%=i%>,sigJunta<%=i%>,sigJuntaBroker<%=i%>,indTipo<%=i%>,indTipoBroker<%=i%>)" >
          <input name="nomJunta_aux" type="hidden" size="20" maxlength="20"  value="<%= ((JuntaBean)juntaSmit.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
		  <input name="codJunta"    type="hidden"  value="<%= ((JuntaBean)juntaSmit.get(i)).getCodJunta() %>">
		</td>
        <td width=90 align="center"> 
          <input name="sigJunta<%=i%>"     type="text" size="10" maxlength="10"  value="<%= ((JuntaBean)juntaSmit.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="moveCampos(nomJunta<%=i%>,nomJuntaBroker<%=i%>,sigJunta<%=i%>,sigJuntaBroker<%=i%>,indTipo<%=i%>,indTipoBroker<%=i%>)" >
          <input name="sigJunta_aux"     type="hidden" size="10" maxlength="10"  value="<%= ((JuntaBean)juntaSmit.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" >
        </td>			
    	<td width=70  align="center"> 
	      <input name="indTipo<%=i%>"  type="text" size="5" maxlength="1"  value="<%= ((JuntaBean)juntaSmit.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" onChange="moveCampos(nomJunta<%=i%>,nomJuntaBroker<%=i%>,sigJunta<%=i%>,sigJuntaBroker<%=i%>,indTipo<%=i%>,indTipoBroker<%=i%>)" >	
	      <input name="indTipo_aux"  type="hidden" size="5" maxlength="1"  value="<%= ((JuntaBean)juntaSmit.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" >	
		</td>
		<td width=10 bgcolor="#FFFFFF" ></td>
    	<td width=150 align="center"> 
          <input name="nomJuntaBroker<%=i%>" type="text" size="20" maxlength="20"  value="<%= ((JuntaBean)juntaBroker.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
          <input name="nomJuntaBroker_aux" type="hidden" size="20" maxlength="20"  value="<%= ((JuntaBean)juntaBroker.get(i)).getNomJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
		  <input name="codJuntaBroker"    type="hidden"  value="<%= ((JuntaBean)juntaBroker.get(i)).getCodJunta() %>">
		</td>
		<td width=90  align="center"> 
          <input name="sigJuntaBroker<%=i%>"     type="text" size="10" maxlength="10"  value="<%= ((JuntaBean)juntaBroker.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
          <input name="sigJuntaBroker_aux"     type="hidden" size="10" maxlength="10"  value="<%= ((JuntaBean)juntaBroker.get(i)).getSigJunta().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
        </td>
		<td align="center"> 
	      <input name="indTipoBroker<%=i%>"  type="text" size="5" maxlength="1"  value="<%= ((JuntaBean)juntaBroker.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" >	
	      <input name="indTipoBroker_aux"  type="hidden" size="5" maxlength="1"  value="<%= ((JuntaBean)juntaBroker.get(i)).getIndTipo().trim() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" >	
		</td>
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#EFF5E2">
		<td width=60 align="center"> 
          <input name="seq" readonly type="text" size="4" value="">
        </td>
        <td width=10 bgcolor="#FFFFFF" ></td>
		<td width=150 align="center"> 
          <input name="nomJunta" readonly type="text" size="20" maxlength="20" value="">
        </td>
	   	<td width=90 align="center"> 
          <input name="sigJunta" readonly type="text" size="10" maxlength="10" value="">
		</td>		
    	<td width=70 align="center"> 
          <input name="indTipo"  readonly type="text" size="5"  maxlength="1"  value="0">
		</td>
	    <td width=10 bgcolor="#FFFFFF" ></td>
    	<td width=150 align="center"> 
          <input name="nomJunta" readonly type="text" size="20" maxlength="20" value="">
	    </td>
		 <td width=90 align="center"> 
          <input name="sigJunta" readonly type="text" size="10" maxlength="10" value="">
		</td>
		<td align="center"> 
          <input name="indTipo"  readonly type="text" size="5"  maxlength="1"  value="0">
		</td>
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>


<div id="MsgErroCliente" style="POSITION: absolute; width:720; z-index: 999; left: 55px; top: 86px; visibility: hidden;">      
  <TABLE  border='1' cellpadding="0" cellspacing="0" width=80% align=center bgcolor="#e7e7e7">
    <tr><td colspan=3 height=18 bgcolor="#e7e7e7"></td></tr>
    <tr> 
	<td width=8% bgcolor="#e7e7e7"></td> 	
	<td width=84%> 
           <table border='0' cellpadding="0" cellspacing="0" width=100% bgcolor="#e7e7e7">
		    <tr><td align=center bgcolor="#e7e7e7">
				<textarea name="textoErro" rows="13" cols=85 align="center"></textarea></td></tr>			
		    <tr>	
			   <td align=center><button type=button NAME="Ok"   style="height: 23px; width: 44px;border: none; background: transparent;" onClick="javascript: valida('1',this.form);">	
					<IMG src="<%= path %>/images/ok_fundo_cinza.gif"  align=left ></button></td>
		    </tr>	
		  </table>
	 </td>
    <td width=8% bgcolor="#e7e7e7"></td> 			 
    </tr>
    <tr><td colspan=3 height=8 bgcolor="#e7e7e7"></td></tr>	
 </table>	
</div> 

<!-- Rodap�-->
<jsp:include page="../REG/Retornar.jsp" flush="true" />

<jsp:include page="../REG/Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= JuntaId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%= OrgaoId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.juntaForm.codOrgao.length;i++){
    if(document.juntaForm.codOrgao.options[i].value==njint){
       document.juntaForm.codOrgao.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


