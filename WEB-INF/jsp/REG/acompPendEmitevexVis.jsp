<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="sys.Util" %>
<% String nPag      = request.getParameter("nPag");    if(nPag==null)     nPag     ="0"; 
   String tituloConsulta = (String)request.getAttribute("tituloConsulta");
   
   String datIni      = request.getParameter("dataIni");    if(datIni==null)     datIni     ="";
   String datFim      = request.getParameter("dataFim");    if(datFim==null)     datFim     ="";
   
   
   
    %>
<jsp:useBean id="acompPendEmitevexBeanId"  scope="request" class="REG.acompPendEmitevexBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>
<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) 
{
 switch (opcao) {
   case 'ImprimeRelatorio':	
        
        fForm.dataIni.value=fForm.datIni.value;
        fForm.dataFim.value=fForm.datFim.value;     
	    fForm.acao.value=opcao;
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	
	  break ; 
   case 'GeraExcel':
	  if (veCampos(fForm)==true){
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "geraxls";  
		fForm.submit();  		  
	  }
	  break ;
   case 'GeraExcelTodos':  
        document.all["codigoArquivo"].value='';      	
       	document.all["acao"].value='GeraExcel'
		fForm.target= "_self";
		fForm.action = "xlsPendEmitevex";  
		fForm.submit(); 
	  break;
	case 'GeraWord':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'R':
			close();
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function setaCodArquivo(opcao,codArquivo,fForm) {
	document.all["acao"].value=opcao;       		
    document.all["codigoArquivo"].value=codArquivo;     
	document.ConsultaLogAcessoImp.target= "_self";
    document.ConsultaLogAcessoImp.action = "acessoTool";  
   	document.ConsultaLogAcessoImp.submit();	  

}



function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.dataIni.value==0) {  valid = false
	   sErro += "Data in�cio n�o informada. \n"
	}
	if (fForm.dataFim.value==0) {  valid = false
	   sErro += "Data fim n�o informada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}

function changeColorOver(obj) {	
	obj.style.background="#EDF4DF";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#deebc2";
	obj.style.textDecoration="none";
}

</script>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="ConsultaLogAcessoImp" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="datIni" type="hidden" value='<%=datIni%>'>	
<input name="datFim" type="hidden" value='<%=datFim%>'>
<input name="dataIni" type="hidden" value=''>	
<input name="dataFim" type="hidden" value=''>
<input name="codigoArquivo" type="hidden" value=''>	
<input name="nomArquivo"  type="hidden" value='Pend_Emitevex_Todos.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>		

<div  style="position:absolute; width:120px; overflow: visible; z-index: 134; top: 74px; right: 50px; visibility: visible;" > 
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
	<tr>		
			<td width="160" align="left">
				<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimeRelatorio',this.form);"> 	
					<IMG src="<%= path %>/images/bot_imprimir_ico.gif" align="left" >
				</button>&nbsp;&nbsp;&nbsp;	     
			   <button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcelTodos',this.form);"> 	
				 	<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
			   </button>	     	
			</td>
	</tr>
	<tr><td colspan="2" height="2"></td></tr>	 
</table> 
</div>

<div id="principal" style="position:absolute; height:40%; top: 100px; left:50px;right: 15px;z-index:50;overflow: visible;visibility: visible; background: #ffffff;">
<div id="Titulotabela" style="position:relative; left:0px; right: 0px;z-index:50;overflow: visible; visibility: visible;"> 
				
			<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
			<tr bgcolor="#a8b980" height="20"> 	
				 <td width="40"  align="center"  ><font color="#ffffff"><b>Seq</b></font></td>
				 <td width="330" align="center"  ><font color="#ffffff"><b>Aquivo</b></font></td>
				 <td width="94"  align="center"  ><font color="#ffffff"><b>Data Envio</b></font></td>		 
				 <td width="72"  align="center"  ><font color="#ffffff"><b>Qtd Env.</b></font></td>					 
				 <td width="72"  align="center"  ><font color="#ffffff"><b>Qtd Ret.</b></font></td>
				 <td             align="center" ><font color="#ffffff"><b>Qtd Pend.</b></font></td>
			 </tr>
		  </table>     
</div>

<div id="corpotabela" style="position: relative; height:100%; z-index: 60;width:100%; left: 0px; right: 0px;overflow: auto; visibility: visible;" > 
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0,npag = 0,seq = 0;	
  	String valorFormatado1="0",valorFormatado2="0",valorFormatado3="0",valorFormatado4="0",valorFormatado5="0",valorFormatado6="0";
	String valor1="0",valor2="0",valor3="0",valor4="0",strqtdpend = "0",strqtdpendZero = "0",valor5="0",valor6="0";
  	String totEnv="";	
	String totRet="";
	String dataAtual="";
	String mesAtual="";
	String mesAnterior="";
	String ano="";	
	 long qtdPendZero 			=0;
	 long qtdEnvZero  			=0;
	 long qtdRetZero  			=0;
	 long qtdPend 				=0;
	 long qtdEnv  				=0;
	 long qtdRet  				=0;
	 long pend  				=0;	
	 long valorTotGerEnv  		=0;
	 long valorTotGerPend  		=0;
	 long valorTotGerRet  		=0;	
	 
	 long totalPendencia=0;  
	 int quantArqPendZero=0; 
	 DecimalFormat formato = new DecimalFormat(",##0") ;
	
	Iterator it = acompPendEmitevexBeanId.getListaArqs().iterator();
	REG.acompPendEmitevexBean relatorioImp  = new REG.acompPendEmitevexBean();					
	if( acompPendEmitevexBeanId.getListaArqs().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.acompPendEmitevexBean)it.next() ;
			   dataAtual = relatorioImp.getDatRecebimento();	
			   mesAtual=relatorioImp.getDatRecebimento().substring(3,5);
			   ano=relatorioImp.getDatRecebimento().substring(6,10);
			   contLinha++;	  
			   
			   qtdPend= Integer.parseInt(relatorioImp.getQtdEnviada()) - Integer.parseInt(relatorioImp.getQtdRetornada());
			   strqtdpend =  String.valueOf(qtdPend)	   ;
			  
					try {  
					 	   valor1 = relatorioImp.getQtdEnviada();
					 	   if( (valor1 != null)||(valor1.equals("")) )  {						   		
						   		valorFormatado1 = formato.format(Long.parseLong(valor1)); 
						   }else valorFormatado1 = "0";				   
						  
					 	   valor2 = relatorioImp.getQtdRetornada();
					 	   if( (valor2 != null)||(valor2.equals("")) )  {						   		
						   		valorFormatado2 = formato.format(Long.parseLong(valor2)); 
						   }else valorFormatado2 = "0";
						   
						   valor3 = strqtdpend;
					 	   if( (valor3 != null)||(valor3.equals("")) )  {						   		
						   		valorFormatado3 = formato.format(Long.parseLong(valor3)); 
						   }else valorFormatado3 = "0";
						   
						   
						   
					}
					catch(Exception e){};		   
			   
			   %>
									   
			  <% if(mesAnterior.equals(mesAtual) == false  && contLinha > 1){	%>		
					
<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand;border-style:none;" width="100%" border="0">						
				<tr>
			      <td height="20" width="466" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Total de Arquivos sem Pend�ncia : <%=quantArqPendZero%></strong>
			      </td>
				  <td width="72"  align="center" bgcolor='#cccccc'     style="line-height:20px;"><%=formato.format(qtdEnvZero)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRetZero)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(qtdPendZero)%></td>	
				</tr>	
				<tr>
			      <td height="1" colspan="4" bgcolor="#000000">   </td>
				
				</tr>
								
				<tr>
			      <td height="20" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Totais&nbsp;<%=Util.mesPorExtenso(mesAnterior)%>&nbsp;-&nbsp;<%=ano%>&nbsp;:&nbsp;&nbsp;</strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'  style="line-height:20px;"><%=formato.format(qtdEnv)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRet)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(pend)%></td>				
				
				</tr>	
				<tr>
			      <td height="1" colspan="4" bgcolor="#000000">   </td>
				
				</tr>	
				
				<tr>
			      <td height="5" colspan="4" bgcolor="#ffffff">   </td>
				
				</tr>			
				
</table>					
<%				
				
				qtdEnv  =0;
	 			qtdRet  =0;
	 			pend    =0;	 		
	 			qtdEnvZero    =0;
	 			qtdRetZero    =0;
	 			qtdPendZero   =0;
	 			quantArqPendZero =0;	
	 			
	 				 		
				
				%>	
<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand;border-style:none;" width="100%" border="0">							
				<tr>
			      <td height="20" colspan="6"  bgcolor='#cccccc'>
			        <strong>&nbsp;&nbsp;<%=Util.mesPorExtenso(mesAtual)%>&nbsp;-&nbsp;<%=ano%></strong>
			      </td>				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>
</table>						
				<%}%>	
							
				<%if (contLinha==1){%>
<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand;border-style:none;" width="100%" border="0">									
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>	
				<tr >
			      <td height="20" colspan="6"  bgcolor='#cccccc'>
			        <strong>&nbsp;&nbsp;<%=Util.mesPorExtenso(mesAtual)%>&nbsp;-&nbsp;<%=ano%></strong>
			      </td>
				
				</tr>	
				<tr>
			      <td height="1" colspan="6" bgcolor="#000000">   </td>
				
				</tr>
</table>							
				<%}%>
				<%				
				qtdEnv = qtdEnv + Long.parseLong(relatorioImp.getQtdEnviada());				
			    qtdRet = qtdRet + Long.parseLong(relatorioImp.getQtdRetornada());	
			    pend = pend + qtdPend;		
			    valorTotGerEnv  		=valorTotGerEnv+Long.parseLong(relatorioImp.getQtdEnviada());
	 			valorTotGerPend  		=valorTotGerPend+qtdPend;
	 			valorTotGerRet  		=valorTotGerRet+Long.parseLong(relatorioImp.getQtdRetornada());	
			    	    
			    
			    
			    if (qtdPend == 0){
			    	qtdEnvZero    = qtdEnvZero+Long.parseLong(relatorioImp.getQtdEnviada());
	 				qtdRetZero    = qtdRetZero+Long.parseLong(relatorioImp.getQtdRetornada());
	 				qtdPendZero   = qtdPendZero + (Long.parseLong(relatorioImp.getQtdEnviada())-Long.parseLong(relatorioImp.getQtdRetornada()));
			    	quantArqPendZero++;  			    	
			    	
			    	try {  
					 	   valor4 = relatorioImp.getQtdEnviada();
					 	   if( (valor4 != null)||(valor4.equals("")) )  {						   		
						   		valorFormatado4 = formato.format(Long.parseLong(valor4)); 
						   }else valorFormatado4 = "0";				   
						  
					 	   valor5 = relatorioImp.getQtdRetornada();
					 	   if( (valor5 != null)||(valor5.equals("")) )  {						   		
						   		valorFormatado5 = formato.format(Long.parseLong(valor5)); 
						   }else valorFormatado5 = "0";
						   
						   valor6 = String.valueOf(qtdPendZero);
					 	   if( (valor6 != null)||(valor6.equals("")) )  {						   		
						   		valorFormatado6 = formato.format(Long.parseLong(valor6)); 
						   }else valorFormatado6 = "0";
						   
						   
						   
					}
					catch(Exception e){};	
			    
			    }
			   %>
		
			<% if (!"0".equals(valorFormatado3) ){	seq++;%>
<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand;border-style:none;" width="100%" border="0">				
			<tr bgcolor="#deebc2" onmouseover="javascript:changeColorOver(this);" onmouseout="javascript:changeColorOut(this);" onclick="setaCodArquivo('detalhe','<%=relatorioImp.getCodArquivo()%>',this.form);"> 
				<td width="40"  align="center"    style="line-height:20px;"><%= seq %></TD>		 		 	  
				<td width="330" align="center"    style="line-height:20px;"><%= relatorioImp.getNomArquivo()%></td>
				<td width="94"  align="center"    style="line-height:20px;"><b><%= relatorioImp.getDatRecebimento()%></b></TD>	
				<td width="72"  align="center"    style="line-height:20px;"><%=valorFormatado1%></TD>					
				<td width="72"  align="center"    style="line-height:20px;"><%= valorFormatado2%></td>				
				<td               align="center"    style="line-height:20px;"><%=valorFormatado3%></td>				
				<input name="codArquivo" type="hidden" value='<%=relatorioImp.getCodArquivo()%>'>	
			</tr>		
			
			<tr><td height="1" bgcolor="#000000" colspan="6"></td></tr>
</table>
	<%}%>  
	  
	  	<% 	  	mesAnterior=mesAtual;				
				  totalPendencia = pend;
				  %>	
	   <% } %>
	   
<table id="LogsImp" cellpadding="0" cellspacing="1" align="center" style="cursor:hand;border-style:none;" width="100%" border="0">		   
		   <tr>
			      <td height="20" width="466" bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Total de Arquivos sem Pend�ncia : <%=quantArqPendZero%></strong>
			       </td>
				  <td width="72"  align="center" bgcolor='#cccccc'     style="line-height:20px;"><%=formato.format(qtdEnvZero)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRetZero)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(qtdPendZero)%></td>	
				</tr>	
				<tr>
			      <td height="1" colspan="4" bgcolor="#000000">   </td>
				
				</tr>
								
				<tr>
			      <td height="20"  bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Totais&nbsp;<%=Util.mesPorExtenso(mesAnterior)%>&nbsp;-&nbsp;<%=ano%>&nbsp;:&nbsp;&nbsp;</strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'  style="line-height:20px;"><%=formato.format(qtdEnv)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(qtdRet)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(pend)%></td>				
				
				</tr>					
				<tr>
			      <td height="5" colspan="4" bgcolor="#ffffff">   </td>
				
				</tr>		
				<tr>
			      <td height="20"  bgcolor='#cccccc'>
			       <strong>&nbsp;&nbsp;Total Geral:&nbsp;&nbsp;:&nbsp;&nbsp;</strong>
			      </td>
			      <td width="72"  align="center" bgcolor='#cccccc'  style="line-height:20px;"><%=formato.format(valorTotGerEnv)%></td>					
				  <td width="72"  align="center" bgcolor='#cccccc'   style="line-height:20px;"><%= formato.format(valorTotGerRet)%></td>				
				  <td               align="center" bgcolor='#cccccc'   style="line-height:20px;"><%=formato.format(valorTotGerPend)%></td>				
				
				</tr>						
 
</table>     	 
<%} else { 
	String msg=(String)request.getAttribute("msg"); if (msg==null) msg="";
%>	
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			 
<%}%>
</div>

</div>

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->
</form>
</body>
</html>