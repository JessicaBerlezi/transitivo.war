<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Vector" %>
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Consulta -->
<jsp:useBean id="ConsultaDownloadBeanId" scope="request" class="REG.ConsultaDownloadBean" /> 
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm,indice) {
	 switch (opcao) {    
	   case 'R':
	    	close() ;
			break;
	 	case 'buscaDownloads':

		if (veCampos(fForm)==true) {
				fForm.acao.value=opcao;
			   	fForm.target= "_self";
			    fForm.action = "acessoTool";  
			   	fForm.submit();	 
		 	}
		  break;
      case 'ImprimeDownload':
			fForm.acao.value=opcao;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;	
		
		 case 'ReimprimirProtocolo':
			fForm.acao.value=opcao;	
			if (indice >= 0 && fForm.codArquivoRec.length >1) {
			fForm.guardaArquivoRec.value=fForm.codArquivoRec[indice].value;		
			}else{
			fForm.guardaArquivoRec.value=fForm.codArquivoRec.value;
			}
			fForm.target= "_blank";
			fForm.action = "acessoTool";  
			fForm.submit();	   		
			break;
		
		 case 'DetalheArquivo':
			document.all["acao"].value=opcao;
			document.all["frmDownload"].target= "_blank";
			document.all["frmDownload"].action = "acessoTool";  
			document.all["frmDownload"].submit();	
			break;		
			
				  		
		case 'retornar':
	    	retornar(fForm) ;
			break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function validaDetalhe(codArquivo,nmArquivo,fForm) {
	document.all["acao"].value="DetalheArquivo";
	document.all["codArquivoRec"].value=codArquivo;
	document.all["nmArquivo"].value=nmArquivo;
	document.all["frmDownload"].target= "_blank";
	document.all["frmDownload"].action = "acessoTool";  
	document.all["frmDownload"].submit();
}

function veCampos(fForm) {
	valid = true ;
	sErro = "" ;
	

	// validar as datas
	if(document.all["De"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'De' � obrigat�rio";
	}
	if(document.all["Ate"].value==''){
		valid=false;
		sErro += "O preenchimento do campo : Data 'At�' � obrigat�rio";
	}

	return valid;
}


<%
 	String arrIdentArquivos[][] = (String[][])request.getAttribute("arrIdentArquivos");
%>
myIdent = new Array ();
<% 	
 	for (int i=0;i<arrIdentArquivos.length;i++) {
 		%> 		
 		myIdent[<%=i%>]= new Array("<%=arrIdentArquivos[i][0]%>","<%=arrIdentArquivos[i][1]%>");
 	<%}
%>


myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

</script>
</head>

<%
 	Vector vetArquivos = (Vector) ConsultaDownloadBeanId.getVetArquivos(); 

%>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="frmDownload" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />

<input name="acao"             type="hidden" value=' '>
<input name="codArquivo"       type="hidden" value=' '>
<input name="extensao"         type="hidden" value=' '>
<input name="guardaArquivoRec" type="hidden" value=' '>
<input name="codArquivoRec" 	type="hidden" value="">
<input name="nmArquivo" 		type="hidden" value="">	  

<!--IN�CIO_CORPO_sistema-->

<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 45px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td  width="27%"><b>&Oacute;rg&atilde;o Atua&ccedil;&atilde;o:&nbsp;</b>
					<select name="codOrgao"> 
					<script>
						var sel="";
						if ("-1"=="<%=ConsultaDownloadBeanId.getCodOrgao()%>")
							sel=" selected";
						else
							sel="";
						document.write("<option value=-1" + sel + "><%=UsuarioBeanId.getSigOrgaoAtuacao()%></option>");
						for (j=0;j<cod_orgao.length;j++) {
							if (cod_orgao[j]=="<%=ConsultaDownloadBeanId.getCodOrgao()%>")
								sel=" selected";
							else
								sel="";
							document.write ("<option value="+cod_orgao[j]+ sel + ">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select>
			  </td>
			  <td width="27%" align="left"><b>Arquivo:&nbsp;</b>
				<select name="codIdentArquivo"> 
					<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						var sel="";
						for (j=0;j<myIdent.length;j++) {
							if (myIdent[j][0]=="<%=ConsultaDownloadBeanId.getCodIdentArquivo()%>")
								sel=" selected";
							else
								sel="";
							document.write ( "<option value=" + myIdent[j][0]+  sel + ">" + myIdent[j][1]) + "</option>"
						}
					</script>
				</select>
			  </td>
			  <td width="36%"><b>Data de Envio:&nbsp;</b>&nbsp;<input name="De" type="text" id="De" size="12" maxlength="12" value="<%=ConsultaDownloadBeanId.getDataIni()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  &nbsp;at&eacute;&nbsp;<input name="Ate" type="text" id="Ate" size="12" maxlength="12" value="<%=ConsultaDownloadBeanId.getDataFim()%>" onkeypress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onfocus="javascript:this.select();">
			  </td>		  
	          <td align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaDownloads',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
					<button type="button" NAME="Imprimir"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimeDownload',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left" >
					</button>
			  </td>          
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:50px; top:120px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr> 
        <td width="10" bgcolor="#FFFFFF"  style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Recebido<br>&nbsp;em processamento</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFB0B0" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Rejeitado</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#C6EAB5"style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Processado</td>
        <td ><img src="images/inv.gif" width=170 height=2></td>
        <td width="10" bgcolor="#FFFFD5" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;efetuado</td>
        <td width="5">&nbsp;</td>
        <td width="10" bgcolor="#FFFFFF" style="border: 1px solid #000000;">&nbsp;</td>
        <td>&nbsp;Download<br>&nbsp;n&atilde;o efetuado</td>
      </tr>
    </table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:45px; top:158px; width:740px; z-index:5; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="1" class="table">
      <tr bgcolor="#deebc2"> 
        <td width="225" height="15"  align="center" bgcolor="#deebc2"><strong>Arquivo Recebido (Arq.Origem)</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>em</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>Processado</strong></td>
        <td width="100" height="15"  align="center" bgcolor="#deebc2"><strong>Gravado</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>Download</strong></td>
        <td width="100" height="15"  align="center"><strong>Rejeitado</strong></td>
        <td width="60" bgcolor="#EDF4DF" align="center"><strong>Download</strong></td>
        <td bgcolor="#deebc2" align="center"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong></td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
  <div id="div2" style="position:absolute; left:45px; top:174px; width:740px; height:162px; z-index:6; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
         
     <%
    	int tam=vetArquivos.size();
    	String nomArquivo, nomArquivoGrav, nomArquivoRej, status, codArquivo, datGrav, datRej;
    	String corArq, corGrav, corRej;
    	
    	int qtdReg, qtdLinhaErro;
    	
      	for(int i=0;i<tam;i++) {
      		REG.ArquivoRecebidoBean ArqRecBean = new REG.ArquivoRecebidoBean();
      		ArqRecBean=(REG.ArquivoRecebidoBean)vetArquivos.get(i);
      		nomArquivo=ArqRecBean.getNomArquivo();
      		codArquivo=ArqRecBean.getCodArquivo();
      		status=ArqRecBean.getCodStatus();
      		try {
      			qtdReg=Integer.parseInt(ArqRecBean.getQtdRegRecebido());
      		}
      		catch (Exception e) {
      			qtdReg=0;
      		}
      		try {
      			qtdLinhaErro=Integer.parseInt(ArqRecBean.getQtdLinhaErro());
      		}
      		catch (Exception e) {
      			qtdLinhaErro=0;
      		}
      		
      		if (status.equals("9")) {
      			corArq="#FFB0B0";
      		}
      		else if (status.equals("8") || status.equals("7") || status.equals("1")) {
      			corArq="#FFFFD5";
      		}
      		else if (status.equals("2")) {
      			corArq="#C6EAB5";
      		}
      		else {
      			corArq="#FFFFFF";
      		}
      		
      		
      		nomArquivoGrav="";
      		nomArquivoRej="";
      		datGrav="";
      		datRej="";
      		corGrav="#FFFFFF";
      		corRej="#FFFFFF";
      		
      		if (status.equals("2")) {
      			if (qtdReg>qtdLinhaErro && !ArqRecBean.getSufixoRetornoGrv().equals("") ) {
      				nomArquivoGrav=ArqRecBean.getCodIdentRetornoGrv() + "..." + " (" + (qtdReg-qtdLinhaErro) + " Reg)";
      				if (ArqRecBean.getDatDownloadGravado()!=null && !ArqRecBean.getDatDownloadGravado().equals("")) {
	      				datGrav=ArqRecBean.getDatDownloadGravado();
      					corGrav="#FFFFD5";
      				}
      			}
      			if (qtdLinhaErro>0) {
      				nomArquivoRej=ArqRecBean.getCodIdentRetornoRej() + "..." + " (" + qtdLinhaErro + " Reg)";
      				if (ArqRecBean.getDatDownloadErro()!=null && !ArqRecBean.getDatDownloadErro().equals("")) {
	      				datRej=ArqRecBean.getDatDownloadErro();
      					corRej="#FFFFD5";
      				}
      			}
      		}
      %>
      <tr > 
        <td width="225" height="15" bgcolor="<%=corArq%>"><%=nomArquivo + " (" + qtdReg + " Reg)"%></td>
        <td width="60" align="center"><%=ArqRecBean.getDatRecebimento()%></td>
        <td width="60" align="center"><%=ArqRecBean.getDatProcessamentoDetran()%></td>
        <td width="100" height="15" bgcolor="<%=corGrav%>"><a href="#" onclick="javascript: validaDetalhe('<%=codArquivo%>','<%=ArqRecBean.getNomArquivo()%>',this.form);"><%=nomArquivoGrav%></a>
        <td width="60" align="center"><%=datGrav%></td>
        <td width="100" height="15" bgcolor="<%=corRej%>"><a href="#" onclick="javascript: validaDetalhe('<%=codArquivo%>','<%=ArqRecBean.getNomArquivo()%>',this.form);"><%=nomArquivoRej%></a>
        <td width="60"  height="15" bgcolor="<%=corRej%>"><%=datRej%></td>
        <td  align="center">
        	<button type="button" NAME="Imprimir"   style="width: 28px;height: 21px; cursor:hand ; border: none; background: transparent;"  onClick="javascript: valida('ReimprimirProtocolo',this.form,<%=i%>);">	
				<IMG src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left" >
			</button>			
		</td>
      </tr>
      <tr> 
        <td height="1" colspan="10"  bgcolor="#cccccc"></td>
      </tr>
      
      <%} %>
    </table>
</div>
<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.png" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->


<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%=ConsultaDownloadBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>