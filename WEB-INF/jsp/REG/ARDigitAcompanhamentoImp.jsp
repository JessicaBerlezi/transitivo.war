<%@ page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="path" value="${pageContext.request.contextPath}"/>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>
<title>:: TRANSITIVO ::</title>
<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
	div.quebrapagina{page-break-after: always}.style1 {color: #FFFFFF}
</style>

<script language="JavaScript1.2" src="${path}/js/itcutil.js" type='text/javascript'></script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<!-- Inicio Div Detalhes variáveis impressao -->
<c:set var="contLinha" value="0"/>
<c:set var="npag" value="0"/>
<c:set var="seq" value="0"/>

<c:forEach items="${ARDigitBeanId.beans}" var="relatorioImp">
	<c:set var="seq" value="${seq+1}"/>
	<c:set var="contLinha" value="${contLinha+1}"/>	
	<c:if test="${contLinha%41==1}">
		<c:set var="npag" value="${npag+1}"/>
		<c:if test="${npag!=1}">
			<jsp:include page="Rod_impConsulta.jsp" flush="true" />
			<div class="quebrapagina"></div>
		</c:if>
		<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "${npag}" />
		</jsp:include>
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
			<tr>
				 <td width="5%"  align="center" bgcolor="#999999" ><font color="#000000"><b>SEQ</b></font></td>
				 <td width="11%" align="center" bgcolor="#999999" ><font color="#000000"><b>DATA</b></font></td>		 
				 <td width="12%" align="center" bgcolor="#999999" ><font color="#000000"><b>EMITIDOS</b></font></td>					 
				 <td width="12%" align="center" bgcolor="#999999" ><font color="#000000"><b>RETORNADOS</b></font></td>
				 <td width="12%" align="center" bgcolor="#999999" ><font color="#000000"><b>DIGITALIZADOS</b></font></td>
				 <td width="12%" align="center" bgcolor="#999999" ><font color="#000000"><b>SALDO</b></font></td>
				 <td width="20%" align="center" bgcolor="#999999" ><font color="#000000"><b>TEMPO M&Eacute;DIO (DIAS)</b></font></td>
				 <td align="center" bgcolor="#999999" ><font color="#000000"><b>PENDENTE</b></font></td>
			</tr>
		</table>
	<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" align="center">		 
	</c:if>   
		<tr> 
			<td width="5%"  align="center" style="line-height:20px;">${seq}</TD>		 		 	  
			<td width="11%" align="center" style="line-height:20px;"><fmt:formatDate value="${relatorioImp.datProc}" pattern="dd/MM/yyyy"/></td>
			<td width="12%" align="right"  style="line-height:20px; padding-right: 10px"><fmt:formatNumber value="${relatorioImp.qtdRecebido}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right"  style="line-height:20px; padding-right: 10px"><fmt:formatNumber value="${relatorioImp.qtdEntregue}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right"  style="line-height:20px; padding-right: 10px"><fmt:formatNumber value="${relatorioImp.qtdRecebido-relatorioImp.qtdEntregue}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right"  style="line-height:20px; padding-right: 10px"><fmt:formatNumber value="${relatorioImp.qtdDigitalizado}" groupingUsed="true"/>&nbsp;&nbsp;</td>
            <td width="20%" align="right"  style="line-height:20px; padding-right: 10px">${relatorioImp.tempoMedio}&nbsp;&nbsp;</td>
			<td align="right"  style="line-height:20px; padding-right: 10px"><fmt:formatNumber value="${relatorioImp.qtdPendentes}" groupingUsed="true"/>&nbsp;&nbsp;</td>
		</tr>		
		<tr><td height=1 bgcolor="#000000" colspan=10></td></tr>
</c:forEach>
	</table>

	<table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" >
		<tr>
			<td width="5%"  align="center"  style="line-height:40px;font-weight: bold" >Total:</td>
			<td width="11%" align="center"></td>
			<td width="12%" align="right" style="font-weight: bold; padding-right: 10px"><fmt:formatNumber value="${ARDigitBeanId.totalQtdRecebido}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right" style="font-weight: bold; padding-right: 10px"><fmt:formatNumber value="${ARDigitBeanId.totalQtdEntregue}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right" style="font-weight: bold; padding-right: 10px"><fmt:formatNumber value="${ARDigitBeanId.totalQtdRecebido-ARDigitBeanId.totalQtdEntregue}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="12%" align="right" style="font-weight: bold; padding-right: 10px"><fmt:formatNumber value="${ARDigitBeanId.totalQtdDigitalizado}" groupingUsed="true"/>&nbsp;&nbsp;</td>
			<td width="20%" align="right" style="font-weight: bold ;padding-right: 10px">${ARDigitBeanId.totalTempoMedio}&nbsp;&nbsp;</td>
			<td align="right" style="font-weight: bold; padding-right: 10px"><fmt:formatNumber value="${ARDigitBeanId.totalQtdPendentes}" groupingUsed="true"/>&nbsp;&nbsp;</td>
		</tr>
	</table>

	<jsp:include page="Rod_impConsulta.jsp" flush="true" />

</body>
</html>