<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="transacaoBean"     scope="request" class="REG.ConsultaBrokerTrans044Bean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>DETRAN &#8226;Sistema de Monitoramento de Infra&ccedil;&otilde;es de Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
function valida(opcao,fForm) {
	 switch (opcao) {    
	   case 'R':
			fForm.acao.value=opcao;
			fForm.target= "_self";
			fForm.action = "acessoTool";  
			fForm.submit();	 
	   		close();
			break;

     case 'retorna':
        fForm.verTable.value="N"	  
    	fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
        break;   
          
	  case 'N':
		  fForm.acao.value="Novo";
		  fForm.target= "_self";
		  fForm.action = "acessoTool";  
		  fForm.submit();	  		  
		  break ;		  
  	  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
  }
}

function changeColorOver(obj) {	
	obj.style.background="#EFF5E2";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="#DEEBC2";
	obj.style.textDecoration="none";
}

</script>
<style type="text/css">
<!--
.style2 {color: #FFFFFF}
-->
</style>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="ConsultaAuto" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=''>

 <div id="dadosTelaAntes" style="position:absolute; left:50px; top:75px; width:722px; height:48px; z-index:13; overflow: visible; visibility: visible;">  
   <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr   height="20" bgcolor="#EFF5E2">
       <td width="8%" > &nbsp;<strong>Dat Proc :</strong></td>
       <td width="23%"><input readonly name="datProc" type="text"  value="<%=transacaoBean.getDatProc() %>" size="25" maxlength="1"
        style="border: 0px none; background-color:transparent;">
       </td>
	   <td width="12%" >&nbsp;<strong>Transa��o :</strong></td>
       <td width="57%"><input readonly name="codTransacao" type="text"  value="<%=transacaoBean.getCodTransacaoSel() %>" size="6" maxlength="3" 
         style="border: 0px none; background-color:transparent;">
         -&nbsp;&nbsp;
         <input name="dscTransacao" type="text"  value="<%=transacaoBean.getDscTransacaoSel() %>" size="58" maxlength="50" 
         style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
 </table>
</div>	 
<div id="parteFixaTitulo" style="position:absolute; left:50px; top:100px; width:356px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" bgcolor="#a8b980"><strong class="espaco2"><font color="#ffffff">&nbsp;&nbsp;&nbsp;PARTE FIXA</font></strong></td>
    </tr>
 </table>
</div>
<div id="parteFixa" style="position:absolute; left:49px; top:120px; width:357px; height:40px; z-index:13; overflow: auto; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
     <tr height="20" bgcolor="#EFF5E2">
       <td width="172" height="10"><strong>&nbsp;&nbsp;Seq Transa��o </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
         <input  readonly name="seqTransacao" type="text" value="<%=transacaoBean.getSeqTransacao()%>" size="10" maxlength="6" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;C�digo Transa��o </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
        <input readonly name="codTransacao" type="text" value="<%=transacaoBean.getCodTransacao()%>" size="6" maxlength="3" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;Usuario/Origem/tamanho </strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;<input readonly name="usrOrigTam" type="text"  value="<%=transacaoBean.getUsrOrigTam()%>" size="30" maxlength="25"
			style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="171" height="10"><strong>&nbsp;&nbsp;Dia Juliano</strong></td>
       <td width="182" height="10"><strong>:</strong>&nbsp;
        <input readonly name="diaJuliano" type="text"  value="<%=transacaoBean.getDiaJuliano()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td  width="171"></td>
       <td  width="182"></td>
     </tr>
 </table>
</div>
<div id="parteVariavelTitulo" style="position:absolute; left:415px; top:99px; width:356px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" bgcolor="#a8b980"><strong class="espaco2 style2">&nbsp;&nbsp;&nbsp;PARTE VARI�VEL</strong></td>
    </tr>
 </table>
</div>
<div id="parteVariavel" style="position:absolute; left:415px; top:120px; width:356px; height:40px; z-index:13; overflow: auto; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
   <tr height="20" bgcolor="#EFF5E2">
       <td width="11%" height="10"><strong>&nbsp;&nbsp;Num Auto </strong></td>
       <td width="21%" height="10"><strong>:</strong>&nbsp;
         <input  readonly name="numAuto" type="text" value="<%=transacaoBean.getNumAuto()%>" size="20" maxlength="12" 
		    style="border: 0px none; background-color:transparent;"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="15%" height="10"><strong>&nbsp;&nbsp;Ind. Continuidade </strong></td>
       <td width="26%" height="10"><strong>:</strong>&nbsp;
        <input readonly name="indCont" type="text" value="<%=transacaoBean.getIndCont()%>" size="20" maxlength="15" 
		    style="border: 0px none; background-color:transparent;">       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
      <td width="13%"></td>
       <td width="14%"></td>
    </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="11%"></td>
       <td width="21%"></td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="20" bgcolor="#EFF5E2">
       <td width="15%"></td>
       <td width="26%"></td>
     </tr>
   </table>
 </div>
<div id="resultadoTitulo" style="position:absolute; left:50px; top:165px; width:722px; height:21px; z-index:13; overflow: visible; visibility: visible;">  
 <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
    <tr height="20" >
       <td width="7%" bgcolor="#a8b980"><strong class="espaco2 style2">&nbsp;&nbsp;&nbsp;RESULTADO</strong></td>
    </tr>
 </table>
</div>

<div id="resultado" style="position:absolute; left:50px; top:184px; width:722px; height:160px; z-index:13; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="0" class="table">
   <tr height="10" bgcolor="#EFF5E2">
       <td width="172"><strong>&nbsp;C�digo Retorno </strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input  readonly name="codRetorno" type="text" value="<%=transacaoBean.getCodRetorno()%>" size="6" maxlength="3" 
		    style="border: 0px none; background-color:transparent;">
       </td>
   </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>&nbsp;Num Auto </strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="numAuto" type="text" value="<%=transacaoBean.getnumAutoResult()%>" size="24" maxlength="12" 
		    style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
      <td width="170"><strong>Num Processo</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="numProcesso" type="text"  value="<%=transacaoBean.getNumProcesso()%>" size="30" maxlength="20"
			style="border: 0px none; background-color:transparent;">
       </td>
    </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>C�digo Status</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="codStatus" type="text"  value="<%=transacaoBean.getCodStatus()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Data Status</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="dataStatus" type="text"  value="<%=transacaoBean.getDatStatus()%>" size="16" maxlength="8"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="10" bgcolor="#EFF5E2">       
       <td width="170"><strong>C�digo Evento</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="codEvento" type="text"  value="<%=transacaoBean.getCodEvento()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Origem Evento</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="origEvento" type="text"  value="<%=transacaoBean.getOrigemEvento()%>" size="6" maxlength="3"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="10" bgcolor="#EFF5E2">        
       <td width="170"><strong>UserName</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="userName" type="text"  value="<%=transacaoBean.getUserName()%>" size="30" maxlength="20"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>C�digo �rg�o Lota��o</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="codOrgLot" type="text"  value="<%=transacaoBean.getCodOrgaoLotacao()%>" size="12" maxlength="6"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="20" bgcolor="#EFF5E2">
       <td width="170"><strong>Data Processamento</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="datProc" type="text"  value="<%=transacaoBean.getDataProc()%>" size="16" maxlength="8"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento1</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento1" type="text"  value="<%=transacaoBean.getComplemento1()%>" size="40" maxlength="30"
			style="border: 0px none; background-color:transparent;">
       </td>
      </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="10" bgcolor="#EFF5E2"> 
       <td width="170"><strong>Complemento2</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento2" type="text"  value="<%=transacaoBean.getComplemento2()%>" size="55" maxlength="45"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento3</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento3" type="text"  value="<%=transacaoBean.getComplemento3()%>" size="60" maxlength="50"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170" valign="top"><strong>Complemento4</strong></td>
       <td width="76%">&nbsp;&nbsp;&nbsp;
         <textarea readonly name="complemento4"  cols="80" rows="12" "dir="ltr" lang="pt"><%=transacaoBean.getComplemento4()%></textarea></td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento5</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento5" type="text"  value="<%=transacaoBean.getComplemento5()%>" size="40" maxlength="30"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="10" bgcolor="#EFF5E2">        
       <td width="170"><strong>Complemento6</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento6" type="text"  value="<%=transacaoBean.getComplemento6()%>" size="60" maxlength="50"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento7</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento7" type="text"  value="<%=transacaoBean.getComplemento7()%>" size="60" maxlength="50"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento8</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento8" type="text"  value="<%=transacaoBean.getComplemento8()%>" size="60" maxlength="50"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento9</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento9" type="text"  value="<%=transacaoBean.getComplemento9()%>" size="22" maxlength="12"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>  
     <tr height="10" bgcolor="#EFF5E2"> 
       <td width="170"><strong>Complemento10</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento10" type="text"  value="<%=transacaoBean.getComplemento10()%>" size="4" maxlength="2"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>     
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Complemento11</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="complemento11" type="text"  value="<%=transacaoBean.getComplemento11()%>" size="40" maxlength="30"
			style="border: 0px none; background-color:transparent;">
       </td>
     </tr>
     <tr>
       <td colspan=6 height="2"></td>
     </tr>
     <tr height="10" bgcolor="#EFF5E2">
       <td width="170"><strong>Ind. Continuidade</strong></td>
       <td width="76%"><strong>:</strong>&nbsp;&nbsp;&nbsp;<input readonly name="indContResult" type="text"  value="<%=transacaoBean.getIndContResult()%>" size="25" maxlength="15"
			style="border: 0px none; background-color:transparent;">
       </td>
  </tr>
  </table>
</div>
 <!--FIM_CORPO_sistema--> 

<!-- Rodap�-->
<!--Bot�o retornar--> 
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= transacaoBean.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>