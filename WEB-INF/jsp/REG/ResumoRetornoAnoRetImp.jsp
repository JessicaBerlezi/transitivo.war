<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<jsp:useBean id="ResumoRetornoBeanId" scope="session" class="REG.ResumoRetornoBean"/> 

<html>   
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.relat {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 text-align: right;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #000000;
	 border-bottom: 1px solid #000000;
	 border-left: 0px solid #999999;
     word-spacing: 0px;
	 line-height: 23px;
}
</style>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ResumoRetornoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
int npag       = 1;
String tituloConsulta = (String)request.getAttribute("tituloConsulta");
if(tituloConsulta==null) tituloConsulta=""; 
%>
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
		  

<!--DADOS-->
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr> 
      <td width="20%"  height="13" align="right" bgcolor="#999999" class="relat" style="border-top: 1px solid #000000;"><strong>Quantidade/M&ecirc;s</strong> &nbsp; </td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JAN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">FEV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">MAR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">ABR</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">MAI</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JUN</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JUL</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">AGO</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">SET</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">OUT</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">NOV</td>
      <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">DEZ</td>
      <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 6px; border-right: 0px none; border-top: 1px solid #000000;">T o t a l</td>
    </tr>
    <tr> 
      <td align="right" class="relat"><strong>Retornada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%></td>
    </tr>
  </table>

<!--FIM DADOS-->				  

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
    <tr>
	  <td width="20%" bgcolor="#999999" class="relat" style="border-right: 0px none;"><strong>&nbsp;C&Oacute;DIGOS DE RETORNO</strong> &nbsp;</td>
      <td colspan="14" bgcolor="#999999" class="relat" style="text-align: left; border-right: 0px none;">&nbsp;</td>
    </tr>
</table>



<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">


    <%for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) {
    if (ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada().equals("0")) continue;    	      	
    %>
    <tr> 
    <td width="20%" align="right" class="relat"><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getDescRetorno()%>&nbsp;<strong><br>
    <%=(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N")%>&nbsp;</strong></td>
      <%for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++) {
      %>
      <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada())%>&nbsp;<br /><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getPercRetornada()%>%&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())%>&nbsp;<br/><%=ResumoRetornoBeanId.getCodigoRetornos(i).getPercRetornada()%>%&nbsp;</td>
    </tr>
    <%
    }
    %>
  </table>
  
  
  
  
  
  <!--recuperados-->

  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda" style="margin-top: 20px">
    <tr> 
     <td width="20%"  height="13" align="right" bgcolor="#999999" class="relat" style="border-top: 1px solid #000000;"><strong>Resumo/M&ecirc;s</strong> &nbsp; </td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JAN</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">FEV</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">MAR</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">ABR</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">MAI</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JUN</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">JUL</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">AGO</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">SET</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">OUT</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">NOV</td>
     <td width="6%" valign="bottom" bgcolor="#999999" class="fontmedia relat" style="text-align: center; font-weight:bold; border-top: 1px solid #000000;">DEZ</td>
     <td align="center" bgcolor="#999999" class="fontmedia relat" style="font-weight:bold;padding-right: 6px; border-right: 0px none; border-top: 1px solid #000000;">T o t a l</td>
    </tr>
  </table>


  
  <table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
<!--LINHA "ENVIADA"-->
   <tr>
      <td  width="20%" height="13" align="right" class="relat"><strong>
      RETORNADAS </strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%></td>
    </tr>
   <tr>
      <td  width="20%" height="13" align="right" class="relat"><strong>CR&Eacute;DITO </strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdCredito())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdCredito())%></td>
    </tr>
	
    <tr>
      <td  width="20%" height="13" align="right" class="relat"><strong>RECUPERADO P/ ENTREGA</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoEntrega())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdRecuperadoEntrega())%></td>
    </tr>
  
    <tr>
      <td  width="20%" height="13" align="right" class="relat"><strong>RECUPERADO P/ SERVI&Ccedil;O</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoSistema())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdRecuperadoSistema())%></td>
    </tr>
	
	<tr>
      <td  width="20%" height="13" align="right" class="relat"><strong>TOTAL</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="6%" class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdFaturamento())%>&nbsp;</td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdFaturamento())%></td>
    </tr>
  </table>  
 <!--FIM recuperados--> 
  
  <jsp:include page="Rod_impConsulta.jsp" flush="true" />


</form>
</body>
</html>