<!--M�dulo : REC
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>

<%@ page errorPage="ErrorPage.jsp" %>


<jsp:useBean id="ResumoRetornoBeanId"     scope="session" class="REG.ResumoRetornoBean"/> 

<html>
<head>
<%@ include file="Css.jsp" %>
<style type="text/css">
.borda td{border: 1px solid #ff0000;}

.style1 {
	color: #FFFFFF;
	font-weight: bold;
	text-align: center;}
.relat {
	 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	 font-size: 9px;
	 text-align: right;
	 border-top: 0px solid #999999;
	 border-right: 1px solid #e7e7e7;
	 border-bottom: 1px solid #e7e7e7;
	 border-left: 0px solid #999999;
     word-spacing: 0px;
     background-color: #ffffff;

}
</style>

<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 


function valida(opcao,fForm, numRetorno, mes, indEnt, dscResumo) {
 switch (opcao) {
   case 'imprimirAno':
		fForm.acao.value=opcao;
   		fForm.target= "_self";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	    break ;
   case 'detalhe':
        if (indEnt == true){
		  document.all.ResumoRetornoAno.indRet.value="S";
        }else{
		  document.all.ResumoRetornoAno.indRet.value="N";
        }
		
		document.all.ResumoRetornoAno.acao.value=opcao;
		document.all.ResumoRetornoAno.numRetorno.value=numRetorno;
		document.all.ResumoRetornoAno.mes.value=mes;
		document.all.ResumoRetornoAno.dscResumo.value=dscResumo;
   		document.all.ResumoRetornoAno.target= "_blank 	";
    	document.all.ResumoRetornoAno.action = "acessoTool";  
   		document.all.ResumoRetornoAno.submit();	  		  
	    break ;
   case 'detalheResumo':
		document.all.ResumoRetornoAno.acao.value=opcao;
		document.all.ResumoRetornoAno.numRetorno.value=numRetorno;
		document.all.ResumoRetornoAno.mes.value=mes;
		document.all.ResumoRetornoAno.dscResumo.value=dscResumo;
   		document.all.ResumoRetornoAno.target= "_blank 	";
    	document.all.ResumoRetornoAno.action = "acessoTool";  
   		document.all.ResumoRetornoAno.submit();	  		  
	    break ;
   case 'V':
		fForm.acao.value=opcao;
   		fForm.target= "_self";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	    break ;
   case 'R':
        close();
	    break ;
	    
  }
}
</script>

</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="ResumoRetornoAno" method="post" action="" >
<%@ include file="Cab_Diretiva.jsp" %>
<input name="acao" type="hidden" value="">	
<input name="numRetorno" type="hidden" value="">	
<input name="mes" type="hidden" value="">	
<input name="numAno" type="hidden" value="<%=ResumoRetornoBeanId.getAno()%>">	
<input name="indRet" type="hidden" value="">	
<input name="dscResumo" type="hidden" value="">	



<div id="WK_SISTEMA" style="position:absolute; width:475px; overflow: visible; z-index: 1; top: 76px; left:200px;" >  
  <table width="100%" cellpadding="0" cellspacing="1">
	<tr>
       <td><strong>Ano :</strong></td>
        <td align="left">&nbsp;<%=ResumoRetornoBeanId.getAno()%></td>
        <td>&nbsp;&nbsp;<strong>Posi��o em :</strong>&nbsp;<%=ResumoRetornoBeanId.getDatReferencia()%></td>
        <td style="text-align: right">
        <button type="button" name="imprimir" style="width: 56px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('imprimirAno',this.form);"> <img src="<%= path %>/images/bot_imprimir_ico.gif" align="left" ></button>
        <button type="button" name="retornar" style="width: 56px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('V',this.form);"> <img src="<%= path %>/images/bot_retornar_ico.gif" align="left" ></button>
        </td>
    </tr>
    <!-- Verifica se est� na etapa de intera��o para sele��o dos arquivos pendentes -->
  </table>
</div>

		
<!--DADOS-->
<div id="relat1" style="position:absolute; left:35px; width:745px; top:105px; height:55px; z-index:1; overflow:visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
    <tr> 
      <td width="94"  height="13" align="right" bgcolor="#a8b980" style="color:#ffffff; font-weight: bold">Quant./M�s &nbsp;</td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia style1">JAN</td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">FEV</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">MAR</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">ABR</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">MAI</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">JUN</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">JUL</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">AGO</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">SET</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">OUT</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">NOV</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">DEZ</div></td>
      <td width="72" align="center" bgcolor="#a8b980" class="fontmedia"><span class="style1">T 
        o t a l</span></td>
    </tr>
    <tr> 
      <td align="right" bgcolor="#edf4df"><strong>Retornada</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td width="46" class="relat">
        <%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%>
        </td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%></td>
    </tr>
	</table>
</div>
<!--FIM DADOS-->
<div id="titCodRetorno" style="position:absolute; left:35px; width:745px; top:136px; height:18px; z-index:1; overflow: visible; visibility: visible;"> 
<table width="100%" border="0" cellpadding="0" cellspacing="1">
    <tr>
      <td colspan="14" height="18" bgcolor="#deebc2" style="letter-spacing:1px;"><strong>&nbsp;&nbsp;C&Oacute;DIGOS DE RETORNO</strong></td>
      </tr>
</table>
</div>

<!--c�digos de retorno-->
<div id="relat2" style="position:absolute; left:35px; width:745px; top:155px; height:92px; z-index:1; overflow: auto; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
    
    <%
    String dscCodRetorno = "";     
    for (int i=0;i<ResumoRetornoBeanId.getListCodigoRetornoMes().size();i++) {
      if (ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada().equals("0")) continue;
    %>
    <tr> 
      <td width="94" align="right" bgcolor="#edf4df"><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getDescRetorno()%>&nbsp;<strong><br>
      <%=(ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue()?"S":"N")%>&nbsp;</strong></td>
      <%for (int j=0;j<ResumoRetornoBeanId.getCodigoRetornos(i).getListRetornoMes().size();j++) {
      %> 
      <td width="46" class="relat">
      <a href="#"  onClick="javascript: valida('detalhe',this.form,<%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getNumRetorno()%>,<%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getMes() %>, <%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetorno().getIndEntregue() %>);"><font color="#000000"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getQtdTotalRetornada())%></font>
      <br/><%=ResumoRetornoBeanId.getCodigoRetornos(i).getRetornos(j).getPercRetornada()%>%
      </td>
      
      
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 6px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getCodigoRetornos(i).getQtdTotalRetornada())%>&nbsp;<br/><%=ResumoRetornoBeanId.getCodigoRetornos(i).getPercRetornada()%>%&nbsp;</td>
    </tr>
    <%
    }
    %>
    
    <tr bgcolor="#faeae5"> 
      <td colspan="14" align="left" bgcolor="#deebc2"  class="fontmedia">&nbsp;</td>
      </tr>
  </table>
 </div>
<!--fim c�digos de retorno-->

  <!--recuperados-->
<div id="recuperados" style="position:absolute; left:35px; width:745px; top:250px; height:55px; z-index:10; overflow:visible; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="0" cellspacing="1">
    <tr> 
      <td width="94"  height="13" align="right" bgcolor="#a8b980" style="color:#ffffff; font-weight: bold">Resumo &nbsp;</td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia style1">JAN</td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">FEV</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">MAR</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">ABR</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">MAI</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">JUN</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">JUL</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">AGO</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">SET</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">OUT</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">NOV</div></td>
      <td width="46" valign="bottom" bgcolor="#a8b980" class="fontmedia"> <div align="center" class="style1">DEZ</div></td>
      <td width="72" align="center" bgcolor="#a8b980" class="fontmedia"><span class="style1">T 
        o t a l</span></td>
    </tr>
  
<!--LINHA "ENVIADA"-->
   <tr>
      <td height="13" bgcolor="#edf4df" align="right" ><strong>
      Retornadas</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdTotalRetornada())%></td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdTotalRetornada())%></td>
    </tr>
   <tr>
      <td height="13" bgcolor="#edf4df" align="right" ><strong>Cr&eacute;dito</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td class="relat">
            <a href="#"  onClick="javascript: valida('detalhe',this.form,'IN (5,17,19,26)',<%=i+1 %>,'N','CR�DITO');"><font color="#000000"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdCredito())%></font>
        </td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdCredito())%></td>
    </tr>
	
    <tr>
      <td height="13" bgcolor="#edf4df" align="right" ><strong>Recup. p/Entrega</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td class="relat">
           <a href="#"  onClick="javascript: valida('detalheResumo',this.form,'<> 99',<%=i+1 %>,'','RECUPERADO P/ ENTREGA');"><font color="#000000"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoEntrega())%></font>
        </td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdRecuperadoEntrega())%></td>
    </tr>
  
    <tr>
      <td  height="13" bgcolor="#edf4df" align="right"><strong>Recup. p/Servi&ccedil;o</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td class="relat">
           <a href="#"  onClick="javascript: valida('detalheResumo',this.form,'= 99',<%=i+1 %>,'','RECUPERADO P/ SERVI�O');"><font color="#000000"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdRecuperadoSistema())%></font>
        </td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdRecuperadoSistema())%></td>
    </tr>
	
	<tr>
      <td height="13" bgcolor="#edf4df" align="right" ><strong>Total</strong> &nbsp;</td>
      <%for (int i=0;i<ResumoRetornoBeanId.getListRetornoMes().size();i++) {
      %>
        <td class="relat"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getRetornos(i).getQtdFaturamento())%></td>
      <%
      }
      %>
      <td align="center" class="relat" style="padding-right: 13px; border-right: 0px none;"><%=sys.Util.formataIntero(ResumoRetornoBeanId.getQtdFaturamento())%></td>
    </tr>
  </table>  
</div>
 <!--FIM recuperados--> 


<!--Div Erros-->
<%
String msgOk         = "";
String msgErro       = ResumoRetornoBeanId.getMsgErro();
String msgErroTop    = "90 px" ;
String msgErroLeft   = "80 px" ;
%>
<%@ include file="EventoErro_Diretiva.jsp"  %> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

</form>
</body>
</html>