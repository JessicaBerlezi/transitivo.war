<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator" %>
<%@ page import="sys.Util" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="RelatArDigitTMPBeanId"      scope="session" class="REG.RelatArDigitTMPBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 





function valida(opcao,fForm,evento,envio) {
 switch (opcao) {
    case 'buscaARPeriodo':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
      }
      break ;
    case 'ImprimirTotal':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
      }
      break ;
   case 'B':
		document.Form.codEvento.value=evento;
		document.Form.datEnvio.value=envio;
		document.Form.acao.value=opcao;
	   	document.Form.target= "_blank";
	    document.Form.action = "acessoTool";  
	   	document.Form.submit();	  		  
      break ;
	case 'R':  
	  close() ;   
      break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


function veCampos(fForm) {

  
    if (fForm.datIni.value=='') {
      alert("Data inicio em branco. \n");
      return false;
    }

    if (fForm.datFim.value=='') {
      alert("Data fim em branco. \n");
      return false;
    }

    return Compara_Data(fForm.datIni.value, fForm.datFim.value);
    
}



function Compara_Data(dataini, datafim)
{
    
	dia = dataini.substr(0,2);
	mes = dataini.substr(3,2);
	ano = dataini.substr(6,4);
	
	dataini = new Date(ano,mes - 1,dia);
	dia = datafim.substr(0,2);
	mes = datafim.substr(3,2);
	ano = datafim.substr(6,4);

	datafim = new Date(ano,mes - 1,dia);
	
	if (dataini > datafim)
	{
		alert('A Data de T�mino deve ser maior ou igual � Data de In�cio!');
		return false;
	}
	return true;
 }


function mascaraData(objeto, evento) {
  var tecla = evento.keyCode;
 
    if (objeto.value.length == 10){
      return;
    }


  if (((tecla < 48) || (tecla > 57)) && (((tecla < 96) || (tecla > 105))) && (tecla != 8) && (tecla != 9) && (tecla != 35) && (tecla != 36) && (tecla != 37) && (tecla != 38) && (tecla != 39) && (tecla != 40)) {
    objeto.value = objeto.value.substr(0,objeto.value.length-1);
  }
  
  if (tecla != 8) {
    //joga a primeira barra da data, assim que o usu�rio digitar 2 caracteres
    if (objeto.value.length == 2) { 
		  objeto.value+="/";
	  }
	  //joga a segunda barra da data, assim que o usu�rio digitar 5 caracteres
	  if (objeto.value.length == 5) { 
  	  objeto.value+="/";
    }
  }
  else {
    if ((objeto.value.length == 2) || (objeto.value.length == 5)) {
      objeto.value = objeto.value.substr(0,objeto.value.length-1);
    }
  }
}

 
</script>



</head>


<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  OnLoad="document.Form.datIni.focus();" style="overflow: hidden;">
<form name="Form" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="codEvento" type="hidden" value=' '>
<input name="datEnvio" type="hidden" value=' '>


<!--IN�CIO_CORPO_sistema-->
<!--FILTROS-->
<div id="WK_SISTEMA" style="position:absolute; width:730px; overflow: visible; z-index: 1; top: 80px; left: 50px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">
      <tr><td colspan="3" height="2"></td></tr>
      <tr>
			  <td width="36%"><b>Data de Envio:&nbsp;</b>&nbsp;<input name="datIni" type="text" id="datIni" size="12" maxlength="12" value="<%=RelatArDigitTMPBeanId.getDatInicio()%>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
			  &nbsp;at�&nbsp;<input name="datFim" type="text" id="datFim" size="12" maxlength="12" value="<%=RelatArDigitTMPBeanId.getDatFim()%>" onKeyPress="javascript:Mascaras(this,'99/99/9999');" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();">
			  </td>		  
	          <td align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaARPeriodo',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td> 
	          <td align="left">
                   <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ImprimirTotal',this.form);">	
                         <img src="<%= path %>/images/bot_imprimir_ico.gif" width="26" height="19" align="left">
                   </button>
          </td>
			           
      </tr>	
      <tr><td colspan="3" height="2"></td></tr>	 
    </table>    
</div>
<!-- FIM FILTROS-->

<!--legenda-->
 <div id="div3" style="position:absolute; left:50px; top:328px; width:720px; height:30px; z-index:5; overflow: visible; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="1" cellspacing="1" >
<tr> 
        <td width="81" height="15"  bgcolor="#CCCCCC"  style="text-align: right"><strong>TOTAL :</strong>&nbsp;&nbsp;&nbsp;&nbsp;</td>		
        <td width="89"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdRetornadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="99"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdApropriadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="79"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdDesprezadosTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="90"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdNoaProcessTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="70"  bgcolor="#CCCCCC"  style="text-align: right"><strong><%= Util.formataIntero(RelatArDigitTMPBeanId.getQtdAvisoTot()) %></strong>&nbsp;&nbsp;&nbsp;</td>
      </tr>
</table>
</div>
<!--FIM legenda-->


<!--t�tulos das colunas da tabela-->
  <div id="div1" style="position:absolute; left:50px; top:105px; width:720px; height:50px; z-index:5; overflow: auto; visibility: visible;">
    <table width="720" border="0" cellpadding="1" cellspacing="1">
      <tr bgcolor="#deebc2"> 
        <td width="81" height="15"  align="center" bgcolor="#deebc2"><strong>Dia</strong></td>
        <td width="89" bgcolor="#EDF4DF" style="text-align: center"><strong>Retornados</strong>&nbsp;&nbsp;&nbsp;</td>
        
        <td width="99" height="15"  style="text-align: center" bgcolor="#deebc2"><strong>Apropriados</strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="79" bgcolor="#EDF4DF" style="text-align: center"><strong>&nbsp;&nbsp;&nbsp;Desconsiderados</strong></td>
        <td width="90" bgcolor="#EDF4DF" style="text-align: center"><strong>&nbsp;&nbsp;&nbsp;� Processados</strong>&nbsp;&nbsp;&nbsp;</td>
        <td width="70" bgcolor="#EDF4DF" style="text-align: center"><strong>Avisos</strong>&nbsp;&nbsp;&nbsp;</td>
      </tr>
    </table>
</div>
<!--FIM t�tulos das colunas da tabela-->

<!--conte�do da tabela-->
<div id="div2" style="position:absolute; left:50px; top:124px; width:720px; height:200px; z-index:5; overflow: auto; visibility: visible;">
<%
	Iterator it = RelatArDigitTMPBeanId.getEventos().iterator();
	REG.RelatArDigitTMPBean_Cont relatorioImp  = new REG.RelatArDigitTMPBean_Cont();					
	if( RelatArDigitTMPBeanId.getEventos().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (REG.RelatArDigitTMPBean_Cont)it.next() ;
%> 
<table width="100%" border="0" cellpadding="1" cellspacing="1" >
<tr> 
        <td width="81" height="15" style="text-align: center"><%=relatorioImp.getDatEnvio()%></td>		
        <td width="89" style="text-align: right">
    	<a href="#"  class="preto"  onClick="javascript: valida('B',this.form,'R','<%=relatorioImp.getDatEnvio()%>')">
        <%= Util.formataIntero(relatorioImp.getQtdRetornados())  %>&nbsp;&nbsp;&nbsp;</a></td>
        <td width="99" style="text-align: right">
    	<a href="#"  class="preto"  onClick="javascript: valida('B',this.form,'A','<%=relatorioImp.getDatEnvio()%>');">
        <%= Util.formataIntero(relatorioImp.getQtdApropriado())%>&nbsp;&nbsp;&nbsp;</a></td>
        <td width="79" style="text-align: right">
    	<a href="#"  class="preto"  onClick="javascript: valida('B',this.form,'D','<%=relatorioImp.getDatEnvio()%>');">
        <%= Util.formataIntero(relatorioImp.getQtdDesprezados())%>&nbsp;&nbsp;&nbsp;</a></td>
        <td width="90" style="text-align: right">
    	<a href="#"  class="preto"  onClick="javascript: valida('B',this.form,'N','<%=relatorioImp.getDatEnvio()%>');">
        <%= Util.formataIntero(relatorioImp.getQtdNaoProcessados()) %>&nbsp;&nbsp;&nbsp;</a></td>
        <td width="70" style="text-align: right">
        <%= Util.formataIntero(relatorioImp.getQtdAvisos()) %>&nbsp;&nbsp;&nbsp;</a></td>
      </tr>
</table>
<%    }
    }%>

</div> 

<!--FIM conte�do da tabela-->

<!--FIM_CORPO_sistema-->

  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',document.frmDownload);"> 
          <img src="<%= path %>/images/REG/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= RelatArDigitTMPBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

</form>
</body>
</html>