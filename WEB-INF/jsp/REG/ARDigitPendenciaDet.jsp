<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %> 
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="REG.NotifControleBean"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.text.DecimalFormat"%>
<%@ page import="java.util.Map"%>

<jsp:useBean id="NotifControleId"  scope="request" class="REG.NotifControleBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html> 
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">

function valida(opcao,fForm) {
	 switch (opcao) {    
	 	 case 'GeraExcel':
			fForm.acao.value=opcao
			fForm.target= "_self";
			fForm.action = "geraXlsARDigitPendencia";  
			fForm.submit();  		  

	      break;
 
	   case 'R':
			close();
			break;
  	   case 'visualizacao':
			fForm.acao.value=opcao;
	   		fForm.target= "_self";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  		break ; 		  
	  break ;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
		  
  }
}

function validaDetalhe(opcao,fForm) {
	document.all["acao"].value="detalhe";
	document.all["sentido"].value=opcao;
	document.all["linhaForm"].target= "_self";
	document.all["linhaForm"].action = "acessoTool";  
	document.all["linhaForm"].submit();
}
</script>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
 
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="linhaForm" method="post" action="">

<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"     type="hidden" value=' '>
<input type="hidden" name="codOrgao" value="<%=(String)request.getAttribute("codOrgao")%>">
<input type="hidden" name="statusAuto" value="<%=(String)request.getAttribute("statusAuto")%>">
<input name="sitEntrega" type="hidden" value="<%=(String)request.getAttribute("sitEntrega")%>">
<input type="hidden" name="prazo" value="<%=(String)request.getAttribute("prazo")%>">
<input type="hidden" name="todosOrgaos" value="<%=(String)request.getAttribute("todosOrgaos")%>">
<input type="hidden" name="totalMesAno" value="<%=(String)request.getAttribute("totalMesAno")%>">
<input type="hidden" name="prazoDigitado" value="<%=(String)request.getAttribute("prazoDigitado")%>">
<input type="hidden" name="primeiroAnterior" value="<%=(String)request.getAttribute("primeiroAnterior")%>">
<input type="hidden" name="numPag" value="<%=(String)request.getAttribute("numPag")%>">
<input type="hidden" name="sentido" value="">
<input type="hidden" name="seq" value="<%=(String) request.getAttribute("seq")%>">
<input name="nomArquivo"  type="hidden" value='Pendencia_digitalizacao_AR.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>


<%
String[] todosOrgaosCod = (String[])request.getParameterValues("todosOrgaosCod");
for (int i=0;i<todosOrgaosCod.length;i++) 
{
%>
	<input type="hidden" name="todosOrgaosCod" value="<%=todosOrgaosCod[i]%>">
<%
}
%>

<%
String totalMesAno = (String)request.getAttribute("totalMesAno");
%>
<div id="div13" style="position:absolute; left:200px; top:85px; width:460px; height:50px; z-index:5; overflow: auto; visibility: visible;"> 
	<table width="100%"cellspacing="0" cellpadding="0" border="0">
	  <tr>
	    <%
	    Map meses = (Map) request.getAttribute("meses");
	    String valorFormatado = "";
	    DecimalFormat formato = new DecimalFormat(",##0");
	    valorFormatado = formato.format(Integer.parseInt(totalMesAno.substring(8).trim()));
	    %>
	   	<td width="67%"><strong>
	   			M&ecirc;s:&nbsp;<%=meses.get(totalMesAno.substring(0, 2))%>
				&nbsp;Ano:&nbsp;<%=totalMesAno.substring(3, 7)%>
				&nbsp;Total:&nbsp;<%=valorFormatado%>&nbsp;&nbsp;</strong>
        </td>
	   <%
	   	Iterator it = NotifControleId.getListaNotifs().iterator();
	   %>
        <td width="13%"  align="center"> <br>
			<button type="button" NAME="ok" style="width: 28px; height: 21px; border: none; background: transparent;" onClick="javascript: validaDetalhe('Imprimir',this.form);">
				<img src="<%= path %>/images/bot_imprimir_ico.gif" alt="Imprimir" align="left" >
		</button>		</td>
		
        <td width="20%"  > <br>
		   <button type="button" NAME="GeraExcel"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form);"> 	
				<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel">
	    </button>        </td>
	</table>
</div>
<div id="div01" style="position:absolute; left:40px; top:135px; width:740px; z-index:1; overflow: auto; visibility: visible;"> 
    <table width="100%" border="0" cellpadding="0" cellspacing="1">
		<tr bgcolor="#deebc2"> 
        	<td width="30"  height="15" align="center"><strong>Seq</strong></td>
        	<td width="70"  align="center"><strong>Notifica&ccedil;&atilde;o</strong></td>
        	<td width="70"  align="center"><strong>Auto</strong></td>
        	<td width="60"  align="center"><strong>Placa</strong></td>
        	<td width="160" align="center"><strong>Arquivo Recebido</strong></td>
        	<td width="70"  align="center"><strong>Emiss&atilde;o</strong></td>        	
        	<td width="70"  align="center"><strong>Envio</strong></td>
        	<td width="100" align="center"><strong>Usu&aacute;rio</strong></td>
        	<td align="center"><strong>Entrega</strong></td>        	
        	
		</tr>
    </table>
</div>

<%
	NotifControleBean notifControle = new NotifControleBean();
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	int i = 0;
	int cont = Integer.parseInt((String)request.getAttribute("seq"));
%>
	<div id="div13" style="position:absolute; left:40px; top:151px; width:740px; height:190px; z-index:5; overflow: auto; visibility: visible;"> 
    	<table width="100%" border="0" cellpadding="0" cellspacing="1">
<%
		while (it.hasNext()) 
		{     		
  			notifControle =(NotifControleBean)it.next();
  			if (i == 0){%>
      			<input type="hidden" name="primeiroRegistro" value="<%=notifControle.getNumNotificacao()%>">
<%  		}
  			if (i == (NotifControleId.getListaNotifs().size() - 1)){%>      				
  				<input type="hidden" name="ultimoRegistro" value="<%=notifControle.getNumNotificacao()%>">
<%      	}%>

	   		<tr bgcolor="#EDF4DF">
	   		   <td width="30" height="18" align="center">
					<%=cont%>
				</td>
				<td width="70" height="18" align="center">
					<%=notifControle.getNumNotificacao()%>
				</td>
			    <td width="70" align="center">
			    	<%=notifControle.getNumAutoInfracao()%>
			    </td>
			    <td width="60" align="center">
			    	<%=notifControle.getNumPlaca()%>
			    </td>
			    <td width="160" align="center">
			    	<%=notifControle.getNomArquivo(notifControle.getCodArquivoEnvio())%>
			    </td>
			    
			<%if(notifControle.getDatEmissao() == null){%>
				<td width="70" align="center"></td>
			<%}else{%>
			    <td width="70" align="center">
			    	<%=sdf.format(notifControle.getDatEmissao())%>
		    	</td>
			<%}
			
			if(notifControle.getDatEnvio() == null){%>
				<td width="70" align="center"></td>
			<%}else{
			%>
			    <td width="70" align="center">
			    	<%=sdf.format(notifControle.getDatEnvio())%>
			    </td>
			<%}%>
			
			<%if( (!(notifControle.getSigCodOrgao(notifControle.getCodOrgaoEnvio()) == null)) && (! (notifControle.getNomUsuarioEnvio()==null) ) ){%>
				<td width="100" align="center">
					<%=notifControle.getSigCodOrgao(notifControle.getCodOrgaoEnvio())%>/<%=notifControle.getNomUsuarioEnvio()%>
				</td>
			<%}else{%>
				<td width="110" align="center"></td>
			<%}%>
			
			<%if(notifControle.getDatEntrega() == null){%>
				<td align="center"></td>
			<%}else{%>		    
			   	<td align="center">
			    	<%=sdf.format(notifControle.getDatEntrega())%>
			    </td>
			<%}%>						
			    
		  </tr>
	<% i++;
  	   cont++;
	}// fim do while
	%>
		
	<%	
      	if(NotifControleId.getListaNotifs().size()==0){%>
			<div id="div14" style="position:absolute; left:45px; top:140px; width:740px; height:207px; z-index:5; overflow: auto; visibility: visible;"> 
		  		<table width="100%" border="0" cellpadding="0" cellspacing="1">     
					<tr bgcolor="#EDF4DF">
						<td><strong>Nenhum notifica��o existente para o status</strong></td>
					</tr>
				</table>
			</div>	
	    <%}%>
	</table>
</div>

<div id="div14" style="position:absolute; left:45px; top:350px; width:740px; z-index:5; overflow: auto; visibility: visible;"> 
    	<table width="100%" border="0" cellpadding="0" cellspacing="1">
		  <tr bgcolor="#EDF4DF">
			   <td colspan="8" align=center>
			     <% if( cont <= Integer.parseInt(totalMesAno.substring(8).trim())){%> 
			     <a href="#" onclick="javascript: validaDetalhe('Prox',this.form);"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>Pr&oacute;xima P&aacute;gina </font></a>&nbsp;&nbsp;&nbsp;
			     <% } %>
			     <% String pag = (String)request.getAttribute("numPag");
			        if (!pag.equals("1")){ %>
			     <a href="#" onclick="javascript: validaDetalhe('Ant',this.form) ;"><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>P&aacute;gina Anterior</font></a>&nbsp;&nbsp;&nbsp;	  
			     <% } %>
		    	 <a href="#" onclick="javascript: valida('R',this.form);  "><font face='Verdana, Arial, Helvetica, sans-serif' size='2' color='#000000'>Retornar       </font></a>	  
	      	   </td>
	      </tr>
	   </table>
</div>
<!--FIM conte�do da tabela-->

<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Fim Rodap� -->

</form>
</body>
</html>