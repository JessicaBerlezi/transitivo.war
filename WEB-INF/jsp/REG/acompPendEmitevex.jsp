<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>

<jsp:useBean id="acompPendEmitevexBeanId" scope="request" class="REG.acompPendEmitevexBean" />
<jsp:useBean id="ParamSistemaBeanId" scope="session" class="ACSS.ParamSistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) 
{
 switch (opcao) {
   case 'ImprimeRelatorio':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 	  
	 case 'VisualizaRelatorio':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;  
	  
   case 'GeraExcel':
	  if (veCampos(fForm)==true){
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "geraxls";  
		fForm.submit();  		  
	  }
	  break ;
	case 'GeraWord':
	  if (veCampos(fForm)==true){	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'R':
			close();
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.dataIni.value==0) {  valid = false
	   sErro += "Data in�cio n�o informada. \n"
	}
	if (fForm.dataFim.value==0) {  valid = false
	   sErro += "Data fim n�o informada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="RelatForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=''>
<input name="codArquivo"  type="hidden" value=''>
<input name="nomArquivo"  type="hidden" value='RelArqNotificacao.xls'>
<input name="contexto"    type="hidden" value='DOWN_ARQUIVO_XLS'>
<input name="extensao"    type="hidden" value=''>	

<div  style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 144px; left: 50px; z-index:1; visibility: visible;" > 
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="58%"  align="center">
	<tr>		
		<td width="21%" align="right"  ><b>Data inicial<strong>:</strong></b></td>
	  		<td width="18%"> 
				&nbsp;&nbsp;&nbsp;<input name="dataIni" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=acompPendEmitevexBeanId.getDatIni()%>" >
			</td> 	
			<td width="22%" align="right"  ><b>Data Final<strong>:</strong></b></td>
	  	    <td width="17%"> 
				&nbsp;&nbsp;&nbsp;<input name="dataFim" type="text" size="12" maxlength="12" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');" value="<%=acompPendEmitevexBeanId.getDatFim()%>" >
			</td> 	
			<td width="22%" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('VisualizaRelatorio',this.form);">	
	        	<IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>																							
			</td>		
	</tr>
	<tr><td colspan="2" height="2"></td></tr>	 
</table> 
<!--table border="0" cellpadding="0" cellspacing="1" width="58%"  align="center">
<tr>
		<td width="50%" align="right">
			<input name="diretorio" type="file" size="50" alt="Indique aqui o arquivo xls para o qual ser� exportado o relat�rio">	
		</td>
		<td align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button type="button" NAME="GeraExcel" style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('GeraExcel',this.form);"> 	
				<IMG src="<%= path %>/images/bot_excel_ico.gif" align="left" alt="Gerar planilha excel" >
			</button>
		</td>
	</tr>	
</table-->   
</div>     

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= acompPendEmitevexBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
</form>
</BODY>
</HTML>

