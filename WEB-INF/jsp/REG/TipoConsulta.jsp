<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<%@ page  import = "java.util.*;" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" > 
</jsp:useBean>
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" > 
</jsp:useBean>
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" > 
</jsp:useBean>

<jsp:useBean id="TipoBeanColId" scope="request" class="TAB.TipoBeanCol" > 
</jsp:useBean>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>Detran - Consulta de Tipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->

self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
  switch (opcao) {
case 'OK':
	fForm.j_cmdFuncao.value=fForm	;
	fForm.j_sigFuncao.value=fForm	;
	fForm.j_jspOrigem.value="/menuSistema.jsp"	;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";
   	fForm.submit();
	break;
   case 'R':
    close() ;   
 	break;
	case 'O':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	  else  document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
    case 'V':
   	  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	  else  document.all["MsgErro"].style.visibility='visible' ; 
	  break;	 
  }
}

<%
		Vector vUF = new Vector();	
	  	vUF=TipoBeanColId.ColToVector();
%>

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);
-->

</script>
</head>

<!--<form name="UsrForm" method="post" action="http://localhost:8080/SistDet/menuSistema.jsp"> -->
<form name="UsrForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
  <tr> 
    <td width="34" valign="top" background="images/REG/detran_bg1.png"><img src="images/REG/detran_bg1.png" width="34" height="108"><br><img src="images/REG/detran_lat.png" width="34" height="131"></td>
    <td  align="center" valign="top" background="images/detran_bg4.gif">&nbsp;</td>
  </tr>
</table>
<!--Fim Fundo da tela-->
<!--Corpo do sistema-->
<!--consulta Marcas-->	
    
<div id="RelacaoArquivos" style="position:absolute; left:50px; top:100px; width:720px; height:10px; z-index:7; background-image: url(images/detran_bg4.gif); overflow: visible; visibility: visible;"> 
 
 <table width="100%" border="0" cellpadding="2" cellspacing="2" class="table">
    <tr bgcolor="#a8b980"> 
      <td width="40"> <div align="center"><font color="#ffffff"><strong>C�digo</strong></font></div></td>
      <td > <div align="center"><font color="#ffffff"><strong>Descri��o</strong></font></div></td>
   </tr>
  </table>
</div>

<!--Inicio da Table do detalhe das informa��es retornadas do banco-->
<div id="RelacaoArquivos2" style="position:absolute; left:50px; top:120px; width:720px; height:173px; z-index:6; background-image: url(images/detran_bg4.gif); overflow: auto; visibility: visible;"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="2">
    <tr bgcolor="#DEEBC2"> 
      
<%    
			for (int t=0; t<vUF.size();t++) {
				
			TAB.TipoBean ufb = (TAB.TipoBean) vUF.elementAt(t);
			out.println("<tr bgcolor=\"#EFF5E2\"><td width=\"40\" height=\"23\">"); 
			out.println(ufb.getCodTipo() + "</td>");
			out.println("<td  height=\"23\">" + ufb.getDscTipo() + "</td>");
			out.println("</tr>");
		  }  

%> 
    </tr>
  </table>
</div>

<div id="botoesconsulta" style="position: absolute; left: 50px; top: 310px; overflow: visible; width:687px;"> 
  <table border="0" align="center" cellpadding="0" cellspacing="0" class="BordaTopo">
    <tr>
      <td align="left"><button style="border: 0px; background-color: transparent; height: 19px; width: 52px; cursor: hand;" onClick="javascript: window.close();"><img src="images/retornar.gif" width="52" height="19" ></button></td>
    </tr>
  </table>
</div>
<!--Fim Rela��o de Arquivos-->

<!--FIM consulta Tipo-->
<!--FIM Corpo do Sistema-->

<input name="token"         type="hidden"  value="">				
<input name="cmdFuncao"     type="hidden"  value="login">
<input name="j_abrevSist"   type="hidden" value="REG">		
<input name="j_jspOrigem"   type="hidden" value="/REG/login.jsp">				
<input name="tipoUsuario"   type="hidden" value="1">				
  <!--Bot�o retornar-->
  <div id="retornar" style="position:absolute; right: 10px; bottom: 7px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
    <table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/REG/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<!--FIM Bot�o retornar-->	
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= TipoBeanColId.getMsgErro() %>" />
  <jsp:param name="msgErroTop"  value= "160 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->
		
</body>
</form>
</html>
