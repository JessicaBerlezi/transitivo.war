<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 

<!-- Chama o Objeto da Tabela de Sistemas-->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" />
<jsp:setProperty name="SistId" property="colunaValue" value="cod_Sistema" />
<jsp:setProperty name="SistId" property="popupNome"   value="codSistema"  />
<jsp:setProperty name="SistId" property="popupString" value = "nom_Sistema, SELECT cod_Sistema, nom_abrev ||' '|| nom_sistema as nom_sistema FROM TCAU_SISTEMA ORDER BY nom_abrev" />

<html>
<head><jsp:include page="Css.jsp" flush="true" />
<title>:: Manutencao Tabela de Mensagens :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!--Abre a janela com o tamanho maximo disponivel -->

self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

function valida( opcao, fForm )
{
 switch (opcao) {
   case 'buscaSistema':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {			
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
        if (veCampos(fForm)==true) {
	    	fForm.acao.value=opcao;
	   	    fForm.target= "_self";
	        fForm.action = "acessoTool";  
	   	    fForm.submit();	  		  
	    }
	    else 
	        alert(sErro)
      break ;
   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
			fForm.atualizarDependente.value="N"	  
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.j_sigFuncao.value = "<%=UsuarioFuncBeanId.getJ_sigFuncao()%>" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function veCampos(fForm) {
   valid   = true ;
   sErro = "" ;
   if (fForm.codSistema.value==0) {
       valid = false
       sErro = sErro + "Sistema n�o selecionado. \n"
   }
   for (k=0; k < fForm.dscMensagem.length; k++)   {
        var ne = trim(fForm.dscMensagem[k].value)
        if (ne.length==0) continue ;
	    <!-- verificar duplicata --> 
      	for (m=0;m<fForm.dscMensagem.length;m++) {
	         var dup = trim(fForm.dscMensagem[m].value)
        	 if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		         sErro += "Mensagem em duplicata: (linhas: "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
        		 valid = false ;			   
  	         }
	    }
   }
   return valid ;
}
function mostra(LAYER) {
    if (document.layers) document.layers[LAYER].visibility='show' ;
    else document.all[LAYER].style.visibility='visible' ;
    return;
}

function esconde(LAYER) {
    if (document.layers) document.layers[LAYER].visibility='hide' ;
    else   document.all[LAYER].style.visibility='hidden' ;
    return;
}

function PassaValorParaTextArea(dscMotivo, dscAcao, i) {
    perfilForm.dscMotivo.value = dscMotivo[i].value;
    perfilForm.dscAcao.value   = dscAcao[i].value;
    perfilForm.Gardaid.value = i;
	mostra('MOTIVOACAO');
    return;
}

function PassaValorParaText(dscMotivo, dscAcao, i) {
    perfilForm.dscMotivo[i].value = dscMotivo.value;
    perfilForm.dscAcao[i].value   = dscAcao.value;
	esconde('MOTIVOACAO');
    return;
}

function MudaStatus(i) {
    if ( (perfilForm.dscMotivo[i].value == "") && (perfilForm.dscAcao[i].value == "") )
         perfilForm.PossuiDoc[i].value = "N�o";	
    else perfilForm.PossuiDoc[i].value = "Sim";
}

function MudaStatus1() {
    for(i=0;i<perfilForm.dscMotivo.length;i++)	{
	    if ( (perfilForm.dscMotivo[i].value == "") && (perfilForm.dscAcao[i].value == "") )
    	     perfilForm.PossuiDoc[i].value = "N�o";	
	    else perfilForm.PossuiDoc[i].value = "Sim";
	}
}
</script>
</head>

<body OnLoad="MudaStatus1();" bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden; ">
<form name="perfilForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value="">	
<input name="Gardaid" type="hidden" value="">	

<!--Inicio da div Motivo acao-->

<div id="MOTIVOACAO" style="position: absolute; overflow: auto; border-style: none; background-color: #ffffff; z-index: 30; left: 150px; top: 145px; width: 440px; height: 267px; visibility: hidden;">      
   <table width="100%" border=1 align="center" bordercolor="#000000" cellpadding=2 cellspacing=3>
        <tr bgcolor="#DFEEF2"><td><P>Motivo</P></TD></TR>
		<tr><td><TEXTAREA class="COMBORDA" style="WIDTH: 400px; HEIGHT: 90px" name=dscMotivo rows=5 cols=12 value= dscMotivo[i].value ></TEXTAREA> </TD></TR>
        <tr bgcolor="#DFEEF2"><td><P>A��o</P></TD></TR>
		<tr><td><TEXTAREA class="COMBORDA" style="WIDTH: 400px; HEIGHT: 90px" name=dscAcao rows=5 cols=12  value= dscAcao[i].value > </TEXTAREA></TD></TR>
		<tr><td align=center><input name="PossuiDoc1"  type=button value="Retornar" onClick="PassaValorParaText(dscMotivo, dscAcao, perfilForm.Gardaid.value),MudaStatus(perfilForm.Gardaid.value);" ></TD></TR>
   </table> 
</div>
<!--Fim da div Motivo acao-->
  <div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1;  left:204px; top: 80px; width:400px; height:130px;" > 
    <!--INICIO CABEC DA TABELA-->
    <table border='0' cellpadding="0" cellspacing="1" width="400"  align=center>
      <tr>
		  <td align="left" width=60%><b>Sistema:&nbsp;</b>
		    <input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
  			<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codSistema"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
			<% }
			else { %>
		        <input name="codSistema" type="hidden" size="50" value="<%= SistId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"   size="50" maxlength="50"  value="<%= SistId.getNomSistema() %>">
			<% } %>			
		  </td>
   		   <% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  		  
	          <td align=left width=20%>&nbsp;&nbsp;&nbsp;
			  	<button type=button NAME="Ok"   style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistema',this.form);">	
		        <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left" ></button>
		  	</td>
	       	<td align=left width=20%>
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
          <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align="left" ></button>
          	</td>
		   <% } %>			
      </tr>	
      <tr> 
        <td height="2" colspan=2 valign="top"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border='0' cellpadding="0" cellspacing="0" width="400" align=center>
    <TR>
		<% if (SistId.getAtualizarDependente().equals("S")){ %>			
	    <td align="left" width="33%">
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align="left"  ></button>
          </td>
       <td align="center" width="34%">
                  <button type=button NAME="imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19" align="left"  ></button>			
          </td>
       <td align="right" width="33%"> 
                  <button type=button NAME="retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align="left" ></button>
          </td>
		<%} %>
    </TR>
  </TABLE>  
</div>

<div style="position:absolute; overflow: auto; z-index: 1; left: 35px; top: 140px; height: 32px; width:750px;">
  <table border='0' cellpadding="0" cellspacing="1" width="100%"  align="center">
    <tr bgcolor="#79B3C4"> 
      <td width=80 align=center ><font color="#ffffff"><b>Seq</b></font></TD>
        <td width=80 align=center ><font color="#ffffff"><b>N&uacute;mero</b></font></TD>
      <td width=360 align=center ><font color="#ffffff"><b>Descri&ccedil;&atilde;o</b></font></TD>
      <td width=80 align=center ><font color="#ffffff"><b>Classe</b></font></TD>
      <td           align=center ><font color="#ffffff"><b>Possui Doc</b></font></TD>
    </tr>
  </table>
</div>

  <div style="position:absolute; overflow: auto; z-index: 1; top: 155px; left=35px; height=190px; width:750px; left: 35px;"> 
    <table border='0' cellpadding="0" cellspacing="1" width="100%"  align="center">
<% 
	if (SistId.getAtualizarDependente().equals("S")) {				   
	   int ocor = SistId.getMensagens().size();
	   if ( ocor <= 0 ) ocor = SistId.getMensagens(10,5).size();
	   for (int i=0; i<ocor; i++) {
%> 
    <tr bgcolor="#DFEEF2"> 
      <td width=80 align=center> 
        <input name="seq" type="text" readonly size="10" value="<%= i+1 %>">
        <input name="codMensagemSistema" type="hidden"   value="<%= SistId.getMensagens(i).getCodMensagemSistema() %>">
	    <input name="dscMotivo"          type="hidden"   value="<%= SistId.getMensagens(i).getDscMotivo() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();">
        <input name="dscAcao"            type="hidden"   value="<%= SistId.getMensagens(i).getDscAcao() %>"   onKeyPress="javascript:f_end();" onFocus="javascript:this.select();">

      <td width=80 align=center> 
        <input name="codMensagemInterno" type="text" size="10" maxlength="15"  value="<%= SistId.getMensagens(i).getCodMensagemInterno() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" >
      </td>
      <td width=360 align=center> 
        <input name="dscMensagem" type="text" size="60" maxlength="100"  value="<%= SistId.getMensagens(i).getDscMensagem() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" >
      </td>
      <td width=80 align=center> 
        <input name="codTipMensagem" type="text" size="4" maxlength="2"  value="<%= SistId.getMensagens(i).getCodTipMensagem() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" >
      </td>
      <td  align=center>                                            
	    <input name="PossuiDoc" type="button" onClick="PassaValorParaTextArea(dscMotivo, dscAcao, <%= i %>),MudaStatus(<%= i %>);" style="height: 18px; width: 30px;">
	  </td>
    </tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#DFEEF2">
		<td width="80"  align=center><input name="seq"                 type="text" readonly size="10" value="<%= i+1 %>"></td>
    	<td width="80"  align=center><input name="codMensagemInterno"  type="text" readonly size="10" maxlength="15"  value="" ></td>
		<td width="360" align=center><input name="dscMensagem"         type="text" readonly size="60" maxlength="100" value="" ></td>
	   	<td width="80"  align=center><input name="codTipMensagem"      type="text" readonly size="4"  maxlength="2"  value="" ></td>
	   	<td             align=center>&nbsp;</td>
	</tr>
<%						}					
					}   
%>	
  </table>
</div>
<!--FIM_CORPO_sistema-->
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
<jsp:param name="msgErro"     value= "<%= SistId.getMsgErro() %>" />
<jsp:param name="msgErroTop"  value= "130 px" />
<jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include>
<!--FIM_Div Erros-->
<script>
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for( i=0; i < document.perfilForm.codSistema.length; i++ ){
    if( document.perfilForm.codSistema.options[i].value == njint ){
        document.perfilForm.codSistema.selectedIndex = i;	  
    }
  }
} 

</script>

</form>
</body>
</html>
