<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>  
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto de Consulta Usu�rio por sistema-->
<jsp:useBean id="ConsultaUsuarioLogadoBeanId" scope="request" class="ACSS.ConsultaUsuarioLogadoBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistBeanId"      scope="request" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#79B3C4";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 Cresult = "#be7272";
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 Cresult = "#a8b980";
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
 		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 


mySist      = new Array(<%= SistBeanId.getSistemas(UsuarioBeanId.getCodUsuario()) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
 
   case 'ok':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();
	   	break ;
	   	
   case 'confirmaBloqueio':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'LogAcesso':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();
	   	break ;
   case 'ImprimeResultConsulta':
		fForm.acao.value=opcao
   		fForm.target= "_blank";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  	break; 	  
	  		  	  
   case 'R':
      close() ;   
	  break;
   case 'Volta':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function veCampos(fForm) {
	valid   = true ;
	marcado = false;
	sErro = "" ;
	if (fForm.bloq.length == null) {
		if (fForm.bloq.checked==true)
		   marcado = true ;
    } else {
    	for (i=0; i<fForm.bloq.length; i++) {
    	   if (fForm.bloq[i].checked==true) {
		       marcado = true ;
		       break ;
		    }
		}
	}
	sErro = "Selecione o Usu�rio a ser bloqueado";
	valid = marcado;
    if (valid==false) alert(sErro)
	return valid ;
}

function LogAcesso(usuario,sessao,orgaoatua,sistema,dtentrada,ip,fForm) {
	fForm.acao.value="LogAcesso";
	fForm.nom_UserName.value = usuario;
	fForm.num_sessao.value = sessao;
	fForm.orgaoatua.value = orgaoatua;
	fForm.sistema.value = sistema;
	fForm.dtentrada.value = dtentrada;
	fForm.ip.value = ip;
	fForm.target= "_self";
	fForm.action = "acessoTool"; 
	fForm.submit();
}

function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["UsrAtuForm"].target= "_self";
	document.all["UsrAtuForm"].action = "acessoTool";  
	document.all["UsrAtuForm"].submit();	 
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

  
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<!--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o 1.0&nbsp;</font>  -->
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

<input name="acao"              type="hidden" value=' '>	
<input name="ordem"          type="hidden" value="<%=ConsultaUsuarioLogadoBeanId.getOrdem()%>">
<input name="ordClass"          type="hidden" value="<%=ConsultaUsuarioLogadoBeanId.getOrdClass()%>">
<input name="nom_UserName"  type="hidden" value="">
<input name="num_sessao"  type="hidden" value="">
<input name="sistema"  type="hidden" value="">
<input name="orgaoatua"  type="hidden" value="">
<input name="dtentrada"  type="hidden" value="">
<input name="ip"  type="hidden" value="">



<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
   		<% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			
  			  <td width="35%"><b>Sistema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
 				  <select name="codSistema"> 
					<option value="0" >&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_sistema.length;j++) {
							document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
						}
					</script>
		         </select>
			  </td>
	          <td width="65%" align="left">
			  		<button type="button" NAME="Ok"   style="width: 28px;height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('ok',document.UsrAtuForm);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19">
					</button>
			  </td>
          <% }
			else { %>
			  <td  colspan="2"><b>Sistema:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
	            <input name="codSistema"          type="hidden" size="14" maxlength="10" value="<%= SistBeanId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"  size="50"  maxlength="50" value="<%= SistBeanId.getNomSistema() %>" style="border: 0px none;">
		 	  </td>
<!---->				
	      <% } %>			  
      </tr>	
<!----><tr><td colspan="4" height="2"></td></tr>
	  <% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			  <tr>
			  <td><b>�rg�o Atua��o:&nbsp;&nbsp;&nbsp;&nbsp;</b>
					<select name="codOrgao"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
		</select>	    </td>
		
						<td colspan=3></td>		  
          <% }
			else { %>
			  <td  colspan="4"><b>Org�o Atua��o:&nbsp;</b>
	            <input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
				<input disabled name="sigOrgao" type="text"  size="15" maxlength="10" value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >
			  </td>
	      <% } %>
		</tr>			  
	  
	  
    </table>    
	<TABLE border="0" cellpadding="0" cellspacing="0" align = "center" width="100%">	 
	  	<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>			 
	 		<td width="75%"  align = "right"> 
	  		  <button type=button NAME="BloqUsuario"  style="height: 26px; width: 74px;border: none; background: transparent;" onClick="javascript: valida('confirmaBloqueio',document.UsrAtuForm);" >
			  	<img src="<%= path %>/images/bot_bloquear_det1.gif" width="74" height="26" >
			  </button>
			</td>
	 		<td width="16%"  align = "right"> 
	  		  <button type=button NAME="Retornar"  style="height: 21px; width: 28px;border: none; background: transparent;" onClick="javascript: valida('Volta',document.UsrAtuForm);" >
				  <img src="<%= path %>/images/bot_retornar_ico.gif" width="26" height="19" >
			  </button>
			</td>
			<td width="3%"></td>
			<td width="6%" align="right">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 28px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimeResultConsulta',document.UsrAtuForm)"> 
              <img src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir" width="26" height="19">
			</button>			
			 </td>
	   <% } %>	
    </table>
</div>


<div style="position:absolute; width:750px; overflow: visible; z-index: 1; top: 142px; left: 35px; height: 25px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
     <tr>
       <td colspan=2 align=right>
		 <font color="#000000">&nbsp;Ordem:&nbsp;<%= ConsultaUsuarioLogadoBeanId.getNomOrdem()%>&nbsp;</font>
	   </td>	
    </tr>  		
  </table>
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr><td colspan=4 height=3></td></tr>		  		
	<tr > 	     
	   	 <td width="40"  align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>SEQ</b></font></TD>		 		 	  
     	 <td width="130" align="center" bgcolor="<%=Cresult%>">
     	   <a href="#" onClick="javascript: Classificacao('NOM_USERNAME',document.UsrAtuForm);"><font color="#ffffff"><b>USU�RIO</b></font></TD>
     	 <td width="80" align="center" bgcolor="<%=Cresult%>">
     	   <a href="#" onClick="javascript: Classificacao('ORGAO_ATUA',document.UsrAtuForm);"><font color="#ffffff"><b>ORG�O ATUA.</b></font></TD>		 
	   	 <td width="70" align="center" bgcolor="<%=Cresult%>">
     	   <a href="#" onClick="javascript: Classificacao('SISTEMA',document.UsrAtuForm);"><font color="#ffffff"><b>SISTEMA</b></font></TD>
     	 <td width="80" align="center" bgcolor="<%=Cresult%>">
     	   <a href="#" onClick="javascript: Classificacao('SIGFUNCAO',document.UsrAtuForm);"><font color="#ffffff"><b>FUNC. EM USO</b></font></TD>
	   	 <td width="130" align="center" bgcolor="<%=Cresult%>">
	   	   <a href="#" onClick="javascript: Classificacao('DAT_SESSAO',document.UsrAtuForm);"><font color="#ffffff"><b>&nbsp;DATA DE ENTRADA</b></font></TD>
	   	 <td width="140" align="center" bgcolor="<%=Cresult%>">
	   	   <a href="#" onClick="javascript: Classificacao('IP',document.UsrAtuForm);"><font color="#ffffff"><b>IP</b></font></TD>
	   	 <td width="40" align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>BLOQ</b></font></TD>  
	   	 <td align="center" bgcolor="<%=Cresult%>"><font color="#ffffff">&nbsp</font></TD>		 
	   	 		 		 	  
	</tr>
   </table>
</div>   

<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>				  
<div style="position:absolute; width:750px; overflow: auto; z-index: 1; top: 175px; left: 35px; height: 181px; ">   
   <table id = fconsulta border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator itOrg = ConsultaUsuarioLogadoBeanId.getDados().iterator() ;
		int seq = 1;	
		ACSS.ConsultaUsuarioLogadoBean myUsrLog =  new ACSS.ConsultaUsuarioLogadoBean() ;
        while (itOrg.hasNext()) {
			// lista os orgaos de atuacao			
			myUsrLog = (ACSS.ConsultaUsuarioLogadoBean)itOrg.next() ;
	%>       
		<tr> 	
    	   	 <td width="40" align="center" bgcolor="<%=Cor%>"><%= seq %></TD>		 		 	  
             <td width="130" bgcolor="<%=Cor%>">&nbsp;&nbsp;<%= myUsrLog.getNomUserName() %></td>
             <td width="80" bgcolor="<%=Cor%>">&nbsp;&nbsp;<%= myUsrLog.getSigOrgaoAtuacao() %> </td>
             <td width="70" bgcolor="<%=Cor%>">&nbsp;&nbsp;<%= myUsrLog.getSistema() %> </td>
             <td width="80" bgcolor="<%=Cor%>">
             <a href="#" onClick="javascript: LogAcesso('<%= myUsrLog.getNomUserName() %>','<%= myUsrLog.getNumSessao() %>','<%= myUsrLog.getSigOrgaoAtuacao() %>','<%= myUsrLog.getSistema() %>','<%= myUsrLog.getdataSessao() %>','<%= myUsrLog.getIp() %>',document.UsrAtuForm);">&nbsp;&nbsp;<%= myUsrLog.getSigFuncao() %></td>
 		   	 <td width="130" bgcolor="<%=Cor%>">&nbsp;&nbsp;<%= myUsrLog.getdataSessao() %></TD>	
             <td width="140" bgcolor="<%=Cor%>">&nbsp;&nbsp;<%= myUsrLog.getIp() %></TD>
             <% if (myUsrLog.getflBloqueado().equals("1") == true){ %>
					<td width="40" align="center" bgcolor="<%=Cor%>"><img src="<%= path %>/images/im_cadeado_fechado_peq.gif" width="23" height="23" ></td>
			 <% }
			  else {%>	
       		   		<td width="40" align="center" bgcolor="<%=Cor%>"></td>
	          <% } %>
             <td align="center" bgcolor="<%=Cor%>"><input type="checkbox" name="bloq" value="<%=myUsrLog.getNomUserName()%>" class="input" >&nbsp</td>
             <input name="codUsuario"  type="hidden" value="<%= myUsrLog.getCodUsuario() %>">
             <input name="codOrgaoLotacao"  type="hidden" value="<%= myUsrLog.getCodOrgaoLotacao() %>">
             <input name="array_num_sessao"  type="hidden" value="<%= myUsrLog.getNumSessao() %>">

    	   	 		 		 	  
				 
 	   <% 
	   		seq++;
	   } 
	   %>	
	</tr>
   </table>    
   <% } %>	 
</div>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= OrgaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->        

<!-- Rodap�-->
<!--Bot�o retornar--> 
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->
	  
</form>
</BODY>
</HTML>

