<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="NoticiaBeanId"     scope="request" class="ACSS.NoticiaBean" />
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#79B3C4";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
</style>
<title>:: Manutencao Tabela de Usuarios :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function vechck( fForm ) {
 if(fForm.indPrioridade[0].checked == true)    fForm.indPriH.value = "A";
 else
 if(fForm.indPrioridade[1].checked == true)    fForm.indPriH.value = "N";
}

function indPublicacao( fForm ) {
 if(fForm.ind_Public[0].checked == true) fForm.indPublicacao.value = "P";
 else
 if(fForm.ind_Public[1].checked == true) fForm.indPublicacao.value = "N";
}

function veCampos(fForm) {
	valid = true;
	sErro = "";
    verString(fForm.dscNoticia," Assunto ",0);
    verString(fForm.datInicio," Data de in�cio ",0);
    verString(fForm.datFim," Data de in�cio ",0);
    verString(fForm.textoNoticia," Descri��o ",0);
    if( valid == false ) alert(sErro); 
    return valid;
}

function LimparCampos(fForm) {
    document.forms[0].dscNoticia.value   ="";
    document.forms[0].datInicio.value    ="";
    document.forms[0].datFim.value       ="";
    document.forms[0].textoNoticia.value ="";
}

function CarregaDiv(fForm,cod_Noticia,dsc_Noticia,txt_Noticia,ind_Prioridade,dat_Inicio,dat_Fim,dat_Envio,i)
{
   if( i>0 || dsc_Noticia.length > 0   ) {
      fForm.codNoticia.value    = cod_Noticia[i].value; 
      fForm.dscNoticia.value    = dsc_Noticia[i].value;
	  fForm.datInicio.value     = dat_Inicio[i].value;
	  fForm.datFim.value        = dat_Fim[i].value;
      if(ind_Prioridade[i].value == "Normal") fForm.indPrioridade[1].checked = true;
	  else fForm.indPrioridade[0].checked = true;
      fForm.textoNoticia.value  = txt_Noticia[i].value;
    }
	else{
      fForm.codNoticia.value    = cod_Noticia.value; 
      fForm.dscNoticia.value    = dsc_Noticia.value;
	  fForm.datInicio.value     = dat_Inicio.value;
	  fForm.datFim.value        = dat_Fim.value;
      if(ind_Prioridade.value == "Normal") fForm.indPrioridade[1].checked = true;
	  else fForm.indPrioridade[0].checked = true;
      fForm.textoNoticia.value  = txt_Noticia.value;
    }
    return;
}

function limparcampos(fForm){
	fForm.codNoticia.value    = ""; 
    fForm.dscNoticia.value    = "";
	fForm.datInicio.value     = "";
	fForm.datFim.value        = "";
	fForm.indPrioridade[1].checked = true;
    fForm.textoNoticia.value  = "";
}

function valida(opcao,fForm)  {
	 switch (opcao) {    
	   case 'P':
		  	    indPublicacao(fForm);
				fForm.target= "_self";
			    fForm.acao.value=opcao;
				fForm.action = "acessoTool";
				fForm.submit();	  
		break;
	   case 'PA':
   		  	    indPublicacao(fForm);
				fForm.target= "_self";
			    fForm.acao.value=opcao;
				fForm.action = "acessoTool";
				fForm.submit();	  
		break;
	   case 'R':
	    	close();
		break;
	   case 'PublicarNoticia':
             	vechck( fForm );
				if ( veCampos(fForm) == false ) return;
				temp = fForm.j_cmdFuncao.value	   
				fForm.j_cmdFuncao.value = "PublicacaoDeNoticiaCmd";
				fForm.target= "_self";
			    fForm.acao.value=opcao;
				fForm.action = "acessoTool";
				fForm.submit();	  
		    	fForm.j_cmdFuncao.value = temp;
				LimparCampos(fForm);
		break;
	   case 'AlterarNoticia':
             	vechck( fForm );
				if ( veCampos(fForm) == false ) return;
                esconde('btAlterar');
				esconde('WK_SISTEMA');

				temp = fForm.j_cmdFuncao.value	   
				fForm.j_cmdFuncao.value = "PublicacaoDeNoticiaCmd";
				fForm.target= "_self";
			    fForm.acao.value=opcao;
				fForm.action = "acessoTool";
				fForm.submit();	  
		    	fForm.j_cmdFuncao.value = temp;
				LimparCampos(fForm);
		  break; 
	   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible'; 
		  break;	 
  }
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" onLoad="esconde('WK_SISTEMA'),LimparCampos(this.form);"  topmargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="UsrForm" method="post" action="" >
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

<input name="acao"          type="hidden" value="">				
<input name="indPriH"       type="hidden" value="">
<input name="indPublicacao" type="hidden" value="">


<!--IN�CIO_CORPO_sistema--> 
<div id="WK_SISTEMA" style="position:absolute; overflow: visible; z-index: 1; left:54px; top: 75px; width:720px; height: 210px;" > 
   <table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr><td height="5" colspan="2">
	  <input name="codNoticia" type="hidden" value="<%=NoticiaBeanId.getCodNoticia()%>"></td></tr>
      <tr bgcolor="<%=Cor%>"> 
         <td height="20" width="18%"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Assunto</font></strong></td>
         <td width="82%" ><strong><font color="#336699">:</font></strong> 
             <input name="dscNoticia" type="text" size="70" maxlength="50"  value='<%=NoticiaBeanId.getDscNoticia()%>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
		 </td>
      </tr>
	  <tr>
         <td height="1" colspan="2"></td>
      </tr>
      <tr bgcolor="<%=Cor%>"> 
         <td height="20" bgcolor="<%=Cor%>"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;In&iacute;cio da publica&ccedil;&atilde;o </font></strong></td>
         <td ><strong><font color="#336699">:</font></strong> <font color="#00000" size="1" face="Verdana, Arial, Helvetica, sans-serif">
             <input type="text" name="datInicio"  size="12" maxlength="10" value="<%=NoticiaBeanId.getDatInicioStr()%>" onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();" onkeypress="javascript:Mascaras(this,'99/99/9999');">
	         </font> 
			 <strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;T�rmino da publica&ccedil;&atilde;o</font></strong>
			 <strong><font color="#336699">:</font></strong><font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;&nbsp;&nbsp;&nbsp;
             <input type="text" name="datFim" size="12" maxlength="10" value='<%=NoticiaBeanId.getDatFimStr()%>'onChange="javascript:ValDt(this,0);" onFocus="javascript:this.select();" onkeypress="javascript:Mascaras(this,'99/99/9999');">
              </font><font color="#00000" size="1" face="Verdana, Arial, Helvetica, sans-serif">&nbsp;
              </font>
	    </td>
      </tr>
	   <tr>
          <td height="1" colspan="2"></td>
	   <tr bgcolor="<%=Cor%>"> 
          <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Prioridade</font></strong></td>
          <td >
            <strong><font color="#336699">:</font></strong> 
            <% 
               String str1="",str2="";
               if(NoticiaBeanId.getIndPrioridade().equals("A"))       str1="checked";
               else if(NoticiaBeanId.getIndPrioridade().equals("N"))  str2="checked";   
            %>
		   	<strong><font color="#666666">Alta</font></strong>   
		   	<input <%=str1%> name="indPrioridade" type="radio" class="sem-borda"   style="width: 13px; height: 13px; value="">
			<strong><font color="#666666">&nbsp;&nbsp;&nbsp;Normal</font></strong> 
			<input <%=str2%> name="indPrioridade" type="radio" class="sem-borda"  style="width: 13px; height: 13px; value="" onBlur="document.UsrForm.textoNoticia.focus();">
		  </td>
       </tr>
       <tr> <td height="1" colspan="2"></td>
       </tr>
       <tr> <td height="1" colspan="2"></td>
       </tr>
       <tr bgcolor="<%=Cor%>"> 
           <td ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Descri&ccedil;&atilde;o</font></strong></td>
           <td ><textarea name="textoNoticia" rows="3"  value="<%=NoticiaBeanId.getTxtNoticia()%>" style="width:500px; height:100px;" tabindex="4" onChange="this.value=this.value.toUpperCase()"></textarea></td>
      </tr>
      <tr bgcolor="<%=Cor%>"><td colspan="4"></td>
	  <tr bgcolor="#FFFFFF"> 
         <td colspan="4" align="center">
			  <table width="100%"  border="0">
                <tr>
                  <td width="31%">
                     	<div align="center">
                     	  <button id="btIncluir" style="border: 0px; background-color: transparent; height: 26px; width: 63px; cursor: hand;" type="button" onClick="valida('PublicarNoticia',this.form);">
                     	  <img src="<%=path%>/images/bot_incluir_det1.gif"  width="63px" height="26px" style="cursor: hand;" > </button>
               	        </div>
				  </td>
                  <td width="35%">
				      	<div align="center">
				      	  <button id="btAlterar" style="border: 0px; background-color: transparent; height: 26px; width: 63px; cursor: hand;" type="button" onClick="valida('AlterarNoticia',this.form);">
				      	  <img src="<%=path%>/images/bot_alterar_det1.gif"  width="63px" height="26px"  style="cursor: hand;" > </button>
	      	      </div>
				  </td>

				  <td>
				   <div align="center">
					  <font color="#FFFFFF">
					  <button  style="border: 0px; background-color: transparent; height: 26px; width: 63px; cursor: hand;" type="button" onClick="esconde('WK_SISTEMA'),esconde('btIncluir'),esconde('btAlterar'),mostra('LISTA'),limparcampos(this.form);">
					  <img src="<%=path%>/images/retornar.gif"  width="52px" height="19px" style="cursor: hand;" > </button>
					</font></div>	  
				  </td>	  
                </tr>
              </table>              
 	    </td>
      </tr>
    </table>
</div>

<div id="LISTA" style="position:absolute; overflow: visible; z-index: 100; left:53px; top: 74px; width:720px; background-color: #FFFFFF; layer-background-color: #FFFFFF; height: 258px; border: 1px none #000000;" > 
  <table width=711 border="0" align="center" cellpadding="1">
    <tr valign="top" bgcolor="<%=Cor%>">
    <tr>
      <td width="343">
<%
     String sstrr1="",sstrr2="",sstrr= request.getParameter("indPublicacao");
     if( (sstrr == null) || (sstrr.equals(""))) { sstrr1="checked"; sstrr2=""; }
     else
	 if( sstrr.equals("P") ) { sstrr1="checked"; sstrr2=""; }
	 else
     if( sstrr.equals("N") ) {sstrr2="checked"; sstrr1="";}
%>        <div align="center"><strong><font color="#666666">Publicadas</font></strong>
            <input <%=sstrr1%> name="ind_Public" type="radio" class="sem-borda" style="width: 13px; height: 13px; value="P" onClick="valida('P',this.form);">
      </div>
	  </td>
      <td>
        <div align="center"><strong><font color="#666666">Publica��es anteriores</font></strong>
            <input <%=sstrr2%> name="ind_Public" type="radio" class="sem-borda" style="width: 13px; height: 13px; value="N" onClick="valida('PA',this.form);">
      </div></td>
    </tr>
  </table>
  <table width=713 height="76" border="0" align="center" cellpadding="0" cellspacing="0">
	<tr>
		<td width="707">
			<table width=711 border="0" align="center" cellpadding="1" cellspacing="2">
			   <tr valign="top" bgcolor="<%=Cresult%>"> 
				 <td width=383  bgcolor="<%=Cresult%>">  <div align="left"><strong><font color="#ffffff"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Assunto</font></strong></div></td>
				 <td width=72  bgcolor="<%=Cresult%>">   <div align="center"><strong><font color="#ffffff">In�cio</font></strong></div> </td>
				 <td width=78   bgcolor="<%=Cresult%>">  <div align="center"><strong><font color="#ffffff">T�rmino</font></strong></div></td>
				 <td  bgcolor="<%=Cresult%>">   <div align="center"><strong><font color="#ffffff">Prioridade</font></strong></div></td>
			   </tr>
		</table>		
	  </td>	
	</tr>
	<tr>
	  <td >
	<!--Inicio da lista-->
    <div id="Layer1" style="position: relative; width:698; z-index:1; overflow: auto; height: 170;"> 
      <table width="100%" border="0" align="center" cellpadding="0">
      <% 
     	int total = NoticiaBeanId.getVetNoticias().size();
        for ( int i=0; i < total; i++ ) 
        {
      %> 
         <tr bgcolor="<%=Cor%>"> 
	       <td height="21" width=32>
		      <button style="border: 0px; background-color: transparent; height: 14px; width: 22px; cursor: hand;" type="button" onClick="esconde('btIncluir'),mostra('btAlterar'),mostra('WK_SISTEMA'),esconde('LISTA'),CarregaDiv(this.form,cod_Noticia,dsc_Noticia,txt_Noticia,ind_Prioridade,dat_Inicio,dat_Fim,dat_Envio,<%=i%>);">
			  <img src="<%=path%>/images/lapis.gif" alt="Para fazer altera��o da noticia."  width="21px" height="13px"  style="cursor: hand;" >
		      </button>
		   </td>
           <td width=351>
 			                <input name="cod_Noticia" type="hidden" value="<%= NoticiaBeanId.getVetNoticias(i).getCodNoticia() %>">
 			                <input name="dsc_Noticia" readonly size="60" maxlength="100" type="text" value="<%= NoticiaBeanId.getVetNoticias(i).getDscNoticia()%>"> 			    </td>
           <td width=72>    <input name="dat_Inicio"  readonly size="12" maxlength="10"  type="text" value="<%= NoticiaBeanId.getVetNoticias(i).getDatInicioStr()%>"> </td>
           <td width=78 >    <input name="dat_Fim"    readonly  size="12" maxlength="10"  type="text" value="<%= NoticiaBeanId.getVetNoticias(i).getDatFimStr()%>">    </td>
	       <td>   
           <% String strss="";
		      if(NoticiaBeanId.getVetNoticias(i).getIndPrioridade().equals("A")) strss="Alta";
			  else strss="Normal";	 
		   %>
				  <input name="ind_Prioridade" readonly size="6"  maxlength="6"  type="text" value="<%=strss %>">				  <input name="txt_Noticia"  type="hidden" value="<%= NoticiaBeanId.getVetNoticias(i).getTxtNoticia()%>">		          <input name="dat_Envio"    type="hidden" value="<%= NoticiaBeanId.getVetNoticias(i).getDatEnvio()%>">	            </td>
           </tr>
      <% } %> 
     </table>
   </div></td>
</tr>
</table>
  <table width="100%"  border="0">
    <tr>
      <td>
	   <div align="center">
       	  <font color="#FFFFFF">
       	  <button  style="border: 0px; background-color: transparent; height: 26px; width: 63px; cursor: hand;" type="button" onClick="esconde('LISTA'),esconde('btAlterar'),mostra('btIncluir'),mostra('WK_SISTEMA'),limparcampos(this.form);">
      	  <img src="<%=path%>/images/bot_incluir_det1.gif"  width="63px" height="26px" style="cursor: hand;" > </button>
   	    </font></div>	  
 	  </td>
    </tr>
  </table>
  </div>
<!--fim da lista-->

<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->
<!--Div Erros-->
<%
  String msgErro   = NoticiaBeanId.getMsgErro();  if(msgErro==null) msgErro ="";     
  String mostraMsg = "hidden" ;   
  if( msgErro.trim().length() !=0 ) mostraMsg = "visible" ;
%>
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= msgErro %>" />
  <jsp:param name="msgErroTop"  value= "250 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>