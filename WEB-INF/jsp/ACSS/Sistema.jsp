<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
	// se movimentacao no arquivo - nao precisa validar os campos
	if ("Top,Proximo,Anterior,Fim,Novo,Pkid".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
		fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	    return ;
	 }
	 // se inclusao ou alteracao - valida todos os campos
	 if ("12".indexOf(opcao)>=0) { 		  
	      if (veCampos(opcao,fForm)==false) return ;
	  }	  
	 // inclusao, alteracao ou exclusao 
	 if ("123".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  } 
	 switch (opcao) {    
	   case 'R':
	    close() ;
		break;
	   case 'C':
		  if (document.layers) fForm.layers["SistemaCons"].visibility='show' ; 
	      else                 document.all["SistemaCons"].style.visibility='visible' ; 
		  break;
	   case 'I':   // Imprimir 
		  temp = fForm.j_cmdFuncao.value
		  fForm.j_cmdFuncao.value = "construcao" ;
	      fForm.target= "_blank";
		  fForm.action = "acessoTool";  		  
	      fForm.submit();	
		  fForm.j_cmdFuncao.value = temp ;		    
		  break;
	   case 'L':  // Limpar
		  document.UsuForm.reset() ;
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;	 
  }
}
function veCampos(opcao,fForm) {
	valid = true ;
	sErro = "" ;

	verString(fForm.nomSistema,"Nome do Sistema",0);
	verString(fForm.nomAbrev,"Abreviatura do Sistema",0);

	if (valid==false) alert(sErro) 
	return valid ;
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" OnLoad="document.UsrForm.nomSistema.focus();" style="overflow: hidden;">

<form name="UsrForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>				
<input name="pkid"              type="hidden" value="<%=SistId.getPkid()%>">


<!--IN�CIO_CORPO_sistema--> 
<div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1; left:35px; top: 95px; width:730px; height:130px;" > 
   <!--Botoes navega��o-->
   <jsp:include page="../sys/BotoesNav.jsp" flush="true" />
   <!--FIM_Botoes navega��o-->

  <table width="100%"  height="100%" border="0" cellpadding="3" cellspacing="0">
       <tr><td  height="10" valign="top"><img src="images/inv.gif" width="3" height="3"></td></tr>
       <tr> 
         <td  height="120" valign="middle"> 
            <table width="80%" border="0" align="center" cellpadding="3" cellspacing="1">
              <tr bgcolor="#DFEEF2"> 
                <td width="25%" height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;C&oacute;digo 
                    do Sistema</font></strong></td>
                <td height="25"><strong><font color="#336699">:</font></strong> 
                    <input disabled name="codSistema" type="text" id="codSistema" size="13" maxlength="10" value='<%=SistId.getCodSistema() %>' onkeypress="javascript:f_num();" onfocus="javascript:this.select();" > 
                </td>
              </tr>
              <tr bgcolor="#DFEEF2"> 
                <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Nome 
                    do Sistema</font></strong></td>
                <td height="25"><strong><font color="#336699">:</font></strong> 
                    <input name="nomSistema" type="text" size="70" maxlength="50" value='<%=SistId.getNomSistema() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()"></td>
              </tr>
              <tr bgcolor="#DFEEF2"> 
                <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Abreviatura</font></strong></td>
                <td height="25"><strong><font color="#336699">:</font></strong> 
                    <input name="nomAbrev" type="text"  size="20" maxlength="15" value='<%=SistId.getNomAbrev() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onKeyUp="this.value=this.value.toUpperCase()"></td>
              </tr>
              <tr bgcolor="#DFEEF2"> 
                <td height="25"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Vers&atilde;o</font></strong></td>
                <td height="25"><strong><font color="#336699">:</font></strong> 
                    <input name="sigVersao" type="text"  size="13" maxlength="10" value='<%=SistId.getSigVersao() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
              </tr>
               <tr><td height="10"></td></tr>			  
            </table>
         </td>
      </tr>
  </table>
  <!--Botoes padr�o-->
  <jsp:include page="../sys/Botoes.jsp" flush="true" />		
  <!--FIM_Botoes padr�o-->
</div>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= SistId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "130 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
  
<!--FIM_CORPO_sistema--> 
<jsp:include page="sistemaConsulta.jsp" flush="true" > 
  <jsp:param name="SistemaConsTop"  value= "250 px" />
  <jsp:param name="SistemaConsLeft" value= "0 px" />  
</jsp:include> 
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
</form>
</body>
</html>

