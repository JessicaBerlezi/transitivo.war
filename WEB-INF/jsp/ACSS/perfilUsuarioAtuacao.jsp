<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"       scope="request" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Perfil/Orgao/Atuacao -->
<jsp:useBean id="PerfilAtuBeanId" scope="request" class="ACSS.PerfilUsuarioAtuacaoBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistBeanId"      scope="request" class="ACSS.SistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaosACSS(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}
myUsr        = new Array(<%= UsrBeanId.getUsuarios() %>);
cod_usuario  = new Array(myUsr.length/3);
nom_username = new Array(myUsr.length/3);
for (i=0; i<myUsr.length/3; i++) {
	cod_usuario[i] = myUsr[i*3+0] ;
	nom_username[i] = myUsr[i*3+1] ;
}

mySist      = new Array(<%= SistBeanId.getSistemas(UsuarioBeanId.getCodUsuario()) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
	  if (fForm.codOrgao.value==0) {
		alert("�rg�o n�o selecionado.") ;
	  }
	  else {	
	    fForm.acao.value=opcao
		   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'buscaSistemas':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codUsuario.value==0) {
	   valid = false
	   sErro += "Usu�rio n�o selecionado. \n"
	}
   if(fForm.codSistema.value == 0){
       valid = false
       sErro += "Sistema n�o selecionado. \n"
    }
    if (valid==false) alert(sErro)
	return valid ;
}
 
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>				

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
   		<% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td  width="16%"><b>�rg�o Lota��o:&nbsp;</b> </td>
			  <td width="55%" >
					<select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select>
			  </td>
			  <td width="23%"><b>Usu�rio:&nbsp;</b>
				<select name="codUsuario"> 
					<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_usuario.length;j++) {
							document.write ("<option value="+cod_usuario[j]+">"+nom_username[j])+"</option>"
						}
					</script>
				</select>
			  </td>	
	          <td width="6%">
			  		<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistemas',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>
			</tr>
			<tr><td colspan="2" height="2"></td></tr>	 
			<tr>  	  
  			  <td width="16%"><b>Sistema:&nbsp;</b></td>
		      <td > 
 				  <select name="codSistema"> 
					<option value="0" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_sistema.length;j++) {
							document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
						}
					</script>
		         </select>
			  </td>    
			  </tr>
			  </table>  
          <% }
			else { %>
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
		   <tr>
			  <td width="28%"><b>Org�o Lota��o:&nbsp;</b></td>
			  <td width="55%" >
	            <input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
			  <input disabled name="sigOrgao" type="text"  size="15" maxlength="10" value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >			  </td>
			  <td width="23%"></td>
			  <td><b>Usu�rio:&nbsp;</b>
	            <input name="codUsuario"           type="hidden" size="14" maxlength="10" value="<%= UsrBeanId.getCodUsuario() %>"> 
        		<input disabled name="nomUserName" type="text"  size="30" maxlength="20" value="<%= UsrBeanId.getNomUserName() %>" style="border: 0px none;">
			  </td>		
			</tr>
			<tr><td colspan="2" height="2"></td></tr>	 
			<tr> 
			  <td><b>Sistema:&nbsp;</b></td>
			  <td >
	            <input name="codSistema"          type="hidden" size="14" maxlength="10" value="<%= SistBeanId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"  size="60"  maxlength="50" value="<%= SistBeanId.getNomSistema() %>" style="border: 0px none;">
		 	  </td>		
	      <% } %>			  
      </tr>	
      <tr><td colspan="2" height="2"></td></tr>	 
    </table>    
	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	 
	  	<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>			 
	 		<td align="right">
			  <button type="button" NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">
			  	<IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align="left"  >
			  </button>
			</td>	
	  		<td width="20%"> </td>
			<td> 
	  		  <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
				  <img src="<%= path %>/images/bot_retornar.gif" width="52" height="19" >
			  </button>
			</td>
	   <% } %>	
    </table>
</div>


<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 148px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr><td colspan=4 height=3></td></tr>		  		
	<tr > 	
	   	 <td width="100"></td>		 		 	  
	   	 <td width="40" align="center" bgcolor="#79B3C4"><font color="#ffffff"><b>SEQ</b></font></TD>		 		 	  
	   	 <td width="150"  bgcolor="#79B3C4"><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;ORG�O</b></font></TD>		 
	   	 <td width="300" align="center" bgcolor="#79B3C4"><font color="#ffffff"><b>PERFIL</b></font></TD>		 
	   	 <td >&nbsp;</td>		 		 	  
	</tr>
   </table>
</div>   

<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>				  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 168px; left: 50px; height: 172px; visibility: visible;">   
   <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator itOrg = PerfilAtuBeanId.getPerfisOrgao().iterator() ;
		int seq = 1;	
		ACSS.PerfilUsuarioAtuacaoBean myOrg =  new ACSS.PerfilUsuarioAtuacaoBean() ;
        while (itOrg.hasNext()) {
			// lista os orgaos de atuacao			
			myOrg = (ACSS.PerfilUsuarioAtuacaoBean)itOrg.next() ;
	%>
		<tr> 	
		   	 <td width="100"></td>		 		 	  	
		   	 <td width="40" align="center" bgcolor="#DFEEF2"><%= seq %></TD>		 		 	  
		   	 <td width="150" bgcolor="#DFEEF2">&nbsp;&nbsp;&nbsp;
		   	 	<input name="codOrgaoAtuacao" type="hidden" size="13" maxlength="10" value='<%=myOrg.getCodOrgaoAtuacao()%>'>		 
          		<%= myOrg.getSigOrgaoAtuacao() %></TD>		 
	         <td width=300 align="center"  bgcolor="#DFEEF2">    
				<select name="codPerfilNov"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			      <%
			        Iterator itPerf = PerfilAtuBeanId.getPerfis().iterator() ;
		 		    ACSS.PerfilBean myPerf =  new ACSS.PerfilBean ();
	                    while (itPerf.hasNext()) {
			          	 	// lista os perfis do sistema selecionado			
			           		myPerf = (ACSS.PerfilBean)itPerf.next() ;
					 %>
					   <option value="<%= myPerf.getCodPerfil() %>" <%= sys.Util.isSelected(myPerf.getCodPerfil(), myOrg.getCodPerfil())%>><%= myPerf.getNomDescricao() %></option>
    		 		<%
			 		}    
				  %>
		       </select>	      
		    </TD>	
		   	 <td>&nbsp;</td>		 		 	  
				 
 	   <% 
	   		seq++;
	   } 
	   %>	

   </table>    
   <% } %>	 
</div>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= OrgaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codOrgao.length;i++){
		if(document.UsrAtuForm.codOrgao.options[i].value==njint){
			document.UsrAtuForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>			  
</form>
</BODY>
</HTML>

