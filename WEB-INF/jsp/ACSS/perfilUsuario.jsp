<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistBeanId" scope="request" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto da Tabela de Perfil -->
<jsp:useBean id="PerfBeanId" scope="request" class="ACSS.PerfilBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId" scope="request" class="ACSS.UsuarioBean" /> 

<jsp:setProperty name="UsrBeanId" property="j_abrevSist" value="ACSS" />  
<jsp:setProperty name="UsrBeanId" property="popupExt"      value=" onChange=\"javascript: setaSist()\"" />
<jsp:setProperty name="UsrBeanId" property="colunaValue" value="cod_Usuario" />
<jsp:setProperty name="UsrBeanId" property="popupNome"   value="codUsuario"  />
<jsp:setProperty name="UsrBeanId" property="popupString" value="nom_Usuario,SELECT cod_Usuario,nom_Usuario FROM TCAU_USUARIO ORDER BY nom_usuario" />


<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 
myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

mySist      = new Array(<%= SistBeanId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}
myPerfs      = new Array(<%= PerfBeanId.getPerfis() %>);
cod_sistPerf = new Array(myPerfs.length/3);
cod_perfil   = new Array(myPerfs.length/3);
nom_perfil   = new Array(myPerfs.length/3);
for (i=0; i<myPerfs.length/3; i++) {
    cod_sistPerf[i] = myPerfs[i*3+0]
	cod_perfil[i]   = myPerfs[i*3+1] ;
	nom_perfil[i]   = myPerfs[i*3+2] ;
}


function valida(opcao,fForm) {
  // se inclusao ou alteracao - valida todos os campos
  if ("1".indexOf(opcao)>=0) { 		  
      if (veCampos(opcao,fForm)==false) return ;
      if (veCampos2(opcao,fForm)==false) return ;	  
  }	  
  // inclusao, alteracao ou exclusao 
  if ("13".indexOf(opcao)>=0) { 
		perfilForm.j_jspDestino.value = "perfilUsuario.jsp"	;												
		perfilForm.acao.value=opcao;
	   	perfilForm.target= "_self";
	    perfilForm.action = "acessoTool";  
	   	perfilForm.submit();	  		  
  } 

 switch (opcao) {
   case 'buscaUsuarios':
      if (veCamposOrg(fForm)==true) {
		perfilForm.j_jspDestino.value = "perfilUsuario.jsp"	;												
		perfilForm.acao.value=opcao;
	   	perfilForm.target= "_self";
	    perfilForm.action = "acessoTool";  
	   	perfilForm.submit();	  		  
	  }
	  break ;
 
   case 'buscaPerfis':
      if (veCampos(fForm)==true) {
		perfilForm.j_jspDestino.value = "perfilUsuario.jsp"	;												
		perfilForm.acao.value=opcao;
	   	perfilForm.target= "_self";
	    perfilForm.action = "acessoTool";  
	   	perfilForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;	 
  }
}
function veCamposOrg(fForm) {
valid   = true ;
sErro = "" ;
if (perfilForm.codOrgao.value==0) {
   valid = false
   sErro = sErro + "Org�o n�o selecionado. \n"
}
if (valid==false) alert(sErro)
return valid ;
} 

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (perfilForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}
if (perfilForm.codOrgao.value==0) {
   valid = false
   sErro = sErro + "Org�o n�o selecionado. \n"
}
if (perfilForm.codUsuario.value==0) {
   valid = false
   sErro = sErro + "Usuario n�o selecionado. \n"
}
if (valid==false) alert(sErro)
return valid ;
} 
function veCampos2(fForm) {
	valid   = true ;
	sErro = "" ;
	if (perfilForm.codPerfil.value==0) {
	   valid = false
	   sErro = sErro + "Perfil n�o selecionado. \n"
	}
	if (valid==false) alert(sErro)
	return valid ;
} 
function setaPerfil(objComboEscolha,objComboMontar) {
	var codSist =  objComboEscolha.value
	// esvazia combo de grupos
	objComboMontar.length = 1
	objComboMontar.options[0] = new Option(" ")
	k=1	
	for(i=0;i<myPerfs.length;i++){
	    if(cod_sistPerf[i]==codSist){		
		   objComboMontar.options[k] = new Option(nom_perfil[i],cod_perfil[i])
	       k++	
	    }
	}
}
function setaSist() {
       document.perfilForm.codSistema.selectedIndex=0;
 }	
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="perfilForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>				
<input name="pkid"              type="hidden" value="<%=PerfBeanId.getPkid()%>">
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 204px; top: 55px; width:468px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
	<tr><td width="12%" align="left"><font color="#000000"><strong>CONTROLE DE ACESSO > Cadastrar Perfil por Usu&aacute;rio</strong></font></td>
	</tr>
  </table>
</div>
<!--FIM T�TULO DO M�DULO-->
<div id="WK_SISTEMA" style="position:absolute; overflow: visible; z-index: 1; top: 94px; width:750px; left=35px;" > 
  <!--INICIO CABEC DA TABELA-->
  <table border='0' cellpadding="0" cellspacing="0" width=90%  align=center>    
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
      <tr bgcolor="#DFEEF2">
		  
        <td width="20%" align=left><b>&nbsp;&nbsp;&nbsp;�rg�o:&nbsp;</b> </td>	  
		  
        <td width="60%"> 
          <select name="codOrgao" onChange="javascript: valida('buscaUsuarios')"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_orgao.length;j++) {
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
        </td>
          
        <td width="20%" align=left></td>
      </tr>	
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
      <tr bgcolor="#DFEEF2">
		  
        <td align=left><b>&nbsp;&nbsp;&nbsp;Usu�rio:&nbsp;</b> </td>	  
		  
        <td> 
          <jsp:getProperty name="UsrBeanId" property="popupString" />
		  </td>
 	  	 
        <td></td>
      </tr>	
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
      <tr bgcolor="#DFEEF2">
		  
        <td align=left><b>&nbsp;&nbsp;&nbsp;Sistema:&nbsp;</b> </td>	  
		  <td>
			<select name="codSistema" onChange="javascript: valida('buscaPerfis')"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
		  </td>
          <td align=left></td>
	  </tr>
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
      <tr bgcolor="#DFEEF2">
		  <td align=left><b>&nbsp;&nbsp;&nbsp;Perfil:&nbsp;</b></td>	  
		  <td>
	       <script>
			  if ( document.forms[0].pkid.value == 0) {
				  	document.write("<input name=\"nomDescricao\" type=\"hidden\"   size=\"50\" maxlength=\"50\"  value=\"<%= PerfBeanId.getNomDescricao() %>\">");
					document.write("<select name=\"codPerfil\"> ");
					document.write("<option value=\"0\" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>");
					document.write("</select>");
				}
   			  else {
			  	document.write("<input readonly name=\"nomDescricao\" type=\"text\"   size=\"50\" maxlength=\"50\"  value=\"<%= PerfBeanId.getNomDescricao() %>\">");
			  	document.write("<input name=\"codPerfil\"    type=\"hidden\"  value=\"<%= PerfBeanId.getCodPerfil() %>\">");
			}
		  </script>
		  </td>
          <td align=left></td>
	  </tr>
      <tr> 
        <td valign="top" colspan=3><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
      <tr bgcolor="#DFEEF2">
		  
        <td align=left><b>&nbsp;&nbsp;&nbsp;N&iacute;vel:&nbsp;</b> </td>	  
		  <td>
			<select name="sigNivel"> 
				<option value="0">Usu�rio</option>
				<option value="1">Administrador Local</option>
				<option value="2">Administrador Global</option>												
			</select>
		  </td>
          <td align=left></td>
	  </tr>
      <tr> 
        <td valign="top" colspan=4><img src="images/inv.gif" width="3" height="20"></td>
      </tr> 
  </table>
<table width=90% border='0' align="center" cellpadding="0" cellspacing="0">
     <tr> 
	 	<td width=20% height="20"></td>
		<td width=30% align=center>	 
	       <!-- coloca botao incluir ou de alteracao e exclusao -->
 	      <script>
		  if ( document.forms[0].pkid.value == 0) {
			document.write("<button type=button NAME=\"incluir\"   style=\"height: 28px; width: 64px;border: none; background: transparent;\" onClick=\"javascript: valida('1',this.form);\">") ;	
			document.write("  <IMG src=\"<%= path %>/images/bot_incluir_det1.gif\"  align=left  ></button>") ;
		  }
		  else {
			document.write("<td><button type=button NAME=\"excluir\"   style=\"height: 28px; width: 65px;border: none; background: transparent;\" onClick=\"javascript: valida('3',this.form);\">") ;	
			document.write("   <IMG src=\"<%= path %>/images/bot_excluir_det1.gif\"  align=left  ></button></td>") ;
			}
	   </script>
	   </td>
       <td width=30% align=center><button type=button NAME="retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                     <IMG src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" align="left"  ></button>
       </td>
	   <td width=20%></td>
     </tr>
</table>			  

</div>
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= PerfBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "300 px" />
  <jsp:param name="msgErroLeft" value= "130 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<script>			  
// posiciona o popup na descri�ao equivalente ao Usuario
nj = '<%= UsrBeanId.getCodUsuario() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.perfilForm.codUsuario.length;i++){
    if(document.perfilForm.codUsuario.options[i].value==njint){
       document.perfilForm.codUsuario.selectedIndex=i;	  
    }
  }
}  

// posiciona o popup na descri�ao equivalente ao Orgao
nj = '0'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.perfilForm.codOrgao.length;i++){
    if(document.perfilForm.codOrgao.options[i].value==njint){
       document.perfilForm.codOrgao.selectedIndex=i;	  
    }
  }
}  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= PerfBeanId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.perfilForm.codSistema.length;i++){
    if(document.perfilForm.codSistema.options[i].value==njint){
       document.perfilForm.codSistema.selectedIndex=i;	  
    }
  }
}  
// posiciona o perfil
if ( document.forms[0].pkid.value == 0) {
	setaPerfil(document.perfilForm.codSistema,document.perfilForm.codPerfil) ;
}
// posiciona o popup na descri�ao equivalente ao Nivel do Usuario
nj = '<%= PerfBeanId.getCodNivel() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.perfilForm.sigNivel.length;i++){
    if(document.perfilForm.sigNivel.options[i].value==njint){
       document.perfilForm.sigNivel.selectedIndex=i;	  
    }
  }
}  

</script>
 
</form>
</BODY>
</HTML>


