<!--M�dulo : REG
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page import="java.util.Iterator" %>
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" />
<jsp:useBean id="consultHistParamOrgBean" scope="session" class="ACSS.ConsultaHistoricoParamOrgBean" />

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>   
<head>

<jsp:include page="CssImpressao.jsp" flush="true" />
<style type="text/css">
DIV.quebrapagina{page-break-after: always}
</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	color: #000000;
	font-weight: bold;
}
.style2 {color: #000000}
-->
</style>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int npag = 0,seq = 0, contLinha = 0;	
  	Iterator it = consultHistParamOrgBean.getDados().iterator();
 	ACSS.ConsultaHistoricoParamOrgBean relatorioImp  = new ACSS.ConsultaHistoricoParamOrgBean();					
	if( consultHistParamOrgBean.getDados().size()>0 ) { 
		while (it.hasNext()) {
			   relatorioImp = (ACSS.ConsultaHistoricoParamOrgBean)it.next() ;
			   seq++; 
			   contLinha++;//Controla a contagem de as linhas de cada bloco de cod infracao
		       if (contLinha%18==1){
					npag++;							
					if (npag!=1){
%> 
		  		  <jsp:include page="rodape_impConsulta.jsp" flush="true" />
		  		  <div class="quebrapagina"></div>
<%					} %>	
		  		  <jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			        <jsp:param name="nPag" value= "<%=npag%>" />				
			      </jsp:include> 

			<table width="100%" border="0" class="semborda" align="center" cellpadding="0" cellspacing="0">  	   		
			  <tr><td height="10"></td></tr>
			  <tr>	
			      <td ><strong class="sem-borda"><font color="#000000">&nbsp;&nbsp;&Oacute;rg&atilde;o:&nbsp;</font></strong><%=OrgaoBeanId.getSigOrgao()%> - Grupo: <%= consultHistParamOrgBean.getGrupo()%></TD> 
			  </tr>
			  <tr><td height ="10"></td></tr>
			</table>  
	        <table width="100%" border="0" class="semborda"  align="center" cellpadding="0" cellspacing="0">  	   		
		 		 <tr bgcolor="#cccccc" class="td_linha">
					<td width="48"  align="center" rowspan ="3" height="15" ><span class="style1">SEQ</span></td>
					<td width="100"  align="center"  height="15"><span class="style1">DATA</span></td>
					<td width="500" align="center"  ><span class="style1">PAR&Acirc;METRO</span></td>
					<td align="center" style="border-right: 1px solid #000000" ><span class="style1">USU&Aacute;RIO</span></td>
				 </tr>
				 <tr class="td_linha">
  					 <td  bgcolor="#cccccc" colspan="4" style="border-right: 1px solid #000000" height="15"><span class="style2">&nbsp;&nbsp;<b>VALOR ANTERIOR</b></span></td>
				 </tr>
				 <tr class="td_linha">
  					 <td  bgcolor="#cccccc" colspan="4" style="border-right: 1px solid #000000" height="15"><span class="style2">&nbsp;&nbsp;<b>VALOR ATUAL</b></span></td>
				 </tr>
  </table>     
			<%}%>
		    <table id="LogsImp" width="100%" class="semborda" border="0" cellpadding="0" cellspacing="0" align="center">							
  				<tr class="td_linha_semborda"> 
				  <td width="48" rowspan="3"   align="center" style="line-height:15px; border-bottom: 0px none;" ><%=seq%></TD>
				  <td width="100"   align="center" height="15"><font color="#000000"><%=relatorioImp.getDatLog()%></font></TD>
	 		      <td width="500"  ><font color="#000000">&nbsp;&nbsp;<%=relatorioImp.getNomDescNew()%></font></TD>
				  <td align="center" style="border-right: 1px solid #000000"><font color="#000000"><%=relatorioImp.getNomUsrName()%></font></TD>
			    </tr>
			    <tr class="td_linha">
			      <td  colspan="4" style="border-right: 1px solid #000000" height="15">&nbsp;&nbsp;<font color="#000000"><%=relatorioImp.getValParamOld()%></font></TD>
				</tr>
				<tr class="td_linha_semborda">  
 				  <td colspan="4" style="border-right: 1px solid #000000;" height="15">&nbsp;&nbsp;<font color="#000000"><%=relatorioImp.getValParamNew()%></font></TD>
				</tr>

	       </table>
   <% }%>
<%} else { 
	String msg=(String)request.getAttribute("semLog"); if (msg==null) msg="";
%>
	<div class="quebrapagina"></div>
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="0">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
   	      <td height="35" align="center" bgcolor="#CCCCCC"><b><%= msg %></b></td>
		</tr>	
	</table>      			
<%} %>    
  <jsp:include page="rodape_impConsulta.jsp" flush="true" />

</form>
</body>
</html>