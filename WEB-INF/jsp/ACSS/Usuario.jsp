<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Orgao -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Usuario -->
<jsp:useBean id="UsrBeanId" scope="request" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto da Tabela de Protocolo UPO -->
<jsp:useBean id="protocoloUpoBeanId" scope="request" class="ACSS.ProtocoloUpoBean" />  

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: Manutencao Tabela de Usuarios :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

myProtUpo     = new Array(<%= protocoloUpoBeanId.getProtocolosUpo(  UsuarioBeanId  ) %>);
cod_prot_upo = new Array(myProtUpo.length/2);
nm_prot_upo = new Array(myProtUpo.length/2);
for (i=0; i<myProtUpo.length/2; i++) {
	cod_prot_upo[i] = myProtUpo[i*2+0] ;
	nm_prot_upo[i] = myProtUpo[i*2+1] ;
}

function valida(opcao,fForm) {
	// se movimentacao no arquivo - nao precisa validar os campos
	if ("Top,Proximo,Anterior,Fim,Novo,Pkid".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
		fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	    return ;
	 }
	 // se inclusao ou alteracao - valida todos os campos
	 if ( ("12".indexOf(opcao)>=0) && (veCampos(opcao,fForm)==false) ) return ;
	  
	 // se exclusao - atualiza campo radio
	 // inclusao, alteracao ou exclusao 
	 if ("123".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  } 
	 switch (opcao) {    
	   case 'R':
	    close() ;
		break;
	   case 'C':
		  if (document.layers) fForm.layers["UsuarioCons"].visibility='show' ; 
	      else                 document.all["UsuarioCons"].style.visibility='visible' ; 
		  break;
	   case 'I':   // Imprimir
			temp = fForm.j_cmdFuncao.value	   
			fForm.j_cmdFuncao.value = "construcao" ;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  		  
			fForm.submit();	  
		    fForm.j_cmdFuncao.value = temp ;				
			break;
	   case 'L':  // Limpar
		  document.UsuForm.reset() ;
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;
	  case 'Biometria':
      	if (veCampos(opcao,fForm)==true) { 
			fForm.acao.value=opcao;
	   		fForm.target= "_blank";
	    	fForm.action = "acessoTool";  
	   		fForm.submit();	  		  
	  	}
	  	break;	 
  }
  
}
function veCampos(opcao,fForm) {
	fForm.codOrgao.value=trim(fForm.codOrgao.value)
	fForm.nomUsuario.value=trim(fForm.nomUsuario.value)
	fForm.nomUserName.value=trim(fForm.nomUserName.value)
	fForm.codSenha.value=trim(fForm.codSenha.value)
	sErro = "" ;
	valid = true ;
	verString(fForm.codOrgao,"�rg�o",0);
	verString(fForm.nomUsuario,"Nome do Usu�rio",0);
	verString(fForm.nomUserName,"Login obrigat�rio.",0);
	verString(fForm.codSenha,"Senha",4);
	// validar as datas
	valid = ValDt(fForm.datCadastro,1) && valid
	valid = ValDt(fForm.datValidade,1) && valid
	
	<!--Verifica se o CPF n�o preenchido-->	

       var cpf = trim(fForm.numCpf.value);
	   if(cpf.length==0){
   		  valid = false
   		  sErro = sErro + "O CPF deve ser preenchido. \n";
       }
      if (cpf.length != 0){
	     if(veCPF(fForm.numCpf, 1)== true){
		   valid = true
		 }
		 else{
		   valid = false
		 }
      } 
      
 	if (valid==false) alert(sErro) 
	return valid ;
}

function atuRadio(objradio,objind) {
// validar campo de radio e setar o hidden para Bean == verifica se tem pelo menos 1 checado
	naochq = true
	for (i=0; i<objradio.length; i++) {
    	if (objradio[i].checked) {
		   naochq = false ;
		   objind.value = objradio[i].value ;
		   break ;
		   }
	}
	return naochq ;
}

function veCPF(obj,forma) {
  <!-- editar a celula -->
  valor = trim(obj.value)

  if (valor.length != 0) {     
     valor = Tiraedt(valor,11);
   <!-- validar se cpf se preenchido-->
      if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {
		     alert("CPF com digito verificador invalido: "+obj.value);
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido: "+obj.value+" /n"		 
		 }
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}

function verifica_email(myobj) {
	if (valida_email(myobj)==false) {
		alert("Email inv�lido.")
	}
}
function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" OnLoad="document.UsrForm.nomUsuario.focus();" style="overflow: hidden;">
<form name="UsrForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>				
<input name="pkid"              type="hidden" value="<%=UsrBeanId.getPkid()%>">
<input name="codUsrResp"        type="hidden" value="<%=UsrBeanId.getCodUsrResp()%>">
<input name="codUsrRespAlt"     type="hidden" value="<%=UsrBeanId.getCodUsrRespAlt()%>">
<input name="DatCadastroAlt"    type="hidden" value="<%=UsrBeanId.getDatCadastroAlt()%>">
<!--IN�CIO_CORPO_sistema--> 
<div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1; left:50px; top: 69px; width:720px;" > 
     <!--Botoes navega��o-->
     <jsp:include page="../sys/BotoesNav.jsp" flush="true" />
     <!--FIM_Botoes navega��o-->
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr><td height="5" colspan="2"></td></tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20" width="25%"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Nome 
                do Usu&aacute;rio </font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="nomUsuario" type="text" size="70" maxlength="50"  value='<%=UsrBeanId.getNomUsuario() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
			  </td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            </tr>
            
            <tr bgcolor="#DFEEF2"> 
              <td height="20" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;CPF do Usu�rio</font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="numCpf" type="text" size="19" maxlength="14"  value="<%= UsrBeanId.getNumCpfEdt()%>" onkeypress="javascript:Mascaras(this,'999.999.999-99');" onfocus="javascript:this.select();" onChange = "javascript:veCPF(this,0);"></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            
			<tr bgcolor="#DFEEF2"> 
              <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;�rg�o Lota&ccedil;&atilde;o </font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
				<select name="codOrgao" onChange="javascript: valida('buscaUsuarios')"> 
				<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_orgao.length;j++) {
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
			  </td>
            </tr>
            <tr>
              <td height="1" colspan="2"></td>
            </tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Login 
                </font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="nomUserName" type="text" size="27" maxlength="20" value='<%=UsrBeanId.getNomUserName() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            </tr>
            </tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Senha</font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="codSenha" type="password" size="13" maxlength="10" value='<%=UsrBeanId.getCodSenha() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();"></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            </tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Data 
                do Cadastro </font></strong></td>
              <td ><strong><font color="#336699">: 
                </font></strong><font color="#00000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><input type="text" name="datCadastro" disabled size="12" maxlength="10" value='<%=UsrBeanId.getDatCadastro() %>' onchange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" onkeypress="javascript:f_hora();"></font></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            </tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Data 
                de Validade </font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <font color="#000000" size="1" face="Verdana, Arial, Helvetica, sans-serif"><input type="text" name="datValidade" size="12" maxlength="10" value='<%=UsrBeanId.getDatValidade() %>'onchange="javascript:ValDt(this,0);" onfocus="javascript:this.select();" onkeypress="javascript:f_hora();"></font></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
            </tr>
            <tr bgcolor="#DFEEF2"> 
              <td height="20" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Hora de Validade</font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="horaValidade" type="text" size="09" maxlength="04"  value="<%= UsrBeanId.getHoraValidade()%>"  onfocus="javascript:this.select();" onChange = "javascript:ValidaHora(this,0);"></td>
            </tr>
			<tr>
              <td height="1" colspan="2"></td>
          <tr bgcolor="#DFEEF2"> 
              <td height="20" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;E-mail</font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="email" type="text" size="60" maxlength="50"  value="<%= UsrBeanId.getEmail()%>"  onfocus="javascript:this.select();" onchange="verifica_email(this);"></td>
          </tr>
			<tr>
              <td height="1" colspan="2"></td>
          <tr bgcolor="#DFEEF2"> 
              <td height="20" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Local de trabalho</font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
                <input name="localTrabalho" type="text" size="40" maxlength="30"  value="<%= UsrBeanId.getLocalTrabalho()%>"  onkeypress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>
          </tr>
			<tr bgcolor="#DFEEF2"> 
              <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Protocolo UPO </font></strong></td>
              <td ><strong><font color="#336699">:</font></strong> 
				<select name="codProtUpo" onChange="javascript: valida('buscaUsuarios')"> 
				<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_prot_upo.length;j++) {
						document.write ("<option value="+cod_prot_upo[j]+">"+nm_prot_upo[j])+"</option>"
					}
				</script>
				</select>
			  </td>
            </tr>
	 </table>
		 
<!--Botoes padr�o-->
   <jsp:include page="../sys/BotoesBiometria.jsp" flush="true" />
   <!--FIM_Botoes padr�o-->
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= UsrBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "175 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<!--FIM_CORPO_sistema--> 
<jsp:include page="usuarioConsulta.jsp" flush="true" > 
  <jsp:param name="UsuarioConsTop"  value= "200 px" />
  <jsp:param name="UsuarioConsLeft" value= "40 px" />  
</jsp:include> 

<script>
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%= UsrBeanId.getOrgao().getCodOrgao() %>'
njint  = parseInt(nj,10);

if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.UsrForm.codOrgao.length;i++){
    if(document.UsrForm.codOrgao.options[i].value==njint){
       document.UsrForm.codOrgao.selectedIndex=i;	  
    }
  }
}

//posiciona o popup na descri�ao equivalente ao Protocolo UPO
njprotupo = '<%= UsrBeanId.getProtocoloUpoBean().getCodProtocoloUpo() %>'
njintprotupo  = parseInt(njprotupo,10);

if ((njprotupo.length>0) && (njprotupo!=0)){
	for(x=0;x<document.UsrForm.codProtUpo.length;x++){
	  if(document.UsrForm.codProtUpo.options[x].value==njintprotupo){
       document.UsrForm.codProtUpo.selectedIndex=x;	  
    }
  }
}
</script>

</form>
</body>
</html>

