<!--M�dulo : PNT.NOT
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../../sys/ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<% String tituloConsulta = (String)request.getAttribute("tituloConsulta");
  if(tituloConsulta==null) tituloConsulta=""; 
%>

<%@ include file="CssImpressao.jsp" %>
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ParamSistImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha  = 999;
	int npag       = 0;
	int seq        = 0;		
			
	if (SistId.getParametros().size()>0) { 	
	 for (int i=0; i<SistId.getParametros().size(); i++) {			
			seq++;
			contLinha++;
		if(!"".equals(SistId.getParametros(i).getValParametro())){
			if (contLinha>31) {
				contLinha = 1;
				npag++;	
				if (npag!=1){
		%>			 					
				<%@ include file="rodape_impConsulta_Diretiva.jsp" %>
				<div class="quebrapagina"></div>
				<% } %>						
			<%@ include file="Cab_impConsulta_Diretiva.jsp" %>				
			<!--INICIO CABEC DA TABELA-->
		<table border="0" cellpadding="1" cellspacing="1" width="100%"  align="center" class="semborda">
      		<tr bgcolor="#cccccc"> 
        		<td width="38"  align="center" rowspan=2><b>Seq</b></TD>
        		<td width="449" ><b>Descri��o</b></TD>
        		<td width="227" ><b>Ordem</b></TD>
      		</tr>
      		<tr bgcolor="#cccccc"> 
        		<td ><b>Par�metro</b></TD>
				<td  ><strong>Valor</strong></TD>
      		</tr>
    	</table>
			<% } %>	
	<table border="0" cellpadding="1" cellspacing="1" width="100%"  align="center" class="semborda">			 			
			
		<tr>
			<td width="38" rowspan=2 align="center" ><%= i+1 %>&nbsp;			   
			</td>		
			<td width="449">
				<%= SistId.getParametros(i).getNomDescricao() %>
			</td>
			<td width="227">
	            <%= SistId.getParametros(i).getNumOrdem() %>
	        </td>	        		
		</tr>
	    <tr>
		    <td>
		        <%= SistId.getParametros(i).getNomParam() %>
		    </td>
		    <td>
	            <%= SistId.getParametros(i).getValParametro() %>
		    </td>
	    </tr>
		<tr> 
        	<td height="1" bgcolor = "#000000" colspan="4"></td>			
	    </tr>
		
	</table>   	
  <%
  	  }
    } 
  }else { 
%>
	<div class="quebrapagina"></div>
	<%npag=1;%>
	<%@ include file="Cab_impConsulta_Diretiva.jsp" %>		
   <tr><td colspan="4" height="35"></td></tr>	 	  
		 <tr>
			  <td width="30"></td>
			  <td width="30"></td>			  			  			  
			  <td width="150"  align = "center">Nenhum Par�metro localizado</td>
			  <td></td>
   </tr>
   </table>  
<%} %>
  </table> 
  <%@ include file="rodape_impConsulta_Diretiva.jsp" %>
</form>
</body>
</html>
