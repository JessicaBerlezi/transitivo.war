<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaSistema':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else alert(sErro)
	  break ;
   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
			fForm.atualizarDependente.value="N"	  
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.j_sigFuncao.value = "<%=UsuarioFuncBeanId.getJ_sigFuncao()%>" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}
// fim de validar combo
for (k=0;k<fForm.sFuncao.length;k++) {
   var ne = trim(fForm.sFuncao[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.sFuncao.length;m++) {
	   var dup = trim(fForm.sFuncao[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Fun��o em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
	<!-- verificar se � filho dele mesmo --> 
	var pai = trim(fForm.sigFuncaoPai[k].value)
    if ((pai.length!=0) && (pai==ne)) {
	   sErro += "Fun��o igual ao Fun��o Pai: (seq: "+(k+1)+") "+ne+ "\n" ;
	   valid = false ;			   
	}	
	if (pai.length!=0) {
		<!-- validar existencia do pai -->   	
		faltaPai = true ;
		for (m=0;m<fForm.sFuncao.length;m++) {
		   if ((m!=k) && (pai==trim(fForm.sFuncao[m].value))) {
		   	faltaPai = false ;
			break ;
		   }
		}
		if (faltaPai==true) {
		   sErro += "Fun��o Pai (Fun Pai) n�o existente: (seq: "+(k+1)+") "+pai+ "\n" ;
		   valid = false ;		
		}
	}
	<!-- validar existencia de filho -->   	
	temFilho = false ;
	for (m=0;m<fForm.sFuncao.length;m++) {
		if ((ne==trim(fForm.sigFuncaoPai[m].value)) && (trim(fForm.sFuncao[m].value).length!=0)) {
			temFilho = true ;
			break ;
	   }
	}		
	if (temFilho == true) {
		<!-- validar executa e apresentacao deve ser brancos -->   		
		if (trim(fForm.nomExecuta[k].value).length!=0) {
		   sErro += "Fun��o quando tem 'filhos' n�o pode ter 'Executa' preenchido (seq: "+(k+1)+")"+ " \n";	
		   valid = false ;					
		}
		if (trim(fForm.nomApresentacao[k].value).length!=0) {
		   sErro += "Fun��o quando tem 'filhos' n�o pode ter 'Apresenta��o' preenchido (seq: "+(k+1)+")"+ " \n";	
		   valid = false ;					
		}
	}
	else {
		<!-- validar executa e apresentacao deve ser preenchidos -->   		
		if (trim(fForm.nomExecuta[k].value).length==0) {
		   sErro += "Fun��o quando n�o tem 'filhos', 'Executa' deve ser preenchido (seq: "+(k+1)+")"+ " \n";	
		   valid = false ;					
		}
		if (trim(fForm.nomApresentacao[k].value).length==0) {
		   sErro += "Fun��o quando n�o tem 'filhos', 'Apresenta��o' deve ser preenchido (seq: "+(k+1)+")"+ " \n";	
		   valid = false ;					
		}
	}	
	<!-- verificar se o texto esta preenchido --> 			
	verString(fForm.nomOperacao[k],"Nome n�o preenchido (seq: "+(k+1)+")",0);	
}
return valid ;
}
 
</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="operacoesForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				

  <div id="WK_SISTEMA" style="position:absolute; overflow: visible; z-index: 1; left:204px; top: 80px; width:400px; height:85px;" > 
    <!--INICIO CABEC DA TABELA-->
    <table border="0" cellpadding="0" cellspacing="1" width=400  align=center>
      <tr>
		  
        <td width=60% align="left" valign="top"><b>Sistema:&nbsp;</b> 
          <input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
  			<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codSistema"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
          <% }
			else { %>
          <input name="codSistema" type="hidden" size="50" value="<%= SistId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"   size="50" maxlength="50"  value="<%= SistId.getNomSistema() %>">
			<% } %>			
		  </td>
   		   <% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  		  
	          
        <td align=right width=20%>&nbsp;&nbsp;&nbsp; 
          <button type=button NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistema',this.form);">	
		        <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left" ></button>
		  	</td>
	       	
        <td align=right width=20%> 
          <button type=button NAME="retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
          <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align="left"  ></button>
          	</td>
		   <% } %>			
      </tr>	
      <tr> 
        <td height="2" colspan=2 valign="top"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
  <!--INICIO BOTOES-->  
    <TABLE border='0' cellpadding="0" cellspacing="0" width=400 align=center>
      <TR>
		<% if (SistId.getAtualizarDependente().equals("S")){ %>			
	    <td align="left" width="33%"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
          </td>
       <td align="center" width="34%">
                  <button type=button NAME="Imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>			
          </td>
       <td align="right" width="33%">
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
          </td>
		<%} %>
    </TR>
  </TABLE>  
  <!--FIM BOTOES-->  
</div>
<div style="position:absolute; overflow: auto; z-index: 1; left:35px; top: 130px; width:750px; height:32px;">
  <table border="0" cellpadding="0" cellspacing="1" width="750"  align="center">
    <tr bgcolor="#79B3C4"> 
      <td rowspan="2" width="40" align="center" ><font color="#ffffff"><b>Seq</b></font></TD>		 	  
      <td rowspan="2" width="80" align="center" ><font color="#ffffff"><b>Fun��o</b></font></TD>		 	   	 
        <td width="80" rowspan="2" align="center" bgcolor="#79B3C4" ><font color="#ffffff"><b>Fun&ccedil;&atilde;o 
          Pai</b></font></TD>
      <td width="245"><font color="#ffffff"><b>&nbsp;&nbsp;Nome</b></font></TD>
      	<% 
			if (UsuarioFuncBeanId.getJ_sigFuncao().equals("ACSS350")) {
		%>
			<td width="245" ><font color="#ffffff"><b>&nbsp;&nbsp;Executa</b></font></TD>
			<td rowspan="2" align="center" bgcolor="#79B3C4" ><font color="#ffffff"><b>Bio</b></font></TD>
		<% 
					}else{
		%>
			<td><font color="#ffffff"><b>&nbsp;&nbsp;Executa</b></font></TD>
		<% 
					}
		%>
    </tr>
    <tr bgcolor="#79B3C4">
	   	 <td><font color="#ffffff"><b>&nbsp;&nbsp;Descri��o</b></font></TD>		 
		 <td><font color="#ffffff"><b>&nbsp;&nbsp;Apresenta��o</b></font></TD>
    </tr>
      </table>
<!--FIM CABEC DA TABELA-->
</div>

<div style="position:absolute; overflow: auto; z-index: 1; left: 35px; top: 157px; height: 190px; width:750px;">
  <table border='0' cellpadding="0" cellspacing="1" width="100%"  align=center> 
<% 
					if (SistId.getAtualizarDependente().equals("S")) {				   
		               int ocor   = SistId.getFuncoes().size() ;
					   if (ocor<=0) ocor=SistId.getFuncoes(20,5).size() ;
					   for (int i=0; i<ocor; i++) {			
%>
  	<tr bgcolor="#DFEEF2">
		<td rowspan="2" width="40" align="center"><input name="seq"           type="text" readonly size="3" value="<%= i+1 %>">
			<input name="codOperacao"   type="hidden"  value="<%= SistId.getFuncoes(i).getCodOperacao() %>">
			<input name="codOperacaoFk" type="hidden"  value="<%= SistId.getFuncoes(i).getCodOperacaoFk() %>"></td>								
			<input name="numOrdem"      type="hidden"  value="<%= SistId.getFuncoes(i).getNumOrdem() %>" onkeypress="javascript:f_num();" onfocus="javascript:this.select();" ></td>

    	<td rowspan="2" width="80" align="center">
			<input name="sFuncao"      type="text" size="10" maxlength="30"  value="<%= SistId.getFuncoes(i).getSigFuncao() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
		<td rowspan="2" width="80" align="center">
			<input name="sigFuncaoPai" type="text" size="10" maxlength="30"  value="<%= SistId.getFuncoes(i).getSigFuncaoPai() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
	   	<td width="245" align="center">
			<input name="nomOperacao"  type="text" size="42" maxlength="60"  value="<%= SistId.getFuncoes(i).getNomOperacao() %>" onfocus="javascript:this.select();" ></td>
		<% 
					if (UsuarioFuncBeanId.getJ_sigFuncao().equals("ACSS350")) {
		%>
			<td width="245" align="center">
			<input name="nomExecuta"   type="text" size="42" maxlength="80"  value="<%= SistId.getFuncoes(i).getNomExecuta() %>"  onfocus="javascript:this.select();" ></td>
			<td rowspan="2" align="center">
				<input name="sitBiometria" type="text" size="3" maxlength="3"  value="<%= SistId.getFuncoes(i).getSitBiometria() %>"  onfocus="javascript:this.select();" ></td>
		<% 
					}else{
		%>
			<td align="center">
				<input name="nomExecuta"   type="text" size="42" maxlength="80"  value="<%= SistId.getFuncoes(i).getNomExecuta() %>"  onfocus="javascript:this.select();" ></td>
			<td rowspan="2" align="center">
				<input name="sitBiometria" type="hidden" size="3" maxlength="3"  value="<%= SistId.getFuncoes(i).getSitBiometria() %>"  onfocus="javascript:this.select();" ></td>
		<% 
					}
		%>
	</tr>
	<tr bgcolor="#DFEEF2">
	   	<td align="center"><input name="nomDescricao"    type="text" size="42" maxlength="240"  value="<%= SistId.getFuncoes(i).getNomDescricao() %>"    onfocus="javascript:this.select();" ></td>
		<td align="center"><input name="nomApresentacao" type="text" size="42" maxlength="200"  value="<%= SistId.getFuncoes(i).getNomApresentacao() %>" onfocus="javascript:this.select();" ></td>
	</tr>				
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#DFEEF2">
		<td rowspan=2 width="40" align=center><input name="seq" type="text" readonly size="3" value="<%= i+1 %>">
			<input name="numOrdem"   type="hidden" value="" >
		</td>
    	<td rowspan=2 width="80" align="center"><input name="sFuncao"  type="text" readonly size="10" maxlength="30"  value="" ></td>
	   	<td rowspan=2 width="80" align=center><input name="sigFuncaoPai" type="text" readonly size="10" maxlength="30"  value="" ></td>
	   	<td width="245" align="center">							
		    <input name="nomOperacao"  type="text" readonly size="45" maxlength="60"  value="" ></td>
		 <% 
			if (UsuarioFuncBeanId.getJ_sigFuncao().equals("ACSS350")) {
		%>
			<td width="245" align="center">							
		    	<input name="nomExecuta"   type="text" readonly size="45" maxlength="80"  value="" ></td>
			<td rowspan=2 align="center"><input name="sitBiometria" type="text" readonly size="3" maxlength="3"  value="" ></td>
		<% 
					}else{
		%>
			<td align="center">							
		    	<input name="nomExecuta"   type="text" readonly size="45" maxlength="80"  value="" ></td>
		    <td rowspan=2 align="center"><input name="sitBiometria" type="hidden" readonly size="3" maxlength="3"  value="" ></td>
		<% 
					}
		%>
	</tr>
	<tr bgcolor="#DFEEF2">
	   	<td align="center"><input name="nomDescricao"    type="text" readonly size="45" maxlength="240"  value="" ></td>
		<td align="center"><input name="nomApresentacao" type="text" readonly size="45" maxlength="200"  value="" ></td>
	</tr>					
<%						}					
					}   %>

  </table>
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= SistId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.operacoesForm.codSistema.length;i++){
    if(document.operacoesForm.codSistema.options[i].value==njint){
       document.operacoesForm.codSistema.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>