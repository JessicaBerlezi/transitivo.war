<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaSistema':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else alert(sErro)
	  break ;
   case 'R':
	  close() ;   
	  break;
	  
   case 'V':
	  if (fForm.atualizarDependente.value=="S") {
			fForm.atualizarDependente.value="N"	  
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}

function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}
// fim de validar combo
for (k=0;k<fForm.nomDescricao.length;k++) {
	fForm.nomDescricao[k].value=trim(fForm.nomDescricao[k].value)
   var ne = trim(fForm.nomDescricao[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.nomDescricao.length;m++) {
	   var dup = trim(fForm.nomDescricao[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Perfil em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
function veOpc(obj) {

  if ( ( (event.keyCode==115) || (event.keyCode==110) ||
	   (event.keyCode==83)  || (event.keyCode==78)  ||
	   (event.keyCode==32) ) == false) {
     event.returnValue = false;  
  }

}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="perfilForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>				

  <div id="WK_SISTEMA" style="position:absolute; width:400px; overflow: visible; z-index: 1; top: 75px; left: 204px; visibility: visible;" > 
    <table border="0" cellpadding="0" cellspacing="1" width=400  align="center">
      <tr> 
        <td valign="top" colspan="2" height="5"><img src="images/inv.gif" width="3" height="5"></td>
      </tr>
      <tr>
		  
        <td width="60%" align="left" valign="top"><b>Sistema:&nbsp;</b> 
          <input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
   			<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codSistema"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
          <% }
			else { %>
          <input name="codSistema" type="hidden" size="50" maxlength="50"  value="<%= SistId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text" size="50" maxlength="50"  value="<%= SistId.getNomSistema() %>">
			<% } %>			
	    </td>
   		  <% if (SistId.getAtualizarDependente().equals("S")==false){ %>			  
 	         <td align="right" width=20%>&nbsp;&nbsp;&nbsp;
			  	<button type="button" NAME="Ok" style="width: 44px; height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistema',this.form);">	
	    	    <IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>
			 </td>
	       	 <td width=20%>        	 </td>
		<% } %>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="7"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
    <TABLE border="0" cellpadding="0" cellspacing="0" width="400" align="center">
      <TR>
		<% if (SistId.getAtualizarDependente().equals("S")){ %>			
	 	   
        <td width=33% align="left"> 
          <button type=button NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
          <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
   	    </td>
	       
        <td  width=34% align="center"> 
          <button type=button NAME="Imprimir"  style="height: 18px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
          <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19" ></button>
   	    </td>
	       
        <td width="33%" align="right"> 
          <button type=button NAME="Retornar"  style="height: 18px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" > 
          <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" ></button>
        </td>
		<%} %>
    </TR>

  </TABLE>  
<!--FIM BOTOES-->  
</div>
  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 130px; height: 46px; visibility: visible;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="720"  align="center">
	<% if ( (SistId.getCodSistema().equals("45")) || 
			(SistId.getCodSistema().equals("21"))			
		  ){ %>	
	  <tr bgcolor="#79B3C4"> 
    	    <td rowspan=2 width="40" align="center" ><font color="#ffffff"><b>Seq</b></font></TD>		 	  
        	<td rowspan=2 width="224" align="center" ><font color="#ffffff"><b>Perfil</b></font></TD>		 
	        <td colspan=10 align="center" ><font color="#ffffff"><b>Visualiza</b></font></TD>		 
	  </tr>
      <tr bgcolor="#79B3C4"> 
	        <td width="040" align="center" ><font color="#ffffff"><b>End.</b></font></TD>		 
    	    <td width="045" align="center" ><font color="#ffffff"><b>2a.Via</b></font></TD>		 
			<td width="045" align="center" ><font color="#ffffff"><b>Rec. Not.</b></font></TD>		 
	        <td width="045" align="center" ><font color="#ffffff"><b>AR Dig.</b></font></TD>		 
    	    <td width="045" align="center" ><font color="#ffffff"><b>A. Dig</b></font></TD>		 
    	    <td width="045" align="center" ><font color="#ffffff"><b>Foto</b></font></TD>		     	    
        	<td width="045" align="center" ><font color="#ffffff"><b>Req.</b></font></TD>		 
	        <td width="045" align="center" ><font color="#ffffff"><b>Hist.</b></font></TD>		 
    	    <td width="040" align="center" ><font color="#ffffff"><b>Impr.</b></font></TD>		 
        	<td  align="center" ><font color="#ffffff"><b>Todos</b></font></TD>		 
	  </tr>			
	<% } else { %>
	  <tr bgcolor="#79B3C4"> 
    	    <td width="40"  align="center" ><font color="#ffffff"><b>Seq</b></font></TD>		 	  
        	<td width="224" align="center" ><font color="#ffffff"><b>Perfil</b></font></TD>		 
	        <td  align="center" >&nbsp;</TD>		 
	  </tr>
	<%  } %>
   </table>
<!--FIM CABEC DA TABELA-->
</div>

  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 156px; height: 191px; visibility: visible;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
					if (SistId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = SistId.getPerfis().size() ;
					   if (ocor<=0) ocor=SistId.getPerfis(10,5).size() ;
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="#DFEEF2">	  
		<td width="40" align="center"> 
          <input name="seq"          type="text" readonly size="3" value="<%= i+1 %>">
		    <input name="codPerfil"    type="hidden"  value="<%= SistId.getPerfis(i).getCodPerfil() %>">
        <td width="224" align="center"> 
          <input name="nomDescricao" type="text" size="40" maxlength="40"  value="<%= SistId.getPerfis(i).getNomDescricao() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" >        </td>	
	<% if ( (SistId.getCodSistema().equals("45")) || 
			(SistId.getCodSistema().equals("21"))
		  )
		  { %>	
        <td width="40" align="center"> 
          <input name="indVisEndereco" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisEndereco() %>" 
		  	onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVis2via" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVis2via() %>"
		    onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
		<td width="45" align="center"> 
          <input name="indRecebNot" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndRecebNot()%>"
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>

        <td width="45" align="center"> 
          <input name="indVisAr" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisAr() %>" 
		    onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVisAuto" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisAuto() %>"
		    onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
        <td width="45" align="center"> 
          <input name="indVisFoto" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisFoto() %>"
		    onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			        
        <td width="45" align="center"> 
          <input name="indVisReq" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisReq() %>" 
		    onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVisHist" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisHist() %>"
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
        <td  width="40" align="center"> 
          <input name="indVisImpr" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisImpr() %>"
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td  align="center"> 
          <input name="indVisTodosOrg" type="text" size="1" maxlength="1"  value="<%= SistId.getPerfis(i).getIndVisTodosOrg() %>"
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
		
	<% } else { %>
        <td  align="center">&nbsp;</td>
	<% } %>
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<10; i++) {				   %>					
  	<tr bgcolor="#DFEEF2">
		<td width="40" align="center"> 
          <input name="seq"  readonly  type="text" size="3" value="<%= i+1 %>">
        </td>
	   	<td width="224" align="center"> 
          <input name="nomDescricao" readonly  type="text" size="40" maxlength="40"  value="">        </td>	
	<% if ( (SistId.getCodSistema().equals("45")) ||
			(SistId.getCodSistema().equals("21"))	
			)
	     { %>	
        <td width="40" align="center"> 
          <input name="indVisEndereco" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVis2via" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
		<td width="40" align="center"> 
          <input name="indRecebNot" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
 
        <td width="45" align="center"> 
          <input name="indVisAr"   readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVisAuto" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
        <td width="45" align="center"> 
          <input name="indVisFoto" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			        
        <td width="45" align="center"> 
          <input name="indVisReq"  readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td width="45" align="center"> 
          <input name="indVisHist" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>			
        <td width="40" align="center"> 				
          <input name="indVisImpr" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
        <td  align="center"> 		
          <input name="indVisTodosOrg" readonly type="text" size="1" maxlength="1"  value=""
		   onkeypress = "javascript:veOpc(this);" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
        </td>
		
	<% } else { %>
        <td  align="center">&nbsp;</td>
	<% } %>
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
  <!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= SistId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.perfilForm.codSistema.length;i++){
    if(document.perfilForm.codSistema.options[i].value==njint){
       document.perfilForm.codSistema.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


