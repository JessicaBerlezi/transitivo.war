<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: PGMT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaSistema':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
  	 if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
		
	}else alert(sErro)
	  break ;
	  
  case 'C':
     document.all["escolha"].style.visibility='visible';
     break;
     
  case 'G':
     if (fForm.codSistemaCp.value==0) {
		alert("Sistema de destino n�o selecionado. \n")
	 }
	 if(fForm.codSistema.value == fForm.codSistemaCp.value){
	    alert( "Sistema de destino deve ser diferente do sistema selecionado. \n")
	 }
	 else{
	   fForm.acao.value=opcao
	   fForm.target= "_self";
	   fForm.action = "acessoTool";  
	   fForm.submit();	  				  
	 }
  	 break ;

   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	  
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	    fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  			   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}

for (k=0;k<fForm.nomDescricao.length;k++) {
   var estado = fForm.habilita[k].checked;
   if (estado==false)	
	document.all["ativa"][k].value ="N"
   else
	document.all["ativa"][k].value ="S"	
}



return valid ;
}
function ativaDesativa(obj,index) {
	valid   = true ;
	temFilho = false ;
	sErro = "" ;
	var indice=index
	   var ne = trim(document.all["sFuncao"][index].value)		 
	   <!-- validar existencia de filho --> 
				if ((ne==trim(document.all["sigFuncaoPai"][(parseInt(indice)+1)].value))) {
					temFilho = true ;
					sErro ="Fun��o pai n�o pode ser desabilitada";
					document.all["habilita"][index].checked=true;
				
			   }  

	if(temFilho == true)
	alert(sErro);
}



</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="parametroForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />

<input name="acao" type="hidden" value=' '>				
 
 <div id="WK_SISTEMA" style="position:absolute; overflow: visible; z-index: 1; left:204px; top: 80px; width:400px; height:85px;" > 
    <!--INICIO CABEC DA TABELA-->
    <table border="0" cellpadding="0" cellspacing="1" width=400  align=center>
      <tr>
		  
        <td width=60% align="left" valign="top"><b>Sistema:&nbsp;</b> 
          <input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
  			<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codSistema"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
          <% }
			else { %>
          <input name="codSistema" type="hidden" size="50" value="<%= SistId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"   size="50" maxlength="50"  value="<%= SistId.getNomSistema() %>">
			<% } %>			
		  </td>
   		   <% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  		  
	          
        <td align=right width=20%>&nbsp;&nbsp;&nbsp; 
          <button type=button NAME="Ok" style="width: 28px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistema',this.form);">	
		        <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left" ></button>
		  	</td>
	       	
        <td align=right width=20%> 
          <button type=button NAME="retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
          <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" align="left"  ></button>
          	</td>
		   <% } %>			
      </tr>	
      <tr> 
        <td height="2" colspan=2 valign="top"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
  <!--INICIO BOTOES-->  
    <TABLE border='0' cellpadding="0" cellspacing="0" width=400 align=center>
      <TR>
		<% if (SistId.getAtualizarDependente().equals("S")){ %>			
	    <td align="left" width="20%"> 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
          </td>
      
       <td align="right" width="20%">
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
          </td>
		<%} %>
    </TR>
  </TABLE>  
  <!--FIM BOTOES-->  
</div>
 
  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 140px; height: 64px;"> 
    <table border="0" cellpadding="1" cellspacing="1" width="720"  align="center">
      <tr bgcolor="#79B3C4"> 
        <td width="34"  align="center" rowspan=2><font color="#ffffff"><b>Seq</b></font></TD>
        <td width="80" align="center" ><font color="#ffffff"><b>Fun��o</b></font></TD>	
        <td width="80" rowspan="2" align="center" bgcolor="#79B3C4" ><font color="#ffffff"><b>Fun&ccedil;&atilde;o 
          Pai</b></font></TD>	 
        <td width="449" bgcolor="#79B3C4"><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Descri��o</b></font></TD>
        <td bgcolor="#79B3C4" ><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Habilita</b></font></TD>
      </tr>     
    </table>
<!--FIM CABEC DA TABELA-->
</div>

  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 171px; height: 175px;"> 
    <table border="0" cellpadding="1" cellspacing="1" width="100%"  align="center">
      <% 
					if (SistId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = SistId.getFuncoes().size() ;
					   if (ocor<=0) ocor=SistId.getFuncoes(20,5).size() ;
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="#DFEEF2">
		<td width="33" align="center" bgcolor="#DFEEF2"> 
          <input name="seq"          type="text" readonly size="3" value="<%= i+1 %>">
		  <input name="codFuncao" type="hidden"  value="<%= SistId.getFuncoes(i).getCodOperacao() %>">
		  <input name="ativa" type="hidden"  value="">
		  </td>		
		  
		<td width="80" align="center">
			<input name="sFuncao"     readonly="readonly" type="text" size="10" maxlength="30"  value="<%= SistId.getFuncoes(i).getSigFuncao() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
			<td width="80" align="center">
			<input name="sigFuncaoPai"     readonly="readonly" type="text" size="10" maxlength="30"  value="<%= SistId.getFuncoes(i).getSigFuncaoPai() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
		
		<td width="450"><input name="nomDescricao" readonly="readonly" type="text" size="75" maxlength="200"  value="<%= SistId.getFuncoes(i).getNomOperacao() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();"onChange="this.value=this.value.toUpperCase()"></td>
		<td bgcolor="#79B3C4" ><input type="checkbox" name="habilita"  <%= sys.Util.isChecked(SistId.getFuncoes(i).getHabilita(),"S") %> class="input"   value="<%=SistId.getFuncoes(i).getHabilita()%>" onclick="javascript:ativaDesativa(this,'<%=i%>');"></TD>
	</tr>      
	  <tr> 
        <td height="1" colspan="3"></td>			
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<20; i++) {				   %>					
  	<tr bgcolor="#DFEEF2">
		<td width="33" align="center" rowspan=2 > 
          <input name="seq"  readonly  type="text" size="3" value="<%= i+1 %>">
		  <input name="codFuncao"    type="hidden"  value="">
        </td>
        <td width="80" align="center">
			<input name="sFuncao"     readonly="readonly" type="text" size="10" maxlength="30"  value="" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
			<td width="80" align="center">
			<input name="sFuncaoPai"     readonly="readonly" type="text" size="10" maxlength="30"  value="" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" ></td>
		<td width="450" > 
          <input name="nomDescricao2" readonly type="text" size="75" maxlength="400" value="">
		</td >
		 <td bgcolor="#79B3C4" ><input type="checkbox" name="habilita" value="" class="input" ></TD>
        
	</tr>  	
	<tr>
	   	<td height="1" colspan="3"></td>			
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!-- Combo para escoha de orga para copia de parametros -->
  <div id = "escolha"  style="position:absolute; top: 87px; width: 382px; height: 71px; left: 398px; overflow: visible; z-index: 15; visibility: hidden;" > 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr><td width="79%" align="center"><b>Sistema de Destino:</b>&nbsp; 
         <select name="codSistemaCp">
           <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
           <script>
	         for (j=0;j<cod_sistema.length;j++) {
	    	  document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
	         }
	     </script>
         </select>
      </td>
	   <td width="21%">
	     <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('G',this.form);">	
        <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left"></button>       </td>
   	  </tr>
	</table>
  </div>
<!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= SistId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.parametroForm.codSistema.length;i++){
    if(document.parametroForm.codSistema.options[i].value==njint){
       document.parametroForm.codSistema.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


