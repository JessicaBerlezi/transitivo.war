<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<jsp:include page="ImprimirOperacoesCss.jsp" flush="true" />
<title>:: TRANSITIVO ::</title></title>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="operacoesForm" method="post" action="">

<%
    int numLinhaPag = 30 ;
  	int contLinha = 99;		
	int npag = 0;
	String nomePai = "";
	String nomeApresentacaoPai = "";
	boolean imprimePai = true ;
	boolean prim = true ;
	boolean paiOrigem = false;
	int numFuncoesFilho = 0 ;
	int ocor = SistId.getFuncoes().size() ;
	for (int i=0; i<ocor; i++) {			
		// inicio do menu principal
		if ("0".equals(SistId.getFuncoes(i).getNivel())) {
			nomePai = SistId.getFuncoes(i).getNomOperacao() ;
			nomeApresentacaoPai = SistId.getFuncoes(i).getNomApresentacao()	;
			imprimePai = true ;
			if (contLinha>numLinhaPag-6) {
			    // verifica quantos filhos existem at� o proximo nivel=0
				for (int j=i+1; j<ocor; j++) {
		            if ("0".equals(SistId.getFuncoes(i).getNivel())) break ;
					numFuncoesFilho++;

				}
				if (contLinha+numFuncoesFilho > numLinhaPag) {
					contLinha = 99;
				}
			}
		}
		contLinha++;
		if (contLinha > numLinhaPag){
		    contLinha=1;
			npag++;			
			if (npag!=1){			
%>				
				<tr><td height=2></td></tr>
				</table>      
				<jsp:include page="ImprimirOperacoesRod.jsp" flush="true" />
				<div class="quebrapagina"></div>
		<% } %>				
		<jsp:include page="ImprimirOperacoesCab.jsp" flush="true" >
			<jsp:param name="nPag" value= "<%= npag %>" />				
		</jsp:include>     
    <%    imprimePai = true ;
    	} %>
    	
<% if (imprimePai) { 

		imprimePai = false;
		if (prim) prim = false ;
		else {	
%>
			<tr><td height=2></td></tr>
			</table> 
<%      } %>
   	  <table id="espaco" cellpadding=0 cellspacing=0 border="0" width="100%" class="semborda">
   	    <tr>  <td height="4" ></td>   </tr>
   	  </table>
      <table id="autosImp" width="70%" align=center border="0" cellpadding="0" cellspacing="0" >
	   <tr><td width="100%">
    	  <table width="100%" border="0" cellpadding=0 cellspacing=0 class="semborda" >
        	<tr>
       	      <td width="86%" height="18" valign="top" class="semborda"><strong>&nbsp;<%= SistId.getFuncoes(i).getSigFuncao() %>&nbsp;- &nbsp;<%= nomePai %>&nbsp;</strong>
            </td>
            <% if (nomeApresentacaoPai.length() != 0) { 	%>
      	    <td width="14%" align="center"><input readonly type="checkbox" value=" ">	        </td>
    	     <% } %>  
        	</tr>
	      </table>
	      </td>
	   </tr>
	   <tr><td>
<% } 
	  int pbrancos = 20 + Integer.parseInt(SistId.getFuncoes(i).getNivel())*5;
	  int pnome    = 100-pbrancos ;
	  if (SistId.getFuncoes(i).getNomApresentacao().length()>0) pnome-=14;
	%>
      <table width="100%" border="0" cellpadding=0 cellspacing=0 class="semborda" >
        <tr>
    
          <% if ((SistId.getFuncoes(i).getNomApresentacao().equals(nomeApresentacaoPai))&&(!SistId.getFuncoes(i).getCodOperacaoFk().equals(""))) { %>
              <td width="<%= pbrancos %>%" height="18">&nbsp;</td>
	          <td width="<%= pnome %>%" class="semborda">&nbsp;<%= SistId.getFuncoes(i).getSigFuncao() %>&nbsp;- &nbsp;<%= SistId.getFuncoes(i).getNomOperacao() %></td>  
	      <% } %> 
          <% if (!SistId.getFuncoes(i).getNomApresentacao().equals(nomeApresentacaoPai)&&(!SistId.getFuncoes(i).getCodOperacaoFk().equals(""))) { %>     
              <td width="<%= pbrancos %>%" height="18">&nbsp;</td>
	          <td width="<%= pnome %>%" class="semborda">&nbsp;<%= SistId.getFuncoes(i).getSigFuncao() %>&nbsp;- &nbsp;<%= SistId.getFuncoes(i).getNomOperacao() %></td>              
		      <% if (SistId.getFuncoes(i).getNomApresentacao().length()>0) { %>
   	        <td width="14%" align="center" class="semborda"><input readonly type="checkbox" value=" "></td>
          <% } %> 
        <% } %>  
        </tr>
        <% if ((!SistId.getFuncoes(i).getNomApresentacao().equals(nomeApresentacaoPai))||(!SistId.getFuncoes(i).getCodOperacaoFk().equals(""))) { %>     
        <tr> 
          <td width="<%= pbrancos %>%" height="1"></td>
          <td colspan=2 bgcolor="#000000" ></td>           
        </tr>
        <% } %> 
      </table>
 <% } %>
 
<%	if (imprimePai==false) { %>
		</td></tr>
		<tr><td height=2></td></tr>		
        </table>   
      <% if (SistId.getCodSistema().equals("45")){%>     
          <jsp:include page="ImprimirOperacoesVisualizacaoImp.jsp" flush="true" />
       <%}%>      
        <jsp:include page="ImprimirOperacoesRod.jsp" flush="true" />
 <%} %>



</form>
</BODY>
</HTML>