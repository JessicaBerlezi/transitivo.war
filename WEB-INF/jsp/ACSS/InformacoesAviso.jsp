<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<jsp:useBean id="UsuarioBeanId"  scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="AvisoBeanId" scope="request" class="ACSS.AvisoBean" /> 

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de 
Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
<script language="JavaScript" type="text/JavaScript">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,codigo,fForm) {
 switch (opcao) {
   case 'mostraAviso':
	    fForm.acao.value=opcao;
	    fForm.codigo.value=codigo;
		fForm.target= "_self";
	    fForm.action = "acessoTool";
	   	fForm.submit();
	  break ;
   case 'R':
      close();   
	  break;	  
	}
}
</script>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="/SMIT/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="form" method="post" action="">
	<jsp:include page="Cab.jsp" flush="true" />
	<input name="acao" type="hidden" value="">
	<input name="codigo" type="hidden" value="">
	
<!--T�tulo da P�gina-->
<div id="titPagina" style="position:absolute; left: 204px; top: 55px; width:468px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
  <script>esconde('titModulo');</script>
    <tr> 
      <td width="12%"><div align="left"><font color="#000000"><strong>AVISOS</strong></font></div></td>
    </tr>
  </table>
</div>
<!--Fim T�tulo da P�gina-->
<!--�rea de Not�cias--> 
  <div id="avisos" style="position:absolute; left:50px; top:81px; width:720px; height:260px; z-index:13; overflow: hidden; visibility: visible;"> 
    <table height="100%" width="100%" border="0" cellpadding="4" cellspacing="0" class="table">
      <tr> 
        <td colspan="3" valign="top" bgcolor="#faeae5"> 
          <table width="100%" border="0" cellpadding="5" cellspacing="5"   class="table3">
            <tr > 
              <td width="58%" valign="top" height="240"> 
              	<p align="justify" style="font-size: 14px">Assunto:<strong><br><%=AvisoBeanId.getDscAviso()%></strong></p>
                <strong>De: <%=AvisoBeanId.getUsuario().getNomUsuario()%></strong><br>
                <strong>Enviado em: <%=AvisoBeanId.getDatEnvioStr()%></strong><br>
                <p align="justify"><%=AvisoBeanId.getTxtAviso()%></p>
              </td>
              <td width="3%" valign="top" style="border-right: 1px solid #999999;">&nbsp;</td>
             <td width="39%" valign="top" >
            
			  <table width="100%" border="0" cellspacing="0" cellpadding="0">
               
                <tr>
                  <td>
				     <div id="linhas" style="position:absolute; left:390px; top:14px; width:235px; height:234px; z-index:13; overflow:auto; hidden; visibility: visible;"> 
                        <table width="100%" >
			              <%String str="";
            			    for (int i = 0; i < AvisoBeanId.getAvisos().length; i++) {%> 
                                <tr valign="top" style="cursor: hand;" onClick="javascript: valida('mostraAviso','<%=AvisoBeanId.getAvisos()[i].getCodAviso()%>',document.form);"> 
			                    <%if(AvisoBeanId.getAvisos()[i].getIndPrioridade().equals("A")) str="color=#FF0000";
            			          else str="color=#000000"; %>
                            <td width="3">�</td>
							<td width="164"><font size="1" <%=str%> >
			                    <%if (AvisoBeanId.getAvisos()[i].lido(UsuarioBeanId)) {%>
	        		            <%=AvisoBeanId.getAvisos()[i].getDscAviso()%>
								<%} else {%>
    	    		            	<strong><%=AvisoBeanId.getAvisos()[i].getDscAviso()%></strong>
                    			<%}%></font>
							</td>
                          </tr>
                          <tr>
                            <td colspan="2" height="4"></td>
                          </tr>
					  <%}%>
                        </table>
					</div>
				 </td>
                </tr>
              </table>
			   </td>
            </tr>
          </table> 
		 </td>
<!--fim-->
        <td width="12%" style="background-image: url(<%=path%>/images/ACSS/info_aviso.jpg); background-position: 2px -5px">&nbsp;</td>
      </tr>
      <script>document.form.focus();</script>
    </table>
  </div>
<!--Fim �rea de Not�cias--> 

<!--Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!--FIM Rodap�-->

</form>
</body>
</html>