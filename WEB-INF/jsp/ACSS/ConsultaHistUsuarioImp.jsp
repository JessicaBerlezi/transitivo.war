<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<%@ page import="java.util.Iterator" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<jsp:useBean id="ConsultaHistoricosBeanId" scope="session" class="ACSS.ConsultaHistoricosBean" /> 

<!-- Chama o Objeto da Tabela de Orgaos -->

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">DIV.quebrapagina{page-break-after: always}</style>

<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>
<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
<form name="ConsultaLogAcessoImp" method="post" action="">
<!-- Inicio Div Detalhes vari�veis impressao -->	 
<%
  	int contLinha=0;
	int npag = 0;
	int seq = 0;
	Iterator it = ConsultaHistoricosBeanId.getDados().iterator() ;
	ACSS.UsuarioBean myOrg  = new ACSS.UsuarioBean();					
	if (ConsultaHistoricosBeanId.getDados().size()>0) { 
		while (it.hasNext()) {
			myOrg   = (ACSS.UsuarioBean)it.next() ;
			seq++;
			contLinha++;
			
			if (contLinha%54==1){
				npag++;							
				if (npag!=1){
		%>			 					
				<jsp:include page="Cab_impConsulta.jsp" flush="true" />
				<div class="quebrapagina"></div>
				<% } %>				
			<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
			<jsp:param name="nPag" value= "<%=npag%>" />				
			</jsp:include> 
			  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
           <tr><td colspan=4 height=3>De :  <%= ConsultaHistoricosBeanId.getDatInicio() %>&nbsp;&nbsp;Ate:&nbsp;&nbsp; <%= ConsultaHistoricosBeanId.getDatFim() %></td></tr>		
 	<tr > 	
 	
				 <td width="4%" rowspan="3" align="center" bgcolor="#999999"><font color="#000000"><b>SEQ</b></font></td>
				 <td width="16%" bgcolor="#999999" class="td_linha_left" style="line-height:20px;padding-left: 2px"><font color="#000000"><b>C&Oacute;D. USU&Aacute;RIO/&Oacute;RG&Atilde;O</b></font></td>		 
				 <td width="13%" style="padding-left: 2px" bgcolor="#999999" class="td_linha_left"><font color="#000000"><b>LOGIN</b></font></td>
				 <td width="25%" bgcolor="#999999" class="td_linha_left"><font color="#000000" style="padding-left: 2px"><b>NOME USU&Aacute;RIO </b></font></td>
				 <td width="12%" bgcolor="#999999" class="td_linha_left"><font color="#000000" style="padding-left: 2px"><b>DATA CADASTRO </b></font></td>
				 <td width="15%" bgcolor="#999999" class="td_linha_left"><font color="#000000" style="padding-left: 2px"><b>DATA/HORA VALID. </b></font></td>
				 <td bgcolor="#999999" class="td_linha_left" style="line-height:20px;padding-left: 2px"><font color="#000000"><b>A&Ccedil;&Atilde;O</b></font></td>
	 </tr>  	   		
     </table>

			<% } %>	
											
    <table id="LogsImp" width="100%" border="0" cellpadding="0" cellspacing="1" >							
      <tr>
        <td width="4%" rowspan="4" align="center" valign="top" class="table_linha_top" style="padding-right: 2px; padding-top: 4px"><%= seq %></td>
        <td width="16%" class="td_linha_top_left"  style="line-height:17px; padding-left: 2px"><b><%= myOrg.getCodUsuario() %>/<%= myOrg.getOrgao().getSigOrgao() %></b></td>
        <td width="13%" class="td_linha_top_left" style="padding-left: 2px"><strong><%= myOrg.getNomUserName() %></strong></td>
        <td width="25%" class="td_linha_top_left" style="padding-left: 2px" ><strong><%= myOrg.getNomUsuario() %></strong></td>
        <td width="12%" class="td_linha_top_left" style="padding-left: 2px" ><%= myOrg.getDatCadastro() %></td>
        <td width="15%" class="td_linha_top_left" style="padding-left: 2px" ><%= myOrg.getDatValidade() %>&nbsp;&nbsp;<%= myOrg.getHoraValidadeEdt() %></td>
        <td class="td_linha_top_left" style="padding-left: 2px"><strong><%= "I".equals(myOrg.getAcaoAuditoria()) ? "INCLUS�O" : "" %><%= "A".equals(myOrg.getAcaoAuditoria()) ? "ALTERA��O" : "" %><%= "E".equals(myOrg.getAcaoAuditoria()) ? "EXCLUS�O" : "" %><%= "S".equals(myOrg.getAcaoAuditoria()) ? "ALTER. SENHA" : "" %><%= "B".equals(myOrg.getAcaoAuditoria()) ? "ALTER. BIOMETRIA" : "" %></strong></td>
      </tr>
      <tr>
        <td colspan="7" class="td_linha_top_left" style="line-height:20px;padding-left: 2px">CPF.: <%= myOrg.getNumCpfEdt() %>
        &nbsp;&nbsp;E-MAIL : <%= myOrg.getEmail() %>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SENHA: <%= myOrg.getDatSenhaAlterada() %> 
        &nbsp;&nbsp;BIOMETRIA: <%= myOrg.getDatBioAlterada() %> </td>
      </tr>
      <tr>
        <td colspan="3" class="td_linha_top_left" style="line-height:20px;padding-left: 2px">EXECUTADO POR : <%= myOrg.getNomUserNameOper() %> / <%= myOrg.getSigOrgaoLotacaoOper() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;EM: <%= myOrg.getDatProcOper() %> </td>
        <td colspan="3" class="td_linha_top_left" style="line-height:20px;padding-left: 2px">LOCAL TRAB.: <%= myOrg.getLocalTrabalho() %></td>
      </tr>
      <tr>
        <td colspan="7" height="2" bgcolor="#000000"></td>
      </tr>
	<%	} %>
	</table>      
<%} else { 
	String msg=(String)request.getAttribute("semLog"); if (msg==null) msg="";
%>

	<div class="quebrapagina"></div>
	
	<jsp:include page="Cab_impConsulta.jsp" flush="true" >	
	<jsp:param name="nPag" value= "1" />				
	</jsp:include> 
    <table id="autosImp" width="100%" border="0" cellpadding="0" cellspacing="1" class="table">									
		<tr><td height="40" ></td></tr>		
		<tr bgcolor='#CCCCCC'> 
	   	    <td height="35" align=center><b><%= msg %></b></td>
		</tr>	
	</table>      			
    <jsp:include page="rodape_impConsulta.jsp" flush="true" />
<%} %>
<%	if (ConsultaHistoricosBeanId.getDados().size()>0) { 
		if (contLinha<54){

		} %>
		<jsp:include page="rodape_impConsulta.jsp" flush="true" />
<%} %>
</form>
</body>
</html>