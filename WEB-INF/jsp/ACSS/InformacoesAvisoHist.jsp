<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<jsp:useBean id="AvisoBeanId" scope="request" class="ACSS.AvisoBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />  
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#79B3C4";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de 
Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
<script language="JavaScript" type="text/JavaScript">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,codigo,fForm) {
 switch (opcao) {
   case 'mostraAvisoHist':
	    fForm.acao.value=opcao;
	    fForm.codigo.value=codigo;
		fForm.target= "_self";
	    fForm.action = "acessoTool";
	   	fForm.submit();
	  break ;
   case 'R':
      close();   
	  break;	  
	}
}

function valida_Not(fForm) {
    fForm.acao.value="histAviso";
	fForm.target= "_self";
    fForm.action = "acessoTool";
   	fForm.submit();
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
</script>
<SCRIPT LANGUAGE="JavaScript1.2" SRC="/SMIT/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="form" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

	<input name="acao" type="hidden" value="">
	<input name="codigo" type="hidden" value="">
	
<!--T�tulo da P�gina-->
<div id="titPagina" style="position:absolute; left: 204px; top: 55px; width:468px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
  <script>esconde('titModulo');</script>  
    <tr> 
      <td width="12%"><div align="left"><font color="#000000"><strong>HIST�RICO DE AVISOS</strong></font></div></td>
    </tr>
  </table>
</div>
<!--Fim T�tulo da P�gina-->
<!--�rea de Not�cias--> 
  <div id="avisos" style="position:absolute; left:50px; top:81px; width:720px; height:260px; z-index:13; overflow: hidden; visibility: visible; background-color: #DFEEF2; layer-background-color: #DFEEF2; border: 1px none #000000;"> 
    <table height="100%" bgcolor="<%=Cor%>" width="100%" border="0" cellpadding="4" cellspacing="0" class="table">
      <tr> 
        <td colspan="3" valign="top" bgcolor="<%=Cor%>"> 
          <table width="100%" border="0" cellpadding="5" cellspacing="5" bgcolor="<%=Cor%>" class="table3">
            <tr > 
              <td width="58%" valign="top" height="240"> 
              	<p align="justify" style="font-size: 14px">Assunto:<strong><br><%=AvisoBeanId.getDscAviso()%></strong></p>
                <strong>De: <%=AvisoBeanId.getUsuario().getNomUsuario()%></strong><br>
                <strong>Enviado em: <%=AvisoBeanId.getDatEnvioStr()%></strong><br>
                <p align="justify"><%=AvisoBeanId.getTxtAviso()%></p>
              </td>
              <td width="3%" valign="top" style="border-right: 1px solid #999999;">&nbsp;</td>
              <td width="39%" valign="top" >
            
			  <table width="100%"  bgcolor="<%=Cor%>" border="0" cellspacing="0" cellpadding="0">
                <tr >
                  <td>                        
                    <table width="100%"  border="0">
                      <tr bgcolor="<%=Cor%>"> 
                        <td width="40%" nowrap>De  <input value='<%=AvisoBeanId.getDatInicioAviso()%>' name="datInicioAviso" size="12" maxlength="10" type="text" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');"></td>
                        <td width="40%" nowrap>at� <input value='<%=AvisoBeanId.getDatFimAviso()%>' name="datFimAviso"    size="12" maxlength="10" type="text" onchange="javascript:ValDt(this,0);" onkeypress="javascript:Mascaras(this,'99/99/9999');"> </td>
                        <td width="15%"><button type=button NAME="Ok" style="height: 21px; width: 28px;border: none; background: transparent;" onClick="valida_Not(this.form);">	
							      		<IMG src="<%= path %>/images/REC/bot_ok.gif"  width="26" height="19"></button>
						</td>
                      </tr>
                    </table>
			 	</td>
                </tr>
                
                <tr bgcolor="<%=Cor%>" >
                  <td>
				     <div id="linhas" style="position:absolute; left:390px; top:58px; width:235px; height:190px; z-index:13; overflow:auto; hidden; visibility: visible;"> 
                        <table width="100%" >
			              <%String str="";
            			    for (int i = 0; i < AvisoBeanId.getAvisos().length; i++) {%> 
                                <tr valign="top" style="cursor: hand;" onClick="javascript: valida('mostraAvisoHist','<%=AvisoBeanId.getAvisos()[i].getCodAviso()%>',document.form);"> 
			                    <%if(AvisoBeanId.getAvisos()[i].getIndPrioridade().equals("A")) str="color=#FF0000";
            			          else str="color=#000000"; %>
                            <td width="3">�</td>
							<td width="164"><font size="1" <%=str%> >
			                    <%if (AvisoBeanId.getAvisos()[i].lido(UsuarioBeanId)) {%>
	        		            <%=AvisoBeanId.getAvisos()[i].getDscAviso()%>
								<%} else {%>
    	    		            	<strong><%=AvisoBeanId.getAvisos()[i].getDscAviso()%></strong>
                    			<%}%></font>
							</td>
                          </tr>
                          <tr>
                            <td colspan="2" height="4"></td>
                          </tr>
					  <%}%>
                        </table>
					</div>
				 </td>
                </tr>
              </table>
		      </td>
            </tr>
          </table> 
	    </td>
<!--fim-->
        <td width="12%" style="background-image: url(<%=path%>/images/ACSS/info_aviso.jpg); background-position: 2px -5px">&nbsp;</td>
      </tr>
      <script>document.form.focus();</script>
    </table>
  </div>
<!--Fim �rea de Not�cias--> 

<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->

</form>
</body>
</html>