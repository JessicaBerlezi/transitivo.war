<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"       scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Operacao -->
<jsp:useBean id="OperBeanId"       scope="session" class="ACSS.OperacaoBean" /> 
<!-- Chama o Objeto da Tabela de Perfil/Orgao/Atuacao -->
<jsp:useBean id="ConsultaAcessoFuncaoBeanId" scope="session" class="ACSS.ConsultaAcessoFuncaoBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistBeanId"      scope="session" class="ACSS.SistemaBean" /> <!-- Chama o Objeto do Sistema logado -->
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#79B3C4";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 Cresult = "#be7272";
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 Cresult = "#a8b980";
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
 		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistBeanId.getSistemas(UsuarioBeanId.getCodUsuario()) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}
      
myOper      = new Array(<%= OperBeanId.getFuncao(SistBeanId.getCodSistema(),SistBeanId) %>);
cod_operacao = new Array(myOper.length/2);
nom_operacao = new Array(myOper.length/2);
for (i=0; i<myOper.length/2; i++) {
	cod_operacao[i] = myOper[i*2+0] ;
	nom_operacao[i] = myOper[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaFuncao':
	  if (fForm.codSistema.value==0) {
		alert("Sistema n�o selecionado.") ;
	  }
	  else {	
	    fForm.acao.value=opcao
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 

   case 'buscaDados':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
	  
   case 'ImprimeResultConsulta':
		fForm.acao.value=opcao
   		fForm.target= "_blank";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  	break; 	  
	  		  
   case 'R':
      close() ;   
	  break;
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codSistema.value==0) {
	   valid = false
	   sErro = "Sistema n�o selecionado. \n"
	}	
	if (fForm.codOperacao.value==0) {
	   valid = false
	   sErro += "Fun��o n�o selecionada. \n"
	}
    if (valid==false) alert(sErro)
	return valid ;
}
function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["UsrAtuForm"].target= "_self";
	document.all["UsrAtuForm"].action = "acessoTool";  
	document.all["UsrAtuForm"].submit();	 
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

 
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

<input name="acao" type="hidden" value=' '>				
<input name="ordem" type="hidden" value="Data">
<input name="valueCombo" type="hidden" value='<%=(String)request.getAttribute("valueCombo")%> '>	
<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
          <% if (((String)request.getAttribute("verTable")).compareTo("N")==0){%>			  
			  <td  align = "right"><b>Sistema:&nbsp;</b></td>
					<td width="75%"><select name="codSistema" onChange="javascript: valida('buscaFuncao',this.form);" > 
					<option value="0" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_sistema.length;j++) {
							document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
						}
					</script>
				</select>
			  </td>
	  </tr>
		  <tr>	  
			  <td width = "19%" align="right"><b>Fun��o:&nbsp;</b></td>
			    <td><select name="codOperacao"  > 
						<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
						<script>
							for (j=0;j<cod_operacao.length;j++) {
								document.write ("<option value="+cod_operacao[j]+">"+nom_operacao[j])+"</option>"
							}
						</script>
				</select></td>		  
         </tr>			  
			  
			  
			  
	     <tr>
		 	 <td align="right" ><strong>&nbsp;Usu&aacute;rios:</strong>&nbsp;</td> 
			 <td width="42%" >
				<input name="tipoUsuario" type="radio"  value="A"  class="sem-borda" style="height: 14px; width: 14px;">
				 Ativos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 <input name="tipoUsuario" type="radio" value="I"  class="sem-borda" style="height: 14px; width: 14px;">
				 Inativos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				 <input checked name="tipoUsuario" type="radio" value="T"  class="sem-borda" style="height: 14px; width: 14px;">
				 Ambos&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             </td>
             
	          <td width="3%">
			  		<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaDados',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>
			  
         </tr>			  			  
			  
			  
			  
			  
			  
			  
			  
          <% }
			else { %>
      <tr>
			  <td  align = "right"><b>Sistema:&nbsp;</b></td>
			  <td>
	            <input name="codSistema" type="hidden" size="10" maxlength="6"  value="<%= SistBeanId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text"  size="60" maxlength="50" value="<%= SistBeanId.getNomSistema() %>" style="border: 0px none;" >	    </td>
			  <td ></td>
      </tr>
			 <tr> 
			  <td width = "19%" align="right"><b>Fun��o:&nbsp;</b></td>
			  <td>
	            <input name="codOperacao" type="hidden" size="14" maxlength="10" value="<%= OperBeanId.getCodOperacao()%>"> 
       		   <input disabled name="nomOperacao" type="text"  size="70" maxlength="60" value="<%= OperBeanId.getNomOperacao() %>" style="border: 0px none;">			  </td>		
            </tr>       		   
            
            
		<tr>
			<td height="1" align="right" ><strong>&nbsp;Usu&aacute;rios:</strong>&nbsp;</td>
			<td> 
			  <input  disabled name="tipoUsuario" type="text" value="<%= ConsultaAcessoFuncaoBeanId.getDescIndUsuario() %>"  class="sem-borda"   >
		</tr>
       		   
	      <% } %>			  
      
      
       <tr><td colspan="2" height="2"></td></tr>	   
    </table>
    </div>    
	<!-- TABLE border="0" cellpadding="0" cellspacing="0" width="100%"-->	 
	  	<% if (((String)request.getAttribute("verTable")).compareTo("S")==0){ %>
<div id="botoes" style="position:absolute; width:200px; overflow: visible; z-index: 30; top: 115px; right: 10px; visibility: visible;" > 
	  	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">
	  	<tr>			 
	 		<td width="75%"  align = "right"> 
	  		  <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
				  <img src="<%= path %>/images/bot_retornar.gif" width="52" height="19" >
			  </button>
			</td>
			<td width="5%"></td>
			<td width="20%">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimeResultConsulta',this.form)"> 
              <img src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">
			</button>			
			 </td></tr>	
			 </table>
			 </div>
	   <% } %>	
    <!-- /table -->
<!--  /div-->     


<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 142px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: none; visibility: visible;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
     <tr>
       <td colspan=2 align=right>
		 <font color="#000000">&nbsp;Ordem:&nbsp;<%= ConsultaAcessoFuncaoBeanId.getNomOrdem()%>&nbsp;</font>
	   </td>	
    </tr>  		
  </table>
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr><td colspan=4 height=3></td></tr>		
 	<tr > 	
	   	 <td width="40" align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>SEQ</b></font></TD>		 	  		 		 	  
	   	 <td width="100"  bgcolor="<%=Cresult%>">
		  <a href="#" onClick="javascript: Classificacao('OrgaoLot',this.form);">
	   	  <font color="#ffffff"><b>&nbsp;&nbsp;ORG�O LOTA��O</b></font></TD>		 
	   	 <td width="280" align="center" bgcolor="<%=Cresult%>">
		   <a href="#" onClick="javascript: Classificacao('Usuario',this.form);">
	   	   <font color="#ffffff"><b>USU�RIO</b></font></TD>	
		 <td width="100"  bgcolor="<%=Cresult%>">
		   <a href="#" onClick="javascript: Classificacao('OrgaoAtu',this.form);">
		   <font color="#ffffff"><b>&nbsp;&nbsp;ORG�O ATUA��O</b></font></TD>		 
	   	 <td align="center" bgcolor="<%=Cresult%>">
		   <a href="#" onClick="javascript: Classificacao('Perfil',this.form);">
	   	   <font color="#ffffff"><b>PERFIL</b></font></TD>		 	 
	</tr> 
   </table>
</div>   

<% if (((String)request.getAttribute("verTable")).compareTo("S")==0){ %>				  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 173px; left: 50px; height: 190px; visibility: visible;">   
   <table id=fconsulta border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator it = ConsultaAcessoFuncaoBeanId.getDados().iterator() ;
		int seq = 1;	
		ACSS.ConsultaAcessoFuncaoBean AcessoFuncao =  new ACSS.ConsultaAcessoFuncaoBean() ;
        while (it.hasNext()) {
			// lista os dados		
			AcessoFuncao = (ACSS.ConsultaAcessoFuncaoBean)it.next();
			ConsultaAcessoFuncaoBeanId.setDados(ConsultaAcessoFuncaoBeanId.getDados()) ;
	%>       
		<tr bgcolor="<%=Cor%>" > 	
    	   	 <td width="40"  align="center"><%= seq %></TD>		 		 	  
             <td width="100">&nbsp;&nbsp;<%= AcessoFuncao.getSigOrgaoLotacao() %> </td>
 		   	 <td width="280">&nbsp;&nbsp;<%= AcessoFuncao.getNomUsuario() %></TD>	
			 <td width="100">&nbsp;&nbsp;<%= AcessoFuncao.getSigOrgaoAtuacao() %> </td>
             <td>&nbsp;&nbsp;<%= AcessoFuncao.getNomDescricao() %></TD>	
    <% 
	  		seq++;
	   }
	   %>	

   </table>    
   <% } %>	 
</div>

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= ConsultaAcessoFuncaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistBeanId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codSistema.length;i++){
		if(document.UsrAtuForm.codSistema.options[i].value==njint){
			document.UsrAtuForm.codSistema.selectedIndex=i;	  
		}
	}
}
</script>	
</form>
</BODY>
</HTML>

