<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->
<!-- Abre a Sessao -->
<%@ page session="true"%>
<%@ page import="java.util.*"%>

<%String path = request.getContextPath();

            %>
<%@ page errorPage="ErrorPage.jsp"%>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" />
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId" scope="request" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="consultHistParamOrgBean" scope="session"
	class="ACSS.ConsultaHistoricoParamOrgBean" />
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" />
<jsp:useBean id="UsuarioFuncBeanId" scope="session"
	class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId" scope="request"
	class="sys.RequisicaoBean" />

<%String mySistema = UsuarioFuncBeanId.getAbrevSistema();
            String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao();
            String nomeSistema = "CONTROLE DE ACESSO";
            String Cor = "#DFEEF2";
            String Cresult = "#be7272";
            String resultline = "#faeae5";
            if ("REC".equals(mySistema)) {
                Cor = "#faeae5";
                Cresult = "#be7272";
                nomeSistema = "RECURSO";
            }
            if ("REG".equals(mySistema)) {
                Cor = "#EFF5E2";
                Cresult = "#a8b980";
                nomeSistema = "REGISTRO";
            }
            if ("PNT".equals(mySistema)) {
                Cor = "#E9EEFE";
                nomeSistema = "PONTUA��O";
            }
            if ("CID".equals(mySistema)) {
                Cor = "#faeae5";
                nomeSistema = "COMISS�O CIDAD�";
            }
            if ("GER".equals(mySistema)) {
                Cor = "#F8EFD3";
                nomeSistema = "GERENCIAL";
                Cresult = "#B8A47A";
                resultline = "#efeadc";
            }

            %>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<style type="text/css">
.branco2 {
	 COLOR: white; FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif; TEXT-DECORATION: none
}
.branco2:link {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco2:visited {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco2:hover {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica; TEXT-DECORATION: underline
}
.identacao td{
		padding-left: 3px; padding-right: 3px;
}
</style>
<title>:: SMIT::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%=path%>/js/itcutil.js"
	TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}


function veCampos(fForm){
	valid   = true ;
	sErro = "" ;
	verString(fForm.datInicio,"Data Inicial",0);
	verString(fForm.datFim,"Data Final",0);
	if (document.all["codOrgao"].selectedIndex<=0  )
	{  valid = false
	   sErro += "Selecione um �rg�o \n"
	      
	}	
	if (comparaDatas(fForm.datInicio.value,fForm.datFim.value)==false){
       valid = false
       sErro += "Data final n�o pode ser inferior a data inicial. \n"
	   document.all.datFim.focus();       
    }

    if (valid==false) alert(sErro)
	return valid ;
}
	
function comparaDatas(data1,data2) {
// Verifica se as datas s�o validas de acordo com os eventos anteriores
// Exemplo: Data de Definir Junta/relator n�o pode ser menor que a data do recurso

	var valid = true;
	var dataEventAtual = new Date(data2.substring(6),(data2.substring(3,5)-1),data2.substring(0,2));
	var dataEventAnterior = new Date(data1.substring(6),(data1.substring(3,5)-1),data1.substring(0,2));
	var difference = dataEventAnterior-dataEventAtual;//obt�m o resultado em milisegundos.

	formatdifference=Math.round(difference/1000/60/60/24); //agora em dias.
    if (formatdifference > 0) valid = false;
	return valid;
}

function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["UsrAtuForm"].target= "_self";
	document.all["UsrAtuForm"].action = "acessoTool";  
	document.all["UsrAtuForm"].submit();	 
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
		fForm.codUsuario.options.length = 0;
	    fForm.acao.value=opcao;
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ; 
  	  
   case 'consultaLogParam':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 	  
   case 'R':
      close() ;   
	  break;
   case 'ImprimeResultConsulta':
		fForm.acao.value=opcao
   		fForm.target= "_blank";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  	break;   
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}


function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}



function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

function changeColorOver(obj) {	
	obj.style.background="<%=Cresult%>";
	obj.style.textDecoration="underline";
}

function changeColorOut(obj) {
	obj.style.background="<%=Cor%>";
	obj.style.textDecoration="none";
}

</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0"
	marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action=""><input name="acao"
	type="hidden"> <input name="coluna" type="hidden"> <input name="ordem"
	type="hidden" value="Data"> <!--INICIO - CAB. INDEPENDE DO MODULO--> <!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true" /> <!-- Fim da Div -->
<div id="cabecalho"
	style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td background="images/<%= mySistema %>/detran_bg_cab.png"><img
			src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
	</tr>
</table>
</div>

<div id="TextoCabecalho"
	style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="205" height="85" valign="middle"><img src="images/inv.gif"
			width="205" height="3"></td>
<%-- 		<td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font> --%>
<%-- 		<%String qb = (String) session.getAttribute("j_qualBanco"); --%>
<!--             if (qb == null) -->
<!--                 qb = "???"; -->
<!--             if ("(P)".equals(qb)) -->
<!--                 out.print("<font color=#999999>" + qb + "</font>"); -->
<!--             else -->
<!--                 out.print("<font color=red><b>" + qb + "</b></font>"); -->

<%--             %> <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0, 10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13, 18)%></font> --%>
<%-- 		&nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o --%>
<%-- 		Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font></td> --%>

		<td align="right" valign="top">
		<button type="button"
			style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;"
			name="j_Info" onClick="javascript: j_info(this.form);"><%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png"
			alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%> <img src="<%= path %>/images/bot_infovisited_ico.png"
			alt="N�o existem Mensagens para Voc�." width="25" height="32"
			border="0"> <%}%></td>

		<td width="32" align="right" valign="top"><a href="AutoCicloVida.html"
			target="_blank"> <img src="images/ico_ciclo.png"
			alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32"
			border="0"></a></td>

		<td width="45" align="center" valign="top">
		<button type="button"
			style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;"
			name="j_Ajuda" onClick="javascript: j_ajuda(this.form);"><img
			src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema"
			width="25" height="32" border="0"></button>
		</td>
	</tr>
</table>
</div>

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td width="34" valign="top"
			background="images/<%= mySistema %>/detran_bg1.png"><img
			src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img
			src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td align="center" valign="top">&nbsp;</td>
	</tr>
</table>

<input name="j_token" type="hidden"
	value="<%=RequisicaoBeanId.getToken()%>"> <input name="j_cmdFuncao"
	type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>"> <input
	name="j_sigFuncao" type="hidden"
	value="<%=RequisicaoBeanId.getSigFuncao()%>"> <input name="j_jspOrigem"
	type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>"> <input
	name="j_abrevSist" type="hidden"
	value="<%=RequisicaoBeanId.getAbrevSist()%>"> <!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo"
	style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;"
	class="espaco">
<table width="100%" border="0" cellpadding="2" cellspacing="0"
	class="espaco">
	<tr>
		<td><font color="#000000"><strong> <%if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length() > 0) {

                %> <%=UsuarioFuncBeanId.getJ_nomFuncaoPai()%>&nbsp; <img
			src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp; <%}

            %> <%=UsuarioFuncBeanId.getJ_nomFuncao()%></strong></font></td>
	</tr>
</table>
</div>
<!-- FIM - CAB. -->

<div id="WK_SISTEMA"
	style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 86px; left: 50px; z-index:1; visibility: visible;">
<!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"
	align="center">
	<%if (consultHistParamOrgBean.getAtualizarTela().equals("N")) {

                %>
	<tr>
		<td width="5%" align="right"><b>&Oacute;rg&atilde;o:</b>&nbsp;</td>
		<td width="16%">&nbsp;&nbsp; <select name="codOrgao">
			<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			<script>
				for (j=0;j<cod_orgao.length;j++) {
					document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
				}
			</script>
		</select></td>
		<td width="7%" align="right"><b>Grupo:</b>&nbsp;</td>
		<td width="16%">&nbsp;&nbsp; <select name="numGrupo">
			<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			<option value="1">DEFESA PR�VIA</option>
			<option value="2">PROCESSO</option>
			<option value="3">1� INST�NCIA</option>
			<option value="4">2� INST�NCIA</option>
			<option value="7">GERAL</option>
		</select></td>
		<td width="6%" align="right"><b>De:</b>&nbsp;</td>
		<td width="10%">&nbsp;&nbsp; <input name="datInicio" type="text"
			size="14" maxlength="10" onchange="javascript:ValDt(this,0);"
			onKeyPress="javascript:Mascaras(this,'99/99/9999');"
			"
			value="<%=consultHistParamOrgBean.getDatIni()%>"></td>
		<td align="right"><b>Ate:</b>&nbsp;</td>
		<td width="13%">&nbsp;&nbsp; <input name="datFim" type="text"
			size="14" maxlength="10" onchange="javascript:ValDt(this,0);"
			onKeyPress="javascript:Mascaras(this,'99/99/9999');"
			"
			value="<%=consultHistParamOrgBean.getDatFim()%>"></td>
		<td colspan="5" align="right">
		<button type="button" NAME="Ok"
			style="width: 44px;height: 23px;  border: none; background: transparent;"
			onClick="javascript: valida('consultaLogParam',this.form);"><IMG
			src="<%= path %>/images/bot_ok.gif" align="left"></button>
		</td>

	</tr>

	<%} else if (consultHistParamOrgBean.getAtualizarTela().equals("S")) {

                %>
	<tr>
		<td width="5%" align="right" nowrap><b>Org&atilde;o:&nbsp;</b></td>
		<td>&nbsp;&nbsp; <input name="codOrgao" type="hidden" size="10"
			maxlength="6" value="<%= OrgaoBeanId.getCodOrgao() %>"> <input
			readonly="readonly" name="sigOrgao" type="text" size="15"
			maxlength="15" value="<%= OrgaoBeanId.getSigOrgao() %>"
			style="border: 0px none;" class="sem-borda"></td>
		<td width="7%" align="right" nowrap><b>Grupo:&nbsp;</b></td>
		<td>&nbsp;&nbsp; <input name="numGrupo" type="text" size="20"
			maxlength="20"
			value="<%= consultHistParamOrgBean.getGrupo(consultHistParamOrgBean.getGrupo())%>"
			style="border: 0px none;" readonly="readonly" class="sem-borda"></td>
		<td align="right"><b>De:</b>&nbsp;</td>
		<td>&nbsp;&nbsp; <input readonly="readonly" name="datInicio"
			type="text" size="14" maxlength="10"
			onchange="javascript:ValDt(this,0);"
			onkeypress="javascript:f_hora();"
			value="<%=consultHistParamOrgBean.getDatIni()%>" class="sem-borda"></td>
		<td width="6%" align="right"><b>At&eacute;:</b>&nbsp;</td>
		<td colspan="3">&nbsp;&nbsp; <input readonly="readonly" name="datFim"
			type="text" size="20" maxlength="20"
			onchange="javascript:ValDt(this,0);"
			value="<%=consultHistParamOrgBean.getDatFim()%>" class="sem-borda"></td>
		<td width="8%" align="right">
		<button type=button NAME="Retornar"
			style="height: 21px; width: 54px;border: none; background: transparent;"
			onClick="javascript: valida('V',this.form);"><img
			src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
		</td>
		<td width="1%"></td>
		<%if (consultHistParamOrgBean.getDados().size() > 0) {%>
		<td width="4%">
		<button
			style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;"
			type="button"
			onClick="javascript: valida('ImprimeResultConsulta',this.form)"><img
			src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir"
			width="29" height="19"></button>
		</td>
		<%}%>
		<%}%>
	</tr>
</table>
</div>
<%if (consultHistParamOrgBean.getAtualizarTela().equals("S")
                    && consultHistParamOrgBean.getDados().size() > 0) {

                %>
<div
	style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 130px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;"
	id="TITULOS">
<table border="0" cellpadding="0" cellspacing="1" width="100%"
	align="center">
	<tr>
		<td colspan=2 align=right><font color="#000000">&nbsp;Ordem:&nbsp;<%=consultHistParamOrgBean.getNomOrdem()%>&nbsp;</font>
		</td>
	</tr>
</table>

<table width="100%" border="0" align="center" cellspacing="1"
	class="identacao">
	<tr>
		<td colspan="4" height="3"></td>
	</tr>
	<tr>
		<td width="40" align="center" bgcolor="<%=Cresult%>"><font
			color="#ffffff"><b>SEQ</b></font></TD>
		<td width="120" align="center" bgcolor="<%=Cresult%>"><font
			color="#ffffff"><b><a href="#" class="branco2"
			onClick="javascript: Classificacao('DAT_LOG', this.form);">DATA</a></B></font>
		</td>
		<td width="230" bgcolor="<%=Cresult%>"><font color="#ffffff"> <a
			href="#" class="branco2"
			onClick="javascript: Classificacao('NOM_DESC_OLD',this.form);"><b>PAR&Acirc;METRO</b></a>
		</font></td>
		<td width="80" align="center" bgcolor="<%=Cresult%>"><font
			color="#ffffff"><b><a href="#" class="branco2"
			onClick="javascript: Classificacao('NOM_USERNAME', this.form);">USU&Aacute;RIO</a></B></font>
		</td>
		<td width="100" bgcolor="<%=Cresult%>"><font color="#ffffff"><a
			href="#" class="branco2"
			onClick="javascript: Classificacao('VAL_PARAM_OLD',this.form);"><b>VALOR
		ANTERIOR</b></a></font></td>
		<td bgcolor="<%=Cresult%>"><font color="#ffffff"><strong><a href="#"
			class="branco2"
			onClick="javascript: Classificacao('VAL_PARAM_NEW', this.form);">VALOR
		ATUAL</a></strong></font></td>
	</tr>
</table>
</div>

<div
	style="position:absolute; width:720px; overflow: auto; z-index: 2; top: 164px; left: 50px; height: 210px; visibility: visible;"
	id="DADOS">

<table width="100%" border="0" align="center" cellspacing="1"
	class="identacao">
<%    String altOld = "";
      String altNew = "";
      int seq = 0;
      String styleOld = "";
      String styleNew = "";
      Iterator it = consultHistParamOrgBean.getDados().iterator();
      seq = 1;
      while (it.hasNext()) {
          ACSS.ConsultaHistoricoParamOrgBean myConsultParmaOrg = new ACSS.ConsultaHistoricoParamOrgBean();
          myConsultParmaOrg = (ACSS.ConsultaHistoricoParamOrgBean) it
                  .next();

          if (myConsultParmaOrg.getValParamOld().length() >= 14) {
              altOld = myConsultParmaOrg.getValParamOld();
              styleOld = ";text-decoration: underline;";
          } else {
              altOld = "";
              styleOld = "";
          }
          
          if (myConsultParmaOrg.getValParamNew().length() >= 14) {
              altNew = myConsultParmaOrg.getValParamNew();
              styleNew = ";text-decoration: underline;";
          } else {
              altNew = "";
              styleNew = "";
          }
%>
	<tr>
		<td width="40"  align="center" bgcolor="<%=Cor%>"><%=seq%></td>
		<td width="120" align="left" bgcolor="<%=Cor%>"><%=myConsultParmaOrg.getDatLog()%></td>
		<td width="230" align="left" bgcolor="<%=Cor%>"><%=myConsultParmaOrg.getNomDescNew().toLowerCase()%></td>
		<td width="80" align="left" bgcolor="<%=Cor%>">&nbsp;<%=myConsultParmaOrg.getNomUsrName()%></td>
		<td width="100" align="center" bgcolor="<%=Cor%>" title="<%=altOld%>"><input type="text"
			readonly="readonly" name="valParamOld" size="14" maxlength="14"
			style="background:background-color:<%=Cor%><%=styleOld%>"
			value="<%=myConsultParmaOrg.getValParamOld()%>" class="sem-borda"
			title="<%=altOld%>"></td>
		<td align="center" bgcolor="<%=Cor%>" title="<%=altNew%>"><input name="valParamNew"
			type="text" readonly="readonly" size="14" maxlength="14"
			style="background:background-color:<%=Cor%><%=styleNew%>"
			value="<%=myConsultParmaOrg.getValParamNew()%>" class="sem-borda"
			title="<%=altNew%>"></td>
	</tr>
<%          seq++;
       }
%>
</table>
</div>
<%} else {
                String msg = (String) request.getAttribute("semLog");
                if (msg == null)
                    msg = "";

                %>
<div
	style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 130px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;"
	id="semLog">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td height="40"></td>
	</tr>
	<tr>
		<td height="35" align="center"><b><%=msg%></b></td>
	</tr>
</table>
</div>
<%}

            %> <!--Div Erros--> <jsp:include page="../sys/DivErro.jsp"
	flush="true">
	<jsp:param name="msgErro"
		value="<%= consultHistParamOrgBean.getMsgErro()%>" />
	<jsp:param name="msgErroTop" value="230 px" />
	<jsp:param name="msgErroLeft" value="50 px" />
</jsp:include> <!--FIM_Div Erros--> <!-- Rodap�--> <!--Bot�o retornar-->

<div id="retornar"
	style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;">
<table width="25" border="0" cellpadding="0" cellspacing="0"
	class="table">
	<tr>
		<td height="26">
		<button
			style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;"
			type="button" onClick="javascript:  valida('R',this.form);"><img
			src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif"
			alt="Retornar ao Menu"></button>
		</td>
	</tr>
</table>
</div>
<div id="rodape"
	style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;">
<table width="100%" height="94" border="0" cellpadding="0"
	cellspacing="0">
	<tr>
		<td valign="bottom"
			background="images/<%= mySistema %>/detran_bg3.png"><img
			src="images/RJ/<%= mySistema %>/detran_rod_logos.png"
			width="794" height="94"></td>
	</tr>
</table>
</div>
<!-- Fim Rodap� --> <script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codOrgao.length;i++){
		if(document.UsrAtuForm.codOrgao.options[i].value==njint){
			document.UsrAtuForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script></form>
</BODY>
</HTML>

