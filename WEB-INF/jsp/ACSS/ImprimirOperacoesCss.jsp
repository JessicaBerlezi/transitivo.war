<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<style type="text/css">
DIV.quebrapagina{page-break-after: always}
table
     {
     font-family: verdana;
     font-size: 8px;
	 border: 1px solid #000000;
	 color: #000000;
     }	 
td
	{
	font-family: verdana;
	font-size: 9px;
	position: static;
	border: 0px solid #000000;
	color: #000000;
	
}
.tdborda
	{
	border: 1px solid #000000;
}
.table_linha_top
	{
	border-top: 0.1em solid #000000;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em none #ffffff;
}

.td_linha_left_top
	{
	border-top: 0.1em solid #000000;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}

.td_linha_bottom
	{
	border-top: 0.1em none ;
	border-right: 0.1em none ;
	border-bottom: 0.1em solid #000000;
	border-left: 0.1em none ;	
}
	
.td_linha_left
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em none #ffffff;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}
.td_linha_r_l
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em solid #000000;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em solid #000000;	
}

.td_linha_r_l_b
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em solid #000000;
	border-bottom: 0.1em solid #000000;
	border-left: 0.1em solid #000000;	
}

.td_linha_right
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em solid #000000;
	border-bottom: 0.1em none #ffffff;
	border-left: 0.1em none #ffffff;	
}

.td_linha_r_b
	{
	border-top: 0.1em none #ffffff;
	border-right: 0.1em solid #000000;
	border-bottom: 0.1em solid #000000;
	border-left: 0.1em none #ffffff;	
}

select
     {
	 font-family: verdana;
	 font-size: 9px;
	 border: 2px solid #000000;
	 }

textarea
     {
	 font-family: verdana;
	 font-size: 9px; 
	 border-color: 1px solid #666666; 
	 background-color: #eeeeee;
	 }

input 
	 {
	border: 1px solid #000000;
	background-color: #ffffff;
	width: 12px;
	height: 12px;
	 }
	 
.input_semborda 
	 {
	font-family: verdana;
	font-size: 9px;
	border: 0px none ;
	background-color: #eeeeee;
	}
.semborda
     {
	 border-style: none ; 
	 background-color: #ffffff;
	 }
	 
.linha
	{
	font-family: verdana;
	font-size: 9px;
	height: auto;
	margin: 0px;
	position: static;
	border: thin none #ffffff;
	
}
.fontmaior
    {
	font-family:  verdana;
	font-size: 12px
	}
	
.fontmedia
    {
	font-family:  verdana;
	font-size: 9px
	}
.fontmenor
    {
	font-family:  verdana;
	font-size: 7px
	}
.fontseis
    {
	font-family:  verdana;
	font-size: 6px
	}
</style>
