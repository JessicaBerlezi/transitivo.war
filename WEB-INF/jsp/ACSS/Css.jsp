<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->
<head>
<link rel="icon" href="./images/sys/Transitivo_title.png" type="image/x-icon">
</head>
<style type=text/css>
DIV.quebrapagina {
	page-break-after: always
}

input,select,textarea {
	font-family: verdana;
	font-size: 10px;
	border: 1px solid #999999;
}

.input {
	font-family: verdana;
	font-size: 9px;
	border: 0px;
}

button {
	font-family: verdana;
	font-size: 9px;
	border: 1px solid;
	font-variant: normal;
	color: #000000;
	background-color: #D4D4D4;
	margin: 0px;
	padding: 0px;
}

table {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	font-size: 9px;
	letter-spacing: normal;
	font-variant: normal;
	text-transform: none;
	text-decoration: none;
}

.td2 {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	border-top: 0px solid #999999;
	border-right: 1px solid #000000;
	border-bottom: 1px solid #000000;
	border-left: 0px solid #999999;
	word-spacing: 10px;
}

.texto {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	color: #d0d0d0;
	font-size: 9px;
	font-weight: bold;
	letter-spacing: normal;
	font-variant: small-caps;
	text-transform: none;
	text-decoration: none;
}

.texto2 {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	color: #666666;
	font-size: 9px;
	letter-spacing: normal;
	font-variant: normal;
	text-transform: none;
	text-decoration: none;
}

.td {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	border-top: 1px solid #999999;
	border-right: 1px solid #999999;
	border-bottom: 1px solid #999999;
	border-left: 0px solid #999999;
	word-spacing: 10px;
}

.sem-borda {
	border: 0px none;
}

td {
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	font-size: 9px;
	color: #000000;
	border: 0px;
}

A {
	COLOR: black;
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	TEXT-DECORATION: none
}

A:link {
	COLOR: #000000;
	FONT-FAMILY: Verdana, Arial, Helvetica
}

A:visited {
	COLOR: #000000;
	FONT-FAMILY: Verdana, Arial, Helvetica
}

A:hover {
	COLOR: #003366;
	FONT-FAMILY: Verdana, Arial, Helvetica;
	TEXT-DECORATION: underline
}

.branco {
	COLOR: white;
	FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
	TEXT-DECORATION: none
}

.branco:link {
	COLOR: #ffffff;
	FONT-FAMILY: Verdana, Arial, Helvetica
}

.branco:visited {
	COLOR: #e7e7e7;
	FONT-FAMILY: Verdana, Arial, Helvetica
}

.branco:hover {
	COLOR: #99ccff;
	FONT-FAMILY: Verdana, Arial, Helvetica;
	TEXT-DECORATION: underline
}

body {
	height: 100%;
	width: 100%;
/* 	background: url(images/PGMT/fundo.png); */
}

.readonly {
	background-color: #EFF5E2;
}

.transp {
	background-color: transparent;
	border-style: none;
	border-width: 0px;
	color: #ffffff;
	font-weight: bold;
}

.espaco {
	letter-spacing: 2px;
}

.espaco2 {
	letter-spacing: 1px;
}

.BordaTopo {
	margin-top: 5px;
	margin-right: 0px;
	margin-bottom: 0px;
	margin-left: 0px;
}

#container {
	position: absolute;
	top: 238px;
	width: 100%;
	height: 786px;
	float: left;
}

#blocoNotas {
	position: relative;
	top: 30px;
	padding-top: 30px;
	width: 164px;
	height: 168px;
	text-align: center;
	margin-left: 25px;
	vertical-align: middle;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
	background: url(images/PGMT/bloco_nota.png) no-repeat;
}

#ajudaSistema {
	position: relative;
	float: right;
	right: 15px;
	width: 24px;
	height: 24px;
	background: url(images/PGMT/ajuda.png) no-repeat;
}

#imgGov {
	position: relative;
	float: inherit;
	width: 126px;
	height: 111px;
	top: 30px;
	left: 40px;
	background: url(images/PGMT/img_gov.png) no-repeat;
}

#menuLateral {
	position: absolute;
	width: 212px;
	top: 72px;
	height: 100%;
	float: left;
}

#menuLateralAntigo {
	position: absolute;
	width: 212px;
	top: 72px;
	height: 100%;
	float: left;
	background: #68A6EA;
	background: -webkit-gradient(linear, left top, left bottom, from(#6eb0f8),
		to(#395c81) ); /* webkit browsers */
	background: -moz-linear-gradient(top, #6eb0f8, #395c81);
	/* firefox3.6+ */
	filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0,
		StartColorStr='#6eb0f8', EndColorStr='#395c81' ); /* IE */
}


.Sair {
	position: relative;
	margin: 2px;
	padding-bottom: 4px;
	width: 24px;
	height: 24px;
	margin: 5px;
	cursor: hand;
	border: none;
	background: url(images/PGMT/sair.png) no-repeat;
}

#areaTrabalho {
	position:absolute;
	width: 70%;
	top: 65px;
	margin: 10px;
	height: 800px;
	vertical-align: middle;
	left: 200px;

}

.menuOpcoes {
	display: none;
	text-decoration: none;
	outline: 0;
	border: none;
	z-index: 13;
	position: absolute;
	top: 140px;
	width: 650px;
	height: 450px;
	left: 50%;
	margin-left: -320px;
	margin-top: -220px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}

/* somente para IE */ 
.ie .menuOpcoes {
	display: none;
	text-decoration: none;
	outline: 0;
	border: none;
	z-index: 13;
	position: absolute;
	top: 140px;
	width: 650px;
	height: 450px;
	*left: 35%; /* IE6 */
	_left: 35%; /* IE7 */
	left: 35% \9; /* IE8 + 9 */
	margin-left: -320px;
	margin-top: -220px;
	font-family: Arial, Helvetica, sans-serif;
	font-size: 14px;
	font-weight: bold;
}

/* botoes */
#botoes {
	position: relative;
	top: 80px;
	vertical-align: middle;
}

.btDesab {
	position: absolute;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.registro {
	background: url(images/PGMT/botoes/registro_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.registro:ACTIVE {
	background: url(images/PGMT/botoes/registro_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

.recurso {
	background: url(images/PGMT/botoes/recurso_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
	width: 185px;
}

.recurso:ACTIVE {
	background: url(images/PGMT/botoes/recurso_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

.pontuacao {
	background: url(images/PGMT/botoes/pontuacao_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.pontuacao:ACTIVE {
	background: url(images/PGMT/botoes/pontuacao_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

.gerencial {
	background: url(images/PGMT/botoes/gerencial_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.gerencial:ACTIVE {
	background: url(images/PGMT/botoes/gerencial_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

.acesso {
	background: url(images/PGMT/botoes/acesso_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.acesso:ACTIVE {
	background: url(images/PGMT/botoes/acesso_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

.processos {
	background: url(images/PGMT/botoes/processo_normal.png) no-repeat;
	background-color: transparent;
	cursor: pointer;
	margin: 10px;
	width: 190px;
	height: 210px;
	border: none;
}

.processos:ACTIVE {
	background: url(images/PGMT/botoes/processo_clicado.png) no-repeat;
	background-color: transparent;
	outline: 0;
}

/* FIM de botoes */
#content {
	background: transparent;
	vertical-align: middle;
	text-decoration: none;
	margin-top: 50px;
	width: 600px;
	overflow-y: scroll;
	height: 350px;
	text-decoration: none;
}

/*-------0800---------*/
/*Esta � a posi��o do n�mero de suporte do 0800, no rodap� das p�ginas*/
#telSuporte {
	position: absolute;
	left: 35px;
	bottom: 0px;
	width: 550px;
	height: 15px;
	z-index: 10;
	background-color: transparent;
	overflow: visible;
	color: #FFFFFF;
	font-family: verdana;
	font-size: 55%;
	font-weight: bold;
	letter-spacing: 1px;
}
/********************************/
</style>

