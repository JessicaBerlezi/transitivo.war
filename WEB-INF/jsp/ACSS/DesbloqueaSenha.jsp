<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %> 
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"       scope="request" class="ACSS.UsuarioBean" /> 
<!-- Chama o Bean DesbloqueiaSenha-->
<jsp:useBean id="desbloqBean"       scope="request" class="ACSS.BloqueioSenhaBean" /> 

<html>   
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}

myUsr        = new Array(<%= UsrBeanId.getUsuariosBloqueados() %>);
cod_usuario  = new Array(myUsr.length/3);
nom_username = new Array(myUsr.length/3);
for (i=0; i<myUsr.length/3; i++) {
	cod_usuario[i] = myUsr[i*3+0] ;
	nom_username[i] = myUsr[i*3+1] ;
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
	  if (fForm.codOrgao.value==0) {
		alert("�rg�o n�o selecionado.") ;
	  }
	  else {	
	    fForm.acao.value=opcao
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'busca':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'desbloquear':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codUsuario.value==0) {
	   valid = false
	   sErro += "Usu�rio n�o selecionado. \n"
	}

    if (valid==false) alert(sErro)
	return valid ;
}
 
</script>
<input name="acao"              type="hidden" value=' '>				

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
   		<% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td  width="15%"><b>�rg�o Lota��o:&nbsp;</b> </td>
			  <td width="30%" >
					<select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				</select>
			  </td>
			  <td width="49%" ><b>Usu�rio:&nbsp;</b>
				<select name="codUsuario"> 
					<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_usuario.length;j++) {
							document.write ("<option value="+cod_usuario[j]+">"+nom_username[j])+"</option>"
						}
					</script>
				</select>
			  </td>	
	          <td width="6%">
			  		<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('busca',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>
	  </tr>
    </table>  
          <% }
			else { %>
		<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
		   <tr>
			  <td width="15%"><b>Org�o Lota��o:&nbsp;</b></td>
			  <td width="30%" >
	            <input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
			    <input disabled name="sigOrgao" type="text"  size="15" maxlength="10" value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >			  </td>
			  <td width="55%"><b>Usu�rio:&nbsp;</b>
	            <input name="codUsuario"           type="hidden" size="14" maxlength="10" value="<%= UsrBeanId.getCodUsuario() %>"> 
        		<input disabled name="nomUserName" type="text"  size="30" maxlength="20" value="<%= UsrBeanId.getNomUserName() %>" style="border: 0px none;">
			  </td>		
		  </tr>
     <% } %>			  
      </tr>	
      <tr><td colspan="2" height="10"></td></tr>	 
    </table>    
</div>
<% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>	
	  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 127px; left: 50px; height: 148px; visibility: visible;">   
   <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">  	   		
	<%
        Iterator it = desbloqBean.getDados().iterator() ;
        while (it.hasNext()) {
			// lista os dados		
			ACSS.BloqueioSenhaBean dados =  new ACSS.BloqueioSenhaBean() ;   			
			dados = (ACSS.BloqueioSenhaBean)it.next() ;
	%>
		<tr height="25" bgcolor="#DFEEF2"> 
		   <td width="157"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;Data do Desbloqueio</font></strong></td>
		   <td width="29"><strong><font color="#336699">:</font></strong></td>
		   	 <td width="534" height="25" align="left" bgcolor="#DFEEF2"><%= dados.getDataIni() %></TD>		 
		</tr>
		<tr>
          <td height="1" colspan="2"></td>
        </tr>
		<tr height="25" bgcolor="#DFEEF2"> 
		   <td><strong><font color="#666666">&nbsp;&nbsp;&nbsp;Motivo</font></strong></td>
		   <td><strong><font color="#336699">:</font></strong></td>   	 
	       <td width="534" height="25" align="left" bgcolor="#DFEEF2"><%= dados.getTxtMotivo() %></TD>	
	    </tr>
	    <tr>
          <td height="1" colspan="2"></td>
        </tr>
	    <tr height="25" bgcolor="#DFEEF2"> 
		   <td><strong><font color="#666666">&nbsp;&nbsp;&nbsp;Desbloqueado Por</font></strong></td>
		   <td><strong><font color="#336699">:</font></strong></td>
	       <td height="25" align="left" bgcolor="#DFEEF2"><%= dados.getNomUserName()%></TD>	
	    </tr>
 	   <% 

	   }
	   %>	
   </table>    
</div>
<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 273px; left: 49px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;">
  <TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	 
    <% if (((String)request.getAttribute("desapareceBotao")).compareTo("S")==0){%> 	
		 	<td align="right">
			  <button type="button" NAME="Desbloquear"  style="height: 28px; width: 92px;border: none; background: transparent;" onClick="javascript: valida('desbloquear',this.form);">
			  	<IMG src="<%= path %>/images/bot_desbloquear_det1.gif" width="90" height="26" align="left"  >
			  </button>
			</td>	
	<%}%>			
	  		<td width="20%"> </td>
			<td> 
	  		  <button type=button NAME="Retornar"  style="height: 28px; width: 73px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
				  <img src="<%= path %>/images/bot_retornar_det1.gif" width="71" height="26" >
			  </button>
			</td>
    </table>
 </div>
    
  <% } %>
<!--Div Erros-->
<%
String msgErro=desbloqBean.getMsgErro();
String mostraMsg = "hidden";
String msgErroTop="150px";
String msgErroLeft="40 px";
%>
<%@ include file="../sys/DivErro_Diretiva.jsp" %> 

<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>
<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codOrgao.length;i++){
		if(document.UsrAtuForm.codOrgao.options[i].value==njint){
			document.UsrAtuForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>			  
</form>
</BODY>
</HTML>

