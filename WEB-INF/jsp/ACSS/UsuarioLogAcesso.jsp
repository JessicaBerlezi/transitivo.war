<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>  
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto de Consulta Usu�rio por sistema-->
<jsp:useBean id="ConsultaUsuarioLogadoBeanId" scope="request" class="ACSS.ConsultaUsuarioLogadoBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />

<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) {
 switch (opcao) {
  case 'retornarLista':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'R':
      close() ;   
	  break;
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
    if (valid==false) alert(sErro)
	return valid ;
}
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"              type="hidden" value=' '>	
<input name="ordem"          type="hidden" value="DAT_OPERACAO">			

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>	
			<td width="30%"><b>Usu�rio:&nbsp;</b>
		        <input disabled name="Usuario" type="text"  size="30"  maxlength="30" value="<%= ConsultaUsuarioLogadoBeanId.getNomUserName() %>" style="border: 0px none;">
		 	</td>
		 	<td width="40%"><b>Org�o Atua��o:&nbsp;</b>
		        <input disabled name="orgaoatua" type="text"  size="20"  maxlength="20" value="<%= ConsultaUsuarioLogadoBeanId.getSigOrgaoAtuacao() %>" style="border: 0px none;">
		 	</td>
		 	<td width="30%"><b>Sistema:&nbsp;</b>
		        <input disabled name="sist" type="text"  size="15"  maxlength="15" value="<%= ConsultaUsuarioLogadoBeanId.getSistema() %>" style="border: 0px none;">
		 	</td>
      </tr>
      <tr>	
		 	<td width="50%"><b>Dt. de Entrada:&nbsp;</b>
		        <input disabled name="dtentrada" type="text"  size="30"  maxlength="30" value="<%= ConsultaUsuarioLogadoBeanId.getdataSessao() %>" style="border: 0px none;">
		 	</td>
		 	<td width="41%"><b>IP:&nbsp;</b>
		        <input disabled name="ip" type="text"  size="30"  maxlength="30" value="<%= ConsultaUsuarioLogadoBeanId.getIp() %>" style="border: 0px none;">
		 	</td>
		 	<td width="3%"></td>
			<td width="6%" align="right"> 
	  		  <button type=button NAME="Retornar"  style="height: 21px; width: 28px;border: none; background: transparent;" onClick="javascript: valida('retornarLista',this.form);" >
			  <img src="<%= path %>/images/bot_retornar_ico.gif" width="26" height="19" >
			  </button>
			</td> 	  
      </tr>
    </table> 
</div>


<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 142px; left: 50px; height: 20px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000;">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   			
  </table>
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">		  		
	<tr>
	   	 <td width="40"  align="center" bgcolor="#79B3C4"><font color="#ffffff"><b>SEQ</b></font></TD>		 		 	  
     	 <td width="100" align="center" bgcolor="#79B3C4">
     	   <font color="#ffffff"><b>FUNCIONALIDADE</b></font></TD>	
     	 <td width="200" align="center" bgcolor="#79B3C4">
     	   <font color="#ffffff"><b>NOME DA OPERA��O</b></font></TD>	
     	 <td width="100" align="center" bgcolor="#79B3C4">
     	   <font color="#ffffff"><b>NOME ABREV.</b></font></TD>
     	 <td align="center" bgcolor="#79B3C4">
     	   <font color="#ffffff"><b>DATA DA OPERA��O</b></font></TD>		  
	</tr>
   </table>
</div>   
			  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 155px; left: 50px; height: 180px; ">   
   <table id = fconsulta border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
	<%
        Iterator itOrg = ConsultaUsuarioLogadoBeanId.getDados().iterator() ;
		int seq = 1;	
		ACSS.ConsultaUsuarioLogadoBean myUsrLog =  new ACSS.ConsultaUsuarioLogadoBean() ;
        while (itOrg.hasNext()) {
			// lista os orgaos de atuacao			
			myUsrLog = (ACSS.ConsultaUsuarioLogadoBean)itOrg.next() ;
	%>       
		<tr> 	
    	   	 <td width="40" align="center" bgcolor="#DFEEF2"><%= seq %></TD>		 		 	  
             <td width="100" bgcolor="#DFEEF2">&nbsp;&nbsp;<%= myUsrLog.getSigFuncao() %> </td>
             <td width="200" bgcolor="#DFEEF2">&nbsp;&nbsp;<%= myUsrLog.getNomOperacao() %> </td>
             <td width="100" bgcolor="#DFEEF2">&nbsp;&nbsp;<%= myUsrLog.getNomAbrev() %> </td>
             <td bgcolor="#DFEEF2">&nbsp;&nbsp;<%= myUsrLog.getDatOperacao() %> </td>
 	   <% 
	   		seq++;
	   } 
	   %>
	</tr>
   </table>    
</div>
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErroTop" value= "340 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->        

<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
	  
</form>
</BODY>
</HTML>

