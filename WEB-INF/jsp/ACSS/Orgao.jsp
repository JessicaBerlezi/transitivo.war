<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../sys/ErrorPage.jsp" %>

<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Orgao logado -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 

<jsp:useBean id="UFCNHId" scope="request" class="sys.UFBean" />

<jsp:useBean id="MunicipioId" scope="request" class="TAB.MunicipioBean" />


<html>  
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: Manutencao Tabela de Orgaos :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
<!-- Carrega e monta a combo-->
<jsp:setProperty name="UFCNHId" property="j_abrevSist" value="ACSS" /> 
<jsp:setProperty name="UFCNHId" property="popupExt"    value="onChange=\"javascript: valida('buscaMunicipio',this.form)\" " />                 	  	 	     	 	  
<jsp:setProperty name="UFCNHId" property="colunaValue" value="cod_UF" />  
<jsp:setProperty name="UFCNHId" property="popupNome"   value="codUfCNHTR"  />  
<jsp:setProperty name="UFCNHId" property="popupString" value="cod_UF,SELECT cod_UF FROM TSMI_UF ORDER BY cod_UF" />                 	 



<!-- Fim de preenchimento da combo -->
function valida(opcao,fForm) {
	// se movimentacao no arquivo - nao precisa validar os campos
	if ("Top,Proximo,Anterior,Fim,Novo,Pkid".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
		fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	    return ;
	 }
	 // se inclusao ou alteracao - valida todos os campos
	 if ("12".indexOf(opcao)>=0) { 		  
	      if (veCampos(opcao,fForm)==false) return ;
	  }	  
	 // inclusao, alteracao ou exclusao 
	 if ("123".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  } 
	 switch (opcao) {  
	   case 'Z':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		 
       break;   
       case 'buscaMunicipio':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		 
       break;   
	   case 'R':
	    close() ;
	   break;
	   case 'C':
		  if (document.layers) fForm.layers["OrgaoCons"].visibility='show' ; 
	      else                 document.all["OrgaoCons"].style.visibility='visible' ; 
		  break;
	   case 'I':   // Imprimir
			temp = fForm.j_cmdFuncao.value	   
			fForm.j_cmdFuncao.value = "construcao" ;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  		  
			fForm.submit();	  
		    fForm.j_cmdFuncao.value = temp ;				
			break;
	   case 'L':  // Limpar
		  document.UsuForm.reset() ;
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;	 
  }
}
function veCampos(opcao,fForm) {
	fForm.nomOrgao.value=trim(fForm.nomOrgao.value)
	fForm.sigOrgao.value=trim(fForm.sigOrgao.value)
	fForm.codOrgao.value=trim(fForm.codOrgao.value)
	valid = true ;
	sErro = "" ;	
	verString(fForm.nomOrgao,"Nome do Org�o",0);
	verString(fForm.sigOrgao,"Sigla",0);
	verString(fForm.codOrgao,"C�digo do Org�o",0);
	verString(fForm.nomContato,"Contato do Org�o",0);
	verString(fForm.nomEndereco,"Endere�o do Org�o",0);	
	verString(fForm.numEndereco,"N�mero do Endere�o do Org�o",0);
	verString(fForm.compEndereco,"Complemento do Endere�o do Org�o",0);
	verString(fForm.nomBairro,"Bairro do Org�o",0);	
	//verString(fForm.codMunicipio,"Munic�pio do Org�o",0);
	verString(fForm.numDDD,"N�mero do DDD",0);
	verString(fForm.numCEP,"CEP do Org�o",0);
	verString(fForm.numTelefone,"Telefone do Org�o",0);	
	if (valid==false) alert(sErro) 
	return valid ;
}
function verifica_email(myobj) {
	if (valida_email(myobj)==false) {
		alert("Email inv�lido.")
	}
}
function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" OnLoad="document.UsrForm.nomOrgao.focus();" style="overflow: hidden;">
<form name="UsrForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"        type="hidden" value=' '>				
<input name="pkid"        type="hidden" value="<%=OrgaoBeanId.getPkid()%>">
<!--<input name="codOrgao"    type="hidden" value="<%=OrgaoBeanId.getCodOrgao()%>"> -->
<input name="seqEndereco" type="hidden" value="<%=OrgaoBeanId.getEndereco().getSeqEndereco()%>">	
<input name="seqTelefone" type="hidden" value="<%=OrgaoBeanId.getTelefone().getSeqTelefone()%>">	
		
<!--IN�CIO_CORPO_sistema--> 
  <div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1;  left:50px; top:64px; width:720px; height:110px;" > 
    <!--Botoes navega��o-->
    <jsp:include page="../sys/BotoesNav.jsp" flush="true" />
   <!--FIM_Botoes navega��o-->
    <table width="720" height="195" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td height="5" colspan="6" ></td></tr>
      <tr height="25"> 
        <td width="98"  bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Nome do �rg�o</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <input name="nomOrgao" type="text" size="42" maxlength="30"  value='<%=OrgaoBeanId.getNomOrgao() %>' onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"> 
        </td>
        <td width="59" bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;Sigla</font></strong></td>
        <td width="326" bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong> 
          <input name="sigOrgao" type="text" size="17" maxlength="10" value='<%=OrgaoBeanId.getSigOrgao() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
      </tr>
      <tr> 
        <td height="1" colspan="6"></td>
      </tr>      
      <tr height="25"> 
	    <td bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Codigo do �rg�o</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong> 
          <input name="codOrgao" type="text" size="17" maxlength="06" value='<%=OrgaoBeanId.getCodOrgao() %>' onKeyPress="javascript:f_num();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
        <td height="10" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;Contato</font></strong></td>
        <td  bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <input name="nomContato" type="text" size="42" maxlength="45"value='<%=OrgaoBeanId.getNomContato() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
      </tr>
      <tr> 
        <td height="1" colspan="6"></td>
      </tr>
      <tr> 
        <td height="25" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Endere&ccedil;o</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <input name="nomEndereco" type="text" size="42" maxlength="25"value='<%=OrgaoBeanId.getEndereco().getTxtEndereco() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;N&uacute;mero</font></strong></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong> 
          <input name="numEndereco" type="text" size="7" maxlength="5"value='<%=OrgaoBeanId.getEndereco().getNumEndereco() %>' onKeyPress="javascript:f_num();" onFocus="javascript:this.select();"></td>
      </tr>
      <tr> 
        <td height="1" colspan="6"></td>
      </tr>
      <tr> 
        <td height="25" bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Complemento</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <input name="compEndereco" type="text" size="12" maxlength="10"value='<%=OrgaoBeanId.getEndereco().getTxtComplemento() %>' onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;Bairro</font></strong></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong> 
          <input name="nomBairro" type="text" size="32" maxlength="30"value='<%=OrgaoBeanId.getEndereco().getNomBairro() %>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
      </tr>
      <tr> 
        <td height="1" colspan="6"></td>       
      </tr>
      <tr> 
        <td height="25" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;UF</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
            <jsp:getProperty name="UFCNHId" property="popupString" />                   	 
		</td>
        <td bgcolor="#DFEEF2"> <strong><font color="#666666">&nbsp;Cidade</font></strong></td>
        <td bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong>
		<jsp:getProperty name="MunicipioId" property="popupString" />        </td>
      </tr>
      <tr> 
        <td height="1" colspan="6"></td>
      </tr>
      <tr> 
        <td height="25" bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;CEP</font></strong></td>
        <td width="110"  bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <strong><font color="#336699"> </font></strong>          <strong><font color="#336699">
          <input name="numCEP" type="text" size="10" maxlength="8"value='<%=OrgaoBeanId.getEndereco().getNumCEP() %>' onKeyPress="javascript:f_num();" onFocus="javascript:this.select();">
          </font></strong></td>
		  <td width="37" align="right" bgcolor="#DFEEF2"><strong></strong><strong></strong> 
          <strong><font color="#666666">DDD</font></strong></td>
        <td width="86" bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
           <input name="numDDD" type="text" size="5" maxlength="3"value='<%=OrgaoBeanId.getTelefone().getCodDDD() %>' onKeyPress="javascript:f_num();" onFocus="javascript:this.select();">
		</td>
        <td bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;Telefone</font></strong></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong> 
          <input name="numTelefone" type="text" size="15" maxlength="8"value='<%=OrgaoBeanId.getTelefone().getNumTelResidencial() %>' onkeypress="javascript:f_tel();" onfocus="javascript:this.select();">      
          <strong><font color="#666666">&nbsp;&nbsp;E-mail:&nbsp;&nbsp;</font></strong><input type="text" name="txtEmail" size="30" maxlength="60" value='<%=OrgaoBeanId.getTxtEmail() %>'  onfocus="javascript:this.select();" onchange="verifica_email(this);"></td>
      
      </tr>
	  <tr> 
        <td height="1" colspan="6"></td>
      </tr>
	  <tr height="25"> 
        <td bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Autoridade</font></strong> 
		</td>
        <td colspan="3" bgcolor="#DFEEF2">
          <strong><font color="#336699">:</font></strong>
          <%if((OrgaoBeanId.getIntegrada()).equals("S")){%>
          <input name="integrada" type="radio" value="S" checked class="sem-borda" style="width: 13px; height: 13px; ">
        <%}else{%>
			<input name="integrada" type="radio" value="S" class="sem-borda" style="width: 13px; height: 13px; ">
		<%}%>
        <strong><font color="#666666">Integrada</font></strong>	
		&nbsp;&nbsp;&nbsp;
		 <%if((OrgaoBeanId.getIntegrada()).equals("N")){%>
        	<input name="integrada" type="radio" value="N" checked class="sem-borda" style="width: 13px; height: 13px;">
 		  <%}else{%>
			<input name="integrada" type="radio" value="N" class="sem-borda" style="width: 13px; height: 13px;">
		<%}%>
 		<strong><font color="#666666">Isolada</font></strong>	</td>
        <td bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;Emite VEX</font></strong></td>
        <td bgcolor="#DFEEF2" ><strong><font color="#336699">:</font></strong>
    	<%if((OrgaoBeanId.getVex()).equals("S")){%>
        	<input name="vex" type="checkbox" value="S" checked class="sem-borda" style="width: 13px; height: 13px;"> 
   		<%}else{%>
			<input name="vex" type="checkbox" value="S" class="sem-borda" style="width: 13px; height: 13px;"><%}%>  
		</td>
      </tr>
	  <tr> 
        <td height="1" colspan="6"></td>
      </tr>
  	  <tr height="25"> 
        <td bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Autua</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2">
          <strong><font color="#336699">:</font></strong>
			<input name="indAutua" type="radio" value="S" <% if(OrgaoBeanId.getIndAutua().equals("S")){%>checked<%}%> class="sem-borda" style="width: 13px; height: 13px;"> 
		   &nbsp;<strong><font color="#666666">Sim</font></strong>&nbsp;&nbsp;&nbsp;
		   <input name="indAutua" type="radio" value="N" <% if(OrgaoBeanId.getIndAutua().equals("N")){%>checked<%}%> class="sem-borda" style="width: 13px; height: 13px;">&nbsp;
		   <strong><font color="#666666">N�o</font></strong>
        </td>
		<td bgcolor="#DFEEF2"><strong><font color="#666666">&nbsp;PGMT DOL</font></strong></td>
        <td colspan="3" bgcolor="#DFEEF2">
          <strong><font color="#336699">:</font></strong>
			<input name="indSmitDol" type="radio" <% if(OrgaoBeanId.getIndSmitDol().equals("S")){%>checked<%}%> value="S" class="sem-borda" style="width: 13px; height: 13px;"> 
		   &nbsp;<strong><font color="#666666">Sim</font></strong>&nbsp;&nbsp;&nbsp;
		   <input name="indSmitDol" type="radio" <% if(OrgaoBeanId.getIndSmitDol().equals("N")){%>checked<%}%> value="N" class="sem-borda" style="width: 13px; height: 13px;">&nbsp;<strong><font color="#666666">N�o</font></strong>
        </td>
      </tr>
      <tr> 
        <td height="15" colspan="6"></td>
      </tr>
    </table>
      
<!--Botoes padr�o-->
    <jsp:include page="../sys/Botoes.jsp" flush="true" />
	<!--FIM_Botoes padr�o-->
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= OrgaoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "260 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<!--FIM_CORPO_sistema--> 
<jsp:include page="orgaoConsulta.jsp" flush="true" > 
  <jsp:param name="OrgaoConsTop"  value= "80 px" />
  <jsp:param name="OrgaoConsLeft" value= "50 px" />  
</jsp:include>


<script>
// posiciona o popup na descri�ao equivalente ao Orgao
nj = '<%= OrgaoBeanId.getEndereco().getCodUF() %>'
if ((nj.length>0) && (nj!="")){
  for(i=0;i<document.UsrForm.codUfCNHTR.length;i++){
    if(document.UsrForm.codUfCNHTR.options[i].text==nj){
       document.UsrForm.codUfCNHTR.selectedIndex=i;	  
    }
  }
}  
</script>

</form>
</body>
</html>

