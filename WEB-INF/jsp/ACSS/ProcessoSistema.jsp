<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:useBean id="ProcessoId" scope="request" class="ACSS.ProcessoSistemaBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: PGMT - Par�metros por Sistema :: </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaSistema':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  else alert(sErro)
	  break ;
	  
  case 'C':
     document.all["escolha"].style.visibility='visible';
     break;
     
  case 'G':
     if (fForm.codSistemaCp.value==0) {
		alert("Sistema de destino n�o selecionado. \n")
	 }
	 if(fForm.codSistema.value == fForm.codSistemaCp.value){
	    alert( "Sistema de destino deve ser diferente do sistema selecionado. \n")
	 }
	 else{
	   fForm.acao.value=opcao
	   fForm.target= "_self";
	   fForm.action = "acessoTool";  
	   fForm.submit();	  				  
	 }
  	 break ;

   case 'R':
	  if (fForm.atualizarDependente.value=="S") {
		fForm.atualizarDependente.value="N"	  
		fForm.acao.value=opcao
		fForm.target= "_self";
		fForm.action = "acessoTool";  
		fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	
	 fForm.j_cmdFuncao.value = temp ;		   
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}


function veCampos(fForm) {
valid   = true ;
sErro = "" ;
// verifica se o popup foi selecionado (value do popupNome = )
if (fForm.codSistema.value==0) {
   valid = false
   sErro = sErro + "Sistema n�o selecionado. \n"
}
// fim de validar combo
for (k=0;k<fForm.nomDescricao.length;k++) {
   var ne = trim(fForm.nomDescricao[k].value)
   if (ne.length==0) continue ;
	<!-- verificar duplicata --> 
	for (m=0;m<fForm.nomDescricao.length;m++) {
	   var dup = trim(fForm.nomDescricao[m].value)
	   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
		   sErro += "Processo em duplicata: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n" ;
		   valid = false ;			   
	   }
	}
}
return valid ;
}
 
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="parametroForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />

<input name="acao" type="hidden" value=' '>				

  <div id="WK_SISTEMA" style="position:absolute; width:493px; overflow: visible; z-index: 1; top: 75px; left=35px;left: 166px;" > 
    <table border="0" cellpadding="0" cellspacing="1" width=480  align="center">
      <tr> 
        <td valign="top" colspan="2"><img src="images/inv.gif" width="3" height="15"></td>
      </tr>
      <tr>
		  
        <td width="20%" align="left" valign="top"><b>&nbsp;Sistema:&nbsp;</b> 
          <input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
   			<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
				<select name="codSistema"> 
				   <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				       <script>
					      for (j=0;j<cod_sistema.length;j++) {
						    document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					      }
				       </script>
				</select>
          <% }
			else { %>
              <input name="codSistema" type="hidden" size="10" maxlength="20"  value="<%= SistId.getCodSistema() %>"> 
		      <input disabled name="nomSistema" style="border: 0px none;" type="text" size="30" value="<%= SistId.getNomSistema() %>">
		   	<% } %>			
	    </td>
   		  <% if (SistId.getAtualizarDependente().equals("S")==false){ %>			  
 	             <td align=right width=20%>&nbsp;&nbsp;&nbsp; 
                <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistema',this.form);">	
          <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left"></button>
			 </td>
	    <% } %>
      </tr>	
      <tr> 
        <td valign="top" colspan=2><img src="images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>
<!--INICIO BOTOES-->  
  <TABLE border="0" cellpadding="0" cellspacing="0" width="400" align="center">
    <TR>
		<% if (SistId.getAtualizarDependente().equals("S")){ %>			
	 	   <td align="left" width="33%"  height="15" > 
                  <button type=button NAME="Atualizar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19"></button>
     	   </td>
	       <td  align="center" width="34%" > 
                  <button type=button NAME="Imprimir"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('I',this.form);">	
                  <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19"></button>
    	   </td>
	       <td align="right" width="33%"> 
                  <button type=button NAME="Retornar"  style="height: 21px; width: 54px; border: none; background: transparent;" onClick="javascript: valida('R',this.form);" > 
                  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19"></button>
           </td>
		<%} %>
    </TR>
  </TABLE>  
<!--FIM BOTOES-->  
</div>
  <div style="position:absolute; width:720px; overflow: auto; z-index: 1; left: 50px; top: 140px; height: 64px;"> 
    <table border="0" cellpadding="1" cellspacing="1" width="720"  align="center">
      <tr bgcolor="#79B3C4"> 
        <td width="34"  align="center" rowspan=2><font color="#ffffff"><b>Seq</b></font></TD>
        <td width="449" bgcolor="#79B3C4" colspan="2"><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Descri��o</b></font></TD>
        <!--
        <td width="227" bgcolor="#79B3C4" ><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Ordem</b></font></TD>
        -->
      </tr>
      <tr bgcolor="#79B3C4"> 
        <td bgcolor="#79B3C4" colspan=2><font color="#ffffff"><b>&nbsp;&nbsp;&nbsp;Processo</b></font></TD>
      </tr>
    </table>
<!--FIM CABEC DA TABELA-->
</div>

  <div style="position:absolute; width:720; overflow: auto; z-index: 1; left: 50px; top: 171px; height: 175px;"> 
    <table border="0" cellpadding="1" cellspacing="1" width="100%"  align="center">
      <% 
					if (SistId.getAtualizarDependente().equals("S")) {				   
  	            	   int ocor   = SistId.getParametros().size() ;
					   if (ocor<=0) ocor= SistId.getParametros(20,5).size() ;
					   for (int i=0; i<ocor; i++) {				   %>
      <tr bgcolor="#DFEEF2">
		<td width="33" rowspan=2 align="center" bgcolor="#DFEEF2"> 
            <input name="seq"          type="text"   readonly size="3" value="<%= i+1 %>">
		    <input name="codProcesso"  type="hidden" value="<%= SistId.getProcessos(i).getCodProcesso() %>"></td>		
		<td width="450"><input name="nomDescricao"   type="text" size="60" maxlength="0"  value="<%= SistId.getProcessos(i).getNomDescricao() %>" onKeyPress="javascript:f_end();" onFocus="javascript:this.select();"onChange="this.value=this.value.toUpperCase()"></td>
		<td><input name="nomProcesso"  type="text"   size="50" maxlength="60"  value="<%= SistId.getProcessos(i).getNomProcesso() %>"  onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"></td>
		
	</tr>
	  <tr> 
        <td height="1" colspan="4"></td>			
	</tr>
<% 						}
					}
					else { 
					   for (int i=0; i<20; i++) {				   %>					
  	<tr bgcolor="#DFEEF2">
		<td width="33" align="center" rowspan=2 > 
          <input name="seq"  readonly  type="text" size="3" value="<%= i+1 %>">
		  <input name="codProcesso" type="hidden"  value="">
        </td>
		<td width="450"> 
          <input name="nomDescricao" readonly type="text" size="75" maxlength="400" value="">		
        </td>		
        <td>
          <input name="nomProcesso" readonly type="text" size="75" maxlength="100"  value="">          
        </td>         
	</tr>
	<tr>
	   	<td height="1" colspan="4"></td>			
	</tr>
					   
<%						}					
					}   %>
  </table>
</div>
<jsp:include page="Retornar.jsp" flush="true" />
<!-- Rodap�-->
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->
<!-- Combo para escoha de orga para copia de parametros -->
  <div id = "escolha"  style="position:absolute; top: 87px; width: 382px; height: 71px; left: 398px; overflow: visible; z-index: 15; visibility: hidden;" > 
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
	 <tr><td width="79%" align="center"><b>Sistema de Destino:</b>&nbsp; 
         <select name="codSistemaCp">
           <option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
           <script>
	         for (j=0;j<cod_sistema.length;j++) {
	    	  document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
	         }
	     </script>
         </select>
      </td>
	   <td width="21%">
	     <button type="button" NAME="Ok" style="width: 30px; height: 21px;  border: none; background: transparent;"  onClick="javascript: valida('G',this.form);">	
        <IMG src="<%= path %>/images/bot_ok.gif" width="26" height="19" align="left"></button>       </td>
   	  </tr>
	</table>
  </div>
<!--FIM_CORPO_sistema-->
  <!--Div Erros-->
  <jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= SistId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.parametroForm.codSistema.length;i++){
    if(document.parametroForm.codSistema.options[i].value==njint){
       document.parametroForm.codSistema.selectedIndex=i;	  
    }
  }
}  
</script>
</form>
</BODY>
</HTML>


