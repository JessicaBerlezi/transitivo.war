<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"  scope="request" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Perfil/Sistema/Lotacao -->
<jsp:useBean id="PerfilLotBeanId" scope="request" class="ACSS.PerfilLotacaoBean" /> 

<html>
<head>
<%@ include file="Css.jsp" %>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrLotForm" method="post" action="">
<%@ include file="Cab_Diretiva.jsp" %>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaosACSS(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}
myUsr        = new Array(<%= UsrBeanId.getUsuarios() %>);
<!--cod_usuario  = new Array(myUsr.length/3);-->
<!--nom_username = new Array(myUsr.length/3);-->
cod_usuario  = new Array();
nom_username = new Array();

j = 0 ;
for (i=0; i<myUsr.length/3; i++) {
	if (myUsr[i*3+0]!=<%= UsuarioBeanId.getCodUsuario() %>) {
		cod_usuario[j] = myUsr[i*3+0] ;
		nom_username[j] = myUsr[i*3+1] ;
		j++ ;		
	}
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
	  if (fForm.codOrgao.value==0) {
		alert("�rg�o n�o selecionado.") ;
  	  }
	  else {	
		fForm.acao.value=opcao
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'buscaSistemas':
      if (veCampos(fForm)==true) {   
		fForm.acao.value=opcao
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'O':  // Esconder os erro
	  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  	  
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codOrgao.value==0) {
	   valid = false
	   sErro = "�rg�o n�o selecionado. \n"
	}	
	if (fForm.codUsuario.value==0) {
	   valid = false
	   sErro += "Usu�rio n�o selecionado. \n"
	}
		
    if (valid==false) alert(sErro)
	return valid ;
}
 
</script>
<input name="acao"              type="hidden" value=' '>				

<div id="WK_SISTEMA" style="position:absolute; width:680px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>
      <tr>
   		<% if (OrgaoBeanId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td align="left" width="40%"><b>�rg�o Lota��o:&nbsp;</b>
				<select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_orgao.length;j++) {
						document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
					}
				</script>
				</select>
			  </td>
			  <td align="left" width="40%"><b>&nbsp;&nbsp;&nbsp;&nbsp;Usu�rio:&nbsp;</b>
				<select name="codUsuario"> 
				<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_usuario.length;j++) {
						document.write ("<option value="+cod_usuario[j]+">"+nom_username[j])+"</option>"
					}
				</script>
				</select>
			  </td>		  
	          <td align="right" width="10%" ><button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaSistemas',this.form);">	
		        <IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>
			  </td>
          <% }
			else { %>
			  <td align="left" width="40%"><b>Org�o Lota��o:&nbsp;</b>
	            <input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
		        <input disabled name="sigOrgao" type="text"   size="24" maxlength="10" value="<%= OrgaoBeanId.getSigOrgao() %>">
			  </td>
			  <td width="4%"></td>
			  
			  <td align="left" width="30%"><b>Usu�rio:&nbsp;</b>
	            <input name="codUsuario"           type="hidden" size="14" maxlength="10" value="<%= UsrBeanId.getCodUsuario() %>"> 
		        <input disabled name="nomUserName" type="text"   size="24" maxlength="20" value="<%= UsrBeanId.getNomUserName() %>">
			  </td>			  
			  <td align="right" width="13%"> 
<%        		if (PerfilLotBeanId.getPerfisLotacao().size()>0) { %> 			  
 	                 <button type="button" NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
	                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align="left"  ></button>
<%              }   %>					  
	          </td>	
	          <td width="13%" align="right"> 
		          <button type=button NAME="Retornar"  style="height: 18px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" > 
        		  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" ></button>
	          </td>
		<% } %>			  
      </tr>	
      <tr> 
        <td valign="top" colspan="2"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>    
</div>


<div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 111px; left: 50px;">
  <table border="0" cellpadding="4" cellspacing="1" width="100%"  align="center">  	   		
	<tr bgcolor="#79B3C4"> 	
	   	 <td width=34  rowspan="2" align="center"><font color="#ffffff">&nbsp;&nbsp;<b>SEQ</b></font></TD>		 		 	  
	   	 <td width=206><font color="#ffffff">&nbsp;&nbsp;<b>SISTEMA</b></font></TD>		 
	   	 <td width=221><font color="#ffffff">&nbsp;&nbsp;<b>PERFIL</b></font></TD>		 
	   	 <td ><font color="#ffffff">&nbsp;&nbsp;<b>NIVEL</b></font></TD>		 

	</tr>
	<tr bgcolor="#79B3C4">
	   <td  colspan="3" bgcolor="#79B3C4"><font color="#ffffff">&nbsp;&nbsp;<b>JUSTIFICATIVA</b></font></td>
	</tr>
   </table>
</div>   
   <% if (OrgaoBeanId.getAtualizarDependente().equals("S")){ %>				  
<div style="position:absolute; width:720px; overflow: auto; z-index: 1; top: 154px; left: 50px; height: 179px;">   
   <%
        Iterator it = PerfilLotBeanId.getPerfisLotacao().iterator() ;
		int seq = 1;	
		ACSS.PerfilLotacaoBean mySist =  new ACSS.PerfilLotacaoBean() ;
        while (it.hasNext()) {
			// pega o sistema			
			mySist = (ACSS.PerfilLotacaoBean)it.next() ;
	%>
	  <table border="0" cellpadding="4" cellspacing="1" width="100%"  align="center">  	   		
		<tr bgcolor="#DFEEF2"> 	
		   	 <td width=34  rowspan="2" align="center"><%= seq %>		  </TD>		 		 	  
		   	 <td width=206>&nbsp;&nbsp;
                 <input name="codSistema" type="hidden" size="13" maxlength="10" value='<%=mySist.getCodSistema() %>'>
		  <%= mySist.getNomSistema() %></TD>		 
		   	 <td width=221 bgcolor="#DFEEF2">&nbsp;&nbsp;
				<select name="codPerfilUsrnov"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
			 <%
	         Iterator itp = mySist.getPerfis().iterator() ;
			 ACSS.PerfilBean myPerf =  new ACSS.PerfilBean() ;
	         while (itp.hasNext()) {
				// pega o perfil do sistema			
				myPerf = (ACSS.PerfilBean)itp.next() ;
			 %>
				<option value="<%= myPerf.getCodPerfil() %>" <%= sys.Util.isSelected(myPerf.getCodPerfil(),mySist.getCodPerfilUsr()) %>><%= myPerf.getNomDescricao() %></option>
			 <%
			 }
			 %>
		  </select></TD>		 
		   	 <td >&nbsp;&nbsp;
				<select name="codNivelUsrnov"> 
				<option value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<option value="0" <%= sys.Util.isSelected("0",mySist.getCodNivelUsr()) %>>Usu�rio</option>
				<option value="1" <%= sys.Util.isSelected("1",mySist.getCodNivelUsr()) %>>Administrador Local</option>
				<option value="2" <%= sys.Util.isSelected("2",mySist.getCodNivelUsr()) %>>Administrador Global</option>
		  </select></TD>
		</tr>
		<tr bgcolor="#DFEEF2">
	      <td  colspan="3" bgcolor="#DFEEF2">	        
		  <input name="txtJustificativa" type="text" size="100" maxlength="300" value='<%=myPerf.getTxtJustificativa()%>' onKeyPress="javascript:f_end();" onChange="this.value=this.value.toUpperCase()"></td>
	   </tr>
	  <tr> 
        <td height="3" colspan="4" bgcolor="#79b3c4"></td>			
	  </tr>
      </table>  
	   <% 
	   		seq++;
	   } 
	   %>			  
</div>
   <% } %>	




<!--FIM_CORPO_sistema-->
  
<!--Div Erros-->
<%
String msgErro=PerfilLotBeanId.getMsgErro();
String mostraMsg = "hidden";
String msgErroTop="50px";
String msgErroLeft="80 px";
%>
<%@ include file="../sys/DivErro_Diretiva.jsp" %> 
<!--FIM_Div Erros-->


<!--FIM_Div Erros-->

<!-- Rodap�-->
<%@ include file="Retornar_Diretiva.jsp" %>

<%@ include file="Rod_Diretiva.jsp" %>
<!-- Fim Rodap� -->

<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrLotForm.codOrgao.length;i++){
		if(document.UsrLotForm.codOrgao.options[i].value==njint){
			document.UsrLotForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>			  
</form>
</BODY>
</HTML>

