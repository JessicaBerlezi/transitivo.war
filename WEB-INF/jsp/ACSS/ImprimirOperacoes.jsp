<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="SistemaBeanId" scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" /> 
<!-- Chama o Objeto do Sistema logado -->

<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = "Alterar Senha" ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String cor = "#DFEEF2" ;
    if ("REC".equals(mySistema)) {
		 cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;		 		 
	}	
    if ("DWN".equals(mySistema)) {
		 cor = "#faeae5" ;
		 nomeSistema= "DOWNLOADS" ;
	}
    if ("PNT".equals(mySistema)) {
		 cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;		 		 
	}	
%>


<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 
mySistema   = '<%=SistemaBeanId.getNomSistema()%>';
mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
if (mySistema!='PGMT - CONTROLE DE ACESSO' && mySistema!='PGMT - GER�NCIAL')
{
  cod_sistema = new Array(1);
  nom_sistema = new Array(1);
  cod_sistema[0]='<%=SistemaBeanId.getCodSistema()%>'
  nom_sistema[0]='<%=SistemaBeanId.getNomSistema()%>'
}
else
{
  cod_sistema = new Array(mySist.length/2);
  nom_sistema = new Array(mySist.length/2);
  for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
  }
}
function valida(opcao,fForm) {
 switch (opcao) {
   case 'Imprimir':
		if (fForm.codSistema.value==0) {
		   alert("Sistema n�o selecionado. \n")
		}
		else {
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
		}
	  break ; 
   case 'R':
      close() ;   
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;	  
  }
}
</script>
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="operacoesForm" method="post" action="">


<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
 <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
      <td width="187" valign="middle" align="center"><font color="#999999">
		Vers�o 1.0&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font></td>
      <td width="280" valign="middle" class="table" align="right">
          <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font>
      </td>
      <td height="85" valign="middle"><font color="#FFFFFF">&nbsp;</font></td>
      <td width="46"  valign="top" align="left">
			<button  type="button"  style="height: 33px; width: 35px; border: none; background: transparent; cursor: hand;" name="j_Ajuda"  onClick="javascript: j_ajuda(this.form);" >
				<img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="34" height="32" border="0"></button>
	  </td>
    </tr>
  </table>
  
<input name="j_id"          type="hidden" value="<%=RequisicaoBeanId.getId()%>">
<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">
</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:468px; height:23px; z-index:14; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr><td><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">
		<% } %>
		&nbsp;<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
  </table>
</div>





<input name="acao" type="hidden" value=' '>				
  <div id="WK_SISTEMA" style="position:absolute; overflow: visible; z-index: 1; left:204px; top: 150px; width:400px; height:85px;" > 
    <!--INICIO CABEC DA TABELA-->
    <table border="0" cellpadding="0" cellspacing="1" width=400  align=center>
      <tr>	  
        <td width=70% valign="top"><b>Sistema:&nbsp;</b> 
			<select name="codSistema"> 
			<script>
				for (j=0;j<cod_sistema.length;j++) {
					document.write ("<option value=" + cod_sistema[j]+ ">" + nom_sistema[j])+"</option>"
				}
			</script>
			</select>
 	    </td>
        <td align=center width=30%>&nbsp;&nbsp;&nbsp; 
          <button type=button NAME="Imprimir"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('Imprimir',this.form);">	
              <IMG src="<%= path %>/images/bot_imprimir.gif" width="52" height="19">
          </button>			
		</td>	       	
      </tr>	
      <tr><td height="2" colspan=2></td></tr>	 
  </table>
</div>
<!-- Rodap�-->

<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
  for(i=0;i<document.ImprimirOperacoesForm.codSistema.length;i++){
    if(document.ImprimirOperacoesForm.codSistema.options[i].value==njint){
       document.ImprimirOperacoesForm.codSistema.selectedIndex=i;	  
    }
  }
}  
</script>

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:9; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  <img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!--texto 0800-->
<jsp:include page="../sys/telSuporte.jsp" flush="true" /> 
<!--FIM texto 0800-->
<!-- Fim Rodap� -->
</form>
</body>
</html>




