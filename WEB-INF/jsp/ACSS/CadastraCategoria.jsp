<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path  = request.getContextPath(); %>
<%@ page errorPage = "ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<jsp:useBean id="CadCatId"      scope="request" class="ACSS.CadastraCategoriaBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#79B3C4";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 Cresult = "#be7272";
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 Cresult = "#a8b980";
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
 		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function valida(opcao,fForm) 
{
  switch (opcao) {
   case 'I':
     if (veCampos(fForm)==true) 
     {
//		 temp = fForm.j_cmdFuncao.value;
	 	 fForm.j_cmdFuncao.value = "CadastraCategoriaCmd";
	     fForm.acao.value = "I";
	 	 fForm.target= "_self";
	 	 fForm.action = "acessoTool";  		  
	 	 fForm.submit();	
//	 	 fForm.j_cmdFuncao.value = temp;
	 }
	 else alert(sErro);	 
	 break;
   case 'R': close();
 	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
    case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
 }
}
//--------------------------------------------------------------------------------
function veCampos(fForm) 
{
	valid   = true ;
	sErro = "" ;

	for (k=0;k<fForm.desCat.length;k++) {
	   var descat = trim(fForm.desCat[k].value);
	   var email  = trim(fForm.emailCat[k].value);
	   if(descat =="" && email!=""){
	   	 valid = false
	   	 sErro = sErro + "Categoria deve ser preenchida.\n";
	   }
	}
	
	for (k=0;k<fForm.desCat.length;k++) {
	   var ne = trim(fForm.desCat[k].value)
	   if (ne.length==0) continue ;
		<!-- verificar duplicata --> 
		for (m=0;m<fForm.desCat.length;m++) {
		   var dup = trim(fForm.desCat[m].value)
		   if ((m!=k) && (dup.length!=0) && (dup==ne)) {
			   sErro += "Categoria duplicada: (seq(s): "+(k+1)+","+(m+1)+") "+ne+ "\n";
			   valid = false;
		   }
		}
	}
	return valid;
}
function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

</script>
</head>

<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="juntaForm" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->
 
  <input name="acao" type="hidden" value="">


<!--INICIO BOTOES-->  
<div style="position:absolute; width:580px; overflow: auto; z-index: 1; left: 50px; top: 90px; height: 191px;"> 
<TABLE border="0" cellpadding="0" cellspacing="0" width="100%" align="center">
    <TR>
	 	   <td width="33%" align="center"> 
  			  	 <button type="button" NAME="Ok" style="width: 28px; height: 21px;  border: none; background transparent;"  onClick="javascript: valida('I',this.form);">	
	    	     <IMG src="<%= path %>/images/bot_ok.gif" align="left" width="26" height="19"></button>																			
     	   </td>
    </tr>
</TABLE>  
</div>
<!--FIM BOTOES-->  
  <div style="position:absolute; width:580px; overflow: auto; z-index: 30; left: 50px; top: 120px; height: 20px"> 
    <table border="0" cellpadding="0" cellspacing="1" width="580"  align="center">  	   		
	<tr bgcolor="<%=Cresult%>"> 
        <!--td width=130 align="center" bgcolor="#be7272"><font color="#ffffff"><b>Seq</b></font></TD-->		 	  
        <td width=260 align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>Categoria </b></font></TD>		 
        <td   align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>E-mail</b></font></TD>		 
	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->
  <div style="position:absolute; width:580px; overflow: auto; z-index: 1; left: 50px; top: 134px; height: 191px;"> 
    <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <% 
          int id = CadCatId.getVetCadCats().size();
	      for (int i=0; i<id; i++) {
	  %>
			      <tr bgcolor="<%=Cor%>">
				   <!--td width=129 align="center"> 
				       <input name="seq"    type="hidden" size="10" maxlength="10" value="<//%=CadCatId.getCadCat(i).getCodCategoria().trim() %>">
				   </td-->
				   <td width=260 align="center"> 
				       <input name="codCat" type="hidden" size="10" maxlength="10" value="<%=CadCatId.getCadCat(i).getCodCategoria().trim()%>">
				       <input name="desCat" type="text"   size="30" maxlength="49"  value="<%=CadCatId.getCadCat(i).getDesCat().trim() %>" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
				   </td>			
				   <td  align="center"> <input name="emailCat" type="text" size="50" maxlength="99"  value="<%=CadCatId.getCadCat(i).getEmailCat().trim() %>" onfocus="javascript:this.select();"> </td>
      </tr>
		<% }
		   int a=0,i=id;
		   while(a<=20) {  %>					
    		  	<tr bgcolor="<%=Cor%>">
					<!--td width=129 align="center"--> 
					  <input name="codCat"   type="hidden" size="10" maxlength="10" value=""><!--/td-->
				   	<td width=260 align=center> <input name="desCat"   type="text" size="30" maxlength="49" value="" onkeypress="javascript:f_end();" onfocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()"> </td>			
			    	<td  align=center>          <input name="emailCat" type="text" size="50" maxlength="99" value="" onfocus="javascript:this.select();" > </td>
				</tr>
			
		<%  i++; a++; 
		   } %>
  </table>
</div>
<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
<jsp:param name="msgErro" value= "<%=CadCatId.getMsgErro()%>" />
<jsp:param name="msgErroTop"  value= "230 px" />
<jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</BODY>
</HTML>