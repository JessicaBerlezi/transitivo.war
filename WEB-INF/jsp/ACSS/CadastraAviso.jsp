<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"    scope="session" class="ACSS.UsuarioBean" />
<jsp:useBean id="AvisoBeanId"      scope="request" class="ACSS.AvisoBean" />
<jsp:useBean id="RequisicaoBeanId" scope="request" class="sys.RequisicaoBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="OrgaoBeanId" scope="request" class="ACSS.OrgaoBean" /> 
<jsp:setProperty name="OrgaoBeanId" property="j_abrevSist" value="ACSS" />  	 	     	 	  
<jsp:setProperty name="OrgaoBeanId" property="colunaValue" value="COD_ORGAO" />  
<jsp:setProperty name="OrgaoBeanId" property="popupExt"    value="onchange=valida('orgaoUsuario',this.form)" />
<jsp:setProperty name="OrgaoBeanId" property="popupNome"   value="ORGAO"  />  
<jsp:setProperty name="OrgaoBeanId" property="popupString" value="SIG_ORGAO,SELECT COD_ORGAO,SIG_ORGAO FROM TSMI_ORGAO ORDER BY SIG_ORGAO" />

<jsp:useBean id="PerfilSistemaBeanId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:setProperty name="PerfilSistemaBeanId" property="j_abrevSist" value="ACSS" />  	 	     	 	  
<jsp:setProperty name="PerfilSistemaBeanId" property="colunaValue" value="COD_SISTEMA" />  
<jsp:setProperty name="PerfilSistemaBeanId" property="popupExt"    value="onchange=valida('perfilSistemaUsuario',this.form)" />
<jsp:setProperty name="PerfilSistemaBeanId" property="popupNome"   value="PERFIL"/>  
<jsp:setProperty name="PerfilSistemaBeanId" property="popupString" value="NOM_SISTEMA,SELECT COD_SISTEMA,NOM_SISTEMA FROM TCAU_SISTEMA ORDER BY NOM_SISTEMA" />


<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#be7272";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>
<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: Manutencao Tabela de Usuarios :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>

<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function vechck( fForm )
{
	 if(fForm.indPrioridade[0].checked == true)   fForm.indPriH.value = "A";
	 else
	 if(fForm.indPrioridade[1].checked == true)   fForm.indPriH.value = "N";
}

function veChecked( fForm )
{
     bol = true;
	 if(fForm.radiobutton[0].checked == true)   fForm.tipoH.value = "1";
	 else
	 if(fForm.radiobutton[1].checked == true)   fForm.tipoH.value = "2";
	 else
	 if(fForm.radiobutton[2].checked == true)   fForm.tipoH.value = "3";
	 else
	 if(fForm.radiobutton[3].checked == true)   fForm.tipoH.value = "4";
	 else
     bol = false;
 return bol;	  
}

function veCampos(fForm){
	valid = true;
	sErro = "";
    verString(fForm.dscAviso," Assunto ",0);
    verString(fForm.textoAviso," Descri��o ",0);
    if( valid == false ) alert(sErro); 
    return valid;
}

function LimparCampos(fForm){
    fForm.dscAviso.value           ="";
    fForm.textoAviso.value         ="";
	fForm.indPrioridade[1].checked = true;
}

function limparvetor(fForm)
{
  if( fForm.checkbox != null) 
  {
	for(i=0; i<fForm.checkbox.length; i++ )
		fForm.checkbox[i].checked=false;
  }
}

function validaChecked(fForm) { 
  bol = false;
  if( fForm.checkbox != null) 
  {
	for(b=0,i=0; i<fForm.checkbox.length; b++,i++ )	{
		if (fForm.checkbox[i].checked) {
		    fForm.variosH[b].value = fForm.checkbox[i].value;
//            fForm.lista.options[lista.options.length] = new Option(i,i);
      }
	}
	for(b=0,i=0;  i < fForm.checkbox.length; i++)	{
		if (fForm.checkbox[i].checked == false) b++;
	}	
    if ( b == fForm.checkbox.length){ 
	    alert('N�o foi selecionado o destinat�rio!'); 
		bol = true;
    }
  }	
   return bol;
}

function carregaLista(fForm) 
{ 
  if( fForm.checkbox != null) 
  {
	for(b=0,i=0; i<fForm.checkbox.length; b++,i++ )	{
		if (fForm.checkbox[i].checked) {
            fForm.lista.options[fForm.lista.options.length] = new Option(fForm.checkbox[i].text,i);
      }
	}
  }
}

function valida(opcao,fForm) 
{
	 switch (opcao) {    
	   case 'orgaoLotacaoUsario':
	         if( fForm.checkbox == null) { alert ('N�o foi selecionado o destinat�rio!'); return; }
//			 if( validaChecked(fForm) != false ) 	    	 
		     esconde('ORGAOUSUARIO');
		     esconde('ORGAOUSUARIO_01');			 
			 mostra('WK_SISTEMA');
 			 mostra('WK_SISTEMA_01');
		break;
		case 'SISTEMA':
	          vechck( fForm );
			  veChecked( fForm );
			  fForm.target      = "_self";
			  fForm.acao.value  = opcao;
			  fForm.action      = "acessoTool";
			  fForm.tipoH.value = "4";

			  fForm.SistH.value="1";
			  fForm.orgH.value="0";
			  fForm.combo_perfilH.value="0";
			  fForm.combo_oulH.value="0";
			  fForm.divH.value="1";
              fForm.wkSistemaH.value="0";
			  fForm.submit();	  
		break;
		case 'ORGAO':
			  vechck( fForm );
			  veChecked( fForm );
			  fForm.target      = "_self";
			  fForm.acao.value  = opcao;
			  fForm.action      = "acessoTool";
			  fForm.tipoH.value = "3";
			  
			  fForm.SistH.value="0";			  
			  fForm.orgH.value="1";
			  fForm.combo_perfilH.value="0";
			  fForm.combo_oulH.value="0";
			  fForm.divH.value="1";
			  fForm.wkSistemaH.value="0";
			  fForm.submit();	  
		break;
		case 'PERFIL':
			  vechck( fForm );
			  veChecked( fForm );
			  fForm.target      = "_self";
			  fForm.acao.value  = opcao;
			  fForm.action      = "acessoTool";
			  fForm.tipoH.value = "2";

			  fForm.SistH.value="0";
			  fForm.orgH.value="0";
			  fForm.combo_perfilH.value="1";
			  fForm.combo_oulH.value="0";
			  fForm.divH.value="1";
			  fForm.wkSistemaH.value="0";
			  fForm.submit();	  
		  break;
	   case 'OLU':
			 vechck( fForm );
			 veChecked( fForm );
			 fForm.tipoH.value = "1";
  		     fForm.target      = "_self";
			 fForm.acao.value  = opcao;
			 fForm.action      = "acessoTool";

			 fForm.SistH.value="0";
			 fForm.orgH.value="0";
			 fForm.combo_perfilH.value="0";
			 fForm.combo_oulH.value="1";
			 fForm.divH.value="1";
			 fForm.wkSistemaH.value="0";
			 fForm.submit();	  
		 break;
	   case 'R': close();
		 break;
	   case 'orgaoUsuario': 
             vechck( fForm );
			 veChecked( fForm );
			 fForm.target= "_self";
			 fForm.acao.value = opcao;
			 fForm.action = "acessoTool";
			 fForm.tipoH.value = "1";

			 fForm.orgH.value="0";
			 fForm.combo_perfilH.value="0";
			 fForm.combo_oulH.value="1";
			 fForm.divH.value="1";
             fForm.wkSistemaH.value="0";
			 fForm.submit();	  
	   	  break;
		case 'perfilSistemaUsuario': 
             vechck( fForm );
			 veChecked( fForm );
			 fForm.target= "_self";
			 fForm.acao.value = opcao;
			 fForm.action = "acessoTool";

			 fForm.orgH.value="0";
			 fForm.combo_perfilH.value="1";
			 fForm.combo_oulH.value="0";
			 fForm.divH.value="1";
             fForm.wkSistemaH.value="0";
			 fForm.submit();	  
	   	  break;
	   case 'C':
             vechck( fForm );
			 if ( veCampos(fForm) == false ) return;
			 
			 if( veChecked( fForm ) == false ) { 
			   alert('N�o foi selecionado o destinat�rio 1!');
			   return;
			 }
 			 if( validaChecked(fForm) ) return;

 	         if( fForm.checkbox == null) { alert ('N�o foi selecionado o destinat�rio!'); return; }

			 fForm.target= "_self";
			 fForm.acao.value=opcao;
			 fForm.action = "acessoTool";

			 fForm.combo_perfilH.value="0";
			 fForm.combo_oulH.value="0";
			 fForm.divH.value="0";
			 fForm.submit();	  
		  break;
	   case 'O': if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	    	  	 else document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V': if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      	 	 else document.all["MsgErro"].style.visibility='visible' ; 
		  break;
  }
}
function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
</script>
</head>

<body bgcolor="#ffffff" leftmargin="0"  topmargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="form" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- // 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- // 			if (qb==null) qb="???" ; -->
<!-- // 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- // 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<%-- 		%> --%>
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="0%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->


<input name="acao"    type="hidden" value="">				
<input name="indPriH" type="hidden" value="">
<input name="tipoH"   type="hidden" value="">

<input name="orgH" type="hidden" value="">
<input name="SistH" type="hidden" value="">
<input name="combo_perfilH" type="hidden" value="">
<input name="combo_oulH"    type="hidden" value="">
<input name="divH"          type="hidden" value="">
<input name="wkSistemaH"    type="hidden" value="">

<!--IN�CIO_CORPO_sistema--> 
<div id="WK_SISTEMA_01" style="position:absolute; overflow: visible; z-index: 1; left:492px; top: 74px; width:158px; visibility: hidden; height: 231px;"> 
   <table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
	  <tr><td><strong><font color="#666666">Item(ns) selecionado(s)</font></strong></td></tr>	
      <tr>
        <td height="5" colspan="2">  
			<select name="lista" size=14 width="20" disabled> 
		  </select>
		</td>
      </tr>
   </table>
</div>   
<div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1; left:50px; top: 73px; width:438px; visibility: hidden;"> 
   <table width="100%" border="0" align="center" cellpadding="4" cellspacing="0">
      <tr><td height="5" colspan="2"></td></tr>
      <tr bgcolor="<%=Cor%>"> 
         <td height="20" width="19%"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Assunto</font></strong></td>
         <td width="81%" ><strong><font color="#336699">:</font></strong> 
             <input name="dscAviso" type="text" size="60" maxlength="50"  value='<%=AvisoBeanId.getDscAviso()%>' onkeypress="javascript:f_end();" onfocus="javascript:this.select();">
		 </td>
      </tr>
	  <tr><td height="1" colspan="2"></td></tr>
      <tr>
          <td height="1" colspan="2"></td>
      <tr bgcolor="<%=Cor%>"> 
          <td height="20"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Prioridade</font></strong></td>
          <td >
            <strong><font color="#336699">:</font></strong> 
            <%
               String str1="",str2="";
               if( AvisoBeanId.getIndPrioridade().equals("A") )      str1="checked";
               else if( (AvisoBeanId.getIndPrioridade().equals("N")) || (AvisoBeanId.getIndPrioridade() == null) ) str2="checked";   
			%>
		   	<input <%=str1%> name="indPrioridade" type="radio" class="sem-borda"   style="width: 13px; height: 13px; value="">
			<strong><font color="#666666">Alta</font></strong>&nbsp;&nbsp;&nbsp;
			<input <%=str2%> name="indPrioridade" type="radio" class="sem-borda"  style="width: 13px; height: 13px; value="">
			<strong><font color="#666666">Normal</font></strong> 
		  </td>
      </tr>
       <tr>
           <td height="1" colspan="2"></td>
       </tr>
       <tr bgcolor="<%=Cor%>"> 
           <td height="115"><strong><font color="#666666">&nbsp;&nbsp;&nbsp;&nbsp;Descri&ccedil;&atilde;o</font></strong></td>
           <td ><textarea name="textoAviso" rows="7" wrap="soft" value="" style="width:300px; height:100px;" tabindex="4" onChange="this.value=this.value.toUpperCase()"><%=AvisoBeanId.getTxtAviso()%></textarea></td>
       </tr>
	   <tr>
         <td height="1" colspan="2"></td>
       </tr>
       <tr bgcolor="<%=Cor%>"> 
		   <td></td>
           <td height="23" colspan="2">
<%
   String strr = request.getParameter("tipoH");
   if(strr == null) strr="";   
   String str11="",str22="",str33="",str44="";
   if(strr.equals("1")) str11="checked";
   if(strr.equals("2")) str22="checked"; 
   if(strr.equals("3")) str33="checked";   
   if(strr.equals("4")) str44="checked"; 
%>		   
			 &nbsp;
		     <input name="radiobutton" <%= str11 %> type="radio" class="sem-borda" value="radiobutton" onClick="valida('OLU',this.form),esconde('WK_SISTEMA'),esconde('WK_SISTEMA_01');">
			 <strong><font color="#666666">Usu&aacute;rio&nbsp;</font></strong>
			 &nbsp;&nbsp;
			 <input name="radiobutton" <%= str22 %> type="radio" class="sem-borda" value="radiobutton" onClick="valida('PERFIL',this.form),esconde('WK_SISTEMA'),esconde('WK_SISTEMA_01');">
			 <strong><font color="#666666">Perfil&nbsp;</font></strong>
			 &nbsp;&nbsp;
         	 <input name="radiobutton" <%= str33 %> type="radio" class="sem-borda" value="radiobutton" onClick="valida('ORGAO',this.form),esconde('WK_SISTEMA'),esconde('WK_SISTEMA_01');">
			 <strong><font color="#666666">�rg&atilde;o atua��o</font></strong>
			 &nbsp;&nbsp;
			 <input name="radiobutton" <%= str44 %> type="radio" class="sem-borda" value="radiobutton" onClick="valida('SISTEMA',this.form),esconde('WK_SISTEMA'),esconde('WK_SISTEMA_01');">
			 <strong><font color="#666666">Sistema </font></strong>
		 </td>
	   </tr>
       <tr bgcolor="<%=Cor%>"><td colspan="4"></td></tr>
	   <tr bgcolor="#FFFFFF"> 
         <td height="20" colspan="4" align="center">
			 <button style="border: 0px; background-color: transparent; height: 27px; width: 63px; cursor: hand;" type="button" onClick="valida('C',this.form);">
			  <img src="<%=path%>/images/bot_incluir_det1.gif"  width="62px" height="26px" align="right" style="cursor: hand;" >
		   </button>
		 </td>
       </tr>
    </table>
</div>
<%
   String wkSistemaH = request.getParameter("wkSistemaH");
   if (wkSistemaH == null ) wkSistemaH="";
   if( wkSistemaH.equals("") ) {
%>
<script> mostra('WK_SISTEMA');mostra('WK_SISTEMA_01');</script>
<%}%>

<!--inicio da div orgao usuario--> 
<div id="ORGAOUSUARIO" style="position: absolute;  width:387px; height:205px; z-index:1; left:174px; top:72px; overflow: auto;" visibility: hidden;>
  <table width="100%"  border="0" cellpadding="2" cellspacing="2">
<%
   String combo_perfilH = request.getParameter("combo_perfilH");
   if (combo_perfilH == null ) combo_perfilH="";
   if( combo_perfilH.equals("1") ) {
%>
    <tr><td  bgcolor="<%=Cor%>">	<div align="center"><strong><font color="#666666">Selecione o Sistema  :</font></strong>
          <jsp:getProperty name="PerfilSistemaBeanId" property="popupString" /></div>	</td> </tr>
<%
  }
  String combo_oulH = request.getParameter("combo_oulH");
  if( combo_oulH == null ) combo_oulH ="";
  if( combo_oulH.equals("1") ) {
%>	
    <tr><td  bgcolor="<%=Cor%>">
			<div align="center"><strong><font color="#666666">Selecione o �rg�o de lota��o :</font></strong> <jsp:getProperty name="OrgaoBeanId" property="popupString" /></div>
		  </td>
    </tr>
<%}
   String orgH = request.getParameter("orgH");
   if( orgH == null ) orgH="";
   if( orgH.equals("1") ) {
%>
    <tr><td  bgcolor="<%=Cor%>">	<div align="center"><strong><font color="#666666">Selecione o �rg�o atua��o :</font></strong> </div>	</td> </tr>
<%}
   String SistH = request.getParameter("SistH");
   if( SistH == null ) SistH="";
   if( SistH.equals("1") ) {
%>
    <tr><td  bgcolor="<%=Cor%>">	<div align="center"><strong><font color="#666666">Selecione o sistema  :</font></strong> </div>	</td> </tr>
<!--inicio de for -->
<%}
   	int ocor = AvisoBeanId.getVetItens().size();
    for ( int i=0; i < ocor; i++ )  {
%> 
	    <tr>
		  <input name="variosH" type="hidden" value="">
    	  <td height="23" bgcolor="<%=Cor%>">&nbsp;&nbsp;&nbsp;<input class="sem-borda" name="checkbox" type="checkbox" text="<%=AvisoBeanId.getVetItens(i).getNomGenerico()%>" value="<%=AvisoBeanId.getVetItens(i).getCodGenerico()%>"> <%=AvisoBeanId.getVetItens(i).getNomGenerico()%></td>
	    </tr>
<%}%>    

<!--fim do for -->
  </table> 
</div>
<!--Fim da div orgao usuario-->
<div id="ORGAOUSUARIO_01" style="position: absolute;  width:387px; height:48px; z-index:1; left:175px; top:279px; overflow: auto;" visibility: hidden;>
  <table width="100%"  border="0" cellpadding="2" cellspacing="2">
   	<tr bgcolor="#FFFFFF"> 
		  <td height="29" colspan="4" align="center">
			  <button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" type="button" onClick="valida('orgaoLotacaoUsario',this.form),carregaLista(this.form);">
			  <img src="<%=path%>/images/bot_ok.gif"  width="26" height="19" align="right" style="cursor: hand;" >
			  </button>	  
			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  			  <button style="border: 0px; background-color: transparent; height: 20px; width: 54px; cursor: hand;" type="button" onClick="esconde('ORGAOUSUARIO'),esconde('ORGAOUSUARIO_01'),mostra('WK_SISTEMA'),mostra('WK_SISTEMA_01'),limparvetor(this.form);">
			  <img src="<%=path%>/images/retornar.gif"  height="19" width="52"  align="right" style="cursor: hand;" >
			  </button>	  
		  </td>
      </tr>
  </table>
</div>

<%
	String mostra = request.getParameter("divH");
	if( (mostra == null)||(mostra.equals("0"))||(mostra.equals("")) ) { mostra="";
%>
	<script>esconde('ORGAOUSUARIO'),esconde('ORGAOUSUARIO_01')</script>
<%}else{%>
	<script>mostra('ORGAOUSUARIO'),mostra('ORGAOUSUARIO_01')</script>
<%}%>

<script>
<%
  combo_oulH = request.getParameter("combo_oulH");
  if(combo_oulH == null ) combo_oulH="";
  if( combo_oulH.equals("1") ) {
%>	
		nj = '<%= AvisoBeanId.getOrgao()%>'
		njint  = parseInt(nj,10)
		if ((nj.length>0) && (nj!=0)){
		  for(i=0;i<document.form.ORGAO.length;i++){
			if(document.form.ORGAO.options[i].value==njint) document.form.ORGAO.selectedIndex=i;	  
		  }
		}
<%}
  combo_perfilH = request.getParameter("combo_perfilH");
  if(combo_perfilH == null ) combo_perfilH="";
  if( combo_perfilH.equals("1") ) {
%>
		nj = '<%= AvisoBeanId.getSistemPerfil()%>'
		njint  = parseInt(nj,10)
		if ((nj.length>0) && (nj!=0)){
		  for(i=0;i<document.form.PERFIL.length;i++){
			if(document.form.PERFIL.options[i].value==njint) document.form.PERFIL.selectedIndex=i;	  
		  }
		}
<%}%>
</script>
<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->

<% if( RequisicaoBeanId.getCmdFuncao().compareTo("ACSS.CadastraAvisoCmd") == 0 ) {%>
<script>esconde('POPUP');</script>
<%}%>
<!--Div Erros-->

<%
  String msgErro   = AvisoBeanId.getMsgErro();
  if(msgErro==null) msgErro ="";
  String mostraMsg = "hidden" ;   
  if( msgErro.trim().length() !=0 ) mostraMsg = "visible" ;
%>
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro"     value= "<%= msgErro %>" />
  <jsp:param name="msgErroTop"  value= "250 px" />
  <jsp:param name="msgErroLeft" value= "60 px" />
</jsp:include> 
<!--FIM_Div Erros-->
</form>
</body>
</html>