<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>

<%String sFuncaoPai="";%>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="SistId" scope="request" class="ACSS.SistemaBean" /> 
<jsp:setProperty name="SistId" property="j_abrevSist" value="ACSS" />  
<jsp:setProperty name="SistId" property="popupExt"    value="onChange='javascript: setaPerfil(this,document.perfilForm.codPerfil)'" />  	 	 
<jsp:setProperty name="SistId" property="colunaValue" value="cod_Sistema" />
<jsp:setProperty name="SistId" property="popupNome"   value="codSistema"  />
<jsp:setProperty name="SistId" property="popupString" value="nom_Sistema,SELECT cod_Sistema,nom_abrev||' - '||nom_sistema as nom_sistema FROM TCAU_SISTEMA ORDER BY nom_abrev" />

<!-- Chama o Objeto da Tabela de Perfil -->
<jsp:useBean id="PerfBeanId" scope="request" class="ACSS.PerfilBean" /> 

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

mySist      = new Array(<%= SistId.getSistemas(  UsuarioBeanId.getCodUsuario()  ) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}

myPerfs      = new Array(<%= PerfBeanId.getPerfis() %>);
cod_sistPerf = new Array(myPerfs.length/3);
cod_perfil   = new Array(myPerfs.length/3);
nom_perfil   = new Array(myPerfs.length/3);
for (i=0; i<myPerfs.length/3; i++) {
    cod_sistPerf[i] = myPerfs[i*3+0]
	cod_perfil[i]   = myPerfs[i*3+1] ;
	nom_perfil[i]   = myPerfs[i*3+2] ;
}



function setaPerfil(objComboEscolha,objComboMontar) {
	var codSist =  objComboEscolha.value
	// esvazia combo de grupos
	objComboMontar.length = 1
	objComboMontar.options[0] = new Option(" ")
	k=1	
	for(i=0;i<myPerfs.length;i++){
	    if(cod_sistPerf[i]==codSist){		
		   objComboMontar.options[k] = new Option(nom_perfil[i],cod_perfil[i])
	       k++	
	    }
	}
}

function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaOperacoes':
      if (veCampos(fForm)==true) {   
		fForm.acao.value=opcao
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
   case 'A':
      if (veCampos(fForm)==true) {
		SelecionaSelect(fForm.perfOperacoes,fForm.perfOperacoes,true) 	
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'V':
	  if (fForm.atualizarDependente.value=="S") {
			fForm.atualizarDependente.value="N"	  
			fForm.acao.value=opcao
		   	fForm.target= "_self";
		    fForm.action = "acessoTool";  
		   	fForm.submit();	  		  
	  }
	  else {
	      close() ;   
	  }
	  break;
   case 'I':
	 temp = fForm.j_cmdFuncao.value
	 fForm.j_cmdFuncao.value = "construcao" ;
	 fForm.j_sigFuncao.value = "<%=UsuarioFuncBeanId.getJ_sigFuncao()%>" ;
	 fForm.target= "_blank";
	 fForm.action = "acessoTool";  		  
	 fForm.submit();	  
	 fForm.j_cmdFuncao.value = temp ;		    
	 break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].display='none' ; 
	      else                 document.all["MsgErro"].style.display='none' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].display='block' ; 
	      else                 document.all["MsgErro"].style.display='block' ; 
	  break;	  
  }
}

function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
 if ((fForm.perfOperacoes==null) || (fForm.perfOperacoes==null) || (fForm.perfOperacoes==null)) {
	 alert("Selecione o Perfil desejado e confirme no 'OK'")
	 return ;
 }
 switch (opcao) {
 
   case 'IT':
		ApagaSelect(fForm.perfOperacoes,true)    	
		CopiaSelect(fForm.tempOperacoes,fForm.perfOperacoes,true)
		ApagaSelect(fForm.sistOperacoes,true)    	
		break; 
			
   case 'ET':   
		ApagaSelect(fForm.sistOperacoes,true)    	
		CopiaSelect(fForm.tempOperacoes,fForm.sistOperacoes,true)
		ApagaSelect(fForm.perfOperacoes,true)    	
		break; 	
		
   case 'Inclui':
			DeselecionaSelect(fForm.tempOperacoes,fForm.tempOperacoes,false) 
			SelecionaSelect(fForm.perfOperacoes,fForm.tempOperacoes,true) 	
			SelecionaSelect(fForm.sistOperacoes,fForm.tempOperacoes,false)
			ApagaSelect(fForm.perfOperacoes,true)    	
	    	CopiaSelect(fForm.tempOperacoes,fForm.perfOperacoes,false)	
			ApagaSelect(fForm.sistOperacoes,false)   

   case 'Exclui':  
		DeselecionaSelect(fForm.tempOperacoes,fForm.tempOperacoes,false) 
		SelecionaSelect(fForm.sistOperacoes,fForm.tempOperacoes,true) 	
		SelecionaSelect(fForm.perfOperacoes,fForm.tempOperacoes,false) 
		ApagaSelect(fForm.sistOperacoes,true)    							
		CopiaSelect(fForm.tempOperacoes,fForm.sistOperacoes,false)	
		ApagaSelect(fForm.perfOperacoes,false)
							
 }	
}

function veCampos(fForm) {
	valid   = true ;
	sErro = "" ;
	if (fForm.codSistema.value==0) {
	   valid = false
	   sErro = "Sistema n�o selecionado. \n"
	}	
	if (fForm.codPerfil.value==0) {
	   valid = false
	   sErro += "Perfil n�o selecionado. \n"
	}	
    if (valid==false) alert(sErro)
return valid ;
}

function selecionaPaiSist(fForm){
//Prepara lista de pais 1 Nivel
  listPai = new Array (<%= SistId.getFuncoes().size()%>);
  listFilho = new Array (<%= SistId.getFuncoes().size()%>);
  listStatus = new Array (<%= SistId.getFuncoes().size()%>);
<%String pai = ""; 
  for (int i =0; i< SistId.getFuncoes().size(); i++){ 
%>
         listPai[<%=i%>]= "<%=SistId.getFuncoes(i).getSigFuncaoPai()%>";
         listFilho[<%=i%>]= "<%=SistId.getFuncoes(i).getSigFuncao()%>"
         <% if (SistId.getFuncoes(i).getNomExecuta().equals("")){%>
            listStatus[<%=i%>]= "N" //n�
         <%}else {%>
            listStatus[<%=i%>]= "F" //folha
         <% }%>
         
            
<%       
  }%>    
  
  
  var position;
  for(i=0;i<listFilho.length;i++){
  	if(listFilho[i]==fForm.sistOperacoes.value)
  		position = i;
  }
 
  while (listPai[position] != "") {
    valPai = listPai[position];
    for(k=0;k<fForm.sistOperacoes.length;k++){                      
      if ( valPai == fForm.sistOperacoes.options[k].value ){
         for(y=0;y<listFilho.length;y++){
  		 	if(listFilho[y]==fForm.sistOperacoes.options[k].value)
  				position = y;
  		 }
		 fForm.sistOperacoes.options[k].selected=true;
		 break;
       }
   	}
   if(k == fForm.sistOperacoes.length) break;
 }
}





 



function selecionaPaiPerf(fForm){

//Prepara lista de pais 1 Nivel
  listPai = new Array (<%= SistId.getFuncoes().size()%>);
  listFilho = new Array (<%= SistId.getFuncoes().size()%>);
  listaNome = new Array (<%= SistId.getFuncoes().size()%>);
<% 
  for (int i =0; i< SistId.getFuncoes().size(); i++){ 
%>
         listPai[<%=i%>]= "<%=SistId.getFuncoes(i).getSigFuncaoPai()%>";
         listFilho[<%=i%>]= "<%=SistId.getFuncoes(i).getSigFuncao()%>"
         listaNome[<%=i%>]= "<%=SistId.getFuncoes(i).getSigFuncao()+" - "+ SistId.getFuncoes(i).getNomOperacao()%>"
<%       
  }%>    
  
  
  var position;
  var x;
  for(x=0;x<listFilho.length;x++){
  	if(listFilho[x]==fForm.perfOperacoes.value){
  		position = x;
  		break;	
  	}
  }

 
  
  //Enquanto existe pai executa
  while (listPai[position] != ""){
    valPai = listPai[position];
    //varre a combo em busca do pai
    for(k=0;k<fForm.perfOperacoes.length;k++){  
      if ( valPai == fForm.perfOperacoes.options[k].value ){
         //encontrei o pai e procuro os filhos
         var w;        
         var z;
         for(z=0;z<fForm.perfOperacoes.length;z++){
     	 	for(w=0;w<listFilho.length;w++){
  				if(listFilho[w]== fForm.perfOperacoes.options[z].value){
  				   //encontrei filho
  					position = w;
  					break;
  				}
  				
  			}
         	if( (listPai[position] == valPai) && (x!=w)){ 
         	    //paro a procura
         		break;
         	}
         }                
         if(z<fForm.perfOperacoes.length){
            // Varreu toda a combo
         	k = fForm.perfOperacoes.length;
         	break;
         } 
         var y; 
         for(y=0;y<listFilho.length;y++){
           	if(listFilho[y]==fForm.perfOperacoes.options[k].value)
  				position = y;
  		 }
  		 //Seleciono os pais
		 fForm.perfOperacoes.options[k].selected=true;
		 x = position;
		 break;
       }
   	}
   if(k == fForm.perfOperacoes.length) break;
 }
}
 
</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="perfilForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao" type="hidden" value=' '>	
<input name="dscNot" type="hidden" size="10" maxlength="20"  value= ""> 		
	

<div id="WK_SISTEMA" style="position:absolute; width:703px; overflow: visible; z-index: 1; top: 75px; left: 52px; " > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">    
      <tr> 
        <td valign="top" colspan="2"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>
      <tr>
   		<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td align="left" width="74%"><b>Sistema:&nbsp;</b>
				<select name="codSistema" onChange="javascript: setaPerfil(this,document.perfilForm.codPerfil)"> 
				<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<script>
					for (j=0;j<cod_sistema.length;j++) {
						document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
					}
				</script>
				</select>
			  </td>
	          <td align="right" width="10%" ><button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('buscaOperacoes',this.form);">	
		        <IMG src="<%= path %>/images/bot_ok.gif" align="left" ></button>
			  </td>
          <% }
			else { %>
			  <td align="left" width="74%"><b>Sistema:&nbsp;</b>
	            <input name="codSistema" type="hidden" size="40" maxlength="50"  value="<%= SistId.getCodSistema() %>"> 
		        <input disabled name="nomSistema" type="text" size="40" maxlength="50"  value="<%= SistId.getNomSistema() %>" style="border: 0px none; background-color:transparent;">
			  </td>
			  <td align="right" width="13%"> 
 	                 <button type="button" NAME="Atualizar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('A',this.form);">	
	                  <IMG src="<%= path %>/images/bot_atualizar.gif" width="52" height="19" align="left"  ></button>
	          </td>	
	          <td width="13%" align="right"> 
		          <button type=button NAME="Retornar"  style="height: 18px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" > 
        		  <IMG src="<%= path %>/images/bot_retornar.gif" width="52" height="19" ></button>
	          </td>
		<% } %>			  
      </tr>	
      <tr> 
        <td valign="top" colspan="2"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
      <tr>
   		<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  
			  <td><b>&nbsp;&nbsp;&nbsp;&nbsp;Perfil:&nbsp;</b>
				<select name="codPerfil"> 
				<option value="0" selected>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				</select>
			  </td>		  
	          <td>
			  </td>
          <% }
			else { %>
			  <td><b>Perfil:&nbsp;</b>
	            <input name="codPerfil" type="hidden" size="40" maxlength="50"  value="<%= PerfBeanId.getCodPerfil() %>"> 
		        <input disabled name="nomPerfil" type="text" size="35" maxlength="50"  value="<%= PerfBeanId.getNomDescricao() %>" style="border: 0px none; background-color:transparent;">
			  </td>			  
			  <td> 
	          </td>	
	          <td> 
	          </td>
		<% } %>			  
      </tr>	
      <tr> 
        <td valign="top" colspan="2"><img src="<%= path %>/images/inv.gif" width="3" height="2"></td>
      </tr>	 
  </table>    
</div>


<div style="position:absolute; width:680px; overflow: auto; z-index: 1; top: 129px; left: 75px; height: 32px;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%"  align="center">  	   		
    <tr><td colspan=6 height=3></td></tr>		  		
	<tr> 		 	  
	   	 <td width=41% align="center"  bgcolor="#79B3C4"><font color="#ffffff"><b>SISTEMA</b></font></TD>		 
	   	 <td width=14%></TD>		 
	   	 <td width=45% align="center"  bgcolor="#79B3C4"><font color="#ffffff"><b>PERFIL</b></font></TD>		 
	</tr>
   </table>
</div>
<!--FIM CABEC DA TABELA-->
<div style="position:absolute; width:703px; overflow: hidden; z-index: 1; top: 158px; left: 52px; height: 212px;">
  <table border="0" cellpadding="0" cellspacing="0" width=100% height="212" align="center">  	   		
    <tr><td colspan="6" height="3"></td></tr>		  		
	<tr height="48">
		 <td width="41%" align="center"  bgcolor="#DFEEF2" rowspan="4">
		  <table bgcolor="#DFEEF2" border="0" cellpadding="0" cellspacing="0" width="100%"  align="left"> 
		  	<tr bgcolor="#DFEEF2"><td align="center">
<%
       if (SistId.getFuncoes().size()>0) {
				out.println("<select name=\"sistOperacoes\" multiple size=15 onChange = \"javaScript:selecionaPaiSist(this.form);\">");
				boolean achou;
 				for (int i=0; i<SistId.getFuncoes().size(); i++) {				   
					achou =false ;
    			    for (int k =0; k<PerfBeanId.getFuncoes().size(); k++) {
						if (PerfBeanId.getFuncoes(k).getCodOperacao().equals(SistId.getFuncoes(i).getCodOperacao())) {
							   achou = true ;
							   break;
						}
					 }
					 if (achou==false) 
					 {
					 out.println("<option value=\""+SistId.getFuncoes(i).getSigFuncao()+"\">"+SistId.getFuncoes(i).getSigFuncao()+" - "+ SistId.getFuncoes(i).getNomOperacao()+"</option>");
					 }
					 
				}
				out.println("</select>");
		 }
		 
		 
%>
			</td></tr>
		  </table>	
		</td>
	   	<td width=14%>
		   <div align="center">
				<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="FimImg"  onClick="javascript: atualiza('IT',this.form);">
				<img alt="Inclui Todas" src="<%= path %>/images/bot_final.gif" width="20" height="19">
				</button>
		   </div></td>				
 	   	 <td width=45% align=center  bgcolor="#DFEEF2" rowspan=4>
		  <table bgcolor="#DFEEF2" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
		  	<tr bgcolor="#DFEEF2"><td align=center>
<%
			if (SistId.getFuncoes().size()>0) {
				   out.println("<select name=\"perfOperacoes\" multiple size=15 onChange = \"javaScript:selecionaPaiPerf(this.form);\">");
 				   for (int i=0; i<PerfBeanId.getFuncoes().size(); i++) {	
				      out.println("<option value=\""+PerfBeanId.getFuncoes(i).getSigFuncao()+"\">"+PerfBeanId.getFuncoes(i).getSigFuncao()+" - "+PerfBeanId.getFuncoes(i).getNomOperacao()+"</option>");
				   }
				   out.println("</select>");
		    }
%>
			</td></tr>
		  </table>	
		</td>
	</tr>	
	<tr height="48"><td align="center">
			<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="ProximoImg"  onClick="javascript: atualiza('Inclui',this.form);">
			<img alt="Inclui" src="<%= path %>/images/bot_prox.gif" width="20" height="19">
			</button>
		</td>				
    </tr>
	<tr height="48"><td align="center">
			<button type=button style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="AnteriorImg"  onClick="javascript: atualiza('Exclui',this.form);">
			<img alt="Exclui" src="<%= path %>/images/bot_ant.gif" width="20" height="19">
			</button>
		</td>				
	</tr>
	<tr height="48"><td align="center">
			<button type="button" style="height: 21px; width: 22px;border: none; background: transparent;"  NAME="InicioImg"  onClick="javascript: atualiza('ET',this.form);"> 
          <img alt="Exclui Todas" src="<%= path %>/images/bot_inicial.gif" width="20" height="19"> 
          </button>
		</td>
	</tr>		
  </table>
<input name="atualizarDependente" type="hidden" size="1" value="<%= SistId.getAtualizarDependente() %>"> 
</div>

<!--FIM_CORPO_sistema-->
  
<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= PerfBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "350 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<div style="position:absolute; width:100%; overflow: auto; z-index: 1; top: 180px; height=195px; display:none; visibility='hidden';">
  <table bgcolor="#DFEEF2" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
  	<tr bgcolor="#DFEEF2"><td align=center>
<%      if (SistId.getFuncoes().size()>0) { 
			out.println("<select name=\"tempOperacoes\" multiple size=15 onChange=\"javasript:carregaDescricao(this.form)\">");
 		   for (int i=0; i<SistId.getFuncoes().size(); i++) {
 		   	   out.println("<option value=\""+SistId.getFuncoes(i).getSigFuncao()+"\">"+SistId.getFuncoes(i).getSigFuncao()+" - "+ SistId.getFuncoes(i).getNomOperacao()+"</option>");
			}
			out.println("</select>");
		}
%>
	</td></tr>
  </table>	
</div>

<div style="position:absolute; width:100%; overflow: auto; z-index: 1; top: 180px; height=195px; display:none; visibility='hidden';">
  <table bgcolor="#DFEEF2" border='0' cellpadding="0" cellspacing="0" width=100%  align=left> 
  	<tr bgcolor="#DFEEF2"><td align=center>
<%      if (SistId.getFuncoes().size()>0) { 
			out.println("<select name=\"tempPaiOperacoes\" multiple size=15 onChange=\"javasript:carregaDescricao(this.form)\">");
 		   for (int i=0; i<SistId.getFuncoes().size(); i++) {
   		       if (SistId.getFuncoes(i).getCodOperacao().equals(sFuncaoPai)){
   		           out.println("<option value=\""+SistId.getFuncoes(i).getCodOperacao()+"\">"+SistId.getFuncoes(i).getSigFuncao()+" - "+ SistId.getFuncoes(i).getNomOperacao()+"</option>");
 			  }
		   }
			out.println("</select>");
		}
%>
	</td></tr>
  </table>	
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />

<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= SistId.getCodSistema() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.perfilForm.codSistema.length;i++){
		if(document.perfilForm.codSistema.options[i].value==njint){
			document.perfilForm.codSistema.selectedIndex=i;	  
		}
	}
}
<% if (SistId.getAtualizarDependente().equals("S")==false){ %>				  

nj = '<%= PerfBeanId.getCodPerfil() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)) {
  for(i=0;i<document.perfilForm.codPerfil.length;i++){
    if(document.perfilForm.codPerfil.options[i].value==njint){
       document.perfilForm.codPerfil.selectedIndex=i;	  
    }
  }
}  
<% } %>	



		  
</form>
</BODY>
</HTML>

