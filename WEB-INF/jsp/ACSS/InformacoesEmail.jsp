<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<% String path=request.getContextPath(); %>
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="SistemaBeanId"     scope="session" class="ACSS.SistemaBean" /> 
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<!-- Chama o Objeto da Tabela de Fiscais -->
<jsp:useBean id="CadCatId" scope="request" class="ACSS.CadastraCategoriaBean" />
<jsp:setProperty name="CadCatId" property="j_abrevSist" value="ACSS" />  	 	     	 	  
<jsp:setProperty name="CadCatId" property="colunaValue" value="EMAIL_ENVIO" />  
<jsp:setProperty name="CadCatId" property="popupNome"   value="destinatario"  />  
<jsp:setProperty name="CadCatId" property="popupString" value="DSC_CATEGORIA_DUVIDA,SELECT EMAIL_ENVIO,DSC_CATEGORIA_DUVIDA FROM TCAU_CATEGORIA_DUVIDA ORDER BY DSC_CATEGORIA_DUVIDA" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#be7272";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>DETRAN &#8226; Sistema de Monitoramento de Infra&ccedil;&otilde;es de 
Tr&acirc;nsito</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<jsp:include page="Css.jsp" flush="true" />
<script language="JavaScript" type="text/JavaScript">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

function verifica_email(myobj) {
	if (valida_email(myobj)==false) {
		alert("Email inv�lido.")
	}
}
function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}

function textCounter(field, countfield, maxlimit) {
		 if (field.value.length > maxlimit) 
		 	field.value = field.value.substring(0, maxlimit);
   		 else 
		 	  countfield.value = maxlimit - field.value.length;
}

function validacamp(fForm)
{
    ret = true;

    if(fForm.destinatario.selectedIndex == 0) {
       alert("Deve ser selecionado um destino!");
       ret = false;
    }
    else
    if(fForm.assunto.value == "" ) {
       alert("Deve ser digitado um assunto!");
       ret = false;
    }
    else
    if(fForm.comment.value == "" ) {
       alert("Deve ser digitado uma mensagem!");
       ret = false;
    }
    return ret;
}

function valida(opcao,fForm) {
 switch (opcao) {
    case 'enviaEmail':
    
        if( validacamp(fForm) == false ) return;
    
	    fForm.acao.value=opcao;
		fForm.target= "_self";
	    fForm.action = "acessoTool";
	   	fForm.submit();
	  break ;
   case 'R':
      close();   
	  break;	  
	}
}
function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
</script>
</head>

<body leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0"  style="overflow: hidden;">
<form name="form" method="post" action="">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

  <input name="acao" type="hidden" value="">
	
<!--T�tulo da p�gina-->
<div id="titPagina" style="position:absolute; left: 204px; top: 55px; width:468px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
  <script>esconde('titModulo');</script>  
    <tr> 
      <td width="12%"><div align="left"><font color="#000000"><strong>E-MAIL</strong></font></div></td>
    </tr>
  </table>
</div>
<!--Fim T�tulo da p�gina-->


<!--�rea de e-mail--> 
  <div id="eMail" style="position:absolute; left:50px; top:81px; width:720px; height:88px; z-index:13; overflow: visible; visibility: hidden;"> 
    <table width="100%" border="0" bgcolor="<%=Cor%>"cellpadding="4" cellspacing="0" class="table">
      <tr> 
        <td width="12%"><strong>&nbsp;&nbsp;De</strong></td>
        <td width="3%" ><strong> :</strong></td>
        <td width="72%">
		<input type="text"   size="80"  readonly  name="des" tabindex="1" style="width: 500px; border: 0px none; background-color: transparent;" onChange="this.value=this.value.toUpperCase()" value="<%=UsuarioBeanId.getEmail()%>">
		<input type="hidden" size="80"  name="nome"      value="<%=UsuarioBeanId.getNomUserName()%>">
		<input type="hidden" size="80"  name="mandante"  value="<%=UsuarioBeanId.getEmail()%>">
		</td>
        
        <td width="13%" rowspan="9" style="background-image: url(../images/ACSS/info_email.jpg); background-position: 3px -5px">&nbsp;</td>
      </tr>
      <tr> 
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
      </tr>
      <script>document.form.focus();</script>
      <tr> 
        <td><strong>&nbsp;&nbsp;Destino</strong></td>
        <td><strong> :</strong></td>
        <td><jsp:getProperty name="CadCatId" property="popupString" /></td>
      </tr>
      <tr> 
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
      </tr>
      <tr> 
        <td><strong>&nbsp;&nbsp;Assunto</strong></td>
        <td><strong> :</strong></td>
        <td>
		<input type="text" size="70" name="assunto" tabindex="3" onChange="this.value=this.value.toUpperCase()"></td>
      </tr>
      <tr> 
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
      </tr>
      <tr> 
        <td valign="top"><strong>&nbsp;&nbsp;Mensagem </strong></td>
        <td valign="top"><strong>:</strong></td>
        <td> 
          <textarea name="comment" rows="7" wrap="soft" style="width:500px; height:100px;" onKeyDown="textCounter(this.form.comment,this.form.remLen,1000);" onKeyUp="textCounter(this.form.comment,this.form.remLen,1000);" tabindex="4" onChange="this.value=this.value.toUpperCase()"></textarea> 
        </td>
      </tr>
      <tr> 
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
      </tr>
      <tr> 
        <td height="45">&nbsp;</td>
        <td>&nbsp;</td>
        <td valign="top">
	<table width="98%" bgcolor="<%=Cor%>" border="0" cellspacing="0" cellpadding="0">
            <tr> 
              <td><input readonly type="text" name="remLen" size="4" maxlength="4" value="1000" class="sem-borda" style="background-color: <%=Cor%>; font-weight: bold;" > 
                <span class="txt2"> caracteres restantes</span> <script>textCounter(document.form.comment,document.form.remLen,1000);</script> 
              </td>
              <td align="right"> <button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" type="button" onClick="javascript: valida('enviaEmail',document.form);"><img src="<%=path%>/images/bot_enviar_vm.gif"  width="41" height="19" align="right" style="cursor: hand;" ></button></td>
            </tr>
          </table></td>
      </tr>
    </table>
  </div>
<!--Fim �rea de e-mail-->


<!--inicio da mensagem de confirmacao de e-mail--> 
  <div id="MensagemDeEnvio" style="position:absolute; left:170px; top:150px; width:468px; height:88px; z-index:13; overflow: visible; visibility: hidden;"> 
    <table width="100%" bgcolor="<%=Cor%>" border="0" cellpadding="4" cellspacing="0" class="table">
      <tr> 
        <td width="12%"></td>
        <td width="3%" ></td>
        <td width="84%"></td>
        <td width="1%" rowspan="9"></td>
      </tr>
      <script>document.form.focus();</script>
      <tr> 
        <td colspan="3"><strong> </strong></td>
      </tr>
      <tr> 
        <td colspan="3"><div align="center">
          <p>&nbsp;</p>
          <%String strMsg="Mensagem enviada com sucesso !";%>

		<% if( UsuarioBeanId.getEmail().equals("") ) {
		       strMsg ="Usu�rio n�o possui e-mail cadastrado,favor entrar em contato com o controle de acesso !";
		%>
			<script>esconde('eMail'); mostra('MensagemDeEnvio');</script>
		<%}else{%>
			<script>esconde('MensagemDeEnvio'); mostra('eMail');</script>
		<%}%>
          <p><strong> <%=strMsg%> </strong></p>
          <p>&nbsp; </p>
        </div></td>
      </tr>
      <tr> 
        <td colspan="3" valign="top"></td>
      </tr>
      <tr> 
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
        <td height="1" bgcolor="#FFFFFF"></td>
      </tr>
      <tr> 
        <td height="45" colspan="3" >	      <div align="center">
          <p>
          <% if( UsuarioBeanId.getEmail().equals("") ) {%>
                 <button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" type="button" onClick="javascript: valida('R',this.form);">
                 <img src="<%=path%>/images/REC/bot_ok.gif"  width="26" height="19" align="right" style="cursor: hand;" ></button>
          <%} else {%> 
              <button style="border: 0px; background-color: transparent; height: 21px; width: 43px; cursor: hand;" type="button" onClick="javascript: esconde('MensagemDeEnvio'),mostra('eMail');">
              <img src="<%=path%>/images/REC/bot_ok.gif"  width="26" height="19" align="right" style="cursor: hand;" ></button>
           <% } %>   
          </p>
          </div></td>
      </tr>
    </table>
</div>
<!--Fim da mensagem de confirmacao de e-mail-->

<%
  String status =(String)request.getAttribute("status");
  if( (status == null) || status.equals("0")  ){ 
  %>
       <script>//mostra('eMail');esconde('MensagemDeEnvio');</script>	
<%}else if( (status.equals("1")) || status.equals("2") ){%>
       <script>esconde('eMail'); mostra('MensagemDeEnvio');</script>
<%}%>

<!-- Rodap�-->
<!--Bot�o retornar--> 
<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->
</form>
</body>
</html>