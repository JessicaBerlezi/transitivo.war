<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="../sys/ErrorPage.jsp" %>

<jsp:useBean id="UsuarioBeanId" scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Orgao logado -->
<jsp:useBean id="ProtocoloUpoBeanId" scope="request" class="ACSS.ProtocoloUpoBean" /> 



<html>  
<head>
<jsp:include page="Css.jsp" flush="true" />
<title>:: Manutencao Tabela de Protocolo UPO :: v.0</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%= path %>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);
self.resizeTo(screen.availWidth,screen.availHeight); 

<!-- Fim de preenchimento da combo -->
function valida(opcao,fForm) {
	// se movimentacao no arquivo - nao precisa validar os campos
	if ("Top,Proximo,Anterior,Fim,Novo,Pkid".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
		fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	    return ;
	 }
	 // se inclusao ou alteracao - valida todos os campos
	 if ("12".indexOf(opcao)>=0) { 		  
	      if (veCampos(opcao,fForm)==false) return ;
	  }	  
	 // inclusao, alteracao ou exclusao 
	 if ("123".indexOf(opcao)>=0) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  } 
	 switch (opcao) {  
	   case 'Z':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		 
       break;   
       case 'buscaMunicipio':
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		 
       break;   
	   case 'R':
	    close() ;
	   break;
	   case 'C':
		  if (document.layers) fForm.layers["ProtUpoCons"].visibility='show' ; 
	      else                 document.all["ProtUpoCons"].style.visibility='visible' ; 
		  break;
	   case 'I':   // Imprimir
			temp = fForm.j_cmdFuncao.value	   
			fForm.j_cmdFuncao.value = "construcao" ;
			fForm.target= "_blank";
			fForm.action = "acessoTool";  		  
			fForm.submit();	  
		    fForm.j_cmdFuncao.value = temp ;				
			break;
	   case 'L':  // Limpar
		  document.UsuForm.reset() ;
		  break;  
	   case 'O':  // Esconder os erro
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
		  break;  
	   case 'V':  // Ver os erros
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
		  break;	 
  }
}
function veCampos(opcao,fForm) {
	fForm.nomProtUpo.value=trim(fForm.nomProtUpo.value)
	fForm.codProtUpo.value=trim(fForm.codProtUpo.value)
	valid = true ;
	sErro = "" ;	
	verString(fForm.nomProtUpo,"Nome do Protocolo UPO",0);
	if (valid==false) alert(sErro) 
	return valid ;
}
function verifica_email(myobj) {
	if (valida_email(myobj)==false) {
		alert("Email inv�lido.")
	}
}
function valida_email(myobj) {
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(myobj.value)){
		return (true)
	}
	myobj.focus();		
	return (false);
}
</script>
<link href="Css.jsp" rel="stylesheet" type="text/css">
</head>
<body bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" OnLoad="document.UsrForm.nomProtUpo.focus();" style="overflow: hidden;">
<form name="UsrForm" method="post" action="">
<jsp:include page="Cab.jsp" flush="true" />
<input name="acao"        type="hidden" value=' '>				
<input name="pkid"        type="hidden" value="<%=ProtocoloUpoBeanId.getPkid()%>">
<!--<input name="codOrgao"    type="hidden" value="<%//=ProtocoloUpoBeanId.getCodProtocoloUpo()%>"> -->
		
<!--IN�CIO_CORPO_sistema--> 
  <div id="WK_SISTEMA" style="position:absolute;  overflow: visible; z-index: 1;  left:50px; top:64px; width:720px; height:110px;" > 
    <!--Botoes navega��o-->
    <jsp:include page="../sys/BotoesNav.jsp" flush="true" />
   <!--FIM_Botoes navega��o-->
    <table width="720" height="25" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr><td height="5" colspan="6" ></td></tr>
      <tr height="25"> 
        <td width="98"  bgcolor="#DFEEF2" ><strong><font color="#666666">&nbsp;&nbsp;&nbsp;Nome protocolo</font></strong></td>
        <td width="200"   bgcolor="#DFEEF2"><strong><font color="#336699">:</font></strong> 
          <input name="nomProtUpo" type="text" size="26" maxlength="60"  value='<%=ProtocoloUpoBeanId.getNomProtocoloUpo() %>' onKeyPress="javascript:f_end();" onFocus="javascript:this.select();" onChange="this.value=this.value.toUpperCase()">
          <input name="codProtUpo" type="hidden" value='<%=ProtocoloUpoBeanId.getCodProtocoloUpo() %>'>
        </td>
      </tr>
    </table>
      
<!--Botoes padr�o-->
    <jsp:include page="../sys/Botoes.jsp" flush="true" />
	<!--FIM_Botoes padr�o-->
</div>
<!-- Rodap�-->
<jsp:include page="Retornar.jsp" flush="true" />
<jsp:include page="Rod.jsp" flush="true" />
<!-- Fim Rodap� -->

<!--Div Erros-->
<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= ProtocoloUpoBeanId.getMsgErro() %>" />
  <jsp:param name="msgErroTop" value= "260 px" />
  <jsp:param name="msgErroLeft" value= "80 px" />
</jsp:include> 
<!--FIM_Div Erros-->
<!--FIM_CORPO_sistema--> 
<jsp:include page="protocoloUpoConsulta.jsp" flush="true" > 
  <jsp:param name="ProtocoloUpoConsTop"  value= "80 px" />
  <jsp:param name="ProtocoloUpoConsLeft" value= "50 px" />  
</jsp:include>

</form>
</body>
</html>

