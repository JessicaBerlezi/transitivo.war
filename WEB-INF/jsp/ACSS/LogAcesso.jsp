<!--M�dulo : ACSS
	Vers�o : 1.0
	Atualiza��es:
-->	
<!-- Abre a Sessao -->
<%@ page session="true" %>
<%@ page import="java.util.*" %>

<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"   scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Orgaos -->
<jsp:useBean id="OrgaoBeanId"     scope="request" class="ACSS.OrgaoBean" /> 
<!-- Chama o Objeto da Tabela de Usuarios -->
<jsp:useBean id="UsrBeanId"       scope="request" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id = "SistemaBeanId" scope="session" class="ACSS.SistemaBean"/>
<jsp:useBean id="SistBeanId"      scope="request" class="ACSS.SistemaBean" /> 
<!-- Chama o Objeto da Tabela de Sistemas -->
<jsp:useBean id="LogAcessoBeanId" scope="session" class="ACSS.LogAcessoBean" /> 
<!-- Chama o Objeto do Sistema logado -->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" />
<!-- Chama o Objeto dos Parametros da Requisicao -->
<jsp:useBean id="RequisicaoBeanId"  scope="request" class="sys.RequisicaoBean" />

<% 
	String mySistema  = UsuarioFuncBeanId.getAbrevSistema() ; 
	String nomeTitulo = UsuarioFuncBeanId.getJ_nomFuncao() ;  
	String nomeSistema= "CONTROLE DE ACESSO" ;  	
	String Cor = "#DFEEF2" ;
	String Cresult="#be7272";
	String resultline = "#faeae5";
    if ("REC".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 Cresult = "#be7272";
		 nomeSistema= "RECURSO" ;		 
	}
    if ("REG".equals(mySistema)) {
		 Cor = "#EFF5E2" ;
		 Cresult = "#a8b980";
		 nomeSistema= "REGISTRO" ;
	}	
    if ("PNT".equals(mySistema)) {
		 Cor = "#E9EEFE" ;
 		 nomeSistema= "PONTUA��O" ;		 		 
	}	
    if ("CID".equals(mySistema)) {
		 Cor = "#faeae5" ;
		 nomeSistema= "COMISS�O CIDAD�" ;		 		 
	}	
    if ("GER".equals(mySistema)) {
		 Cor = "#F8EFD3" ;					
		 nomeSistema= "GERENCIAL" ;
		 Cresult="#B8A47A";			
		 resultline = "#efeadc"	; 		 
	}	
%>

<html>
<head>
<jsp:include page="Css.jsp" flush="true" />
<style type=text/css>
.branco2 {
	 COLOR: white; FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif; TEXT-DECORATION: none
}
.branco2:link {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco2:visited {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica
}
.branco2:hover {
	 COLOR: #ffffff; FONT-FAMILY: Verdana, Arial, Helvetica; TEXT-DECORATION: underline
}
</style>
<title>:: TRANSITIVO ::</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<SCRIPT LANGUAGE="JavaScript1.2" SRC="<%=path%>/js/itcutil.js" TYPE='text/javascript'></SCRIPT>
<SCRIPT LANGUAGE="JavaScript1.2">
<!-- Abre a janela com o tamanho maximo disponivel -->
self.moveTo(0,0);self.resizeTo(screen.availWidth,screen.availHeight); 

myOrg     = new Array(<%= OrgaoBeanId.getOrgaos(  UsuarioBeanId  ) %>);
cod_orgao = new Array(myOrg.length/2);
sig_orgao = new Array(myOrg.length/2);
for (i=0; i<myOrg.length/2; i++) {
	cod_orgao[i] = myOrg[i*2+0] ;
	sig_orgao[i] = myOrg[i*2+1] ;
}
myUsr        = new Array(<%= UsrBeanId.getUsuarios() %>);
cod_usuario  = new Array(myUsr.length/3);
nom_username = new Array(myUsr.length/3);
for (i=0; i<myUsr.length/3; i++) {
	cod_usuario[i] = myUsr[i*3+0] ;
	nom_username[i] = myUsr[i*3+1] ;
}

mySist      = new Array(<%= SistBeanId.getSistemas(UsuarioBeanId.getCodUsuario()) %>);
cod_sistema = new Array(mySist.length/2);
nom_sistema = new Array(mySist.length/2);
for (i=0; i<mySist.length/2; i++) {
	cod_sistema[i] = mySist[i*2+0] ;
	nom_sistema[i] = mySist[i*2+1] ;
}


function veCampos(fForm){


	var sData;
	var	acao;
	var edtDatIni		= fForm.edtDe;
	var edtDatFim		= fForm.edtAte;
	var cmbCodOrgao 	= fForm.codOrgao;
	var cmbCodUsuario	= fForm.codUsuario;

	
	if (edtDatIni.value == ""){
		alert("Digite a data inicial");
		edtDatIni.focus();
		return false;	
	}


	if (!ValDt(edtDatIni,0))
		return false;		


	if (edtDatFim.value == ""){
		alert("Digite a data final");
		edtDatFim.focus();
		return false;		
	}							
		
		
	if (!ValDt(edtDatFim,0))
		return false;		
		
		
	if (edtDatIni.value > edtDatFim.value){
		alert("Data final deve ser maior ou igual a data inicial");
		edtDatFim.focus();
		return false;			
	}	
		
		
	dia = edtDatIni.value.toString().substring(0,2);
	mes = edtDatIni.value.toString().substring(3,5);
	ano = edtDatIni.value.toString().substring(6,10);	
	mes = mes - 1;	
	
	var dataInicio = new Date();		
	dataInicio.setDate(dia);
	dataInicio.setMonth(mes);
	dataInicio.setYear(ano);		
		
	dia = edtDatFim.value.toString().substring(0,2);
	mes = edtDatFim.value.toString().substring(3,5);
	ano = edtDatFim.value.toString().substring(6,10);	
	mes = mes - 1;
			
	var dataFinal  = new Date();	
	dataFinal.setDate(dia);
	dataFinal.setMonth(mes);
	dataFinal.setYear(ano);
	
	var diff = dataFinal.getTime() - dataInicio.getTime();	
	var dias = Math.floor(diff/(1000*60*60*24));

	if (fForm.codSistema.value == 0){
		alert("Informe o Sistema.");
		return false;			
	}				

   return true;
	
}

function Classificacao(ordem,fForm) {
	document.all["acao"].value="Classifica";
	document.all["ordem"].value=ordem;
	document.all["UsrAtuForm"].target= "_self";
	document.all["UsrAtuForm"].action = "acessoTool";  
	document.all["UsrAtuForm"].submit();	 
}


function valida(opcao,fForm) {
 switch (opcao) {
   case 'buscaUsuarios':
		fForm.codUsuario.options.length = 0;
	    fForm.acao.value=opcao;
		fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  break ; 
   case 'buscaSistemas':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 
  case 'mostraRespConsulta':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 	  
	  	  
   case 'consultaRegistros':
      if (veCampos(fForm)==true) { 
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ; 	  
   case 'A':
      if (veCampos(fForm)==true) {
		fForm.acao.value=opcao;
	   	fForm.target= "_self";
	    fForm.action = "acessoTool";  
	   	fForm.submit();	  		  
	  }
	  break ;
   case 'R':
      close() ;   
	  break;
   case 'ImprimeResultConsulta':
		fForm.acao.value=opcao
   		fForm.target= "_blank";
    	fForm.action = "acessoTool";  
   		fForm.submit();	  		  
	  	break;   
   case 'V':
	fForm.acao.value=opcao
   	fForm.target= "_self";
    fForm.action = "acessoTool";  
   	fForm.submit();	  		  
	  break;
   case 'O':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='hide' ; 
	      else                 document.all["MsgErro"].style.visibility='hidden' ; 
	  break;  
   case 'V':
   		  if (document.layers) fForm.layers["MsgErro"].visibility='show' ; 
	      else                 document.all["MsgErro"].style.visibility='visible' ; 
	  break;
  }
}


function atualiza(opcao,fForm) {
 if (veCampos(fForm)==false) return ;
}


function SetConfig(){
	var dia;
	var mes;
	var ano;
	var hoje;	

	data = new Date();
	dia = "01";
	mes = data.getMonth();
	ano = data.getYear();
		
	if (mes.toString().length == 1)
		mes = "0" + mes;	
	
	document.forms[0].edtDe.value = dia + "/" + mes + "/" + ano;
	document.forms[0].edtAte.value = PegaDtHoje();
}

function j_ajuda(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "AjudaCommand" ;
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}
function j_info(fForm) {
	temp = fForm.j_cmdFuncao.value
	fForm.j_cmdFuncao.value = "InformacoesCmd";
      document.forms[0].acaoNA.value = "";
   	fForm.target= "_blank";
    fForm.action = "acessoTool";  
   	fForm.submit();	  	
	fForm.j_cmdFuncao.value= temp		  
}

</script>
</head>

<body  bgcolor="#ffffff" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow: hidden;">
<form name="UsrAtuForm" method="post" action="">
<input name="acao"		type="hidden">			
<input name="coluna" 	type="hidden">	
<input name="ordem"          type="hidden" value="Data">
<!--INICIO - CAB. INDEPENDE DO MODULO-->
<!-- Div de mensagem de Noticia e Avisos  -->
<jsp:include page="../sys/MensagemAviso.jsp" flush="true"/>
<!-- Fim da Div -->
<div id="cabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:108px; z-index:1; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr><td background="images/<%= mySistema %>/detran_bg_cab.png"><img src="images/<%= mySistema %>/detran_cab.png" width="673" height="108"></td>
    </tr>
  </table>
</div>

<div id="TextoCabecalho" style="position:absolute; left:0px; top:0px; width:100%; height:33px; z-index:12; visibility: visible;"> 
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td width="205" height="85"  valign="middle"><img src="images/inv.gif" width="205" height="3"></td>
<%--       <td width="534" valign="middle" align="center"><font color="#999999">Vers�o <%=SistemaBeanId.getSigVersao()%>&nbsp;</font>  --%>
<%--         <%	 --%>
<!-- 			String qb = (String)session.getAttribute("j_qualBanco"); -->
<!-- 			if (qb==null) qb="???" ; -->
<!-- 			if ("(P)".equals(qb)) out.print("<font color=#999999>"+qb+"</font>"); -->
<!-- 			else out.print("<font color=red><b>"+qb+"</b></font>"); -->
<!-- 		%> -->
<%--         <font color="#999999">&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%></font>  --%>
<%--         &nbsp;&nbsp;&nbsp;&nbsp; <font color="#999999"><%=UsuarioBeanId.getNomUserName()%>&nbsp;-&nbsp;�rg�o  --%>
<%--       Atua��o:&nbsp; <%=UsuarioBeanId.getSigOrgaoAtuacao()%></font> </td> --%>
      
      <td align="right" valign="top">
	  <button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Info" onClick="javascript: j_info(this.form);">
		<%if (UsuarioBeanId.possuiInformacao(UsuarioFuncBeanId)) {%>
		<img src="<%= path %>/images/bot_info_ico.png" alt="Existem Mensagens para Voc� !" width="25" height="32" border="0"></button>
		<%} else {%>
		<img src="<%= path %>/images/bot_infovisited_ico.png" alt="N�o existem Mensagens para Voc�." width="25" height="32" border="0"></button>
		<%}%></td>
	  
	  <td width="32" align="right" valign="top"><a href="AutoCicloVida.html" target="_blank"> 
		<img src="images/ico_ciclo.png" alt="Ciclo de Vida do Auto de Infra��o" width="25" height="32" border="0"></a>
	  </td>

	  <td width="45" align="center"  valign="top">
		<button type="button" style="height: 33px; width: 27px; border: none; background: transparent; cursor: hand;" name="j_Ajuda" onClick="javascript: j_ajuda(this.form);">
	  <img src="<%= path %>/images/detran_help.png" alt="Ajuda do sistema" width="25" height="32" border="0"></button>	  </td>
    </tr>
  </table>

</div>  

<!--Fundo da tela-->
<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0">
	  <tr> 
	    <td width="34" valign="top" background="images/<%= mySistema %>/detran_bg1.png">
			<img src="images/<%= mySistema %>/detran_bg1.png" width="34" height="108"><img src="images/<%= mySistema %>/detran_lat.png" width="34" height="131">
		</td>
		<td  align="center" valign="top">&nbsp;</td>
	 </tr>
</table>

<input name="j_token"       type="hidden" value="<%=RequisicaoBeanId.getToken()%>">				
<input name="j_cmdFuncao"   type="hidden" value="<%=RequisicaoBeanId.getCmdFuncao()%>">
<input name="j_sigFuncao"   type="hidden" value="<%=RequisicaoBeanId.getSigFuncao()%>">
<input name="j_jspOrigem"   type="hidden" value="<%=RequisicaoBeanId.getJspOrigem()%>">
<input name="j_abrevSist"   type="hidden" value="<%=RequisicaoBeanId.getAbrevSist()%>">	
  
<!--FIM Fundo da tela-->
<!--T�TULO DO M�DULO-->
<div id="titModulo" style="position:absolute; left: 270px; top: 55px; width:516px; height:23px; z-index:11; overflow: visible; visibility: visible;" class="espaco"> 
	  <table width="100%" border="0" cellpadding="2" cellspacing="0" class="espaco">
		<tr>
        <td><font color="#000000"><strong>
		<% if (UsuarioFuncBeanId.getJ_nomFuncaoPai().length()>0) { %>
			<%= UsuarioFuncBeanId.getJ_nomFuncaoPai() %>&nbsp;
			<img src="<%= path %>/images/seta_tit.gif" width="4" height="6">&nbsp;
		<% } %>
		<%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></font>
		</td>
		</tr>
	  </table>
</div>
<!-- FIM - CAB. -->

<div id="WK_SISTEMA" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 75px; left: 50px; z-index:1; visibility: visible;" > 
  <!--INICIO CABEC DA TABELA-->
<table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">
      <tr><td colspan="2" height="2"></td></tr>
      <tr>
	  	<% if (LogAcessoBeanId.getAtualizarTela().equals("N")){ %>				
			  <td  width="28%"><b>�rg�o Lota��o:&nbsp;</b>
					<select name="codOrgao"  onChange="javascript: valida('buscaUsuarios',this.form);"> 
					<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>
						for (j=0;j<cod_orgao.length;j++) {
							document.write ("<option value="+cod_orgao[j]+">"+sig_orgao[j])+"</option>"
						}
					</script>
				   </select>
			  </td>

  			  <td width="44%"><b>Sistema:&nbsp;</b>
				  <select name="codSistema"> 
						<option value="0" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
					<script>					
						var codigosistema = <%= SistBeanId.getCodSistema()%>;
						var indice = 0;
						for (j=0;j<cod_sistema.length;j++) {
							codsys = cod_sistema[j];
							document.write ("<option value="+cod_sistema[j]+">"+nom_sistema[j])+"</option>"
							if (codigosistema == codsys){
								indice = j+1;
							}
						}
						document.forms[0].codSistema.selectedIndex = indice;								
					</script>
		         </select>
			  </td>    
	          <td width="5%">
			  		<button type="button" NAME="Ok"   style="width: 44px;height: 23px;  border: none; background: transparent;"  onClick="javascript: valida('consultaRegistros',this.form);">	
		    	    	<IMG src="<%= path %>/images/bot_ok.gif" align="left" >
					</button>
			  </td>
		<% }else if (LogAcessoBeanId.getAtualizarTela().equals("S") ){ %>				
			  	<% if (!OrgaoBeanId.getCodOrgao().equals("0")){%>
		  		<td width="30%" nowrap><b>Org�o Lota��o:&nbsp;</b>
	            	<input name="codOrgao"          type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
					<input disabled name="sigOrgao" type="text"  size="15" maxlength="15" value="<%= OrgaoBeanId.getSigOrgao() %>" style="border: 0px none;" >	
				</td>
				<%}	else{ %>
		  		<td width="30%" nowrap><b>Org�o Lota��o:&nbsp;</b>
	            	<input name="codOrgao"          type="hidden" size="10" maxlength="6"  value=""> 
					<input disabled name="sigOrgao" type="text"  size="15" maxlength="15" value="TODOS" style="border: 0px none;" >	
				</td>				
				<% } %>
			  	<td width="0%"></td>
				<% if (!UsrBeanId.getCodUsuario().equals("0")){ %>
			  	<td nowrap width="40%"><b>Usu�rio:&nbsp;</b>
	            	<input name="codUsuario"           type="hidden" size="14" maxlength="10" value="<%= UsrBeanId.getCodUsuario() %>"> 
	            	<input disabled name="nomUserName" type="text"  size="20" maxlength="20" value="<%= UsrBeanId.getNomUserName() %>" style="border: 0px none;">
			  	</td>		
				<% }else{ %>
			  	<td nowrap width="40%"><b>Usu�rio:&nbsp;</b>
	            	<input name="codUsuario"           type="hidden" size="14" maxlength="10" value=""> 
        			<input disabled name="nomUserName" type="text"  size="20" maxlength="20" value="TODOS" style="border: 0px none;">
			  	</td>		
				<% } %>
				<% if (!SistBeanId.getCodSistema().equals("0")){ %>
			  	<td><b>Sistema:&nbsp;</b>
	            	<input name="codSistema"          type="hidden" size="14" maxlength="10" value="<%= SistBeanId.getCodSistema() %>"> 
		        	<input disabled name="nomSistema" type="text"  size="50"  maxlength="50" value="<%= SistBeanId.getNomSistema() %>" style="border: 0px none;">
		 	  	</td>				
				<% }else{ %>
			  	<td><b>Sistema:&nbsp;</b>
	            	<input name="codSistema"           type="hidden" size="14" maxlength="10" value=""> 
        			<input disabled name="nomSistema"  type="text"  size="50" maxlength="50" value="TODOS" style="border: 0px none;">
				</td>												
				<% } %>
			<% } %>	  					  			  
	  </tr>	      
	  <tr>
	  <% if (LogAcessoBeanId.getAtualizarTela().equals("N")){ %>
        <td width="28%"><p><b>Usu�rio: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><b>&nbsp;&nbsp;&nbsp;&nbsp;		
              <select name="codUsuario">
			  		<option value="0">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
            	<script>
						var codigousuario = <%=UsrBeanId.getCodUsuario()%>;
						var indice = 0;
						for (j=0;j<cod_usuario.length;j++) {
							codigo = cod_usuario[j];
							document.write ("<option value="+cod_usuario[j]+">"+nom_username[j])+"</option>"
							if (codigousuario == codigo){
								indice = j+1;
							}
						}
						document.forms[0].codUsuario.selectedIndex = indice;
				</script>
          	</select>
          	&nbsp;&nbsp;&nbsp; </b>
        	</p>        
		<% } %>  
		</td>
		<td width="15%"><p><b>
	  	<% if (LogAcessoBeanId.getAtualizarTela().equals("N")){ %>
			De: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<input name="edtDe" type="text" size="14" maxlength="10" onchange="javascript:ValDt(this,0);" onkeypress="javascript:f_hora();" value="<%=LogAcessoBeanId.getDatIni()%>" >
			</b>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<b>Ate: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 		
				<input name="edtAte" type="text" size="14" maxlength="10" onchange="javascript:ValDt(this,0);" onkeypress="javascript:f_hora();" value="<%=LogAcessoBeanId.getDatFim()%>"></b>
		<% }else if (LogAcessoBeanId.getAtualizarTela().equals("N")){ %>
				<input name="edtDe" type="hidden" size="14" maxlength="10" value="<%=LogAcessoBeanId.getDatIni()%>">
			    <input name="edtAte" type="hidden" size="14" maxlength="10" value="<%=LogAcessoBeanId.getDatFim()%>">
		<% } %>
		</td>
      </tr>
	  <tr>
	  </tr>
    </table>    
	<TABLE border="0" cellpadding="0" cellspacing="0" width="100%">	 
		<tr>
	  	<% if (LogAcessoBeanId.getAtualizarTela().equals("S")){ %>			 		
	 		<td width="91%"  align = "right"> 
	  		  <button type=button NAME="Retornar"  style="height: 21px; width: 54px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
				  <img src="<%= path %>/images/bot_retornar.gif" width="52" height="19" >
			  </button>
			</td>
			<td width="3%"></td>
			<td width="6%">
			<button style="border: 0px; background-color: transparent; height: 21px; width: 31px; cursor: hand;" type="button" onClick= "javascript: valida('ImprimeResultConsulta',this.form)"> 
              <img src="<%= path %>/images/ACSS/bot_imprimir_ico.gif" alt="Imprimir" width="29" height="19">
			</button>			
		  </td>				
	   <% } %>	
	</tr>	   
    </table>	
</div>
<% String  msgErro = "";%> 
<% if (LogAcessoBeanId.getAtualizarTela().equals("confirma") ){ %>
<div id="verifica_resposta" style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 90px; left: 53px; z-index:1; visibility: visible;" > 
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>
      <td width="33%">Total de registros localizados :
        <input readonly align="right" name="totalRegistros" type="text"  size="10" maxlength="10" value="<%=LogAcessoBeanId.getTamTotal()%>" style="border: 0px none;"></td>
    </tr>
	<tr height="18"></tr>
  </table>	
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
    <tr>  
      <td width="25%">Deseja prosseguir ?</td>
    </tr>
    <tr>
      <td width="25%" align="right" > 
        <input name="edtDe" type="hidden" size="14" maxlength="10" value="<%=LogAcessoBeanId.getDatIni()%>">
		<input name="edtAte" type="hidden" size="14" maxlength="10" value="<%=LogAcessoBeanId.getDatFim()%>"> 
		<input name="codSistema" type="hidden" size="14" maxlength="10" value="<%= SistBeanId.getCodSistema() %>"> 
		<input name="codUsuario" type="hidden" size="14" maxlength="10" value="<%= UsrBeanId.getCodUsuario() %>"> 
		<input name="codOrgao" type="hidden" size="10" maxlength="6"  value="<%= OrgaoBeanId.getCodOrgao() %>"> 
  		<button style="border: 0px; background-color: transparent; height: 20px; width: 60px; cursor: hand;" type="button" onClick= "javascript:valida('mostraRespConsulta',this.form);"> 
         <img src="<%= path %>/images/bot_continuar.gif" alt="" width="59" height="19">
	     </button>
	  </td>
	  <td width="3%"></td>
	  <td width="72%">
	     <button type=button NAME="Retornar"  style="height: 21px; width: 84px;border: none; background: transparent;" onClick="javascript: valida('V',this.form);" >
		 <img src="<%= path %>/images/bot_novapesquisa.gif" width="82" height="19" >
	     </button>
	     			
	 </td>	  
    </tr>
  </table> 
</div>
<% } %>   
<% if (LogAcessoBeanId.getAtualizarTela().equals("S")){ %>  
 <div style="position:absolute; width:720px; overflow: visible; z-index: 1; top: 132px; left: 50px; height: 32px; background-color: #FFFFFF; layer-background-color: #FFFFFF; border: 1px none #000000; visibility: visible;" id="TITULOS">
  <table border="0" cellpadding="0" cellspacing="1" width="100%"  align="center">  	   		
     <tr>
       <td colspan=2 align=right>
		 <font color="#000000">&nbsp;Ordem:&nbsp;<%= LogAcessoBeanId.getNomOrdem()%>&nbsp;</font>
	   </td>	
    </tr>  		   
  </table>

  <table width="100%" border="0"  align="center" cellpadding="1" cellspacing="1">  	   		
    <tr><td colspan=8 height=3></td></tr>		  		
	<tr> 		   	 		 		 	  
	   	 <td width = "34" align="center" bgcolor="<%=Cresult%>"><font color="#ffffff"><b>SEQ</b></font></TD>		 		 	  
   	     <td  width = "82" bgcolor="<%=Cresult%>"><div align="center"><font color="#ffffff"><b>
		   <a href="#" class="branco2" onClick="javascript: Classificacao('Orgao', this.form);">ORG&Atilde;O&nbsp;</a></b></font></div></TD>
         <td  width = "122" bgcolor="<%=Cresult%>"><div align="center">&nbsp;&nbsp;&nbsp; 
	       <font color="#ffffff"><b><a href="#" class="branco2" onClick="javascript: Classificacao('Usuario', this.form);">USU&Aacute;RIO</a></B></font></div></TD>
	   	 <td width = "44" align="center" bgcolor="<%=Cresult%>"><font color="#ffffff">
		   <a href="#" class="branco2" onClick="javascript: Classificacao('Sistema',this.form);"><b>SIST.</b></a>
		   </font></TD>		 
	   	 <td width = "224" align="center" bgcolor="<%=Cresult%>">
		   <font color="#ffffff"><a href="#" class="branco2" onClick="javascript: Classificacao('Operacao',this.form);"><b>FUN&Ccedil;&Atilde;O</b></a></font></TD>
	   	 <td width = "127" align="center" bgcolor="<%=Cresult%>">
		   <font color="#ffffff"><b><a href="#" class="branco2" onClick="javascript: Classificacao('DatOperacao',this.form);">DAT. OPER.</a></B></font></TD>
	   	 <td bgcolor="<%=Cresult%>"><font color="#ffffff"><strong><a href="#" class="branco2" onClick="javascript: Classificacao('Atuacao', this.form);">ATUA&Ccedil;&Atilde;O</a></strong></font></TD>
	</tr>
   </table>
</div>   


<div style="position:absolute; width:720px; overflow: auto; z-index: 2; top: 165px; left: 50px; height: 166px; visibility: visible;" id="DADOS">   
		  
   <table width="100%" border="0"  align="center" cellpadding="1" cellspacing="1" id="fevento">  	   		
	<%
		Iterator lstUsrSys = LogAcessoBeanId.getListaTotal().iterator();				
		int seq = 	1;
        while (lstUsrSys.hasNext()) {				
			ACSS.LogAcessoBean logAcesso =  new ACSS.LogAcessoBean() ;				
			logAcesso = (ACSS.LogAcessoBean)lstUsrSys.next(); 		
	%>
		<tr> 			   
	   	  <td width="34" align="center" bgcolor="<%=Cor%>">&nbsp;<%=seq%></TD>		 		 	  
		  <td width="82" align="left"  bgcolor="<%=Cor%>"><div align="left">&nbsp;&nbsp;<%=logAcesso.getSigOrgao()%></div></TD>
		  <td width="122" align="left" bgcolor="<%=Cor%>"><div align="left">&nbsp;&nbsp;<%=logAcesso.getNomUserName()%></div></TD>		 
          <td width="44" align="left"  bgcolor="<%=Cor%>">&nbsp;&nbsp;<%=logAcesso.getNomAbrev()%></TD>
          <td width="224" align="left"  bgcolor="<%=Cor%>">&nbsp;&nbsp;<%=logAcesso.getNomOperacao()%></TD>
          <td width="127" align="center"  bgcolor="<%=Cor%>"><%=logAcesso.getDatOperacao()%></TD>
          <td bgcolor="<%=Cor%>">&nbsp;<%=logAcesso.getSigOrgaoAtuacao()%></TD>
	  </tr>
       <%	
	   		seq++;
	   	}												
	%> 		  		  
  </table>  
       
<% } %>     
</div>
<!--Div Erros-->

<jsp:include page="../sys/DivErro.jsp" flush="true" >
  <jsp:param name="msgErro" value= "<%= LogAcessoBeanId.getMsgErro()%>" />
  <jsp:param name="msgErroTop" value= "230 px" />
  <jsp:param name="msgErroLeft" value= "50 px" />
</jsp:include> 
<!--FIM_Div Erros-->

<!-- Rodap�-->
<!--Bot�o retornar--> 

<div id="retornar" style="position:absolute; right: 10px; bottom: 4px; width:26px; height:25px; z-index:10; background-color: transparent; overflow: visible; visibility: visible;"> 
	<table width="25" border="0" cellpadding="0" cellspacing="0" class="table">
		<tr> 
		  <td height="26"><button style="border: 0px; background-color: transparent; height: 25px; width: 25px; cursor: hand;" type="button" onClick="javascript:  valida('R',this.form);"> 
          <img src="<%= path %>/images/<%= mySistema %>/bot_retornar.gif" alt="Retornar ao Menu"></button></td>
		</tr>
	</table>
</div>
<div id="rodape" style="position:absolute; left:0px; bottom:-1px; width:100%; height:94px; z-index:1; visibility: visible;"> 
  <table width="100%" height="94" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="bottom" background="images/<%= mySistema %>/detran_bg3.png">
	  	<img src="images/RJ/<%= mySistema %>/detran_rod_logos.png" width="794" height="94"></td>
    </tr>
  </table>
</div>
<!-- Fim Rodap� -->
<script>			  
// posiciona o popup na descri�ao equivalente ao Sistema
nj = '<%= OrgaoBeanId.getCodOrgao() %>'
njint  = parseInt(nj,10)
if ((nj.length>0) && (nj!=0)){
	for(i=0;i<document.UsrAtuForm.codOrgao.length;i++){
		if(document.UsrAtuForm.codOrgao.options[i].value==njint){
			document.UsrAtuForm.codOrgao.selectedIndex=i;	  
		}
	}
}
</script>			  
</form>
</BODY>
</HTML>

