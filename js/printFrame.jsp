<%@ page session="true" %>
<% String path=request.getContextPath(); %>
<%@ page errorPage="ErrorPage.jsp" %>
<!-- Chama o Objeto do Usuario logado -->
<jsp:useBean id="UsuarioBeanId"     scope="session" class="ACSS.UsuarioBean" /> 
<!-- Chama o Objeto do Usuario Funcao-->
<jsp:useBean id="UsuarioFuncBeanId" scope="session" class="ACSS.UsuarioFuncBean" /> 


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>SMIT</title>
	<style type="text/css">
		DIV.quebrapagina{page-break-after: always}
		table {
			 FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
			 font-size: 9px;
			 letter-spacing: normal;
			 font-variant: normal;
			 text-transform: none;
			 text-decoration: none;			 	 
		}
		td {
			FONT-FAMILY: Verdana, Helvetica, Arial, sans-serif;
		  	font-size: 9px;
			color: #000000;
			border: 0px;	
		}
		
		
	</style>
</head>
<body topmargin="0" leftmargin="0">
<!--CABE�ALHO IMPRESS�O-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="semborda">
  <tr><td width="20%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr><td height="20" valign="top">EMISS&Atilde;O : <strong>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(0,10)%>&nbsp;&nbsp;<%=sys.Util.formatedToday().substring(13,18)%> </strong></td>
        </tr>
        <tr> 
          <td width="38%" height="40" valign="top"><img src="../images/im_logodetran.gif"></td>
        </tr>
      </table>
        
    </td>
          
    <td width="65%" align="center" valign="top" class="fontmaior"> <strong><%=UsuarioBeanId.getOrgao().getNomOrgao()%></strong>
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>          
      	 <STRONG style="font-size: 11px;">:: TRANSITIVO ::</strong> 
      <table border="0" class="semborda"><tr><td height="2"></td></tr></table>
      <span class="fontmedia"><STRONG><%= UsuarioFuncBeanId.getJ_nomFuncao() %></strong></span></td>
    <td width="15%" align="center"> 
      <table width="100%" height="60" border="0" cellpadding="0" cellspacing="0" class="semborda">
        <tr>
          <td height="20" align="right" valign="top"></td>
        </tr>
        <tr> 
          <td height="40" align="right" valign="top"><img src="../images/im_logosmit.gif" width="98" height="30"></td>
        </tr>
      </table>
		  
    </td>
        </tr>        
        
</table>


<!--Linha-->
<table cellpadding=0 cellspacing=0 border="0" width="100%" bordercolor="#ffffff" class="semborda">
	<tr> 
		<td height="1" bgcolor="#000000"></td>
	</tr>
</table>
<!--FIM Linha-->

<!--FIM CABE�ALHO IMPRESS�O-->


<table ID="myTABLE" width="720" border="0" cellpadding="0" cellspacing="0" class="table">
<TBODY>
<!--conteudo inicio-->
<!--conteudo fim-->
</TBODY>
</TABLE>

<div id="rodape" style="position:absolute; left:0px; bottom:5px; width:100%; height:50px; z-index:1; visibility: visible;"> 
<!--RODAP� IMPRESS�O-->
<!--Linha-->
<hr noshade color="#000000">
<!--FIM Linha-->
<table width="100%" border="0" cellpadding="3" cellspacing="0" class="semborda">
  <tr> 
    <td width="31%" ><strong>USU&Aacute;RIO :</strong>&nbsp;<span class="fontmedia"><%=UsuarioBeanId.getNomUserName()%></span></td>
    <td width="69%" height="30" align="right" >
		<strong>ORG&Atilde;O LOTA&Ccedil;&Atilde;O :</strong>&nbsp;<span class="fontmedia"><%=UsuarioBeanId.getSigOrgaoAtuacao()%></span> 
    </td>
  </tr>
</table>
<!--FIM RODAP� IMPRESS�O-->
</div>
</body>
</html>