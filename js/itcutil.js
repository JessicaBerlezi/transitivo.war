/*
Indice das fun??es em Javascript da MIND
-------------------------------------------------------------------
01 - autoTab(input,len, e) ==> Tab autom?tico
	input
-------------------------------------------------------------------	
02 - bissexto(umAno) ==> Verifica se o ano ? bissexto
	umAno = "aaaa" ==> return = True ou False
-------------------------------------------------------------------
03 - verData(elemento1,elemento2,elemento3, nome,preencher) ==> Verifica uma data v?lida
	elemento1,elemento2,elemento3, nome,preencher = "31,02,2003, nome do campo , " ==> return = "O campo " + nome + " n?o foi preenchido corretamente!
-------------------------------------------------------------------
04 - verString(elemento, nome, minimo) ==> Verifica se o campo est? vazio
	elemento, nome, minimo = "  (vazio)  ", nome, tamanho minimo ==> return = "campo vazio" ou "campo deve ter no minimo +tamanho minimo+"
-------------------------------------------------------------------
05 - apagar
-------------------------------------------------------------------
06 - ValidaHora(elemento) ==> Verifica a hora e o minuto v?lido
	elemento = "25:61" ==> return "Hora invalida" e "Minuto invalido"
-------------------------------------------------------------------
07 - verValor(elemento, nome) ==> O valor do campo n?o pode ser "zero" ou vazio
	elemento, nome = "0", nome ==> return "O campo n?o pode ser zero" e "O campo n?o pode ser vazio"
-------------------------------------------------------------------
08 - verValFk(elemento, nome) ==> O campo n?o pode ser vazio
	elemento, nome = "  (vazio) ", nome ==> return "O campo n?o pode ser vazio"
-------------------------------------------------------------------
09 - function edtCmp(obj,tam,tpId)  ==> Edita os campos digitados e retorna a m?scara v?lida
	obj,tam,tpId = "   11111111111    , 11, CPF " ==> return = "111.111.111-11"
--------------------------------------------------------------------
10 - trim(theString) ==> Tira os espa?os em branco do campo
	theString="      sergio    mont "     ==>   return="sergio   mont"
--------------------------------------------------------------------
11 - f_num() ==> Aceita somente numero
	"A" ==> return False
-------------------------------------------------------------------
12 - f_num_vir() ==> Aceita numeros e virgula
	"0,78" ==> return "True"
-------------------------------------------------------------------
13 - f_num_pont() ==> Aceita numeros e ponto
	"58.6" ==> return = "False"
-------------------------------------------------------------------
14 - f_tel() ==> Aceita telefone (numeros, "-", "(" e ")") 
	"(21)3514-6887" ==> return = "True"
-------------------------------------------------------------------
15 -  f_let() ==> Aceita letras (mai. e min.)
	"MINDInForMaTica" ==> return = "True"
-------------------------------------------------------------------
16 - f_end() ==> Enderecos (Numeros, Letras, "/", ",", "-","."
	"Rua A, bl.04/204" ==> return = "True"
-------------------------------------------------------------------
17 - f_hora() ==> Hora (Numeros, "/", ".", "-",":")
	"12:40" ==> return = "True"
-------------------------------------------------------------------
18 - f_cgc() ==> CGC (Numeros, "/", ".", "-")
	"28.294.023/0001-75" ==> return "True"
-------------------------------------------------------------------
19 -
-------------------------------------------------------------------
20 -
-------------------------------------------------------------------
21 -
-------------------------------------------------------------------
22 - ValCGC(obj) ==> Valida campo de 15 posicoes com o numero do CGC
	"28.294.023/0001-75" ==> return = "True" (chama a fun??o "dig_cgc(cgcinf)"  para calcular o digito verificador do CGC)
-------------------------------------------------------------------
23 - dig_cgc(cgcinf) ==> Valida o digito verificador do CGG
	? usada pela fun??o "ValCGC(obj)" para calcular o CGC
-------------------------------------------------------------------
24 - ValCPF(obj) ==> Valida campo de 11 posicoes com o numero do CPF
	"111.111.111-11" ==> return "True" (chama a fun??o "dig_cpf(cpfinf)"  para calcular o digito verificador do CPF)
-------------------------------------------------------------------
25 - dig_cpf(cpfinf) ==> Valida o digito verificador do CPF
	? usada pela fun??o "ValCPF(obj)" para calcular o CPF
-------------------------------------------------------------------
26 - 
-------------------------------------------------------------------
27 - Valida uma data
-------------------------------------------------------------------
28 - Pega a data de hoje
-------------------------------------------------------------------
29 - Coloca o valor do objeto recebido em letras mai?sculas.
-------------------------------------------------------------------
30 - Coloca o valor do objeto recebido em letras min?sculas.
-------------------------------------------------------------------
?? - ValNumProcesso(obj) ==> Valida numero do processo de at? 20 posicoes 
-------------------------------------------------------------------

*/




var valid = true;
var sErro = "Erro(s): \n";
var sRet, sLocal;
var ord_asc;
//var isNN = (navigator.appName.indexOf("Netscape")!=-1);

//01 
function autoTab(input,len, e) {
  var keyCode = (isNN) ? e.which : e.keyCode; 
  var filter = (isNN) ? [0,8,9] : [0,8,9,16,17,18,37,38,39,40,46];
  if(input.value.length >= len && !containsElement(filter,keyCode)) {
    input.value = input.value.slice(0, len);
    input.form[(getIndex(input)+1) % input.form.length].focus();
  }
   function containsElement(arr, ele) {
    var found = false, index = 0;
    while(!found && index < arr.length)
        if(arr[index] == ele)
	      found = true;
        else
          index++;
    return found;
  }
function getIndex(input) {
    var index = -1, i = 0, found = false;
    while (i < input.form.length && index == -1)
      if (input.form[i] == input)
	    index = i;
      else
	    i++;
    return index;
  }
  return true;
}

//02
function bissexto(umAno) {
  if (((umAno % 4 == 0) && (umAno % 100 != 0)) || (umAno % 400 == 0))
    return true;
  else
    return false;
}

//03
function verData(elemento1,elemento2,elemento3, nome,preencher) {
  var valid2 = true;
  var jdia = elemento1.value;	
  var jmes = elemento2.value;
  var jano = elemento3.value;
  var jdatai = jdia+jmes+jano;
  jdatai=trim(jdatai);
  var jmesfev = '28';
  if (jmes>'12' || jmes<'01'){
     valid2 = false;
  }
  if (jdia>'31' || jdia<'01'){
     valid2 = false;
  }
  if (jdia>'30' && (jmes=='04' || jmes=='06' || jmes=='09' || jmes=='11')){
     valid2 = false;
  }
  if (bissexto(parseInt(jano))){
    jmesfev='29';
    }else{
    jmesfev='28';
  }
  if (jdia>jmesfev && jmes=='02'){
    valid2 = false;
  }
  if (parseInt(jano)==0){
    valid2 = false;
  }
  if (jdatai.length < 8) {
    valid2 = false;
  } else {
    for (i = 0; i < jdatai.length; i++) {
      ch = jdatai.charAt(i).toLowerCase();
      if (ch < '0' || ch > '9') valid2 = false;
    }
  }
  if (!valid2) {
    if (preencher==1) {
      valid = false;
      sErro = sErro +  "O campo " + nome + " n?o foi preenchido corretamente! \n";  
	}  
  }
}

//04
function verString(elemento, nome, minimo) {
  berr= true
  str =trim(elemento.value)
  if (str.length < 1) {
  	berr = false 
	valid = false
    sErro = sErro +  "O campo '" + nome + "' n?o pode ser vazio! \n";
  } 
  if (str.length < minimo) {  
  	berr = false 
	valid = false	   
    sErro = sErro +  "O campo '" + nome + "' deve ter no m?nimo " + minimo + " posicoes \n";
  }
  return ;
}

//05
function verPopup(elemento, nome) {
  berr= true
  for( pos = 0 ; pos < elemento.length ; pos++ )
  { 
  	if (elemento.charAt(pos) != ' ') {	return berr ; }
  } 
  berr = false
  valid = false;  
  sErro = sErro +  "O campo '" + nome + "' n?o pode ser vazio! \n";
} 

//06
function ValidaHora(elemento) {
  berr= true
  if (elemento.value.substring(0,2)>24) {
  	berr = false
    valid = false;  
    sErro = sErro +  "Hora Inv?lida. \n";
  } 
  if (elemento.value.substring(3,5)>60) {  
  	berr = false  
    valid = false;  
    sErro = sErro +  "Minuto Inv?lido. \n";
  }
  return berr ;
}

//07
function verValor(elemento, nome) {
  if (elemento.value.length < 1) {
    valid = false;  
    sErro = sErro +  "O campo " + nome + " n?o pode ser vazio! \n";
  } 
  if (parseFloat(elemento.value)==0) {  
    valid = false;  
    sErro = sErro +  "O campo " + nome + " n?o pode ser igual a 0 \n";
  }
  return
}

//08
function verValFk(elemento, nome) {
  if (elemento.value == '0') {
    valid = false;  
    sErro = sErro +  "O campo " + nome + " n?o pode ser vazio! \n";
  } 
  return
}

//09
//Edita os campos digitados e retorna a m?scara v?lida
function edtCmp(obj,tam,tpId) {
 a = trim(obj.value) ;
 tpId +=""
 switch (tpId) {    
   case "CGC":
	 a=Tiraedt(a,15)  ;   
     a = a.substring(0,3)+"."+a.substring(3,6)+"."+a.substring(6,9)+"/"+a.substring(9,13)+"-"+a.substring(13,15)      
     break ;
   case "CPF":
     if (ValCPF(obj)) {
		a=Tiraedt(a,11)  ;   
     	a = a.substring(0,3)+"."+a.substring(3,6)+"."+a.substring(6,9)+"-"+a.substring(9,11)      	 
	 }
     break ;	   
   case "PROCESSO":
	 a=Tiraedt(a,11)  ;   
     a = a.substring(0,3)+"/"+a.substring(3,9)+"/"+a.substring(9,11)
     break ;	 
   case "CONTA CONTABIL":
	 a=Tiraedt(a,8)  ;   
     a = a.substring(0,1)+"."+a.substring(1,2)+"."+a.substring(2,3)+"."+a.substring(3,4)+"."+a.substring(4,6)+"."+a.substring(6,8)
     break ;	 
   case "INSCRICAO ESTADUAL":   
	 a=Tiraedt(a,9)  ;   
     a = a.substring(0,3)+"."+a.substring(3,6)+"."+a.substring(6,9)
     break ;
   case "INSCRICAO MUNICIPAL":
      a=Tiraedt(a,9)  ;   
       a = a.substring(0,3)+"."+a.substring(3,6)+"."+a.substring(6,9)
       break ;	 	 	 
   case "CEP":
	 a=Tiraedt(a,8)  ;   
     a = a.substring(0,5)+"-"+a.substring(5,8)
     break ;	 
   case "INSCRICAO IPTU":
	 a=Tiraedt(a,6)  ;   	   
     a = a.substring(0,5)+"-"+a.substring(5,6)
     break ;	 
   case "ORGAO":
	 a=Tiraedt(a,4)  ;      
     a = a.substring(0,2)+"."+a.substring(2,4)
     break ;
   case "HORA":
	 a=Tiraedt(a,4)  ;      
     a = a.substring(0,2)+":"+a.substring(2,4)
	 break ; 
   case "VALORES":
	 a=Edtnum(Numsedt(a))  ;      		  	   
     break ;	 
   default :
     // Outros 
	 a=Tiraedt(a,tam)  ;      		  
     break ;	 
 }      
  obj.value = a
}

//10
function allTrim(theString) {
  var aString = theString;
  var newString = "";
  var kBlank = " ";  
  if (aString.indexOf(kBlank) >= 0)  {
	for (var i = 0; i < aString.length; i++) {
        if (aString.charAt(i) != kBlank) break;
	}	
	for (var j =  aString.length-1; j >= 0; j--) {
		if (aString.charAt(j) != kBlank) break;
	}
	if (i == aString.length && j == -1)	newString = "";
	else {
		if (i != 0 || j != aString.length -1)	newString = trim(aString.substring(i, j+1));
		else   									newString = aString.charAt(i) + trim(aString.substring(i+1, j+1)) + aString.charAt(j+1);
	}
  }
  
  else newString = aString;			
  return newString;
}
function trim(theString) {
  var aString = theString;
  var newString = "";
  var kBlank = " ";  
  if (aString.indexOf(kBlank) >= 0)  {
	for (var i = 0; i < aString.length; i++) {
        if (aString.charAt(i) != kBlank) break;
	}	
	for (var j =  aString.length-1; j >= 0; j--) {
		if (aString.charAt(j) != kBlank) break;
	}
	if (i == aString.length && j == -1)	{
	newString = "";
	}
	else {
		if (i != 0 || j != aString.length -1)	newString = aString.substring(i, j+1);
		else   									newString = aString.charAt(i) + (aString.substring(i+1, j+1)) + aString.charAt(j+1);
	}
  }
  else newString = aString;			
  return newString;
}

//11
//Aceita somente numero
function f_num() {
   if (event.keyCode < 48 || event.keyCode > 57) 
      event.returnValue = false;
}

//12
//Aceita numeros e virgula
function f_num_vir() {
   if (event.keyCode != 44) { 
      if (event.keyCode < 48 || event.keyCode > 57) 
         event.returnValue = false;
   } 
}

//13
//Aceita numeros e ponto
function f_num_pont() {
   if (event.keyCode != 46) { 
      if (event.keyCode < 48 || event.keyCode > 57) 
         event.returnValue = false;
   } 
}

//14
//Aceita telefone (numeros, "-", "(" e ")") 
function f_tel() {
   if ( (event.keyCode != 45) && (event.keyCode != 40) && (event.keyCode != 41) ) { 
      if (event.keyCode < 48 || event.keyCode > 57) 
         event.returnValue = false;
   } 
}

//15
//Aceita letras (mai. e min.)
function f_let()
{
   if ( (event.keyCode < 65) || (event.keyCode > 90) )
   {
      if ( (event.keyCode < 97) || (event.keyCode > 122) )
      {
         if ( (event.keyCode != 32)  && (event.keyCode < 192)) 
            event.returnValue = false;
      }
   }
}

//16
//Enderecos (Numeros, Letras, "/", ",", "-","."
function f_end()
{
   if ( (event.keyCode < 65) || (event.keyCode > 90) )
   {
      if ( (event.keyCode < 97) || (event.keyCode > 122) )
      {
      	 if (event.keyCode < 44 || event.keyCode > 57) 
         {
            if ((event.keyCode != 32) && (event.keyCode < 192))       
               event.returnValue = false;
         }   
      }
   }
}

//17
//Hora (Numeros, "/", ".", "-",":")
function f_hora()
{
      	 if (event.keyCode < 48 || event.keyCode > 57) 
         {
            if ( (event.keyCode != 47) && (event.keyCode != 46) &&
                 (event.keyCode != 45) && (event.keyCode != 58) 
               )
               event.returnValue = false;
         }   
}

//18
//CGC (Numeros, "/", ".", "-")
function f_cgc()
{
      	 if (event.keyCode < 48 || event.keyCode > 57) 
         {
            if ( (event.keyCode != 47) && (event.keyCode != 46) &&
                 (event.keyCode != 45)  
               )
               event.returnValue = false;
         }   
}

//19 
function Tiraedt(Numcomedt,Tam) {
  var numsed = '' ;
  for (i=0; i<Numcomedt.length; i++) {
      ch = Numcomedt.charAt(i);
      if (ch < '0' || ch > '9') {continue ; } 
      numsed = numsed + ch ;
  }
  if (Tam!=0) {
	 for (i=numsed.length; i<Tam; i++) 	 {
	    numsed = '0'+numsed ;
	 }
	 if (numsed.length > Tam) numsed = numsed.substring(0,Tam)
  }
  return numsed ;
}

//20
function Numsedt(Numcedt) {
  var numsed = '' ;
  var temVirg = -1
  for (i=0; i<Numcedt.length; i++) {
      ch = Numcedt.charAt(i);
      if ((ch < '0' || ch > '9') && (ch != ',')) continue ; 	  
      if (ch == ',')  {
	 	  if (temVirg<0) temVirg = i;
		  else continue ;
      }
      numsed = numsed + ch ;
  }
  if (temVirg == 0)   {    	  
	  numsed  = numsed.substring(1,numsed.length) ;
	  if (numsed.length>2)  numsed = numsed.substring(0,2) ;
	  if (numsed.length==1) numsed +="0" ;	  
  }

  else   {
	  if (temVirg < 0)  numsed = numsed + '00' ;	  
	  else {
		  if (temVirg == (numsed.length-1)) numsed = numsed.substring(0,(numsed.length-1)) + '00'  ;
		  else {
			  if (temVirg == (numsed.length-2)) numsed = numsed.substring(0,temVirg) + numsed.substring(temVirg+1,temVirg+3) + "0"  ;  
			  else  {
				  if (temVirg <  (numsed.length-2)) numsed = numsed.substring(0,temVirg) + numsed.substring(temVirg+1,temVirg+3)  ;
			  }
		  }
	  }
  }
  return numsed ;
}

//21
function Edtnum(Numsedt) {
  var inputStr = Numsedt.toString() ;
/*  numced = string() ; */
  numced = '' ;
  if (inputStr.length == 0) {
      numced = '0,00' ;
  }
  if (inputStr.length == 1) {
      numced = '0,0'+inputStr ;
  }
  if (inputStr.length == 2) {
      numced = '0,'+inputStr ;
  }
  if (inputStr.length >= 3) {
     for (i=0; i<inputStr.length; i++) {
         ch = inputStr.charAt(i);
         j  = inputStr.length - i ;
         if (j == 3) {
            numced = numced + ch + ',' ; }
         else { if ((j == 6) || (j == 9) || (j == 12) || (j == 15)) {
                   numced = numced + ch + '.' ;     }   
                else { numced = numced + ch ;
                }         
         }
     }       
  }  
  return numced ;
}   

//22
function ValCGC(obj) {
/**
* cgc  -> campo caracter de 15 posicoes com o numero do CGC
*  retorno = true  - digito correto
*  retorno = false - digito invalido
*/
  /* editar a celula */
  valor = obj.value
  valor = Tiraedt(valor,15);  
  /* validar se cgc se preenchido */
  if (valor.length != 0) {  
     if (dig_cgc(valor) == false) {
         sErro = sErro +"CGC com digito verificador invalido. \n";
		 valid = false ;
         return false ;  	 
	 }
  }
  return true;
}

//23
function dig_cgc(cgcinf) {
  w = Tiraedt(cgcinf,15); 
  if (w.length < 15) { return false }   
  var dvcgc = w.substring(0,13);  
  var s1 = 0;
  for (i=0; i<5  ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(6-i)) }
  for (i=5; i<13 ; i++) { s1 = s1+(dvcgc.substring(i,i+1)*(14-i)) }  
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1 * 2
  for (i = 0; i < 6 ; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(7-i)) }
  for (i = 6; i < 13; i++) { s2 = s2 + (dvcgc.substring(i,i+1)*(15-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(13,15)
  return (oldDV == DV) ;
}

//24
function ValCPF(obj,forma) {
/**
* cpf  -> campo caracter de 11 posicoes com o numero do CPF
*  retorno = true  - digito correto
*  retorno = false - digito invalido
*  forma = 0 - mensagem direta
*  forma = 1 - mensagem coletiva em sErro
*/
  /* editar a celula */
  valor = trim(obj.value)
  if (valor.length != 0) {   
	  valor = Tiraedt(valor,11);   
  /* validar se cpf se preenchido */
     if (dig_cpf(valor) == false) { 
	 	 if (forma==0) {		 		 
		     alert("CPF com digito verificador invalido."+obj.value)
			 setTimeout("doSelection(document." + obj.form.name + ". " + obj.name + ")", 0) ;	  
	 	 }
		 else {	
		  	sErro = sErro + "CPF com digito verificador invalido."+obj.value+" \n"		 
		 }
         return false ;  	 
	 } 
	 obj.value = valor.substring(0,3)+"."+valor.substring(3,6)+"."+valor.substring(6,9)+"-"+valor.substring(9,11)      	 
  }
  return true;
}

//25
function dig_cpf(cpfinf) {
  w = Tiraedt(cpfinf,11); 
  if (w.length < 11) { return false }   
  var dvcpf = w.substring(0,9);    
  var s1 = 0;
  for (i=0; i<9  ; i++) { s1 = s1+(dvcpf.substring(i,i+1)*(10-i)) }
  r1 = s1 % 11
  if (r1<2) dv1 = 0
  else dv1 = 11 - r1  
  var s2 = dv1*2 
  for (i = 0; i < 9 ; i++) { s2 = s2 + (dvcpf.substring(i,i+1)*(11-i)) }
  r2 = s2 % 11
  if (r2<2) dv2 = 0
  else dv2 = 11 - r2
  var DV = "" + dv1 + dv2
  var oldDV = w.substring(9,11)
  return (oldDV == DV) ;
}

//26
function doSelection(fld) {
	fld.focus()
	fld.select()
    return;
}

//27
function ValDt(obj,forma) { 
  if (trim(obj.value).length == 0) return true ;
  var sdia = "00" ;
  var smes = "00" ;
  var sano = "0000" ; 
  if ( (obj.value.indexOf("/")>=0) || (obj.value.indexOf("-")>=0) || (obj.value.indexOf(":")>=0) || (obj.value.indexOf(" ")>=0) ){  
	  sdia = SeparaDt(obj,0) ;
	  smes = SeparaDt(obj,sdia.length+1) ;
	  sano = SeparaDt(obj,sdia.length+1+smes.length+1) ;  
  }
  else {
	  a = trim(obj.value) ;
	  if (trim(obj.value).length==4) {
	  	sdia = "0"+a.substring(0,1)
	  	smes = "0"+a.substring(1,2)
	  	sano = a.substring(2,4)				
	  }    
	  if (trim(obj.value).length==6) {
	  	sdia = a.substring(0,2)
	  	smes = a.substring(2,4)
	  	sano = a.substring(4,6)				
	  }    
	  if (trim(obj.value).length==8) {
	  	sdia = a.substring(0,2)
	  	smes = a.substring(2,4)
	  	sano = a.substring(4,8)				
	  }    	  
  }
  if (sdia.length > 2)  sdia = sdia.substring(0,2) ;
  if (sdia.length < 2)  sdia = "0"+sdia ;
  if (smes.length > 2)  smes = smes.substring(0,2) ;
  if (smes.length < 2)  smes = "0"+smes ;
  if (sano.length > 4)  sano = sano.substring(0,4) ;
  if (sano.length == 3) sano = "0"+sano ;
  if (sano.length == 1)  sano = "0"+sano ;   
  if (sano.length == 2) {
  	if (sano < 20) sano = "20"+sano ;
    else           sano = "19"+sano ;
  }
  var semErro = true;
  // validar datas
  if ((smes < 1) || (smes > 12)) semErro = false;
  if (((sano%4) == 0) && (smes == 02) && ((sdia < 01) || (sdia > 29))) semErro = false;
  if ((sano%4) != 0 && (smes == 02) && ((sdia < 01) || (sdia > 28)))   semErro = false;
  if (((smes == 04) || (smes == 06) || (smes == 09) || (smes == 11)) && ((sdia < 01) || (sdia > 30))) semErro = false;
  if (sdia > 31) semErro = false; 
  if (sano < 1900) semErro = false;   
  if (semErro) {
      obj.value = sdia+'/'+smes+'/'+sano ; 
  }
  else   {
  	  if (forma==0) {
	  	  alert("Data invalida: "+obj.value)
		  setTimeout("doSelection(document." + obj.form.name + ". " + obj.name + ")", 0)	  
  	  }
	  else {
	  	sErro = sErro + "Data invalida: "+obj.value+" \n"
	  }
  }
  return semErro ;
}
function SeparaDt(obj,ini) {
  spedaco = "" ; 
  x = obj.value
  for (i=ini; i < x.length; i++) {
      ch = x.charAt(i);
      if (ch == '/' || ch == ':' || ch == ' ' || ch == '-')  break
	  spedaco += ch	 
  }
  return spedaco
}

//28
function PegaDtHoje() {
   hoje = new Date() ;
   dia  = hoje.getDate();
   mes  = hoje.getMonth() + 1;
   dia   = dia+"";
   for (i=dia.length;i<2;i++) { dia="0"+dia; }      
   mes=mes+"";
   for (i=mes.length;i<2;i++) { mes="0"+mes; }      
   ano  = hoje.getYear();   
   return dia+"/"+mes+"/"+ano ;   
}

//29
function tornarMaiuscula ( objetoTexto )
{
	objetoTexto.value = objetoTexto.value.toUpperCase ();
}

//30
function tornarMinuscula ( objetoTexto )
{
	objetoTexto.value = objetoTexto.value.toLowerCase ();
}
//31
function montaWhereTexto (objTexto,nomVariavel,nomExterno)
{
	wWhere = ""
	a = trim(objTexto.value) ;	   
	if (a.length>0) { 
		wWhere = " and "+nomVariavel+" like '" + a +"'";  
		m  =  nomExterno+" igual(is) a '"+a+"' <br> "	
		if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
		else   		    	{ param1 += m ; numparam = 0 ; }
	}
	return wWhere
}	

//32
function montaWhereRadio ( objRadio,nomVariavel,nomExterno ) {
	wWhere = ""
	for (i=0; i<objRadio.length; i++) {
	    if (objRadio[i].checked) {
			wWhere  += " and "+nomVariavel+" = '" + objRadio[i].value +"'";
			m  =  nomExterno +" igual '"+objRadio[i].value+"'"; 
			if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
			else   		    	{ param1 += m ; numparam = 0 ; }
			break
	    }		
	}
	return wWhere
}	
//33
function montaWhereCGC ( objCGC,nomVariavel,nomExterno )
{
	wWhere = ""
	a = trim(objCGC.value) ;	   
	if (a.length>0) {  
		var numsed = '' ;
		for (i=0; i<a.length; i++) {
		    ch = a.charAt(i);
		    if (ch < '0' || ch > '9') continue ; 
		    numsed += ch ;
		}			
		wWhere = " and "+nomVariavel+" like '" + numsed +"'";  
		m  =  nomExterno+" igual(is) a '"+a+"' <br> "				
		if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
		else   		    	{ param1 += m ; numparam = 0 ; }
	}
	return wWhere			
}

//34
function montaWhereDt ( objDt,nomVariavel,nomExterno )
{
	wWhere = ""
	a = trim(objDt.value) ;	   
	if (a.length>0) {  
		wWhere = " and "+nomVariavel+" = " + "to_date("+a+" ,'dd/mm/yyyy')";  
		m  =  nomExterno+" igual(is) a '"+a+"' <br> "				
		if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
		else   		    	{ param1 += m ; numparam = 0 ; }
	}
	return wWhere			
}
//35
function montaWhereNum ( objNum,nomVariavel,nomExterno ) {
	wWhere = ""
	a = trim(objNum.value) ;
	if (a.length>0) {  
		var numsed = ''  ;
		var mcmd   = "" ;
		var flagprimNum = false ;   		
		for (i=0; i<a.length; i++) {
		    ch = a.charAt(i);
			if ((flagprimNum==false) && (ch=='=' || ch=='<' || ch=='>')) mcmd += ch ;
		    if (ch < '0' || ch > '9') continue ; 
		    flagprimNum=true
		    numsed += ch ;
		}
		if (mcmd=="") mcmd = "=" ;
		wWhere = " and "+nomVariavel+" "+mcmd+" " + numsed ;  
		m  =  nomExterno+" "+a+" <br> "				
		if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
		else   		    	{ param1 += m ; numparam = 0 ; }
	}
	return wWhere			
}
//36
function montaWhereCombo ( objCombo,nomVariavel,nomExterno )
{
	wWhere = ""
	a = objCombo.selectedIndex ;
	b = trim(objCombo.options[a].text) ;	  	  
	a = trim(objCombo.options[a].value) ;	  	  
	if ((a.length>0) && (a!=0)) {
		wWhere  += " and "+nomVariavel+" = " + a ;
		m  =  nomExterno +" igual "+b; 
		if (numparam == 0)  { param0 += m ; numparam = 1 ; }		
		else   		    	{ param1 += m ; numparam = 0 ; }
	}
	return wWhere
}

//Aceita numeros, virgula e ponto
function f_moeda() {
   if ((event.keyCode != 44) && (event.keyCode != 46)){ 
      if (event.keyCode < 48 || event.keyCode > 57) 
         event.returnValue = false;
   } 

}




function Mascaras(Objeto, Masc)
{ 
  //  'X' Qualquer Caracter da tabela ASCII Ex: L 9 , . / $ +
  //  'A' S? Letras
  //  '9' S? N?meros
  //  'D' Delimitador  Ex: ! @ # $ % ? & * ( ) _ - _ -

  Masc = Masc.toLocaleUpperCase();

  var Tecla = event.keyCode;         //pega a tecla
  VetMasc = new Array(Masc.length);   //cria o vetor da mascara

  SizeObjeto = Objeto.value.length;   //pega o tamanho da text

  //tamanho Objt >= tamanho mascara
  if( SizeObjeto >= Masc.length ) {
      event.returnValue = false;
      return;
  }

  //preenche o vetor com a mascara  '9','A','. / ( ) S V','X'
  for( i=0; i < Masc.length; i++ ) { VetMasc[i] = Masc.substring( i,i+1);}

  switch( VetMasc[SizeObjeto] )
  {
         case '9': 
                   event.returnValue = ProcNumero(Tecla)
                   break
         case 'A': 
                   event.returnValue = ProcLetra(Tecla)
                   break
         case 'X': 
                   event.returnValue = true;
                   break
         default:  
                   if( Tecla == VetMasc[SizeObjeto].charCodeAt() )
                       event.returnValue = true;
                   else 
                   { 
                       while(SizeObjeto < Masc.length - 1)
                       {
                             SizeObjeto = Objeto.value.length;   //pega o tamanho da text
                             if( ((VetMasc[SizeObjeto] != '9' && VetMasc[SizeObjeto] != 'A') && VetMasc[SizeObjeto] != 'X') )
                                   Objeto.value += VetMasc[SizeObjeto];
                             else
                                  break
                       }
                       // se a masc for "N-----N" e eu clicar em um letra 
                       // nao mostra a letra clicada so 9----- este bloco trata isso
                       switch( VetMasc[SizeObjeto] )
                       {
                               case '9':
                                         event.returnValue = ProcNumero(Tecla)
                                         break
                               case 'A':
                                         event.returnValue = ProcLetra(Tecla)
                                         break
                               case 'X': 
                                         event.returnValue = true;
                               break
                               default:  
                                         event.returnValue = false;
                      }

                   }
  }
}

//S? Numeros
function ProcNumero(Tecla1)
{ 
  return( (Tecla1 >= 48) && (Tecla1 <= 57) ) 
}

//S? Letras
function ProcLetra(Tecla1)
{ 
  return( ((Tecla1 >= 65) && (Tecla1 <= 90 )) || ((Tecla1 >= 97) && (Tecla1 <= 122)) ) 
}
function MaxTextarea(Objeto, Max) { 
  SizeObjeto = Objeto.value.length;   //pega o tamanho da text
  //tamanho Objt >= Maximo
  event.returnValue = true;
  if( SizeObjeto >= Max ) { event.returnValue = false; }
  return;
}

function FormataReais(Objeto, ponto, virgula, evento)
{
   var key = aux = aux2 = '';
   var i = j = sep = 0;
   var len = len2 = 0;
   var strCheck = '0123456789';

   var whichCode = (window.Event) ? evento.which : evento.keyCode;

   //se o tamanho do obj > tamanho mascara
   if( Objeto.value.length > 15 ) {
       evento.returnValue = false;
       return;
   }
   if (whichCode == 13) return true;
   key = String.fromCharCode(whichCode);  // Valor para o c?digo da Chave
   if (strCheck.indexOf(key) == -1) return false;  // Chave inv?lida
   len = Objeto.value.length;
   for(i = 0; i < len; i++)
       if ((Objeto.value.charAt(i) != '0') && (Objeto.value.charAt(i) != virgula) )
            break;
   aux = '';
   for(; i < len; i++)
      if (strCheck.indexOf(Objeto.value.charAt(i))!=-1) 
          aux += Objeto.value.charAt(i);

   aux += key;
   len = aux.length;
   if (len == 0) Objeto.value = '';
   if (len == 1) Objeto.value = '0'+ virgula + '0' + aux;
   if (len == 2) Objeto.value = '0'+ virgula + aux;
   if (len > 2)
   {
       aux2 = '';
       for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += ponto;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
       }
       Objeto.value = '';
       len2 = aux2.length;
       for (i = len2 - 1; i >= 0; i--)
            Objeto.value += aux2.charAt(i);

       Objeto.value += virgula + aux.substr(len - 2, len);
   }
   return false;
}

function ValorExtenso(tipo, idvet)
{
  var aUnid = new Array("um ","dois ","tres ","quatro ","cinco ","seis ","sete ",
                        "oito ","nove ","dez ","onze ","doze ","treze ","quatorze ",
                        "quize ","dezesseis ","dezessete ","dezoito ","dezenove ");

  var aDezena = new Array("dez ","vinte ","trinta ","quarenta ","cinquenta ",
                          "sessenta ","setenta ","oitenta ","noventa ");

  var aCentena = new Array("cento ","duzentos ","trezentos ","quatrocentos ","quinhentos ",
                           "seiscentos ","setecentos ","oitocentos ","novecentos ");

  var aMilhar = new Array("milh?o ","milh?es ","bilh?o ","bilh?es ");

  switch( tipo )
  {
         case 1: return aUnid[idvet];
                   break
         case 2: return aDezena[idvet];
                   break
         case 3: return aCentena[idvet];
                   break
         case 4: return aMilhar[idvet];
                   break
   }
}


function Extenso(nValor, moeda)
{ 
  var Real = "", Reais = "", cParte = "";
  var nContador = 0, nTamanho = 0, cValor = 0, cParte = 0, cFinal = 0;
  var aGrupo = Array(5);
  var aTexto = Array(5);

  var numsed = "";
  var contVirg = 0;
  var contNum = 0;
  var cont = 0;

  for (i=0; i < nValor.length; i++)
  {
      ch = nValor.charAt(i);
      if( (ch >= 0) || (ch <= 9) ) {
           contNum =1;
           if(contVirg != 1) numsed += ch;
           else
              if(cont < 2) {
                 numsed += ch;
                 cont += 1; 
              }
      }
      else
      if( ((ch == ',') && (contNum != 0)) && contVirg != 1) {
             contVirg = 1;
             numsed += ch;
      }
  }

  nValor = numsed;

  if(moeda){Real = "real"; Reais = "reais"; }

  if( nValor.length > 15 || nValor == "" || nValor == 0 ) return "Zero";

  if( nValor.indexOf(",") >= 0 ) {
      cValor = nValor.substring( 0, nValor.indexOf(",") );
      if(nValor.length - nValor.indexOf(",") > 2 ) 
         cValor += nValor.substring( nValor.indexOf(","), nValor.length );
      else {
            cValor = nValor;
            for( var ix = nValor.length - nValor.indexOf(","); ix <= 2; ix++ ) {
                 cValor += "0";
            }
      }
  }
  else 
       cValor = nValor + ",00";

  //----------------------------------------------------------------------------
  for(var ix = 16 - cValor.length; cValor.length < 16; ix++)    {cValor = "0" + cValor; }
     
  aGrupo[0] = cValor.substring (1, 4); 
  aGrupo[1] = cValor.substring (4, 7);
  aGrupo[2] = cValor.substring (7, 10);
  aGrupo[3] = cValor.substring (10, 13);
  aGrupo[4] = "0" + cValor.substring (14,16);


  for(var nContador = 0; nContador < 5; nContador++){aTexto[nContador] = "";}
  for(var nContador = 0; nContador < 5; nContador++)
  {
          cParte = aGrupo[nContador];
          if( cParte < 10 )  nTamanho = 1;
          else {
                if( cParte < 100 )  nTamanho = 2;
                else
                    nTamanho = 3;
         }
         if( nTamanho == 3 ) {
             if( cParte.substring(1,3) != "00" ) {
                 aTexto[nContador] += ValorExtenso(3, cParte.substring(0,1)-1) + " e ";
                 nTamanho = 2;
             }
             else {
                  if(cParte.substring(0,1) == "1") 
                     aTexto[nContador] += "cem ";
                  else 
                      aTexto[nContador] += ValorExtenso(3, cParte.substring(0,1)-1);
             }
          }
          if( nTamanho == 2){
              if( cParte.substring(1,3) < 20 ) 
                  aTexto[nContador] += ValorExtenso(1, cParte.substring(1,3)-1);
              else {
                   aTexto[nContador] += ValorExtenso(2, cParte.substring(1,2)-1);
                   if( cParte.substring(2,3) != "0" ) {
                       aTexto[nContador] = aTexto[nContador] + " e ";
                       nTamanho =1;
                   }
              }
          }
          if( nTamanho == 1)  {
              if( cParte.substring(2,3) != 0 ) 
                  aTexto[nContador] +=ValorExtenso(1, cParte.substring(2,3)-1);
          }
          if( aGrupo[0] + aGrupo[1] + aGrupo[2] + aGrupo[3] == 0 && aGrupo[4] != 0 ) {
              if( moeda )
              {
                  cFinal = aTexto[3] + "Centavo";
                  if( aGrupo[4] != 1 )
                      cFinal += "s";
              }
              else
                   cFinal= "ponto" + aTexto[3];
          }
          else {
                cFinal = "";
//--------------------esse grupo e dos bilhoes----------------------------------
                if( aGrupo[0] != 0 ) {  
                    cFinal = cFinal + aTexto[0];
                    if( aGrupo[0] == 1 )
                        cFinal += ValorExtenso(4,2); //bilhao
                    else
                        cFinal += ValorExtenso(4,3);//bilhoes
                }
                else
                     cFinal = cFinal + "";
//---------------------esse grupo e dos milhao----------------------------------
                if( aGrupo[1] != 0 ) {         
                    cFinal = cFinal + aTexto[1];
                    if( aGrupo[1] == 1 )
                        cFinal += ValorExtenso(4,0);//milhao
                    else
                         cFinal += ValorExtenso(4,1);//milhoes
                }
                else
                     cFinal = cFinal + "";
//--------------------esse grupo e do milhar------------------------------------
                if( aGrupo[2] != 0 )  
                    cFinal +=""+aTexto[2]+"mil ";
                else 
                     cFinal = cFinal+"";
//-----------------esse grupo e das centenas------------------------------------
                cFinal = cFinal + aTexto[3]; 
                if( aGrupo[0] + aGrupo[1] + aGrupo[2] + aGrupo[3] != 0 ) {
                   if( aGrupo[0] + aGrupo[1] + aGrupo[2] + aGrupo[3] == 1 )  
                       cFinal += " "+ Real +" ";
                   else
                        cFinal +=" "+ Reais +" ";
                }
//-------------------esse grupo e das dezenas----------------------------------
                if( aGrupo[4] != 0 ) {     
                    if(moeda) {
                       cFinal = cFinal + " e " + aTexto[4]+ "centavo";
                       if( aGrupo[4]!= 1 )
                           cFinal += "s";
                    }
                    else
                         cFinal = cFinal + "ponto" + aTexto[4];
               }
               else
                    cFinal = cFinal + "";
//------------------------------------------------------------------------------
      }
  }

      return cFinal;
}


function FormataReais(Objeto, ponto, virgula, evento)
{
   var key = aux = aux2 = '';
   var i = j = sep = 0;
   var len = len2 = 0;
   var strCheck = '0123456789';

   var whichCode = (window.Event) ? evento.which : evento.keyCode;

   //se o tamanho do obj > tamanho mascara
   if( Objeto.value.length > 15 ) {
       evento.returnValue = false;
       return;
   }
   if (whichCode == 13) return true;
   key = String.fromCharCode(whichCode);  // Valor para o c?digo da Chave
   if (strCheck.indexOf(key) == -1) return false;  // Chave inv?lida
   len = Objeto.value.length;
   for(i = 0; i < len; i++)
       if ((Objeto.value.charAt(i) != '0') && (Objeto.value.charAt(i) != virgula) )
            break;
   aux = '';
   for(; i < len; i++)
      if (strCheck.indexOf(Objeto.value.charAt(i))!=-1) 
          aux += Objeto.value.charAt(i);

   aux += key;
   len = aux.length;
   if (len == 0) Objeto.value = '';
   if (len == 1) Objeto.value = '0'+ virgula + '0' + aux;
   if (len == 2) Objeto.value = '0'+ virgula + aux;
   if (len > 2)
   {
       aux2 = '';
       for (j = 0, i = len - 3; i >= 0; i--) {
            if (j == 3) {
                aux2 += ponto;
                j = 0;
            }
            aux2 += aux.charAt(i);
            j++;
       }
       Objeto.value = '';
       len2 = aux2.length;
       for (i = len2 - 1; i >= 0; i--)
            Objeto.value += aux2.charAt(i);

       Objeto.value += virgula + aux.substr(len - 2, len);
   }
   return false;
}
function CopiaSelect(objOri,objDest,isTodos) {
<!-- // Copia o objOri para objDest  
//	    se isTodos e verdadeiro entao copia TODOS
//      senao apaga somente os selecionados-->
	for (i=0;i<objOri.length;i++) {	
		if ((isTodos) || (objOri.options[i].selected)) {	
			objDest.options[objDest.options.length]=new Option(objOri.options[i].text,objOri.options[i].value);
		}
	}	
}
function ApagaSelect(objOri,isTodos) {
<!-- Apaga no objOri todas as opcoes -->
<!--     se isTodos e verdadeiro entao apaga TODOS-->
<!--     senao apaga somente os selecionados-->
	if (isTodos) objOri.options.length = 0 ;
	else {
		for (i=objOri.options.length-1;i>=0;i--) {	
			if (objOri.options[i].selected)	objOri.options[i]=null ;
		}	
	}
}

function SelecionaSelect(objOri,objDest,isTodos) {
<!-- Selecona no objDest todas as opcoes que possuem o mesmo value e estao no objOri -->
<!--     se isTodos e verdadeiro entao seleciona TODOS  -->
<!--     senao selecona somente os selecionados no objOri-->
	for (i=0;i<objOri.options.length;i++) {	
		if ((isTodos) || (objOri.options[i].selected)) {
			for (k=0;k<objDest.options.length;k++) {	
				if (objOri.options[i].value==objDest.options[k].value) {			
					objDest.options[k].selected=true;
				}
			}
		}
	}	
}
function DeselecionaSelect(objOri,objDest,isTodos) {
<!-- DesSelecona no objDest todas as opcoes que possuem o mesmo value e estaonobjOri -->
<!--     se isTodos e verdadeiro entao deseleciona TODOS -->
<!--     senao deseleciona somente os selecionados no objOri-->
	for (i=0;i<objOri.options.length;i++) {	
		if ((isTodos) || (objOri.options[i].selected)) {
			for (k=0;k<objDest.options.length;k++) {	
				if (objOri.options[i].value==objDest.options[k].value) {
					objDest.options[k].selected=false;
				}
			}
		}
	}	
}

// Para rollover de imagens

function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}

function LimitarAnexo(Arquivo, Extensoes) {
	retorno = false;
	
	if (!Arquivo) return;
	while (Arquivo.indexOf("\\") != -1)
		Arquivo = Arquivo.slice(Arquivo.indexOf("\\") + 1);
	ext = Arquivo.slice(Arquivo.indexOf(".")).toLowerCase();
	for (var i = 0; i < Extensoes.length; i++) {
		if (Extensoes[i] == ext) {
			retorno = true;
			break; 
		}
	}

	if (!retorno) {
		if (Extensoes.length > 1)
			alert("Apenas ? permitida a sele??o de arquivos dos seguintes tipos: " 
				+ (Extensoes.join("  ")) + "\nPor favor selecione outro arquivo.");
		else
			alert("Apenas ? permitida a sele??o de arquivos do tipo '" 
				+ (Extensoes.join("  ")) + "'\nPor favor selecione outro arquivo.");
	}
	return retorno;
}

// In?cio da Fun??o 54

/*
M?todo: 	ColorTable()
Descri??o:	Colore linha a linha uma tabela, com as vari?veis COR1 e COR2
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function ColorTable(Table, QtdRows, cor1, cor2){
	for(var i=0; i < QtdRows; i++){
		if( (i%2) == 0 ){
			Table.rows[i].style.background = cor1
			Table.rows[i].bgColor = cor1
		}
		else{
			Table.rows[i].style.background = cor2
			Table.rows[i].bgColor = cor2
		}
	}
}

/*
M?todo: 	Sort_String()
Descri??o:	Ordena as linhas de uma tabela de forma crescente ou decrescente, de acordo com a 
			vari?vel global ord_asc. A ordena??o ? feita para dados do tipo Date.
Par?metros:	Table: Tabela a ser ordenada
			QtdRows: N?mero de linhas da tabela
			Posicao: Coluna a ser usada a regra de ordena??o 
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function Sort_String(Table,QtdRows,Posicao){
	var retorno = 0
	for(var i = 0; i < (QtdRows - 1); i++){
		for(var x = (i + 1); x < QtdRows; x++){
			if(ord_asc == 1){
				if( ( (Table.rows[i].cells[Posicao].innerHTML).localeCompare(Table.rows[x].cells[Posicao].innerHTML) == 0) ){
					for(var y = i; (y < x) && ( (Table.rows[i].cells[Posicao].innerHTML).localeCompare(Table.rows[y].cells[Posicao].innerHTML) == 0); y++);
					if(y == x)
						continue
					Table.moveRow(i+1,x)
					Table.moveRow(x-1,i+1)
					retorno = 1
				}
				else if( (Table.rows[i].cells[Posicao].innerHTML).localeCompare(Table.rows[x].cells[Posicao].innerHTML) > 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
					retorno = 1
				}
			}
			else{
				if( (Table.rows[i].cells[Posicao].innerHTML).localeCompare(Table.rows[x].cells[Posicao].innerHTML) < 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
				}
			}
		}
	}
	return retorno
}

/*
M?todo: 	convertDatetoString()
Descri??o:	Fun??o converte uma vari?vel no formato de data em string para o tipo Date
Par?metros:	String: Vari?vel que ter? seu conteudo transformado em Date
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function convertDatetoString(string){
	var data = new Date
	if( parseInt(string.substring(0,1)) == 0 ){
		data.setDate( parseInt(string.substring(1,2)) )
	}
	else{
		data.setDate( parseInt(string.substring(0,2)) )
	}
	
	data.setMonth( parseInt(string.substring(3,5)) - 1 )
	data.setYear( parseInt(string.substring(6,10)) )
	return data
}

/*
M?todo: 	Sort_Date()
Descri??o:	Ordena as linhas de uma tabela de forma crescente ou decrescente, de acordo com a 
			vari?vel global ord_asc. A ordena??o ? feita para dados do tipo Date.
Par?metros:	Table: Tabela a ser ordenada
			QtdRows: N?mero de linhas da tabela
			Posicao: Coluna a ser usada a regra de ordena??o 
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function Sort_Date(Table,QtdRows,Posicao){
	var first, second 
	var retorno = 0
	for(var i = 0; i < (QtdRows - 1); i++){
		for(var x = (i + 1); x < QtdRows; x++){
			first = convertDatetoString(Table.rows[i].cells[Posicao].innerHTML)
			second = convertDatetoString(Table.rows[x].cells[Posicao].innerHTML)
			if(ord_asc == 1){
				if( (first == second) ){
					for(var y = i; (y < x) && (first == convertDatetoString(Table.rows[y].cells[Posicao].innerHTML)); y++);
					if(y == x)
						continue
					Table.moveRow(i+1,x)
					Table.moveRow(x-1,i+1)
					retorno = 1
				}
				else if( first > second ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
					retorno = 1
				}
			}
			else{
				if( first < second ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
				}
			}
		}
	}
	return retorno 
}


/*
M?todo: 	Sort_String()
Descri??o:	Ordena as linhas de uma tabela de forma crescente ou decrescente, de acordo com a 
			vari?vel global ord_asc. A ordena??o ? feita para dados do tipo Date.
Par?metros:	Table: Tabela a ser ordenada
			QtdRows: N?mero de linhas da tabela
			Posicao: Coluna a ser usada a regra de ordena??o 
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function Sort_InputText(Table, Objeto, Posicao){
	var retorno = 0
	for(var i = 0; Objeto[i] != null; i++){
		if( (Objeto[i].value).localeCompare('') == 0 )
				continue
		for(var x = (i + 1); Objeto[x] != null; x++){
			if(ord_asc == 1){
				if( (Objeto[x].value).localeCompare('') == 0 )
					continue
				if( ( (Objeto[i].value).localeCompare(Objeto[x].value) == 0) ){
					for(var y = i; (y < x) && ( (Objeto[i].value).localeCompare(Objeto[y].value) == 0); y++);
					if(y == x)
						continue
					Table.moveRow(i+1,x)
					Table.moveRow(x-1,i+1)
					retorno = 1
				}
				else if( (Objeto[i].value).localeCompare(Objeto[x].value) > 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
					retorno = 1
				}
			}
			else{
				if( (Objeto[i].value).localeCompare(Objeto[x].value) < 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
				}
			}
		}
	}
	return retorno
}

/*
M?todo: 	Sort_String()
Descri??o:	Ordena as linhas de uma tabela de forma crescente ou decrescente, de acordo com a 
			vari?vel global ord_asc. A ordena??o ? feita para dados do tipo Date.
Par?metros:	Table: Tabela a ser ordenada
			QtdRows: N?mero de linhas da tabela
			Posicao: Coluna a ser usada a regra de ordena??o 
Autor:		Sergio Roberto Junior
Data:		08/07/2004
Vers?o:		1.0 
*/
function Sort_InputDate(Table, Objeto, Posicao){
	var retorno = 0
	for(var i = 0; Objeto[i] != null; i++){
		if( (Objeto[i].value).localeCompare('') == 0 )
			continue
		for(var x = (i + 1); Objeto[x] != null; x++){
			if(ord_asc == 1){
				if( (Objeto[x].value).localeCompare('') == 0 )
					continue
				if( ( (Objeto[i].value).localeCompare(Objeto[x].value) == 0) ){
					for(var y = i; (y < x) && ( (Objeto[i].value).localeCompare(Objeto[y].value) == 0); y++);
					if(y == x)
						continue	
					Table.moveRow(i+1,x)
					Table.moveRow(x-1,i+1)
					retorno = 1
				}
				else if( (Objeto[i].value).localeCompare(Objeto[x].value) > 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
					retorno = 1
				}
			}
			else{
				if( (Objeto[i].value).localeCompare(Objeto[x].value) < 0 ){
					Table.moveRow(i,x)
					Table.moveRow(x-1,i)
				}
			}
		}
	}
	return retorno
}

/*
M?todo: 		Classifica()
Descri??o:		Fun??o ? chamada por uma a??o do usu?rio e realiza as chamadas necess?ria para ordenar e recolorir
				uma tabela.
Par?metros:		Table: Tabela a ter suas linhas ordenadas
				Posicao: Coluna a ser usada a regra de ordena??o 
				Tipo: Tipo do dado da coluna, que ser? ordenada. (Date, String)
				Cor1: Cor a ser usada para as linhas ?mpares da tabela
				Cor2: Cor a ser usada para as linhas pares da tabela
Autor:			Sergio Roberto Junior
Data Cria??o:	08/07/2004
?ltima Atualiz:	12/07/2004
Vers?o:			2.0 
*/
function Classifica(Table, Objeto, Posicao , tipo, cor1, cor2) {
	
	ord_asc = 1
	// Direciona qual o m?todo ir? ser executado de acordo com o tipo do dado a ser verificado
	switch (tipo){
		case 'date':
			if( Sort_Date(Table,Table.rows.length,Posicao) == 0 ){
				ord_asc = 0 
				Sort_Date(Table,Table.rows.length,Posicao)
			}
			break
		case 'string':
			if( Sort_String(Table,Table.rows.length,Posicao) == 0 ){
				ord_asc = 0 
				Sort_String(Table,Table.rows.length,Posicao)
			}
			break
		case 'input_text':
			if( Sort_InputText(Table, Objeto, Posicao) == 0 ){
				ord_asc = 0
				Sort_InputText(Table, Objeto, Posicao)
			}
			break
		case 'input_date':
			if( Sort_InputDate(Table, Obejto, Posicao) == 0 ){
				ord_asc = 0 
				Sort_InputDate(Table, Obejto, Posicao)
			}
			break
	}
	
	// Re-colore a tabela resultante, ap?s a re-ordena??o
	ColorTable(Table,Table.rows.length, cor1, cor2)
}

// Fim da Fun??o 54

//24
function ValNumProcesso(numProc) {
/**
* NumProcesso  -> Enn/nnnnnn/bbbb/aaaa
*  variavel global valid = true   - sem erro
*  variavel global valid = false  - com erro
*  variavel global sErro = mensagens de erro, se existir
*  retorno = numero de processo editado - se sem erro
*  retorno = numero de processo original - se com erro
*/
    valid = true ;
	sErro = "" ;
	var today =new Date();
	var ano = today.getYear();
	var primPos = "";
	var proc = numProc.value;
	// Verifico processo esta preenchido
	if (proc.length==0) {
		  sErro += "Processo n?o informado. \n"
		  valid = false ;
	}else if(proc.length==20){
		valid==true
		return valid ;
	}
	else {
	    primPos = proc.substring(0,1).toUpperCase();
		if ( "ABCDEFGHIJKLMNOPQRSTUVXZYW".indexOf(primPos)<0 ) {
		  sErro += "A primeira posi??o do processo tem que ser uma letra"
		  valid = false ;
		}	
	}
	if (valid==true){ 	    
		// separando os blocos
		var p1="";
		var p2="";
		var p4="";

		var posB1=-1;
		var posB2=-1;
		var posB3=-1;
		posB1=proc.indexOf('/');
		if (posB1>0) posB2=proc.indexOf('/',posB1+1);
		if (posB2>0) posB3=proc.indexOf('/',posB2+1);	
		if ( (posB1<0) || (posB2<0) ||(posB3<0))    {
		  sErro += "Formato do processo invalido. Tente 'E12/123456/ /2007'"
		  valid = false ;
		}
		if (valid==true) {
			//Monto os peda?os e conto as barras			
    	    p1 = proc.substring(0,posB1);
    	    if (p1.length>1) p1 = primPos+p1.substring(1,p1.length);
    	    else if (p1.length==1) p1=primPos ;
       	    if (posB2>posB1+1) p2 = proc.substring(posB1+1,posB2);
       	    p2 = "000000"+p2;
       	    p2 = p2.substring(p2.length-6,p2.length);
       	    var p2Num = parseInt(p2); 
   	       
       	    if (isNaN(p2Num)) {
				sErro += "Numero do Processo n?o numerico.Tente 'E12/123456/ /2007' \n";
				valid = false ;	
			}
    	    if (proc.length > posB3+1) p4 = proc.substring(posB3+1,proc.length);
       	    var p4Num = parseInt(p4); 
			if ( (isNaN(p4Num)) || (p4<2000) || (p4>ano) ) {
				sErro += "Ano do Processo inv?lido. \n"
				valid = false ;	
			}	
		}				
	}		
	if (valid==false){
	  alert(sErro) ;
	  setTimeout("doSelection(document." + obj.form.name + ". " + obj.name + ")", 0) ;	  	
	  }
	else {
		numProc.value = p1+"/"+p2+"/    /"+p4;
	}
	return valid ;	
}

